Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Reports
Imports BACRM.UserInterface.DropDownListAdapter
Imports BACRM.UserInterface.Reports

Namespace BACRM.UserInterface.Reports
    Partial Public Class frmCustomReportWiz : Inherits BACRMPage

        Dim CustReportId As Long = 0

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            Try
                If Not IsPostBack Then

                    LoadDropDowns()
                    If GetQueryStringVal("ReportId") <> "" Then
                        CustReportId = GetQueryStringVal("ReportId")
                    End If
                    calTo.SelectedDate = DateAdd(DateInterval.Day, 0, Now())
                    txtReportId.Text = CustReportId
                    BindSchedulerGrid()
                    ddlNoRows.Items.FindByValue(0).Selected = True
                    Session("dsKPI") = Nothing
                End If

                If chkLinerGrid.Checked = True Then
                    radOppTab.Tabs(3).Visible = False
                Else : radOppTab.Tabs(3).Visible = True
                End If

                ddlModule.Attributes.Add("onchange", "CheckDropDown()")
                'btnBuildFields.Attributes.Add("onclick", "return OrderList()")
                'btnBuildListItems.Attributes.Add("onclick", "return OrderList()")
                btnBuildFilterList.Attributes.Add("onclick", "return OrderList()")
                btnBuildFilterList.Attributes.Add("onclick", "return OrderList()")
                btnAddFilterRow.Attributes.Add("onclick", "return OrderList()")
                btnBuildSummation.Attributes.Add("onclick", "return OrderList()")
                ' btnAddSummationRow.Attributes.Add("onclick", "return OrderList()")
                btnReport.Attributes.Add("onclick", "return CheckOrderListLength()")
                btnSave.Attributes.Add("onclick", "return Check()")
                btnSaveClose.Attributes.Add("onclick", "return Check()")
                btnAddKPI.Attributes.Add("onclick", "return CheckKPI()")
                btnNewScheduler.Attributes.Add("onclick", "return openScheduler('0','" & txtReportId.Text & "')")
                Dim ddlGrp1List As New DropDownList
                ddlGrp1List.CssClass = "form-control"
                ddlGrp1List.ID = "ddlGrp1List"
                ddlGrp1List.Attributes.Add("onchange", "return ChangeEd()")
                spnGrp1List.Controls.Add(ddlGrp1List)
                hplAdvFilter.Attributes.Add("onClick", "return AdvFltCheck('0')")
                hplClrAdvFilter.Attributes.Add("onClick", "return AdvFltCheck('1')")
                lstReportColumnsOrder.Attributes.Add("onChange", "return GetName()")
                If CInt(txtReportId.Text) = 0 Then
                    btnNewScheduler.Visible = False
                Else : btnNewScheduler.Visible = True
                End If

                Dim dtTable As DataTable
                Dim StdFilter As String = "0"
                If Not IsPostBack And CInt(txtReportId.Text) <> 0 Then
                    Dim objReports As New CustomReports

                    objReports.ReportID = CustReportId
                    objReports.DomainID = Session("DomainId")
                    dtTable = objReports.getReportDetails()
                    If dtTable.Rows.Count > 0 Then
                        If Not ddlModule.Items.FindByValue(dtTable.Rows(0).Item("numModuleId")) Is Nothing Then
                            ddlModule.ClearSelection()
                            ddlModule.Items.FindByValue(dtTable.Rows(0).Item("numModuleId")).Selected = True
                            bindGroups()
                        End If

                        If Not lstGroup.Items.FindByValue(dtTable.Rows(0).Item("numGroupId")) Is Nothing Then
                            lstGroup.ClearSelection()
                            lstGroup.Items.FindByValue(dtTable.Rows(0).Item("numGroupId")).Selected = True
                        End If
                        txtchkAdvFilter.Text = IIf(IsDBNull(dtTable.Rows(0).Item("FilterType")), 0, dtTable.Rows(0).Item("FilterType"))
                        txtAdvFilter.Text = IIf(IsDBNull(dtTable.Rows(0).Item("AdvFilterStr")), "", dtTable.Rows(0).Item("AdvFilterStr"))
                        txtSql.Text = dtTable.Rows(0).Item("textQuery")
                        txtReportName.Text = dtTable.Rows(0).Item("vcReportName")
                        txtReportDesc.Text = dtTable.Rows(0).Item("vcReportDescription")
                        If dtTable.Rows(0).Item("bitFilterRelAnd") = True Then
                            radAnd.Checked = True
                            radOr.Checked = False
                        Else
                            radAnd.Checked = False
                            radOr.Checked = True
                        End If

                        If Not ddlNoRows.Items.FindByValue(dtTable.Rows(0).Item("bintRows")) Is Nothing Then
                            ddlNoRows.ClearSelection()
                            ddlNoRows.Items.FindByValue(dtTable.Rows(0).Item("bintRows")).Selected = True
                        End If

                        If Not ddlCustomTime.Items.FindByValue(dtTable.Rows(0).Item("custFilter")) Is Nothing Then
                            ddlCustomTime.ClearSelection()
                            ddlCustomTime.Items.FindByValue(dtTable.Rows(0).Item("custFilter")).Selected = True
                        End If
                        If Not dtTable.Rows(0).Item("FromDate") = "1900-01-01 00:00:00.000" Then
                            calFrom.SelectedDate = dtTable.Rows(0).Item("FromDate")
                            calTo.SelectedDate = dtTable.Rows(0).Item("ToDate")
                        End If
                        If dtTable.Rows(0).Item("bitGridType") = True Then
                            chkLinerGrid.Checked = True
                            chkSummationGrid.Checked = False
                        Else
                            chkSortRecCount.Checked = dtTable.Rows(0).Item("bitSortRecCount")
                            chkDrllDownRpt.Checked = dtTable.Rows(0).Item("bitDrillDown")
                            chkLinerGrid.Checked = False
                            chkSummationGrid.Checked = True
                            If Not ddlGrp1Order.Items.FindByValue(dtTable.Rows(0).Item("varGrpOrd")) Is Nothing Then
                                ddlGrp1Order.ClearSelection()
                                ddlGrp1Order.Items.FindByValue(dtTable.Rows(0).Item("varGrpOrd")).Selected = True
                            End If
                            If Not ddlGrp1Filter.Items.FindByValue(dtTable.Rows(0).Item("varGrpflt")) Is Nothing Then
                                ddlGrp1Filter.ClearSelection()
                                ddlGrp1Filter.Items.FindByValue(dtTable.Rows(0).Item("varGrpflt")).Selected = True
                            End If
                            Textbox1.Text = dtTable.Rows(0).Item("textQueryGrp")
                            radOppTab.Tabs(3).Visible = True
                        End If
                        StdFilter = dtTable.Rows(0).Item("varstdFieldId")
                    End If
                End If

                BuildSummationTab()
                BindReportFields()
                BindGroupingList()
                BuildListItems()
                BuildListItemsOrdered()
                BuildFilterList()

                If txtReportId.Text = "0" Then
                    btnExportPdf.Visible = False
                Else : btnExportPdf.Visible = True
                End If
                If CInt(txtReportId.Text) <> 0 Then btnBuildFilterList.Attributes.Add("onclick", "return ChangeTabFIndex()")
                If Not IsPostBack And CInt(txtReportId.Text) <> 0 Then
                    If dtTable.Rows.Count > 0 Then
                        BindSelectedFields()
                        BindOrderList()
                        BindfilterLists()
                        BindSummationLists()
                        If chkSummationGrid.Checked = True Then
                            If Not CType(radOppTab.MultiPage.FindControl("ddlGrp1List"), DropDownList).Items.FindByValue(dtTable.Rows(0).Item("varGrpfld")) Is Nothing Then
                                CType(radOppTab.MultiPage.FindControl("ddlGrp1List"), DropDownList).ClearSelection()
                                CType(radOppTab.MultiPage.FindControl("ddlGrp1List"), DropDownList).Items.FindByValue(dtTable.Rows(0).Item("varGrpfld")).Selected = True
                            End If
                            If ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field U" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field D" Then
                                ddlGrp1Filter.Enabled = True
                            End If
                            Dim i As Integer = 0
                            For Each item As ListItem In lstReportColumnsOrder.Items
                                If i = 0 Then
                                    txtOrderListText.Text = item.Text
                                Else : txtOrderListText.Text = txtOrderListText.Text & "|" & item.Text
                                End If
                                i = i + 1
                            Next
                        End If

                        CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue = StdFilter
                        If Not CType(radOppTab.MultiPage.FindControl("ddlOrderflt"), DropDownList).Items.FindByValue(dtTable.Rows(0).Item("OrderbyFld")) Is Nothing Then
                            CType(radOppTab.MultiPage.FindControl("ddlOrderflt"), DropDownList).SelectedValue = dtTable.Rows(0).Item("OrderbyFld")
                        End If

                        If Not CType(radOppTab.MultiPage.FindControl("ddlCompFlt1"), DropDownList).Items.FindByValue(dtTable.Rows(0).Item("CompFilter1")) Is Nothing Then
                            CType(radOppTab.MultiPage.FindControl("ddlCompFlt1"), DropDownList).SelectedValue = dtTable.Rows(0).Item("CompFilter1")
                        End If
                        CType(radOppTab.MultiPage.FindControl("ddlCompFOpe"), DropDownList).SelectedValue = dtTable.Rows(0).Item("CompFilterOpr")
                        If Not CType(radOppTab.MultiPage.FindControl("ddlCompFlt2"), DropDownList).Items.FindByValue(dtTable.Rows(0).Item("CompFilter2")) Is Nothing Then
                            CType(radOppTab.MultiPage.FindControl("ddlCompFlt2"), DropDownList).SelectedValue = dtTable.Rows(0).Item("CompFilter2")
                        End If

                        If dtTable.Rows(0).Item("MttId") = 1 Then
                            radMyself.Checked = True
                            If Not CType(radOppTab.MultiPage.FindControl("ddlMyself"), DropDownList).Items.FindByValue(dtTable.Rows(0).Item("MttValue")) Is Nothing Then
                                CType(radOppTab.MultiPage.FindControl("ddlMyself"), DropDownList).SelectedValue = dtTable.Rows(0).Item("MttValue")
                            End If
                        ElseIf dtTable.Rows(0).Item("MttId") = 2 Then
                            radTeamSelected.Checked = True
                            If Not CType(radOppTab.MultiPage.FindControl("ddlTeam"), DropDownList).Items.FindByValue(dtTable.Rows(0).Item("MttValue")) Is Nothing Then
                                CType(radOppTab.MultiPage.FindControl("ddlTeam"), DropDownList).SelectedValue = dtTable.Rows(0).Item("MttValue")
                            End If
                        ElseIf dtTable.Rows(0).Item("MttId") = 3 Then
                            radTerriotary.Checked = True
                            If Not CType(radOppTab.MultiPage.FindControl("ddlTerr"), DropDownList).Items.FindByValue(dtTable.Rows(0).Item("MttValue")) Is Nothing Then
                                CType(radOppTab.MultiPage.FindControl("ddlTerr"), DropDownList).SelectedValue = dtTable.Rows(0).Item("MttValue")
                            End If
                        End If
                        ddlOrderFilter.SelectedValue = dtTable.Rows(0).Item("Orderf")
                        BindDataGrid()
                    End If

                End If
                'If ddlGrp1List.SelectedValue.Split("~").Length >= 3 Then
                '    If ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field" Then
                '        ddlGrp1Filter.Enabled = True
                '    End If
                'End If
                If chkLinerGrid.Checked = True Then
                    pnllinear.Visible = True
                Else : pnllinear.Visible = False
                End If
                If txtchkAdvFilter.Text = 0 Then
                    hplAdvFilter.Style("display") = ""
                    hplClrAdvFilter.Style("display") = "none"
                    lblAdvFltTxt.Style("display") = "none"
                    txtAdvFilter.Style("display") = "none"
                    pnlAndOr.Style("display") = ""
                Else
                    hplAdvFilter.Style("display") = "none"
                    hplClrAdvFilter.Style("display") = ""
                    lblAdvFltTxt.Style("display") = ""
                    txtAdvFilter.Style("display") = ""
                    pnlAndOr.Style("display") = "none"
                End If
                If ddlModule.SelectedValue = 11 And lstGroup.SelectedValue = 31 Then
                    CType(radOppTab.MultiPage.FindControl("chkRecCount"), CheckBox).Checked = False
                    CType(radOppTab.MultiPage.FindControl("chkRecCount"), CheckBox).Enabled = False
                Else : CType(radOppTab.MultiPage.FindControl("chkRecCount"), CheckBox).Enabled = True
                End If

                calFrom.RegisterCalanderScript()
                calTo.RegisterCalanderScript()

                If Not IsPostBack Then
                    radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub LoadDropDowns()
            Try
                'step 1 loading the module and group information
                Dim ds As DataSet
                Dim ojReports As New CustomReports
                ojReports.numCustModuleId = 1   'setting the group as 
                ds = ojReports.GetModules()
                ddlModule.DataSource = ds.Tables(0)
                ddlModule.DataTextField = "vcCustModuleName"
                ddlModule.DataValueField = "numCustModuleId"
                ddlModule.DataBind()
                If Not ddlModule.Items.FindByValue("1") Is Nothing Then ddlModule.Items.FindByValue("1").Selected = True

                lstGroup.DataSource = ds.Tables(1)
                lstGroup.DataTextField = "vcFieldGroupName"
                lstGroup.DataValueField = "numReportOptionGroupId"
                lstGroup.DataBind()

                If lstGroup.Items.Count > 0 Then lstGroup.Items(0).Selected = True
                
                'objCommon.sb_FillComboFromDBwithSel(ddlRelContact, 8, Session("DomainID"))
                'objCommon.sb_FillComboFromDBwithSel(ddlRelOrg, 5, Session("DomainID"))
                ' ddlRelContact.Items.FindByValue(0).Text = "All Types"
                ' ddlRelOrg.Items.FindByValue(0).Text = "All Relationships"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function PopulatePeriod() As DataTable
            Try
                Dim dsKPI As New DataSet
                dsKPI = Session("dsKPI")
                Return dsKPI.Tables(1)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function PopulatePriority() As DataTable
            Try
                Dim dsKPI As New DataSet
                dsKPI = Session("dsKPI")
                Return dsKPI.Tables(2)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function PopulateStatusIcon() As DataTable
            Try
                Dim dsKPI As New DataSet
                dsKPI = Session("dsKPI")
                Return dsKPI.Tables(3)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub GetKPIGroups()
            Try
                Dim dtReportFields As DataTable
                Dim dtKPIFields As DataTable
                Dim objRow As DataRow
                Dim objKPIRow As DataRow
                dtReportFields = Session("ReportFields")

                dtKPIFields = New DataTable

                dtKPIFields.Columns.Add("numFieldGroupID")
                dtKPIFields.Columns("numFieldGroupID").AutoIncrement = True
                dtKPIFields.Columns("numFieldGroupID").AutoIncrementSeed = 1
                dtKPIFields.Columns.Add("vcScrFieldName")


                For Each objRow In dtReportFields.Rows
                    objKPIRow = dtKPIFields.NewRow
                    'objKPIRow.Item("numFieldGroupID") = objRow.Item("numFieldGroupID")
                    objKPIRow.Item("vcScrFieldName") = objRow.Item("vcScrFieldName")
                    dtKPIFields.Rows.Add(objKPIRow)
                Next

                dtKPIFields.AcceptChanges()
                ddlKPI.DataSource = dtKPIFields
                ddlKPI.DataTextField = "vcScrFieldName"
                ddlKPI.DataValueField = "numFieldGroupID"
                ddlKPI.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindReportFields()
            Try
                Dim objCustomReports As New CustomReports

                Dim dtReportFields, dtCustomReportFields As DataTable           'declare a datatable to store the default fields and the custom fields resp.
                Dim drCustomReportFieldsCopy As DataRow                         'Declare a DataRow      
                dtReportFields = Session("ReportFields")
                Dim dtCell As TableCell
                Dim dtRow As TableRow
                Dim maxFieldGroupId As Integer
                maxFieldGroupId = dtReportFields.Compute("max(numFieldGroupID)", "")
                Dim dvReportFieldsOrder, dvReportFieldsSumm As DataView 'declare a databviews for each of the list box

                Dim dsKPI As New DataSet
                Dim objReport As CReports
                objReport = New CReports

                If radOppTab.SelectedIndex = 2 Then
                    dsKPI = objReport.GetCustReportKPI(0)
                    Session("dsKPI") = dsKPI

                    dgKPI.DataSource = dsKPI.Tables(0)
                    dgKPI.DataBind()

                    GetKPIGroups()
                End If


                'get the default view
                Dim i As Integer
                i = 6
                For i = 6 To maxFieldGroupId
                    If i <> 30 Then
                        dvReportFieldsOrder = dtReportFields.DefaultView
                        dvReportFieldsOrder.RowFilter = "numFieldGroupID = " & i
                        If dvReportFieldsOrder.Count > 0 Then
                            dtRow = New TableRow
                            dtCell = New TableCell
                            dtCell.ColumnSpan = 4
                            Dim l As New Label

                            l.Text = getGroupName(i)

                            l.CssClass = "tblrowHeader"
                            dtCell.Controls.Add(l)

                            Dim chkAll As New CheckBox
                            chkAll.ID = "chkGroupAll_" & i
                            chkAll.ClientIDMode = ClientIDMode.Static
                            chkAll.Attributes.Add("onchange", "SelectGroupField(" & i & ")")
                            chkAll.Attributes.Add("onclick", "SelectGroupField(" & i & ")")
                            dtCell.Controls.Add(chkAll)

                            dtRow.Cells.Add(dtCell)
                            tblFileds.Rows.Add(dtRow)
                            dtRow = New TableRow
                            dtCell = New TableCell
                            dtRow.Width = Unit.Percentage(100)
                            Dim cellcount As Integer = 0
                            Dim count As Integer = 0
                            For Each drv As DataRowView In dvReportFieldsOrder
                                If cellcount = 4 Then
                                    cellcount = 0
                                    dtRow.Cells.Add(dtCell)
                                    tblFileds.Rows.Add(dtRow)
                                    dtRow = New TableRow
                                    dtCell = New TableCell
                                    dtRow.Width = Unit.Percentage(100)
                                End If
                                Dim chk As New CheckBox
                                chk.Text = drv.Item("vcScrFieldName")
                                If CType(IIf(IsDBNull(drv.Item("boolDefaultColumnSelection")), False, drv.Item("boolDefaultColumnSelection")), Boolean) = True Then
                                    chk.Checked = True
                                End If
                                If drv.Item("numFieldGroupID") = 39 And drv.Item("vcDbFieldName") = "GJH.datEntry_Date" Then
                                    chk.Visible = False
                                End If
                                chk.ID = drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & drv.Item("CType")
                                chk.CssClass = "normal1"
                                chk.InputAttributes.Add("Group_Name", i)

                                chk.Width = Unit.Percentage(20)
                                dtCell.Controls.Add(chk)
                                cellcount = cellcount + 1
                                count = count + 1
                                If dvReportFieldsOrder.Count = count Then
                                    dtRow.Cells.Add(dtCell)
                                    tblFileds.Rows.Add(dtRow)

                                    Dim drEmpty As New TableRow
                                    drEmpty.Height = 20
                                    tblFileds.Rows.Add(drEmpty)
                                End If
                            Next
                        End If
                    End If
                Next
                dvReportFieldsSumm = dtReportFields.DefaultView
                dvReportFieldsSumm.RowFilter = "boolSummationPossible = " & 1
                If dvReportFieldsSumm.Count > 0 Then
                    For Each drv As DataRowView In dvReportFieldsSumm
                        If (CType(radOppTab.MultiPage.FindControl("Sum" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")), CheckBox).Checked = True) _
                        Or (CType(radOppTab.MultiPage.FindControl("Avg" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")), CheckBox).Checked = True) _
                        Or (CType(radOppTab.MultiPage.FindControl("Max" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")), CheckBox).Checked = True) _
                        Or (CType(radOppTab.MultiPage.FindControl("Min" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")), CheckBox).Checked = True) Then

                            CType(radOppTab.MultiPage.FindControl(drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & drv.Item("CType")), CheckBox).Checked = True

                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindGroupingList()
            Try
                If chkSummationGrid.Checked = True Then
                    Dim dtReportFields, dtCustomReportFields As DataTable
                    Dim drCustomReportFieldsCopy As DataRow
                    If Not Session("ReportFields") Is Nothing Then
                        dtReportFields = Session("ReportFields")
                        Dim ddlGrp1List As DropDownList
                        ddlGrp1List = radOppTab.MultiPage.FindControl("ddlGrp1List")
                        ' ddlGrp1List.DataSource = dtReportFields
                        ddlGrp1List.Items.Clear()
                        For Each row As DataRow In dtReportFields.Rows
                            If row.Item("numFieldGroupID") <> 30 Then
                                If row.Item("numFieldGroupID") = 39 And row.Item("vcDbFieldName") <> "GJH.datEntry_Date" Then
                                Else
                                    Dim lstItem As New ListItem
                                    lstItem.Text = row.Item("vcScrFieldName")
                                    lstItem.Value = "Grp1" & "~" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID") & "~" & row.Item("vcFieldType") & "~" & row.Item("CType") & "~" & row.Item("ListId")
                                    lstItem.Attributes("OptionGroup") = getGroupName(row.Item("numFieldGroupID"))
                                    ddlGrp1List.Items.Add(lstItem)
                                End If
                            End If
                        Next

                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnBuildFields_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildFields.Click
            Try
                tblFileds.Rows.Clear()
                BindReportFields()
                If chkLinerGrid.Checked = True Then
                    radOppTab.SelectedIndex = 4
                Else : radOppTab.SelectedIndex = 3
                End If

                radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnBuildListItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildListItems.Click
            Try
                lstReportColumnsOrder.Items.Clear()
                BuildListItems()
                radOppTab.SelectedIndex = 5
                radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BuildListItems()
            Try
                Dim i As Integer = 0
                Dim dtReportFields As New DataTable
                dtReportFields = Session("ReportFields")
                If dtReportFields.Rows.Count > 0 Then
                Else : lstReportColumnsOrder.Items.Clear()
                End If

                For Each row As DataRow In dtReportFields.Rows
                    If row.Item("numFieldGroupID") <> 30 Then
                        Dim chk As New CheckBox
                        chk = radOppTab.MultiPage.FindControl(row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID") & "~" & row.Item("CType"))
                        If chk.Checked = True Then
                            Dim lstItem As New ListItem
                            lstItem.Text = row.Item("vcScrFieldName")
                            lstItem.Value = row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID") & "~" & row.Item("boolSummationPossible") & "~" & row.Item("vcFieldType") & "~" & row.Item("CType") & "~" & row.Item("ListId")
                            lstReportColumnsOrder.Items.Add(lstItem)
                        End If
                    End If
                Next

                SpanorderFlt.Controls.Clear()

                Dim ddlOrderflt As New DropDownList
                ddlOrderflt.CssClass = "form-control"
                ddlOrderflt.ID = "ddlOrderflt"
                Dim itemos As New ListItem
                itemos.Text = "--SelectOne--"
                itemos.Value = 0
                ddlOrderflt.Items.Add(itemos)
                For Each itm As ListItem In lstReportColumnsOrder.Items
                    Dim itemO As New ListItem
                    itemO.Text = itm.Text
                    itemO.Value = itm.Value
                    If itm.Value.Split("~").Length >= 2 Then itemO.Attributes("OptionGroup") = getGroupName(itm.Value.Split("~")(1))
                    ddlOrderflt.Items.Add(itemO)
                Next
                SpanorderFlt.Controls.Add(ddlOrderflt)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BuildListItemsOrdered()
            Try
                If txtOrderListText.Text <> "" And txtOrderListValues.Text <> "" Then
                    Dim strText As String()
                    Dim strValues As String()
                    strText = txtOrderListText.Text.Split("|")
                    strValues = txtOrderListValues.Text.Split("|")
                    If strText.Length = strValues.Length Then
                        Dim i As Integer = 0
                        lstReportColumnsOrder.Items.Clear()
                        For i = 0 To strText.Length - 1
                            Dim lstItem As New ListItem
                            lstItem.Text = strText(i)
                            lstItem.Value = strValues(i)
                            lstReportColumnsOrder.Items.Add(lstItem)
                        Next
                    End If
                End If
                SpanorderFlt.Controls.Clear()

                Dim ddlOrderflt As New DropDownList
                ddlOrderflt.CssClass = "form-control"
                ddlOrderflt.ID = "ddlOrderflt"
                Dim itemos As New ListItem
                itemos.Text = "--SelectOne--"
                itemos.Value = 0
                ddlOrderflt.Items.Add(itemos)
                For Each itm As ListItem In lstReportColumnsOrder.Items
                    Dim itemO As New ListItem
                    itemO.Text = itm.Text
                    itemO.Value = itm.Value
                    If itm.Value.Split("~").Length >= 2 Then itemO.Attributes("OptionGroup") = getGroupName(itm.Value.Split("~")(1))
                    ddlOrderflt.Items.Add(itemO)
                Next
                SpanorderFlt.Controls.Add(ddlOrderflt)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnBuildFilterList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildFilterList.Click
            Try
                tblAddFilters.Controls.Clear()
                BuildFilterList()
                radOppTab.SelectedIndex = 6
                radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BuildFilterList()
            Try
                Dim i As Integer = 0
                Dim dtReportFields As New DataTable
                dtReportFields = Session("ReportFields")
                Dim dvReportFields As DataView
                dvReportFields = dtReportFields.DefaultView
                dvReportFields.RowFilter = "vcFieldType = 'Date Field' or vcFieldType = 'Date Field U' or vcFieldType = 'Date Field D'"
                dvReportFields.Sort = "numFieldGroupID"
                'ddlStdfilter.Items.Clear()
                spnStdFlt.Controls.Clear()
                Dim ddlStdfilter As New DropDownList
                ddlStdfilter.CssClass = "form-control"
                ddlStdfilter.ID = "ddlStdfilter"
                Dim item As New ListItem
                item.Text = "--SelectOne--"
                item.Value = 0
                ddlStdfilter.Items.Add(item)
                For Each drv As DataRowView In dvReportFields
                    Dim lstItem As New ListItem
                    lstItem.Text = drv.Item("vcScrFieldName")
                    lstItem.Value = drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & drv.Item("CType") & "~" & drv.Item("vcFieldType")
                    lstItem.Attributes("OptionGroup") = getGroupName(drv.Item("numFieldGroupID"))
                    ddlStdfilter.Items.Add(lstItem)
                Next

                spnStdFlt.Controls.Add(ddlStdfilter)
                spnddlCompFlt1.Controls.Clear()
                spnddlCompFlt2.Controls.Clear()

                Dim ddlCompFlt1 As New DropDownList
                ddlCompFlt1.CssClass = "form-control"
                ddlCompFlt1.ID = "ddlCompFlt1"
                ddlCompFlt1.Attributes.Add("class", "form-control")
                Dim lstItemsCF1 As New ListItem
                lstItemsCF1.Text = "---SelectOne---"
                lstItemsCF1.Value = 0
                ddlCompFlt1.Items.Add(lstItemsCF1)

                Dim ddlCompFlt2 As New DropDownList
                ddlCompFlt2.CssClass = "form-control"
                ddlCompFlt2.ID = "ddlCompFlt2"
                ddlCompFlt2.Attributes.Add("class", "form-control")
                Dim lstItemsCF2 As New ListItem
                lstItemsCF2.Text = "---SelectOne---"
                lstItemsCF2.Value = 0
                ddlCompFlt2.Items.Add(lstItemsCF2)

                For Each row As DataRow In dtReportFields.Rows
                    If row.Item("numFieldGroupID") = 39 And row.Item("vcDbFieldName") <> "GJH.datEntry_Date" Then
                    Else
                        Dim lstItem1 As New ListItem
                        lstItem1.Text = row.Item("vcScrFieldName")
                        lstItem1.Value = row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID") & "~" & row.Item("vcFieldType") & "~" & row.Item("ListId") & "~" & row.Item("CType")
                        lstItem1.Attributes("OptionGroup") = getGroupName(row.Item("numFieldGroupID"))
                        ddlCompFlt1.Items.Add(lstItem1)

                        Dim lstItem2 As New ListItem
                        lstItem2.Text = row.Item("vcScrFieldName")
                        lstItem2.Value = row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID") & "~" & row.Item("vcFieldType") & "~" & row.Item("ListId") & "~" & row.Item("CType")
                        lstItem2.Attributes("OptionGroup") = getGroupName(row.Item("numFieldGroupID"))
                        ddlCompFlt2.Items.Add(lstItem2)
                    End If
                Next
                spnddlCompFlt1.Controls.Add(ddlCompFlt1)
                spnddlCompFlt2.Controls.Add(ddlCompFlt2)

                For i = 0 To CInt(noFilterRows.Text)
                    Dim tr As New HtmlTableRow
                    Dim tdRowNo As New HtmlTableCell
                    Dim td As New HtmlTableCell
                    Dim td1 As New HtmlTableCell
                    Dim td2 As New HtmlTableCell
                    Dim td3 As New HtmlTableCell
                    Dim ddl As New DropDownList
                    ddl.ID = "ddlField" & i
                    ddl.Attributes.Add("class", "form-control")
                    ddl.Attributes.Add("onchange", "return ChkFilterType('" & ddl.ID & "','" & i & "')")
                    Dim lstItems As New ListItem
                    lstItems.Text = "---SelectOne---"
                    lstItems.Value = 0
                    ddl.Items.Add(lstItems)

                    For Each row As DataRow In dtReportFields.Rows
                        If row.Item("numFieldGroupID") = 39 And row.Item("vcDbFieldName") <> "GJH.datEntry_Date" Then
                        Else
                            Dim lstItem As New ListItem
                            lstItem.Text = row.Item("vcScrFieldName")
                            lstItem.Value = row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID") & "~" & row.Item("vcFieldType") & "~" & row.Item("ListId") & "~" & row.Item("CType")
                            lstItem.Attributes("OptionGroup") = getGroupName(row.Item("numFieldGroupID"))
                            ddl.Items.Add(lstItem)
                        End If
                    Next

                    td.Controls.Add(ddl)

                    Dim ddl1 As New HtmlSelect
                    ddl1.ID = "Operator" & i
                    ddl1.Name = "Operator" & i
                    ddl1.Attributes.Add("class", "form-control")

                    ddl1.Items.Add(New ListItem("equals", "="))
                    ddl1.Items.Add(New ListItem("not equal to", "!="))
                    ddl1.Items.Add(New ListItem("less than", "<"))
                    ddl1.Items.Add(New ListItem("greater than", ">"))
                    ddl1.Items.Add(New ListItem("greater or equal", ">="))
                    ddl1.Items.Add(New ListItem("less or equal", "<="))
                    ddl1.Items.Add(New ListItem("contains", "Like"))
                    ddl1.Items.Add(New ListItem("does not contain", "Not Like"))
                    ddl1.Items.Add(New ListItem("starts with", "se"))
                    ddl1.Items.Add(New ListItem("end with", "Ew"))

                    td1.Controls.Add(ddl1)

                    Dim txt As New HtmlInputText
                    txt.ID = "Value" & i
                    txt.Name = "Value" & i
                    txt.Attributes.Add("class", "form-control")
                    txt.MaxLength = 100
                    td2.Controls.Add(txt)

                    Dim txtd As New HtmlInputText
                    txtd.ID = "Valued" & i
                    txtd.Name = "Valued" & i
                    txtd.Attributes.Add("class", "form-control")
                    txtd.Style("Display") = "none"
                    td2.Controls.Add(txtd)

                    Dim img As New System.Web.UI.WebControls.Image
                    img.ImageUrl = "../images/SEARCH_ICON.GIF"
                    img.Style("display") = "none"
                    img.ID = "imgHpl" & i
                    img.EnableViewState = True
                    img.Attributes.Add("onclick", "return openPop('" & i & "')")
                    td3.Controls.Add(img)

                    tdRowNo.InnerHtml = "<span class=""normal7""> " & (i + 1) & "</span>"
                    tr.Cells.Add(tdRowNo)
                    tr.Cells.Add(td)
                    tr.Cells.Add(td1)
                    tr.Cells.Add(td2)
                    tr.Cells.Add(td3)
                    tblAddFilters.Rows.Add(tr)
                Next

                dvReportFields.RowFilter = "tintMyself = 1"
                spnMyself.Controls.Clear()
                Dim ddlMyself As New DropDownList
                ddlMyself.CssClass = "form-control"
                ddlMyself.ID = "ddlMyself"
                Dim itemM As New ListItem
                itemM.Text = "--SelectOne--"
                itemM.Value = 0
                ddlMyself.Items.Add(itemM)
                For Each drv As DataRowView In dvReportFields
                    Dim lstItem As New ListItem
                    lstItem.Text = drv.Item("vcScrFieldName")
                    lstItem.Value = drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")
                    lstItem.Attributes("OptionGroup") = getGroupName(drv.Item("numFieldGroupID"))
                    ddlMyself.Items.Add(lstItem)
                Next
                spnMyself.Controls.Add(ddlMyself)
                dvReportFields.RowFilter = "tintTeam = 1"

                spnTeam.Controls.Clear()
                Dim ddlTeam As New DropDownList
                ddlTeam.CssClass = "form-control"
                ddlTeam.ID = "ddlTeam"
                Dim itemT As New ListItem
                itemT.Text = "--SelectOne--"
                itemT.Value = 0
                ddlTeam.Items.Add(itemT)
                For Each drv As DataRowView In dvReportFields
                    Dim lstItem As New ListItem
                    lstItem.Text = drv.Item("vcScrFieldName")
                    lstItem.Value = drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")
                    lstItem.Attributes("OptionGroup") = getGroupName(drv.Item("numFieldGroupID"))
                    ddlTeam.Items.Add(lstItem)
                Next
                spnTeam.Controls.Add(ddlTeam)
                dvReportFields.RowFilter = "tintTerr = 1"
                spnTerr.Controls.Clear()
                Dim ddlTerr As New DropDownList
                ddlTerr.CssClass = "form-control"
                ddlTerr.ID = "ddlTerr"
                Dim itemTe As New ListItem
                itemTe.Text = "--SelectOne--"
                itemTe.Value = 0
                ddlTerr.Items.Add(itemTe)
                For Each drv As DataRowView In dvReportFields
                    Dim lstItem As New ListItem
                    lstItem.Text = drv.Item("vcScrFieldName")
                    lstItem.Value = drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")
                    lstItem.Attributes("OptionGroup") = getGroupName(drv.Item("numFieldGroupID"))
                    ddlTerr.Items.Add(lstItem)
                Next
                spnTerr.Controls.Add(ddlTerr)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAddFilterRow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddFilterRow.Click
            Try
                AddFilterRow()
                radOppTab.SelectedIndex = 6
                radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub AddFilterRow()
            Try
                Dim i As Integer = CInt(noFilterRows.Text) + 1
                Dim strFields As New System.Text.StringBuilder
                Dim dtReportFields As New DataTable
                dtReportFields = Session("ReportFields")
                Dim tr As New HtmlTableRow
                Dim td As New HtmlTableCell
                Dim td1 As New HtmlTableCell
                Dim td2 As New HtmlTableCell
                Dim ddl As New DropDownList
                ddl.ID = "ddlField" & i
                'ddl.Name = "td" & i
                ddl.Attributes.Add("class", "form-control")
                Dim lstItems As New ListItem
                lstItems.Text = "---SelectOne---"
                lstItems.Value = 0
                ddl.Items.Add(lstItems)

                For Each row As DataRow In dtReportFields.Rows
                    Dim lstItem As New ListItem
                    lstItem.Text = row.Item("vcScrFieldName")
                    lstItem.Value = row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID") & "~" & row.Item("vcFieldType") & "~" & row.Item("ListId") & "~" & row.Item("CType")
                    lstItem.Attributes("OptionGroup") = getGroupName(row.Item("numFieldGroupID"))
                    ddl.Items.Add(lstItem)
                Next

                td.Controls.Add(ddl)

                Dim ddl1 As New HtmlSelect
                ddl1.ID = "Operator" & i
                ddl1.Name = "Operator" & i
                ddl1.Attributes.Add("class", "form-control")
                Dim lstItem1 As New ListItem
                Dim lstItem2 As New ListItem
                Dim lstItem3 As New ListItem
                Dim lstItem4 As New ListItem
                Dim lstItem5 As New ListItem
                Dim lstItem6 As New ListItem
                Dim lstItem7 As New ListItem
                Dim lstItem8 As New ListItem
                Dim lstItem9 As New ListItem
                Dim lstItem10 As New ListItem

                lstItem1.Text = "equals"
                lstItem1.Value = "="
                ddl1.Items.Add(lstItem1)

                lstItem2.Text = "not equal to"
                lstItem2.Value = "!="
                ddl1.Items.Add(lstItem2)

                lstItem3.Text = "less than"
                lstItem3.Value = "<"
                ddl1.Items.Add(lstItem3)

                lstItem4.Text = "greater than"
                lstItem4.Value = ">"
                ddl1.Items.Add(lstItem4)

                lstItem5.Text = "greater or equal"
                lstItem5.Value = ">="
                ddl1.Items.Add(lstItem5)

                lstItem6.Text = "less or equal"
                lstItem6.Value = "<="
                ddl1.Items.Add(lstItem6)

                lstItem7.Text = "contains"
                lstItem7.Value = "Like"
                ddl1.Items.Add(lstItem7)

                lstItem8.Text = "does not contain"
                lstItem8.Value = "Not Like"
                ddl1.Items.Add(lstItem8)

                lstItem9.Text = "starts with"
                lstItem9.Value = "se"
                ddl1.Items.Add(lstItem9)

                lstItem10.Text = "end with"
                lstItem10.Value = "Ew"
                ddl1.Items.Add(lstItem10)

                td1.Controls.Add(ddl1)

                Dim txt As New HtmlInputText
                txt.ID = "Value" & i
                txt.Name = "Value" & i
                txt.Attributes.Add("class", "form-control")
                txt.MaxLength = 100
                td2.Controls.Add(txt)

                Dim txtd As New HtmlInputText
                txtd.ID = "Valued" & i
                txtd.Name = "Valued" & i
                txtd.Attributes.Add("class", "form-control")
                txtd.Style("Display") = "none"
                td2.Controls.Add(txtd)

                tr.Cells.Add(td)
                tr.Cells.Add(td1)
                tr.Cells.Add(td2)

                tblAddFilters.Rows.Add(tr)
                noFilterRows.Text = i
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnBuildSummation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildSummation.Click
            Try
                tblSum.Controls.Clear()
                BuildSummationTab()
                radOppTab.SelectedIndex = 2
                radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BuildSummationTab()
            Try
                Dim objCustomReports As New CustomReports
                Dim dtReportFields, dtCustomReportFields As DataTable           'declare a datatable to store the default fields and the custom fields resp.
                Dim drCustomReportFieldsCopy As DataRow                         'Declare a DataRow
                objCustomReports.numReportOptTypeId = lstGroup.SelectedValue
                objCustomReports.DomainID = Session("DomainID")
                Select Case lstGroup.SelectedValue
                    Case 4
                        objCustomReports.numContactType = 0
                        objCustomReports.numOrgRel = 0
                    Case 5
                        objCustomReports.numContactType = 0
                        objCustomReports.numOrgRel = 0
                    Case Else
                        objCustomReports.numContactType = 0
                        objCustomReports.numOrgRel = 0
                End Select
                dtReportFields = objCustomReports.getColumnsForCustomReport() 'call to get the default fields
                Session("ReportFields") = dtReportFields
                Dim i As Integer = 0

                Dim TRHeader As New TableRow
                Dim TC0Header As New TableCell
                Dim TC1Header As New TableCell
                Dim TC2Header As New TableCell
                Dim TC3Header As New TableCell
                Dim TC4Header As New TableCell

                TC0Header.Text = "FieldName"
                TC0Header.CssClass = "normal7"

                TC1Header.Text = "Sum"
                TC1Header.CssClass = "normal7"

                TC2Header.Text = "AVG"
                TC2Header.CssClass = "normal7"

                TC3Header.Text = "Max Value"
                TC3Header.CssClass = "normal7"

                TC4Header.Text = "Min Value"
                TC4Header.CssClass = "normal7"

                TRHeader.Cells.Add(TC0Header)
                TRHeader.Cells.Add(TC1Header)
                TRHeader.Cells.Add(TC2Header)
                TRHeader.Cells.Add(TC3Header)
                TRHeader.Cells.Add(TC4Header)
                TRHeader.Attributes.Add("class", "hs")
                tblSum.Rows.Add(TRHeader)
                tblSum.Attributes.Add("border-style", "hidden")

                Dim LabelRec As New Label
                Dim chkRecCount As New CheckBox
                chkRecCount.ID = "chkRecCount"
                LabelRec.Text = "Record count"
                LabelRec.CssClass = "normal7"
                If ddlModule.SelectedValue = 11 And lstGroup.SelectedValue = 31 Then
                    chkRecCount.Enabled = False
                Else : chkRecCount.Enabled = True
                End If
                Dim TbR As New TableRow
                Dim TbC As New TableCell
                Dim TbC1 As New TableCell
                TbC.Controls.Add(LabelRec)
                TbC1.Controls.Add(chkRecCount)
                TbC1.ColumnSpan = 4
                TbR.Cells.Add(TbC)
                TbR.Cells.Add(TbC1)
                tblSum.Rows.Add(TbR)

                Dim dvReportFieldsSumm As DataView
                dvReportFieldsSumm = dtReportFields.DefaultView
                dvReportFieldsSumm.RowFilter = "boolSummationPossible = " & 1
                If dvReportFieldsSumm.Count > 0 Then
                    For Each drv As DataRowView In dvReportFieldsSumm
                        Dim Label As New Label
                        Dim ChkSum As New CheckBox
                        Dim ChkAvg As New CheckBox
                        Dim ChkMax As New CheckBox
                        Dim ChkMin As New CheckBox
                        Dim TR As New TableRow
                        Dim TC0 As New TableCell
                        Dim TC1 As New TableCell
                        Dim TC2 As New TableCell
                        Dim TC3 As New TableCell
                        Dim TC4 As New TableCell

                        Label.Text = drv.Item("vcScrFieldName")
                        Label.CssClass = "normal7"

                        ChkSum.ID = "Sum" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")
                        ChkAvg.ID = "Avg" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")
                        ChkMax.ID = "Max" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")
                        ChkMin.ID = "Min" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")

                        TC0.Controls.Add(Label)
                        TC1.Controls.Add(ChkSum)
                        TC2.Controls.Add(ChkAvg)
                        TC3.Controls.Add(ChkMax)
                        TC4.Controls.Add(ChkMin)

                        TR.Cells.Add(TC0)
                        TR.Cells.Add(TC1)
                        TR.Cells.Add(TC2)
                        TR.Cells.Add(TC3)
                        TR.Cells.Add(TC4)
                        TR.VerticalAlign = VerticalAlign.Top
                        TR.CssClass = "normal1"
                        tblSum.Rows.Add(TR)
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
            Try
                BuildSqlQuery()
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BuildSqlQuery()
            Try
                Dim strOrderListValues As String() = txtOrderListValues.Text.Split("|")
                Dim strOrderListText As String() = txtOrderListText.Text.Split("|")
                Dim Sql As New System.Text.StringBuilder
                If ddlNoRows.SelectedValue = "0" Or chkSummationGrid.Checked = True Then
                    Sql.Append("Select ")
                Else : Sql.Append("Select Top " & ddlNoRows.SelectedValue & " ")
                End If

                Select Case ddlModule.SelectedValue
                    Case 1 : Sql.Append("ACI.numContactId as ##Contacts##,ACI.numContactType as ##numContactType##, DivisionMaster.numDivisionID as ##Organization##,CompanyInfo.numCompanyid as ##numCompanyid##,coalesce(ACI.numRecOwner,0) AS ##numRecOwner##,coalesce(DivisionMaster.numAssignedBy,0) AS ##numAssignedBy##,coalesce(DivisionMaster.numAssignedTo,0) AS ##numAssignedTo##,ACI.numCreatedBy AS ##numCreatedBy##,ACI.numModifiedBy AS ##numModifiedBy##,") 'Contacts
                    Case 2 : Sql.Append("DivisionMaster.numDivisionID as ##Organization##,CompanyInfo.numCompanyid as ##numCompanyid##,DivisionMaster.numRecOwner AS ##numRecOwner##,DivisionMaster.numAssignedBy AS ##numAssignedBy##,DivisionMaster.numAssignedTo AS ##numAssignedTo##,CompanyInfo.numCreatedBy AS ##numCreatedBy##,CompanyInfo.numModifiedBy AS ##numModifiedBy##,") 'Organization
                    Case 3 : Sql.Append("ProMas.numProId as ##Projects##, DivisionMaster.numDivisionID as ##Organization##,CompanyInfo.numCompanyid as ##numCompanyid##,ProMas.numRecOwner AS ##numRecOwner##,ProMas.numAssignedBy AS ##numAssignedBy##,ProMas.numAssignedTo AS ##numAssignedTo##,ProMas.numCreatedBy AS ##numCreatedBy##,ProMas.numModifiedBy AS ##numModifiedBy##,") 'Projects
                    Case 4 : Sql.Append("Cases.numCaseId  ##Cases##,Cases.numRecOwner AS ##numRecOwner##,Cases.numAssignedBy AS ##numAssignedBy##,Cases.numAssignedTo AS ##numAssignedTo##,Cases.numCreatedBy AS ##numCreatedBy##,Cases.numModifiedBy AS ##numModifiedBy##,") 'Cases
                    Case 5
                        If lstGroup.SelectedValue = 18 Then
                            Sql.Append("OppMas.numOppId as ##Opportunities##,OppBDoc.numOppBizDocsId as ##OpportunitiesBizDoc##,coalesce(ACI.numContactId,0) as ##Contacts##,ACI.numContactType as ##numContactType##, DivisionMaster.numDivisionID as ##Organization##,CompanyInfo.numCompanyid as ##numCompanyid##,OppMas.numRecOwner AS ##numRecOwner##,OppMas.numAssignedBy AS ##numAssignedBy##,OppMas.numAssignedTo AS ##numAssignedTo##,OppMas.numCreatedBy AS ##numCreatedBy##,OppMas.numModifiedBy AS ##numModifiedBy##,") 'OpportunitiesBizDoc
                        Else
                            Sql.Append("OppMas.numOppId as ##Opportunities##,coalesce(ACI.numContactId,0) as ##Contacts##,ACI.numContactType as ##numContactType##, DivisionMaster.numDivisionID as ##Organization##,CompanyInfo.numCompanyid as ##numCompanyid##,OppMas.numRecOwner AS ##numRecOwner##,OppMas.numAssignedBy AS ##numAssignedBy##,OppMas.numAssignedTo AS ##numAssignedTo##,OppMas.numCreatedBy AS ##numCreatedBy##,OppMas.numModifiedBy AS ##numModifiedBy##,") 'Opportunities
                        End If
                    Case 6 : Sql.Append("Item.numItemCode as ##Items##,") 'Items
                    Case 7 : Sql.Append("") 'Forecast
                    Case 8 : Sql.Append("") 'Web Analysis
                    Case 9 : Sql.Append("") 'Campaign
                    Case 10 : Sql.Append("numCommId as ##Action##,CaseId as ##CaseId##,CaseTimeId as ##CaseTimeId##,CaseExpId as ##CaseExpId##,ACI.numContactId as ##Contacts##,ACI.numContactType as ##numContactType##, DivisionMaster.numDivisionID as ##Organization##,CompanyInfo.numCompanyid as ##numCompanyid##,DivisionMaster.numRecOwner AS ##numRecOwner##,DivisionMaster.numAssignedBy AS ##numAssignedBy##,DivisionMaster.numAssignedTo AS ##numAssignedTo##,ACI.numCreatedBy AS ##numCreatedBy##,ACI.numModifiedBy AS ##numModifiedBy##,") 'Action Items
                    Case 11 : Sql.Append("") 'Accounting
                    Case 12 : Sql.Append("EmailHistory.numEmailHstrID as ##EmailHstrID##,") 'Emails
                End Select

                Dim CFW_FLD_Values_Cont, CFW_FLD_Values, CFW_FLD_Values_Case, CFW_Fld_Values_Opp, CFW_FLD_Values_Pro, CFW_FLD_Values_Item As New ArrayList()
                Dim iOrder As Integer = 0
                For iOrder = 0 To strOrderListValues.Length - 1
                    Dim strN As String = ""
                    If iOrder = 0 Or Sql.ToString = " " Then
                        strN = " "
                    Else : strN = ","
                    End If
                    If strOrderListValues(iOrder).Split("~")(4) = 1 Then
                        Dim strCustFunVal, tblAlias, CFWColumn, strCustFieldID As String

                        strCustFieldID = strOrderListValues(iOrder).Split("~")(0).Split("d")(1)
                        Select Case strOrderListValues(iOrder).Split("~")(1)
                            Case 6
                                strCustFunVal = ",4,ACI.numContactId"
                                tblAlias = "CFW_FLD_Values_Cont"

                                CFW_FLD_Values_Cont.Add("[" & strCustFieldID & "]")
                            Case 7, 8
                                strCustFunVal = ",1,DivisionMaster.numDivisionId"
                                tblAlias = "CFW_FLD_Values"

                                CFW_FLD_Values.Add("[" & strCustFieldID & "]")
                            Case 10, 11, 12, 13, 14
                                strCustFunVal = ",1,ProMas.numProId"
                                tblAlias = "CFW_FLD_Values"

                                CFW_FLD_Values.Add("[" & strCustFieldID & "]")
                            Case 15, 16, 17
                                strCustFunVal = ",3,Cases.numCaseId"
                                tblAlias = "CFW_FLD_Values_Case"

                                CFW_FLD_Values_Case.Add("[" & strCustFieldID & "]")
                            Case 18, 19, 20, 21, 22, 23, 25, 26
                                strCustFunVal = ",6,OppMas.numOppid"
                                tblAlias = "CFW_Fld_Values_Opp"

                                CFW_Fld_Values_Opp.Add("[" & strCustFieldID & "]")
                            Case 9
                                strCustFunVal = ",11,ProMas.numProId"
                                tblAlias = "CFW_FLD_Values_Pro"

                                CFW_FLD_Values_Pro.Add("[" & strCustFieldID & "]")
                            Case 27
                                strCustFunVal = ",5,Item.numItemCode"
                                tblAlias = "CFW_FLD_Values_Item"

                                CFW_FLD_Values_Item.Add("[" & strCustFieldID & "]")
                            Case Else
                                strCustFunVal = ",1,DivisionMaster.numDivisionId"
                                tblAlias = "CFW_FLD_Values"

                                CFW_FLD_Values.Add("[" & strCustFieldID & "]")
                        End Select
                        CFWColumn = tblAlias & ".[" & strCustFieldID & "]"

                        If strOrderListValues(iOrder).Split("~")(3) = "Drop Down List" Then
                            Sql.Append(strN & "fn_GetListItemName(" & CFWColumn & ") as '" & strOrderListText(iOrder) & "'")
                        ElseIf strOrderListValues(iOrder).Split("~")(3) = "Check box" Then
                            Sql.Append(strN & "fn_getCustomDataValue(" & CFWColumn & ",9) as '" & strOrderListText(iOrder) & "'")
                        ElseIf strOrderListValues(iOrder).Split("~")(3) = "Date Field" Or strOrderListValues(iOrder).Split("~")(3) = "Date Field D" Then
                            Sql.Append(strN & "FormatedDateFromDate(" & CFWColumn & ",@numDomainId) as '" & strOrderListText(iOrder) & "'")
                        ElseIf strOrderListValues(iOrder).Split("~")(3) = "Date Field U" Then
                            Sql.Append(strN & "FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset," & CFWColumn & "),@numDomainId) as '" & strOrderListText(iOrder) & "'")
                        Else
                            Sql.Append(strN & CFWColumn & " as '" & strOrderListText(iOrder) & "'")
                        End If
                    Else
                        If strOrderListValues(iOrder).Split("~")(1) = "39" Then
                            Select Case strOrderListValues(iOrder).Split("~")(0)
                                'Case "Income" : Sql.Append(strN & "sum(case when numAcntType=822 then GJD.numCreditAmt-GJD.numDebitAmt else 0 end) as  '" & strOrderListText(iOrder) & "'")
                                'Case "Expense" : Sql.Append(strN & "sum(case when numAcntType=824 then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)  as  '" & strOrderListText(iOrder) & "'")
                                'Case "COGS" : Sql.Append(strN & "sum(case when numAcntType=823 then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)  as '" & strOrderListText(iOrder) & "'")
                                'Case "OIncome" : Sql.Append(strN & "sum(case when numAcntType=825 then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)  as  '" & strOrderListText(iOrder) & "'")
                                'Case "OExpense" : Sql.Append(strN & "sum(case when numAcntType=826 then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)  as  '" & strOrderListText(iOrder) & "'")
                                'Case "GrossProfit"
                                '    Sql.Append(strN & "(sum(case when numAcntType=822 then GJD.numCreditAmt-GJD.numDebitAmt else 0 end) - ")
                                '    Sql.Append("sum(case when numAcntType=823 then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)) as  '" & strOrderListText(iOrder) & "'")
                                'Case "NetIncome"
                                '    Sql.Append(strN & "((sum(case when numAcntType=822 then GJD.numCreditAmt-GJD.numDebitAmt else 0 end) - ")
                                '    Sql.Append("sum(case when numAcntType=823 then GJD.numDebitAmt-GJD.numCreditAmt else 0 end))-")
                                '    Sql.Append("sum(case when numAcntType=824 then GJD.numDebitAmt-GJD.numCreditAmt else 0 end) )+")
                                '    Sql.Append("(sum(case when numAcntType=825 then GJD.numDebitAmt-GJD.numCreditAmt else 0 end) -")
                                '    Sql.Append("sum(case when numAcntType=826 then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)) as  '" & strOrderListText(iOrder) & "'")

                                Case "Income" : Sql.Append(strN & "sum(case when C.vcAccountCode ilike '010301%' then GJD.numCreditAmt-GJD.numDebitAmt else 0 end) as  '" & strOrderListText(iOrder) & "'")
                                Case "Expense" : Sql.Append(strN & "sum(case when C.vcAccountCode ilike '010401%' then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)  as  '" & strOrderListText(iOrder) & "'")
                                Case "COGS" : Sql.Append(strN & "sum(case when  C.vcAccountCode ilike 'COGS%' then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)  as '" & strOrderListText(iOrder) & "'")
                                Case "OIncome" : Sql.Append(strN & "sum(case when C.vcAccountCode ilike '010302%' then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)  as  '" & strOrderListText(iOrder) & "'")
                                Case "OExpense" : Sql.Append(strN & "sum(case when C.vcAccountCode ilike '010402%' then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)  as  '" & strOrderListText(iOrder) & "'")
                                Case "GrossProfit"
                                    Sql.Append(strN & "(sum(case when C.vcAccountCode ilike '010301%' then GJD.numCreditAmt-GJD.numDebitAmt else 0 end) - ")
                                    Sql.Append("sum(case when C.vcAccountCode ilike 'COGS%' then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)) as  '" & strOrderListText(iOrder) & "'")
                                Case "NetIncome"
                                    Sql.Append(strN & "((sum(case when C.vcAccountCode ilike '010301%' then GJD.numCreditAmt-GJD.numDebitAmt else 0 end) - ")
                                    Sql.Append("sum(case when C.vcAccountCode ilike 'COGS%' then GJD.numDebitAmt-GJD.numCreditAmt else 0 end))-")
                                    Sql.Append("sum(case when C.vcAccountCode ilike '010401%' then GJD.numDebitAmt-GJD.numCreditAmt else 0 end) )+")
                                    Sql.Append("(sum(case when C.vcAccountCode ilike '010302%' then GJD.numDebitAmt-GJD.numCreditAmt else 0 end) -")
                                    Sql.Append("sum(case when C.vcAccountCode ilike '010402%' then GJD.numDebitAmt-GJD.numCreditAmt else 0 end)) as  '" & strOrderListText(iOrder) & "'")
                                Case "ARecivable" : Sql.Append(strN & "sum(case when C.vcAccountCode ilike '01010105%' then GJD.numDebitAmt-GJD.numCreditAmt else 0 end) as '" & strOrderListText(iOrder) & "'")
                                Case "APayable" : Sql.Append(strN & "sum(case when C.vcAccountCode ilike '01020102%' then GJD.numCreditAmt-GJD.numDebitAmt else 0 end) as '" & strOrderListText(iOrder) & "'")
                                Case Else
                                    Sql.Append(strN & strOrderListValues(iOrder).Split("~")(0) & " as '" & strOrderListText(iOrder) & "'")
                            End Select
                        Else
                            If strOrderListValues(iOrder).Split("~")(3) = "Contact" Then
                                Sql.Append(strN & "fn_GetContactName(" & strOrderListValues(iOrder).Split("~")(0) & ") as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "Drop Down List" Then
                                Sql.Append(strN & "fn_GetListItemName(" & strOrderListValues(iOrder).Split("~")(0) & ") as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "Terriotary" Then
                                Sql.Append(strN & "fn_GetTerritoryName(" & strOrderListValues(iOrder).Split("~")(0) & ") as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "Company" Then
                                Sql.Append(strN & "fn_GetComapnyName(" & strOrderListValues(iOrder).Split("~")(0) & ") as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "Group" Then
                                Sql.Append(strN & "fn_GetGroupName(" & strOrderListValues(iOrder).Split("~")(0) & ") as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "State" Then
                                Sql.Append(strN & "fn_GetState(" & strOrderListValues(iOrder).Split("~")(0) & ") as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "Customdata" Then
                                Sql.Append(strN & "fn_getCustomDataValue(" & strOrderListValues(iOrder).Split("~")(0) & "," & strOrderListValues(iOrder).Split("~")(5) & ") as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "Date Field" Then
                                Sql.Append(strN & "FormatedDateTimeFromDate(" & strOrderListValues(iOrder).Split("~")(0) & ",@numDomainId) as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "Date Field U" Then
                                Sql.Append(strN & "FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset," & strOrderListValues(iOrder).Split("~")(0) & "),@numDomainId) as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "Date Field D" Then
                                Sql.Append(strN & "FormatedDateFromDate(" & strOrderListValues(iOrder).Split("~")(0) & ",@numDomainId) as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "UOM" Then
                                Sql.Append(strN & "fn_GetUOMName(" & strOrderListValues(iOrder).Split("~")(0) & ") as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "ItemGroup" Then
                                Sql.Append(strN & "fn_GetItemGroupsName(" & strOrderListValues(iOrder).Split("~")(0) & ") as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "IncomeAccount" Or strOrderListValues(iOrder).Split("~")(3) = "AssetAcCOA" Or strOrderListValues(iOrder).Split("~")(3) = "COG" Then
                                Sql.Append(strN & "fn_GetChart_Of_AccountsName(" & strOrderListValues(iOrder).Split("~")(0) & ") as '" & strOrderListText(iOrder) & "'")
                            ElseIf strOrderListValues(iOrder).Split("~")(3) = "Currency" Then
                                Sql.Append(strN & "fn_GetCurrencyName(" & strOrderListValues(iOrder).Split("~")(0) & ") as '" & strOrderListText(iOrder) & "'")
                            Else
                                If strOrderListValues(iOrder).Split("~")(0).ToString.ToLower.Contains("numcheckno") Then 'added by chintan reason: Custom report was showing check no as decimal with "," e.g. 1,256.00
                                    Sql.Append(strN & "CAST(" & strOrderListValues(iOrder).Split("~")(0) & " AS VARCHAR) as '" & strOrderListText(iOrder) & "'")
                                Else
                                    Sql.Append(strN & strOrderListValues(iOrder).Split("~")(0) & " as '" & strOrderListText(iOrder) & "'")
                                End If
                            End If
                        End If
                    End If
                Next
                '========================================================================================================================
                '========================================================================================================================
                'for summatiob Grid
                '========================================================================================================================
                '========================================================================================================================
                '===========<StandardFilters>
                Dim SqlSummGrid1 As New System.Text.StringBuilder
                Dim SqlSummGrid2 As New System.Text.StringBuilder
                Dim SqlSummGrid3 As New System.Text.StringBuilder
                Dim SqlSummGridFilter As New System.Text.StringBuilder
                SqlSummGrid1.Append(" ")

                If chkSummationGrid.Checked = True Then
                    Dim ddlGrp1List As New DropDownList
                    ddlGrp1List = radOppTab.MultiPage.FindControl("ddlGrp1List")
                    If ddlGrp1List.SelectedValue.Split("~")(4) = 1 Then  'If Custom field
                        Dim strCustFunVal As String = ""
                        Dim tblAlias, CFWColumn, strCustFieldID

                        strCustFieldID = ddlGrp1List.SelectedValue.Split("~")(1).Split("d")(1)

                        Select Case ddlGrp1List.SelectedValue.Split("~")(2)
                            Case 6
                                strCustFunVal = ",4,ACI.numContactId"
                                tblAlias = "CFW_FLD_Values_Cont"

                                CFW_FLD_Values_Cont.Add("[" & strCustFieldID & "]")
                            Case 7, 8
                                strCustFunVal = ",1,DivisionMaster.numDivisionId"
                                tblAlias = "CFW_FLD_Values"

                                CFW_FLD_Values.Add("[" & strCustFieldID & "]")
                            Case 10, 11, 12, 13, 14
                                strCustFunVal = ",1,ProMas.numProId"
                                tblAlias = "CFW_FLD_Values"

                                CFW_FLD_Values.Add("[" & strCustFieldID & "]")
                            Case 15, 16, 17
                                strCustFunVal = ",3,Cases.numCaseId"
                                tblAlias = "CFW_FLD_Values_Case"

                                CFW_FLD_Values_Case.Add("[" & strCustFieldID & "]")
                            Case 18, 19, 20, 21, 22, 23, 25, 26
                                strCustFunVal = ",6,OppMas.numOppid"
                                tblAlias = "CFW_Fld_Values_Opp"

                                CFW_Fld_Values_Opp.Add("[" & strCustFieldID & "]")
                            Case 9
                                strCustFunVal = ",11,ProMas.numProId"
                                tblAlias = "CFW_FLD_Values_Pro"

                                CFW_FLD_Values_Pro.Add("[" & strCustFieldID & "]")
                            Case 27
                                strCustFunVal = ",5,Item.numItemCode"
                                tblAlias = "CFW_FLD_Values_Item"

                                CFW_FLD_Values_Item.Add("[" & strCustFieldID & "]")
                            Case Else
                                strCustFunVal = ",1,DivisionMaster.numDivisionId"
                                tblAlias = "CFW_FLD_Values"

                                CFW_FLD_Values.Add("[" & strCustFieldID & "]")
                        End Select

                        CFWColumn = tblAlias & ".[" & strCustFieldID & "]"

                        If ddlGrp1List.SelectedValue.Split("~")(3) = "Drop Down List" Then
                            SqlSummGrid1.Append(" fn_GetListItemName(" & CFWColumn & ") as '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_GetListItemName(" & CFWColumn & ")")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Check box" Then
                            SqlSummGrid1.Append(" fn_getCustomDataValue(" & CFWColumn & ",9) as '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_getCustomDataValue(" & CFWColumn & ",9)")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field U" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field D" Then
                            Dim strGetDate As String
                            If ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field D" Then
                                strGetDate = "getDate()"
                            ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field U" Then
                                strGetDate = "getUtcDate()"
                            End If
                            Select Case ddlGrp1Filter.SelectedValue
                                Case 0
                                    SqlSummGrid1.Append("  FormatedDateFromDate(" & CFWColumn & ",@numDomainId) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append("  ,year(" & CFWColumn & ") as  'year'")
                                    SqlSummGrid2.Append(" group by FormatedDateFromDate(" & CFWColumn & ",@numDomainId) ")
                                    SqlSummGrid2.Append("  ,year(" & CFWColumn & ") ")
                                Case 1
                                    SqlSummGrid1.Append("   FormatedDateFromDate(get_week_start(" & CFWColumn & "),@numDomainID)+' - '+ FormatedDateFromDate(get_week_end( " & CFWColumn & "),@numDomainID) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append("  ,DATEPART(year," & CFWColumn & ") as  'year'")
                                    SqlSummGrid2.Append(" group by FormatedDateFromDate(get_week_start(" & CFWColumn & "),@numDomainID)+' - '+ FormatedDateFromDate(get_week_end( " & CFWColumn & "),@numDomainID)")
                                    SqlSummGrid2.Append(" ,DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ") ")
                                Case 2
                                    SqlSummGrid1.Append("  DATENAME(month," & CFWColumn & ")+'-'+convert(varchar(4),DATEPART(year," & CFWColumn & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append(" ,DATEPART(year," & CFWColumn & ") as  'year'")
                                    SqlSummGrid2.Append(" group by DATENAME(month," & CFWColumn & ")+'-'+convert(varchar(4),DATEPART(year," & CFWColumn & "))")
                                    SqlSummGrid2.Append(" ,DATEPART(year," & CFWColumn & ") ")
                                Case 3
                                    SqlSummGrid1.Append("  fn_getCustomDataValue(DATEPART(q," & CFWColumn & "),13)+'-'+convert(varchar(4),DATEPART(year," & CFWColumn & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append("  ,DATEPART(year," & CFWColumn & ") as  'year'")
                                    SqlSummGrid2.Append(" group by fn_getCustomDataValue(DATEPART(q," & CFWColumn & "),13)+'-'+convert(varchar(4),DATEPART(year," & CFWColumn & ")) ")
                                    SqlSummGrid2.Append(" ,DATEPART(year," & CFWColumn & ") ")
                                Case 4
                                    SqlSummGrid1.Append("  convert(varchar(4),DATEPART(year," & CFWColumn & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append("  ,convert(varchar(4),DATEPART(year," & CFWColumn & ")) as  'year'")
                                    SqlSummGrid2.Append(" group by convert(varchar(4),DATEPART(year," & CFWColumn & ")) ")
                                    'SqlSummGrid2.Append(" group by DATEPART(year," & CFWColumn & ") ")
                                Case 5
                                    SqlSummGrid1.Append(" fn_getCustomDataValue(GetFiscalQuarter(" & CFWColumn & ",@numDomainId),13) ")
                                    SqlSummGrid1.Append(" +' '+FormatedDateFromDate(GetFQDate(" & CFWColumn & ", GetFiscalQuarter(" & CFWColumn & ",@numDomainId),'S',@numDomainId),@numDomainId)")
                                    SqlSummGrid1.Append("+'- '+FormatedDateFromDate(GetFQDate(" & CFWColumn & ", GetFiscalQuarter(" & CFWColumn & ",@numDomainId),'E',@numDomainId),@numDomainId) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append(" ,FormatedDateFromDate(GetFiscalStartDate(getFiscalYear(" & CFWColumn & ",@numDomainId),@numDomainId),@numDomainId)as 'Fstart'")
                                    SqlSummGrid2.Append(" group by  fn_getCustomDataValue(GetFiscalQuarter(" & CFWColumn & ",@numDomainId),13)")
                                    SqlSummGrid2.Append(" +' '+FormatedDateFromDate(GetFQDate(" & CFWColumn & ", GetFiscalQuarter(" & CFWColumn & ",@numDomainId),'S',@numDomainId),@numDomainId)")
                                    SqlSummGrid2.Append("+'- '+FormatedDateFromDate(GetFQDate(" & CFWColumn & ", GetFiscalQuarter(" & CFWColumn & ",@numDomainId),'E',@numDomainId),@numDomainId) ")
                                    SqlSummGrid2.Append(" ,FormatedDateFromDate(GetFiscalStartDate(getFiscalYear(" & CFWColumn & ",@numDomainId),@numDomainId),@numDomainId)")
                                Case 6
                                    SqlSummGrid1.Append(" FormatedDateFromDate(GetFiscalStartDate(GetFiscalyear(" & CFWColumn & ",@numDomainId),@numDomainId),@numDomainId)")
                                    SqlSummGrid1.Append(" +'-'+ FormatedDateFromDate(dateadd(year,1,GetFiscalStartDate(GetFiscalyear(" & CFWColumn & ",@numDomainId),@numDomainId)),@numDomainId) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid2.Append(" group by  FormatedDateFromDate(GetFiscalStartDate(GetFiscalyear(" & CFWColumn & ",@numDomainId),@numDomainId),@numDomainId)")
                                    SqlSummGrid2.Append(" +'-'+ FormatedDateFromDate(dateadd(year,1,GetFiscalStartDate(GetFiscalyear(" & CFWColumn & ",@numDomainId),@numDomainId)),@numDomainId)")
                                Case 7
                                    SqlSummGrid1.Append("  DATENAME(month," & CFWColumn & ")+'-'+convert(varchar(4),DATEPART(year," & CFWColumn & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append("  ,DATEPART(year," & CFWColumn & ") as  'year'")
                                    SqlSummGrid2.Append(" group by DATENAME(month," & CFWColumn & ")+'-'+convert(varchar(4),DATEPART(year," & CFWColumn & "))")
                                    SqlSummGrid2.Append(" , DATEPART(year," & CFWColumn & ") ")
                                    SqlSummGridFilter.Append("and year(" & CFWColumn & ") = year(" & strGetDate & ")")
                                Case 8
                                    SqlSummGrid1.Append("  FormatedDateFromDate(" & CFWColumn & ",@numDomainId) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append(" ,year(" & CFWColumn & ") as  'year'")
                                    SqlSummGrid2.Append(" group by FormatedDateFromDate(" & CFWColumn & ",@numDomainId) ")
                                    SqlSummGrid2.Append(" ,year(" & CFWColumn & ") ")
                                    ' SqlSummGridFilter.Append("and year(ACI." & CFWColumn & ") = year(getdate())")
                                Case 9
                                    SqlSummGrid1.Append("  convert(varchar(4),DATEPART(week," & CFWColumn & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append(" ,convert(varchar(4),year(" & CFWColumn & ")) as  'year'")
                                    SqlSummGrid2.Append(" group by convert(varchar(4),DATEPART(week," & CFWColumn & ")) ")
                                    SqlSummGrid2.Append(" ,convert(varchar(4),year(" & CFWColumn & ")) ")
                                    'SqlSummGridFilter.Append("and year(" & CFWColumn & ") = year(getdate())")
                                    SqlSummGridFilter.Append(" and " & CFWColumn & " >= GetDateRange('LW','S')")
                                    SqlSummGridFilter.Append(" and " & CFWColumn & " < GetDateRange('TW','')")
                                    'SqlSummGridFilter.Append(" and year(" & CFWColumn & ") = case ")
                                    'SqlSummGridFilter.Append(" when datepart(week," & CFWColumn & ")= 1 then  year(" & CFWColumn & ")-1")
                                    'SqlSummGridFilter.Append(" when datepart(week," & CFWColumn & ")= 53 then  year(" & CFWColumn & ")+1")
                                    'SqlSummGridFilter.Append(" else year(getdate()) end ")
                                    'SqlSummGridFilter.Append("and DATEPART(week," & CFWColumn & ") in (datepart(week,getdate())-1,datepart(week,getdate()))")
                                Case 10
                                    SqlSummGrid1.Append("  convert(varchar(4),DATEPART(month," & CFWColumn & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append("  ,convert(varchar(4),year(" & CFWColumn & ")) as  'year'")
                                    SqlSummGrid2.Append(" group by convert(varchar(4),DATEPART(month," & CFWColumn & ")) ")
                                    SqlSummGrid2.Append(" ,convert(varchar(4),year(" & CFWColumn & ")) ")
                                    SqlSummGridFilter.Append(" and " & CFWColumn & " >= GetDateRange('LM','S')")
                                    SqlSummGridFilter.Append(" and " & CFWColumn & " < GetDateRange('TM','')")
                                    'SqlSummGridFilter.Append(" and year(" & CFWColumn & ") = case ")
                                    'SqlSummGridFilter.Append(" when datepart(month," & CFWColumn & ")= 1 then  year(" & CFWColumn & ")-1")
                                    ' SqlSummGridFilter.Append(" when datepart(month," & CFWColumn & ")= 12 then  year(" & CFWColumn & ")+1")
                                    ' SqlSummGridFilter.Append(" else year(getdate()) end ")
                                    ' SqlSummGridFilter.Append("and year(" & CFWColumn & ") = year(getdate())")
                                    'SqlSummGridFilter.Append("and month(" & CFWColumn & ") in (datepart(month,getdate())-1,datepart(month,getdate()))")
                                Case 11
                                    SqlSummGrid1.Append("  convert(varchar(4),DATEPART(year," & CFWColumn & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append("  ,convert(varchar(4),year(" & CFWColumn & ")) as  'year'")
                                    SqlSummGrid2.Append(" group by convert(varchar(4),DATEPART(year," & CFWColumn & ")) ")
                                    SqlSummGridFilter.Append(" and " & CFWColumn & " >= GetDateRange('LY','S')")
                                    SqlSummGridFilter.Append(" and " & CFWColumn & " < GetDateRange('TY','')")
                                    'SqlSummGrid2.Append(" ,year(" & CFWColumn & ") ")
                                    'SqlSummGridFilter.Append("and year(ACI." & CFWColumn & ") = year(getdate())")
                                    ' SqlSummGridFilter.Append("and year(" & CFWColumn & ") in (datepart(year,getdate())-1,datepart(year,getdate()))")
                                Case 12
                                    SqlSummGrid1.Append("  convert(varchar(4),DATEPART(q," & CFWColumn & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                    SqlSummGrid1.Append("  ,convert(varchar(4),year(" & CFWColumn & ")) as  'year'")
                                    SqlSummGrid2.Append(" group by convert(varchar(4),DATEPART(q," & CFWColumn & ")) ")
                                    SqlSummGrid2.Append(" ,convert(varchar(4),year(" & CFWColumn & ")) ")
                                    ' SqlSummGridFilter.Append("and year(" & CFWColumn & ") = year(getdate())")
                                    SqlSummGridFilter.Append(" and " & CFWColumn & " >= GetDateRange('LQ','S')")
                                    SqlSummGridFilter.Append(" and " & CFWColumn & " < GetDateRange('TQ','')")
                                    ' SqlSummGridFilter.Append(" when datepart(q," & CFWColumn & ")= 1 then  year(" & CFWColumn & ")-1")
                                    'SqlSummGridFilter.Append(" when datepart(q," & CFWColumn & ")= 4 then  year(" & CFWColumn & ")+1")
                                    'SqlSummGridFilter.Append(" else year(getdate()) end ")
                                    'SqlSummGridFilter.Append("and DATEPART(q," & CFWColumn & ") in (datepart(q,getdate())-1,datepart(q,getdate()))")
                            End Select
                        Else
                            SqlSummGrid1.Append(" " & CFWColumn & " as '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  " & CFWColumn & "")
                        End If
                    Else
                        If ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field U" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field D" Then
                            If ddlGrp1List.SelectedValue.Split("~")(2) = "31" Then
                            Else
                                Dim strGetDate As String
                                If ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field D" Then
                                    strGetDate = "getDate()"
                                ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field U" Then
                                    strGetDate = "getUtcDate()"
                                End If
                                Select Case ddlGrp1Filter.SelectedValue
                                    Case 0
                                        SqlSummGrid1.Append("  FormatedDateFromDate(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append("  ,year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  'year'")
                                        SqlSummGrid2.Append(" group by FormatedDateFromDate(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId) ")
                                        SqlSummGrid2.Append("  ,year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") ")
                                    Case 1
                                        SqlSummGrid1.Append("   FormatedDateFromDate(get_week_start(" & ddlGrp1List.SelectedValue.Split("~")(1) & "),@numDomainID)+' - '+ FormatedDateFromDate(get_week_end( " & ddlGrp1List.SelectedValue.Split("~")(1) & "),@numDomainID) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append("  ,DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  'year'")
                                        SqlSummGrid2.Append(" group by FormatedDateFromDate(get_week_start(" & ddlGrp1List.SelectedValue.Split("~")(1) & "),@numDomainID)+' - '+ FormatedDateFromDate(get_week_end( " & ddlGrp1List.SelectedValue.Split("~")(1) & "),@numDomainID)")
                                        SqlSummGrid2.Append(" ,DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ") ")
                                    Case 2
                                        SqlSummGrid1.Append("  DATENAME(month," & ddlGrp1List.SelectedValue.Split("~")(1) & ")+'-'+convert(varchar(4),DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append(" ,DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  'year'")
                                        SqlSummGrid2.Append(" group by DATENAME(month," & ddlGrp1List.SelectedValue.Split("~")(1) & ")+'-'+convert(varchar(4),DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & "))")
                                        SqlSummGrid2.Append(" ,DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ") ")
                                    Case 3
                                        SqlSummGrid1.Append("  fn_getCustomDataValue(DATEPART(q," & ddlGrp1List.SelectedValue.Split("~")(1) & "),13)+'-'+convert(varchar(4),DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append("  ,DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  'year'")
                                        SqlSummGrid2.Append(" group by fn_getCustomDataValue(DATEPART(q," & ddlGrp1List.SelectedValue.Split("~")(1) & "),13)+'-'+convert(varchar(4),DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) ")
                                        SqlSummGrid2.Append(" ,DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ") ")
                                    Case 4
                                        SqlSummGrid1.Append("  convert(varchar(4),DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append("  ,convert(varchar(4),DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  'year'")
                                        SqlSummGrid2.Append(" group by convert(varchar(4),DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) ")
                                        'SqlSummGrid2.Append(" group by DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ") ")
                                    Case 5
                                        SqlSummGrid1.Append(" fn_getCustomDataValue(GetFiscalQuarter(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),13) ")
                                        SqlSummGrid1.Append(" +' '+FormatedDateFromDate(GetFQDate(" & ddlGrp1List.SelectedValue.Split("~")(1) & ", GetFiscalQuarter(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),'S',@numDomainId),@numDomainId)")
                                        SqlSummGrid1.Append("+'- '+FormatedDateFromDate(GetFQDate(" & ddlGrp1List.SelectedValue.Split("~")(1) & ", GetFiscalQuarter(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),'E',@numDomainId),@numDomainId) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append(" ,FormatedDateFromDate(GetFiscalStartDate(getFiscalYear(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),@numDomainId),@numDomainId)as 'Fstart'")
                                        SqlSummGrid2.Append(" group by  fn_getCustomDataValue(GetFiscalQuarter(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),13)")
                                        SqlSummGrid2.Append(" +' '+FormatedDateFromDate(GetFQDate(" & ddlGrp1List.SelectedValue.Split("~")(1) & ", GetFiscalQuarter(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),'S',@numDomainId),@numDomainId)")
                                        SqlSummGrid2.Append("+'- '+FormatedDateFromDate(GetFQDate(" & ddlGrp1List.SelectedValue.Split("~")(1) & ", GetFiscalQuarter(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),'E',@numDomainId),@numDomainId) ")
                                        SqlSummGrid2.Append(" ,FormatedDateFromDate(GetFiscalStartDate(getFiscalYear(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),@numDomainId),@numDomainId)")
                                    Case 6
                                        SqlSummGrid1.Append(" FormatedDateFromDate(GetFiscalStartDate(GetFiscalyear(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),@numDomainId),@numDomainId)")
                                        SqlSummGrid1.Append(" +'-'+ FormatedDateFromDate(dateadd(year,1,GetFiscalStartDate(GetFiscalyear(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),@numDomainId)),@numDomainId) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid2.Append(" group by  FormatedDateFromDate(GetFiscalStartDate(GetFiscalyear(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),@numDomainId),@numDomainId)")
                                        SqlSummGrid2.Append(" +'-'+ FormatedDateFromDate(dateadd(year,1,GetFiscalStartDate(GetFiscalyear(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId),@numDomainId)),@numDomainId)")
                                    Case 7
                                        SqlSummGrid1.Append("  DATENAME(month," & ddlGrp1List.SelectedValue.Split("~")(1) & ")+'-'+convert(varchar(4),DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append("  ,DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  'year'")
                                        SqlSummGrid2.Append(" group by DATENAME(month," & ddlGrp1List.SelectedValue.Split("~")(1) & ")+'-'+convert(varchar(4),DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & "))")
                                        SqlSummGrid2.Append(" , DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ") ")
                                        SqlSummGridFilter.Append("and year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") = year(" & strGetDate & ")")
                                    Case 8
                                        SqlSummGrid1.Append("  FormatedDateFromDate(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append(" ,year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  'year'")
                                        SqlSummGrid2.Append(" group by FormatedDateFromDate(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId) ")
                                        SqlSummGrid2.Append(" ,year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") ")
                                        ' SqlSummGridFilter.Append("and year(ACI." & ddlGrp1List.SelectedValue.Split("~")(1) & ") = year(getdate())")
                                    Case 9
                                        SqlSummGrid1.Append("  convert(varchar(4),DATEPART(week," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append(" ,convert(varchar(4),year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  'year'")
                                        SqlSummGrid2.Append(" group by convert(varchar(4),DATEPART(week," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) ")
                                        SqlSummGrid2.Append(" ,convert(varchar(4),year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")) ")
                                        'SqlSummGridFilter.Append("and year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") = year(getdate())")
                                        SqlSummGridFilter.Append(" and " & ddlGrp1List.SelectedValue.Split("~")(1) & " >= GetDateRange('LW','S')")
                                        SqlSummGridFilter.Append(" and " & ddlGrp1List.SelectedValue.Split("~")(1) & " < GetDateRange('TW','')")
                                        'SqlSummGridFilter.Append(" and year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") = case ")
                                        'SqlSummGridFilter.Append(" when datepart(week," & ddlGrp1List.SelectedValue.Split("~")(1) & ")= 1 then  year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")-1")
                                        'SqlSummGridFilter.Append(" when datepart(week," & ddlGrp1List.SelectedValue.Split("~")(1) & ")= 53 then  year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")+1")
                                        'SqlSummGridFilter.Append(" else year(getdate()) end ")
                                        'SqlSummGridFilter.Append("and DATEPART(week," & ddlGrp1List.SelectedValue.Split("~")(1) & ") in (datepart(week,getdate())-1,datepart(week,getdate()))")
                                    Case 10
                                        SqlSummGrid1.Append("  convert(varchar(4),DATEPART(month," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append("  ,convert(varchar(4),year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  'year'")
                                        SqlSummGrid2.Append(" group by convert(varchar(4),DATEPART(month," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) ")
                                        SqlSummGrid2.Append(" ,convert(varchar(4),year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")) ")
                                        SqlSummGridFilter.Append(" and " & ddlGrp1List.SelectedValue.Split("~")(1) & " >= GetDateRange('LM','S')")
                                        SqlSummGridFilter.Append(" and " & ddlGrp1List.SelectedValue.Split("~")(1) & " < GetDateRange('TM','')")
                                        'SqlSummGridFilter.Append(" and year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") = case ")
                                        'SqlSummGridFilter.Append(" when datepart(month," & ddlGrp1List.SelectedValue.Split("~")(1) & ")= 1 then  year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")-1")
                                        ' SqlSummGridFilter.Append(" when datepart(month," & ddlGrp1List.SelectedValue.Split("~")(1) & ")= 12 then  year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")+1")
                                        ' SqlSummGridFilter.Append(" else year(getdate()) end ")
                                        ' SqlSummGridFilter.Append("and year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") = year(getdate())")
                                        'SqlSummGridFilter.Append("and month(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") in (datepart(month,getdate())-1,datepart(month,getdate()))")
                                    Case 11
                                        SqlSummGrid1.Append("  convert(varchar(4),DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append("  ,convert(varchar(4),year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  'year'")
                                        SqlSummGrid2.Append(" group by convert(varchar(4),DATEPART(year," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) ")
                                        SqlSummGridFilter.Append(" and " & ddlGrp1List.SelectedValue.Split("~")(1) & " >= GetDateRange('LY','S')")
                                        SqlSummGridFilter.Append(" and " & ddlGrp1List.SelectedValue.Split("~")(1) & " < GetDateRange('TY','')")
                                        'SqlSummGrid2.Append(" ,year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") ")
                                        'SqlSummGridFilter.Append("and year(ACI." & ddlGrp1List.SelectedValue.Split("~")(1) & ") = year(getdate())")
                                        ' SqlSummGridFilter.Append("and year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") in (datepart(year,getdate())-1,datepart(year,getdate()))")
                                    Case 12
                                        SqlSummGrid1.Append("  convert(varchar(4),DATEPART(q," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        SqlSummGrid1.Append("  ,convert(varchar(4),year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")) as  'year'")
                                        SqlSummGrid2.Append(" group by convert(varchar(4),DATEPART(q," & ddlGrp1List.SelectedValue.Split("~")(1) & ")) ")
                                        SqlSummGrid2.Append(" ,convert(varchar(4),year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")) ")
                                        ' SqlSummGridFilter.Append("and year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") = year(getdate())")
                                        SqlSummGridFilter.Append(" and " & ddlGrp1List.SelectedValue.Split("~")(1) & " >= GetDateRange('LQ','S')")
                                        SqlSummGridFilter.Append(" and " & ddlGrp1List.SelectedValue.Split("~")(1) & " < GetDateRange('TQ','')")
                                        ' SqlSummGridFilter.Append(" when datepart(q," & ddlGrp1List.SelectedValue.Split("~")(1) & ")= 1 then  year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")-1")
                                        'SqlSummGridFilter.Append(" when datepart(q," & ddlGrp1List.SelectedValue.Split("~")(1) & ")= 4 then  year(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")+1")
                                        'SqlSummGridFilter.Append(" else year(getdate()) end ")
                                        'SqlSummGridFilter.Append("and DATEPART(q," & ddlGrp1List.SelectedValue.Split("~")(1) & ") in (datepart(q,getdate())-1,datepart(q,getdate()))")
                                End Select
                            End If

                            ' SqlSummGrid1.Append("  convert(varchar," & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            ' SqlSummGrid2.Append(" group by convert(varchar," & ddlGrp1List.SelectedValue.Split("~")(1) & ",111) ")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Drop Down List" Then
                            SqlSummGrid1.Append(" fn_GetListItemName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_GetListItemName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Contact" Then
                            SqlSummGrid1.Append(" fn_GetContactName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_GetContactName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Company" Then
                            SqlSummGrid1.Append(" fn_GetComapnyName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_GetComapnyName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Group" Then
                            SqlSummGrid1.Append(" fn_GetGroupName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_GetGroupName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "State" Then
                            SqlSummGrid1.Append(" fn_GetState(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_GetState(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Territory" Then
                            SqlSummGrid1.Append(" fn_GetTerritoryName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_GetTerritoryName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Customdata" Then
                            SqlSummGrid1.Append(" fn_getCustomDataValue(" & ddlGrp1List.SelectedValue.Split("~")(1) & "," & ddlGrp1List.SelectedValue.Split("~")(5) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_getCustomDataValue(" & ddlGrp1List.SelectedValue.Split("~")(1) & "," & ddlGrp1List.SelectedValue.Split("~")(5) & ")")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "UOM" Then
                            SqlSummGrid1.Append(" fn_GetUOMName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_GetUOMName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "ItemGroup" Then
                            SqlSummGrid1.Append(" fn_GetItemGroupsName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_GetItemGroupsName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "IncomeAccount" Or ddlGrp1List.SelectedValue.Split("~")(3) = "AssetAcCOA" Or ddlGrp1List.SelectedValue.Split("~")(3) = "COG" Then
                            SqlSummGrid1.Append(" fn_GetChart_Of_AccountsName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_GetChart_Of_AccountsName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Currency" Then
                            SqlSummGrid1.Append(" fn_GetCurrencyName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by  fn_GetCurrencyName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                        Else
                            SqlSummGrid1.Append(" " & ddlGrp1List.SelectedValue.Split("~")(1) & " as  '" & ddlGrp1List.SelectedItem.Text & "'")
                            SqlSummGrid2.Append(" group by " & ddlGrp1List.SelectedValue.Split("~")(1))
                        End If
                    End If

                    'SqlSummGrid1.Append(" ,count(*) as  Count")
                    If chkSortRecCount.Checked = True Then
                        SqlSummGrid3.Append(" order by  RecCount  " & ddlGrp1Order.SelectedValue)
                    Else
                        SqlSummGrid3.Append(" order by  '" & ddlGrp1List.SelectedItem.Text & "'  " & ddlGrp1Order.SelectedValue)
                    End If
                    If ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field U" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field D" Then
                        SqlSummGridFilter.Append("")
                    End If
                End If

                Sql.Append(" from ")

                '========================================================================================================================
                '========================================================================================================================
                'Building the Filter 
                '========================================================================================================================
                '========================================================================================================================
                '===========<StandardFilters>
                Dim SqlFilterStd As New System.Text.StringBuilder
                SqlFilterStd.Append(" ")

                If CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue <> "0" Then
                    If CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(2) = 1 Then
                        Select Case CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(1)
                            Case 6
                                SqlFilterStd.Append(" and ACI.numContactID in (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(0).Split("d")(1) & " and convert(datetime,Fld_Value ) ")
                            Case 7, 8
                                SqlFilterStd.Append(" and DivisionMaster.numDivisionId in (select RecId from CFW_FLD_Values where Fld_ID =" & CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(0).Split("d")(1) & " and convert(datetime,Fld_Value ) ")
                            Case 10, 11, 12, 13, 14
                                SqlFilterStd.Append(" and ProMas.numProId in (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(0).Split("d")(1) & " and convert(datetime,Fld_Value ) ")
                            Case 15, 16, 17
                                SqlFilterStd.Append(" and Cases.numCaseId in (select RecId from CFW_FLD_Values_Case where Fld_ID =" & CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(0).Split("d")(1) & " and convert(datetime,Fld_Value ) ")
                            Case 18, 19, 20, 21, 22, 23, 24, 25, 26
                                SqlFilterStd.Append(" and OppMas.numOppId in (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(0).Split("d")(1) & " and convert(datetime,Fld_Value ) ")
                            Case 9
                                SqlFilterStd.Append(" and ProMas.numProId in (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(0).Split("d")(1) & " and convert(datetime,Fld_Value ) ")
                            Case 27
                                SqlFilterStd.Append(" and Item.numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(0).Split("d")(1) & " and convert(datetime,Fld_Value ) ")
                        End Select
                    Else
                        If CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~").Length >= 3 Then
                            If CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(3) = "Date Field U" Then
                                SqlFilterStd.Append(" and dateadd(minute,-@ClientTimeZoneOffset," & CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(0) & ")")
                            Else : SqlFilterStd.Append(" and " & CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(0))
                            End If
                        Else : SqlFilterStd.Append(" and " & CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(0))
                        End If
                    End If
                    If ddlCustomTime.SelectedValue <> "0" Then
                        Dim strStdF As String = ""
                        Dim TodaysDate As String = GetTodaysDate()
                        Select Case ddlCustomTime.SelectedValue
                            Case "yesterday" : strStdF = " between dateadd(day,-1," & TodaysDate & ") and " & TodaysDate
                            Case "today" : strStdF = " between " & TodaysDate & " and dateadd(day,1," & TodaysDate & ")"
                            Case "tomorrow" : strStdF = " between TodaysDate and dateadd(day,1," & TodaysDate & ")"
                            Case "last7" : strStdF = " between dateadd(day, -7, " & TodaysDate & ") and " & TodaysDate
                            Case "last15" : strStdF = " between dateadd(day, -15, " & TodaysDate & ") and " & TodaysDate
                            Case "last30" : strStdF = " between dateadd(day, -30, " & TodaysDate & ") and " & TodaysDate
                            Case "last60" : strStdF = " between dateadd(day, -60, " & TodaysDate & ") and " & TodaysDate
                            Case "last90" : strStdF = " between dateadd(day, -90, " & TodaysDate & ") and " & TodaysDate
                            Case "last120" : strStdF = " between dateadd(day, -120," & TodaysDate & ") and " & TodaysDate
                            Case "next7" : strStdF = " between  " & TodaysDate & " and dateadd(day, 7, " & TodaysDate & ") "
                            Case "next15" : strStdF = " between  " & TodaysDate & " and dateadd(day, 15, " & TodaysDate & ") "
                            Case "next30" : strStdF = " between  " & TodaysDate & " and dateadd(day, 30, " & TodaysDate & ") "
                            Case "next60" : strStdF = " between  " & TodaysDate & " and dateadd(day, 60, " & TodaysDate & ") "
                            Case "next90" : strStdF = " between  " & TodaysDate & " and dateadd(day, 90, " & TodaysDate & ") "
                            Case "next120" : strStdF = " between  " & TodaysDate & " and dateadd(day, 120, " & TodaysDate & ") "
                            Case "lastweek" : strStdF = " between dateadd(week, -1, datediff(d,0,get_week_start(" & TodaysDate & "))) and get_week_start(" & TodaysDate & ")"
                            Case "thisweek" : strStdF = " between get_week_start(" & TodaysDate & ") and get_week_end(" & TodaysDate & ")"
                            Case "nextweek" : strStdF = " between dateadd(day, 1, datediff(d,0,get_week_end(" & TodaysDate & "))) and dateadd(day, 7, datediff(d,0,get_week_end(" & TodaysDate & ")))"
                            Case "lastmonth" : strStdF = " between get_month_start(dateadd(month, -1, " & TodaysDate & ")) and get_month_end(dateadd(month, -1, " & TodaysDate & "))"
                            Case "thismonth" : strStdF = " between get_month_start( " & TodaysDate & ") and get_month_end(" & TodaysDate & ")"
                            Case "nextmonth" : strStdF = " between get_month_start(dateadd(month, 1," & TodaysDate & ")) and get_month_end(dateadd(month, 1, " & TodaysDate & "))"
                            Case "lastthismonth" : strStdF = " between get_month_start(dateadd(month, -1, " & TodaysDate & ")) and get_month_end(" & TodaysDate & ")"
                            Case "thisnextmonth" : strStdF = " between get_month_start(" & TodaysDate & ") and get_month_end(dateadd(month, 1, " & TodaysDate & "))"
                            Case "cury" : strStdF = " between DATEADD(year,DATEDIFF(year,0, " & TodaysDate & "),0) and DATEADD(year,DATEDIFF(year,0, " & TodaysDate & "),ufn_GetDaysInYear(dateadd(year,0," & TodaysDate & ")))"
                            Case "prevy" : strStdF = " between convert(varchar(4),year(dateadd(year,-1," & TodaysDate & "))) +'-'+'01'+'-'+'01' and convert(varchar(4),year(dateadd(year,-1," & TodaysDate & ")))+'-'+'12'+'-'+'31'"
                            Case "prev2y" : strStdF = " between convert(varchar(4),year(dateadd(year,-2," & TodaysDate & ")))) +'-'+'01'+'-'+'01' and convert(varchar(4),year(dateadd(year,-1," & TodaysDate & ")))+'-'+'12'+'-'+'31'"
                            Case "ago2y" : strStdF = " < convert(varchar(4),year(dateadd(year,-2," & TodaysDate & "))) +'-'+'01'+'-'+'01' "
                            Case "nexty" : strStdF = " between convert(varchar(4),year(dateadd(year,1," & TodaysDate & "))) +'-'+'01'+'-'+'01' and convert(varchar(4),year(dateadd(year,1," & TodaysDate & ")))+'-'+'12'+'-'+'31'"
                            Case "prevcury" : strStdF = " between convert(varchar(4),year(dateadd(year,-1," & TodaysDate & "))) +'-'+'01'+'-'+'01' and convert(varchar(4),year(dateadd(year,0," & TodaysDate & ")))+'-'+'12'+'-'+'31'"
                            Case "prevcur2y" : strStdF = " between convert(varchar(4),year(dateadd(year,-2," & TodaysDate & "))) +'-'+'01'+'-'+'01' and convert(varchar(4),year(dateadd(year,0," & TodaysDate & ")))+'-'+'12'+'-'+'31'"
                            Case "curnexty" : strStdF = " between convert(varchar(4),year(dateadd(year,0," & TodaysDate & "))) +'-'+'01'+'-'+'01' and convert(varchar(4),year(dateadd(year,1," & TodaysDate & ")))+'-'+'12'+'-'+'31'"
                        End Select
                        SqlFilterStd.Append(strStdF)
                    ElseIf (Not calFrom.SelectedDate Is Nothing) And (Not calTo.SelectedDate Is Nothing) Then
                        Dim fromDate As DateTime = calFrom.SelectedDate
                        Dim toDate As DateTime = calTo.SelectedDate
                        SqlFilterStd.Append(" between '" & fromDate.Year & "-" & fromDate.Month & "-" & fromDate.Day & "' and '" & toDate.Year & "-" & toDate.Month & "-" & toDate.Day & "'")
                    End If
                    If CType(radOppTab.MultiPage.FindControl("ddlStdfilter"), DropDownList).SelectedValue.Split("~")(2) = 1 Then
                        SqlFilterStd.Append(" ) ")
                    End If
                End If

                Dim iFilter As Integer = 0
                Dim sqlfilter As New System.Text.StringBuilder
                Dim Relation As String
                If radAnd.Checked = True Then
                    Relation = " And "
                Else : Relation = " Or "
                End If
                sqlfilter.Append(" ")
                Dim strJoinADC As Boolean = False

                Dim sqlFrom As New System.Text.StringBuilder
                sqlFrom.Append(" ")

                'Dim strCustomFieldArray As String() = New String(CustomFieldArray.Count - 1) {}
                'CustomFieldArray.CopyTo(strCustomFieldArray)

                'Dim strCustomItems As String() = strCustomFieldArray.Distinct().Where(Function(x) x.Contains("CFW_FLD_Values_Item")).ToArray

                ' If strJoinADC = False Then
                If lstGroup.SelectedValue = 4 Then
                    sqlFrom.Append(" AdditionalContactsInformation ACI join DivisionMaster on DivisionMaster.numDivisionID=ACI.numDivisionID  join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid  where ACI.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 5 Then
                    sqlFrom.Append(" DivisionMaster join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID  ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD1 ON AD1.numRecordID=DivisionMaster.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD2 ON AD2.numRecordID=DivisionMaster.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 ")
                    sqlFrom.Append("  where ACI.numDomainId = @numDomainID ")
                ElseIf lstGroup.SelectedValue = 6 Then
                    sqlFrom.Append(" DivisionMaster join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid  join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD1 ON AD1.numRecordID=DivisionMaster.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD2 ON AD2.numRecordID=DivisionMaster.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 ")
                    sqlFrom.Append(" where numContactType=70 and DivisionMaster.numDomainId = @numDomainID ")
                    ' sqlFrom.Append(" join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                ElseIf lstGroup.SelectedValue = 7 Then
                    sqlFrom.Append("   DivisionMaster  left join CompanyAssets on companyassets.numDivId  = DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD1 ON AD1.numRecordID=DivisionMaster.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD2 ON AD2.numRecordID=DivisionMaster.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 ")
                    sqlFrom.Append(" left join Item  on Item.numItemcode =CompanyAssets.numItemcode ")
                    sqlFrom.Append(" left join WareHouseItmsDTL on WareHouseItmsDTL.numWarehouseitmsdtlid = CompanyAssets.numWarehouseitmsdtlid")
                    sqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")
                    sqlFrom.Append(" where DivisionMaster.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 8 Then
                    sqlFrom.Append(" ProjectsMaster ProMas")

                    sqlFrom.Append(" left join DivisionMaster on ProMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")

                    sqlFrom.Append(" where ProMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 9 Then
                    sqlFrom.Append(" ProjectsMaster ProMas")
                    sqlFrom.Append(" Left Join ProjectsExpense ProExp on ProMas.numProId = ProExp.numProId")
                    sqlFrom.Append(" Left Join ProjectsTime ProTime on ProMas.numProId = ProTime.numProId")

                    sqlFrom.Append(" left join DivisionMaster on ProMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")

                    sqlFrom.Append(" where ProMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 10 Then
                    sqlFrom.Append(" ProjectsMaster ProMas")
                    sqlFrom.Append(" Left join StagePercentageDetails SPDtl on ProMas.numProId = SPDtl.numProjectID ")

                    sqlFrom.Append(" left join DivisionMaster on ProMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")

                    sqlFrom.Append(" where ProMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 11 Then
                    sqlFrom.Append(" ProjectsMaster ProMas")
                    sqlFrom.Append(" Left Join ProjectsContacts ProCon on ProMas.numProid =ProCon.numProid ")

                    sqlFrom.Append(" left join DivisionMaster on ProMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")

                    sqlFrom.Append(" where ProMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 12 Then
                    sqlFrom.Append(" Cases ")
                    sqlFrom.Append(" where Cases.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 13 Then
                    sqlFrom.Append(" Cases ")
                    sqlFrom.Append(" Join ContractManagement  on Cases.numContractId = ContractManagement.numContractId ")
                    'sqlFrom.Append(" Left Join CaseExpense  on Cases.numCaseId = CaseExpense.numCaseId")
                    'sqlFrom.Append(" Left Join CaseTime  on Cases.numCaseId = CaseTime.numCaseId")
                    sqlFrom.Append(" where Cases.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 14 Then
                    sqlFrom.Append(" OpportunityMaster OppMas ")
                    'sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")

                    sqlFrom.Append(" left join DivisionMaster on OppMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" left join AdditionalContactsInformation ACI on OppMas.numContactId=ACI.numContactId ")

                    sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 15 Then
                    sqlFrom.Append(" OpportunityMaster OppMas ")
                    'sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                    sqlFrom.Append(" Left Join OpportunityTime OppTime on OppMas.numOppId = OppTime.numOppId")
                    sqlFrom.Append(" Left Join OpportunityExpense OppExp on OppMas.numOppId = OppExp.numOppId")

                    sqlFrom.Append(" left join DivisionMaster on OppMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" left join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID and OppMas.numContactId=ACI.numContactId ")

                    sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 16 Then
                    sqlFrom.Append(" OpportunityMaster OppMas ")
                    ' sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                    sqlFrom.Append(" Left join StagePercentageDetails SPDtl on OppMas.numOppId = SPDtl.numOppID ")

                    sqlFrom.Append(" left join DivisionMaster on OppMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" left join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID and OppMas.numContactId=ACI.numContactId ")

                    sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 17 Then
                    sqlFrom.Append(" OpportunityMaster OppMas ")
                    'sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                    sqlFrom.Append(" Left Join OpportunityContact OppCon on OppMas.numOppId = OppCon.numOppId")

                    sqlFrom.Append(" left join DivisionMaster on OppMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" left join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID and OppMas.numContactId=ACI.numContactId ")

                    sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 18 Then
                    sqlFrom.Append(" OpportunityMaster OppMas ")
                    ' sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                    sqlFrom.Append(" Left Join OpportunityBizDocs OppBDoc on OppMas.numOppId = OppBDoc.numOppId")

                    sqlFrom.Append(" left join DivisionMaster on OppMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" left join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID and OppMas.numContactId=ACI.numContactId ")
                    'sqlFrom.Append(" left join listdetails LD on LD.numlistitemid=OppMas.intBillingdays ")
                    sqlFrom.Append(" left join BillingTerms BL ON BL.numTermsID=OppMas.intBillingdays ")

                    sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 19 Then
                    sqlFrom.Append(" OpportunityMaster OppMas ")
                    ' sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                    sqlFrom.Append(" Left Join OpportunityItems OppItems on OppMas.numOppId = OppItems.numOppId")
                    sqlFrom.Append(" Left Join Item  on OppItems.numItemcode = Item.numItemcode")
                    sqlFrom.Append(" Left Join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseItemID = OppItems.numWarehouseItmsID")
                    sqlFrom.Append(" Left Join Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID")
                    sqlFrom.Append(" Left Join WareHouseItmsDTL  on WareHouseItems.numWareHouseItemId = WareHouseItmsDTL.numWareHouseItemId")

                    sqlFrom.Append(" left join DivisionMaster on OppMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" left join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID and OppMas.numContactId=ACI.numContactId ")
                    sqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")

                    sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 63 Then
                    sqlFrom.Append(" OpportunityMaster OppMas ")

                    sqlFrom.Append(" Left Join OpportunityItems OppItems on OppMas.numOppId = OppItems.numOppId")
                    sqlFrom.Append(" Left Join Item  on OppItems.numItemcode = Item.numItemcode")
                    sqlFrom.Append(" Left Join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseItemID = OppItems.numWarehouseItmsID")
                    sqlFrom.Append(" Left Join Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID")
                    sqlFrom.Append(" Left Join WareHouseItmsDTL  on WareHouseItems.numWareHouseItemId = WareHouseItmsDTL.numWareHouseItemId")

                    sqlFrom.Append(" Left Join OpportunityBizDocs OppBDoc on OppMas.numOppId = OppBDoc.numOppId")

                    sqlFrom.Append(" Left Join opportunitybizdocsdetails on opportunitybizdocsdetails.numDomainId = @numDomainId and opportunitybizdocsdetails.numBizDocsId=OppBDoc.numOppBizDocsId")

                    sqlFrom.Append(" left join DivisionMaster on OppMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" left join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID and OppMas.numContactId=ACI.numContactId ")
                    'sqlFrom.Append(" left join listdetails LD on LD.numlistitemid=OppMas.intBillingdays ")
                    sqlFrom.Append(" left join BillingTerms BL ON BL.numTermsID=OppMas.intBillingdays ")

                    sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")

                ElseIf lstGroup.SelectedValue = 64 Then
                    sqlFrom.Append(" OpportunityMaster OppMas ")

                    sqlFrom.Append(" Left Join OpportunityBizDocs OppBDoc on OppMas.numOppId = OppBDoc.numOppId")
                    sqlFrom.Append(" Left Join OpportunityBizDocItems OppBizItems on OppBDoc.numOppBizDocsId = OppBizItems.numOppBizDocID")

                    sqlFrom.Append(" Left Join OpportunityItems OppItems on OppMas.numOppId = OppItems.numOppId and OppBizItems.numOppItemID=oppItems.numoppitemtCode")
                    sqlFrom.Append(" Left Join Item  on OppItems.numItemcode = Item.numItemcode")

                    sqlFrom.Append(" Left Join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseItemID = OppItems.numWarehouseItmsID")
                    sqlFrom.Append(" Left Join Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID")
                    sqlFrom.Append(" Left Join WareHouseItmsDTL  on WareHouseItems.numWareHouseItemId = WareHouseItmsDTL.numWareHouseItemId")

                    sqlFrom.Append(" left join DivisionMaster on OppMas.numDivisionId=DivisionMaster.numDivisionId and DivisionMaster.numDomainId = @numDomainID ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" left join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID and OppMas.numContactId=ACI.numContactId and ACI.numContactType=70 ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD1 ON AD1.numRecordID=DivisionMaster.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD2 ON AD2.numRecordID=DivisionMaster.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 ")

                    'sqlFrom.Append(" left join listdetails LD on LD.numlistitemid=OppMas.intBillingdays ")
                    sqlFrom.Append(" left join BillingTerms BL ON BL.numTermsID=OppMas.intBillingdays ")

                    sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 20 Then
                    sqlFrom.Append(" Item  ")
                    sqlFrom.Append(" Left Join ItemGroups ItemGroup on ItemGroup.numItemGroupId = Item.numItemGroup")
                    sqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")

                    sqlFrom.Append(" where Item.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 22 Then
                    sqlFrom.Append(" Item  ")
                    'sqlFrom.Append("  Join ItemGroups ItemGroup on ItemGroup.numItemGroupId = Item.numItemGroup")
                    sqlFrom.Append("  Join ItemGroupsDTL ItemGroupDTL on ItemGroupDTL.numOppAccAttrID=Item.numItemCode and ItemGroupDTL.tintType = 1")
                    sqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")
                    sqlFrom.Append(" where Item.numDomainId = @numDomainID ")
                ElseIf lstGroup.SelectedValue = 23 Then
                    sqlFrom.Append(" Item  ")
                    sqlFrom.Append("  join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and Item.numItemCode=WareHouseItems.numItemID")
                    sqlFrom.Append("  join Warehouses Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID=WareHouseItems.numWareHouseID")

                    sqlFrom.Append("  Join ItemGroups ItemGroup on ItemGroup.numDomainId = @numDomainID and ItemGroup.numItemGroupId = Item.numItemGroup")
                    sqlFrom.Append("  Join ItemGroupsDTL ItemGroupDTL on ItemGroup.numItemGroupId = ItemGroupDTL.numItemGroupId and ItemGroupDTL.tintType = 2")
                    sqlFrom.Append("  Join CFW_Fld_Master CFM on ItemGroupDTL.numOppAccAttrID=CFM.fld_id and Grp_id=9  and subgrp=0 and CFM.numDomainID=@numDomainID")
                    sqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")
                    sqlFrom.Append(" where Item.numDomainId = @numDomainID  ")
                ElseIf lstGroup.SelectedValue = 21 Then
                    sqlFrom.Append(" WareHouses  ")
                    sqlFrom.Append(" Left Join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseId = WareHouses.numWareHouseId")
                    sqlFrom.Append(" Left Join WareHouseItmsDTL  on WareHouseItems.numWareHouseItemId = WareHouseItmsDTL.numWareHouseItemId")
                    sqlFrom.Append(" Left Join Item  on WareHouseItems.numItemId = Item.numItemCode")
                    sqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")
                    sqlFrom.Append(" where Item.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 24 Then
                    sqlFrom.Append(" Forecast  ")
                    sqlFrom.Append(" Left Join Item  on Forecast.numItemCode = Item.numItemCode")
                    sqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")
                    sqlFrom.Append(" where Forecast.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 25 Then
                    sqlFrom.Append(" TrackingVisitorsHDR TracVisHDR")
                    sqlFrom.Append(" Join TrackingVisitorsDTL TracVisDtl on TracVisDtl.numTracVisitorsHDRID = TracVisHDR.numTrackingID")
                    sqlFrom.Append(" Left Join PageMasterWebAnlys PWebAnly on TracVisDtl.numPageID = PWebAnly.numPageID")
                    sqlFrom.Append(" Join DivisionMaster on TracVisHDR.numDivisionID = DivisionMaster.numDivisionID")
                    sqlFrom.Append(" where DivisionMaster.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 26 Then
                    sqlFrom.Append(" CampaignMaster CampMas")
                    sqlFrom.Append(" where CampMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 27 Then
                    sqlFrom.Append(" AdditionalContactsInformation ACI ")
                    sqlFrom.Append("join  communication Comm on ACI.numcontactId = Comm.numContactid")
                    sqlFrom.Append(" left join DivisionMaster on DivisionMaster.numDivisionId = Comm.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" where ACI.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 28 Then
                    sqlFrom.Append(" DivisionMaster join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append("join  communication Comm on DivisionMaster.numDivisionId = Comm.numDivisionId")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD1 ON AD1.numRecordID=DivisionMaster.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD2 ON AD2.numRecordID=DivisionMaster.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 ")

                    sqlFrom.Append(" where DivisionMaster.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 61 Then
                    sqlFrom.Append(" DivisionMaster join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    'sqlFrom.Append(" join  CompanyAssociations on DivisionMaster.numDivisionId = CompanyAssociations.numAssociateFromDivisionID ")
                    'sqlFrom.Append(" join  DivisionMaster CADivisionMaster on CADivisionMaster.numDivisionId = CompanyAssociations.numDivisionID ")
                    'sqlFrom.Append(" join  CompanyInfo CACompanyInfo on CADivisionMaster.numCompanyID = CACompanyInfo.numCompanyID ")

                    sqlFrom.Append(" Join (SELECT numAssoTypeLabel AS numReferralType,CA.numDivisionId,CACompanyInfo.* from CompanyAssociations CA ")
                    sqlFrom.Append(" JOIN DivisionMaster CADM on CADM.numDivisionId = CA.numAssociateFromDivisionID  ")
                    sqlFrom.Append(" JOIN CompanyInfo CACompanyInfo on CACompanyInfo.numCompanyID = CADM.numCompanyID  ")

                    sqlFrom.Append(" UNION ALL ")

                    sqlFrom.Append(" SELECT numReferralType,CA.numAssociateFromDivisionID,CACompanyInfo.* from ")
                    sqlFrom.Append(" CompanyAssociations CA JOIN DivisionMaster CADM on CADM.numDivisionId = CA.numDivisionId ")
                    sqlFrom.Append(" JOIN CompanyInfo CACompanyInfo on CACompanyInfo.numCompanyID = CADM.numCompanyID) CACompanyInfo ")
                    sqlFrom.Append(" on DivisionMaster.numDivisionId = CACompanyInfo.numDivisionId ")

                    sqlFrom.Append(" LEFT JOIN AddressDetails AD1 ON AD1.numRecordID=DivisionMaster.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD2 ON AD2.numRecordID=DivisionMaster.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 ")

                    sqlFrom.Append(" where DivisionMaster.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 62 Then
                    sqlFrom.Append(" communication Comm ")
                    sqlFrom.Append(" JOIN [Correspondence] C ON C.[numCommID]=Comm.[numCommId] ")
                    sqlFrom.Append(" join Item on Item.numItemCode= C.numOpenRecordID ")
                    sqlFrom.Append(" Left Join ItemGroups ItemGroup on ItemGroup.numItemGroupId = Item.numItemGroup ")
                    sqlFrom.Append(" join DivisionMaster  on DivisionMaster.numDivisionId = Comm.numDivisionId ")
                    sqlFrom.Append(" join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD1 ON AD1.numRecordID=DivisionMaster.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD2 ON AD2.numRecordID=DivisionMaster.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 ")
                    sqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")
                    sqlFrom.Append(" where C.tintCorrType=7 and C.numDomainID=@numDomainID  and DivisionMaster.numDomainId = @numDomainID and Item.numDomainId = @numDomainID ")

                ElseIf lstGroup.SelectedValue = 29 Then
                    sqlFrom.Append(" communication Comm ")
                    sqlFrom.Append(" left join AdditionalContactsInformation ACI on Comm.numContactId = ACI.numContactId ")
                    sqlFrom.Append(" left join DivisionMaster on DivisionMaster.numDivisionId = Comm.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")

                    sqlFrom.Append(" LEFT JOIN AddressDetails AD1 ON AD1.numRecordID=DivisionMaster.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD2 ON AD2.numRecordID=DivisionMaster.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 ")

                    sqlFrom.Append(" left join Cases on Comm.CaseId = Cases.numCaseId ")
                    sqlFrom.Append(" where Comm.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 30 Then
                    sqlFrom.Append(" Cases ")
                    sqlFrom.Append("join  communication Comm on Comm.CaseId = Cases.numCaseId ")
                    sqlFrom.Append(" where Cases.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 31 Then
                    sqlFrom.Append(" General_Journal_Header GJH  ")
                    sqlFrom.Append(" join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId ")
                    sqlFrom.Append(" join Chart_of_Accounts C on C.numAccountId=GJD.numChartAcntId ")
                    'sqlFrom.Append(" where numAcntType in(822,823,824,825,826,814,815)")
                    sqlFrom.Append(" where C.numdomainId=@numDomainID ")
                    sqlFrom.Append(" and (C.vcAccountCode ilike '010301%' or C.vcAccountCode ilike '010401%' ")
                    sqlFrom.Append(" or C.vcAccountCode ilike '010302%' or C.vcAccountCode ilike '010402%' ")
                    sqlFrom.Append(" or C.vcAccountCode ilike '01010105%' or C.vcAccountCode ilike '01020102%') ")

                    ' sqlFrom.Append(" group by numAcntType /*addGroupby*/")
                ElseIf lstGroup.SelectedValue = 32 Then
                    sqlFrom.Append(" CheckDetails")
                    sqlFrom.Append(" where CheckDetails.numDomainId=@numDomainID ")
                ElseIf lstGroup.SelectedValue = 33 Then
                    sqlFrom.Append(" EmailHistory")
                    sqlFrom.Append(" where EmailHistory.numDomainID=@numDomainID ")
                ElseIf lstGroup.SelectedValue = 34 Then
                    sqlFrom.Append(" OpportunityMaster OppMas ")
                    ' sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                    sqlFrom.Append(" Join OpportunityItems OppItems on OppMas.numOppId = OppItems.numOppId")
                    sqlFrom.Append(" Join Item  on OppItems.numItemcode = Item.numItemcode")
                    sqlFrom.Append(" Join Returns  on OppItems.numoppitemtCode = Returns.numOppItemCode")
                    sqlFrom.Append(" Left Join WareHouseItems on WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseItemID = OppItems.numWarehouseItmsID")
                    sqlFrom.Append(" Left Join Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID")

                    sqlFrom.Append(" left join DivisionMaster on OppMas.numDivisionId=DivisionMaster.numDivisionId ")
                    sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                    sqlFrom.Append(" left join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID and OppMas.numContactId=ACI.numContactId ")
                    sqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")

                    sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                ElseIf lstGroup.SelectedValue = 35 Then
                    sqlFrom.Append(" opportunitybizdocsdetails ")
                    ' sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                    sqlFrom.Append(" INNER JOIN OpportunityBizDocsPaymentDetails  ON opportunitybizdocsdetails.numBizDocsPaymentDetId = OpportunityBizDocsPaymentDetails.numBizDocsPaymentDetId")
                    sqlFrom.Append(" INNER JOIN DivisionMaster ON opportunitybizdocsdetails.numDivisionId = DivisionMaster.numDivisionID")
                    sqlFrom.Append(" INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId")

                    sqlFrom.Append(" LEFT JOIN AddressDetails AD1 ON AD1.numRecordID=DivisionMaster.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 ")
                    sqlFrom.Append(" LEFT JOIN AddressDetails AD2 ON AD2.numRecordID=DivisionMaster.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 ")

                    sqlFrom.Append(" where opportunitybizdocsdetails.numDomainId = @numDomainId  AND opportunitybizdocsdetails.bitAuthoritativeBizDocs = 1  AND OpportunityBizDocsPaymentDetails.bitIntegrated = 0  AND opportunitybizdocsdetails.numBizDocsId = 0 ")
                ElseIf lstGroup.SelectedValue >= 36 And lstGroup.SelectedValue <= 60 Then
                    If lstGroup.SelectedValue = 57 Or lstGroup.SelectedValue = 58 Then
                        sqlFrom.Append("  View_BizDocsSummary ")
                        Select Case lstGroup.SelectedValue
                            Case 57
                                sqlFrom.Append(" where  View_BizDocsSummary.tintOppType=2 and View_BizDocsSummary.numDomainId = @numDomainId ")
                            Case 58
                                sqlFrom.Append(" where  View_BizDocsSummary.tintOppType=1 and View_BizDocsSummary.numDomainId = @numDomainId ")

                        End Select
                    Else

                        sqlFrom.Append(" Chart_of_Accounts ")
                        sqlFrom.Append(" Inner Join AccountTypeDetail On Chart_of_Accounts.numParntAcntTypeId =AccountTypeDetail.numAccountTypeID ")
                        sqlFrom.Append(" inner join General_Journal_Details ON Chart_of_Accounts.numAccountId=General_Journal_Details.numChartAcntId ")
                        sqlFrom.Append(" inner join General_Journal_Header on General_Journal_Header.numJournal_Id=General_Journal_Details.numJournalId ")
                        sqlFrom.Append(" inner join Currency on Currency.numCurrencyId = case  coalesce(General_Journal_Details.numCurrencyId,0) when 0 then (select Domain.numCurrencyId from  Domain where Domain.numDomainID= @numDomainId ) else  General_Journal_Details.numCurrencyId end ")
                        sqlFrom.Append(" left join DivisionMaster on DivisionMaster.numDivisionId=General_Journal_Details.numCustomerId ")
                        sqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyId=DivisionMaster.numCompanyId ")
                        sqlFrom.Append(" left join OpportunityBizDocs on OpportunityBizDocs.numOppBizDocsId=General_Journal_Header.numOppBizDocsId ")
                        sqlFrom.Append(" left join ProjectsMaster on ProjectsMaster.numProId=General_Journal_Header.numProjectId ")
                        sqlFrom.Append(" where Chart_of_Accounts.numDomainID= @numDomainId ")
                    End If
                    Select Case lstGroup.SelectedValue
                        Case 36 'Bank Accounts
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01010101%'")
                        Case 37 'Cash-in-Hand
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01010102%'")
                        Case 38 'Investments
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01010103%'")
                        Case 39 'Stock
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01010104%'")
                        Case 40 'Account Receivable
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01010105%'")
                        Case 41 'Statutory Deposits
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01010201%'")
                        Case 42 'Property &amp; Equipment
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01010202%'")
                        Case 43 'Long Term Investments
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01010203%'")
                        Case 44 'Intangible Assets
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01010204%'")
                        Case 45 'Duties &amp; Taxes
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01020101%'")
                        Case 46 'Account Payable
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01020102%'")
                        Case 47 'Loans (Liability)
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01020103%'")
                        Case 48 'Suspense A/c
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01020104%'")
                        Case 49 'Direct Expenses
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '010401%'")
                        Case 50 'Indirect Expenses
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '010402%'")
                        Case 56 'Capital &amp; Reserves
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '01050101%'")
                        Case 59 'Direct Income
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '010301%'")
                        Case 60 'Indirect Income
                            sqlFrom.Append(" and Chart_of_Accounts.vcAccountCode ilike '010302%'")
                    End Select
                End If
                'Else
                '    If lstGroup.SelectedValue = 4 Then
                '        sqlFrom.Append(" AdditionalContactsInformation ACI join AdditionalContactsInformation ACI1 on ACI.numContactId = ACI1.numContactId where ACI.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 5 Then
                '        sqlFrom.Append("   DivisionMaster join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID join AdditionalContactsInformation ACI1 on ACI.numContactId = ACI1.numContactId where ACI.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 6 Then
                '        sqlFrom.Append("   DivisionMaster join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid join AdditionalContactsInformation ACI1 on DivisionMaster.numDivisionID=ACI1.numDivisionID   where DivisionMaster.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 7 Then
                '        sqlFrom.Append("   DivisionMaster join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid join AdditionalContactsInformation ACI1 on DivisionMaster.numDivisionID=ACI1.numDivisionID left join CompanyAssets on companyassets.numDivId  = DivisionMaster.numDivisionId ")
                '        sqlFrom.Append("left join Item on Item.numItemcode =CompanyAssets.numItemcode ")
                '        sqlFrom.Append("left join WareHouseItmsDTL on WareHouseItmsDTL.numWarehouseitmsdtlid = CompanyAssets.numWarehouseitmsdtlid")
                '        sqlFrom.Append(" where DivisionMaster.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 8 Then
                '        sqlFrom.Append(" ProjectsMaster ProMas left join DivisionMaster  on ProMas.numDivisionID=DivisionMaster.numDivisionID ")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on DivisionMaster.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" where ProMas.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 9 Then
                '        sqlFrom.Append(" ProjectsMaster ProMas left join DivisionMaster  on ProMas.numDivisionID=DivisionMaster.numDivisionID ")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on DivisionMaster.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" Left Join ProjectsExpense ProExp on ProMas.numProId = ProExp.numProId")
                '        sqlFrom.Append(" Left Join ProjectsTime ProTime on ProMas.numProId = ProTime.numProId")
                '        sqlFrom.Append(" where ProMas.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 10 Then
                '        sqlFrom.Append(" ProjectsMaster ProMas left join DivisionMaster  on ProMas.numDivisionID=DivisionMaster.numDivisionID ")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on DivisionMaster.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" Left join ProjectsStageDetails ProStgDtl on ProMas.numProId = ProStgDtl.numProId ")
                '        sqlFrom.Append(" Left join ProjectsSubStageDetails ProsubStgDtl on ProMas.numProId = ProsubStgDtl.numProId ")
                '        sqlFrom.Append(" where ProMas.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 11 Then
                '        sqlFrom.Append(" ProjectsMaster ProMas left join DivisionMaster  on ProMas.numDivisionID=DivisionMaster.numDivisionID ")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on DivisionMaster.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" Left Join ProjectsContacts ProCon on ProMas.numProid =ProCon.numProid ")
                '        sqlFrom.Append(" where ProMas.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 12 Then
                '        sqlFrom.Append(" Cases ")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on Cases.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" where Cases.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 13 Then
                '        sqlFrom.Append(" Cases left join DivisionMaster  on Cases.numDivisionID=DivisionMaster.numDivisionID")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on DivisionMaster.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" Left Join CaseExpense  on Cases.numCaseId = CaseExpense.numCaseId")
                '        sqlFrom.Append(" Left Join CaseTime  on Cases.numCaseId = CaseTime.numCaseId")
                '        sqlFrom.Append(" where Cases.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 14 Then
                '        sqlFrom.Append(" OpportunityMaster OppMas ")
                '        'sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on OppMas.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 15 Then
                '        sqlFrom.Append(" OpportunityMaster OppMas ")
                '        ' sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                '        sqlFrom.Append(" Left Join OpportunityTime OppTime on OppMas.numOppId = OppTime.numOppId")
                '        sqlFrom.Append(" Left Join OpportunityExpense OppExp on OppMas.numOppId = OppExp.numOppId")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on OppMas.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 16 Then
                '        sqlFrom.Append(" OpportunityMaster OppMas ")
                '        ' sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                '        sqlFrom.Append(" Left Join OpportunityStageDetails OppStgDtl on OppMas.numOppId = OppStgDtl.numOppId")
                '        sqlFrom.Append(" Left Join OpportunitySubStageDetails OppSubStgDtl on OppMas.numOppId = OppSubStgDtl.numOppId")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on OppMas.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 17 Then
                '        sqlFrom.Append(" OpportunityMaster OppMas ")
                '        ' sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                '        sqlFrom.Append(" Left Join OpportunityContact OppCon on OppMas.numOppId = OppCon.numOppId")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on OppMas.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 18 Then
                '        sqlFrom.Append(" OpportunityMaster OppMas ")
                '        ' sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                '        sqlFrom.Append(" Left Join OpportunityBizDocs OppBDoc on OppMas.numOppId = OppBDoc.numOppId")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on OppMas.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 19 Then
                '        sqlFrom.Append(" OpportunityMaster OppMas ")
                '        ' sqlFrom.Append(" Left Join OpportunityAddress OppAdr on OppMas.numOppId = OppAdr.numOppId")
                '        sqlFrom.Append(" Left Join OpportunityItems OppItems on OppMas.numOppId = OppItems.numOppId")
                '        sqlFrom.Append(" Left Join Item  on OppItems.numItemcode = Item.numItemcode")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on OppMas.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" where OppMas.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 24 Then
                '        sqlFrom.Append(" Forecast  ")
                '        sqlFrom.Append(" Left Join Item  on Forecast.numItemCode = Item.numItemCode")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on Forecast.numDomainID=ACI1.numDomainID")
                '        sqlFrom.Append(" where Forecast.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 25 Then
                '        sqlFrom.Append(" TrackingVisitorsHDR TracVisHDR")
                '        sqlFrom.Append(" Join TrackingVisitorsDTL TracVisDtl on TracVisDtl.numTracVisitorsHDRID = TracVisHDR.numTrackingID")
                '        sqlFrom.Append(" Left Join PageMasterWebAnlys PWebAnly on TracVisDtl.numPageID = PWebAnly.numPageID")
                '        sqlFrom.Append(" Join DivisionMaster on TracVisHDR.numDivisionID = DivisionMaster.numDivisionID")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on TracVisHDR.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append(" where DivisionMaster.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 26 Then
                '        sqlFrom.Append(" CampaignMaster CampMas")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on CampMas.numDomainID=ACI1.numDomainID")
                '        sqlFrom.Append(" where CampMas.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 27 Then
                '        sqlFrom.Append(" AdditionalContactsInformation ACI join AdditionalContactsInformation ACI1 on ACI.numContactId = ACI1.numContactId")
                '        sqlFrom.Append("join  communication Comm on ACI.numcontactId = Comm.numContactid")
                '        sqlFrom.Append(" where ACI.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 28 Then
                '        sqlFrom.Append("   DivisionMaster join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid join AdditionalContactsInformation ACI1 on DivisionMaster.numDivisionID=ACI1.numDivisionID ")
                '        sqlFrom.Append("join  communication Comm on DivisionMaster.numDivisionId = Comm.numDivisionId")
                '        sqlFrom.Append(" where DivisionMaster.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 29 Then
                '        sqlFrom.Append(" communication Comm ")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on Comm.numContactId = ACI1.numContactId")
                '        sqlFrom.Append(" where Comm.numDomainId = @numDomainID")
                '    ElseIf lstGroup.SelectedValue = 30 Then
                '        sqlFrom.Append(" Cases ")
                '        sqlFrom.Append(" join AdditionalContactsInformation ACI1 on Cases.numDivisionID=ACI1.numDivisionID")
                '        sqlFrom.Append("join  communication Comm on Comm.CaseId = Cases.numCaseId ")
                '        sqlFrom.Append(" where Cases.numDomainId = @numDomainID")
                '    End If
                'End If

                Dim sqlCustomFLD As New System.Text.StringBuilder

                If CFW_FLD_Values_Item.Count > 0 Then
                    Dim strCFW_FLD_Values_Item As String() = New String(CFW_FLD_Values_Item.Count - 1) {}
                    CFW_FLD_Values_Item.CopyTo(strCFW_FLD_Values_Item)

                    sqlCustomFLD.Append(" Left Join (SELECT * FROM (SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=@numDomainID AND t1.fld_type <> 'Link') p PIVOT (MAX([Fld_Value]) FOR fld_id IN ( " & String.Join(",", strCFW_FLD_Values_Item.Distinct()) & " ) ) AS pvt) ")
                    sqlCustomFLD.Append(" as CFW_FLD_Values_Item on CFW_FLD_Values_Item.RecId = Item.numItemCode ")
                End If

                If CFW_FLD_Values_Cont.Count > 0 Then
                    Dim strCFW_FLD_Values_Cont As String() = New String(CFW_FLD_Values_Cont.Count - 1) {}
                    CFW_FLD_Values_Cont.CopyTo(strCFW_FLD_Values_Cont)

                    sqlCustomFLD.Append(" Left Join (SELECT * FROM (SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 join CFW_Fld_Dtl t11  on  t1.Fld_id = t11.numFieldId JOIN CFW_FLD_Values_Cont AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 4 and t1.numDomainID=@numDomainID AND t1.fld_type <> 'Link') p PIVOT (MAX([Fld_Value]) FOR fld_id IN ( " & String.Join(",", strCFW_FLD_Values_Cont.Distinct()) & " ) ) AS pvt) ")
                    sqlCustomFLD.Append(" as CFW_FLD_Values_Cont on CFW_FLD_Values_Cont.RecId = ACI.numContactId ")
                End If

                If CFW_FLD_Values.Count > 0 Then
                    Dim strCFW_FLD_Values As String() = New String(CFW_FLD_Values.Count - 1) {}
                    CFW_FLD_Values.CopyTo(strCFW_FLD_Values)

                    sqlCustomFLD.Append(" Left Join (SELECT * FROM (SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 join CFW_Fld_Dtl t11  on  t1.Fld_id = t11.numFieldId JOIN CFW_FLD_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 1 and t1.numDomainID=@numDomainID AND t1.fld_type <> 'Link') p PIVOT (MAX([Fld_Value]) FOR fld_id IN ( " & String.Join(",", strCFW_FLD_Values.Distinct()) & " ) ) AS pvt) ")
                    sqlCustomFLD.Append(" as CFW_FLD_Values on CFW_FLD_Values.RecId = DivisionMaster.numDivisionId ")
                End If

                If CFW_FLD_Values_Case.Count > 0 Then
                    Dim strCFW_FLD_Values_Case As String() = New String(CFW_FLD_Values_Case.Count - 1) {}
                    CFW_FLD_Values_Case.CopyTo(strCFW_FLD_Values_Case)

                    sqlCustomFLD.Append(" Left Join (SELECT * FROM (SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_FLD_Values_Case AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 3 and t1.numDomainID=@numDomainID AND t1.fld_type <> 'Link') p PIVOT (MAX([Fld_Value]) FOR fld_id IN ( " & String.Join(",", strCFW_FLD_Values_Case.Distinct()) & " ) ) AS pvt) ")
                    sqlCustomFLD.Append(" as CFW_FLD_Values_Case on CFW_FLD_Values_Case.RecId = Cases.numCaseId ")
                End If

                If CFW_FLD_Values_Pro.Count > 0 Then
                    Dim strCFW_FLD_Values_Pro As String() = New String(CFW_FLD_Values_Pro.Count - 1) {}
                    CFW_FLD_Values_Pro.CopyTo(strCFW_FLD_Values_Pro)

                    sqlCustomFLD.Append(" Left Join (SELECT * FROM (SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 join CFW_Fld_Dtl t11  on  t1.Fld_id = t11.numFieldId  JOIN CFW_FLD_Values_Pro AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 11 and t1.numDomainID=@numDomainID AND t1.fld_type <> 'Link') p PIVOT (MAX([Fld_Value]) FOR fld_id IN ( " & String.Join(",", strCFW_FLD_Values_Pro.Distinct()) & " ) ) AS pvt) ")
                    sqlCustomFLD.Append(" as CFW_FLD_Values_Pro on CFW_FLD_Values_Pro.RecId = ProMas.numProId ")
                End If

                If CFW_Fld_Values_Opp.Count > 0 Then
                    Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values_Opp.Count - 1) {}
                    CFW_Fld_Values_Opp.CopyTo(strCFW_Fld_Values_Opp)

                    sqlCustomFLD.Append(" Left Join (SELECT * FROM (SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values_Opp AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 2 or t1.Grp_id = 6) and t1.numDomainID=@numDomainID AND t1.fld_type <> 'Link') p PIVOT (MAX([Fld_Value]) FOR fld_id IN ( " & String.Join(",", strCFW_Fld_Values_Opp.Distinct()) & " ) ) AS pvt) ")
                    sqlCustomFLD.Append(" as CFW_Fld_Values_Opp on CFW_Fld_Values_Opp.RecId = OppMas.numOppid ")
                End If

                If sqlCustomFLD.ToString.Trim().Length > 0 Then
                    If sqlFrom.ToString.IndexOf("where") > 0 Then
                        sqlFrom.Replace("where", sqlCustomFLD.ToString() & " where ")
                    Else
                        sqlFrom.Append(" " & sqlCustomFLD.ToString())
                    End If
                End If
                Sql.Append(sqlFrom.ToString)

                If SqlFilterStd.ToString <> "" Then Sql.Append(" " & SqlFilterStd.ToString & " ")
                '====================================================================================
                '====================================================================================
                'comparision Filter
                '====================================================================================
                '====================================================================================
                Dim ddlCompFlt1 As New DropDownList
                Dim ddlCompFlt2 As New DropDownList
                Dim sqlCompFlt As New System.Text.StringBuilder
                sqlCompFlt.Append(" ")
                ddlCompFlt1 = radOppTab.MultiPage.FindControl("ddlCompFlt1")
                ddlCompFlt2 = radOppTab.MultiPage.FindControl("ddlCompFlt2")
                If ddlCompFlt1.SelectedIndex <> 0 And ddlCompFlt2.SelectedIndex <> 0 Then
                    sqlCompFlt.Append(" " & ddlCompFlt1.SelectedValue.Split("~")(0))
                    sqlCompFlt.Append(" " & ddlCompFOpe.SelectedValue)
                    sqlCompFlt.Append(" " & ddlCompFlt2.SelectedValue.Split("~")(0))
                End If

                '====================================================================================

                If sqlCompFlt.ToString <> " " Then Sql.Append(" and (" & sqlCompFlt.ToString & " ) ")
                If txtchkAdvFilter.Text = 0 Then
                    For iFilter = 0 To CInt(noFilterRows.Text)
                        If (Not radOppTab.MultiPage.FindControl("ddlField" & iFilter) Is Nothing) And (Not radOppTab.MultiPage.FindControl("Operator" & iFilter) Is Nothing) And (Not radOppTab.MultiPage.FindControl("Value" & iFilter) Is Nothing) Then
                            Dim ddlField As New DropDownList
                            '                        Dim ddlField As New DropDownList
                            Dim ddlOperator As New HtmlSelect
                            Dim txtValue As New HtmlInputText
                            Dim txtValued As New HtmlInputText
                            ddlField = radOppTab.MultiPage.FindControl("ddlField" & iFilter)


                            If ddlField.SelectedValue <> "0" Then
                                Dim value As String = -1
                                ddlOperator = radOppTab.MultiPage.FindControl("Operator" & iFilter)
                                txtValue = radOppTab.MultiPage.FindControl("Value" & iFilter)
                                txtValued = radOppTab.MultiPage.FindControl("Valued" & iFilter)
                                Select Case ddlField.SelectedValue.Split("~")(2)
                                    Case "Drop Down List", "Check box", "Customdata", "Contact", "State", "UOM", "ItemGroup", "IncomeAccount", "AssetAcCOA", "COG", "Currency"
                                        If txtValued.Value = "" Then
                                            value = 0
                                        Else : value = txtValued.Value
                                        End If
                                    Case "Date Field", "Date Field U", "Date Field D"
                                        If txtValued.Value = "" Then
                                            value = ""
                                        Else : value = txtValued.Value
                                        End If
                                    Case Else
                                        value = txtValue.Value
                                End Select
                                Dim strOperator As String
                                Select Case ddlOperator.Value
                                    Case "Like" : strOperator = " ilike '%" & value & "%'"
                                    Case "Not Like" : strOperator = " Not ilike '%" & value & "%'"
                                    Case "se" : strOperator = " ilike '" & value & "%'"
                                    Case "Ew" : strOperator = " ilike '%" & value & "'"
                                    Case Else
                                        If ddlField.SelectedValue.Split("~")(2) = "Date Field" Or ddlField.SelectedValue.Split("~")(2) = "Date Field U" Or ddlField.SelectedValue.Split("~")(2) = "Date Field D" Then
                                            strOperator = ddlOperator.Value & " " & value & ""
                                        Else
                                            strOperator = ddlOperator.Value & " '" & value & "'"
                                        End If

                                End Select
                                Dim strN As String = ""
                                If iFilter = 0 Or sqlfilter.ToString() = " " Then
                                    strN = " "
                                Else : strN = Relation
                                End If

                                If ddlField.SelectedValue.Split("~")(4) = 1 Then
                                    Select Case ddlField.SelectedValue.Split("~")(1)
                                        Case 6
                                            sqlfilter.Append(strN & " ACI.numContactID in (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                        Case 7, 8
                                            sqlfilter.Append(strN & "DivisionMaster.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                        Case 10, 11, 12, 13, 14
                                            sqlfilter.Append(strN & "ProMas.numProID in (select RecId from CFW_FLD_Values where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                        Case 15, 16, 17
                                            sqlfilter.Append(strN & "Cases.numCaseId in (select RecId from CFW_FLD_Values_Case where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                        Case 18, 19, 20, 21, 22, 23, 24, 25, 26
                                            sqlfilter.Append(strN & "OppMas.numOppId in (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                        Case 9
                                            sqlfilter.Append(strN & "ProMas.numProId in (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                        Case 27
                                            sqlfilter.Append(strN & "Item.numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                    End Select
                                Else
                                    If ddlField.SelectedValue.Split("~")(2) = "Terriotary" Then
                                        sqlfilter.Append(strN & ddlField.SelectedValue.Split("~")(0) & " in ")
                                        sqlfilter.Append("(select numListItemID from ListDetails where numDomainid= @numDomainID and numListID=78 and vcData " & strOperator & ")")
                                    ElseIf ddlField.SelectedValue.Split("~")(2) = "Group" Then
                                        sqlfilter.Append(strN & ddlField.SelectedValue.Split("~")(0) & " in ")
                                        sqlfilter.Append("(select numGrpId from Groups where vcGrpName " & strOperator & ")")
                                    ElseIf ddlField.SelectedValue.Split("~")(2) = "Company" Then
                                        sqlfilter.Append(strN & ddlField.SelectedValue.Split("~")(0) & " in ")
                                        sqlfilter.Append("(select DivisionMaster.numDivisionId from DivisionMaster join companyinfo on companyinfo.numcompanyId = DivisionMaster.numcompanyId  where DivisionMaster.numDomainid= @numDomainID and companyinfo.vcCompanyName " & strOperator & ")")
                                    Else : sqlfilter.Append(strN & ddlField.SelectedValue.Split("~")(0) & strOperator)
                                    End If
                                End If
                            End If
                        End If
                    Next
                Else
                    '========================================================================================
                    '========================================================================================
                    'Advanced Filter
                    If txtAdvFilter.Text <> "" Then
                        Dim stradvfilter As String
                        Dim iRepalace As Integer
                        For iRepalace = 1 To CInt(noFilterRows.Text)
                            If iRepalace = 1 Then
                                stradvfilter = txtAdvFilter.Text.Replace(iRepalace, "|" & iRepalace & "|")
                            Else : stradvfilter = stradvfilter.Replace(iRepalace, "|" & iRepalace & "|")
                            End If
                        Next
                        For iFilter = 0 To CInt(noFilterRows.Text)
                            If (Not radOppTab.MultiPage.FindControl("ddlField" & iFilter) Is Nothing) And (Not radOppTab.MultiPage.FindControl("Operator" & iFilter) Is Nothing) And (Not radOppTab.MultiPage.FindControl("Value" & iFilter) Is Nothing) Then
                                Dim ddlField As New DropDownList
                                '                        Dim ddlField As New DropDownList
                                Dim ddlOperator As New HtmlSelect
                                Dim txtValue As New HtmlInputText
                                Dim txtValued As New HtmlInputText
                                Dim value As String = -1
                                ddlField = radOppTab.MultiPage.FindControl("ddlField" & iFilter)
                                Dim strAdvFilter1 As New System.Text.StringBuilder
                                strAdvFilter1.Append(" ")
                                If ddlField.SelectedValue <> "0" Then
                                    ddlOperator = radOppTab.MultiPage.FindControl("Operator" & iFilter)
                                    txtValue = radOppTab.MultiPage.FindControl("Value" & iFilter)
                                    txtValued = radOppTab.MultiPage.FindControl("Valued" & iFilter)
                                    Select Case ddlField.SelectedValue.Split("~")(2)
                                        Case "Drop Down List", "Check box", "Customdata", "Contact", "State"
                                            If txtValued.Value = "" Then
                                                value = 0
                                            Else : value = txtValued.Value
                                            End If
                                        Case "Date Field", "Date Field U", "Date Field D"
                                            If txtValued.Value = "" Then
                                                value = ""
                                            Else : value = txtValued.Value
                                            End If
                                        Case Else
                                            value = txtValue.Value
                                    End Select
                                    Dim strOperator As String
                                    Select Case ddlOperator.Value
                                        Case "Like" : strOperator = " ilike '%" & value & "%'"
                                        Case "Not Like" : strOperator = " Not ilike '%" & value & "%'"
                                        Case "se" : strOperator = " ilike '" & value & "%'"
                                        Case "Ew" : strOperator = " ilike '%" & value & "'"
                                        Case Else
                                            strOperator = ddlOperator.Value & " '" & value & "'"
                                    End Select

                                    If ddlField.SelectedValue.Split("~")(4) = 1 Then
                                        Select Case ddlField.SelectedValue.Split("~")(1)
                                            Case 6
                                                strAdvFilter1.Append(" ACI.numContactID in (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                            Case 7, 8
                                                strAdvFilter1.Append(" DivisionMaster.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                            Case 10, 11, 12, 13, 14
                                                strAdvFilter1.Append(" ProMas.numProId in (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                            Case 15, 16, 17
                                                strAdvFilter1.Append(" Cases.numCaseId in (select RecId from CFW_FLD_Values_Case where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                            Case 18, 19, 20, 21, 22, 23, 24, 25, 26
                                                strAdvFilter1.Append(" OppMas.numOppID in (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                            Case 9
                                                strAdvFilter1.Append(" ProMas.numProId in (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                            Case 27
                                                strAdvFilter1.Append(" Item.numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & ddlField.SelectedValue.Split("~")(0).Split("d")(1) & " and Fld_Value  " & strOperator & ")")
                                        End Select
                                    Else
                                        If ddlField.SelectedValue.Split("~")(2) = "Terriotary" Then
                                            strAdvFilter1.Append("" & ddlField.SelectedValue.Split("~")(0) & " in ")
                                            strAdvFilter1.Append("(select numListItemID from ListDetails where numDomainid= @numDomainID and numListID=78 and vcData " & strOperator & ")")
                                        ElseIf ddlField.SelectedValue.Split("~")(2) = "Group" Then
                                            strAdvFilter1.Append("" & ddlField.SelectedValue.Split("~")(0) & " in ")
                                            strAdvFilter1.Append("(select numGrpId from Groups where vcGrpName " & strOperator & ")")
                                        ElseIf ddlField.SelectedValue.Split("~")(2) = "Company" Then
                                            strAdvFilter1.Append("" & ddlField.SelectedValue.Split("~")(0) & " in ")
                                            strAdvFilter1.Append("(select DivisionMaster.numDivisionId from DivisionMaster join companyinfo on companyinfo.numcompanyId = DivisionMaster.numcompanyId  where DivisionMaster.numDomainid=  @numDomainID   and companyinfo.vcCompanyName " & strOperator & ")")
                                        Else : strAdvFilter1.Append(ddlField.SelectedValue.Split("~")(0) & strOperator)
                                        End If
                                    End If
                                End If
                                If strAdvFilter1.ToString <> " " Then
                                    If iFilter = 0 Then
                                        txtAdvFilterStr.Text = stradvfilter.Replace("|" & iFilter + 1 & "|", strAdvFilter1.ToString)
                                    Else : txtAdvFilterStr.Text = txtAdvFilterStr.Text.Replace("|" & iFilter + 1 & "|", strAdvFilter1.ToString)
                                    End If
                                End If
                            End If
                        Next
                        If txtAdvFilterStr.Text.Length > 5 Then sqlfilter.Append(txtAdvFilterStr.Text)
                    End If
                End If

                If sqlfilter.ToString <> " " Then Sql.Append(" and (" & sqlfilter.ToString & ")")
                '========================================================================================================================
                '========================================================================================================================
                'MySelf Team selected Territory
                '========================================================================================================================
                '========================================================================================================================
                Dim sqlMTT As New System.Text.StringBuilder
                sqlMTT.Append(" ")
                If radMyself.Checked = True And CType(radOppTab.MultiPage.FindControl("ddlMySelf"), DropDownList).SelectedValue <> "0" Then
                    Dim ddlMySelf As New DropDownList
                    ddlMySelf = CType(radOppTab.MultiPage.FindControl("ddlMySelf"), DropDownList)
                    If ddlMySelf.SelectedValue.Split("~")(1) = "6" Then
                        sqlMTT.Append(" and " & ddlMySelf.SelectedValue.Split("~")(0) & " = @numUserCntId")
                    Else : sqlMTT.Append(" and " & ddlMySelf.SelectedValue.Split("~")(0) & " = @numUserCntId ")
                    End If
                End If
                If radTeamSelected.Checked = True And CType(radOppTab.MultiPage.FindControl("ddlTeam"), DropDownList).SelectedValue <> "0" Then
                    Dim ddlTeam As New DropDownList
                    ddlTeam = CType(radOppTab.MultiPage.FindControl("ddlTeam"), DropDownList)
                    sqlMTT.Append(" and " & ddlTeam.SelectedValue.Split("~")(0) & " in ")
                    sqlMTT.Append("(select numContactID from AdditionalContactsInformation where  numDomainID=@numDomainID and numTeam in ")
                    sqlMTT.Append(" (select distinct numTeam from UserTeams where numusercntid=@numUserCntID))")
                End If
                If radTerriotary.Checked = True And CType(radOppTab.MultiPage.FindControl("ddlTerr"), DropDownList).SelectedValue <> "0" Then
                    Dim ddlTerr As New DropDownList
                    ddlTerr = CType(radOppTab.MultiPage.FindControl("ddlTerr"), DropDownList)
                    sqlMTT.Append(" and " & ddlTerr.SelectedValue.Split("~")(0) & " in ")
                    sqlMTT.Append("(select distinct numTerritoryID from UserTerritory where numusercntid=@numUserCntID)")
                End If

                '========================================================================================================================
                '========================================================================================================================
                'building Summation Rows
                '========================================================================================================================
                '========================================================================================================================
                Dim iSummation As Integer = 0
                Dim sqlSummation As New System.Text.StringBuilder
                Dim sqlSummationColumn As New System.Text.StringBuilder
                sqlSummation.Append(" ")
                sqlSummationColumn.Append(" ")

                Dim dtReportFields As DataTable
                dtReportFields = Session("ReportFields")
                Dim i As Integer = 0

                For Each row As DataRow In dtReportFields.Rows
                    If row("boolSummationpossible") = True Or row("boolSummationpossible") = 1 Then
                        If (Not radOppTab.MultiPage.FindControl("Sum" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID")) Is Nothing) _
                            And (Not radOppTab.MultiPage.FindControl("Avg" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID")) Is Nothing) _
                           And (Not radOppTab.MultiPage.FindControl("Max" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID")) Is Nothing) _
                           And Not radOppTab.MultiPage.FindControl("Min" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID")) Is Nothing Then

                            Dim chksum As New CheckBox
                            Dim chkAvg As New CheckBox
                            Dim chkMax As New CheckBox
                            Dim chkMin As New CheckBox
                            chksum = radOppTab.MultiPage.FindControl("Sum" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID"))
                            chkAvg = radOppTab.MultiPage.FindControl("Avg" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID"))
                            chkMax = radOppTab.MultiPage.FindControl("Max" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID"))
                            chkMin = radOppTab.MultiPage.FindControl("Min" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID"))

                            Dim colName As String = ""

                            If ddlNoRows.SelectedValue <> "0" And chkLinerGrid.Checked = True Then
                                colName = "[" & row.Item("vcDbFieldName") & "]"
                                If sqlSummationColumn.ToString = " " Then
                                    sqlSummationColumn.Append(" " & row.Item("vcDbFieldName") & " as '" & row.Item("vcDbFieldName") & "'")
                                Else : sqlSummationColumn.Append(" ," & row.Item("vcDbFieldName") & " as '" & row.Item("vcDbFieldName") & "'")
                                End If
                            Else
                                colName = row.Item("vcDbFieldName")
                            End If

                            If i = 0 Or sqlSummation.ToString = " " Then
                                If chksum.Checked = True Then
                                    If sqlSummation.ToString = " " Then
                                        sqlSummation.Append(" Sum(" & colName & ") as '" & "Sum" & row.Item("vcScrFieldName") & "'")
                                    Else : sqlSummation.Append(", Sum(" & colName & ") as '" & "Sum" & row.Item("vcScrFieldName") & "'")
                                    End If
                                End If
                                If chkAvg.Checked = True Then
                                    If sqlSummation.ToString = " " Then
                                        sqlSummation.Append(" Avg(" & colName & ") as '" & "Avg" & row.Item("vcScrFieldName") & "'")
                                    Else : sqlSummation.Append(", Avg(" & colName & ") as '" & "Avg" & row.Item("vcScrFieldName") & "'")
                                    End If
                                End If
                                If chkMax.Checked = True Then
                                    If sqlSummation.ToString = " " Then
                                        sqlSummation.Append(" Max(" & colName & ") as '" & "Max" & row.Item("vcScrFieldName") & "'")
                                    Else : sqlSummation.Append(", Max(" & colName & ") as '" & "Max" & row.Item("vcScrFieldName") & "'")
                                    End If
                                End If
                                If chkMin.Checked = True Then
                                    If sqlSummation.ToString = " " Then
                                        sqlSummation.Append(" Min(" & colName & ") as '" & "Min" & row.Item("vcScrFieldName") & "'")
                                    Else : sqlSummation.Append(" ,Min(" & colName & ") as '" & "Min" & row.Item("vcScrFieldName") & "'")
                                    End If
                                End If
                            Else
                                If chksum.Checked = True Then sqlSummation.Append(", Sum(" & colName & ") as '" & "Sum" & row.Item("vcScrFieldName") & "'")
                                If chkAvg.Checked = True Then sqlSummation.Append(",Avg(" & colName & ") as '" & "Avg" & row.Item("vcScrFieldName") & "'")
                                If chkMax.Checked = True Then sqlSummation.Append(",Max(" & colName & ") as '" & "Max" & row.Item("vcScrFieldName") & "'")
                                If chkMin.Checked = True Then sqlSummation.Append(",Min(" & colName & ") as '" & "Min" & row.Item("vcScrFieldName") & "'")
                            End If

                        End If
                        i = i + 1
                    End If
                Next
                If CType(radOppTab.MultiPage.FindControl("chkRecCount"), CheckBox).Checked = True Or chkSortRecCount.Checked = True Then
                    If sqlSummation.ToString <> " " Then
                        sqlSummation.Append(" ,Count(*) as RecCount")
                    Else : sqlSummation.Append(" Count(*) as RecCount ")
                    End If
                End If
                If sqlMTT.ToString <> " " Then Sql.Append(sqlMTT)
                If chkLinerGrid.Checked = True Then
                    Dim ddlordflt As New DropDownList
                    ddlordflt = radOppTab.MultiPage.FindControl("ddlOrderflt")
                    If ddlordflt.SelectedValue <> "0" Then
                        Sql.Append(" Order By ")
                        Sql.Append("'" & ddlordflt.SelectedItem.Text & "'")
                        Sql.Append(" " & ddlOrderFilter.SelectedValue & " ")
                    End If
                End If
                '================================================================================================================================
                '================================================================================================================================
                'Query to get the summation row
                '================================================================================================================================
                '================================================================================================================================
                'If sqlSummation.ToString <> " " And chkLinerGrid.Checked = True Then
                If sqlSummation.ToString <> " " And chkLinerGrid.Checked = True Then
                    Sql.Append(" /*sum*/ ")
                    Sql.Append("    select ")

                    Sql.Append(" " & sqlSummation.ToString)
                    Sql.Append(" from ")

                    If ddlNoRows.SelectedValue = "0" Or sqlSummationColumn.ToString.Trim.Length = 0 Then
                        Sql.Append(" " & sqlFrom.ToString)
                        Sql.Append(" " & SqlFilterStd.ToString)
                        If sqlCompFlt.ToString <> " " Then Sql.Append(" and (" & sqlCompFlt.ToString & " ) ")
                        If sqlfilter.ToString <> " " Then Sql.Append(" and (" & sqlfilter.ToString & ")")
                        If sqlMTT.ToString <> " " Then Sql.Append(" " & sqlMTT.ToString)
                    Else
                        Sql.Append("(Select Top " & ddlNoRows.SelectedValue & " " & sqlSummationColumn.ToString & " from ")

                        Sql.Append(" " & sqlFrom.ToString)
                        Sql.Append(" " & SqlFilterStd.ToString)
                        If sqlCompFlt.ToString <> " " Then Sql.Append(" and (" & sqlCompFlt.ToString & " ) ")
                        If sqlfilter.ToString <> " " Then Sql.Append(" and (" & sqlfilter.ToString & ")")
                        If sqlMTT.ToString <> " " Then Sql.Append(" " & sqlMTT.ToString)

                        Dim ddlordflt As New DropDownList
                        ddlordflt = radOppTab.MultiPage.FindControl("ddlOrderflt")
                        If ddlordflt.SelectedValue <> "0" Then
                            Sql.Append(" Order By ")
                            Sql.Append("'" & ddlordflt.SelectedValue.Split("~")(0) & "'")
                            Sql.Append(" " & ddlOrderFilter.SelectedValue & " ")
                        End If

                        Sql.Append(" ) as temp ")
                    End If

                End If

                'If lstGroup.SelectedValue = 61 Then
                '    Sql.Append(" union " & Sql.ToString.Replace("DivisionMaster.numDivisionId = CompanyAssociations.numAssociateFromDivisionID", "DivisionMaster.numDivisionId = CompanyAssociations.numDivisionID").Replace("CADivisionMaster.numDivisionId = CompanyAssociations.numDivisionID", "CADivisionMaster.numDivisionId = CompanyAssociations.numAssociateFromDivisionID"))
                'End If

                txtSql.Text = Sql.ToString
                If chkSummationGrid.Checked = True Then
                    Textbox1.Text = IIf(ddlNoRows.SelectedValue = "0", "select ", "Select Top " & ddlNoRows.SelectedValue & " ") & SqlSummGrid1.ToString & IIf(sqlSummation.ToString = " ", "", "," & sqlSummation.ToString) & " from " _
                        & sqlFrom.ToString & SqlFilterStd.ToString
                    If sqlfilter.ToString <> " " Then Textbox1.Text = Textbox1.Text & " " & "and ( " & sqlfilter.ToString & " ) "
                    If sqlCompFlt.ToString <> " " Then Textbox1.Text = Textbox1.Text & " and (" & sqlCompFlt.ToString & " ) "
                    If sqlMTT.ToString <> " " Then Textbox1.Text = Textbox1.Text & " " & sqlMTT.ToString
                    Textbox1.Text = Textbox1.Text & SqlSummGridFilter.ToString & SqlSummGrid2.ToString & SqlSummGrid3.ToString
                    If sqlSummation.ToString <> " " And ddlNoRows.SelectedValue = "0" Then
                        Textbox1.Text = Textbox1.Text & " /*sum*/ "
                        Textbox1.Text = Textbox1.Text & "    select "
                        Textbox1.Text = Textbox1.Text & " " & sqlSummation.ToString
                        Textbox1.Text = Textbox1.Text & " from "
                        Textbox1.Text = Textbox1.Text & " " & sqlFrom.ToString & SqlFilterStd.ToString
                        If sqlfilter.ToString <> " " Then Textbox1.Text = Textbox1.Text & " " & " and (" & sqlfilter.ToString & ")"
                        If sqlCompFlt.ToString <> " " Then Textbox1.Text = Textbox1.Text & " and (" & sqlCompFlt.ToString & " ) "
                        If sqlMTT.ToString <> " " Then Textbox1.Text = Textbox1.Text & " " & sqlMTT.ToString
                        Textbox1.Text = Textbox1.Text & " " & SqlSummGridFilter.ToString
                    End If
                End If

                'If lstGroup.SelectedValue = 61 Then
                '    Textbox1.Text = Textbox1.Text.Replace(SqlSummGrid3.ToString, "") & " union " & Textbox1.Text.Replace("DivisionMaster.numDivisionId = CompanyAssociations.numAssociateFromDivisionID", "DivisionMaster.numDivisionId = CompanyAssociations.numDivisionID").Replace("CADivisionMaster.numDivisionId = CompanyAssociations.numDivisionID", "CADivisionMaster.numDivisionId = CompanyAssociations.numAssociateFromDivisionID")
                'End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindDataGrid()
            Try
                Dim Sql As String
                If chkLinerGrid.Checked = True Then
                    Sql = txtSql.Text
                    If Sql <> "" Then
                        Dim objCustomReports As New CustomReports
                        Dim bField As BoundField
                        objCustomReports.DynamicQuery = Sql
                        objCustomReports.UserCntID = Session("UserContactId")
                        objCustomReports.DomainID = Session("DomainId")
                        objCustomReports.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        Dim ds As DataSet
                        ds = objCustomReports.ExecuteDynamicSql()
                        If ds.Tables.Count = 2 Then
                            dgReport.ShowFooter = True
                        Else : dgReport.ShowFooter = False
                        End If
                        dgReport.AutoGenerateColumns = False
                        Dim icol As Integer = 0
                        For Each column As DataColumn In ds.Tables(0).Columns

                            If Not column.ColumnName.Contains("##") Then
                                Dim bc1 As New BoundColumn()
                                bc1.DataField = column.ColumnName
                                bc1.HeaderText = column.ColumnName
                                If column.DataType.Name = "Decimal" Then bc1.DataFormatString = "{0:#,###0.00}"
                                If ds.Tables.Count = 2 Then
                                    Dim tbF As DataTable = ds.Tables(1)
                                    If tbF.Rows.Count > 0 Then
                                        If icol = 0 Then If tbF.Columns.Contains("RecCount") Then bc1.FooterText = bc1.FooterText & "Total Records =" & tbF.Rows(0).Item("RecCount") & "&nbsp;"
                                        If tbF.Columns.Contains("Sum" & column.ColumnName) Then bc1.FooterText = bc1.FooterText & " Total =" & Format(IIf(IsDBNull(tbF.Rows(0).Item("Sum" & column.ColumnName)), 0, tbF.Rows(0).Item("Sum" & column.ColumnName)), "f")
                                        If tbF.Columns.Contains("Avg" & column.ColumnName) Then bc1.FooterText = bc1.FooterText & " Avg=" & Format(IIf(IsDBNull(tbF.Rows(0).Item("Avg" & column.ColumnName)), 0, tbF.Rows(0).Item("Avg" & column.ColumnName)), "f")
                                        If tbF.Columns.Contains("Max" & column.ColumnName) Then bc1.FooterText = bc1.FooterText & " Max=" & Format(IIf(IsDBNull(tbF.Rows(0).Item("Max" & column.ColumnName)), 0, tbF.Rows(0).Item("Max" & column.ColumnName)), "f")
                                        If tbF.Columns.Contains("Min" & column.ColumnName) Then bc1.FooterText = bc1.FooterText & " Min=" & Format(IIf(IsDBNull(tbF.Rows(0).Item("Min" & column.ColumnName)), 0, tbF.Rows(0).Item("Min" & column.ColumnName)), "f")
                                    End If
                                End If
                                dgReport.Columns.Add(bc1)
                                icol = icol + 1
                            End If
                        Next
                        dgReport.DataSource = ds.Tables(0)
                        dgReport.DataBind()
                    End If
                Else
                    Sql = Textbox1.Text
                    If Sql <> "" Then
                        Dim objCustomReports As New CustomReports
                        Dim bField As BoundField
                        objCustomReports.DynamicQuery = Sql
                        objCustomReports.UserCntID = Session("UserContactId")
                        objCustomReports.DomainID = Session("DomainId")
                        objCustomReports.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        Dim ds As DataSet
                        ds = objCustomReports.ExecuteDynamicSql()
                        Dim dtGroupTable As DataTable
                        Dim dtFooter As DataTable
                        dtGroupTable = ds.Tables(0)
                        If ds.Tables.Count = 2 Then dtFooter = ds.Tables(1)

                        Dim TRH As New TableRow
                        Dim TR As TableRow
                        Dim TD As TableCell
                        Dim strfields() As String = txtOrderListText.Text.Split("|")
                        Dim i As Integer
                        For i = 0 To strfields.Length - 1
                            TD = New TableCell
                            'TD.Text = "<font color=white>" & strfields(i) & "</font>"
                            TD.Text = strfields(i)
                            TRH.CssClass = "hs"
                            TRH.Cells.Add(TD)
                        Next
                        tblSummaryGrid.Rows.Add(TRH)
                        Dim ddlGrp1List As New DropDownList
                        ddlGrp1List = radOppTab.MultiPage.FindControl("ddlGrp1List")
                        If ddlGrp1List.SelectedValue.Split("~")(3) <> "Date Field" And ddlGrp1List.SelectedValue.Split("~")(3) <> "Date Field U" And ddlGrp1List.SelectedValue.Split("~")(3) <> "Date Field D" Then
                            ddlGrp1Filter.SelectedIndex = 0
                        End If
                        Dim dvSumm As DataView
                        dvSumm = dtGroupTable.DefaultView
                        'Select Case ddlGrp1Filter.SelectedValue
                        '    Case 8, 9, 10, 12
                        '        dvSumm.RowFilter = "year = " & DatePart(DateInterval.Year, Now)
                        '    Case 11
                        '        dvSumm.RowFilter = "year = " & DatePart(DateInterval.Year, Now) & "Or year = " & DatePart(DateInterval.Year, Now) - 1
                        '    Case Else
                        'End Select
                        ' For Each row As DataRow In dtGroupTable.Rows
                        For Each row As DataRowView In dvSumm
                            TR = New TableRow
                            TD = New TableCell
                            Select Case ddlGrp1Filter.SelectedValue
                                'Case 1


                                '    Dim startday As Date
                                '    Dim Endday As Date = New Date(row.Item("year"), 1, 1)
                                '    'startday.Add()
                                '    Dim days As Integer = row.Item(ddlGrp1List.SelectedItem.Text) * 7 - 1
                                '    Endday = Endday.AddDays(days)
                                '    'Dim Endday As Date
                                '    'startday = DateAdd(DateInterval.WeekOfYear, row.Item(ddlGrp1List.SelectedItem.Text), Year(Now))
                                '    startday = DateAdd(DateInterval.Day, -6, Endday)
                                '    TD.Text = FormattedDateFromDate(startday, Session("DateFormat")) & " - " & FormattedDateFromDate(Endday, Session("DateFormat"))
                                'Case 7
                                '    If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                '        TD.Text = MonthName(row.Item(ddlGrp1List.SelectedItem.Text)) & "-" & row.Item("year")
                                '    Else
                                '        TD.Text = "-"
                                '    End If

                                Case 3
                                    If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                        'Select Case row.Item(ddlGrp1List.SelectedItem.Text)
                                        '    Case "1"
                                        '        TD.Text = "I Quarter" & "-" & row.Item("year")
                                        '    Case "2"
                                        '        TD.Text = "II Quarter" & "-" & row.Item("year")
                                        '    Case "3"
                                        '        TD.Text = "III Quarter" & "-" & row.Item("year")
                                        '    Case "4"
                                        '        TD.Text = "IV Quarter" & "-" & row.Item("year")
                                        'End Select
                                        TD.Text = row.Item(ddlGrp1List.SelectedItem.Text)
                                    Else : TD.Text = "-"
                                    End If
                                Case 5
                                    If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                        If Not IsDBNull(row.Item("Fstart")) Then
                                            'Select Case row.Item(ddlGrp1List.SelectedItem.Text)
                                            '    Case "1"
                                            '        TD.Text = "I Quarter" & "-" & row.Item("Fstart") & " to " & FormattedDateFromDate(DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 3, row.Item("Fstart"))), Session("DateFormat"))
                                            '    Case "2"
                                            '        TD.Text = "II Quarter" & "-" & row.Item("Fstart") & " to " & FormattedDateFromDate(DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 3, row.Item("Fstart"))), Session("DateFormat"))
                                            '    Case "3"
                                            '        TD.Text = "III Quarter" & "-" & row.Item("Fstart") & " to " & FormattedDateFromDate(DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 3, row.Item("Fstart"))), Session("DateFormat"))
                                            '    Case "4"
                                            '        TD.Text = "IV Quarter" & "-" & row.Item("Fstart") & " to " & FormattedDateFromDate(DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 3, row.Item("Fstart"))), Session("DateFormat"))
                                            'End Select
                                            TD.Text = row.Item(ddlGrp1List.SelectedItem.Text)
                                            '& "-" & row.Item("Fstart") & " to " & FormattedDateFromDate(DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 3, row.Item("Fstart"))), Session("DateFormat"))
                                        End If
                                    Else : TD.Text = "-"
                                    End If
                                Case 9
                                    Dim week As Integer
                                    week = getWeeknumber.Weeknumbers(Now)
                                    'week = datepart(weNow
                                    If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                        If row.Item(ddlGrp1List.SelectedItem.Text) = week And row.Item("Year") = Now().Year Then
                                            TD.Text = "This week"
                                        ElseIf row.Item("Year") = Now().Year Then
                                            TD.Text = "Last week"
                                        End If
                                    End If
                                    'TD.Text = IIf(IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)), "-", row.Item(ddlGrp1List.SelectedItem.Text))
                                Case 10
                                    Dim month1 As Integer
                                    month1 = DatePart(DateInterval.Month, Now)
                                    If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                        If row.Item(ddlGrp1List.SelectedItem.Text) = month1 And row.Item("Year") = Now().Year Then
                                            TD.Text = "This Month"
                                        ElseIf row.Item("Year") = Now().Year Then
                                            TD.Text = "Last Month"
                                        End If
                                    End If
                                    'TD.Text = IIf(IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)), "-", row.Item(ddlGrp1List.SelectedItem.Text))
                                Case 11
                                    Dim year1 As Integer
                                    year1 = DatePart(DateInterval.Year, Now)
                                    If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                        If row.Item(ddlGrp1List.SelectedItem.Text) = year1 Then
                                            TD.Text = "This Year"
                                        ElseIf row.Item(ddlGrp1List.SelectedItem.Text) = year1 - 1 Then
                                            TD.Text = "Last Year"
                                        End If
                                    End If
                                Case 12
                                    Dim quarter As Integer
                                    quarter = DatePart(DateInterval.Quarter, Now)
                                    If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                        If row.Item(ddlGrp1List.SelectedItem.Text) = quarter Then
                                            TD.Text = "This Quarter"
                                        Else : TD.Text = "Last Quarter"
                                        End If
                                    End If
                                Case Else
                                    TD.Text = IIf(IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)), "-", row.Item(ddlGrp1List.SelectedItem.Text))
                            End Select

                            If CType(radOppTab.MultiPage.FindControl("chkRecCount"), CheckBox).Checked = True Then
                                TD.Text = TD.Text & " ( " & row.Item("RecCount") & " )"
                            End If
                            Dim ifields As Integer = 0
                            Dim str As String = ""
                            If dtGroupTable.Columns.Contains("sum" & strfields(0)) Then
                                str = str & " Total = " & Format(IIf(IsDBNull(row.Item("sum" & strfields(0))), 0, row.Item("sum" & strfields(0))), "f")
                            End If
                            If dtGroupTable.Columns.Contains("Avg" & strfields(0)) Then
                                str = str & "  Avg = " & Format(IIf(IsDBNull(row.Item("Avg" & strfields(0))), 0, row.Item("Avg" & strfields(0))), "f")
                            End If
                            If dtGroupTable.Columns.Contains("Max" & strfields(0)) Then
                                str = str & " Max = " & Format(IIf(IsDBNull(row.Item("Max" & strfields(0))), 0, row.Item("Max" & strfields(0))), "f")
                            End If
                            If dtGroupTable.Columns.Contains("Min" & strfields(0)) Then
                                str = str & " Min = " & Format(IIf(IsDBNull(row.Item("Min" & strfields(0))), 0, row.Item("Min" & strfields(0))), "f")
                            End If
                            TD.Text = TD.Text & str
                            TR.Cells.Add(TD)
                            'TD.ColumnSpan = lstReportColumnsOrder.Items.Count
                            For ifields = 1 To strfields.Length - 1
                                TD = New TableCell
                                Dim str1 As String = ""
                                If dtGroupTable.Columns.Contains("sum" & strfields(ifields)) Then
                                    str1 = str1 & " Total = " & Format(IIf(IsDBNull(row.Item("sum" & strfields(ifields))), 0, row.Item("sum" & strfields(ifields))), "f")
                                End If
                                If dtGroupTable.Columns.Contains("Avg" & strfields(ifields)) Then
                                    str1 = str1 & " Avg = " & Format(IIf(IsDBNull(row.Item("Avg" & strfields(ifields))), 0, row.Item("Avg" & strfields(ifields))), "f")
                                End If
                                If dtGroupTable.Columns.Contains("Max" & strfields(ifields)) Then
                                    str1 = str1 & " Max = " & Format(IIf(IsDBNull(row.Item("Max" & strfields(ifields))), 0, row.Item("Max" & strfields(ifields))), "f")
                                End If
                                If dtGroupTable.Columns.Contains("Min" & strfields(ifields)) Then
                                    str1 = str1 & " Min = " & Format(IIf(IsDBNull(row.Item("Min" & strfields(ifields))), 0, row.Item("Min" & strfields(ifields))), "f")
                                End If
                                TD.Text = str1
                                TR.Cells.Add(TD)
                            Next

                            TR.CssClass = "reportG"
                            tblSummaryGrid.Rows.Add(TR)
                            If chkDrllDownRpt.Checked = True Then


                                Dim SqlSummGrid1 As New System.Text.StringBuilder

                                If ddlGrp1List.SelectedValue.Split("~")(4) = 1 Then
                                    Dim strCustFunVal As String

                                    Dim tblAlias, CFWColumn, strCustFieldID As String

                                    Select Case ddlGrp1List.SelectedValue.Split("~")(2)
                                        Case 6
                                            strCustFunVal = ",4,ACI.numContactId"
                                            tblAlias = "CFW_FLD_Values_Cont"
                                        Case 7, 8
                                            strCustFunVal = ",1,DivisionMaster.numDivisionId"
                                            tblAlias = "CFW_FLD_Values"
                                        Case 10, 11, 12, 13, 14
                                            strCustFunVal = ",1,ProMas.numProId"
                                            tblAlias = "CFW_FLD_Values"
                                        Case 15, 16, 17
                                            strCustFunVal = ",3,Cases.numCaseId"
                                            tblAlias = "CFW_FLD_Values_Case"
                                        Case 18, 19, 20, 21, 22, 23, 25, 26
                                            strCustFunVal = ",6,OppMas.numOppid"
                                            tblAlias = "CFW_Fld_Values_Opp"
                                        Case 9
                                            strCustFunVal = ",11,ProMas.numProId"
                                            tblAlias = "CFW_FLD_Values_Pro"
                                        Case 27
                                            strCustFunVal = ",5,Item.numItemCode"
                                            tblAlias = "CFW_FLD_Values_Item"
                                        Case Else
                                            strCustFunVal = ",1,DivisionMaster.numDivisionId"
                                            tblAlias = "CFW_FLD_Values"
                                    End Select

                                    strCustFieldID = ddlGrp1List.SelectedValue.Split("~")(1).Split("d")(1)
                                    CFWColumn = tblAlias & ".[" & strCustFieldID & "]"

                                    If ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field U" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field D" Then
                                        Select Case ddlGrp1Filter.SelectedValue
                                            Case 0
                                                SqlSummGrid1.Append("  and  FormatedDateFromDate(" & CFWColumn & ",@numDomainId)")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 1
                                                SqlSummGrid1.Append(" and   FormatedDateFromDate(get_week_start(" & CFWColumn & "),@numDomainID)+' - '+ FormatedDateFromDate(get_week_end( " & CFWColumn & "),@numDomainID)")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" ilike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 2
                                                SqlSummGrid1.Append(" and   DATENAME(month," & CFWColumn & ")+'-'+convert(varchar(4),year(" & CFWColumn & "))")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" ilike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 3
                                                SqlSummGrid1.Append(" and   fn_getCustomDataValue(DATEPART(q," & CFWColumn & "),13)+'-'+convert(varchar(4),year(" & CFWColumn & "))")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 4
                                                SqlSummGrid1.Append("  and  DATEPART(year," & CFWColumn & ")")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 5
                                                SqlSummGrid1.Append(" and  fn_getCustomDataValue(GetFiscalQuarter(" & CFWColumn & ",@numDomainId),13)")
                                                SqlSummGrid1.Append(" +' '+FormatedDateFromDate(GetFQDate(" & CFWColumn & ", GetFiscalQuarter(" & CFWColumn & ",@numDomainId),'S',@numDomainId),@numDomainId)")
                                                SqlSummGrid1.Append("+'- '+FormatedDateFromDate(GetFQDate(" & CFWColumn & ", GetFiscalQuarter(" & CFWColumn & ",@numDomainId),'E',@numDomainId),@numDomainId) ")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append(" and FormatedDateFromDate(GetFiscalStartDate(getFiscalYear(" & CFWColumn & ",@numDomainId),@numDomainId),@numDomainId)")
                                                If Not IsDBNull(row.Item("Fstart")) Then
                                                    SqlSummGrid1.Append(" = '" & row.Item("Fstart") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 6
                                                SqlSummGrid1.Append(" and  FormatedDateFromDate(GetFiscalStartDate(GetFiscalyear(" & CFWColumn & ",@numDomainId),@numDomainId),@numDomainId)")
                                                SqlSummGrid1.Append(" +'-'+ FormatedDateFromDate(dateadd(year,1,GetFiscalStartDate(GetFiscalyear(" & CFWColumn & ",@numDomainId),@numDomainId)),@numDomainId)")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 7
                                                SqlSummGrid1.Append(" and   DATENAME(month," & CFWColumn & ")+'-'+convert(varchar(4),year(" & CFWColumn & "))")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("and year(" & CFWColumn & ") = year(getdate())")
                                            Case 8
                                                SqlSummGrid1.Append("  and  FormatedDateFromDate(" & CFWColumn & ",@numDomainId)")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 9
                                                SqlSummGrid1.Append("  and  DATEPART(week," & CFWColumn & ")")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("and DATEPART(week," & CFWColumn & ") in (datepart(week,getdate())-1,datepart(week,getdate()))")
                                            Case 10
                                                SqlSummGrid1.Append("  and  DATEPART(month," & CFWColumn & ")")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and year(" & CFWColumn & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("and month(" & CFWColumn & ") in (datepart(month,getdate())-1,datepart(month,getdate()))")
                                            Case 11
                                                SqlSummGrid1.Append("  and  DATEPART(year," & CFWColumn & ")")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 12
                                                SqlSummGrid1.Append("  and  DATEPART(q," & CFWColumn & ")")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("and year(" & CFWColumn & ") = year(getdate())")
                                                SqlSummGrid1.Append("and DATEPART(q," & CFWColumn & ") in (datepart(q,getdate())-1,datepart(q,getdate()))")
                                        End Select
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Drop Down List" Then
                                        SqlSummGrid1.Append("  and fn_GetListItemName(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Customdata" Then
                                        SqlSummGrid1.Append(" and fn_getCustomDataValue(" & CFWColumn & "," & ddlGrp1List.SelectedValue.Split("~")(5) & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Contact" Then
                                        SqlSummGrid1.Append("  and fn_GetContactName(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Company" Then
                                        SqlSummGrid1.Append("  and fn_GetComapnyName(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Group" Then
                                        SqlSummGrid1.Append("  and fn_GetGroupName(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "UOM" Then
                                        SqlSummGrid1.Append("  and fn_GetUOMName(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "ItemGroup" Then
                                        SqlSummGrid1.Append("  and fn_GetItemGroupsName(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "IncomeAccount" Or ddlGrp1List.SelectedValue.Split("~")(3) = "AssetAcCOA" Or ddlGrp1List.SelectedValue.Split("~")(3) = "COG" Then
                                        SqlSummGrid1.Append("  and fn_GetChart_Of_AccountsName(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Currency" Then
                                        SqlSummGrid1.Append("  and fn_GetCurrencyName(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Else
                                        SqlSummGrid1.Append(" and " & CFWColumn)
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" = '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    End If
                                    ' SqlSummGrid1.Append(" fn_GetListItemName(ACI." & ddlGrp1List.SelectedValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                Else
                                    If ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field U" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field D" Then
                                        Dim Field As String
                                        If ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field D" Then
                                            Field = ddlGrp1List.SelectedValue.Split("~")(1)
                                        ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field U" Then
                                            Field = "DateAdd(minute,-@ClientTimeZoneOffset," & ddlGrp1List.SelectedValue.Split("~")(1) & ")"
                                        End If
                                        Select Case ddlGrp1Filter.SelectedValue
                                            Case 0
                                                SqlSummGrid1.Append("  and  FormatedDateFromDate(" & Field & ",@numDomainId)")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & Field & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 1
                                                SqlSummGrid1.Append(" and   FormatedDateFromDate(get_week_start(" & Field & "),@numDomainID)+' - '+ FormatedDateFromDate(get_week_end( " & Field & "),@numDomainID)")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & Field & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 2
                                                SqlSummGrid1.Append(" and   DATENAME(month," & Field & ")+'-'+convert(varchar(4),year(" & Field & "))")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & Field & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 3
                                                SqlSummGrid1.Append(" and   fn_getCustomDataValue(DATEPART(q," & Field & "),13)+'-'+convert(varchar(4),year(" & Field & "))")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & Field & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 4
                                                SqlSummGrid1.Append("  and  DATEPART(year," & Field & ")")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & Field & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 5
                                                SqlSummGrid1.Append(" and  fn_getCustomDataValue(GetFiscalQuarter(" & Field & ",@numDomainId),13)")
                                                SqlSummGrid1.Append(" +' '+FormatedDateFromDate(GetFQDate(" & Field & ", GetFiscalQuarter(" & Field & ",@numDomainId),'S',@numDomainId),@numDomainId)")
                                                SqlSummGrid1.Append("+'- '+FormatedDateFromDate(GetFQDate(" & Field & ", GetFiscalQuarter(" & Field & ",@numDomainId),'E',@numDomainId),@numDomainId) ")
                                                ' SqlSummGrid1.Append(" and fn_getCustomDataValue(GetFiscalQuarter(" & Field & ",@numDomainId),13)")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append(" and FormatedDateFromDate(GetFiscalStartDate(getFiscalYear(" & Field & ",@numDomainId),@numDomainId),@numDomainId)")
                                                If Not IsDBNull(row.Item("Fstart")) Then
                                                    SqlSummGrid1.Append(" = '" & row.Item("Fstart") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 6
                                                SqlSummGrid1.Append(" and  FormatedDateFromDate(GetFiscalStartDate(GetFiscalyear(" & Field & ",@numDomainId),@numDomainId),@numDomainId)")
                                                SqlSummGrid1.Append(" +'-'+ FormatedDateFromDate(dateadd(year,1,GetFiscalStartDate(GetFiscalyear(" & Field & ",@numDomainId),@numDomainId)),@numDomainId)")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                            Case 7
                                                SqlSummGrid1.Append(" and   DATENAME(month," & Field & ")+'-'+convert(varchar(4),year(" & Field & "))")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("and year(" & Field & ") = year(getdate())")
                                            Case 8
                                                SqlSummGrid1.Append("  and  FormatedDateFromDate(" & Field & ",@numDomainId)")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & Field & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                ' SqlSummGrid1.Append("and year(ACI." & Field & ") = year(getdate())")
                                            Case 9
                                                SqlSummGrid1.Append("  and  DATEPART(week," & Field & ")")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & Field & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                ' SqlSummGrid1.Append("and year(" & Field & ") = year(getdate())")
                                                SqlSummGrid1.Append("and DATEPART(week," & Field & ") in (datepart(week,getdate())-1,datepart(week,getdate()))")
                                            Case 10
                                                SqlSummGrid1.Append("  and  DATEPART(month," & Field & ")")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and year(" & Field & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                ' SqlSummGrid1.Append("and year(" & Field & ") = year(getdate())")
                                                SqlSummGrid1.Append("and month(" & Field & ") in (datepart(month,getdate())-1,datepart(month,getdate()))")
                                            Case 11
                                                SqlSummGrid1.Append("  and  DATEPART(year," & Field & ")")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & Field & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                'SqlSummGridFilter.Append("and year(ACI." & Field & ") = year(getdate())")
                                                ' SqlSummGrid1.Append("and year(" & Field & ") in (datepart(year,getdate())-1,datepart(year,getdate()))")
                                            Case 12
                                                SqlSummGrid1.Append("  and  DATEPART(q," & Field & ")")
                                                If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("  and  year(" & Field & ")")
                                                If Not IsDBNull(row.Item("year")) Then
                                                    SqlSummGrid1.Append(" iLike '" & row.Item("year") & "'")
                                                Else : SqlSummGrid1.Append(" is null")
                                                End If
                                                SqlSummGrid1.Append("and year(" & Field & ") = year(getdate())")
                                                SqlSummGrid1.Append("and DATEPART(q," & Field & ") in (datepart(q,getdate())-1,datepart(q,getdate()))")
                                        End Select
                                        ' SqlSummGrid1.Append("  FormatedDateFromDate(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                        ' SqlSummGrid2.Append(" group by FormatedDateFromDate(" & ddlGrp1List.SelectedValue.Split("~")(1) & ",@numDomainId) ")
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Drop Down List" Then
                                        SqlSummGrid1.Append("  and fn_GetListItemName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Customdata" Then
                                        SqlSummGrid1.Append(" and fn_getCustomDataValue(" & ddlGrp1List.SelectedValue.Split("~")(1) & "," & ddlGrp1List.SelectedValue.Split("~")(5) & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Contact" Then
                                        SqlSummGrid1.Append("  and fn_GetContactName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Company" Then
                                        SqlSummGrid1.Append("  and fn_GetComapnyName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Group" Then
                                        SqlSummGrid1.Append("  and fn_GetGroupName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "UOM" Then
                                        SqlSummGrid1.Append("  and fn_GetUOMName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "ItemGroup" Then
                                        SqlSummGrid1.Append("  and fn_GetItemGroupsName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "IncomeAccount" Or ddlGrp1List.SelectedValue.Split("~")(3) = "AssetAcCOA" Or ddlGrp1List.SelectedValue.Split("~")(3) = "COG" Then
                                        SqlSummGrid1.Append("  and fn_GetChart_Of_AccountsName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    ElseIf ddlGrp1List.SelectedValue.Split("~")(3) = "Currency" Then
                                        SqlSummGrid1.Append("  and fn_GetCurrencyName(" & ddlGrp1List.SelectedValue.Split("~")(1) & ")")
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" iLike '" & row.Item(ddlGrp1List.SelectedItem.Text) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Else
                                        SqlSummGrid1.Append(" and " & ddlGrp1List.SelectedValue.Split("~")(1))
                                        If Not IsDBNull(row.Item(ddlGrp1List.SelectedItem.Text)) Then
                                            SqlSummGrid1.Append(" = '" & row.Item(ddlGrp1List.SelectedItem.Text).Replace("'", "''") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    End If
                                End If
                                'Dim objCustomReports As New CustomReports
                                '  Dim bField As BoundField
                                'If lstGroup.SelectedValue <> 31 Then
                                Sql = txtSql.Text & " " & SqlSummGrid1.ToString
                                'Else
                                'Sql = txtSql.Text.Replace("/*addfilters*/", SqlSummGrid1.ToString)
                                'End If

                                objCustomReports.DynamicQuery = Sql
                                objCustomReports.UserCntID = Session("UserContactId")
                                objCustomReports.DomainID = Session("DomainId")
                                objCustomReports.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                ' Dim ds As DataSet
                                ds = objCustomReports.ExecuteDynamicSql()
                                Dim dtDtlTable As DataTable
                                dtDtlTable = ds.Tables(0)
                                Dim iTr As Integer = 0
                                Dim Trd As TableRow
                                For Each rowDtl As DataRow In dtDtlTable.Rows
                                    Trd = New TableRow
                                    For i = 0 To strfields.Length - 1
                                        TD = New TableCell
                                        TD.Text = IIf(IsDBNull(rowDtl.Item(strfields(i))), "", rowDtl.Item(strfields(i)))
                                        If iTr Mod 2 = 0 Then
                                            Trd.CssClass = "report1"
                                        Else : Trd.CssClass = "report2"
                                        End If
                                        Trd.Cells.Add(TD)
                                    Next
                                    tblSummaryGrid.Rows.Add(Trd)
                                    iTr = iTr + 1
                                Next
                            End If
                        Next
                        If Not dtFooter Is Nothing Then
                            'Dim dtFooter As DataTable = dtFooterTable
                            Dim TRF As New TableRow
                            Dim TRfo As TableRow
                            Dim TDfo As TableCell
                            'Dim strfields() As String = txtOrderListText.Text.Split("|")
                            Dim ifo As Integer

                            For ifo = 0 To strfields.Length - 1
                                Dim str As String = " "
                                If ifo = 0 Then
                                    If dtFooter.Columns.Contains("RecCount") Then
                                        str = str & " Total Records = " & IIf(IsDBNull(dtFooter.Rows(0).Item("RecCount")), 0, dtFooter.Rows(0).Item("RecCount"))
                                    End If
                                End If
                                If dtFooter.Columns.Contains("sum" & strfields(ifo)) Then
                                    str = str & " Total = " & Format(IIf(IsDBNull(dtFooter.Rows(0).Item("sum" & strfields(ifo))), 0, dtFooter.Rows(0).Item("sum" & strfields(ifo))), "f")
                                End If
                                If dtFooter.Columns.Contains("Avg" & strfields(ifo)) Then
                                    str = str & " Avg = " & Format(IIf(IsDBNull(dtFooter.Rows(0).Item("Avg" & strfields(ifo))), 0, dtFooter.Rows(0).Item("Avg" & strfields(ifo))), "f")
                                End If
                                If dtFooter.Columns.Contains("Max" & strfields(ifo)) Then
                                    str = str & " Max = " & Format(IIf(IsDBNull(dtFooter.Rows(0).Item("Max" & strfields(ifo))), 0, dtFooter.Rows(0).Item("Max" & strfields(ifo))), "f")
                                End If
                                If dtFooter.Columns.Contains("Min" & strfields(ifo)) Then
                                    str = str & " Min = " & Format(IIf(IsDBNull(dtFooter.Rows(0).Item("Min" & strfields(ifo))), 0, dtFooter.Rows(0).Item("Min" & strfields(ifo))), "f")
                                End If
                                TDfo = New TableCell
                                'TDfo.Text = TDfo.Text & "<font color=white>" & str & "</font>"
                                TDfo.Text = TDfo.Text & str
                                TRF.CssClass = "hs"
                                TRF.Cells.Add(TDfo)
                            Next
                            tblSummaryGrid.Rows.Add(TRF)
                        End If
                    End If
                End If
                radOppTab.SelectedIndex = 7
                radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                BuildSqlQuery()
                Save()
                BindDataGrid()
                btnNewScheduler.Visible = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub Save()
            Try
                Dim iReportFields As Integer = 0
                Dim dtReportFields As New DataTable
                dtReportFields = Session("ReportFields")
                Dim CheckedFields As String = ""
                Dim IsChecked As String = ""

                Dim strFields As String
                Dim ds As New DataSet

                Dim dtFields As New DataTable
                dtFields.Columns.Add("vcField")
                dtFields.Columns.Add("vcValue")
                Dim dtRow As DataRow
                Dim i As Integer = 0

                For iReportFields = 0 To dtReportFields.Rows.Count - 1
                    Dim chk As New CheckBox
                    If Not radOppTab.MultiPage.FindControl(dtReportFields.Rows(iReportFields).Item("vcDbFieldName") & "~" & dtReportFields.Rows(iReportFields).Item("numFieldGroupID") & "~" & dtReportFields.Rows(iReportFields).Item("CType")) Is Nothing Then
                        chk = radOppTab.MultiPage.FindControl(dtReportFields.Rows(iReportFields).Item("vcDbFieldName") & "~" & dtReportFields.Rows(iReportFields).Item("numFieldGroupID") & "~" & dtReportFields.Rows(iReportFields).Item("CType"))
                        If chk.Checked = True Then
                            dtRow = dtFields.NewRow
                            dtRow("vcField") = chk.ID
                            dtRow("vcValue") = chk.Checked
                            dtFields.Rows.Add(dtRow)
                        End If
                    End If
                Next

                Dim ReportId As Long = 0
                If Not txtReportId.Text Is Nothing Then ReportId = CInt(txtReportId.Text)
                If CType(radOppTab.MultiPage.FindControl("ddlGrp1List"), DropDownList).SelectedValue.Split("~").Length >= 3 Then
                    If CType(radOppTab.MultiPage.FindControl("ddlGrp1List"), DropDownList).SelectedValue.Split("~")(3) <> "Date Field" And CType(radOppTab.MultiPage.FindControl("ddlGrp1List"), DropDownList).SelectedValue.Split("~")(3) <> "Date Field U" And CType(radOppTab.MultiPage.FindControl("ddlGrp1List"), DropDownList).SelectedValue.Split("~")(3) <> "Date Field D" Then
                        ddlGrp1Filter.SelectedIndex = 0
                    End If
                Else : ddlGrp1Filter.SelectedIndex = 0
                End If

                Dim objCustomReport As New CustomReports

                objCustomReport.ReportName = txtReportName.Text
                objCustomReport.DynamicQuery = txtSql.Text
                objCustomReport.ReportDesc = txtReportDesc.Text
                objCustomReport.UserCntID = Session("userContactId")
                objCustomReport.DomainID = Session("DomainId")
                objCustomReport.ReportID = ReportId
                objCustomReport.GroupId = lstGroup.SelectedValue
                objCustomReport.numCustModuleId = ddlModule.SelectedValue
                objCustomReport.FilterRel = IIf(radAnd.Checked = True, True, False)
                objCustomReport.FilterType = CInt(txtchkAdvFilter.Text)
                objCustomReport.AdvFilterStr = txtAdvFilter.Text
                objCustomReport.GridType = chkLinerGrid.Checked
                objCustomReport.OrderbyFld = CType(radOppTab.MultiPage.FindControl("ddlOrderflt"), DropDownList).SelectedValue
                objCustomReport.Order = ddlOrderFilter.SelectedValue
                objCustomReport.CompFilter1 = CType(radOppTab.MultiPage.FindControl("ddlCompFlt1"), DropDownList).SelectedValue
                objCustomReport.CompFilter2 = CType(radOppTab.MultiPage.FindControl("ddlCompFlt2"), DropDownList).SelectedValue
                objCustomReport.CompFilterOpr = ddlCompFOpe.SelectedValue
                objCustomReport.SortRecCount = chkSortRecCount.Checked
                objCustomReport.DrillDown = chkDrllDownRpt.Checked
                If radMyself.Checked = True Then
                    objCustomReport.MttId = 1
                    objCustomReport.MttValue = CType(radOppTab.MultiPage.FindControl("ddlMyself"), DropDownList).SelectedValue
                ElseIf radTeamSelected.Checked = True Then
                    objCustomReport.MttId = 2
                    objCustomReport.MttValue = CType(radOppTab.MultiPage.FindControl("ddlTeam"), DropDownList).SelectedValue
                ElseIf radTerriotary.Checked = True Then
                    objCustomReport.MttId = 3
                    objCustomReport.MttValue = CType(radOppTab.MultiPage.FindControl("ddlTerr"), DropDownList).SelectedValue
                Else
                    objCustomReport.MttId = 0
                    objCustomReport.MttValue = 0
                End If
                If chkLinerGrid.Checked = False Then

                    objCustomReport.strGrpfld = CType(radOppTab.MultiPage.FindControl("ddlGrp1List"), DropDownList).SelectedValue
                    objCustomReport.strGrpOrd = ddlGrp1Order.SelectedValue
                    objCustomReport.strGrpflt = ddlGrp1Filter.SelectedValue
                    objCustomReport.sqlGrp = Textbox1.Text
                Else
                    objCustomReport.strGrpfld = ""
                    objCustomReport.strGrpOrd = ""
                    objCustomReport.strGrpflt = ""
                    objCustomReport.sqlGrp = ""
                End If

                objCustomReport.stdFieldId = CType(radOppTab.MultiPage.FindControl("ddlStdFilter"), DropDownList).SelectedValue
                objCustomReport.custFilter = ddlCustomTime.SelectedValue
                If Not calFrom.SelectedDate Is Nothing Then objCustomReport.FromDate = calFrom.SelectedDate
                If Not calTo.SelectedDate Is Nothing Then objCustomReport.ToDate = calTo.SelectedDate

                objCustomReport.Rows = ddlNoRows.SelectedValue

                ReportId = objCustomReport.SaveCustomReport()
                txtReportId.Text = ReportId

                dtFields.TableName = "Table"
                ds.Tables.Add(dtFields.Copy)
                strFields = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                SaveSummationList(ReportId)
                SaveCheckedFields(ReportId, strFields)
                SaveOrderList(ReportId)
                SaveFilterList(ReportId)

                If txtReportId.Text <> 0 Then
                    btnExportPdf.Visible = True
                Else : btnExportPdf.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveCheckedFields(ByVal ReportId As Long, ByVal strFields As String)
            Try
                If strFields <> "" Then
                    Dim objCustomReport As New CustomReports
                    objCustomReport.ReportID = ReportId
                    objCustomReport.strCheckedFields = strFields
                    objCustomReport.SaveCustomReportCheckedFields()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveOrderList(ByVal ReportId As Long)
            Try
                Dim strOrderText As String()
                Dim strOrderValues As String()
                Dim strFields As String
                Dim ds As New DataSet

                Dim dtFields As New DataTable
                dtFields.Columns.Add("vcText")
                dtFields.Columns.Add("vcValue")
                dtFields.Columns.Add("numOrder")
                Dim dtRow As DataRow
                Dim i As Integer = 0
                For Each listitem As ListItem In lstReportColumnsOrder.Items
                    dtRow = dtFields.NewRow
                    dtRow("vcText") = listitem.Text
                    dtRow("vcValue") = listitem.Value
                    dtRow("numOrder") = i + 1
                    dtFields.Rows.Add(dtRow)
                    i = i + 1
                Next

                dtFields.TableName = "Table"
                ds.Tables.Add(dtFields.Copy)
                strFields = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                Dim objCustomReport As New CustomReports
                objCustomReport.ReportID = ReportId
                objCustomReport.strOrderFieldsText = strFields
                objCustomReport.SaveCustomReportOrderList()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveFilterList(ByVal ReportId As Long)
            Try
                Dim strFields As String
                Dim ds As New DataSet

                Dim dtFields As New DataTable
                dtFields.Columns.Add("vcFilterFieldsText")
                dtFields.Columns.Add("vcFilterFieldsValue")
                dtFields.Columns.Add("vcFilterFieldsOperator")
                dtFields.Columns.Add("vcFilterValue")
                dtFields.Columns.Add("vcFilterValued")
                Dim dtRow As DataRow
                Dim iFilter As Integer = 0
                For iFilter = 0 To CInt(noFilterRows.Text)
                    If (Not radOppTab.MultiPage.FindControl("ddlField" & iFilter) Is Nothing) And (Not radOppTab.MultiPage.FindControl("Operator" & iFilter) Is Nothing) And (Not radOppTab.MultiPage.FindControl("Value" & iFilter) Is Nothing) Then
                        Dim ddlField As New DropDownList
                        Dim ddlOperator As New HtmlSelect
                        Dim txtValue As New HtmlInputText
                        Dim txtValued As New HtmlInputText
                        ddlField = radOppTab.MultiPage.FindControl("ddlField" & iFilter)
                        If ddlField.SelectedValue <> "0" Then
                            ddlOperator = radOppTab.MultiPage.FindControl("Operator" & iFilter)
                            txtValue = radOppTab.MultiPage.FindControl("Value" & iFilter)
                            txtValued = radOppTab.MultiPage.FindControl("Valued" & iFilter)
                            dtRow = dtFields.NewRow
                            dtRow("vcFilterFieldsText") = ddlField.SelectedItem.Text
                            dtRow("vcFilterFieldsValue") = ddlField.SelectedValue
                            dtRow("vcFilterFieldsOperator") = ddlOperator.Value
                            dtRow("vcFilterValue") = txtValue.Value
                            dtRow("vcFilterValued") = txtValued.Value
                            dtFields.Rows.Add(dtRow)
                        End If
                    End If
                Next

                dtFields.TableName = "Table"
                ds.Tables.Add(dtFields.Copy)
                strFields = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                Dim objCustomReport As New CustomReports
                objCustomReport.ReportID = ReportId
                objCustomReport.strOrderFieldsText = strFields
                objCustomReport.SaveCustomReportFilterList()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveSummationList(ByVal Reportid As Long)
            Try
                Dim strFields As String
                Dim ds As New DataSet

                Dim dtFields As New DataTable
                dtFields.Columns.Add("vcField", GetType(String))
                dtFields.Columns.Add("bitSum", GetType(Boolean))
                dtFields.Columns.Add("bitAvg", GetType(Boolean))
                dtFields.Columns.Add("bitMax", GetType(Boolean))
                dtFields.Columns.Add("bitMin", GetType(Boolean))
                Dim dtRow As DataRow
                Dim iSummation As Integer = 0
                Dim dtReportFields As DataTable
                dtReportFields = Session("ReportFields")

                For Each row As DataRow In dtReportFields.Rows
                    If row("boolSummationpossible") = 1 Then
                        If (Not radOppTab.MultiPage.FindControl("Sum" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID")) Is Nothing) _
                               And (Not radOppTab.MultiPage.FindControl("Avg" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID")) Is Nothing) _
                              And (Not radOppTab.MultiPage.FindControl("Max" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID")) Is Nothing) _
                              And Not radOppTab.MultiPage.FindControl("Min" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID")) Is Nothing Then
                            Dim ChkSum As New CheckBox
                            Dim ChkAvg As New CheckBox
                            Dim ChkMax As New CheckBox
                            Dim ChkMin As New CheckBox
                            ChkSum = radOppTab.MultiPage.FindControl("Sum" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID"))
                            ChkAvg = radOppTab.MultiPage.FindControl("Avg" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID"))
                            ChkMax = radOppTab.MultiPage.FindControl("Max" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID"))
                            ChkMin = radOppTab.MultiPage.FindControl("Min" & row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID"))
                            dtRow = dtFields.NewRow
                            dtRow("vcField") = row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID")
                            dtRow("bitSum") = ChkSum.Checked
                            dtRow("bitAvg") = ChkAvg.Checked
                            dtRow("bitMax") = ChkMax.Checked
                            dtRow("bitMin") = ChkMin.Checked
                            dtFields.Rows.Add(dtRow)
                        End If
                    End If
                Next
                dtRow = dtFields.NewRow
                dtRow("vcField") = "chkRecCount"
                dtRow("bitSum") = CType(radOppTab.MultiPage.FindControl("chkRecCount"), CheckBox).Checked
                dtRow("bitAvg") = False
                dtRow("bitMax") = False
                dtRow("bitMin") = False
                dtFields.Rows.Add(dtRow)

                dtFields.TableName = "Table"
                ds.Tables.Add(dtFields.Copy)
                strFields = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                Dim objCustomReport As New CustomReports
                objCustomReport.ReportID = Reportid
                objCustomReport.strOrderFieldsText = strFields
                objCustomReport.SaveCustomReportSumList()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindSelectedFields()
            Try
                Dim objReport As New CustomReports
                Dim dtTable As DataTable
                Dim dtReportFields As DataTable
                dtReportFields = Session("ReportFields")
                For Each row As DataRow In dtReportFields.Rows
                    If Not radOppTab.MultiPage.FindControl(row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID") & "~" & row.Item("CType")) Is Nothing Then
                        CType(radOppTab.MultiPage.FindControl(row.Item("vcDbFieldName") & "~" & row.Item("numFieldGroupID") & "~" & row.Item("CType")), CheckBox).Checked = False
                    End If
                Next
                objReport.ReportID = CInt(txtReportId.Text)
                dtTable = objReport.getCheckedFields()
                If dtTable.Rows.Count > 0 Then
                    For Each row As DataRow In dtTable.Rows
                        If Not radOppTab.MultiPage.FindControl(row.Item("vcFieldName")) Is Nothing Then
                            CType(radOppTab.MultiPage.FindControl(row.Item("vcFieldName")), CheckBox).Checked = row.Item("vcValue")
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindOrderList()
            Try
                Dim objReport As New CustomReports
                Dim dtTable As DataTable
                objReport.ReportID = CInt(txtReportId.Text)
                dtTable = objReport.getReportOrderList()
                If dtTable.Rows.Count > 0 Then
                    lstReportColumnsOrder.Items.Clear()
                    For Each row As DataRow In dtTable.Rows
                        Dim listItem As New ListItem
                        listItem.Text = row.Item("vcFieldText")
                        listItem.Value = row.Item("vcValue")
                        lstReportColumnsOrder.Items.Add(listItem)
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindfilterLists()
            Try
                Dim objReport As New CustomReports
                Dim dtTable As DataTable
                objReport.ReportID = CInt(txtReportId.Text)
                dtTable = objReport.getReportFilterList()
                If dtTable.Rows.Count > 0 Then
                    If dtTable.Rows.Count - 1 > CInt(noFilterRows.Text) Then
                        Dim noFilterRow As Integer = CInt(noFilterRows.Text)
                        Dim i As Integer
                        For i = 1 To (dtTable.Rows.Count - 1) - noFilterRow
                            AddFilterRow()
                        Next
                    End If

                    Dim iFilter As Integer
                    For iFilter = 0 To dtTable.Rows.Count - 1
                        If (Not radOppTab.MultiPage.FindControl("ddlField" & iFilter) Is Nothing) And (Not radOppTab.MultiPage.FindControl("Operator" & iFilter) Is Nothing) And (Not radOppTab.MultiPage.FindControl("Value" & iFilter) Is Nothing) Then

                            Dim ddlField As New DropDownList
                            Dim ddlOperator As New HtmlSelect
                            Dim txtValue As New HtmlInputText
                            Dim txtValued As New HtmlInputText
                            Dim img As New HtmlImage
                            ddlField = radOppTab.MultiPage.FindControl("ddlField" & iFilter)
                            ddlOperator = radOppTab.MultiPage.FindControl("Operator" & iFilter)
                            txtValue = radOppTab.MultiPage.FindControl("Value" & iFilter)
                            txtValued = radOppTab.MultiPage.FindControl("Valued" & iFilter)

                            If Not ddlField.Items.FindByValue(dtTable.Rows(iFilter).Item("vcFieldsValue")) Is Nothing Then
                                ddlField.Items(ddlField.SelectedIndex).Selected = False
                                ddlField.Items.FindByValue(dtTable.Rows(iFilter).Item("vcFieldsValue")).Selected = True
                            End If

                            If Not ddlOperator.Items.FindByValue(dtTable.Rows(iFilter).Item("vcFieldsOperator")) Is Nothing Then
                                ddlOperator.Items(ddlOperator.SelectedIndex).Selected = False
                                ddlOperator.Items.FindByValue(dtTable.Rows(iFilter).Item("vcFieldsOperator")).Selected = True
                            End If
                            txtValue.Value = IIf(IsDBNull(dtTable.Rows(iFilter).Item("vcFilterValue")), "", dtTable.Rows(iFilter).Item("vcFilterValue"))
                            txtValued.Value = IIf(IsDBNull(dtTable.Rows(iFilter).Item("vcFilterValued")), "", dtTable.Rows(iFilter).Item("vcFilterValued"))
                        End If
                    Next
                End If

                Dim ddlOrderflt As DropDownList = radOppTab.MultiPage.FindControl("ddlOrderflt")
                ddlOrderflt.Items.Clear()
                Dim itemos As New ListItem
                itemos.Text = "--SelectOne--"
                itemos.Value = 0
                ddlOrderflt.Items.Add(itemos)
                For Each itm As ListItem In lstReportColumnsOrder.Items
                    Dim itemO As New ListItem
                    itemO.Text = itm.Text
                    itemO.Value = itm.Value
                    If itm.Value.Split("~").Length >= 2 Then itemO.Attributes("OptionGroup") = getGroupName(itm.Value.Split("~")(1))
                    ddlOrderflt.Items.Add(itemO)
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindSummationLists()
            Try
                tblSum.Controls.Clear()
                BuildSummationTab()
                Dim objReport As New CustomReports
                Dim dtTable As DataTable
                objReport.ReportID = CInt(txtReportId.Text)
                dtTable = objReport.getReportSummationList()

                If dtTable.Rows.Count > 0 Then
                    For Each row As DataRow In dtTable.Rows
                        If row.Item("vcfieldName") = "chkRecCount" Then
                            If Not radOppTab.MultiPage.FindControl(row.Item("vcfieldName")) Is Nothing Then
                                CType(radOppTab.MultiPage.FindControl(row.Item("vcfieldName")), CheckBox).Checked = row.Item("bitsum")
                            End If
                        Else
                            If Not radOppTab.MultiPage.FindControl("Sum" & row.Item("vcfieldName")) Is Nothing Then
                                CType(radOppTab.MultiPage.FindControl("Sum" & row.Item("vcfieldName")), CheckBox).Checked = row.Item("bitsum")
                                CType(radOppTab.MultiPage.FindControl("Avg" & row.Item("vcfieldName")), CheckBox).Checked = row.Item("bitAvg")
                                CType(radOppTab.MultiPage.FindControl("Max" & row.Item("vcfieldName")), CheckBox).Checked = row.Item("bitMax")
                                CType(radOppTab.MultiPage.FindControl("Min" & row.Item("vcfieldName")), CheckBox).Checked = row.Item("bitMin")
                            End If

                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindSchedulerGrid()
            Try
                If CInt(txtReportId.Text) <> 0 Then

                    Dim objCustReport As New CustomReports
                    objCustReport.ReportID = CInt(txtReportId.Text)
                    objCustReport.DomainID = Session("DomainId")
                    dgScheduler.DataSource = objCustReport.getSchedulerListRpt()
                    dgScheduler.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgScheduler_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgScheduler.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objCustReport As New CustomReports
                    With objCustReport
                        .ScheduleId = e.Item.Cells(0).Text
                        .DelSchedule()
                    End With
                    BindSchedulerGrid()
                    BindDataGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgScheduler_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgScheduler.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim hplEmail As HyperLink
                    hplEmail = e.Item.FindControl("hplSchdeule")
                    hplEmail.NavigateUrl = "#"
                    hplEmail.Attributes.Add("onclick", "return openScheduler('" & e.Item.Cells(0).Text & "'," & txtReportId.Text & ")")
                    Dim btnDel As Button
                    btnDel = e.Item.FindControl("btnDelete")
                    btnDel.Attributes.Add("onclick", "return DelSchedule('" & e.Item.Cells(0).Text & "')")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btndelSchedule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelSchedule.Click
            Try
                BindSchedulerGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModule.SelectedIndexChanged
            Try
                bindGroups()

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub bindGroups()
            Try
                Dim ds As DataSet
                Dim ojReports As New CustomReports
                ojReports.numCustModuleId = ddlModule.SelectedValue   'setting the group as 
                ds = ojReports.GetModules()

                lstGroup.DataSource = ds.Tables(1)
                lstGroup.DataTextField = "vcFieldGroupName"
                lstGroup.DataValueField = "numReportOptionGroupId"
                lstGroup.DataBind()





                If lstGroup.Items.Count > 0 Then lstGroup.Items(0).Selected = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
            'Try
            If chkLinerGrid.Checked = True Then
                '    Dim Sql As String
                '    Sql = txtSql.Text
                '    If Sql <> "" Then
                '        dgReport.AutoGenerateColumns = True
                '        Dim objCustomReports As New CustomReports
                '        Dim bField As BoundField
                '        objCustomReports.DynamicQuery = Sql
                '        objCustomReports.UserCntID = Session("UserContactId")
                '        objCustomReports.DomainID = Session("DomainId")
                '        objCustomReports.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                '        Dim ds As DataSet
                '        ds = objCustomReports.ExecuteDynamicSql()
                '        If ds.Tables.Count = 2 Then dgReport.FooterStyle.CssClass = "hs"
                '        dgReport.DataSource = ds.Tables(0)
                '        dgReport.DataBind()
                BindDataGrid()
                ExportToExcel.DataGridToExcel(dgReport, Response)
                'End If
            Else
                BindDataGrid()
                dgReport.AutoGenerateColumns = False
                ' binding datagrid result to export list
                Response.Clear()
                Response.AddHeader("content-disposition", "attachment;filename=DetailReport" & Now().ToString & ".xls")
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = "application/vnd.xls"
                Dim stringWrite As IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As HtmlTextWriter = New HtmlTextWriter(stringWrite)
                tblSummaryGrid.RenderControl(htmlWrite)
                Response.Write(stringWrite.ToString())
                Response.End()
                ' ExportToExcel.aspTableToExcel(tblSummaryGrid, Response)
            End If
            'Catch ex As Exception
            '    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            '    Response.Write(ex)
            'End Try
        End Sub

        Private Sub btnExportPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportPdf.Click
            Dim objPdf As Object
            Try
                Dim objHTMLToPDF As New HTMLToPDF
                Dim strURL As String = "http://Portal.bizautomation.com/reports/frmCustomReport.aspx?reportId=" & txtReportId.Text & "&UserContactId=" & Session("UserContactId") & "&DomainId=" & Session("DomainId")
                objPdf = objHTMLToPDF.Convert(strURL, Session("DomainID"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
            Response.Clear()
            Response.ClearContent()
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment; filename=" + objPdf.ToString)
            Response.WriteFile(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & objPdf.ToString)
            Response.End()
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                BuildSqlQuery()
                Save()
                pg_redirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub frmCustomReportWiz_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
            Try
                Dim i As Integer
                For i = 0 To CInt(noFilterRows.Text)
                    Dim ddl As New DropDownList
                    ddl = radOppTab.MultiPage.FindControl("ddlField" & i)
                    If ddl.SelectedValue.Split("~").Length >= 2 Then
                        If ddl.SelectedValue.Split("~")(2) = "Drop Down List" Or ddl.SelectedValue.Split("~")(2) = "Customdata" Or ddl.SelectedValue.Split("~")(2) = "Check box" Or _
                        ddl.SelectedValue.Split("~")(2) = "Contact" Or ddl.SelectedValue.Split("~")(2) = "State" Or ddl.SelectedValue.Split("~")(2) = "Date Field" Or ddl.SelectedValue.Split("~")(2) = "Date Field U" Or ddl.SelectedValue.Split("~")(2) = "Date Field D" Or ddl.SelectedValue.Split("~")(2) = "Terriotary" Or ddl.SelectedValue.Split("~")(2) = "UOM" Or ddl.SelectedValue.Split("~")(2) = "ItemGroup" Or ddl.SelectedValue.Split("~")(2) = "IncomeAccount" Or ddl.SelectedValue.Split("~")(2) = "AssetAcCOA" Or ddl.SelectedValue.Split("~")(2) = "COG" Or ddl.SelectedValue.Split("~")(2) = "Currency" Then
                            If ddl.SelectedValue.Split("~")(2) = "Date Field" Or ddl.SelectedValue.Split("~")(2) = "Date Field U" Or ddl.SelectedValue.Split("~")(2) = "Date Field D" Then
                                CType(radOppTab.MultiPage.FindControl("imgHpl" & i), System.Web.UI.WebControls.Image).ImageUrl = "../Images/Calender.gif"
                            Else : CType(radOppTab.MultiPage.FindControl("imgHpl" & i), System.Web.UI.WebControls.Image).ImageUrl = "../Images/SEARCH_ICON.GIF"
                            End If
                            CType(radOppTab.MultiPage.FindControl("imgHpl" & i), System.Web.UI.WebControls.Image).Style("Display") = ""
                            CType(radOppTab.MultiPage.FindControl("Value" & i), HtmlInputText).Disabled = True
                        Else
                            CType(radOppTab.MultiPage.FindControl("imgHpl" & i), System.Web.UI.WebControls.Image).Style("Display") = "none"
                            CType(radOppTab.MultiPage.FindControl("Value" & i), HtmlInputText).Disabled = False
                        End If
                    End If
                Next
                Dim ddlGrp1List As DropDownList
                ddlGrp1List = radOppTab.MultiPage.FindControl("ddlGrp1List")
                If ddlGrp1List.SelectedValue.Split("~").Length >= 3 Then
                    If ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field U" Or ddlGrp1List.SelectedValue.Split("~")(3) = "Date Field D" Then
                        ddlGrp1Filter.Enabled = True
                    Else : ddlGrp1Filter.Enabled = False
                    End If
                End If
                Session("ReportFields") = Nothing
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Function getGroupName(ByVal groupId As Long) As String
            Try
                Dim ret As String = ""
                Select Case groupId
                    Case 6 : ret = "Contacts"
                    Case 7 : ret = "Organization"
                    Case 8 : ret = "Organization Assets"
                    Case 9 : ret = "Projects"
                    Case 10 : ret = "Projects Stages"
                    Case 11 : ret = "Projects Sub Stages"
                    Case 12 : ret = "Projects Contacts"
                    Case 13 : ret = "Projects Expenses"
                    Case 14 : ret = "Projects Time"
                    Case 15 : ret = "Cases"
                    Case 16 : ret = "Cases Expenses"
                    Case 17 : ret = "Cases Time"
                    Case 18 : ret = "Opportunities/Orders"
                    Case 19 : ret = "Opportunities Stages"
                    Case 20 : ret = "Opportunities Sub"
                    Case 21 : ret = "Opportunities Time"
                    Case 22 : ret = "Opportunities Expense"
                    Case 23 : ret = "Opportunities Contacts"
                    Case 24 : ret = "Opportunities Address"
                    Case 25 : ret = "BizDocs"
                    Case 26 : ret = "Opportunities Items"
                    Case 27 : ret = "Items"
                    Case 28 : ret = "Items Group"
                    Case 29 : ret = "Items Header"
                    Case 30 : ret = "Items with Options & Accesories"
                    Case 31 : ret = "Items Tax"
                    Case 32 : ret = "WareHouses"
                    Case 33 : ret = "WareHouses Item"
                    Case 34 : ret = "Items with Attributes"
                    Case 35 : ret = "Forecast"
                    Case 36 : ret = "Web Analysis"
                    Case 37 : ret = "Campaign"
                    Case 38 : ret = "Action Items"
                    Case 39 : ret = "Accounting"
                    Case 40 : ret = "Checks"
                    Case 41 : ret = "Email"
                    Case 42 : ret = "Sales Return"
                    Case 82 : ret = "Association"
                    Case 83 : ret = "Payments"
                    Case 84 : ret = "BizDocs Items"

                    Case Else
                        ret = ""
                End Select
                Return ret
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub pg_redirect()
            Try
                Dim frm = GetQueryStringVal("frm")
                If frm = "CustomReport" Then
                    Response.Redirect("../Reports/frmCustomReport.aspx?frm=" & GetQueryStringVal("frm1") & "&ReportId=" & txtReportId.Text, False)
                Else : Response.Redirect("../Reports/frmCustomRptList.aspx", False)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
            Try
                pg_redirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Function GetTodaysDate() As String
            Dim TodaysDate As String
            TodaysDate = "dateadd(minute,-@ClientTimeZoneOffset,getutcdate())"
            Dim FormattedDate As String = "convert(varchar," & TodaysDate & ",101)"

            Return "dateadd(Minute,@ClientTimeZoneOffset," & FormattedDate & ")"

        End Function

        Private Sub dgKPI_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgKPI.ItemCommand
            Dim objReport As CReports
            objReport = New CReports
            Dim dsKPI As New DataSet
            Dim dsKPITemp As New DataSet

            Dim dllIndicator As DropDownList
            Dim dllPeriod As DropDownList
            Dim dllPriority As DropDownList
            Dim dllStatus As DropDownList


            If e.CommandName = "Add" Then

                'dllIndicator = e.Item.FindControl("ddlIndicator")
                'dllPeriod = e.Item.FindControl("ddlPeriod")
                'dllPriority = e.Item.FindControl("ddlPriority")
                'dllStatus = e.Item.FindControl("ddlStatus")

                'If Not dllIndicator.SelectedItem Is Nothing Then e.Item.Cells(2).Text = dllIndicator.SelectedItem.Text
                'If Not dllPeriod.SelectedItem Is Nothing Then e.Item.Cells(3).Text = dllPeriod.SelectedItem.Text
                'If Not dllPriority.SelectedItem Is Nothing Then e.Item.Cells(4).Text = dllPriority.SelectedItem.Text
                'If Not dllStatus.SelectedItem Is Nothing Then e.Item.Cells(5).Text = dllStatus.SelectedItem.Text

                dsKPI = Session("dsKPI")

                'dsKPI.Tables(0).Rows(e.Item.ItemIndex).Item(2) = e.Item.Cells(2).Text
                'dsKPI.Tables(0).Rows(e.Item.ItemIndex).Item(3) = e.Item.Cells(3).Text
                'dsKPI.Tables(0).Rows(e.Item.ItemIndex).Item(4) = e.Item.Cells(4).Text
                'dsKPI.Tables(0).Rows(e.Item.ItemIndex).Item(5) = e.Item.Cells(5).Text


                If dgKPI.DataSource Is Nothing Then
                    dsKPITemp = objReport.GetCustReportKPI(0)

                    dsKPI.Tables(0).Merge(dsKPITemp.Tables(0))
                    dsKPI.AcceptChanges()

                    dgKPI.DataSource = dsKPI.Tables(0)
                    dgKPI.DataBind()
                    Session("dsKPI") = dsKPI
                End If
            End If
        End Sub

        Private Sub dgKPI_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgKPI.ItemDataBound


            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim dsKPI As New DataSet
                dsKPI = Session("dsKPI")
                Dim ddlPeriod As DropDownList
                Dim ddlPriority As DropDownList
                Dim ddlStatus As DropDownList

                ddlPeriod = e.Item.FindControl("ddlPeriod")
                ddlPriority = e.Item.FindControl("ddlPriority")
                ddlStatus = e.Item.FindControl("ddlStatus")

                If Not ddlPeriod Is Nothing Then
                    If Not ddlPeriod.Items.FindByText(dsKPI.Tables(0).Rows(e.Item.ItemIndex).Item(3)) Is Nothing Then
                        ddlPeriod.Items.FindByText(dsKPI.Tables(0).Rows(e.Item.ItemIndex).Item(3)).Selected = True
                    End If
                End If

                If Not ddlPriority Is Nothing Then
                    If Not ddlPriority.Items.FindByText(dsKPI.Tables(0).Rows(e.Item.ItemIndex).Item(4)) Is Nothing Then
                        ddlPriority.Items.FindByText(dsKPI.Tables(0).Rows(e.Item.ItemIndex).Item(4)).Selected = True
                    End If
                End If

                If Not ddlStatus Is Nothing Then
                    If Not ddlStatus.Items.FindByText(dsKPI.Tables(0).Rows(e.Item.ItemIndex).Item(5)) Is Nothing Then
                        ddlStatus.Items.FindByText(dsKPI.Tables(0).Rows(e.Item.ItemIndex).Item(5)).Selected = True
                    End If
                End If
            End If
        End Sub

        Private Sub btnAddKPI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddKPI.Click
            Try
                Dim dsKPI As New DataSet
                Dim dtKPI As New DataTable
                Dim objRow As DataRow
                Dim dg As DataGridItem

                Dim ddlPeriod As DropDownList
                Dim ddlPriority As DropDownList
                Dim ddlStatus As DropDownList

                dsKPI = Session("dsKPI")
                dtKPI = dsKPI.Tables(0)

                For Each dg In dgKPI.Items
                    ddlPeriod = dg.FindControl("ddlPeriod")
                    ddlPriority = dg.FindControl("ddlPriority")
                    ddlStatus = dg.FindControl("ddlStatus")


                    dtKPI.Rows(dg.ItemIndex).Item(3) = ddlPeriod.SelectedItem.Text
                    dtKPI.Rows(dg.ItemIndex).Item(4) = ddlPriority.SelectedItem.Text
                    dtKPI.Rows(dg.ItemIndex).Item(5) = ddlStatus.SelectedItem.Text
                    dtKPI.AcceptChanges()
                Next




                objRow = dtKPI.NewRow
                objRow.Item("numReportOptionGroupId") = 0
                objRow.Item("numFieldGroupID") = lstGroup.SelectedValue
                objRow.Item("vcIndicator") = ddlKPI.SelectedItem.Text
                objRow.Item("vcPeriod") = ""
                objRow.Item("vcPriority") = ""
                objRow.Item("vcStatusIcon") = ""
                dtKPI.Rows.Add(objRow)
                dtKPI.AcceptChanges()
                dsKPI.Tables(0).Merge(dtKPI)
                dgKPI.DataSource = dsKPI.Tables(0)
                Session("dsKPI") = dsKPI
                dgKPI.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class

    Public Class getWeeknumber

        Shared Function Weeknumbers(ByVal inDate As DateTime) As Integer
            Try
                Const JAN As Integer = 1
                Const DEC As Integer = 12
                Const LASTDAYOFDEC As Integer = 31
                Const FIRSTDAYOFJAN As Integer = 1
                Const THURSDAY As Integer = 4
                Dim ThursdayFlag As Boolean = False

                ' Get the day number since the beginning of the year
                Dim DayOfYear As Integer = inDate.DayOfYear

                ' Get the numeric weekday of the first day of the
                ' year (using sunday as FirstDay)
                Dim StartWeekDayOfYear As Integer = _
                   DirectCast(New DateTime(inDate.Year, JAN, FIRSTDAYOFJAN).DayOfWeek, Integer)
                Dim EndWeekDayOfYear As Integer = _
                    DirectCast(New DateTime(inDate.Year, DEC, LASTDAYOFDEC).DayOfWeek, Integer)

                ' Compensate for the fact that we are using monday
                ' as the first day of the week
                If StartWeekDayOfYear = 0 Then StartWeekDayOfYear = 7
                If EndWeekDayOfYear = 0 Then EndWeekDayOfYear = 7

                ' Calculate the number of days in the first and last week
                Dim DaysInFirstWeek As Integer = 8 - StartWeekDayOfYear
                Dim DaysInLastWeek As Integer = 8 - EndWeekDayOfYear

                ' If the year either starts or ends on a thursday it will have a 53rd week
                If StartWeekDayOfYear = THURSDAY OrElse EndWeekDayOfYear = THURSDAY Then ThursdayFlag = True

                ' We begin by calculating the number of FULL weeks between the start of the year and
                ' our date. The number is rounded up, so the smallest possible value is 0.
                Dim FullWeeks As Integer = _
                    CType(Math.Ceiling((DayOfYear - DaysInFirstWeek) / 7), Integer)

                Dim WeekNumber As Integer = FullWeeks

                ' If the first week of the year has at least four days, then the actual week number for our date
                ' can be incremented by one.
                If DaysInFirstWeek >= THURSDAY Then WeekNumber = WeekNumber + 1

                ' If week number is larger than week 52 (and the year doesn't either start or end on a thursday)
                ' then the correct week number is 1.
                If WeekNumber > 52 AndAlso Not ThursdayFlag Then WeekNumber = 1

                'If week number is still 0, it means that we are trying to evaluate the week number for a
                'week that belongs in the previous year (since that week has 3 days or less in our date's year).
                'We therefore make a recursive call using the last day of the previous year.
                If WeekNumber = 0 Then WeekNumber = Weeknumbers(New DateTime(inDate.Year - 1, DEC, LASTDAYOFDEC))
                Return WeekNumber
            Catch ex As Exception
                Throw ex
            End Try
        End Function




    End Class

End Namespace