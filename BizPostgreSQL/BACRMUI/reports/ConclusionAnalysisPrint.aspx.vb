Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class ConclusionAnalysisPrint
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblreportby As System.Web.UI.WebControls.Label
        Protected WithEvents lblcurrent As System.Web.UI.WebControls.Label
        Protected WithEvents lblReportHeader As System.Web.UI.WebControls.Label
        Protected WithEvents Label3 As System.Web.UI.WebControls.Label
        Protected WithEvents lblfromdt As System.Web.UI.WebControls.Label
        Protected WithEvents Label4 As System.Web.UI.WebControls.Label
        Protected WithEvents lbltodt As System.Web.UI.WebControls.Label
        Protected WithEvents tblData As System.Web.UI.WebControls.Table

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Dim objPredefinedReports As New PredefinedReports
        Dim intaryRights() As Integer

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                'intaryRights = clsAuthorization.fn_GetReportRights("Hypdealconcl", Session("UserID"))
                lblreportby.Text = Session("UserName")
                lblcurrent.Text = FormattedDateFromDate(Now(), Session("DateFormat"))
                'Set teh dates to a range of one week ending today.
                lbltodt.Text = FormattedDateFromDate(GetQueryStringVal( "tdt"), Session("DateFormat"))
                lblfromdt.Text = FormattedDateFromDate(GetQueryStringVal( "fdt"), Session("DateFormat"))

                If GetQueryStringVal( "Type") = "Deal" Then
                    lblReportHeader.Text = "Deal Conclusion Analysis Report"
                Else : lblReportHeader.Text = "Source Conclusion Analysis Report"
                End If
                DisplayRecords(GetQueryStringVal( "Type"))
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayRecords(ByVal strDeal As String)
            Try
                Dim dtPredefinedReports As DataTable
                Dim intPReportsCount As Integer
                'Declare the DataTable Row Object.
                Dim trRow As TableRow
                'Declare The DataTable Cell Objects.
                Dim tcColName As TableCell
                Dim tcColValue As TableCell
                Dim strImage As String

                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(lblfromdt.Text))
                objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(lbltodt.Text))
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.TerritoryID = Session("UserTerID")
                'Set the Rights
                'If intaryRights(RIGHTSTYPE.VIEW) = RIGHTSVALUE.ONLY_OWNER Then
                '    objPredefinedReports.UserCntID = Session("UserContactID")
                'Else
                '    objPredefinedReports.UserCntID = 0
                'End If
                'If intaryRights(RIGHTSTYPE.VIEW) = RIGHTSVALUE.ONLY_TERRITORY Then
                '    objPredefinedReports.TerritoryID = Session("UserTerID")
                'Else
                '    objPredefinedReports.TerritoryID = 0
                'End If
                Select Case GetQueryStringVal( "ReportType")
                    Case 0 : objPredefinedReports.UserRights = 0
                    Case 1 : objPredefinedReports.UserRights = 1
                    Case 2 : objPredefinedReports.UserRights = 2
                End Select

                If strDeal = "Deal" Then
                    objPredefinedReports.ReportType = 10
                Else : objPredefinedReports.ReportType = 11
                End If

                If GetQueryStringVal( "Criteria") = 0 Then objPredefinedReports.DealStatus = 1
                If GetQueryStringVal( "Criteria") = 1 Then objPredefinedReports.DealStatus = 2

                Dim intRowCount As Integer
                dtPredefinedReports = objPredefinedReports.GetDealConclusion(strDeal)
                For intRowCount = 1 To tblData.Rows.Count - 1
                    tblData.Rows(intRowCount).Cells.Clear()
                Next
                For intPReportsCount = 0 To dtPredefinedReports.Rows.Count - 1
                    'Get a New Row Object.
                    trRow = New TableRow
                    'Get the New Column Objects and assign the values And Add to row.
                    'Set the image strng for graph.
                    strImage = "<img src='../images/bottom_line.gif' width='" & dtPredefinedReports.Rows(intPReportsCount).Item("Percentage") * 5 & "' height='20' > "

                    ' NAME
                    tcColName = New TableCell
                    tcColName.CssClass = "text"
                    tcColName.Controls.Add(New LiteralControl(dtPredefinedReports.Rows(intPReportsCount).Item("ConclusionBasis")))
                    tcColName.HorizontalAlign = HorizontalAlign.Right
                    'tcColName.Width="40%"
                    tcColName.Height.Point(20)
                    tcColName.VerticalAlign = VerticalAlign.Bottom
                    trRow.Cells.Add(tcColName)

                    ' VALUE
                    tcColValue = New TableCell
                    tcColValue.CssClass = "text"
                    tcColValue.Controls.Add(New LiteralControl(strImage & "&nbsp; " & Format(dtPredefinedReports.Rows(intPReportsCount).Item("Percentage"), "#,###.00") & "%"))
                    tcColValue.HorizontalAlign = HorizontalAlign.Left
                    trRow.Cells.Add(tcColValue)

                    'Add the row to the table
                    tblData.Rows.Add(trRow)
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace