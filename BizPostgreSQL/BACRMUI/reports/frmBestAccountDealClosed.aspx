<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBestAccountDealClosed.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmBestAccountDealClosed" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
    <title>Best Accounts Closed Deals</title>
		
		<script language="JavaScript" type="text/javascript" src="../javascript/date-picker.js">
		</script>
		<script language="javascript" type="text/javascript" >
		function OpenSelTeam(repNo)
		{
		
			window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function OpenTerritory(repNo)
		{
			window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function PopupCheck()
		{
		    document.frmBestAccounts.btnGo.click()
		}
		function Close()
		{
			window.close()
			return false;
		}
		</script>
</head>
<body>
   <form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		<table>
                <td width="100%"></td>
                <td  align="center">
		           <asp:UpdateProgress ID ="up1"  runat="server" DynamicLayout="false">
				        <ProgressTemplate>
				         <asp:Image ID="Image1"  ImageUrl="~/images/updating.gif" runat="server"/>
				         </ProgressTemplate>
    			    </asp:UpdateProgress>
    		    </td>
	    </table>
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>

			<br>
			<br>
		
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
			<tr>
			<td colspan=2 align=center class=normal1>
			No of records : <asp:label id="lblTotalRecord" runat="server"></asp:label>
			</td>
			<td align="right" class=normal1>
			<asp:button id="btnClose" Runat="server" Text="Close" Width="50" CssClass="button"></asp:button>
        	</td> 
			</tr>
			</table>
			<asp:table id="Table3" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
						BorderColor="black" GridLines="None" Height="350">
						<asp:TableRow>
							<asp:TableCell VerticalAlign="Top">
							
					
								<asp:datagrid id="dgOpportuntity" AllowSorting="True" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
									BorderColor="white">
									<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
									<ItemStyle CssClass="is"></ItemStyle>
									<HeaderStyle CssClass="hs"></HeaderStyle>
									<Columns>
										<asp:BoundColumn Visible="False" DataField="numOppId"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="numTerID" ></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="numRecOwner" ></asp:BoundColumn>
										<asp:BoundColumn  DataField="Name"   HeaderText="<font color=white>Name</font>" SortExpression="Name"></asp:BoundColumn>
										<asp:BoundColumn DataField="Company"  HeaderText="<font color=white>Vendor</font>" SortExpression="Company"></asp:BoundColumn>
										<asp:BoundColumn DataField="Contact"  HeaderText="<font color=white>Contact</font>" SortExpression="Contact"></asp:BoundColumn>
										<asp:BoundColumn DataField="STAGE"  HeaderText="<font color=white>Status</font>" SortExpression="STAGE"></asp:BoundColumn>
										<asp:TemplateColumn HeaderText="<font color=white>Due Date</font>" SortExpression="CloseDate">
											<ItemTemplate>
												<%# ReturnName(DataBinder.Eval(Container.DataItem, "CloseDate")) %>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="<font color=white>Amount</font>" SortExpression="monPAmount">
											<ItemTemplate>
												<%#ReturnMoney(DataBinder.Eval(Container.DataItem, "monPAmount"))%>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="UserName" HeaderText="<font color=white>Record Owner</font>" SortExpression="UserName" ></asp:BoundColumn>
										<asp:BoundColumn DataField="AssignedToBy"  HeaderText="<font color=white>Assigned To/By</font>" SortExpression="AssignedToBy"></asp:BoundColumn>
										</Columns>
									<PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
								</asp:datagrid>
								
							</asp:TableCell>
						</asp:TableRow>
					</asp:table>
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server"></asp:Literal></td>
				</tr>
			</table>
			<asp:TextBox ID="txtTotalPageSales" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtTotalRecordsSales" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtSortCharSales" Runat="server" style="DISPLAY:none"></asp:TextBox>
			</ContentTemplate>
			</asp:updatepanel>
			</form>
	</body>
</html>
