<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmForeCastReptItem.aspx.vb"
    MasterPageFile="~/common/GridMasterRegular.Master" Inherits="BACRM.UserInterface.Reports.frmForeCastReptItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>frmForeCastReptItem</title>
    <script language="javascript" type="text/javascript">
        function OpenForAmt(a) {
            // alert(a);
            window.open("../Forecasting/frmUpdateForAmt.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ForId=" + a, '', 'toolbar=no,titlebar=no,left=800, top=300,width=175,height=40,scrollbars=no,resizable=yes')
        }

        function OpenSelTeam() {
            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=20", '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=20", '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click()
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <table cellspacing="2" cellpadding="2">
        <tr>
            <td class="normal1" align="right">
                Forecasting Report for
            </td>
            <td>
                &nbsp;
                <asp:DropDownList ID="ddlYear" CssClass="signup" runat="server" Width="60">
                </asp:DropDownList>
            </td>
            <td class="normal1" align="right">
                of Quarter
            </td>
            <td>
                &nbsp;
                <asp:DropDownList ID="ddlQuarter" CssClass="signup" runat="server" Width="130">
                    <asp:ListItem Value="0" Selected="True">--Select One--</asp:ListItem>
                    <asp:ListItem Value="1">First Quarter</asp:ListItem>
                    <asp:ListItem Value="2">Second Quarter</asp:ListItem>
                    <asp:ListItem Value="3">Third Quarter</asp:ListItem>
                    <asp:ListItem Value="4">Fourth Quarter</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="normal1">
                Item &nbsp;
                <asp:DropDownList ID="ddlItem" runat="server" Width="130" CssClass="signup">
                </asp:DropDownList>
                &nbsp;
                <asp:Button ID="btnShow" Text="Go" CssClass="button" runat="server"></asp:Button>
            </td>
            <td class="text_bold" align="right">
                Total Forecast :
            </td>
            <td valign="top" width="100">
                &nbsp;<asp:Label ID="lblForecastamt" CssClass="text" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table cellspacing="2" cellpadding="2" width="100%">
        <tr>
            <td align="center" class="normal1">
                <asp:CheckBox ID="chkShowEmpFor" Checked="true" AutoPostBack="true" runat="server"
                    Text="Show Employee Forecasts" />
                <asp:CheckBox ID="chkShowPartFor" runat="server" AutoPostBack="true" Text="Show Partner Forecasts" />
            </td>
            <td align="right">
                <asp:Button ID="btnAddtoMyRtpList" runat="server" CssClass="button" Text="Add To My Reports">
                </asp:Button>
                <asp:Button ID="btnTeams" Text="Choose Teams" CssClass="button" runat="server"></asp:Button>
                <asp:Button ID="btnTer" Text="Choose Territory" CssClass="button" runat="server">
                </asp:Button>
                <asp:Button ID="btnExcel" Text="Export to Excel" CssClass="button" runat="server">
                </asp:Button>
                <asp:Button ID="btnCancel" Text="Back" CssClass="button" runat="server"></asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Forecasting report (Item)
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table id="tblDetails" class="dg" cellspacing="1" cellpadding="1" width="100%" border="0"
        runat="server">
        <tr class="hs">
            <td class="normalbol" width="10%">
                Month
            </td>
            <td class="normalbol" width="10%">
                Quota
            </td>
            <td class="normalbol" width="10%">
                Sold
            </td>
            <td class="normalbol" width="10%">
                Opp Pipeline
            </td>
            <td class="normalbol" width="20%">
                Opp Probability %
            </td>
            <td class="normalbol" width="20%">
                Opp Probable Quantity
            </td>
            <td class="normalbol" width="20%">
                Forecast Quantity
            </td>
        </tr>
    </table>
    <asp:Button ID="btnGo" runat="server" Style="display: none"></asp:Button>
</asp:Content>
