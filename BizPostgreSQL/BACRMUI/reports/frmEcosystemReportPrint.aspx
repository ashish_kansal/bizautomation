<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmEcosystemReportPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmEcosystemReportPrint"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>frmEcosystemReportPrint</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	
		
		<SCRIPT language="JavaScript" type="text/javascript" src="../javascript/date-picker.js">
		</SCRIPT>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						<asp:Label ID="lblReportHeader" Runat="server">Ecosystem Report</asp:Label>
					</td>
				</tr>
				<tr>
				</tr>
			</table>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<BR>
			<asp:DataGrid ID="dgEcosystemReport" CssClass="dg" Width="100%" Runat="server" BorderColor="white"
				AutoGenerateColumns="False" AllowSorting="True">
				<AlternatingItemStyle CssClass="is"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="Entity1" SortExpression="Entity1" HeaderText="<font color=white>Entity,Division</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="SelectedData1" SortExpression="SelectedData1" HeaderText="<font color=white>Selected Field Data</font>"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Association">
						<ItemTemplate>
							<asp:Label ID="lblAssociation" Runat="server">To</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="Entity2" SortExpression="Entity2" HeaderText="<font color=white>Entity 2,Division</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="SelectedData2" SortExpression="SelectedData2" HeaderText="<font color=white>Selected Field Data</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="Type" SortExpression="AsitsType" HeaderText="<font color=white>''As Its'' Type</font>"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			<script language="JavaScript" type="text/javascript">
			function Lfprintcheck()
			{
				this.print()
				//history.back()
				document.location.href = "frmEcosystemReport.aspx";
			}
			</script>
		</form>
	</body>
</HTML>
