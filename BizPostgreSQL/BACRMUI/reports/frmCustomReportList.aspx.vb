﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports

Public Class frmCustomReportList
    Inherits BACRMPage

    Dim reportName As String = ""
    Dim reportType As Integer = -1
    Dim perspective As Integer = -1
    Dim timeline As String = ""
    Dim reportModule As Long = 0
    Dim objReportManage As New CustomReportsManage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then
                LoadDropDowns()

                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                    txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                    txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                    txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                    reportName = CCommon.ToString(PersistTable("ReportNameFilter"))
                    reportType = CCommon.ToInteger(PersistTable("ReportTypeFilter"))
                    perspective = CCommon.ToInteger(PersistTable("PerspectiveFilter"))
                    timeline = CCommon.ToString(PersistTable("TimelineFilter"))
                    reportModule = CCommon.ToLong(PersistTable("ReportModuleFilter"))
                    'If ddlModule.Items.FindByValue(CCommon.ToString(PersistTable(ddlModule.ID))) IsNot Nothing Then
                    '    ddlModule.ClearSelection()
                    '    ddlModule.Items.FindByValue(CCommon.ToString(PersistTable(ddlModule.ID))).Selected = True
                    'End If
                End If

                If (CCommon.ToInteger(txtCurrrentPage.Text) = 0) Then
                    txtCurrrentPage.Text = "1"
                End If

                BindDataGrid()
                BindDashboardTemplate()

                'KEEP PERMISSION CHECK AT LAST
                Dim m_aryRightsForNewCustomReport() As Integer
                m_aryRightsForNewCustomReport = GetUserRightsForPage_Other(MODULEID.Dashboard, 2)
                If m_aryRightsForNewCustomReport(RIGHTSTYPE.VIEW) = 0 Then
                    btnNewReport.Visible = False
                End If

                Dim m_aryRightsForAllowedReports() As Integer
                m_aryRightsForAllowedReports = GetUserRightsForPage_Other(MODULEID.Dashboard, 3)
                If m_aryRightsForAllowedReports(RIGHTSTYPE.VIEW) = 0 Then
                    btnAllowList.Visible = False
                End If

                Dim m_aryRightsForKPIReports() As Integer
                m_aryRightsForKPIReports = GetUserRightsForPage_Other(MODULEID.Dashboard, 4)
                If m_aryRightsForKPIReports(RIGHTSTYPE.VIEW) = 0 Then
                    btnKPIGroups.Visible = False
                End If

                Dim m_aryRightsForDeleteTemplate() As Integer
                m_aryRightsForDeleteTemplate = GetUserRightsForPage_Other(MODULEID.Dashboard, 5)
                If m_aryRightsForDeleteTemplate(RIGHTSTYPE.VIEW) = 0 Then
                    gvDashboardTemplate.Columns(1).Visible = False
                End If
            Else
                'Dim m_aryRightsForDeleteTemplate() As Integer
                'm_aryRightsForDeleteTemplate = GetUserRightsForPage_Other(MODULEID.Dashboard, 2)
                'If m_aryRightsForDeleteTemplate(RIGHTSTYPE.VIEW) = 0 Then
                '    gvDashboardTemplate.Columns(1).Visible = False
                'End If
                Dim m_aryRightsForNewCustomReport() As Integer
                m_aryRightsForNewCustomReport = GetUserRightsForPage_Other(MODULEID.Dashboard, 2)
                If m_aryRightsForNewCustomReport(RIGHTSTYPE.VIEW) = 0 Then
                    btnNewReport.Visible = False
                End If
                If hdnSelectedTab.Value = "1" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "FocusDashboard", " $('.nav-tabs a[href=""#tabReportList""]').tab('show');", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "FocusDashboard", " $('.nav-tabs a[href=""#tabDashboardTemplates""]').tab('show');", True)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub LoadDropDowns()
        Try
            Dim ds As DataSet
            objReportManage = New CustomReportsManage
            ds = objReportManage.GetReportModuleANDGroupMaster

            Dim dtModuleList As DataTable
            dtModuleList = ds.Tables(0)

            'ddlModule.DataSource = dtModuleList
            'ddlModule.DataTextField = "vcModuleName"
            'ddlModule.DataValueField = "numReportModuleID"
            'ddlModule.DataBind()

            'ddlModule.Items.Insert(0, New ListItem("--All--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindDataGrid()
        Try
            If Not gvReport.HeaderRow Is Nothing Then
                If Not gvReport.HeaderRow.FindControl("txtReportName") Is Nothing Then
                    reportName = DirectCast(gvReport.HeaderRow.FindControl("txtReportName"), TextBox).Text
                End If
                If Not gvReport.HeaderRow.FindControl("ddlReportType") Is Nothing Then
                    reportType = CCommon.ToInteger(DirectCast(gvReport.HeaderRow.FindControl("ddlReportType"), DropDownList).SelectedValue)
                End If
                If Not gvReport.HeaderRow.FindControl("ddlPerspective") Is Nothing Then
                    perspective = CCommon.ToInteger(DirectCast(gvReport.HeaderRow.FindControl("ddlPerspective"), DropDownList).SelectedValue)
                End If
                If Not gvReport.HeaderRow.FindControl("ddlTimeline") Is Nothing Then
                    timeline = DirectCast(gvReport.HeaderRow.FindControl("ddlTimeline"), DropDownList).SelectedValue
                End If
                If Not gvReport.HeaderRow.FindControl("ddlModule") Is Nothing Then
                    reportModule = DirectCast(gvReport.HeaderRow.FindControl("ddlModule"), DropDownList).SelectedValue
                End If
            End If

            Dim dtTable As DataTable
            With objReportManage
                .DomainID = Session("DomainId")
                .UserCntID = Session("UserContactID")
                .SortCharacter = txtSortChar.Text.Trim()

                If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                .CurrentPage = txtCurrrentPage.Text.Trim()
                objReportManage.PageSize = Session("PagingRows")
                objReportManage.TotalRecords = 0

                If txtSortColumn.Text <> "" Then
                    .columnName = txtSortColumn.Text.Trim()
                Else : .columnName = "numReportID"
                End If

                If txtSortOrder.Text = "D" Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If

                objReportManage.ReportModuleID = reportModule
                .ReportName = reportName
                .tintReportType = reportType
                .Perspective = perspective
                .Timeline = timeline
            End With

            dtTable = objReportManage.GetReportListMaster()

            PersistTable.Clear()
            PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
            PersistTable.Add(PersistKey.CurrentPage, IIf(dtTable.Rows.Count > 0, txtCurrrentPage.Text, "1"))
            PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
            PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
            PersistTable.Add("ReportNameFilter", reportName)
            PersistTable.Add("ReportTypeFilter", reportType)
            PersistTable.Add("PerspectiveFilter", perspective)
            PersistTable.Add("TimelineFilter", timeline)
            PersistTable.Add("ReportModuleFilter", reportModule)

            PersistTable.Save()

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objReportManage.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text

            gvReport.DataSource = dtTable
            gvReport.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindDashboardTemplate()
        Try
            Dim objDashboardTemplate As New DashboardTemplate
            objDashboardTemplate.DomainID = CCommon.ToLong(Session("DomainId"))
            gvDashboardTemplate.DataSource = objDashboardTemplate.GetAllByDomain()
            gvDashboardTemplate.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnGo1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            If Not gvReport.HeaderRow Is Nothing Then
                If Not gvReport.HeaderRow.FindControl("txtReportName") Is Nothing Then
                    DirectCast(gvReport.HeaderRow.FindControl("txtReportName"), TextBox).Text = ""
                End If
                If Not gvReport.HeaderRow.FindControl("ddlReportType") Is Nothing Then
                    DirectCast(gvReport.HeaderRow.FindControl("ddlReportType"), DropDownList).SelectedValue = "-1"
                End If
                If Not gvReport.HeaderRow.FindControl("ddlPerspective") Is Nothing Then
                    DirectCast(gvReport.HeaderRow.FindControl("ddlPerspective"), DropDownList).SelectedValue = "-1"
                End If
                If Not gvReport.HeaderRow.FindControl("ddlTimeline") Is Nothing Then
                    DirectCast(gvReport.HeaderRow.FindControl("ddlTimeline"), DropDownList).SelectedValue = "-1"
                End If
                If Not gvReport.HeaderRow.FindControl("ddlModule") Is Nothing Then
                    DirectCast(gvReport.HeaderRow.FindControl("ddlModule"), DropDownList).SelectedValue = "0"
                End If
            End If
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvReport_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvReport.RowCommand
        Try
            If e.CommandName = "DeleteReport" Then
                Dim gvRow As GridViewRow
                gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                objReportManage.DomainID = Session("DomainId")
                objReportManage.ReportID = gvReport.DataKeys(gvRow.DataItemIndex).Value
                objReportManage.DeleteReportListMaster()

                txtCurrrentPage.Text = 1

                BindDataGrid()
            ElseIf e.CommandName = "AddToMyReport" Then
                Dim gvRow As GridViewRow
                gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                objReportManage.ReportID = gvReport.DataKeys(gvRow.DataItemIndex).Value
                objReportManage.UserCntID = Session("UserContactId")
                objReportManage.DomainID = Session("DomainId")
                objReportManage.tintReportType = 1
                objReportManage.ManageUserReportList()
            ElseIf e.CommandName = "DuplicateReport" Then
                Dim gvRow As GridViewRow
                gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                With objReportManage
                    .ReportID = gvReport.DataKeys(gvRow.DataItemIndex).Value
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .ManageReportListMaster(bitDuplicate:=True)
                End With

                Response.Redirect("../reports/frmReportStep2.aspx?ReptID=" & objReportManage.ReportID)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvReport_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvReport.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                If Not e.Row.FindControl("txtReportName") Is Nothing Then
                    DirectCast(e.Row.FindControl("txtReportName"), TextBox).Text = reportName
                End If
                If Not e.Row.FindControl("ddlReportType") Is Nothing Then
                    DirectCast(e.Row.FindControl("ddlReportType"), DropDownList).SelectedValue = reportType
                End If
                If Not e.Row.FindControl("ddlPerspective") Is Nothing Then
                    DirectCast(e.Row.FindControl("ddlPerspective"), DropDownList).SelectedValue = perspective
                End If
                If Not e.Row.FindControl("ddlTimeline") Is Nothing Then
                    DirectCast(e.Row.FindControl("ddlTimeline"), DropDownList).SelectedValue = timeline
                End If

                If Not e.Row.FindControl("ddlModule") Is Nothing Then
                    Dim ds As DataSet
                    objReportManage = New CustomReportsManage
                    ds = objReportManage.GetReportModuleANDGroupMaster

                    Dim dtModuleList As DataTable
                    dtModuleList = ds.Tables(0)
                    Dim ddlModule As DropDownList = DirectCast(e.Row.FindControl("ddlModule"), DropDownList)
                    ddlModule.DataSource = dtModuleList
                    ddlModule.DataTextField = "vcModuleName"
                    ddlModule.DataValueField = "numReportModuleID"
                    ddlModule.DataBind()
                    ddlModule.Items.Insert(0, New ListItem("--All--", "0"))

                    ddlModule.SelectedValue = reportModule
                End If
            End If

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim hplEdit As HyperLink = e.Row.FindControl("hplEdit")
                hplEdit.NavigateUrl = "../reports/frmReportStep2.aspx?ReptID=" & DataBinder.Eval(e.Row.DataItem, "numReportID")

                Dim hplRun As HyperLink = e.Row.FindControl("hplRun")
                hplRun.NavigateUrl = "../reports/frmCustomReportRun.aspx?ReptID=" & DataBinder.Eval(e.Row.DataItem, "numReportID")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
   
    Protected Sub txtReportName_TextChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = 1
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = 1
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlPerspective_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = 1
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlTimeline_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = 1
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlModule_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = 1
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvDashboardTemplate_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvDashboardTemplate.RowCommand
        Try
            If e.CommandName = "DeleteTemplate" Then
                Dim gvRow As GridViewRow
                gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                Dim objDashboardTemplate As New DashboardTemplate
                objDashboardTemplate.DomainID = CCommon.ToLong(Session("DomainId"))
                objDashboardTemplate.TemplateID = CCommon.ToLong(gvDashboardTemplate.DataKeys(gvRow.DataItemIndex).Value)
                objDashboardTemplate.Delete()

                txtCurrrentPage.Text = 1
                BindDashboardTemplate()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class