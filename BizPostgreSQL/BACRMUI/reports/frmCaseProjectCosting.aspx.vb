Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Partial Public Class frmCaseProjectCosting : Inherits BACRMPage

       
        Dim SortField As String

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    
                    
                    GetUserRightsForPage(8, 72)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                FillCustomer(ddlCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Public Function FillCustomer(ByVal ddlCombo As DropDownList)
            Try
                
                With objCommon
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .Filter = Trim(txtCompName.Text) & "%"
                    .UserCntID = Session("UserContactID")
                    ddlCombo.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                    ddlCombo.DataTextField = "vcCompanyname"
                    ddlCombo.DataValueField = "numdivisionId"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
            Try
                LoadPro()
                LoadCase()
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindDataGrid()
            Try
                Dim objPredefinedReports As New PredefinedReports
                Dim dtTable As DataTable
                objPredefinedReports.Division = ddlCompany.SelectedValue
                If radCase.Checked = True And ddlCase.SelectedValue > 0 Then
                    objPredefinedReports.CaseID = ddlCase.SelectedValue
                Else : objPredefinedReports.CaseID = 0
                End If
                If radPro.Checked = True And ddlPro.SelectedValue > 0 Then
                    objPredefinedReports.ProID = ddlPro.SelectedValue
                Else : objPredefinedReports.ProID = 0
                End If
                ' objPredefinedReports.ProID = ddlPro.SelectedValue
                objPredefinedReports.DomainID = Session("DomainId")
                dtTable = objPredefinedReports.GetCaseProjectCosting()
                If dtTable.Rows.Count > 0 Then
                    Dim amountbiled As Integer = CType(dtTable.Compute("SUM(AmountBilled)", ""), Double)
                    Dim labourrate As Integer = CType(dtTable.Compute("SUM(labourrate)", ""), Double)
                    Dim cost As Double = amountbiled - labourrate
                    If cost > 0 Then
                        lblProfit.CssClass = "normal7"
                        lblProfit.Text = cost.ToString
                    Else
                        lblProfit.CssClass = "normal12"
                        lblProfit.Text = "( " & cost.ToString & " )"
                    End If
                Else : lblProfit.Text = ""
                End If
                Dim dv As DataView = New DataView(dtTable)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgCosting.DataSource = dv
                dgCosting.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadPro()
            Try
                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.DivisionID = ddlCompany.SelectedItem.Value()
                objTimeExp.DomainID = Session("DomainId")
                If radHistory.Checked = True Then
                    objTimeExp.byteMode = 1
                Else : objTimeExp.byteMode = 0
                End If
                ddlPro.DataSource = objTimeExp.GetProByDivID
                ddlPro.DataTextField = "vcProjectName"
                ddlPro.DataValueField = "numProid"
                ddlPro.DataBind()
                ddlPro.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadCase()
            Try
                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.DivisionID = ddlCompany.SelectedItem.Value()
                If radHistory.Checked = True Then
                    objTimeExp.byteMode = 1
                Else : objTimeExp.byteMode = 0
                End If
                objTimeExp.DomainID = Session("DomainId")
                ddlCase.DataSource = objTimeExp.GetCaseByDivID
                ddlCase.DataTextField = "vcCaseNumber"
                ddlCase.DataValueField = "numCaseId"
                ddlCase.DataBind()
                ddlCase.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgCosting_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgCosting.SortCommand
            Try
                SortField = e.SortExpression
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlCase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCase.SelectedIndexChanged
            Try
                If ddlCompany.SelectedValue > 0 And ddlCase.SelectedValue > 0 Then
                    radCase.Checked = True
                    radPro.Checked = False
                    ddlPro.SelectedIndex = 0
                    BindDataGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlPro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPro.SelectedIndexChanged
            Try
                If ddlCompany.SelectedValue > 0 And ddlPro.SelectedValue > 0 Then
                    radPro.Checked = True
                    radCase.Checked = False
                    ddlCase.SelectedIndex = 0
                    BindDataGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                ExportToExcel.DataGridToExcel(dgCosting, Response)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub radHistory_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radHistory.CheckedChanged
            Try
                LoadPro()
                LoadCase()
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Dim DivId As Long = 0
                Dim CaseId As Long = 0
                Dim ProID As Long = 0
                Dim DivName As String = ""
                Dim CaseName As String = ""
                Dim ProName As String = ""
                If ddlCompany.Items.Count > 0 Then
                    DivId = ddlCompany.SelectedValue
                    DivName = ddlCompany.SelectedItem.Text
                End If

                If radCase.Checked = True And ddlCase.SelectedValue > 0 Then
                    CaseId = ddlCase.SelectedValue
                    CaseName = ddlCase.SelectedItem.Text
                Else : CaseId = 0
                End If
                If radPro.Checked = True And ddlPro.SelectedValue > 0 Then
                    ProID = ddlPro.SelectedValue
                    ProName = ddlPro.SelectedItem.Text
                Else : ProID = 0
                End If
                Response.Redirect("../Reports/frmCaseProjectCostingPrint.aspx?Divid=" & DivId & "&CaseId=" & CaseId & "&ProId=" & ProID & "&DivName=" & DivName & "&CaseName=" & CaseName & "&ProName = " & ProName)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 50
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace