Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class frmSelectReportFlds : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents hdnValue As System.Web.UI.WebControls.TextBox
        Protected WithEvents lstFielAvail As System.Web.UI.WebControls.ListBox
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents btnRemove As System.Web.UI.WebControls.Button
        Protected WithEvents lstFldAdded As System.Web.UI.WebControls.ListBox
        Protected WithEvents tbl As System.Web.UI.WebControls.Table

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    Dim objReports As New PredefinedReports
                    Dim dtAvailableFields As New DataTable
                    objReports.byteMode = 0
                    dtAvailableFields = objReports.GetAvailablefields
                    lstFielAvail.DataSource = dtAvailableFields
                    lstFielAvail.DataTextField = "vcFieldName"
                    lstFielAvail.DataValueField = "numRepFieldID"
                    lstFielAvail.DataBind()

                    Dim dtAddedFields As New DataTable
                    objReports.byteMode = 1
                    dtAddedFields = objReports.GetAvailablefields
                    lstFldAdded.DataSource = dtAddedFields
                    lstFldAdded.DataTextField = "vcFieldName"
                    lstFldAdded.DataValueField = "numRepFieldID"
                    lstFldAdded.DataBind()
                    
                End If
                btnAdd.Attributes.Add("OnClick", "return move(document.Form1.lstFielAvail,document.Form1.lstFldAdded)")
                btnRemove.Attributes.Add("OnClick", "return remove(document.Form1.lstFldAdded,document.Form1.lstFielAvail)")
                btnSave.Attributes.Add("onclick", "Save()")
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objRep As New PredefinedReports
                objRep.strLinks = hdnValue.Text
                objRep.ManageReportFields()
                Response.Write("<script>opener.PopupCheck(); self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace

