Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Partial Public Class frmTimeExpPrint : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblreportby.Text = Session("UserName")
                lblcurrent.Text = FormattedDateFromDate(Now(), Session("DateFormat"))
                DisplayRecords()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayRecords()
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = GetQueryStringVal( "fdt")
                objPredefinedReports.ToDate = GetQueryStringVal( "tdt")
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.byteMode = GetQueryStringVal( "ReportType")
                objPredefinedReports.ReportType = 24
                dgContactRole.DataSource = objPredefinedReports.GetTimeAndExp
                dgContactRole.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace