<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmCustomReportConfigSaver.aspx.vb" Inherits="frmCustomReportConfigSaver" ValidateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>Repository of Custom Report Configuration</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<SCRIPT language="JavaScript" src="../javascript/CustomReports.js" type="text/javascript"></SCRIPT>
	</HEAD>
	<body >
		<form id="frmCustomReportConfigSaver" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		<table>
                <td width="100%"></td>
                <td  align="center">
		           <asp:UpdateProgress ID ="up1"  runat="server" DynamicLayout="false">
				        <ProgressTemplate>
				         <asp:Image ID="Image1"  ImageUrl="~/images/updating.gif" runat="server"/>
				         </ProgressTemplate>
    			    </asp:UpdateProgress>
    		    </td>
	    </table>
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>

			<input type="hidden" id="hdCustomReportConfig" runat="server" NAME="hdCustomReportConfig">
			<asp:Button id="btnOpenReport" runat="server" style="visibility:hidden;" Text="Open Report"></asp:Button>
			<asp:literal id="litClientMessage" Runat="server" EnableViewState="False"></asp:literal>
			</ContentTemplate>
			</asp:updatepanel>

		</form>
	</body>
</HTML>
