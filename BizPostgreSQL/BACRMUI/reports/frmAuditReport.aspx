﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmAuditReport.aspx.vb" Inherits=".frmAuditReport1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .tblDataGrid tr:first-child td {
            background: #e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
<asp:Label ID="lblTitle" runat="server">Allocation / On-Order Imbalance</asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="table-responsive">
<asp:GridView ID="gvAuditReport" AutoGenerateColumns="False" runat="server" Width="100%" CssClass="table table-responsive table-bordered tblDataGrid" DataKeyNames="">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <Columns>
            <asp:BoundField DataField="vcItemName" HeaderText="Item Name"></asp:BoundField>
            <asp:BoundField DataField="vcWareHouse" HeaderText="Warehouse"></asp:BoundField>
            <asp:BoundField DataField="numAllocation" HeaderText="Allocation"></asp:BoundField>
            <asp:BoundField DataField="numBackOrder" HeaderText="Back Order">
               <ItemStyle width="70px"/>
            </asp:BoundField>
            <asp:BoundField DataField="WTotal" HeaderText="WTotal"></asp:BoundField>
            <asp:BoundField DataField="SOQty" HeaderText="SOQty"></asp:BoundField>
        </Columns>
        <HeaderStyle CssClass="hs"></HeaderStyle>
    </asp:GridView>
        </div>
</asp:Content>
