Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Imports System.Reflection

Namespace BACRM.UserInterface.Reports
    Public Class frmItemsReport : Inherits BACRMPage

        Dim objPredefinedReports As New PredefinedReports
        Dim dtPredefinedReports As New DataTable
        Dim SortField As String
       

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                Dim intNode As Integer

                intNode = GetQueryStringVal("a")

                If Not IsPostBack Then

                    GetUserRightsForPage(8, 61)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    ' = "Reports"

                    objCommon.DomainID = Session("DomainID")
                    objCommon.UserCntID = Session("UserContactID")


                    objCommon.GetCompanySpecificValues1()
                    Dim strCompany, strContactID As String
                    strCompany = objCommon.GetCompanyName
                    strContactID = objCommon.ContactID
                    radCmbCompany.Text = strCompany
                    radCmbCompany.SelectedValue = IIf(objCommon.DivisionID > 0, objCommon.DivisionID, "")

                    ' radCmbCompany_SelectedIndexChanged()


                End If
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub DisplayRecords()
            Try
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
                objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.Division = IIf(radCmbCompany.SelectedValue = "", 0, radCmbCompany.SelectedValue)
                objPredefinedReports.SortOrder = ddlSortBy.SelectedItem.Value
                dtPredefinedReports = objPredefinedReports.GetRepItems
                Dim dv As DataView = New DataView(dtPredefinedReports)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgItems.DataSource = dv
                dgItems.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        '    Dim strPath As String = Server.MapPath("../documents/docs")
        '    DisplayRecords()
        '    objPredefinedReports.ExportToExcel(dtPredefinedReports.DataSet, Server.MapPath(""), strPath, Session("UserName"), Session("UserID"), "Items", Session("DateFormat"))

        '    Response.Write("<script language=javascript>")
        '    Response.Write("alert('Exported Successfully !')")
        '    Response.Write("</script>")
        '    Dim strref As String = "../Documents/docs/" & Session("UserName") & "_" & Session("UserID") & "_Items.csv"

        '    Response.Write("<script language=javascript>")
        '    Response.Write("window.open('" & strref & "')")
        '    Response.Write("</script>")
        'End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 38
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItems.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    e.Item.Cells(8).Attributes.Add("onclick", "return OpenItemDetails('" & e.Item.Cells(0).Text & "','" & "1" & "')")
                    e.Item.Cells(9).Attributes.Add("onclick", "return OpenItemDetails('" & e.Item.Cells(0).Text & "','" & "2" & "')")
                    e.Item.Cells(10).Attributes.Add("onclick", "return OpenItemDetails('" & e.Item.Cells(0).Text & "','" & "3" & "')")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgItems_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgItems.SortCommand
            Try
                SortField = e.SortExpression
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            ExportToExcel.DataGridToExcel(dgItems, Response)
        End Sub

        'Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        '    Try
        '        Server.Transfer("../reports/frmItemsReptPrint.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&fdt=" & calFrom.SelectedDate & "&tdt=" & DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) & "&Sort=" & SortField)
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            DisplayRecords()
        End Sub
        
        Private Sub ddlSortBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSortBy.SelectedIndexChanged
            DisplayRecords()
        End Sub
    End Class
End Namespace
