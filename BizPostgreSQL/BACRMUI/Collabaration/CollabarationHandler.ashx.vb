﻿Imports System.IO
Imports System.Web
Imports System.Web.Services
Imports BACRM.BusinessLogic
Imports BACRM.BusinessLogic.Common

Public Class CollabarationHandler
        Implements System.Web.IHttpHandler

        Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim objCCollabaration = New BACRM.BusinessLogic.CCollabaration.CCollabaration()
        Dim output As String = "1"
        objCCollabaration.numTopicId = context.Request.Form("numTopicId")
        objCCollabaration.intRecordType = context.Request.Form("intRecordType")
        objCCollabaration.numRecordId = context.Request.Form("numRecordId")
        objCCollabaration.vcTopicTitle = context.Request.Form("vcTopicTitle")
        objCCollabaration.CreatedBy = context.Request.Form("CreatedBy")
        objCCollabaration.numUpdatedBy = context.Request.Form("numUpdatedBy")
        objCCollabaration.bitIsInternal = context.Request.Form("bitIsInternal")
        objCCollabaration.numDomainId = context.Request.Form("numDomainId")
        objCCollabaration.Status = context.Request.Form("Status")


        objCCollabaration.ManageTopicMaster()
        Dim files As HttpFileCollection = context.Request.Files

        For i As Integer = 0 To files.Count - 1
            Dim file As HttpPostedFile = files(i)
            Dim strFName As String()
            Dim strFilePath, strFileName, strFileType As String
            strFileName = Path.GetFileName(file.FileName)
            If Directory.Exists(CCommon.GetDocumentPhysicalPath(objCCollabaration.numDomainId)) = False Then ' If Folder Does not exists create New Folder.
                Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(objCCollabaration.numDomainId))
            End If
            Dim newFileName As String = Path.GetFileNameWithoutExtension(strFileName) & DateTime.UtcNow.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(strFileName)
            Dim strFileLogicalPath = String.Empty
            strFilePath = CCommon.GetDocumentPhysicalPath(objCCollabaration.numDomainId) & newFileName
            strFName = Split(strFileName, ".")
            strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
            file.SaveAs(strFilePath)
            objCCollabaration.vcAttachmentName = newFileName
            objCCollabaration.vcAttachmentUrl = strFilePath
            objCCollabaration.ManageTopicMessageAttachments()
        Next

        'Dim json As String = String.Empty
        context.Response.ContentType = "text/plain"
        context.Response.Write(objCCollabaration.numTopicId)

    End Sub

        ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property

End Class