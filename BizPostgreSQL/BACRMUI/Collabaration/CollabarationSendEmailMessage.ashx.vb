﻿Imports System.Web
Imports System.Web.Services
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects

Public Class CollabarationSendEmailMessage
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim objCCollabaration = New BACRM.BusinessLogic.CCollabaration.CCollabaration()
        Dim output As String = "1"
        objCCollabaration.numMessageId = context.Request.Form("numMessageId")
        objCCollabaration.numTopicId = context.Request.Form("numTopicId")
        objCCollabaration.numParentMessageId = context.Request.Form("numParentMessageId")
        objCCollabaration.intRecordType = context.Request.Form("intRecordType")
        objCCollabaration.numRecordId = context.Request.Form("numRecordId")
        objCCollabaration.vcMessage = context.Request.Form("vcMessage")
        objCCollabaration.CreatedBy = context.Request.Form("CreatedBy")
        objCCollabaration.numUpdatedBy = context.Request.Form("numUpdatedBy")
        objCCollabaration.bitIsInternal = context.Request.Form("bitIsInternal")
        objCCollabaration.numDomainId = context.Request.Form("numDomainId")
        objCCollabaration.Status = context.Request.Form("Status")
        Dim taskTitle As String = ""
        Dim taskCompanyName As String = ""
        Try

            Dim objType As Object
            Dim dtAssociatedContacts As New DataTable
            Select Case objCCollabaration.intRecordType
                Case 1
                    'objType = New CaseIP
                    'objType.DomainID = objCCollabaration.DomainID
                    'objType.ClientTimeZoneOffset = objCCollabaration.ClientMachineUTCTimeOffset
                    'dtAssociatedContacts = objType.AssCntsByCaseID
                    Dim objActionItem As New ActionItem
                    objActionItem.CommID = objCCollabaration.numRecordId
                    objActionItem.ClientTimeZoneOffset = objCCollabaration.ClientMachineUTCTimeOffset
                    dtAssociatedContacts = objActionItem.GetCommunicationAttendees.Tables(0)
                    Dim dtActDetails As DataTable
                    objActionItem = New ActionItem
                    objActionItem.CommID = CCommon.ToLong(objCCollabaration.numRecordId)
                    dtActDetails = objActionItem.GetActionItemDetails
                    If (dtActDetails.Rows.Count > 0) Then
                        taskTitle = dtActDetails.Rows(0).Item("textDetails")
                    End If
                    Dim objCollabarationCommon As New CCommon
                    objCollabarationCommon.CommID = CCommon.ToLong(objCCollabaration.numRecordId)
                    objCollabarationCommon.charModule = "A"
                    objCollabarationCommon.PhoneExt = ""
                    objCollabarationCommon.CustomerRelation = ""
                    objCollabarationCommon.GetCompanySpecificValues1()
                    taskCompanyName = objCollabarationCommon.CompanyName
                Case 2
                    objType = New CaseIP
                    objType.DomainID = objCCollabaration.numDomainId
                    objType.CaseID = objCCollabaration.numRecordId
                    objType.ClientTimeZoneOffset = objCCollabaration.ClientMachineUTCTimeOffset
                    dtAssociatedContacts = objType.AssCntsByCaseID
                    objType.GetCaseDetails()
                    Dim dtCaseDetails As DataTable
                    dtCaseDetails = objType.GetCaseDTL
                    taskTitle = CCommon.ToString(objType.Subject)
                    taskCompanyName = CCommon.ToString(dtCaseDetails.Rows(0).Item("vcCompanyName"))
                Case 3
                    objType = New OppotunitiesIP
                    objType.DomainID = objCCollabaration.numDomainId
                    objType.OpportunityId = objCCollabaration.numRecordId
                    objType.ClientTimeZoneOffset = objCCollabaration.ClientMachineUTCTimeOffset
                    dtAssociatedContacts = objType.AssociatedbyOppID
                    Dim objOpportunityDetails As New OppotunitiesIP
                    objOpportunityDetails.OpportunityId = objCCollabaration.numRecordId
                    objOpportunityDetails.DomainID = objCCollabaration.numDomainId
                    objOpportunityDetails.ClientTimeZoneOffset = objCCollabaration.ClientMachineUTCTimeOffset
                    objOpportunityDetails.OpportunityDetails()
                    Dim dtDetails As New DataTable
                    dtDetails = objOpportunityDetails.OpportunityDTL.Tables(0)
                    taskTitle = CCommon.ToString(objOpportunityDetails.OpportunityName)
                    taskCompanyName = CCommon.ToString(dtDetails.Rows(0).Item("vcCompanyName"))
                Case 4
                    objType = New ProjectIP
                    Dim dtProjects As New DataTable
                    objType.DomainID = objCCollabaration.numDomainId
                    dtProjects = objType.GetProjectDetailsByTaskId(objCCollabaration.numRecordId)
                    Dim dtUserList As DataTable
                    Dim dtExternalUserList As DataTable
                    Dim objProject As New Project
                    objProject.DomainID = objCCollabaration.numDomainId
                    objProject.ProjectID = CCommon.ToLong(dtProjects.Rows(0)("numProId"))
                    taskTitle = CCommon.ToString(dtProjects.Rows(0)("vcProjectName"))
                    taskCompanyName = CCommon.ToString(dtProjects.Rows(0)("vcCompanyName"))
                    Dim dsTemp As DataSet = objProject.GetAssociatedContacts()
                    If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 Then
                        dtUserList = dsTemp.Tables(0)
                    End If
                    dtAssociatedContacts = dtUserList
            End Select
            Dim objCommon = New CCommon

            Dim objSendEmail As New Email
            Dim strSubject As String
            Dim strBody As String
            Dim strFromEmail As String
            Dim strToEmail As String

            strBody = "<div style='font-family: Arial;font-size:13px'>The following update from ##MessageOrganizationName## was posted by: ##MessagePostedBy## (##MessagePostedByEmail##)<br />" &
    "<div style='clear:both'></div><div style='margin: 0px !important;'><b style='color:#128bd2;float:left;margin-top: 13px;'>Topic: </b><div  style='float:left;padding-left:10px'> ##TopicContent##</div></div>" &
    "<div style='margin: 0px !important;clear:both'> <b style='color:#128bd2;'>Posted on: </b>##TopicPostedDate##, <b style='color:#128bd2;'>By: </b>##TopicCreatedBy##, ##TopicOrganizationName## (##TopicEmployeeDesignation##)</div><br/><div><b style='color:#af8100;float:left;margin-top:13px;'>Reply: </b><div style='float:left;padding-left:10px'>##MessageContent##</div></div>" &
    "<div  style='margin: 0px !important;clear:both'><b style='color:#af8100;'>Posted on: </b>##MessagePostedDate##,<b style='color:#af8100;'>By: </b>##MessagePostedBy##, ##MessageOrganizationName## (##MessageEmployeeDesignation##)</div><br/><br/>##CompanyLogo##"
            strBody = strBody & "<br/><br/><b>To respond: </b><a href='" + ConfigurationManager.AppSettings("HostedAppURL") + "/Collabaration/frmCollabaration.aspx?numMessageId=##MessageId##&numDomainId=##DomainId##&numContactId=##ContactId##'>click here<a/>"
            strBody = strBody & "<div  style='font-size:11px'><br/><br/><b>Powered by: </b><a href='https://www.bizautomation.com' target='_blank'>BizAutomation.com</a><br/><i style='color:#5a5757'>ONE system for your ENTIRE business - <a href='https://www.bizautomation.com' target='_blank'>Click here</a> to learn more.</i></div></div>"

            Dim dtDomainDetails As DataTable
            objCommon.DomainID = objCCollabaration.numDomainId
            dtDomainDetails = objCommon.GetDomainDetails()
            Dim strLogoFilePath As String
            Dim FilePath = context.Request.Form("vcBizDocFilePath")

            Dim dtMessageDetails As DataTable
            dtMessageDetails = objCCollabaration.GetMessageMaster()
            If dtMessageDetails.Rows.Count > 0 Then
                Dim strAttachmentName As String
                strAttachmentName = CCommon.ToString(dtMessageDetails.Rows(0)("vcAttachmentName"))
                If strAttachmentName <> "" Then
                    Dim attachmentArray As String()
                    attachmentArray = strAttachmentName.Split(",")
                    If attachmentArray.Count > 0 And attachmentArray(0).Contains("$@#") Then
                        objCCollabaration.vcAttachmentName = attachmentArray(0).Split("$@#")(0)
                    End If
                End If
            End If
            Dim strTempBody As String
            strSubject = taskTitle & " : " & "(" & taskCompanyName & ")"
            objCommon = New CCommon
            Dim ddlAttendeeUser As New DropDownList
            objCommon.sb_FillConEmpFromDBSel(ddlAttendeeUser, objCCollabaration.numDomainId, 0, 0)
            If objCCollabaration.bitIsInternal = True Then
                For Each dr As DataRow In dtAssociatedContacts.Rows
                    If (ddlAttendeeUser.Items.FindByValue(dr("numContactId")) Is Nothing) Then
                        dr.Delete()
                    End If
                Next
            End If
            dtAssociatedContacts.AcceptChanges()
            If dtAssociatedContacts.Rows.Count > 0 Then
                For Each dr As DataRow In dtAssociatedContacts.Rows
                    If dr("numContactId") <> objCCollabaration.CreatedBy Then
                        'If dr("Email") = "prasantapradhan9@gmail.com" Then
                        strTempBody = strBody
                        If CCommon.ToString(dtDomainDetails.Rows(0)("vcLogoForBizTheme")) <> "" Then
                            strLogoFilePath = FilePath & CCommon.ToString(dtDomainDetails.Rows(0)("vcLogoForBizTheme"))
                            strTempBody = strTempBody.Replace("##CompanyLogo##", "<img src='" + strLogoFilePath + "' style='max-height: 37px;width:auto' /> ")
                        Else
                            strLogoFilePath = ""
                            strTempBody = strTempBody.Replace("##CompanyLogo##", "")
                        End If
                        strTempBody = strTempBody.Replace("##MessageOrganizationName##", CCommon.ToString(dtMessageDetails.Rows(0)("CreatedByOrgName")))
                        strTempBody = strTempBody.Replace("##MessagePostedBy##", CCommon.ToString(dtMessageDetails.Rows(0)("CreatedBy")))
                        strTempBody = strTempBody.Replace("##MessagePostedByEmail##", CCommon.ToString(dtMessageDetails.Rows(0)("CreatedByEmail")))
                        strTempBody = strTempBody.Replace("##TopicPostedDate##", CCommon.ToSqlDate(dtMessageDetails.Rows(0)("TopicPostedDate")).ToString("MMM dd hh:mm tt"))
                        strTempBody = strTempBody.Replace("##TopicOrganizationName##", CCommon.ToString(dtMessageDetails.Rows(0)("TopicOrgName")))
                        strTempBody = strTempBody.Replace("##TopicEmployeeDesignation##", CCommon.ToString(dtMessageDetails.Rows(0)("TopicCreatedByDesignation")))
                        strTempBody = strTempBody.Replace("##TopicContent##", HttpUtility.UrlDecode(dtMessageDetails.Rows(0)("vcTopicTitle")))
                        strTempBody = strTempBody.Replace("##MessagePostedDate##", CCommon.ToSqlDate(dtMessageDetails.Rows(0)("dtmCreatedOn")).ToString("MMM dd hh:mm tt"))
                        strTempBody = strTempBody.Replace("##MessagePostedBy##", CCommon.ToString(dtMessageDetails.Rows(0)("CreatedBy")))
                        strTempBody = strTempBody.Replace("##MessageContent##", HttpUtility.UrlDecode(dtMessageDetails.Rows(0)("vcMessage")))
                        strTempBody = strTempBody.Replace("##MessageEmployeeDesignation##", CCommon.ToString(dtMessageDetails.Rows(0)("CreatedByDesignation")))
                        strTempBody = strTempBody.Replace("##TopicCreatedBy##", CCommon.ToString(dtMessageDetails.Rows(0)("TopicCreatedBy")))
                        strTempBody = strTempBody.Replace("##MessageId##", CCommon.ToString(dtMessageDetails.Rows(0)("numMessageId")))

                        strTempBody = strTempBody.Replace("##DomainId##", CCommon.ToString(objCCollabaration.numDomainId))
                        strTempBody = strTempBody.Replace("##ContactId##", CCommon.ToString(dr("numContactId")))

                        objCommon = New CCommon
                        Dim objContact = New CContacts
                        objCommon.ContactID = objCCollabaration.CreatedBy
                        strFromEmail = objCommon.GetContactsEmail()
                        strToEmail = dr("Email")
                        objSendEmail.SendCustomCollabarationEmail(strSubject, strTempBody, "", strFromEmail, strToEmail, objCCollabaration.vcAttachmentName, "", "", objCCollabaration.numDomainId)
                        'End If
                    End If
                Next
            End If

        Catch ex As Exception
            Dim strException As String
            strException = ex.ToString()
        End Try
        context.Response.ContentType = "text/plain"
        context.Response.Write("Hello World!")

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class