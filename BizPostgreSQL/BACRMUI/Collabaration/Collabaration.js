﻿
var topicArray = [];
var messageArray = [];
$(document).ready(function () {
    topicArray = [];
    messageArray = [];
    if ($("#hdnExternalAccess").val() == "1") {
        $("#divCollabarationInternalThread").css("display", "none");
    }
    if ($("#hdnRecordType").val() == 4 && $("#hdnExternalAccess").val() == "1") {
        getTopic();
    } else if ($("#hdnRecordType").val() != 4) {
        getTopic();
    }
})
function SaveTopicMessage() {
    if (tinyMCE.get('myTextareaCollabaration').getContent() == "") {
        alert("Please enter title");
        return false;
    }
    if ($("#hdnIsTopicMessage").val() == "0") {
        SaveTopic();
    } else {
        SaveMessage();
    }
}
function Collabaration_UplodedFiles() {
    //var fileName = $('#fileCollabarationUpload').val().split('\\')[$("#fileCollabarationUpload").val().split('\\').length - 1];
    $("#spnCollbarationFilePath").html("");
    var fileUpload = $("#fileCollabarationUpload").get(0);
    var files = fileUpload.files;
    for (var i = 0; i < files.length; i++) {
        $("#spnCollbarationFilePath").append(files[i].name+"<br/>");
    }
}
function Collbaration_hplOpenDocumentClick() {
    $("#fileCollabarationUpload").click();
}
function SaveTopic() {
    $("#UpdateProgressCollabaration").css("display", "block")
    var formData = new FormData();
    formData.append('numDomainId', $("#hdnDomainId").val());
    formData.append('numTopicId', $("#hdnTopicId").val());
    formData.append('intRecordType', $("#hdnRecordType").val());
    formData.append('numRecordId', $("#hdnRecordId").val());
    formData.append('vcTopicTitle', encodeURI(tinyMCE.get('myTextareaCollabaration').getContent()));
    formData.append('CreatedBy', $("#hdnLoggedInUser").val());
    formData.append('numUpdatedBy', $("#hdnLoggedInUser").val());
    formData.append('bitIsInternal', $("#chkInternal").is(":checked"));
    formData.append('vcBizDocFilePath', $("#hdnCollabarationPortalDocUrl").val());
    formData.append('Status', "");
    var fileUpload = $("#fileCollabarationUpload").get(0);
    var files = fileUpload.files;
    for (var i = 0; i < files.length; i++) {
        formData.append(i, files[i]);
    }
    //var dataParam = '{numDomainId:' + $("#hdnDomainId").val() + ',numTopicId:' + $("#hdnTopicId").val() + ',intRecordType:"' + $("#hdnRecordType").val() + '", numRecordId: "' + $("#hdnRecordId").val() + '", vcTopicTitle: "' + encodeURI(tinyMCE.get('myTextarea').getContent()) + '",CreatedBy: "' + $("#hdnLoggedInUser").val() + '",numUpdatedBy: "' + $("#hdnLoggedInUser").val() + '",bitIsInternal:'+ $("#chkInternal").is(":checked") + ',Status : ""}';
    $.ajax({
        type: 'POST',
        url: "../Collabaration/CollabarationHandler.ashx",
       // contentType: "application/json; charset=utf-8",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            if (parseFloat(response) >0) {
                $("#UpdateProgressCollabaration").css("display", "none");
                if (confirm("Would you like to broadcast your comment out to subscribed contacts ?")) {
                    sendEmail(formData, response,1);
                }
                getTopic();
                CancelTopicMessage();
            }
            //var Jresponse = $.parseJSON(response.d);
            //if (Jresponse == "1") {
            //    getTopic();
            //    CancelTopicMessage();
            //}
        },
        error: function (xhr, ajaxOptions, thrownError) {
            debugger;
            //do something
        }
    });
}
function getTopic() {
    var externalAccess = false;
    if ($("#hdnExternalAccess").val() == "1") {
        externalAccess = true;
    }
    var dataParam = '{numDomainID:' + $("#hdnDomainId").val() + ',intRecordType:' + $("#hdnRecordType").val() + ',numRecordId:' + $("#hdnRecordId").val() + ',IsExternalUser:' + externalAccess + '}';
    topicArray = [];
    $(".CollabarationContent").html('');
    $.ajax({
        type: 'POST',
        url: "../Collabaration/frmCollabaration.aspx/WebMethodGetTopicMasterFrmCollabaration",
        contentType: "application/json; charset=utf-8",
        data: dataParam,
        dataType: "json",
        success: function (response) {
            debugger;
            var topicContent = "";
            var Jresponse = $.parseJSON(response.d);
            var i = 0;
            if ($("#hdnRecordType").val() == 4) {
                $("#projectTask_" + $("#hdnRecordId").val() + "").text(Jresponse.length);
            }
            $.each(Jresponse, function (index, value) {
                topicArray.push(value);
                topicContent = "";
                var attachmentList = "";
                if (value.vcAttachmentName != "" && value.vcAttachmentName!=null) {
                    var attachmentArray = [];
                    attachmentArray = value.vcAttachmentName.split(',');
                    if (attachmentArray.length > 0) {
                        $(attachmentArray).each(function (index, data) {
                            var dataIdArray = data.split("$@#");
                            attachmentList = attachmentList + '<a target="_blank" href="' + $("#hdnCollabarationPortalDocUrl").val() + dataIdArray[0] + '" class="repliesHyperLinkDelete">' + dataIdArray[0] + '</a>&nbsp;<img class="deleteImageIcons" src="../images/Delete24.png" onclick="DeleteCollabarationDocumentOrLink(' + dataIdArray[1] + ','+value.numTopicId+',0);return false;" />&nbsp;&nbsp;';
                        });
                    }
                }
                var IsInternal = '';
                var CollapseIn = 'in';
                if (i > 0) {
                    CollapseIn = '';
                }
                i = i + 1;
                if (value.bitIsInternal == true) {
                    IsInternal = 'Internal';
                }
                topicContent = topicContent + '<div class="box-group boxForList" id="DropDownListaccordion' + value.numTopicId+'">';
                topicContent = topicContent + '<div class="panel box box-solid box-primary boxForCollabaration">';

                //#region Topic Header
                topicContent = topicContent + '<div class="box-header">';

                topicContent = topicContent + '<div class="col-md-5">'
                topicContent = topicContent + '<span class="box-title"><a data-toggle="collapse" href="#collapseTopic' + value.numTopicId + '" data-parent="#DropDownListaccordion' + value.numTopicId + '" aria-expanded="false" class="collapsed"><b>' + formattedDate(value.dtmCreatedOn) + '</b> ' + value.CreatedBy + ', ' + value.CreatedByOrganization + ' (' + value.CreatedByDesignation+')</a></span>'
                topicContent = topicContent + '</div>'

                topicContent = topicContent + '<div class="col-md-3">'
                topicContent = topicContent + '<span class="box-title"><a data-toggle="collapse" href="#collapseTopic' + value.numTopicId + '" data-parent="#DropDownListaccordion' + value.numTopicId + '" aria-expanded="false" class="collapsed">[Last Modified] <b>' + formattedDate(value.dtmUpdatedOn) + '</b></a></span>'
                topicContent = topicContent + '</div>'

                topicContent = topicContent + '<div class="col-md-2">'
                topicContent = topicContent + '<span class="box-title"><a data-toggle="collapse" href="#collapseTopic' + value.numTopicId +'" data-parent="#DropDownListaccordion' + value.numTopicId + '" aria-expanded="false" class="collapsed">' + IsInternal+'</a></span>'
                topicContent = topicContent + '</div>';

                topicContent = topicContent + '<div class="col-md-2">'
                topicContent = topicContent + '<a href="javascript:void(0)" onclick="ReplyTopic(' + value.numTopicId + ')" class="btn btn-default btn-xs">Reply</a>';
                if (value.numCreateBy == $("#hdnLoggedInUser").val()) {
                    topicContent = topicContent + '<a href="javascript:void(0)" onclick="editTopic(' + value.numTopicId + ')" class="btn btn-default btnTransparent btn-xs"><img src="../Accounting/ImageMapEditor/img/pencil.png" /></a>'
                    topicContent = topicContent + '<a href="javascript:void(0)" onclick="deleteTopic(' + value.numTopicId + ')" class="btn btn-danger btn-xs">X</a>';
                }
                topicContent = topicContent + '</div>';

                topicContent = topicContent + '</div>'
                //#endregion 

                //#region Message Content
                topicContent = topicContent + '<div class="box-body">';

                topicContent = topicContent + '<div class="topicDetails">';

                topicContent = topicContent + '<div class="col-md-10 internalBoxHeader">';
                topicContent = topicContent + ' <div class="form-inline">';
                topicContent = topicContent + "<b style='color:red;float:left'>Topic:&nbsp;</b>" + decodeURI(value.vcTopicTitle) + attachmentList;
                topicContent = topicContent + '</div>';
                topicContent = topicContent + '</div>';

                topicContent = topicContent + '<div class="col-md-2">';
                if (value.MessageCount > 0) {
                    topicContent = topicContent + '<a   data-toggle="collapse" href="#collapseTopic' + value.numTopicId + '" data-parent="#DropDownListaccordion' + value.numTopicId + '" aria-expanded="false" class="repliesHyperLink">' + value.MessageCount + ' replies</a>';
                }
                topicContent = topicContent + '</div>';

                topicContent = topicContent + '</div>';

                topicContent = topicContent + '<div id="collapseTopic' + value.numTopicId + '" class="panel-collapse collapse ' + CollapseIn+'" aria-expanded="true">';
                

                topicContent = topicContent + '<div class="clearfix"></div>';
                topicContent = topicContent + '<div id="topicMessage' + value.numTopicId + '"></div>';

                topicContent = topicContent + '</div>';
                topicContent = topicContent + '</div>';
                //#endregion Message Content

                topicContent = topicContent + "</div>";
                topicContent = topicContent + "</div>";

                $(".CollabarationContent").append(topicContent);
                getMessage(value.numTopicId);
            });
            
        }
    });
}
function editTopic(topicId) {
    var topicDetails = topicArray.filter(x => x.numTopicId == topicId);
    tinyMCE.get('myTextareaCollabaration').setContent(decodeURI(topicDetails[0].vcTopicTitle));
    $("#hdnIsTopicMessage").val("0");
    $("#hdnTopicId").val(topicId);
    $("#hdnParentMessageId").val(0);
    if (topicDetails[0].bitIsInternal == true) {
        $("#chkInternal").prop("checked", true);
    } else {
        $("#chkInternal").prop("checked", false);
    }
    $("#btnCollabarationSave").text("Post Update");
    tinyMCE.activeEditor.focus();
    return false;
}
function ReplyTopic(topicId) {
    var topicDetails = topicArray.filter(x => x.numTopicId == topicId);
    $("#hdnIsTopicMessage").val("1");
    $("#hdnTopicId").val(topicId);
    $("#hdnParentMessageId").val(0);
    $("#divTopicReplyToMessageContent").html(decodeURI(topicDetails[0].vcTopicTitle));
    $("#divTopicReplyToMessageContentArea").css("display","block")
    $("#btnCollabarationSave").text("Post Reply");
    tinyMCE.activeEditor.focus();
}
function CancelTopicMessage() {
    $("#hdnIsTopicMessage").val(0);
    $("#hdnTopicId").val(0);
    $("#hdnMessageId").val(0);
    $("#hdnParentMessageId").val(0);
    $("#btnCollabarationSave").text("Post New Topic");
    $("#spnCollbarationFilePath").text("");
    $("#divTopicReplyToMessageContent").html("");
    $("#divTopicReplyToMessageContentArea").css("display", "none")
    tinyMCE.get('myTextareaCollabaration').setContent("");
    if ($("#hdnRecordType").val() == 4 && $("#hdnExternalAccess").val() != "1") {
        $("#modalCollaborationMessage").modal("hide")
    }
}
function deleteTopic(topicId) {
    if (confirm("Are you sure you want to remove the topic?")) {

        $("#UpdateProgressCollabaration").css("display", "block")
        var dataParam = "{numDomainId:'" + $("#hdnDomainId").val() + "',numTopicId: '" + topicId + "'} ";
        $.ajax({
            type: 'POST',
            url: "../Collabaration/frmCollabaration.aspx/WebMethodDeleteTOPICMASTERFrmCollabaration",
            contentType: "application/json; charset=utf-8",
            data: dataParam,
            success: function (response) {
                debugger;
                var Jresponse = $.parseJSON(response.d);
                if (Jresponse == "1") {

                    $("#UpdateProgressCollabaration").css("display", "none")
                    getTopic();
                }
            }
        });
    }
}
function SaveMessage() {

    $("#UpdateProgressCollabaration").css("display", "block")
    //var dataParam = '{numMessageId:' + $("#hdnMessageId").val() + ',numDomainId:' + $("#hdnDomainId").val() + ',numTopicId:' + $("#hdnTopicId").val() + ',intRecordType:"' + $("#hdnRecordType").val() + '", numRecordId: "' + $("#hdnRecordId").val() + '", vcMessage: "' + encodeURI(tinyMCE.get('myTextarea').getContent()) + '",CreatedBy: "' + $("#hdnLoggedInUser").val() + '",numUpdatedBy: "' + $("#hdnLoggedInUser").val() + '",bitIsInternal:' + $("#chkInternal").is(":checked") + ',numParentMessageId : ' + $("#hdnParentMessageId").val() + ',Status:""}';
    var formData = new FormData();
    formData.append('numMessageId', $("#hdnMessageId").val());
    formData.append('numDomainId', $("#hdnDomainId").val());
    formData.append('numTopicId', $("#hdnTopicId").val());
    formData.append('intRecordType', $("#hdnRecordType").val());
    formData.append('numRecordId', $("#hdnRecordId").val());
    formData.append('vcMessage', encodeURI(tinyMCE.get('myTextareaCollabaration').getContent()));
    formData.append('CreatedBy', $("#hdnLoggedInUser").val());
    formData.append('numUpdatedBy', $("#hdnLoggedInUser").val()); 
    formData.append('bitIsInternal', $("#chkInternal").is(":checked"));
    formData.append('numParentMessageId', $("#hdnParentMessageId").val());
    formData.append('vcBizDocFilePath', $("#hdnCollabarationPortalDocUrl").val());
    
    formData.append('Status', "");
    var fileUpload = $("#fileCollabarationUpload").get(0);
    var files = fileUpload.files;
    for (var i = 0; i < files.length; i++) {
        formData.append(i, files[i]);
    }
    $.ajax({
        type: 'POST',
        //url: "../Collabaration/frmCollabaration.aspx/WebMethodManageMessageMasterFrmCollabaration",
        url: "../Collabaration/CollabarationMessageHandler.ashx",
        //contentType: "application/json; charset=utf-8",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
           // var Jresponse = $.parseJSON(response.d);
            if (parseFloat(response) > 0) {
                
                $("#UpdateProgressCollabaration").css("display", "none")
                if (confirm("Would you like to broadcast your comment out to subscribed contacts ?")) {
                    sendEmail(formData, response,0);
                }
                getMessage($("#hdnTopicId").val());
                CancelTopicMessage();
            }
        }
    });
}
function sendEmail(formData, messageId, IsTopic) {
    var url = '';
    if (IsTopic == 1) {
        url = "../Collabaration/CollabarationSendEmailTopic.ashx"
    } else {
        url = "../Collabaration/CollabarationSendEmailMessage.ashx";
    }
    formData.set('numMessageId', messageId);
    $.ajax({
        type: 'POST',
        url:url,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        async : true,
        success: function (response) {
        
        }
    });
}
function ReplyTopicMessage(messageId, topicId) {
    $("#commentsBox_" + $("#hdnParentMessageId").val() + "").css("background-color", "#e1e1e1");
    $("#hdnParentMessageId").val(0);
    var messageDetails = messageArray.filter(x => x.numMessageId == messageId);
    $("#hdnIsTopicMessage").val("1");
    $("#hdnTopicId").val(topicId);
    $("#hdnParentMessageId").val(messageId);
    $("#btnCollabarationSave").text("Post Reply");
    $("#commentsBox_" + messageId + "").css("background-color", "rgb(232 209 44 / 38%)");

    $("#divTopicReplyToMessageContent").html(decodeURI(messageDetails[0].vcMessage));
    $("#divTopicReplyToMessageContentArea").css("display", "block")
    tinyMCE.activeEditor.focus();
}
function getMessage(TopicId) {
    var externalAccess = false;
    if ($("#hdnExternalAccess").val() == "1") {
        externalAccess = true;
    }
    var dataParam = '{numDomainID:' + $("#hdnDomainId").val() + ',numTopicId:' + TopicId + ',IsExternalUser:' + externalAccess + '}';
    $("#topicMessage" + TopicId + "").html("");
    $.ajax({
        type: 'POST',
        url: "../Collabaration/frmCollabaration.aspx/WebMethodGetMessageMasterFrmCollabaration",
        contentType: "application/json; charset=utf-8",
        data: dataParam,
        async : true,
        dataType: "json",
        success: function (response) {
            
            var messageContent = "";
            var Jresponse = $.parseJSON(response.d);
            if (Jresponse.length > 0) {
                $.each(Jresponse, function (index, value) {
                    if (messageArray.filter(x => x.numMessageId == value.numMessageId) == 1) {
                        messageArray.filter(x => x.numMessageId == value.numMessageId)[0] = value;
                    } else {
                        messageArray.push(value);
                    }
                    var attachmentList = "";
                    if (value.vcAttachmentName != "" && value.vcAttachmentName != null) {
                        var attachmentArray = [];
                        attachmentArray = value.vcAttachmentName.split(',');
                        if (attachmentArray.length > 0) {
                            $(attachmentArray).each(function (index, data) {
                                var dataIdArray = data.split("$@#");
                                attachmentList = attachmentList + '<a target="_blank" class="repliesHyperLinkDelete" href="' + $("#hdnCollabarationPortalDocUrl").val() + dataIdArray[0] + '">' + dataIdArray[0] + '</a>&nbsp;<img class="deleteImageIcons" src="../images/Delete24.png" onclick="DeleteCollabarationDocumentOrLink(' + dataIdArray[1] + ',' + value.numTopicId + ',' + value.numMessageId +');return false;" />&nbsp;&nbsp;';
                            });
                        }
                    }
                    var IsInternal = '';
                    if (value.bitIsInternal == true) {
                        IsInternal = 'Internal';
                    }
                    messageContent = "";
                    messageContent = messageContent + '<div class="messageSection">';
                    messageContent = messageContent + '<div class="commentsBox col-md-12" id="commentsBox_'+value.numMessageId+'">';
                    //#region Message Header
                    messageContent = messageContent + '<div class="col-md-5">'
                    messageContent = messageContent + '<span class="box-title"><a data-toggle="collapse"  class="collapsed"><b>' + formattedDate(value.dtmCreatedOn) + '</b> ' + value.CreatedBy + ', ' + value.CreatedByOrganization + ' (' + value.CreatedByDesignation + ')</a></span>'
                    messageContent = messageContent + '</div>'

                    messageContent = messageContent + '<div class="col-md-3">'
                    messageContent = messageContent + '<span class="box-title"><a data-toggle="collapse"  class="collapsed">[Last Modified] <b>' + formattedDate(value.dtmUpdatedOn) + '</b></a></span>'
                    messageContent = messageContent + '</div>'

                    messageContent = messageContent + '<div class="col-md-2">'
                    messageContent = messageContent + '<span class="box-title"><a data-toggle="collapse" class="collapsed">' + IsInternal + '</a></span>'
                    messageContent = messageContent + '</div>';

                    messageContent = messageContent + '<div class="col-md-2">'
                    messageContent = messageContent + '<a href="javascript:void(0)" onclick="ReplyTopicMessage(' + value.numMessageId + ',' + value.numTopicId + ')" class="btn btn-default btn-xs">Reply</a>';
                    if (value.numCreatedBy == $("#hdnLoggedInUser").val()) {
                        messageContent = messageContent + '<a href="javascript:void(0)" onclick="editMessage(' + value.numMessageId + ')" class="btn btn-default btnTransparent btn-xs"><img src="../Accounting/ImageMapEditor/img/pencil.png" /></a>'
                        messageContent = messageContent + '<a href="javascript:void(0)" onclick="deleteMessage(' + value.numMessageId + ',' + value.numTopicId + ')" class="btn btn-danger btn-xs">X</a>';
                    }
                    messageContent = messageContent + '</div>';


                    messageContent = messageContent + '</div>';
                    //#endregion Message Header
                    messageContent = messageContent + '<div class="clearfix"></div>';

                    messageContent = messageContent + ' <div class="comments">';
                    messageContent = messageContent + '<div class="col-md-10 internalBoxHeader">';
                    messageContent = messageContent + ' <div class="form-inline">';
                    messageContent = messageContent + attachmentList + decodeURI(value.vcMessage);
                    messageContent = messageContent + '</div>';
                    messageContent = messageContent + '</div>';

                    messageContent = messageContent + '<div class="col-md-2">';
                    if (value.MessageCount > 0) {
                        messageContent = messageContent + '<a href="javascript:void(0)" class="repliesHyperLink">' + value.MessageCount + ' replies</a>';
                    }
                    messageContent = messageContent + '</div>';
                    messageContent = messageContent + '</div>';

                    messageContent = messageContent + '</div>';
                    $("#topicMessage" + value.numTopicId + "").append(messageContent);

                    if (parseInt($("#hdnParentMessageId").val()) > 0 && $("#hdnExternalAccess").val()=="1") {
                        $("#commentsBox_" + $("#hdnParentMessageId").val() + "").css("background-color", "rgb(232 209 44 / 38%)");
                    }
                });
            }
        }
    });
}
function editMessage(messageId) {
    var messageDetails = messageArray.filter(x => x.numMessageId == messageId);
    tinyMCE.get('myTextareaCollabaration').setContent(decodeURI(messageDetails[0].vcMessage));
    $("#hdnIsTopicMessage").val("1");
    $("#hdnTopicId").val(messageDetails[0].numTopicId);
    $("#hdnMessageId").val(messageDetails[0].numMessageId);
    $("#hdnParentMessageId").val(messageDetails[0].numParentMessageId);
    if (messageDetails[0].bitIsInternal == true) {
        $("#chkInternal").prop("checked", true);
    } else {
        $("#chkInternal").prop("checked", false);
    }

    $("#commentsBox_" + messageId + "").css("background-color", "rgb(232 209 44 / 38%)");
    $("#btnCollabarationSave").text("Post Update");
    tinyMCE.activeEditor.focus();
    return false;
}

function deleteMessage(messageId,topicId) {
    if (confirm("Are you sure you want to remove the message?")) {

        $("#UpdateProgressCollabaration").css("display", "block")
        var dataParam = "{numDomainId:'" + $("#hdnDomainId").val() + "',numMessageId: '" + messageId + "'} ";
        $.ajax({
            type: 'POST',
            url: "../Collabaration/frmCollabaration.aspx/WebMethodDeleteMessageMasterFrmCollabaration",
            contentType: "application/json; charset=utf-8",
            data: dataParam,
            success: function (response) {
                debugger;
                var Jresponse = $.parseJSON(response.d);
                if (Jresponse == "1") {

                    $("#UpdateProgressCollabaration").css("display", "none")
                    getMessage(topicId);
                }
            }
        });
    }
}
function DeleteCollabarationDocumentOrLink(attachmentId,topicId,messageId) {
    if (confirm("Are you sure you want to remove the attachment?")) {

        $("#UpdateProgressCollabaration").css("display", "block")
        var dataParam = "{numDomainId:'" + $("#hdnDomainId").val() + "',numAttachmentId: '" + attachmentId + "'} ";
        $.ajax({
            type: 'POST',
            url: "../Collabaration/frmCollabaration.aspx/WebMethodDeleteTopicMessageAttachmentsFrmCollabaration",
            contentType: "application/json; charset=utf-8",
            data: dataParam,
            success: function (response) {
                debugger;
                var Jresponse = $.parseJSON(response.d);
                if (Jresponse == "1") {

                    $("#UpdateProgressCollabaration").css("display", "none")
                    if (messageId > 0) {
                        getMessage(topicId);
                    } else {
                        getTopic();
                    }
                }
            }
        });
    }
}
function formattedDate(selectedDate) {
    return moment(selectedDate).format('MMM Do, hh:mm A');
}