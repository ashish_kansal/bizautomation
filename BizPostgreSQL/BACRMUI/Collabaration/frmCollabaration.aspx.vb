﻿Imports System.Collections.Generic
Imports System.Web.Services
Imports BACRM.BusinessLogic.CCollabaration
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports Newtonsoft.Json

Public Class frmCollabaration
    Inherits System.Web.UI.Page
    Dim MessageId As Long
    Dim ContactId As Long
    Dim DomainId As Long
    Dim TopicId As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MessageId = CCommon.ToLong(GetQueryStringVal("numMessageId"))
            ContactId = CCommon.ToLong(GetQueryStringVal("numContactId"))
            DomainId = CCommon.ToLong(GetQueryStringVal("numDomainId"))
            TopicId = CCommon.ToLong(GetQueryStringVal("numTopicId"))
            Dim hdnIsTopicMessage As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnIsTopicMessage"), HiddenField)
            Dim hdnTopicId As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnTopicId"), HiddenField)
            Dim hdnParentMessageId As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnParentMessageId"), HiddenField)
            Dim hdnSelectedMessageOrTopicExternal As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnSelectedMessageOrTopicExternal"), HiddenField)

            Dim hdnCollabarationLoggedInUser As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnLoggedInUser"), HiddenField)
            Dim hdnCollabarationRecordId As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnRecordId"), HiddenField)
            Dim hdnCollabarationRecordType As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnRecordType"), HiddenField)
            Dim hdnCollabarationDomainId As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnDomainId"), HiddenField)
            Dim hdnCollabarationPortalDocUrl As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnCollabarationPortalDocUrl"), HiddenField)
            Dim hdnClientMachineUTCTimeOffset As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnClientMachineUTCTimeOffset"), HiddenField)
            Dim hdnExternalAccess As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnExternalAccess"), HiddenField)
            If MessageId > 0 Then
                Dim objCCollabaration As New CCollabaration
                Dim dtMessageDetails As DataTable
                objCCollabaration.numMessageId = MessageId
                objCCollabaration.numDomainId = DomainId
                dtMessageDetails = objCCollabaration.GetMessageMaster()

                Dim objCommon As New CCommon
                Dim dtDomainDetails As DataTable
                objCommon.DomainID = DomainId
                dtDomainDetails = objCommon.GetDomainDetails()
                Session("vcThemeClass") = dtDomainDetails.Rows(0)("vcThemeClass")
                Dim strFilePath As String
                If CCommon.ToBool(dtDomainDetails.Rows(0)("bitLogoAppliedToBizTheme")) = True Then
                    If CCommon.ToString(dtDomainDetails.Rows(0)("vcLogoForBizTheme")) <> "" Then
                        Dim FilePath = CCommon.GetDocumentPath(DomainId)
                        strFilePath = FilePath & CCommon.ToString(dtDomainDetails.Rows(0)("vcLogoForBizTheme"))
                        imgThemeLogo.ImageUrl = strFilePath
                    Else
                        strFilePath = "~/images/BizBlack.png"
                        imgThemeLogo.ImageUrl = strFilePath
                    End If
                End If

                hdnIsTopicMessage.Value = "1"
                hdnTopicId.Value = dtMessageDetails.Rows(0)("numTopicId")
                hdnParentMessageId.Value = MessageId
                hdnSelectedMessageOrTopicExternal.Value = dtMessageDetails.Rows(0)("vcMessage")

                hdnCollabarationLoggedInUser.Value = CCommon.ToString(ContactId)
                hdnCollabarationDomainId.Value = CCommon.ToString(DomainId)
                hdnClientMachineUTCTimeOffset.Value = CCommon.ToString((DateTime.UtcNow - DateTime.Now).TotalMinutes)
                hdnCollabarationRecordType.Value = dtMessageDetails.Rows(0)("intRecordType")
                hdnCollabarationRecordId.Value = dtMessageDetails.Rows(0)("numRecordId")
                hdnExternalAccess.Value = "1"
                hdnCollabarationPortalDocUrl.Value = CCommon.GetDocumentPath(DomainId)
            Else
                Dim objCCollabaration As New CCollabaration
                Dim dtTopicDetails As DataTable
                objCCollabaration.numTopicId = TopicId
                objCCollabaration.numDomainId = DomainId
                dtTopicDetails = objCCollabaration.GetTopicMaster()

                Dim objCommon As New CCommon
                Dim dtDomainDetails As DataTable
                objCommon.DomainID = DomainId
                dtDomainDetails = objCommon.GetDomainDetails()
                Session("vcThemeClass") = dtDomainDetails.Rows(0)("vcThemeClass")
                Dim strFilePath As String
                If CCommon.ToBool(dtDomainDetails.Rows(0)("bitLogoAppliedToBizTheme")) = True Then
                    If CCommon.ToString(dtDomainDetails.Rows(0)("vcLogoForBizTheme")) <> "" Then
                        Dim FilePath = CCommon.GetDocumentPath(DomainId)
                        strFilePath = FilePath & CCommon.ToString(dtDomainDetails.Rows(0)("vcLogoForBizTheme"))
                        imgThemeLogo.ImageUrl = strFilePath
                    Else
                        strFilePath = "~/images/BizBlack.png"
                        imgThemeLogo.ImageUrl = strFilePath
                    End If
                End If

                hdnIsTopicMessage.Value = "1"
                hdnTopicId.Value = TopicId
                hdnParentMessageId.Value = 0
                hdnSelectedMessageOrTopicExternal.Value = dtTopicDetails.Rows(0)("vcTopicTitle")

                hdnCollabarationLoggedInUser.Value = CCommon.ToString(ContactId)
                hdnCollabarationDomainId.Value = CCommon.ToString(DomainId)
                hdnClientMachineUTCTimeOffset.Value = CCommon.ToString((DateTime.UtcNow - DateTime.Now).TotalMinutes)
                hdnCollabarationRecordType.Value = dtTopicDetails.Rows(0)("intRecordType")
                hdnCollabarationRecordId.Value = dtTopicDetails.Rows(0)("numRecordId")
                hdnExternalAccess.Value = "1"
                hdnCollabarationPortalDocUrl.Value = CCommon.GetDocumentPath(DomainId)
            End If
        End If
    End Sub
    Public Function GetQueryStringVal(ByVal Key As String, Optional ByVal boolReturnComplete As Boolean = False) As String
        Try
            Dim objQSV As QueryStringValues
            If Session("objQSV") Is Nothing Then
                objQSV = New QueryStringValues
                Session("objQSV") = objQSV
            Else
                objQSV = CType(Session("objQSV"), QueryStringValues)
            End If
            Return CCommon.ToString(objQSV.GetQueryStringVal(Request.QueryString("enc"), Key, boolReturnComplete))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <WebMethod()>
    Public Shared Function WebMethodManageTopicMasterFrmCollabaration(ByVal numTopicId As Long, ByVal intRecordType As Integer,
                                                                      ByVal numRecordId As Long, ByVal vcTopicTitle As String,
                                                                      ByVal CreatedBy As Long, ByVal numUpdatedBy As String,
                                                                      ByVal bitIsInternal As Boolean, ByVal numDomainId As Long,
                                                                      ByVal Status As String) As String
        Try
            Dim objCCollabaration = New CCollabaration()
            Dim output As String = "1"
            objCCollabaration.numTopicId = numTopicId
            objCCollabaration.intRecordType = intRecordType
            objCCollabaration.numRecordId = numRecordId
            objCCollabaration.vcTopicTitle = vcTopicTitle
            objCCollabaration.CreatedBy = CreatedBy
            objCCollabaration.numUpdatedBy = numUpdatedBy
            objCCollabaration.bitIsInternal = bitIsInternal
            objCCollabaration.numDomainId = numDomainId
            objCCollabaration.Status = Status


            objCCollabaration.ManageTopicMaster()
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(output, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try
    End Function

    <WebMethod()>
    Public Shared Function WebMethodDeleteTOPICMASTERFrmCollabaration(ByVal numTopicId As Long, ByVal numDomainId As Long) As String
        Try
            Dim objCCollabaration = New CCollabaration()
            Dim output As String = "1"
            objCCollabaration.numTopicId = numTopicId
            objCCollabaration.numDomainId = numDomainId


            objCCollabaration.DeleteTOPICMASTER()
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(output, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try
    End Function

    <WebMethod()>
    Public Shared Function WebMethodGetTopicMasterFrmCollabaration(ByVal intRecordType As Integer, ByVal numRecordId As Long, ByVal numDomainID As Long, ByVal IsExternalUser As Boolean) As String
        Try
            Dim objCCollabaration = New CCollabaration()
            Dim dtTable As DataTable
            objCCollabaration.intRecordType = intRecordType
            objCCollabaration.numRecordId = numRecordId
            objCCollabaration.numDomainId = numDomainID
            objCCollabaration.IsExternalUser = IsExternalUser
            dtTable = objCCollabaration.GetTopicMaster()
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dtTable, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try
    End Function

    <WebMethod()>
    Public Shared Function WebMethodManageMessageMasterFrmCollabaration(ByVal numMessageId As Long, ByVal numTopicId As Long,
                                                                        ByVal numParentMessageId As Long, ByVal intRecordType As Integer,
                                                                      ByVal numRecordId As Long, ByVal vcMessage As String,
                                                                      ByVal CreatedBy As Long, ByVal numUpdatedBy As String,
                                                                      ByVal bitIsInternal As Boolean, ByVal numDomainId As Long,
                                                                      ByVal Status As String) As String
        Try
            Dim objCCollabaration = New CCollabaration()
            Dim output As String = "1"
            objCCollabaration.numMessageId = numMessageId
            objCCollabaration.numTopicId = numTopicId
            objCCollabaration.numParentMessageId = numParentMessageId
            objCCollabaration.intRecordType = intRecordType
            objCCollabaration.numRecordId = numRecordId
            objCCollabaration.vcMessage = vcMessage
            objCCollabaration.CreatedBy = CreatedBy
            objCCollabaration.numUpdatedBy = numUpdatedBy
            objCCollabaration.bitIsInternal = bitIsInternal
            objCCollabaration.numDomainId = numDomainId
            objCCollabaration.Status = Status


            objCCollabaration.ManageMessageMaster()
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(output, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try
    End Function

    <WebMethod()>
    Public Shared Function WebMethodDeleteMessageMasterFrmCollabaration(ByVal numMessageId As Long, ByVal numDomainId As Long) As String
        Try
            Dim objCCollabaration = New CCollabaration()
            Dim output As String = "1"
            objCCollabaration.numMessageId = numMessageId
            objCCollabaration.numDomainId = numDomainId


            objCCollabaration.DeleteMessageMaster()
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(output, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try
    End Function

    <WebMethod()>
    Public Shared Function WebMethodGetMessageMasterFrmCollabaration(ByVal numTopicId As Long, ByVal numDomainID As Long, ByVal IsExternalUser As Boolean) As String
        Try
            Dim objCCollabaration = New CCollabaration()
            Dim dtTable As DataTable
            objCCollabaration.numTopicId = numTopicId
            objCCollabaration.numDomainId = numDomainID
            objCCollabaration.IsExternalUser = IsExternalUser
            dtTable = objCCollabaration.GetMessageMaster()
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dtTable, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try
    End Function

    <WebMethod()>
    Public Shared Function WebMethodManageTopicMessageAttachmentsFrmCollabaration(ByVal numAttachmentId As Long, ByVal numTopicId As Long,
                                                                      ByVal numMessageId As Long, ByVal vcAttachmentName As String, ByVal vcAttachmentUrl As String,
                                                                      ByVal CreatedBy As Long, ByVal numUpdatedBy As String,
                                                                      ByVal numDomainId As Long, ByVal Status As String) As String
        Try
            Dim objCCollabaration = New CCollabaration()
            Dim output As String = "1"
            objCCollabaration.numAttachmentId = numAttachmentId
            objCCollabaration.numTopicId = numTopicId
            objCCollabaration.numMessageId = numMessageId
            objCCollabaration.vcAttachmentName = vcAttachmentName
            objCCollabaration.vcAttachmentUrl = vcAttachmentUrl
            objCCollabaration.CreatedBy = CreatedBy
            objCCollabaration.numUpdatedBy = numUpdatedBy
            objCCollabaration.numDomainId = numDomainId
            objCCollabaration.Status = Status


            objCCollabaration.ManageTopicMessageAttachments()
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(output, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try
    End Function

    <WebMethod()>
    Public Shared Function WebMethodDeleteTopicMessageAttachmentsFrmCollabaration(ByVal numAttachmentId As Long, ByVal numDomainId As Long) As String
        Try
            Dim objCCollabaration = New CCollabaration()
            Dim output As String = "1"
            objCCollabaration.numAttachmentId = numAttachmentId
            objCCollabaration.numDomainId = numDomainId


            objCCollabaration.DeleteTopicMessageAttachments()
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(output, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try
    End Function
End Class