﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCollabaration.aspx.vb" Inherits=".frmCollabaration" %>

<%@ Register Src="../common/frmCollaborationUserControl.ascx" TagName="frmCollaborationUserControl" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../CSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../CSS/biz.css" rel="stylesheet" />
    <link href="../CSS/biz-theme.css" rel="stylesheet" />
    <link href="../CSS/font-awesome.min.css" rel="stylesheet" />
    <link href="../CSS/biz-skin.css" rel="stylesheet" />
    <link href="../CSS/biz-theme.css" rel="stylesheet" />
    <script src="../JavaScript/jquery-1.9.1.min.js"></script>
    <script src="../JavaScript/bootstrap.min.js"></script>
</head>
<body class="_<%=BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass")) %>">
    <form id="form1" runat="server">
        <div class="col-md-10 col-md-offset-2">
             <asp:HyperLink ID="hlpBiz" runat="server" NavigateUrl="~/Home.aspx" class="logo">
                            <asp:Image ID="imgThemeLogo" class="img img-responsive" style="max-height:37px" runat="server" ImageUrl="~/images/BizBlack.png" />
                            <div style="font-size: 12px;color: #000;font-weight: normal;margin-top: 6px;font-style:italic"><label>Powered By </label>&nbsp;BizAutomation.com</div>
                        </asp:HyperLink>
        <uc1:frmCollaborationUserControl ID="frmCollaborationUserControl" runat="server" />
            </div>
    </form>
</body>
</html>
