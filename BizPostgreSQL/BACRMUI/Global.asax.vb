Imports BACRM.BusinessLogic.Admin
Imports System.Web.Caching
Imports System.IO
Imports System.Data.SqlClient
Imports System.Web.Routing
Imports System.Web.Optimization
Imports DatabaseNotification
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common


Public Class [Global]
    Inherits System.Web.HttpApplication
#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer
    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

#End Region

    Private mnSessionMinutes As Integer = 120
    Private msSessionCacheName As String = "SessionTimeOut"
    Private Delegate Sub SqlDependencyDelegate(sender As Object, e As SqlNotificationEventArgs)


    Public Sub InsertSessionCacheItem(ByVal sUserID As String)

        Try
            If Me.GetSessionCacheItem(sUserID) <> "" Then
                Return
            End If
            Dim oRemove As New CacheItemRemovedCallback(AddressOf Me.SessionEnded)

            System.Web.HttpContext.Current.Cache.Insert(msSessionCacheName & sUserID, sUserID, Nothing, DateTime.MaxValue, TimeSpan.FromMinutes(mnSessionMinutes), CacheItemPriority.High, oRemove)
        Catch e As Exception
            Response.Write(e.Message)
        End Try
    End Sub

    Public Function GetSessionCacheItem(ByVal sUserID As String) As String
        Dim sRet As String = ""

        Try
            sRet = DirectCast(System.Web.HttpContext.Current.Cache(msSessionCacheName & sUserID), String)
            If sRet Is Nothing Then
                sRet = ""
            End If
        Catch generatedExceptionName As Exception
        End Try
        Return sRet
    End Function

    Public Sub SessionEnded(ByVal key As String, ByVal val As Object, ByVal r As CacheItemRemovedReason)
        If key IsNot Nothing Then
            'Dim objCache As New Cache
            'objCache.Remove(key)
            HttpRuntime.Cache.Remove(key)
        End If
    End Sub

    Private Function ValidateWebConfigSettings() As Boolean
        If Not ConfigurationSettings.AppSettings("BACRMLocation") Is Nothing Then
            If Not Directory.Exists(ConfigurationSettings.AppSettings("BACRMLocation")) Then
                Throw New Exception("Invalid configuration in Key: BACRMLocation")
                Return False
            End If
        End If
        If Not ConfigurationSettings.AppSettings("PortalLocation") Is Nothing Then
            If Not Directory.Exists(ConfigurationSettings.AppSettings("PortalLocation")) Then
                Throw New Exception("Invalid configuration in Key: PortalLocation")
                Return False
            End If
        End If
        If Not ConfigurationSettings.AppSettings("MailQueuePath") Is Nothing Then
            If Not Directory.Exists(ConfigurationSettings.AppSettings("MailQueuePath")) Then
                Throw New Exception("Invalid configuration in Key: MailQueuePath")
                Return False
            End If
        End If
        If Not ConfigurationSettings.AppSettings("CartLocation") Is Nothing Then
            If Not Directory.Exists(ConfigurationSettings.AppSettings("CartLocation")) Then
                Throw New Exception("Invalid configuration in Key: CartLocation")
                Return False
            End If
        End If
        If Not ConfigurationSettings.AppSettings("LuceneIndex") Is Nothing Then
            If Not Directory.Exists(ConfigurationSettings.AppSettings("LuceneIndex")) Then
                Throw New Exception("Invalid configuration in Key: LuceneIndex")
                Return False
            End If
        End If
        Return True
    End Function

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        'RouteTable.Routes.MapHubs()
        'RegisterNotifications()

        ' Fires when the application is started
        Application("IsConfigValid") = False
        If Not ValidateWebConfigSettings() Then
            Application("IsConfigValid") = False
        Else
            Application("IsConfigValid") = True
        End If
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        SqlDependency.Stop(ConfigurationManager.AppSettings("ConnectionString"))
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        Session("DisplayedAlertIntially") = False

        ' In some constructor or method that runs when the app starts.
        ' 1st parameter is the callback to be run every iteration.
        ' 2nd parameter is optional parameters for the callback.
        ' 3rd parameter is telling the timer when to start.
        ' 4th parameter is telling the timer how often to run.
        'Dim timer As New System.Threading.Timer(New TimerCallback(AddressOf TimerElapsed), Nothing, New TimeSpan(0), New TimeSpan(0, 5, 0))
        'Dim timer As New Threading.Timer(AddressOf Reminder_Tick, Nothing, New TimeSpan(0), New TimeSpan(0, 1, 0))
        'this will serve for popup
        'If Request.Url.Segments(Request.Url.Segments.Length - 1).ToUpper() <> "WEBFORM1.ASPX" And Request.Url.Segments(Request.Url.Segments.Length - 1).ToUpper() <> "LOGIN.ASPX" And Request.Url.Segments(Request.Url.Segments.Length - 1).ToUpper() <> "ENDSESSION.ASPX" Then
        '    'Session.Abandon()
        '    'Response.Redirect("../LogOff.htm")
        '    Response.Write("<script language='javascript'> if (parent.frames['mainframe'] !=null) { parent.frames['mainframe'].location.href='../LogOff.htm' } else { document.location.href='../LogOff.htm'}</script>")
        'End If
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
        If Application("IsConfigValid") = False Then
            If Not ValidateWebConfigSettings() Then
                Application("IsConfigValid") = False
                Response.StatusCode = 200
                Response.End()
            Else
                Application("IsConfigValid") = True
            End If
        End If

    End Sub

    'Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
    '    ' Fires upon attempting to authenticate the use
    'End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Dim ex As Exception = Server.GetLastError
        If ex.GetBaseException().GetType() Is GetType(HttpRequestValidationException) Then
            Response.Write("<html><head><title>HTML Not Allowed</title>" & _
                            "<script language='JavaScript'><!--" & _
                            "function back() { history.go(-1); } //--></script></head> " & _
                            "<body style='font-family: Arial, Sans-serif;'>" & _
                            "<h1>Oops!</h1>" & _
                            "<p>I'm sorry, but HTML entry is not allowed on that page.</p>" & _
                            "<p>Please make sure that your entries do not contain " & _
                            "any angle brackets like &lt; or &gt;.</p>" & _
                            "<p><a href='javascript:back()'>Go back</a></p> " & _
                            "</body></html>")
            Response.StatusCode = 200
            Response.End()
        End If
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        If Not IsNothing(Session("AccessID")) Then
            Dim objAdmin As New CAdmin
            objAdmin.AccessID = Session("AccessID")
            objAdmin.LoggedOut()
        End If

        If Not IsNothing(Session("UserID")) Then
            HttpRuntime.Cache.Remove(msSessionCacheName & CCommon.ToLong(Session("UserID")))
        End If
    End Sub

    'Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
    '    ' Fires when the application ends

    'End Sub

#Region "Alert Panel"

    Private Sub RegisterNotifications()
        Try
            SqlDependency.Start(ConfigurationManager.AppSettings("ConnectionString"))

            InitializeDependnecy("SELECT numCompanyId,numRecOwner,numAssignedTo FROM View_CompanyAlert", AddressOf dependency_OnChange)
            InitializeDependnecy("SELECT numCaseId,numRecOwner,numAssignedTo FROM View_CaseAlert", AddressOf dependencyCase_OnChange)
            InitializeDependnecy("SELECT numOppId,numRecOwner,numAssignedTo FROM View_OpportunityAlert", AddressOf dependencyOpportunity_OnChange)
            InitializeDependnecy("SELECT numProId,numRecOwner,numAssignedTo FROM View_ProjectsAlert", AddressOf dependencyProject_OnChange)
            InitializeDependnecy("SELECT numEmailHstrID,dtReceivedOn FROM View_EmailAlert", AddressOf dependencyEmail_OnChange)
            InitializeDependnecy("SELECT numCommId,numAssign FROM View_TicklerAlert", AddressOf dependencyTickler_OnChange)
            InitializeDependnecy("SELECT numStageDetailsId,numAssignTo,bintModifiedDate FROM View_ProcessAlert", AddressOf dependencyProcess_OnChange)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub InitializeDependnecy(ByVal query As String, ByVal sqlDelegate As Npgsql.NotificationEventHandler)
        Try
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")

            Using connection As New Npgsql.NpgsqlConnection(conStr)
                Using command As New Npgsql.NpgsqlCommand(query, connection)
                    If connection.State = ConnectionState.Closed Then
                        connection.Open()
                    End If

                    AddHandler connection.Notification, sqlDelegate

                    Using sqlCaseReader As Npgsql.NpgsqlDataReader = command.ExecuteReader()

                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dependency_OnChange(sender As Object, e As Npgsql.NpgsqlNotificationEventArgs)
        SendNotifications()
        'Reinitialize dependency otherwise alert will not fire second time
        InitializeDependnecy("SELECT numCompanyId,numRecOwner,numAssignedTo FROM View_CompanyAlert", AddressOf dependency_OnChange)
    End Sub

    Private Sub dependencyCase_OnChange(sender As Object, e As Npgsql.NpgsqlNotificationEventArgs)
        SendCaseNotifications()
        'Reinitialize dependency otherwise alert will not fire second time
        InitializeDependnecy("SELECT numCaseId,numRecOwner,numAssignedTo from View_CaseAlert", AddressOf dependencyCase_OnChange)
    End Sub

    Private Sub dependencyOpportunity_OnChange(sender As Object, e As Npgsql.NpgsqlNotificationEventArgs)
        SendOpportunityNotifications()
        'Reinitialize dependency otherwise alert will not fire second time
        InitializeDependnecy("SELECT numOppId,numRecOwner,numAssignedTo FROM View_OpportunityAlert", AddressOf dependencyOpportunity_OnChange)
    End Sub

    Private Sub dependencyProject_OnChange(sender As Object, e As Npgsql.NpgsqlNotificationEventArgs)
        SendProjectNotifications()
        'Reinitialize dependency otherwise alert will not fire second time
        InitializeDependnecy("SELECT numProId,numRecOwner,numAssignedTo FROM View_ProjectsAlert", AddressOf dependencyProject_OnChange)
    End Sub

    Private Sub dependencyEmail_OnChange(sender As Object, e As Npgsql.NpgsqlNotificationEventArgs)
        SendEmailNotifications()
        'Reinitialize dependency otherwise alert will not fire second time
        InitializeDependnecy("SELECT numEmailHstrID,dtReceivedOn FROM View_EmailAlert", AddressOf dependencyEmail_OnChange)
    End Sub

    Private Sub dependencyTickler_OnChange(sender As Object, e As Npgsql.NpgsqlNotificationEventArgs)
        SendTicklerNotifications()
        'Reinitialize dependency otherwise alert will not fire second time
        InitializeDependnecy("SELECT numCommId,numAssign FROM View_TicklerAlert", AddressOf dependencyTickler_OnChange)
    End Sub

    Private Sub dependencyProcess_OnChange(sender As Object, e As Npgsql.NpgsqlNotificationEventArgs)
        SendProcessNotifications()
        'Reinitialize dependency otherwise alert will not fire second time
        InitializeDependnecy("SELECT numStageDetailsId,numAssignTo,bintModifiedDate FROM View_ProcessAlert", AddressOf dependencyProcess_OnChange)
    End Sub

    Private Sub SendNotifications()
        Dim nHub As New NotificationsHub()
        nHub.NotifyAllClients()
    End Sub

    Private Sub SendCaseNotifications()
        Dim nHub As New CaseHub()
        nHub.NotifyCaseAllClients()
    End Sub

    Private Sub SendOpportunityNotifications()
        Dim nHub As New OpportunityHub()
        nHub.NotifyOpportunityAllClients()
    End Sub

    Private Sub SendProjectNotifications()
        Dim nHub As New ProjectHub()
        nHub.NotifyProjectAllClients()
    End Sub

    Private Sub SendEmailNotifications()
        Dim nHub As New EmailHub()
        nHub.NotifyEmailAllClients()
    End Sub

    Private Sub SendTicklerNotifications()
       Dim nHub As New TicklerHub()
        nHub.NotifyTicklerAllClients()
    End Sub

    Private Sub SendProcessNotifications()
        Dim nHub As New ProcessHub()
        nHub.NotifyProcessAllClients()
    End Sub

#End Region

End Class