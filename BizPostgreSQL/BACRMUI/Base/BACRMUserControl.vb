﻿Option Explicit On
Option Strict Off
Imports BACRM.BusinessLogic.Contacts

Namespace BACRM.BusinessLogic.Common
    Public Class BACRMUserControl
        Inherits System.Web.UI.UserControl


        'Declare common variables and properties used in all pages here
        Public objCommon As New CCommon
        Public m_aryRightsForPage(4) As Integer


        Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
            Try
                'Set Currunt page url in session, so we can load help for current page in help management
                Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), Request)
                Response.Write(ex)
            End Try
        End Sub

        Public Function GetUserRightsForPage_Other(ByVal numModuleID As MODULEID, ByVal numPageID As Long, Optional ByRef ModuleName As String = "", Optional ByRef PermissionName As String = "") As Array
            Try
                Dim m_aryRightsForPageTemp(4) As Integer

                'Check page access rights from Cached XML for current auth group
                Dim strApplicationDataSetName As String = "UserAuthRightsDataSet_" & CCommon.ToLong(Session("DomainID")).ToString()


                Dim dsAuthDetail As DataSet
                If Application(strApplicationDataSetName) Is Nothing Then
                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                    dsAuthDetail = objCommon.GetAuthDetailsForDomain()
                    If dsAuthDetail.Tables(0).Rows.Count > 0 Then
                        Application(strApplicationDataSetName) = dsAuthDetail.Copy()
                        dsAuthDetail.Clear()
                    End If
                End If
                Dim dr As DataRow
                If Not Application(strApplicationDataSetName) Is Nothing Then
                    dsAuthDetail = CType(Application(strApplicationDataSetName), DataSet)
                    If dsAuthDetail.Tables(0).Rows.Count > 0 Then
                        Dim drArray() As DataRow = dsAuthDetail.Tables(0).Select("numGroupID='" + CCommon.ToString(Session("UserGroupID")) + "' and numModuleID='" + CCommon.ToInteger(numModuleID).ToString() + "' and numPageID='" + CCommon.ToString(numPageID) + "'", "")
                        If drArray.Length = 1 Then
                            dr = drArray(0)
                            m_aryRightsForPageTemp(RIGHTSTYPE.VIEW) = CInt(dr("intViewAllowed"))
                            m_aryRightsForPageTemp(RIGHTSTYPE.ADD) = CInt(dr("intAddAllowed"))
                            m_aryRightsForPageTemp(RIGHTSTYPE.DELETE) = CInt(dr("intDeleteAllowed"))
                            m_aryRightsForPageTemp(RIGHTSTYPE.EXPORT) = CInt(dr("intExportAllowed"))
                            m_aryRightsForPageTemp(RIGHTSTYPE.UPDATE) = CInt(dr("intUpdateAllowed"))
                            ModuleName = CCommon.ToString(dr("vcModuleName"))
                            PermissionName = CCommon.ToString(dr("vcPageDesc"))
                        End If
                    End If
                End If
                If dr Is Nothing Then
                    m_aryRightsForPageTemp(RIGHTSTYPE.VIEW) = 0
                    m_aryRightsForPageTemp(RIGHTSTYPE.ADD) = 0
                    m_aryRightsForPageTemp(RIGHTSTYPE.DELETE) = 0
                    m_aryRightsForPageTemp(RIGHTSTYPE.EXPORT) = 0
                    m_aryRightsForPageTemp(RIGHTSTYPE.UPDATE) = 0
                End If

                Return m_aryRightsForPageTemp
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetUserRightsForPage(ByVal numModuleID As MODULEID, ByVal numPageID As Long) As Array
            Try
                Dim strModuleName, strPermissionName As String
                m_aryRightsForPage = GetUserRightsForPage_Other(numModuleID, numPageID, strModuleName, strPermissionName)

                'When user has no view rights, show warning message
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC&Module=" & strModuleName & "&Permission=" & strPermissionName)
                End If

                Return m_aryRightsForPage
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub AddToRecentlyViewed(ByVal RecordType As RecetlyViewdRecordType, RecordID As Long)
            Try
                If objCommon Is Nothing Then objCommon = New CCommon
                Select Case RecordType
                    Case RecetlyViewdRecordType.Account_Pospect_Lead
                        objCommon.charModule = "C"
                    Case RecetlyViewdRecordType.ActionItem
                        objCommon.charModule = "A"
                    Case RecetlyViewdRecordType.Document
                        objCommon.charModule = "D"
                    Case RecetlyViewdRecordType.Opportunity
                        objCommon.charModule = "O"
                    Case RecetlyViewdRecordType.Support
                        objCommon.charModule = "S"
                    Case RecetlyViewdRecordType.Project
                        objCommon.charModule = "P"
                    Case RecetlyViewdRecordType.AssetItem
                        objCommon.charModule = "AI"
                    Case RecetlyViewdRecordType.Contact
                        objCommon.charModule = "U"
                    Case RecetlyViewdRecordType.Item
                        objCommon.charModule = "I"
                    Case Else
                End Select
                objCommon.RecordId = RecordID
                objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objCommon.AddVisiteddetails()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

       
        Public Function GetQueryStringVal(ByVal Key As String, Optional ByVal boolReturnComplete As Boolean = False) As String
            Try
                Dim objQSV As QueryStringValues
                If Session("objQSV") Is Nothing Then
                    objQSV = New QueryStringValues
                    Session("objQSV") = objQSV
                Else
                    objQSV = CType(Session("objQSV"), QueryStringValues)
                End If
                Return CCommon.ToString(objQSV.GetQueryStringVal(Request.QueryString("enc"), Key, boolReturnComplete))
            Catch ex As Exception
                Throw ex
            End Try
        End Function


    End Class


End Namespace

