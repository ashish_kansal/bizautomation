''**********************************************************************************
'' <IPage.vb>
'' 
'' 	CHANGE CONTROL:
''	
''	AUTHOR: Goyal	  DATE:03-Mar-04	VERSION	CHANGES	KEYSTRING:
''**********************************************************************************

'Namespace BACRM.UserInterface.Interfaces

'    '**********************************************************************************
'    ' Module Name  : IPage
'    ' Module Type  : Interface
'    ' 
'    ' Description  : This is the base Interface for the Master Page in BACRM application
'    '**********************************************************************************
'    Public Interface IPage
'        Property UserId() As Int32
'        Property ScreenId() As Int32
'        Property ScreenName() As String
'    End Interface

'    Public Structure SPermissions
'        Public intUserID As Integer
'        Public intScreenID As Integer

'        Public bRead As Boolean
'        Public bCreate As Boolean
'        Public bUpdate As Boolean
'        Public bDelete As Boolean
'        Public bPrint As Boolean
'        Public bExport As Boolean


'        Public Sub New(ByVal i As Integer)
'            intUserID = 1
'            intScreenID = 0

'            bRead = False
'            bCreate = False
'            bUpdate = False
'            bDelete = False
'            bPrint = False
'            bExport = False
'        End Sub 'New


'        Public ReadOnly Property Read() As Boolean
'            Get
'                Return bRead
'            End Get
'        End Property

'        Public ReadOnly Property Create() As Boolean
'            Get
'                Return bCreate
'            End Get
'        End Property

'        Public ReadOnly Property Update() As Boolean
'            Get
'                Return bUpdate
'            End Get
'        End Property

'        Public ReadOnly Property Delete() As Boolean
'            Get
'                Return bDelete
'            End Get
'        End Property

'        Public ReadOnly Property Export() As Boolean
'            Get
'                Return bExport
'            End Get
'        End Property

'        Public ReadOnly Property Print() As Boolean
'            Get
'                Return bPrint
'            End Get
'        End Property

'        Public Function HasScreenPermission() As Boolean
'            If Not bRead And Not bCreate And Not bUpdate And Not bDelete And Not bExport And Not bPrint Then
'                Return False
'            Else
'                Return True
'            End If
'        End Function 'HasScreenPermission



'    End Structure 'PermissionStruct
'End Namespace
