﻿Option Explicit On
Option Strict Off

Imports BACRM.BusinessLogic.Contacts
Imports System.Runtime.CompilerServices

Namespace BACRM.BusinessLogic.Common
    Public Class BACRMMasterPage
        Inherits System.Web.UI.MasterPage
#Region "Memeber Variables"
        Public objCommon As New CCommon
        Public m_aryRightsForPage(4) As Integer
#End Region

        Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
            Try
                'Set Currunt page url in session, so we can load help for current page in help management
                Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1)
                'Throw user out when domainid = 0
                If Request.Url.Segments(Request.Url.Segments.Length - 1).ToUpper() <> "LOGIN.ASPX" And Request.Url.Segments(Request.Url.Segments.Length - 1).ToUpper() <> "ENDSESSION.ASPX" Then
                    If Not HttpContext.Current.User.Identity.IsAuthenticated Or CCommon.ToLong(Session("DomainID")) = 0 Then
                        System.Web.Security.FormsAuthentication.SignOut()
                        Session.Abandon()
                        Response.Redirect("~/Login.aspx", True)
                    End If
                End If
            Catch exThread As Threading.ThreadAbortException
                'Do not log
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), Request)
                Response.Write(ex)
            End Try
        End Sub

#Region "Public Methods"

        Public Function GetUserRightsForPage(ByVal numModuleID As MODULEID, ByVal numPageID As Long) As Array
            Try
                Dim strModuleName, strPermissionName As String
                m_aryRightsForPage = GetUserRightsForPage_Other(numModuleID, numPageID, strModuleName, strPermissionName)

                'When user has no view rights, show warning message
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC&Module=" & strModuleName & "&Permission=" & strPermissionName)
                End If

                Return m_aryRightsForPage
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetUserRightsForPage_Other(ByVal numModuleID As MODULEID, ByVal numPageID As Long, Optional ByRef ModuleName As String = "", Optional ByRef PermissionName As String = "") As Array
            Try
                Dim m_aryRightsForPageTemp(4) As Integer

                'Check page access rights from Cached XML for current auth group
                Dim strApplicationDataSetName As String = "UserAuthRightsDataSet_" & CCommon.ToLong(Session("DomainID")).ToString()


                Dim dsAuthDetail As DataSet
                If Application(strApplicationDataSetName) Is Nothing Then
                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                    dsAuthDetail = objCommon.GetAuthDetailsForDomain()
                    If dsAuthDetail.Tables(0).Rows.Count > 0 Then
                        Application(strApplicationDataSetName) = dsAuthDetail.Copy()
                        dsAuthDetail.Clear()
                    End If
                End If
                Dim dr As DataRow
                If Not Application(strApplicationDataSetName) Is Nothing Then
                    dsAuthDetail = CType(Application(strApplicationDataSetName), DataSet)
                    If dsAuthDetail.Tables(0).Rows.Count > 0 Then
                        Dim drArray() As DataRow = dsAuthDetail.Tables(0).Select("numGroupID='" + CCommon.ToString(Session("UserGroupID")) + "' and numModuleID='" + CCommon.ToInteger(numModuleID).ToString() + "' and numPageID='" + CCommon.ToString(numPageID) + "'", "")
                        If drArray.Length = 1 Then
                            dr = drArray(0)
                            m_aryRightsForPageTemp(RIGHTSTYPE.VIEW) = CInt(dr("intViewAllowed"))
                            m_aryRightsForPageTemp(RIGHTSTYPE.ADD) = CInt(dr("intAddAllowed"))
                            m_aryRightsForPageTemp(RIGHTSTYPE.DELETE) = CInt(dr("intDeleteAllowed"))
                            m_aryRightsForPageTemp(RIGHTSTYPE.EXPORT) = CInt(dr("intExportAllowed"))
                            m_aryRightsForPageTemp(RIGHTSTYPE.UPDATE) = CInt(dr("intUpdateAllowed"))
                            ModuleName = CCommon.ToString(dr("vcModuleName"))
                            PermissionName = CCommon.ToString(dr("vcPageDesc"))
                        End If
                    End If
                End If
                If dr Is Nothing Then
                    m_aryRightsForPageTemp(RIGHTSTYPE.VIEW) = 0
                    m_aryRightsForPageTemp(RIGHTSTYPE.ADD) = 0
                    m_aryRightsForPageTemp(RIGHTSTYPE.DELETE) = 0
                    m_aryRightsForPageTemp(RIGHTSTYPE.EXPORT) = 0
                    m_aryRightsForPageTemp(RIGHTSTYPE.UPDATE) = 0
                End If

                Return m_aryRightsForPageTemp
            Catch ex As Exception
                Throw ex
            End Try
        End Function

#End Region

    End Class
End Namespace
