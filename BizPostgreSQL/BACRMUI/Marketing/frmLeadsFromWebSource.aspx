﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmLeadsFromWebSource.aspx.vb" Inherits=".frmLeadsFromWebSource"
    MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Web Sources Leads / Prospects / Accounts</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Web Sources Leads / Prospects / Accounts
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvWebSourceLeads" CssClass="table table-bordered table-striped" runat="server" AutoGenerateColumns="true" Width="100%" ClientIDMode="Static" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundField DataField="vcCompanyName" HeaderText="Company Name" />
                        <asp:BoundField DataField="vcFirstName" HeaderText="First Name" />
                        <asp:BoundField DataField="vcLastName" HeaderText="Last Name" />
                        <asp:BoundField DataField="vcEmail" HeaderText="Email" />
                        <asp:BoundField DataField="NumberOfUsers" HeaderText="No.Of Users" />
                        <asp:BoundField DataField="numPhone" HeaderText="Phone" />
                        <asp:BoundField DataField="numPhoneExtension" HeaderText="Phone Ext" />
                        <asp:BoundField DataField="CompanyCreatedOn" HeaderText="Created On" />
                        <asp:BoundField DataField="CRMType" HeaderText="CRM Type" />
                        <asp:BoundField DataField="Survey" HeaderText="Survey" />
                        <asp:BoundField DataField="SourceDomain" HeaderText="Source Domain" />
                        <asp:BoundField Visible="false" DataField="numDivisionID" HeaderText="numDivisionID" />
                        <asp:BoundField Visible="false" DataField="numCompanyId" HeaderText="numCompanyId" />
                        <asp:BoundField Visible="false" DataField="numContactId" HeaderText="numContactId" />
                        <asp:BoundField Visible="false" DataField="numTerID" HeaderText="numTerID" />
                        <asp:BoundField Visible="false" DataField="numRecOwner" HeaderText="numRecOwner" />
                    </Columns>
                    <EmptyDataTemplate>
                        No records found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
