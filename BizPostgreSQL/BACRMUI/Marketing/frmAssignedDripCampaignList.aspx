﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAssignedDripCampaignList.aspx.vb"
    Inherits=".frmAssignedDripCampaignList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Assigned Drip Campaigns</title>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    OBSOLETE FORM -- Bug ID 262
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;Assigned Drip Campaigns&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="normal1" width="200">
                            &nbsp;&nbsp;No of Records:
                            <asp:Label ID="lblRecordCount" runat="server"></asp:Label>
                        </td>
                        <td class="normal1" nowrap align="left" width="100">
                            <asp:HyperLink ID="hplAssignDripCamp" CssClass="hyperlink" runat="server" NavigateUrl="~/Marketing/frmAssignDripCampaign.aspx">
							<font color="#180073">Assign Drip Campaign</font>
                            </asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="Table3" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                    Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:DataGrid ID="dgAssignedECamapign" AllowSorting="True" runat="server" Width="100%"
                                CssClass="dg" AutoGenerateColumns="False" >
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="numECampaignID" HeaderText="numECampaignID">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="numECampaignAssigneeID" HeaderText="numECampaignAssigneeID">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="numEmailGroupID">
                                    </asp:BoundColumn>
                                    <asp:ButtonColumn DataTextField="vcECampName" SortExpression="vcECampName" HeaderText="<font color=white>Drip-Camapign Name</font>"
                                        CommandName="Campaign"></asp:ButtonColumn>
                                    <asp:BoundColumn DataField="vcEmailGroupName" HeaderText="<font color=white>Email Group</font>">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="dtStartDate" HeaderText="<font color=white>Start Date</font>">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                                        <ItemTemplate>
                                            <asp:Button ID="btnDeleteECamapign" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                            </asp:Button>
                                            <asp:LinkButton ID="lnkDeleteECamapign" runat="server" Visible="false">
													<font color="#730000">*</font></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages">
                                </PagerStyle>
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
