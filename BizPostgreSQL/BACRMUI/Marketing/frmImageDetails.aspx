<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmImageDetails.aspx.vb" Inherits="frmImageDetails"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Image Details</title>
		<script language="javascript">
		function OpenLink(Link)
		{
			window.open(Link);
			return false;
		}
		function OpenURL(Link)
		{
			window.open(Link);
			return false;
		}
		function Save()
		{
			if (document.Form1.txtDocName.value=="")
			{
				alert("Enter Name")
				document.Form1.txtDocName.focus()
				return false;
			}
			
			
				if (document.Form1.fileupload.value=="")
				{
					alert("Select document")
					return false;
				}
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" encType="multipart/form-data" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%" align="center">
				<tr>
					<td>
						<table id="tblMenu" borderColor="black" cellSpacing="0" cellPadding="0" width="100%" border="0"
							runat="server">
							<tr>
								<td class="tr1" align="center"><b>Created By: </b>
									<asp:label id="lblCreatedBy" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Last Modified By: </b>
									<asp:label id="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td></td>
								<TD align="right"><asp:button id="btnSaveCLose" text="Save &amp; Close" Runat="server" CssClass="button"></asp:button>
									<asp:button id="btnSave" text="Save" Runat="server" CssClass="button"></asp:button>
									<asp:button id="btnCancel" Runat="server" CssClass="button" Width=50 Text="Back"></asp:button></TD>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td vAlign="bottom" align="left" colSpan="2">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Image Details&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td vAlign="top"><asp:table id="tblOppr" Runat="server" GridLines="None" BorderColor="black" Width="100%" BorderWidth="1" CssClass="aspTable" >
							<asp:TableRow>
								<asp:TableCell>
									<br>
									<TABLE width="100%" border="0">
										<TR>
											<TD class="normal1" align="right">Image Name<FONT color="red">*</FONT>
											</TD>
											<TD>
												<asp:textbox id="txtDocName" Runat="server" cssclass="signup"></asp:textbox>
											</TD>
										</TR>
										<tr>
											<td class="normal1" align="right">Upload Document from
												<br>
												Your Local Network
											</td>
											<TD><input class="signup" id="fileupload" type="file" name="fileupload" runat="server"></TD>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<asp:Image ID="img" Runat="server" Width="200" Height="200"></asp:Image>
											</td>
										</tr>
									</TABLE>
									<br>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
