''Created BY Anoop Jayaraj
Imports System.IO
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports Telerik.Web.UI

Partial Class frmAddAttachments
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'Dim strDocName As String                                        'To Store the information where the File is stored.
    'Dim strFileType As String
    'Dim strFileName As String
    ''Dim strFileType As String
    'Dim strFileSize As String
    'Dim URLType As String
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.

    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Events"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then

                'objCommon.sb_FillComboFromDBwithSel(ddldocumentsCtgr, 29, Session("DomainID"))
                'LoadDocuments()
                'LoadCategories()
                'LoadDropDowns()
                bindGrid()

                If GetQueryStringVal("DivId") <> "" Or GetQueryStringVal("ContId") <> "" Or GetQueryStringVal("OppId") <> "" Then

                    GetDocumentFilesLink()
                    GetParentRecord()
                    GetBizDocs()

                    'objCommon = New CCommon
                    'objCommon.UserCntID = Session("UserContactID")

                    'If GetQueryStringVal("ContId") <> "" Then
                    '    objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("ContId"))
                    '    objCommon.charModule = "C"
                    'ElseIf GetQueryStringVal("DivId") <> "" Then
                    '    objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
                    '    objCommon.charModule = "D"
                    '    'ElseIf GetQueryStringVal("tyrCV") <> "" Then
                    '    '    objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                    '    '    objCommon.charModule = "P"
                    'ElseIf GetQueryStringVal("OppId") <> "" Then
                    '    objCommon.OppID = CCommon.ToLong(GetQueryStringVal("OppId"))
                    '    objCommon.charModule = "O"
                    '    'ElseIf GetQueryStringVal("fghTY") <> "" Then
                    '    '    objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                    '    '    objCommon.charModule = "S"
                    'End If

                    'objCommon.GetCompanySpecificValues1()
                    'If objCommon.DivisionID > 0 Then


                    '    Dim objDocuments As New DocumentList
                    '    Dim dtDocuments As DataTable
                    '    objDocuments.RecID = objCommon.DivisionID
                    '    objDocuments.strType = "A"
                    '    objDocuments.DocCategory = 0
                    '    objDocuments.DomainID = Session("DomainID")
                    '    objDocuments.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    '    Dim dtTable As DataTable = objDocuments.GetSpecificDocuments1

                    '    If dtTable.Rows.Count > 0 Then
                    '        radUploadOrganization.Text = "<img src='../images/Building-16.gif'/>  " & objCommon.GetCompanyName
                    '        trOrganization.Visible = True

                    '        radOrganizationDocuments.DataSource = dtTable
                    '        radOrganizationDocuments.DataTextField = "vcdocname"
                    '        radOrganizationDocuments.DataValueField = "numGenericDocid"
                    '        radOrganizationDocuments.DataBind()

                    '        radOrganizationDocuments.ShowDropDownOnTextboxClick = True
                    '        radOrganizationDocuments.CheckBoxes = True
                    '        radOrganizationDocuments.EnableTextSelection = True
                    '    End If
                    'End If

                    'If CCommon.ToLong(GetQueryStringVal("OppId")) > 0 Then

                    'End If

                End If
            End If
            'btnUpload.Attributes.Add("onclick", "return Upload()")
            btnClose.Attributes.Add("onclick", "return Close1()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgAttachments_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAttachments.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim dtTable As DataTable
                Dim intcnt As Integer
                Dim myRow As DataRow
                dtTable = Session("Attachements")
                For Each myRow In dtTable.Rows
                    If intcnt = e.Item.ItemIndex Then
                        dtTable.Rows.Remove(myRow)
                        Exit For
                    End If
                    intcnt = intcnt + 1
                Next
                Session("Attachements") = dtTable
                bindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgAttachments_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAttachments.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim hpl As HyperLink
                hpl = e.Item.FindControl("hplLink")
                hpl.NavigateUrl = Replace(hpl.NavigateUrl, "..", "../../" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
                'Dim hpl1 As HyperLink
                'hpl1 = e.Item.FindControl("hplBizdcoc")
                'Dim txtOppBizId As TextBox
                'txtOppBizId = e.Item.FindControl("txtBizDocID")
                'hpl1.Attributes.Add("onclick", "return OpenBizInvoice('" & GetQueryStringVal("OpID") & "','" & txtOppBizId.Text & "')")

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnAttachFile_Click(sender As Object, e As EventArgs)
        If fileupload.PostedFile.ContentLength > 0 Then
            Dim strFName As String()
            Dim strFilePath, strFileName, strFileType As String
            Try
                If (fileupload.PostedFile.ContentLength > 0) Then
                    strFileName = Path.GetFileName(fileupload.PostedFile.FileName)      'Getting the File Name
                    If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                    End If
                    strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                    strFName = Split(strFileName, ".")
                    strFileType = fileupload.PostedFile.ContentType  'strFName(strFName.Length - 1)                     'Getting the Extension of the File
                    fileupload.PostedFile.SaveAs(strFilePath)
                    'strFileName = CCommon.GetDocumentPath(Session("DomainID")) & strFileName

                    UploadFile(strFileName, strFileType)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End If
        bindGrid()
    End Sub

    Protected Sub btnAttachDocuments_Click(sender As Object, e As EventArgs)
        If radDocuments.CheckedItems().Count > 0 Then
            'Dim lngOppID As Long = CCommon.ToLong(Session("OppID"))
            Dim lngOppID As Long
            If GetQueryStringVal("OppId") IsNot Nothing AndAlso GetQueryStringVal("OppId") <> "0" Then
                lngOppID = CCommon.ToLong(GetQueryStringVal("OppId"))
            ElseIf Session("OppID") IsNot Nothing AndAlso Session("OppID") <> "0" Then
                lngOppID = CCommon.ToLong(Session("OppID"))
            Else
                lngOppID = 0
            End If
            For Each item As RadComboBoxItem In radDocuments.CheckedItems
                If item.Checked = True Then
                    Dim objDocuments As New BACRM.BusinessLogic.Documents.DocumentList
                    Dim dt As DataTable
                    objDocuments.GenDocID = item.Value 'radDocuments.SelectedItem.Value
                    objDocuments.UserCntID = Session("UserContactID")
                    objDocuments.DomainID = Session("DomainID")
                    dt = objDocuments.GetDocByGenDocID

                    If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                        UploadFile(Replace(dt.Rows(0).Item("VcFileName"), "../documents/docs/", ""), "application/octet-stream")
                    End If
                End If
            Next
        End If
        bindGrid()
    End Sub

    Protected Sub btnParentRecord_Click(sender As Object, e As EventArgs)
        If radParentRecord.CheckedItems().Count > 0 Then
            Dim lngOppID As Long
            If GetQueryStringVal("OppId") IsNot Nothing AndAlso GetQueryStringVal("OppId") <> "0" Then
                lngOppID = CCommon.ToLong(GetQueryStringVal("OppId"))
            ElseIf Session("OppID") IsNot Nothing AndAlso Session("OppID") <> "0" Then
                lngOppID = CCommon.ToLong(Session("OppID"))
            Else
                lngOppID = 0
            End If

            Dim sw As New System.IO.StringWriter()
            Dim RefType As Short = 0
            If Session("OppType") IsNot Nothing Then
                OppType.Value = Session("OppType")
            End If
            If Session("OppStatus") IsNot Nothing Then
                OppStatus.Value = Session("OppStatus")
            End If

            If OppType.Value = 1 And OppStatus.Value = 0 Then
                RefType = 3

            ElseIf OppType.Value = 1 And OppStatus.Value = 1 Then
                RefType = 1

            ElseIf OppType.Value = 2 And OppStatus.Value = 0 Then
                RefType = 4

            ElseIf OppType.Value = 2 And OppStatus.Value = 1 Then
                RefType = 2
            End If
            Server.Execute("../opportunity/frmMirrorBizDoc.aspx" & QueryEncryption.EncryptQueryString("RefID=" & lngOppID & "&RefType=" & RefType & "&DocGenerate=1"), sw)
            Dim htmlCodeToConvert As String = sw.GetStringBuilder().ToString()
            sw.Close()

            For Each item As RadComboBoxItem In radParentRecord.CheckedItems
                If item.Checked = True Then
                    Dim i As Integer
                    Dim strFileName As String = ""
                    Dim strFilePhysicalLocation As String = ""

                    Dim objHTMLToPDF As New HTMLToPDF
                    strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(Session("DomainID")))

                    strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                    objCommon.AddAttchmentToSession(item.Text & ".pdf", CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                End If
            Next
        End If
        bindGrid()
    End Sub

    Protected Sub btnBizDocAttach_Click(sender As Object, e As EventArgs)
        If radBizDocsDocuments.CheckedItems().Count > 0 Then
            'Dim lngOppID As Long = CCommon.ToLong(Session("OppID"))
            Dim lngOppID As Long
            If GetQueryStringVal("OppId") IsNot Nothing AndAlso GetQueryStringVal("OppId") <> "0" Then
                lngOppID = CCommon.ToLong(GetQueryStringVal("OppId"))
            ElseIf Session("OppID") IsNot Nothing AndAlso Session("OppID") <> "0" Then
                lngOppID = CCommon.ToLong(Session("OppID"))
            Else
                lngOppID = 0
            End If
            For Each item As RadComboBoxItem In radBizDocsDocuments.CheckedItems
                If item.Checked = True Then
                    Dim objBizDocs As New OppBizDocs
                    Dim dt As DataTable
                    objBizDocs.BizDocId = item.Value
                    objBizDocs.DomainID = Session("DomainID")
                    objBizDocs.BizDocTemplateID = 0
                    objBizDocs.intAddReferenceDocument = 1
                    dt = objBizDocs.GetBizDocAttachments
                    Dim i As Integer
                    Dim strFileName As String = ""
                    Dim strFilePhysicalLocation As String = ""
                    If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                        For i = 0 To dt.Rows.Count - 1
                            If dt.Rows(i).Item("vcDocName") = "BizDoc" Then
                                strFileName = "File" & Format(Now, "ddmmyyyyhhmmssfff") & ".pdf"
                                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                                Dim objHTMLToPDF As New HTMLToPDF
                                System.IO.File.WriteAllBytes(strFilePhysicalLocation, objHTMLToPDF.CreateBizDocPDF(lngOppID, item.Value, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), CCommon.ToString(Session("DateFormat"))))

                                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                                objCommon.AddAttchmentToSession(item.Text & ".pdf", CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                            Else
                                strFileName = dt.Rows(i).Item("vcURL")
                                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                                objCommon.AddAttchmentToSession(strFileName, CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                            End If

                        Next
                    End If
                End If
            Next
        End If
        bindGrid()
    End Sub
#End Region

#Region "Methods"
    'Added by Neelam on 02/16/2018 - Fetch documents of the record you�re in*/
    Private Sub GetDocumentFilesLink()
        Try
            Dim objOpp As New OppotunitiesIP
            Dim dtDocLinkName As New DataTable
            With objOpp
                .DomainID = Session("DomainID")
                If GetQueryStringVal("DivId") IsNot Nothing AndAlso GetQueryStringVal("DivId") <> "0" Then
                    .DivisionID = GetQueryStringVal("DivId")
                ElseIf Session("DivisionID") IsNot Nothing AndAlso Session("DivisionID") <> "0" Then
                    .DivisionID = CCommon.ToLong(Session("DivisionID"))
                Else : .DivisionID = 0
                End If
                dtDocLinkName = .GetDocumentFilesLink("E")
            End With

            If dtDocLinkName IsNot Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then
                trDocuments.Visible = True
                radDocuments.DataSource = dtDocLinkName
                radDocuments.DataTextField = "vcdocname"
                radDocuments.DataValueField = "numGenericDocid"
                radDocuments.DataBind()
                'radDocuments.Items.Insert(0, "--Select One--")
                'radDocuments.Items.FindByText("--Select One--").Value = 0
            Else
                trDocuments.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Private Sub GetParentRecord()
        Try
            Dim dtOppBiDocDtl As DataTable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.ReferenceType = 1
            If GetQueryStringVal("OppId") IsNot Nothing AndAlso GetQueryStringVal("OppId") <> "0" Then
                objOppBizDocs.ReferenceID = CCommon.ToLong(GetQueryStringVal("OppId"))
                trParentOrganization.Visible = True
            ElseIf Session("OppID") IsNot Nothing AndAlso Session("OppID") <> "0" Then
                objOppBizDocs.ReferenceID = CCommon.ToLong(Session("OppID"))
                trParentOrganization.Visible = True
            Else : objOppBizDocs.ReferenceID = 0
                trParentOrganization.Visible = False
                Exit Sub
            End If
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.UserCntID = Session("UserContactID")
            objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            Dim ds1 As DataSet = objOppBizDocs.GetMirrorBizDocDtls
            dtOppBiDocDtl = ds1.Tables(0)

            If dtOppBiDocDtl.Rows.Count = 0 Then Exit Sub
            If dtOppBiDocDtl IsNot Nothing AndAlso dtOppBiDocDtl.Rows.Count > 0 Then
                radParentRecord.DataSource = dtOppBiDocDtl
                radParentRecord.DataTextField = "OppName"
                radParentRecord.DataValueField = "numBizDocId"
                radParentRecord.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GetBizDocs()
        Dim dtTable As DataTable = Nothing
        Dim objBizDocs As New OppBizDocs

        If GetQueryStringVal("OppId") IsNot Nothing AndAlso GetQueryStringVal("OppId") <> "0" Then
            objBizDocs.OppId = CCommon.ToLong(GetQueryStringVal("OppID"))
            trBizDocs.Visible = True
        ElseIf Session("OppID") IsNot Nothing AndAlso Session("OppID") <> "0" Then
            objBizDocs.OppId = CCommon.ToLong(Session("OppID"))
            trBizDocs.Visible = True
        Else : objBizDocs.OppId = 0
            trBizDocs.Visible = False
            Exit Sub
        End If

        objBizDocs.DomainID = Session("DomainID")
        objBizDocs.UserCntID = Session("UserContactID")

        objBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

        Dim dsBizDocs As DataSet = objBizDocs.GetBizDocsByOOpId

        If Not dsBizDocs Is Nothing AndAlso dsBizDocs.Tables.Count > 1 Then
            dtTable = dsBizDocs.Tables(1)
        End If

        If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 Then
            'trBizDocs.Visible = True

            radBizDocsDocuments.DataSource = dtTable
            'radBizDocsDocuments.DataTextField = "BizDoc"
            radBizDocsDocuments.DataTextField = "vcBizDocID"
            'radBizDocsDocuments.DataValueField = "numBizDocId"
            radBizDocsDocuments.DataValueField = "numOppBizDocsId"
            radBizDocsDocuments.DataBind()

            radBizDocsDocuments.ShowDropDownOnTextboxClick = True
            radBizDocsDocuments.CheckBoxes = True
            radBizDocsDocuments.EnableTextSelection = True
        End If
    End Sub

    Sub UploadFile(ByVal strDocName As String, ByVal strFileType As String)
        Try
            If File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then

                Dim f As FileInfo = New FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                Dim strFileSize As String = f.Length.ToString

                objCommon.AddAttchmentToSession(strDocName, CCommon.GetDocumentPath(Session("DomainID")) & strDocName, CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName, strFileType, strFileSize)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub bindGrid()
        Try
            Dim dtTable As DataTable
            Dim i As Integer
            If Not Session("Attachements") Is Nothing Then
                dtTable = Session("Attachements")
                dgAttachments.DataSource = dtTable
                dgAttachments.DataBind()
                txtAttachements.Text = ""
                For i = 0 To dtTable.Rows.Count - 1
                    txtAttachements.Text = txtAttachements.Text + dtTable.Rows(i).Item("Filename") + ","
                Next
                txtAttachements.Text = txtAttachements.Text.TrimEnd(",")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Commented"
    'Sub LoadDocuments()
    '    Try
    '        Dim objDocuments As New DocumentList
    '        Dim dtDocuments As DataTable
    '        dtDocuments = objDocuments.GetGenericDocList
    '        ddlDocuments.DataSource = dtDocuments
    '        ddlDocuments.DataTextField = "vcdocname"
    '        ddlDocuments.DataValueField = "numGenericDocid"
    '        ddlDocuments.DataBind()
    '        ddlDocuments.Items.Insert(0, "--Select One--")
    '        ddlDocuments.Items.FindByText("--Select One--").Value = 0
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub LoadCategories()
    '    Try
    '        
    '        objCommon.sb_FillComboFromDBwithSel(ddlCategoryOpp, 29, Session("DomainID"))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub LoadDropDowns()
    '    Try
    'Dim objDocuments As New DocumentList
    'objDocuments.byteMode = 0

    'If GetQueryStringVal( "DivId") <> Nothing Then
    '    objDocuments.DivisionID = GetQueryStringVal( "DivId")
    'Else : objDocuments.DivisionID = 0
    'End If
    'If GetQueryStringVal( "ContId") <> Nothing Then
    '    objDocuments.ContactID = GetQueryStringVal( "ContId")
    'Else : objDocuments.ContactID = 0
    'End If

    'Dim dtTable As DataTable
    'Dim dtTableOpp As DataTable
    'dtTable = objDocuments.GetDocumentsAttachment

    'ddlDocumentsCon.DataSource = dtTable
    'ddlDocumentsCon.DataTextField = "vcDocName"
    'ddlDocumentsCon.DataValueField = "numSpecificDocID"
    'ddlDocumentsCon.DataBind()
    'ddlDocumentsCon.Items.Insert(0, "--Select One--")
    'ddlDocumentsCon.Items.FindByText("--Select One--").Value = 0

    'objDocuments.byteMode = 1
    'objDocuments.DocCategory = ddlCategoryOpp.SelectedValue
    'If GetQueryStringVal( "DivId") <> Nothing Then
    '    objDocuments.DivisionID = GetQueryStringVal( "DivId")
    'Else : objDocuments.DivisionID = 0
    'End If
    'If GetQueryStringVal( "ContId") <> Nothing Then
    '    objDocuments.ContactID = GetQueryStringVal( "ContId")
    'Else : objDocuments.ContactID = 0
    'End If
    'dtTableOpp = objDocuments.GetDocumentsAttachment

    'ddlDocumentOpp.DataSource = dtTableOpp
    'ddlDocumentOpp.DataTextField = "vcDocName"
    'ddlDocumentOpp.DataValueField = "numSpecificDocID"
    'ddlDocumentOpp.DataBind()
    'ddlDocumentOpp.Items.Insert(0, "--Select One--")
    'ddlDocumentOpp.Items.FindByText("--Select One--").Value = 0
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
    '    Try
    '        Dim dtTable As DataTable
    '        Dim dr As DataRow

    '        If radUploadOrganization.Checked = True AndAlso radOrganizationDocuments.CheckedItems().Count > 0 Then
    '            Dim objDocuments As New DocumentList
    '            Dim dtDocuments As DataTable

    '            For Each item As RadComboBoxItem In radOrganizationDocuments.CheckedItems
    '                If item.Checked = True Then
    '                    objDocuments = New DocumentList()
    '                    objDocuments.GenDocID = item.Value
    '                    objDocuments.UserCntID = Session("UserContactID")
    '                    objDocuments.DomainID = Session("DomainID")
    '                    dtDocuments = objDocuments.GetDocByGenDocID
    '                    If dtDocuments.Rows.Count > 0 Then
    '                        UploadFile(Replace(dtDocuments.Rows(0).Item("VcFileName"), "../documents/docs/", ""), "application/octet-stream")
    '                    End If
    '                End If
    '            Next
    '        ElseIf radBizDocsDocuments.CheckedItems().Count > 0 Then
    '            Dim lngOppID As Long = CCommon.ToLong(GetQueryStringVal("OppId"))
    '            For Each item As RadComboBoxItem In radBizDocsDocuments.CheckedItems
    '                If item.Checked = True Then
    '                    Dim sw As New System.IO.StringWriter()
    '                    Server.Execute("../opportunity/frmBizInvoice.aspx" & QueryEncryption.EncryptQueryString("OpID=" & lngOppID & "&OppBizId=" & item.Value & "&DocGenerate=1"), sw)
    '                    Dim htmlCodeToConvert As String = sw.GetStringBuilder().ToString()
    '                    sw.Close()

    '                    Dim objBizDocs As New OppBizDocs
    '                    Dim dt As DataTable
    '                    objBizDocs.BizDocId = item.Value
    '                    objBizDocs.DomainID = Session("DomainID")
    '                    objBizDocs.BizDocTemplateID = 0
    '                    dt = objBizDocs.GetBizDocAttachments
    '                    Dim i As Integer
    '                    Dim strFileName As String = ""
    '                    Dim strFilePhysicalLocation As String = ""
    '                    For i = 0 To dt.Rows.Count - 1
    '                        If dt.Rows(i).Item("vcDocName") = "BizDoc" Then
    '                            Dim objHTMLToPDF As New HTMLToPDF
    '                            strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(Session("DomainID")))
    '                        Else
    '                            strFileName = dt.Rows(i).Item("vcURL")
    '                        End If
    '                        strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
    '                        objCommon.AddAttchmentToSession(item.Text & ".pdf", CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
    '                    Next
    '                End If
    '            Next
    '            'ElseIf radUploadItem.Checked = True AndAlso radItemDocuments.CheckedItems().Count > 0 Then
    '            '    Dim objItem As New BACRM.BusinessLogic.Item.CItems
    '            '    objItem.ItemCode = hdnCurrentSelectedItem.Value
    '            '    objItem.DomainID = Session("DomainId")

    '            '    Dim dtImage As DataTable
    '            '    dtImage = objItem.ImageItemDetails()

    '            '    If dtImage.Rows.Count > 0 Then
    '            '        For Each item As RadComboBoxItem In radItemDocuments.CheckedItems
    '            '            If item.Checked = True Then

    '            '                Dim drFLCheck() As DataRow = dtImage.Select("numItemImageId=" & item.Value)

    '            '                If drFLCheck.Length > 0 Then
    '            '                    Dim strFName As String()
    '            '                    Dim strFilePath, strFileType As String

    '            '                    strFName = Split(drFLCheck(0)("vcPathForImage"), ".")
    '            '                    strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File

    '            '                    UploadFile(Replace(drFLCheck(0)("vcPathForImage"), "../documents/docs/", ""), "application/octet-stream")
    '            '                End If
    '            '            End If
    '            '        Next
    '            '    End If
    '            'ElseIf radDocumentLib.Checked = True Then
    '            '    Dim objDocuments As New DocumentList
    '            '    Dim dtDocuments As DataTable
    '            '    objDocuments.GenDocID = ddldocuments.SelectedItem.Value
    '            '    objDocuments.UserCntID = Session("UserContactID")
    '            '    objDocuments.DomainID = Session("DomainID")
    '            '    dtDocuments = objDocuments.GetDocByGenDocID
    '            '    If dtDocuments.Rows.Count > 0 Then
    '            '        'If dtDocDetails.Rows(0).Item("numDocCategory") = 370 Then
    '            '        '    strFileName = CCommon.GetDocumentPath(Session("DomainID")) & Replace(dtDocDetails.Rows(0).Item("VcFileName"), "../documents/docs/", "")
    '            '        'Else : strFileName = dtDocDetails.Rows(0).Item("VcFileName")
    '            '        'End If

    '            '        UploadFile(Replace(dtDocuments.Rows(0).Item("VcFileName"), "../documents/docs/", ""), "application/octet-stream")
    '            '    End If

    '        ElseIf fileupload.PostedFile.ContentLength > 0 Then
    '            Dim strFName As String()
    '            Dim strFilePath, strFileName, strFileType As String
    '            Try
    '                If (fileupload.PostedFile.ContentLength > 0) Then
    '                    strFileName = Path.GetFileName(fileupload.PostedFile.FileName)      'Getting the File Name
    '                    If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
    '                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
    '                    End If
    '                    strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

    '                    strFName = Split(strFileName, ".")
    '                    strFileType = fileupload.PostedFile.ContentType  'strFName(strFName.Length - 1)                     'Getting the Extension of the File
    '                    fileupload.PostedFile.SaveAs(strFilePath)
    '                    'strFileName = CCommon.GetDocumentPath(Session("DomainID")) & strFileName

    '                    UploadFile(strFileName, strFileType)
    '                End If
    '            Catch ex As Exception
    '                Throw ex
    '            End Try
    '        End If

    '        'bindGrid()
    '        'radCmbDoc.SelectedValue = 0

    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "Close", "Close1();", True)

    '        'If radFile.Checked = True Then
    '        '    If radCmbDoc.SelectedValue <> "" And radCmbDoc.SelectedValue <> "0" Then
    '        '        Dim lngDocID As Long = radCmbDoc.SelectedValue.Split("~")(0)
    '        '        Dim charDocType As String = radCmbDoc.SelectedValue.Split("~")(1)
    '        '        Dim dtDocDetails As DataTable
    '        '        Dim objDocuments As New DocumentList
    '        '        If charDocType = "G" Then 'Generic docs
    '        '            With objDocuments
    '        '                .GenDocID = lngDocID
    '        '                .DomainID = Session("DomainID")
    '        '                dtDocDetails = .GetDocByGenDocID
    '        '            End With
    '        '        ElseIf charDocType = "S" Then
    '        '            With objDocuments
    '        '                .SpecDocID = lngDocID
    '        '                .DomainID = Session("DomainID")
    '        '                dtDocDetails = .GetSpecdocDtls1
    '        '            End With
    '        '        End If

    '        '        If dtDocDetails.Rows.Count > 0 Then
    '        '            'If dtDocDetails.Rows(0).Item("numDocCategory") = 370 Then
    '        '            '    strFileName = CCommon.GetDocumentPath(Session("DomainID")) & Replace(dtDocDetails.Rows(0).Item("VcFileName"), "../documents/docs/", "")
    '        '            'Else : strFileName = dtDocDetails.Rows(0).Item("VcFileName")
    '        '            'End If
    '        '            strDocName = Replace(dtDocDetails.Rows(0).Item("VcFileName"), "../documents/docs/", "")
    '        '        End If
    '        '        'ElseIf radCont.Checked = True Then
    '        '        '    If ddlDocumentsCon.SelectedValue > 0 Then
    '        '        '        Dim dtDocDetails As DataTable
    '        '        '        Dim objDocuments As New DocumentList
    '        '        '        With objDocuments
    '        '        '            .RecID = ddlDocumentsCon.SelectedValue
    '        '        '            dtDocDetails = .GetSpecficDocDtl
    '        '        '        End With
    '        '        '        Dim strfile As String = dtDocDetails.Rows(0).Item("VcFileName")
    '        '        '        strFileName = Replace(strfile, "..", "../../" & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
    '        '        '        strDocName = Replace(dtDocDetails.Rows(0).Item("VcFileName"), "../documents/docs/", "")
    '        '        '    End If
    '        '        'ElseIf radOpp.Checked = True Then
    '        '        '    If ddlDocumentOpp.SelectedValue > 0 Then
    '        '        '        Dim dtDocDetails As DataTable
    '        '        '        Dim objDocuments As New DocumentList
    '        '        '        With objDocuments
    '        '        '            .RecID = ddlDocumentOpp.SelectedValue
    '        '        '            dtDocDetails = .GetSpecficDocDtl
    '        '        '        End With
    '        '        '        ' Dim strFile As String = dtDocDetails.Rows(0).Item("VcFileName")
    '        '        '        ' Replace(e.Item.Cells(2).Text, "..", "../../" & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
    '        '        '        Dim strfile As String = dtDocDetails.Rows(0).Item("VcFileName")
    '        '        '        strFileName = Replace(strfile, "..", "../../" & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
    '        '        '        strDocName = Replace(dtDocDetails.Rows(0).Item("VcFileName"), "../documents/docs/", "")
    '        '        '    End If

    '        '        strFileType = "application/octet-stream"
    '        '    End If
    '        'ElseIf radUpload.Checked = True Then
    '        '    UploadFile()
    '        'End If


    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
    'Private Sub ddlCategoryOpp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategoryOpp.SelectedIndexChanged
    '    Try
    '        Dim objDocuments As New DocumentList
    '        Dim dtTableOpp As DataTable

    '        objDocuments.byteMode = 1
    '        objDocuments.DocCategory = ddlCategoryOpp.SelectedValue
    '        objDocuments.DivisionID = GetQueryStringVal( "DivId")
    '        objDocuments.ContactID = GetQueryStringVal( "ContId")
    '        dtTableOpp = objDocuments.GetDocumentsAttachment

    '        ddlDocumentOpp.DataSource = dtTableOpp
    '        ddlDocumentOpp.DataTextField = "vcDocName"
    '        ddlDocumentOpp.DataValueField = "numSpecificDocID"
    '        ddlDocumentOpp.DataBind()
    '        ddlDocumentOpp.Items.Insert(0, "--Select One--")
    '        ddlDocumentOpp.Items.FindByText("--Select One--").Value = 0
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub LoadDocuments()
    '    Try
    '        Dim objDocuments As New DocumentList
    '        Dim dtDocuments As DataTable
    '        objDocuments.DocCategory = ddldocumentsCtgr.SelectedValue
    '        objDocuments.DomainID = Session("DomainID")
    '        dtDocuments = objDocuments.GetGenericDocList
    '        ddldocuments.DataSource = dtDocuments
    '        ddldocuments.DataTextField = "vcdocname"
    '        ddldocuments.DataValueField = "numGenericDocid"
    '        ddldocuments.DataBind()
    '        ddldocuments.Items.Insert(0, "--Select One--")
    '        ddldocuments.Items.FindByText("--Select One--").Value = 0
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ddldocumentsCtgr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddldocumentsCtgr.SelectedIndexChanged
    '    Try
    '        LoadDocuments()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub lkbItemSelected_Click(sender As Object, e As EventArgs) Handles lkbItemSelected.Click
    '    Try
    '        'hdnCurrentSelectedItem.Value
    '        Dim objItem As New BACRM.BusinessLogic.Item.CItems
    '        objItem.ItemCode = hdnCurrentSelectedItem.Value
    '        objItem.DomainID = Session("DomainId")

    '        Dim dtImage As DataTable
    '        dtImage = objItem.ImageItemDetails()

    '        radItemDocuments.DataSource = dtImage
    '        radItemDocuments.DataTextField = "vcPathForImage"
    '        radItemDocuments.DataValueField = "numItemImageId"
    '        radItemDocuments.DataBind()

    '        radItemDocuments.ShowDropDownOnTextboxClick = True
    '        radItemDocuments.CheckBoxes = True
    '        radItemDocuments.EnableTextSelection = True
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub lkbItemRemoved_Click(sender As Object, e As EventArgs) Handles lkbItemRemoved.Click
    '    Try
    '        radItemDocuments.Items.Clear()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub radCmbDoc_ItemsRequested(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles radCmbDoc.ItemsRequested
    '    If e.Text.Trim().Length > 1 Then
    '        Dim objDocuments As New DocumentList
    '        objDocuments.DomainID = Session("DomainID")
    '        objDocuments.input = e.Text.Trim
    '        Dim dtDocs As DataTable = objDocuments.GetDocListForAttachment

    '        radCmbDoc.DataSource = dtDocs
    '        radCmbDoc.DataTextField = "vcDocName"
    '        radCmbDoc.DataValueField = "DocID"
    '        radCmbDoc.DataBind()
    '    End If
    'End Sub
#End Region

End Class
