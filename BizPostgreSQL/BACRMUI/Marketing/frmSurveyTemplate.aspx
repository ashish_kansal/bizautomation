﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSurveyTemplate.aspx.vb"
    Inherits=".frmSurveyTemplate" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/JavaScript">

        function Save() {
            if (document.form1.ddlSurvey.value == "0") {
                alert("Select Survey")
                document.form1.ddlSurvey.focus()
                return false;
            }
            return true;
        }
        function OnClientCommandExecuting(editor, args) {
            var name = args.get_name();
            var val = args.get_value();
            if (name == "BizDocElement" || name == "CustomField") {
                editor.pasteHtml(val);
                //Cancel the further execution of the command as such a command does not exist in the editor command list
                args.set_cancel(true);
            }
        }
    </script>
     <style>
        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="javascript:return Save();">
            </asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="btn btn-danger" Text="Close" >
            </asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Modify Survey Design
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
        runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table width="100%" border="0" class="table table-responsive tblNoBorder">
                    <tr>
                        <td class="text" align="right">
                            Survey<font color="#ff0000">*</font>
                        </td>
                        <td colspan="3" align="left" class="text">
                            <asp:DropDownList ID="ddlSurvey" runat="server" CssClass="form-control" AutoPostBack="true" style="max-width:200px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="text" align="right">
                            Page Type<font color="#ff0000">*</font>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlPageType" CssClass="form-control" AutoPostBack="true" style="max-width:200px">
                                <asp:ListItem Text="Registration Page" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Landing Page" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Survey Page" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right" valign="top">
                            Template HTML
                        </td>
                        <td colspan="3">
                            <telerik:RadEditor ID="RadEditor1" runat="server" style="max-width:100%" Height="515px" OnClientPasteHtml="OnClientPasteHtml"
                                OnClientCommandExecuting="OnClientCommandExecuting" ToolsFile="~/Marketing/EditorTools.xml"
                                ClientIDMode="Static">
                                <Content>
        
                                </Content>
                                <Tools>
                                    <telerik:EditorToolGroup>
                                        <telerik:EditorDropDown Name="CustomField" Text="Custom Field">
                                        </telerik:EditorDropDown>
                                    </telerik:EditorToolGroup>
                                </Tools>
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td class="text" align="right">
                            Select Style
                        </td>
                        <td colspan="2">
                            <asp:CheckBoxList ID="cblStyle" runat="server">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td class="text" align="right">
                            Select JavaScript
                        </td>
                        <td colspan="2">
                            <asp:CheckBoxList ID="cblJS" runat="server">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
