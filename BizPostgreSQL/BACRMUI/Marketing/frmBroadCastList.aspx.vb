Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Partial Class frmBroadCastList
    Inherits BACRMPage
    Dim objCommon As CCommon
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Table1 As System.Web.UI.WebControls.Table
    Dim strColumn As String

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Events"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                GetEmailDetail()
                GetUserRightsForPage(6, 5)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                'txtCurrrentPage.Text = 1
                BindDatagrid()
                'ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('3');}else{ parent.SelectTabByValue('3'); } ", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub dgBroadCast_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBroadCast.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objBroadCast As New Campaign
                objBroadCast.broadcastID = e.CommandArgument
                objBroadCast.DeleteBroadcastHistory()
                BindDatagrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub dgBroadCast_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgBroadCast.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If txtSortColumn.Text.Trim() <> strColumn Then
                txtSortColumn.Text = strColumn
                txtSortOrder.Text = "A"
            Else
                If txtSortOrder.Text = "A" Then
                    txtSortOrder.Text = "D"
                Else
                    txtSortOrder.Text = "A"
                End If
            End If

            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub dgBroadCast_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBroadCast.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(1).Attributes.Add("onclick", "return OpenWindow(" & e.Item.Cells(0).Text & ")")
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")

                Dim hlView As HyperLink
                hlView = e.Item.FindControl("hlView")
                hlView.Attributes.Add("onClick", "return OpenWindowView(" & e.Item.Cells(0).Text & ")")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
#End Region

#Region "Functions"
    Sub GetEmailDetail()
        Try
            Dim dtTable As DataTable
            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.Mode = 24
            objCommon.Str = 0
            Dim strEmailInfo As String()
            Dim strEmailData As String
            strEmailData = CCommon.ToString(objCommon.GetSingleFieldValue())

            If strEmailData <> "" Then
                strEmailInfo = CCommon.ToString(objCommon.GetSingleFieldValue()).Split("-")
                If strEmailInfo.Length > 0 Then
                    If CCommon.ToLong(strEmailInfo(0)) = CCommon.ToLong(strEmailInfo(1)) Then
                        lblLeft.Text = "0/0"
                        lblSent.Text = "0"
                    Else
                        lblLeft.Text = CCommon.ToString((CCommon.ToLong(strEmailInfo(1)) - CCommon.ToLong(strEmailInfo(0)))) + "/" + strEmailInfo(1)
                        lblSent.Text = strEmailInfo(0)
                    End If
                End If
            Else
                lblLeft.Text = "0/0"
                lblSent.Text = "0"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            Dim dtBroadCast As DataTable
            Dim objBroadCast As New Campaign
            With objBroadCast
                .DomainID = Session("DomainId")
                .UserCntID = Session("UserContactID")

                If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                .CurrentPage = txtCurrrentPage.Text.Trim()

                .PageSize = Session("PagingRows")
                .TotalRecords = 0

                If txtSortColumn.Text <> "" Then
                    .columnName = txtSortColumn.Text
                Else : .columnName = "bintBroadCastDate"
                End If

                If txtSortOrder.Text = "D" Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If

            End With

            dtBroadCast = objBroadCast.GetBroadcastList

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objBroadCast.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text

            dgBroadCast.DataSource = dtBroadCast
            dgBroadCast.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Function ReturnName(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            strTargetResolveDate = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), CloseDate), Session("DateFormat"))
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class
