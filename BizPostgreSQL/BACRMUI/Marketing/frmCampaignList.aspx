<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCampaignList.aspx.vb"
    Inherits="BACRM.UserInterface.Marketing.frmCampaignList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Campaign List</title>
    <style type="text/css">
        #gvCampaign {
            margin: 0 auto;
        }

            #gvCampaign th, #gvCampaign td {
                border-right: 0px;
                border-left: 0px;
            }

            #gvCampaign td {
                padding-left: 20px;
                padding-right: 20px;
            }

            #gvCampaign tr td:first-child {
                background-color: inherit;
            }

            #gvCampaign td {
                background-color: #f5f5f5;
            }

                #gvCampaign td:nth-child(even) {
                    background-color: #f5f5f5;
                }
    </style>
    <script type="text/javascript">
        function ShowFirstOrders(divisionIDs) {
            $("[id$=hdnDivisionIDs]").val(divisionIDs);
            $("[id$=btnShowOrders]").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline" id="tdCamapign" runat="server">
                    <div class="form-group">
                        <label>View:</label>
                        <asp:DropDownList ID="ddlColumnType" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlColumnType_SelectedIndexChanged">
                            <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Quarter" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Year" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>
                            Campaign:
                        </label>
                        <asp:DropDownList ID="ddlCampaign" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlCampaign_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <div class="form-group">
                        <label>From</label>
                        <telerik:RadDatePicker runat="server" ID="calFrom" DateInput-CssClass="form-control" Width="100" />
                    </div>
                    <div class="form-group">
                        <label>To</label>
                        <telerik:RadDatePicker ID="calTo" runat="server" DateInput-CssClass="form-control" Width="100" />
                    </div>
                    <asp:Button ID="btnUpdate" runat="server" Text="Go" CssClass="btn btn-primary" OnClick="btnUpdate_Click"></asp:Button>
                    <asp:Button ID="btnShowOrders" runat="server" Style="display: none" OnClick="btnShowOrders_Click" />
                    <asp:HiddenField runat="server" ID="hdnDivisionIDs" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server" ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Campaigns&nbsp;<a href="#" onclick="return OpenHelpPopUp('marketing/frmcampaignlist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView runat="server" ID="gvCampaign" UseAccessibleHeader="true" ShowHeaderWhenEmpty="true" CssClass="table table-bordered" Width="5%" AutoGenerateColumns="false">
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No Data Available
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-orders" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">�</span>
                    </button>
                    <h4 class="modal-title">Sales Order(s)</h4>
                </div>
                <div class="modal-body">
                    <asp:GridView ID="gvOrders" runat="server" UseAccessibleHeader="true" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" CssClass="table table-bordered table-striped">
                        <Columns>
                            <asp:BoundField HeaderText="Organization Name (Date Created)" DataField="vcCOmpanyName" />
                            <asp:BoundField HeaderText="Sales Order (Date Created)" DataField="vcPOppName" />
                            <asp:BoundField HeaderText="Amount" DataField="monDealAmount" />
                            <asp:BoundField HeaderText="Record Owner" DataField="vcRecordOwner" />
                            <asp:BoundField HeaderText="Assignee" DataField="vcAssignee" />
                            <asp:BoundField HeaderText="Days it took to convert" DataField="intDaysTookToConvert" />
                        </Columns>
                        <EmptyDataTemplate>
                        No Data Available
                    </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
