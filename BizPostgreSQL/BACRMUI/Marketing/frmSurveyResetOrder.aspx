﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSurveyResetOrder.aspx.vb" Inherits=".frmSurveyResetOrder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link runat="server" rel="stylesheet" href="~/CSS/Import.css" type="text/css" id="AdaptersInvariantImportCSS" />

        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 207px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
 <asp:literal id="litClientMessage" Runat="server" EnableViewState="False"></asp:literal>
 	    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
			<table cellSpacing="0" cellPadding="0" 
        style="width: 42%; height: 25px;">
				<tr>
					<td vAlign="bottom" class="style1" height="23">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Reorder Questions 
									
								</td>
								
							</tr>
							</table>
							</td>
							
							<td class="normal1" align="right" height="23"><asp:button   id="btnDone" Runat="Server" cssClass="button" Text="Done"></asp:button>&nbsp<asp:button id="btnClose" Runat="Server" cssClass="button"  OnClientClick="javascript:CloseMe();"  Text="Close"></asp:button>&nbsp;
						</td>
				</tr>
			</table>
		
			<asp:table id="tblSelectField" Width="33%" GridLines="None" BorderColor="black" 
                        BorderWidth="1" CssClass="aspTable"
				Runat="server" Height="264px">
    <asp:TableRow  style="width: 555px">
    
    <asp:TableCell >
    
        <ajaxToolkit:ReorderList runat ="server"  ID="reorderList" Height="59px"  
            AllowReorder="true" LayoutType="Table" DragHandleAlignment ="Left" 
            Width="500px" BorderStyle="None" >
            <ItemTemplate>
                <span style="cursor: pointer;">
                    <asp:Label ID="ItemName" runat="server" 
                               Text='<%# Container.DataItem("vcQuestion") %>' />
                </span>
            </ItemTemplate>
        </ajaxToolkit:ReorderList>
    </asp:TableCell>
    
</asp:TableRow> 
    </asp:table> 
    </form>
</body>
</html>
