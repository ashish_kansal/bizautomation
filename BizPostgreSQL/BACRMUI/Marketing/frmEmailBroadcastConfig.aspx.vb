﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports System.Net
Imports System.IO

Public Class frmEmailBroadcastConfig
    Inherits BACRMPage
#Region "Global Declaration"

    Private strIP As String = CCommon.ToString(ConfigurationManager.AppSettings("EmailBroadcastServerIP"))
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try


            If Not IsPostBack Then

                If Me.ConfigurationID > 0 Then

                    Dim objCampaign As New Campaign
                    Dim dtTable As DataTable
                    objCampaign.ConfigurationId = Me.ConfigurationID
                    objCampaign.DomainID = CCommon.ToString(Session("DomainId"))
                    objCampaign.Mode = 2

                    dtTable = objCampaign.GetEmailBroadcastConfiguration()

                    If dtTable.Rows.Count > 0 Then
                        BindData(dtTable)

                    End If

                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnBroadcast_Click(sender As Object, e As System.EventArgs) Handles btnBroadcast.Click
        Try

            Dim objCampaign As New Campaign
            Dim dtTable As DataTable

            If Me.ConfigurationID > 0 Then

                Dim strCheckDomain As String
                Try
                    Dim IP As IPAddress() = Dns.GetHostAddresses("www." + txtDomain.Text)
                    strCheckDomain = ""
                Catch ex As Exception
                    strCheckDomain = ex.Message.ToString()
                End Try
                If strCheckDomain = "" Then
                    objCampaign.ConfigurationId = Me.ConfigurationID
                    objCampaign.Mode = 2
                    dtTable = objCampaign.GetEmailBroadcastConfiguration()

                    If dtTable.Rows.Count > 0 Then
                        UpdateBroadcastDetail()

                        objCampaign.ConfigurationId = Me.ConfigurationID
                        objCampaign.Mode = 2
                        dtTable = objCampaign.GetEmailBroadcastConfiguration()

                        If dtTable.Rows.Count > 0 Then
                            BindData(dtTable)
                        End If

                    End If
                Else
                    lblMessage.Text = "Domain Name does not exist.Enter valid Domain Name."
                    txtDomain.Focus()
                End If
            Else

                'objCampaign.DomainID = CCommon.ToString(Session("DomainId"))
                'objCampaign.Mode = 1

                'dtTable = objCampaign.GetEmailBroadcastConfiguration()
                Dim strCheckDomain As String
                Try
                    Dim IP As IPAddress() = Dns.GetHostAddresses("www." + txtDomain.Text)
                    strCheckDomain = ""
                Catch ex As Exception
                    strCheckDomain = ex.Message.ToString()
                End Try
                If strCheckDomain = "" Then

                    ' Dim strPhysicalPath As String = CCommon.GetDocumentPhysicalPath(CCommon.ToString(Session("DomainID")))
                    If CheckForDomainName(txtFrom.Text, txtDomain.Text) Then
                        lblMessage.Text = "Dublicate record found for From Address ."
                    Else
                        With objCampaign

                            .DomainID = Session("DomainID")
                            .FromName = txtFrom.Text
                            .DomainName = txtDomain.Text
                            .DkimPrivateKey = txtPrivateKey.Text
                            .DkimPublicKey = txtPublicKey.Text

                            .ManageEmailBroadcastConfiguration()
                            Me.ConfigurationID = .ConfigurationId
                        End With

                        objCampaign.Mode = 2
                        objCampaign.ConfigurationId = Me.ConfigurationID
                        dtTable = objCampaign.GetEmailBroadcastConfiguration()

                        If dtTable.Rows.Count > 0 Then
                            BindData(dtTable)
                        End If

                        UpdateBroadcastDetail()

                        lblSelector.Text = hdnSelector.Value
                        lblMessage.Text = ""

                        objCampaign.Mode = 2
                        objCampaign.ConfigurationId = Me.ConfigurationID
                        dtTable = objCampaign.GetEmailBroadcastConfiguration()
                        If dtTable.Rows.Count > 0 Then
                            BindData(dtTable)
                        End If
                    End If
                Else
                    lblMessage.Text = "Domain Name does not exist.Enter valid Domain Name."
                    txtDomain.Focus()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../Marketing/frmEmailBroadcastConfigDetail.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region
#Region "Function"
    Sub UpdateBroadcastDetail()
        Try
            ' Dim strPhysicalPath As String = CCommon.GetDocumentPhysicalPath(CCommon.ToString(Session("DomainID")))
            If Not CheckForDomainName(txtFrom.Text, txtDomain.Text) Then
                hdnSelector.Value = "bizautomationmail._domainkey." + txtDomain.Text
                Dim objCampaign As New Campaign
                With objCampaign
                    .ConfigurationId = Me.ConfigurationID
                    .DomainID = Session("DomainID")
                    .FromName = txtFrom.Text
                    .DomainName = txtDomain.Text
                    .DkimPrivateKey = txtPrivateKey.Text.Replace(vbCr, "").Replace(vbLf, "")
                    .DkimPublicKey = txtPublicKey.Text.Replace(vbCr, "").Replace(vbLf, "")
                    .SpfPass = CheckForSPF()
                    .DkimPass = CheckForDKIM(txtPublicKey.Text.Replace("-----BEGIN PUBLIC KEY-----", "").Replace("-----END PUBLIC KEY-----", "").Replace(vbCr, "").Replace(vbLf, ""))
                    .DomainKeyPass = CheckForDomainKey()
                    '.SenderIdPass = CheckForSPF()
                    If .DkimPass = True And .DomainKeyPass = True And .SpfPass = True Then
                        .Enabled = True
                    Else
                        .Enabled = False
                    End If
                    .ManageEmailBroadcastConfiguration()
                End With
                lblSelector.Text = hdnSelector.Value
            Else
                lblMessage.Text = "Dublicate record found for From Address ."
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function CheckForSPF() As Boolean
        Try
            Dim objCampaign As New Campaign
            If txtDomain.Text <> "" Then
                Dim checkSPF As Boolean = False
                checkSPF = objCampaign.CheckSPF(txtDomain.Text, strIP)
                If checkSPF = True Then
                    lblSpfCheck.Text = "Pass"
                    Return True
                Else
                    lblSpfCheck.Text = "Fail"
                    Return False
                End If
            Else
                lblMessage.Text = "Enter Domain Name."
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function CheckForDKIM(ByVal PublicKey As String) As Boolean
        Try
            If txtDomain.Text <> "" Then
                'DNS cache states the domain 
                Dim objCampaign As New Campaign
                objCampaign.DomainID = Session("DomainID")
                Dim checkDKIM As Boolean = False
                checkDKIM = objCampaign.CheckDKIM(hdnSelector.Value, PublicKey)
                If checkDKIM = True Then
                    lblDkimCheck.Text = "Pass"
                    Return True
                Else
                    lblDkimCheck.Text = "Fail"
                    Return False
                End If
                lblSelector.Text = hdnSelector.Value
            Else
                lblMessage.Text = "Enter Domain Name."
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function CheckForDomainKey() As Boolean
        Try
            If txtDomain.Text <> "" Then
                'DNS cache states the domain 
                Dim objCampaign As New Campaign

                objCampaign.DomainID = Session("DomainID")
                Dim checkDKIM As Boolean = False
                checkDKIM = objCampaign.CheckDomainKey("_domainkey." + txtDomain.Text)
                If checkDKIM = True Then
                    lblDkimCheck.Text = "Pass"
                    Return True
                Else
                    lblDkimCheck.Text = "Fail"
                    Return False
                End If
                lblSelector.Text = hdnSelector.Value
            Else
                lblMessage.Text = "Enter Domain Name."
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function BindData(ByVal dtTable As DataTable)
        Try
            txtFrom.Text = CCommon.ToString(dtTable.Rows(0)("vcFromName"))
            txtDomain.Text = CCommon.ToString(dtTable.Rows(0)("vcDomainName"))
            txtPublicKey.Text = CCommon.ToString(dtTable.Rows(0)("vcDkimPublicKey"))
            txtPrivateKey.Text = CCommon.ToString(dtTable.Rows(0)("vcDkimPrivateKey"))

            If CCommon.ToBool(dtTable.Rows(0)("bitSpfPass")) = True Then
                lblSenderIDCheck.Text = "Pass"
                lblSpfCheck.Text = "Pass"
            Else
                lblSenderIDCheck.Text = "Fail"
                lblSpfCheck.Text = "Fail"
            End If

            If CCommon.ToBool(dtTable.Rows(0)("bitDkimPass")) = True Then
                lblDkimCheck.Text = "Pass"
            Else
                lblDkimCheck.Text = "Fail"
            End If

            If CCommon.ToBool(dtTable.Rows(0)("bitDomainKeyPass")) = True Then
                lblDomainKeyCheck.Text = "Pass"
            Else
                lblDomainKeyCheck.Text = "Fail"
            End If
            lblSelector.Text = "bizautomationmail_domainkey." + CCommon.ToString(dtTable.Rows(0)("vcDomainName"))
            lblSPF.Text = "v=spf1 ip4:" & strIP & " ~all"
            lblDomainKey.Text = "o=-;t=y"

            If CCommon.ToString(dtTable.Rows(0)("vcDkimPublicKey")) <> "" Then
                txtDkim.Text = String.Format("k=rsa;p={0}", CCommon.ToString(dtTable.Rows(0)("vcDkimPublicKey")).Replace("-----BEGIN PUBLIC KEY-----", "").Replace("-----END PUBLIC KEY-----", "").Replace(vbCr, "").Replace(vbLf, ""))
            End If
            lblSpfHost.Text = "@"
            lblDkimHost.Text = "bizautomationmail._domainkey." + CCommon.ToString(dtTable.Rows(0)("vcDomainName"))
            hdnSelector.Value = "bizautomationmail._domainkey." + CCommon.ToString(dtTable.Rows(0)("vcDomainName"))
            lblDomainAdd.Text = CCommon.ToString(dtTable.Rows(0)("vcDomainName"))
            lblSelectorAdd.Text = "bizautomationmail"
            lblDomainKeyHost.Text = "_domainkey." + CCommon.ToString(dtTable.Rows(0)("vcDomainName"))
            If CCommon.ToString(dtTable.Rows(0)("bitEnabled")) = "True" Then
                pnlMessage.CssClass = "successInfo"
                lblMessage.Text = "Your broadcasting details are verified."
                hlEmailBroadcast.Visible = True
            Else
                pnlMessage.CssClass = "warning"
                lblMessage.Text = "Your broadcasting details are not varified.you can not broadcast your E-mail."
                hlEmailBroadcast.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function CheckForDomainName(ByVal FromName As String, ByVal DomainName As String) As Boolean
        Try

            Dim objCampaign As New Campaign
            Dim dtTable As DataTable
            objCampaign.DomainID = CCommon.ToString(Session("DomainId"))
            objCampaign.ConfigurationId = Me.ConfigurationID
            objCampaign.Mode = 3
            dtTable = objCampaign.GetEmailBroadcastConfiguration()
            If dtTable.Rows.Count > 0 Then

                Dim dRows As DataRow() = dtTable.Select("vcFromName = '" & FromName & "' and vcDomainName = '" & DomainName & "'")

                If dRows.Length > 0 Then
                    Return True
                End If
            End If

            Return False

        Catch ex As Exception
            Throw ex
        End Try

    End Function
#End Region
#Region "Custom Property"
    Public Property ConfigurationID() As Long
        Get
            Dim o As Object = ViewState("ConfigurationID")
            If Not o Is Nothing Then
                Return CType(o, Long)
            Else
                If GetQueryStringVal("ConfigurationID") <> "" Then
                    ViewState("ConfigurationID") = GetQueryStringVal("ConfigurationID")
                    Return CType(ViewState("ConfigurationID"), Long)
                Else
                    Return 0
                End If
            End If
        End Get
        Set(ByVal Value As Long)
            ViewState("ConfigurationID") = Value
        End Set
    End Property
#End Region

End Class