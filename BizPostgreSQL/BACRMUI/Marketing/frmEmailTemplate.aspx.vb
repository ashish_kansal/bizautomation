Imports BACRM.BusinessLogic.Marketing
Imports System.IO
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities

Partial Public Class frmEmailTemplate
    Inherits BACRMPage

    Dim AlertDTLID As Long = 0
    Dim objCamp As Campaign
    Dim lngDocID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'oEditHtml.UploadedFilesDirectory = ConfigurationManager.AppSettings("StateAndCountryList")
            If GetQueryStringVal("AlertDTLID") <> "" Then AlertDTLID = GetQueryStringVal("AlertDTLID")
            If Not IsPostBack Then
                txtTemplateName.Focus()
                BindEmailMergeModule()
                BindBizDocsCategory()
                PopulateGroups()
                BindFollowUpStatus()
                hdnURLRefferer.Value = CCommon.ToString(Request.UrlReferrer)

                ddlEmailMergeModule.Attributes.Add("onChange", "return Confirm(this);")
            End If

            btnSaveClose.Attributes.Add("onclick", "return Save()")
            'Set Image Manage and Template Manager
            Campaign.SetRadEditorPath(RadEditor1, CCommon.ToLong(Session("DomainID")), Page)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindFollowUpStatus()
        Try
            Dim dt As New DataTable()
            ddlFollowupStatus.Items.Clear()
            objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
            objCommon.ListID = CCommon.ToLong(30)
            dt = objCommon.GetMasterListItemsWithRights()
            Dim item As ListItem = New ListItem("-- All --", "0")
            ddlFollowupStatus.Items.Add(item)
            ddlFollowupStatus.AutoPostBack = False
            For Each dr As DataRow In dt.Rows
                item = New ListItem(CCommon.ToString(dr("vcData")), CCommon.ToString(dr("numListItemID")))
                item.Attributes("OptionGroup") = CCommon.ToString(dr("vcListItemGroupName"))
                item.Attributes("class") = CCommon.ToString(dr("vcColorScheme"))
                ddlFollowupStatus.Items.Add(item)
            Next
        Catch ex As Exception

        End Try
    End Sub
    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            Dim strScript As String = "<script language=JavaScript>"
            If hdnURLRefferer.Value.ToLower.IndexOf("frmdoclist") = -1 Then
                strScript += "self.close();"
            Else
                strScript += "window.opener.location.href='../Documents/frmDocuments.aspx?DocId=" & lngDocID.ToString & "&frm=DocList&SI1=0&SI2=369&TabID=1'; self.close(); "
            End If
            strScript += "</script>"

            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub Save()
        Try
            If CCommon.ToLong(Session("DomainId")) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            Dim objDocuments As New DocumentList
            With objDocuments
                .DomainID = Session("DomainId")
                .UserCntID = Session("UserContactID")
                .UrlType = ""
                .DocCategory = 369
                .numCategoryId = ddlCategory.SelectedValue
                .bitLastFollowupUpdate = chkLastFollowUpDate.Checked
                .numFollowUpStatusId = ddlFollowupStatus.SelectedValue
                .bitUpdateFollowupStatus = chkFollowUpStatus.Checked
                .FileType = ""
                .DocName = txtTemplateName.Text
                .Subject = txtSubject.Text
                .DocDesc = RadEditor1.Content
                .vcGroupsPermission = hdnGroups.Value
                If forMarketingDept.Checked Then
                    .DocumentSection = "M"
                End If
                .DocumentType = 1 '1=generic,2=specific
                .ModuleID = ddlEmailMergeModule.SelectedValue
                'Dim arrOutPut As String()
                'arrOutPut = .SaveDocuments()
                If (CCommon.ToLong(ddlEmailMergeModule.SelectedValue) = 8) Then
                    .BizDocOppType = ddlBizDocOppType.SelectedValue
                    .BizDocType = ddlBizDocType.SelectedValue
                    .BizDocTemplate = ddlBizDocTemplate.SelectedValue
                Else
                    .BizDocOppType = ""
                    .BizDocType = ""
                    .BizDocTemplate = ""
                End If
                Dim arrOutPut As String()
                arrOutPut = .SaveDocumentsForEmailTemplate()
                lngDocID = CCommon.ToLong(arrOutPut(0))
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub PopulateGroups()
        Try
            Dim objUserAccess As UserAccess
            objUserAccess = New UserAccess
            objUserAccess.SelectedGroupTypes = "1,4"
            objUserAccess.DomainID = Session("DomainID")
            ddlGroups.DataTextField = "vcGroupName"
            ddlGroups.DataValueField = "numGroupID"
            ddlGroups.DataSource = objUserAccess.GetGroups
            ddlGroups.DataBind()
            ddlGroups.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindEmailMergeModule()
        Try
            Dim objDocument As New DocumentList
            Dim dtTable As DataTable
            objDocument.ModuleID = 0
            objDocument.byteMode = 1
            dtTable = objDocument.GetMergeFieldsByModuleID()

            ddlEmailMergeModule.DataSource = dtTable
            ddlEmailMergeModule.DataTextField = "vcModuleName"
            ddlEmailMergeModule.DataValueField = "numModuleID"
            ddlEmailMergeModule.DataBind()
            ddlEmailMergeModule.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlBizDocType_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindBizDocsTemplate()
        BindFollowUpStatus()
    End Sub

    Protected Sub radcmbBizDocMergeFields_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        BindFollowUpStatus()

        If (radcmbBizDocMergeFields.SelectedValue = "##BizDoc Template Merge fields without HTML & CSS##") Then
            FillMergeFieldsForBizDocs()
        ElseIf (radcmbBizDocMergeFields.SelectedValue = "##BizDocTemplateFromGlobalSettings##") Then
            RadEditor1.Content = ""
            radcmbCustomField.Visible = False
            RadEditor1.Content = "##BizDocTemplateFromGlobalSettings##"
        End If
    End Sub

    Protected Sub ddlBizDocTemplate_SelectedIndexChanged(sender As Object, e As EventArgs)
        If (radcmbBizDocMergeFields.SelectedValue = "##BizDocTemplateFromGlobalSettings##") Then
            RadEditor1.Content = "##BizDocTemplateFromGlobalSettings##"
        ElseIf (radcmbBizDocMergeFields.SelectedValue = "##BizDoc Template Merge fields without HTML & CSS##") Then
            FillMergeFieldsForBizDocs()
        End If
        BindFollowUpStatus()
    End Sub

    Protected Sub ddlBizDocOppType_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindBizDocsType()
        BindFollowUpStatus()
    End Sub
    Private Sub BindCustomField()
        Try
            If ddlEmailMergeModule.SelectedItem.Text.Contains("Leads/Prospects/Accounts") Then
                Dim dtCustFld As DataTable
                Dim objCusField As New CustomFields()
                objCusField.DomainID = Session("DomainID")
                objCusField.locId = 45
                dtCustFld = objCusField.CustomFieldList
                For Each row As DataRow In dtCustFld.Rows
                    Dim Item As New RadComboBoxItem()
                    Item.Text = row("Fld_label").ToString()
                    Item.Value = "#" & row("Fld_label").ToString.Trim.Replace(" ", "") & "#"
                    radcmbCustomField.Items.Add(Item)
                    Item.DataBind()
                Next
            ElseIf ddlEmailMergeModule.SelectedItem.Text.Contains("Opportunity/Order") Then
                Dim dtCustFld As DataTable
                Dim objCusField As New CustomFields()
                objCusField.DomainID = Session("DomainID")
                objCusField.locId = 26 ' Sales and Purchase Combined
                dtCustFld = objCusField.CustomFieldList
                For Each row As DataRow In dtCustFld.Rows
                    Dim Item As New RadComboBoxItem()
                    Item.Text = row("Fld_label").ToString()
                    Item.Value = "#" & row("Fld_label").ToString.Trim.Replace(" ", "") & "#"
                    radcmbCustomField.Items.Add(Item)
                    Item.DataBind()
                Next

            ElseIf ddlEmailMergeModule.SelectedItem.Text.Contains("BizDocs") Then
                Dim dtCustFld As DataTable
                Dim objCusField As New CustomFields()
                objCusField.DomainID = Session("DomainID")
                objCusField.locId = 8
                dtCustFld = objCusField.CustomFieldList
                For Each row As DataRow In dtCustFld.Rows
                    Dim Item As New RadComboBoxItem()
                    Item.Text = row("Fld_label").ToString()
                    Item.Value = "#" & row("Fld_label").ToString.Trim.Replace(" ", "") & "#"
                    radcmbCustomField.Items.Add(Item)
                    Item.DataBind()
                Next
            End If
            radcmbCustomField.Items.Insert(0, New RadComboBoxItem("--Add Custom Merge Field--", "0"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub BindEmailTemplateMergeFields(ByVal objDocuments As DocumentList, ByVal ModuleID As Long)
        Try
            Dim AlertDTLID As Long = 0
            Dim dtTable As DataTable
            objDocuments.ModuleID = ModuleID
            objDocuments.byteMode = 0
            dtTable = objDocuments.GetMergeFieldsByModuleID()

            radcmbMergerFields.DataSource = dtTable
            radcmbMergerFields.DataTextField = "vcMergeField"
            radcmbMergerFields.DataValueField = "vcMergeFieldValue"
            radcmbMergerFields.DataBind()
            radcmbMergerFields.Items.Insert(0, New RadComboBoxItem("--Add Default Merge Field--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindBizDocsCategory()
        Try
            objCommon.DomainID = Session("DomainID")
            objCommon.ListID = 29
            ddlCategory.Items.Clear()
            ddlCategory.DataSource = objCommon.GetMasterListItemsWithOutDefaultItemsRights
            ddlCategory.DataTextField = "vcData"
            ddlCategory.DataValueField = "numListItemID"
            ddlCategory.DataBind()
            ddlCategory.Items.Insert(0, New ListItem("--Select--", "0"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindBizDocsType()
        Try
            objCommon.DomainID = Session("DomainID")
            objCommon.BizDocType = ddlBizDocOppType.SelectedValue
            ddlBizDocType.Items.Clear()
            ddlBizDocType.DataSource = objCommon.GetBizDocType
            ddlBizDocType.DataTextField = "vcData"
            ddlBizDocType.DataValueField = "numListItemID"
            ddlBizDocType.DataBind()
            ddlBizDocType.Items.Insert(0, New ListItem("--Select BizDoc Type--", "0"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindBizDocsTemplate()
        Try
            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.BizDocId = ddlBizDocType.SelectedValue
            objOppBizDoc.OppType = ddlBizDocOppType.SelectedValue
            objOppBizDoc.byteMode = 0

            Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

            ddlBizDocTemplate.DataSource = dtBizDocTemplate
            ddlBizDocTemplate.DataTextField = "vcTemplateName"
            ddlBizDocTemplate.DataValueField = "numBizDocTempID"
            ddlBizDocTemplate.DataBind()

            ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select BizDoc Template--", 0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function CompareDatatables(strBizDocUI As String) As DataTable

        Dim dtAllElement = New DataTable()
        Dim dcName = New DataColumn("Name", GetType(String))
        Dim dcValue = New DataColumn("Value", GetType(String))
        dtAllElement.Columns.Add(dcName)
        dtAllElement.Columns.Add(dcValue)

        dtAllElement.Rows.Add("Amount Paid Popup", "#AmountPaidPopUp#")
        dtAllElement.Rows.Add("Amount Paid", "#AmountPaid#")
        dtAllElement.Rows.Add("Assignee Name", "#AssigneeName#")
        dtAllElement.Rows.Add("Assignee Email", "#AssigneeEmail#")
        dtAllElement.Rows.Add("Assignee Phone No", "#AssigneePhoneNo#")
        dtAllElement.Rows.Add("Balance Due", "#BalanceDue#")
        dtAllElement.Rows.Add("Billing Terms Name", "#BillingTermsName#")
        dtAllElement.Rows.Add("Billing Terms", "#BillingTerms#")
        dtAllElement.Rows.Add("Billing Terms-From Date", "#BillingTermFromDate#")
        dtAllElement.Rows.Add("BizDoc-Comments", "#Comments#")
        dtAllElement.Rows.Add("BizDoc Created Date", "#BizDocCreatedDate#")
        dtAllElement.Rows.Add("BizDoc ID", "#BizDocID#")
        dtAllElement.Rows.Add("BizDoc Status", "#BizDocStatus#")
        dtAllElement.Rows.Add("BizDoc Summary", "#BizDocSummary#")
        dtAllElement.Rows.Add("BizDoc Template Name", "#BizDocTemplateName#")
        dtAllElement.Rows.Add("BizDoc Type", "#BizDocType#")

        dtAllElement.Rows.Add("Change Customer/Vendor Bill To Header", "#OppOrderChangeBillToHeader#")
        dtAllElement.Rows.Add("Change Customer/Vendor Ship To Header", "#OppOrderChangeShipToHeader#")
        dtAllElement.Rows.Add("Change Employer Bill To Header", "#EmployerChangeBillToHeader(Bill To)#")
        dtAllElement.Rows.Add("Change Employer Ship To Header", "#EmployerChangeShipToHeader(Ship To)#")
        dtAllElement.Rows.Add("Currency", "#Currency#")

        dtAllElement.Rows.Add("Customer/Vendor Bill To Address Name", "#Customer/VendorBillToAddressName#")
        dtAllElement.Rows.Add("Customer/Vendor Bill To Address", "#Customer/VendorBillToAddress#")
        dtAllElement.Rows.Add("Customer/Vendor Bill To City", "#Customer/VendorBillToCity#")
        dtAllElement.Rows.Add("Customer/Vendor Bill To Company Name", "#Customer/VendorBillToCompanyName#")
        dtAllElement.Rows.Add("Customer/Vendor Bill To Country", "#Customer/VendorBillToCountry#")
        dtAllElement.Rows.Add("Customer/Vendor Bill To Postal", "#Customer/VendorBillToPostal#")
        dtAllElement.Rows.Add("Customer/Vendor Bill To State", "#Customer/VendorBillToState#")
        dtAllElement.Rows.Add("Customer/Vendor Bill To Street", "#Customer/VendorBillToStreet#")
        dtAllElement.Rows.Add("Customer/Vendor Organization Comments", "#Customer/VendorOrganizationComments#")
        dtAllElement.Rows.Add("Customer/Vendor Organization Contact Email", "#Customer/VendorOrganizationContactEmail#")
        dtAllElement.Rows.Add("Customer/Vendor Organization Contact Name", "#Customer/VendorOrganizationContactName#")
        dtAllElement.Rows.Add("Customer/Vendor Organization Contact Phone", "#Customer/VendorOrganizationContactPhone#")
        dtAllElement.Rows.Add("Customer/Vendor Organization Name", "#Customer/VendorOrganizationName#")
        dtAllElement.Rows.Add("Customer/Vendor Organization Phone", "#Customer/VendorOrganizationPhone#")

        dtAllElement.Rows.Add("Customer/Vendor Ship To Address Name", "#Customer/VendorShipToAddressName#")
        dtAllElement.Rows.Add("Customer/Vendor Ship To Address", "#Customer/VendorShipToAddress#")
        dtAllElement.Rows.Add("Customer/Vendor Ship To City", "#Customer/VendorShipToCity#")
        dtAllElement.Rows.Add("Customer/Vendor Ship To Company Name", "#Customer/VendorShipToCompanyName#")
        dtAllElement.Rows.Add("Customer/Vendor Ship To Country", "#Customer/VendorShipToCountry#")
        dtAllElement.Rows.Add("Customer/Vendor Ship To Postal", "#Customer/VendorShipToPostal#")
        dtAllElement.Rows.Add("Customer/Vendor Ship To State", "#Customer/VendorShipToState#")
        dtAllElement.Rows.Add("Customer/Vendor Ship To Street", "#Customer/VendorShipToStreet#")

        dtAllElement.Rows.Add("Deal or Order ID", "#OrderID#")
        dtAllElement.Rows.Add("Discount", "#Discount#")
        dtAllElement.Rows.Add("Due Date Change Popup", "#ChangeDueDate#")
        dtAllElement.Rows.Add("Due Date", "#DueDate#")

        dtAllElement.Rows.Add("Employer Bill To Address Name", "#EmployerBillToAddressName#")
        dtAllElement.Rows.Add("Employer Bill To Address", "#EmployerBillToAddress#")
        dtAllElement.Rows.Add("Employer Bill To City", "#EmployerBillToCity#")
        dtAllElement.Rows.Add("Employer Bill To Company Name", "#EmployerBillToCompanyName#")
        dtAllElement.Rows.Add("Employer Bill To Country", "#EmployerBillToCountry#")
        dtAllElement.Rows.Add("Employer Bill To Postal", "#EmployerBillToPostal#")
        dtAllElement.Rows.Add("Employer Bill To State", "#EmployerBillToState#")
        dtAllElement.Rows.Add("Employer Bill To Street", "#EmployerBillToStreet#")
        dtAllElement.Rows.Add("Employer Organization Name", "#EmployerOrganizationName#")
        dtAllElement.Rows.Add("Employer Organization Phone", "#EmployerOrganizationPhone#")

        dtAllElement.Rows.Add("Employer Ship To Address Name", "#EmployerShipToAddressName#")
        dtAllElement.Rows.Add("Employer Ship To Address", "#EmployerShipToAddress#")
        dtAllElement.Rows.Add("Employer Ship To City", "#EmployerShipToCity#")
        dtAllElement.Rows.Add("Employer Ship To Company Name", "#EmployerShipToCompanyName#")
        dtAllElement.Rows.Add("Employer Ship To Country", "#EmployerShipToCountry#")
        dtAllElement.Rows.Add("Employer Ship To Postal", "#EmployerShipToPostal#")
        dtAllElement.Rows.Add("Employer Ship To State", "#EmployerShipToState#")
        dtAllElement.Rows.Add("Employer Ship To Street", "#EmployerShipToStreet#")
        dtAllElement.Rows.Add("Inventory Status", "#InventoryStatus#")
        dtAllElement.Rows.Add("Logo", "#Logo#")
        dtAllElement.Rows.Add("Order Record Owner", "#OrderRecOwner#")
        dtAllElement.Rows.Add("PO/Invoice #", "#P.O.NO#")
        dtAllElement.Rows.Add("Products", "#Products#")
        dtAllElement.Rows.Add("Ship Via", "#ShippingCompany#")
        dtAllElement.Rows.Add("Total Quantity", "#TotalQuantity#")
        dtAllElement.Rows.Add("Tracking Numbers", "#TrackingNo#")
        dtAllElement.Rows.Add("Total Qyt by UOM", "#TotalQytbyUOM#")

        dtAllElement.Rows.Add("BillingContact", "#BillingContact#")
        dtAllElement.Rows.Add("ShippingContact", "#ShippingContact#")
        dtAllElement.Rows.Add("SO-Comments", "#SOComments#")
        dtAllElement.Rows.Add("Parcel Shipping Account #", "#ParcelShippingAccount#")
        dtAllElement.Rows.Add("Opp/Order Bill To Address Name", "#OppOrderBillToAddressName#")
        dtAllElement.Rows.Add("Opp/Order Bill To Address", "#OppOrderBillToAddress#")
        dtAllElement.Rows.Add("Opp/Order Bill To City", "#OppOrderBillToCity#")
        dtAllElement.Rows.Add("Opp/Order Bill To Company Name", "#OppOrderBillToCompanyName#")
        dtAllElement.Rows.Add("Opp/Order Bill To Country", "#OppOrderBillToCountry#")
        dtAllElement.Rows.Add("Opp/Order Bill To Postal", "#OppOrderBillToPostal#")
        dtAllElement.Rows.Add("Opp/Order Bill To State", "#OppOrderBillToState#")
        dtAllElement.Rows.Add("Opp/Order Bill To Street", "#OppOrderBillToStreet#")

        dtAllElement.Rows.Add("Opp/Order Ship To Address Name", "#OppOrderShipToAddressName#")
        dtAllElement.Rows.Add("Opp/Order Ship To Address", "#OppOrderShipToAddress#")
        dtAllElement.Rows.Add("Opp/Order Ship To City", "#OppOrderShipToCity#")
        dtAllElement.Rows.Add("Opp/Order Ship To Company Name", "#OppOrderShipToCompanyName#")
        dtAllElement.Rows.Add("Opp/Order Ship To Country", "#OppOrderShipToCountry#")
        dtAllElement.Rows.Add("Opp/Order Ship To Postal", "#OppOrderShipToPostal#")
        dtAllElement.Rows.Add("Opp/Order Ship To State", "#OppOrderShipToState#")
        dtAllElement.Rows.Add("Opp/Order Ship To Street", "#OppOrderShipToStreet#")
        dtAllElement.Rows.Add("Opp/Order Created Date", "#OrderCreatedDate#")

        dtAllElement.Rows.Add("Partner Source", "#PartnerSource#")
        dtAllElement.Rows.Add("Release Date", "#ReleaseDate#")
        dtAllElement.Rows.Add("Required Date", "#RequiredDate#")
        dtAllElement.Rows.Add("Customer PO#", "#CustomerPO#")

        Dim dtBizDocElement = New DataTable()
        Dim colName = New DataColumn("Name", GetType(String))
        Dim colValue = New DataColumn("Value", GetType(String))
        dtBizDocElement.Columns.Add(colName)
        dtBizDocElement.Columns.Add(colValue)

        For Each drow As DataRow In dtAllElement.Rows
            If (strBizDocUI.Contains(drow("Value").ToString())) Then
                dtBizDocElement.Rows.Add(drow.ItemArray)
            End If
        Next
        Return dtBizDocElement
    End Function

    Protected Function FillMergeFieldsForBizDocs()
        radcmbCustomField.Visible = True
        RadEditor1.Content = ""
        Dim objOppBizDoc As New OppBizDocs
        objOppBizDoc.BizDocTemplateID = CCommon.ToLong(ddlBizDocTemplate.SelectedValue)
        Dim dt As DataTable = objOppBizDoc.GetOpportunityModifiedBizDocs()

        If dt.Rows.Count > 0 Then
            Dim OppID, OppBizDocId As Long
            OppID = CCommon.ToLong(dt.Rows(0).Item("numOppID"))
            OppBizDocId = CCommon.ToLong(dt.Rows(0).Item("numOppBizDocsId"))
            Dim dtOppBiDocDtl As DataTable
            Dim objOppBizDocs As New OppBizDocs
            Dim strBizDocUI As String

            objOppBizDocs.OppBizDocId = OppBizDocId
            objOppBizDocs.OppId = OppID
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.UserCntID = Session("UserContactID")
            objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl

            If dtOppBiDocDtl.Rows(0)("bitEnabled") = True And dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString().Length > 0 Then
                strBizDocUI = HttpUtility.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString())

                Dim dtBizDocElement As DataTable = CompareDatatables(strBizDocUI)

                Dim item1 As New RadComboBoxItem
                item1.Text = "Organization Name"
                item1.Value = "##OrganizationName##"
                Dim item2 As New RadComboBoxItem
                item2.Text = "Contact First Name"
                item2.Value = "##ContactFirstName##"
                Dim item3 As New RadComboBoxItem
                item3.Text = "Contact Last Name"
                item3.Value = "##ContactLastName##"

                radcmbCustomField.Items.Clear()
                radcmbCustomField.DataSource = dtBizDocElement
                radcmbCustomField.DataTextField = "Name"
                radcmbCustomField.DataValueField = "Value"
                radcmbCustomField.DataBind()
                radcmbCustomField.Items.Insert(0, New RadComboBoxItem("--Select --"))
                radcmbCustomField.Items.Add(item1)
                radcmbCustomField.Items.Add(item2)
                radcmbCustomField.Items.Add(item3)

            End If
        End If
    End Function

    Protected Sub ddlEmailMergeModule_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            BindFollowUpStatus()
            'Dim objDocuments As New DocumentList
            'objDocuments.BindEmailTemplateMergeFields(RadEditor1, ddlEmailMergeModule.SelectedValue)
            Dim objDocument As New DocumentList
            If (ddlEmailMergeModule.SelectedValue = 8) Then
                tdBizDocOpptype.Visible = True
                tdBizDocType.Visible = True
                tdBizDocTemplate.Visible = True
                ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select BizDoc Template--", "0"))
                radcmbBizDocMergeFields.Visible = True
                radcmbMergerFields.Visible = False

            Else
                tdBizDocOpptype.Visible = False
                tdBizDocType.Visible = False
                tdBizDocTemplate.Visible = False
                radcmbBizDocMergeFields.Visible = False
                radcmbMergerFields.Visible = True
                BindEmailTemplateMergeFields(objDocument, ddlEmailMergeModule.SelectedValue)
                radcmbCustomField.Items.Clear()
                BindCustomField()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class