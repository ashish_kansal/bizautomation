<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>

<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmEmailCampaignProfileSearch.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmEmailCampaignProfileSearch" ValidateRequest="false" %>

<%@ Register TagPrefix="menu1" TagName="Menu" Src="../include/webmenu.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Email Campaign Profile Search</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="JavaScript" src="../javascript/AdvSearchScripts.js" type="text/javascript"></script>
    <script language="javascript" src="../javascript/AdvSearchColumnCustomization.js"
        type="text/javascript"></script>
    <script language="javascript" src="../javascript/Validation.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function PopupCheck() {
        }
        function CheckNumber() {
            if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                window.event.keyCode = 0;
            }
        }
        function Search() {
            var str = '';
            var SelectedValue;
            for (var i = 0; i < document.form1.lstSelectedfld.options.length; i++) {
                SelectedValue = document.form1.lstSelectedfld.options[i].value;
                str = str + SelectedValue + ','
            }
            document.form1.hdnCol.value = str;
            //alert( document.form1.hdnCol.value)
        }
        function Add() {
            var ItemID = $find('radItem').get_value();
            if (ItemID != "") {
                for (var j = 0; j < document.form1.lstSelectedfld.options.length; j++) {
                    if (ItemID == document.form1.lstSelectedfld.options[j].value) {
                        alert("Item is already Added");
                        return false;
                    }
                }
                var myOption;
                myOption = document.createElement("Option");
                myOption.text = $find('radItem').get_text();
                myOption.value = ItemID;
                document.form1.lstSelectedfld.options.add(myOption);
            }
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <menu1:Menu ID="webmenu1" runat="server"></menu1:Menu>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center" colspan="3">
                <asp:RadioButtonList ID="rdlReportType" AutoPostBack="False" runat="server" CssClass="normal1"
                    RepeatLayout="Table" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
                    <asp:ListItem Value="2">Team Selected</asp:ListItem>
                    <asp:ListItem Value="3">Territory Selected</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Email Campaign Profile Search&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td class="normal1" valign="bottom" width="100" style="padding-bottom: 5px;" runat="server"
                id="trCommerceMarketing">
            </td>
            <td class="normal1" align="right">
                Select Email Group
                <asp:DropDownList ID="ddlEmailGroup" runat="server" CssClass="normal1" AutoPostBack="true">
                </asp:DropDownList>
                &nbsp;
                <input class="button" id="btnChooseTeams" style="width: 120px" onclick="javascript: OpenSelTeam(7)"
                    type="button" value="Choose Teams" name="btnChooseTeams" runat="server" />
                <input class="button" id="btnChooseTerritories" style="width: 120px" onclick="javascript: OpenTerritory(7)"
                    type="button" value="Choose Territories" name="btnChooseTerritories" runat="server" />
                <asp:Button ID="btnSearch" runat="server" CausesValidation="True" Width="80" CssClass="ybutton"
                    Text="Search" OnClientClick="Search();"></asp:Button>
                <input class="button" id="btnCancel" onclick="javascript:Tickler();" type="reset"
                    value="Cancel" />
            </td>
        </tr>
    </table>
    <asp:Table ID="tblProspects" CssClass="aspTable" CellPadding="0" CellSpacing="0"
        BorderWidth="1" Height="300" runat="server" Width="100%" BorderColor="black"
        GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <table border="0" width="100%" id="tblSearch" runat="server">
                    <tr>
                        <td>
                        </td>
                        <td class="normal1" colspan="2">
                            <asp:CheckBox ID="cbSearchInLeads" runat="server" AutoPostBack="False" Checked="True"
                                Text="Leads "></asp:CheckBox>
                            <asp:CheckBox ID="cbSearchInProspects" runat="server" AutoPostBack="False" Checked="True"
                                Text="Prospects"></asp:CheckBox>
                            <asp:CheckBox ID="cbSearchInAccounts" runat="server" AutoPostBack="False" Checked="True"
                                Text="Accounts"></asp:CheckBox>
                        </td>
                        <td colspan="2">
                            <asp:RadioButtonList ID="rbListSearchContext" AutoPostBack="False" runat="server"
                                CssClass="normal1" RepeatLayout="Table" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Selected="True">Contains</asp:ListItem>
                                <asp:ListItem Value="2">Starting With</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td class="normal1" rowspan="2">
                            <fieldset style="width: 300px;">
                                <legend>Created
                                    <input type="checkbox" id="cbCreatedOnFilteration" runat="server" name="cbCreatedOnFilteration">
                                    Or Modified
                                    <input type="checkbox" id="cbModifiedOnFilteration" runat="server" name="cbModifiedOnFilteration">
                                </legend>&nbsp;&nbsp;&nbsp;
                                <table>
                                    <tr>
                                        <td>
                                            From
                                        </td>
                                        <td>
                                            <BizCalendar:Calendar ID="calFrom" runat="server" />
                                        </td>
                                        <td>
                                            To
                                        </td>
                                        <td>
                                            <BizCalendar:Calendar ID="calTo" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Relationship
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRelationship" CssClass="signup" runat="server" Width="156px"
                                AutoPostBack="False">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="right">
                            Group
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLead" CssClass="signup" runat="server" Width="156" AutoPostBack="False">
                                <asp:ListItem Value="0" Selected="True">All Groups</asp:ListItem>
                                <asp:ListItem Value="1">My Web Leads</asp:ListItem>
                                <asp:ListItem Value="2">Public Leads</asp:ListItem>
                                <asp:ListItem Value="5">My Leads</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <asp:Panel ID="HeaderCommerceMktg" runat="server" CssClass="normal1">
                                <br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/expand_blue.jpg" />&nbsp;&nbsp;&nbsp;<asp:Label
                                    ID="Label3" CssClass="text_bold" runat="server">Commerce Marketing </asp:Label>
                                <asp:Label ID="Label1" runat="server">(Show Details...)</asp:Label>
                            </asp:Panel>
                            <asp:Panel ID="ContentCommerceMktg" runat="server" CssClass="normal1">
                                <asp:Table ID="tblCommerceMktg" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
                                    BorderColor="black" GridLines="None">
                                    <asp:TableRow>
                                        <asp:TableCell></asp:TableCell></asp:TableRow>
                                    <asp:TableRow Style="padding-bottom: 10px;">
                                        <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:CheckBox ID="cbOptionOne" runat="server" Text="" />
                                        </asp:TableCell>
                                        <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                                            Generate a list of Accounts that haven�t bought from us in more than
                                            <asp:TextBox runat="Server" ID="txtDaysOptionOne" MaxLength="3" Width="20" CssClass="normal1"></asp:TextBox>
                                            days.
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow Style="padding-bottom: 10px;">
                                        <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:CheckBox ID="cbOptionTwo" runat="server" Text="" />
                                        </asp:TableCell>
                                        <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                                            Generate a list of Accounts that have purchased more than
                                            <asp:TextBox ID="txtDealAmount" runat="Server" CssClass="normal1" MaxLength="15"
                                                Width="50"></asp:TextBox>
                                            (combined deal won amount) within the last
                                            <asp:TextBox runat="Server" ID="txtDaysOptionTwo" MaxLength="3" Width="20" CssClass="normal1"></asp:TextBox>
                                            days.
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:CheckBox ID="cbOptionThree" runat="server" Text="" />
                                        </asp:TableCell>
                                        <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                                            Generate a list of Accounts that have bought the <a href="javascript: ShowItemList();"
                                                class="">following items</a> within the last
                                            <asp:TextBox runat="Server" ID="txtDaysOptionThree" MaxLength="3" Width="20" CssClass="normal1"></asp:TextBox>
                                            days.
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trItemList" Style="display: none;">
                                        <asp:TableCell CssClass="normal1" VerticalAlign="Top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				                                             &nbsp;&nbsp;&nbsp;&nbsp;</asp:TableCell>
                                        <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                                            <table align="right">
                                                <tr>
                                                    <td align="center" class="normal1" valign="top" colspan="2">
                                                        <telerik:radcombobox id="radItem" runat="server" width="200" dropdownwidth="200px"
                                                            enablescreenboundarydetection="true" enableloadondemand="true" allowcustomtext="True">
                                                            <webservicesettings path="../common/Common.asmx" method="GetItems" />
                                                        </telerik:radcombobox>
                                                        <%--<rad:radcombobox id="radItem" enablescreenboundarydetection="true" width="200px"
                                                            dropdownwidth="200px" skin="WindowsXP" externalcallbackpage="../Items/frmLoadItems.aspx"
                                                            runat="server" allowcustomtext="True" enableloadondemand="True">
                                                                </rad:radcombobox>--%>
                                                        <input type="button" class="button" onclick="Add();" value="Add"></input>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <%-- Available Items<br>
                                                                &nbsp;&nbsp;
                                                                <asp:ListBox ID="lstAvailablefld" runat="server" Width="150" Height="150" CssClass="signup"
                                                                    EnableViewState="False"></asp:ListBox>--%>
                                                    <%--<td align="center" class="normal1" valign="middle">
                                                                <input type="button" id="btnAdd" class="button" value="Add >" onclick="javascript: move(document.form1.lstAvailablefld,document.form1.lstSelectedfld);">
                                                                <br>
                                                                <br>
                                                                <input type="button" id="btnRemove" class="button" value="< Remove" onclick="javascript: remove(document.form1.lstSelectedfld,document.form1.lstAvailablefld);">
                                                            </td>--%>
                                                    <td align="center" class="normal1" colspan="2">
                                                        Added Items<br>
                                                        <asp:ListBox ID="lstSelectedfld" runat="server" Width="150" Height="150" CssClass="signup"
                                                            EnableViewState="False"></asp:ListBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="cpeCommerceMktg" runat="Server" TargetControlID="ContentCommerceMktg"
                                ExpandControlID="HeaderCommerceMktg" CollapseControlID="HeaderCommerceMktg" Collapsed="True"
                                TextLabelID="Label1" ExpandedText="(Hide Details...)" CollapsedText="(Show Details...)"
                                ImageControlID="Image1" ExpandedImage="~/images/collapse_blue.jpg" CollapsedImage="~/images/expand_blue.jpg"
                                SuppressPostBack="false" />
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <asp:Panel ID="HeaderSurveyRatingFilter" runat="server" CssClass="normal1">
                                <br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/expand_blue.jpg" />&nbsp;<asp:Label
                                    ID="Label4" CssClass="text_bold" runat="server">SurveyRatingFilter</asp:Label>
                                <asp:Label ID="Label2" runat="server">(Show Details...)</asp:Label>
                            </asp:Panel>
                            <asp:Panel ID="ContentSurveyRatingFilter" runat="server">
                                <asp:Table ID="tblSurveyRatingFilter" runat="server" BorderColor="black" GridLines="None"
                                    Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				                                            &nbsp;&nbsp;&nbsp;
                                        </asp:TableCell>
                                        <asp:TableCell CssClass="normal1" Style="padding-left: 10px;" ColumnSpan="2">
                                            <asp:CheckBox ID="chkSurveyBet" runat="server" Text="Survey rating between" />
                                            <asp:TextBox ID="txtRatingStart" runat="Server" CssClass="signup" MaxLength="7" Width="38"></asp:TextBox>
                                            to
                                            <asp:TextBox ID="txtRatingEnd" runat="Server" CssClass="signup" MaxLength="7" Width="38"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				                                            &nbsp;&nbsp;&nbsp;
                                        </asp:TableCell>
                                        <asp:TableCell CssClass="normal1" Style="padding-left: 10px;">
                                            <asp:CheckBox ID="chkSurvEqalTo" runat="server" Text="Survey rating equal to" />
                                        </asp:TableCell>
                                        <asp:TableCell CssClass="normal1">
                                            <asp:TextBox ID="txtSurveys" runat="Server" CssClass="signup" MaxLength="50" Width="95"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="Server"
                                TargetControlID="ContentSurveyRatingFilter" ExpandControlID="HeaderSurveyRatingFilter"
                                CollapseControlID="HeaderSurveyRatingFilter" Collapsed="True" TextLabelID="Label2"
                                ExpandedText="(Hide Details...)" CollapsedText="(Show Details...)" ImageControlID="Image2"
                                ExpandedImage="~/images/collapse_blue.jpg" CollapsedImage="~/images/expand_blue.jpg"
                                SuppressPostBack="false" />
                        </td>
                    </tr>
                </table>
                <br />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:Literal ID="litClientScript" runat="server"></asp:Literal>
    <asp:HiddenField ID="hdnCol" runat="server" />
    </form>
</body>
</html>
