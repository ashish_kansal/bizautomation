﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCampaignSurveyMatrixColumns.aspx.vb"
    Inherits=".frmCampaignSurveyMatrixColumns" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Campaign Survey Matrix Columns</title>
    <script language="javascript" src="../javascript/Surveys.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveAnswer" runat="Server" CssClass="button" Text="Save" />&nbsp;
            <asp:Button ID="btnClose" Text="Close" CssClass="button" runat="server" Width="50">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Matrix Columns
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <input id="hdSurId" type="hidden" name="hdSurId" runat="server">
    <input id="hdQId" type="hidden" name="hdQId" runat="server">
    <input id="hdAnsId" type="hidden" name="hdAnsId" runat="server">
    <input type="hidden" runat="server" name="hdAction" id="hdAction">
    <asp:Table ID="tblSurveyQuestionAnswer" runat="server" CellSpacing="1" CellPadding="1"
        Width="100%">
        <asp:TableRow CssClass="normal1">
            <asp:TableCell CssClass="hs" Width="90%"></asp:TableCell>
            <asp:TableCell CssClass="hs" Width="10%">Rules</asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:ValidationSummary ID="ValidationSummary" runat="server" HeaderText="Please check the following value(s)"
        ShowSummary="False" ShowMessageBox="True" DisplayMode="List"></asp:ValidationSummary>
</asp:Content>
