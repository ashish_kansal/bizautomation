﻿Imports BACRM.BusinessLogic.Common
Imports System.Reflection
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Tracking
Imports System.Linq
Imports System.Net
Imports BACRM.BusinessLogic.Contacts
Imports Infragistics.WebUI.UltraWebChart

Imports BACRM.BusinessLogic.Admin
Imports System.Collections.Generic

Imports System.Data.OleDb
Imports Infragistics.UltraChart.Resources.Appearance


Partial Public Class frmCampaignPerformance
    Inherits BACRMPage

    'Public GA_UserId As String = "carlzxc@gmail.com"
    'Public GA_Password As String = "Fabis321"
    'Public GA_ProfileID As String = "816107"
    'Public GA_UserId As String
    'Public GA_Password As String
    'Public GA_ProfileID As String

    Public dtUniqueVisitors As DataTable
    Public dtKeyWords As DataTable
    Public dtSource As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                Dim dtTable As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = CCommon.ToLong(Session("DomainID"))
                dtTable = objUserAccess.GetDomainDetails()
                hdnGA_UserId.Value = dtTable.Rows(0).Item("vcGAUserEMail")
                hdnGA_Password.Value = dtTable.Rows(0).Item("vcGAUserPassword")
                hdnGA_ProfileID.Value = dtTable.Rows(0).Item("vcGAUserProfileId")

                PersistTable.Load()
                If PersistTable.Count > 0 Then

                    Dim ddlCustomer As DropDownList = DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("ddlCustomerStage"), DropDownList)
                    If ddlCustomer IsNot Nothing Then
                        If ddlCustomer.Items.FindByValue(CCommon.ToInteger(PersistTable(ddlCustomer.ID))) IsNot Nothing Then
                            ddlCustomer.Items.FindByValue(CCommon.ToInteger(PersistTable(DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("ddlCustomerStage"), DropDownList).ID))).Selected = True
                        End If
                    End If

                    Dim ddlNoOfDays As DropDownList = DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("ddlNoOfDays"), DropDownList)
                    If ddlNoOfDays IsNot Nothing Then
                        If ddlNoOfDays.Items.FindByValue(CCommon.ToInteger(PersistTable(ddlNoOfDays.ID))) IsNot Nothing Then
                            ddlNoOfDays.Items.FindByValue(CCommon.ToInteger(PersistTable(DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("ddlNoOfDays"), DropDownList).ID))).Selected = True
                        End If
                    End If

                    If Not ddlUniqueVisitors.Items.FindByValue(CCommon.ToString(PersistTable(ddlUniqueVisitors.ID))) Is Nothing Then
                        ddlUniqueVisitors.Items.FindByValue(CCommon.ToString(PersistTable(ddlUniqueVisitors.ID))).Selected = True
                        BindGoogleAnalyticsMonths(CCommon.ToLong(PersistTable(ddlUniqueVisitors.ID)))
                    End If

                    If Not ddlLeadSources.Items.FindByValue(CCommon.ToString(PersistTable(ddlLeadSources.ID))) Is Nothing Then
                        ddlLeadSources.Items.FindByValue(CCommon.ToString(PersistTable(ddlLeadSources.ID))).Selected = True
                        BindActualLeadSources((CCommon.ToLong(PersistTable(ddlLeadSources.ID))))
                    End If

                    If Not ddlTopTrafficSources.Items.FindByValue(CCommon.ToString(PersistTable(ddlTopTrafficSources.ID))) Is Nothing Then
                        ddlTopTrafficSources.Items.FindByValue(CCommon.ToString(PersistTable(ddlTopTrafficSources.ID))).Selected = True
                        BindGoogleAnalyticsTopSources(CCommon.ToLong(PersistTable(ddlTopTrafficSources.ID)))
                    End If

                End If



                BindReport()
            End If

            If Request("__EVENTTARGET") = "radPanOfflineCampaign" Then
                BindReport()
                SaveDefaultFilter()
            End If

            ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('3');}else{ parent.SelectTabByValue('3'); } ", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    'Public Function GetFilterData() As String()
    '    Try
    '        Dim objContact As New CContacts
    '        objContact.DomainID = Session("DomainId")
    '        objContact.UserCntID = Session("UserContactID")
    '        objContact.ContactType = 0
    '        objContact.FormId = 47
    '        objContact.GroupID = 2
    '        Dim dt As DataTable = objContact.GetDefaultfilter()
    '        Dim ListDetails As String()
    '        If dt.Rows.Count > 0 Then
    '            ListDetails = CCommon.ToString(dt.Rows(0)("vcFilter")).Split("~")
    '        End If
    '        Return ListDetails
    '    Catch ex As Exception
    '        Throw ex
    '    End Try


    'End Function

    Private Sub BindActualLeadSources(ByVal Months As Integer)

        Dim LeadSourcesFromDate As New DateTime
        Try
            If Months > 0 Then
                If Months = 6 Then
                    LeadSourcesFromDate = DateTime.Now.AddMonths(-6)

                ElseIf Months = 12 Then
                    LeadSourcesFromDate = DateTime.Now.AddMonths(-12)

                ElseIf Months = 24 Then
                    LeadSourcesFromDate = DateTime.Now.AddMonths(-24)

                End If
            Else
                LeadSourcesFromDate = DateTime.Now.AddMonths(-6)

            End If

            Dim objCampaign As New Campaign
            objCampaign.DomainID = Session("DomainID")
            objCampaign.LeadSourcesFromDate = LeadSourcesFromDate
            Dim dt As DataTable = objCampaign.GetActualLeadSource()
            Dim dtLeadSource As New DataTable
            dtLeadSource = dt.Clone()
            dtLeadSource.Columns.Add("SNo")
            dtLeadSource.Columns.Add("TotalPercentage")
            dtLeadSource.Columns.Add("AccountsPercentage")
            dtLeadSource.Columns.Add("AccountsCountPercentage")
            dtLeadSource.Columns.Add("AmtGenerated")

            'dt = GetDataTablefromExcel()

            If dt.Rows.Count > 0 Then

                Dim TotalLeadSource As Integer = 0
                Dim RowCount As Integer = 0

                For Each dr As DataRow In dt.Rows
                    TotalLeadSource += CCommon.ToInteger(dr("TotalCount"))
                Next
                Dim dr1 As DataRow

                For Each dr As DataRow In dt.Rows
                    RowCount += 1
                    dr1 = dtLeadSource.NewRow()
                    dr1("SNo") = RowCount
                    dr1("SourceDomain") = CCommon.ToString(dr("SourceDomain"))
                    dr1("TotalPercentage") = IIf(System.Math.Round(((CCommon.ToInteger(dr("LeadsCount")) / TotalLeadSource) * 100), 0) < 1, "<1", CCommon.ToString(System.Math.Round(((CCommon.ToInteger(dr("LeadsCount")) / TotalLeadSource) * 100), 0)))
                    dr1("LeadsCount") = CCommon.ToString(dr("LeadsCount"))
                    dr1("AccountsCount") = dr("AccountsCount")
                    dr1("AccountsPercentage") = IIf(System.Math.Round(((CCommon.ToInteger(dr("AccountsCount")) / (CCommon.ToInteger(dr("TotalCount")))) * 100), 0) < 1, "<1", CCommon.ToString(System.Math.Round(((CCommon.ToInteger(dr("AccountsCount")) / (CCommon.ToInteger(dr("TotalCount")))) * 100), 0)))
                    dr1("AmtGenerated") = CCommon.ToString(Session("Currency") & IIf(ReturnMoney(dr("AmountGenerated")) = "", 0.0, ReturnMoney(dr("AmountGenerated"))))
                    dtLeadSource.Rows.Add(dr1)

                Next

            End If

            dgTopLeadSources.DataSource = dtLeadSource
            dgTopLeadSources.DataBind()


        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Public Function GetDataTablefromExcel() As DataTable

        Try
            Dim connectionString As String = ""

            Dim fileName As String = ""
            Dim fileExtension As String = "xls"
            Dim fileLocation As String = "C:\Users\Joseph\Desktop\LeadSources.xls"

            If fileExtension = ".xls" Then
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
            ElseIf fileExtension = ".xlsx" Then
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            End If

            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=Excel 12.0;Persist Security Info=False"

            'Create OleDB Connection and OleDb Command

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            Return dtExcelRecords

        Catch ex As Exception
            Throw ex

        End Try


    End Function

    Private Sub SaveDefaultFilter()
        Try
            Dim ddl1 As DropDownList = DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("ddlCustomerStage"), DropDownList)
            'ddl1.ID = "ddl1"
            Dim ddl2 As DropDownList = DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("ddlNoOfDays"), DropDownList)
            'ddl2.ID = "ddl2"

            Dim ddl3 As DropDownList = DirectCast(RadPanUniqueVisitors.FindItemByValue("UniqueVisitors").FindControl("ddlUniqueVisitors"), DropDownList)

            Dim ddl4 As DropDownList = DirectCast(RadPanTopLeadSources.FindItemByValue("TopLeadSources").FindControl("ddlLeadSources"), DropDownList)

            Dim ddl5 As DropDownList = DirectCast(RadPanTopTrafficSources.FindItemByValue("TopTrafficSources").FindControl("ddlTopTrafficSources"), DropDownList)

            'Persist Form Settings
            PersistTable.Clear()
            'PersistTable.Add(hdnPanel1.ID, hdnPanel1.Value)
            'PersistTable.Add(hdnPanel2.ID, hdnPanel2.Value)
            PersistTable.Add(ddl1.ID, ddl1.SelectedValue)
            PersistTable.Add(ddl2.ID, ddl2.SelectedValue)
            PersistTable.Add(ddl3.ID, ddl3.SelectedValue)
            PersistTable.Add(ddl4.ID, ddl4.SelectedValue)
            PersistTable.Add(ddl5.ID, ddl5.SelectedValue)
            PersistTable.Save()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindReport()
        Try
            Dim objCamp As New Campaign
            Dim ds As DataSet
            objCamp.DomainID = Session("DomainID")
            objCamp.CRMType = DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("ddlCustomerStage"), DropDownList).SelectedValue
            objCamp.StartDate = DateTime.Now.UtcNow.AddDays(-DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("ddlNoOfDays"), DropDownList).SelectedValue).Date
            objCamp.EndDate = DateTime.Now.UtcNow.Date.AddDays(1)
            ds = objCamp.CampaignPerformanceReport()

            Dim ucOfflineCampaign As UltraChart = DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("ucOfflineCampaign"), UltraChart)
            'Dim ChartColors() As Color

            'ChartColors = New Color() {Color.Blue, Color.Yellow, Color.Red, Color.Green, Color.Orange, Color.Indigo, Color.Violet}

            'ucOfflineCampaign.ColorModel.CustomPalette = ChartColors
            'ucOfflineCampaign.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear
            ucOfflineCampaign.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomSkin
            ucOfflineCampaign.ColorModel.Skin.PEs.Add(New PaintElement(Color.Red))
            ucOfflineCampaign.ColorModel.Skin.PEs.Add(New PaintElement(Color.Green))
            ucOfflineCampaign.ColorModel.Skin.PEs.Add(New PaintElement(Color.Blue))
            ucOfflineCampaign.ColorModel.Skin.PEs.Add(New PaintElement(Color.Yellow))
            ucOfflineCampaign.ColorModel.Skin.PEs.Add(New PaintElement(Color.Orange))
            ucOfflineCampaign.ColorModel.Skin.PEs.Add(New PaintElement(Color.Blue))
            ucOfflineCampaign.ColorModel.Skin.PEs.Add(New PaintElement(Color.Green))
            ucOfflineCampaign.ColorModel.Skin.PEs.Add(New PaintElement(Color.Black))
            ucOfflineCampaign.ColorModel.Skin.PEs.Add(New PaintElement(Color.CornflowerBlue))
            If ds.Tables(1).Rows.Count > 0 Then
                ucOfflineCampaign.Visible = True
                ucOfflineCampaign.PieChart.ColumnIndex = 4
                ucOfflineCampaign.Legend.Visible = True
                ucOfflineCampaign.Legend.FormatString = "<ITEM_LABEL>: (<PERCENT_VALUE:#>%)"
                ucOfflineCampaign.Legend.Location = Infragistics.UltraChart.Shared.Styles.LegendLocation.Top
                ucOfflineCampaign.DataSource = ds.Tables(1)

                ucOfflineCampaign.DataBind()

                Dim dgOfflineCampaign As DataGrid = DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("dgOfflineCampaign"), DataGrid)

                dgOfflineCampaign.Columns(6).HeaderText = "Cost Per " & DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("ddlCustomerStage"), DropDownList).SelectedItem.Text & ""
                dgOfflineCampaign.DataSource = ds.Tables(1)
                dgOfflineCampaign.DataBind()

                DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("lblOfflineToalAmountPaid"), Label).Text = ReturnMoney(ds.Tables(1).Compute("sum(monCampaignCost)", ""))
                DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("lblOfflineToalAmountMade"), Label).Text = ReturnMoney(ds.Tables(1).Compute("sum(AmtFromOrders)", ""))
                DirectCast(radPanOfflineCampaign.FindItemByValue("OfflineCampaignSources").FindControl("lblOfflineROI"), Label).Text = ReturnMoney(ds.Tables(1).Compute("sum(ROI)", ""))
            Else
                ucOfflineCampaign.Visible = False
            End If

            ' BindAnalytics()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If IsDBNull(Money) Then
                Return ""
            End If
            Return String.Format("{0:#,###.##}", Math.Round(Money, 2, MidpointRounding.ToEven))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub ddlCustomerStage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCustomerStage.SelectedIndexChanged
        Try
            BindReport()
            SaveDefaultFilter()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Dim Records As Long
    Dim CampaignCost As Double
    Protected Sub dgOfflineCampaign_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Records = CCommon.ToLong(CType(e.Item.FindControl("lblRecordsCreated"), Label).Text)
                CampaignCost = CCommon.ToDouble(String.Format("{0:###0.00}", CType(e.Item.FindControl("lblCampaignCost"), Label).Text))
                If Records > 0 And CampaignCost > 0 Then
                    CType(e.Item.FindControl("lblCostPer"), Label).Text = ReturnMoney(CType(e.Item.FindControl("lblCampaignCost"), Label).Text / Records)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    'Protected Sub dgOnlineCampaigns_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
    '    Try
    '        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '            Records = CCommon.ToLong(CType(e.Item.FindControl("lblRecordsCreated"), Label).Text)
    '            CampaignCost = CCommon.ToDouble(String.Format("{0:###0.00}", CType(e.Item.FindControl("lblCampaignCost"), Label).Text))
    '            If Records > 0 And CampaignCost > 0 Then
    '                CType(e.Item.FindControl("lblCostPer"), Label).Text = ReturnMoney(CType(e.Item.FindControl("lblCampaignCost"), Label).Text / Records)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.Message)
    '    End Try
    'End Sub

    Protected Sub ddlNoOfDays_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlNoOfDays.SelectedIndexChanged
        Try
            BindReport()
            SaveDefaultFilter()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlUniqueVisitors_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUniqueVisitors.SelectedIndexChanged
        Try
            Dim Months As Integer = ddlUniqueVisitors.SelectedValue
            If Months > 0 Then
                SaveDefaultFilter()
                BindGoogleAnalyticsMonths(Months)
            Else
                BulidUniqueVisitorsTable()
                dgUniqueVisitors.DataSource = dtUniqueVisitors
                dgUniqueVisitors.DataBind()

            End If

        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Private Sub ddlLeadSources_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLeadSources.SelectedIndexChanged
        Try
            Dim Months As Integer = ddlLeadSources.SelectedValue
            If Months > 0 Then

                SaveDefaultFilter()
                BindActualLeadSources(Months)
            Else
                dgTopLeadSources.DataSource = Nothing
                dgTopLeadSources.DataBind()

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ddlTopTrafficSources_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTopTrafficSources.SelectedIndexChanged
        Try
            Dim Months As Integer = ddlTopTrafficSources.SelectedValue
            If Months > 0 Then
                SaveDefaultFilter()
                Select Case (Months)
                    Case 6
                        If Not Cache("__dtGA_TopSources_6_Cache_Key") Is Nothing Then
                            Dim dtGATopSources As DataTable = DirectCast(Cache("__dtGA_TopSources_6_Cache_Key"), DataTable)
                            dgTopSources.DataSource = dtGATopSources
                            dgTopSources.DataBind()
                        Else
                            BindGoogleAnalyticsTopSources(6)
                        End If

                    Case 12
                        If Not Cache("__dtGA_TopSources_12_Cache_Key") Is Nothing Then
                            Dim dtGATopSources As DataTable = DirectCast(Cache("__dtGA_TopSources_12_Cache_Key"), DataTable)
                            dgTopSources.DataSource = dtGATopSources
                            dgTopSources.DataBind()
                        Else
                            BindGoogleAnalyticsTopSources(12)
                        End If

                    Case 24
                        If Not Cache("__dtGA_TopSources_24_Cache_Key") Is Nothing Then
                            Dim dtGATopSources As DataTable = DirectCast(Cache("__dtGA_TopSources_24_Cache_Key"), DataTable)
                            dgTopSources.DataSource = dtGATopSources
                            dgTopSources.DataBind()
                        Else
                            BindGoogleAnalyticsTopSources(24)
                        End If

                End Select
            Else
                dgTopLeadSources.DataSource = Nothing
                dgTopLeadSources.DataBind()

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindGoogleAnalyticsTopSources(ByVal months As Integer)

        Try
            Dim GA_UserId As String = ""
            Dim GA_Password As String = ""
            Dim GA_ProfileID As String = ""

            GA_UserId = hdnGA_UserId.Value
            GA_Password = hdnGA_Password.Value
            GA_ProfileID = hdnGA_ProfileID.Value

            If Not String.IsNullOrEmpty(GA_UserId) AndAlso Not String.IsNullOrEmpty(GA_Password) AndAlso Not String.IsNullOrEmpty(GA_ProfileID) Then
                Dim Fromdate As DateTime
                Fromdate = DateTime.Now.AddMonths(-months)

                Dim ToDate As DateTime = DateTime.Now
                Dim email As String = GA_UserId
                Dim password As String = GA_Password

                Dim objDimension As New List(Of GAConnect.Dimension)
                Dim objMetrics As New List(Of GAConnect.Metric)

                objDimension.Add(GAConnect.Dimension.source)

                objMetrics.Add(GAConnect.Metric.pageviews)
                objMetrics.Add(GAConnect.Metric.visits)
                objMetrics.Add(GAConnect.Metric.newVisits)


                Dim gaDataFetcher = New GAConnect.GADataFetcher(email, password)
                Dim data As IEnumerable(Of GAConnect.GAData) = gaDataFetcher.GetAnalytics(GA_ProfileID, Fromdate, ToDate, 10, objDimension, objMetrics, GAConnect.Metric.visits, GAConnect.SortDirection.Descending)

                Dim dt As New DataTable
                dt = ToDataTable(Of GAConnect.GAData)(data)
                If dt.Rows.Count > 0 Then

                    dt.DefaultView.Sort = "Visits DESC"
                    dt = dt.DefaultView.ToTable()
                    BulidTopSourceTable()
                    dtSource.Rows.Clear()
                    Dim TotalVisits As Integer = 0
                    For Each dr As DataRow In dt.Rows
                        TotalVisits += CCommon.ToInteger(dr("Visits"))
                    Next
                    Dim dr1 As DataRow
                    Dim RowCount As Integer = 0
                    For Each dr As DataRow In dt.Rows
                        RowCount += 1
                        dr1 = dtSource.NewRow()
                        dr1("SNo") = RowCount
                        dr1("vcSource") = CCommon.ToString(dr("Source"))
                        dr1("Visits") = dr("Visits")
                        dr1("Percentage") = System.Math.Round(((CCommon.ToInteger(dr("Visits")) / TotalVisits) * 100), 2)
                        dtSource.Rows.Add(dr1)

                    Next

                End If

                dgTopSources.DataSource = dtSource
                dgTopSources.DataBind()
                Dim strCache As String = "__dtGA_TopSources_" + CCommon.ToString(months) + "_Cache_Key"
                Cache.Insert(strCache, dtSource, Nothing, (DateTime.Now.AddDays(1)), Nothing)

            End If
        Catch ex As WebException
            If ex.Status = WebExceptionStatus.ProtocolError Then
                Dim response = TryCast(ex.Response, HttpWebResponse)
                If response IsNot Nothing Then
                    If CType(ex.Response, HttpWebResponse).StatusCode = "403" Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType, "AuthorizationFailure", "alert('Authentication Failed! Go to Administration -> Global Settings -> General and provide correct Google Analytics Configurations.');", True)
                    Else
                        Throw
                    End If
                Else
                    Throw
                End If
            Else
                Throw
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Private Sub BindGoogleAnalyticsMonths(ByVal months As Integer)
        Try
            If Not Cache("__dtGA_MonthlyVisits_Cache_Key") Is Nothing Then
                Dim dtGAMonthlyVisits As DataTable = DirectCast(Cache("__dtGA_MonthlyVisits_Cache_Key"), DataTable)

                Dim dtMonthlyVisits As New DataTable
                dtMonthlyVisits.Columns.Clear()
                dtMonthlyVisits.Columns.Add("vcMonth")
                dtMonthlyVisits.Columns.Add("Visits")
                Dim drMonthlyVisits As DataRow
                For i As Integer = 0 To months
                    drMonthlyVisits = dtMonthlyVisits.NewRow()
                    drMonthlyVisits("vcMonth") = dtGAMonthlyVisits(i)("vcMonth")
                    drMonthlyVisits("Visits") = dtGAMonthlyVisits(i)("Visits")
                    dtMonthlyVisits.Rows.Add(drMonthlyVisits)

                Next

                dgUniqueVisitors.DataSource = dtMonthlyVisits
                dgUniqueVisitors.DataBind()

            Else
                Dim GA_UserId As String = ""
                Dim GA_Password As String = ""
                Dim GA_ProfileID As String = ""

                GA_UserId = hdnGA_UserId.Value
                GA_Password = hdnGA_Password.Value
                GA_ProfileID = hdnGA_ProfileID.Value

                If Not String.IsNullOrEmpty(GA_UserId) AndAlso Not String.IsNullOrEmpty(GA_Password) AndAlso Not String.IsNullOrEmpty(GA_ProfileID) Then

                    Dim Fromdate As DateTime
                    Fromdate = DateTime.Now.AddMonths(-24)
                    Dim ToDate As DateTime = DateTime.Now
                    Dim email As String = GA_UserId
                    Dim password As String = GA_Password
                    Dim objDimension As New List(Of GAConnect.Dimension)
                    Dim objMetrics As New List(Of GAConnect.Metric)
                    objDimension.Add(GAConnect.Dimension.year)
                    objDimension.Add(GAConnect.Dimension.month)
                    ' objMetrics.Add(GAConnect.Metric.pageviews)
                    'objMetrics.Add(GAConnect.Metric.visits)
                    objMetrics.Add(GAConnect.Metric.newVisits)

                    Dim gaDataFetcher = New GAConnect.GADataFetcher(email, password)
                    Dim data As IEnumerable(Of GAConnect.GAData) = gaDataFetcher.GetAnalytics(GA_ProfileID, Fromdate, ToDate, 1000, objDimension, objMetrics, GAConnect.Metric.newVisits, GAConnect.SortDirection.Descending)

                    Dim dt As DataTable = ToDataTable(Of GAConnect.GAData)(data)

                    If dt.Rows.Count > 0 Then
                        dt.DefaultView.Sort = "Year DESC, Month DESC"
                        dt = dt.DefaultView.ToTable()

                        'Dim View As DataView = dt.DefaultView

                        'dt.DefaultView.Sort = "Year DESC, Month ASC"

                        BulidUniqueVisitorsTable()
                        dtUniqueVisitors.Rows.Clear()

                        Dim dr1 As DataRow

                        For Each dr As DataRow In dt.Rows
                            dr1 = dtUniqueVisitors.NewRow()
                            dr1("vcMonth") = CCommon.ToString(dr("Year")) + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dr("Month"))
                            dr1("Visits") = dr("NewVisits")
                            dtUniqueVisitors.Rows.Add(dr1)

                        Next

                    End If
                    Cache.Insert("__dtGA_MonthlyVisits_Cache_Key", dtUniqueVisitors, Nothing, (DateTime.Now.AddDays(1)), Nothing)

                    Dim dtMonthlyVisits As New DataTable
                    dtMonthlyVisits.Columns.Clear()
                    dtMonthlyVisits.Columns.Add("vcMonth")
                    dtMonthlyVisits.Columns.Add("Visits")
                    Dim drMonthlyVisits As DataRow
                    For i As Integer = 0 To months
                        drMonthlyVisits = dtMonthlyVisits.NewRow()
                        drMonthlyVisits("vcMonth") = dtUniqueVisitors(i)("vcMonth")
                        drMonthlyVisits("Visits") = dtUniqueVisitors(i)("Visits")
                        dtMonthlyVisits.Rows.Add(drMonthlyVisits)

                    Next

                    dgUniqueVisitors.DataSource = dtMonthlyVisits
                    dgUniqueVisitors.DataBind()
                    ' Cache.Insert("__dtGA_MonthlyVisits_Cache_Key", dtUniqueVisitors, Nothing, (DateTime.Now.AddDays(1)), Nothing)

                End If
            End If
        Catch ex As WebException
            If ex.Status = WebExceptionStatus.ProtocolError Then
                Dim response = TryCast(ex.Response, HttpWebResponse)
                If response IsNot Nothing Then
                    If CType(ex.Response, HttpWebResponse).StatusCode = "403" Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType, "AuthorizationFailure", "alert('Authentication Failed! Go to Administration -> Global Settings -> General and provide correct Google Analytics Configurations.');", True)
                    Else
                        Throw
                    End If
                Else
                    Throw
                End If
            Else
                Throw
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BulidUniqueVisitorsTable()
        Try
            If dtUniqueVisitors Is Nothing Then
                dtUniqueVisitors = New DataTable

            End If
            dtUniqueVisitors.Columns.Clear()
            dtUniqueVisitors.Columns.Add("vcMonth")
            dtUniqueVisitors.Columns.Add("Visits")

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub BulidTopSourceTable()
        Try
            If dtSource Is Nothing Then
                dtSource = New DataTable

            End If
            dtSource.Columns.Clear()
            dtSource.Columns.Add("SNo")
            dtSource.Columns.Add("vcSource")
            dtSource.Columns.Add("Visits")
            dtSource.Columns.Add("Percentage")

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Public Shared Function ToDataTable(Of T)(items As IEnumerable(Of T)) As DataTable
        Dim tb = New DataTable(GetType(T).Name)

        Dim props As PropertyInfo() = GetType(T).GetProperties(BindingFlags.[Public] Or BindingFlags.Instance)

        For Each prop As Object In props
            tb.Columns.Add(prop.Name, prop.PropertyType)
        Next
        For Each item As Object In items
            Dim values = New Object(props.Length - 1) {}
            For i As Integer = 0 To props.Length - 1
                values(i) = props(i).GetValue(item, Nothing)
            Next

            tb.Rows.Add(values)
        Next
        Return tb
    End Function
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class