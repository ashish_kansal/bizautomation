﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Survey

Public Class frmCampaignSurveyMatrixColumns
    Inherits BACRMPage
    Dim numQuestionId As Integer
    Dim numSurId As Integer
    Dim numMatrix As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            numQuestionId = GetQueryStringVal( "numQuestionId") 'Get the Question Id in local variable
            hdQId.Value = numQuestionId                                         'Set the Question Id in hidden
            numSurId = GetQueryStringVal( "numSurId")           'Get the Survey Id in local variable
            hdSurId.Value = numSurId   'Set the Survey Id in hidden

            numMatrix = GetQueryStringVal( "numMatrix")

            Dim objSurvey As New SurveyAdministration                       'Create an object of Survey Administration Class
            objSurvey.DomainId = Session("DomainID")                        'Set the Domain ID
            objSurvey.UserId = Session("UserID")                            'Set the User ID
            objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
            objSurvey.SurveyId = numSurId                              'Set the Survey ID
            objSurvey.QuestionId = numQuestionId                            'Set the question id
            objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()

            If Not IsPostBack Then
                btnClose.Attributes.Add("onclick", "return Close()")
            End If

            Dim dtSurverQuestionAnswers As DataTable                        'Declare a Datatable object
            dtSurverQuestionAnswers = objSurvey.GetMatrixForSurveyQuestion() 'Get the answers for the selected question
            DisplayAnswers(dtSurverQuestionAnswers)                         'Display the answers
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub DisplayAnswers(ByVal dtAnswerContent As DataTable)
        Try
            tblSurveyQuestionAnswer.Rows.Clear()                                            'Clear all rows
            tblSurveyQuestionAnswer.Controls.Clear()                                        'clear all controls
            Dim tblRowDisplayAnswer As TableRow                                             'Declare a Table Row object
            Dim tblCellTwoDisplayAnswer, tblCellThreeDisplayAnswer As TableCell 'Declare Table Cells object

            Dim numAnswerIndex As Integer                                                   'Declare the row index
            Dim numMatrixID As Integer                                                         'Declare the variable to store teh Rule Id and Answer Index
            Dim boolSurveyRuleAttached As Boolean                                           'Variable to store a falag indicating if a Rule is associated
            Dim vcAnsLabel As String                                                        'Declare the variable to store the amswer
            For numAnswerIndex = 0 To numMatrix - 1
                If numAnswerIndex < dtAnswerContent.Rows.Count Then
                    boolSurveyRuleAttached = dtAnswerContent.Rows(numAnswerIndex).Item("boolSurveyRuleAttached") 'Store the Rule associated with the answer
                    vcAnsLabel = dtAnswerContent.Rows(numAnswerIndex).Item("vcAnsLabel")    'Store the answer
                    numMatrixID = dtAnswerContent.Rows(numAnswerIndex).Item("numMatrixID")        'Store teh answer id
                Else
                    boolSurveyRuleAttached = 0
                    numMatrixID = numAnswerIndex + 1
                    vcAnsLabel = ""
                End If
                tblRowDisplayAnswer = New TableRow                                          'Instantiate a new table row everytime
                tblRowDisplayAnswer.EnableViewState = False                                 'Not to have a view state for the row
                tblRowDisplayAnswer.Attributes.Add("style", "display:inline;")
                tblRowDisplayAnswer.ID = "tRow" & numAnswerIndex + 1                        'Generate Unique ros Ids

                tblCellTwoDisplayAnswer = New TableCell                                     'Create an object of table cell
                tblCellTwoDisplayAnswer.CssClass = "normal1"                                'Add the class attribute
                Dim oTextBox As New TextBox                                                 'Instantiate a TextBox
                oTextBox.ID = "txtAnswer" & numAnswerIndex + 1                              'set the name of the textbox"
                oTextBox.Width = Unit.Pixel(380)                                            'set the width of the textbox
                oTextBox.TextMode = TextBoxMode.SingleLine                                  'This is a Single textarea
                oTextBox.CssClass = "signup"                                                'Set the class attribute for the textbox control
                oTextBox.MaxLength = 100                                                    'Max 100 characters allowed
                oTextBox.Text = ReSetFromXML(vcAnsLabel)                                    'Set the answer for display
                oTextBox.EnableViewState = False                                            'Text box will not have a view state

                tblCellTwoDisplayAnswer.Controls.Add(oTextBox)                              'Add the textbox to the table cell
                Dim valReqControl As New RequiredFieldValidator                             'Declare a required field validator
                valReqControl.ID = "vReq" & numAnswerIndex + 1                              'Giving id to the validator control
                valReqControl.ControlToValidate = "txtAnswer" & numAnswerIndex + 1          'Attach the control to validate
                valReqControl.EnableClientScript = True                                     'Enable Client script
                valReqControl.InitialValue = ""                                             'Set the Initial value so that it also validates drop down
                valReqControl.ErrorMessage = "Please enter " & "Answer " & numAnswerIndex + 1 & "." 'Specify the error message to be displayed
                valReqControl.Display = ValidatorDisplay.None                               'Error message never displays inline
                tblCellTwoDisplayAnswer.Controls.Add(valReqControl)                         'Add the validator to the table cell

                tblCellThreeDisplayAnswer = New TableCell                                   'Create an object of table cell
                tblCellThreeDisplayAnswer.CssClass = "normal1"                              'Add the class attribute
                tblCellThreeDisplayAnswer.VerticalAlign = VerticalAlign.Top                 'align the buttons etc to the top
                Dim oAddEditAnsRuleButton As New Button                                     'Instantiate a button
                If boolSurveyRuleAttached <> 0 Then
                    oAddEditAnsRuleButton.Text = "Edit Column Header"                                'Set the lable to the button
                Else : oAddEditAnsRuleButton.Text = "Add Column Header"                                 'Set the lable to the button
                End If
                oAddEditAnsRuleButton.CssClass = "button"                                   'Set the class attribute
                oAddEditAnsRuleButton.ID = "btnRule" & numMatrixID
                If Trim(vcAnsLabel) = "" Then
                    oAddEditAnsRuleButton.Attributes.Add("onclick", "javascript: return AddEditAnswerRule(" & numMatrixID & ",'New',1);") 'Add script to enable adding/ editing an Rule
                Else : oAddEditAnsRuleButton.Attributes.Add("onclick", "javascript: return AddEditAnswerRule(" & numMatrixID & ",'Edit',1);") 'Add script to enable adding/ editing an Rule
                End If
                tblCellThreeDisplayAnswer.Controls.Add(oAddEditAnsRuleButton)               'Add the Add Edit button to the cell
                tblRowDisplayAnswer.Cells.Add(tblCellTwoDisplayAnswer)                      'Add the lable cell specify to question textbox
                tblRowDisplayAnswer.Cells.Add(tblCellThreeDisplayAnswer)                    'Add the lable cell specify to buttons
                tblSurveyQuestionAnswer.Rows.Add(tblRowDisplayAnswer)                       'Add the row to the table
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSaveAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAnswer.Click
        Try
            Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
            objSurvey.DomainId = Session("DomainID")                            'Set the Domain ID
            objSurvey.UserId = Session("UserID")                                'Set the User ID
            objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
            objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID
            objSurvey.QuestionId = hdQId.Value                                  'Set the question id

            objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()         'Call to retrieve the existing data temporarily
            UpdateAnswers(objSurvey)                                            'Update the rows in the answer table
            objSurvey.SaveSurveyInformationTemp()                               'Call to save the existing data temporarily
            Dim btnSrcButton As Button = CType(sender, Button)                  'typecast to button

            Dim dtSurverQuestionAnswers As DataTable                            'Declare a Datatable object
            dtSurverQuestionAnswers = objSurvey.GetMatrixForSurveyQuestion()   'Get the answers for the selected question
            DisplayAnswers(dtSurverQuestionAnswers)                             'Display the answers
            'Call to bind the AOIs

            hdAnsId.Value = ""                                              'Reset teh Ans Id value

            hdAction.Value = ""
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to sync the dataset of Survey Information with the latest changes
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/14/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Function UpdateAnswers(ByRef objSurvey As SurveyAdministration)
        Try
            Dim dtSurveyAnswerMaster As DataTable
            dtSurveyAnswerMaster = objSurvey.SurveyInfo.Tables("SurveyMatrixMaster")
            Dim numSurveyAnwerMasterTableRowIndex As Integer                    'Declare a index for datatable rows
            Dim numSurveyAnwerMasterRowIndex As Integer = 1                     'Declare a index for htmltable rows
            While numSurveyAnwerMasterTableRowIndex < dtSurveyAnswerMaster.Rows.Count
                If dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numQID") = hdQId.Value Then
                    If (Request.Form("txtAnswer" & numSurveyAnwerMasterRowIndex) <> "") Then
                        dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numSurID") = hdSurId.Value 'set the Survey ID
                        dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numMatrixID") = numSurveyAnwerMasterRowIndex 'set the ans id
                        dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("vcAnsLabel") = MakeSafeForXML(Request.Form("txtAnswer" & numSurveyAnwerMasterRowIndex))  'Capture the answer
                        dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("boolDeleted") = 0         'Sync the deleted flag
                        numSurveyAnwerMasterRowIndex += 1                       'increment the htmltable row counter
                    Else
                        objSurvey.DeleteSurveyWorkFlowTempMatrix(hdQId.Value, numSurveyAnwerMasterRowIndex)  'Request deletion of rules
                        dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Delete() 'Delete the answer row
                        numSurveyAnwerMasterTableRowIndex -= 1                  'since row is deleted so decrement the counter
                    End If
                End If
                numSurveyAnwerMasterTableRowIndex += 1                          'increment the datatable row counter
            End While
            Dim drSurveyAnswerMasterRow As DataRow                              'Declare a DataRow
            While numSurveyAnwerMasterRowIndex <= numMatrix
                drSurveyAnswerMasterRow = dtSurveyAnswerMaster.NewRow()         'Get an new row of the table
                drSurveyAnswerMasterRow.Item("numSurID") = hdSurId.Value        'set the Survey ID
                drSurveyAnswerMasterRow.Item("numQID") = hdQId.Value            'Insert the new question id for the answer
                drSurveyAnswerMasterRow.Item("numMatrixID") = numSurveyAnwerMasterRowIndex 'Insert the new answer id for the answer
                drSurveyAnswerMasterRow.Item("vcAnsLabel") = MakeSafeForXML(Request.Form("txtAnswer" & numSurveyAnwerMasterRowIndex)) 'Insert the new answer
                drSurveyAnswerMasterRow.Item("boolSurveyRuleAttached") = 0      'no rule for these rows
                drSurveyAnswerMasterRow.Item("boolDeleted") = 0                 'Set the flag indicating that it is not deleted
                dtSurveyAnswerMaster.Rows.Add(drSurveyAnswerMasterRow)          'Add the question to the table
                numSurveyAnwerMasterRowIndex += 1                               'increment the datatable row counter
            End While

            Dim dtSurveyQuestion As DataTable                                                               'Declare the datatable
            dtSurveyQuestion = objSurvey.SurveyInfo.Tables("SurveyQuestionMaster")                          'Get the DataTable Data
            Dim drSurveyQuestion As DataRow                                                                 'Declare a DataRow
            Dim iQuestionRowIndex As Integer                                                                'Declare a row index
            For iQuestionRowIndex = 0 To dtSurveyQuestion.Rows.Count - 1                                    'loop through the rows that match the criteria
                If dtSurveyQuestion.Rows(iQuestionRowIndex).Item("numQID") = hdQId.Value Then
                    dtSurveyQuestion.Rows(iQuestionRowIndex).Item("boolMatrixColumn") = 1
                    dtSurveyQuestion.Rows(iQuestionRowIndex).Item("intNumOfMatrixColumn") = numMatrix
                    Exit For
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function ReSetFromXML(ByVal sValue As String) As String
        Try
            Return Replace(Replace(Replace(Replace(sValue, "&amp;", "&"), "&#39;", "'"), "&lt;", "<"), "&gt;", ">")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function MakeSafeForXML(ByVal sValue As String) As String
        Try
            Return Replace(Replace(Replace(Replace(Replace(sValue, "'", "&#39;"), """", "&#39;&#39;"), "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class