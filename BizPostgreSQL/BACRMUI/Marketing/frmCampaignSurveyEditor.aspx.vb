''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Survey
''' Class	 : frmCampaignSurveyEditor
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a interface for editing a Survey
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	09/13/2005	Created
''' </history>
''' ''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing

Namespace BACRM.UserInterface.Survey

    Public Class frmCampaignSurveyEditor
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnCancelSurvey As System.Web.UI.WebControls.Button
        Protected WithEvents btnCloneSurvey As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveAndClose As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveQuestions As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveSurvey As System.Web.UI.WebControls.Button
        Protected WithEvents btnAddNewQuestion As System.Web.UI.WebControls.Button
        Protected WithEvents btnCancelQuestions As System.Web.UI.WebControls.Button
        Protected WithEvents btnOpenSurveyQuestionnaire As System.Web.UI.WebControls.Button
        Protected WithEvents tblSurveyQuestions As System.Web.UI.WebControls.Table
        Protected WithEvents hdSurId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents tblSurveyEdited As System.Web.UI.WebControls.Table
        Protected WithEvents txtSurveyName As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtRedirectionURL As System.Web.UI.WebControls.TextBox
        Protected WithEvents cbSkipRegistration As System.Web.UI.HtmlControls.HtmlInputCheckBox
        Protected WithEvents cbRandomAnswers As System.Web.UI.HtmlControls.HtmlInputCheckBox
        Protected WithEvents cbSinglePageSurvey As System.Web.UI.HtmlControls.HtmlInputCheckBox
        Protected WithEvents ValidationSummary As System.Web.UI.WebControls.ValidationSummary
        Protected WithEvents btnSaveSurveyActualButHidden As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveAndCloseActualButHidden As System.Web.UI.WebControls.Button
        Protected WithEvents btnOpenRegPage As System.Web.UI.WebControls.Button
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime the page is called. In this event we will 
        '''     get the data from the DB and populate the Tables and the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                'Put user code to initialize the page here
                Dim numSurId As Integer                                             'Variable to store the Survey ID
                If hdSurId.Value <> "" Then
                    numSurId = hdSurId.Value                                        'Get the Survey Id from hidden to a local variable
                Else : numSurId = GetQueryStringVal("numSurId")                      'Get the Survey Id in local variable
                End If
                If numSurId = 0 Then
                    btnOpenSurveyQuestionnaire.Enabled = False                      'Button is disabled for new surveys
                    btnCloneSurvey.Enabled = False                                  'Cloning not possible for new surveys
                End If
                hdSurId.Value = numSurId                                            'Set the Survey Id in hidden
                Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
                objSurvey.DomainID = Session("DomainID")                            'Set the Domain ID
                objSurvey.UserId = Session("UserID")                                'Set the User ID
                objSurvey.XMLFilePath = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" 'Set the path to the xml file where the survey info is temp stored
                objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID
                objSurvey.GetSurveyInformationTemp()                                'Get the Survey Information in xml

                If Not IsPostBack Then
                    txtSurveyName.Focus()
                    '
                    'objCommon = New CCommon
                    'objCommon.sb_FillComboFromDBwithSel(ddlRelationShip, 5, Session("DomainID"))
                    LoadTemplates(ddlEmailTemplate1)
                    LoadTemplates(ddlEmailTemplate2)

                    BindElementsToMasterDataBase()

                    ' = "MarSurvey"
                    DisplaySurveyMasterInformation()                                'Call to get the Survey Master Information
                End If
                lbCreateRecord.Attributes.Add("onclick", "return ShowSurveyCreateRecord();")

                cbRandomAnswers.Visible = False                                 'Hiding this button unless functionality si atchivable
                PostInitializeControlsClientEvents()                                'Call to attache the client events
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BindElementsToMasterDataBase()
            Try
                Dim objConfigWizard As New BACRM.BusinessLogic.Admin.FormGenericFormContents        'Create an object of class which encaptulates the functionaltiy
                objConfigWizard.DomainID = Session("DomainID")                                      'set the domain id
                objConfigWizard.ListItemType = "LI"                                                  'set the listitem type
                Dim dtRelationTable As DataTable                                                    'declare a datatable
                dtRelationTable = objConfigWizard.GetMasterListByListId(5) 'call function to fill the datatable with Relationship
                ddlRelationShip.DataSource = dtRelationTable                                        'set the datasource of the Relationship drop down
                ddlRelationShip.DataTextField = "vcItemName"                                        'Set the Text property of the Relationship drop down
                ddlRelationShip.DataValueField = "numItemID"                                        'Set the value attribute of the Relationship drop down
                ddlRelationShip.DataBind()                                                          'Databind the drop down

                Dim dtGroupTable As DataTable                                                       'declare a datatable
                objConfigWizard.ListItemType = "AG"                                                  'set the listitem type
                dtGroupTable = objConfigWizard.GetMasterListByListId(38)                            'call function to fill the datatable with Groups
                ddlGroup.DataSource = dtGroupTable                                                  'set the datasource of the Groups drop down
                ddlGroup.DataTextField = "vcItemName"                                               'Set the Text property of the Groups drop down
                ddlGroup.DataValueField = "numItemID"                                               'Set the value attribute of the Groups drop down
                ddlGroup.DataBind()                                                                 'Databind the drop down

                Dim dtRecordOwner As DataTable                                                      'declare a datatable
                Dim objGenericAdvSearch As New BACRM.BusinessLogic.Admin.FormGenericAdvSearch       'Create an object of class which encaptulates the functionaltiy
                objGenericAdvSearch.DomainID = Session("DomainID")                                  'set the domain id
                dtRecordOwner = objGenericAdvSearch.getAvailableEmployees()                         'Call to get the list of employees
                Dim drRecordOwner As DataRow = dtRecordOwner.NewRow                                 'Get a new row instance of campaign
                drRecordOwner.Item("vcUserName") = "Let Auto Routing Rules decide"                  'Set the displayable text 
                drRecordOwner.Item("numUserID") = 0                                                 'Set the Id as 0
                dtRecordOwner.Rows.InsertAt(drRecordOwner, 0)                                       'Insert the new row at position 0
                ddlRecordOwner.DataSource = dtRecordOwner                                           'Set the datasource of the record owners drop down
                ddlRecordOwner.DataTextField = "vcUserName"                                         'Set the Text displayed in the dropdown as the employee names
                ddlRecordOwner.DataValueField = "numUserId"                                         'Set the values displayed in the dropdown as the employee Ids
                ddlRecordOwner.DataBind()                                                           'DataBind the Record Owner drop down
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub LoadProfile()
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.RelID = ddlRelationShip.SelectedValue
                objUserAccess.DomainID = Session("DomainID")
                ddlProfile.DataSource = objUserAccess.GetRelProfileD
                ddlProfile.DataTextField = "ProName"
                ddlProfile.DataValueField = "numProfileID"
                ddlProfile.DataBind()
                ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub LoadTemplates(ByVal ddlEmailTemplate As DropDownList)
            Try
                Dim objCampaign As New Campaign
                Dim dtTable As DataTable
                objCampaign.DomainID = Session("DomainID")
                dtTable = objCampaign.GetEmailTemplates
                ddlEmailTemplate.DataSource = dtTable
                ddlEmailTemplate.DataTextField = "VcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()
                ddlEmailTemplate.Items.Insert(0, "--Select One--")
                ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to populate the survey master information in the form
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub DisplaySurveyMasterInformation()
            Try
                Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
                objSurvey.DomainId = Session("DomainID")                            'Set the Domain ID
                objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID
                If hdSurId.Value > 0 Then                                           'Display filled form only if existing survey is selected
                    Dim dtSurveyMasterInfo As DataTable                             'Declare a DataTable
                    dtSurveyMasterInfo = objSurvey.getSurveyMasterData()            'Get the Survey Master information in a datatable
                    txtSurveyName.Text = ReSetFromXML(CStr(dtSurveyMasterInfo.Rows(0).Item("vcSurName"))) 'Display the name of the survey

                    txtRedirectionURL.Text = dtSurveyMasterInfo.Rows(0).Item("vcRedirectURL") 'Display the redirection URL of the survey

                    cbSkipRegistration.Checked = dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration") 'Display check/ uncheck for Skip Registration
                    cbRandomAnswers.Checked = dtSurveyMasterInfo.Rows(0).Item("bitRandomAnswers") 'Display check/ uncheck for Random answers in Survey
                    cbSinglePageSurvey.Checked = dtSurveyMasterInfo.Rows(0).Item("bitSinglePageSurvey") 'Display check/ uncheck for Single Page Survey
                    cbPostRegistration.Checked = dtSurveyMasterInfo.Rows(0).Item("bitPostRegistration") 'Display check/ uncheck for going to resistration at the end of the Survey
                    cbPostRegistration.Disabled = dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration")

                    txtSurveyRedirect.Text = ReSetFromXML(CStr(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("vcSurveyRedirect")), "", dtSurveyMasterInfo.Rows(0).Item("vcSurveyRedirect"))))

                    cbNewRelationship.Checked = dtSurveyMasterInfo.Rows(0).Item("bitRelationshipProfile")
                    cbNewRelationship.Disabled = dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration")

                    cbCreateRecord.Checked = dtSurveyMasterInfo.Rows(0).Item("bitCreateRecord")
                    cbCreateRecord.Disabled = dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration")

                    cbRedirect.Checked = dtSurveyMasterInfo.Rows(0).Item("bitSurveyRedirect")
                    cbRedirect.Disabled = dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration")

                    cbEmail.Checked = dtSurveyMasterInfo.Rows(0).Item("bitEmailTemplateContact")
                    cbEmail.Disabled = dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration")

                    cbRecordOwner.Checked = dtSurveyMasterInfo.Rows(0).Item("bitEmailTemplateRecordOwner")
                    cbRecordOwner.Disabled = dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration")

                    cbLandingPage.Checked = dtSurveyMasterInfo.Rows(0).Item("bitLandingPage")
                    cbLandingPage.Disabled = dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration")

                    cbNewRelationship.Disabled = dtSurveyMasterInfo.Rows(0).Item("bitCreateRecord")
                    cbCreateRecord.Disabled = dtSurveyMasterInfo.Rows(0).Item("bitRelationshipProfile")

                    txtLandingPageYes.Text = ReSetFromXML(CStr(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("vcLandingPageYes")), "", dtSurveyMasterInfo.Rows(0).Item("vcLandingPageYes"))))
                    txtLandingPageNo.Text = ReSetFromXML(CStr(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("vcLandingPageNo")), "", dtSurveyMasterInfo.Rows(0).Item("vcLandingPageNo"))))
                    txtLandingPageYes.Enabled = IIf(dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration"), False, True)
                    txtLandingPageNo.Enabled = IIf(dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration"), False, True)

                    LoadProfile()

                    If cbNewRelationship.Checked Then
                        If ddlRelationShip.Items.FindByValue(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRelationShipId")), 0, dtSurveyMasterInfo.Rows(0).Item("numRelationShipId")).ToString) IsNot Nothing Then
                            ddlRelationShip.Items.FindByValue(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRelationShipId")), 0, dtSurveyMasterInfo.Rows(0).Item("numRelationShipId")).ToString).Selected = True
                        End If

                        If ddlCRMType.Items.FindByValue(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("tIntCRMType")), 0, dtSurveyMasterInfo.Rows(0).Item("tIntCRMType")).ToString) IsNot Nothing Then
                            ddlCRMType.Items.FindByValue(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("tIntCRMType")), 0, dtSurveyMasterInfo.Rows(0).Item("tIntCRMType")).ToString).Selected = True
                        End If

                        If ddlGroup.Items.FindByValue(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numGrpId")), 0, dtSurveyMasterInfo.Rows(0).Item("numGrpId")).ToString) IsNot Nothing Then
                            ddlGroup.Items.FindByValue(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numGrpId")), 0, dtSurveyMasterInfo.Rows(0).Item("numGrpId")).ToString).Selected = True
                        End If

                        If ddlRecordOwner.Items.FindByValue(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRecOwner")), 0, dtSurveyMasterInfo.Rows(0).Item("numRecOwner")).ToString) IsNot Nothing Then
                            ddlRecordOwner.Items.FindByValue(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRecOwner")), 0, dtSurveyMasterInfo.Rows(0).Item("numRecOwner")).ToString).Selected = True
                        End If

                        LoadProfile()

                        If ddlProfile.Items.FindByValue(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numProfileId")), 0, dtSurveyMasterInfo.Rows(0).Item("numProfileId")).ToString) IsNot Nothing Then
                            ddlProfile.Items.FindByValue(IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numProfileId")), 0, dtSurveyMasterInfo.Rows(0).Item("numProfileId")).ToString).Selected = True
                        End If
                    End If


                    ddlEmailTemplate1.SelectedValue = dtSurveyMasterInfo.Rows(0).Item("numEmailTemplate1Id")
                    ddlEmailTemplate2.SelectedValue = dtSurveyMasterInfo.Rows(0).Item("numEmailTemplate2Id")

                    txtSurveyRedirect.Enabled = IIf(dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration"), False, True)

                    ddlRelationShip.Enabled = IIf(dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration"), False, True)
                    ddlProfile.Enabled = IIf(dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration"), False, True)

                    ddlCRMType.Enabled = IIf(dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration"), False, True)
                    ddlGroup.Enabled = IIf(dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration"), False, True)
                    ddlRecordOwner.Enabled = IIf(dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration"), False, True)

                    ddlEmailTemplate1.Enabled = IIf(dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration"), False, True)
                    ddlEmailTemplate2.Enabled = IIf(dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration"), False, True)

                    btnCloneSurvey.Enabled = True                                   'Cloning the survey is possible here
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to handle savign the survey when the hidden button for Savign the Survey
        '''     "btnSaveSurveyActualButHidden" button is clicked
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Private Sub btnSaveSurveyActualButHidden_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveSurveyActualButHidden.Click, btnSaveAndCloseActualButHidden.Click, btnCloneSurvey.Click
            Try
                Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
                Dim btnSrcButton As Button = CType(sender, Button)                  'typecast to button
                objSurvey.DomainId = Session("DomainID")                            'Set the Domain ID
                objSurvey.UserId = Session("UserID")                                'Set the User ID
                objSurvey.XMLFilePath = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" 'Set the path to the xml file where the survey info is temp stored
                objSurvey.SurveyId = hdSurId.Value                                  'Updating an existing Survey Information
                objSurvey.SurveyName = MakeSafeForXML(txtSurveyName.Text)           'Set the Survey Name
                objSurvey.SkipRegistration = IIf(cbSkipRegistration.Checked, 1, 0)  'Sets the Skip Registration Flag
                objSurvey.RandomAnswers = IIf(cbRandomAnswers.Checked, 1, 0)        'Sets the Random Answers Flag
                objSurvey.SinglePageSurvey = IIf(cbSinglePageSurvey.Checked, 1, 0)  'Sets the Single Page Survey Flag
                objSurvey.PostSurveyRegistration = IIf(cbPostRegistration.Checked, 1, 0) 'Sets the Post Registration Survey Flag

                objSurvey.RelationshipProfile = IIf(cbNewRelationship.Checked, 1, 0) 'Sets the Post Registration Survey Flag
                objSurvey.SurveyRedirect = IIf(cbRedirect.Checked, 1, 0) 'Sets the Post Registration Survey Flag
                objSurvey.EmailTemplateContact = IIf(cbEmail.Checked, 1, 0) 'Sets the Post Registration Survey Flag
                objSurvey.EmailTemplateRecordOwner = IIf(cbRecordOwner.Checked, 1, 0) 'Sets the Post Registration Survey Flag
                objSurvey.LandingPage = IIf(cbLandingPage.Checked, 1, 0) 'Sets the Post Registration Survey Flag

                objSurvey.CreateRecord = IIf(cbCreateRecord.Checked, 1, 0) 'Sets the Post Registration Survey Flag

                If cbLandingPage.Checked Then
                    objSurvey.LandingPageYes = MakeSafeForXML(txtLandingPageYes.Text)
                    objSurvey.LandingPageNo = MakeSafeForXML(txtLandingPageNo.Text)
                Else
                    objSurvey.LandingPageYes = ""
                    objSurvey.LandingPageNo = ""
                End If

                objSurvey.RedirectionURL = MakeSafeForXML(txtRedirectionURL.Text)   'Sets the Redirection URL

                If cbRedirect.Checked Then
                    objSurvey.SurveyRedirectURL = MakeSafeForXML(txtSurveyRedirect.Text)   'Sets the Redirection URL
                Else
                    objSurvey.SurveyRedirectURL = ""
                End If

                objSurvey.RelationShipId = IIf(cbNewRelationship.Checked, ddlRelationShip.SelectedValue, 0)

                If ddlProfile.SelectedValue.Length > 0 Then
                    objSurvey.ProfileId = IIf(cbNewRelationship.Checked, ddlProfile.SelectedValue, 0)
                Else
                    objSurvey.ProfileId = 0
                End If

                objSurvey.CRMType = IIf(cbNewRelationship.Checked, ddlCRMType.SelectedValue, 0)
                objSurvey.GrpId = IIf(cbNewRelationship.Checked, ddlGroup.SelectedValue, 0)
                objSurvey.RecOwner = IIf(cbNewRelationship.Checked, ddlRecordOwner.SelectedValue, 0)

                objSurvey.EmailTemplate1Id = IIf(cbEmail.Checked, ddlEmailTemplate1.SelectedValue, 0)
                objSurvey.EmailTemplate2Id = IIf(cbRecordOwner.Checked, ddlEmailTemplate2.SelectedValue, 0)

                objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()         'Call to retrieve the existing data temporarily

                'Check For Rule #3 and New Relationship Conflict
                'Dim dtSurveyWorkFlowRules As DataTable
                'dtSurveyWorkFlowRules = objSurvey.GetSurveyWorkflowRulesDetailsForSurvey()
                'Dim drWorkFlowRules As DataRow                                                          'Declare a DataRow
                'If dtSurveyWorkFlowRules.Rows.Count > 0 Then
                '    For Each drWorkFlowRules In dtSurveyWorkFlowRules.Rows                              'Loop through the rules table
                '        If drWorkFlowRules.Item("numRuleID") = 3 AndAlso cbNewRelationship.Checked Then
                '            litClientAlertMessageScript.Text = "<script language='javascript'>alert('The answers have rule #3 checked, which creates a conflict. Please uncheck those rules and try again');</script>"
                '            DisplaySurveyMasterInformation()
                '            Exit Sub
                '        End If
                '    Next
                'End If

                objSurvey.AddSurveyMasterInformationTable()                         'Call to Add the Survey Master Information Table
                If btnSrcButton.ID.ToString() = "btnCloneSurvey" Then objSurvey.SurveyId = 0 'Cloning the survey is inserting the same survey with a new id
                hdSurId.Value = objSurvey.SaveSurveyInformationInDatabase()         'Call to Save the Survey Information in teh database and retrieves the ID
                objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID
                If btnSrcButton.ID.ToString() = "btnSaveAndCloseActualButHidden" Or btnSrcButton.ID.ToString() = "btnCloneSurvey" Then
                    objSurvey.SurveyId = 0                                          'Cloning the survey is inserting the same survey with a new id
                    objSurvey.DeleteTempSurveyInformation()                         'Requests deletion of the temporary file
                    objSurvey.SurveyId = hdSurId.Value                              'Set the Survey ID
                    objSurvey.DeleteTempSurveyInformation()                         'Requests deletion of the temporary file
                    Response.Redirect("frmCampaignSurveyList.aspx", True)           'Redirect to the Survey List Screen
                Else
                    DisplaySurveyMasterInformation()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is to calcel the entire survey and go back to the list page
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Private Sub btnCancelSurvey_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelSurvey.Click
            Try
                Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
                objSurvey.DomainId = Session("DomainID")                            'Set the Domain ID
                objSurvey.UserId = Session("UserID")                                'Set the User ID
                objSurvey.XMLFilePath = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" 'Set the path to the xml file where the survey info is temp stored
                objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID
                objSurvey.DeleteTempSurveyInformation()                             'Requests deletion of the temporary file
                Response.Redirect("frmCampaignSurveyList.aspx", True)               'Redirect to Survey List Page
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is safe casts the string for XML
        ''' </summary>
        ''' <param name = "sValue">The string which is to be made XML safe</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function MakeSafeForXML(ByVal sValue As String) As String
            Try
                Return Replace(Replace(Replace(Replace(Replace(sValue, "'", "&#39;"), """", "&#39;&#39;"), "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is reverts the string to its former html display format
        ''' </summary>
        ''' <param name = "sValue">The string which is to be made XML safe</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function ReSetFromXML(ByVal sValue As String) As String
            Try
                Return Replace(Replace(Replace(Replace(sValue, "&amp;", "&"), "&#39;", "'"), "&lt;", "<"), "&gt;", ">")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to attach client side events to the controls
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub PostInitializeControlsClientEvents()
            Try
                btnSaveQuestions.Attributes.Add("onclick", "javascript: return RequestSurveyQuestionnaireSave();")     'calls for saving the questions
                btnAddNewQuestion.Attributes.Add("onclick", "javascript: return RequestSurveyQuestionnaireNew();")     'calls for adding a new question
                btnSaveSurvey.Attributes.Add("onclick", "javascript: return RequestSurveySave('SaveB4SavingSurvey');") 'calls for saving the survey along with the questionsa dn answers and rules
                btnSaveAndClose.Attributes.Add("onclick", "javascript: return RequestSurveySave('SaveB4SavingAndClosingSurvey');") 'calls for saving the survey along with the questionsa dn answers and rules
                btnCloneSurvey.Attributes.Add("onclick", "javascript: return RequestSurveyClone();")                   'calls for cloning the survey along with the questions and answers and rules
                If cbPostRegistration.Checked = True Then              'Request to display the survey registration at the end
                    btnOpenSurveyQuestionnaire.Attributes.Add("onclick", "javascript: return RequestToViewSurveyQuestionnaireWithPostReg('" & ConfigurationManager.AppSettings("SurveyScreen").Replace("frmGenericFormSurvey.aspx", "frmSurveyExecutionSurvey.aspx") & "?D=" & Session("DomainID") & "&numSurId=" & hdSurId.Value & "');") 'calls for opening the Survey Questionnaire with post registration
                Else
                    If cbSkipRegistration.Checked = True Then
                        btnOpenSurveyQuestionnaire.Attributes.Add("onclick", "javascript: return RequestToViewSurveyQuestionnaire('" & ConfigurationManager.AppSettings("SurveyScreen") & "?D=" & Session("DomainID") & "','Y');") 'calls for opening the Survey Questionnaire
                    Else : btnOpenSurveyQuestionnaire.Attributes.Add("onclick", "javascript: return RequestToViewSurveyQuestionnaire('" & ConfigurationManager.AppSettings("SurveyScreen") & "?D=" & Session("DomainID") & "','N');") 'calls for opening the Survey Questionnaire
                    End If
                End If
                btnOpenRegPage.Attributes.Add("onclick", "javascript: return RequestToViewSurveyRegistrationScreen('" & ConfigurationManager.AppSettings("SurveyScreen") & "?D=" & Session("DomainID") & "','Y');")   'calls for opening the Survey Registration Screen
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlRelationShip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationShip.SelectedIndexChanged
            LoadProfile()
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
    End Class
End Namespace