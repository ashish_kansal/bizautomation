﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSurveyCss.aspx.vb" Inherits=".frmSurveyCss"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script src="../JavaScript/EditArea/edit_area_full.js" type="text/javascript"></script>
    <script type="text/JavaScript">

        function Save() {
            if (document.form1.txtStyleName.value == "") {
                alert("Enter " + document.getElementById("lblName1").innerHTML + " Name")
                document.form1.txtStyleName.focus()
                return false;
            }
            if (document.form1.fuCss.value == "") {
                alert("Select " + document.getElementById("lblName2").innerHTML + " to upload")
                document.form1.fuCss.focus()
                return false;
            }
        }

        editAreaLoader.init({
            id: "txtEditCss"		// textarea id
        	, syntax: "css"			// syntax to be uses for highgliting
	        , start_highlight: true		// to display with highlight mode on start-up
            , toolbar: "search, go_to_line, undo, redo, select_font"
        });

    </script>
    <style>
        .EditCss
        {
            font-family: arial;
            font-size: 10pt;
            color: Black;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;New
                            <asp:Label ID="lblName" runat="server"></asp:Label>
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" Text="Save &amp; Close" CssClass="button"
                    OnClientClick="javascript:return Save();"></asp:Button>
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                </asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
        runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table width="100%" border="0">
                    <tr>
                        <td class="text" align="right">
                            <asp:Label ID="lblName1" runat="server"></asp:Label>&nbsp;Name<font color="#ff0000">*</font>
                        </td>
                        <td>
                            <asp:TextBox ID="txtStyleName" runat="server" CssClass="signup" Width="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trFileUpload" runat="server">
                        <td class="normal1" align="right" valign="top">
                            Select a&nbsp;<asp:Label ID="lblName2" runat="server"></asp:Label>&nbsp;to upload
                        </td>
                        <td colspan="3">
                            <asp:FileUpload ID="fuCss" runat="server" CssClass="signup" />
                        </td>
                    </tr>
                    <tr id="trEditCss" runat="server" visible="false">
                        <td class="normal1" align="right" valign="top">
                            <asp:Label ID="lblCssFileName" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox TextMode="MultiLine" ID="txtEditCss" runat="server" CssClass="EditCss"
                                Width="600" Height="400"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnCssFileName" runat="server" />
    </form>
</body>
</html>
