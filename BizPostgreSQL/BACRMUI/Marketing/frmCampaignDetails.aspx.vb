Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Accounting

Namespace BACRM.UserInterface.Marketing
    Public Class frmCampaignDetails
        Inherits BACRMPage

        Dim lngCampaignID As Long
       
        Dim objCommon As CCommon

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
     

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngCampaignID = GetQueryStringVal( "CampID")
                If Not IsPostBack Then
                    DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                    DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                    AddToRecentlyViewed(RecetlyViewdRecordType.Campaign, lngCampaignID)
                    ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "MarCampaign"
                    'To Set Permission
                    objCommon = New CCommon
                    GetUserRightsForPage(6, 2)
                    ''''call function for sub-tab management  - added on 29jul09 by Mohan
                    'objCommon.ManageSubTabs(radOppTab, Session("DomainID"), 6)
                    '''''
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                    End If
                    LoadDropdowns()
                    If lngCampaignID > 0 Then
                        loadDetails()
                        BindCompDetails()
                        BindOppDetails()
                        BindCostDetails()
                    End If
                End If

                btnSave.Attributes.Add("onclick", "return Save('" & CCommon.GetValidationDateFormat() & "')")
                btnSaveClose.Attributes.Add("onclick", "return Save('" & CCommon.GetValidationDateFormat() & "')")
                If (hplTransfer.Visible = False) Then
                    hplTransferNonVis.Visible = True
                End If
                'txtCost.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtNoSent.Attributes.Add("onkeypress", "CheckNumber(2)")
                ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('3');}else{ parent.SelectTabByValue('3'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub

        Sub loadDetails()
            Try
                Dim objCamapign As New Campaign
                Dim dtCamapaigndtls As DataTable
                objCamapign.CampaignID = lngCampaignID
                objCamapign.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtCamapaigndtls = objCamapign.CampaignDetails
                If dtCamapaigndtls.Rows.Count > 0 Then
                    If Not IsDBNull(dtCamapaigndtls.Rows(0).Item("vcCampaignName")) Then
                        txtCampaignName.Text = dtCamapaigndtls.Rows(0).Item("vcCampaignName")
                    End If
                    If Not IsDBNull(dtCamapaigndtls.Rows(0).Item("numCampaignStatus")) Then
                        If Not dllCampaignStatus.Items.FindByValue(dtCamapaigndtls.Rows(0).Item("numCampaignStatus")) Is Nothing Then
                            dllCampaignStatus.Items.FindByValue(dtCamapaigndtls.Rows(0).Item("numCampaignStatus")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtCamapaigndtls.Rows(0).Item("numCampaignType")) Then
                        If Not ddlCampaignType.Items.FindByValue(dtCamapaigndtls.Rows(0).Item("numCampaignType")) Is Nothing Then
                            ddlCampaignType.Items.FindByValue(dtCamapaigndtls.Rows(0).Item("numCampaignType")).Selected = True
                            objCamapign.CampaignType = dtCamapaigndtls.Rows(0).Item("numCampaignType")
                            objCamapign.DomainID = Session("DomainId")
                            objCamapign.GetMarketBudgetCampaign()
                            If objCamapign.bitMarketCampaign = True AndAlso dtCamapaigndtls.Rows(0).Item("numCampaignType") <> 0 Then
                                'lblCampaignBudgetMonth.Text = String.Format("{0:#,###.00}", objCamapign.MonthlyAmt, 2) & "(mo)"
                                'lblCampaignBudgetYear.Text = String.Format("{0:#,###.00}", objCamapign.YearlyAmt, 2) & "(Yr)"
                            Else
                                'lblCampaignBudget.Visible = False
                                'lblCampaignBudgetMonth.Visible = False
                                'lblCampaignBudgetYear.Visible = False
                            End If
                        End If
                    End If

                    If Not IsDBNull(dtCamapaigndtls.Rows(0).Item("intLaunchDate")) Then calLaunch.SelectedDate = dtCamapaigndtls.Rows(0).Item("intLaunchDate")
                    'If Not IsDBNull(dtCamapaigndtls.Rows(0).Item("intEndDate")) Then calEnd.SelectedDate = dtCamapaigndtls.Rows(0).Item("intEndDate")

                    If Not IsDBNull(dtCamapaigndtls.Rows(0).Item("numRegion")) Then
                        If Not ddlRegion.Items.FindByValue(dtCamapaigndtls.Rows(0).Item("numRegion")) Is Nothing Then
                            ddlRegion.Items.FindByValue(dtCamapaigndtls.Rows(0).Item("numRegion")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtCamapaigndtls.Rows(0).Item("numNoSent")) Then
                        txtNoSent.Text = String.Format("{0:####0}", dtCamapaigndtls.Rows(0).Item("numNoSent"), 2)
                    End If
                    'If Not IsDBNull(dtCamapaigndtls.Rows(0).Item("monCampaignCost")) Then
                    '    lblCost.Text = String.Format("{0:####0.00}", dtCamapaigndtls.Rows(0).Item("monCampaignCost"), 2)
                    'End If

                    If Not IsDBNull(dtCamapaigndtls.Rows(0).Item("numTotalCampaignCost")) Then
                        lblCost.Text = String.Format("{0:####0.00}", dtCamapaigndtls.Rows(0).Item("numTotalCampaignCost"), 2)
                    End If

                    'If Not IsDBNull(dtCamapaigndtls.Rows(0).Item("bitIsOnline")) Then
                    '    If Not rblCampaignMode.Items.FindByValue(dtCamapaigndtls.Rows(0).Item("bitIsOnline")) Is Nothing Then
                    '        rblCampaignMode.Items.FindByValue(dtCamapaigndtls.Rows(0).Item("bitIsOnline")).Selected = True
                    '        If rblCampaignMode.SelectedValue = "True" Then
                    '            trCampaignCode.Visible = True
                    '        Else
                    '            trCampaignCode.Visible = False
                    '        End If
                    '    End If
                    'End If
                    chkMonthly.Checked = dtCamapaigndtls.Rows(0).Item("bitIsMonthly")
                    chkActive.Checked = dtCamapaigndtls.Rows(0).Item("bitActive")
                    'If Not IsDBNull(dtCamapaigndtls.Rows(0).Item("vcCampaignCode")) Then
                    '    txtCampaignCode.Text = dtCamapaigndtls.Rows(0).Item("vcCampaignCode")
                    'End If
                    lblCreatedBy.Text = dtCamapaigndtls.Rows(0).Item("CreatedBy") & "&nbsp;" & dtCamapaigndtls.Rows(0).Item("CreatedDate")
                    lblLastModifiedBy.Text = dtCamapaigndtls.Rows(0).Item("ModifiedBy") & "&nbsp;" & dtCamapaigndtls.Rows(0).Item("ModifiedDate")
                    lblRecordOwner.Text = dtCamapaigndtls.Rows(0).Item("CreatedBy")

                    lblCostMfgDeal.Text = String.Format("{0:#,###.##}", dtCamapaigndtls.Rows(0).Item("CostMFG"), 2)
                    If dtCamapaigndtls.Rows(0).Item("ROI") > 0 Then
                        lblROI.Text = "<font color=Green><b>" & String.Format("{0:#,###.##}", dtCamapaigndtls.Rows(0).Item("ROI"), 2) & "</b></font>"
                    ElseIf dtCamapaigndtls.Rows(0).Item("ROI") < 0 Then
                        lblROI.Text = "<font color=Red>" & String.Format("{0:#,###.##}", dtCamapaigndtls.Rows(0).Item("ROI"), 2) & "</font>"
                    End If
                    lblTotalIncDealsWon.Text = String.Format("{0:#,###.##}", dtCamapaigndtls.Rows(0).Item("TI"), 2)
                    lblTotalIncOpenOpp.Text = String.Format("{0:#,###.##}", dtCamapaigndtls.Rows(0).Item("TIP"), 2)
                    'lblCOGS.Text = String.Format("{0:#,###.##}", dtCamapaigndtls.Rows(0).Item("COGS"), 2)
                    'lblCostofLabor.Text = String.Format("{0:#,###.##}", dtCamapaigndtls.Rows(0).Item("LaborCost"), 2)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadDropdowns()
            Try
                Dim objTab As New Tabs
                Dim dtRelationship As DataTable
                objTab.ListId = 5
                objTab.DomainID = Session("DomainID")
                objTab.mode = False
                dtRelationship = objTab.GetTabMenuItem()
                Dim i As Integer
                For i = 0 To dtRelationship.Rows.Count - 1
                    ddlFilterCompany.Items.Insert(4 + i, dtRelationship.Rows(i).Item("vcData"))
                    ddlFilterCompany.Items.FindByText(dtRelationship.Rows(i).Item("vcData")).Value = dtRelationship.Rows(i).Item("numListItemID")
                Next

                'objCommon.sb_FillComboFromDBwithSel(ddlCampaignName, 24, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(dllCampaignStatus, 23, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlCampaignType, 22, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlRegion, 38, Session("DomainID"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                If Save() = True Then
                    If GetQueryStringVal("frm") = "CampPer" Then
                        Response.Redirect("../Marketing/frmCampaignPerformance.aspx")
                    ElseIf GetQueryStringVal("frm") = "CampRep" Then
                        Response.Redirect("../reports/frmCampaign.aspx")
                    Else : Response.Redirect("../Marketing/frmCampaignList.aspx")
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Function Save() As Boolean
            Try
                Dim objCampaign As New Campaign
                objCampaign.CampaignID = lngCampaignID
                objCampaign.CampaignName = txtCampaignName.Text.Trim
                objCampaign.CampaignStatus = dllCampaignStatus.SelectedItem.Value
                objCampaign.CampaignType = ddlCampaignType.SelectedItem.Value
                objCampaign.CampaignRegion = ddlRegion.SelectedItem.Value
                objCampaign.LaunchDate = calLaunch.SelectedDate
                'end date is obsolete
                objCampaign.EndDate = calLaunch.SelectedDate 'calEnd.SelectedDate
                objCampaign.CampaignCost = IIf(Replace(lblCost.Text, ",", "") = "", 0, Replace(lblCost.Text, ",", ""))
                objCampaign.NoSent = IIf(Replace(txtNoSent.Text, ",", "") = "", 0, Replace(txtNoSent.Text, ",", ""))
                objCampaign.IsOnline = False 'rblCampaignMode.SelectedItem.Value
                objCampaign.IsMonthly = chkMonthly.Checked
                objCampaign.CampaignCode = "" 'txtCampaignCode.Text
                objCampaign.DomainID = Session("DomainID")
                objCampaign.UserCntID = Session("UserContactID")
                objCampaign.byteMode = 1 'IIf(rblCampaignMode.SelectedValue, 2, 1)
                'Commented by chintan reason - bug id 465 #2
                'If objCampaign.CampaignOverlapChecking = 0 Then
                objCampaign.IsActive = chkActive.Checked
                objCampaign.CampaignManage()
                If objCampaign.IsDuplicate Then
                    litMessage.Text = "A campaign with given Campaign Code is found. Choose different Campaign Code."
                    Return False
                End If
                Return True
                'Else
                'litMessage.Text = "A campaign with this name is active for a portion of the date range you've chosen. Choose a date range that doesn't overlap."
                'Return False
                'End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                If GetQueryStringVal("frm") = "CampPer" Then
                    Response.Redirect("../Marketing/frmCampaignPerformance.aspx")
                ElseIf GetQueryStringVal("frm") = "CampRep" Then
                    Response.Redirect("../reports/frmCampaign.aspx")
                Else : Response.Redirect("../Marketing/frmCampaignList.aspx")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BindCompDetails()
            Try
                Dim objCampaign As New Campaign
                Dim dtCompList As DataTable
                objCampaign.CampaignID = lngCampaignID
                objCampaign.SearchType = ddlFilterCompany.SelectedItem.Value

                objCampaign.StartDate = DateTime.Now.UtcNow.AddDays(-ddlNoOfDays.SelectedValue).Date
                objCampaign.EndDate = DateTime.Now.UtcNow.Date.AddDays(1)

                dtCompList = objCampaign.CampaignCompList
                dgCompany.DataSource = dtCompList
                dgCompany.DataBind()
                Session("dtCompList") = dtCompList
                lblNoOfRecdComp.Text = objCampaign.TotalRecords
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindOppDetails()
            Try
                Dim objCampaign As New Campaign
                Dim dtOppList As DataTable
                objCampaign.CampaignID = lngCampaignID
                objCampaign.SearchType = ddlFilterOpp.SelectedItem.Value
                dtOppList = objCampaign.CampaignOppList
                dgOpportunity.DataSource = dtOppList
                dgOpportunity.DataBind()
                Session("dtOppList") = dtOppList
                lblNoOfRecdOpp.Text = objCampaign.TotalRecords
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlFilterCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterCompany.SelectedIndexChanged
            Try
                BindCompDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlFilterOpp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterOpp.SelectedIndexChanged
            Try
                BindOppDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgOpportunity_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgOpportunity.SortCommand
            Try
                Dim dt As DataTable
                dt = Session("dtOppList")

                Dim dv As DataView = New DataView(dt)
                If Session("Order") = "ASC" Then
                    dv.Sort = e.SortExpression.ToString() & " DESC"
                    Session("Order") = "DESC"
                Else
                    dv.Sort = e.SortExpression.ToString() & " ASC"
                    Session("Order") = "ASC"
                End If
                'dv.Sort = e.SortExpression
                'Session("dv") = dv
                dgOpportunity.DataSource = dv
                dgOpportunity.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgCompany_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCompany.ItemCommand
            Try
                If e.CommandName = "Company" Then
                    Dim CRMType As Short = e.Item.Cells(3).Text
                    If CRMType = 0 Then
                        Response.Redirect("../Leads/frmLeads.aspx?frm=CampDetails&CmpID=" & e.Item.Cells(0).Text & "&DivID=" & e.Item.Cells(1).Text & "&CRMTYPE=0&CntID=" & e.Item.Cells(2).Text & "&CampID=" & lngCampaignID)
                    ElseIf CRMType = 1 Then
                        Response.Redirect("../prospects/frmProspects.aspx?frm=CampDetails&CmpID=" & e.Item.Cells(0).Text & "&DivID=" & e.Item.Cells(1).Text & "&CRMTYPE=1&CntID=" & e.Item.Cells(2).Text & "&CampID=" & lngCampaignID)
                    ElseIf CRMType = 2 Then
                        Response.Redirect("../Account/frmAccounts.aspx?frm=CampDetails&&CmpID=" & e.Item.Cells(0).Text & "&klds+7kldf=fjk-las&DivId=" & e.Item.Cells(1).Text & "&CRMTYPE=2&CntID=" & e.Item.Cells(2).Text & "&CampID=" & lngCampaignID)
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgOpportunity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOpportunity.ItemCommand
            Try
                If e.CommandName = "Company" Then
                    Dim CRMType As Short = e.Item.Cells(3).Text
                    If CRMType = 0 Then
                        Response.Redirect("../Leads/frmLeads.aspx?frm=CampDetails&CmpID=" & e.Item.Cells(0).Text & "&DivID=" & e.Item.Cells(1).Text & "&CRMTYPE=0&CntID=" & e.Item.Cells(2).Text & "&CampID=" & lngCampaignID)
                    ElseIf CRMType = 1 Then
                        Response.Redirect("../prospects/frmProspects.aspx?frm=CampDetails&CmpID=" & e.Item.Cells(0).Text & "&DivID=" & e.Item.Cells(1).Text & "&CRMTYPE=" & 1 & "&CntID=" & e.Item.Cells(2).Text & "&CampID=" & lngCampaignID)
                    ElseIf CRMType = 2 Then
                        Response.Redirect("../Account/frmAccounts.aspx?frm=CampDetails&&CmpID=" & e.Item.Cells(0).Text & "&klds+7kldf=fjk-las&DivId=" & e.Item.Cells(1).Text & "&CRMTYPE=" & 2 & "&CntID=" & e.Item.Cells(2).Text & "&CampID=" & lngCampaignID)
                    End If
                End If
                If e.CommandName = "Name" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?frm=CampDetails&&opID=" & e.Item.Cells(4).Text & "&CampID=" & lngCampaignID)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgCompany_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgCompany.SortCommand
            Try
                Dim dt As DataTable
                dt = Session("dtCompList")

                Dim dv As DataView = New DataView(dt)
                If Session("Order") = "ASC" Then
                    dv.Sort = e.SortExpression.ToString() & " DESC"
                    Session("Order") = "DESC"
                Else
                    dv.Sort = e.SortExpression.ToString() & " ASC"
                    Session("Order") = "ASC"
                End If
                'dv.Sort = e.SortExpression
                'Session("dv") = dv
                dgCompany.DataSource = dv
                dgCompany.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Function ReturnMoney(ByVal Money)
            Try
                If Money Is Nothing Then Money = 0
                Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlCampaignType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampaignType.SelectedIndexChanged
            Try
                If ddlCampaignType.SelectedItem.Value <> 0 Then
                    Dim objCampaign As New Campaign
                    objCampaign.CampaignID = lngCampaignID
                    objCampaign.CampaignType = ddlCampaignType.SelectedItem.Value
                    objCampaign.DomainID = Session("DomainId")
                    objCampaign.GetMarketBudgetCampaign()
                    If objCampaign.bitMarketCampaign = True Then
                        'lblCampaignBudget.Visible = True
                        'lblCampaignBudgetMonth.Visible = True
                        'lblCampaignBudgetYear.Visible = True

                        'lblCampaignBudgetMonth.Text = String.Format("{0:#,###.00}", objCampaign.MonthlyAmt, 2) & "(mo)"
                        'lblCampaignBudgetYear.Text = String.Format("{0:#,###.00}", objCampaign.YearlyAmt, 2) & "(Yr)"
                    Else
                        'lblCampaignBudget.Visible = False
                        'lblCampaignBudgetMonth.Visible = False
                        'lblCampaignBudgetYear.Visible = False
                    End If
                Else
                    'lblCampaignBudget.Visible = False
                    'lblCampaignBudgetMonth.Visible = False
                    'lblCampaignBudgetYear.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        'Private Sub rblCampaignMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblCampaignMode.SelectedIndexChanged
        '    Try
        '        If rblCampaignMode.SelectedValue = "True" Then
        '            trCampaignCode.Visible = True
        '        Else
        '            trCampaignCode.Visible = False
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Private Sub ddlNoOfDays_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlNoOfDays.SelectedIndexChanged
            Try
                BindCompDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub BindCostDetails()
            Try
                Dim objCampaign As New Campaign
                Dim dtCostDetails As DataTable
                objCampaign.CampaignID = lngCampaignID
                objCampaign.DomainID = Session("DomainID")
                dtCostDetails = objCampaign.GetCostDetailsForCampaign()
                dgCostDetails.DataSource = dtCostDetails
                dgCostDetails.DataBind()
                Session("dtCostDetails") = dtCostDetails
                lblNoOfRecdOpp.Text = objCampaign.TotalRecords
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgCostDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCostDetails.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim hrfOpenBill As HyperLink = e.Item.FindControl("hrfOpenBill")
                    If hrfOpenBill IsNot Nothing Then
                        hrfOpenBill.Attributes.Add("onclick", "return OpenBill(" & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numBillID")) & ");")
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgCostDetails_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgCostDetails.SortCommand
            Try
                Dim dt As DataTable
                dt = Session("dtCostDetails")

                Dim dv As DataView = New DataView(dt)
                If Session("Order") = "ASC" Then
                    dv.Sort = e.SortExpression.ToString() & " DESC"
                    Session("Order") = "DESC"
                Else
                    dv.Sort = e.SortExpression.ToString() & " ASC"
                    Session("Order") = "ASC"
                End If
                'dv.Sort = e.SortExpression
                'Session("dv") = dv
                dgCostDetails.DataSource = dv
                dgCostDetails.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
    End Class
End Namespace
