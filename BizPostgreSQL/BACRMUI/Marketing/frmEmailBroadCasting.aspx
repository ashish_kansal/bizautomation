<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmEmailBroadCasting.aspx.vb"
    ValidateRequest="false" Inherits="frmEmailBroadCasting" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Email BroadCasting</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)" />
    <script type="text/javascript">
        function OpenSearchResult(a, b, c) {
            if (b == 15) {
                document.location.href = '../admin/frmItemSearchRes.aspx?SearchID=' + a + '&BroadcastId=' + c;
            }
            if (b == 1) {
                document.location.href = '../admin/frmAdvancedSearchRes.aspx?SearchID=' + a + '&BroadcastId=' + c + "&frm=EmailBroadcasting";
            }
            if (b == 29) {
                document.location.href = '../admin/frmAdvSerInvItemsRes.aspx?SearchID=' + a + '&BroadcastId=' + c;
            }
        }
        function Close() {
            document.location.href = '../Marketing/frmBroadCastList.aspx'
            return false;
        }
        function BroMessage() {
            var tempv = false;
            if (document.all) {
                if (document.getElementById('lblRecords').innerText == 0)
                    tempv = true;
                else
                    tempv = false;
            } else {
                if (document.getElementById('lblRecords').textContent == 0)
                    tempv = true;
                else
                    tempv = false;
            }
            if ($("#chkFollowUpStatus").prop("checked") == true && $("#ddlFollowupStatus option:selected").val() == 0) {
                alert("Please select Follow Up Status");
                return false;
            }
            if (tempv) {
                alert("There is no recipient to broadcast the message !")
                return false;
            }

            if (document.getElementById("txtName").value == '') {
                alert("Enter Broadcast Name/Description.")
                document.getElementById("txtName").focus()
                return false;
            }
            else if (document.getElementById("txtSubject").value == '') {
                alert("Enter Subject.")
                document.getElementById("txtSubject").focus()
                return false;
            }
            else if (document.getElementById("ddlEmailTemplate").value == 0) {
                alert("Select a Template")
                document.getElementById("ddlEmailTemplate").focus()
                return false;
            }
            var bln;
            bln = confirm('You are about to broadcast your message to the entire list of recipients. Are you sure you want to do this ?')
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }

        }
        function SaveMessage() {
            var tempv = false;
            if (document.all) {
                if (document.getElementById('lblRecords').innerText == 0)
                    tempv = true;
                else
                    tempv = false;
            } else {
                if (document.getElementById('lblRecords').textContent == 0)
                    tempv = true;
                else
                    tempv = false;
            }
            if (tempv) {
                alert("There is no recipient to save the message !")
                return false;
            }

            if (document.getElementById("txtName").value == '') {
                alert("Enter  Name/Description.")
                document.getElementById("txtName").focus()
                return false;
            }
            else if (document.getElementById("txtSubject").value == '') {
                alert("Enter Subject.")
                document.getElementById("txtSubject").focus()
                return false;
            }
            else if (document.getElementById("ddlEmailTemplate").value == 0) {
                alert("Select a Template")
                document.getElementById("ddlEmailTemplate").focus()
                return false;
            }

            return true;
        }

        function SendMsgToMyself() {
            if (document.getElementById("txtName").value == '') {
                alert("Enter  Name/Description .")
                document.getElementById("txtName").focus()
                return false;
            }
            else if (document.getElementById("txtSubject").value == '') {
                alert("Enter Subject.")
                document.getElementById("txtSubject").focus()
                return false;
            }
            else if (document.getElementById("ddlEmailTemplate").value == 0) {
                alert("Select a Template")
                document.getElementById("ddlEmailTemplate").focus()
                return false;
            }
        }

        function ShowWindow(Page, q, att, a) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                if (a == 1) {
                    return true;
                }
                else {
                    return false;
                }


            }

        }
        function OpenImageList() {
            window.open('../Marketing/frmImageList.aspx', '', 'toolbar=no,titlebar=no,left=300, top=100,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenAttachments() {
            window.open('../Marketing/frmAddAttachments.aspx', '', 'toolbar=no,titlebar=no,left=300, top=100,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenSignature() {
            window.open('../Marketing/frmAddSignature.aspx', '', 'toolbar=no,titlebar=no,left=300, top=100,width=305,height=280,scrollbars=no,resizable=yes');
            return false;
        }
        function AttachmentDetails(a) {
            document.getElementById('lblAttachents').innerText = '';
            document.getElementById('lblAttachents').innerText = a;
            return false;
        }
        function Modify(a) {
            if (document.getElementById("ddlEmailTemplate").value == 0) {
                alert("Select a Template")
                document.getElementById("ddlEmailTemplate").focus()
                return false;
            }
        }
        function OnClientCommandExecuting(editor, args) {
            var name = args.get_name();
            var val = args.get_value();
            if (name == "MergeField") {
                editor.pasteHtml(val);
                //Cancel the further execution of the command as such a command does not exist in the editor command list
                args.set_cancel(true);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
            <div class="col-md-6 callout callout callout-success" id="divSendLimit" runat="server">
                <b>Max Send Limit 24 Hour:</b>&nbsp;<asp:Label ID="lblMax24Send" runat="server"></asp:Label>
            </div>
            <div class="col-md-4 callout callout-info" style="margin-left: 10px;" id="divSentMail" runat="server">
                <b>Sent Last 24 Hour:</b>&nbsp;<asp:Label ID="lblSentLast24Hour" runat="server"></asp:Label>
            </div>
        </div>
            <div class="pull-right">
                <asp:LinkButton ID="btnSend" runat="server" CssClass="btn btn-primary" OnClick="btnSend_Click"><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;Send</asp:LinkButton>
                <asp:LinkButton ID="btnSendToMyself" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Send Test Msg. to Myself</asp:LinkButton>
                <asp:LinkButton ID="btnBroadcast" runat="server" CssClass="btn btn-primary"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;BroadCast Message</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <div style="padding: 0px 0px 0px 16px">
                    <asp:Panel runat="server" ID="pnlMessage">
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                        <asp:Label ID="litSuccess" ForeColor="Green" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Prepare Message
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="col-md-8 col-md-offset-2">
        <div class="row padbottom10">
            <div class="col-md-12">
                <ul class="list-inline">
                    <li>
                        <asp:Label ID="lblRecords" runat="server"></asp:Label>&nbsp;Recipients</li>
                    <li>
                        <asp:HyperLink ID="hlView" Text="Add" runat="server"></asp:HyperLink></li>
                </ul>
            </div>
        </div>
        <div class="row padbottom10">
            <div class="col-md-12">
                <ul class="list-inline">
                    <li>
                        <asp:CheckBox ID="chkTrack" runat="server"></asp:CheckBox>&nbsp;Track This Message</li>
                    <li>
                        <asp:CheckBox ID="chkSendCopy" AutoPostBack="true" runat="server"></asp:CheckBox>&nbsp;Send copy to myself</li>
                    <li>
                        <asp:CheckBox ID="chkFollowUpStatus" runat="server" />Change Follow-up Status to:</li>
                    <li>
                        <asp:DropDownList ID="ddlFollowupStatus" CssClass="form-control" runat="server"></asp:DropDownList></li>
                    <li>
                        <asp:HyperLink ID="hplAttachments" NavigateUrl="#" runat="server">Attachments</asp:HyperLink>

                    </li>
                </ul>
            </div>
            <div class="col-md-12">
                <asp:Label ID="lblAttachents" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row padbottom10">
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label><b>Name</b>&nbsp;<span style="color: red">*</span></label>
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label style="width: 70px">Categry</label>
                    <asp:DropDownList ID="ddlCategory" AutoPostBack="True" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label>Email Template&nbsp;<span style="color: red">*</span></label>
                    <asp:DropDownList ID="ddlEmailTemplate" AutoPostBack="True" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label><b>Subject</b>&nbsp;<span style="color: red">*</span></label>
                    <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:HiddenField ID="hdnFollowupDateUpdateTemplate" Value="0" runat="server" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <telerik:RadEditor ID="RadEditor1" runat="server" Width="100%" Height="300" ToolsFile="EditorTools.xml"
                        OnClientPasteHtml="OnClientPasteHtml" OnClientCommandExecuting="OnClientCommandExecuting">
                        <Content>
                        </Content>
                        <Tools>
                            <telerik:EditorToolGroup>
                                <telerik:EditorDropDown Name="MergeField" Text="Merge Field">
                                </telerik:EditorDropDown>
                            </telerik:EditorToolGroup>
                        </Tools>
                    </telerik:RadEditor>
                </div>
            </div>
        </div>
    </div>
    <asp:Table ID="Table2" runat="server" BorderWidth="1" Width="100%" Height="300" BorderColor="black"
        CssClass="aspTable" GridLines="None" Style="display: none">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hdnConfigurationID" runat="server" />
    <asp:HiddenField ID="hdnFrom" runat="server" />
    <asp:HiddenField ID="hdnDomain" runat="server" />
    <asp:HiddenField ID="hdnAWSAccessKey" runat="server" />
    <asp:HiddenField ID="hdnAWSSecretKey" runat="server" />
    <asp:HiddenField ID="hdnDivIds" runat="server" />
    <asp:HiddenField ID="hdnStatementDate" runat="server" />
</asp:Content>
