<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAddAttachments.aspx.vb"
    Inherits="frmAddAttachments" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Attachments</title>
    <script type='text/javascript' src="../JavaScript/select2.js"></script>
    <link rel="stylesheet" href="../CSS/select2.css" />
    <script language="javascript" type="text/javascript">
        function Upload() {
            if (document.getElementById('radUpload').checked) {
                if (document.getElementById('fileupload').value == '') {
                    alert("Browse a file to upload!!")
                    return false;
                }
            }
            if (document.getElementById('radFile').checked) {
                if ($find('ctl00_Content_radCmbDoc').get_value() == "") {
                    alert("Select a file from document library!!")
                    return false;
                }
            }
        }

        function Close1() {
            var attch = document.getElementById('txtAttachements').value;
            window.opener.AttachmentDetails(attch)
            window.close()
            return false;
        }

        $(function () {
            var columns;
            var userDefaultPageSize = '<%= Session("PagingRows")%>';
            var userDefaultCharSearch = '<%= Session("ChrForItemSearch") %>';
            var varPageSize = parseInt(userDefaultPageSize, 10);
            var varCharSearch = 1;
            if (parseInt(userDefaultCharSearch, 10) > 0) {
                varCharSearch = parseInt(userDefaultCharSearch, 10);
            }

            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/GetSearchedItems',
                data: '{ searchText: "abcxyz", pageIndex: 1, pageSize: 10, divisionId: 0, isGetDefaultColumn: true, warehouseID: 0, searchOrdCusHistory: 0, oppType: 1, isTransferTo: false,IsCustomerPartSearch:false,searchType:"1"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    // Replace the div's content with the page method's return.
                    if (data.hasOwnProperty("d"))
                        columns = $.parseJSON(data.d)
                    else
                        columns = $.parseJSON(data);
                },
                results: function (data) {
                    columns = $.parseJSON(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });

            function formatItem(row) {
                var numOfColumns = 0;
                var ui = "<table style=\"width: 100%;font-size:12px;\" cellpadding=\"0\" cellspacing=\"0\">";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";
                ui = ui + "<td style=\"width:auto; padding: 3px; vertical-align:top; text-align:center\">";
                if (row["vcPathForTImage"] != null && row["vcPathForTImage"].indexOf('.') > -1) {
                    ui = ui + "<img id=\"Img7\" height=\"75\" width=\"75\" title=\"Item Image\" src=\"" + row["vcPathForTImage"] + "\" alt=\"Item Image\" >";
                } else {
                    ui = ui + "<img id=\"Img7\" height=\"75\" width=\"75\" title=\"Item Image\" src=\"" + "../images/icons/cart_large.png" + "\" alt=\"Item Image\" >";
                }

                ui = ui + "</td>";
                ui = ui + "<td style=\"width: 100%; vertical-align:top\">";
                ui = ui + "<table style=\"width: 100%;\" cellpadding=\"0\" cellspacing=\"0\"  >";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";

                $.each(columns, function (index, column) {
                    if (numOfColumns == 4) {
                        ui = ui + "</tr><tr>";
                        numOfColumns = 0;
                    }

                    if (numOfColumns == 0) {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:80%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    else {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:20%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    numOfColumns += 2;
                });
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";
                ui = ui + "</td>";
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";


                return ui;
            }

            $('#txtItem').on("change", function (e) {
                if (e.added != null) {
                    $('#hdnTempSelectedItems').val(JSON.stringify($('#txtItem').select2('data')));
                    $("#hdnTempSelectedItemWarehouse").val(e.added.numWareHouseItemID);
                    $("#hdnCurrentSelectedItem").val(e.added.id.toString().split("-")[0]);
                    document.getElementById("lkbItemSelected").click();
                }

            })


            $('#txtItem').on("select2-removed", function (e) {
                $("#hdnCurrentSelectedItem").val("");
                document.getElementById("lkbItemRemoved").click();
            })

            $('#txtItem').select2(
                {
                    placeholder: 'Select Items',
                    minimumInputLength: varCharSearch,
                    multiple: false,
                    formatResult: formatItem,
                    width: "550px",
                    dataType: "json",
                    allowClear: true,
                    ajax: {
                        quietMillis: 500,
                        url: '../common/Common.asmx/GetSearchedItems',
                        type: 'POST',
                        params: {
                            contentType: 'application/json; charset=utf-8'
                        },
                        dataType: 'json',
                        data: function (term, page) {
                            return JSON.stringify({
                                searchText: term,
                                pageIndex: page,
                                pageSize: varPageSize,
                                divisionId: 0,
                                isGetDefaultColumn: false,
                                warehouseID: 0,
                                searchOrdCusHistory: 0,
                                oppType: 1,
                                isTransferTo: false,
                                IsCustomerPartSearch: false,
                                searchType: "1"
                            });
                        },
                        results: function (data, page) {

                            if (data.hasOwnProperty("d"))
                                data = $.parseJSON(data.d)
                            else
                                data = $.parseJSON(data);

                            var more = (page.page * varPageSize) < data.Total;
                            return { results: $.parseJSON(data.results), more: more };
                        }
                    }
                });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="right-input" style="width: 700px">
        <div class="input-part">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <%--<asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload and close"></asp:Button>--%>
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager runat="server" ID="scriptManager">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" border="0" width="350px" align="left">
        <tr>
            <td valign="top" align="right" >
                <img id="imgDocument" runat="server" src="../images/icons/AttachSmall.png" alt="" /></td>
            <td valign="top" align="left" style="font-weight:bold;padding-left:2px;">
                Attach a Document 
            </td>
        </tr>
    </table>
    <br />
    <table cellspacing="0" cellpadding="0" border="0" width="700px" align="center">


        <tr>
            <td valign="top" style="text-align: right;">
                <i>From your local drive&nbsp;</i>
            </td>
            <td valign="top" style="padding: 5px;">
                <input class="signup" id="fileupload" type="file" name="fileupload" runat="server" />
            </td>
            <td valign="top">
                <asp:Button ID="btnAttachFile" runat="server" Text="Attach" CssClass="btn btn-primary" OnClick="btnAttachFile_Click"></asp:Button>
            </td>
        </tr>
        <tr runat="server" id="trDocuments">
            <td valign="top" style="text-align: right;">
                <i>From the record you�re in&nbsp;</i>
            </td>
            <td valign="top" style="padding: 5px;">
                <%-- <asp:DropDownList ID="ddldocuments" CssClass="signup" runat="server" Width="130">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>--%>
                <telerik:RadComboBox ID="radDocuments" runat="server" CheckBoxes="true" Width="350px">
                </telerik:RadComboBox>
            </td>
            <td valign="top">
                <asp:Button ID="btnAttachDocuments" runat="server" Text="Attach" CssClass="btn btn-primary" OnClick="btnAttachDocuments_Click"></asp:Button>
            </td>
        </tr>
        <tr runat="server" id="trParentOrganization">
            <td valign="top" style="text-align: right;" >
                <i>From the parent organization of the record you�re in&nbsp;</i>
            </td>
            <td valign="top" style="padding: 5px;">
                <telerik:RadComboBox ID="radParentRecord" runat="server" CheckBoxes="true" Width="350px">
                </telerik:RadComboBox>
            </td>
            <td valign="top">
                <asp:Button ID="btnParentRecord" runat="server" Text="Attach" CssClass="btn btn-primary" OnClick="btnParentRecord_Click"></asp:Button>
            </td>
        </tr>
        <tr runat="server" id="trBizDocs">
            <td valign="top" style="text-align: right;">
                <i>BizDocs&nbsp;</i>
            </td>
            <td valign="top" style="padding: 5px;">
                <telerik:RadComboBox ID="radBizDocsDocuments" runat="server" CheckBoxes="true" Width="350px">
                </telerik:RadComboBox>
                <%--<asp:DropDownList ID="ddlBizDocs" CssClass="signup" runat="server" Width="130">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>--%>
            </td>
            <td>
                <asp:Button ID="btnBizDocAttach" runat="server" Text="Attach" CssClass="btn btn-primary" OnClick="btnBizDocAttach_Click"></asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DataGrid ID="dgAttachments" runat="server" CssClass="dg" Width="100%"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn SortExpression="vcfiletype" HeaderText="File">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Filename") %>'
                                    NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "FileLocation") %>'>
                                </asp:HyperLink>
                                <%--<asp:TextBox ID="txtBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numOppBizDocsId") %>'>
                                </asp:TextBox>--%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
																	<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <%-- <asp:GridView ID="gvBizDocs" CssClass="table table-bordered table-striped" runat="server"
                    AutoGenerateColumns="false" Width="1050px" CellPadding="3"
                    CellSpacing="3" BorderColor="#f0f0f0" OnRowDeleting="gvBizDocs_RowDeleting">
                    <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                    <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                    <Columns>
                        <asp:BoundField DataField="vcdocname" HeaderText="File" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderStyle-Width="64" ItemStyle-Width="64" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false"><font color="#730000">*</font></asp:LinkButton>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>No Records Found.</EmptyDataTemplate>
                </asp:GridView>--%>
            </td>
        </tr>
        <%-- <tr id="trOrganization" runat="server" visible="false">
            <td valign="top" style="padding: 10px;" colspan="2">
                <asp:RadioButton ID="radUploadOrganization" AutoPostBack="false" Checked="false" runat="server" Font-Bold="true"
                    CssClass="normal1" GroupName="rad" Text="&lt;img src=&quot;../images/Building-16.gif&quot;/&gt; Organization"></asp:RadioButton>
                &nbsp;&nbsp;<telerik:RadComboBox ID="radOrganizationDocuments" runat="server" CheckBoxes="true">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr id="trBizDocs" runat="server" visible="false">
            <td valign="top" style="padding: 10px;" colspan="2">
                <asp:RadioButton ID="radBizDocs" AutoPostBack="false" Checked="false" runat="server" Font-Bold="true"
                    CssClass="normal1" GroupName="rad" Text="&lt;img src=&quot;../images/icons/cart.png&quot;/&gt; BizDocs"></asp:RadioButton>
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr id="trItem" runat="server">
            <td valign="top" style="padding: 10px;" colspan="2">
                <asp:RadioButton ID="radUploadItem" AutoPostBack="false" Checked="false" runat="server" Font-Bold="true"
                    CssClass="normal1" GroupName="rad" Text="&lt;img src=&quot;../images/tag.png&quot;/&gt; Upload from the Item" Style="float: left;"></asp:RadioButton>
                <div style="float: left;">
                    <asp:UpdatePanel ID="UpdatePanelItem" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            &nbsp;
                            <asp:TextBox ID="txtItem" ClientIDMode="Static" runat="server"></asp:TextBox>
                         </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div style="float: left;">
                    <asp:UpdatePanel ID="UpdatePanelItemDetials" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="lkbItemSelected" />
                            <asp:AsyncPostBackTrigger ControlID="lkbItemRemoved" />
                        </Triggers>
                        <ContentTemplate>
                            &nbsp;<telerik:RadComboBox ID="radItemDocuments" runat="server" CheckBoxes="true">
                            </telerik:RadComboBox>
                            <asp:LinkButton ID="lkbItemSelected" runat="server" Style="display: none"></asp:LinkButton>
                            <asp:LinkButton ID="lkbItemRemoved" runat="server" Style="display: none"></asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:HiddenField ID="hdnTempSelectedItems" runat="server" />
                <asp:HiddenField ID="hdnTempSelectedItemWarehouse" runat="server" />
                <asp:HiddenField ID="hdnCurrentSelectedItem" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding: 10px;" colspan="2">
                <asp:RadioButton ID="radDocumentLib" AutoPostBack="false" Checked="true" runat="server" Font-Bold="true"
                    CssClass="normal1" GroupName="rad" Text="&lt;img src=&quot;../images/tf_note.gif&quot;/&gt; Upload from the Document Library"></asp:RadioButton>

                Select Category
                <asp:DropDownList ID="ddldocumentsCtgr" AutoPostBack="true" CssClass="signup" runat="server"
                    Width="130">
                </asp:DropDownList>
                Select a document from library
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DataGrid ID="dgAttachments" runat="server" CssClass="dg" Width="100%"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn SortExpression="vcfiletype" HeaderText="File">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Filename") %>'
                                    NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "FileLocation") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
																	<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>--%>
    </table>


    <%--<div style="display:none;">
    <asp:RadioButton ID="radFile" GroupName="type" Text="Select a file from Documents Library"
        runat="server" />
    <telerik:RadComboBox ID="radCmbDoc" runat="server" Width="400px" Height="140px" EmptyMessage="Type a Document Name"
        MarkFirstMatch="true" AllowCustomText="false" EnableLoadOnDemand="true" Style="vertical-align: top;">
    </telerik:RadComboBox>
    </div>--%>

    <asp:HiddenField ID="OppType" runat="server" />
    <asp:HiddenField ID="OppStatus" runat="server" />

    <table align="center" style="width: 700px">
        <tr>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtAttachements" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Add Attachments
</asp:Content>
