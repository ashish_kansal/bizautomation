<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmEmailTemplate.aspx.vb"
    Inherits=".frmEmailTemplate" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Prepare Email Template</title>
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <link href="../JavaScript/MultiSelect/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../JavaScript/MultiSelect/bootstrap-multiselect.js"></script>
    <link href="../CSS/GridColorScheme.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.option-droup-multiSelection-Group').multiselect({
                enableClickableOptGroups: true,
                onSelectAll: function () {
                    console.log("select-all-nonreq");
                },
                optionClass: function (element) {
                    var value = $(element).attr("class");
                    return value;
                }
            });
        })
        function OnClientCommandExecuting(editor, args) {
            var name = args.get_name();
            var val = args.get_value();
            if (name == "MergeField") {
                editor.pasteHtml(val);
                //Cancel the further execution of the command as such a command does not exist in the editor command list
                args.set_cancel(true);
            }
        }

        function Save() {
            $("#hdnGroups").val($("#ddlGroups").val().toString());
            if ($('#txtTemplateName').val() == "") {
                alert("Enter Template Name")
                document.getElementById("txtTemplateName").focus();
                return false;
            }
            if ($('#txtSubject').val() == 0) {
                alert("Enter Subject")
                document.getElementById("txtSubject").focus();
                return false;
            }
            if ($('#ddlEmailMergeModule').val() == 0) {
                alert("Select Module")
                document.getElementById("ddlEmailMergeModule").focus();
                return false;
            }
        }


        function OnClientLoad(combobox, args) {
            makeUnselectable(combobox.get_element());
        }

        function makeUnselectable(element) {
            $telerik.$("*", element).attr("unselectable", "on");
        }

        function getSelectedItemValue(combobox, args) {

            var value = combobox.get_value();
            var editor = $find("<%=RadEditor1.ClientID%>");
            editor.pasteHtml(value);
        }

        function getSelectedBizDocItemValue(combobox, args) {

            var value = combobox.get_value();
            var editor = $find("<%=RadEditor1.ClientID%>");
            editor.pasteHtml(value);
            if (value == "##BizDoc Template Merge fields without HTML & CSS##") {

                $find("<%= radcmbCustomField.ClientID %>").set_visible(true);
                editor.set_html("");


            }
            else {
                $find("<%= radcmbCustomField.ClientID %>").set_visible(false);
                return false;
            }
            __doPostBack("<%= radcmbBizDocMergeFields.ClientID %>", '');
        }

        function Confirm(sender) {
            var selectedText = $(sender).find("option:selected").text();
            var confirmMsg = confirm("Selecting this module will remove any merge fields already added to your template (because merge fields from two modules, can't be mixed). Are you sure you want to do this ?");
            if (confirmMsg == true) {
                var editor = $find("<%=RadEditor1.ClientID%>");
                editor.set_html("");
                __doPostBack('ddlEmailMergeModule', '');
                return true;
            }
            else {
                var hv = $('#hfModule').val();
                sender.selectedIndex = hv;
                return false;
            }
        }
    </script>
    <style type="text/css">
        .td1class {
            width: 120px !important;
            padding-left: 15px !important;
            padding-right: 15px !important;
            text-align: right;
        }

        .tdtopbottom {
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .multiselect-group {
            background-color: #3e3e3e !important;
        }

            .multiselect-group a:hover {
                background-color: #3e3e3e !important;
            }

            .multiselect-group a {
                color: #fff !important;
            }

        .multiselect-container > li.multiselect-group label {
            padding: 3px 20px 3px 5px !important;
        }

        .multiselect-container {
            min-width: 250px;
            border: 1px solid #e1e1e1;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveClose" runat="server" Text="Save & Close" CssClass="button"></asp:Button>&nbsp;
                        <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="javascript:self.close()"
                            CssClass="button"></asp:Button>&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Prepare Email Template
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
    <asp:Table runat="server" CssClass="aspTable" BorderColor="black" BorderWidth="1"
        CellPadding="0" CellSpacing="0" Width="100%">
        <asp:TableRow Height="300" VerticalAlign="Top">
            <asp:TableCell>
                <table width="1000px" border="0">
                    <tr>
                        <td class="td1class">Email Template Name
                        </td>
                        <td width="400px">
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtTemplateName" runat="server" CssClass="signup" Width="230"></asp:TextBox>
                                        </td>
                                        <td>Category</td>
                                        <td>&nbsp;&nbsp;
                                            <asp:DropDownList ID="ddlCategory" Width="300px" runat="server"></asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <%--     <asp:CheckBox ID='forMarketingDept' runat="server" Text="Marketing Dept" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1class">
                            <label>Subject</label>
                        </td>
                        <td align="left">
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtSubject" runat="server" CssClass="signup" Width="500px"></asp:TextBox>
                                        </td>
                                        <td>&nbsp;&nbsp;<asp:CheckBox ID="chkLastFollowUpDate" runat="server" Text="When sent, update �Last Follow-up� date" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr id="trEmailMrgeModule" runat="server">
                        <td class="td1class">Module
                        </td>
                        <td class="tdtopbottom" style="width: 800px;">
                            <div>
                                <table>
                                    <tr>
                                        <td style="padding-right: 5px">
                                            <asp:DropDownList ID="ddlEmailMergeModule" Width="180px" AutoPostBack="true" OnSelectedIndexChanged="ddlEmailMergeModule_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                            <asp:HiddenField ID="hfModule" runat="server" />
                                        </td>
                                        <td style="padding-right: 5px" id="tdBizDocOpptype" runat="server" visible="false">
                                            <asp:DropDownList ID="ddlBizDocOppType" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlBizDocOppType_SelectedIndexChanged" AutoPostBack="true" Width="100">
                                                <asp:ListItem Text="-- Select --" Value="0">
                                                </asp:ListItem>
                                                <asp:ListItem Text="Sales" Value="1">
                                                </asp:ListItem>
                                                <asp:ListItem Text="Purchase" Value="2">
                                                </asp:ListItem>
                                            </asp:DropDownList></td>
                                        <td style="padding-right: 5px" id="tdBizDocType" runat="server" visible="false">
                                            <asp:DropDownList ID="ddlBizDocType" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlBizDocType_SelectedIndexChanged" Width="140"></asp:DropDownList>
                                        </td>
                                        <td style="padding-right: 5px" id="tdBizDocTemplate" runat="server" visible="false">
                                            <asp:DropDownList ID="ddlBizDocTemplate" OnSelectedIndexChanged="ddlBizDocTemplate_SelectedIndexChanged" Width="140" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                        </td>
                                        <td style="padding-right: 5px;">
                                            <telerik:RadComboBox ID="radcmbMergerFields" Visible="true" DropDownWidth="250px" ExpandDirection="up" runat="server" OnClientSelectedIndexChanged="getSelectedItemValue"
                                                OnClientLoad="OnClientLoad">
                                            </telerik:RadComboBox>
                                            <telerik:RadComboBox ID="radcmbBizDocMergeFields" AutoPostBack="true" Visible="false" DropDownWidth="300px" Width="100px" ExpandDirection="up" runat="server" OnSelectedIndexChanged="radcmbBizDocMergeFields_SelectedIndexChanged">
                                                <%--OnClientSelectedIndexChanged="getSelectedBizDocItemValue" OnClientLoad="OnClientLoad"--%>
                                                <Items>
                                                    <telerik:RadComboBoxItem Value="0" Text="--Select Option--" />
                                                    <telerik:RadComboBoxItem Value="##BizDocTemplateFromGlobalSettings##" Text="BizDoc HTML & CSS Template" />
                                                    <telerik:RadComboBoxItem Value="##BizDoc Template Merge fields without HTML & CSS##" Text="BizDoc Template Merge fields without HTML & CSS" />
                                                </Items>
                                            </telerik:RadComboBox>
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="radcmbCustomField" DropDownWidth="250px" Visible="true" Width="100px" runat="server" ExpandDirection="up" OnClientSelectedIndexChanged="getSelectedItemValue" OnClientLoad="OnClientLoad"></telerik:RadComboBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 180px;display:none">
                            <label>
                                <asp:Label ID="lblMarketingDeptHelp" Text="[?]" CssClass="tip" runat="server" ToolTip="Selecting this check box will prevent the email template from being listed in the user email template list inside the compose message window (it will still show up within the �Prepare message� window when sending an email broadcast).�" />
                                Marketing Department Only                                        
                            </label>
                        </td>
                        <td class="tdtopbottom" colspan="2" style="padding-left: 118px;">
                            <div>
                                <table>
                                    <tr>
                                        <td style="display:none">
                                            <asp:CheckBox ID='forMarketingDept' CssClass="signup" runat="server" />
                                        </td>
                                        <td>Add Category & Email Templates to users in the following permission groups
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlGroups" CssClass="option-droup-multiSelection-Group" multiple="multiple" runat="server"></asp:DropDownList>
                                            <asp:HiddenField ID="hdnGroups" runat="server" Value="0" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                     <tr> 
                                                    <td colspan="2" style="padding-left:120px;padding-bottom:20px"><label style="float: left;
    padding-right: 20px;">
                                                        <asp:CheckBox ID="chkFollowUpStatus" style="float:left" runat="server" />Change Follow-up Status to:</label>
                                                        <asp:DropDownList ID="ddlFollowupStatus" style="width:350px;float:left" runat="server"></asp:DropDownList>
                                                    </td>
                                                </tr>
                    <tr>
                        <td colspan="2" align="left" valign="top">
                            <telerik:RadEditor ID="RadEditor1" runat="server" Height="350px" OnClientPasteHtml="OnClientPasteHtml"
                                OnClientCommandExecuting="OnClientCommandExecuting" ToolsFile="~/Marketing/EditorTools.xml">
                                <Content>
                                </Content>
                                <Tools>
                                    <telerik:EditorToolGroup>
                                        <%-- <telerik:EditorDropDown Name="MergeField" Text="Merge Field">
                                        </telerik:EditorDropDown>--%>
                                    </telerik:EditorToolGroup>
                                </Tools>
                            </telerik:RadEditor>
                        </td>
                    </tr>
                </table>
                <br>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hdnURLRefferer" runat="server" />
</asp:Content>
