﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAssignDripCampaign.aspx.vb"
    Inherits=".frmAssignDripCampaign" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>    
    <title>Assign Drip Campaign </title>
    <script>
        function Save() {
            if (document.form1.ddlEmailGroup.selectedIndex == 0) {
                alert("Select Email Group ")
                document.form1.ddlEmailGroup.focus()
                return false;
            }
            if (document.form1.ddlDripCampaign.selectedIndex == 0) {
                alert("Select Drip Campaign")
                document.form1.ddlDripCampaign.focus()
                return false;
            }
            if (document.form1.calStartDate_txtDate.value == '') {
                alert("Enter Start Date")
                document.form1.calStartDate_txtDate.focus()
                return false;
            }
            return true;
        }
        </script>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    OBSOLETE FORM -- Bug ID 262
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Assign Drip Campaign &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:Button ID="btnSave" CssClass="button" Text="Save & Close" runat="server" OnClientClick="return Save();"></asp:Button>
                <asp:Button ID="btnClose" CssClass="button" Text="Close" runat="server"></asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" runat="server" GridLines="None" BorderColor="black" Width="100%"
        BorderWidth="1" CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell>
                <br>
                <table width="100%" border="0">
                    <tr>
                        <td class="normal1">
                            <table width="100%">
                                <tr>
                                    <td class="normal1" align="right">
                                        Email Group
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmailGroup" runat="server" CssClass="signup">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right">
                                        &nbsp;&nbsp;&nbsp;Drip Campaign
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlDripCampaign" runat="server" CssClass="signup">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        &nbsp;&nbsp;&nbsp;Starting Date
                                    </td>
                                    <td align="left">
                                        <BizCalendar:Calendar ID="calStartDate" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
