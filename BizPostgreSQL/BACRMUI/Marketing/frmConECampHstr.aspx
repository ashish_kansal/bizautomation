<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmConECampHstr.aspx.vb"
    Inherits="frmConECampHstr" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>E-Campaign History</title>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Email Campaign History
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:DataGrid ID="dgECampHstr" runat="server" AllowSorting="True" CssClass="dg" Width="850" AutoGenerateColumns="False">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="vcECampName" HeaderText="Campaign"></asp:BoundColumn>
            <asp:BoundColumn DataField="Status" HeaderText="Status" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
            <asp:BoundColumn DataField="Template" HeaderText="Event"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcExecutionDate" HeaderText="Scheduled Date" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcSendDate" HeaderText="Message Sent On" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Execution Status" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("SendStatus")%>' ForeColor='<%# IIf(Eval("SendStatus") = "Success", System.Drawing.Color.Green, IIf(Eval("SendStatus") = "Failed", System.Drawing.Color.Red, System.Drawing.Color.SaddleBrown))%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Email Read?" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# If(Eval("bitEmailRead") = "1", "Yes", "")%>' ForeColor="Green" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Link Clicked" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <%# If(Eval("NoOfLinks") = "0", "-", Eval("NoOfLinkClicked") & " Out Of " & Eval("NoOfLinks") & " " & String.Format("{0:(0.00 %)}",(Convert.ToInt32(Eval("NoOfLinkClicked")) / Convert.ToInt32(Eval("NoOfLinks")))))%>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
</asp:Content>
