﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCampaignSurveyWorflowRuleSelectField.aspx.vb"
    Inherits="frmCampaignSurveyWorflowRuleSelectField" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Select Fields</title>
    <script language="javascript" src="../javascript/Surveys.js"></script>
    <script language="javascript" src="../javascript/Validation.js"></script>
    <script language="javascript">
        function CloseMe() {
            window.close();
        }
    </script>
    <style type="text/css">
        .style1
        {
            font-family: Tahoma, Verdana, Arial, Helvetica;
            font-size: 8pt;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            color: #000000;
            width: 199px;
        }
        .style2
        {
            font-family: Tahoma, Verdana, Arial, Helvetica;
            font-size: 8pt;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            color: #000000;
            width: 218px;
        }
        .style3
        {
            width: 77px;
        }
        .style4
        {
            width: 376px;
        }
        .style5
        {
            width: 97px;
        }
        .style6
        {
            width: 228px;
        }
        #frmSelectWorkflowRuleField
        {
            width: 368px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnDone" runat="Server" CssClass="button" Text="Done"></asp:Button>&nbsp<asp:Button
                ID="btnClose" runat="Server" CssClass="button" OnClientClick="javascript:CloseMe();"
                Text="Close"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Select Fields
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <input id="hdSurId" type="hidden" name="hdSurId" runat="server">
    <input type="hidden" runat="server" id="hdXMLString" value="" name="hdXMLString" />
    <input type="hidden" runat="server" name="hdQId" id="hdQId">
    <input id="hdAnsId" type="hidden" name="hdAnsId" runat="server">
    <input type="hidden" runat="server" name="hdXml" id="hdXml">
    <input type="hidden" runat="server" name="hdAction" id="hdAction">
    <input type="hidden" runat="server" name="hdRuleId" id="hdRuleId">
    <input type="hidden" runat="server" name="hdRadioId" id="hdRadioId">
    <input id="hdMatrix" type="hidden" name="hdMatrix" runat="server">
    <asp:Literal ID="litClientMessage" runat="server" EnableViewState="False"></asp:Literal>
    <asp:Table ID="tblSelectField" Width="33%" GridLines="None" BorderColor="black" BorderWidth="1"
        CssClass="aspTable" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <table cellpadding="0" cellspacing="0" style="width: 33%">
                    <tr>
                        <td align="center" class="style2" valign="top">
                            Available Fields<br>
                            <asp:ListBox ID="lstAvailablefld" runat="server" Width="140" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td align="center" valign="middle" class="style3">
                            <input type="button" id="btnAddOne" class="button" value="Add >" style="width: 65;"
                                onclick="javascript:move(document.form1.lstAvailablefld,document.form1.lstSelectedfldOne,1,0)">
                            <br>
                            <br>
                            <br>
                            <input type="button" id="btnRemoveOne" class="button" value="< Remove" style="width: 65;"
                                onclick="javascript:remove1(document.form1.lstSelectedfldOne,document.form1.lstAvailablefld,0)">
                        </td>
                        <td align="center" class="style1">
                            Selected Fields<br>
                            <asp:ListBox ID="lstSelectedfldOne" runat="server" Width="140" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                        </td>
                        <td align="center" valign="middle" class="style4">
                            <br>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
