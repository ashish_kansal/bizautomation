Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Partial Class frmConECampHstr
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindGrid()
        Try
            Dim objCamapign As New Campaign
            Dim dtECampHstr As DataTable

            objCamapign.ConEmailCampID = CCommon.ToLong(GetQueryStringVal("ConECampID"))
            objCamapign.ContactId = CCommon.ToLong(GetQueryStringVal("pqwRT"))
            objCamapign.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

            dtECampHstr = objCamapign.ConECampaignHstr
            dgECampHstr.DataSource = dtECampHstr
            dgECampHstr.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Function ReturnName(ByVal SDate As Date) As String
        Try
            Dim strDate As String = ""
            If Not IsDBNull(SDate) Then strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
            Return strDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
