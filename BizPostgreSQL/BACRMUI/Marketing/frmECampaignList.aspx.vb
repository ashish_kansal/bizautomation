﻿Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Partial Public Class frmECampignList
    Inherits BACRMPage
    Dim strColumn As String
   
    Dim objCommon As CCommon
    Dim objCampaign As Campaign

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If objCommon Is Nothing Then objCommon = New CCommon
            GetUserRightsForPage(6, 6)
            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then hplNewECamp.Visible = False
            If Not IsPostBack Then
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                'txtSortOrder.Text = 1
                'txtCurrrentPageECamapign.Text = 1
                BindDatagrid()

                BindCampaignGrid()
            End If
            'If txtSortCharECamp.Text <> "" Then BindDatagrid()
            ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('3');}else{ parent.SelectTabByValue('3'); } ", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim dtCampaign As DataTable
            If objCampaign Is Nothing Then objCampaign = New Campaign

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) <> 0 Then
                With objCampaign
                    .SortCharacter = txtSortChar.Text
                    .UserCntID = Session("UserContactID")
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    .DomainID = Session("DomainID")

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    If txtCurrrentPage.Text.Trim <> "" Then
                        .CurrentPage = txtCurrrentPage.Text
                    Else : .CurrentPage = 1
                    End If

                    If txtSortColumn.Text <> "" Then
                        .columnName = strColumn
                    Else : .columnName = "vcECampName"
                    End If
                    dtCampaign = objCampaign.ECampaignList
                    dgECamapign.DataSource = dtCampaign
                    dgECamapign.DataBind()
                End With
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindCampaignGrid()
        Dim dtReport As DataTable
        Dim objCampaign As New Campaign
        objCampaign.PageSize = Session("PagingRows")
        objCampaign.TotalRecords = 0

        If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
        objCampaign.CurrentPage = txtCurrrentPage.Text.Trim()

        bizPager.PageSize = Session("PagingRows")
        bizPager.RecordCount = objCampaign.TotalRecords
        bizPager.CurrentPageIndex = txtCurrrentPage.Text

        objCampaign.DomainID = Session("DomainId")
        objCampaign.CampaignID = 0      'CCommon.ToLong(ddlCampaign.SelectedValue)
        dtReport = objCampaign.ECampaignReport()
        dgCampaigns.DataSource = dtReport
        dgCampaigns.DataBind()

    End Sub

    Private Sub dgECamapign_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgECamapign.ItemCommand
        Try
            If e.CommandName = "Campaign" Then Response.Redirect("../Marketing/frmECampaignDtls.aspx?ECampID=" & e.Item.Cells(0).Text)
            If e.CommandName = "Delete" Then
                If objCampaign Is Nothing Then objCampaign = New Campaign
                objCampaign.ECampaignID = e.Item.Cells(0).Text
                objCampaign.byteMode = 1
                objCampaign.ECampaignDtls() ''deleting E campaign details
                BindDatagrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgECamapign_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgECamapign.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDeleteECamapign")
                btnDelete = e.Item.FindControl("btnDeleteECamapign")
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False
                    lnkDelete.Visible = True
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    'Protected Sub chkDisplayCompletedCampaigns_CheckedChanged(sender As Object, e As EventArgs)
    '    If chkDisplayCompletedCampaigns.Checked = True Then

    '        Dim dtCompletedCampaigns As New DataTable()
    '        Dim strStages As String
    '        Dim strStagesCompleted As String
    '        Dim i As Integer

    '        'Add columns to DataTable.

    '        For Each col As DataGridColumn In dgCampaigns.Columns
    '            If (col.HeaderText = "Process Name") Then
    '                dtCompletedCampaigns.Columns.Add("vcECampName")
    '            ElseIf (col.HeaderText = "Next Send Date") Then
    '                dtCompletedCampaigns.Columns.Add("dtNextStage")
    '            ElseIf (col.HeaderText = "From Email Address") Then
    '                dtCompletedCampaigns.Columns.Add("FromEmail")
    '            ElseIf (col.HeaderText = "NoOfStages") Then
    '                dtCompletedCampaigns.Columns.Add("Stage")
    '            ElseIf (col.HeaderText = "Next Temaplate to go out(Stage)") Then
    '                dtCompletedCampaigns.Columns.Add("NextTemplate")
    '            ElseIf (col.HeaderText = "Recipient(Opened)") Then
    '                dtCompletedCampaigns.Columns.Add("Recipient")
    '            Else
    '                dtCompletedCampaigns.Columns.Add(col.HeaderText)
    '            End If


    '        Next
    '        Dim keys(1) As DataColumn
    '        keys(0) = dtCompletedCampaigns.Columns(0)
    '        dtCompletedCampaigns.PrimaryKey = keys

    '        'Loop through the DataGrid and copy rows.
    '        For Each dataGridItem As DataGridItem In dgCampaigns.Items

    '            Dim dRow1 As DataRow = dtCompletedCampaigns.NewRow
    '            For i = 0 To dgCampaigns.Columns.Count - 1

    '                If (dgCampaigns.Columns(i).HeaderText.ToString() = "NoOfStages") Then
    '                    strStages = dataGridItem.Cells(i).Text
    '                End If
    '                If (dgCampaigns.Columns(i).HeaderText.ToString() = "NoOfStagesCompleted") Then
    '                    strStagesCompleted = dataGridItem.Cells(i).Text
    '                End If
    '                dRow1(i) = dataGridItem.Cells(i).Text
    '            Next
    '            If (strStages = strStagesCompleted And ((Convert.ToInt32(strStages) And Convert.ToInt32(strStagesCompleted)) <> 0)) Then
    '                dtCompletedCampaigns.Rows.Add(dRow1)
    '            End If
    '            'If (dataGridItem.Cells(8).Text = "New Fleet Lead 1(0 Of 3)") Then
    '            '    dtCompletedCampaigns.Rows.Add(dRow1)
    '            'End If
    '        Next

    '        Dim dv As DataView = New DataView(dtCompletedCampaigns)
    '        dgCampaigns.DataSource = dv
    '        dgCampaigns.DataBind()

    '    Else
    '        BindCampaignGrid()
    '    End If


    'End Sub


    Protected Sub lnkEndCamapign_Click1(sender As Object, e As EventArgs)
        Try

            For Each dataGridItem As DataGridItem In dgCampaigns.Items
                If CType(dataGridItem.FindControl("chk"), CheckBox).Checked = True Then
                    Dim objCampaign As New Campaign
                    With objCampaign

                        .ContactId = dataGridItem.Cells(1).Text
                        .ECampaignID = dataGridItem.Cells(2).Text
                        .End_Disengage_SelectedCampaign()
                    End With
                End If

            Next
            BindCampaignGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub
End Class