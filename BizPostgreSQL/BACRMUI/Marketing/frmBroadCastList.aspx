<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmBroadCastList.aspx.vb"
    Inherits="frmBroadCastList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>BroadCast List</title>
    <script type="text/javascript" language="javascript">
        function OpenWindow(a) {
            document.location.href = "../Marketing/frmEmailBroadcasting.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ID=" + a
            return false;
        }
        function OpenWindowView(a) {
            window.open("../Marketing/frmBroadcastDTLs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ID=" + a, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="pull-left col-md-6">
            <div class="col-md-4 callout callout callout-success">
                <label>
                    No of Email Sent : 
                </label>
                <asp:Label ID="lblSent" runat="server"></asp:Label>
            </div>
            <div class="col-md-4 callout callout-info" style="margin-left: 10px;">
                <label>
                    No of Email Left : 
                </label>
                <asp:Label ID="lblLeft" runat="server"></asp:Label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="pull-right">
                <asp:HyperLink ID="hlBroadcastEmail" runat="server" CssClass="btn btn-primary" Target="_blank" NavigateUrl="http://help.bizautomation.com/default.aspx?pageurl=EMAILBROADCAST">[ How to Broadcast E-Mail ]&nbsp;&nbsp;<i class="fa fa-question-circle"></i></asp:HyperLink>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Broadcast List&nbsp;<a href="#" onclick="return OpenHelpPopUp('marketing/frmbroadcastlist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgBroadCast" runat="server" Width="100%" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn DataField="numBroadCastID" Visible="False"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcBroadCastName" SortExpression="vcBroadCastName"
                            HeaderText="Broadcast Name"></asp:ButtonColumn>
                        <asp:TemplateColumn HeaderText="BroadCast Date" SortExpression="bintBroadCastDate">
                            <ItemTemplate>
                                <%#ReturnName(DataBinder.Eval(Container.DataItem, "bintBroadCastDate"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="numBroadCastBy" SortExpression="numBroadCastBy" HeaderText="BroadCast By"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numCountRecipients" SortExpression="numCountRecipients"
                            HeaderText="Total Recipients"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Is Broadcasted">
                            <ItemTemplate>
                                <asp:Label ID="lblBroadcasted" Text='<%# iif(Eval("bitBroadcasted")=true,"Yes","No") %>' runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Statistics">
                            <ItemTemplate>
                                Clicked &nbsp;:&nbsp;
                                            <asp:Label ID="lblNoOfClick" Text='<%#Eval("intNoOfClick")%>' runat="server"></asp:Label>
                                ;
                                            Unsubscribed &nbsp;:&nbsp;
                                            <asp:Label ID="lblUnsubscribe" Text='<%#Eval("bitUnsubscribe")%>' runat="server"></asp:Label>
                                ;
                                            <asp:HyperLink ID="hlView" runat="server" Text="Detail">
                                            </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"
                                    CommandArgument='<%# Bind("numBroadCastID") %>' Visible='<%# iif(Eval("bitBroadcasted")=true,false,true) %>'></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
