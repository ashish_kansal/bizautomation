﻿Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common

Public Class frmSurveyDirect
    Inherits BACRMPage
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
            
                DisplaySurveyLists()
            End If
            btnCancel.Attributes.Add("onclick", "return Close()")
            btnSurvey.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            ' btnTime.Attributes.Add("onclick", "openTime('" & GetQueryStringVal( "CaseId") & "','" & GetQueryStringVal( "CntId") & "','" & ddlCompanyName.SelectedValue & "','" & txtCaseTimeID.Text & "')")
            'btnExpense.Attributes.Add("onclick", "openExpense('" & GetQueryStringVal( "CaseId") & "','" & GetQueryStringVal( "CntId") & "','" & ddlCompanyName.SelectedValue & "','" & txtCaseExpenseID.Text & "')")
            If radCmbCompany.SelectedValue <> "" Then
                sb_LoadContacts()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub sb_LoadContacts()
        Try
            ddlTaskContact.Items.Clear()
            Dim objOpportunities As New COpportunities
            objOpportunities.DivisionID = radCmbCompany.SelectedValue
            ddlTaskContact.DataSource = objOpportunities.ListContact().Tables(0).DefaultView()
            ddlTaskContact.DataTextField = "ContactType"
            ddlTaskContact.DataValueField = "numcontactId"
            ddlTaskContact.DataBind()
            ddlTaskContact.Items.Insert(0, New ListItem("--Select One--", "0"))
            If ddlTaskContact.Items.Count = 2 Then
                ddlTaskContact.Items(1).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Function DisplaySurveyLists()
        Try
            Dim objSurvey As New SurveyAdministration                 'Declare and create a object of SurveyAdministration
            objSurvey.DomainId = Session("DomainID")                  'Set the Doamin Id
            Dim dtSurveylist As DataTable                             'Declare a datatable
            dtSurveylist = objSurvey.getSurveyList()                  'call function to get the list of available surveys
            Dim drSurveyList As DataRow                               'Declare a DataRow object

            ddlSurvey.Items.Clear()
            For Each drSurveyList In dtSurveylist.Rows                'Loop through the rows on the table containing the survey list
                ddlSurvey.Items.Add(New ListItem(ReSetFromXML(drSurveyList.Item("vcSurName")), drSurveyList.Item("numSurID")))
            Next

            ddlSurvey.Items.Insert(0, "--Select One--")
            ddlSurvey.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReSetFromXML(ByVal sValue As String) As String
        Try
            Return Replace(Replace(Replace(Replace(sValue, "&amp;", "&"), "&#39;", "'"), "&lt;", "<"), "&gt;", ">")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSurvey_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSurvey.Click
        Try
            Dim numSurveyRespondentsID As Integer                                   'Declare a variable for the Survey Respondents Id
            Dim objSurvey As New SurveyAdministration                               'Create an object of Survey Administration Class
            objSurvey.SurveyId = ddlSurvey.SelectedValue                                      'Set the Survey ID
            objSurvey.DomainId = Session("DomainID")                                        'Set the Domain ID
            objSurvey.ContactId = ddlTaskContact.SelectedValue


            numSurveyRespondentsID = CInt(objSurvey.CheckRegisteredUserAndSaveInformationInDatabase_Direct()) 'Call to Save the Survey Respondents Information in teh database and retrieves the ID of the Respondent
            If numSurveyRespondentsID <> 0 Then
                litClientScript.Text = "<script language='javascript' type='text/javascript'>window.open('" & ConfigurationManager.AppSettings("SurveyScreen").Replace("frmGenericFormSurvey.aspx", "frmSurveyExecutionSurvey.aspx") & "?numSurId=" & ddlSurvey.SelectedValue & "&numRespondantID=" & numSurveyRespondentsID & "&D=" & Session("DomainID") & "');</script>"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class