<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmECampaignDtls.aspx.vb"
    Inherits="frmECampaignDtls" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Follow-up Campaign Details</title>
    <script language="javascript" type="text/javascript">
        function Save() {
            if (document.form1.txtCampaignName.value == '') {
                alert("Enter Campaign Name")
                document.form1.txtCampaignName.focus()
                return false;
            }
            if (document.form1.ddlEmailTemplate1.value == 0 && document.form1.listActionItemTemplate1.value == 0) {
                alert("Select First ")
                document.form1.ddlEmailTemplate1.focus()
                return false;
            }
        }
        function ddlValidation(a) {
            var str1 = 'ddlEmailTemplate';
            var str2 = 'listActionItemTemplate';
            var ddl = '';
            if (a.toString().indexOf("ddlEmailTemplate") >= 0) {
                ddl = str2 + a.toString().substr(16, 2);
            }
            else if (a.toString().indexOf("listActionItemTemplate") >= 0) {
                ddl = str1 + a.toString().substr(22, 2);
            }
            document.getElementById(ddl).selectedIndex = 0;
        }

    </script>
    <style>
        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            #dllCampaignStatus {
                width: 100%;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
        <tr>
            <td align="right">
                 <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
   Automated Follow-up
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="tbl" runat="server" GridLines="None" BorderColor="black" Width="100%"  CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell>
                <div class="col-md-10 col-md-offset-1">
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Follow-up Name<font color="red">*</font></label>
                        <asp:TextBox ID="txtCampaignName" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Start Time</label>
                        
                        <img src="../images/Clock-16.gif" />
                        <div>
                            <asp:DropDownList ID="ddltime" runat="server" CssClass="form-control" style="width:70%;display:initial">
                                <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
                                <asp:ListItem Selected="true" Value="15">8:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;AM<input id="chkAM" type="radio" checked value="0" name="AM" runat="server">PM<input
                                id="chkPM" type="radio" value="1" name="AM" runat="server">
                            </div>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Description</label>
                         <asp:TextBox ID="txtDescription" CssClass="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label>Who's email address should populate the "From" field?</label><br />
                     <asp:RadioButton ID="rbRecOwner" GroupName="From" runat="server" CssClass="signup"
                                Text="'Record Owner' of the company that contact belongs to" Style="line-height: 25px;" /><br />
                            <asp:RadioButton ID="rbAssignee" GroupName="From" runat="server" CssClass="signup"
                                Text="'Assignee' of the company that contact belongs to" Style="line-height: 25px;" /><br />
                            <asp:RadioButton ID="rbBizUser" GroupName="From" runat="server" CssClass="signup"
                                Text="From" />&nbsp;<asp:DropDownList ID="ddlEmployee" runat="server" CssClass="signup">
                                </asp:DropDownList>
                        </div>
                </div>
                </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Time Zone</label>
                              <asp:DropDownList ID="ddlTimeZone" runat="server" CssClass="form-control">
                                <asp:ListItem Value="-12~1">(GMT - 12) International Date Line West</asp:ListItem>
                                <asp:ListItem Value="-11~2">(GMT - 11) Midway Island, Samoa</asp:ListItem>
                                <asp:ListItem Value="-10~3">(GMT - 10) Hawaii</asp:ListItem>
                                <asp:ListItem Value="-9~4">(GMT - 09) Alaska</asp:ListItem>
                                <asp:ListItem Value="-8~6">(GMT - 08) Pacific Time (US &amp; Canada); Tijuana</asp:ListItem>
                                <asp:ListItem Value="-8~7">(GMT - 08) Baja California</asp:ListItem>
                                <asp:ListItem Value="-7~9">(GMT - 07) Arizona</asp:ListItem>
                                <asp:ListItem Value="-7~10">(GMT - 07) Chihuahua, La Paz, Mazatlan</asp:ListItem>
                                <asp:ListItem Value="-7~11">(GMT - 07) Mountain Time (US &amp; Canada)</asp:ListItem>
                                <asp:ListItem Value="-6~12">(GMT - 06) Central America</asp:ListItem>
                                <asp:ListItem Value="-6~13">(GMT - 06) Central Time(US &amp; Canada)</asp:ListItem>
                                <asp:ListItem Value="-6~14">(GMT - 06) Guadalajara, Mexico City, Monterrey</asp:ListItem>
                                <asp:ListItem Value="-6~15">(GMT - 06) Saskatchewan</asp:ListItem>
                                <asp:ListItem Value="-5~16">(GMT - 05) Eastern Time (US & Canada)</asp:ListItem>
                                <asp:ListItem Value="-5~17">(GMT - 05) Bogota, Lima, Quito</asp:ListItem>
                                <asp:ListItem Value="-5~19">(GMT - 05) Indiana (East) </asp:ListItem>
                                <asp:ListItem Value="-4~21">(GMT - 04) Atlantic Time(Canada)</asp:ListItem>
                                <asp:ListItem Value="-4.5~22">(GMT - 04:30) Caracas, La Paz, Santiago</asp:ListItem>
                                <asp:ListItem Value="-3.5~23">(GMT - 3.5) Newfoundland</asp:ListItem>
                                <asp:ListItem Value="-3~24">(GMT - 03) Brasilia</asp:ListItem>
                                <asp:ListItem Value="-3~25">(GMT - 03) Buenos Aires</asp:ListItem>
                                <asp:ListItem Value="-3~26">(GMT - 03)  Cayenne, Fortaleza</asp:ListItem>
                                <asp:ListItem Value="-3~27">(GMT - 03)  Greenland</asp:ListItem>
                                <asp:ListItem Value="-2~28">(GMT - 02) Mid-Atlantic</asp:ListItem>
                                <asp:ListItem Value="-1~29">(GMT - 01) Azores</asp:ListItem>
                                <asp:ListItem Value="-1~30">(GMT - 01) Cape Verde Is.</asp:ListItem>
                                <asp:ListItem Value="0~31">(GMT) Casablanca, Monrovia</asp:ListItem>
                                <asp:ListItem Value="0~32">(GMT) Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London</asp:ListItem>
                                <asp:ListItem Value="1~33">(GMT + 01) Belgrade, Bratislava, Budapest, Ljubljana, Prague</asp:ListItem>
                                <asp:ListItem Value="1~34">(GMT + 01) Brussels, Copenhagen, Madrid, Paris</asp:ListItem>
                                <asp:ListItem Value="1~35">(GMT + 01) Sarajevo, Skopje, Warsaw, Zagreb</asp:ListItem>
                                <asp:ListItem Value="1~36">(GMT + 01) West Central Africa</asp:ListItem>
                                <asp:ListItem Value="2~37">(GMT + 02) Athens, Beirut, Istanbul, Minsk</asp:ListItem>
                                <asp:ListItem Value="2~38">(GMT + 02) Beirut</asp:ListItem>
                                <asp:ListItem Value="2~39">(GMT + 02) Cairo</asp:ListItem>
                                <asp:ListItem Value="2~40">(GMT + 02) Harare, Pretoria</asp:ListItem>
                                <asp:ListItem Value="2~41">(GMT + 02) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</asp:ListItem>
                                <asp:ListItem Value="2~42">(GMT + 02) Jerusalem</asp:ListItem>
                                <asp:ListItem Value="3~43">(GMT + 03) Baghdad</asp:ListItem>
                                <asp:ListItem Value="3~44">(GMT + 03) Kuwait, Riyadh</asp:ListItem>
                                <asp:ListItem Value="3~45">(GMT + 03) Moscow, St. Petersburg, Volgograd</asp:ListItem>
                                <asp:ListItem Value="3~46">(GMT + 03) Nairobi</asp:ListItem>
                                <asp:ListItem Value="3.5~47">(GMT + 3:30) Tehran</asp:ListItem>
                                <asp:ListItem Value="4~48">(GMT + 04) Abu Dhabi, Muscat</asp:ListItem>
                                <asp:ListItem Value="4~49">(GMT + 04) Baku, Tbilisi, Yerevan</asp:ListItem>
                                <asp:ListItem Value="4.5~50">(GMT + 4:30) Kabul</asp:ListItem>
                                <asp:ListItem Value="5~51">(GMT + 05) Ekaterinburg</asp:ListItem>
                                <asp:ListItem Value="5~52">(GMT + 05) Islamabad, Karachi, Tashkent</asp:ListItem>
                                <asp:ListItem Value="5.5~53">(GMT + 5:30) Chennai, Kolkata, Mumbai, New Delhi</asp:ListItem>
                                <asp:ListItem Value="5.5~57">(GMT + 5:30) Sri Jayawardenepura</asp:ListItem>
                                <asp:ListItem Value="5.75~54">(GMT + 5:45) Kathmandu</asp:ListItem>
                                <asp:ListItem Value="6~55">(GMT + 06) Novosibirsk</asp:ListItem>
                                <asp:ListItem Value="6~56">(GMT + 06) Dhaka</asp:ListItem>
                                <asp:ListItem Value="6.5~58">(GMT + 6:30) Rangoon</asp:ListItem>
                                <asp:ListItem Value="7~59">(GMT + 07) Bangkok, Hanoi, Jakarta</asp:ListItem>
                                <asp:ListItem Value="7~60">(GMT + 07) Krasnoyarsk</asp:ListItem>
                                <asp:ListItem Value="8~61">(GMT + 08) Beijing, Chongqing, Hong Kong, Urumqi</asp:ListItem>
                                <asp:ListItem Value="8~62">(GMT + 08) Irkutsk, Ulaan Bataar</asp:ListItem>
                                <asp:ListItem Value="8~63">(GMT + 08)Kuala Lumpur, Singapore</asp:ListItem>
                                <asp:ListItem Value="8~64">(GMT + 08) Perth</asp:ListItem>
                                <asp:ListItem Value="8~65">(GMT + 08) Taipei</asp:ListItem>
                                <asp:ListItem Value="9~66">(GMT + 9) Osaka, Sapporo, Tokyo</asp:ListItem>
                                <asp:ListItem Value="9~67">(GMT + 09) Seoul</asp:ListItem>
                                <asp:ListItem Value="9~68">(GMT + 09) Yakutsk</asp:ListItem>
                                <asp:ListItem Value="9.5~59">(GMT + 9:30) Adelaide</asp:ListItem>
                                <asp:ListItem Value="9.5~70">(GMT + 9.5) Darwin</asp:ListItem>
                                <asp:ListItem Value="10~71">(GMT + 10) Brisbane</asp:ListItem>
                                <asp:ListItem Value="10~72">(GMT + 10) Canberra, Melbourne, Sydney</asp:ListItem>
                                <asp:ListItem Value="10~73">(GMT + 10) Guam, Port Moresby</asp:ListItem>
                                <asp:ListItem Value="10~74">(GMT + 10) Hobart</asp:ListItem>
                                <asp:ListItem Value="10~75">(GMT + 10) Vladivostok</asp:ListItem>
                                <asp:ListItem Value="11~76">(GMT + 11) Magadan, Solomon Is., New Caledonia</asp:ListItem>
                                <asp:ListItem Value="12~77">(GMT + 12) Auckland, Wellington</asp:ListItem>
                                <asp:ListItem Value="12~78">(GMT + 12) Fiji, Kamchatka, Marshall Is. </asp:ListItem>
                                <asp:ListItem Value="13~79">(GMT + 13) Nuku'alofa</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div style="clear:both;margin-bottom:30px"></div>
                <table class="tblNoBorder table table-responsive table-bordered">
                   
                    <tr>
                        <th style="font-weight: bold" align="right">Stage
                        </th>
                        <th style="font-weight: bold" align="center">Select Email Template
                        </th>
                        <th style="font-weight: bold" align="center">Or
                        </th>
                        <th style="font-weight: bold" align="center">Select Action Item Template
                        </th>
                        <th style="font-weight: bold" align="center">Then wait
                        </th>
                        <th style="font-weight: bold" align="center">Then set follow-up status to
                        </th>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <font color="red">*</font>&nbsp;1.&nbsp;
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlEmailTemplate1" CssClass="form-control" runat="server"
                                onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td align="center"></td>
                        <td align="center">
                            <asp:DropDownList ID="listActionItemTemplate1" CssClass="form-control" runat="server"
                                onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlDays1" CssClass="form-control" style="width:40%;display:initial" runat="server" >
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="32">32</asp:ListItem>
                                <asp:ListItem Value="36">36</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="44">44</asp:ListItem>
                                <asp:ListItem Value="48">48</asp:ListItem>
                                <asp:ListItem Value="52">52</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddlDayWeek1" CssClass="form-control" style="width:40%;display:initial" runat="server">
                                <asp:ListItem Text="Day(s)" Value="1" />
                                <asp:ListItem Text="Week(s)" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlFollowUp1" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">2.&nbsp;
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlEmailTemplate2" CssClass="form-control" runat="server" 
                                onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td align="center"></td>
                        <td align="center">
                            <asp:DropDownList ID="listActionItemTemplate2" CssClass="form-control" runat="server"
                                 onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlDays2" CssClass="form-control" runat="server"  style="width:40%;display:initial">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="32">32</asp:ListItem>
                                <asp:ListItem Value="36">36</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="44">44</asp:ListItem>
                                <asp:ListItem Value="48">48</asp:ListItem>
                                <asp:ListItem Value="52">52</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddlDayWeek2" runat="server" CssClass="form-control"  style="width:40%;display:initial">
                                <asp:ListItem Text="Day(s)" Value="1" />
                                <asp:ListItem Text="Week(s)" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlFollowUp2" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">3.&nbsp;
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlEmailTemplate3" CssClass="form-control" runat="server" 
                                onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td align="center"></td>
                        <td align="center">
                            <asp:DropDownList ID="listActionItemTemplate3" CssClass="form-control" runat="server"
                                onchange="ddlValidation(this.id)" >
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlDays3" CssClass="form-control" runat="server"  style="width:40%;display:initial">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="32">32</asp:ListItem>
                                <asp:ListItem Value="36">36</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="44">44</asp:ListItem>
                                <asp:ListItem Value="48">48</asp:ListItem>
                                <asp:ListItem Value="52">52</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddlDayWeek3" runat="server" CssClass="form-control"  style="width:40%;display:initial">
                                <asp:ListItem Text="Day(s)" Value="1" />
                                <asp:ListItem Text="Week(s)" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlFollowUp3" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">4.&nbsp;
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlEmailTemplate4" CssClass="form-control" runat="server" 
                                onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td align="center"></td>
                        <td align="center">
                            <asp:DropDownList ID="listActionItemTemplate4" CssClass="form-control" runat="server"
                                 onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlDays4" CssClass="form-control" runat="server"  style="width:40%;display:initial">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="32">32</asp:ListItem>
                                <asp:ListItem Value="36">36</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="44">44</asp:ListItem>
                                <asp:ListItem Value="48">48</asp:ListItem>
                                <asp:ListItem Value="52">52</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddlDayWeek4" runat="server" CssClass="form-control"  style="width:40%;display:initial">
                                <asp:ListItem Text="Day(s)" Value="1" />
                                <asp:ListItem Text="Week(s)" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlFollowUp4" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">5.&nbsp;
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlEmailTemplate5" CssClass="form-control" runat="server" 
                                onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td align="center"></td>
                        <td align="center">
                            <asp:DropDownList ID="listActionItemTemplate5" CssClass="form-control" runat="server"
                                 onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlDays5" CssClass="form-control" runat="server"  style="width:40%;display:initial">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="32">32</asp:ListItem>
                                <asp:ListItem Value="36">36</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="44">44</asp:ListItem>
                                <asp:ListItem Value="48">48</asp:ListItem>
                                <asp:ListItem Value="52">52</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddlDayWeek5" runat="server" CssClass="form-control"  style="width:40%;display:initial">
                                <asp:ListItem Text="Day(s)" Value="1" />
                                <asp:ListItem Text="Week(s)" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlFollowUp5" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">6.&nbsp;
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlEmailTemplate6" CssClass="form-control" runat="server" 
                                onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td align="center"></td>
                        <td align="center">
                            <asp:DropDownList ID="listActionItemTemplate6" CssClass="form-control" runat="server"
                                 onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlDays6" CssClass="form-control" runat="server"  style="width:40%;display:initial">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="32">32</asp:ListItem>
                                <asp:ListItem Value="36">36</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="44">44</asp:ListItem>
                                <asp:ListItem Value="48">48</asp:ListItem>
                                <asp:ListItem Value="52">52</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddlDayWeek6" runat="server" CssClass="form-control"  style="width:40%;display:initial">
                                <asp:ListItem Text="Day(s)" Value="1" />
                                <asp:ListItem Text="Week(s)" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlFollowUp6" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">7.&nbsp;
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlEmailTemplate7" CssClass="form-control" runat="server" 
                                onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td align="center"></td>
                        <td align="center">
                            <asp:DropDownList ID="listActionItemTemplate7" CssClass="form-control" runat="server"
                                 onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlDays7" CssClass="form-control" runat="server"  style="width:40%;display:initial">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="32">32</asp:ListItem>
                                <asp:ListItem Value="36">36</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="44">44</asp:ListItem>
                                <asp:ListItem Value="48">48</asp:ListItem>
                                <asp:ListItem Value="52">52</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddlDayWeek7" runat="server" CssClass="form-control"  style="width:40%;display:initial">
                                <asp:ListItem Text="Day(s)" Value="1" />
                                <asp:ListItem Text="Week(s)" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlFollowUp7" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">8.&nbsp;
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlEmailTemplate8" CssClass="form-control" runat="server" 
                                onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td align="center"></td>
                        <td align="center">
                            <asp:DropDownList ID="listActionItemTemplate8" CssClass="form-control" runat="server"
                                onchange="ddlValidation(this.id)" >
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlDays8" CssClass="form-control" runat="server" style="width:40%;display:initial">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="32">32</asp:ListItem>
                                <asp:ListItem Value="36">36</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="44">44</asp:ListItem>
                                <asp:ListItem Value="48">48</asp:ListItem>
                                <asp:ListItem Value="52">52</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddlDayWeek8" runat="server" CssClass="form-control"  style="width:40%;display:initial">
                                <asp:ListItem Text="Day(s)" Value="1" />
                                <asp:ListItem Text="Week(s)" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlFollowUp8" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">9.&nbsp;
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlEmailTemplate9" CssClass="form-control" runat="server" 
                                onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td align="center"></td>
                        <td align="center">
                            <asp:DropDownList ID="listActionItemTemplate9" CssClass="form-control" runat="server"
                                onchange="ddlValidation(this.id)" >
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlDays9" CssClass="form-control" runat="server"  style="width:40%;display:initial">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="32">32</asp:ListItem>
                                <asp:ListItem Value="36">36</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="44">44</asp:ListItem>
                                <asp:ListItem Value="48">48</asp:ListItem>
                                <asp:ListItem Value="52">52</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddlDayWeek9" runat="server" CssClass="form-control"  style="width:40%;display:initial">
                                <asp:ListItem Text="Day(s)" Value="1" />
                                <asp:ListItem Text="Week(s)" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlFollowUp9" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">10.&nbsp;
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlEmailTemplate10" CssClass="form-control" runat="server"
                                onchange="ddlValidation(this.id)">
                            </asp:DropDownList>
                        </td>
                        <td align="center"></td>
                        <td align="center">
                            <asp:DropDownList ID="listActionItemTemplate10" CssClass="form-control" runat="server"
                                onchange="ddlValidation(this.id)" >
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlDays10" CssClass="form-control" style="width:40%;display:initial" runat="server">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="32">32</asp:ListItem>
                                <asp:ListItem Value="36">36</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="44">44</asp:ListItem>
                                <asp:ListItem Value="48">48</asp:ListItem>
                                <asp:ListItem Value="52">52</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddlDayWeek10" CssClass="form-control"  style="width:40%;display:initial" runat="server">
                                <asp:ListItem Text="Day(s)" Value="1" />
                                <asp:ListItem Text="Week(s)" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="center">
                            <asp:DropDownList ID="ddlFollowUp10" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hdnECampDtlID1" runat="server" />
    <asp:HiddenField ID="hdnECampDtlID2" runat="server" />
    <asp:HiddenField ID="hdnECampDtlID3" runat="server" />
    <asp:HiddenField ID="hdnECampDtlID4" runat="server" />
    <asp:HiddenField ID="hdnECampDtlID5" runat="server" />
    <asp:HiddenField ID="hdnECampDtlID6" runat="server" />
    <asp:HiddenField ID="hdnECampDtlID7" runat="server" />
    <asp:HiddenField ID="hdnECampDtlID8" runat="server" />
    <asp:HiddenField ID="hdnECampDtlID9" runat="server" />
    <asp:HiddenField ID="hdnECampDtlID10" runat="server" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
