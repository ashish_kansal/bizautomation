''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Survey
''' Class	 : frmCampaignSurveyList
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a interface for listing the Surveys in Marketing
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	09/13/2005	Created
''' </history>
''' ''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Survey
    Public Class frmCampaignSurveyList
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents tblSurveyList As System.Web.UI.WebControls.Table
        Protected WithEvents dgSurveyList As System.Web.UI.WebControls.DataGrid
        Protected WithEvents txtColumnSorted As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtSortOrder As System.Web.UI.WebControls.TextBox
        Dim sColumnSorted As String
        Dim sSortOrder As String
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime the page is called. In this event we will 
        '''     get the data from the DB and populate the Tables and the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
       
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'Put user code to initialize the page here
                objCommon = New CCommon
                GetUserRightsForPage(6, 7)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnAddNewSurvey.Visible = False
                End If
                If Not IsPostBack Then
                    ' = "MarSurvey"
                    DisplaySurveyLists()                                                    'Call to display the list of surveys
                End If
                'ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('3');}else{ parent.SelectTabByValue('3'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the surveys from the database
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function DisplaySurveyLists()
            Try
                Dim objSurvey As New SurveyAdministration                 'Declare and create a object of SurveyAdministration
                objSurvey.DomainId = Session("DomainID")                  'Set the Doamin Id
                Dim dtSurveylist As DataTable                             'Declare a datatable
                dtSurveylist = objSurvey.getSurveyList()                  'call function to get the list of available surveys
                Dim drSurveyList As DataRow                               'Declare a DataRow object
                For Each drSurveyList In dtSurveylist.Rows                'Loop through the rows on the table containing the survey list
                    drSurveyList.Item("vcSurName") = Server.HtmlDecode(CStr(drSurveyList.Item("vcSurName")))
                Next
                Dim dvSurveylist As DataView = dtSurveylist.DefaultView   'Get the DataView
                dvSurveylist.Sort = txtColumnSorted.Text & "  " & txtSortOrder.Text 'Sort the column    

                dgSurveyList.DataSource = dvSurveylist                    'set the datasource
                dgSurveyList.DataBind()                                   'databind the datgrid
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnDateTime(ByVal CloseDate As Date) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), CloseDate), Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete the Survey
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Protected Overridable Sub btnDeleteAction_Command(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Dim btnUpdt As LinkButton = CType(sender, LinkButton)                                               'typecast to button
                Dim dgContainer As DataGridItem = CType(btnUpdt.NamingContainer, DataGridItem)              'get the containeer object
                Dim gridIndex As Integer = dgContainer.ItemIndex                                            'Get the row idnex of datagrid
                Dim numSurId As Integer = CInt(dgSurveyList.Items(gridIndex).Cells(0).Text)                 'Get the Survey Id from the column

                Dim objSurvey As New SurveyAdministration                                                   'Declare and create a object of SurveyAdministration
                objSurvey.UserId = Session("UserID")                                                        'Set the User Id
                objSurvey.DomainId = Session("DomainID")                                                    'Set the Domain Id
                objSurvey.SurveyId = numSurId                                                               'Get the Survey Id
                objSurvey.XMLFilePath = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" 'Set the path to the xml file where the survey info is temp stored
                objSurvey.DeleteTempSurveyInformation()                                                     'Requests deletion of the temporary file
                objSurvey.DeleteSurvey()                                                                    'Call to delete the Survey
                DisplaySurveyLists()                                                                        'Call to display the list of surveys
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgSurveyList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSurveyList.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As LinkButton
                    Dim lnkDelete As LinkButton
                    lnkDelete = e.Item.FindControl("lnkdelete")
                    btnDelete = e.Item.FindControl("btnDeleteAction")
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired each time the DataGrid is sorted.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub dgSurveyList_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgSurveyList.SortCommand
            Try
                sColumnSorted = e.SortExpression.ToString()                 'Retrieve the column sorting
                If txtColumnSorted.Text <> sColumnSorted Then
                    txtColumnSorted.Text = sColumnSorted
                    txtSortOrder.Text = "Asc"                               'Default sort is Ascending
                Else
                    If txtSortOrder.Text = "Asc" Then                       'Toggle sort order
                        txtSortOrder.Text = "Desc"
                    Else : txtSortOrder.Text = "Asc"
                    End If
                End If
                DisplaySurveyLists()                                        'Call to display the survey list
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub


        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        End Sub
    End Class
End Namespace