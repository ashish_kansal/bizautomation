﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing

Imports BACRM.BusinessLogic.Admin


Public Class frmLeadsFromWebSource
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            Dim dtTable As DataTable
            Dim LeadOrAccounts As Integer = 0
            Dim FromDate As Integer = 0
            Dim SourceDomain As String = ""


            If Not IsPostBack Then
                If GetQueryStringVal("LeadOrAccounts") <> "" Then
                    LeadOrAccounts = CCommon.ToInteger(GetQueryStringVal("LeadOrAccounts"))
                End If

                If GetQueryStringVal("FromDate") <> "" Then
                    FromDate = CCommon.ToInteger(GetQueryStringVal("FromDate"))
                End If

                If GetQueryStringVal("SourceDomain") <> "" Then
                    SourceDomain = CCommon.ToString(GetQueryStringVal("SourceDomain"))
                End If

                GetWebSourceList(LeadOrAccounts, FromDate, SourceDomain)

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub GetWebSourceList(ByVal LeadOrAccounts As Integer, ByVal FromDate As Integer, ByVal SourceDomain As String)
        Try
            Dim LeadSourcesFromDate As New DateTime
            If FromDate > 0 Then
                If FromDate = 6 Then
                    LeadSourcesFromDate = DateTime.Now.AddMonths(-6)

                ElseIf FromDate = 12 Then
                    LeadSourcesFromDate = DateTime.Now.AddMonths(-12)

                ElseIf FromDate = 24 Then
                    LeadSourcesFromDate = DateTime.Now.AddMonths(-24)

                End If
            Else
                LeadSourcesFromDate = DateTime.Now.AddMonths(-6)

            End If

            Dim objCampaign As New Campaign
            objCampaign.DomainID = Session("DomainID")
            objCampaign.LeadSourcesFromDate = LeadSourcesFromDate
            objCampaign.LeadsOrAccounts = LeadOrAccounts
            objCampaign.SourceDomain = SourceDomain
            Dim dt As DataTable = objCampaign.GetListWebSourceLeads()
            gvWebSourceLeads.DataSource = dt
            gvWebSourceLeads.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SlectTab", "selectTab('');", True)
        Catch ex As Exception

        End Try
    End Sub
End Class