' Created  BY Anoop jayaraj
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common

Partial Class frmImageList
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim strColumn As String
   
    Dim objCommon As CCommon
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objCommon = New CCommon
            GetUserRightsForPage(14, 2)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
            If Not IsPostBack Then
                Session("Asc") = 1
                txtCurrrentPage.Text = 1
                BindDatagrid()
                
            End If
            If txtSortChar.Text <> "" Then
                ViewState("SortChar") = txtSortChar.Text
                ViewState("Column") = "vcDocName"
                Session("Asc") = 0
                BindDatagrid()
                txtSortChar.Text = ""
            End If
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim dtDocuments As DataTable
            Dim objDocuments As New DocumentList
            Dim SortChar As Char
            If txtSortChar.Text.Trim <> "" Then
                SortChar = txtSortChar.Text
            Else : SortChar = "0"
            End If
            With objDocuments
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                .SortOrder = 3 ' All Records

                .SortCharacter = SortChar
                .KeyWord = txtKeyWord.Text
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else : .CurrentPage = 1
                End If
                .DocCategory = 370
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                If ViewState("Column") <> "" Then
                    .columnName = ViewState("Column")
                Else : .columnName = "bintcreateddate"
                End If
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If
            End With
            dtDocuments = objDocuments.GetDocuments
            If objDocuments.TotalRecords = 0 Then
                hidenav.Visible = False
                lblRecordCount.Text = 0
            Else
                hidenav.Visible = True
                lblRecordCount.Text = String.Format("{0:#,###}", objDocuments.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordCount.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                    lblTotal.Text = strTotalPage(0)
                    txtTotalPage.Text = strTotalPage(0)
                Else
                    lblTotal.Text = strTotalPage(0) + 1
                    txtTotalPage.Text = strTotalPage(0) + 1
                End If
                txtTotalRecords.Text = lblRecordCount.Text
            End If
            dgDocs.DataSource = dtDocuments
            dgDocs.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        Try
            txtCurrrentPage.Text = txtTotalPage.Text
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        Try
            If txtCurrrentPage.Text = 1 Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text - 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        Try
            If txtCurrrentPage.Text = txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        Try
            If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 2
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        Try
            If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 3
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        Try
            If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 4
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        Try
            If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 5
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDocs.ItemCommand
        Try
            If e.CommandName = "Name" Then
                Session("DocID") = e.Item.Cells(0).Text()
                Response.Redirect("../Marketing/frmImageDetails.aspx")

            End If
            If e.CommandName = "Delete" Then
                Dim objDocuments As New DocumentList()
                With objDocuments
                    .GenDocID = e.Item.Cells(0).Text()
                    .DomainID = Session("DomainId")
                    .UserCntID = Session("UserContactId")
                    .DelDocumentsByGenDocID()
                End With
                BindDatagrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgDocs_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgDocs.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDocs.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False
                    lnkDelete.Visible = True
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class

