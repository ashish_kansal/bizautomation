﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master"
    CodeBehind="frmEmailBroadcastConfig.aspx.vb" Inherits=".frmEmailBroadcastConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        function Save() {
            if (document.getElementById('txtFrom').value == "") {
                alert("Enter From Name.")
                document.getElementById('txtFrom').focus()
                return false;
            }
            if (document.getElementById('txtDomain').value == "") {
                alert("Enter Domain Name.")
                document.getElementById('txtDomain').focus()
                return false;
            }
            return true;
        }
        function getDomain() {
            if ($('#txtDomain').val() != "") {
                $('#lblSelector').text('bizautomationmail._domainkey.' + $('#txtDomain').val());
                $('#lblDomainAdd').text($('#txtDomain').val());
                $('#lblSelectorAdd').text('bizautomationmail');
                $('#hdnSelector').val('bizautomationmail._domainkey.' + $('#txtDomain').val());
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
  <div class="right-input">
        <div class="input-part">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        
                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="50px" CssClass="button" />
                        &nbsp;
                    </td>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="normal4" align="center" colspan="2" valign="top">
                  <div style="padding: 0px 0px 0px 16px">
                                <asp:Panel runat="server" ID="pnlMessage" >
                                  <asp:Label ID="lblMessage" Text="" runat="server"></asp:Label>
                                  <asp:HyperLink ID="hlEmailBroadcast"  NavigateUrl="../Admin/frmAdvancedSearch.aspx"
                                        Text="Start Broadcasting ." Visible ="false" runat="server"></asp:HyperLink>
                                </asp:Panel>
                            </div>
                
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Email Broadcasting Configuration
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:Table ID="tbl" BorderWidth="0" CellPadding="0" CellSpacing="0" runat="server"
        Width="97%" CssClass="aspTable" BorderColor="Black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <br />
                <table cellpadding="" cellspacing="" width="100%">
                    <tr>
                        <td align="right" width="15%">
                            Step 1 :
                        </td>
                        <td align="left" width="85%">
                            &nbsp;&nbsp; Enter From Address <font color="#ff0000">*</font> &nbsp; &nbsp;
                            <asp:TextBox ID="txtFrom" runat="server">
                            </asp:TextBox>
                            @
                            <asp:TextBox ID="txtDomain" AutoPostBack="false" onKeyUp="javascript:getDomain()"
                                AutoComplete="off" runat="server">
                            </asp:TextBox>
                            &nbsp;
                            <asp:Label ID="lblExFromAddress" Text="(i.e yourname@yourdomainname.com)" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left">
                            &nbsp;&nbsp; Domain Key Selector &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblSelector" runat="server" EnableViewState="true">
                            </asp:Label>
                            <asp:HiddenField ID="hdnSelector" runat="server" />
                        </td>
                    </tr>
                    <tr height="10px">
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Step 2 :
                        </td>
                        <td>
                            &nbsp;&nbsp; Generate public key and private key for your domain.
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left">
                            &nbsp;&nbsp;&nbsp;&nbsp; 1) &nbsp;&nbsp;
                            <asp:HyperLink ID="hlGenerateKey" Text="Click Here" NavigateUrl="http://www.port25.com/support/domainkeysdkim-wizard/"
                                Target="_blank" runat="server"></asp:HyperLink>
                            &nbsp; to Generate Key.
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left">
                            &nbsp;&nbsp;&nbsp;&nbsp; 2) &nbsp;&nbsp; Enter &nbsp;<asp:Label ID="lblDomainAdd"
                                Text="______________" runat="server">
                            </asp:Label>&nbsp; in domain field.
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left">
                            &nbsp;&nbsp;&nbsp;&nbsp; 3) &nbsp;&nbsp; Enter &nbsp;<asp:Label ID="lblSelectorAdd"
                                Text="______________" runat="server"></asp:Label>
                            &nbsp;in Selector field.
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left">
                            &nbsp;&nbsp;&nbsp;&nbsp; 4)&nbsp;&nbsp;&nbsp; Choose 1024 as Key Size.
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left">
                            &nbsp;&nbsp;&nbsp;&nbsp; 5) &nbsp;&nbsp; Click on Create Key.
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp; 6) &nbsp;&nbsp; Copy Public Key and Private Key to respective
                            fields given below.(starting from "<b>----BEGIN...</b>" and Ending with "<b>...KEY-----</b>").
                        </td>
                    </tr>
                    <tr height="10px">
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                        </td>
                        <td align="left">
                            <div >
                                <table cellpadding="0" cellspacing="0" width ="100%">
                                    <tr>
                                        <td align ="right" width = "8%">
                                           
                                            Public Key
                                        </td>
                                        <td align ="left" width = "33%">
                                           &nbsp;&nbsp;
                                            <asp:TextBox ID="txtPublicKey" TextMode="MultiLine" Height="80px" Width="274px" runat="server"></asp:TextBox>
                                        </td>
                                        <td align ="right" width="8%" >
                                           
                                            Private Key 
                                        </td>
                                        <td align ="left">
                                           &nbsp;&nbsp;
                                            <asp:TextBox ID="txtPrivateKey" TextMode="MultiLine" Height="80px" Width="274px"
                                                runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr height="10px">
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Step 3 :
                        </td>
                        <td align="left">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnBroadcast" runat="server" CssClass="button" OnClientClick="javascript:return Save();"
                                Text="Enable Broadcasting" />
                        </td>
                    </tr>
                    <tr height="10px">
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Step 4 :
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Add following txt records to "DNS Manager/ Editor" of your domain name registar(i.e GoDaddy.com)
                            .After adding following txt records to your domain ,it may take 1
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; hour to 48 hours to update DNS Records.
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left">
                            <div style="padding-left: 15px">
                                <table width="100%" class="tbl" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <th>
                                        </th>
                                        <th align="center">
                                            <b>Host </b>
                                        </th>
                                        <th align="center">
                                            <b>TXT Value</b>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            Your SPF Record is :
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblSpfHost" Width="300px" runat="server"></asp:Label>
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:Label ID="lblSPF" Width="400px" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            Your DKIM Signature is :
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblDkimHost" Width="300px" runat="server"></asp:Label>
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:TextBox ID="txtDkim" Height="100px" Width="400px" runat="server" TextMode="MultiLine"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            Your Domain Key is :
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblDomainKeyHost" Width="300px" runat="server"></asp:Label>
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:Label ID="lblDomainKey" Width="400px" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr height="10px">
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Step 5 :
                        </td>
                        <td align="left">
                            &nbsp;&nbsp;&nbsp;&nbsp; <b><u>Validation Status</u></b> ( In order to start Email
                            Broadcasting SPF And DKIM must be pass validation .)
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                        </td>
                        <td align="left">
                            <div style="padding-left: 15px">
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="right">
                                            SPF Validation &nbsp;:
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:Label ID="lblSpfCheck" Text="" runat="server"></asp:Label>
                                        </td>
                                        <td align="left">
                                            &nbsp;&nbsp;
                                            <asp:HyperLink ID="hlSpf" Text="[ What is SPF ? ]" Target="_blank" NavigateUrl="http://en.wikipedia.org/wiki/Sender_Policy_Framework"
                                                runat="server">
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            DKIM Validation &nbsp;:
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:Label ID="lblDkimCheck" Text="" runat="server"></asp:Label>
                                        </td>
                                        <td align="left">
                                            &nbsp;&nbsp;
                                            <asp:HyperLink ID="hlDkim" Text="[ What is DKIM ? ]" Target="_blank" NavigateUrl="http://en.wikipedia.org/wiki/DomainKeys_Identified_Mail"
                                                runat="server">
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            Domain Key &nbsp;:
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:Label ID="lblDomainKeyCheck" Text="" runat="server"></asp:Label>
                                        </td>
                                        <td align="left">
                                            &nbsp;&nbsp;
                                            <asp:HyperLink ID="hlDomainKey" Text="[ What is Domain Key ? ]" Target="_blank" NavigateUrl="http://en.wikipedia.org/wiki/DomainKeys"
                                                runat="server">
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td align="right">
                                            Sender ID &nbsp;:
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                            <asp:Label ID="lblSenderIDCheck" Text="" runat="server"></asp:Label>
                                        </td>
                                        <td align="left">
                                            &nbsp;&nbsp;
                                            <asp:HyperLink ID="hlSenderID" Text="[ What is Sender ID? ]" Target="_blank" NavigateUrl="http://en.wikipedia.org/wiki/Sender_ID"
                                                runat="server">
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                        </td>
                        <td align="left">
                            <div style="padding: 0px 0px 0px 16px">
                                <asp:Panel runat="server" ID="pnlSMTPError" CssClass="info">
                                    <b>Note:</b><br />
                                    Please read
                                    <asp:HyperLink ID="hlDeliverability" Target="_blank" NavigateUrl="http://help.bizautomation.com/default.aspx?pageurl=Deliverability"
                                        Text="Email deliverability best practice" runat="server"></asp:HyperLink>
                                    before you broadcast mails out.
                                </asp:Panel>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
