''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Survey
''' Class	 : frmCampaignSurveyRespondents
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a interface for listing the Respondents for a Survey
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	09/29/2005	Created
''' </history>
''' ''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Survey
    Public Class frmCampaignSurveyRespondents
        Inherits BACRMPage

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime the page is called. In this event we will 
        '''     get the data from the DB and populate the Tables and the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                If Not IsPostBack Then
                    DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                    DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                    ' = "MarSurvey"
                    DisplaySurveyRespondentLists()              'Call to display the list of respondent for the surveys
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the respondents of a survey from the database
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function DisplaySurveyRespondentLists()
            Try
                Dim objSurvey As New SurveyAdministration                                   'Declare and create a object of SurveyAdministration
                objSurvey.DomainId = Session("DomainID")                                    'Set the Doamin Id
                objSurvey.SurveyId = GetQueryStringVal("numSurID")                        'Set the Survey Id as it comes from the query parameter
                objSurvey.DomainId = Session("DomainID")
                Dim dtSurveyRespondentlist As DataTable                                     'Declare a datatable
                dtSurveyRespondentlist = objSurvey.getSurveyRespondentsList().Tables(0)     'call function to get the list of available survey respondents

                Dim dvSurveyRespondentlist As DataView = GetRowsInPageRange(dtSurveyRespondentlist).DefaultView 'Create a DataView
                dvSurveyRespondentlist.Sort = SortExp.Text                                  'Set teh Sort Order
                If FilterExp.Text <> "" Then dvSurveyRespondentlist.RowFilter = "vcRespondentName like '%" & FilterExp.Text & "%'" 'Set the row filter
                dgSurveyRespondentList.DataSource = dvSurveyRespondentlist                  'set the datasource
                dgSurveyRespondentList.DataBind()                                           'databind the datgrid

                bizPager.PageSize = Session("PagingRows")
                bizPager.CurrentPageIndex = txtCurrrentPage.Text
                bizPager.RecordCount = dtSurveyRespondentlist.Rows.Count
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to pick up a selected range of rows
        ''' </summary>
        ''' <remarks> Returns the table with limited rows
        ''' </remarks>
        ''' <param name="dtSurveyRespondentlist">Table which contains the search result to be displayed in the DataGrid</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function GetRowsInPageRange(ByVal dtSurveyRespondentlist As DataTable) As DataTable
            Try
                Dim numRowIndex As Integer                                                          'Declare an index variable
                Dim dtResultTableCopy As New DataTable                                              'Declare a datatable object and instantiate
                dtResultTableCopy = dtSurveyRespondentlist.Clone()                                  'Clone the results table

                Dim iRowIndex As Integer = (txtCurrrentPage.Text * Session("PagingRows")) - Session("PagingRows") 'Declare a row index and set it to the first row in the table which has ot be displayed
                Dim iLastRowIndex As Integer = (iRowIndex + Session("PagingRows")) - 1                                        'Nos of records to be traversed
                For numRowIndex = iRowIndex To iLastRowIndex                                        'Loop through the row index from the table
                    If ((numRowIndex >= 0) And (dtSurveyRespondentlist.Rows.Count > numRowIndex)) Then 'Ensure that the row exists
                        If dtSurveyRespondentlist.Rows(numRowIndex).Item("bitAddedToBz") = "Yes" Then 'Check if the Respondent has been added to Biz 
                            dtSurveyRespondentlist.Rows(numRowIndex).Item("vcRespondentName") = "<a href='javascript: GoContactDetails(" & dtSurveyRespondentlist.Rows(numRowIndex).Item("numRegisteredRespondentContactId") & ")'>" & dtSurveyRespondentlist.Rows(numRowIndex).Item("vcRespondentName") & "</a>"
                            dtSurveyRespondentlist.Rows(numRowIndex).Item("vcCompanyName") = "<a href='javascript: GoOrgDetails(" & dtSurveyRespondentlist.Rows(numRowIndex).Item("numRegisteredRespondentContactId") & ")'>" & dtSurveyRespondentlist.Rows(numRowIndex).Item("vcCompanyName") & "</a>"
                        End If
                        dtResultTableCopy.ImportRow(dtSurveyRespondentlist.Rows(numRowIndex))       'import the range of rows
                    End If
                Next
                Return dtResultTableCopy
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to process the datagrid commands
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the System.Web.UI.WebControls.DataGridSortCommandEventArgs.</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub dgSurveyRespondentList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgSurveyRespondentList.SortCommand
            Try
                Dim sOrder As String = SortExp.Text                                     'Get teh Sort ORder
                If sOrder.IndexOf(e.SortExpression.ToString) > -1 Then                  'Compare the Sort Order
                    SortExp.Text = IIf(SortExp.Text.IndexOf("asc") > -1, Replace(SortExp.Text, "asc", "desc"), Replace(SortExp.Text, "desc", "asc")) 'Toggle the Sort Order
                Else : SortExp.Text = e.SortExpression.ToString & " asc"
                End If
                DisplaySurveyRespondentLists()                                          'Call to display the Survey Respondents
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to filter the Respondent's names and initiated by a click on the Go button
        ''' </summary>
        ''' <remarks> 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                FilterExp.Text = txtRespondentName.Text                                 'Set the Respondent filter name
                DisplaySurveyRespondentLists()                                          'Call to display the Survey Respondents
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo1_Click(sender As Object, e As System.EventArgs) Handles btnGo1.Click
            FilterExp.Text = txtRespondentName.Text                                 'Set the Respondent filter name
            DisplaySurveyRespondentLists()                                          'Call to display the Survey Respondents
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to Prepare the search result for export to Excel
        ''' </summary>
        ''' <remarks> Returns the DataSet which contains the Results DataTable which can be directly exported to Excel
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/23/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function PrepareDataTableForExportToExcel() As DataSet
            Try
                Dim objSurvey As New SurveyAdministration                                   'Declare and create a object of SurveyAdministration
                objSurvey.DomainId = Session("DomainID")                                    'Set the Doamin Id
                objSurvey.SurveyId = GetQueryStringVal("numSurID")                        'Set the Survey Id as it comes from the query parameter
                objSurvey.DomainId = Session("DomainID")
                Dim dsSurveyRespondentsList As DataSet = objSurvey.getSurveyRespondentsList() 'call function to get the list of available survey respondents
                Dim dtSurveyRespondentsList As DataTable = dsSurveyRespondentsList.Tables(0) 'Table containing the Sruvey Respondent List
                dtSurveyRespondentsList.Columns.Remove("numSurId")
                dtSurveyRespondentsList.Columns.Remove("vcSurName")
                dtSurveyRespondentsList.Columns(dtSurveyRespondentsList.Columns.IndexOf("dtDateofResponse")).ColumnName = "Date Of Response"
                dtSurveyRespondentsList.Columns.Remove("numRespondantID")
                dtSurveyRespondentsList.Columns(dtSurveyRespondentsList.Columns.IndexOf("vcRespondentName")).ColumnName = "Respondents"
                'dtSurveyRespondentsList.Columns(dtSurveyRespondentsList.Columns.IndexOf("vcPhoneNumberAndExt")).ColumnName = "Phone Number & Ext"
                dtSurveyRespondentsList.Columns(dtSurveyRespondentsList.Columns.IndexOf("vcEmail")).ColumnName = "Email"
                dtSurveyRespondentsList.Columns(dtSurveyRespondentsList.Columns.IndexOf("bintCreatedDate")).ColumnName = "Date Contact Added"
                dtSurveyRespondentsList.Columns(dtSurveyRespondentsList.Columns.IndexOf("vcCompanyName")).ColumnName = "Organization"
                dtSurveyRespondentsList.Columns(dtSurveyRespondentsList.Columns.IndexOf("bitAddedToBz")).ColumnName = "Added to BizAutomation ?"
                dtSurveyRespondentsList.Columns(dtSurveyRespondentsList.Columns.IndexOf("numSurRating")).ColumnName = "Survey Rating"
                Return dsSurveyRespondentsList
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This is executed whenever an request to export the data to excel is requested
        ''' </summary>
        ''' <remarks> Passing the datatable to a predefined function (by Mahalingam in reports) servers the purpose
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportRespondentsToExcel.Click
            Try
                Dim objPredefinedReports As New PredefinedReports                                               'Declare and instantiate an object of predefined reports (created by Maha) and used for exporting the report to ms-excel
                Dim strPath As String = CCommon.GetDocumentPhysicalPath() & "Report" 'Path where the files are to be exported
                Dim strRef As String                                                                            'Variable to capture the file name 
                objPredefinedReports.ExportToExcel(PrepareDataTableForExportToExcel(), CCommon.GetDocumentPhysicalPath(), strPath, Session("UserName"), Session("UserID"), "Survey_Respondents", Session("DateFormat"))
                strRef = CCommon.GetDocumentPath() & "/Report/" & Session("UserName") & "_" & Session("UserID") & "_Survey_Respondents.csv"
                litClientMessage.Text = "<script language=javascript>alert('Exported Successfully !');window.open('" & strRef & "');</script>" 'Alert to user and open the Excel
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the back button is clicked
        ''' </summary>
        ''' <remarks> Redirects to the Survey Listing Page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("frmCampaignSurveyList.aspx", True)                       'Redirect to the Survey List page
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete Survey Respondents
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the System.Web.UI.WebControls.DataGridSortCommandEventArgs.</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/27/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub dgSurveyRespondentList_ItemCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSurveyRespondentList.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim numRespondantID As Integer                                                  'Declare a variable for Respondent Id
                    numRespondantID = e.Item.Cells(0).Text                                          'Get the Respondent Id
                    Dim objSurvey As New SurveyAdministration                                       'Declare and create a object of SurveyAdministration
                    objSurvey.SurveyId = GetQueryStringVal("numSurID")                            'Get the Survey Id
                    objSurvey.SurveyExecution.SurveyExecution().RespondentID = numRespondantID      'Get the Respondent ID
                    objSurvey.DeleteSurveyHistoryForContact()                                       'Call to delete the Survey History

                    DisplaySurveyRespondentLists()                                                  'Call to Display the Results
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to attach valudation funciton to the delete button of Survey Respondents
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the System.Web.UI.WebControls.DataGridSortCommandEventArgs.</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/27/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub dgSurveyRespondentList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSurveyRespondentList.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As Button                                                     'Declare a button
                    btnDelete = e.Item.FindControl("btnDelete")                                 'Capture the handle to the button
                    btnDelete.Attributes.Add("onclick", "return DeleteRespondentRecord();")               'Attach event to the button
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnDateTime(ByVal CloseDate) As String
            Try
                If IsDBNull(CloseDate) = False Then Return FormattedDateTimeFromDate(CloseDate, Session("DateFormat"))
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                DisplaySurveyRespondentLists()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
    End Class
End Namespace