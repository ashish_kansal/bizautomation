﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSurveyDirect.aspx.vb"
    Inherits=".frmSurveyDirect" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Campaign Survey</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script type="text/javascript" src="../javascript/comboClientSide.js"></script>
    <script language="javascript" type="text/javascript">
        function pageLoad(sender, args) {
            initComboExtensions();
            cbCompany = $find("radCmbCompany");
            if (cbCompany != null) {
                cbCompany.onDataBound = cb_onDataBoundOrganization;
            }
        }

        function Close() {
            window.close()
            return false;
        }

        function Save() {

            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Company")
                return false;
            }

            var e = document.getElementById("ddlTaskContact");
            var contact = e.options[e.selectedIndex].value;

            if (contact == "0") {
                alert("Select Contact")
                return false;
            }

            var e1 = document.getElementById("ddlSurvey");
            var survey = e1.options[e1.selectedIndex].value;

            if (survey == 0) {
                alert("Select Survey")
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td valign="bottom">
                    <table class="TabStyle">
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;Survey for Contact&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Table ID="tblSurveyCreate" runat="server" CellSpacing="1" CellPadding="1" Width="100%"
            BorderColor="black" BorderWidth="1" CssClass="aspTable">
            <asp:TableRow>
                <asp:TableCell CssClass="normal1">
                  Customer<font color="red">*</font>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1">
                    <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                        OnClientItemsRequested="OnClientItemsRequestedOrganization"
                        ClientIDMode="Static"
                        ShowMoreResultsBox="true"
                        Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                        <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                    </telerik:RadComboBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="normal1">
            Contact<font color="red">*</font>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1">
                    <asp:DropDownList ID="ddlTaskContact" CssClass="signup" runat="server" Width="180">
                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="normal1">
            Survey<font color="red">*</font>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1">
                    <asp:DropDownList ID="ddlSurvey" runat="server" CssClass="signup" Width="100">
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="normal1">
                    &nbsp;
                <asp:Literal ID="litClientScript" runat="server"></asp:Literal>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1">
                    <asp:Button ID="btnSurvey" runat="Server" CssClass="button" Text="Proceed to Survey" />&nbsp;&nbsp;
                <asp:Button ID="btnCancel" runat="Server" CssClass="button" Text="Cancel" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </form>
</body>
</html>
