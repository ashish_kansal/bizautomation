<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCampaignSurveyWorflowRuleEditor.aspx.vb"
    Inherits="BACRM.UserInterface.Survey.frmCampaignSurveyWorflowRuleEditor" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Survey Workflow Rules Editor</title>
    <script language="javascript" src="../javascript/Surveys.js"></script>
    <script language="javascript" src="../javascript/Validation.js"></script>
    <script language="javascript" type="text/javascript">
        function OpenWorkflowSelectFields(intRuleNoRadioNo) {

            var target;
            var RadioId = 0;
            var valid = false;
            target = 'frmCampaignSurveyWorflowRuleSelectField.aspx?numSurId=' + document.getElementById('hdSurId').value + '&numQuestionId=' + document.getElementById('hdQId').value + '&numAnsID=' + document.getElementById('hdAnsId').value + '&numMatrix=' + document.getElementById('hdMatrix').value;
            if (intRuleNoRadioNo == 41) {
                if (document.getElementById('rRule4Radio1').checked && document.getElementById('cbActivationRuleFour').checked) {
                    RadioId = 1;
                    target = target + '&numRuleNo=4';
                    target = target + '&numRadioNo=' + RadioId;
                    valid = true;
                }
                else {
                    alert('Select the proper Radio for Rule no 4');
                }
            }
            if (intRuleNoRadioNo == 42) {
                if (document.getElementById('rRule4Radio2').checked && document.getElementById('cbActivationRuleFour').checked) {
                    RadioId = 2;
                    target = target + '&numRuleNo=4';
                    target = target + '&numRadioNo=' + RadioId;
                    valid = true;
                }
                else {
                    alert('Select the proper Radio for Rule no 4');
                }
            }
            if (intRuleNoRadioNo == 51) {
                if (document.getElementById('cbActivationRuleFive').checked) {
                    target = target + '&numRuleNo=5';
                    target = target + '&numRadioNo=1';
                    valid = true;
                }
                else {
                    alert('Select a rule first');
                    return false;
                }

            }
            if (intRuleNoRadioNo == 52) {
                if (document.getElementById('cbActivationRuleFive').checked && document.getElementById('cboRuleFiveCheck1').checked) {
                    target = target + '&numRuleNo=5';
                    target = target + '&numRadioNo=2';
                    valid = true;
                }
                else {
                    alert('Please select a rule and check the Populate fields Check box before clicking on the link');
                    return false;
                }

            }
            var hnadw
            if (valid == true) {
                hnadw = window.open(target, 'SurveySelectField', 'toolbar=no,titlebar=no,left=200, top=200,width=400,height=300,scrollbars=yes,resizable=no');
                if (hnadw != null) {
                    hnadw.focus();
                }
                else {
                    alert('A popup has been blocked , please allow popups to show the form');
                }
            }
            return false;
        }
		
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveWorkFlow" runat="Server" CssClass="button" Text="Save"></asp:Button>&nbsp;<asp:Button
                ID="btnSaveAndCloseWorkFlow" runat="Server" CssClass="button" Text="Save &amp; Close">
            </asp:Button>&nbsp;<asp:Button ID="btnClose" runat="Server" CssClass="button" Text="Close">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Survey Workflow Rules Editor
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <input id="hdSurId" type="hidden" name="hdSurId" runat="server">
    <input type="hidden" runat="server" name="hdQId" id="hdQId">
    <input id="hdAnsId" type="hidden" name="hdAnsId" runat="server">
    <input type="hidden" runat="server" name="hdAction" id="hdAction">
    <input id="hdMatrix" type="hidden" name="hdMatrix" runat="server">
    <asp:Literal ID="litClientMessage" runat="server" EnableViewState="False"></asp:Literal>
    <div style="margin-left: 40px">
        <asp:Table ID="tblWorkFlowRulesEdited" runat="server" CellSpacing="1" CellPadding="8"
            Width="100%" CssClass="aspTable" GridLines="None" BorderColor="#000000" BorderWidth="1">
            <asp:TableRow CssClass="normal1">
                <asp:TableCell CssClass="hs" ColumnSpan="2">
						&nbsp;Activation
                </asp:TableCell>
                <asp:TableCell CssClass="hs" HorizontalAlign="Left" VerticalAlign="Top">
						&nbsp;Order of events
                </asp:TableCell>
                <asp:TableCell CssClass="hs">
						&nbsp;Workflow Rule
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="trActivationRuleOne" CssClass="normal1">
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
						1.
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                    <asp:CheckBox ID="cbActivationRuleOne" runat="Server"></asp:CheckBox>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:DropDownList ID="ddlOrderOfEventsOne" runat="server" CssClass="signup">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <%--<asp:ListItem Value="7">7</asp:ListItem>--%>
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Left">
                    Ask respondent question(s)
                    <asp:TextBox ID="txtAskQuestions" runat="server" CssClass="signup" MaxLength="50"></asp:TextBox>
                    of Survey ID
                    <asp:TextBox ID="txtLinkedSurveyID" runat="server" CssClass="signup" MaxLength="9"></asp:TextBox><br>
                    <strong>Note:</strong> <span class="normal3">You can not enter the ID of this Questionaire/Survey</span>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="trActivationRuleTwo" CssClass="normal1">
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
						2.
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                    <asp:CheckBox ID="cbActivationRuleTwo" runat="Server"></asp:CheckBox>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:DropDownList ID="ddlOrderOfEventsTwo" runat="server" CssClass="signup">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <%--<asp:ListItem Value="7">7</asp:ListItem>--%>
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Left">
                    Skip question(s)
                    <asp:TextBox ID="txtSkipQuestions" runat="server" CssClass="signup" MaxLength="50"></asp:TextBox>
                    of this Survey
                    <asp:TextBox ID="txtLinkedSkipSurveyID" runat="server" CssClass="signup" Style="display: none;"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <%--  <asp:TableRow ID="trActivationRuleThree" cssclass="normal1">
                        <asp:TableCell cssClass="normal1" VerticalAlign="Top">
						3.
					</asp:TableCell>
                        <asp:TableCell cssClass="normal1" VerticalAlign="Top">
						<asp:CheckBox ID="cbActivationRuleThree" Runat="Server"></asp:CheckBox>
					</asp:TableCell>
                        <asp:TableCell cssclass="normal1" HorizontalAlign="Center" VerticalAlign="Top">
						<asp:DropDownList ID="ddlOrderOfEventsThree" Runat="server" CssClass="signup">
							<asp:ListItem Value="1">1</asp:ListItem>
							<asp:ListItem Value="2">2</asp:ListItem>
							<asp:ListItem Value="3">3</asp:ListItem>
							<asp:ListItem Value="4">4</asp:ListItem>
							<asp:ListItem Value="5">5</asp:ListItem>
							<asp:ListItem Value="6">6</asp:ListItem>
							<asp:ListItem Value="7">7</asp:ListItem>
						</asp:DropDownList>
					</asp:TableCell>
                        <asp:TableCell cssclass="normal1" HorizontalAlign="Left" VerticalAlign="Top">
						Create a  						
						<asp:DropDownList ID="ddlCRMType" Runat="server" CssClass="signup">
							<asp:ListItem Value="0">Lead</asp:ListItem>
							<asp:ListItem Value="1">Prospect</asp:ListItem>
							<asp:ListItem Value="2">Account</asp:ListItem>
						</asp:DropDownList>
						with relationship of
						<asp:DropDownList ID="ddlRelationShip" Runat="server" CssClass="signup"></asp:DropDownList>
						in the group
						<asp:DropDownList ID="ddlGroup" Runat="server" CssClass="signup"></asp:DropDownList><br>
						assigned to (record owner)
						<asp:DropDownList ID="ddlRecordOwner" Runat="server" CssClass="signup"></asp:DropDownList><br>
						<strong>Note:</strong> <span class="normal3">Information Source for the record will 
							be automatically populated with the Questionnaire name.</span>
					</asp:TableCell>
                    </asp:TableRow>--%>
            <asp:TableRow ID="trActivationRuleFour" CssClass="normal1">
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
						3.
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                    <asp:CheckBox ID="cbActivationRuleFour" runat="Server"></asp:CheckBox>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:DropDownList ID="ddlOrderOfEventsFour" runat="server" CssClass="signup">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <%--<asp:ListItem Value="7">7</asp:ListItem>--%>
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Left">
                    If Company / Contact record exists OR if new record is created from this survey
                    / questionnaire, take the answer value and do the following:
                    <br />
                    <asp:RadioButton runat="server" ID="rRule4Radio1" GroupName="RuleFourRadio" Text="Within the contact detail of the respondent's record take the value of the answer, and use it to populate the following field: " />
                    <asp:LinkButton runat="server" ForeColor="Blue" ID="lnkRuleFourRadioOne" OnClientClick="return OpenWorkflowSelectFields(41)">Select Field</asp:LinkButton>
                    <br />
                    <asp:RadioButton runat="server" ID="rRule4Radio2" GroupName="RuleFourRadio" Text="Within the company detail of the respondent's company record take the value of the answer, and use it to populate the following field: " />
                    <asp:LinkButton runat="server" ForeColor="Blue" ID="lnkRuleFourRadioTwo" OnClientClick="return OpenWorkflowSelectFields(42)">Select Field</asp:LinkButton>
                    <br />
                    <asp:RadioButton runat="server" ID="rRule4Radio3" GroupName="RuleFourRadio" Text="Create a new: " />
                    <asp:DropDownList ID="ddlRuleFourRadio3" runat="server" CssClass="signup">
                        <asp:ListItem Value="0">Sale Opportunity</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <asp:RadioButton runat="server" ID="rRule4Radio4" GroupName="RuleFourRadio" Text="Open the Sales Order form, with the Company / Contact name pre-populated with the respondentís Company / Contact name." />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="trActivationRuleFive" CssClass="normal1">
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
						4.
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                    <asp:CheckBox ID="cbActivationRuleFive" runat="Server"></asp:CheckBox>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:DropDownList ID="ddlOrderOfEventsFive" runat="server" CssClass="signup">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <%--<asp:ListItem Value="7">7</asp:ListItem>--%>
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Left">
                    If new Sales Opportunity or Sales Order is created as a result of this survey/questionnaire
                    (3rd or 4th radio button of rule 4), then within the detail page of that Sales Opportunity
                    or Sales Order, take the answer value, and populate the following field:
                    <asp:LinkButton runat="server" ForeColor="Blue" ID="lnkButtonRuleFiveLink1" OnClientClick="javascript:OpenWorkflowSelectFields(51)">Select Field</asp:LinkButton>
                    <br />
                    <asp:CheckBox ID="cboRuleFiveCheck1" runat="server" />
                    Populate the Sales Opportunity or Order with the following Items:
                    <asp:LinkButton runat="server" ForeColor="Blue" ID="lnkButtonRuleFiveLink2" OnClientClick="javascript:OpenWorkflowSelectFields(52)">Select Field</asp:LinkButton>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="trActivationRuleSix" CssClass="normal1">
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
						5.
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                    <asp:CheckBox ID="cbActivationRuleSix" runat="Server"></asp:CheckBox>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:DropDownList ID="ddlOrderOfEventsSix" runat="server" CssClass="signup">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <%--<asp:ListItem Value="7">7</asp:ListItem>--%>
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Left">
                    Open the following page:
                    <asp:TextBox ID="txtOpenURL" runat="server" CssClass="signup" MaxLength="100" Width="300"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="trActivationRuleSeven" CssClass="normal1">
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
						6.
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                    <asp:CheckBox ID="cbActivationRuleSeven" runat="Server"></asp:CheckBox>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:DropDownList ID="ddlOrderOfEventsSeven" runat="server" CssClass="signup">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <%--<asp:ListItem Value="7">7</asp:ListItem>--%>
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell CssClass="normal1" HorizontalAlign="Left">
                    Add the following value to the respondentís survey rating
                    <asp:TextBox ID="txtAddSurveyRating" runat="server" CssClass="signup" MaxLength="2"
                        Width="20"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</asp:Content>
