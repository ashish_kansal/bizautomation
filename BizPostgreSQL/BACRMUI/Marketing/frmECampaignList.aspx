﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmECampaignList.aspx.vb"
    Inherits=".frmECampignList" MasterPageFile="~/common/GridMaster.Master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script language="javascript" type="text/javascript">
        function reDirectPage(url) {
            window.location.href = url
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            
            return false;
        }
        function gotoPage(target, frame, div) {
            window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=750,height=450,scrollbars=no,resizable=yes')
        }
        function OpenHelp() {
            window.open('../Help/Automated_FollowUps.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        
    </script>
    <style>
        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            #dllCampaignStatus {
                width: 100%;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="pull-right padbottom10">
        <asp:HyperLink ID="hplNewECamp" NavigateUrl="~/Marketing/frmECampaignDtls.aspx" CssClass="btn btn-primary"
            runat="server"><i class=" fa fa-plus-circle"></i>&nbsp;&nbsp;New Automated Follow-up Process
        </asp:HyperLink>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Automated Follow-ups&nbsp;<a href="#" onclick="return OpenHelpPopUp('marketing/frmecampaignlist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
     <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
   <%-- <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">--%>

    <telerik:RadTabStrip ID="radFollowUpTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" AutoPostBack="True" 
        MultiPageID="radMultiPage_FollowUpTab" ClientIDMode="Static">
        <Tabs>
            <telerik:RadTab Text="Automated Follow-up Processes" ClientIDMode="Static" Selected="true" Value="FollowUpProcesses" PageViewID="radPageView_FollowUpProcesses">
            </telerik:RadTab>
            <telerik:RadTab Text="Automated Follow-up Campaigns" ClientIDMode="Static" Value="FollowUpCampaigns" PageViewID="radPageView_FollowUpCampaigns">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_FollowUpTab" SelectedIndex="0"  runat="server" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_FollowUpProcesses" runat="server">
            
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        
                        <asp:DataGrid ID="dgECamapign" AllowSorting="True" runat="server" Width="100%" CssClass="table table-bordered table-striped"
                            AutoGenerateColumns="False" UseAccessibleHeader="true">
                            <Columns>
                                <asp:BoundColumn Visible="False" DataField="numECampaignID" HeaderText="numECampaignID"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="vcECampName" SortExpression="vcECampName" HeaderText="Follow-Up Type"
                                    CommandName="Campaign"></asp:ButtonColumn>
                                <asp:BoundColumn DataField="txtDesc" SortExpression="txtDesc" HeaderText="Description"></asp:BoundColumn>
                                <asp:BoundColumn DataField="NoOfUsersAssigned" HeaderText="Active Recipients" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="NoOfEmailSent" HeaderText="Messages Sent" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Messages Read" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%# If(Eval("NoOfEmailSent") = "0", "0 (0.00 %)", Eval("NoOfEmailOpened") & " " & String.Format("{0:(0.00 %)}", (Convert.ToInt32(Eval("NoOfEmailOpened")) / Convert.ToInt32(Eval("NoOfEmailSent")))))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Links Clicked" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%# If(Eval("NoOfLinks") = "0", "-", Eval("NoOfLinkClicked") & " Out Of " & Eval("NoOfLinks") & " " & String.Format("{0:(0.00 %)}", (Convert.ToInt32(Eval("NoOfLinkClicked")) / Convert.ToInt32(Eval("NoOfLinks")))))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDeleteECamapign" runat="server" CssClass="btn btn-danger btn-xs"  CommandName="Delete"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDeleteECamapign" runat="server" Visible="false"><font color="#730000">*</font></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                    <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
                         
                    </div>
                </div>
            </div>
        </telerik:RadPageView>

         <telerik:RadPageView ID="radPageView_FollowUpCampaigns" runat="server">
              <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                       
                            
                           <div class="form-group">
                               <asp:Label ID="lblNote" Text="Note - Only active campaigns are displayed" runat="server" Font-Italic="true"></asp:Label>
                               <%--<asp:CheckBox ID="chkDisplayCompletedCampaigns" Text="Display only Completed Campaigns" AutoPostBack="true" OnCheckedChanged="chkDisplayCompletedCampaigns_CheckedChanged" runat="server" />&nbsp;--%>                              
                                <div class="pull-right padbottom10">
                               <asp:LinkButton ID="lnkEndCamapign" OnClick="lnkEndCamapign_Click1"  CausesValidation ="false"  runat="server" CssClass="btn btn-primary">End selected Campaign(s)</asp:LinkButton>
                            </div>
                            
                       </div>
                         
                        <asp:DataGrid ID="dgCampaigns" AllowSorting="True" runat="server" DataKeyField="ID" Width="100%" CssClass="table table-bordered table-striped"
                            AutoGenerateColumns="False" UseAccessibleHeader="true">
                       
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID"  ></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="numContactID" HeaderText="numContactID"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="numECampaignId" HeaderText="numECampaignId"></asp:BoundColumn>
                                    <%--<asp:BoundColumn DataField="CompanyName" HeaderText="Organization" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Contact Name" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <a href="#" onclick="OpenContact(<%# Eval("numContactID") %>);">
                                                <%# Eval("ContactName") %></a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>--%>
                                    <asp:BoundColumn DataField="vcECampName" SortExpression="vcECampName" HeaderText="Follow-up Type" HeaderStyle-HorizontalAlign="Center"><%--HeaderText="Drip Campaign"--%>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="dtNextStage" HeaderText="Next Send Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="FromEmail" HeaderText="From Email Address" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Stage" HeaderText="NoOfStages" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NoOfStagesCompleted" HeaderText="NoOfStagesCompleted" Visible="false"></asp:BoundColumn>
                                    <%--<asp:TemplateColumn HeaderText="No Of Stages Completed" ItemStyle-Width="100" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%# Eval("NoOfStagesCompleted")%> Out Of <%# Eval("Stage")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>--%>
                                    <asp:BoundColumn DataField="NextTemplate" HeaderText="Next Temaplate to go out(Stage)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Recipient" HeaderText="Recipient(Opened)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <%-- <asp:BoundColumn DataField="Template" HeaderText="Last Event" HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Executed on / Success or Failure?" ItemStyle-Width="120" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%# If(Eval("dtSentDate") = "", "", Eval("dtSentDate") & " / ")%>
                                            <asp:Label runat="server" Text='<%# Eval("SendStatus")%>' ForeColor='<%# IIf(Eval("SendStatus") = "Success", System.Drawing.Color.Green, IIf(Eval("SendStatus") = "Failed", System.Drawing.Color.Red, System.Drawing.Color.SaddleBrown))%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="dtNextStage" HeaderText="Next Execution On" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NoOfEmailSent" HeaderText="No of Eamil Sent" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NoOfEmailOpened" HeaderText="No of Eamil Opened" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>--%>
                                    <%--<asp:TemplateColumn HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80">
                                        <ItemTemplate>
                                            <a href="#" onclick="openDrip(<%# Eval("numConEmailCampID") %>);">View Detail</a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>--%>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chk" runat="server" onclick="SelectAll('chk','chk1')" />
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign ="Center" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk" CssClass="chk1" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            
                    
              </div>
                </div>
            </div>
        </telerik:RadPageView>  
    </telerik:RadMultiPage>

             
           <%-- </div>
        </div>
    </div>--%>

    <asp:Button ID="btnGo1" Style="display: none" runat="server" Text="Go"></asp:Button>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
          <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
