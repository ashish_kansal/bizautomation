<%@ Page Language="vb" AutoEventWireup="false" Codebehind="frmCampaignSurveyQuestions.aspx.vb" Inherits="BACRM.UserInterface.Survey.frmCampaignSurveyQuestions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
		<link runat="server" rel="stylesheet" href="~/CSS/Import.css" type="text/css" id="AdaptersInvariantImportCSS" />

        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>Marketing Survey Question Editor</title>
		
		
		
		
		

		<script language="javascript" src="../javascript/Surveys.js"></script>
	</HEAD>
	<body >
		<form id="frmCampaignSurveyQuestions" method="post" runat="server">
			<asp:table id="tblCampaignSurveyQuestions" Runat="server" BorderWidth="0" borderColor="#ffffff"
				GridLines="None" Width="100%" EnableViewState="False"></asp:table>
			<asp:Button ID="btnAddNewQuestion" Text="Add New Question" cssClass="button" Runat="Server"
				style="DISPLAY:none"></asp:Button>
			<asp:Button ID="btnDeleteQuestion"  CssClass="button Delete" Text="X" Runat="Server" style="DISPLAY:none" CausesValidation="False"></asp:Button>
			<input type="hidden" runat="server" name="hdSurId" id="hdSurId">
			<asp:Button ID="btnSaveQuestion" Text="Save" cssClass="button" Runat="Server" style="DISPLAY:none"></asp:Button>
			 <input type="hidden" runat="server" name="hdAction" id="hdAction"><input type="hidden" runat="server" name="hdQIDDelete" id="hdQIDDelete">
			 <input type="hidden" runat="server" name="hdQuesNewRow" id="hdQuesNewRow" value="False">
			<asp:validationsummary id="ValidationSummary" runat="server" HeaderText="Please check the following value(s)"
				ShowSummary="False" ShowMessageBox="True" DisplayMode="List"></asp:validationsummary>
			<asp:literal id="litClientMessage" Runat="server" EnableViewState="False"></asp:literal>
		</form>
	</body>
</HTML>
