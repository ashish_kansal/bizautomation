<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCampaignSurveyEditor.aspx.vb"
    Inherits="BACRM.UserInterface.Survey.frmCampaignSurveyEditor" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Marketing Survey Editor</title>
    <script language="javascript" src="../javascript/Surveys.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnCloneSurvey" Text="Clone this survey" CssClass="btn btn-primary" runat="Server">
            </asp:Button>
            &nbsp;&nbsp;<asp:Button ID="btnOpenRegPage"
                Text="Open Registration Page" CssClass="btn btn-primary" runat="Server"></asp:Button>&nbsp;&nbsp;<asp:Button
                    ID="btnSaveSurvey" Text="Save Survey" CssClass="btn btn-primary" runat="Server"></asp:Button>&nbsp;&nbsp;<asp:Button
                        ID="btnSaveSurveyActualButHidden" Text="Save" CssClass="btn btn-primary" runat="Server"
                        Style="display: none"></asp:Button>&nbsp;<asp:Button ID="btnSaveAndClose" Text="Save &amp; Close Survey"
                            CssClass="btn btn-primary" runat="Server"></asp:Button>&nbsp;<asp:Button ID="btnSaveAndCloseActualButHidden"
                                Text="Save &amp; Close" CssClass="btn btn-primary" runat="Server" Style="display: none">
            </asp:Button>&nbsp;<asp:Button ID="btnCancelSurvey" Text="Cancel" CssClass="btn btn-danger"
                runat="Server" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Survey Editor
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
     <div class="table-responsive">
    <asp:Table ID="tblSurveyEdited" runat="server" BorderWidth="1" BorderColor="#000000"
        GridLines="None" Width="100%" CellPadding="1" CellSpacing="1">
        <asp:TableRow>
            <asp:TableCell Width="40%" CssClass="normal1" VerticalAlign="Middle">
                <div class="form-inline">
                    <div class="col-md-12">
                    <label>Survey Name<span class="normal4">*</span> : </label>
                     <asp:TextBox ID="txtSurveyName" runat="server" CssClass="form-control" AutoPostBack="False" style="min-width:50%"  MaxLength="100"></asp:TextBox>
                          <asp:RequiredFieldValidator ControlToValidate="txtSurveyName" EnableClientScript="True"
                    Display="None" InitialValue="" ErrorMessage="Please enter a Survey Name." runat="server"
                    ID="ValidateSurveyName"></asp:RequiredFieldValidator>
                    </div>
                </div>
               
              
            </asp:TableCell>
            <asp:TableCell Width="60%" CssClass="normal1" VerticalAlign="Top" RowSpan="3" style="padding:10px;border-color:#e1e1e1 !important;">
                <div class="form-inline">
                    <div class="col-md-12">
                        <input type="checkbox" runat="server" id="cbSkipRegistration" onclick="javascript: ReassurePostRegistration(this);" />Skip Registration?
                    </div>
                    <div class="col-md-12"  style="padding-right:0px;line-height: 40px;">
                        <input type="checkbox" runat="server" id="cbNewRelationship" onclick="javascript: CreateRecord();" />
                Use registration details to create a new: Create a <asp:DropDownList ID="ddlCRMType" runat="server" CssClass="form-control">
                    <asp:ListItem Value="0">Lead</asp:ListItem>
                    <asp:ListItem Value="1">Prospect</asp:ListItem>
                    <asp:ListItem Value="2">Account</asp:ListItem>
                </asp:DropDownList>&nbsp;with relationship of&nbsp;<asp:DropDownList ID="ddlRelationShip" runat="server" CssClass="form-control" AutoPostBack="true">
                </asp:DropDownList>&nbsp;& profile <asp:DropDownList ID="ddlProfile" runat="server" CssClass="form-control">
                </asp:DropDownList>&nbsp;
                in the group&nbsp;
                <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control">
                </asp:DropDownList>&nbsp;
                assigned to (record owner)&nbsp;
                <asp:DropDownList ID="ddlRecordOwner" runat="server" CssClass="form-control" style="max-width:190px;">
                </asp:DropDownList>
                    </div>
                    <div class="col-md-12">
                        <input type="checkbox" runat="server" id="cbCreateRecord" onclick="javascript: CreateRecord();" />&nbsp;
                <asp:LinkButton runat="server" ID="lbCreateRecord">ONLY create record when the following rule applies</asp:LinkButton>
                    </div>
                    <div class="col-md-12">
                        <input type="checkbox" runat="server" id="cbRedirect" />
                On Registration submission, do not proceed to the survey / questionnaire after registration
                submission. Instead redirect to
                <asp:TextBox ID="txtSurveyRedirect" runat="server" CssClass="form-control" AutoPostBack="False" MaxLength="225" ClientIDMode="Static"></asp:TextBox>
                    </div>
                    <div class="col-md-12">
                        <input type="checkbox" runat="server" id="cbPostRegistration" />
                Require registration at end of the survey?
                    </div>
                    <div class="col-md-12">
                         <input type="checkbox" runat="server" id="cbRandomAnswers" text="&nbsp;Present Answers in random order?" />
                         <input type="checkbox" runat="server" id="cbSinglePageSurvey" />
                Show all Questions and Answers on the same page?
                    </div>
                    <div class="col-md-12" style="line-height:42px;">
                        <input type="checkbox" runat="server" id="cbEmail" />&nbsp;
                WHEN an Organization is created from the registration page, send the following email
                template&nbsp;
                <asp:DropDownList ID="ddlEmailTemplate1" runat="server" CssClass="form-control" ClientIDMode="Static">
                </asp:DropDownList>&nbsp;
                to the Email address of the Organizationís contact.
                    </div>
                    <div class="col-md-12" style="line-height:42px;">
                        <input type="checkbox" runat="server" id="cbRecordOwner" />
                AND send the following email template&nbsp;
                <asp:DropDownList ID="ddlEmailTemplate2" runat="server" style="max-width:200px" CssClass="form-control" ClientIDMode="Static">
                </asp:DropDownList>&nbsp;
                to the record owner
                    </div>
                    <div class="col-md-12">
                        <input type="checkbox" runat="server" id="cbLandingPage" onclick="javascript: LandingPage(this);" />
                Landing Page?
                    </div>
                    <div class="col-md-12" style="margin-bottom:10px;">
                     <label>  Yes:</label> <asp:TextBox ID="txtLandingPageYes" runat="server" CssClass="form-control"
                    AutoPostBack="False" Width="335" MaxLength="225" ClientIDMode="Static"></asp:TextBox>
                    </div>
                    <div class="col-md-12">
                       <label> No:</label>&nbsp;&nbsp;<asp:TextBox ID="txtLandingPageNo" runat="server" CssClass="form-control"
                    AutoPostBack="False" Width="335" MaxLength="225" ClientIDMode="Static"></asp:TextBox>
                    </div>
                </div>
              
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                 <div class="form-inline">
                    <div class="col-md-12">
                        <label>Redirection URL<span class="normal4">*</span> : </label>
                        <asp:TextBox ID="txtRedirectionURL" runat="server" CssClass="form-control" AutoPostBack="False" style="min-width:50%" MaxLength="225"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtRedirectionURL" EnableClientScript="True"
                    Display="None" InitialValue="" ErrorMessage="Please enter a Redirection URL."
                    runat="server" ID="ValidateRedirectionURL"></asp:RequiredFieldValidator>
                        </div>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnOpenSurveyQuestionnaire" Text="Open this Survey / Questionnaire"
                    CssClass="btn btn-primary" runat="Server"></asp:Button>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
         </div>
    <asp:ValidationSummary ID="ValidationSummary" runat="server" HeaderText="Please check the following value(s)"
        ShowSummary="False" ShowMessageBox="True" DisplayMode="List"></asp:ValidationSummary>
    <br>
     <div class="table-responsive">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td style="width: 150px" valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Question Editor&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td class="normal1" align="right" height="23">
                <asp:Button ID="btnSaveQuestions" Text="Save Questionnaire" CssClass="button" runat="Server"
                    Style="display: none"></asp:Button>&nbsp;<asp:Button ID="btnAddNewQuestion" Text="Add New Question"
                        CssClass="btn btn-primary" runat="Server"></asp:Button>&nbsp;<asp:Button ID="btnCancelQuestions"
                            Text="Cancel" CssClass="btn btn-danger" runat="Server" CausesValidation="False" Style="display: none">
                        </asp:Button>&nbsp;
            </td>
        </tr>
    </table>
         </div>
     <div class="table-responsive">
    <asp:Table ID="tblSurveyQuestions" runat="server" BorderWidth="1" BorderColor="black"
        GridLines="None" Width="100%">
        <asp:TableRow>
            <asp:TableCell>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <input type="hidden" runat="server" name="hdSurId" id="hdSurId">
                            <iframe id="ifSurveyQuestions" src="about:blank" width="100%" height="300" frameborder="no"
                                scrolling="auto" style="overflow: auto"></iframe>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
         </div>
    <asp:Literal ID="litClientAlertMessageScript" runat="server" EnableViewState="False"></asp:Literal>
    <script language="javascript">
        ShowSurveyQuestions(); //Call to show the questions
    </script>
</asp:Content>
