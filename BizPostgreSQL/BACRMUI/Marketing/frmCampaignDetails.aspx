<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCampaignDetails.aspx.vb"
    Inherits="BACRM.UserInterface.Marketing.frmCampaignDetails" ValidateRequest="false"
    MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Campaign Details</title>
    <script>
        $(document).ready(function () {
            InitializeValidation();
            $('#litMessage').fadeIn().delay(5000).fadeOut();
        });

        function OpenBill(a) {
            window.open('../Accounting/frmAddBill.aspx?BillID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
            return false;
        }
        function $Search(name, tag, elm) {
            var tag = tag || "*";
            var elm = elm || document;
            var elements = (tag == "*" && elm.all) ? elm.all : elm.getElementsByTagName(tag);
            var returnElements = [];
            var current;
            var length = elements.length;

            for (var i = 0; i < length; i++) {
                current = elements[i];
                if (current.id.indexOf(name) > 0) {
                    returnElements.push(current);
                }
            }
            return returnElements[0];
        };
        //        function CheckNumber(cint) {
        //            if (cint == 1) {
        //                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
        //                    window.event.keyCode = 0;
        //                }
        //            }
        //            if (cint == 2) {
        //                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
        //                    window.event.keyCode = 0;
        //                }
        //            }

        //        }
        function Save() {
            if ($Search('txtCampaignName').value == '') {
                alert("Enter Campaign Name")
                $Search('txtCampaignName').focus()
                return false;
            }
            if ($Search('txtCampaignCode') != null)
                if ($Search('txtCampaignCode').value == '') {
                    alert("Enter Campaign Code")
                    $Search('txtCampaignCode').focus()
                    return false;
                }
            if ($Search('calLaunch_txtDate').value == '') {
                alert("Select Launch Date")
                $Search('calLaunch_txtDate').focus()
                return false;
            }
            //            if ($Search('calEnd_txtDate').value == '') {
            //                alert("Select End Date")
            //                $Search('calEnd_txtDate').focus()
            //                return false;
            //            }

            if (!isDate($Search('calLaunch_txtDate').value, format)) {
                alert("Enter Valid Launch Date");
            }

            //            if (!isDate($Search('calEnd_txtDate').value, format)) {
            //                alert("Enter Valid End Date");
            //            }

            //            if (compareDates($Search('calLaunch_txtDate').value, format, $Search('calEnd_txtDate').value, format) == 1) {
            //                alert("End Date must be greater than or equal to Launch Date");
            //                return false;
            //            }
        }
        function OpenHelp() {
            window.open('../Help/Marketing.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
    <style>
        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            #dllCampaignStatus {
                width: 100%;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="col-sm-4">
                <div class="record-small-box bg-green">
                    <div class="inner">
                        <asp:Label ID="lblRecordOwner" runat="server"></asp:Label>
                    </div>
                    <asp:HyperLink ID="hplTransfer" CssClass="small-box-footer" runat="server" Visible="true" Text="Record Owner:"
                        ToolTip="Transfer Ownership">Record Owner <i class="fa fa-user"></i>
                    </asp:HyperLink>
                    <a href="#" id="hplTransferNonVis" runat="server" visible="false" class="small-box-footer">Record Owner <i class="fa fa-user"></i></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="record-small-box bg-aqua">
                    <div class="inner">
                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                    </div>
                    <a href="#" class="small-box-footer">Created By <i class="fa fa-user"></i></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="record-small-box bg-yellow">
                    <div class="inner">
                        <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                    </div>
                    <a href="#" class="small-box-footer">Last Modified By <i class="fa fa-user"></i></a>
                </div>
            </div>
        </div>
    </div>
    <table width="100%" align="center">
        <tr>
            <td>
                <div class="input-part">
                    <div class="right-input">
                        <table cellpadding="0" cellspacing="0" width="10px">
                            <tr>
                                <td class="leftBorder">
                                    <a href="#" class="print">&nbsp;</a>
                                </td>
                                <td class="leftBorder">
                                    <a href="#" class="help">&nbsp;</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td align="right">
                <table cellpadding="5">
                    <tr>
                        <td>
                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>&nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>&nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary" UseSubmitBehavior="false"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <table>
        <tr>
            <td class="normal4" align="center">
                <asp:Literal EnableViewState="false" ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Campaign Detail&nbsp;<a href="#" onclick="return OpenHelp()"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server"
    ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table width="100%" align="center">
        <tr>
            <td>
                <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
                    Skin="Default" ClickSelectedTab="True" AutoPostBack="false" SelectedIndex="0"
                    MultiPageID="radMultiPage_OppTab">
                    <Tabs>
                        <telerik:RadTab Text="&nbsp;&nbsp;Campaign Details&nbsp;&nbsp;" Value="CampaignDetails"
                            PageViewID="radPageView_CampaignDetails">
                        </telerik:RadTab>
                        <telerik:RadTab Text="&nbsp;&nbsp;Organizations&nbsp;&nbsp;" Value="Organizations"
                            PageViewID="radPageView_Organizations">
                        </telerik:RadTab>
                        <telerik:RadTab Text="&nbsp;&nbsp;Opportunities / Deals&nbsp;&nbsp;" Value="Opportunities"
                            PageViewID="radPageView_Opportunities">
                        </telerik:RadTab>
                        <telerik:RadTab Text="&nbsp;&nbsp;Cost Details&nbsp;&nbsp;" Value="CostDetails" PageViewID="radPageView_CostDetails">
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
                    <telerik:RadPageView ID="radPageView_CampaignDetails" runat="server">
                        <asp:Table ID="Table5" runat="server" BorderWidth="1" Width="100%" Height="300" BorderColor="black"
                            CssClass="aspTable" GridLines="None">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Top">
                                    <br>
                                    <center>
                                    <table style="max-width:80% !important;font-size:13px;" border="0" class="tblNoBorder table table-responsive">
                           
                                        <tr>
                                            <td width="25%"  align="right">
                                                Campaign Name
                                            </td>
                                            <td width="25%">
                                                <asp:TextBox ID="txtCampaignName" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </td> 
                                            <td width="25%"  align="right">
                                                Campaign Status
                                            </td>
                                            <td width="25%">
                                                <asp:DropDownList ID="dllCampaignStatus" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="normal1" align="right">
                                                Launch Date
                                            </td>
                                            <td>
                                                <BizCalendar:Calendar ID="calLaunch" runat="server" />
                                            </td>
                                            <td class="normal1" align="right">
                                                Campaign Type
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCampaignType" runat="server"  CssClass="form-control"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="normal1" align="right">
                                                Region
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlRegion" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </td>
                                              <td class="normal1" align="right">
                                                Cost
                                            </td>
                                            <td>
                                                
                                                <asp:Label ID="lblCost" runat="server"></asp:Label>
                                                <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="How do enter cost ? Add A bill From Accounting ->Add Bill Page and select Campaign Dropdown for line items which represents cost of campaign" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="normal1" align="right">
                                                Number Sent
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtNoSent" Text="0" runat="server" CssClass="form-control required_integer {required:false ,number:true, messages:{required:'Please provide value!',number:'provide valid value for Number Sent!'}}">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="normal1" align="right">
                                                <label for="chkMonthly" title="">
                                                    Monthly Campaign?</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkMonthly" CssClass="signup" />
                                            </td>
                                            <td class="normal1" align="right">
                                                <label for="chkActive">
                                                    Is Active?</label>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkActive" CssClass="signup" />
                                            </td>
                                        </tr>
                                        <%--<tr id="trCampaignCode" runat="server" visible="false">
                                            <td class="normal1" align="right" valign="top">
                                                Campaign Code:
                                            </td>
                                            <td class="normal1">
                                                <asp:TextBox runat="server" ID="txtCampaignCode" CssClass="signup" Width="200px">
                                                </asp:TextBox>
                                                <br />
                                                e.g. MarchCPC500
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <%--<td class="normal1" align="right" valign="top">
                                                COGS:<br />
                                                <i>(Deals Won)</i>
                                            </td>
                                            <td class="normal1" valign="top" style="width: 95px">
                                                <asp:Label ID="lblCOGS" runat="server" Text="100"></asp:Label>
                                            </td>--%>
                                            <td class="normal1" align="right" valign="top">
                                                Total Income:<br />
                                                <i>(Deals Won)</i>
                                            </td>
                                            <td class="normal1" valign="top">
                                                <asp:Label ID="lblTotalIncDealsWon" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1" align="right" valign="top">
                                                Total Income Potential:<br />
                                                <i>(Open Sales Opportunities)</i>
                                            </td>
                                            <td class="normal1" valign="top">
                                                <asp:Label ID="lblTotalIncOpenOpp" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td class="normal1" align="right" valign="top">
                                                Cost of Labor:<br />
                                                <i>(Deals Won)</i>
                                            </td>
                                            <td class="normal1" valign="top" style="width: 95px">
                                                <asp:Label ID="lblCostofLabor" runat="server"></asp:Label>
                                            </td>
                                            
                                        </tr>--%>
                                        <tr>
                                            <td class="normal1" align="right" valign="top">
                                                Cost to Mfg. a Deal:<br />
                                                <i>(Based on gross orders)</i>
                                            </td>
                                            <td class="normal1" valign="top">
                                                <asp:Label ID="lblCostMfgDeal" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1" align="right" valign="top">
                                                Return on Investment(ROI):
                                            </td>
                                            <td class="normal1" valign="top">
                                                <asp:Label ID="lblROI" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                        </center>
                                    <br>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="radPageView_Organizations" runat="server">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>No of Records </label>
                                        <div> <asp:Label ID="lblNoOfRecdComp" runat="server"></asp:Label></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Filter</label>
                                        <asp:DropDownList ID="ddlFilterCompany" AutoPostBack="True" CssClass="form-control"
                                                    runat="server">
                                                    <asp:ListItem Value="4">All</asp:ListItem>
                                                    <asp:ListItem Value="0">Leads</asp:ListItem>
                                                    <asp:ListItem Value="1">Prospects</asp:ListItem>
                                                    <asp:ListItem Value="2">Accounts</asp:ListItem>
                                                </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <div> <asp:DropDownList ID="ddlNoOfDays" runat="server" AutoPostBack="true" CssClass="form-control">
                                                    <asp:ListItem Text="Last 30 days" Value="30">
                                                    </asp:ListItem>
                                                    <asp:ListItem Text="Last 60 days" Value="60">
                                                    </asp:ListItem>
                                                    <asp:ListItem Text="Last 90 days" Value="90">
                                                    </asp:ListItem>
                                                    <asp:ListItem Text="Last 120 days" Value="120">
                                                    </asp:ListItem>
                                                    <asp:ListItem Text="Last 240 days" Value="240">
                                                    </asp:ListItem>
                                                    <asp:ListItem Text="Last 360 days" Value="360">
                                                    </asp:ListItem>
                                                </asp:DropDownList></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Table ID="Table1" CellPadding="0" CellSpacing="0" runat="server" BorderWidth="1"
                            Width="100%" CssClass="aspTable" Height="300" BorderColor="black" GridLines="None">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Top">
                                    <br>
                                    <asp:DataGrid ID="dgCompany" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False"
                                        AllowSorting="True">
                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                        <ItemStyle CssClass="is"></ItemStyle>
                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn DataField="numCompanyID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="numDivisionID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="numContactID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="tintCRMType" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="bintCreatedDate" HeaderText="Created Date"></asp:BoundColumn>
                                            <asp:ButtonColumn CommandName="Company" DataTextField="CompanyName" SortExpression="CompanyName"
                                                HeaderText="Customer"></asp:ButtonColumn>
                                            <asp:BoundColumn DataField="Follow" SortExpression="Follow" HeaderText="Follow-Up Status"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="numNoOfEmployeesId" HeaderText="Employees"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="OrganizationComments" SortExpression="OrganizationComments"
                                                HeaderText="Organization Comments"></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="radPageView_Opportunities" runat="server">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>No of Records </label>
                                        <div> <asp:Label ID="lblNoOfRecdOpp" runat="server"></asp:Label></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Filter</label>
                                      <asp:DropDownList ID="ddlFilterOpp" CssClass="form-control" AutoPostBack="True" runat="server">
                                                    <asp:ListItem Value="0">All</asp:ListItem>
                                                    <asp:ListItem Value="1">Sales Opportunities</asp:ListItem>
                                                    <asp:ListItem Value="2">Purchase Opportunities</asp:ListItem>
                                                    <asp:ListItem Value="3">Sales Deal Won/Completed</asp:ListItem>
                                                    <asp:ListItem Value="4">Purchase Deal Won/Completed</asp:ListItem>
                                                    <asp:ListItem Value="5">Sales Deal Lost</asp:ListItem>
                                                    <asp:ListItem Value="6">Purchase Deal Lost</asp:ListItem>
                                                </asp:DropDownList>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <asp:Table ID="Table2" runat="server" CellPadding="0" CellSpacing="0" BorderWidth="1"
                            Width="100%" CssClass="aspTable" Height="300" GridLines="None">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Top">
                                    <br>
                                    <asp:DataGrid ID="dgOpportunity" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False"
                                        AllowSorting="True">
                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                        <ItemStyle CssClass="is"></ItemStyle>
                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn DataField="numCompanyID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="numDivisionID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="numContactID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="tintCRMType" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="numOppID" Visible="false"></asp:BoundColumn>
                                            <asp:ButtonColumn CommandName="Name" DataTextField="Name" SortExpression="Name" HeaderText="Opportunity / Deal Name"></asp:ButtonColumn>
                                            <asp:ButtonColumn CommandName="Company" DataTextField="Company" SortExpression="Company"
                                                HeaderText="Customer,Division"></asp:ButtonColumn>
                                            <asp:BoundColumn DataField="Milestone" SortExpression="Milestone" HeaderText="Total Progress (%)"
                                                ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                            <%--<asp:BoundColumn DataField="Stage" SortExpression="Stage" HeaderText="Last Stage Completed, Date Stage Completed">
                                                    </asp:BoundColumn>--%>
                                            <asp:TemplateColumn HeaderText="Amount" SortExpression="monPAmount">
                                                <ItemTemplate>
                                                    <%# ReturnMoney(DataBinder.Eval(Container.DataItem, "monPAmount")) %>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="radPageView_CostDetails" runat="server">
                        <table width="100%" cellpadding="2" cellspacing="2">
                            <tr>
                                <td>
                                    <asp:DataGrid ID="dgCostDetails" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False"
                                        AllowSorting="True">
                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                        <ItemStyle CssClass="is"></ItemStyle>
                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Chart Of Account" SortExpression="vcAccountName">
                                                <ItemTemplate>
                                                    <%--<a id="hrfCOA" class="text_bold" href="javascript:OpenBill(" <%# DataBinder.Eval(Container.DataItem, "numBillID") %> ");" style="margin-right:2px"> </a>--%>
                                                    <asp:HyperLink ID="hrfOpenBill" runat="server" Style="margin-right: 2px" Text='<%# DataBinder.Eval(Container.DataItem, "vcAccountName") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Cost" SortExpression="monAmount">
                                                <ItemTemplate>
                                                    <%# ReturnMoney(DataBinder.Eval(Container.DataItem, "monAmount")) %>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="vcClassName" HeaderText="Class" SortExpression="vcClassName"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="vcDescription" HeaderText="Description" SortExpression="vcDescription"></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
