''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Survey
''' Class	 : frmCampaignSurveyAnswers
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a interface for editing a Survey answers for a question
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	09/15/2005	Created
''' </history>
''' ''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Survey
    Public Class frmCampaignSurveyAnswers
        Inherits BACRMPage

        Protected WithEvents btnSaveAnswer As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveAndClose As System.Web.UI.WebControls.Button
        Protected WithEvents btnCloseAnswerWindow As System.Web.UI.WebControls.Button
        Protected WithEvents rdbResponseTypes As System.Web.UI.WebControls.RadioButtonList
        Protected WithEvents btnAOIs As System.Web.UI.WebControls.Button
        Protected WithEvents ddlNumberOfResponse As System.Web.UI.WebControls.DropDownList
        Protected WithEvents tblSurveyEdited As System.Web.UI.WebControls.Table
        Protected WithEvents hdQId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdAction As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdSurId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents litClientMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents hdAnsId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents tblSurveyQuestionAnswer As System.Web.UI.WebControls.Table
        Protected WithEvents ddlAvailableAOIs As System.Web.UI.WebControls.ListBox
        Protected WithEvents ddlSelectedAOIs As System.Web.UI.WebControls.ListBox
        Protected WithEvents ValidationSummary As System.Web.UI.WebControls.ValidationSummary
        Protected WithEvents litMaxAOIs As System.Web.UI.WebControls.Literal

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime the page is called. In this event we will 
        '''     get the data from the DB and populate the Tables and the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                Dim numQuestionId As Integer = GetQueryStringVal("numQuestionId") 'Get the Question Id in local variable
                hdQId.Value = numQuestionId                                         'Set the Question Id in hidden
                Dim numSurId As Integer = GetQueryStringVal("numSurId")           'Get the Survey Id in local variable
                hdSurId.Value = numSurId   'Set the Survey Id in hidden
                ' = "MarSurvey"
                If hdAction.Value <> "Save" Then
                    Dim objSurvey As New SurveyAdministration                       'Create an object of Survey Administration Class
                    objSurvey.DomainID = Session("DomainID")                        'Set the Domain ID
                    objSurvey.UserId = Session("UserID")                            'Set the User ID
                    objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
                    objSurvey.SurveyId = hdSurId.Value                              'Set the Survey ID
                    objSurvey.QuestionId = numQuestionId                            'Set the question id
                    objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()     'Call to retrieve the existing data temporarily
                    If Not IsPostBack Then DisplayResponseTypeDetails(objSurvey) 'Call to dispaly the response type details
                    Dim dtSurverQuestionAnswers As DataTable                        'Declare a Datatable object
                    dtSurverQuestionAnswers = objSurvey.GetAnswersForSurveyQuestion() 'Get the answers for the selected question
                    DisplayAnswers(dtSurverQuestionAnswers)                         'Display the answers
                    BindAOIList(objSurvey)                                          'Call to bind the AOIs

                    If chkMatrix.Checked Then
                        Dim numSurveyAnwerMasterRowIndex As Integer = 1

                        While numSurveyAnwerMasterRowIndex <= ddlNumberOfResponse.SelectedValue
                            CType(tblSurveyQuestionAnswer.FindControl("btnRule" & numSurveyAnwerMasterRowIndex), Button).Enabled = False
                            numSurveyAnwerMasterRowIndex += 1
                        End While
                    Else
                        Dim numSurveyAnwerMasterRowIndex As Integer = 1

                        While numSurveyAnwerMasterRowIndex <= ddlNumberOfResponse.SelectedValue
                            CType(tblSurveyQuestionAnswer.FindControl("btnRule" & numSurveyAnwerMasterRowIndex), Button).Enabled = True
                            numSurveyAnwerMasterRowIndex += 1
                        End While
                    End If
                End If
                PostInitializeControlsClientEvents()                                'Call to attache the client events
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method creates a rows of question for the Survey
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub DisplayAnswers(ByVal dtAnswerContent As DataTable)
            Try
                tblSurveyQuestionAnswer.Rows.Clear()                                            'Clear all rows
                tblSurveyQuestionAnswer.Controls.Clear()                                        'clear all controls
                Dim tblRowDisplayAnswer As TableRow                                             'Declare a Table Row object
                Dim tblCellOneDisplayAnswer, tblCellTwoDisplayAnswer, tblCellThreeDisplayAnswer As TableCell 'Declare Table Cells object

                Dim numAnswerIndex As Integer                                                   'Declare the row index
                Dim numAnsID As Integer                                                         'Declare the variable to store teh Rule Id and Answer Index
                Dim boolSurveyRuleAttached As Boolean                                           'Variable to store a falag indicating if a Rule is associated
                Dim vcAnsLabel As String                                                        'Declare the variable to store the amswer
                For numAnswerIndex = 0 To ddlNumberOfResponse.SelectedValue - 1
                    If numAnswerIndex < dtAnswerContent.Rows.Count Then
                        boolSurveyRuleAttached = dtAnswerContent.Rows(numAnswerIndex).Item("boolSurveyRuleAttached") 'Store the Rule associated with the answer
                        vcAnsLabel = dtAnswerContent.Rows(numAnswerIndex).Item("vcAnsLabel")    'Store the answer
                        numAnsID = dtAnswerContent.Rows(numAnswerIndex).Item("numAnsID")        'Store teh answer id
                    Else
                        boolSurveyRuleAttached = 0
                        numAnsID = numAnswerIndex + 1
                        vcAnsLabel = ""
                    End If
                    tblRowDisplayAnswer = New TableRow                                          'Instantiate a new table row everytime
                    tblRowDisplayAnswer.EnableViewState = False                                 'Not to have a view state for the row
                    tblRowDisplayAnswer.Attributes.Add("style", "display:inline;")
                    tblRowDisplayAnswer.ID = "tRow" & numAnswerIndex + 1                        'Generate Unique ros Ids

                    tblCellOneDisplayAnswer = New TableCell                                     'Create an object of table cell
                    tblCellOneDisplayAnswer.HorizontalAlign = HorizontalAlign.Right             'Align the label to right an top
                    tblCellOneDisplayAnswer.VerticalAlign = VerticalAlign.Top
                    tblCellOneDisplayAnswer.CssClass = "normal1"                                'Add the class attribute
                    tblCellOneDisplayAnswer.Text = rdbResponseTypes.SelectedItem.Text & " " & numAnswerIndex + 1 & " Label <span class='normal4'>*</span> : " 'Display the label

                    tblCellTwoDisplayAnswer = New TableCell                                     'Create an object of table cell
                    tblCellTwoDisplayAnswer.CssClass = "normal1"                                'Add the class attribute
                    Dim oTextBox As New TextBox                                                 'Instantiate a TextBox
                    oTextBox.ID = "txtAnswer" & numAnswerIndex + 1                              'set the name of the textbox"
                    oTextBox.Width = Unit.Pixel(380)                                            'set the width of the textbox
                    oTextBox.TextMode = TextBoxMode.SingleLine                                  'This is a Single textarea
                    oTextBox.CssClass = "signup"                                                'Set the class attribute for the textbox control
                    oTextBox.MaxLength = 100                                                    'Max 100 characters allowed
                    oTextBox.Text = ReSetFromXML(vcAnsLabel)                                    'Set the answer for display
                    oTextBox.EnableViewState = False                                            'Text box will not have a view state
                    If rdbResponseTypes.SelectedValue = 3 Then                                  'AOI are selected. here AOIs are to be selected from the  listbox
                        oTextBox.ReadOnly = True                                                'Do not allow typing on these edit boxes
                    Else : oTextBox.ReadOnly = False                                               'Typing is allowed on these edit boxes
                    End If
                    tblCellTwoDisplayAnswer.Controls.Add(oTextBox)                              'Add the textbox to the table cell
                    Dim valReqControl As New RequiredFieldValidator                             'Declare a required field validator
                    valReqControl.ID = "vReq" & numAnswerIndex + 1                              'Giving id to the validator control
                    valReqControl.ControlToValidate = "txtAnswer" & numAnswerIndex + 1          'Attach the control to validate
                    valReqControl.EnableClientScript = True                                     'Enable Client script
                    valReqControl.InitialValue = ""                                             'Set the Initial value so that it also validates drop down
                    valReqControl.ErrorMessage = "Please enter " & "Answer " & numAnswerIndex + 1 & "." 'Specify the error message to be displayed
                    valReqControl.Display = ValidatorDisplay.None                               'Error message never displays inline
                    tblCellTwoDisplayAnswer.Controls.Add(valReqControl)                         'Add the validator to the table cell

                    tblCellThreeDisplayAnswer = New TableCell                                   'Create an object of table cell
                    tblCellThreeDisplayAnswer.CssClass = "normal1"                              'Add the class attribute
                    tblCellThreeDisplayAnswer.VerticalAlign = VerticalAlign.Top                 'align the buttons etc to the top
                    Dim oAddEditAnsRuleButton As New Button                                     'Instantiate a button
                    If boolSurveyRuleAttached <> 0 Then
                        oAddEditAnsRuleButton.Text = "Edit Rule"                                'Set the lable to the button
                    Else : oAddEditAnsRuleButton.Text = "Add Rule"                                 'Set the lable to the button
                    End If
                    oAddEditAnsRuleButton.CssClass = "button"                                   'Set the class attribute
                    oAddEditAnsRuleButton.ID = "btnRule" & numAnsID
                    If Trim(vcAnsLabel) = "" Then
                        oAddEditAnsRuleButton.Attributes.Add("onclick", "javascript: return AddEditAnswerRule(" & numAnsID & ",'New',0);") 'Add script to enable adding/ editing an Rule
                    Else : oAddEditAnsRuleButton.Attributes.Add("onclick", "javascript: return AddEditAnswerRule(" & numAnsID & ",'Edit',0);") 'Add script to enable adding/ editing an Rule
                    End If
                    tblCellThreeDisplayAnswer.Controls.Add(oAddEditAnsRuleButton)               'Add the Add Edit button to the cell
                    tblRowDisplayAnswer.Cells.Add(tblCellOneDisplayAnswer)                      'Add the lable cell specify to question number
                    tblRowDisplayAnswer.Cells.Add(tblCellTwoDisplayAnswer)                      'Add the lable cell specify to question textbox
                    tblRowDisplayAnswer.Cells.Add(tblCellThreeDisplayAnswer)                    'Add the lable cell specify to buttons
                    tblSurveyQuestionAnswer.Rows.Add(tblRowDisplayAnswer)                       'Add the row to the table
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the response Type details
        ''' </summary>
        ''' <remarks>
        '''     Returns a datatable of answers for the selected question
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function DisplayResponseTypeDetails(ByRef objSurvey As SurveyAdministration) As DataTable
            Try
                Dim dtSurveyQuestion As DataTable = objSurvey.GetResponseTypeDetailsForSurveyQuestion()                     'Call for the Datarelated to the question
                rdbResponseTypes.ClearSelection()                                                                           'clear selection if any
                If Not rdbResponseTypes.Items.FindByValue(dtSurveyQuestion.Rows(0).Item("tIntAnsType")) Is Nothing Then     'Check if the text exists in the dropdown of Response Types
                    rdbResponseTypes.Items.FindByValue(dtSurveyQuestion.Rows(0).Item("tIntAnsType")).Selected = True        'show the appropriate item as selected for the Response Types
                Else : rdbResponseTypes.Items.FindByValue(0).Selected = True                                                   'show the checkbozxes as selected
                End If

                ddlNumberOfResponse.ClearSelection()                                                                        'clear selection if any
                If Not ddlNumberOfResponse.Items.FindByValue(dtSurveyQuestion.Rows(0).Item("intNumOfAns")) Is Nothing Then  'Check if the text exists in the dropdown of nos of response
                    ddlNumberOfResponse.Items.FindByValue(dtSurveyQuestion.Rows(0).Item("intNumOfAns")).Selected = True     'show the appropriate item as selected for the nos of response
                Else : ddlNumberOfResponse.Items.FindByValue(4).Selected = True                                                'show the 8 as selected
                End If

                If dtSurveyQuestion.Columns("boolMatrixColumn") IsNot Nothing Then

                    If Not dtSurveyQuestion.Rows(0).Item("boolMatrixColumn") Is Nothing Then
                        If Not IsDBNull(dtSurveyQuestion.Rows(0).Item("boolMatrixColumn")) Then
                            If dtSurveyQuestion.Rows(0).Item("boolMatrixColumn") = "1" Then
                                chkMatrix.Checked = True
                            Else
                                chkMatrix.Checked = False
                            End If
                        End If
                    Else : chkMatrix.Checked = False
                    End If

                    ddMatrix.ClearSelection()
                    If Not IsDBNull(dtSurveyQuestion.Rows(0).Item("intNumOfMatrixColumn")) Then
                        If Not ddMatrix.Items.FindByValue(dtSurveyQuestion.Rows(0).Item("intNumOfMatrixColumn")) Is Nothing Then
                            ddMatrix.Items.FindByValue(dtSurveyQuestion.Rows(0).Item("intNumOfMatrixColumn")).Selected = True
                        Else : ddMatrix.Items.FindByValue(2).Selected = True
                        End If
                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the response Type details
        ''' </summary>
        ''' <param name="objSurvey">The object containing the Survey Information</param>
        ''' <remarks>
        '''     Returns a datatable of answers for the selected question
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function BindAOIList(ByRef objSurvey As SurveyAdministration)
            Try
                If rdbResponseTypes.SelectedValue = 3 Then                              'AOI are selected
                    btnAOIs.Enabled = True                                              'Enable the AOI button
                    Dim dtAvailableAOIList As DataTable                                 'Declare a DataTable
                    dtAvailableAOIList = objSurvey.getAOIList()                         'call function to get the AOI's
                    Dim dsTempDataSet As New DataSet                                    'Create a new dataset
                    Dim dtSelectedAOIList As DataTable                                  'Declare a DataTable
                    dtSelectedAOIList = objSurvey.GetAnswersForSurveyQuestion()         'copy the data
                    dsTempDataSet.Tables.Add(dtSelectedAOIList)                         'Add the datatable
                    dsTempDataSet.Tables.Add(dtAvailableAOIList.Copy)                   'Create a copy of the table

                    ddlAvailableAOIs.DataSource = RemoveDuplicatesRows(dsTempDataSet.Tables(1), "vcAOIName", dsTempDataSet.Tables(0), "vcAnsLabel") 'Set the datasource for the AOI's
                    ddlAvailableAOIs.DataTextField = "vcAOIName"                        'set the textfield
                    ddlAvailableAOIs.DataValueField = "numAOIId"                        'set the value attribute
                    ddlAvailableAOIs.DataBind()

                    ddlSelectedAOIs.DataSource = dtSelectedAOIList                      'Set the datasource for the AOI's
                    ddlSelectedAOIs.DataTextField = "vcAnsLabel"                        'set the textfield
                    ddlSelectedAOIs.DataValueField = "vcAnsLabel"                       'set the value attribute
                    ddlSelectedAOIs.DataBind()
                    litMaxAOIs.Text = ddlNumberOfResponse.SelectedItem.Text             'show the nos of allowed selections
                Else : btnAOIs.Enabled = False                                         'Disable the AOI button
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        '''<summary>
        '''     Removes duplicate rows from given DataTable
        '''</summary>
        '''<param name="tblOne">Table to scan for duplicate rows</param>
        '''<param name="tblTwo">Reference table for matchign duplicate rows</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function RemoveDuplicatesRows(ByVal tblOne As DataTable, ByVal keyColumnTblOne As String, ByRef tblTwo As DataTable, ByVal keyColumnTblTwo As String) As DataTable
            Try
                Dim intTblOneRowIndex As Integer                                                                            'Declare a row index
                Dim intTblOneRowCount As Integer = tblOne.Rows.Count - 1                                                    'Declare a row index
                While intTblOneRowIndex <= intTblOneRowCount                                                              'loop through the rows in child table
                    If tblTwo.Select(keyColumnTblTwo & " = '" & tblOne.Rows(intTblOneRowIndex).Item(keyColumnTblOne) & "'").Length > 0 Then
                        tblOne.Rows.RemoveAt(intTblOneRowIndex)                                                             'Remove the Parent Row
                        intTblOneRowCount -= 1                                                                              'Decrement the row count
                    End If
                    intTblOneRowIndex += 1
                End While
                Return tblOne
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the answers temporarily after the Save button is clicked
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Private Sub btnSaveAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAnswer.Click, btnSaveAndClose.Click
            Try
                Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
                objSurvey.DomainID = Session("DomainID")                            'Set the Domain ID
                objSurvey.UserId = Session("UserID")                                'Set the User ID
                objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
                objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID
                objSurvey.QuestionId = hdQId.Value                                  'Set the question id

                objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()         'Call to retrieve the existing data temporarily
                UpdateAnswers(objSurvey)                                            'Update the rows in the answer table
                objSurvey.SaveSurveyInformationTemp()                               'Call to save the existing data temporarily
                Dim btnSrcButton As Button = CType(sender, Button)                  'typecast to button
                If btnSrcButton.ID.ToString = "btnSaveAnswer" Then                  'check the source of the button
                    Dim dtSurverQuestionAnswers As DataTable                            'Declare a Datatable object
                    dtSurverQuestionAnswers = objSurvey.GetAnswersForSurveyQuestion()   'Get the answers for the selected question
                    DisplayAnswers(dtSurverQuestionAnswers)                             'Display the answers
                    BindAOIList(objSurvey)                                              'Call to bind the AOIs
                    If hdAction.Value = "SaveB4AddingAnswers" Then                      'check the action reque3sted, If it is save data before opening answers window
                        litClientMessage.Text = "<script language=javascript>AddEditAnswerRule(" & hdAnsId.Value & ",'Edit',0);</script>" 'clsoe this window
                    End If
                    hdAnsId.Value = ""                                              'Reset teh Ans Id value
                Else : litClientMessage.Text = "<script language=javascript>CloseAnswerWin();</script>" 'clsoe this window
                End If
                hdAction.Value = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to sync the dataset of Survey Information with the latest changes
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Function UpdateAnswers(ByRef objSurvey As SurveyAdministration)
            Try
                Dim dtSurveyAnswerMaster As DataTable
                dtSurveyAnswerMaster = objSurvey.SurveyInfo.Tables("SurveyAnsMaster")
                Dim numSurveyAnwerMasterTableRowIndex As Integer                    'Declare a index for datatable rows
                Dim numSurveyAnwerMasterRowIndex As Integer = 1                     'Declare a index for htmltable rows
                While numSurveyAnwerMasterTableRowIndex < dtSurveyAnswerMaster.Rows.Count
                    If dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numQID") = hdQId.Value Then
                        If (Request.Form("ctl00$Content$txtAnswer" & numSurveyAnwerMasterRowIndex) <> "") Then
                            dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numSurID") = hdSurId.Value 'set the Survey ID
                            dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numAnsID") = numSurveyAnwerMasterRowIndex 'set the ans id
                            dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("vcAnsLabel") = MakeSafeForXML(Request.Form("ctl00$Content$txtAnswer" & numSurveyAnwerMasterRowIndex))  'Capture the answer
                            dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("boolDeleted") = 0         'Sync the deleted flag

                            numSurveyAnwerMasterRowIndex += 1                       'increment the htmltable row counter
                        Else
                            objSurvey.DeleteSurveyWorkFlowTemp(hdQId.Value, numSurveyAnwerMasterRowIndex)  'Request deletion of rules
                            dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Delete() 'Delete the answer row
                            numSurveyAnwerMasterTableRowIndex -= 1                  'since row is deleted so decrement the counter
                        End If
                    End If
                    numSurveyAnwerMasterTableRowIndex += 1                          'increment the datatable row counter
                End While
                Dim drSurveyAnswerMasterRow As DataRow                              'Declare a DataRow
                While numSurveyAnwerMasterRowIndex <= ddlNumberOfResponse.SelectedValue
                    drSurveyAnswerMasterRow = dtSurveyAnswerMaster.NewRow()         'Get an new row of the table
                    drSurveyAnswerMasterRow.Item("numSurID") = hdSurId.Value        'set the Survey ID
                    drSurveyAnswerMasterRow.Item("numQID") = hdQId.Value            'Insert the new question id for the answer
                    drSurveyAnswerMasterRow.Item("numAnsID") = numSurveyAnwerMasterRowIndex 'Insert the new answer id for the answer
                    drSurveyAnswerMasterRow.Item("vcAnsLabel") = MakeSafeForXML(Request.Form("ctl00$Content$txtAnswer" & numSurveyAnwerMasterRowIndex)) 'Insert the new answer
                    drSurveyAnswerMasterRow.Item("boolSurveyRuleAttached") = 0      'no rule for these rows
                    drSurveyAnswerMasterRow.Item("boolDeleted") = 0                 'Set the flag indicating that it is not deleted

                    dtSurveyAnswerMaster.Rows.Add(drSurveyAnswerMasterRow)          'Add the question to the table
                    numSurveyAnwerMasterRowIndex += 1                               'increment the datatable row counter
                End While

                Dim dtSurveyQuestion As DataTable                                                               'Declare the datatable
                dtSurveyQuestion = objSurvey.SurveyInfo.Tables("SurveyQuestionMaster")                          'Get the DataTable Data
                Dim drSurveyQuestion As DataRow                                                                 'Declare a DataRow
                Dim iQuestionRowIndex As Integer                                                                'Declare a row index
                For iQuestionRowIndex = 0 To dtSurveyQuestion.Rows.Count - 1                                    'loop through the rows that match the criteria
                    If dtSurveyQuestion.Rows(iQuestionRowIndex).Item("numQID") = hdQId.Value Then
                        dtSurveyQuestion.Rows(iQuestionRowIndex).Item("tIntAnsType") = rdbResponseTypes.SelectedValue 'Marks the answer response types
                        dtSurveyQuestion.Rows(iQuestionRowIndex).Item("intNumOfAns") = ddlNumberOfResponse.SelectedValue 'Stores teh number of answers

                        If chkMatrix.Checked Then
                            dtSurveyQuestion.Rows(iQuestionRowIndex).Item("boolMatrixColumn") = 1
                        Else
                            dtSurveyQuestion.Rows(iQuestionRowIndex).Item("boolMatrixColumn") = 0
                        End If
                        Exit For
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to attach client side events to the controls
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub PostInitializeControlsClientEvents()
            Try
                btnCloseAnswerWindow.Attributes.Add("onclick", "javascript: return CloseAnswerWin();")     'calls for closing the answer window
                btnSaveAnswer.Attributes.Add("onclick", "javascript: SaveAnswers();")               'calls for saving the answer
                btnSaveAndClose.Attributes.Add("onclick", "javascript: SaveAnswers();")             'calls for saving the answer and then closing the window
                btnAOIs.Attributes.Add("onclick", "javascript: return AllowSelectionOfAOIs();")            'calls for allowing selection of AOIs
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is safe casts the string for XML
        ''' </summary>
        ''' <param name = "sValue">The string which is to be made XML safe</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function MakeSafeForXML(ByVal sValue As String) As String
            Try
                Return Replace(Replace(Replace(Replace(Replace(sValue, "'", "&#39;"), """", "&#39;&#39;"), "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is reverts the string to its former html display format
        ''' </summary>
        ''' <param name = "sValue">The string which is to be made XML safe</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function ReSetFromXML(ByVal sValue As String) As String
            Try
                Return Replace(Replace(Replace(Replace(sValue, "&amp;", "&"), "&#39;", "'"), "&lt;", "<"), "&gt;", ">")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub chkMatrix_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMatrix.CheckedChanged
            Try

                If chkMatrix.Checked Then
                    Dim numSurveyAnwerMasterRowIndex As Integer = 1

                    While numSurveyAnwerMasterRowIndex <= ddlNumberOfResponse.SelectedValue
                        CType(tblSurveyQuestionAnswer.FindControl("btnRule" & numSurveyAnwerMasterRowIndex), Button).Enabled = False
                        numSurveyAnwerMasterRowIndex += 1
                    End While
                Else
                    Dim numSurveyAnwerMasterRowIndex As Integer = 1

                    While numSurveyAnwerMasterRowIndex <= ddlNumberOfResponse.SelectedValue
                        CType(tblSurveyQuestionAnswer.FindControl("btnRule" & numSurveyAnwerMasterRowIndex), Button).Enabled = True
                        numSurveyAnwerMasterRowIndex += 1
                    End While
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lbMatrix_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbMatrix.Click
            If rdbResponseTypes.SelectedValue = 0 Or rdbResponseTypes.SelectedValue = 1 Then
                Dim str As String
                str = String.Format("<script>window.open('frmCampaignSurveyMatrixColumns.aspx?numQuestionId={0}&numSurId={1}&numMatrix={2}','SurveyMatrixColumn','toolbar=no,titlebar=no,left=100, top=100,width=700,height=600,scrollbars=yes,resizable=yes')</script>", GetQueryStringVal("numQuestionId"), GetQueryStringVal("numSurId"), ddMatrix.SelectedValue)
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "MatrixColumn", str)
            End If
        End Sub
    End Class
End Namespace