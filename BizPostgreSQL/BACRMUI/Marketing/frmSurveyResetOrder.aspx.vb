﻿Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Partial Public Class frmSurveyResetOrder
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim numSurId As Integer                                                         'Declare the integer for the Survey Id
        numSurId = GetQueryStringVal( "numSurId")
        Dim numDomainId As Integer
        numDomainId = GetQueryStringVal( "numDomainId")
        Dim objSurvey As New SurveyAdministration
        objSurvey.DomainId = Session("DomainID")                        'Set the Domain ID
        objSurvey.UserId = Session("UserID")                            'Set the User ID
        objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
        objSurvey.SurveyId = numSurId                           'Set the Survey ID
        'objSurvey.AnswerId = numAnsID                                   'Set the Answer id
        'objSurvey.QuestionId = numQuestionId                            'Set the Question id

        objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()     'Call to retrieve the existing data temporarily
        Dim dtQuestionnaireContent As DataTable                                     'Declare a new datatable
        dtQuestionnaireContent = objSurvey.SurveyInfo.Tables("SurveyQuestionMaster") 'Get the DataTable
        If Not IsPostBack Then
            reorderList.DataSource = dtQuestionnaireContent
            reorderList.SortOrderField = "numQId"
            reorderList.DataBind()
        End If
        


    End Sub

End Class



