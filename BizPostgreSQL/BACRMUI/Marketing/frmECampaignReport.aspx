﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmECampaignReport.aspx.vb"
    Inherits=".frmECampaignReport" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Drip Campaign Tracker</title>
    <style type="text/css">
        .style1
        {
            width: 711px;
        }
        .style2
        {
            width: 214px;
        }
        .style3
        {
            font-family: Tahoma, Verdana, Arial, Helvetica;
            font-size: 8pt;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            color: #000000;
            width: 139px;
        }
         .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            #dllCampaignStatus {
                width: 100%;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
        .table {
        border-color:#e1e1e1
        }
            .table > tbody > tr:first-child {
            background:#e1e1e1;
            font-weight:bold;
            }
    </style>
    <script type="text/javascript">
        //        function SelectAll(a) {
        //            var str;

        //            if (typeof (a) == 'string') {
        //                if (document.all) {
        //                    a = document.all[a];
        //                }
        //                else {
        //                    a = document.getElementById(a);
        //                }
        //            }
        //            var dgCampaigns = document.getElementById('dgCampaigns');
        //            if (a.checked == true) {
        //                for (var i = 1; i <= dgCampaigns.rows.length; i++) {
        //                    if (i < 10) {
        //                        str = '0' + i
        //                    }
        //                    else {
        //                        str = i
        //                    }
        //                    if (document.all) {
        //                        document.all('dgCampaigns_ctl' + str + '_chk').checked = true;
        //                    }
        //                    else {
        //                        document.getElementById('dgCampaigns_ctl' + str + '_chk').checked = true;
        //                    }

        //                }
        //            }
        //            else if (a.checked == false) {
        //                for (var i = 1; i <= dgCampaigns.rows.length; i++) {
        //                    if (i < 10) {
        //                        str = '0' + i
        //                    }
        //                    else {
        //                        str = i
        //                    }
        //                    if (document.all) {
        //                        document.all('dgCampaigns_ctl' + str + '_chk').checked = false;
        //                    }
        //                    else {
        //                        document.getElementById('dgCampaigns_ctl' + str + '_chk').checked = false;
        //                    }

        //                }
        //            }
        //        }
        function openDrip(a) {
            window.open("../Marketing/frmConECampHstr.aspx?ConECampID=" + a, '', 'toolbar=no,titlebar=no,top=250,left=250,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
        function OpenContact(a) {
            str = "../contact/frmContacts.aspx?frm=ECampReport&CntId=" + a;
            document.location.href = str;
        }
        function OpenHelp() {
            window.open('../Help/Marketing.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="pull-left">
        <div class="form-inline">
            <label>Select Campaign</label>
            <asp:DropDownList ID="ddlCampaign" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="pull-right">
        <div class="form-group">
            <div><asp:LinkButton ID="btnRemove" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Disengage from Drip Campaign</asp:LinkButton></div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Active Drip Campaigns&nbsp;<a href="#" onclick="return OpenHelp()"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="tbl" runat="server" Width="100%" Height="350" GridLines="None" CssClass="aspTable"
        CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgCampaigns" runat="server" AllowSorting="false" CssClass="table table-responsive"
                    AllowCustomPaging="true" AutoGenerateColumns="False" DataKeyField="numContactID"
                    Width="100%">
                    <%--<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>--%>
                  <%--  <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>--%>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numECampaignId"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CompanyName" HeaderText="Organization" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Contact Name" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <a href="#" onclick="OpenContact(<%# Eval("numContactID") %>);">
                                    <%# Eval("ContactName") %></a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcECampName" SortExpression="vcECampName" HeaderText="Drip Campaign" HeaderStyle-HorizontalAlign="Center">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="No Of Stages Completed" ItemStyle-Width="100" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                               <%# Eval("NoOfStagesCompleted")%> Out Of <%# Eval("Stage")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%-- <asp:BoundColumn DataField="Template" HeaderText="Last Event" HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
                         <asp:TemplateColumn HeaderText="Executed on / Success or Failure?" ItemStyle-Width="120" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# If(Eval("dtSentDate") = "", "", Eval("dtSentDate") & " / ")%>
                                <asp:Label runat="server" Text='<%# Eval("SendStatus")%>' ForeColor='<%# IIf(Eval("SendStatus") = "Success", System.Drawing.Color.Green, IIf(Eval("SendStatus") = "Failed", System.Drawing.Color.Red, System.Drawing.Color.SaddleBrown))%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="dtNextStage" HeaderText="Next Execution On" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                      <asp:BoundColumn DataField="NoOfEmailSent" HeaderText="No of Eamil Sent" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="NoOfEmailOpened" HeaderText="No of Eamil Opened" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>--%>
                        <asp:TemplateColumn HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80">
                            <ItemTemplate>
                                <a href="#" onclick="openDrip(<%# Eval("numConEmailCampID") %>);">View Detail</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chk" runat="server" onclick="SelectAll('chk','chk1')" />
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chk" CssClass="chk1" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
