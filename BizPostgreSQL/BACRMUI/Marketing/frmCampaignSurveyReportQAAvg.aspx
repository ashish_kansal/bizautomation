<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCampaignSurveyReportQAAvg.aspx.vb"
    Inherits="BACRM.UserInterface.Survey.frmCampaignSurveyReportQAAvg" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Q & A Averages</title>
    <script language="javascript" src="../javascript/Surveys.js"></script>
         <style>
         .table {
        border-color:#CACACA;
        }
            .table > tbody > tr:first-child {
            background:rgba(225, 225, 225, 0.23);
            font-weight:bold;
            }
             .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <table id="tblMainHeader" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td class="normal1" align="left">
                <label> Graph Type:</label>
                <asp:DropDownList ID="ddlGraphType" CssClass="form-control" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="pie">Pie Charts</asp:ListItem>
                    <asp:ListItem Value="bar">Bar Graphs</asp:ListItem>
                    <asp:ListItem Value="column">Column</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="normal1" align="left">
                <label>Chart Color:</label>
                <asp:DropDownList ID="ddlChartColor" CssClass="form-control" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="False">Live Colors</asp:ListItem>
                    <asp:ListItem Value="True">Print Colors</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="normal1" align="right" height="23">
                &nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="btnExportToExcel" runat="server" CssClass="btn btn-success" CausesValidation="False"><i class="fa fa-file-excel-o"></i>&nbsp;Export to Exce</asp:LinkButton>
                &nbsp;
                <asp:LinkButton ID="btnPrint" CausesValidation="False" CssClass="btn btn-primary" runat="server"><i class="fa fa-print"></i>&nbsp;Print</asp:LinkButton>
                &nbsp;&nbsp;
                <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" CausesValidation="False" runat="server"><i class="fa fa-backward"></i>&nbsp;Back</asp:LinkButton>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Q &amp; A Averages
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div id="dvSurveyQAAvg">
        <asp:Table ID="tblSurveyQAAvg" CellPadding="0" CellSpacing="1" BorderWidth="1" runat="server"
            Width="100%" BorderColor="Black" GridLines="None" CssClass="table table-responsive">
            <asp:TableRow Height="22px">
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                    &nbsp;
                    <asp:Label ID="lblSurNameLbl" CssClass="text_bold" runat="server" Text="Survey Name: "></asp:Label>
                    <asp:Label ID="lblSurveyName" CssClass="text_bold" runat="server"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow Height="22px">
                <asp:TableCell Width="5%" Text="Q.No." CssClass="normalbol" HorizontalAlign="Center"></asp:TableCell>
                <asp:TableCell Width="95%" ColumnSpan="2" Text="Survey Question" CssClass="normalbol"
                    HorizontalAlign="Center"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
            <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
