﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmEmailBroadcastConfigDetail.aspx.vb" Inherits=".frmEmailBroadcastConfigDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script>
     function DeleteRecord() {
         if (confirm('Are you sure, you want to delete the selected record?')) {
             return true;
         }
         else {
             return false;
         }
     }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
   <div class="right-input">
        <div class="input-part">
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                <tr>
                    <td align="right">
                        <asp:HyperLink ID="hplNew" runat="server" CssClass="hyperlink" NavigateUrl="frmEmailBroadcastConfig.aspx">New Configuration
                        </asp:HyperLink>
                        &nbsp;
                    </td>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
   <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="normal4" align="center" colspan="2" valign="top">
              <asp:Label ID="lblMessage" Text="" runat="server"></asp:Label>   
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Email Broadcasting Configuration Detail
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:DataGrid ID="dgEmailConfigurationDetail" AllowSorting="True" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
             <asp:TemplateColumn HeaderText = "From Address">
                  <ItemStyle width="320px"/> 
                  <ItemTemplate>
                       <asp:Label ID= "lblFromAddress" Text ='<%#Eval("vcFromName").toString() + "@" + Eval("vcDomainName").toString() %>'   runat ="server">
                       </asp:Label>
                       
                       <asp:Label ID= "lblnumConfigurationId" Text = '<%#Eval("numConfigurationId") %>' Visible = "false"   runat ="server">
                       </asp:Label>
             </ItemTemplate>
            </asp:TemplateColumn>
             <asp:TemplateColumn HeaderText = "SPF Pass ?">
                  <ItemStyle width="80px"/> 
                  <ItemTemplate>
                       <img src= '<%# iif(Eval("bitSpfPass")=true,"../images/PlUploadImages/done.gif","../images/PlUploadImages/cancel-icon.png") %>'  alt=""/>
             </ItemTemplate>
            </asp:TemplateColumn>
             <asp:TemplateColumn HeaderText = "DKIM Pass ?">
                  <ItemStyle width="80px"/> 
                  <ItemTemplate>
                       <img src= '<%# iif(Eval("bitDkimPass")=true,"../images/PlUploadImages/done.gif","../images/PlUploadImages/cancel-icon.png") %>'  alt=""/>
             </ItemTemplate>
            </asp:TemplateColumn>
             <asp:TemplateColumn HeaderText = "Domain Key Pass ?">
                  <ItemStyle width="80px"/> 
                  <ItemTemplate>
                       <img src= '<%# iif(Eval("bitDomainKeyPass")=true,"../images/PlUploadImages/done.gif","../images/PlUploadImages/cancel-icon.png") %>'  alt=""/>
             </ItemTemplate>
            </asp:TemplateColumn>
             <asp:TemplateColumn HeaderText = "SenderID Pass ?">
                  <ItemStyle width="80px"/> 
                  <ItemTemplate>
                       <img src= '<%# iif(Eval("bitSpfPass")=true,"../images/PlUploadImages/done.gif","../images/PlUploadImages/cancel-icon.png") %>'  alt=""/>
             </ItemTemplate>
            </asp:TemplateColumn>
             <asp:TemplateColumn HeaderText = "Is Enable ?">
                  <ItemStyle width="80px"/> 
                  <ItemTemplate>
                       <img src= '<%# iif(Eval("bitEnabled")=true,"../images/PlUploadImages/done.gif","../images/PlUploadImages/cancel-icon.png") %>'  alt=""/>
             </ItemTemplate>
            </asp:TemplateColumn>
             <asp:TemplateColumn HeaderStyle-Width="8" ItemStyle-Width="8">
                                         <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete" CommandArgument = '<%#Eval("numConfigurationId") %>'>
                                            </asp:Button>
                                            <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
													<font color="#730000">*</font></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
             <asp:TemplateColumn HeaderStyle-Width = "30" ItemStyle-Width = "30">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hplImage"   NavigateUrl='<%#"frmEmailBroadcastConfig.aspx?ConfigurationID=" + Eval("numConfigurationId").toString() %>' runat="server" CssClass="hyperlink">Edit </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
</asp:Content>
