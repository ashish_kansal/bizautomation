﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmEmailBroadcastSetting.aspx.vb" Inherits=".frmEmailBroadcastSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function Validate() {
            var errorMessage = ""

            if ($("#txtDomain").val().length == 0) {
                errorMessage = "Domian is required." + "\n";
            }

            if ($("#txtFromEmail").val().length == 0) {
                errorMessage = "Email is required." + "\n";
            } else {
                if (!ValidateEmail($("#txtFromEmail").val())) {
                    errorMessage = "Email is invalid." + "\n";
                }
            }

            if ($("#txtAWSAcceccKey").val().length == 0) {
                errorMessage += "AWSAccessKeyId is required." + "\n";
            }

            if ($("#txtAWSAcceccKey").val().length == 0) {
                errorMessage += "AWSSecretKey is required." + "\n";
            }

            if (errorMessage.length > 0) {
                errorMessage = errorMessage.substr(0, errorMessage.length - 2);
                alert(errorMessage);
                return false;
            } else {
                return true;
            }
        }

        function ValidateEmail(email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" OnClientClick="return Validate();"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div style="text-align: center">
        <asp:Label ID="litSuccess" runat="server" Style="color: red"></asp:Label>
        <asp:Label ID="litError" Style="color: red" runat="server"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Email Broadcast Configuration - Amazon SES Settings&nbsp;<a href="#" onclick="return OpenHelpPopUp('marketing/frmemailbroadcastsetting.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Domain<span style="color: red">&nbsp;*</span><asp:Label ID="Label1" runat="server" ForeColor="Blue" Font-Bold="false" ToolTip="Domain you have added in your Amazon SES Account. i.e bizautomation.com" Text="[?]"></asp:Label></label>
                    <asp:TextBox ID="txtDomain" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>
                        Email<span style="color: red">&nbsp;*</span>
                        <asp:Label ID="lblEmailTooltip" runat="server" ForeColor="Blue" Font-Bold="false" ToolTip="Email must be from domian you have added in your Amazon SES Account because this will used as from address while sending email and email will not be delivered if email do not belong to domain. i.e xyz@bizautomation.com" Text="[?]"></asp:Label></label>
                    <asp:TextBox ID="txtFromEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>AWSAccessKeyId<span style="color: red">&nbsp;*</span></label>
                    <asp:TextBox ID="txtAWSAcceccKey" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>AWSSecretKey<span style="color: red">&nbsp;*</span></label>
                    <asp:TextBox ID="txtAWSSecretKey" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Identity Verification</label>
                    <div>
                        <asp:Label ID="lblIdentityStatus" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>DKIM Verification</label>
                    <div>
                        <asp:Label ID="lblDKIMStatus" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>DKIM</label>
                    <div>
                        <asp:Label ID="lblDKIM" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-solid box-primary">
        <div class="box-header">
            <h3 class="box-title">Steps to configure your Amazon Simple Email Service (Amazon SES) Account</h3>
        </div>
        <div class="box-body">
            <p style="font-size: 13px;">
                You have to create your own Amazon Simple Email Service (Amazon SES) account to use email broadcast feature because Amazon Simple Email Service (Amazon SES) is a cost-effective outbound-only email-sending service built on the reliable and scalable infrastructure that Amazon.com has developed to serve its own customer base. With Amazon SES, you can send transactional email, marketing messages, or any other type of high-quality content and you only pay for what you use. Following steps will help you to set up account:
            </p>
            <table width="100%" class="table table-bordered table-striped">
                <tr>
                    <th>No</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td style="text-align: center; font-weight: bold">1.</td>
                    <td style="width: 100%">You need to create an AWS account before you can use Amazon SES or other AWS services. When you create an AWS account, AWS automatically signs up your account for all services. You are charged only for the services that you use. Go to <a href="http://aws.amazon.com/ses" target="_blank" style="color: #0026ff">http://aws.amazon.com/ses</a>, and click <span class="emphasis"><em>Sign up now</em></span>.
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; font-weight: bold">2.</td>
                    <td style="width: 100%">
                        <a style="color: #0026ff" href="http://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-domains.html" target="_blank">Verifying Domains in Amazon SES</a>. Verify domian instead of email so that you so not have to verify each email seperately.
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; font-weight: bold">3.</td>
                    <td style="width: 100%">
                        <a style="color: #0026ff" href="http://docs.aws.amazon.com/ses/latest/DeveloperGuide/easy-dkim.html#easy-dkim-existing-domain" target="_blank">Authenticating Email with DKIM in Amazon SES</a>. Once DKIM was setup and verified successfully you have to still enable it in the AWS console at SES -> Domains -> DKIM.
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; font-weight: bold">4.</td>
                    <td style="width: 100%">After you've signed up for Amazon SES, you'll need to obtain your AWS access keys if you want to access Amazon SES. You can get access and secret key pairs, by going to the <a style="color: #0026ff" href="https://console.aws.amazon.com/iam/home?#security_credential" target="_blank">Security Credentials</a> page in the AWS Management Console. Now add these keys in the <b>AWSAccessKeyId</b> and <b>AWSSecretKey</b> fileds above.
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; font-weight: bold">5.</td>
                    <td style="width: 100%">
                        <a style="color: #0026ff" href="http://docs.aws.amazon.com/ses/latest/DeveloperGuide/easy-dkim.html#easy-dkim-existing-domain" target="_blank">Requesting Production Access to Amazon SES</a>. In sandbox environment:
                    <asp:BulletedList ID="BulletedList1" runat="server" Style="margin-left: 15px;">
                        <asp:ListItem style="list-style: disc !important" Text="Emails can be sent only to the Amazon SES mailbox simulator and to verified email addresses or domains." Value="1"></asp:ListItem>
                        <asp:ListItem style="list-style: disc !important" Text="Emails can be sent only from verified email addresses or domains." Value="2"></asp:ListItem>
                        <asp:ListItem style="list-style: disc !important" Text="You can send a maximum of 200 messages per 24-hour period." Value="3"></asp:ListItem>
                        <asp:ListItem style="list-style: disc !important" Text="You can send a maximum of one message per second." Value="4"></asp:ListItem>
                    </asp:BulletedList>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
