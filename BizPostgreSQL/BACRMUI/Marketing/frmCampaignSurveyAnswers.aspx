<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCampaignSurveyAnswers.aspx.vb"
    Inherits="BACRM.UserInterface.Survey.frmCampaignSurveyAnswers" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Survey Answer Editor</title>
    <script language="javascript" src="../javascript/Surveys.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveAnswer" runat="Server" CssClass="button" Text="Save"></asp:Button>&nbsp;<asp:Button
                ID="btnSaveAndClose" runat="Server" CssClass="button" Text="Save &amp; Close">
            </asp:Button>&nbsp;<asp:Button ID="btnCloseAnswerWindow" runat="Server" CssClass="button"
                Text="Close"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Response Editor
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <input id="hdSurId" type="hidden" name="hdSurId" runat="server">
    <input id="hdQId" type="hidden" name="hdQId" runat="server">
    <input id="hdAnsId" type="hidden" name="hdAnsId" runat="server">
    <input type="hidden" runat="server" name="hdAction" id="hdAction">
    <asp:Literal ID="litClientMessage" EnableViewState="False" runat="server"></asp:Literal>
    <asp:Table ID="tblSurveyEdited" runat="server" CellSpacing="0" CellPadding="0" Width="100%"
        CssClass="aspTable" GridLines="None" BorderColor="#000000" BorderWidth="1">
        <asp:TableRow>
            <asp:TableCell CssClass="normal1">
                <asp:RadioButtonList ID="rdbResponseTypes" runat="server" CssClass="normal1" AutoPostBack="True"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="4">Drop Down List Box</asp:ListItem>
                    <asp:ListItem Value="0">Check Boxes</asp:ListItem>
                    <asp:ListItem Value="1">Radio Buttons</asp:ListItem>
                    <asp:ListItem Value="2">Textbox</asp:ListItem>
                    <asp:ListItem Value="3">Areas Of Interest</asp:ListItem>
                </asp:RadioButtonList>
            </asp:TableCell>
            <asp:TableCell CssClass="normal1" HorizontalAlign="Left">
                <asp:Button ID="btnAOIs" Text="Select AOIs" CssClass="button" runat="Server"></asp:Button>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell CssClass="normal1" ColumnSpan="2">
                &nbsp;&nbsp;&nbsp;Number of Responses
                <asp:DropDownList ID="ddlNumberOfResponse" runat="server" CssClass="signup" AutoPostBack="True">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5">5</asp:ListItem>
                    <asp:ListItem Value="6">6</asp:ListItem>
                    <asp:ListItem Value="7">7</asp:ListItem>
                    <asp:ListItem Value="8">8</asp:ListItem>
                    <asp:ListItem Value="9">9</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                    <asp:ListItem Value="13">13</asp:ListItem>
                    <asp:ListItem Value="14">14</asp:ListItem>
                    <asp:ListItem Value="15">15</asp:ListItem>
                </asp:DropDownList>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:CheckBox ID="chkMatrix" runat="server" Text="Make answers appear in a matrix with"
                    AutoPostBack="True" />
                &nbsp;
                <asp:DropDownList ID="ddMatrix" runat="server" CssClass="signup" AutoPostBack="True">
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5">5</asp:ListItem>
                    <asp:ListItem Value="6">6</asp:ListItem>
                    <asp:ListItem Value="7">7</asp:ListItem>
                    <asp:ListItem Value="8">8</asp:ListItem>
                    <asp:ListItem Value="9">9</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:LinkButton ID="lbMatrix" runat="server">matrix columns</asp:LinkButton>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Table ID="tblSurveyQuestionAnswer" runat="server" CellSpacing="1" CellPadding="1"
        Width="100%">
        <asp:TableRow CssClass="normal1">
            <asp:TableCell CssClass="hs" Width="25%">Reponse Type</asp:TableCell>
            <asp:TableCell CssClass="hs" Width="65%">Caption</asp:TableCell>
            <asp:TableCell CssClass="hs" Width="10%">Rules</asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <div id="divAOISelector" style="z-index: 2; left: 0px; background-image: none; visibility: hidden;
        width: 680px; position: absolute; top: 71px; height: 500px; background-color: white">
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="normal1" valign="middle" align="left" colspan="2" height="23">
                    &nbsp;&nbsp;&nbsp;<b>AOI Include List</b>
                </td>
                <td valign="middle" align="right" colspan="2" height="23">
                    <input class="button" id="btnNew2Ok" onclick="makeAOITransfer()" type="button" value="Ok"
                        name="btnNew2Ok">
                    <input class="button" id="btnNew2" onclick="cancelAOI()" type="button" value="Close"
                        name="btnNew2">&nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" class="normal1" valign="top">
                    AOIs<br>
                    &nbsp;&nbsp;
                    <asp:ListBox ID="ddlAvailableAOIs" runat="server" Width="170" Height="174" CssClass="signup"
                        EnableViewState="False"></asp:ListBox>
                </td>
                <td align="center" valign="middle">
                    <input type="button" id="btnAddTwo" class="button" value="Add >" style="width: 57px"
                        onclick="javascript:move(document.form1.ddlAvailableAOIs,document.form1.ddlSelectedAOIs)">
                    <br>
                    <br>
                    &nbsp;<input type="button" id="btnRemoveTwo" class="button" value="< Remove" style="width: 57px"
                        onclick="javascript:remove1(document.form1.ddlSelectedAOIs,document.form1.ddlAvailableAOIs)">&nbsp;
                </td>
                <td class="normal1">
                    AOIs Chosen<br>
                    <asp:ListBox ID="ddlSelectedAOIs" runat="server" Width="170" Height="174" CssClass="signup"
                        EnableViewState="False"></asp:ListBox>
                    (Select
                    <asp:Literal ID="litMaxAOIs" runat="server"></asp:Literal>)
                </td>
                <td align="left" valign="middle">
                    <img id="btnMoveupTwo" src="../images/upArrow.gif" onclick="javascript:MoveUp(document.form1.ddlSelectedAOIs)" />&nbsp;
                    <br>
                    <br>
                    <img id="btnMoveDownTwo" src="../images/downArrow1.gif" onclick="javascript:MoveDown(document.form1.ddlSelectedAOIs)" />&nbsp;
                </td>
            </tr>
        </table>
    </div>
    <asp:ValidationSummary ID="ValidationSummary" runat="server" HeaderText="Please check the following value(s)"
        ShowSummary="False" ShowMessageBox="True" DisplayMode="List"></asp:ValidationSummary>
</asp:Content>
