﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmCampaignSurveyCreateRecord

    '''<summary>
    '''btnSaveAnswer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveAnswer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hdSurId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdSurId As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''tblSurveyCreateRecord control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblSurveyCreateRecord As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''txtRatingMin1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRatingMin1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRatingMax1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRatingMax1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlCRMType1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCRMType1 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlRelationShip1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRelationShip1 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlProfile1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProfile1 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlGroup1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlGroup1 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlRecordOwner1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRecordOwner1 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddFollowUpStatus1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddFollowUpStatus1 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''rbDefault1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbDefault1 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''txtRatingMin2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRatingMin2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRatingMax2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRatingMax2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlCRMType2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCRMType2 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlRelationShip2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRelationShip2 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlProfile2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProfile2 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlGroup2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlGroup2 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlRecordOwner2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRecordOwner2 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddFollowUpStatus2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddFollowUpStatus2 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''rbDefault2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbDefault2 As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''txtRatingMin3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRatingMin3 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRatingMax3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRatingMax3 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlCRMType3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCRMType3 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlRelationShip3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRelationShip3 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlProfile3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProfile3 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlGroup3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlGroup3 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlRecordOwner3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRecordOwner3 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddFollowUpStatus3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddFollowUpStatus3 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''rbDefault3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbDefault3 As Global.System.Web.UI.WebControls.RadioButton
End Class
