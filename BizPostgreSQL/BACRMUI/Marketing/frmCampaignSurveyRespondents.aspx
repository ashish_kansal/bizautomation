<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCampaignSurveyRespondents.aspx.vb"
    Inherits="BACRM.UserInterface.Survey.frmCampaignSurveyRespondents" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Survey Respondents</title>
    <script language="javascript" src="../javascript/Surveys.js"></script>
     <style>
         .table {
        border-color:#CACACA;
        }
            .table > tbody > tr:first-child {
            background:rgba(225, 225, 225, 0.23);
            font-weight:bold;
            }
             .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="col-md-6" style="margin-bottom:10px;">
        <div class="pull-left">
            <asp:Button ID="btnExportRespondentsToExcel" runat="server" CssClass="btn btn-success" Text="Export to Excel"
                CausesValidation="False"></asp:Button>&nbsp;<asp:Button ID="btnBack" runat="Server"
                    Text="Back" CssClass="btn btn-primary" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="col-md-6" style="margin-bottom:10px;"   >
        <div class="pull-right">
            <div class="form-inline">
                <label>Find Respondent</label>
                     <asp:TextBox ID="txtRespondentName" runat="server" CssClass="form-control"></asp:TextBox>&nbsp;<asp:Button
                    ID="btnGo" runat="Server" Text="Go" CssClass="btn btn-primary" ValidationGroup="vgBo"></asp:Button>
                <asp:RequiredFieldValidator ID="ValidateRespondentName" runat="server" ErrorMessage="Please enter the Respondent Name to search."
                    InitialValue="" Display="None" EnableClientScript="True" ControlToValidate="txtRespondentName" ValidationGroup="vgBo"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Surveys Respondents
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="tblSurveyRespondentList" runat="server" Width="100%" CssClass="aspTable"
        Height="400" GridLines="None" BorderColor="Black" BorderWidth="1" CellSpacing="0"
        CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top" ColumnSpan="6">
                <asp:DataGrid ID="dgSurveyRespondentList" runat="server" Width="100%" AutoGenerateColumns="False"
                    CssClass="table table-responsive" 
                    DataKeyField="numSurID" BorderWidth="1px" CellPadding="2" CellSpacing="0" ShowHeader="True"
                    AllowSorting="True">
                    <Columns>
                        <asp:BoundColumn Visible="False" HeaderText="Respondent ID" DataField="numRespondantID">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Date of Response" SortExpression="dtDateofResponse">
                            <ItemTemplate>
                                <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "dtDateofResponse"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Date Contact Added" SortExpression="bintCreatedDate">
                            <ItemTemplate>
                                <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "bintCreatedDate"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="True" HeaderText="Respondents" DataField="vcRespondentName"
                            SortExpression="vcRespondentName"></asp:BoundColumn>
                        <asp:BoundColumn Visible="True" HeaderText="Organization" DataField="vcCompanyName"
                            SortExpression="vcCompanyName"></asp:BoundColumn>
                        <asp:BoundColumn Visible="True" HeaderText="Email" DataField="vcEmail" ItemStyle-Width="150"
                            SortExpression="vcEmail"></asp:BoundColumn>
                        <asp:BoundColumn Visible="True" HeaderText="Added to BizAutomation ?" DataField="bitAddedToBz"
                            SortExpression="bitAddedToBz"></asp:BoundColumn>
                        <asp:BoundColumn Visible="True" HeaderText="Survey Rating" DataField="numSurRating"
                            SortExpression="numSurRating"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"
                                    CausesValidation="False"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <asp:Label ID="SortExp" runat="server" Visible="False" Text="dtDateofResponse Asc" />
                <asp:Label ID="FilterExp" runat="server" Visible="False" Text="" />
                <asp:Literal ID="litClientMessage" runat="server" EnableViewState="False"></asp:Literal>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <%--  <iframe id="IfrOpenOrgContact" src="frmOrgContactRedirect.aspx" frameborder="0" width="10"
        scrolling="no" height="10" left="0" right="0">
        <input type="hidden" name="hdRedirectionEntity" id="hdRedirectionEntity" runat="server"><input
            type="hidden" name="hdContactId" id="hdContactId" runat="server">
    </iframe>--%>
    <asp:ValidationSummary ID="ValidationSummary" runat="server" HeaderText="Please check the following value(s)"
        ShowSummary="False" ShowMessageBox="True" DisplayMode="List" ValidationGroup="vgBo"></asp:ValidationSummary>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none" Text="1"></asp:TextBox>
    <asp:Button ID="btnGo1" runat="server" Style="display: none" />
        <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
