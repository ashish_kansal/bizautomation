﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports Amazon
Imports Amazon.SimpleEmail
Imports Amazon.SimpleEmail.Model

Public Class frmEmailBroadcastSetting
    Inherits BACRMPage
#Region "Member Variables"
    Dim objCampaign As Campaign
#End Region

#Region "Constructor"
    Sub New()
        objCampaign = New Campaign
    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            litSuccess.Text = ""
            litError.Text = ""

            If Not Page.IsPostBack Then
                GetBroadcastConfiguration()
                CheckDomainVerificationStatus()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub GetBroadcastConfiguration()
        Try
            objCampaign.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dt As DataTable = objCampaign.GetBroadcastConfiguration()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                txtFromEmail.Text = CCommon.ToString(dt.Rows(0)("vcFrom"))
                txtDomain.Text = CCommon.ToString(dt.Rows(0)("vcAWSDomain"))
                txtAWSAcceccKey.Text = CCommon.ToString(dt.Rows(0)("vcAWSAccessKey"))
                txtAWSSecretKey.Text = CCommon.ToString(dt.Rows(0)("vcAWSSecretKey"))
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub SaveBroadcastConfiguration()
        Try
            If CheckDomainVerificationStatus() Then
                objCampaign.DomainID = CCommon.ToLong(Session("DomainID"))
                objCampaign.SaveBroadcastConfiguration(txtAWSAcceccKey.Text.Trim(), txtAWSSecretKey.Text.Trim(), txtDomain.Text.Trim(), txtFromEmail.Text.Trim())

                If Not lblDKIMStatus.Text.ToLower().Contains("verified") Or Not lblIdentityStatus.Text.ToLower().Contains("verified") Then
                    litError.Text = "AWS account detail is saved successfully but Identity and DKIM must be verified to use email broadcast"
                Else
                    litSuccess.Text = "AWS account detail is verified and saved successfully."
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Function CheckDomainVerificationStatus() As Boolean
        Dim isValidCredential As Boolean = False

        Try
            If Not String.IsNullOrEmpty(txtAWSAcceccKey.Text.Trim()) AndAlso
                Not String.IsNullOrEmpty(txtAWSSecretKey.Text.Trim()) AndAlso
                Not String.IsNullOrEmpty(txtDomain.Text.Trim()) AndAlso
                Not String.IsNullOrEmpty(txtFromEmail.Text.Trim()) Then

                Dim region As RegionEndpoint = RegionEndpoint.USWest2
                Dim client As New AmazonSimpleEmailServiceClient(txtAWSAcceccKey.Text.Trim(), txtAWSSecretKey.Text.Trim(), region)

                'Check Domain Verification Status
                Dim requestIdentity As New GetIdentityVerificationAttributesRequest()
                requestIdentity.Identities.Add(txtDomain.Text.Trim())

                Dim responseIdentity As GetIdentityVerificationAttributesResponse = client.GetIdentityVerificationAttributes(requestIdentity)

                If Not responseIdentity Is Nothing AndAlso responseIdentity.VerificationAttributes.Count > 0 AndAlso responseIdentity.VerificationAttributes.ContainsKey(txtDomain.Text.Trim()) Then
                    Dim identityAttriubute As IdentityVerificationAttributes = responseIdentity.VerificationAttributes(txtDomain.Text.Trim())

                    If identityAttriubute.VerificationStatus = "Success" Then
                        lblIdentityStatus.Text = "Verified"
                        lblIdentityStatus.Style.Add("color", "green")
                    Else
                        lblIdentityStatus.Text = "Pending"
                        lblIdentityStatus.Style.Add("color", "red")
                    End If

                    Dim requestDkimIdentity As GetIdentityDkimAttributesRequest = New GetIdentityDkimAttributesRequest()
                    requestDkimIdentity.Identities.Add(txtDomain.Text)

                    Dim responseDkimIdentity As GetIdentityDkimAttributesResponse = client.GetIdentityDkimAttributes(requestDkimIdentity)
                    If responseDkimIdentity Is Nothing Or responseDkimIdentity.DkimAttributes.Count = 0 Or Not responseDkimIdentity.DkimAttributes.ContainsKey(txtDomain.Text.Trim()) Or responseDkimIdentity.DkimAttributes(txtDomain.Text.Trim()).DkimVerificationStatus <> "Success" Then
                        lblDKIMStatus.Text = "Pending"
                        lblDKIMStatus.Style.Add("color", "red")

                        lblDKIM.Text = "Disabled"
                        lblDKIMStatus.Style.Add("color", "red")
                    Else
                        lblDKIMStatus.Text = "Verified"
                        lblDKIMStatus.Style.Add("color", "green")

                        If responseDkimIdentity.DkimAttributes(txtDomain.Text.Trim()).DkimEnabled Then
                            lblDKIM.Text = "Enabled"
                            lblDKIM.Style.Add("color", "green")
                        Else
                            lblDKIM.Text = "Disabled"
                            lblDKIM.Style.Add("color", "red")
                        End If
                    End If
                Else
                    litError.Text = "Given domain does not exist in your amazon account"
                End If

                isValidCredential = True
            End If
        Catch ex As Exception
            isValidCredential = False

            If ex.[GetType]().Name = "AmazonSimpleEmailServiceException" Then
                Dim exAmazon As AmazonSimpleEmailServiceException = DirectCast(ex, AmazonSimpleEmailServiceException)

                If exAmazon.ErrorCode = "InvalidClientTokenId" Then
                    litError.Text = "Enter valid Domian, AWSAccessKey and AWSSecretKey."
                ElseIf exAmazon.ErrorCode = "InvalidParameterValue" Then
                    litError.Text = "Invalid domain."
                Else
                    Throw
                End If
            Else
                Throw
            End If
        End Try

        Return isValidCredential
    End Function
#End Region

#Region "Event Handlers"
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            If Not String.IsNullOrEmpty(txtDomain.Text) AndAlso Not String.IsNullOrEmpty(txtAWSAcceccKey.Text) AndAlso Not String.IsNullOrEmpty(txtAWSSecretKey.Text) Then
                SaveBroadcastConfiguration()
                GetBroadcastConfiguration()
            Else
                litError.Text = "Email, Domian, AWSAccessKey and AWSSecretKey are required."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
#End Region
    
End Class