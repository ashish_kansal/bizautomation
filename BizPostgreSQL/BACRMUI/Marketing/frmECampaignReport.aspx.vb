﻿Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Partial Public Class frmECampaignReport
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                BindCampaign()
                BindGrid()
            End If
            'ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('3');}else{ parent.SelectTabByValue('3'); } ", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub
    Private Sub BindCampaign()
        Try
            Dim objCampaign As New Campaign
            objCampaign.DomainID = Session("DomainId")
            Dim dt As DataTable = objCampaign.GetECampaigns()
            ddlCampaign.DataSource = dt
            ddlCampaign.DataTextField = "vcECampName"
            ddlCampaign.DataValueField = "numECampaignID"
            ddlCampaign.DataBind()
            Dim listItem As New ListItem
            listItem.Text = "--Select One--"
            listItem.Value = "0"
            ddlCampaign.Items.Insert(0, listItem)
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub BindGrid()
        Dim dtReport As DataTable
        Dim objCampaign As New Campaign
        objCampaign.PageSize = Session("PagingRows")
        objCampaign.TotalRecords = 0

        If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
        objCampaign.CurrentPage = txtCurrrentPage.Text.Trim()

        bizPager.PageSize = Session("PagingRows")
        bizPager.RecordCount = objCampaign.TotalRecords
        bizPager.CurrentPageIndex = txtCurrrentPage.Text

        objCampaign.DomainID = Session("DomainId")
        objCampaign.CampaignID = CCommon.ToLong(ddlCampaign.SelectedValue)
        dtReport = objCampaign.ECampaignReport()
        dgCampaigns.DataSource = dtReport
        dgCampaigns.DataBind()

    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemove.Click
        Try
            Dim i As Integer = 0

            For Each row As DataGridItem In dgCampaigns.Items
                If CType(row.FindControl("chk"), CheckBox).Checked = True Then
                    Dim objCampaign As New Campaign
                    With objCampaign

                        .ContactId = dgCampaigns.DataKeys(i).ToString()
                        .ECampaignID = dgCampaigns.Items(i).Cells(0).Text
                        .DeleteFromCampaign()

                    End With
                End If
                i += 1
            Next
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlCampaign_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCampaign.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = ""
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class
