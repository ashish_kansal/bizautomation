' Craeted By Anoop Jayaraj
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports System.IO

Partial Class frmImageDetails
    Inherits BACRMPage

    Dim objCommon As CCommon
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim strDocName As String                                        'To Store the information where the File is stored.
    Dim strFileType As String
    Dim strFileName As String
    Dim URLType As String
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If GetQueryStringVal( "New") = 1 Then
                Session("DocID") = Nothing
                tblMenu.Visible = False
            Else : tblMenu.Visible = True
            End If
            If Not IsPostBack Then
                
               
                objCommon = New CCommon
                GetUserRightsForPage(14, 3)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                Else : If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnSaveCLose.Visible = False
                End If
                If Session("DocID") <> "" Then LoadInformation()
                btnSaveCLose.Attributes.Add("onclick", "return Save()")
                btnSave.Attributes.Add("onclick", "return Save()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadInformation()
        Try
            Dim dtDocDetails As DataTable
            Dim objDocuments As New DocumentList
            With objDocuments
                .GenDocID = Session("DocID")
                .DomainID = Session("DomainID")
                .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtDocDetails = .GetDocByGenDocID
            End With
            ViewState("FileName") = IIf(IsDBNull(dtDocDetails.Rows(0).Item("VcFileName")), "", dtDocDetails.Rows(0).Item("VcFileName"))
            txtDocName.Text = dtDocDetails.Rows(0).Item("vcDocName")
            img.ImageUrl = Replace(dtDocDetails.Rows(0).Item("VcFileName"), "..", Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
            lblCreatedBy.Text = dtDocDetails.Rows(0).Item("Created")
            lblLastModifiedBy.Text = dtDocDetails.Rows(0).Item("Modified")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub UploadFile()
        Try
            Dim strFName As String()
            Dim strFilePath As String
            URLType = "L"
            If (fileupload.PostedFile.ContentLength > 0) Then
                strFileName = Path.GetFileName(fileupload.PostedFile.FileName)      'Getting the File Name
                If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                End If
                strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                strFName = Split(strFileName, ".")
                strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                strDocName = strFName(0)                  'Getting the Name of the File without Extension
                fileupload.PostedFile.SaveAs(strFilePath)
                strFileName = "../documents/docs/" & strFileName
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub Save()
        Try
            If CCommon.ToLong(Session("DomainId")) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            UploadFile()
            Dim arrOutPut As String()
            Dim objDocuments As New DocumentList()
            With objDocuments
                .DomainID = Session("DomainId")
                .GenDocID = Session("DocID")
                .UserCntID = Session("UserContactID")
                .UrlType = URLType
                .DocDesc = ""
                .DocumentStatus = 0
                .DocCategory = 370
                .FileType = strFileType
                .DocName = txtDocName.Text
                .FileName = strFileName
                .Subject = ""
                .DocumentType = 1 '1=generic,2=specific
                arrOutPut = .SaveDocuments()
            End With
            Session("DocID") = arrOutPut(0)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveCLose.Click
        Try
            Save()
            Response.Redirect("../Marketing/frmImageList.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect("../Marketing/frmImageList.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
            Response.Redirect("../Marketing/frmImageDetails.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
