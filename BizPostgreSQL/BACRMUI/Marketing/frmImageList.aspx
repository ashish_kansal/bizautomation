<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmImageList.aspx.vb" Inherits="frmImageList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Image List</title>
		<script language="javascript">
		function Close()
		{
			window.close()
			return false;
		}
		function fnSortByChar(varSortChar)
			{
				document.Form1.txtSortChar.value = varSortChar;
				if (document.Form1.txtCurrrentPage!=null)
				{
					document.Form1.txtCurrrentPage.value=1;
				}
				document.Form1.submit();
			}
		function DeleteAccount()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
			function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
		</script>
	</HEAD>
	<BODY>
		<form id="Form1" method="post" runat="server">
		<br>
			<TABLE align="right">
				<TR>
					<TD class="normal1" width="150">No of Records:
						<asp:label id="lblRecordCount" runat="server"></asp:label></TD>
					<td class="normal1" align="right">Keyword Search&nbsp;
					</td>
					<td><asp:textbox id="txtKeyWord" CssClass="signup" Runat="server"></asp:textbox>&nbsp;</td>
					<td><asp:button id="btnGo" CssClass="button" Runat="server" Text="Go"></asp:button>
						<asp:button id="btnClose" CssClass="button" Runat="server" Width="50" Text="Close"></asp:button>
					</td>
				</TR>
			</TABLE>
			<br>
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Documents&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td class="normal1" align="right">
						<asp:HyperLink ID="hplNew" Runat="server"  NavigateUrl="../Marketing/frmImageDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&New=1">Upload a Picture</asp:HyperLink>
					</td>
					<td id="hidenav" noWrap align="right" runat="server">
						<table>
							<tr>
								<td><asp:label id="lblNext" runat="server" cssclass="Text_bold">Next:</asp:label></td>
								<td class="normal1"><asp:linkbutton id="lnk2" runat="server">2</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk3" runat="server">3</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk4" runat="server">4</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk5" runat="server">5</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkFirst" runat="server"><div class="LinkArrow"><<</div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkPrevious" runat="server"><div class="LinkArrow"><</div>
									</asp:linkbutton></td>
								<td class="normal1"><asp:label id="lblPage" runat="server">Page</asp:label></td>
								<td><asp:textbox id="txtCurrrentPage" runat="server" Width="28px" CssClass="signup" AutoPostBack="True"
										MaxLength="5"></asp:textbox></td>
								<td class="normal1"><asp:label id="lblOf" runat="server">of</asp:label></td>
								<td class="normal1"><asp:label id="lblTotal" runat="server"></asp:label></td>
								<td><asp:linkbutton id="lnkNext" runat="server" CssClass="LinkArrow"><div class="LinkArrow">></div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkLast" runat="server"><div class="LinkArrow">>></div>
									</asp:linkbutton></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table cellSpacing="1" cellPadding="1" width="100%" border="0">
				<tr>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="a" href="javascript:fnSortByChar('a')">
							<div class="A2Z">A</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="b" href="javascript:fnSortByChar('b')">
							<div class="A2Z">B</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="c" href="javascript:fnSortByChar('c')">
							<div class="A2Z">C</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="d" href="javascript:fnSortByChar('d')">
							<div class="A2Z">D</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="e" href="javascript:fnSortByChar('e')">
							<div class="A2Z">E</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="f" href="javascript:fnSortByChar('f')">
							<div class="A2Z">F</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="g" href="javascript:fnSortByChar('g')">
							<div class="A2Z">G</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="h" href="javascript:fnSortByChar('h')">
							<div class="A2Z">H</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="I" href="javascript:fnSortByChar('i')">
							<div class="A2Z">I</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="j" href="javascript:fnSortByChar('j')">
							<div class="A2Z">J</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="k" href="javascript:fnSortByChar('k')">
							<div class="A2Z">K</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="l" href="javascript:fnSortByChar('l')">
							<div class="A2Z">L</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="m" href="javascript:fnSortByChar('m')">
							<div class="A2Z">M</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="n" href="javascript:fnSortByChar('n')">
							<div class="A2Z">N</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="o" href="javascript:fnSortByChar('o')">
							<div class="A2Z">O</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="p" href="javascript:fnSortByChar('p')">
							<div class="A2Z">P</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="q" href="javascript:fnSortByChar('q')">
							<div class="A2Z">Q</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="r" href="javascript:fnSortByChar('r')">
							<div class="A2Z">R</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="s" href="javascript:fnSortByChar('s')">
							<div class="A2Z">S</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="t" href="javascript:fnSortByChar('t')">
							<div class="A2Z">T</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="u" href="javascript:fnSortByChar('u')">
							<div class="A2Z">U</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="v" href="javascript:fnSortByChar('v')">
							<div class="A2Z">V</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="w" href="javascript:fnSortByChar('w')">
							<div class="A2Z">W</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="x" href="javascript:fnSortByChar('x')">
							<div class="A2Z">X</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="y" href="javascript:fnSortByChar('y')">
							<div class="A2Z">Y</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="z" href="javascript:fnSortByChar('z')">
							<div class="A2Z">Z</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="all" href="javascript:fnSortByChar('0')">
							<div class="A2Z">All</div>
						</A>
					</td>
				</tr>
			</table>
			<asp:table id="Table3" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
				BorderColor="black" GridLines="None" Height="350">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<asp:datagrid id="dgDocs" runat="server" Width="100%" CssClass="dg" AllowSorting="True" AutoGenerateColumns="False"
							>
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="numGenericDocID" HeaderText="numGenericDocID"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="numDocCategory" HeaderText="numDocCategory"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="VcFileName" HeaderText="VcFileName"></asp:BoundColumn>
								<asp:ButtonColumn DataTextField="vcDocName" SortExpression="vcDocName" HeaderText="<font color=white>Document Name</font>"
									CommandName="Name"></asp:ButtonColumn>
								<asp:BoundColumn DataField="Mod" SortExpression="Mod" HeaderText="<font color=white>Last Modified By, Last Modified On</font>"></asp:BoundColumn>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<asp:Button ID="btnHDelete" Runat="server" CssClass="button Delete" Text="X" ></asp:Button>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Button ID="btnDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
										<asp:LinkButton ID="lnkDelete" Runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server"></asp:Literal></td>
				</tr>
			</table>
			<asp:TextBox ID="txtTotalPage" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtTotalRecords" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtSortChar" Runat="server" style="DISPLAY:none"></asp:TextBox></form>
	</BODY>
</HTML>
