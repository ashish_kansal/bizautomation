﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCampaignSurveyCreateRecord.aspx.vb"
    Inherits=".frmCampaignSurveyCreateRecord" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Campaign Survey Create Record</title>
    <script language="javascript" src="../javascript/Surveys.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveAnswer" runat="Server" CssClass="button" Text="Save" />&nbsp;
            <asp:Button ID="btnClose" Text="Close" CssClass="button" runat="server" Width="50">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Create Record
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <input id="hdSurId" type="hidden" name="hdSurId" runat="server">
    <div style="background-color: #fff">
        <asp:Table ID="tblSurveyCreateRecord" runat="server" CellSpacing="1" CellPadding="1"
            Width="100%" BorderColor="black" BorderWidth="1" CssClass="aspTable">
            <asp:TableRow>
                <asp:TableCell CssClass="normal1">
                    When survey rating is between
                    <asp:TextBox ID="txtRatingMin1" runat="server" CssClass="signup"></asp:TextBox>
                    &
                    <asp:TextBox ID="txtRatingMax1" runat="server" CssClass="signup"></asp:TextBox>
                    Create a
                    <asp:DropDownList ID="ddlCRMType1" runat="server" CssClass="signup">
                        <asp:ListItem Value="0">Lead</asp:ListItem>
                        <asp:ListItem Value="1">Prospect</asp:ListItem>
                        <asp:ListItem Value="2">Account</asp:ListItem>
                    </asp:DropDownList>
                    with relationship of
                    <asp:DropDownList ID="ddlRelationShip1" runat="server" CssClass="signup" AutoPostBack="true">
                    </asp:DropDownList>
                    & profile
                    <asp:DropDownList ID="ddlProfile1" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    in the group
                    <asp:DropDownList ID="ddlGroup1" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    assigned to record owner
                    <asp:DropDownList ID="ddlRecordOwner1" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    with follow-up status set to
                    <asp:DropDownList ID="ddFollowUpStatus1" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    &nbsp; Set Default<asp:RadioButton ID="rbDefault1" runat="server" GroupName="Default" />
                    <br />
                    <br />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="normal1">
                    When survey rating is between
                    <asp:TextBox ID="txtRatingMin2" runat="server" CssClass="signup"></asp:TextBox>
                    &
                    <asp:TextBox ID="txtRatingMax2" runat="server" CssClass="signup"></asp:TextBox>
                    Create a
                    <asp:DropDownList ID="ddlCRMType2" runat="server" CssClass="signup">
                        <asp:ListItem Value="0">Lead</asp:ListItem>
                        <asp:ListItem Value="1">Prospect</asp:ListItem>
                        <asp:ListItem Value="2">Account</asp:ListItem>
                    </asp:DropDownList>
                    with relationship of
                    <asp:DropDownList ID="ddlRelationShip2" runat="server" CssClass="signup" AutoPostBack="true">
                    </asp:DropDownList>
                    & profile
                    <asp:DropDownList ID="ddlProfile2" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    in the group
                    <asp:DropDownList ID="ddlGroup2" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    assigned to record owner
                    <asp:DropDownList ID="ddlRecordOwner2" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    with follow-up status set to
                    <asp:DropDownList ID="ddFollowUpStatus2" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    &nbsp; Set Default<asp:RadioButton ID="rbDefault2" runat="server" GroupName="Default" />
                    <br />
                    <br />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell CssClass="normal1">
                    When survey rating is between
                    <asp:TextBox ID="txtRatingMin3" runat="server" CssClass="signup"></asp:TextBox>
                    &
                    <asp:TextBox ID="txtRatingMax3" runat="server" CssClass="signup"></asp:TextBox>
                    Create a
                    <asp:DropDownList ID="ddlCRMType3" runat="server" CssClass="signup">
                        <asp:ListItem Value="0">Lead</asp:ListItem>
                        <asp:ListItem Value="1">Prospect</asp:ListItem>
                        <asp:ListItem Value="2">Account</asp:ListItem>
                    </asp:DropDownList>
                    with relationship of
                    <asp:DropDownList ID="ddlRelationShip3" runat="server" CssClass="signup" AutoPostBack="true">
                    </asp:DropDownList>
                    & profile
                    <asp:DropDownList ID="ddlProfile3" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    in the group
                    <asp:DropDownList ID="ddlGroup3" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    assigned to record owner
                    <asp:DropDownList ID="ddlRecordOwner3" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    with follow-up status set to
                    <asp:DropDownList ID="ddFollowUpStatus3" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    &nbsp; Set Default<asp:RadioButton ID="rbDefault3" runat="server" GroupName="Default" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</asp:Content>
