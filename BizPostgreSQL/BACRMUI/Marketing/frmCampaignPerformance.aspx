﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCampaignPerformance.aspx.vb"
    Inherits=".frmCampaignPerformance" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <%--<script src="../JavaScript/jquery.min.js" type="text/javascript"></script>--%>
    <script type="text/javascript">

        function OpenCampaign(a) {
            document.location.href = 'frmCampaignDetails.aspx?frm=CampPer&CampID=' + a;
        }
        function ExpandedStateChanged(sender, args) {
            //var CustStage = "";
            //var BaseOn = "";
            //CustStage = document.getElementById('ctl00_GridPlaceHolder_radPanOnlineCampaign_i0_ddlCustomerStage').value;//document.getElementById('radPanOnlineCampaign_i0_ddlCustomerStage').value;
            //BaseOn = document.getElementById('ctl00_GridPlaceHolder_radPanOnlineCampaign_i0_ddlNoOfDays').value;

            //if (sender.get_clientStateFieldID() == "radPanOnlineCampaign_ClientState") {
            //    if (sender.get_selectedItem() != null)
            //        $('#hdnPanel1').val(sender.get_selectedItem().get_expanded());
            //    else
            //        $('#hdnPanel1').val(true);

            //    __doPostBack('radPanOnlineCampaign', '1');
            //}
            //else
            if (sender.get_clientStateFieldID() == "radPanOfflineCampaign_ClientState") {
                if (sender.get_selectedItem() != null)
                    $('#hdnPanel2').val(sender.get_selectedItem().get_expanded());
                else
                    $('#hdnPanel2').val(true);

                __doPostBack('radPanOfflineCampaign', '1');
            }

            //$.ajax({
            //    type: "POST",
            //    url: "../common/Common.asmx/SaveFilter",
            //    data: "{Panel1:'" + $('#hdnPanel1').val() + "',Panel2:'" + $('#hdnPanel2').val() + "',CustStage:'" + CustStage + "',BaseOn:'" + BaseOn + "'}",
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    success: function (msg) {
            //        alert("{Panel1:'" + $('#hdnPanel1').val() + "',Panel2:'" + $('#hdnPanel2').val() + "',CustStage:'" + CustStage + "',BaseOn:'" + BaseOn + "'}")
            //    }
            //});
        }


        function leadsList(linkButton) {
            var CurrentRow = linkButton.parentNode.parentNode;
            var SourceDomain = $(CurrentRow).find("#lblSourceDomain").text();
            var FromDate = $("#ddlLeadSources").val();
            console.log('SourceDomain : ' + SourceDomain);
            console.log('FromDate : ' + FromDate);
            var strUrl = "../Marketing/frmLeadsFromWebSource.aspx?LeadOrAccounts=" + 1 + "&SourceDomain=" + SourceDomain + "&FromDate=" + FromDate;
            console.log('strUrl : ' + strUrl);
            window.location.href = "../Marketing/frmLeadsFromWebSource.aspx?LeadOrAccounts=" + 1 + "&SourceDomain=" + SourceDomain + "&FromDate=" + FromDate;
            return false;

        }

        function accounts(linkButton) {
            var CurrentRow = linkButton.parentNode.parentNode;
            var SourceDomain = $(CurrentRow).find("#lblSourceDomain").text();
            var FromDate = $("#ddlLeadSources").val();
            console.log('Leads ');
            console.log('SourceDomain : ' + SourceDomain);
            console.log('FromDate : ' + FromDate);
            var strUrl = "../Marketing/frmLeadsFromWebSource.aspx?LeadOrAccounts=" + 2 + "&SourceDomain=" + SourceDomain + "&FromDate=" + FromDate;
            console.log('strUrl : ' + strUrl);
            window.location.href = "../Marketing/frmLeadsFromWebSource.aspx?LeadOrAccounts=" + 2 + "&SourceDomain=" + SourceDomain + "&FromDate=" + FromDate;
            return false;
        }
        function OpenHelp() {
            window.open('../Help/Marketing.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
    <style type="text/css">
        .descriptionPanelHeader
        {
            font-family: Arial;
        }

        .ProgressBar
        {
            height: 5px;
            width: 0px;
            background-color: Green;
        }

        .ProgressContainer
        {
            border: 1px solid black;
            height: 5px;
            font-size: 1px;
            background-color: White;
            line-height: 0;
        }
        .table {
        border-color:#e1e1e1
        }
            .table > tbody > tr:first-child {
            background:#e1e1e1;
            font-weight:bold;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Campaign Sources&nbsp;<a href="#" onclick="return OpenHelp()"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table border="0" width="100%">
        <%-- <tr>
            <td align="center" valign="top" colspan="3">
                <telerik:RadPanelBar runat="server" ID="radPanOnlineCampaign" OnClientItemExpand="ExpandedStateChanged"
                    Width="100%" ExpandMode="MultipleExpandedItems" PersistStateInCookie="true">
                    <Items>
                        <telerik:RadPanelItem Text="Online Campaign Sources" Value="OnlineCampaignSources"
                            Width="100%">
                            <ContentTemplate>
                                <table width="100%">
                                    <tr>
                                        <td width="30%">
                                            <igchart:UltraChart runat="server" ID="ucOnlineCampaing" BackgroundImageFileName=""
                                                Border-Color="Black" Border-Thickness="0" ChartType="PieChart" EmptyChartText=""
                                                Version="9.1">
                                                <ColorModel AlphaLevel="150" ColorBegin="Pink" ColorEnd="DarkRed" ModelStyle="CustomLinear">
                                                </ColorModel>
                                                <PieChart>
                                                    <Labels FormatString="<ITEM_LABEL>: (<PERCENT_VALUE:#>%)" />
                                                </PieChart>
                                                <Axis>
                                                    <PE ElementType="None" Fill="Cornsilk"></PE>
                                                    <X Visible="True" TickmarkInterval="0" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="&lt;ITEM_LABEL&gt;" HorizontalAlign="Near" VerticalAlign="Center"
                                                            Orientation="Horizontal" Font="Verdana, 7pt" FontColor="DimGray">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="DimGray" FormatString="">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </X>
                                                    <Y Visible="True" TickmarkInterval="10" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="&lt;DATA_VALUE:00.##&gt;" HorizontalAlign="Near" VerticalAlign="Center"
                                                            Orientation="Horizontal" Font="Verdana, 7pt" FontColor="DimGray">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="DimGray" FormatString="">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </Y>
                                                    <Y2 Visible="False" TickmarkInterval="0" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="" Visible="False" HorizontalAlign="Near" VerticalAlign="Center"
                                                            Orientation="Horizontal" Font="Verdana, 7pt" FontColor="Gray">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="Gray" FormatString="">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </Y2>
                                                    <X2 Visible="False" TickmarkInterval="0" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="" Visible="False" HorizontalAlign="Near" VerticalAlign="Center"
                                                            Orientation="Horizontal" Font="Verdana, 7pt" FontColor="Gray">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="Gray" FormatString="">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </X2>
                                                    <Z Visible="False" TickmarkInterval="0" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="" HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                            Font="Verdana, 7pt" FontColor="DimGray" Visible="False">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="DimGray">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </Z>
                                                    <Z2 Visible="False" TickmarkInterval="0" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="" Visible="False" HorizontalAlign="Near" VerticalAlign="Center"
                                                            Orientation="Horizontal" Font="Verdana, 7pt" FontColor="Gray">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="Gray">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </Z2>
                                                </Axis>
                                                <Effects>
                                                    <Effects>
                                                        <igchartprop:GradientEffect />
                                                    </Effects>
                                                </Effects>
                                                <Data UseRowLabelsColumn="True">
                                                </Data>
                                                <TitleTop Font="Arial, 11.25pt, style=Bold" Text="">
                                                </TitleTop>
                                                <Border Thickness="0" />
                                                <Tooltips Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" />
                                            </igchart:UltraChart>
                                        </td>
                                        <td valign="top" align="left">
                                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left">
                                                        <b>Customer Stage</b>&nbsp;
                                                        <asp:DropDownList ID="ddlCustomerStage" runat="server" AutoPostBack="true" CssClass="signup">
                                                            <asp:ListItem Text="Lead" Value="0">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Prospect" Value="1">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Account" Value="2">
                                                            </asp:ListItem>
                                                        </asp:DropDownList>
                                                        &nbsp;&nbsp; <b>Based on</b>&nbsp;
                                                        <asp:DropDownList ID="ddlNoOfDays" runat="server" AutoPostBack="true" CssClass="signup">
                                                            <asp:ListItem Text="Last 30 days" Value="30">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Last 60 days" Value="60">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Last 90 days" Value="90">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Last 120 days" Value="120">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Last 240 days" Value="240">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Last 360 days" Value="360">
                                                            </asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Table ID="Table2" runat="server" BorderWidth="1" Width="100%" Height="300" BorderColor="black"
                                                CssClass="aspTable" GridLines="None">
                                                <asp:TableRow>
                                                    <asp:TableCell VerticalAlign="Top" HorizontalAlign="Right">
                                                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="8" align="left">
                                                                    <b>Online Campaigns</b>
                                                                </td>
                                                            </tr>
                                                            <tr class="normal1" align="right">
                                                                <td>
                                                                    <b>Total amount paid / mo:&nbsp;</b>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblOnlineTotalAmtPaid" runat="server" Text=""></asp:Label>
                                                                </td>
                                                                <td align="right">
                                                                    <b>Total amount made / mo:&nbsp;</b>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblOnlineTotalAmtMade" runat="server" Text=""></asp:Label>
                                                                </td>
                                                                <td align="right">
                                                                    <b>ROI:&nbsp;</b>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblOnlineROI" runat="server" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="8" valign="top" align="left">
                                                                    <asp:DataGrid ID="dgOnlineCampaigns" runat="server" AllowSorting="false" CssClass="dg"
                                                                        AutoGenerateColumns="False" OnItemDataBound="dgOnlineCampaigns_ItemDataBound">
                                                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                                        <ItemStyle CssClass="is"></ItemStyle>
                                                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                                                        <Columns>
                                                                            <asp:BoundColumn Visible="False" DataField="numCampaignId"></asp:BoundColumn>
                                                                            <asp:TemplateColumn HeaderText="Campaign Name">
                                                                                <ItemTemplate>
                                                                                    <a href="#" onclick="OpenCampaign('<%# Eval("numCampaignId")%>')">
                                                                                        <%# Eval("CampaignName")%></a>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Cost/Mo" SortExpression="monCampaignCost">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCampaignCost" runat="server" Text='<%# String.Format("{0:#####0.00}",ReturnMoney(Eval("monCampaignCost"))) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Records Created" SortExpression="">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblRecordsCreated" runat="server" Text='<%# String.Format("{0:#####0.00}", Eval("RecordsCreated")) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Amt from Orders" SortExpression="income">
                                                                                <ItemTemplate>
                                                                                    <%#ReturnMoney(Eval("AmtFromOrders"))%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="ROI" SortExpression="ROI">
                                                                                <ItemTemplate>
                                                                                    <%#ReturnMoney(Eval("ROI"))%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Cost Per " SortExpression="">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCostPer" runat="server" Text=""></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </td>
        </tr>--%>
        <tr>
            <td align="center" valign="top" colspan="3">
                <telerik:RadPanelBar runat="server" ID="radPanOfflineCampaign" OnClientItemExpand="ExpandedStateChanged"
                    Width="100%" ExpandMode="MultipleExpandedItems" PersistStateInCookie="true">
                    <Items>
                        <telerik:RadPanelItem Text="Campaign Sources" Value="OfflineCampaignSources"
                            Width="100%">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="box box-primary direct-chat direct-chat-primary">
                                        <div class="box-header with-border">
                                        <h3 class="box-title">Share of Income</h3>
                                            </div>
                                        <igchart:UltraChart runat="server" ID="ucOfflineCampaign" BackgroundImageFileName=""
                                                Border-Thickness="0" ChartType="PieChart" EmptyChartText=""
                                                Version="9.1"  >
                                                <%--<ColorModel AlphaLevel="150" ColorBegin="Pink" ColorEnd="DarkRed" ModelStyle="CustomLinear">
                                                </ColorModel>--%>
                                                <PieChart>
                                                    <Labels FormatString="<ITEM_LABEL>: (<PERCENT_VALUE:#>%)" />
                                                </PieChart>
                                                <Axis>
                                                    <PE ElementType="None" Fill="Cornsilk"></PE>
                                                    <X Visible="True" TickmarkInterval="0" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="&lt;ITEM_LABEL&gt;" HorizontalAlign="Near" VerticalAlign="Center"
                                                            Orientation="Horizontal" Font="Verdana, 7pt" FontColor="DimGray">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="DimGray" FormatString="">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </X>
                                                    <Y Visible="True" TickmarkInterval="10" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="&lt;DATA_VALUE:00.##&gt;" HorizontalAlign="Near" VerticalAlign="Center"
                                                            Orientation="Horizontal" Font="Verdana, 7pt" FontColor="DimGray">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="DimGray" FormatString="">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </Y>
                                                    <Y2 Visible="False" TickmarkInterval="0" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="" Visible="False" HorizontalAlign="Near" VerticalAlign="Center"
                                                            Orientation="Horizontal" Font="Verdana, 7pt" FontColor="Gray">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="Gray" FormatString="">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </Y2>
                                                    <X2 Visible="False" TickmarkInterval="0" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="" Visible="False" HorizontalAlign="Near" VerticalAlign="Center"
                                                            Orientation="Horizontal" Font="Verdana, 7pt" FontColor="Gray">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="Gray" FormatString="">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </X2>
                                                    <Z Visible="False" TickmarkInterval="0" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="" HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                            Font="Verdana, 7pt" FontColor="DimGray" Visible="False">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="DimGray">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </Z>
                                                    <Z2 Visible="False" TickmarkInterval="0" LineThickness="1" TickmarkStyle="Smart">
                                                        <MajorGridLines Visible="True" DrawStyle="Dot" Color="Gainsboro" Thickness="1" AlphaLevel="255"></MajorGridLines>
                                                        <MinorGridLines Visible="False" DrawStyle="Dot" Color="LightGray" Thickness="1" AlphaLevel="255"></MinorGridLines>
                                                        <Labels ItemFormatString="" Visible="False" HorizontalAlign="Near" VerticalAlign="Center"
                                                            Orientation="Horizontal" Font="Verdana, 7pt" FontColor="Gray">
                                                            <SeriesLabels HorizontalAlign="Near" VerticalAlign="Center" Orientation="Horizontal"
                                                                Font="Verdana, 7pt" FontColor="Gray">
                                                                <Layout Behavior="Auto">
                                                                </Layout>
                                                            </SeriesLabels>
                                                            <Layout Behavior="Auto">
                                                            </Layout>
                                                        </Labels>
                                                    </Z2>
                                                </Axis>
                                                <Effects>
                                                    <Effects>
                                                        <igchartprop:GradientEffect />
                                                    </Effects>
                                                </Effects>
                                                <Data UseRowLabelsColumn="True">
                                                </Data>
                                                <TitleTop Font="Arial, 11.25pt, style=Bold" Text="">
                                                </TitleTop>
                                                <Border Thickness="0" />
                                                <Tooltips Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" />
                                            </igchart:UltraChart>
                                    </div>
                                </div>
                                    <div class="col-md-7">
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left">
                                                        <b>Customer Stage</b>&nbsp;
                                                        <asp:DropDownList ID="ddlCustomerStage" runat="server" AutoPostBack="true" CssClass="signup">
                                                            <asp:ListItem Text="Lead" Value="0">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Prospect" Value="1">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Account" Value="2">
                                                            </asp:ListItem>
                                                        </asp:DropDownList>
                                                        &nbsp;&nbsp; <b>Based on</b>&nbsp;
                                                        <asp:DropDownList ID="ddlNoOfDays" runat="server" AutoPostBack="true" CssClass="signup">
                                                            <asp:ListItem Text="Last 30 days" Value="30">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Last 60 days" Value="60">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Last 90 days" Value="90">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Last 120 days" Value="120">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Last 240 days" Value="240">
                                                            </asp:ListItem>
                                                            <asp:ListItem Text="Last 360 days" Value="360">
                                                            </asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Table ID="Table1" runat="server" Width="100%"
                                                GridLines="None">
                                                <asp:TableRow>
                                                    <asp:TableCell VerticalAlign="Top">
                                                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td colspan="8">
                                                                    <b>Campaigns</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="8">
                                                                    <table cellpadding="2" cellspacing="2" width="60%">
                                                                        <tr class="normal1">
                                                                            <td align="right">
                                                                                <b>Total amount paid :&nbsp;</b>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:Label ID="lblOfflineToalAmountPaid" runat="server" Text=""></asp:Label>
                                                                            </td>
                                                                            <td align="right">
                                                                                <b>Total amount made:&nbsp;</b>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:Label ID="lblOfflineToalAmountMade" runat="server" Text=""></asp:Label>
                                                                            </td>
                                                                            <td align="right">
                                                                                <b>ROI:&nbsp;</b>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:Label ID="lblOfflineROI" runat="server" Text=""></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="8" valign="top" align="left">
                                                                    <asp:DataGrid ID="dgOfflineCampaign" runat="server" AllowSorting="false"  CssClass="table table-responsive"
                                                                        AutoGenerateColumns="False" OnItemDataBound="dgOfflineCampaign_ItemDataBound">
                                                                       <%-- <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                                        <ItemStyle CssClass="is"></ItemStyle>
                                                                        <HeaderStyle CssClass="hs"></HeaderStyle>--%>
                                                                        <Columns>
                                                                            <asp:BoundColumn Visible="False" DataField="numCampaignId"></asp:BoundColumn>
                                                                            <asp:TemplateColumn HeaderText="Campaign Name">
                                                                                <ItemTemplate>
                                                                                    <a href="#" onclick="OpenCampaign('<%# Eval("numCampaignId")%>')">
                                                                                        <%# Eval("CampaignName")%></a>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Cost" SortExpression="monCampaignCost">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCampaignCost" runat="server" Text='<%# ReturnMoney(Eval("monCampaignCost")) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Associated Customers" SortExpression="">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblRecordsCreated" runat="server" Text='<%# Eval("RecordsCreated") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Amt from Orders" SortExpression="AmtFromOrders">
                                                                                <ItemTemplate>
                                                                                    <%#ReturnMoney(Eval("AmtFromOrders"))%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="ROI" SortExpression="ROI">
                                                                                <ItemTemplate>
                                                                                    <%#ReturnMoney(Eval("ROI"))%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Cost Per " SortExpression="">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCostPer" runat="server" Text=""></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                    </div>
                                </div>

                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </td>
        </tr>
        <%--  <tr>
            <td valign="top" align="left">
                <telerik:RadPanelBar runat="server" ID="radPanTop5SearchTerms" Width="100%" ExpandMode="MultipleExpandedItems">
                    <Items>
                        <telerik:RadPanelItem Text="Top 5 Search Terms" Expanded="True" Value="Top5SearchTerms"
                            Width="100%">
                            <ContentTemplate>
                                <asp:DataGrid ID="dgKeyWords" runat="server" AllowSorting="false" CssClass="dg" Width="100%"
                                    AutoGenerateColumns="False" ShowHeader="false">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="vcSearchTerm"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <%#Eval("Count").ToString() + "%"%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </td>
            <td valign="top" align="left">
                <telerik:RadPanelBar runat="server" ID="radTop5WebSources" Width="100%" ExpandMode="MultipleExpandedItems">
                    <Items>
                        <telerik:RadPanelItem Text="Top 5 Web Sources" Expanded="True" Value="Top5WebSources"
                            Width="100%">
                            <ContentTemplate>
                                <asp:DataGrid ID="dgWebSource" runat="server" AllowSorting="false" CssClass="dg"
                                    AutoGenerateColumns="False" ShowHeader="false">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="Source"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <%#CType(Eval("percentage"), Double).ToString("0.00") + " %"%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </td>
            <td valign="top" align="left">
                <telerik:RadPanelBar runat="server" ID="radTop5Sources" Width="100%" ExpandMode="MultipleExpandedItems">
                    <Items>
                        <telerik:RadPanelItem Text="Top 5 Sources" Expanded="True" Value="Top5Sources" Width="100%">
                            <ContentTemplate>
                                <asp:DataGrid ID="dgSources" runat="server" AllowSorting="false" CssClass="dg" Width="100%"
                                    AutoGenerateColumns="False" ShowHeader="false">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="ConclusionBasis"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <%#CType(Eval("Percentage"), Integer).ToString() + "%"%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </td>
             <td valign="top" align="right">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                       
                    </tr>
                </table>
            </td>
        </tr>--%>
        <tr>
            <td valign="top" align="left">
                <telerik:RadPanelBar runat="server" ID="RadPanTopLeadSources" Width="100%" ExpandMode="MultipleExpandedItems">
                    <Items>

                        <telerik:RadPanelItem Text="Top Actual Search Lead Sources with end results" Expanded="True" Value="TopLeadSources"
                            Width="100%">
                            <ContentTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:DropDownList ClientIDMode="Static" runat="server" ID="ddlLeadSources" Width="120px" AutoPostBack="true">
                                                <asp:ListItem Text="-- Select One --" Value="0" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Last 6 Months" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="Last 12 Months" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="Last 24 Months" Value="24"></asp:ListItem>
                                            </asp:DropDownList></td>
                                    </tr>
                                    <%-- <div style="text-align: center;">
                                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel1">
                                        <ProgressTemplate>
                                            <img alt="Loading..." src="../images/pgBar.gif" runat="server" id="pgBar1"/>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>--%>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlLeadSources" EventName="SelectedIndexChanged" />
                                                </Triggers>
                                                <ContentTemplate>
                                                    <asp:DataGrid ID="dgTopLeadSources" runat="server" AllowSorting="false" CssClass="table table-responsive" Width="100%"
                                                        AutoGenerateColumns="False" ShowHeader="true">
                                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="is"></ItemStyle>
                                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Source Domain">
                                                                <ItemTemplate>
                                                                    <asp:Label ClientIDMode="Static" runat="server" ID="lblSourceDomain" Text='<%# Eval("SourceDomain")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>

                                                            <asp:TemplateColumn HeaderText="% of Total">
                                                                <ItemTemplate>
                                                                    <%#Eval("TotalPercentage") + " %"%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Leads / Prospects">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton CssClass="leadsList" ID="lnkbtnLeadsList" runat="server" OnClientClick="leadsList(this);"
                                                                        CommandArgument='<%# Eval("LeadsCount")%>' Text='<%# Eval("LeadsCount")%>'></asp:LinkButton>

                                                                    <%--<%#CType(Eval("LeadsCount"), Double).ToString("0")%>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Accounts(Conv %)">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkbtnAccountsList" CssClass="accounts" runat="server" OnClientClick="accounts(this);"
                                                                        CommandArgument='<%# Eval("AccountsCount")%>' Text='<%# Eval("AccountsCount")%>'></asp:LinkButton>

                                                                    <%#" (" + Eval("AccountsPercentage") + " %)"%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Amount Generated">
                                                                <ItemTemplate>
                                                                    <%#Eval("AmtGenerated")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%--  <asp:BoundColumn DataField="TotalPercentage" HeaderText="% of Total"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="LeadsCount" HeaderText="Leads/Prospects"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="strAccountsCount" HeaderText="Accounts(Conv %)"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="AmountGenerated" HeaderText="Amount Generated"></asp:BoundColumn>--%>

                                                            <%-- <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <%#Eval("visits").ToString() + "%"%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                         <asp:BoundColumn DataField="vcPagesperVisit"></asp:BoundColumn>--%>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>



                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </td>
            <td valign="top" align="left">
                <telerik:RadPanelBar runat="server" ID="RadPanUniqueVisitors" Width="100%" ExpandMode="MultipleExpandedItems">
                    <Items>
                        <telerik:RadPanelItem Text="Unique Visitors" Expanded="True" Value="UniqueVisitors"
                            Width="100%">
                            <ContentTemplate>

                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddlUniqueVisitors" Width="120px" AutoPostBack="true">

                                                <asp:ListItem Text="-- Select One --" Value="0" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Last 6 Months" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="Last 12 Months" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="Last 24 Months" Value="24"></asp:ListItem>

                                            </asp:DropDownList></td>
                                    </tr>
                                    <div style="text-align: center;">
                                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="uppnlUniqueVisitors">
                                            <ProgressTemplate>
                                                <img alt="Loading..." src="../images/pgBar.gif" runat="server" id="pgBar" />
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </div>

                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="uppnlUniqueVisitors" runat="server">
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlUniqueVisitors" EventName="SelectedIndexChanged" />
                                                </Triggers>
                                                <ContentTemplate>

                                                    <asp:DataGrid ID="dgUniqueVisitors" runat="server" AllowSorting="false" CssClass="dg" Width="100%"
                                                        AutoGenerateColumns="False" ShowHeader="true">
                                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="is"></ItemStyle>
                                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundColumn DataField="vcMonth" HeaderText="Year / Month"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Visits" HeaderText="Visits"></asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>


                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </td>

            <td valign="top" align="left">
                <telerik:RadPanelBar runat="server" ID="RadPanTopTrafficSources" Width="100%" ExpandMode="MultipleExpandedItems">
                    <Items>
                        <telerik:RadPanelItem Text="Top 10 Traffic Sources" Expanded="True" Value="TopTrafficSources"
                            Width="100%">
                            <ContentTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddlTopTrafficSources" Width="120px" AutoPostBack="true">
                                                <asp:ListItem Text="-- Select One --" Value="0" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Last 6 Months" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="Last 12 Months" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="Last 24 Months" Value="24"></asp:ListItem>
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <asp:DataGrid ID="dgTopSources" runat="server" AllowSorting="false" CssClass="dg" Width="100%"
                                                AutoGenerateColumns="False" ShowHeader="true">
                                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                <ItemStyle CssClass="is"></ItemStyle>
                                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="vcSource" HeaderText="Source"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Visits" HeaderText="Visits"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Percentage">
                                                        <ItemTemplate>

                                                            <div>
                                                                <%#Eval("Percentage").ToString() + "%"%>
                                                                <div id="TotalProgressContainer0" class="ProgressContainer" style="width: 100px; height: 5px;">
                                                                    &nbsp;
                                                    <div id="TotalProgress0" class="ProgressBar" style="height: 5px; width: <%#Eval("Percentage")%>px">&nbsp;</div>
                                                                </div>
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </td>

        </tr>
    </table>
    <asp:HiddenField ID="hdnPanel1" runat="server" Value="false" />
    <asp:HiddenField ID="hdnPanel2" runat="server" Value="false" />

    <asp:HiddenField ID="hdnGA_UserId" runat="server" Value="" />
    <asp:HiddenField ID="hdnGA_Password" runat="server" Value="" />
    <asp:HiddenField ID="hdnGA_ProfileID" runat="server" Value="" />


</asp:Content>
