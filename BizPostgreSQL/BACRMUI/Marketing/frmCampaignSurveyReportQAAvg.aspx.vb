''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Survey
''' Class	 : frmCampaignSurveyReportQAAvg
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a interface for displaying the Q & A Agerages for the Survey
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	10/02/2005	Created
''' </history>
''' ''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Survey
    Public Class frmCampaignSurveyReportQAAvg
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnPrint As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnBack As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.LinkButton
        Protected WithEvents tblSurveyQAAvg As System.Web.UI.WebControls.Table
        Protected WithEvents ddlGraphType As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlChartColor As System.Web.UI.WebControls.DropDownList
        Protected WithEvents lblSurveyName As System.Web.UI.WebControls.Label
        Dim numThisSurveyId As Integer
        Protected WithEvents lblSurNameLbl As System.Web.UI.WebControls.Label

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime the page is called. In this event we will 
        '''     get the data from the DB and populate the Tables and the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                numThisSurveyId = GetQueryStringVal( "numSurID")               'Get this Survey Id
                'Put user code to initialize the page here
                DisplaySurveyQAAgerages()                                       'Call to display Q & A Averagrs
                PostInitializeControlsClientEvents()                            'Call to attache the client events
                 ' = "MarSurvey"
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the Q & A Averages from the database
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function DisplaySurveyQAAgerages()
            Try
                Dim objSurvey As New SurveyAdministration                                   'Declare and create a object of SurveyAdministration
                objSurvey.SurveyId = GetQueryStringVal("numSurID")                        'Set the Survey Id as it comes from the query parameter
                objSurvey.SurveyInfo = objSurvey.getSurveyInformation4Execution             'Get the Survey Information
                objSurvey.SurveyInfo.Tables(0).TableName = "SurveyMaster"                   'Set the name of the table of Survey Master Infor
                objSurvey.SurveyInfo.Tables(1).TableName = "SurveyQuestionMaster"           'Set the name of the table of Questions
                objSurvey.SurveyInfo.Tables(2).TableName = "SurveyAnsMaster"                'Set the name of the table of Answers
                objSurvey.SurveyInfo.Tables(3).TableName = "SurveyWorkflowRules"            'Set the name of the table of Work Flow Rules
                objSurvey.SurveyInfo.Tables(4).TableName = "SurveyMatrixMaster"            'Set the name of the table of Work Flow Rules

                lblSurveyName.Text = objSurvey.SurveyInfo.Tables(0).Rows(0).Item("vcSurName") 'Display the Survey name

                CallForQuestionnaireGeneration(objSurvey)
            Catch ex As Exception
                Throw ex
            End Try
            'Call to generate the Questions
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to create the interface for Survey Questions.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Function CallForQuestionnaireGeneration(ByRef objSurvey As SurveyAdministration)
            Try
                Dim drTableRow As DataRow                                               'Declare a Data Row Obejct
                Dim tblRowQ, tblRowA As TableRow                                        'Declare a TableRow Object
                Dim tblCell As TableCell                                                'Declare a TableCell Object
                Dim numThisSurveyQuestionRowIndex As Integer                            'Declare an Integer Row Index for tracking this Survey (Only)
                Dim numBlankRowIndex As Int16                                           'Declare blank row index
                Dim sTBListScript As New System.Text.StringBuilder                      'Declare a StringBuilder to contain the Table Rows ids
                objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow = 0  'Reset the Current Question Row Index
                objSurvey.SurveyExecution.SurveyExecution().NumberOfQuestions = objSurvey.SurveyInfo.Tables("SurveyQuestionMaster").Rows.Count 'Init Number of Questions
                While objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow < objSurvey.SurveyExecution.SurveyExecution().NumberOfQuestions
                    drTableRow = objSurvey.SurveyInfo.Tables("SurveyQuestionMaster").Rows(objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow)
                    objSurvey.SurveyId = drTableRow.Item("numSurID")                    'Set the Survey ID for each row
                    tblRowQ = New TableRow                                              'Instantiate a new TableRow Object
                    tblRowQ.Height = Unit.Pixel(18)                                     'Set the height of the row
                    tblRowQ.Attributes.Add("class", "ais")                              'Sets teh color of the row

                    tblCell = New TableCell                                             'Instantiate a New TableCell
                    tblCell.CssClass = "normal1"                                        'Set the Css Class
                    tblCell.HorizontalAlign = HorizontalAlign.Left                      'Left Align the questions
                    tblCell.VerticalAlign = VerticalAlign.Top                           'Top align the question
                    tblCell.Style.Add("PADDING-LEFT", "5px")                            'Left Pad teh data
                    tblCell.Text = "Q." & objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow + 1 'Display the Question Number
                    tblRowQ.Cells.Add(tblCell)                                          'Add the Table Cell to the Row

                    tblCell = New TableCell                                             'Instantiate a New TableCell
                    tblCell.ColumnSpan = 2                                              'Set the column span to 2
                    tblCell.CssClass = "normal1"                                        'Set the Css Class
                    tblCell.HorizontalAlign = HorizontalAlign.Left                      'Left Align the questions
                    tblCell.VerticalAlign = VerticalAlign.Top                           'Top align the question
                    tblCell.Style.Add("PADDING-LEFT", "5px;")                           'Left Pad teh data
                    tblCell.Text = Server.HtmlDecode(drTableRow.Item("vcQuestion"))     'Display the Question
                    tblRowQ.Cells.Add(tblCell)                                          'Add the Table Cell to the Row
                    tblSurveyQAAvg.Rows.Add(tblRowQ)                                    'Add the row to teh table

                    objSurvey.QuestionId = drTableRow.Item("numQID")                    'Associate the Questio Id
                    CallForAnswerGeneration(objSurvey, drTableRow.Item("numQID"), drTableRow.Item("tintAnsType")) 'Call to display teh answers

                    objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow += 1 'Increment the Row index in Question Table
                End While
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to create the interface for Survey Answers inside a question.
        ''' </summary>
        ''' <param name="objSurvey">Class Object containing the Dataset of Survey Information</param>
        ''' <param name="tblRow">The TableRow to which Answers will be added</param>
        ''' <param name="numQID">The Question Id</param>
        ''' <param name="tIntAnsType">The Answer Type</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Function CallForAnswerGeneration(ByRef objSurvey As SurveyAdministration, ByVal numQID As Integer, ByVal tIntAnsType As Integer)
            Try
                Dim dtAnswers As DataTable                                                  'Declare a DataTable
                Dim drAnswer As DataRow                                                     'Declare a DataRow
                dtAnswers = objSurvey.GetAnswersAndResponseForSurveyQuestion(numThisSurveyId) 'Request for Answers related to the Sruvey and also get the nos of responses
                If dtAnswers.Rows.Count = 0 Then Exit Function
                If ddlGraphType.SelectedItem.Value = "bar" Or ddlGraphType.SelectedItem.Value = "pie" Or ddlGraphType.SelectedItem.Value = "column" Then
                    Dim tblCell As New TableCell                                            'Instantiate a New TableCell
                    tblCell.ColumnSpan = 2                                                  '2 Columns
                    tblCell.CssClass = "normal1"                                            'Set the Css Class
                    tblCell.HorizontalAlign = HorizontalAlign.Left                          'Left Align the questions
                    tblCell.Style.Add("PADDING-LEFT", "40px")                               'Pad teh data from all three sides
                    tblCell.Style.Add("PADDING-TOP", "5px")
                    tblCell.Style.Add("PADDING-BOTTOM", "15px")
                    Dim sTBGraphXParams As New System.Text.StringBuilder                    'Declare a StringBuilder to contain the Graph X Params
                    Dim sTBGraphYParams As New System.Text.StringBuilder                    'Declare a StringBuilder to contain the Graph Y Params
                    For Each drAnswer In dtAnswers.Rows
                        sTBGraphXParams.Append(Replace(Replace(Server.HtmlDecode(ReSetFromXML(drAnswer.Item("vcAnsLabel"))), "|", ":"), "#", ":") & "|") 'Set the label
                        sTBGraphYParams.Append(drAnswer.Item("numResponses") & "|")         'Set the Count 
                        objSurvey.AnswerId = drAnswer.Item("numAnsID")                      'Set the Answer ID
                        CallForAnsForkedQuestionnaireGeneration(objSurvey, drAnswer)        'If selection of an answer gives rise to forking to another question
                    Next
                    Dim strTBGraphXParams As String = sTBGraphXParams.ToString(0, sTBGraphXParams.ToString.Length - 1)
                    Dim strTBGraphYParams As String = sTBGraphYParams.ToString(0, sTBGraphYParams.ToString.Length - 1)
                    tblCell.Controls.Add(New LiteralControl("<IMG src='../admin/ChartGenerator.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&xValues=" & strTBGraphXParams & "&yValues=" & strTBGraphYParams & "&ChartType=" & ddlGraphType.SelectedItem.Value & "&Print=" & ddlChartColor.SelectedItem.Value & "'>"))
                    Dim tblRowA As New TableRow                                             'Instantiate a new TableRow Object
                    tblCell.ColumnSpan = 2
                    tblRowA.Cells.Add(tblCell)                                              'Add the Table Cell to the Row
                    tblSurveyQAAvg.Rows.Add(tblRowA)                                        'Add the row to teh table
                ElseIf ddlGraphType.SelectedItem.Value = "tab" Then
                    Dim numTotalResponses As Integer = CInt(dtAnswers.Compute("Sum(numResponses)", "")) 'Compute the total number of responses
                    For Each drAnswer In dtAnswers.Rows
                        Dim tblCellA As New TableCell                                       'Instantiate a New TableCell
                        tblCellA.ColumnSpan = 2
                        tblCellA.CssClass = "normal1"                                       'Set the Css Class
                        tblCellA.Style.Add("PADDING-LEFT", "40px")                          'Pad the data from the left
                        tblCellA.HorizontalAlign = HorizontalAlign.Left                     'Left Align the answers
                        Dim tblCellR As New TableCell                                       'Instantiate a New TableCell
                        tblCellR.CssClass = "normal1"                                       'Set the Css Class
                        tblCellR.Width = Unit.Percentage(30)                                'Set the width fo the num of responses col
                        tblCellR.HorizontalAlign = HorizontalAlign.Left                     'Left Align the Number of responses
                        Dim tblRowA As New TableRow                                         'Instantiate a new TableRow Object
                        tblRowA.Height = Unit.Pixel(18)                                     'Set the height of the row

                        tblCellA.Text = Replace(Replace(Server.HtmlDecode(drAnswer.Item("vcAnsLabel")), "|", ":"), "#", ":")    'Set the text to teh amswer
                        tblCellR.Text = drAnswer.Item("numResponses") & " (" & Math.Round(drAnswer.Item("numResponses") / numTotalResponses * 100, 2) & "%)" 'Set the text to the number of responses

                        tblRowA.Cells.Add(tblCellA)                                         'Add the Table Cell to the Row
                        tblRowA.Cells.Add(tblCellR)                                         'Add the Table Cell to the Row
                        tblSurveyQAAvg.Rows.Add(tblRowA)                                    'Add the row to teh table
                        CallForAnsForkedQuestionnaireGeneration(objSurvey, drAnswer)        'If selection of an answer gives rise to forking to another question
                    Next
                    Dim tblCell1Total As New TableCell                                      'Instantiate a New TableCell
                    tblCell1Total.ColumnSpan = 2                                            'Column Span = 2
                    tblCell1Total.HorizontalAlign = HorizontalAlign.Right                   'Right Align the label
                    Dim tblCell2Total As New TableCell                                      'Instantiate a New TableCell
                    tblCell2Total.HorizontalAlign = HorizontalAlign.Left                    'Left Align the value
                    Dim tblRowTotal As New TableRow                                         'Instantiate a new TableRow Object

                    tblCell1Total.Text = "Total Responses: "                                          'Set the text to Total
                    tblCell2Total.Text = numTotalResponses                                  'Set the text to the total number of responses

                    tblRowTotal.Cells.Add(tblCell1Total)                                    'Add the Table Cell to the Row
                    tblRowTotal.Cells.Add(tblCell2Total)                                    'Add the Table Cell to the Row
                    tblSurveyQAAvg.Rows.Add(tblRowTotal)                                    'Add the row to teh table
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function ReSetFromXML(ByVal sValue As String) As String
            Try
                Return Replace(Replace(Replace(Replace(sValue, "&amp;", "&"), "&#39;", "'"), "&lt;", "<"), "&gt;", ">")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to insert questions (if forking is there).
        ''' </summary>
        ''' <param name="objSurvey">Class Object containing the Dataset of Survey Information</param>
        ''' <param name="tblDataRow">The DataRow containing the Answer </param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Function CallForAnsForkedQuestionnaireGeneration(ByRef objSurvey As SurveyAdministration, ByVal tblAnsDataRow As DataRow)
            Try
                If tblAnsDataRow.Item("boolSurveyRuleAttached") Then                            'If a Rule is attached
                    Dim dtWorkFlowRules As DataTable                                            'Declare a DataTable of Rules
                    dtWorkFlowRules = objSurvey.GetSurveyWorkflowRulesDetailsForSurveyAnswer()  'Get the Rules for the Survey Answer
                    Dim drWorkFlowRule As DataRow                                               'Declare a DataRow
                    For Each drWorkFlowRule In dtWorkFlowRules.Rows                             'Loop through the rules
                        Select Case drWorkFlowRule.Item("numRuleID")
                            Case 1
                                Dim dsForkedSurveyQuestionAnswers As DataSet = objSurvey.GetForkedSurveyQuestionAnswer(drWorkFlowRule.Item("numLinkedSurID"), drWorkFlowRule.Item("vcQuestionList")) 'Fork to get the other survey's questions and answers
                                Dim dtForkedSurveyQuestions As DataTable                    'Declare Question DataTables
                                Dim dtForkedSurveyAnswers As DataTable                      'Declare Answer DataTables
                                dtForkedSurveyQuestions = dsForkedSurveyQuestionAnswers.Tables(0)  'Get the Questions Table
                                dtForkedSurveyAnswers = dsForkedSurveyQuestionAnswers.Tables(1) 'Get the Answers Table
                                Dim drForkedSurveyQuestions, drForkedSurveyAnswers As DataRow   'Declare a DataRow object
                                Dim drForkedSurveyQuestionCopy As DataRow
                                Dim intNewQuestionIndex As Integer                              'Declare an integer
                                For intNewQuestionIndex = dtForkedSurveyQuestions.Rows.Count - 1 To 0 Step -1
                                    drForkedSurveyQuestions = dtForkedSurveyQuestions.Rows(intNewQuestionIndex)
                                    drForkedSurveyQuestionCopy = objSurvey.SurveyInfo.Tables("SurveyQuestionMaster").NewRow 'Create a New Row object
                                    drForkedSurveyQuestionCopy.ItemArray = drForkedSurveyQuestions.ItemArray
                                    objSurvey.SurveyInfo.Tables("SurveyQuestionMaster").Rows.InsertAt(drForkedSurveyQuestionCopy, objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow + 1) 'Insert teh Question Row at 1st postion as the 1st question does nto change to another survey
                                Next
                                objSurvey.SurveyExecution.SurveyExecution().NumberOfQuestions += dtForkedSurveyQuestions.Rows.Count 'Increment teh nos of Questions
                                For Each drForkedSurveyAnswers In dtForkedSurveyAnswers.Rows
                                    objSurvey.SurveyInfo.Tables("SurveyAnsMaster").ImportRow(drForkedSurveyAnswers) 'Import the answers rows
                                Next
                        End Select
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the back button is clicked
        ''' </summary>
        ''' <remarks> Redirects to the Survey Listing Page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("frmCampaignSurveyList.aspx", False)                       'Redirect to the Survey List page
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to attach client side events to the controls
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub PostInitializeControlsClientEvents()
            Try
                btnExportToExcel.Attributes.Add("onclick", "javascript: return ExportToExcel(" & numThisSurveyId & ",'" & ddlGraphType.SelectedItem.Value & "');") 'call to open the graph in a new popup
                btnPrint.Attributes.Add("onclick", "javascript: return GoForPrint();") 'call to open the graph in a new popup
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
    End Class
End Namespace
