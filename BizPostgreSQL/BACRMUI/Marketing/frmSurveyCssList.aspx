﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSurveyCssList.aspx.vb"
    Inherits=".frmSurveyCssList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript" language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function NewCssAndJs() {
            if (document.form1.hdnType.value < 2) {
                if (document.form1.SiteSwitch1_ddlSites.selectedIndex == 0) {
                    var hplNew = document.getElementById("hplNew");
                    hplNew.href = "#";
                    alert("Please select a site to upload " + hplNew.innerHTML);
                    document.form1.SiteSwitch1_ddlSites.focus();
                    return false;
                }
            }
            return true;
        }
    </script>
    <style>
           .table {
        border-color:#CACACA
        }
            .table > tbody > tr:first-child {
            background:rgba(225, 225, 225, 0.23);
            font-weight:bold;
            }
                .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table align="right">
        <tr>
            <td >
                <asp:HyperLink ID="hplNew" runat="server" CssClass="btn btn-primary" ><i class="fa fa-plus-circle"></i>&nbsp;New Style</asp:HyperLink>&nbsp;&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblName" runat="server" Text="Survey Styles"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:DataGrid ID="dgStyles" AllowSorting="false" runat="server" Width="100%" CssClass="table table-responsive"
        AutoGenerateColumns="False">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn Visible="false" DataField="numCssID" HeaderText="numCssID"></asp:BoundColumn>
            <asp:ButtonColumn DataTextField="StyleName" SortExpression="" HeaderText="Name" CommandName="Edit">
            </asp:ButtonColumn>
            <asp:BoundColumn DataField="StyleFileName" SortExpression="" HeaderText="File Name">
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete">
                    </asp:Button>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
     <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
