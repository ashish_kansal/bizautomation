Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.IO
Imports System.Math

Namespace BACRM.UserInterface.Admin
    Public Class frmEmailCampaignProfileSearch
        Inherits BACRMPage


        Dim objGenericAdvSearch As New FormGenericAdvSearch
        Dim dtGenericFormConfig As DataTable                                        'Declare a DataTable 
        Dim objCommon As CCommon

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                objCommon = New CCommon
                GetUserRightsForPage(6, 3)
                If Not IsPostBack Then
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    LoadDropdowns()
                    'FillItemLists()
                    'copyTableDataFromOneToAnother(dtGenericFormConfig, dtGenericFormAOIExtraFields) 'Copy the table data
                    callToBindRelationships()
                End If
                With objGenericAdvSearch
                    .DomainID = Session("DomainID")                                             'Set the domain id
                    .FormID = 1                                                                 'Set the form Id (Adv. Search=1)
                    .UserCntID = Session("UserContactID")                                                 'Set the User ID
                    .XMLFilePath = CCommon.GetDocumentPhysicalPath() 'set the file path for storing the xml fiel containing the user's search criteria
                End With

                GenericFormControlsGeneration.FormID = 2                                        'set the form id to Advance Search
                GenericFormControlsGeneration.DomainID = Session("DomainID")                    'Set the domain id
                GenericFormControlsGeneration.UserCntID = Session("UserContactID")                        'Set the User id
                GenericFormControlsGeneration.AuthGroupId = Session("UserGroupID")              'Set the User Authentication Group Id
                GenericFormControlsGeneration.sXMLFilePath = CCommon.GetDocumentPhysicalPath() 'set the file path

                ' = "Advanced Search"

                GetUserRightsForPage(9, 8)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then                             'Commerce Marketing rights not given
                    trCommerceMarketing.Visible = False                                     'Hide the link
                End If

                dtGenericFormConfig = GenericFormControlsGeneration.getFormControlConfig()  'get the datatable to contain the form config
                dtGenericFormConfig.TableName = "AdvSearchFields"                           'give a name to the datatable
                If dtGenericFormConfig.Rows.Count = 0 Then                                  'if the xml for form fields has not been configured
                    'and exit the flow
                Else
                    cbSearchInLeads.Attributes.Add("onclick", "javascript: MakeLeadsCheckBoxSelectionConsistant(this);") 'Ensure consistancy of selection of Leads CheckBox
                    ddlLead.Attributes.Add("onchange", "javascript: MakeLeadsOptionSelectionConsistant();") 'Ensure consistancy of selection of Leads CheckBox
                    cbCreatedOnFilteration.Attributes.Add("onclick", "javascript: UncheckOtherDateFilteration('cbModifiedOnFilteration')") 'Call to uncheck the other Modification Date filteration  checkbox
                    cbModifiedOnFilteration.Attributes.Add("onclick", "javascript: UncheckOtherDateFilteration('cbCreatedOnFilteration')") 'Call to uncheck the other Creation Date filteration checkbox                                        'Call to attach client side events
                End If

                callFuncForFormGenerationNonAOI(dtGenericFormConfig)                        'Calls function for form generation and display for non AOI fields
                Dim dvConfig As DataView
                dvConfig = New DataView(dtGenericFormConfig)
                dvConfig.RowFilter = " vcDbColumnName like '%Country'"
                If dvConfig.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To dvConfig.Count - 1
                        If dvConfig(i).Item("vcDbColumnName") = "vcBillCountry" Then
                            If Not CType(tblSearch.FindControl("vcBilState"), DropDownList) Is Nothing Then
                                Dim dl As DropDownList
                                dl = CType(tblSearch.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                dl.AutoPostBack = True
                                AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                            End If
                        Else
                            If Not CType(tblSearch.FindControl(Replace(dvConfig(i).Item("vcDbColumnName"), "Country", "State")), DropDownList) Is Nothing Then
                                Dim dl As DropDownList
                                dl = CType(tblSearch.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                dl.AutoPostBack = True
                                AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                            End If
                        End If
                    Next
                End If
                'Dim dtGenericFormAOIExtraFields As DataTable                                'Create a table that holds the extra AOi fields which are not yet configured
                callFuncForFormGenerationAOI()                'Calls function to display the complete list of AOI
                'Calls the function to bind Relationships
                litClientScript.Text = GenericFormControlsGeneration.getJavascriptArray(dtGenericFormConfig) 'Create teh javascript array and store

                'Dim dtGenericFormCustomFields As DataTable                                  'Create a table that holds the custom field areas
                'dtGenericFormCustomFields = callFuncForFormGenerationCustomFieldAreas()     'Calls function to get the list of fuctom field areas
                'litClientScript.Text &= vbCrLf & GenericFormControlsGeneration.getCustomFieldJavascriptArray(dtGenericFormCustomFields) 'Create teh javascript array and store Custom Areas
                'callFunctionForTeamsAndTerritories()                                        'Call the fucntion which binds the teams and territories for this user
                'callFunctionForBindingColumnsInView()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Public Sub FillItemLists()
        '    Try
        '        Dim objGenericAdvSearch As New FormGenericAdvSearch                     'Create an object of the class FormGenericAdvSearch
        '        lstAvailablefld.DataSource = objGenericAdvSearch.GetItemList().DefaultView 'sets item list as xml
        '        objGenericAdvSearch.DomainID = Session("DomainID")
        '        lstAvailablefld.DataTextField = "vcItemName"
        '        lstAvailablefld.DataValueField = "numItemCode"
        '        lstAvailablefld.DataBind()
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Sub callToBindRelationships()
            Try
                Dim dtRelations As DataTable                                            'declare a datatable
                dtRelations = objGenericAdvSearch.getListDetails(5) 'call to get the relationships
                Dim dRow As DataRow                                                     'declare a datarow
                dRow = dtRelations.NewRow()                                             'get a new row object
                dtRelations.Rows.InsertAt(dRow, 0)                                      'insert the row
                dtRelations.Rows(0).Item("numItemID") = 0                               'set the value for first entry
                dtRelations.Rows(0).Item("vcItemName") = "All Relationships"            'set the text for the first entey
                dtRelations.Rows(0).Item("vcItemType") = "L"                            'type= list
                dtRelations.Rows(0).Item("flagConst") = 0                               'requried by the stored procedure
                ddlRelationship.DataSource = dtRelations.DefaultView                    'set the datasource
                ddlRelationship.DataTextField = "vcItemName"                            'set the text attribute
                ddlRelationship.DataValueField = "numItemID"                            'set the value attribute
                ddlRelationship.DataBind()                                              'databind the drop down
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub callFuncForFormGenerationNonAOI(ByVal dtGenericFormConfig As DataTable)
            Try
                GenericFormControlsGeneration.boolAOIField = 0                                      'Set the AOI flag to non AOI
                GenericFormControlsGeneration.createFormControls(dtGenericFormConfig, tblSearch)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function callFuncForFormGenerationAOI() As DataTable
            Try
                Dim dsFormConfig As DataSet                                                                       'Declare a DataTable
                objGenericAdvSearch.AuthenticationGroupID = Session("UserGroupID")
                objGenericAdvSearch.FormID = 2
                objGenericAdvSearch.DomainID = Session("DomainID")
                dsFormConfig = objGenericAdvSearch.getAOIList()                                                     'Get the AOIList
                GenericFormControlsGeneration.createFormControlsAOI(dsFormConfig.Tables(1), tblSearch)  'calls to create the form controls and add it to the form
                'Return dtFormConfig                                                                                 'Return the datatable which contains the AOI List
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub copyTableDataFromOneToAnother(ByRef dtGenericFormConfig As DataTable, ByVal dtGenericFormAOIExtraFields As DataTable)
            Try
                Dim drChildTableRows, drParentTableRows As DataRow                      'Declare a Datarow Object
                Dim dcParentColumn As DataColumn                                        'Declare a DataColumn Object
                Dim iIndex As Integer = 0                                               'Declare a integer object and initialize
                For Each drChildTableRows In dtGenericFormAOIExtraFields.Rows           'Loop through the row collection
                    drParentTableRows = dtGenericFormConfig.NewRow()                    'Crete a new row object of the first table
                    For Each dcParentColumn In dtGenericFormConfig.Columns              'Loop through the column collection of parent table
                        If dtGenericFormAOIExtraFields.Columns.Contains(dcParentColumn.ColumnName) Then 'Check if the column is contained in the child table
                            drParentTableRows.Item(dcParentColumn.ColumnName) = IIf(dcParentColumn.ColumnName = "intRowNum", iIndex, drChildTableRows.Item(dcParentColumn.ColumnName)) 'Copy the value from the child to the parent column
                        End If
                    Next
                    dtGenericFormConfig.Rows.Add(drParentTableRows)                     'Add the row to the table
                    iIndex += 1                                                         'Increment teh counter
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                Dim str As String = ""  ''' 4 means Not in the database
                str = " and tintCRMType in (" & IIf(cbSearchInLeads.Checked = True, 0, 4) & "," & IIf(cbSearchInProspects.Checked = True, 1, 4) & "," & IIf(cbSearchInAccounts.Checked = True, 2, 4) & ")"
                If cbCreatedOnFilteration.Checked = True Then
                    If calFrom.SelectedDate <> "" And calTo.SelectedDate <> "" Then
                        str = str & " and DM.bintCreatedDate between '" & calFrom.SelectedDate & "'::TIMESTAMP and  '" & calTo.SelectedDate & "'::TIMESTAMP"
                    End If
                ElseIf cbModifiedOnFilteration.Checked = True Then
                    If calFrom.SelectedDate <> "" And calTo.SelectedDate <> "" Then
                        str = str & " and DM.bintModifiedDate between '" & calFrom.SelectedDate & "'::TIMESTAMP and  '" & calTo.SelectedDate & "'::TIMESTAMP"
                    End If
                End If

                If ddlRelationship.SelectedIndex > 0 Then str = str & " and numCompanyType=" & ddlRelationship.SelectedValue
                If ddlLead.SelectedIndex > 0 Then str = str & " and numGrpId=" & ddlLead.SelectedValue

                Dim i As Integer
                For i = 0 To dtGenericFormConfig.Rows.Count - 1
                    If dtGenericFormConfig.Rows(i).Item("vcAssociatedControlType") = "SelectBox" Then
                        If dtGenericFormConfig.Rows(i).Item("vcFieldType") = "R" Then
                            If CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), DropDownList).SelectedIndex > 0 Then
                                str = str & " and " & dtGenericFormConfig.Rows(i).Item("vcDbColumnName") & " = " & CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), DropDownList).SelectedValue
                            End If
                        ElseIf dtGenericFormConfig.Rows(i).Item("vcFieldType") = "C" Then
                            If CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), DropDownList).SelectedIndex > 0 Then
                                str = str & " and ADC.numContactID in (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & Replace(Replace(dtGenericFormConfig.Rows(i).Item("numFormFieldID"), "C", ""), "D", "") & " and Fld_Value = " & CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), DropDownList).SelectedValue & " )"
                            End If
                        ElseIf dtGenericFormConfig.Rows(i).Item("vcFieldType") = "D" Then
                            If CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), DropDownList).SelectedIndex > 0 Then
                                str = str & " and DM.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID =" & Replace(Replace(dtGenericFormConfig.Rows(i).Item("numFormFieldID"), "D", ""), "C", "") & " and Fld_Value = " & CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), DropDownList).SelectedValue & " )"
                            End If
                        End If
                    Else
                        If CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), TextBox).Text <> "" Then
                            If dtGenericFormConfig.Rows(i).Item("vcFieldType") = "R" Then
                                If dtGenericFormConfig.Rows(i).Item("vcDbColumnName") = "numAge" Then
                                    If IsNumeric(CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), TextBox).Text) = False Then
                                        Response.Write("Please enter numeric value")
                                    Else : str = str & " and Year(bintDOB) = '" & Year(DateAdd(DateInterval.Year, -CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), TextBox).Text, Now())) & "'"
                                    End If
                                ElseIf dtGenericFormConfig.Rows(i).Item("vcDbColumnName") = "vcPostalCode" Or dtGenericFormConfig.Rows(i).Item("vcDbColumnName") = "vcBillPostCode" Or dtGenericFormConfig.Rows(i).Item("vcDbColumnName") = "vcShipPostCode" Then
                                    If CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), TextBox).Text <> "" Then
                                        str = str & "  and " & dtGenericFormConfig.Rows(i).Item("vcDbColumnName") & " like  '%" & Replace(CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), TextBox).Text, "'", "''") & "%'"
                                    End If
                                Else : str = str & " and " & dtGenericFormConfig.Rows(i).Item("vcDbColumnName") & " like  '" & IIf(rbListSearchContext.SelectedValue = 1, "%", "") & Replace(CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), TextBox).Text, "'", "''") & "%' "
                                End If
                            ElseIf dtGenericFormConfig.Rows(i).Item("vcFieldType") = "C" Then
                                str = str & " and ADC.numContactID in (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & Replace(Replace(dtGenericFormConfig.Rows(i).Item("numFormFieldID"), "C", ""), "D", "") & " and Fld_Value ilike '" & IIf(rbListSearchContext.SelectedValue = 1, "%", "") & Replace(CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), TextBox).Text, "'", "''") & "%' )"
                            ElseIf dtGenericFormConfig.Rows(i).Item("vcFieldType") = "D" Then
                                str = str & " and DM.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID =" & Replace(Replace(dtGenericFormConfig.Rows(i).Item("numFormFieldID"), "D", ""), "C", "") & " and Fld_Value ilike '" & IIf(rbListSearchContext.SelectedValue = 1, "%", "") & Replace(CType(tblSearch.FindControl(dtGenericFormConfig.Rows(i).Item("vcDbColumnName")), TextBox).Text, "'", "''") & "%' )"
                            End If
                        End If
                    End If
                Next
                If rdlReportType.SelectedValue = 1 Then
                    str = str & " and DM.numRecOwner = " & Session("UserContactID")
                ElseIf rdlReportType.SelectedValue = 2 Then
                    str = str & " and DM.numRecOwner in (select  numContactID from AdditionalContactsInformation where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType =7))"
                ElseIf rdlReportType.SelectedValue = 3 Then
                    str = str & " and DM.numTerId in (select numTerritory from ForReportsByTerritory where numUserCntID=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType =7)"
                End If

                If cbOptionOne.Checked = True Then str = str & " and DM.numDivisionID not in (select distinct(numDivisionID) from OpportunityMaster where numDomainID=" & Session("DomainID") & " and bintAccountClosingDate between dateadd(day,-" & IIf(IsNumeric(txtDaysOptionOne.Text) = True, txtDaysOptionOne.Text, 0) & ",getdate()) and getdate())"
                If cbOptionTwo.Checked = True Then str = str & " and DM.numDivisionID in (select distinct(numDivisionID) from OpportunityMaster where numDomainID=" & Session("DomainID") & " and bintAccountClosingDate between dateadd(day,-" & IIf(IsNumeric(txtDaysOptionTwo.Text) = True, txtDaysOptionTwo.Text, 0) & ",getdate() and getdate() and monPAmount) >" & IIf(IsNumeric(txtDealAmount.Text) = True, txtDealAmount.Text, 0)
                'If cbOptionTwo.Checked = True Then str = str & " and DM.numDivisionID in (select distinct(numDivisionID) from OpportunityMaster where numDomainID=" & Session("DomainID") & " and bintAccountClosingDate between dateadd(day,-" & IIf(IsNumeric(txtDaysOptionTwo.Text) = True, txtDaysOptionTwo.Text, 0) & ",getdate() and getdate() and monPAmount) >" & IIf(IsNumeric(txtDealAmount.Text) = True, txtDealAmount.Text, 0) & ")"
                If cbOptionThree.Checked = True Then
                    Dim strItemIds As String = ""
                    If hdnCol.Value.Length > 0 Then
                        strItemIds = hdnCol.Value.TrimEnd(",")
                        str = str & " and DM.numDivisionID in (select distinct(OM.numDivisionID) from OpportunityMaster OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] where OM.numDomainID=" & Session("DomainID") & " and OM.bintAccountClosingDate between dateadd(day,-" & IIf(IsNumeric(txtDaysOptionThree.Text) = True, txtDaysOptionThree.Text, 0) & ",getdate()) and getdate() and OI.[numItemCode] IN (" & strItemIds & ") ) "
                    End If
                End If
                If chkSurveyBet.Checked = True Then str = str & " and ADC.numContactID in (select distinct(numRegisteredRespondentContactId) from SurveyRespondentsMaster where numDomainID=" & Session("DomainID") & " and numSurRating between " & IIf(IsNumeric(txtRatingStart.Text) = True, txtRatingStart.Text, 0) & " and " & IIf(IsNumeric(txtRatingEnd.Text) = True, txtRatingEnd.Text, 0) & ")"
                If chkSurvEqalTo.Checked = True Then str = str & " and ADC.numContactID in (select distinct(numRegisteredRespondentContactId) from SurveyRespondentsMaster where numDomainID=" & Session("DomainID") & " and numSurRating = " & IIf(IsNumeric(txtSurveys.Text) = True, txtSurveys.Text, 0) & ")"

                Session("WhereCondition") = str
                str = ""
                If Not tblSearch.FindControl("cbListAOI") Is Nothing Then
                    Dim MyItem As ListItem
                    Dim cbl As CheckBoxList
                    cbl = tblSearch.FindControl("cbListAOI")
                    For Each MyItem In cbl.Items
                        If MyItem.Selected = True Then str = str & MyItem.Value & ","
                    Next
                    str = str.TrimEnd(",")
                    Session("AreasOfInt") = str
                End If
                Response.Redirect("../admin/frmAdvancedSearchRes.aspx?frm=Profile")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub FillStateDyn(ByVal sender As Object, ByVal e As EventArgs)
            Try
                If sender.ID = "vcBillCountry" Then
                    FillState(CType(tblSearch.FindControl("vcBilState"), DropDownList), sender.SelectedItem.Value, Session("DomainID"))
                Else : FillState(CType(tblSearch.FindControl(Replace(sender.ID, "Country", "State")), DropDownList), sender.SelectedItem.Value, Session("DomainID"))
                End If
                'Response.Write(sender.ID)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadDropdowns()
            Try
                objGenericAdvSearch.UserCntID = Session("UserContactID")
                objGenericAdvSearch.DomainID = Session("DomainID")
                ddlEmailGroup.DataSource = objGenericAdvSearch.LoadDropdown()
                ddlEmailGroup.DataTextField = "Name"
                ddlEmailGroup.DataValueField = "GroupId"
                ddlEmailGroup.DataBind()
                ddlEmailGroup.Items.Insert(0, "--Select One--")
                ddlEmailGroup.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmailGroup.SelectedIndexChanged
            Try
                If ddlEmailGroup.SelectedItem.Value > 0 Then
                    Session("WhereCondition") = " and ADC.numContactID in (Select numContactID from ProfileEGroupDTL where numEmailGroupID=" & ddlEmailGroup.SelectedItem.Value & ")"
                    Response.Redirect("../admin/frmAdvancedSearchRes.aspx?frmScreen=Profile")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
