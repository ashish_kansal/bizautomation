﻿Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports System.IO
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Marketing

Partial Public Class frmSurveyTemplate
    Inherits BACRMPage
    
    'http://www.telerik.com/help/aspnet-ajax/addcustomdropdowns.html
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                DisplaySurveyLists()
            End If
            'Set Image Manage and Template Manager
            Campaign.SetRadEditorPath(RadEditor1, CCommon.ToLong(Session("DomainID")), Page)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlSurvey_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSurvey.SelectedIndexChanged
        Try
            If ddlSurvey.SelectedValue > 0 Then
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub DisplaySurveyLists()
        Try
            Dim objSurvey As New SurveyAdministration                 'Declare and create a object of SurveyAdministration
            objSurvey.DomainId = Session("DomainID")                  'Set the Doamin Id
            Dim dtSurveylist As DataTable                             'Declare a datatable
            dtSurveylist = objSurvey.getSurveyList()                  'call function to get the list of available surveys
            Dim drSurveyList As DataRow                               'Declare a DataRow object

            ddlSurvey.Items.Clear()
            For Each drSurveyList In dtSurveylist.Rows                'Loop through the rows on the table containing the survey list
                ddlSurvey.Items.Add(New ListItem(Server.HtmlDecode(CStr(drSurveyList.Item("vcSurName"))), drSurveyList.Item("numSurID")))
            Next

            ddlSurvey.Items.Insert(0, "--Select One--")
            ddlSurvey.Items.FindByText("--Select One--").Value = "0"

            Dim objSite As New Sites
            objSite.SiteID = 0
            objSite.DomainID = Session("DomainID")
            objSite.StyleType = 3 'css
            cblStyle.DataSource = objSite.GetStyles()
            cblStyle.DataTextField = "StyleName"
            cblStyle.DataValueField = "numCssID"
            cblStyle.DataBind()

            objSite.StyleType = 4 'JS
            cblJS.DataSource = objSite.GetStyles()
            cblJS.DataTextField = "StyleName"
            cblJS.DataValueField = "numCssID"
            cblJS.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If ddlSurvey.SelectedValue > 0 Then
                Dim objSurvey As New SurveyAdministration
                objSurvey.DomainId = Session("DomainID")
                objSurvey.SurveyId = ddlSurvey.SelectedValue
                objSurvey.strText = Server.HtmlEncode(RadEditor1.Content)
                objSurvey.PageType = ddlPageType.SelectedValue
                objSurvey.StrItems = GetItems()
                objSurvey.ManageSurveyTemplate()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("frmCampaignSurveyList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindCustomField()
        Try
            Dim ddl As EditorDropDown = CType(RadEditor1.FindTool("CustomField"), EditorDropDown)
            ddl.Items.Clear()
            'Dim dtCustFld As DataTable
            'Dim objCusField As New CustomFields()
            'objCusField.DomainID = Session("DomainID")
            'objCusField.locId = IIf(ddlPageType.SelectedValue = 1, 2, 6)
            'dtCustFld = objCusField.CustomFieldList
            'For Each row As DataRow In dtCustFld.Rows
            '    ddl.Items.Add(row("fld_label"), "#" & row("fld_label").ToString.Trim.Replace(" ", "") & "#")
            'Next
            ddl.Items.Add("Form", "#Form#")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindData()
        Try
            Dim objSurvey As New SurveyAdministration
            objSurvey.DomainID = Session("DomainID")
            objSurvey.SurveyId = ddlSurvey.SelectedValue
            objSurvey.PageType = ddlPageType.SelectedValue
            Dim dsPage As DataSet
            dsPage = objSurvey.GetSurveyTemplate()
            Dim dt As DataTable = dsPage.Tables(0)
            If dt.Rows.Count > 0 Then
                RadEditor1.Content = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtTemplate")))
            Else
                RadEditor1.Content = ""
            End If

            cblStyle.ClearSelection()

            If dsPage.Tables(1).Rows.Count > 0 Then
                For Each dr As DataRow In dsPage.Tables(1).Rows
                    cblStyle.Items.FindByValue(dr("numCssID")).Selected = True
                Next
            End If

            cblJS.ClearSelection()

            If dsPage.Tables(2).Rows.Count > 0 Then
                For Each dr As DataRow In dsPage.Tables(2).Rows
                    cblJS.Items.FindByValue(dr("numCssID")).Selected = True
                Next
            End If

            BindCustomField()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Function GetItems() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numCssID")
            For Each Item As ListItem In cblStyle.Items
                If Item.Selected Then
                    Dim dr As DataRow = dt.NewRow
                    dr("numCssID") = Item.Value
                    dt.Rows.Add(dr)
                End If
            Next

            For Each Item As ListItem In cblJS.Items
                If Item.Selected Then
                    Dim dr As DataRow = dt.NewRow
                    dr("numCssID") = Item.Value
                    dt.Rows.Add(dr)
                End If
            Next

            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "Item"
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub ddlPageType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageType.SelectedIndexChanged
        Try
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class