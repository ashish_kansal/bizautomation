''' -----------------------------------------------------------------------------
''' Project	 : 
''' Class	 : frmEditSurveyRedirection
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     Directs the flow towards Edit Survey Screen
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	01/06/2006	Created
''' </history>
''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Survey
Imports System.IO
Imports BACRM.BusinessLogic.Common
Partial Class frmEditSurveyRedirection
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired eachtime the page is called. In this event we will 
    '''     get the data required to redirect to the details page and then it will redirect.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	02/09/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Not required here 
            '
            'Put user code to initialize the page here
            If GetQueryStringVal( "numSurveyId") <> "" Then                'Postback is necessary
                Dim sBufURL As New System.Text.StringBuilder                'Instantiate a stringbuilder object
                Dim objDirectory As Directory                               'Declare a Directory Object
                Dim objFiles As String()                                    'Array of files
                Dim objFile As String                                       'Object of a single file

                objFiles = objDirectory.GetFiles(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\", "TempSurveyInformation_" & Session("DomainID") & "_" & GetQueryStringVal( "numSurveyId") & "_" & "*" & ".xml")

                If objFiles.Length > 0 Then
                    If CInt(objFiles(0).Split("_")(3).Split(".")(0)) <> Session("UserId") Then
                        Dim objSurvey As New SurveyAdministration                 'Declare and create a object of SurveyAdministration
                        objSurvey.DomainId = Session("DomainID")                  'Set the Doamin Id
                        objSurvey.UserId = CInt(objFiles(0).Split("_")(3).Split(".")(0)) 'Set the User Id
                        sBufURL.Append(objSurvey.getSurveyCheckedOutByUser())    'call function to get the name of the user who has checked out the survey

                        litClientMessage.Text = "<script language=javascript>alert('This survey is being modified by \'" & Replace(sBufURL.ToString, "'", "\'") & "\' and will be available as soon as these modifications are saved.');</script>" 'Alert a message of the survey edited by another User
                        Exit Sub
                    End If
                End If
                sBufURL.Append("frmCampaignSurveyEditor.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numSurId=" & GetQueryStringVal( "numSurveyId"))
                litClientMessage.Text = "<script language=javascript>parent.location.href='" & sBufURL.ToString & "';</script>" 'Open the URL in the same window
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class

