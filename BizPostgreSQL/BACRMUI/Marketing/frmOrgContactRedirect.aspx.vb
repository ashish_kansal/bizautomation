''' -----------------------------------------------------------------------------
''' Project	 : 
''' Class	 : frmOrgContactRedirect
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     Directs the flow towards either contact details or the org details screen
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	01/06/2006	Created
''' </history>
''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Partial Class frmOrgContactRedirect
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired eachtime the page is called. In this event we will 
    '''     get the data required to redirect to the details page and then it will redirect.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	01/06/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            If GetQueryStringVal("RNW") <> "" Then
                hdRedirectionNewWindow.Value = GetQueryStringVal("RNW")
            End If

            If GetQueryStringVal("RE") <> "" Then
                hdRedirectionEntity.Value = GetQueryStringVal("RE")
            End If
            If GetQueryStringVal("CI") <> "" Then
                hdContactId.Value = GetQueryStringVal("CI")
            End If
            If GetQueryStringVal("DI") <> "" Then
                hdDivisionId.Value = GetQueryStringVal("DI")
            End If





            'Put user code to initialize the page here
            'If IsPostBack Then                                              'Postback is necessary
            Dim dtContactInfoForURLRedirection As DataTable             'Declare a datatable
            Dim objSurveyAdministration As New SurveyAdministration     'Instantiate an object of survey administration
            Dim sFromScreen As String = IIf(hdRedirectionNewWindow.Value = 1, "SurveyRespondents", "AdvSearch") 'Indicate where the flow of screen is coming from
            If hdContactId.Value <> "" Then                             'Contact Id exists
                Dim numContactId As Integer = hdContactId.Value         'Store the Contact Id
                dtContactInfoForURLRedirection = objSurveyAdministration.GetContactInfoForURLRedirection(numContactId, "Cont") 'Call to get the URL parameters
            ElseIf hdDivisionId.Value <> "" Then                        'Division Id is passed
                Dim numDivisionId As Integer = hdDivisionId.Value       'Store the Division Id
                dtContactInfoForURLRedirection = objSurveyAdministration.GetContactInfoForURLRedirection(numDivisionId, "Div") 'Call to get the URL parameters
            End If
            Dim sBufURL As New System.Text.StringBuilder                'Instantiate a stringbuilder object
            If dtContactInfoForURLRedirection.Rows.Count > 0 Then       'There must be a contact existing
                If hdRedirectionEntity.Value = "Contact" Then           'Redirecting to Contacts Page
                    sBufURL.Append("../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" & sFromScreen & "&CmpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numCompanyId") & "&DivID=" & dtContactInfoForURLRedirection.Rows(0).Item("numDivisionId") & "&CRMTYPE=" & dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") & "&CntId=" & dtContactInfoForURLRedirection.Rows(0).Item("numContactId"))  'Create the URL string for redirection to contact details
                Else                                                    'Redirecting to Org Details Page
                    If dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") = 0 Then              'Leads
                        sBufURL.Append("../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" & sFromScreen & "&CmpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numCompanyId") & "&DivID=" & dtContactInfoForURLRedirection.Rows(0).Item("numDivisionId") & "&CntID=" & dtContactInfoForURLRedirection.Rows(0).Item("numContactId") & "&GrpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numGrpId")) 'Create the URL string for redirection to Org Leads
                    ElseIf dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") = 1 Then          'Prospects
                        sBufURL.Append("../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" & sFromScreen & "&CmpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numCompanyId") & "&DivID=" & dtContactInfoForURLRedirection.Rows(0).Item("numDivisionId") & "&CRMTYPE=" & dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") & "&GrpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numGrpId")) 'Create the URL string for redirection to Org Prospects

                    ElseIf dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") = 2 Then          'Accounts
                        sBufURL.Append("../Account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" & sFromScreen & "&CmpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numCompanyId") & "&klds+7kldf=fjk-las&DivId=" & dtContactInfoForURLRedirection.Rows(0).Item("numDivisionId") & "&CRMTYPE=" & dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") & "&CntID=" & dtContactInfoForURLRedirection.Rows(0).Item("numContactId")) 'Create the URL string for redirection to Org Accounts

                    End If
                End If
                If hdRedirectionNewWindow.Value = 1 Then
                    litClientMessage.Text = "<script language=javascript>location.href='" & sBufURL.ToString & "';</script>" 'Open the URL in new window
                Else : litClientMessage.Text = "<script language=javascript>parent.location.href='" & sBufURL.ToString & "';</script>" 'Open the URL in the same window
                End If
                hdContactId.Value = ""                                  'Reset the contact id which is used
                hdDivisionId.Value = ""                                 'Reset the division id which is used
            End If
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
