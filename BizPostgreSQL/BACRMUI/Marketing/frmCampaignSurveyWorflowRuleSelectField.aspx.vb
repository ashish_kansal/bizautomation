﻿Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Survey
''' Class	 : frmCampaignSurveyWorflowSelectField
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     this interface is used to select fields for Rule 4 and 5 of the survey
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	09/16/2005	Created
''' </history>
''' ''' -----------------------------------------------------------------------------

Partial Public Class frmCampaignSurveyWorflowRuleSelectField
    Inherits BACRMPage
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub frmCampaignSurveyWorflowRuleSelectField_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        InitializeComponent()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then


            Dim sClientSideString As New System.Text.StringBuilder                              'Declare a string builder
            Dim iRowIndex As Int16 = 0                                                          'Declare an integer to hold the row index

            sClientSideString.Append(vbCrLf & "<script language='javascript'>")                  'Start with the script block
            sClientSideString.Append(vbCrLf & "var arrFieldConfig = new Array();" & vbCrLf)      'Create a array to hold the field information in javascript
            sClientSideString.Append("</script>")                                                'end the client side javascript block
            litClientMessage.Text &= vbCrLf & sClientSideString.ToString()                        'Set the string into the client side displayable lable 




            ' Get all the Values from Quesr string 
            Dim numSurId As Integer = GetQueryStringVal("numSurId")           'Get the Survey Id in local variable
            hdSurId.Value = numSurId                                            'Set the Survey Id in hidden
            Dim numAnsID As Integer = GetQueryStringVal("numAnsID")           'Get the Answer Id in local variable
            hdAnsId.Value = numAnsID                                            'Set the Answer Id in hidden
            Dim numQuestionId As Integer = GetQueryStringVal("numQuestionId") 'Get the Question Id in local variable
            hdQId.Value = numQuestionId
            Dim numRuleNo As Integer = GetQueryStringVal("numRuleNo") 'Get the Question Id in local variable
            hdRuleId.Value = numRuleNo
            Dim numRadioNo As Integer = GetQueryStringVal("numRadioNo") 'Get the Question Id in local variable
            hdRadioId.Value = numRadioNo

            Dim numMatrix As Integer = GetQueryStringVal("numMatrix") 'Get the Matrix Id in local variable
            hdMatrix.Value = numMatrix                                         'Set the Matrix Id in hidden

            Dim objSurvey As New SurveyAdministration                       'Create an object of Survey Administration Class
            objSurvey.DomainID = Session("DomainID")                        'Set the Domain ID
            objSurvey.UserId = Session("UserID")                            'Set the User ID
            objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
            objSurvey.SurveyId = hdSurId.Value                              'Set the Survey ID

            If hdMatrix.Value = 0 Then
                objSurvey.MatrixID = 0                                   'Set the Answer id
                objSurvey.AnswerId = numAnsID                                   'Set the Answer id
            Else
                objSurvey.MatrixID = numMatrix                                   'Set the Answer id
                objSurvey.AnswerId = 0                                   'Set the Answer id
            End If

            objSurvey.QuestionId = numQuestionId                            'Set the Question id
            Dim dvAvailableList As DataView
            Dim dtSelectField As DataTable
            If (hdRuleId.Value = 4) Then
                If (numRadioNo = 1 Or numRadioNo = 2) Then
                    dtSelectField = objSurvey.GetSelectFields(0)
                End If
                dvAvailableList = New DataView(dtSelectField)                 'create a dataview object
                dvAvailableList.Sort = "vcFormFieldName asc"                            'sort order    
                lstAvailablefld.DataSource = dvAvailableList                        'Set the datasource for the available fields
                lstAvailablefld.DataTextField = "vcFormFieldName"                   'set the text field
                lstAvailablefld.DataValueField = "SurveySelectFieldId"                   'set the value attribut
            ElseIf (hdRuleId.Value = 5) Then
                If numRadioNo = 1 Then
                    dtSelectField = objSurvey.GetSelectFields(1)
                    dvAvailableList = New DataView(dtSelectField)                 'create a dataview object
                    dvAvailableList.Sort = "vcFormFieldName asc"                            'sort order    
                    lstAvailablefld.DataSource = dvAvailableList                        'Set the datasource for the available fields
                    lstAvailablefld.DataTextField = "vcFormFieldName"                   'set the text field
                    lstAvailablefld.DataValueField = "SurveySelectFieldId"                   'set the value attribut
                ElseIf numRadioNo = 2 Then

                    dtSelectField = objCommon.GetItems(Session("DomainID"))
                    dvAvailableList = New DataView(dtSelectField)                 'create a dataview object
                    dvAvailableList.Sort = "vcItemName asc"                            'sort order    
                    lstAvailablefld.DataSource = dvAvailableList                        'Set the datasource for the available fields
                    lstAvailablefld.DataTextField = "vcItemName"                   'set the text field
                    lstAvailablefld.DataValueField = "numItemCode"                   'set the value attribut
                End If


            End If

            lstAvailablefld.DataBind()

            '    ' Attach Javascript to the form
            btnDone.Attributes.Add("OnClick", "javascript: return CreateIdList(document.frmSelectWorkflowRuleField.lstSelectedfldOne);")     'calls for validation
            'End If

            If hdMatrix.Value = 0 Then
                objSurvey.MatrixID = 0                                   'Set the Answer id
                objSurvey.AnswerId = hdAnsId.Value                       'Set the Answer id
            Else
                objSurvey.MatrixID = hdMatrix.Value                      'Set the Answer id
                objSurvey.AnswerId = 0                                   'Set the Answer id
            End If

            objSurvey.QuestionId = hdQId.Value                             'Set the Question id

            objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()     'Call to retrieve the existing data temporarily


            Dim dtSurveyAnswerWorkFlowMaster As DataTable
            dtSurveyAnswerWorkFlowMaster = objSurvey.SurveyInfo.Tables("SurveyWorkflowRules")           'Retrieve the datatable


            Dim drSurveyAnswerWorkFlowMasterRow As DataRow                                              'Declare a DataRow
            Dim numSurveyAnwerWorkFlowMasterTableRowIndex As Integer = 1                                'Declare a index for datatable rows
            Dim strSelectFieldId As String


            For Each drSurveyAnswerWorkFlowMasterRow In dtSurveyAnswerWorkFlowMaster.Rows               'Loop through the rules table
                If drSurveyAnswerWorkFlowMasterRow.Item("numAnsID") = objSurvey.AnswerId And drSurveyAnswerWorkFlowMasterRow.Item("numMatrixID") = objSurvey.MatrixID And drSurveyAnswerWorkFlowMasterRow.Item("numQID") = objSurvey.QuestionId And drSurveyAnswerWorkFlowMasterRow.Item("numRuleId") = hdRuleId.Value Then
                    Dim strTemp As String
                    Dim c As Char
                    Dim drTemp As DataRow

                    Select Case hdRuleId.Value
                        Case 4
                            Dim dc(0) As DataColumn
                            dc(0) = dtSelectField.Columns("SurveySelectFieldId")
                            dtSelectField.PrimaryKey = dc
                            If Not IsDBNull(drSurveyAnswerWorkFlowMasterRow("vcRuleFourSelectFields")) Then
                                strSelectFieldId = drSurveyAnswerWorkFlowMasterRow("vcRuleFourSelectFields")
                                For Each strTemp In strSelectFieldId.Split(",")
                                    drTemp = dtSelectField.Rows.Find(strTemp)
                                    If Not drTemp Is Nothing Then
                                        lstSelectedfldOne.Items.Add(New ListItem(drTemp("vcFormFieldName"), strTemp))
                                        ' Remove from the Available list
                                        lstAvailablefld.Items.Remove(lstAvailablefld.Items.FindByValue(strTemp))
                                    End If

                                Next
                            End If
                        Case 5

                            Dim dc(0) As DataColumn
                            If numRadioNo = 1 Then
                                dc(0) = dtSelectField.Columns("SurveySelectFieldId")
                                dtSelectField.PrimaryKey = dc
                                If Not IsDBNull(drSurveyAnswerWorkFlowMasterRow("vcRuleFiveSelectFields")) Then
                                    strSelectFieldId = drSurveyAnswerWorkFlowMasterRow("vcRuleFiveSelectFields")
                                    For Each strTemp In strSelectFieldId.Split(",")
                                        drTemp = dtSelectField.Rows.Find(strTemp)
                                        If Not drTemp Is Nothing Then
                                            If numRadioNo = 1 Then
                                                lstSelectedfldOne.Items.Add(New ListItem(drTemp("vcFormFieldName"), strTemp))
                                                ' Remove from the Available list
                                                lstAvailablefld.Items.Remove(lstAvailablefld.Items.FindByValue(strTemp))
                                            End If
                                        End If

                                    Next
                                End If
                            Else
                                dc(0) = dtSelectField.Columns("numItemCode")
                                dtSelectField.PrimaryKey = dc
                                If Not IsDBNull(drSurveyAnswerWorkFlowMasterRow("vcItem")) Then
                                    strSelectFieldId = drSurveyAnswerWorkFlowMasterRow("vcItem")
                                    For Each strTemp In strSelectFieldId.Split(",")
                                        drTemp = dtSelectField.Rows.Find(strTemp)
                                        If Not drTemp Is Nothing Then

                                            lstSelectedfldOne.Items.Add(New ListItem(drTemp("vcItemName"), strTemp))
                                            ' Remove from the Available list
                                            lstAvailablefld.Items.Remove(lstAvailablefld.Items.FindByValue(strTemp))
                                        End If
                                    Next
                                End If
                            End If


                        Case Else
                            Throw New ArgumentException("Invalid Arguement Value", "hdRuleId")

                    End Select

                End If
            Next


        End If
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This subroutine HTML Decodes the row elements
    ''' </summary>
    ''' <param name="dtTable">Represents the datatable object.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	02/27/2006	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Function HTMLDecodeFieldList(ByVal dtTable As DataTable) As DataTable
        Try
            Dim objrow As DataRow
            For Each objrow In dtTable.Rows                                                       'loop through the rows in parent table
                objrow.Item("vcNewFormFieldName") = Server.HtmlDecode(objrow.Item("vcNewFormFieldName")) 'add new name to the column item
            Next
            Return dtTable
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnDone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDone.Click

        Dim strXML As String = hdXMLString.Value
        Dim objSurvey As New SurveyAdministration                       'Create an object of Survey Administration Class
        objSurvey.DomainID = Session("DomainID")                        'Set the Domain ID
        objSurvey.UserId = Session("UserID")                            'Set the User ID
        objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
        objSurvey.SurveyId = hdSurId.Value                              'Set the Survey ID

        If hdMatrix.Value = 0 Then
            objSurvey.MatrixID = 0                                   'Set the Answer id
            objSurvey.AnswerId = hdAnsId.Value                       'Set the Answer id
        Else
            objSurvey.MatrixID = hdMatrix.Value                      'Set the Answer id
            objSurvey.AnswerId = 0                                   'Set the Answer id
        End If

        objSurvey.QuestionId = hdQId.Value                             'Set the Question id

        objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()     'Call to retrieve the existing data temporarily

        ' from here 

        Dim dtSurveyAnswerWorkFlowMaster As DataTable
        dtSurveyAnswerWorkFlowMaster = objSurvey.SurveyInfo.Tables("SurveyWorkflowRules")           'Retrieve the datatable

        Dim drSurveyAnswerWorkFlowMaster() As DataRow                                               'Declare a DataRow

        Dim drSurveyAnswerWorkFlowMasterRow As DataRow                                              'Declare a DataRow
        Dim numSurveyAnwerWorkFlowMasterTableRowIndex As Integer = 1                                'Declare a index for datatable rows
        Dim drTempFlowMasterRow As DataRow
        ' This code creates rows for the rule if they are not present in the  table
        Dim iFilterRuleId As Integer                                                                'Declare a variable for filter rule
        For iFilterRuleId = 1 To 7
            If iFilterRuleId <> 3 Then
                If hdMatrix.Value = 0 Then
                    drSurveyAnswerWorkFlowMaster = dtSurveyAnswerWorkFlowMaster.Select("numRuleID = " & iFilterRuleId & " and numAnsID = " & objSurvey.AnswerId & " and numQID = " & objSurvey.QuestionId)  'Set the filter
                Else
                    drSurveyAnswerWorkFlowMaster = dtSurveyAnswerWorkFlowMaster.Select("numRuleID = " & iFilterRuleId & " and numMatrixID = " & objSurvey.MatrixID & " and numQID = " & objSurvey.QuestionId)  'Set the filter
                End If

                If drSurveyAnswerWorkFlowMaster.Length = 0 Then                                         'Check if the row exists
                    drTempFlowMasterRow = dtSurveyAnswerWorkFlowMaster.NewRow                           'Get a new row
                    drTempFlowMasterRow.Item("numRuleID") = iFilterRuleId                               'Set the rule id
                    drTempFlowMasterRow.Item("numQID") = objSurvey.QuestionId                           'Set the Question id
                    drTempFlowMasterRow.Item("numAnsID") = objSurvey.AnswerId                           'Set the answer id
                    drTempFlowMasterRow.Item("numMatrixID") = objSurvey.MatrixID
                    drTempFlowMasterRow.Item("numSurID") = hdSurId.Value
                    drTempFlowMasterRow.Item("boolActivation") = 0 'Activation of Rule 4 is stored
                    drTempFlowMasterRow.Item("numEventOrder") = 0 'Event Order is stored'Set the Survey id
                    dtSurveyAnswerWorkFlowMaster.Rows.Add(drTempFlowMasterRow)                          'Add a new row
                End If
            End If
        Next
        For Each drSurveyAnswerWorkFlowMasterRow In dtSurveyAnswerWorkFlowMaster.Rows               'Loop through the rules table
            If drSurveyAnswerWorkFlowMasterRow.Item("numAnsID") = objSurvey.AnswerId And drSurveyAnswerWorkFlowMasterRow.Item("numMatrixID") = objSurvey.MatrixID And drSurveyAnswerWorkFlowMasterRow.Item("numQID") = objSurvey.QuestionId And drSurveyAnswerWorkFlowMasterRow.Item("numRuleId") = hdRuleId.Value Then

                Select Case hdRuleId.Value
                    Case 4
                        drSurveyAnswerWorkFlowMasterRow("vcRuleFourSelectFields") = strXML
                        drSurveyAnswerWorkFlowMasterRow.Item("boolActivation") = 1 'Activation of Rule 4 is stored
                        drSurveyAnswerWorkFlowMasterRow.Item("numEventOrder") = 0 'Event Order is stored
                        'drSurveyAnswerWorkFlowMasterRow.Item("tIntRuleFourRadio") = intRuleFourRadio
                    Case 5

                        If hdRadioId.Value = 1 Then
                            drSurveyAnswerWorkFlowMasterRow("vcRuleFiveSelectFields") = strXML
                            drSurveyAnswerWorkFlowMasterRow.Item("boolActivation") = 1 'Activation of Rule 4 is stored
                            drSurveyAnswerWorkFlowMasterRow.Item("numEventOrder") = 0 'Event Order is stored
                        Else
                            drSurveyAnswerWorkFlowMasterRow("vcItem") = strXML
                            drSurveyAnswerWorkFlowMasterRow.Item("boolActivation") = 1 'Activation of Rule 4 is stored
                            drSurveyAnswerWorkFlowMasterRow.Item("numEventOrder") = 0 'Event Order is stored
                        End If


                    Case Else
                        Throw New ArgumentException("Invalid Arguement Value", "hdRuleId")

                End Select

            End If
        Next

        objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
        objSurvey.SaveSurveyInformationTemp()

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ScriptCloseme", "<script> window.close(); </script>")
    End Sub
End Class
