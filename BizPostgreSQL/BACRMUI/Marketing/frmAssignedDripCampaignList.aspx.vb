﻿Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmAssignedDripCampaignList
    Inherits BACRMPage
    Dim objCampaign As Campaign
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                
                BindDatagrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            Dim dtCampaign As DataTable
            If objCampaign Is Nothing Then objCampaign = New Campaign

            With objCampaign
                .DomainID = Session("DomainID")
                dtCampaign = objCampaign.GetECampaignAssignee
                dgAssignedECamapign.DataSource = dtCampaign
                dgAssignedECamapign.DataBind()
            End With
            lblRecordCount.Text = dtCampaign.Rows.Count
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub dgECamapign_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAssignedECamapign.ItemCommand
        Try
            If e.CommandName = "Campaign" Then Response.Redirect("../Marketing/frmECampaignDtls.aspx?ECampID=" & e.Item.Cells(0).Text)
            If e.CommandName = "Delete" Then
                If objCampaign Is Nothing Then objCampaign = New Campaign
                'remove all contact of email group from campaign
                Dim dtContacts As DataTable
                Dim objEmailGroup As New BAddEmailGroup
                objEmailGroup.strGroupID = e.Item.Cells(2).Text.Trim()
                dtContacts = objEmailGroup.getEmailGroupContacts()
                For Each dr As DataRow In dtContacts.Rows
                    objCampaign.ContactId = dr("numContactId")
                    objCampaign.ECampaignID = e.Item.Cells(0).Text.Trim()
                    objCampaign.DeleteFromCampaign()
                Next

                objCampaign.ECampaignAssigneeID = e.Item.Cells(1).Text.Trim()
                objCampaign.byteMode = 1
                objCampaign.DomainID = Session("DomainID")
                objCampaign.ManageEcampaignAssignee() 'delete
                objCampaign.ECampaignAssigneeID = 0
                BindDatagrid()

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class