Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.WebHtmlEditor
Imports System.Text.RegularExpressions
Imports System.IO
Imports Amazon
Imports Amazon.SimpleEmail
Imports Amazon.SimpleEmail.Model
Imports System.Net.Mail
Imports System.Text
Imports System.Reflection
Imports System.Net.Mime



Partial Class frmEmailBroadCasting
    Inherits BACRMPage

    Dim lngBroadcastID As Long
    Dim lngSearchID As Long
    Dim intRecipient As Integer
    Dim objCampaign As New Campaign
    Dim dtTableConfig As DataTable 'To get all configuration of a domain which are Enabled ...
    Dim dtTableSelectedConfig As DataTable  'To get Configuration which is selected in the dropdown...

#Region "Delegate"
    Public Delegate Sub AsyncMethodCaller(ByVal dtEmailBroadCast As DataTable, ByVal fromEmail As String, ByVal ccEmail As String, ByVal bccEmail As String, ByVal Subject As String, ByVal Body As String)
#End Region



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Events"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            lngBroadcastID = CCommon.ToLong(GetQueryStringVal("ID"))
            lngSearchID = CCommon.ToLong(GetQueryStringVal("SearchID"))
            litSuccess.Text = ""

            If Not IsPostBack Then
                hdnDivIds.Value = CCommon.ToString(Session("AREmailCustomers"))
                If Not Session("ARStatementDate") Is Nothing Then
                    Dim dtStatementDate As DateTime
                    If DateTime.TryParse(Session("ARStatementDate"), dtStatementDate) Then
                        hdnStatementDate.Value = dtStatementDate.ToString()
                    End If
                End If
                

                Session("AREmailCustomers") = Nothing
                GetUserRightsForPage(6, 4)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                BindBizDocsCategory()
                BindFollowUpStatus()
                LoadTemplates()
                Session("Attachements") = Nothing
                intRecipient = RecipientCount()
                lblRecords.Text = CCommon.ToString(intRecipient)

                Dim objDocument As New DocumentList
                objDocument.BindEmailTemplateMergeFields(RadEditor1, 1)

                If lngBroadcastID > 0 Or lngSearchID > 0 Then
                    GetBroadcastDataByID()
                Else
                    If CCommon.ToLong(ddlEmailTemplate.SelectedValue) = 0 Then
                        RadEditor1.Content = Session("Signature")
                    End If

                    hlView.Attributes.Add("onClick", "OpenSearchResult(0" & ",1," & "0)")
                End If

                If CCommon.ToString(GetQueryStringVal("frm")) = "ARAging" Then
                    btnSend.Visible = True

                    divSendLimit.Visible = False
                    divSentMail.Visible = False
                    hplAttachments.Visible = False
                    hlView.Visible = False
                    btnBroadcast.Visible = False
                    btnSaveClose.Visible = False
                    btnSendToMyself.Visible = False

                    Dim isOauthConfigured As Boolean = False

                    'Send through logged in user SMTP Configuration
                    Dim objUserAccess As New UserAccess
                    objUserAccess.DomainID = CCommon.ToLong(Session("DomainID"))
                    objUserAccess.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        If (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) Then
                            If String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) Or String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                                btnSend.Visible = False
                                litMessage.Text = "SMTP details is not configured."
                            Else
                                isOauthConfigured = True
                            End If
                        End If
                    End If

                    If Not isOauthConfigured Then
                        objUserAccess.UserId = Session("UserID")
                        Dim dtUserAccessDetails As DataTable = objUserAccess.GetUserAccessDetails
                        If dtUserAccessDetails.Rows.Count > 0 Then
                            If Not CCommon.ToBool(dtUserAccessDetails.Rows(0)("bitSMTPServer")) Then
                                btnSend.Visible = False
                                litMessage.Text = "SMTP details is not configured."
                            End If
                        End If
                    End If
                Else
                    btnSend.Visible = False
                    btnBroadcast.Visible = True
                    btnSaveClose.Visible = True
                    btnSendToMyself.Visible = True

                    GetBroadcastConfiguration()
                End If
            End If
            Campaign.SetRadEditorPath(RadEditor1, CCommon.ToLong(Session("DomainID")), Page)
            btnBroadcast.Attributes.Add("onclick", "return BroMessage()")
            btnSendToMyself.Attributes.Add("onclick", "return SendMsgToMyself()")
            btnSaveClose.Attributes.Add("onClick", "return SaveMessage()")
            btnClose.Attributes.Add("onclick", "return Close()")
            hplAttachments.Attributes.Add("onclick", "return OpenAttachments()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Private Sub btnBroadcast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBroadcast.Click
        Try
            If CheckAttachmentSize() Then
                If intRecipient < 1000 Then
                    BroadCastEmail()

                Else
                    litMessage.Text = "Please send email in batches if you are sending email to more than 1000 receipants with some time intervals between sending batched emails."
                End If
            Else
                litMessage.Text = "Max allowed email size is 10MB. Check your attachments size and then try to send email again"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Sub BindFollowUpStatus()
        Try
            ddlFollowupStatus.Items.Clear()
            Dim dt As New DataTable()
            objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
            objCommon.ListID = CCommon.ToLong(30)
            dt = objCommon.GetMasterListItemsWithRights()
            Dim item As ListItem = New ListItem("-- All --", "0")
            ddlFollowupStatus.Items.Add(item)
            ddlFollowupStatus.AutoPostBack = False
            For Each dr As DataRow In dt.Rows
                item = New ListItem(CCommon.ToString(dr("vcData")), CCommon.ToString(dr("numListItemID")))
                item.Attributes("OptionGroup") = CCommon.ToString(dr("vcListItemGroupName"))
                item.Attributes("class") = CCommon.ToString(dr("vcColorScheme"))
                ddlFollowupStatus.Items.Add(item)
            Next
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            saveSearchQuery()

            If intRecipient > 0 Then
                Managebroadcast("save")
                Session("ContID") = Nothing
                Response.Redirect("../Marketing/frmBroadCastList.aspx")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnSendToMyself_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendToMyself.Click
        Try
            If ddlEmailTemplate.SelectedValue <> "0" AndAlso Not String.IsNullOrEmpty(txtName.Text.Trim()) AndAlso Not String.IsNullOrEmpty(txtSubject.Text.Trim()) Then

                If CheckAttachmentSize() Then

                    Dim objEmail As New Email
                    Dim dtEmailBroadCast As DataTable
                    dtEmailBroadCast = objEmail.GetEmailMergeData(1, Session("UserContactID"), Session("DomainID"), 0, 0)

                    Dim strBody As String
                    strBody = RadEditor1.Content
                    If chkTrack.Checked = True Then
                        strBody = strBody & "<img style='display:none'  width ='0' height='0' src=" & ConfigurationManager.AppSettings("EmailTracking") & "?BroadCastID=0&ContID=" & "##numContactId##" & ">"
                    End If

                    Dim strUnsubscribeLink As String = String.Empty
                    dtEmailBroadCast.Columns.Add("ContactOpt-OutLink")
                    For Each dr As DataRow In dtEmailBroadCast.Rows
                        strUnsubscribeLink = ConfigurationManager.AppSettings("PortalURL").Replace("/Login.aspx", "") & "/Common/frmUnsubscribe.aspx?e=" & dr("ContactEmail") & "&c=" & dr("numContactID") & "&d=" & Session("DomainID")
                        dr("ContactOpt-OutLink") = "<small>If you want to unsubscribe from future mailings, please visit<br><a href='" & strUnsubscribeLink & "' target='_blank'>" & strUnsubscribeLink & "</a> <small>"
                    Next

                    Try
                        SendRawEmail(dtEmailBroadCast, hdnFrom.Value, "", "", txtSubject.Text.Trim(), strBody)
                        litMessage.Text = "Test Message has been sent, please verify received mail before broadcasting emails."
                        pnlMessage.CssClass = "successInfo"
                    Catch ex As Exception
                        litMessage.Text = "Unable to send Test Message." & "<br />" & "Error: " & ex.Message
                        pnlMessage.CssClass = "warning"
                    End Try

                Else
                    litMessage.Text = "Max allowed email size is 10MB. Check your attachments size and then try to send email again"
                End If
            Else
                litMessage.Text = "Fill all required fields values."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub ddlEmailTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmailTemplate.SelectedIndexChanged
        Try
            LoadTemplateDetail(ddlEmailTemplate.SelectedItem.Value, ddlEmailTemplate.SelectedItem.Text)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub LoadTemplateDetail(ByVal templateID As Long, ByVal templateName As String)
        Try
            txtName.Text = templateName
            Dim dtDocDetails As DataTable
            Dim objDocuments As New DocumentList
            With objDocuments
                .GenDocID = templateID
                .DomainID = Session("DomainID")
                dtDocDetails = .GetDocByGenDocID
            End With
            BindFollowUpStatus()
            If dtDocDetails.Rows.Count > 0 Then
                txtSubject.Text = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))
                hdnFollowupDateUpdateTemplate.Value = IIf(IsDBNull(dtDocDetails.Rows(0).Item("bitLastFollowupUpdate")), 0, dtDocDetails.Rows(0).Item("bitLastFollowupUpdate"))
                RadEditor1.Content = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcDocdesc")), "", dtDocDetails.Rows(0).Item("vcDocdesc"))
                If Not ddlFollowupStatus.Items.FindByValue(dtDocDetails.Rows(0).Item("numFollowUpStatusId")) Is Nothing Then
                    ddlFollowupStatus.Items.FindByValue(dtDocDetails.Rows(0).Item("numFollowUpStatusId")).Selected = True
                End If
                chkFollowUpStatus.Checked = dtDocDetails.Rows(0).Item("bitUpdateFollowUpStatus")
            Else
                hdnFollowupDateUpdateTemplate.Value = 0
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub FilerEmailTemplatesByMarketingDept(ByVal checkedStatus As Boolean)
        Try
            Dim objCampaign As New Campaign
            Dim dtTableEmailTemplate As DataTable
            Dim currentUser As Int32 = -1
            Dim currentDomainID As Int32 = -1
            Dim isForMarketingDept As Int32 = 0 'set  false by defult
            If Not Session("UserID") Is Nothing And Not Session("DomainID") Is Nothing Then
                currentUser = Convert.ToInt32(Session("UserID").ToString())
                currentDomainID = Convert.ToInt32(Session("DomainID").ToString())
            End If

            If (checkedStatus = True) Then
                isForMarketingDept = 1  ' load related to marketing department
            Else : isForMarketingDept = 0  ' Load all no filter
            End If

            dtTableEmailTemplate = objCampaign.GetEmailTemplatesByMarketingDept(currentUser, currentDomainID, isForMarketingDept, ddlCategory.SelectedValue, CCommon.ToLong(Session("UserGroupID")))
            ddlEmailTemplate.DataSource = dtTableEmailTemplate
            ddlEmailTemplate.DataTextField = "VcDocName"
            ddlEmailTemplate.DataValueField = "numGenericDocID"
            ddlEmailTemplate.DataBind()
            ddlEmailTemplate.Items.Insert(0, "--Select One--")
            ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Try
            BindFollowUpStatus()
            LoadTemplates()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindBizDocsCategory()
        Try
            objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, CCommon.ToLong(Session("DomainID")))

            If Not ddlCategory.Items.FindByValue("369") Is Nothing Then
                ddlCategory.Items.FindByValue("369").Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub chkSendCopy_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkSendCopy.CheckedChanged
        Try
            If chkSendCopy.Checked Then
                lblRecords.Text = CCommon.ToString(CCommon.ToInteger(lblRecords.Text) + 1)
            Else
                lblRecords.Text = CCommon.ToString(CCommon.ToInteger(lblRecords.Text) - 1)
            End If
            intRecipient = CCommon.ToInteger(lblRecords.Text)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs)
        Try
            If objCommon Is Nothing Then
                objCommon = New CCommon
            End If
            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dt As DataTable = objCommon.GetARStatementRecipient(hdnDivIds.Value)

            Dim errorContacts As String = ""
            Dim errorEmails As String = ""

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                Dim view As New DataView(dt)
                Dim dtDivisionIDs As DataTable = view.ToTable(True, "numDivisionID", "vcCompanyName")

                For Each drDivision As DataRow In dtDivisionIDs.Rows
                    Dim dtContacts As DataTable = dt.Select("numDivisionID=" & CCommon.ToLong(drDivision("numDivisionID"))).CopyToDataTable()
                    Dim strContactIDs As String = ""
                    Dim toEmails As String = ""
                    Dim messageTo As String = ""

                    If Not dtContacts Is Nothing AndAlso dtContacts.Rows.Count > 0 Then
                        For Each drContact As DataRow In dtContacts.Rows
                            If Not String.IsNullOrEmpty(drContact("vcContactEmail")) Then
                                strContactIDs = strContactIDs & IIf(strContactIDs.Length > 0, ",", "") & CCommon.ToLong(drContact("numContactID"))
                                toEmails = toEmails & IIf(toEmails.Length > 0, ",", "") & CCommon.ToString(drContact("vcContactEmail"))
                                messageTo = messageTo & CCommon.ToString(drContact("vcContactName")) & "$^$" & CCommon.ToString(drContact("vcContactEmail")) & "#^#"
                            Else
                                errorContacts = errorContacts & "Organization: " & CCommon.ToString(drContact("vcCompanyName")) & ", " & "Contact:" & CCommon.ToString(drContact("vcContactName")) & "<br/>"
                            End If
                        Next
                    End If

                    If strContactIDs <> "" Then
                        Try
                            SendARStatementEmail(CCommon.ToLong(drDivision("numDivisionID")), strContactIDs, toEmails, messageTo)
                        Catch ex As Exception
                            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                            errorEmails = errorEmails & "Organization: " & CCommon.ToString(drDivision("vcCompanyName")) & "<br/>"
                        End Try
                    End If
                Next
            End If

            If errorContacts <> "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ErrorAR", "alert('Email not send to following contacts because email address not available:<br/>" & errorContacts & "')", True)
            End If
            If errorEmails <> "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ErrorAR", "alert('Email not send to following organization because unknown error occurred:<br/>" & errorContacts & "')", True)
            End If

            Response.Redirect("~/Accounting/frmAccountsReceivable.aspx", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "functions"
    Sub LoadTemplates()
        Try
            Dim objCampaign As New Campaign
            Dim dtTableEmailTemplate As DataTable
            objCampaign.DomainID = Session("DomainID")
            objCampaign.UserCntID = Session("UserContactID")
            If CCommon.ToString(GetQueryStringVal("frm")) = "ARAging" Then
                objCampaign.bitMarketCampaign = False
            Else
                objCampaign.bitMarketCampaign = True
            End If
            objCampaign.numGroupId = Session("UserGroupID")
            objCampaign.numCategoryId = ddlCategory.SelectedValue
            dtTableEmailTemplate = objCampaign.GetUserEmailTemplates

            If CCommon.ToString(GetQueryStringVal("frm")) = "ARAging" Then
                If Not dtTableEmailTemplate Is Nothing AndAlso dtTableEmailTemplate.Rows.Count > 0 Then
                    Dim item As ListItem
                    For Each drTemplate In dtTableEmailTemplate.Rows
                        item = New ListItem()
                        item.Text = CCommon.ToString(drTemplate("VcDocName"))
                        item.Value = CCommon.ToLong(drTemplate("numGenericDocID"))
                        If CCommon.ToString(drTemplate("VcFileName")) = "#SYS#CUSTOMER_STATEMENT" Then
                            item.Selected = True
                            LoadTemplateDetail(CCommon.ToLong(drTemplate("numGenericDocID")), CCommon.ToString(drTemplate("VcDocName")))
                        End If
                        ddlEmailTemplate.Items.Add(item)
                    Next
                End If
            Else
                ddlEmailTemplate.DataSource = dtTableEmailTemplate
                ddlEmailTemplate.DataTextField = "VcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()
            End If

            ddlEmailTemplate.Items.Insert(0, New ListItem("--Select One--", 0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'save search query only when search ID and broadcast ID is 0
    Sub saveSearchQuery()
        Try
            If lngBroadcastID = 0 And lngSearchID = 0 Then
                Dim objSearch As New FormGenericAdvSearch
                Dim objCampaign As New Campaign
                objSearch.SearchID = lngSearchID
                objSearch.byteMode = 2
                objSearch.DomainID = Session("DomainID")
                objSearch.UserCntID = Session("UserContactID")
                objSearch.QueryWhereCondition = CCommon.ToString(Session("SavedSearchCondition"))
                objSearch.SlidingDays = CCommon.ToInteger(Session("SlidingDays"))
                objSearch.TimeExpression = CCommon.ToString(Session("TimeExpression"))
                objSearch.SearchName = txtName.Text
                objSearch.SearchQueryforDisplay = CCommon.ToString(Session("UserFriendlyQuery"))
                objSearch.FormID = 1
                objSearch.ManageSavedSearch()
                lngSearchID = objSearch.SearchID
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub GetBroadcastDataByID()
        Try
            Dim objCampaign As New Campaign
            Dim dtBroadcast As DataTable

            objCampaign.DomainID = Session("DomainID")
            objCampaign.broadcastID = lngBroadcastID
            objCampaign.SearchID = lngSearchID
            dtBroadcast = objCampaign.GetBroadCastDataByID()

            txtName.Text = CCommon.ToString(dtBroadcast.Rows(0)("vcBroadCastName"))
            hdnFollowupDateUpdateTemplate.Value = dtBroadcast.Rows(0)("bitLastFollowupUpdate")
            txtSubject.Text = CCommon.ToString(dtBroadcast.Rows(0)("vcSubject"))
            ddlEmailTemplate.SelectedValue = dtBroadcast.Rows(0)("numEmailTemplateId")
            ddlCategory.SelectedValue = dtBroadcast.Rows(0)("numCategoryId")
            RadEditor1.Content = CCommon.ToString(dtBroadcast.Rows(0)("txtMessage"))
            hlView.Attributes.Add("onClick", "OpenSearchResult(" & CCommon.ToString(dtBroadcast.Rows(0)("numSearchID")) & ",1," & CCommon.ToString(dtBroadcast.Rows(0)("numBroadCastId")) & ")")
            If CCommon.ToString(dtBroadcast.Rows(0)("txtMessage")).Length <= 2 Then
                RadEditor1.Content = Session("Signature")
            End If
            If CCommon.ToBool(dtBroadcast.Rows(0)("bitBroadcasted")) = True Then
                btnBroadcast.Visible = False
                hlView.Visible = False
                btnSaveClose.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Private Sub BroadCastEmail()
        Try
            If ddlEmailTemplate.SelectedValue <> "0" AndAlso Not String.IsNullOrEmpty(txtName.Text.Trim()) AndAlso Not String.IsNullOrEmpty(txtSubject.Text.Trim()) Then
                'Check if selected receipents count is greater than 0 or not
                If intRecipient > 0 Then

                    'First Insert in Broadcast Table .It doesnt have value in BroadCastBizMessage ...
                    Dim dtEmailBroadCast As DataTable
                    dtEmailBroadCast = Managebroadcast("broadcast")
                    If CCommon.ToBool(hdnFollowupDateUpdateTemplate.Value) = True Then
                        Dim strContactID As String
                        strContactID = ""
                        objCampaign.DomainID = Session("DomainID")
                        For Each dr As DataRow In dtEmailBroadCast.Rows
                            strContactID = strContactID & CCommon.ToString(dr("numContactId")) & ","
                        Next
                        objCampaign.strBroadCastDtls = strContactID
                        objCampaign.bitUpdateFollowUpStatus = chkFollowUpStatus.Checked
                        objCampaign.numFollowUpStatusId = ddlFollowupStatus.SelectedValue
                        objCampaign.UpdateFollowUpDates()
                    End If

                    'Identifying all links and replace original links with Biz Links and store it. 
                    Dim r As Regex = New Regex("href\s*=\s*(?:""(?<1>[^""]*)""|(?<1>\S+))")
                    Dim match As MatchCollection = r.Matches(RadEditor1.Content)
                    Dim strXml As String = "<LinkRecords>"
                    Dim EachLink As Match
                    For Each EachLink In match
                        Dim strEachLink As String = CCommon.ToString(EachLink).Replace("href", "").Replace("=", "").Replace("""", "")
                        If Not strEachLink.ToLower().StartsWith("#") And Not strEachLink.ToLower().StartsWith("mailto") Then
                            strXml = strXml + "<Table>"
                            strXml = strXml + "<vcOriginalLink>" + strEachLink + "</vcOriginalLink><numBroadcastId>" + CCommon.ToString(lngBroadcastID) + "</numBroadcastId>"
                            strXml = strXml + "</Table>"
                        End If
                    Next
                    strXml = strXml + "</LinkRecords>"

                    objCampaign.strBroadcastingLink = strXml
                    objCampaign.ManageBroadcastingLink()

                    'Get all links back and replace all links with Bizlinks in Current body
                    Dim dtBroadcastingLink As DataTable
                    objCampaign.broadcastID = lngBroadcastID
                    dtBroadcastingLink = objCampaign.GetBroadcastingLink()

                    Dim BroadCastBizMessage As String
                    BroadCastBizMessage = RadEditor1.Content

                    If dtBroadcastingLink.Rows.Count > 0 Then
                        For Each dr As DataRow In dtBroadcastingLink.Rows
                            If BroadCastBizMessage.Contains(CCommon.ToString(dr("vcOriginalLink"))) Then
                                BroadCastBizMessage = BroadCastBizMessage.Replace(CCommon.ToString(dr("vcOriginalLink")), ConfigurationManager.AppSettings("EmailTracking") & "?BroadCastID=" & CCommon.ToString(lngBroadcastID) & "&LinkID=" & dr("numLinkId"))
                            End If
                        Next
                    End If

                    Dim strBody As String
                    strBody = BroadCastBizMessage
                    If chkTrack.Checked = True Then
                        strBody = strBody & "<img style='display:none'  width ='0' height='0' src=" & ConfigurationManager.AppSettings("EmailTracking") & "?&BroadCastID=" & lngBroadcastID & "&ContID=" & "##numContactId##" & ">"
                    End If

                    Dim strUnsubscribeLink As String = String.Empty
                    dtEmailBroadCast.Columns.Add("ContactOpt-OutLink")
                    For Each dr As DataRow In dtEmailBroadCast.Rows
                        strUnsubscribeLink = Session("PortalURL").Replace("/Login.aspx", "") & "/Common/frmUnsubscribe.aspx?e=" & dr("ContactEmail") & "&c=" & dr("numContactID") & "&d=" & Session("DomainID") & "&BroacastId=" & lngBroadcastID
                        dr("ContactOpt-OutLink") = strUnsubscribeLink ' "<small>If you want to unsubscribe from future mailings, please visit<br><a href='" & strUnsubscribeLink & "' target='_blank'>" & strUnsubscribeLink & "</a> <small>"
                    Next

                    Dim sendDelegate As New AsyncMethodCaller(AddressOf SendRawEmail)
                    sendDelegate.BeginInvoke(dtEmailBroadCast, hdnFrom.Value, "", "", txtSubject.Text.Trim(), strBody, Nothing, Nothing)

                    litSuccess.Text = "Process started boradcasting emails. Please check status from Marketing -> e-Mail Boradcast -> Broadcast History. If number of recipients are more than 100 please check after sometime."
                    'SendRawEmail(dtEmailBroadCast, hdnFrom.Value, "", "", txtSubject.Text.Trim(), strBody)
                Else
                    litMessage.Text = "You must select at least one recipient."
                End If

                GetAmazonSESUsageDetail()
            Else
                litMessage.Text = "Fill all required fields values."
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'get Recipient 
    Function RecipientCount() As String
        Try
            If CCommon.ToString(GetQueryStringVal("frm")) = "ARAging" Then
                If objCommon Is Nothing Then
                    objCommon = New CCommon
                End If
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objCommon.GetARStatementRecipient(hdnDivIds.Value)

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Return dt.Rows.Count
                Else
                    Return "0"
                End If
            Else
                Dim strContactID As String
                Dim objCampaign As New Campaign
                Dim dtEmailBroadCast As DataTable
                Dim dRows As DataRow()

                If CCommon.ToString(Session("ContIDs")) <> "" Then
                    strContactID = CCommon.ToString(Session("ContIDs"))
                    Dim strRecipientCount As String()
                    Dim objContacts As New CContacts

                    Dim objEmail As New Email
                    dtEmailBroadCast = objEmail.GetEmailMergeData(1, strContactID, Session("DomainID"), 0, 0)
                    dRows = dtEmailBroadCast.Select("bitOptOut = 0")
                    If dRows.Length > 0 Then
                        Return CCommon.ToString(dRows.CopyToDataTable().Rows.Count)
                    Else
                        Return "0"
                    End If
                ElseIf lngBroadcastID > 0 Then
                    objCampaign.broadcastID = lngBroadcastID
                    dtEmailBroadCast = objCampaign.GetBroadCastDetails()
                    Return CCommon.ToString(dtEmailBroadCast.Rows.Count)
                End If
            End If

            Return "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub GetAmazonSESUsageDetail()
        Try
            Dim region As RegionEndpoint = RegionEndpoint.USWest2
            Dim client As New AmazonSimpleEmailServiceClient(hdnAWSAccessKey.Value, hdnAWSSecretKey.Value, region)

            Dim sendQuotaResponse As GetSendQuotaResponse = client.GetSendQuota()

            If Not sendQuotaResponse Is Nothing AndAlso Not sendQuotaResponse.GetSendQuotaResult Is Nothing Then
                lblMax24Send.Text = CCommon.ToInteger(sendQuotaResponse.GetSendQuotaResult.Max24HourSend)
                lblSentLast24Hour.Text = CCommon.ToInteger(sendQuotaResponse.GetSendQuotaResult.SentLast24Hours)
            End If
        Catch ex As Exception
            lblMax24Send.Text = "0"
            lblSentLast24Hour.Text = "0"

            Throw
        End Try
    End Sub
    Private Sub GetBroadcastConfiguration()
        Try
            Dim objCampaign As New Campaign
            objCampaign.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dt As DataTable = objCampaign.GetBroadcastConfiguration()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                hdnConfigurationID.Value = CCommon.ToString(dt.Rows(0)("numConfigurationId"))
                hdnFrom.Value = CCommon.ToString(dt.Rows(0)("vcFrom"))
                hdnDomain.Value = CCommon.ToString(dt.Rows(0)("vcAWSDomain"))
                hdnAWSAccessKey.Value = CCommon.ToString(dt.Rows(0)("vcAWSAccessKey"))
                hdnAWSSecretKey.Value = CCommon.ToString(dt.Rows(0)("vcAWSSecretKey"))

                Try
                    If Not objCampaign.ValidateBroadcastConfiguration(hdnDomain.Value, hdnAWSAccessKey.Value, hdnAWSSecretKey.Value) Then
                        litMessage.Text = "Configured Amazon Simple Email Service (Amazon SES) account detail from Marketing -> e-mail Broadcast -> Configuration is invalid."
                    Else
                        GetAmazonSESUsageDetail()
                    End If
                Catch ex As Exception
                    If ex.Message = "EMAIL_BROADCAST_NOT_CONFIGURED" Then
                        litMessage.Text = "Please configure Amazon Simple Email Service (Amazon SES) account detail from Marketing -> e-mail Broadcast -> Configuration."
                    ElseIf ex.Message = "EMAIL_BROADCAST_CONFIGURATION_INVALID" Then
                        litMessage.Text = "Configured Amazon Simple Email Service (Amazon SES) account detail from Marketing -> e-mail Broadcast -> Configuration is invalid."
                    Else
                        Throw
                    End If

                    btnBroadcast.Visible = False
                    btnSaveClose.Visible = False
                    btnSendToMyself.Visible = False
                End Try
            Else
                btnBroadcast.Visible = False
                btnSaveClose.Visible = False
                btnSendToMyself.Visible = False

                litMessage.Text = "Please configure Amazon Simple Email Service (Amazon SES) account detail from Marketing -> e-mail Broadcast -> Configuration."
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Function CheckAttachmentSize() As Boolean
        Try
            Dim isValidSize As Boolean = True
            Dim contentLength As Double = 0

            If Not Session("Attachements") Is Nothing Then
                Dim dtAttachments As New DataTable
                Dim k As Integer
                Dim dtTable As DataTable = CType(Session("Attachements"), DataTable)
                Dim strSplitedNames() As String

                For k = 0 To dtTable.Rows.Count - 1
                    strSplitedNames = Split(dtTable.Rows(k).Item("FileLocation").ToString, "/")
                    dtTable.Rows(k).Item("FileLocation") = Replace(dtTable.Rows(k).Item("FileLocation").ToString, "../documents/docs/", Server.MapPath("../documents/docs/"))
                    'Checking whether the Files is present
                    Dim strLocation As String
                    strLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strSplitedNames(strSplitedNames.Length - 1).ToString
                    If File.Exists(strLocation) = True Then
                        Dim objAttach As New System.Net.Mail.Attachment(strLocation)
                        contentLength = (objAttach.ContentStream.Length / 1024.0F) / 1024.0F
                    ElseIf File.Exists(dtTable.Rows(k).Item("FileLocation").ToString) = True Then
                        Dim objAttach As New System.Net.Mail.Attachment(dtTable.Rows(k).Item("FileLocation").ToString)
                        contentLength = (objAttach.ContentStream.Length / 1024.0F) / 1024.0F
                    End If
                Next

                If contentLength >= 10 Then
                    isValidSize = False
                End If
            End If

            Return isValidSize
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub UpdateSendStatus(ByVal configurationDtlID As Long)
        Try
            Dim objCampaign As New Campaign
            objCampaign.UpdateEmailSendStatus(configurationDtlID)
        Catch ex As Exception

        End Try
    End Sub
    Function Managebroadcast(ByVal strOperation As String) As DataTable
        Try


            Dim objEmail As New Email
            Dim objCampaign As New Campaign

            'Adding Broadcasting detail without BroadCastBizMessage having Total Successfull = null bcoz same func call in SaveAndClose Event ...
            With objCampaign
                .broadcastID = CCommon.ToLong(GetQueryStringVal("ID"))
                .BroadCastName = txtName.Text
                .EmailTemplateID = ddlEmailTemplate.SelectedItem.Value
                .BroadCastSubject = txtSubject.Text
                .BroadCastMessage = RadEditor1.Content
                .TotalRecipients = lblRecords.Text
                '.TotalSucessfull = CLng(lblRecords.Text) - intUnsussesfull
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .Broadcasted = False
                .SearchID = lngSearchID
                .ConfigurationId = CCommon.ToLong(hdnConfigurationID.Value)
                lngBroadcastID = .ManageBroadCast()
            End With

            'Get Selected Recipients in str Variable
            Dim strContactID As String
            If CCommon.ToString(Session("ContIDs")) <> "" Then
                strContactID = CCommon.ToString(Session("ContIDs"))
            Else
                If lngBroadcastID > 0 Then
                    Dim dtBroadcastDetail As DataTable

                    objCampaign.broadcastID = lngBroadcastID
                    dtBroadcastDetail = objCampaign.GetBroadCastDetails()

                    If dtBroadcastDetail.Rows.Count > 0 Then
                        Dim j As Integer
                        For j = 0 To dtBroadcastDetail.Rows.Count - 1
                            strContactID = strContactID + CCommon.ToString(dtBroadcastDetail.Rows(j)("numContactID")) + ","
                        Next
                    End If
                End If
            End If

            'If send copy to myself checked then add UserContactID to str
            If strOperation.ToLower() = "broadcast" Then
                If chkSendCopy.Checked = True Then
                    strContactID = strContactID & "," & Session("UserContactID")
                End If
            End If

            Dim objContacts As New CContacts
            Dim dt As DataTable
            Dim dtEmailBroadCast As DataTable

            'Add All Contcts to BroadcastDtls having tintSucessfull = 0 .At the time of Broadcasting it will updated to 1
            If strContactID <> "" Then
                dt = objEmail.GetEmailMergeData(1, strContactID, Session("DomainID"), 0, 0)
                Dim dRows As DataRow() = dt.Select("bitOptOut = 0")
                If dRows.Length > 0 Then
                    dtEmailBroadCast = dRows.CopyToDataTable()
                    With objCampaign
                        .broadcastID = lngBroadcastID
                        dtEmailBroadCast.TableName = "Table"
                        Dim ds As New DataSet
                        ds.Tables.Add(dtEmailBroadCast.Copy)
                        .strBroadCastDtls = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))
                        .ManageBroadCastDTLs()
                    End With


                    objCampaign.DomainID = CCommon.ToLong(Session("DomainID"))
                    objCampaign.broadcastID = lngBroadcastID

                    dtEmailBroadCast = objCampaign.GetBroadCastEmailDetail()
                Else
                    litMessage.Text = "Select atleast one recipient to broadcast E-mail."
                    pnlMessage.CssClass = "warning"
                End If
            End If
            Return dtEmailBroadCast
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function ConvertMailMessageToMemoryStream(message As MailMessage) As MemoryStream
        Try
            Dim assembly As Assembly = GetType(SmtpClient).Assembly
            Dim mailWriterType As Type = assembly.[GetType]("System.Net.Mail.MailWriter")
            Dim fileStream As New MemoryStream()

            Dim mailWriterContructor As ConstructorInfo = mailWriterType.GetConstructor(BindingFlags.Instance Or BindingFlags.NonPublic, Nothing, New Type() {GetType(Stream)}, Nothing)
            Dim mailWriter As Object = mailWriterContructor.Invoke(New Object() {fileStream})

            Dim sendMethod As MethodInfo = GetType(MailMessage).GetMethod("Send", BindingFlags.Instance Or BindingFlags.NonPublic)
            sendMethod.Invoke(message, BindingFlags.Instance Or BindingFlags.NonPublic, Nothing, New Object() {mailWriter, True, True}, Nothing)

            Dim closeMethod As MethodInfo = mailWriter.[GetType]().GetMethod("Close", BindingFlags.Instance Or BindingFlags.NonPublic)
            closeMethod.Invoke(mailWriter, BindingFlags.Instance Or BindingFlags.NonPublic, Nothing, New Object() {}, Nothing)

            Return fileStream
        Catch ex As Exception
            Throw
        End Try

    End Function
    Private Sub SaveEmailExceptionDetail(ByVal configurationDtlID As Long, ByVal objEx As Exception)
        Try
            Dim objCampaign As New Campaign
            objCampaign.SaveEmailExceptionDetail(configurationDtlID, CCommon.ToString(objEx.GetType().Name), CCommon.ToString(objEx.Message), CCommon.ToString(objEx.StackTrace))
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SendRawEmail(ByVal dtEmailBroadCast As DataTable, ByVal fromEmail As String, ByVal ccEmail As String, ByVal bccEmail As String,
                            ByVal Subject As String, ByVal originalBody As String)
        Dim mailMessage As New MailMessage()

        Try

            If originalBody Is Nothing Then
                originalBody = ""
            End If

            mailMessage.From = New MailAddress(fromEmail)


            If Not String.IsNullOrEmpty(ccEmail) Then
                mailMessage.CC.Add(New MailAddress(ccEmail))
            End If

            If Not String.IsNullOrEmpty(bccEmail) Then
                mailMessage.Bcc.Add(New MailAddress(bccEmail))
            End If

            mailMessage.Subject = Subject
            mailMessage.SubjectEncoding = Encoding.UTF8

            If Not Session("Attachements") Is Nothing Then
                Dim dtAttachments As New DataTable
                Dim k As Integer
                Dim dtTable As DataTable = CType(Session("Attachements"), DataTable)
                Dim strSplitedNames() As String

                For k = 0 To dtTable.Rows.Count - 1
                    strSplitedNames = Split(dtTable.Rows(k).Item("FileLocation").ToString, "/")
                    dtTable.Rows(k).Item("FileLocation") = Replace(dtTable.Rows(k).Item("FileLocation").ToString, "../documents/docs/", Server.MapPath("../documents/docs/"))
                    'Checking whether the Files is present
                    Dim strLocation As String
                    strLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strSplitedNames(strSplitedNames.Length - 1).ToString
                    If File.Exists(strLocation) = True Then
                        Dim objAttach As New System.Net.Mail.Attachment(strLocation)
                        objAttach.ContentType = New ContentType("application/octet-stream")
                        Dim disposition As System.Net.Mime.ContentDisposition = objAttach.ContentDisposition
                        disposition.DispositionType = "attachment"
                        disposition.CreationDate = System.IO.File.GetCreationTime(strLocation)
                        disposition.ModificationDate = System.IO.File.GetLastWriteTime(strLocation)
                        disposition.ReadDate = System.IO.File.GetLastAccessTime(strLocation)
                        mailMessage.Attachments.Add(objAttach)
                    ElseIf File.Exists(dtTable.Rows(k).Item("FileLocation").ToString) = True Then
                        Dim objAttach As New System.Net.Mail.Attachment(dtTable.Rows(k).Item("FileLocation").ToString)
                        objAttach.ContentType = New ContentType("application/octet-stream")
                        Dim disposition As System.Net.Mime.ContentDisposition = objAttach.ContentDisposition
                        disposition.DispositionType = "attachment"
                        disposition.CreationDate = System.IO.File.GetCreationTime(dtTable.Rows(k).Item("FileLocation").ToString)
                        disposition.ModificationDate = System.IO.File.GetLastWriteTime(dtTable.Rows(k).Item("FileLocation").ToString)
                        disposition.ReadDate = System.IO.File.GetLastAccessTime(dtTable.Rows(k).Item("FileLocation").ToString)
                        mailMessage.Attachments.Add(objAttach)
                    End If
                Next
                Session("Attachements") = Nothing
            End If



            Dim count As Int16 = 0

            For Each dr As DataRow In dtEmailBroadCast.Rows
                Dim Body As String = originalBody
                Dim toEmail As String = CCommon.ToString(dr("ContactEmail"))

                Dim configurationDtlID As Long = 0

                'When SendToMySelf Button is clicked datatable do not contain column numBroadCastDtlId
                If dtEmailBroadCast.Columns.Contains("numBroadCastDtlId") Then
                    configurationDtlID = CCommon.ToLong(dr("numBroadCastDtlId"))
                End If

                For Each column As DataColumn In dtEmailBroadCast.Columns
                    Body = Body.Replace("##" & column.ColumnName & "##", CCommon.ToString(dr(column.ColumnName)))
                Next

                Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString(Regex.Replace(Body, "<(.|\n)*?>", String.Empty), Encoding.UTF8, "text/plain")
                mailMessage.AlternateViews.Add(plainView)

                Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString(Body, Encoding.UTF8, "text/html")
                mailMessage.AlternateViews.Add(htmlView)


                mailMessage.To.Clear()
                mailMessage.To.Add(toEmail)

                Dim toArray() As String = toEmail.Split(",")

                Dim rawMessage As New RawMessage()
                Using memoryStream As MemoryStream = ConvertMailMessageToMemoryStream(mailMessage)
                    rawMessage.Data = memoryStream
                End Using

                Dim request As New SendRawEmailRequest()
                request.RawMessage = rawMessage
                request.Source = fromEmail
                request.Destinations = toArray.ToList()

                Dim region As RegionEndpoint = RegionEndpoint.USWest2
                Dim client As New AmazonSimpleEmailServiceClient(hdnAWSAccessKey.Value, hdnAWSSecretKey.Value, region)

                Try
                    If count = CCommon.ToInteger(ConfigurationManager.AppSettings("AmazonSESMaxPerSec")) Then
                        count = 0
                        System.Threading.Thread.Sleep(1000)
                    End If

                    Dim response As SendRawEmailResponse = client.SendRawEmail(request)
                    Dim result As SendRawEmailResult = response.SendRawEmailResult

                    If (configurationDtlID > 0) Then
                        UpdateSendStatus(configurationDtlID)
                    End If
                Catch ex As Exception
                    If (configurationDtlID > 0) Then
                        SaveEmailExceptionDetail(configurationDtlID, ex)
                    Else
                        Throw
                    End If
                Finally
                    count = count + 1
                    'Added By Priya (25/12/2017)
                    mailMessage.AlternateViews.Remove(plainView)
                    mailMessage.AlternateViews.Remove(htmlView)

                End Try
            Next
        Catch ex As Exception
            Throw
        Finally
            mailMessage.Dispose()
        End Try
    End Sub
    Private Sub SendARStatementEmail(ByVal numDivisionID As Long, ByVal strContactIDs As String, ByVal toEmails As String, ByVal messageTo As String)
        Try
            Dim objEmail As New Email
            Dim objContacts As New CContacts
            Dim dtEmailBroadCast As New DataTable

            objContacts.strContactIDs = strContactIDs.TrimEnd(",")
            dtEmailBroadCast = objEmail.GetEmailMergeData(45, strContactIDs, Session("DomainID"), 0, 0)

            objContacts.MessageTo = messageTo.TrimEnd(New Char() {"#", "^"})
            objContacts.MessageFrom = Session("ContactName") & "$^$" & Session("UserEmail")
            objContacts.MessageFromName = Session("ContactName")
            objContacts.Cc = ""
            objContacts.Bcc = ""
            objContacts.Subject = txtSubject.Text
            objContacts.Body = RadEditor1.Content
            objContacts.ItemId = ""
            objContacts.ChangeKey = ""
            objContacts.bitRead = False
            objContacts.Type = "B"
            objContacts.tinttype = 2
            If Session("Attachements") Is Nothing Then
                objContacts.HasAttachment = False
            Else : objContacts.HasAttachment = True
            End If
            objContacts.CreatedOn = DateTime.UtcNow
            objContacts.MCategory = "B"
            objContacts.MessageFromName = ""
            objContacts.MessageToName = ""
            objContacts.CCName = ""

            Dim encoding As New ASCIIEncoding()
            Dim bytes As Byte() = encoding.GetBytes(RadEditor1.Content)
            Dim length As Long = bytes.Length


            objContacts.Size = length
            objContacts.AttachmentItemId = ""
            objContacts.AttachmentType = ""
            objContacts.FileName = ""
            objContacts.DomainID = Session("DomainId")
            objContacts.UserCntID = Session("UserContactId")
            objContacts.BodyText = objCommon.StripTags(RadEditor1.Content, False)
            objContacts.IsReplied = False
            objContacts.RepliedMailID = 0
            objContacts.bitInvisible = False   'chkInvisible.Checked  'By Priya

            Dim lngEmailHstrID As Long = objContacts.InsertIntoEmailHstr()


            Dim strBody As String

            If chkTrack.Checked = True Then
                strBody = RadEditor1.Content & "<img width='0px' height='0px' runat='server' src='" & ConfigurationManager.AppSettings("EmailTracking") & "?EmailHstrID=" & lngEmailHstrID & "' />"
            Else
                strBody = RadEditor1.Content
            End If

            If (strBody.Contains("##ARStatement##")) Then
                If objCommon Is Nothing Then
                    objCommon = New CCommon
                End If
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dsInvoices As DataSet = objCommon.GetCustomerARStatement(numDivisionID, IIf(hdnStatementDate.Value = "", DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow), Convert.ToDateTime(hdnStatementDate.Value)))

                If Not dsInvoices Is Nothing AndAlso dsInvoices.Tables.Count > 1 Then
                    Dim vcHTML As String = "<div class=""rowInner""><div class=""col-sm-12""><hr /></div></div>"

                    vcHTML = vcHTML & "<div class=""rowInner d-md-none"">" & _
                                    "<div class=""col-sm-12"">" & _
                                    "<div class=""table-responsive"">" & _
                                    "<table class=""table table-bordered"">" & _
                                    "<tr><th style=""background-color: rgba(0, 0, 0, 0.05)"">Statement Date</th><td>" & CCommon.ToString(dsInvoices.Tables(0).Rows(0)("dtStatementDate")) & "</td></tr>" & _
                                    "<tr><th style=""background-color: rgba(0, 0, 0, 0.05)"">Amounts Current</th><td>$" & CCommon.ToString(dsInvoices.Tables(0).Rows(0)("monCurrent")) & "</td></tr>" & _
                                    "<tr><th style=""background-color: rgba(0, 0, 0, 0.05)"">Amounts Past Due</th><td>$" & CCommon.ToString(dsInvoices.Tables(0).Rows(0)("monPastDue")) & "</td></tr>" & _
                                    "</table>" & _
                                    "</div>" & _
                                    "</div>" & _
                                    "</div>"

                    vcHTML = vcHTML & "<div class=""rowInner d-sm-none"">" & _
                                    "<div class=""col-sm-12"">" & _
                                    "<div class=""table-responsive"">" & _
                                    "<table class=""table table-bordered"">" & _
                                    "<thead class=""thead-light""><tr><th>Statement Date</th><th>Amounts Current</th><th>Amounts Past Due</th></tr></thead>" & _
                                    "<tbody>" & _
                                    "<tr>" & _
                                    "<td>" & CCommon.ToString(dsInvoices.Tables(0).Rows(0)("dtStatementDate")) & "</td>" & _
                                    "<td>$" & CCommon.ToString(dsInvoices.Tables(0).Rows(0)("monCurrent")) & "</td>" & _
                                    "<td>$" & CCommon.ToString(dsInvoices.Tables(0).Rows(0)("monPastDue")) & "</td>" & _
                                    "</tr>" & _
                                    "</tbody>" & _
                                    "</table>" & _
                                    "</div>" & _
                                    "</div>" & _
                                    "</div>"

                    Dim vcInvoicesSmallDevice As String = "<div class=""rowInner d-md-none""><div class=""col-sm-12""><div class=""table-responsive""><table class=""table table-bordered"">"
                    Dim vcInvoicesMediumDevice As String = "<div class=""rowInner d-sm-none""><div class=""col-sm-12""><div class=""table-responsive""><table class=""table table-bordered""><thead class=""thead-light"">" & _
                                            "<tr>" & _
                                                "<th>Invoice (Customer PO#)</th>" & _
                                                "<th>Amount Paid</th>" & _
                                                "<th>Amount Due (Due Date)</th>" & _
                                                "<th>Status</th>" & _
                                               IIf(CCommon.ToBool(dsInvoices.Tables(0).Rows(0)("bitShowCardConnectLink")) AndAlso Not String.IsNullOrEmpty(dsInvoices.Tables(0).Rows(0)("vcBluePayID")) AndAlso Not String.IsNullOrEmpty(dsInvoices.Tables(0).Rows(0)("vcBluePayPassword")), "<th></th>", "") & _
                                            "</tr>" & _
                                        "</thead>" & _
                                        "<tbody>"

                    If dsInvoices.Tables(1).Rows.Count > 0 Then
                        Dim testURL As New Bluepay(CCommon.ToString(dsInvoices.Tables(0).Rows(0)("vcBluePayID")), CCommon.ToString(dsInvoices.Tables(0).Rows(0)("vcBluePayPassword")), IIf(CCommon.ToBool(dsInvoices.Tables(0).Rows(0)("bitTest")), "TEST", "LIVE"))
                        Dim paymentURL As String = ""

                        For Each drInvoice As DataRow In dsInvoices.Tables(1).Rows

                            'SHPF_FORM_ID SHPF_ACCOUNT_ID MODE TPS_HASH_TYPE INVOICE_ID ORDER_ID ORDER_NAME INVOICE_NAME AMOUNT
                            paymentURL = testURL.generateURLForBiz(CCommon.ToString(dsInvoices.Tables(0).Rows(0)("vcBluePayFormName")), "https://portal.bizautomation.com/common/BluePayLinkPayment.aspx?domainID=" & CCommon.ToLong(Session("DomainID")), IIf(CCommon.ToBool(dsInvoices.Tables(0).Rows(0)("bitTest")), "TEST", "LIVE") & CCommon.ToString(drInvoice("numOppID")) & CCommon.ToString(drInvoice("numOppBizDocsId")) & CCommon.ToString(drInvoice("monAmountDue")), "MODE ORDER_ID INVOICE_ID AMOUNT", "HMAC_SHA512", CCommon.ToString(drInvoice("numOppBizDocsId")), CCommon.ToString(drInvoice("numOppID")), CCommon.ToString(drInvoice("vcPOppName")), CCommon.ToString(drInvoice("vcBizDocID")), CCommon.ToString(drInvoice("monAmountDue")), "")
                            paymentURL = paymentURL & "&NAME1=ARPAYNOW"

                            vcInvoicesSmallDevice = vcInvoicesSmallDevice & "<tr>" & _
                                                                            "<td>" & _
                                                                                "<div class=""rowInner"">" & _
                                                                                    "<div class=""col-sm-12"" style=""margin-bottom: 5px;"">" & _
                                                                                        "<b>Invoice</b>  (Customer PO#)<br />" & _
                                                                                        CCommon.ToString(drInvoice("vcBizDocID")) & IIf(CCommon.ToString(drInvoice("vcCustomerPO#")) <> "", " (" & CCommon.ToString(drInvoice("vcCustomerPO#")) & ")", "") & _
                                                                                    "</div>" & _
                                                                                "</div>" & _
                                                                                "<div class=""rowInner"">" & _
                                                                                    "<div class=""col-sm-12"" style=""margin-bottom: 5px;"">" & _
                                                                                        "<b>Amount Paid</b><br />" & _
                                                                                        "$" & CCommon.ToString(drInvoice("monAmountPaid")) & _
                                                                                    "</div>" & _
                                                                                "</div>" & _
                                                                                "<div class=""rowInner"">" & _
                                                                                    "<div class=""col-sm-12"" style=""margin-bottom: 5px;"">" & _
                                                                                        "<b>Amount Due</b> (Due Date)<br />" & _
                                                                                        "$" & CCommon.ToString(drInvoice("monAmountDue")) & " (" & CCommon.ToString(drInvoice("dtDueDate")) & ")" & _
                                                                                    "</div>" & _
                                                                                "</div>" & _
                                                                                "<div class=""rowInner"">" & _
                                                                                    "<div class=""col-sm-12"" style=""margin-bottom: 5px;"">" & _
                                                                                        "<b>Status</b><br />" & _
                                                                                        IIf(CCommon.ToLong(drInvoice("numPastDays")) > 0, "<span style=""color: rgb(255,0,0)"">Passed Due</span>", "<span style=""color: rgb(0,204,102)"">Current</span>") & _
                                                                                    "</div>" & _
                                                                                "</div>" & _
                                                                                IIf(CCommon.ToBool(dsInvoices.Tables(0).Rows(0)("bitShowCardConnectLink")) AndAlso Not String.IsNullOrEmpty(dsInvoices.Tables(0).Rows(0)("vcBluePayID")) AndAlso Not String.IsNullOrEmpty(dsInvoices.Tables(0).Rows(0)("vcBluePayPassword")), "<div class=""rowInner""><div class=""col-sm-12 text-center""><a href=""" & paymentURL & """ target=""_blank""><img src=""https://www.bizautomation.com/images/Paynow.png"" alt=""Pay Now"" /></a></div></div>", "") & _
                                                                                "</td>>" & _
                                                                            "</tr>"

                            vcInvoicesMediumDevice = vcInvoicesMediumDevice & "<tr>" & _
                                                                                "<td>" & CCommon.ToString(drInvoice("vcBizDocID")) & IIf(CCommon.ToString(drInvoice("vcCustomerPO#")) <> "", " (" & CCommon.ToString(drInvoice("vcCustomerPO#")) & ")", "") & "</td>" & _
                                                                                "<td>$" & CCommon.ToString(drInvoice("monAmountPaid")) & "</td>" & _
                                                                                "<td>$" & CCommon.ToString(drInvoice("monAmountDue")) & " (" & CCommon.ToString(drInvoice("dtDueDate")) & ")" & _
                                                                                IIf(CCommon.ToLong(drInvoice("numPastDays")) > 0, "<td style=""color: rgb(255,0,0)"">Passed Due</td>", "<td style=""color: rgb(0,204,102)"">Current</td>") & _
                                                                               IIf(CCommon.ToBool(dsInvoices.Tables(0).Rows(0)("bitShowCardConnectLink")) AndAlso Not String.IsNullOrEmpty(dsInvoices.Tables(0).Rows(0)("vcBluePayID")) AndAlso Not String.IsNullOrEmpty(dsInvoices.Tables(0).Rows(0)("vcBluePayPassword")), "<td><a href=""" & paymentURL & """ target=""_blank""><img src=""https://www.bizautomation.com/images/Paynow.png"" alt=""Pay Now"" /></a></td>", "") & _
                                                                            "</tr>"
                        Next
                    Else
                        vcInvoicesSmallDevice = vcInvoicesSmallDevice & "<tr><td>Records not found</td></tr>"
                        vcInvoicesMediumDevice = vcInvoicesMediumDevice & "<tr><td colspan=""" & IIf(CCommon.ToBool(dsInvoices.Tables(0).Rows(0)("bitShowCardConnectLink")), 5, 4) & """>Records not found</td></tr>"
                    End If

                    vcInvoicesSmallDevice = vcInvoicesSmallDevice & "</table></div></div></div>"
                    vcInvoicesMediumDevice = vcInvoicesMediumDevice & "</tbody></table></div></div></div>"


                    vcHTML = vcHTML & vcInvoicesSmallDevice & vcInvoicesMediumDevice
                    vcHTML = vcHTML & "<div class=""rowInner"">" & _
                                                        "<div class=""col-sm-12 text-center"">" & _
                                                            "<div style=""padding: 5px; background-color: rgb(255,255,204)"">" & _
                                                                "<b>Bill-To:</b>" & CCommon.ToString(dsInvoices.Tables(0).Rows(0)("vcCompanyName")) & _
                                                            "</div>" & _
                                                        "</div>" & _
                                                    "</div>" & _
                                                    "<div class=""rowInner"">" & _
                                                        "<div class=""col-sm-12"">" & _
                                                            "<hr />" & _
                                                        "</div>" & _
                                                    "</div>" & _
                                                    "<div class=""rowInner"">" & _
                                                        "<div class=""col-sm-12"">" & _
                                                            "<h4 style=""color: rgb(107,108,114);"">If you receive an email that seems fraudulent, please check with the business owner before paying.</h4>" & _
                                                        "</div>" & _
                                                    "</div>" & _
                                                    "<br />" & _
                                                    "<div class=""rowInner"">" & _
                                                        "<div class=""col-sm-12 text-center"">" & _
                                                            "<i style=""vertical-align: top; line-height: 38px; font-size: 14px;"">Powered By</i>&nbsp;&nbsp;<a href=""https://www.bizautomation.com/""><img itemprop=""image"" src=""https://www.bizautomation.com/images/logo.png"" alt=""BizAutomation"" /></a>" & _
                                                            "<br />" & _
                                                            "<h6>ONE system for your ENTIRE business</h6>" & _
                                                        "</div>" & _
                                                    "</div>"

                    strBody = strBody.Replace("##ARStatement##", vcHTML)
                    strBody = "<!DOCTYPE html><html lang=""en"" xmlns=""http://www.w3.org/1999/xhtml""><head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" /><meta name=""viewport"" content=""width=device-width, initial-scale=1"" /><title></title><style type=""text/css""> html { font-family: sans-serif; line-height: 1.15; -webkit-text-size-adjust: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); } body { margin: 0; font-family: -apple-system, BlinkMacSystemFont, ""Segoe UI"", Roboto, ""Helvetica Neue"", Arial, ""Noto Sans"", sans-serif, ""Apple Color Emoji"", ""Segoe UI Emoji"", ""Segoe UI Symbol"", ""Noto Color Emoji""; font-size: 1rem; font-weight: 400; line-height: 1.5; color: #212529; text-align: left; background-color: #fff; } hr { box-sizing: content-box; height: 0; overflow: visible; } h1, h2, h3, h4, h5, h6 { margin-top: 0; margin-bottom: 0.5rem; } p { margin-top: 0; margin-bottom: 1rem; } .text-center { text-align: center !important; } table { border-collapse: collapse; } .table .thead-light th { color: #495057; background-color: #e9ecef; border-color: #dee2e6; } .row { display: -ms-flexbox; display: flex; -ms-flex-wrap: wrap; flex-wrap: wrap; } .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto { position: relative; width: 100%; padding-right: 15px; padding-left: 15px; } .rowInner .col-sm-1,.rowInner .col-sm-2,.rowInner .col-sm-3,.rowInner .col-sm-4,.rowInner .col-sm-5,.rowInner .col-sm-6,.rowInner .col-sm-7,.rowInner .col-sm-8,.rowInner .col-sm-9,.rowInner .col-sm-10,.rowInner .col-sm-11,.rowInner .col-sm-12,.rowInner .col-sm,.rowInner .col-sm-auto,.rowInner .col-md-1,.rowInner .col-md-2,.rowInner .col-md-3,.rowInner .col-md-4,.rowInner .col-md-5,.rowInner .col-md-6,.rowInner .col-md-7,.rowInner .col-md-8,.rowInner .col-md-9,.rowInner .col-md-10,.rowInner .col-md-11,.rowInner .col-md-12,.rowInner .col-md,.rowInner .col-md-auto { padding-right: 0px; padding-left: 0px; } @media (min-width: 576px) { .col-sm { -ms-flex-preferred-size: 0; flex-basis: 0; -ms-flex-positive: 1; flex-grow: 1; min-width: 0; max-width: 100%; } .col-sm-auto { -ms-flex: 0 0 auto; flex: 0 0 auto; width: auto; max-width: 100%; } .col-sm-1 { -ms-flex: 0 0 8.333333%; flex: 0 0 8.333333%; max-width: 8.333333%; } .col-sm-2 { -ms-flex: 0 0 16.666667%; flex: 0 0 16.666667%; max-width: 16.666667%; } .col-sm-3 { -ms-flex: 0 0 25%; flex: 0 0 25%; max-width: 25%; } .col-sm-4 { -ms-flex: 0 0 33.333333%; flex: 0 0 33.333333%; max-width: 33.333333%; } .col-sm-5 { -ms-flex: 0 0 41.666667%; flex: 0 0 41.666667%; max-width: 41.666667%; } .col-sm-6 { -ms-flex: 0 0 50%; flex: 0 0 50%; max-width: 50%; } .col-sm-7 { -ms-flex: 0 0 58.333333%; flex: 0 0 58.333333%; max-width: 58.333333%; } .col-sm-8 { -ms-flex: 0 0 66.666667%; flex: 0 0 66.666667%; max-width: 66.666667%; } .col-sm-9 { -ms-flex: 0 0 75%; flex: 0 0 75%; max-width: 75%; } .col-sm-10 { -ms-flex: 0 0 83.333333%; flex: 0 0 83.333333%; max-width: 83.333333%; } .col-sm-11 { -ms-flex: 0 0 91.666667%; flex: 0 0 91.666667%; max-width: 91.666667%; } .col-sm-12 { -ms-flex: 0 0 100%; flex: 0 0 100%; max-width: 100%; } } @media (min-width: 768px) { .col-md { -ms-flex-preferred-size: 0; flex-basis: 0; -ms-flex-positive: 1; flex-grow: 1; min-width: 0; max-width: 100%; } .col-md-auto { -ms-flex: 0 0 auto; flex: 0 0 auto; width: auto; max-width: 100%; } .col-md-1 { -ms-flex: 0 0 8.333333%; flex: 0 0 8.333333%; max-width: 8.333333%; } .col-md-2 { -ms-flex: 0 0 16.666667%; flex: 0 0 16.666667%; max-width: 16.666667%; } .col-md-3 { -ms-flex: 0 0 25%; flex: 0 0 25%; max-width: 25%; } .col-md-4 { -ms-flex: 0 0 33.333333%; flex: 0 0 33.333333%; max-width: 33.333333%; } .col-md-5 { -ms-flex: 0 0 41.666667%; flex: 0 0 41.666667%; max-width: 41.666667%; } .col-md-6 { -ms-flex: 0 0 50%; flex: 0 0 50%; max-width: 50%; } .col-md-7 { -ms-flex: 0 0 58.333333%; flex: 0 0 58.333333%; max-width: 58.333333%; } .col-md-8 { -ms-flex: 0 0 66.666667%; flex: 0 0 66.666667%; max-width: 66.666667%; } .col-md-9 { -ms-flex: 0 0 75%; flex: 0 0 75%; max-width: 75%; } .col-md-10 { -ms-flex: 0 0 83.333333%; flex: 0 0 83.333333%; max-width: 83.333333%; } .col-md-11 { -ms-flex: 0 0 91.666667%; flex: 0 0 91.666667%; max-width: 91.666667%; } .col-md-12 { -ms-flex: 0 0 100%; flex: 0 0 100%; max-width: 100%; } } .table { width: 100%; margin-bottom: 1rem; color: #212529; } .table th, .table td { padding: 0.75rem; vertical-align: top; border-top: 1px solid #dee2e6; } .table thead th { vertical-align: bottom; border-bottom: 2px solid #dee2e6; } .table tbody + tbody { border-top: 2px solid #dee2e6; } .table-bordered { border: 1px solid #dee2e6; } .table-bordered th, .table-bordered td { border: 1px solid #dee2e6; } .table-bordered thead th, .table-bordered thead td { border-bottom-width: 2px; } .table-responsive { display: block; width: 100%; overflow-x: auto; -webkit-overflow-scrolling: touch; } .container { width: 100%; margin-right: auto; margin-left: auto; } @media (min-width: 576px) { .container { max-width: 540px; } } @media (min-width: 768px) { .container { max-width: 720px; } } @media (min-width: 992px) { .container { max-width: 960px; } } @media (min-width: 1200px) { .container { max-width: 1140px; } } @media (max-width: 576px) { .d-sm-none { display: none !important; } .d-sm-block { display: block !important; } } @media (min-width: 576px) { .d-md-none { display: none !important; } .d-md-block { display: block !important; } }</style></head><body><div class=""container""><div class=""row""><div class=""col-sm-12"">" & strBody & "</div></div></div></body></html>"
                Else
                    strBody = strBody.Replace("##ARStatement##", "")
                End If
            End If

            If strBody.ToLower().Contains("##ContactOpt-OutLink##".ToLower()) Then
                objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
            End If

            objEmail.AdditionalMergeFields.Add("Signature", Session("Signature"))
            For Each iKey As Object In objEmail.AdditionalMergeFields.Keys
                If dtEmailBroadCast.Columns.Contains(iKey.ToString) = False Then
                    dtEmailBroadCast.Columns.Add(iKey.ToString)
                End If
                'Assign values to rows in merge table
                For Each row As DataRow In dtEmailBroadCast.Rows
                    If iKey.ToString() <> "ContactEmail" Then
                        row(iKey.ToString) = CCommon.ToString(objEmail.AdditionalMergeFields(iKey))
                    End If
                Next
            Next

            Dim dtCustomMergeFields As DataTable = objEmail.GetCustomMergeFieldsData(45, Session("DomainID"), strContactIDs, 0)
            If (dtCustomMergeFields IsNot Nothing) Then
                For Each drow As DataRow In dtCustomMergeFields.Rows
                    If strBody.Contains(drow("CustomField_Name").ToString()) Then
                        strBody = strBody.Replace(drow("CustomField_Name").ToString(), drow("CustomField_Value").ToString())
                    End If
                Next
            End If

            objEmail.AddEmailToJob(txtSubject.Text, strBody, IIf(chkSendCopy.Checked, CCommon.ToString(Session("UserEmail")), ""), Session("UserEmail"), toEmails, dtEmailBroadCast, Session("ContactName"), "", strPageUrl:="/BACRMUI/Marketing/frmEmailBroadCasting.aspx")
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    
End Class
