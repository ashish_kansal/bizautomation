Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Partial Class frmNewCampaign
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                txtCampaignName.Focus()
                ' = "MarCampaign"
                LoadDropdowns()
                'trCampaignCode.Visible = True
            End If
            btnSave.Attributes.Add("onclick", "return Save('" & CCommon.GetValidationDateFormat() & "')")
            'txtCost.Attributes.Add("onkeypress", "CheckNumber(1,event)")
            txtNoSent.Attributes.Add("onkeypress", "CheckNumber(2,event)")
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDropdowns()
        Try
            
            'objCommon.sb_FillComboFromDBwithSel(ddlCampaignName, 24, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(dllCampaignStatus, 23, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlCampaignType, 22, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlRegion, 38, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objCampaign As New Campaign
            Dim boolSucess As Boolean = False
            objCampaign.CampaignID = 0
            objCampaign.CampaignName = txtCampaignName.Text.Trim()
            objCampaign.CampaignStatus = dllCampaignStatus.SelectedItem.Value
            objCampaign.CampaignType = ddlCampaignType.SelectedItem.Value
            objCampaign.CampaignRegion = ddlRegion.SelectedItem.Value
            objCampaign.LaunchDate = calLaunch.SelectedDate
            'end date is obsolete
            objCampaign.EndDate = calLaunch.SelectedDate 'calEnd.SelectedDate
            objCampaign.CampaignCost = 0 'IIf(Replace(txtCost.Text, ",", "") = "", 0, Replace(txtCost.Text, ",", ""))
            objCampaign.NoSent = IIf(Replace(txtNoSent.Text, ",", "") = "", 0, Replace(txtNoSent.Text, ",", ""))
            objCampaign.DomainID = Session("DomainID")
            objCampaign.UserCntID = Session("UserContactID")
            objCampaign.IsOnline = False 'rblCampaignMode.SelectedValue
            objCampaign.IsMonthly = chkMonthly.Checked
            objCampaign.CampaignCode = "" 'txtCampaignCode.Text
            objCampaign.byteMode = 0 'IIf(rblCampaignMode.SelectedValue, 2, 0)
            'Commented by chintan reason - bug id 465 #2
            'If objCampaign.CampaignOverlapChecking = 0 Then
            objCampaign.IsActive = True
            objCampaign.CampaignManage()
            If objCampaign.IsDuplicate Then
                litMessage.Text = "A campaign with given Campaign Code is found. Choose different Campaign Code."
                boolSucess = False
                Exit Sub
            End If
            boolSucess = True
            'Else
            'litMessage.Text = "A campaign with this name is active for a portion of the date range you've chosen. Choose a date range that doesn't overlap."
            'Exit Sub
            'End If
            If boolSucess Then
                Dim strScript As String = "<script language=JavaScript>"
                strScript += "window.opener.reDirectPage('../Marketing/frmCampaignDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CampID=" & objCampaign.CampaignID & "'); self.close();"
                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub rblCampaignMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblCampaignMode.SelectedIndexChanged
    '    Try
    '        If rblCampaignMode.SelectedValue = "True" Then
    '            trCampaignCode.Visible = True
    '        Else
    '            trCampaignCode.Visible = False
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
End Class
