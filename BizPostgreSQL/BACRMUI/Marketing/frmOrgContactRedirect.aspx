<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" EnableViewState="false"
    CodeBehind="frmOrgContactRedirect.aspx.vb" Inherits="frmOrgContactRedirect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title></title>
</head>
<body>
    <form id="frmOrgContactRedirect" method="post" runat="server">
    <asp:Literal ID="litClientMessage" runat="server"></asp:Literal>
    <input type="hidden" name="hdRedirectionNewWindow" id="hdRedirectionNewWindow" value="1"
        runat="server">
    <input type="hidden" name="hdRedirectionEntity" id="hdRedirectionEntity" runat="server"><input
        type="hidden" name="hdContactId" id="hdContactId" runat="server"><input type="hidden"
            name="hdDivisionId" id="hdDivisionId" runat="server">
    </form>
</body>
</html>
