<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmBroadcastDTLs.aspx.vb"
    Inherits="frmBroadcastDTLs" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <%--<link runat="server" rel="stylesheet" href="~/CSS/Import.css" type="text/css" id="AdaptersInvariantImportCSS" />--%>
    <title>Broadcast Details</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Broadcast Details
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:DataGrid ID="dgBroadCast" AllowSorting="True" runat="server" Width="900px" CssClass="dg"
        AutoGenerateColumns="False">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="Name" HeaderText="Contact Name">
            <ItemStyle width="150px"/> 
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Company" HeaderText="Company"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcEmail" HeaderText="Email"></asp:BoundColumn>
            <asp:BoundColumn DataField="Sent" HeaderText="Sent"></asp:BoundColumn>
            <asp:BoundColumn DataField="numNoofTimes" HeaderText="No of times opened">
            <ItemStyle Width = "125px"/>
            </asp:BoundColumn>
             <asp:TemplateColumn HeaderText = "Is Unsubscribed">
                  <ItemStyle width="110px"/> 
                  <ItemTemplate>
                       <asp:Label ID= "lblBroadcasted" Text ='<%# iif(Eval("bitUnsubscribe")=true,"Yes","No") %>'  runat ="server">
                       </asp:Label>
             </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="intNoOfClick" HeaderText="Link Clicked">
            <ItemStyle Width = "90px"/>
            </asp:BoundColumn>

        </Columns>
    </asp:DataGrid>
</asp:Content>
