﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmAssignDripCampaign
    Inherits BACRMPage
    Dim objCampaign As Campaign
    Dim lngECampaignAssigneeID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                
                LoadDropdown()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadDropdown()
        Try
            Dim objGenericAdvSearch As New FormGenericAdvSearch
            objGenericAdvSearch.UserCntID = Session("UserContactID")
            objGenericAdvSearch.DomainID = Session("DomainID")
            ddlEmailGroup.DataSource = objGenericAdvSearch.LoadDropdown()
            ddlEmailGroup.DataTextField = "Name"
            ddlEmailGroup.DataValueField = "GroupId"
            ddlEmailGroup.DataBind()
            ddlEmailGroup.Items.Insert(0, "--Select One--")
            ddlEmailGroup.Items.FindByText("--Select One--").Value = 0
            Dim dtECamp As DataTable
            If objCampaign Is Nothing Then objCampaign = New Campaign
            objCampaign.DomainID = Session("DomainID")
            dtECamp = objCampaign.GetECampaigns()
            ddlDripCampaign.DataSource = dtECamp
            ddlDripCampaign.DataTextField = "vcECampName"
            ddlDripCampaign.DataValueField = "numECampaignID"
            ddlDripCampaign.DataBind()
            ddlDripCampaign.Items.Insert(0, "--Select One--")
            ddlEmailGroup.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If objCampaign Is Nothing Then objCampaign = New Campaign
            objCampaign.ECampaignAssigneeID = lngECampaignAssigneeID
            objCampaign.ECampaignID = ddlDripCampaign.SelectedValue
            objCampaign.EmailGroupID = ddlEmailGroup.SelectedValue
            If calStartDate.SelectedDate = Nothing Then
                objCampaign.StartDate = Nothing
            Else
                objCampaign.StartDate = CDate(calStartDate.SelectedDate)
            End If
            objCampaign.DomainID = Session("DomainID")
            objCampaign.UserCntID = Session("UserContactID")
            lngECampaignAssigneeID = objCampaign.ManageEcampaignAssignee()
            'Engage all contacts in Email group to Slected E Campaign
            Dim dtContacts As DataTable
            Dim objEmailGroup As New BAddEmailGroup
            objEmailGroup.strGroupID = ddlEmailGroup.SelectedValue
            dtContacts = objEmailGroup.getEmailGroupContacts()
            For Each dr As DataRow In dtContacts.Rows
                objCampaign.ConEmailCampID = 0
                objCampaign.ContactId = dr("numContactId")
                objCampaign.Engaged = 1
                objCampaign.UserCntID = Session("UserContactID")
                objCampaign.ManageConECamp()
            Next
            If lngECampaignAssigneeID > 0 Then
                Response.Redirect("../Marketing/frmAssignedDripCampaignList.aspx", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../Marketing/frmAssignedDripCampaignList.aspx", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class