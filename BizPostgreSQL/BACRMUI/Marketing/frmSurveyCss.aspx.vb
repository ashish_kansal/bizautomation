﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO

Partial Public Class frmSurveyCss
    Inherits BACRMPage
    Dim lngCssId As Long
    Dim intType As Integer

    ' Type = 0 means css for site
    ' Type = 1 means js for site
    ' Type = 2 means css for portal (pass null as site id), change location of saved css file
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If GetQueryStringVal( "CssID") <> "" Then
            lngCssId = GetQueryStringVal( "CssID")
        End If
        If GetQueryStringVal( "Type") <> "" Then
            intType = GetQueryStringVal( "Type")
        End If
        If Not IsPostBack Then
            
            If lngCssId > 0 Then
                LoadDetails()
            End If
            lblName.Text = "Style"
            lblName1.Text = "Style"
            lblName2.Text = "Style"
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If lngCssId > 0 Then
                Dim objSite As New Sites
                objSite.WriteToFile(txtEditCss.Text, ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & Session("DomainID") & "\Survey\" & hdnCssFileName.Value)
                objSite.SiteID = 0
                objSite.CssID = lngCssId
                objSite.StyleName = txtStyleName.Text.Trim()
                objSite.DomainID = Session("DomainID")
                objSite.StyleType = intType
                objSite.ManageStyles()
                Response.Redirect("frmSurveyCssList.aspx?Type=" & intType.ToString(), False)
            Else
                If uploadCss() Then
                    Dim objSite As New Sites
                    objSite.SiteID = IIf(intType = 2, 0, Session("SiteID"))
                    objSite.CssID = lngCssId
                    objSite.StyleFile = fuCss.FileName
                    objSite.StyleName = txtStyleName.Text.Trim()
                    objSite.DomainID = Session("DomainID")
                    objSite.StyleType = intType

                    If objSite.ManageStyles() > 0 Then
                        Response.Redirect("frmSurveyCssList.aspx?Type=" & intType.ToString(), False)
                    Else
                        litMessage.Text = lblName1.Text + " name already chosen for this site."
                    End If
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function uploadCss() As Boolean
        Try
            Select Case intType
                Case 0
                    If fuCss.HasFile Then
                        If fuCss.FileName.ToLower.IndexOf(".css") <> -1 Then
                            UploadFile()
                            Return True
                        Else
                            litMessage.Text = "only *.css files are allowed to upload"
                            Return False
                        End If
                    Else
                        litMessage.Text = "Please select a css file upload"
                        Return False
                    End If
                Case 1
                    If fuCss.HasFile Then
                        If fuCss.FileName.ToLower.IndexOf(".js") <> -1 Then
                            UploadFile()
                            Return True
                        Else
                            litMessage.Text = "only *.js files are allowed to upload"
                            Return False
                        End If
                    Else
                        litMessage.Text = "Please select a js file upload"
                        Return False
                    End If
                Case Else
                    If fuCss.HasFile Then
                        If fuCss.FileName.ToLower.IndexOf(".css") <> -1 Then
                            UploadFile()
                            Return True
                        Else
                            litMessage.Text = "only *.css files are allowed to upload"
                            Return False
                        End If
                    Else
                        litMessage.Text = "Please select a css file upload"
                        Return False
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Sub UploadFile()
        Try
            Dim strUploadDirectory As String = String.Empty
            strUploadDirectory = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & Session("DomainID") & "\Survey"


            If Directory.Exists(strUploadDirectory) = False Then ' If Folder Does not exists create New Folder.
                Directory.CreateDirectory(strUploadDirectory)
            End If

            strUploadDirectory = strUploadDirectory & "\" & fuCss.PostedFile.FileName
            If Not fuCss.PostedFile Is Nothing Then fuCss.PostedFile.SaveAs(strUploadDirectory)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("frmSurveyCssList.aspx?Type=" & intType.ToString(), True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objSite As New Sites
            Dim dtStyle As DataTable
            Dim strUploadFile As String
            objSite.CssID = lngCssId
            objSite.SiteID = Session("SiteID")
            objSite.DomainID = Session("DomainID")
            objSite.StyleType = intType
            dtStyle = objSite.GetStyles()
            txtStyleName.Text = dtStyle.Rows(0)("StyleName")
            hdnCssFileName.Value = dtStyle.Rows(0)("StyleFileName")
            lblCssFileName.Text = "Editing File: <b>" & dtStyle.Rows(0)("StyleFileName") & "</b>"
            trEditCss.Visible = True
            trFileUpload.Visible = False


            strUploadFile = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & Session("DomainID") & "\Survey\" & dtStyle.Rows(0)("StyleFileName")
            
            Try
                txtEditCss.Text = objSite.ReadFile(strUploadFile)
            Catch ex As Exception
                litMessage.Text = "File <b>" & dtStyle.Rows(0)("StyleFileName") & "</b> Does not exist"
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class