﻿Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing

Public Class frmCampaignSurveyCreateRecord
    Inherits BACRMPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim numSurId As Integer = GetQueryStringVal( "numSurId")           'Get the Survey Id in local variable
            hdSurId.Value = numSurId   'Set the Survey Id in hidden

            Dim objSurvey As New SurveyAdministration                       'Create an object of Survey Administration Class
            objSurvey.DomainId = Session("DomainID")                        'Set the Domain ID
            objSurvey.UserId = Session("UserID")                            'Set the User ID
            objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
            objSurvey.SurveyId = hdSurId.Value                              'Set the Survey ID
            objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()

            If Not IsPostBack Then
                btnClose.Attributes.Add("onclick", "return Close()")
                BindElementsToMasterDataBase()

                Dim dtCreateRecord As DataTable                        'Declare a Datatable object
                dtCreateRecord = objSurvey.GetCreateRecordForSurvey() 'Get the answers for the selected question
                DisplayRecord(dtCreateRecord)                         'Display the answers
            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindElementsToMasterDataBase()
        Try
            Dim objConfigWizard As New BACRM.BusinessLogic.Admin.FormGenericFormContents        'Create an object of class which encaptulates the functionaltiy
            objConfigWizard.DomainID = Session("DomainID")                                      'set the domain id
            objConfigWizard.ListItemType = "LI"                                                  'set the listitem type
            Dim dtRelationTable As DataTable                                                    'declare a datatable
            dtRelationTable = objConfigWizard.GetMasterListByListId(5) 'call function to fill the datatable with Relationship
            Dim i As Integer
            For i = 1 To 3
                Dim ddlRelationShip As DropDownList = Me.Master.FindControl("Content").FindControl("ddlRelationShip" & i)
                ddlRelationShip.DataSource = dtRelationTable                                        'set the datasource of the Relationship drop down
                ddlRelationShip.DataTextField = "vcItemName"                                        'Set the Text property of the Relationship drop down
                ddlRelationShip.DataValueField = "numItemID"                                        'Set the value attribute of the Relationship drop down
                ddlRelationShip.DataBind()                                                          'Databind the drop down
            Next


            Dim dtGroupTable As DataTable                                                       'declare a datatable
            objConfigWizard.ListItemType = "AG"                                                  'set the listitem type
            dtGroupTable = objConfigWizard.GetMasterListByListId(38)                            'call function to fill the datatable with Groups

            For i = 1 To 3
                Dim ddlGroup As DropDownList = Me.Master.FindControl("Content").FindControl("ddlGroup" & i)
                ddlGroup.DataSource = dtGroupTable                                                  'set the datasource of the Groups drop down
                ddlGroup.DataTextField = "vcItemName"                                               'Set the Text property of the Groups drop down
                ddlGroup.DataValueField = "numItemID"                                               'Set the value attribute of the Groups drop down
                ddlGroup.DataBind()                                                                 'Databind the drop down
            Next

            Dim dtRecordOwner As DataTable                                                      'declare a datatable
            Dim objGenericAdvSearch As New BACRM.BusinessLogic.Admin.FormGenericAdvSearch       'Create an object of class which encaptulates the functionaltiy
            objGenericAdvSearch.DomainID = Session("DomainID")                                  'set the domain id
            dtRecordOwner = objGenericAdvSearch.getAvailableEmployees()                         'Call to get the list of employees
            Dim drRecordOwner As DataRow = dtRecordOwner.NewRow                                 'Get a new row instance of campaign
            drRecordOwner.Item("vcUserName") = "Let Auto Routing Rules decide"                  'Set the displayable text 
            drRecordOwner.Item("numUserID") = 0                                                 'Set the Id as 0
            dtRecordOwner.Rows.InsertAt(drRecordOwner, 0)                                       'Insert the new row at position 0

            For i = 1 To 3
                Dim ddlRecordOwner As DropDownList = Me.Master.FindControl("Content").FindControl("ddlRecordOwner" & i)
                ddlRecordOwner.DataSource = dtRecordOwner                                           'Set the datasource of the record owners drop down
                ddlRecordOwner.DataTextField = "vcUserName"                                         'Set the Text displayed in the dropdown as the employee names
                ddlRecordOwner.DataValueField = "numUserId"                                         'Set the values displayed in the dropdown as the employee Ids
                ddlRecordOwner.DataBind()                                                           'DataBind the Record Owner drop down
            Next

            objCommon = New CCommon
            For i = 1 To 3
                Dim ddFollowUpStatus As DropDownList = Me.Master.FindControl("Content").FindControl("ddFollowUpStatus" & i)
                objCommon.sb_FillComboFromDBwithSel(ddFollowUpStatus, 30, Session("DomainID"))
            Next

            For i = 1 To 3
                Dim ddlProfile As DropDownList = Me.Master.FindControl("Content").FindControl("ddlProfile" & i)
                Dim ddlRelationShip As DropDownList = Me.Master.FindControl("Content").FindControl("ddlRelationShip" & i)
                LoadProfile(ddlProfile, ddlRelationShip.SelectedValue)
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub DisplayRecord(ByVal dtCreateRecord As DataTable)
        Try

            Dim numIndex As Integer                                                   'Declare the row index
            For numIndex = 1 To dtCreateRecord.Rows.Count

                Dim ddlRelationShip As DropDownList = Me.Master.FindControl("Content").FindControl("ddlRelationShip" & numIndex)
                Dim ddlGroup As DropDownList = Me.Master.FindControl("Content").FindControl("ddlGroup" & numIndex)
                Dim ddlRecordOwner As DropDownList = Me.Master.FindControl("Content").FindControl("ddlRecordOwner" & numIndex)
                Dim ddlCRMType As DropDownList = Me.Master.FindControl("Content").FindControl("ddlCRMType" & numIndex)
                Dim ddlProfile As DropDownList = Me.Master.FindControl("Content").FindControl("ddlProfile" & numIndex)
                Dim ddFollowUpStatus As DropDownList = Me.Master.FindControl("Content").FindControl("ddFollowUpStatus" & numIndex)
                Dim rbDefault As RadioButton = Me.Master.FindControl("Content").FindControl("rbDefault" & numIndex)

                Dim txtRatingMin As TextBox = Me.Master.FindControl("Content").FindControl("txtRatingMin" & numIndex)
                Dim txtRatingMax As TextBox = Me.Master.FindControl("Content").FindControl("txtRatingMax" & numIndex)

                txtRatingMin.Text = dtCreateRecord.Rows(numIndex - 1).Item("numRatingMin")
                txtRatingMax.Text = dtCreateRecord.Rows(numIndex - 1).Item("numRatingMax")

                If (ddlRelationShip.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("numRelationShipId")) IsNot Nothing) Then
                    ddlRelationShip.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("numRelationShipId")).Selected = True
                End If

                If (ddlGroup.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("numGrpId")) IsNot Nothing) Then
                    ddlGroup.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("numGrpId")).Selected = True
                End If

                If (ddlRecordOwner.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("numRecOwner")) IsNot Nothing) Then
                    ddlRecordOwner.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("numRecOwner")).Selected = True
                End If

                If (ddlCRMType.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("tIntCRMType")) IsNot Nothing) Then
                    ddlCRMType.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("tIntCRMType")).Selected = True
                End If

                If (ddFollowUpStatus.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("numFollowUpStatus")) IsNot Nothing) Then
                    ddFollowUpStatus.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("numFollowUpStatus")).Selected = True
                End If

                LoadProfile(ddlProfile, ddlRelationShip.SelectedValue)

                If ddlProfile.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("numProfileId")) IsNot Nothing Then
                    ddlProfile.Items.FindByValue(dtCreateRecord.Rows(numIndex - 1).Item("numProfileId")).Selected = True
                End If

                If Not IsDBNull(dtCreateRecord.Rows(numIndex - 1).Item("bitDefault")) Then
                    If dtCreateRecord.Rows(numIndex - 1).Item("bitDefault") = True Then
                        rbDefault.Checked = True
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSaveAnswer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAnswer.Click
        Try
            Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
            objSurvey.DomainId = Session("DomainID")                            'Set the Domain ID
            objSurvey.UserId = Session("UserID")                                'Set the User ID
            objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
            objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID

            objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()         'Call to retrieve the existing data temporarily
            UpdateRecords(objSurvey)                                            'Update the rows in the answer table
            objSurvey.SaveSurveyInformationTemp()                               'Call to save the existing data temporarily
            Dim btnSrcButton As Button = CType(sender, Button)                  'typecast to button

            Dim dtCreateRecord As DataTable                            'Declare a Datatable object
            dtCreateRecord = objSurvey.GetCreateRecordForSurvey()   'Get the Records for the survey
            DisplayRecord(dtCreateRecord)                             'Display the Records
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to sync the dataset of Survey Information with the latest changes
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/14/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Function UpdateRecords(ByRef objSurvey As SurveyAdministration)
        Try
            Dim dtSurveyAnswerMaster As DataTable
            dtSurveyAnswerMaster = objSurvey.SurveyInfo.Tables("SurveyCreateRecord")
            Dim numSurveyAnwerMasterTableRowIndex As Integer                   'Declare a index for datatable rows
            Dim numSurveyAnwerMasterRowIndex As Integer = 1                     'Declare a index for htmltable rows
            Dim i As Integer = 1
            While numSurveyAnwerMasterTableRowIndex < dtSurveyAnswerMaster.Rows.Count
                If (Request.Form("txtRatingMin" & i) <> "" Or Request.Form("txtRatingMax" & i) <> "") Then

                    Dim ddlRelationShip As DropDownList = Me.Master.FindControl("Content").FindControl("ddlRelationShip" & i)
                    Dim ddlGroup As DropDownList = Me.Master.FindControl("Content").FindControl("ddlGroup" & i)
                    Dim ddlRecordOwner As DropDownList = Me.Master.FindControl("Content").FindControl("ddlRecordOwner" & i)
                    Dim ddlCRMType As DropDownList = Me.Master.FindControl("Content").FindControl("ddlCRMType" & i)
                    Dim ddlProfile As DropDownList = Me.Master.FindControl("Content").FindControl("ddlProfile" & i)
                    Dim ddFollowUpStatus As DropDownList = Me.Master.FindControl("Content").FindControl("ddFollowUpStatus" & i)
                    Dim rbDefault As RadioButton = Me.Master.FindControl("Content").FindControl("rbDefault" & i)

                    Dim txtRatingMin As TextBox = Me.Master.FindControl("Content").FindControl("txtRatingMin" & i)
                    Dim txtRatingMax As TextBox = Me.Master.FindControl("Content").FindControl("txtRatingMax" & i)

                    dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numSurID") = hdSurId.Value 'set the Survey ID
                    dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numRatingMin") = txtRatingMin.Text
                    dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numRatingMax") = txtRatingMax.Text
                    dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numRelationShipId") = ddlRelationShip.SelectedValue
                    dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numGrpId") = ddlGroup.SelectedValue
                    dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numRecOwner") = ddlRecordOwner.SelectedValue
                    dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("tIntCRMType") = ddlCRMType.SelectedValue
                    dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("bitDefault") = rbDefault.Checked

                    If ddlProfile.SelectedValue.Length > 0 Then
                        dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numProfileId") = ddlProfile.SelectedValue
                    Else
                        dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numProfileId") = 0
                    End If

                    dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numFollowUpStatus") = ddFollowUpStatus.SelectedValue

                    dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Item("numCreateRecordId") = numSurveyAnwerMasterRowIndex

                    numSurveyAnwerMasterRowIndex += 1                       'increment the htmltable row counter
                Else
                    objSurvey.DeleteSurveyCreateRecord(numSurveyAnwerMasterRowIndex)  'Request deletion of rules
                    dtSurveyAnswerMaster.Rows(numSurveyAnwerMasterTableRowIndex).Delete() 'Delete the answer row
                    numSurveyAnwerMasterTableRowIndex -= 1                  'since row is deleted so decrement the counter
                End If
                i += 1
                numSurveyAnwerMasterTableRowIndex += 1                          'increment the datatable row counter
            End While
            Dim drSurveyAnswerMasterRow As DataRow                              'Declare a DataRow

            While i <= 3
                If (Request.Form("txtRatingMin" & i) <> "" Or Request.Form("txtRatingMax" & i) <> "") Then

                    drSurveyAnswerMasterRow = dtSurveyAnswerMaster.NewRow()         'Get an new row of the table

                    Dim ddlRelationShip As DropDownList = Me.Master.FindControl("Content").FindControl("ddlRelationShip" & i)
                    Dim ddlGroup As DropDownList = Me.Master.FindControl("Content").FindControl("ddlGroup" & i)
                    Dim ddlRecordOwner As DropDownList = Me.Master.FindControl("Content").FindControl("ddlRecordOwner" & i)
                    Dim ddlCRMType As DropDownList = Me.Master.FindControl("Content").FindControl("ddlCRMType" & i)
                    Dim ddlProfile As DropDownList = Me.Master.FindControl("Content").FindControl("ddlProfile" & i)
                    Dim ddFollowUpStatus As DropDownList = Me.Master.FindControl("Content").FindControl("ddFollowUpStatus" & i)
                    Dim rbDefault As RadioButton = Me.Master.FindControl("Content").FindControl("rbDefault" & i)

                    Dim txtRatingMin As TextBox = Me.Master.FindControl("Content").FindControl("txtRatingMin" & i)
                    Dim txtRatingMax As TextBox = Me.Master.FindControl("Content").FindControl("txtRatingMax" & i)

                    drSurveyAnswerMasterRow.Item("numSurID") = hdSurId.Value 'set the Survey ID
                    drSurveyAnswerMasterRow.Item("numRatingMin") = txtRatingMin.Text
                    drSurveyAnswerMasterRow.Item("numRatingMax") = txtRatingMax.Text
                    drSurveyAnswerMasterRow.Item("numRelationShipId") = ddlRelationShip.SelectedValue
                    drSurveyAnswerMasterRow.Item("numGrpId") = ddlGroup.SelectedValue
                    drSurveyAnswerMasterRow.Item("numRecOwner") = ddlRecordOwner.SelectedValue
                    drSurveyAnswerMasterRow.Item("tIntCRMType") = ddlCRMType.SelectedValue
                    drSurveyAnswerMasterRow.Item("bitDefault") = rbDefault.Checked

                    If ddlProfile.SelectedValue.Length > 0 Then
                        drSurveyAnswerMasterRow.Item("numProfileId") = ddlProfile.SelectedValue
                    Else
                        drSurveyAnswerMasterRow.Item("numProfileId") = 0
                    End If

                    drSurveyAnswerMasterRow.Item("numFollowUpStatus") = ddFollowUpStatus.SelectedValue

                    drSurveyAnswerMasterRow.Item("numCreateRecordId") = numSurveyAnwerMasterRowIndex 'set the ans id

                    dtSurveyAnswerMaster.Rows.Add(drSurveyAnswerMasterRow)          'Add the question to the table
                    numSurveyAnwerMasterRowIndex += 1                               'increment the datatable row counter
                End If
                i += 1
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function ReSetFromXML(ByVal sValue As String) As String
        Try
            Return Replace(Replace(Replace(Replace(sValue, "&amp;", "&"), "&#39;", "'"), "&lt;", "<"), "&gt;", ">")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function MakeSafeForXML(ByVal sValue As String) As String
        Try
            Return Replace(Replace(Replace(Replace(Replace(sValue, "'", "&#39;"), """", "&#39;&#39;"), "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ddlRelationShip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationShip1.SelectedIndexChanged, ddlRelationShip2.SelectedIndexChanged, ddlRelationShip3.SelectedIndexChanged
        Dim ddlRelationShip As DropDownList = sender
        Dim ddlProfile As DropDownList = Me.Master.FindControl("Content").FindControl("ddlProfile" & ddlRelationShip.ID.Replace("ddlRelationShip", ""))

        LoadProfile(ddlProfile, ddlRelationShip.SelectedValue)
    End Sub
    Sub LoadProfile(ByVal ddlProfile As DropDownList, ByVal numRelationShip As Long)
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.RelID = numRelationShip
            objUserAccess.DomainID = Session("DomainID")
            ddlProfile.DataSource = objUserAccess.GetRelProfileD
            ddlProfile.DataTextField = "ProName"
            ddlProfile.DataValueField = "numProfileID"
            ddlProfile.DataBind()
            ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class