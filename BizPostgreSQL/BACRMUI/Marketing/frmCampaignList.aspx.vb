Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing

Namespace BACRM.UserInterface.Marketing
    Public Class frmCampaignList
        Inherits BACRMPage
        Dim objCommon As CCommon

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                If Not IsPostBack Then
                    ' Checking Rights for View
                    GetUserRightsForPage(MODULEID.Marketing, 1)

                    Dim lobjGeneralLedger As New GeneralLedger
                    lobjGeneralLedger.DomainID = Session("DomainId")
                    lobjGeneralLedger.Year = CInt(Now.Year)
                    calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()   ''DateAdd(DateInterval.Day, -7, Now())
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.Now) ' DateAdd(DateInterval.Day, 0, Now())

                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.sb_FillComboFromDBwithSel(ddlCampaign, 18, Session("DomainID"))

                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub


        Sub BindDatagrid()
            Try
                gvCampaign.Columns.Clear()
                Dim fromDate As DateTime
                Dim toDate As DateTime
                Dim boundColumn As BoundField
                Dim index As Integer = 0

                If DateTime.TryParse(calFrom.SelectedDate, fromDate) AndAlso DateTime.TryParse(calTo.SelectedDate, toDate) Then
                    Dim objCampaign As New Campaign
                    objCampaign.DomainID = Session("DomainID")
                    objCampaign.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                    objCampaign.ReportFromDate = fromDate
                    objCampaign.ReportToDate = toDate
                    objCampaign.ReportView = CCommon.ToShort(ddlColumnType.SelectedValue)
                    objCampaign.CampaignID = CCommon.ToLong(ddlCampaign.SelectedValue)
                    Dim dtCampaign As DataTable = objCampaign.GetCampaignReport()

                    If Not dtCampaign Is Nothing AndAlso dtCampaign.Rows.Count > 0 Then
                        Dim dtFinal As New DataTable

                        For Each column As DataColumn In dtCampaign.Columns
                            dtFinal.Columns.Add(column.ColumnName, column.DataType)

                            If column.ColumnName <> "vcDivisionIDs" Then
                                boundColumn = New BoundField
                                boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                                If column.ColumnName = "Campaign" Then
                                    boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                                Else
                                    boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                                End If
                                boundColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                                boundColumn.DataField = column.ColumnName
                                boundColumn.HeaderText = column.ColumnName
                                boundColumn.HtmlEncode = False
                                boundColumn.ItemStyle.Wrap = False
                                boundColumn.ItemStyle.Width = New Unit(10, UnitType.Pixel)
                                gvCampaign.Columns.Insert(index, boundColumn)
                                index = index + 1
                            End If
                        Next

                        If ddlColumnType.SelectedValue = "1" Or ddlColumnType.SelectedValue = "2" Then
                            dtFinal.Columns.Add("Totals", GetType(System.String))
                            boundColumn = New BoundField
                            boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                            boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                            boundColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                            boundColumn.DataField = "Totals"
                            boundColumn.HeaderText = "Totals"
                            boundColumn.HtmlEncode = False
                            boundColumn.ItemStyle.Wrap = False
                            boundColumn.ItemStyle.Width = New Unit(10, UnitType.Pixel)
                            gvCampaign.Columns.Insert(index, boundColumn)
                            index = index + 1

                            dtFinal.Columns.Add("Averages", GetType(System.String))
                            boundColumn = New BoundField
                            boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                            boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                            boundColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                            boundColumn.DataField = "Averages"
                            boundColumn.HeaderText = "Averages"
                            boundColumn.HtmlEncode = False
                            boundColumn.ItemStyle.Wrap = False
                            boundColumn.ItemStyle.Width = New Unit(10, UnitType.Pixel)
                            gvCampaign.Columns.Insert(index, boundColumn)
                        End If

                        Dim drNew As DataRow
                        Dim displayValue As String = ""
                        Dim listMonths As System.Collections.Generic.List(Of CampaignPerformanceParameter)

                        For Each dr As DataRow In dtCampaign.Rows
                            listMonths = New Generic.List(Of CampaignPerformanceParameter)

                            drNew = dtFinal.NewRow()
                            drNew("Campaign") = "<b>" & CCommon.ToString(dr("Campaign")) & "</b></br/>" & _
                                "Lead Count / Cost per Lead" & "</br>" & _
                                "Customer Count / Cost per Customer" & "</br>" & _
                                "Conversion % / Avg Days to Convert" & "</br>" & _
                                "Campaign Income & (Expense)" & "</br>" & _
                                "Resulting Gross Profit or (Loss)"

                            For Each column As DataColumn In dtCampaign.Columns
                                If column.ColumnName <> "Campaign" AndAlso column.ColumnName <> "vcDivisionIDs" Then
                                    displayValue = ""
                                    Dim objValue As CampaignPerformanceParameter = Newtonsoft.Json.JsonConvert.DeserializeObject(Of CampaignPerformanceParameter)(CCommon.ToString(dr(column.ColumnName)))
                                    listMonths.Add(objValue)


                                    displayValue = String.Format(CCommon.GetDataFormatStringWithCurrency(), objValue.TotalIncome) & "<br/>" & _
                                                    objValue.LeadCount & " / " & String.Format(CCommon.GetDataFormatStringWithCurrency(), objValue.CostPerLead) & "<br/>" & _
                                                    objValue.CustomerCount & " / " & String.Format(CCommon.GetDataFormatStringWithCurrency(), objValue.CostPerCustomer) & "<br/>" & _
                                                    "<a href=""javascript:ShowFirstOrders('" & CCommon.ToString(objValue.DivisionIDs) & "')"">" & String.Format("{0:#,##0.00}", objValue.ConversionPercent) & "%</a> / " & objValue.AverageDaysToConvert & "<br/>" & _
                                                    String.Format(CCommon.GetDataFormatStringWithCurrency(), objValue.CampaignIncome) & " (" & String.Format(CCommon.GetDataFormatStringWithCurrency(), objValue.CampaignExpense) & ")<br/>" & _
                                                    If(objValue.GrossProfit >= 0, "<span style='color:#00b050'>", "<span style='color:#ff0000'>") & String.Format(CCommon.GetDataFormatStringWithCurrency(), objValue.GrossProfit) & "</span>"


                                    If column.ColumnName = "Totals" Then
                                        displayValue = "<b>" & displayValue & "</b>"
                                    End If

                                    drNew(column.ColumnName) = displayValue
                                End If
                            Next

                            If ddlColumnType.SelectedValue = "1" Or ddlColumnType.SelectedValue = "2" Then

                                drNew("Totals") = "<b>" & String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Sum(Function(x) x.TotalIncome)) & "<br/>" & _
                                                    listMonths.Sum(Function(x) x.LeadCount) & " / " & String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Sum(Function(x) x.CostPerLead)) & "<br/>" & _
                                                    listMonths.Sum(Function(x) x.CustomerCount) & " / " & String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Sum(Function(x) x.CostPerCustomer)) & "<br/>" & _
                                                    "<br/>" & _
                                                    String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Sum(Function(x) x.CampaignIncome)) & " (" & String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Sum(Function(x) x.CampaignExpense)) & ")<br/>" & _
                                                    If(listMonths.Sum(Function(x) x.GrossProfit) >= 0, "<span style='color:#00b050'>", "<span style='color:#ff0000'>") & String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Sum(Function(x) x.GrossProfit)) & "</span></b>"

                                drNew("Averages") = "<b>" & String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Average(Function(x) x.TotalIncome)) & "<br/>" & _
                                                    listMonths.Average(Function(x) x.LeadCount) & " / " & String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Average(Function(x) x.CostPerLead)) & "<br/>" & _
                                                    listMonths.Average(Function(x) x.CustomerCount) & " / " & String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Average(Function(x) x.CostPerCustomer)) & "<br/>" & _
                                                    String.Format("{0:#,##0.00}", listMonths.Average(Function(x) x.ConversionPercent)) & "% / " & listMonths.Average(Function(x) x.AverageDaysToConvert) & "<br/>" & _
                                                    String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Average(Function(x) x.CampaignIncome)) & " (" & String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Average(Function(x) x.CampaignExpense)) & ")<br/>" & _
                                                    If(listMonths.Average(Function(x) x.GrossProfit) >= 0, "<span style='color:#00b050'>", "<span style='color:#ff0000'>") & String.Format(CCommon.GetDataFormatStringWithCurrency(), listMonths.Average(Function(x) x.GrossProfit)) & "</span></b>"
                            End If

                            dtFinal.Rows.Add(drNew)
                        Next

                        gvCampaign.DataSource = dtFinal
                        gvCampaign.DataBind()
                    End If
                Else
                    DisplayError("Invalid from/to Date.")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Private Class CampaignPerformanceParameter
            Public Property TotalIncome As Double
            Public Property LeadCount As Integer
            Public Property CostPerLead As Double
            Public Property CustomerCount As Integer
            Public Property CostPerCustomer As Double
            Public Property ConversionPercent As Double
            Public Property AverageDaysToConvert As Integer
            Public Property CampaignIncome As Double
            Public Property CampaignExpense As Double
            Public Property GrossProfit As Double
            Public Property DivisionIDs As String
        End Class

        Protected Sub ddlCampaign_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub ddlColumnType_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnUpdate_Click(sender As Object, e As EventArgs)
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnShowOrders_Click(sender As Object, e As EventArgs)
            Try
                Dim objCampaign As New Campaign
                objCampaign.DomainID = Session("DomainID")
                objCampaign.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                gvOrders.DataSource = objCampaign.GetConversionPercentOrders(hdnDivisionIDs.Value)
                gvOrders.DataBind()

                hdnDivisionIDs.Value = ""
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ShowOrdersModal", "$('#modal-orders').modal('show');", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class
End Namespace