<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmCampaignSurveyReportQAAvgExcel.aspx.vb" Inherits="BACRM.UserInterface.Survey.frmCampaignSurveyReportQAAvgExcel" ValidateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
		<link runat="server" rel="stylesheet" href="~/CSS/Import.css" type="text/css" id="AdaptersInvariantImportCSS" />

        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>Q & A Averages</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body >
		<form id="frmCampaignSurveyReportQAAvgExcel" method="post" runat="server">
			<asp:button id="btnPrint" Runat="Server" Text="Print" style="DISPLAY:none" cssClass="button"
				CausesValidation="False"></asp:button>
			<input type="hidden" runat="server" id="hdQAAvgLayout" name="hdQAAvgLayout">
			<asp:Label Runat="Server" ID="litClientScriptForExport"></asp:Label>
			<asp:table id="tblSurveyQAAvg" CellPadding="0" CellSpacing="1" BorderWidth="1" Runat="server"
				Width="100%" BorderColor="Black" GridLines="None" CssClass="dg">
				<asp:TableRow Height="22px">
					<asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
						<asp:label id="lblSurNameLbl" CssClass="text_bold" style="font-weight:bold;" Runat="server" Text="Survey Name: "></asp:label>
						<asp:label id="lblSurveyName" CssClass="text_bold" style="font-weight:bold;" Runat="server"></asp:label>
					</asp:TableCell>
				</asp:TableRow>
				<asp:TableRow Height="22px">
					<asp:TableCell Width="5%" style="PADDING-LEFT: 5px;font-weight:bold;" Text="Q.No." CssClass="normalbol"></asp:TableCell>
					<asp:TableCell Width="95%" style="font-weight:bold;" ColumnSpan="2" Text="Survey Question" CssClass="normalbol"></asp:TableCell>
				</asp:TableRow>
			</asp:table>
		</form>
	</body>
</HTML>
