''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Survey
''' Class	 : frmCampaignSurveyWorflowRuleEditor
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a interface for editing a Survey answers Rules
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	09/16/2005	Created
''' </history>
''' ''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Survey
    Public Class frmCampaignSurveyWorflowRuleEditor
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSaveWorkFlow As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveAndCloseWorkFlow As System.Web.UI.WebControls.Button
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents tblWorkFlowRulesEdited As System.Web.UI.WebControls.Table
        Protected WithEvents hdSurId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdAnsId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdQId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdAction As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents ddlOrderOfEventsOne As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlOrderOfEventsTwo As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlOrderOfEventsThree As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlOrderOfEventsFour As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlOrderOfEventsFive As System.Web.UI.WebControls.DropDownList
        Protected WithEvents cbActivationRuleOne As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbActivationRuleTwo As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbActivationRuleThree As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbActivationRuleFour As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbActivationRuleFive As System.Web.UI.WebControls.CheckBox
        Protected WithEvents txtAskQuestions As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtLinkedSurveyID As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtSkipQuestions As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtLinkedSkipSurveyID As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtOpenURL As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtAddSurveyRating As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlCRMType As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlRelationShip As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlGroup As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlRecordOwner As System.Web.UI.WebControls.DropDownList
        Protected WithEvents litClientMessage As System.Web.UI.WebControls.Literal

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime the page is called. In this event we will 
        '''     get the data from the DB and populate the Tables and the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                Dim numSurId As Integer = GetQueryStringVal( "numSurId")           'Get the Survey Id in local variable
                hdSurId.Value = numSurId                                            'Set the Survey Id in hidden
                Dim numAnsID As Integer = GetQueryStringVal( "numAnsID")           'Get the Answer Id in local variable
                hdAnsId.Value = numAnsID                                            'Set the Answer Id in hidden
                Dim numQuestionId As Integer = GetQueryStringVal( "numQuestionId") 'Get the Question Id in local variable
                hdQId.Value = numQuestionId                                         'Set the Question Id in hidden
                Dim numMatrix As Integer = GetQueryStringVal( "numMatrix") 'Get the Question Id in local variable
                hdMatrix.Value = numMatrix                                         'Set the Question Id in hidden

                 ' = "MarSurvey"
                If hdAction.Value <> "Save" Then
                    Dim objSurvey As New SurveyAdministration                       'Create an object of Survey Administration Class
                    objSurvey.DomainId = Session("DomainID")                        'Set the Domain ID
                    objSurvey.UserId = Session("UserID")                            'Set the User ID
                    objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
                    objSurvey.SurveyId = hdSurId.Value                              'Set the Survey ID
                    objSurvey.AnswerId = numAnsID                                   'Set the Answer id
                    objSurvey.QuestionId = numQuestionId                            'Set the Question id
                    txtLinkedSkipSurveyID.Text = hdSurId.Value                      'Set the Survey Id for Skip Quesiton
                    objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()     'Call to retrieve the existing data temporarily
                    If Not IsPostBack Then
                        'BindElementsToMasterDataBase()                              'Call to databond the Drop down etc of master data
                        DisplayWorkFlowRuleDetails(objSurvey)                       'Call to dispaly the Survey Workflow Rules details
                    End If
                End If
                PostInitializeControlsClientEvents()                                'Call to attache the client events
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to attach master data to the drop down and other elements
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub BindElementsToMasterDataBase()
            Try
                Dim objConfigWizard As New BACRM.BusinessLogic.Admin.FormGenericFormContents        'Create an object of class which encaptulates the functionaltiy
                objConfigWizard.DomainID = Session("DomainID")                                      'set the domain id
                objConfigWizard.ListItemType = "LI"                                                  'set the listitem type
                Dim dtRelationTable As DataTable                                                    'declare a datatable
                dtRelationTable = objConfigWizard.GetMasterListByListId(5) 'call function to fill the datatable with Relationship
                ddlRelationShip.DataSource = dtRelationTable                                        'set the datasource of the Relationship drop down
                ddlRelationShip.DataTextField = "vcItemName"                                        'Set the Text property of the Relationship drop down
                ddlRelationShip.DataValueField = "numItemID"                                        'Set the value attribute of the Relationship drop down
                ddlRelationShip.DataBind()                                                          'Databind the drop down

                Dim dtGroupTable As DataTable                                                       'declare a datatable
                objConfigWizard.ListItemType = "AG"                                                  'set the listitem type
                dtGroupTable = objConfigWizard.GetMasterListByListId(38)                            'call function to fill the datatable with Groups
                ddlGroup.DataSource = dtGroupTable                                                  'set the datasource of the Groups drop down
                ddlGroup.DataTextField = "vcItemName"                                               'Set the Text property of the Groups drop down
                ddlGroup.DataValueField = "numItemID"                                               'Set the value attribute of the Groups drop down
                ddlGroup.DataBind()                                                                 'Databind the drop down

                Dim dtRecordOwner As DataTable                                                      'declare a datatable
                Dim objGenericAdvSearch As New BACRM.BusinessLogic.Admin.FormGenericAdvSearch       'Create an object of class which encaptulates the functionaltiy
                objGenericAdvSearch.DomainID = Session("DomainID")                                  'set the domain id
                dtRecordOwner = objGenericAdvSearch.getAvailableEmployees()                         'Call to get the list of employees
                Dim drRecordOwner As DataRow = dtRecordOwner.NewRow                                 'Get a new row instance of campaign
                drRecordOwner.Item("vcUserName") = "Let Auto Routing Rules decide"                  'Set the displayable text 
                drRecordOwner.Item("numUserID") = 0                                                 'Set the Id as 0
                dtRecordOwner.Rows.InsertAt(drRecordOwner, 0)                                       'Insert the new row at position 0
                ddlRecordOwner.DataSource = dtRecordOwner                                           'Set the datasource of the record owners drop down
                ddlRecordOwner.DataTextField = "vcUserName"                                         'Set the Text displayed in the dropdown as the employee names
                ddlRecordOwner.DataValueField = "numUserId"                                         'Set the values displayed in the dropdown as the employee Ids
                ddlRecordOwner.DataBind()                                                           'DataBind the Record Owner drop down
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the response Type details
        ''' </summary>
        ''' <remarks>
        '''     Returns a datatable of answers for the selected question
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function DisplayWorkFlowRuleDetails(ByRef objSurvey As SurveyAdministration) As DataTable
            Try
                Dim dtSurveyWorkFlowRules As DataTable
                If hdMatrix.Value = 0 Then
                    dtSurveyWorkFlowRules = objSurvey.GetSurveyWorkflowRulesDetailsForSurveyAnswer() 'Call for the Datarelated to the WorkFlow Rule
                Else
                    dtSurveyWorkFlowRules = objSurvey.GetSurveyWorkflowRulesDetailsForSurveyMatrix()
                End If

                'Dim boolIsRule3Repeated As Boolean = objSurvey.CheckOccuranceOfRule3()                  'Rule 3 cannot repeat more than once in a survey and hence this validation
                'If boolIsRule3Repeated Then cbActivationRuleThree.Enabled = False 'Repetation of Rule 3
                'cbActivationRuleThree.Attributes.Add("onclick", "javascript: ConfirmRuleApplicability();") 'Rule #5 cannot be checked unesss Rule #3 is checked

                Dim drWorkFlowRules As DataRow                                                          'Declare a DataRow
                If dtSurveyWorkFlowRules.Rows.Count > 0 Then
                    For Each drWorkFlowRules In dtSurveyWorkFlowRules.Rows                              'Loop through the rules table
                        Select Case drWorkFlowRules.Item("numRuleID")
                            Case 1                                                                      'Rule 1 is actiavted and present
                                cbActivationRuleOne.Checked = drWorkFlowRules.Item("boolActivation")    'Activation of Rule 1 is displayed
                                ddlOrderOfEventsOne.ClearSelection()                                    'clear selection if any for Rule 1
                                If Not ddlOrderOfEventsOne.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")) Is Nothing Then     'Check if the text exists in the dropdown of Rule Order
                                    ddlOrderOfEventsOne.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")).Selected = True        'show the appropriate item as selected for the Event Order
                                Else : ddlOrderOfEventsOne.Items(0).Selected = True                        'show the 1st option as selected
                                End If
                                txtAskQuestions.Text = drWorkFlowRules.Item("vcQuestionList")           'Display the questions which are to be executed
                                txtLinkedSurveyID.Text = drWorkFlowRules.Item("numLinkedSurID")         'Display the linked survey id whose questions are to be executed
                            Case 2                                                                      'Rule 2 is actiavted and present
                                cbActivationRuleTwo.Checked = drWorkFlowRules.Item("boolActivation")    'Activation of Rule 2 is displayed
                                ddlOrderOfEventsTwo.ClearSelection()                                    'clear selection if any for Rule 2
                                If Not ddlOrderOfEventsTwo.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")) Is Nothing Then     'Check if the text exists in the dropdown of Rule Order
                                    ddlOrderOfEventsTwo.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")).Selected = True        'show the appropriate item as selected for the Event Order
                                Else : ddlOrderOfEventsTwo.Items(1).Selected = True                        'show the 2nd option as selected
                                End If
                                txtSkipQuestions.Text = drWorkFlowRules.Item("vcQuestionList")          'Display the questions which are to be Skipped
                                txtLinkedSkipSurveyID.Text = drWorkFlowRules.Item("numLinkedSurID")     'Display the linked survey id whose questions are not to be executed
                                'Case 3
                                '    cbActivationRuleThree.Checked = drWorkFlowRules.Item("boolActivation")  'Activation of Rule 3 is displayed
                                '    ddlOrderOfEventsThree.ClearSelection()                                  'clear selection if any for Rule 3
                                '    If Not ddlOrderOfEventsThree.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")) Is Nothing Then     'Check if the text exists in the dropdown of Groups
                                '        ddlOrderOfEventsThree.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")).Selected = True        'show the appropriate item as selected for the Groups
                                '    Else : ddlOrderOfEventsThree.Items(2).Selected = True                      'show the 3rd option as selected
                                '    End If
                                '    ddlCRMType.ClearSelection()                                             'clear selection if any for CRM Type of Rule 3
                                '    If Not ddlCRMType.Items.FindByValue(drWorkFlowRules.Item("tIntCRMType")) Is Nothing Then     'Check if the text exists in the dropdown of CRM Types
                                '        ddlCRMType.Items.FindByValue(drWorkFlowRules.Item("tIntCRMType")).Selected = True        'show the appropriate item as selected for the CRM Types
                                '    End If
                                '    ddlGroup.ClearSelection()                                               'clear selection if any for Group of Rule 3
                                '    If Not ddlGroup.Items.FindByValue(drWorkFlowRules.Item("numGrpId")) Is Nothing Then     'Check if the text exists in the dropdown of Rule Order
                                '        ddlGroup.Items.FindByValue(drWorkFlowRules.Item("numGrpId")).Selected = True        'show the appropriate item as selected for the Event Order
                                '    End If
                                '    ddlRelationShip.ClearSelection()                                         'clear selection if any for Company Type of Rule 3
                                '    If Not ddlRelationShip.Items.FindByValue(drWorkFlowRules.Item("numCompanyType")) Is Nothing Then     'Check if the text exists in the dropdown of Conpany type
                                '        ddlRelationShip.Items.FindByValue(drWorkFlowRules.Item("numCompanyType")).Selected = True        'show the appropriate item as selected for the Conpany type
                                '    End If
                                '    ddlRecordOwner.ClearSelection()                                         'clear selection if any for Record Owner of Rule 3
                                '    If Not ddlRecordOwner.Items.FindByValue(drWorkFlowRules.Item("numRecOwner")) Is Nothing Then     'Check if the text exists in the dropdown of Record Owner
                                '        ddlRecordOwner.Items.FindByValue(drWorkFlowRules.Item("numRecOwner")).Selected = True        'show the appropriate item as selected for the Record Owner
                                '    End If
                            Case 4
                                cbActivationRuleFour.Checked = drWorkFlowRules.Item("boolActivation")   'Activation of Rule 4 is displayed
                                ddlOrderOfEventsFour.ClearSelection()                                   'clear selection if any for Rule 4
                                If Not ddlOrderOfEventsFour.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")) Is Nothing Then     'Check if the text exists in the dropdown of Rule Order
                                    ddlOrderOfEventsFour.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")).Selected = True        'show the appropriate item as selected for the Event Order
                                Else : ddlOrderOfEventsFour.Items(2).Selected = True                       'show the 4th option as selected
                                End If


                                ' Select the correct radio button
                                Select Case Convert.ToInt32(drWorkFlowRules.Item("tIntRuleFourRadio"))

                                    Case 1
                                        rRule4Radio1.Checked = True
                                    Case 2
                                        rRule4Radio2.Checked = True
                                    Case 3
                                        rRule4Radio3.Checked = True
                                        ddlOrderOfEventsFour.SelectedValue = drWorkFlowRules.Item("tIntCreateNewId")
                                    Case 4
                                        rRule4Radio4.Checked = True
                                End Select


                            Case 5
                                cbActivationRuleFive.Checked = drWorkFlowRules.Item("boolActivation")   'Activation of Rule 5 is displayed
                                ddlOrderOfEventsFive.ClearSelection()                                   'clear selection if any for Rule 5
                                If Not ddlOrderOfEventsFive.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")) Is Nothing Then     'Check if the text exists in the dropdown of Rule Order
                                    ddlOrderOfEventsFive.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")).Selected = True        'show the appropriate item as selected for the Event Order
                                Else : ddlOrderOfEventsFive.Items(3).Selected = True                       'show the 4th option as selected
                                End If
                                If IsDBNull(drWorkFlowRules.Item("boolPopulateSalesOpportunity")) Then
                                    cboRuleFiveCheck1.Checked = 0
                                Else
                                    cboRuleFiveCheck1.Checked = drWorkFlowRules.Item("boolPopulateSalesOpportunity")
                                End If



                            Case 6
                                cbActivationRuleSix.Checked = drWorkFlowRules.Item("boolActivation")   'Activation of Rule 6 is displayed
                                ddlOrderOfEventsSix.ClearSelection()                                   'clear selection if any for Rule 6
                                If Not ddlOrderOfEventsSix.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")) Is Nothing Then     'Check if the text exists in the dropdown of Rule Order
                                    ddlOrderOfEventsSix.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")).Selected = True        'show the appropriate item as selected for the Event Order
                                Else : ddlOrderOfEventsSix.Items(4).Selected = True                       'show the 5th option as selected
                                End If
                                txtOpenURL.Text = drWorkFlowRules.Item("vcPopUpURL")
                            Case 7
                                cbActivationRuleSeven.Checked = drWorkFlowRules.Item("boolActivation")   'Activation of Rule 7 is displayed
                                ddlOrderOfEventsSeven.ClearSelection()                                   'clear selection if any for Rule 7
                                If Not ddlOrderOfEventsSeven.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")) Is Nothing Then     'Check if the text exists in the dropdown of Rule Order
                                    ddlOrderOfEventsSeven.Items.FindByValue(drWorkFlowRules.Item("numEventOrder")).Selected = True        'show the appropriate item as selected for the Event Order
                                Else : ddlOrderOfEventsSeven.Items(5).Selected = True                       'show the 6th option as selected
                                End If
                                txtAddSurveyRating.Text = drWorkFlowRules.Item("numResponseRating")     'Display the Ratign for this Rule
                        End Select
                    Next
                Else
                    ddlOrderOfEventsOne.Items(0).Selected = True                                        'show the 1st option as selected
                    ddlOrderOfEventsTwo.Items(1).Selected = True                                       'show the 2nd option as selected
                    'ddlOrderOfEventsThree.Items(2).Selected = True                                      'show the 3rd option as selected
                    ddlOrderOfEventsFour.Items(2).Selected = True                                       'show the 4th option as selected
                    ddlOrderOfEventsFive.Items(3).Selected = True                                       'show the 5th option as selected
                    ddlOrderOfEventsSix.Items(4).Selected = True                                       'show the 6th option as selected
                    ddlOrderOfEventsSeven.Items(5).Selected = True                                       'show the 7th option as selected


                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the work flow temporarily after the Save button is clicked
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/16/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Private Sub btnSaveWorkFlow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveWorkFlow.Click, btnSaveAndCloseWorkFlow.Click
            Try
                Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
                objSurvey.DomainId = Session("DomainID")                            'Set the Domain ID
                objSurvey.UserId = Session("UserID")                                'Set the User ID
                objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
                objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID
                objSurvey.QuestionId = hdQId.Value                                  'Set the question id
                objSurvey.AnswerId = hdAnsId.Value                                  'Set the Answer id

                objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()         'Call to retrieve the existing data temporarily
                UpdateWorkFlowRules(objSurvey)                                      'Update the rows in the workflow table
                objSurvey.XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the survey info is temp stored
                objSurvey.SaveSurveyInformationTemp()                               'Call to save the existing data temporarily
                Dim btnSrcButton As Button = CType(sender, Button)                  'typecast to button
                If btnSrcButton.ID.ToString = "btnSaveWorkFlow" Then                'check the source of the button
                    DisplayWorkFlowRuleDetails(objSurvey)                           'Display the workflow rules
                Else : litClientMessage.Text = "<script language=javascript>CloseWorkFlowRulesWin();</script>" 'clsoe this window and return to ans wer window
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to sync the dataset of Survey Information with the latest changes in Auto rules
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/16/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Function UpdateWorkFlowRules(ByRef objSurvey As SurveyAdministration)
            Try
                Dim str As String = ""
                If hdMatrix.Value = 0 Then
                    str = "numAnsID"
                Else
                    str = "numMatrixID"
                End If

                Dim dtSurveyAnswerWorkFlowMaster As DataTable
                dtSurveyAnswerWorkFlowMaster = objSurvey.SurveyInfo.Tables("SurveyWorkflowRules")           'Retrieve the datatable

                Dim drSurveyAnswerWorkFlowMaster() As DataRow                                               'Declare a DataRow
                Dim drTempFlowMasterRow As DataRow                                                          'Delare a datarow
                Dim iFilterRuleId As Integer                                                                'Declare a variable for filter rule
                For iFilterRuleId = 1 To 7
                    If iFilterRuleId <> 3 Then
                        If hdMatrix.Value = 0 Then
                            drSurveyAnswerWorkFlowMaster = dtSurveyAnswerWorkFlowMaster.Select("numRuleID = " & iFilterRuleId & " and numAnsID = " & objSurvey.AnswerId & " and numQID = " & objSurvey.QuestionId)  'Set the filter
                        Else
                            drSurveyAnswerWorkFlowMaster = dtSurveyAnswerWorkFlowMaster.Select("numRuleID = " & iFilterRuleId & " and numMatrixID = " & objSurvey.AnswerId & " and numQID = " & objSurvey.QuestionId)  'Set the filter
                        End If

                        If drSurveyAnswerWorkFlowMaster.Length = 0 Then                                         'Check if the row exists
                            drTempFlowMasterRow = dtSurveyAnswerWorkFlowMaster.NewRow                           'Get a new row
                            drTempFlowMasterRow.Item("numRuleID") = iFilterRuleId                               'Set the rule id
                            drTempFlowMasterRow.Item("numQID") = objSurvey.QuestionId                           'Set the Question id
                            If hdMatrix.Value = 0 Then
                                drTempFlowMasterRow.Item("numAnsID") = objSurvey.AnswerId                           'Set the answer id
                                drTempFlowMasterRow.Item("numMatrixID") = 0                                         'Set the Matrix id
                            Else
                                drTempFlowMasterRow.Item("numAnsID") = 0                                            'Set the answer id
                                drTempFlowMasterRow.Item("numMatrixID") = objSurvey.AnswerId                        'Set the Matrix id
                            End If

                            drTempFlowMasterRow.Item("numSurID") = hdSurId.Value                                'Set the Survey id
                            dtSurveyAnswerWorkFlowMaster.Rows.Add(drTempFlowMasterRow)                          'Add a new row
                        End If
                    End If
                Next
                Dim drSurveyAnswerWorkFlowMasterRow As DataRow                                              'Declare a DataRow
                Dim numSurveyAnwerWorkFlowMasterTableRowIndex As Integer = 1                                'Declare a index for datatable rows
                For Each drSurveyAnswerWorkFlowMasterRow In dtSurveyAnswerWorkFlowMaster.Rows               'Loop through the rules table


                    If drSurveyAnswerWorkFlowMasterRow.Item(Str) = objSurvey.AnswerId And drSurveyAnswerWorkFlowMasterRow.Item("numQID") = objSurvey.QuestionId Then
                        Select Case drSurveyAnswerWorkFlowMasterRow.Item("numRuleID")
                            Case 1                                                                          'Rule 1
                                drSurveyAnswerWorkFlowMasterRow.Item("numRuleID") = 1
                                drSurveyAnswerWorkFlowMasterRow.Item("boolActivation") = IIf(cbActivationRuleOne.Checked, 1, 0) 'Activation of Rule 1 is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("numEventOrder") = ddlOrderOfEventsOne.SelectedValue 'Event Order is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("vcQuestionList") = txtAskQuestions.Text 'The Ask Question order is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("numLinkedSurID") = txtLinkedSurveyID.Text   'The Ask Question from Survey is stored
                            Case 2                                                                          'Rule 2
                                drSurveyAnswerWorkFlowMasterRow.Item("numRuleID") = 2
                                drSurveyAnswerWorkFlowMasterRow.Item("boolActivation") = IIf(cbActivationRuleTwo.Checked, 1, 0) 'Activation of Rule 2 is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("numEventOrder") = ddlOrderOfEventsTwo.SelectedValue 'Event Order is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("vcQuestionList") = txtSkipQuestions.Text 'The Skip Question order is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("numLinkedSurID") = txtLinkedSkipSurveyID.Text 'The Skip Question from Survey is stored
                                'Case 3                                                                          'Rule 3
                                'drSurveyAnswerWorkFlowMasterRow.Item("numRuleID") = 3
                                'drSurveyAnswerWorkFlowMasterRow.Item("boolActivation") = IIf(cbActivationRuleThree.Checked, 1, 0) 'Activation of Rule 3 is stored
                                'drSurveyAnswerWorkFlowMasterRow.Item("numEventOrder") = ddlOrderOfEventsThree.SelectedValue 'Event Order is stored
                                'drSurveyAnswerWorkFlowMasterRow.Item("tIntCRMType") = ddlCRMType.SelectedValue 'The CRM Type is stored
                                'drSurveyAnswerWorkFlowMasterRow.Item("numGrpId") = ddlGroup.SelectedValue   'The Group is stored
                                'drSurveyAnswerWorkFlowMasterRow.Item("numCompanyType") = ddlRelationShip.SelectedValue 'The Company Type/ Relationship is stored
                                'drSurveyAnswerWorkFlowMasterRow.Item("numRecOwner") = ddlRecordOwner.SelectedValue 'The Record Owner is stored
                            Case 4                                                                          'Rule 4
                                drSurveyAnswerWorkFlowMasterRow.Item("numRuleID") = 4
                                drSurveyAnswerWorkFlowMasterRow.Item("boolActivation") = IIf(cbActivationRuleFour.Checked, 1, 0) 'Activation of Rule 4 is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("numEventOrder") = ddlOrderOfEventsFour.SelectedValue 'Event Order is stored
                                Dim intRuleFourRadio As Integer = 0

                                If rRule4Radio1.Checked = True Then
                                    intRuleFourRadio = 1
                                End If
                                If rRule4Radio2.Checked = True Then
                                    intRuleFourRadio = 2
                                End If
                                If rRule4Radio3.Checked = True Then
                                    intRuleFourRadio = 3
                                    drSurveyAnswerWorkFlowMasterRow.Item("tIntCreateNewId") = ddlRuleFourRadio3.SelectedValue
                                End If
                                If rRule4Radio4.Checked = True Then
                                    intRuleFourRadio = 4
                                End If

                                drSurveyAnswerWorkFlowMasterRow.Item("tIntRuleFourRadio") = intRuleFourRadio

                            Case 5                                                                          ' Rule 5
                                drSurveyAnswerWorkFlowMasterRow.Item("numRuleID") = 5
                                drSurveyAnswerWorkFlowMasterRow.Item("boolActivation") = IIf(cbActivationRuleFive.Checked, 1, 0) 'Activation of Rule 5 is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("numEventOrder") = ddlOrderOfEventsFive.SelectedValue 'Event Order is stored

                                drSurveyAnswerWorkFlowMasterRow.Item("boolPopulateSalesOpportunity") = IIf(cboRuleFiveCheck1.Checked, 1, 0)

                            Case 6                                                                          'Rule 6
                                drSurveyAnswerWorkFlowMasterRow.Item("numRuleID") = 6
                                drSurveyAnswerWorkFlowMasterRow.Item("boolActivation") = IIf(cbActivationRuleSix.Checked, 1, 0) 'Activation of Rule 4 is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("numEventOrder") = ddlOrderOfEventsSix.SelectedValue 'Event Order is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("vcPopUpURL") = txtOpenURL.Text        'POPup URL is stored
                            Case 7                                                                          'Rule 7
                                drSurveyAnswerWorkFlowMasterRow.Item("numRuleID") = 7
                                drSurveyAnswerWorkFlowMasterRow.Item("boolActivation") = IIf(cbActivationRuleSeven.Checked, 1, 0) 'Activation of Rule 5 is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("numEventOrder") = ddlOrderOfEventsSeven.SelectedValue 'Event Order is stored
                                drSurveyAnswerWorkFlowMasterRow.Item("numResponseRating") = txtAddSurveyRating.Text 'Response wirghtage/ rating is stored
                        End Select
                    End If
                Next
                Dim dtSurveyAnswerMaster As DataTable

                If hdMatrix.Value = 0 Then
                    dtSurveyAnswerMaster = objSurvey.SurveyInfo.Tables("SurveyAnsMaster")                       'Get the Answer table
                Else
                    dtSurveyAnswerMaster = objSurvey.SurveyInfo.Tables("SurveyMatrixMaster")                       'Get the Answer table
                End If

                Dim drSurveyAnswerMasterRow As DataRow                                                      'Declare a DataRow
                For Each drSurveyAnswerMasterRow In dtSurveyAnswerMaster.Rows                               'Loop through the rows
                    If drSurveyAnswerMasterRow.Item(str) = objSurvey.AnswerId And drSurveyAnswerMasterRow.Item("numQID") = objSurvey.QuestionId Then
                        If cbActivationRuleOne.Checked Or cbActivationRuleTwo.Checked Or cbActivationRuleFour.Checked Or cbActivationRuleFive.Checked Or cbActivationRuleSix.Checked Or cbActivationRuleSeven.Checked Then
                            drSurveyAnswerMasterRow.Item("boolSurveyRuleAttached") = 1                      'Sync the Rule id (temp)
                        Else : drSurveyAnswerMasterRow.Item("boolSurveyRuleAttached") = 0                      'Sync the Rule id (temp)
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is safe casts the string for XML
        ''' </summary>
        ''' <param name = "sValue">The string which is to be made XML safe</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function MakeSafeForXML(ByVal sValue As String) As String
            Try
                Return Replace(Replace(Replace(Replace(Replace(sValue, "'", "&#39;"), """", "&#39;&#39;"), "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to attach client side events to the controls
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub PostInitializeControlsClientEvents()
            Try
                btnClose.Attributes.Add("onclick", "javascript: return CloseWorkFlowRulesWin();")                 'calls for closing the Rules window
                btnSaveAndCloseWorkFlow.Attributes.Add("onclick", "javascript: return ValidateWorkFlowRulesEditor();")                 'calls for closing the Rules window
                btnSaveWorkFlow.Attributes.Add("onclick", "javascript: return ValidateWorkFlowRulesEditor();")                 'calls for closing the Rules window
                cbActivationRuleOne.Attributes.Add("onclick", "javascript: HighLightRule(this,'trActivationRuleOne');") 'Highlight Rule 1
                cbActivationRuleTwo.Attributes.Add("onclick", "javascript: HighLightRule(this,'trActivationRuleTwo');") 'Highlight Rule 2
                cbActivationRuleFour.Attributes.Add("onclick", "javascript: HighLightRule(this,'trActivationRuleFour');") 'Highlight Rule 4
                cbActivationRuleFive.Attributes.Add("onclick", "javascript: HighLightRule(this,'trActivationRuleFive');") 'Highlight Rule 5
                cbActivationRuleSix.Attributes.Add("onclick", "javascript: HighLightRule(this,'trActivationRuleSix');") 'Highlight Rule 5
                cbActivationRuleSeven.Attributes.Add("onclick", "javascript: HighLightRule(this,'trActivationRuleSeven');") 'Highlight Rule 5
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
