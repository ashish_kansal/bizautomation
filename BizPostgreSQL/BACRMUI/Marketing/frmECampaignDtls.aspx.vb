Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Partial Class frmECampaignDtls
    Inherits BACRMPage

    Dim objCampaign As Campaign
    Dim objCommon As CCommon
    Dim lngECampID As Long
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            lngECampID = CCommon.ToLong(GetQueryStringVal("ECampID"))
            If Not IsPostBack Then
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                txtCampaignName.Focus()
                objCommon = New CCommon
                GetUserRightsForPage(6, 8)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                Else
                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                    End If
                End If
                objCampaign = New Campaign
                objCampaign.DomainID = Session("DomainID")
                objCampaign.ModuleID = "1"
                Dim dtTable As DataTable
                dtTable = objCampaign.GetEmailTemplates

                ddlEmailTemplate1.DataSource = dtTable
                ddlEmailTemplate1.DataTextField = "VcDocName"
                ddlEmailTemplate1.DataValueField = "numGenericDocID"
                ddlEmailTemplate1.DataBind()
                ddlEmailTemplate1.Items.Insert(0, "--Select One--")
                ddlEmailTemplate1.Items.FindByText("--Select One--").Value = 0

                ddlEmailTemplate2.DataSource = dtTable
                ddlEmailTemplate2.DataTextField = "VcDocName"
                ddlEmailTemplate2.DataValueField = "numGenericDocID"
                ddlEmailTemplate2.DataBind()
                ddlEmailTemplate2.Items.Insert(0, "--Select One--")
                ddlEmailTemplate2.Items.FindByText("--Select One--").Value = 0

                ddlEmailTemplate3.DataSource = dtTable
                ddlEmailTemplate3.DataTextField = "VcDocName"
                ddlEmailTemplate3.DataValueField = "numGenericDocID"
                ddlEmailTemplate3.DataBind()
                ddlEmailTemplate3.Items.Insert(0, "--Select One--")
                ddlEmailTemplate3.Items.FindByText("--Select One--").Value = 0

                ddlEmailTemplate4.DataSource = dtTable
                ddlEmailTemplate4.DataTextField = "VcDocName"
                ddlEmailTemplate4.DataValueField = "numGenericDocID"
                ddlEmailTemplate4.DataBind()
                ddlEmailTemplate4.Items.Insert(0, "--Select One--")
                ddlEmailTemplate4.Items.FindByText("--Select One--").Value = 0

                ddlEmailTemplate5.DataSource = dtTable
                ddlEmailTemplate5.DataTextField = "VcDocName"
                ddlEmailTemplate5.DataValueField = "numGenericDocID"
                ddlEmailTemplate5.DataBind()
                ddlEmailTemplate5.Items.Insert(0, "--Select One--")
                ddlEmailTemplate5.Items.FindByText("--Select One--").Value = 0

                ddlEmailTemplate6.DataSource = dtTable
                ddlEmailTemplate6.DataTextField = "VcDocName"
                ddlEmailTemplate6.DataValueField = "numGenericDocID"
                ddlEmailTemplate6.DataBind()
                ddlEmailTemplate6.Items.Insert(0, "--Select One--")
                ddlEmailTemplate6.Items.FindByText("--Select One--").Value = 0

                ddlEmailTemplate7.DataSource = dtTable
                ddlEmailTemplate7.DataTextField = "VcDocName"
                ddlEmailTemplate7.DataValueField = "numGenericDocID"
                ddlEmailTemplate7.DataBind()
                ddlEmailTemplate7.Items.Insert(0, "--Select One--")
                ddlEmailTemplate7.Items.FindByText("--Select One--").Value = 0

                ddlEmailTemplate8.DataSource = dtTable
                ddlEmailTemplate8.DataTextField = "VcDocName"
                ddlEmailTemplate8.DataValueField = "numGenericDocID"
                ddlEmailTemplate8.DataBind()
                ddlEmailTemplate8.Items.Insert(0, "--Select One--")
                ddlEmailTemplate8.Items.FindByText("--Select One--").Value = 0

                ddlEmailTemplate9.DataSource = dtTable
                ddlEmailTemplate9.DataTextField = "VcDocName"
                ddlEmailTemplate9.DataValueField = "numGenericDocID"
                ddlEmailTemplate9.DataBind()
                ddlEmailTemplate9.Items.Insert(0, "--Select One--")
                ddlEmailTemplate9.Items.FindByText("--Select One--").Value = 0

                ddlEmailTemplate10.DataSource = dtTable
                ddlEmailTemplate10.DataTextField = "VcDocName"
                ddlEmailTemplate10.DataValueField = "numGenericDocID"
                ddlEmailTemplate10.DataBind()
                ddlEmailTemplate10.Items.Insert(0, "--Select One--")
                ddlEmailTemplate10.Items.FindByText("--Select One--").Value = 0
                LoadActionItemTemplateData()
                BindFollowUp()
                sb_FillEmpList()
                LoadDetails()
                GetAmazonSESConfiguration()
            End If
            btnSave.Attributes.Add("onclick", "return Save()")
            btnSaveClose.Attributes.Add("onclick", "return Save()")
            ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('3');}else{ parent.SelectTabByValue('3'); } ", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            If objCampaign Is Nothing Then objCampaign = New Campaign

            If lngECampID > 0 Then
                Dim ds As DataSet
                Dim dtTable As DataTable
                objCampaign.ECampaignID = lngECampID
                ds = objCampaign.ECampaignDtls
                dtTable = ds.Tables(0)
                txtCampaignName.Text = dtTable.Rows(0).Item("vcECampName")
                txtDescription.Text = dtTable.Rows(0).Item("txtDesc")

                If Not IsDBNull(dtTable.Rows(0).Item("tintTimeZoneIndex")) Then
                    ddlTimeZone.SelectedValue = dtTable.Rows(0).Item("fltTimeZone") & "~" & dtTable.Rows(0).Item("tintTimeZoneIndex")
                End If
                ddltime.ClearSelection()

                If Not dtTable.Rows(0).Item("dtStartTime") Is Nothing Then
                    Dim dateTime As DateTime = CCommon.ToSqlDate(dtTable.Rows(0).Item("dtStartTime"))
                    dateTime = GetLocalTimeFromTimeZone(ddlTimeZone.SelectedValue, dateTime)

                    If Not ddltime.Items.FindByText(Format(dateTime, "h:mm")) Is Nothing Then
                        ddltime.Items.FindByText(Format(dateTime, "h:mm")).Selected = True
                    End If

                    If Format(dateTime, "tt") = "AM" Then
                        chkAM.Checked = True
                    Else : chkPM.Checked = True
                    End If
                End If

                If dtTable.Rows(0).Item("tintFromField") = 1 Then
                    rbRecOwner.Checked = True
                ElseIf dtTable.Rows(0).Item("tintFromField") = 2 Then
                    rbAssignee.Checked = True
                ElseIf dtTable.Rows(0).Item("tintFromField") = 3 Then
                    rbBizUser.Checked = True
                End If
                If rbBizUser.Checked Then
                    If Not ddlEmployee.Items.FindByValue(dtTable.Rows(0).Item("numFromContactID")) Is Nothing Then
                        ddlEmployee.Items.FindByValue(dtTable.Rows(0).Item("numFromContactID")).Selected = True
                    End If
                End If


                Dim i As Integer
                dtTable = ds.Tables(1)
                If dtTable.Rows.Count = 0 Then Exit Sub
                For i = 0 To 9
                    If Not CType(tbl.FindControl("ddlEmailTemplate" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("numEmailTemplate")) Is Nothing Then
                        CType(tbl.FindControl("ddlEmailTemplate" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("numEmailTemplate")).Selected = True
                    End If
                    If Not IsDBNull(dtTable.Rows(i).Item("numActionItemTemplate")) Then
                        If Not CType(tbl.FindControl("listActionItemTemplate" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("numActionItemTemplate")) Is Nothing Then
                            CType(tbl.FindControl("listActionItemTemplate" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("numActionItemTemplate")).Selected = True
                        End If
                    End If
                    'If i <> 0 Then
                    '    If Not IsDBNull(dtTable.Rows(i).Item("tintDays")) Then
                    '        If Not CType(tbl.FindControl("ddlDays" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("tintDays")) Is Nothing Then
                    '            CType(tbl.FindControl("ddlDays" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("tintDays") - dtTable.Rows(i - 1).Item("tintDays")).Selected = True
                    '        End If
                    '    End If
                    'Else
                    If Not IsDBNull(dtTable.Rows(i).Item("tintDays")) Then
                        If Not CType(tbl.FindControl("ddlDays" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("tintDays")) Is Nothing Then
                            CType(tbl.FindControl("ddlDays" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("tintDays")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtTable.Rows(i).Item("tintWaitPeriod")) Then
                        If Not CType(tbl.FindControl("ddlDayWeek" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("tintWaitPeriod")) Is Nothing Then
                            CType(tbl.FindControl("ddlDayWeek" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("tintWaitPeriod")).Selected = True
                        End If
                    End If

                    'End If
                    If Not CType(tbl.FindControl("ddlFollowUp" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("numFollowUpID")) Is Nothing Then
                        CType(tbl.FindControl("ddlFollowUp" & i + 1), DropDownList).Items.FindByValue(dtTable.Rows(i).Item("numFollowUpID")).Selected = True
                    End If
                Next
            Else
                'do nothing keep everything blank
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            Response.Redirect("../Marketing/frmECampaignList.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../Marketing/frmECampaignList.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub Save()
        Try
            If objCampaign Is Nothing Then objCampaign = New Campaign
            Dim dtTable1 As New DataTable
            Dim strStartTime As String
            Dim createNew As Boolean

            strStartTime = Date.Now.Today.ToShortDateString & " " & ddltime.SelectedItem.Text.Trim & ":00" & IIf(chkAM.Checked = True, " AM", " PM")
            If lngECampID > 0 Then
                objCampaign.ECampaignID = lngECampID
                dtTable1 = objCampaign.ECampaignDtls.Tables(1)
                If dtTable1.Rows.Count = 0 Then Exit Sub
                Dim i As Integer
                For i = 1 To 10
                    dtTable1.Rows(i - 1).Item("numEmailTemplate") = CType(tbl.FindControl("ddlEmailTemplate" & i), DropDownList).SelectedItem.Value
                    dtTable1.Rows(i - 1).Item("numActionItemTemplate") = CType(tbl.FindControl("listActionItemTemplate" & i), DropDownList).SelectedItem.Value
                    dtTable1.Rows(i - 1).Item("tintDays") = CType(tbl.FindControl("ddlDays" & i), DropDownList).SelectedItem.Value
                    dtTable1.Rows(i - 1).Item("tintWaitPeriod") = CType(tbl.FindControl("ddlDayWeek" & i), DropDownList).SelectedItem.Value
                    dtTable1.Rows(i - 1).Item("numFollowUpID") = CType(tbl.FindControl("ddlFollowUp" & i), DropDownList).SelectedItem.Value
                Next
                dtTable1.TableName = "Table1"
            Else
                createNew = True
                dtTable1.Columns.Add("numECampDTLId")
                dtTable1.Columns.Add("numEmailTemplate")
                dtTable1.Columns.Add("numActionItemTemplate")
                dtTable1.Columns.Add("tintDays")
                dtTable1.Columns.Add("tintWaitPeriod")
                dtTable1.Columns.Add("numFollowUpID")
                Dim dr As DataRow
                Dim i As Integer
                For i = 1 To 10
                    dr = dtTable1.NewRow
                    dr("numEmailTemplate") = CType(tbl.FindControl("ddlEmailTemplate" & i), DropDownList).SelectedItem.Value
                    dr("numActionItemTemplate") = CType(tbl.FindControl("listActionItemTemplate" & i), DropDownList).SelectedItem.Value
                    dr("tintDays") = CType(tbl.FindControl("ddlDays" & i), DropDownList).SelectedItem.Value
                    dr("tintWaitPeriod") = CType(tbl.FindControl("ddlDayWeek" & i), DropDownList).SelectedItem.Value
                    dr("numFollowUpID") = CType(tbl.FindControl("ddlFollowUp" & i), DropDownList).SelectedItem.Value
                    dtTable1.Rows.Add(dr)
                Next
                dtTable1.TableName = "Table"
            End If

            Dim dsNew As New DataSet

            dsNew.Tables.Add(dtTable1.Copy)
            objCampaign.strECampDtls = dsNew.GetXml
            dsNew.Tables.Remove(dsNew.Tables(0))
            objCampaign.ECampaignID = lngECampID
            objCampaign.ECampaignName = txtCampaignName.Text.Trim
            objCampaign.CampDesc = txtDescription.Text
            objCampaign.StartTime = GetUTCDateBasedOnTimeZone(ddlTimeZone.SelectedValue, strStartTime)
            objCampaign.DomainID = Session("DomainID")
            objCampaign.UserCntID = Session("UserContactId")
            objCampaign.TimeZone = ddlTimeZone.SelectedValue.Split("~")(0)
            objCampaign.TimeZoneIndex = ddlTimeZone.SelectedValue.Split("~")(1)
            objCampaign.FromField = IIf(rbRecOwner.Checked = True, 1, IIf(rbAssignee.Checked = True, 2, 3))
            objCampaign.ContactId = ddlEmployee.SelectedValue
            lngECampID = objCampaign.ManageEcampaign()
            If createNew = True Then
                Response.Redirect("../Marketing/frmECampaignList.aspx", True)
            End If
            'Get Email Contacts for selected email group
            'If ddlEmailGroup.SelectedValue > 0 Then
            '    Dim objEGroup As New BAddEmailGroup
            '    Dim dtTableDB As DataTable
            '    Dim ConCampaignID As Long
            '    objEGroup.strGroupID = ddlEmailGroup.SelectedValue
            '    objEGroup.DomainID = Session("DomainID")
            '    objEGroup.UserCntID = Session("UserContactID")
            '    objEGroup.ContactId = "<NewDataSet> <Table>    <numContactId>0</numContactId>    <vcFirstName>\</vcFirstName>    <vcLastName>\</vcLastName>    <vcEmail /><vcCompanyName>\</vcCompanyName>  </Table></NewDataSet>"
            '    dtTableDB = objEGroup.getContactData()
            '    For Each dr As DataRow In dtTableDB.Rows
            '        objCampaign.ConEmailCampID = 0
            '        objCampaign.ContactId = dr("numContactID")
            '        objCampaign.ECampaignID = GetQueryStringVal( "ECampID")
            '        objCampaign.Engaged = 1
            '        objCampaign.UserCntID = Session("UserContactID")
            '        ConCampaignID = objCampaign.ManageConECamp()
            '    Next
            'End If

            'If listActionItemTemplate1.SelectedValue > 0 Then
            '    Dim objActionItem As New ActionItem
            '    objActionItem.InsertActionTemplateData()
            'End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindFollowUp()
        Try
            objCommon.DomainID = Session("DomainID")
            objCommon.ListID = 30
            Dim dt As DataTable = objCommon.GetMasterListItemsWithRights()
            BindDropDown(ddlFollowUp1, dt)
            BindDropDown(ddlFollowUp2, dt)
            BindDropDown(ddlFollowUp3, dt)
            BindDropDown(ddlFollowUp4, dt)
            BindDropDown(ddlFollowUp5, dt)
            BindDropDown(ddlFollowUp6, dt)
            BindDropDown(ddlFollowUp7, dt)
            BindDropDown(ddlFollowUp8, dt)
            BindDropDown(ddlFollowUp9, dt)
            BindDropDown(ddlFollowUp10, dt)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindDropDown(ByRef ddl As DropDownList, ByVal data As DataTable)
        Try
            ddl.DataTextField = "vcData"
            ddl.DataValueField = "numListItemID"
            ddl.DataSource = data
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select One--", 0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadActionItemTemplateData()
        Try
            Dim objActionItem As ActionItem
            objActionItem = New ActionItem
            Dim dtActionItems As DataTable
            objActionItem.DomainID = Session("DomainId")
            objActionItem.UserCntID = Session("UserContactId")
            dtActionItems = objActionItem.LoadActionItemTemplateData()

            If Not dtActionItems Is Nothing Then
                listActionItemTemplate1.DataSource = dtActionItems
                listActionItemTemplate1.DataTextField = "TemplateName"
                listActionItemTemplate1.DataValueField = "RowID"
                listActionItemTemplate1.DataBind()
                listActionItemTemplate1.Items.Insert(0, New ListItem("--Select One--", 0))

                listActionItemTemplate2.DataSource = dtActionItems
                listActionItemTemplate2.DataTextField = "TemplateName"
                listActionItemTemplate2.DataValueField = "RowID"
                listActionItemTemplate2.DataBind()
                listActionItemTemplate2.Items.Insert(0, New ListItem("--Select One--", 0))

                listActionItemTemplate3.DataSource = dtActionItems
                listActionItemTemplate3.DataTextField = "TemplateName"
                listActionItemTemplate3.DataValueField = "RowID"
                listActionItemTemplate3.DataBind()
                listActionItemTemplate3.Items.Insert(0, New ListItem("--Select One--", 0))

                listActionItemTemplate4.DataSource = dtActionItems
                listActionItemTemplate4.DataTextField = "TemplateName"
                listActionItemTemplate4.DataValueField = "RowID"
                listActionItemTemplate4.DataBind()
                listActionItemTemplate4.Items.Insert(0, New ListItem("--Select One--", 0))

                listActionItemTemplate5.DataSource = dtActionItems
                listActionItemTemplate5.DataTextField = "TemplateName"
                listActionItemTemplate5.DataValueField = "RowID"
                listActionItemTemplate5.DataBind()
                listActionItemTemplate5.Items.Insert(0, New ListItem("--Select One--", 0))

                listActionItemTemplate6.DataSource = dtActionItems
                listActionItemTemplate6.DataTextField = "TemplateName"
                listActionItemTemplate6.DataValueField = "RowID"
                listActionItemTemplate6.DataBind()
                listActionItemTemplate6.Items.Insert(0, New ListItem("--Select One--", 0))

                listActionItemTemplate7.DataSource = dtActionItems
                listActionItemTemplate7.DataTextField = "TemplateName"
                listActionItemTemplate7.DataValueField = "RowID"
                listActionItemTemplate7.DataBind()
                listActionItemTemplate7.Items.Insert(0, New ListItem("--Select One--", 0))

                listActionItemTemplate8.DataSource = dtActionItems
                listActionItemTemplate8.DataTextField = "TemplateName"
                listActionItemTemplate8.DataValueField = "RowID"
                listActionItemTemplate8.DataBind()
                listActionItemTemplate8.Items.Insert(0, New ListItem("--Select One--", 0))

                listActionItemTemplate9.DataSource = dtActionItems
                listActionItemTemplate9.DataTextField = "TemplateName"
                listActionItemTemplate9.DataValueField = "RowID"
                listActionItemTemplate9.DataBind()
                listActionItemTemplate9.Items.Insert(0, New ListItem("--Select One--", 0))

                listActionItemTemplate10.DataSource = dtActionItems
                listActionItemTemplate10.DataTextField = "TemplateName"
                listActionItemTemplate10.DataValueField = "RowID"
                listActionItemTemplate10.DataBind()
                listActionItemTemplate10.Items.Insert(0, New ListItem("--Select One--", 0))

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub sb_FillEmpList()
        Try
            If objCommon Is Nothing Then objCommon = New CCommon
            objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, 0)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GetAmazonSESConfiguration()
        Try
            Dim objCampaign As New Campaign
            objCampaign.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dt As DataTable = objCampaign.GetBroadcastConfiguration()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                Try
                    If Not objCampaign.ValidateBroadcastConfiguration(CCommon.ToString(dt.Rows(0)("vcAWSDomain")), CCommon.ToString(dt.Rows(0)("vcAWSAccessKey")), CCommon.ToString(dt.Rows(0)("vcAWSSecretKey"))) Then
                        litMessage.Text = "Configured Amazon Simple Email Service (Amazon SES) account detail from Marketing -> e-mail Broadcast -> Configuration is invalid."
                    End If
                Catch ex As Exception
                    If ex.Message = "EMAIL_BROADCAST_NOT_CONFIGURED" Then
                        litMessage.Text = "Please configure Amazon Simple Email Service (Amazon SES) account detail from Marketing -> e-mail Broadcast -> Configuration."
                    ElseIf ex.Message = "EMAIL_BROADCAST_CONFIGURATION_INVALID" Then
                        litMessage.Text = "Configured Amazon Simple Email Service (Amazon SES) account detail from Marketing -> e-mail Broadcast -> Configuration is invalid."
                    Else
                        Throw
                    End If

                    btnSave.Visible = False
                    btnSaveClose.Visible = False
                End Try
            Else
                btnSave.Visible = False
                btnSaveClose.Visible = False

                litMessage.Text = "Please configure Amazon Simple Email Service (Amazon SES) account detail from Marketing -> e-mail Broadcast -> Configuration."
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function GetUTCDateBasedOnTimeZone(ByVal timeZoneID As String, ByVal campaignDate As DateTime)
        Try
            Dim timeZone As String = ""

            Select Case timeZoneID
                Case "-12~1"
                    timeZone = "Dateline Standard Time"
                Case "-11~2"
                    timeZone = "Samoa Standard Time"
                Case "-10~3"
                    timeZone = "Hawaiian Standard Time"
                Case "-9~4"
                    timeZone = "Alaskan Standard Time"
                Case "-8~6"
                    timeZone = "Pacific Standard Time"
                Case "-8~7"
                    timeZone = "Pacific Standard Time (Mexico)"
                Case "-7~9"
                    timeZone = "US Mountain Standard Time"
                Case "7~10"
                    timeZone = "Mountain Standard Time (Mexico)"
                Case "7~11"
                    timeZone = "Mountain Standard Time"
                Case "6~12"
                    timeZone = "Central America Standard Time"
                Case "6~13"
                    timeZone = "Central Standard Time"
                Case "6~14"
                    timeZone = "Central Standard Time (Mexico)"
                Case "6~15"
                    timeZone = "Canada Central Standard Time"
                Case "5~16"
                    timeZone = "Eastern Standard Time"
                Case "5~17"
                    timeZone = "SA Pacific Standard Time"
                Case "5~19"
                    timeZone = "US Eastern Standard Time"
                Case "4~21"
                    timeZone = "Atlantic Standard Time"
                Case "4.5~22"
                    timeZone = "Venezuela Standard Time"
                Case "-3.5~23"
                    timeZone = "Newfoundland Standard Time"
                Case "-3~24"
                    timeZone = "E. South America Standard Time"
                Case "-3~25"
                    timeZone = "Argentina Standard Time"
                Case "-3~26"
                    timeZone = "SA Eastern Standard Time"
                Case "-3~27"
                    timeZone = "Greenland Standard Time"
                Case "-2~28"
                    timeZone = "Mid-Atlantic Standard Time"
                Case "-1~29"
                    timeZone = "Azores Standard Time"
                Case "-1~30"
                    timeZone = "Cape Verde Standard Time"
                Case "0~31"
                    timeZone = "Morocco Standard Time"
                Case "0~32"
                    timeZone = "GMT Standard Time"
                Case "1~33"
                    timeZone = "Central Europe Standard Time"
                Case "1~34"
                    timeZone = "Romance Standard Time"
                Case "1~35"
                    timeZone = "Central European Standard Time"
                Case "1~36"
                    timeZone = "W. Central Africa Standard Time"
                Case "2~37"
                    timeZone = "GTB Standard Time"
                Case "2~38"
                    timeZone = "Middle East Standard Time"
                Case "2~39"
                    timeZone = "Egypt Standard Time"
                Case "2~40"
                    timeZone = "South Africa Standard Time"
                Case "2~41"
                    timeZone = "FLE Standard Time"
                Case "2~42"
                    timeZone = "Israel Standard Time"
                Case "3~43"
                    timeZone = "Arabic Standard Time"
                Case "3~44"
                    timeZone = "Arab Standard Time"
                Case "3~45"
                    timeZone = "Russian Standard Time"
                Case "3~46"
                    timeZone = "E. Africa Standard Time"
                Case "3.5~47"
                    timeZone = "Iran Standard Time"
                Case "4~48"
                    timeZone = "Arabian Standard Time"
                Case "4~49"
                    timeZone = "Azerbaijan Standard Time"
                Case "4.5~50"
                    timeZone = "Afghanistan Standard Time"
                Case "5~51"
                    timeZone = "Ekaterinburg Standard Time"
                Case "5~52"
                    timeZone = "Pakistan Standard Time"
                Case "5.5~53"
                    timeZone = "India Standard Time"
                Case "5.75~54"
                    timeZone = "Nepal Standard Time"
                Case "6~55"
                    timeZone = "N. Central Asia Standard Time"
                Case "6~56"
                    timeZone = "Bangladesh Standard Time"
                Case "5.5~57"
                    timeZone = "Sri Lanka Standard Time"
                Case "6.5~58"
                    timeZone = "Myanmar Standard Time"
                Case "7~59"
                    timeZone = "SE Asia Standard Time"
                Case "7~60"
                    timeZone = "North Asia Standard Time"
                Case "8~61"
                    timeZone = "China Standard Time"
                Case "8~62"
                    timeZone = "North Asia East Standard Time"
                Case "8~63"
                    timeZone = "Singapore Standard Time"
                Case "8~64"
                    timeZone = "W. Australia Standard Time"
                Case "8~65"
                    timeZone = "Taipei Standard Time"
                Case "9~66"
                    timeZone = "Tokyo Standard Time"
                Case "9~67"
                    timeZone = "Korea Standard Time"
                Case "9~68"
                    timeZone = "Yakutsk Standard Time"
                Case "9.5~59"
                    timeZone = "Cen. Australia Standard Time"
                Case "9.5~70"
                    timeZone = "AUS Central Standard Time"
                Case "10~71"
                    timeZone = "E. Australia Standard Time"
                Case "10~72"
                    timeZone = "AUS Eastern Standard Time"
                Case "10~73"
                    timeZone = "West Pacific Standard Time"
                Case "10~74"
                    timeZone = "Tasmania Standard Time"
                Case "10~75"
                    timeZone = "Vladivostok Standard Time"
                Case "11~76"
                    timeZone = "Magadan Standard Time"
                Case "12~77"
                    timeZone = "New Zealand Standard Time"
                Case "12~78"
                    timeZone = "Fiji Standard Time"
                Case "13~79"
                    timeZone = "Tonga Standard Time"
            End Select

            Dim info As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone)
            Return TimeZoneInfo.ConvertTimeToUtc(campaignDate, info)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function GetLocalTimeFromTimeZone(ByVal timeZoneID As String, ByVal campaignDate As DateTime)
        Try
            Dim timeZone As String = ""

            Select Case timeZoneID
                Case "-12~1"
                    timeZone = "Dateline Standard Time"
                Case "-11~2"
                    timeZone = "Samoa Standard Time"
                Case "-10~3"
                    timeZone = "Hawaiian Standard Time"
                Case "-9~4"
                    timeZone = "Alaskan Standard Time"
                Case "-8~6"
                    timeZone = "Pacific Standard Time"
                Case "-8~7"
                    timeZone = "Pacific Standard Time (Mexico)"
                Case "-7~9"
                    timeZone = "US Mountain Standard Time"
                Case "7~10"
                    timeZone = "Mountain Standard Time (Mexico)"
                Case "7~11"
                    timeZone = "Mountain Standard Time"
                Case "6~12"
                    timeZone = "Central America Standard Time"
                Case "6~13"
                    timeZone = "Central Standard Time"
                Case "6~14"
                    timeZone = "Central Standard Time (Mexico)"
                Case "6~15"
                    timeZone = "Canada Central Standard Time"
                Case "5~16"
                    timeZone = "Eastern Standard Time"
                Case "5~17"
                    timeZone = "SA Pacific Standard Time"
                Case "5~19"
                    timeZone = "US Eastern Standard Time"
                Case "4~21"
                    timeZone = "Atlantic Standard Time"
                Case "4.5~22"
                    timeZone = "Venezuela Standard Time"
                Case "-3.5~23"
                    timeZone = "Newfoundland Standard Time"
                Case "-3~24"
                    timeZone = "E. South America Standard Time"
                Case "-3~25"
                    timeZone = "Argentina Standard Time"
                Case "-3~26"
                    timeZone = "SA Eastern Standard Time"
                Case "-3~27"
                    timeZone = "Greenland Standard Time"
                Case "-2~28"
                    timeZone = "Mid-Atlantic Standard Time"
                Case "-1~29"
                    timeZone = "Azores Standard Time"
                Case "-1~30"
                    timeZone = "Cape Verde Standard Time"
                Case "0~31"
                    timeZone = "Morocco Standard Time"
                Case "0~32"
                    timeZone = "GMT Standard Time"
                Case "1~33"
                    timeZone = "Central Europe Standard Time"
                Case "1~34"
                    timeZone = "Romance Standard Time"
                Case "1~35"
                    timeZone = "Central European Standard Time"
                Case "1~36"
                    timeZone = "W. Central Africa Standard Time"
                Case "2~37"
                    timeZone = "GTB Standard Time"
                Case "2~38"
                    timeZone = "Middle East Standard Time"
                Case "2~39"
                    timeZone = "Egypt Standard Time"
                Case "2~40"
                    timeZone = "South Africa Standard Time"
                Case "2~41"
                    timeZone = "FLE Standard Time"
                Case "2~42"
                    timeZone = "Israel Standard Time"
                Case "3~43"
                    timeZone = "Arabic Standard Time"
                Case "3~44"
                    timeZone = "Arab Standard Time"
                Case "3~45"
                    timeZone = "Russian Standard Time"
                Case "3~46"
                    timeZone = "E. Africa Standard Time"
                Case "3.5~47"
                    timeZone = "Iran Standard Time"
                Case "4~48"
                    timeZone = "Arabian Standard Time"
                Case "4~49"
                    timeZone = "Azerbaijan Standard Time"
                Case "4.5~50"
                    timeZone = "Afghanistan Standard Time"
                Case "5~51"
                    timeZone = "Ekaterinburg Standard Time"
                Case "5~52"
                    timeZone = "Pakistan Standard Time"
                Case "5.5~53"
                    timeZone = "India Standard Time"
                Case "5.75~54"
                    timeZone = "Nepal Standard Time"
                Case "6~55"
                    timeZone = "N. Central Asia Standard Time"
                Case "6~56"
                    timeZone = "Bangladesh Standard Time"
                Case "5.5~57"
                    timeZone = "Sri Lanka Standard Time"
                Case "6.5~58"
                    timeZone = "Myanmar Standard Time"
                Case "7~59"
                    timeZone = "SE Asia Standard Time"
                Case "7~60"
                    timeZone = "North Asia Standard Time"
                Case "8~61"
                    timeZone = "China Standard Time"
                Case "8~62"
                    timeZone = "North Asia East Standard Time"
                Case "8~63"
                    timeZone = "Singapore Standard Time"
                Case "8~64"
                    timeZone = "W. Australia Standard Time"
                Case "8~65"
                    timeZone = "Taipei Standard Time"
                Case "9~66"
                    timeZone = "Tokyo Standard Time"
                Case "9~67"
                    timeZone = "Korea Standard Time"
                Case "9~68"
                    timeZone = "Yakutsk Standard Time"
                Case "9.5~59"
                    timeZone = "Cen. Australia Standard Time"
                Case "9.5~70"
                    timeZone = "AUS Central Standard Time"
                Case "10~71"
                    timeZone = "E. Australia Standard Time"
                Case "10~72"
                    timeZone = "AUS Eastern Standard Time"
                Case "10~73"
                    timeZone = "West Pacific Standard Time"
                Case "10~74"
                    timeZone = "Tasmania Standard Time"
                Case "10~75"
                    timeZone = "Vladivostok Standard Time"
                Case "11~76"
                    timeZone = "Magadan Standard Time"
                Case "12~77"
                    timeZone = "New Zealand Standard Time"
                Case "12~78"
                    timeZone = "Fiji Standard Time"
                Case "13~79"
                    timeZone = "Tonga Standard Time"
            End Select

            Dim info As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone)
            Return TimeZoneInfo.ConvertTimeFromUtc(campaignDate, info)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class
