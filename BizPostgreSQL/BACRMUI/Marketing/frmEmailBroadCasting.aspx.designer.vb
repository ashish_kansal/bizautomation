'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmEmailBroadCasting

    '''<summary>
    '''divSendLimit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divSendLimit As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblMax24Send control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMax24Send As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''divSentMail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divSentMail As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblSentLast24Hour control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSentLast24Hour As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnSend control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSend As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnSendToMyself control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSendToMyself As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnBroadcast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBroadcast As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnSaveClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveClose As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''pnlMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlMessage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''litMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''litSuccess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litSuccess As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRecords control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRecords As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''hlView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hlView As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''chkTrack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkTrack As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkSendCopy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkSendCopy As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkFollowUpStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkFollowUpStatus As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''ddlFollowupStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlFollowupStatus As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''hplAttachments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplAttachments As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblAttachents control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAttachents As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlCategory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCategory As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlEmailTemplate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlEmailTemplate As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSubject control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSubject As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''hdnFollowupDateUpdateTemplate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnFollowupDateUpdateTemplate As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''RadEditor1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RadEditor1 As Global.Telerik.Web.UI.RadEditor

    '''<summary>
    '''Table2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Table2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''hdnConfigurationID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnConfigurationID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnFrom As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnDomain control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnDomain As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnAWSAccessKey control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAWSAccessKey As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnAWSSecretKey control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAWSSecretKey As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnDivIds control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnDivIds As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnStatementDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnStatementDate As Global.System.Web.UI.WebControls.HiddenField
End Class
