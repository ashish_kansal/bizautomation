﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO
Partial Public Class frmSurveyCssList
    Inherits BACRMPage
    Dim intType As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intType = GetQueryStringVal( "Type")

            If Not IsPostBack Then
                
                BindData()
                hplNew.Attributes.Add("onclick", "NewCssAndJs();")
            End If

            If intType = 3 Then
                lblName.Text = "Survey Styles"
                hplNew.Text = "New Style"
            ElseIf intType = 4 Then
                lblName.Text = "Survey JavaScript"
                hplNew.Text = "New JavaScript"
            End If
            hplNew.NavigateUrl = "~/ECommerce/frmCss.aspx?Type=" & intType

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindData()
        Try
            Dim objSite As New Sites
            Dim dtStyle As DataTable

            objSite.SiteID = 0
            objSite.DomainID = Session("DomainID")
            objSite.StyleType = intType
            dtStyle = objSite.GetStyles()
            dgStyles.DataSource = dtStyle
            dgStyles.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgStyles_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgStyles.ItemCommand
        Try
            If e.CommandName = "Edit" Then Response.Redirect("../ECommerce/frmCss.aspx?CssID=" & e.Item.Cells(0).Text & "&Type=" & intType.ToString(), False)
            If e.CommandName = "Delete" Then
                DeleteFile(e.Item.Cells(2).Text)
                Dim objSite As New Sites
                objSite.CssID = e.Item.Cells(0).Text
                objSite.DomainID = Session("DomainID")
                objSite.DeleteStyle()
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub DeleteFile(ByVal FileName As String)
        Try
            Dim strUploadFile As String = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & Session("DomainID") & "\Survey\" & FileName

            If File.Exists(strUploadFile) Then
                File.Delete(strUploadFile)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgStyles_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgStyles.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class