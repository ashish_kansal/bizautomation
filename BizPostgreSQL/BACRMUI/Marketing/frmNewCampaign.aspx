<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmNewCampaign.aspx.vb"
    Inherits="frmNewCampaign" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Campaign</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <%--<link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />--%>
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/dateFormat.js" type="text/javascript"></script>
    <script language="javascript">
        //        function CheckNumber(cint, e) {
        //            var k;
        //            document.all ? k = e.keyCode : k = e.which;
        //            if (cint == 1) {
        //                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
        //                    if (e.preventDefault) {
        //                        e.preventDefault();
        //                    }
        //                    else
        //                        e.returnValue = false;
        //                    return false;
        //                }
        //            }
        //            if (cint == 2) {
        //                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
        //                    if (e.preventDefault) {
        //                        e.preventDefault();
        //                    }
        //                    else
        //                        e.returnValue = false;
        //                    return false;
        //                }
        //            }
        //        }
        function Save() {
            if (document.form1.txtCampaignName.value == '') {
                alert("Enter Campaign Name")
                document.form1.txtCampaignName.focus()
                return false;
            }
            if (document.form1.txtCampaignCode != null)
                if (document.form1.txtCampaignCode.value == '') {
                    alert("Enter Campaign Code")
                    document.form1.txtCampaignCode.focus()
                    return false;
                }
            if (document.form1.calLaunch_txtDate.value == '') {
                alert("Select Launch Date")
                document.form1.calLaunch_txtDate.focus()
                return false;
            }
            //            if (document.Form1.calEnd_txtDate.value == '') {
            //                alert("Select End Date")
            //                document.Form1.calEnd_txtDate.focus()
            //                return false;
            //            }

            if (!isDate(document.form1.calLaunch_txtDate.value, format)) {
                alert("Enter Valid Launch Date");
            }

            //            if (!isDate(document.form1.calEnd_txtDate.value, format)) {
            //                alert("Enter Valid End Date");
            //            }

            //            if (compareDates(document.form1.calLaunch_txtDate.value, format, document.form1.calEnd_txtDate.value, format) == 1) {
            //                alert("End Date must be greater than or equal to Launch Date");
            //                return false;
            //            }
        }
        function Focus() {
            document.form1.txtCampaignName.focus();
        }
        function Close() {
            window.close()
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save &amp; Close" CssClass="button">
                        </asp:Button>
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                        </asp:Button>&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal EnableViewState="False" ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New Campaign
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="600" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <br>
                <table width="100%" border="0">
                   <%-- <tr>
                        <td width="150px">
                            &nbsp;
                        </td>
                        <td align="left" class="normal1">
                            <asp:RadioButtonList runat="server" ID="rblCampaignMode" RepeatDirection="Horizontal"
                                AutoPostBack="true">
                                <asp:ListItem Text="<b>Online Campaign</b><br><i>Track via landing page</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                    Selected="True" Value="True"></asp:ListItem>
                                <asp:ListItem Text="<b>Offline Campaign</b><br><i>Track via Campaign drop down value (within Leads, Prospects, Accounts)</i>"
                                    Value="False"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="normal1" align="right">
                            Campaign Name<font color="red">*</font>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCampaignName" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Launch Date<font color="red">*</font>
                        </td>
                        <td class="normal1">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <BizCalendar:Calendar ID="calLaunch" runat="server" />
                                    </td>
                                    <%--  <td>
                                        &nbsp;&nbsp;End Date<font color="red">*</font>
                                    </td>
                                    <td>
                                        <BizCalendar:Calendar ID="calEnd" runat="server" />
                                    </td>--%>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <label for="chkMonthly" title="">
                                Monthly Campaign?</label>
                        </td>
                        <td align="left">
                            <asp:CheckBox runat="server" ID="chkMonthly" CssClass="signup" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Campaign Status
                        </td>
                        <td>
                            <asp:DropDownList ID="dllCampaignStatus" runat="server" Width="180" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Campaign Type
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCampaignType" runat="server" Width="180" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Region
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRegion" runat="server" Width="180" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                   <%-- <tr>
                        <td class="normal1" align="right">
                            Cost
                        </td>
                        <td>
                            <asp:TextBox ID="txtCost" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="normal1" align="right">
                            Number Sent
                        </td>
                        <td>
                            <asp:TextBox ID="txtNoSent" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                   <%-- <tr id="trCampaignCode" runat="server" visible="false">
                        <td class="normal1" align="right" valign="top">
                            Campaign Code<font color="red">*</font>
                        </td>
                        <td class="normal1" align="left" valign="top">
                            <asp:TextBox runat="server" ID="txtCampaignCode" CssClass="signup" Width="200px"></asp:TextBox>e.g.
                            MarchCPC500<br />
                            Instructions: Say you have the following landing page: www.123.com. In your PPC
                            campaign, you need to assign it a unique number such as 333. Direct the ad to http://www.123.com?campaign=333
                            . In your Campaign code field enter 333.
                        </td>
                    </tr>--%>
                </table>
                <br>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
