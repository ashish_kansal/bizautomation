''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Survey
''' Class	 : frmCampaignSurveyQuestions
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a interface for editing a Survey Questions
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	09/13/2005	Created
''' </history>
''' ''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Survey

    Public Class frmCampaignSurveyQuestions
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents tblCampaignSurveyQuestions As System.Web.UI.WebControls.Table
        Protected WithEvents hdSurId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdAction As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdQIDDelete As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdQuesNewRow As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents btnAddNewQuestion As System.Web.UI.WebControls.Button
        Protected WithEvents btnDeleteQuestion As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveQuestion As System.Web.UI.WebControls.Button
        Protected WithEvents ValidationSummary As System.Web.UI.WebControls.ValidationSummary
        Protected WithEvents litClientMessage As System.Web.UI.WebControls.Literal
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime the page is called. In this event we will 
        '''     get the data from the DB and populate the Tables and the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                Dim numSurId As Integer = GetQueryStringVal( "numSurId")           'Get the Survey Id in local variable
                 ' = "MarSurvey"
                hdSurId.Value = numSurId                                            'Set the Survey Id in hidden
                If hdAction.Value = "" Then
                    Dim objSurvey As New SurveyAdministration                       'Create an object of Survey Administration Class
                    objSurvey.DomainId = Session("DomainID")                        'Set the Domain ID
                    objSurvey.UserId = Session("UserID")                            'Set the User ID
                    objSurvey.XMLFilePath = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" 'Set the path to the xml file where the survey info is temp stored
                    objSurvey.SurveyId = hdSurId.Value                              'Set the Survey ID

                    Dim dsSurveyInformationTemp As DataSet                          'Declare a DataSet object
                    dsSurveyInformationTemp = objSurvey.GetSurveyInformationTemp()  'Call to retrieve the existing data temporarily
                    DisplayQuestion(dsSurveyInformationTemp, IIf(hdSurId.Value = 0, True, False)) 'Display a blank row of empty question (for new survey only)
                End If
                PostInitializeControlsClientEvents()                                'Call to attache the client events
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method creates a rows of question for the Survey
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub DisplayQuestion(ByVal dsSurveyInformationTemp As DataSet, ByVal boolBlankRow As Boolean)
            Try
                Dim dtQuestionnaireContent As DataTable                                     'Declare a new datatable
                dtQuestionnaireContent = dsSurveyInformationTemp.Tables("SurveyQuestionMaster") 'Get the DataTable
                tblCampaignSurveyQuestions.Rows.Clear()                                     'Clear the rows of the table
                tblCampaignSurveyQuestions.Controls.Clear()                                 'Clear any controls (if exists)

                If boolBlankRow Then                                                        'If blank row is requested then
                    Dim drQuestionnaireContent As DataRow                                   'Declare a new datarow
                    drQuestionnaireContent = dtQuestionnaireContent.NewRow                  'Creat a new row for the same table
                    If dtQuestionnaireContent.Rows.Count > 0 Then
                        dtQuestionnaireContent.Columns.Add("numQuestionID", System.Type.GetType("System.Int16"), "[numQID]") 'Getting the int conversion of the question number for computation
                        drQuestionnaireContent.Item("numQID") = CInt(dtQuestionnaireContent.Compute("Max(numQuestionID)", "")) + 1 'the next count of rows
                        dtQuestionnaireContent.Columns.Remove("numQuestionID")              'Remove the column
                    Else : drQuestionnaireContent.Item("numQID") = 1                           'the first row
                    End If
                    drQuestionnaireContent.Item("vcQuestion") = ""                          'Blank Question
                    drQuestionnaireContent.Item("tIntAnsType") = 0                          'Checkbox by default
                    drQuestionnaireContent.Item("intNumOfAns") = 0                          'Nos of answer is 0 by default
                    drQuestionnaireContent.Item("boolDeleted") = 0                          'No answers when the blank row is added
                    drQuestionnaireContent.Item("boolMatrixColumn") = 0
                    drQuestionnaireContent.Item("intNumOfMatrixColumn") = 0
                    dtQuestionnaireContent.Rows.Add(drQuestionnaireContent)                 'Add this blank row
                End If
                Dim tblRowDisplayQuestion As TableRow                                       'Declare a Table Row object
                Dim tblCellOneDisplayQuestion, tblCellTwoDisplayQuestion, tblCellThreeDisplayQuestion As TableCell 'Declare Table Cells object
                Dim drQuestionnaireDataRows() As DataRow                                    'Declare a DataRow
                drQuestionnaireDataRows = dtQuestionnaireContent.Select("boolDeleted = 0")  'Filter rows which are not deleted

                Dim numQuestionIndex As Integer                                             'Declare the row index
                Dim sRulesAttached As String                                                'Variable to display if a rule is attached to a question or not
                Dim drQuestionnaireDataRow As DataRow
                For Each drQuestionnaireDataRow In drQuestionnaireDataRows
                    sRulesAttached = ""                                                     'No Rules attached initially
                    numQuestionIndex += 1                                                   'Question Index is one more than row index
                    tblRowDisplayQuestion = New TableRow                                    'Instantiate a new table row everytime
                    tblRowDisplayQuestion.EnableViewState = False                           'Not to have a view state for the row
                    tblRowDisplayQuestion.Attributes.Add("style", "display:inline;")
                    tblRowDisplayQuestion.ID = "tRow" & drQuestionnaireDataRow.Item("numQID") 'Generate Unique ros Ids
                    tblCellOneDisplayQuestion = New TableCell                               'Create an object of table cell
                    tblCellOneDisplayQuestion.HorizontalAlign = HorizontalAlign.Right       'Align the label to right an top
                    tblCellOneDisplayQuestion.VerticalAlign = VerticalAlign.Top
                    tblCellOneDisplayQuestion.CssClass = "normal1"                          'Add the class attribute
                    tblCellOneDisplayQuestion.Text = "Question " & numQuestionIndex & " <span class='normal4'>*</span> : " 'Display the label

                    tblCellTwoDisplayQuestion = New TableCell                               'Create an object of table cell
                    tblCellTwoDisplayQuestion.CssClass = "normal1"                          'Add the class attribute
                    Dim oTextBox As New TextBox                                             'Instantiate a TextBox
                    oTextBox.ID = "txtQuestion" & drQuestionnaireDataRow.Item("numQID")     'set the name of the textbox
                    oTextBox.Width = Unit.Pixel(430)                                        'set the width of the textbox
                    oTextBox.Height = Unit.Pixel(40)                                        'set the height of the textbox
                    oTextBox.TextMode = TextBoxMode.MultiLine                               'This is a multiline textarea
                    oTextBox.CssClass = "signup"                                            'Set the class attribute for the textbox control
                    oTextBox.Attributes.Add("onKeyUp", "CheckTextLength(this,250)")         'Add validations to the textbox
                    oTextBox.Attributes.Add("onChange", "CheckTextLength(this,250)")
                    oTextBox.Text = ReSetFromXML(drQuestionnaireDataRow.Item("vcQuestion")) 'Set the question for display
                    oTextBox.EnableViewState = False                                        'Text box will not have a view state
                    tblCellTwoDisplayQuestion.Controls.Add(oTextBox)                        'Add the textbox to the table cell
                    Dim valReqControl As New RequiredFieldValidator                         'Declare a required field validator
                    valReqControl.ID = "vReq" & numQuestionIndex                            'Giving id to the validator control
                    valReqControl.ControlToValidate = "txtQuestion" & drQuestionnaireDataRow.Item("numQID")      'Attach the control to validate
                    valReqControl.EnableClientScript = True                                 'Enable Client script
                    valReqControl.InitialValue = ""                                         'Set the Initial value so that it also validates drop down
                    valReqControl.ErrorMessage = "Please enter " & "Question " & numQuestionIndex & "." 'Specify the error message to be displayed
                    valReqControl.Display = ValidatorDisplay.None                           'Error message never displays inline
                    tblCellTwoDisplayQuestion.Controls.Add(valReqControl)                   'Add the validator to the table cell

                    tblCellThreeDisplayQuestion = New TableCell                             'Create an object of table cell
                    tblCellThreeDisplayQuestion.CssClass = "normal1"                        'Add the class attribute
                    tblCellThreeDisplayQuestion.VerticalAlign = VerticalAlign.Top           'align the buttons etc to the top
                    Dim oAddEditAnsButton As New Button                                     'Instantiate a button
                    oAddEditAnsButton.Text = "Add / Edit Answers"                           'Set the lable to the button
                    oAddEditAnsButton.CssClass = "button"                                   'Set the class attribute
                    If drQuestionnaireDataRow.Item("vcQuestion") <> "" Then                 'The last new entry is nto saved into temp files so ask for save before edit of answer
                        oAddEditAnsButton.Attributes.Add("onclick", "javascript: return AddEditAnswer(" & drQuestionnaireDataRow.Item("numQID") & ",'Edit');") 'Add script to enable adding an answer
                        sRulesAttached = IIf(CheckIfRulesAttached(drQuestionnaireDataRow.Item("numQID"), dsSurveyInformationTemp.Tables("SurveyAnsMaster")), "Rule(s) added", "")  'Rule Attached or not
                    Else : oAddEditAnsButton.Attributes.Add("onclick", "javascript: return AddEditAnswer(" & drQuestionnaireDataRow.Item("numQID") & ",'New');") 'Add script to enable adding an answer
                    End If
                    tblCellThreeDisplayQuestion.Controls.Add(oAddEditAnsButton)             'Add the Add Edit button to the cell
                    If numQuestionIndex > 1 Then
                        Dim oDeleteAnsButton As New Button                                  'Instantiate a button
                        oDeleteAnsButton.Text = "Delete"                                    'Set the lable to the button
                        oDeleteAnsButton.CssClass = "button"                                'Set the class attribute
                        oDeleteAnsButton.Attributes.Add("onclick", "javascript: return DeleteQuestion(" & drQuestionnaireDataRow.Item("numQID") & ",'Delete');") 'Add script to enable deleting an question
                        tblCellThreeDisplayQuestion.Controls.Add(New LiteralControl(" "))   'Add the spacer between the buttons
                        tblCellThreeDisplayQuestion.Controls.Add(oDeleteAnsButton)          'Add the Delete button to the cell
                    End If
                    If sRulesAttached <> "" Then                                            'Indicates that a rule is attached
                        tblCellThreeDisplayQuestion.Controls.Add(New LiteralControl("<br>" & sRulesAttached)) 'Rule is attached
                    End If
                    tblRowDisplayQuestion.Cells.Add(tblCellOneDisplayQuestion)              'Add the lable cell specify to question number
                    tblRowDisplayQuestion.Cells.Add(tblCellTwoDisplayQuestion)              'Add the lable cell specify to question textbox
                    tblRowDisplayQuestion.Cells.Add(tblCellThreeDisplayQuestion)            'Add the lable cell specify to buttons
                    tblCampaignSurveyQuestions.Rows.Add(tblRowDisplayQuestion)              'Add the row to the table
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to handle Delete question after the 
        '''     "Delete" button is clicked
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	05/19/2006	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Private Sub btnDeleteQuestion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteQuestion.Click
            Try
                Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
                objSurvey.DomainId = Session("DomainID")                            'Set the Domain ID
                objSurvey.UserId = Session("UserID")                                'Set the User ID
                objSurvey.XMLFilePath = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" 'Set the path to the xml file where the survey info is temp stored
                objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID
                objSurvey.QuestionId = hdQIDDelete.Value                            'Set the Delete Question Id
                objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()         'Call to retrieve the existing data temporarily
                DeleteQuestionnaire(objSurvey)                                      'Call to delete the question
                hdQIDDelete.Value = ""                                              'Reset the id of the question to be deleted
                DisplayQuestion(objSurvey.SurveyInfo, CBool(hdQuesNewRow.Value))    'Call to display the existing quesitons (plus: if reqeusted) an additional blank row
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to handle adding a new blank question after the 
        '''     "Add New Question" button is clicked
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' 	[Debasish Tapan Nag]	05/19/2006	Modified    Deletion of a Question removed from this logic
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Private Sub btnAddNewQuestion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNewQuestion.Click, btnSaveQuestion.Click
            Try
                Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
                objSurvey.DomainId = Session("DomainID")                            'Set the Domain ID
                objSurvey.UserId = Session("UserID")                                'Set the User ID
                objSurvey.XMLFilePath = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" 'Set the path to the xml file where the survey info is temp stored
                objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID

                objSurvey.SurveyInfo = objSurvey.GetSurveyInformationTemp()         'Call to retrieve the existing data temporarily
                SyncSurveyInformationTemp(objSurvey)                                'Sync the dataset with the latest changes
                objSurvey.SaveSurveyInformationTemp()                               'Call to save the existing data temporarily
                If hdAction.Value = "SaveB4SavingSurvey" Then                       'Entire Survey has to be saved in this case
                    litClientMessage.Text = "<script language=javascript>InitiateSavingSurvey();</script>" 'Initaite saving survey by a click on the button associated with the survey
                    Exit Sub
                ElseIf hdAction.Value = "SaveB4SavingAndClosingSurvey" Then         'Entire Survey has to be saved in this case but also the survey has to be closed
                    litClientMessage.Text = "<script language=javascript>InitiateSavingSurveyBeforeClose();</script>" 'Initaite saving survey by a click on the button associated with the survey
                    Exit Sub
                End If
                Dim btnSrcButton As Button = CType(sender, Button)                  'typecast to button
                DisplayQuestion(objSurvey.SurveyInfo, IIf(btnSrcButton.ID.ToString = "btnAddNewQuestion", True, False)) 'Call to display the existing quesitons (plus: if reqeusted) an additional blank row
                If hdAction.Value = "SaveB4AddingAnswers" Then                      'check the action reque3sted, If it is save data before opening answers window
                    litClientMessage.Text = "<script language=javascript>AddEditAnswer(" & objSurvey.SurveyInfo.Tables("SurveyQuestionMaster").Rows(objSurvey.SurveyInfo.Tables("SurveyQuestionMaster").Rows.Count - 1).Item("numQID") & ",'Edit');</script>" 'clsoe this window
                    hdQuesNewRow.Value = "False"                                    'New blank row for Question not Added
                Else : hdQuesNewRow.Value = "True"                                     'New Question Added
                End If
                hdAction.Value = ""                                                 'Reset the action flag
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to sync the dataset of Survey Information with the latest changes
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Function SyncSurveyInformationTemp(ByRef objSurvey As SurveyAdministration)
            Try
                Dim dtSurveyQuestionMaster As DataTable = objSurvey.SurveyInfo.Tables("SurveyQuestionMaster")
                Dim drSurveyQuestionMasterRow As DataRow                            'Declare a DataRow
                Dim numSurveyQuestionMasterRowIndex As Integer                      'Declare a index for rows
                For Each drSurveyQuestionMasterRow In dtSurveyQuestionMaster.Rows   'Loop through the rows
                    numSurveyQuestionMasterRowIndex = drSurveyQuestionMasterRow.Item("numQID")
                    If Trim(Request.Form("txtQuestion" & numSurveyQuestionMasterRowIndex)) <> "" And (Request.Form("txtQuestion" & numSurveyQuestionMasterRowIndex) <> "$$@@$$RequestDeletion$$@@$$") And drSurveyQuestionMasterRow.Item("boolDeleted") = 0 Then
                        drSurveyQuestionMasterRow.Item("vcQuestion") = MakeSafeForXML(Request.Form("txtQuestion" & numSurveyQuestionMasterRowIndex)) 'Capture the question
                    End If
                Next
                numSurveyQuestionMasterRowIndex += 1                                'Increment the counter
                If Trim(Request.Form("txtQuestion" & numSurveyQuestionMasterRowIndex)) <> "" And Trim(Request.Form("txtQuestion" & numSurveyQuestionMasterRowIndex)) <> "$$@@$$RequestDeletion$$@@$$" Then 'Check if a new question is added
                    drSurveyQuestionMasterRow = dtSurveyQuestionMaster.NewRow()     'Get an new row of the table
                    drSurveyQuestionMasterRow.Item("numQID") = numSurveyQuestionMasterRowIndex 'Insert the new question id
                    drSurveyQuestionMasterRow.Item("vcQuestion") = MakeSafeForXML(Request.Form("txtQuestion" & numSurveyQuestionMasterRowIndex)) 'Insert the new question
                    drSurveyQuestionMasterRow.Item("tIntAnsType") = 0               'Checkbox by default
                    drSurveyQuestionMasterRow.Item("intNumOfAns") = 0               'Nos of answer is 0 by default
                    drSurveyQuestionMasterRow.Item("boolDeleted") = 0               'Set the flag indicating that it is not deleted
                    drSurveyQuestionMasterRow.Item("boolMatrixColumn") = 0
                    drSurveyQuestionMasterRow.Item("intNumOfMatrixColumn") = 0
                    dtSurveyQuestionMaster.Rows.Add(drSurveyQuestionMasterRow)      'Add the question to the table
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to Delete a Survey Questionnaire
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Sub DeleteQuestionnaire(ByRef objSurvey As SurveyAdministration)
            Try
                objSurvey.DeleteSurveyQuestionTemp(hdQIDDelete.Value)               'Call to delete teh question
                SyncSurveyInformationTemp(objSurvey)                                'Sync the dataset with the latest changes
                objSurvey.SaveSurveyInformationTemp()                               'Call to save the existing data temporarily
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to check if a particular question has a rule attached to any of its answers
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <param name = "numQID">The Question ID which is being checked for answers</param>
        ''' <param name = "dtAnswerRow">The DataTable of answers</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/26/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Function CheckIfRulesAttached(ByVal numQID As Integer, ByVal dtAnswerRow As DataTable) As Boolean
            Try
                Dim drSurveyAnswers() As DataRow                                                                'Declare a DataRow array
                drSurveyAnswers = dtAnswerRow.Select("numQID = '" & numQID & "'")                                      'Filter the rows of this question
                Dim drSurveyAnswer As DataRow                                                                   'Declare a DataRow
                For Each drSurveyAnswer In drSurveyAnswers                                                      'loop through the rows that match the criteria
                    If drSurveyAnswer.Item("boolSurveyRuleAttached") = "1" Then Return True
                Next
                Return False
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to attach client side events to the controls
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub PostInitializeControlsClientEvents()
            Try
                btnAddNewQuestion.Attributes.Add("onclick", "javascript: AuroSaveQuestionsBeforeAddingAnswers();") 'calls for adding a new question
                btnSaveQuestion.Attributes.Add("onclick", "javascript: SetActionOfPage();")         'calls for adding a new question
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is safe casts the string for XML
        ''' </summary>
        ''' <param name = "sValue">The string which is to be made XML safe</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function MakeSafeForXML(ByVal sValue As String) As String
            Try
                Return Replace(Replace(Replace(Replace(Replace(sValue, "'", "&#39;"), """", "&#39;&#39;"), "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is reverts the string to its former html display format
        ''' </summary>
        ''' <param name = "sValue">The string which is to be made XML safe</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' 	[Debasish Tapan Nag]	05/20/2006	Modified to add condition for blank string
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function ReSetFromXML(ByVal sValue As String) As String
            Try
                If sValue <> "" Then
                    Return Replace(Replace(Replace(Replace(sValue, "&amp;", "&"), "&#39;", "'"), "&lt;", "<"), "&gt;", ">")
                Else : Return ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace