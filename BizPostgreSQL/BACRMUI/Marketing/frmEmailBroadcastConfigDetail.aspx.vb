﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports System.Net
Imports System.IO

Public Class frmEmailBroadcastConfigDetail
    Inherits BACRMPage

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub dgEmailConfigurationDetail_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmailConfigurationDetail.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objCampaign As New Campaign
                objCampaign.ConfigurationId = CCommon.ToDecimal(e.CommandArgument)
                objCampaign.DomainID = CCommon.ToDecimal(Session("DomainID"))

                Try
                    objCampaign.DeleteEmailBroadcastConfiguration()

                Catch ex As Exception
                    If ex.Message = "PRIMARY" Then
                        lblMessage.Text = "Child Records Found.Can not delete Email Broadcasting Configuration!!"
                    Else
                        Throw ex
                    End If
                End Try
                
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub dgEmailConfigurationDetail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgEmailConfigurationDetail.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

    Sub BindData()
        Try
            Dim objCampaign As New Campaign
            Dim dtTable As DataTable

            objCampaign.DomainID = CCommon.ToString(Session("DomainId"))
            objCampaign.Mode = 1

            dtTable = objCampaign.GetEmailBroadcastConfiguration()

            If dtTable.Rows.Count > 0 Then
                dgEmailConfigurationDetail.DataSource = dtTable
                dgEmailConfigurationDetail.DataBind()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


End Class