<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCampaignSurveyList.aspx.vb"
    Inherits="BACRM.UserInterface.Survey.frmCampaignSurveyList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Marketing Survey List</title>
    <script language="javascript" src="../javascript/Surveys.js"></script>
    <script>
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <iframe id="IfrOpenSurvey" src="frmEditSurveyRedirection.aspx" frameborder="0" width="10"
                    scrolling="no" height="10" left="0" right="0"></iframe>
            </div>
            <div class="pull-right">
                <input class="btn btn-primary" runat="server" id="btnAddNewSurvey" onclick="javascript: EditSurvey(0)"
                    type="button" value="New Survey">
                &nbsp;&nbsp;&nbsp;<input class="btn btn-primary" runat="server" id="Button1" onclick="javascript: SurveyCss(3)"
                    type="button" value="Survey CSS">&nbsp;&nbsp;&nbsp;<input class="btn btn-primary" runat="server"
                        id="Button3" onclick="javascript: SurveyCss(4)" type="button" value="Survey Javascript">
                &nbsp;&nbsp;&nbsp;<input class="btn btn-primary" runat="server" id="Button2" onclick="javascript: document.location.href = 'frmSurveyTemplate.aspx';"
                    type="button" value="Survey Template">
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Surveys/ Questionnaires
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgSurveyList" runat="server" Width="100%" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-striped"
                    DataKeyField="numSurID" CellPadding="0" CellSpacing="0" ShowHeader="True" AllowPaging="False"
                    AllowSorting="True" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="false" HeaderText="Survey ID" DataField="numSurID" SortExpression="numSurID"></asp:BoundColumn>
                        <asp:HyperLinkColumn ItemStyle-HorizontalAlign="Left" DataNavigateUrlField="numSurID"
                            DataNavigateUrlFormatString="javascript:EditSurvey({0});" DataTextField="vcSurName"
                            HeaderText="Survey Name" SortExpression="vcSurName"></asp:HyperLinkColumn>
                        <asp:TemplateColumn HeaderText="Date Created" SortExpression="bintCreatedOn">
                            <ItemTemplate>
                                <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "bintCreatedOn"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:HyperLinkColumn ItemStyle-HorizontalAlign="Left" HeaderText="Total Respondents"
                            DataNavigateUrlField="LinkedReport" DataNavigateUrlFormatString="javascript:EditSurveyRespondents({0});"
                            SortExpression="numTotalRespondents" DataTextField="numTotalRespondents"></asp:HyperLinkColumn>
                        <asp:HyperLinkColumn ItemStyle-HorizontalAlign="Left" HeaderText="Q & A Averages"
                            DataNavigateUrlField="numSurID" DataNavigateUrlFormatString="javascript:EditSurveyQAAgerages({0});"
                            SortExpression="" Text="Open Report"></asp:HyperLinkColumn>
                        <asp:TemplateColumn ItemStyle-Width="25" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDeleteAction" runat="server" CssClass="btn btn-danger btn-xs"
                                    CommandName="Delete" Visible="True" OnClick="btnDeleteAction_Command" CausesValidation="False"><i class="fa fa-trash"></i></asp:LinkButton>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
													<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtColumnSorted" runat="server" Text="bintCreatedOn" Visible="False"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Text="Desc" Visible="False"></asp:TextBox>

     <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
