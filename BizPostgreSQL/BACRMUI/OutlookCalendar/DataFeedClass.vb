﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Collections
Imports System.Collections.Generic

Imports System.Globalization
Imports System.Xml
Imports System.Xml.XPath
Imports System.Net
Imports System.DateTime
Imports Microsoft.VisualBasic.DateAndTime
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common
Namespace wdCalender.Code

    Public Class DataFeedClass

        Public Function addCalender(ByVal StartDateTimeUtc As DateTime, ByVal EndDateTimeUtc As DateTime, ByVal subject As String,
                                               ByVal AllDayEvent As Boolean) As Dictionary(Of String, Object)

            Dim ret As Dictionary(Of String, Object) = New Dictionary(Of String, Object)()
            Dim sqlReturn As Integer = 0

            Try
                Dim RecurringId As Integer = -999
                '' Adding apponitment data to DB
                Dim ObjOutlook As New COutlook


                ObjOutlook.ResourceName = HttpContext.Current.Session("ResourceIDEmployee")
                ObjOutlook.AllDayEvent = AllDayEvent

                Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                strSplitStartTimeDate = CType(StartDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                strSplitEndTimeDate = CType(EndDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC

                ObjOutlook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                ObjOutlook.StartDateTimeUtc = strSplitStartTimeDate

                ObjOutlook.Subject = subject
                ObjOutlook.EnableReminder = 0
                ObjOutlook.ReminderInterval = 0
                ObjOutlook.ShowTimeAs = 0
                ObjOutlook.Importance = 0
                ObjOutlook.Status = 0
                ObjOutlook.RecurrenceKey = RecurringId
                sqlReturn = ObjOutlook.AddActivity()

                ret.Add("IsSuccess", True)
                ret.Add("Msg", "add success")
                ret.Add("Data", sqlReturn.ToString)

            Catch ex As Exception

                ret.Add("IsSuccess", False)
                ret.Add("Msg", "add failed")
                ret.Add("Data", sqlReturn.ToString)

            End Try

            Return ret

        End Function

        Public Function addDetailedCalendar(ByVal StartDateTimeUtc As DateTime, ByVal EndDateTimeUtc As DateTime, ByVal subject As String,
                                               ByVal AllDayEvent As Boolean, ByVal ActivityDescription As String, ByVal Location As String, ByVal color As Integer,
                                               ByVal tz As String, ByVal ShowTimeAs As Integer, ByVal Importance As Integer, ByVal EnableReminder As Boolean,
                                               ByVal ReminderInterval As Long, ByVal IsRecurring As Boolean, ByVal EndDateUtc As DateTime, ByVal DayOfWeekMaskUtc As Integer,
                                               ByVal DayOfMonth As Integer, ByVal MonthOfYear As Integer, ByVal Period As Char, ByVal PeriodMultiple As Integer, ByVal EditType As Integer) As Dictionary(Of String, Object)

            Dim ret As Dictionary(Of String, Object) = New Dictionary(Of String, Object)()
            Dim sqlReturn As Integer = 0

            Try

                Dim RecurringId As Integer = -999
                '' Adding apponitment data to DB
                Dim ObjOutlook As New COutlook
                If IsRecurring = True Then '' If appointment is recurring
                    ObjOutlook.EndDateUtc = CType(EndDateUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                    ObjOutlook.DayOfWeekMaskUtc = DayOfWeekMaskUtc
                    ObjOutlook.UtcOffset = CCommon.ToLong(HttpContext.Current.Session("ClientMachineUTCTimeOffset")) * -60
                    ObjOutlook.DayOfMonth = DayOfMonth
                    ObjOutlook.MonthOfYear = MonthOfYear
                    ObjOutlook.Period = Period
                    ObjOutlook.EditType = EditType

                    ObjOutlook.PeriodMultiple = PeriodMultiple
                    RecurringId = ObjOutlook.AddRecurring
                End If

                ObjOutlook.ResourceName = HttpContext.Current.Session("ResourceIDEmployee")
                ObjOutlook.AllDayEvent = AllDayEvent
                ObjOutlook.Location = Location
                ObjOutlook.ActivityDescription = ActivityDescription

                Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                strSplitStartTimeDate = CType(StartDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                strSplitEndTimeDate = CType(EndDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC

                ObjOutlook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                ObjOutlook.StartDateTimeUtc = strSplitStartTimeDate

                ObjOutlook.Subject = subject
                ObjOutlook.EnableReminder = EnableReminder
                ObjOutlook.ReminderInterval = ReminderInterval
                ObjOutlook.ShowTimeAs = ShowTimeAs
                ObjOutlook.Importance = Importance
                ObjOutlook.Status = 0
                ObjOutlook.Color = color

                ObjOutlook.RecurrenceKey = RecurringId
                sqlReturn = ObjOutlook.AddActivity()

                ret.Add("IsSuccess", True)
                ret.Add("Msg", "add success")
                ret.Add("Data", sqlReturn.ToString)

            Catch ex As Exception

                ret.Add("IsSuccess", False)
                ret.Add("Msg", "add failed")
                ret.Add("Data", sqlReturn.ToString)

            End Try

            Return ret

        End Function

        Public Function AddRecurrence(ByVal EndDateUtc As DateTime, ByVal DayOfWeekMaskUtc As Integer,
                                         ByVal DayOfMonth As Integer, ByVal MonthOfYear As Integer, ByVal Period As Char, ByVal PeriodMultiple As Integer,
                                         ByVal EditType As Integer) As Long

            Try
                Dim ObjOutlook As New COutlook

                Dim RecurringId As Integer

                ObjOutlook.EndDateUtc = CType(EndDateUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                ObjOutlook.DayOfWeekMaskUtc = DayOfWeekMaskUtc
                ObjOutlook.UtcOffset = CCommon.ToLong(HttpContext.Current.Session("ClientMachineUTCTimeOffset")) * -60
                ObjOutlook.DayOfMonth = DayOfMonth
                ObjOutlook.MonthOfYear = MonthOfYear
                ObjOutlook.Period = Period
                ObjOutlook.EditType = EditType

                ObjOutlook.PeriodMultiple = PeriodMultiple
                RecurringId = ObjOutlook.AddRecurring

                Return RecurringId
            Catch ex As Exception
                Throw ex
            End Try

            Return -999

        End Function

        Public Function UpdateRecurrence(ByVal RecurrenceID As Long, ByVal EndDateUtc As DateTime, ByVal DayOfWeekMaskUtc As Integer,
                                       ByVal DayOfMonth As Integer, ByVal MonthOfYear As Integer, ByVal Period As Char, ByVal PeriodMultiple As Integer,
                                       ByVal EditType As Integer)

            Try
                Dim ObjOutlook As New COutlook

                ObjOutlook.EndDateUtc = CType(EndDateUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                ObjOutlook.DayOfWeekMaskUtc = DayOfWeekMaskUtc
                ObjOutlook.UtcOffset = CCommon.ToLong(HttpContext.Current.Session("ClientMachineUTCTimeOffset")) * -60
                ObjOutlook.DayOfMonth = DayOfMonth
                ObjOutlook.MonthOfYear = MonthOfYear
                ObjOutlook.Period = Period
                ObjOutlook.EditType = EditType

                ObjOutlook.PeriodMultiple = PeriodMultiple

                ObjOutlook.LastReminderDateTimeUtc = DateTime.UtcNow
                ObjOutlook.RecurrenceKey = RecurrenceID
                ObjOutlook.UpdateRecurrence()
                ObjOutlook.Status = 0
                ObjOutlook.UpdateReminderByReccurence()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Friend Function IsInDayOfWeekMask(ByVal dowCandidate As DayOfWeek, ByVal dowMask As Integer) As Boolean
            Dim num As Integer = CInt(dowMask)
            Dim num2 As Integer = CInt(dowCandidate)
            Dim num3 As Integer = CInt(1) << num2
            Return ((num And num3) = num3)
        End Function

        Protected Function IsDayOfWeekMatch(ByVal date1 As DateTime, ByVal dowMask As Integer) As Boolean
            Return ((((dowMask And 1) <> 0) AndAlso (date1.DayOfWeek = DayOfWeek.Sunday)) _
                    OrElse ((((dowMask And 2) <> 0) AndAlso (date1.DayOfWeek = DayOfWeek.Monday)) _
                    OrElse ((((dowMask And 4) <> 0) AndAlso (date1.DayOfWeek = DayOfWeek.Tuesday)) _
                    OrElse ((((dowMask And 8) <> 0) AndAlso (date1.DayOfWeek = DayOfWeek.Wednesday)) _
                    OrElse ((((dowMask And 16) <> 0) AndAlso (date1.DayOfWeek = DayOfWeek.Thursday)) _
                    OrElse ((((dowMask And 32) <> 0) AndAlso (date1.DayOfWeek = DayOfWeek.Friday)) _
                    OrElse (((dowMask And 64) <> 0) AndAlso (date1.DayOfWeek = DayOfWeek.Saturday))))))))
        End Function

        Protected Function GetDayOfWeekFromDayOfWeekMask(ByVal dayOfWeekMask As Integer) As DayOfWeek
            Select Case dayOfWeekMask
                Case 1
                    Return DayOfWeek.Sunday

                Case 2
                    Return DayOfWeek.Monday

                Case 4
                    Return DayOfWeek.Tuesday

                Case 8
                    Return DayOfWeek.Wednesday

                Case 16
                    Return DayOfWeek.Thursday

                Case 32
                    Return DayOfWeek.Friday

                Case 34
                    Return DayOfWeek.Saturday
            End Select
            Return DayOfWeek.Monday
        End Function

        Protected Friend Function IdoToDate(ByVal dayOfMonth As Integer, ByVal dayOfWeekMask As Integer, ByVal month As Integer, ByVal year As Integer) As DateTime
            Dim date1 As DateTime = New DateTime(year, month, 1)
            Dim num As Integer = CInt(dayOfMonth)
            If dayOfWeekMask <> 0 Then
                If ((dayOfWeekMask = 127) OrElse (dayOfWeekMask = 62)) OrElse (dayOfWeekMask = 65) Then
                    Dim time As DateTime
                    If dayOfWeekMask = 127 Then
                        If dayOfMonth = -5 Then
                            date1 = New DateTime(date1.Year, date1.Month, CultureInfo.CurrentCulture.Calendar.GetDaysInMonth(year, month), date1.Hour, date1.Minute, date1.Second)
                            Return date1
                        End If
                        date1 = New DateTime(date1.Year, date1.Month, Math.Abs(num), date1.Hour, date1.Minute, date1.Second)

                        Return date1
                    End If
                    Dim num2 As Double = 0.0
                    If dayOfMonth = -5 Then
                        num2 = -1.0
                        num = 1
                        date1 = New DateTime(date1.Year, date1.Month, CultureInfo.CurrentCulture.Calendar.GetDaysInMonth(year, month), date1.Hour, date1.Minute, date1.Second)
                    Else
                        num2 = 1.0
                        num = Math.Abs(num)
                    End If
                    Dim time2 As New DateTime(year, month, date1.Day)
                    Do
                        time = time2
                        If Me.IsInDayOfWeekMask(time.DayOfWeek, dayOfWeekMask) Then
                            num -= 1
                        End If
                        time2 = time.AddDays(num2)
                    Loop While num > 0
                    Return time
                End If
                Dim dayOfWeekFromDayOfWeekMask As DayOfWeek = GetDayOfWeekFromDayOfWeekMask(dayOfWeekMask)
                While date1.DayOfWeek <> dayOfWeekFromDayOfWeekMask
                    date1 = date1.AddDays(1.0)
                End While
            Else
                num = Math.Min(num, CultureInfo.CurrentCulture.Calendar.GetDaysInMonth(year, month))
                date1 = New DateTime(date1.Year, date1.Month, num, date1.Hour, date1.Minute, date1.Second)
                Return date1
            End If
            Select Case dayOfMonth
                Case -5
                    If True Then
                        Dim date2 As DateTime = date1.AddDays(28.0)
                        If date2.Month = date1.Month Then
                            Return date2
                        End If
                        Return date1.AddDays(21.0)
                    End If
                Case -4
                    Return date1.AddDays(21.0)

                Case -3
                    Return date1.AddDays(14.0)

                Case -2
                    Return date1.AddDays(7.0)

                Case -1
                    Return date1
            End Select
            Return Nothing
        End Function

        Protected Function IsWeekDay(ByVal date1 As DateTime) As Boolean
            If ((date1.DayOfWeek <> DayOfWeek.Monday) AndAlso (date1.DayOfWeek <> DayOfWeek.Tuesday)) AndAlso (((date1.DayOfWeek <> DayOfWeek.Wednesday) AndAlso (date1.DayOfWeek <> DayOfWeek.Thursday)) AndAlso (date1.DayOfWeek <> DayOfWeek.Friday)) Then
                Return False
            End If
            Return True
        End Function

        Protected Function IsWeekendDay(ByVal date1 As DateTime) As Boolean
            If (date1.DayOfWeek <> DayOfWeek.Saturday) AndAlso (date1.DayOfWeek <> DayOfWeek.Sunday) Then
                Return False
            End If
            Return True
        End Function

        Protected Function CheckEndDate(ByVal recurrenceEndDateUtc As DateTime, ByVal dateIter As DateTime, ByVal EndDateUtc As DateTime) As Boolean
            If (recurrenceEndDateUtc.Date <> Date.MinValue.Date) AndAlso (dateIter.Date > recurrenceEndDateUtc.Date) Then
                Return False
            End If
            If dateIter.Date > EndDateUtc.Date Then
                Return False
            End If
            Return True
        End Function

        Protected Function GetFirstDateForPeriodMultiple(ByVal Period As Char, ByVal PeriodMultiple As Integer, ByVal fIStartDateUTC As DateTime, ByVal StartDateTimeUtc As DateTime) As DateTime
            Dim date1 As DateTime = Nothing
            Dim span As TimeSpan
            If PeriodMultiple <= 1 Then
                Return Nothing
            End If
            Dim startDateUTC As DateTime = fIStartDateUTC
            Select Case Period
                Case "D"
                    span = DirectCast(fIStartDateUTC.Date - StartDateTimeUtc.[Date], TimeSpan)
                    Return startDateUTC.AddDays(CDbl(PeriodMultiple - (span.Days Mod PeriodMultiple)))

                Case "W"
                    span = DirectCast(fIStartDateUTC.Date - StartDateTimeUtc.[Date], TimeSpan)
                    PeriodMultiple *= 7
                    Return startDateUTC.AddDays(CDbl(PeriodMultiple - (span.Days Mod PeriodMultiple)))

                Case "M"
                    If True Then
                        Dim num2 As Integer = fIStartDateUTC.Date.Year - StartDateTimeUtc.[Date].Year
                        Dim month As Integer = StartDateTimeUtc.[Date].Month
                        Dim num4 As Integer = fIStartDateUTC.Date.Month
                        If (num2 > 0) AndAlso (num4 < month) Then
                            num2 -= 1
                            num4 += 12
                        End If
                        Dim num5 As Integer = (num2 * 12) + (num4 - month)
                        Return startDateUTC.AddMonths(PeriodMultiple - (num5 Mod PeriodMultiple))
                    End If
                Case "Y"
                    Return date1
            End Select
            Return date1
        End Function

        Function DailyRecurrenceEngine(ByVal StartDateTimeUtc As DateTime, ByVal StartDateUTC_interval As DateTime, ByVal EndDateUtc_interval As DateTime,
                                       ByVal EndDateUtc_recurrence As DateTime, ByVal DayOfWeekMaskUtc As Integer, ByVal UtcOffset As Integer, ByVal DayOfMonth As Integer,
                                       ByVal MonthOfYear As Integer, ByVal PeriodMultiple As Integer, ByVal Period As Char) As ArrayList
            Dim DateTimeList As New ArrayList
            Dim dateIter As DateTime

            If StartDateTimeUtc.Date > EndDateUtc_interval.Date Then
                dateIter = Date.MinValue
                Return DateTimeList
            End If
            If StartDateTimeUtc.Date > StartDateUTC_interval.Date Then
                dateIter = StartDateTimeUtc.Date
            Else
                dateIter = StartDateUTC_interval.Date
            End If

            While CheckEndDate(EndDateUtc_recurrence, dateIter, EndDateUtc_interval)
                While Not IsInDayOfWeekMask(dateIter.DayOfWeek, DayOfWeekMaskUtc)
                    dateIter = dateIter.AddDays(1.0)
                End While

                If (CheckEndDate(EndDateUtc_recurrence, dateIter, EndDateUtc_interval)) Then
                    DateTimeList.Add(dateIter)
                Else
                    Exit While
                End If
                dateIter = dateIter.AddDays(1.0)
            End While

            Return DateTimeList
        End Function
        Function BasicRecurrenceEngine(ByVal StartDateTimeUtc As DateTime, ByVal StartDateUTC_interval As DateTime, ByVal EndDateUtc_interval As DateTime,
                                    ByVal EndDateUtc_recurrence As DateTime, ByVal DayOfWeekMaskUtc As Integer, ByVal UtcOffset As Integer, ByVal DayOfMonth As Integer,
                                    ByVal MonthOfYear As Integer, ByVal PeriodMultiple As Integer, ByVal Period As Char) As ArrayList

            Dim DateTimeList As New ArrayList
            Dim dateIter, date1 As DateTime
            Dim week As DayOfWeek
            Dim num As Integer

            If StartDateTimeUtc.Date > EndDateUtc_interval.Date Then
                dateIter = Date.MinValue
                Return DateTimeList
            End If
            If StartDateTimeUtc > StartDateUTC_interval Then
                Select Case Period
                    Case "D"
                        dateIter = StartDateTimeUtc
                        Exit Select

                    Case "W"
                        dateIter = StartDateTimeUtc
                        Exit Select

                    Case "M"
                        dateIter = StartDateTimeUtc
                        Exit Select

                    Case "Y"
                        dateIter = StartDateTimeUtc
                        Exit Select
                End Select
            Else
                Select Case Period
                    Case "D"
                        If PeriodMultiple <= 1 Then
                            dateIter = StartDateUTC_interval.Date
                            Exit Select
                        End If
                        dateIter = GetFirstDateForPeriodMultiple(Period, PeriodMultiple, StartDateUTC_interval, StartDateTimeUtc)
                        If dateIter <= EndDateUtc_interval Then
                            Exit Select
                        End If
                        Return DateTimeList

                    Case "W"
                        If PeriodMultiple <= 1 Then
                            dateIter = StartDateUTC_interval.Date
                            GoTo Label_01C5
                        End If
                        dateIter = GetFirstDateForPeriodMultiple(Period, PeriodMultiple, StartDateUTC_interval, StartDateTimeUtc)
                        If dateIter <= EndDateUtc_interval Then
                            GoTo Label_01C5
                        End If
                        Return DateTimeList

                    Case "M"
                        If PeriodMultiple <= 1 Then
                            dateIter = StartDateUTC_interval.[Date]
                            GoTo Label_025B
                        End If
                        dateIter = GetFirstDateForPeriodMultiple(Period, PeriodMultiple, StartDateUTC_interval, StartDateTimeUtc)
                        If dateIter <= EndDateUtc_interval Then
                            GoTo Label_025B
                        End If
                        Return DateTimeList

                    Case "Y"
                        date1 = New DateTime(StartDateUTC_interval.Year, StartDateTimeUtc.Month, StartDateTimeUtc.Day)
                        If date1 >= StartDateUTC_interval Then
                            GoTo Label_031D
                        End If
                        date1 = date1.AddYears(1)
                        If date1 <= EndDateUtc_interval Then
                            GoTo Label_031D
                        End If
                        Return DateTimeList
                End Select
            End If
            GoTo Label_0324
Label_01C5:
            week = StartDateTimeUtc.[Date].DayOfWeek
            While dateIter.DayOfWeek <> week
                dateIter = dateIter.AddDays(1.0)
            End While
            GoTo Label_0324
Label_025B:
            num = StartDateTimeUtc.[Date].Day
            If dateIter.Day <> num Then
                dateIter = New DateTime(dateIter.Year, dateIter.Month, num)
            End If
            GoTo Label_0324
Label_031D:
            dateIter = date1
Label_0324:

            If PeriodMultiple < 1 Then
                PeriodMultiple = 1
            End If

            While CheckEndDate(EndDateUtc_recurrence, dateIter, EndDateUtc_interval)
                If (CheckEndDate(EndDateUtc_recurrence, dateIter, EndDateUtc_interval)) Then
                    DateTimeList.Add(dateIter)
                Else
                    Exit While
                End If

                Select Case Period
                    Case "D"
                        dateIter = dateIter.AddDays(CDbl(PeriodMultiple))
                        Exit Select

                    Case "W"
                        dateIter = dateIter.AddDays(CDbl(7 * PeriodMultiple))
                        Exit Select

                    Case "M"
                        dateIter = dateIter.AddMonths(PeriodMultiple)
                        Exit Select

                    Case "Y"
                        dateIter = dateIter.AddYears(PeriodMultiple)
                        Return DateTimeList
                End Select
            End While

            Return DateTimeList
        End Function

        Function WeeklyRecurrenceEngine(ByVal StartDateTimeUtc As DateTime, ByVal StartDateUTC_interval As DateTime, ByVal EndDateUtc_interval As DateTime,
                             ByVal EndDateUtc_recurrence As DateTime, ByVal DayOfWeekMaskUtc As Integer, ByVal UtcOffset As Integer, ByVal DayOfMonth As Integer,
                             ByVal MonthOfYear As Integer, ByVal PeriodMultiple As Integer, ByVal Period As Char) As ArrayList


            Dim DateTimeList As New ArrayList
            Dim dateIter, cycleIter As DateTime

            Dim dayCycle As DayOfWeek
            dayCycle = DayOfWeek.Monday

            If StartDateTimeUtc.Date > EndDateUtc_interval.Date Then
                cycleIter = Date.MinValue
                Return DateTimeList
            End If
            If StartDateTimeUtc.Date >= StartDateUTC_interval.Date Then
                cycleIter = StartDateTimeUtc.Date
            ElseIf PeriodMultiple > 1 Then
                dateIter = GetFirstDateForPeriodMultiple(Period, PeriodMultiple, StartDateUTC_interval, StartDateTimeUtc)
                If dateIter > EndDateUtc_interval Then
                    Return DateTimeList
                End If
                cycleIter = dateIter
            Else
                cycleIter = StartDateUTC_interval.[Date]
                Dim date1 As DateTime = StartDateTimeUtc.Date
                While Not IsDayOfWeekMatch(cycleIter, DayOfWeekMaskUtc)
                    cycleIter = cycleIter.AddDays(1.0)
                End While
            End If
            Dim friday As DayOfWeek = DayOfWeek.Friday
            'cycleIter = Nothing

            Select Case DayOfWeekMaskUtc
                Case 65 'AllWeekendDays
                    While Not IsWeekendDay(cycleIter)
                        cycleIter = cycleIter.AddDays(1.0)
                    End While
                    Exit Select

                Case 127 'All
                    Exit Select

                Case 0 'None
                    cycleIter = Date.MinValue
                    Return DateTimeList

                Case 62 'AllWeekdays
                    While Not IsWeekDay(cycleIter)
                        cycleIter = cycleIter.AddDays(1.0)
                    End While
                    friday = DayOfWeek.Monday
                    Exit Select
                Case Else

                    While Not IsDayOfWeekMatch(cycleIter, DayOfWeekMaskUtc)
                        cycleIter = cycleIter.AddDays(1.0)
                    End While
                    If (DayOfWeekMaskUtc And 2) > 0 Then
                        friday = DayOfWeek.Monday
                    ElseIf (DayOfWeekMaskUtc And 4) > 0 Then
                        friday = DayOfWeek.Tuesday
                    ElseIf (DayOfWeekMaskUtc And 8) > 0 Then
                        friday = DayOfWeek.Wednesday
                    ElseIf (DayOfWeekMaskUtc And 16) > 0 Then
                        friday = DayOfWeek.Thursday
                    ElseIf (DayOfWeekMaskUtc And 32) > 0 Then
                        friday = DayOfWeek.Friday
                    End If
                    cycleIter = cycleIter
                    While cycleIter.DayOfWeek <> friday
                        cycleIter = cycleIter.AddDays(-1.0)
                    End While
                    Exit Select
            End Select

            dayCycle = friday
            dateIter = cycleIter

            If PeriodMultiple < 1 Then
                PeriodMultiple = 1
            End If

            While CheckEndDate(EndDateUtc_recurrence, dateIter, EndDateUtc_interval)
                If (CheckEndDate(EndDateUtc_recurrence, dateIter, EndDateUtc_interval)) Then
                    DateTimeList.Add(dateIter)
                Else
                    Exit While
                End If
                dateIter = dateIter.AddDays(1.0)

                Select Case DayOfWeekMaskUtc
                    Case 62 'AllWeekdays
                        While Not IsWeekDay(dateIter)
                            dateIter = dateIter.AddDays(1.0)
                        End While
                        Exit Select

                    Case 65 'AllWeekendDays
                        While Not IsWeekendDay(dateIter)
                            dateIter = dateIter.AddDays(1.0)
                        End While
                        Exit Select

                    Case 127 'All
                        Exit Select
                    Case Else

                        While Not IsDayOfWeekMatch(dateIter, DayOfWeekMaskUtc)
                            dateIter = dateIter.AddDays(1.0)
                        End While
                        Exit Select
                End Select
                If (dateIter.DayOfWeek = dayCycle) AndAlso (DayOfWeekMaskUtc <> 127) Then
                    cycleIter = cycleIter.AddDays(CDbl(7 * PeriodMultiple))
                    If DayOfWeekMaskUtc = 62 Then
                        While cycleIter.DayOfWeek <> DayOfWeek.Monday
                            cycleIter = cycleIter.AddDays(-1.0)
                        End While
                    End If
                    dateIter = cycleIter
                End If
            End While

            Return DateTimeList
        End Function

        Function MonthlyRecurrenceEngine(ByVal StartDateTimeUtc As DateTime, ByVal StartDateUTC_interval As DateTime, ByVal EndDateUtc_interval As DateTime,
                             ByVal EndDateUtc_recurrence As DateTime, ByVal DayOfWeekMaskUtc As Integer, ByVal UtcOffset As Integer, ByVal DayOfMonth As Integer,
                             ByVal MonthOfYear As Integer, ByVal PeriodMultiple As Integer, ByVal Period As Char) As ArrayList

            Dim DateTimeList As New ArrayList
            Dim dateIter As DateTime
            Dim time As DateTime = StartDateTimeUtc.AddSeconds(UtcOffset)
            Dim startDateUTC As DateTime = time
            If startDateUTC.Date > EndDateUtc_interval.Date Then
                dateIter = Date.MinValue
                Return DateTimeList
            End If
            If PeriodMultiple <= 1 Then
                startDateUTC = StartDateUTC_interval
                If startDateUTC < time Then
                    startDateUTC = time
                End If
            Else
                While startDateUTC <= EndDateUtc_interval
                    If (startDateUTC >= StartDateUTC_interval.[Date]) AndAlso (startDateUTC <= EndDateUtc_interval.[Date]) Then
                        Exit While
                    End If
                    startDateUTC = startDateUTC.AddMonths(PeriodMultiple)
                End While
                If startDateUTC > EndDateUtc_interval Then
                    dateIter = Date.MinValue
                    Return DateTimeList
                End If
            End If
            startDateUTC = New DateTime(startDateUTC.Year, startDateUTC.Month, 1, startDateUTC.Hour, startDateUTC.Minute, startDateUTC.Second)

            Dim date2 As DateTime = IdoToDate(DayOfMonth, DayOfWeekMaskUtc, startDateUTC.Month, startDateUTC.Year)
            If date2.[Date] < time.[Date] Then
                startDateUTC = startDateUTC.AddMonths(1)
                date2 = IdoToDate(DayOfMonth, DayOfWeekMaskUtc, startDateUTC.Month, startDateUTC.Year)
            End If
            dateIter = New DateTime(date2.Year, date2.Month, date2.Day, time.Hour, time.Minute, 0).AddSeconds(-UtcOffset)

            If PeriodMultiple < 1 Then
                PeriodMultiple = 1
            End If

            While CheckEndDate(EndDateUtc_recurrence, dateIter, EndDateUtc_interval)
                If (CheckEndDate(EndDateUtc_recurrence, dateIter, EndDateUtc_interval)) Then
                    DateTimeList.Add(dateIter)
                Else
                    Exit While
                End If

                Dim date3 As DateTime = dateIter
                date3 = date3.AddSeconds(UtcOffset).AddMonths(PeriodMultiple)
                Dim date4 As DateTime = New DateTime(date3.Year, date3.Month, 1, date3.Hour, date3.Minute, date3.Minute)

                date4 = IdoToDate(DayOfMonth, DayOfWeekMaskUtc, date4.Month, date4.Year)
                dateIter = New DateTime(date4.Year, date4.Month, date4.Day, date3.Hour, date3.Minute, 0).AddSeconds(-UtcOffset)
            End While

            Return DateTimeList
        End Function

        Function YearlyRecurrenceEngine(ByVal StartDateTimeUtc As DateTime, ByVal StartDateUTC_interval As DateTime, ByVal EndDateUtc_interval As DateTime,
                           ByVal EndDateUtc_recurrence As DateTime, ByVal DayOfWeekMaskUtc As Integer, ByVal UtcOffset As Integer, ByVal DayOfMonth As Integer,
                           ByVal MonthOfYear As Integer, ByVal PeriodMultiple As Integer, ByVal Period As Char) As ArrayList

            Dim DateTimeList As New ArrayList
            Dim dateIter As DateTime

            Dim date1 As DateTime = IdoToDate(DayOfMonth, DayOfWeekMaskUtc, MonthOfYear, StartDateUTC_interval.Year)
            If date1 > EndDateUtc_interval Then
                dateIter = Date.MinValue
                Return DateTimeList
            End If
            If date1 >= StartDateUTC_interval.[Date] Then
                dateIter = date1
            Else
                date1 = date1.AddYears(1)
                date1 = IdoToDate(DayOfMonth, DayOfWeekMaskUtc, MonthOfYear, date1.Year)
                dateIter = date1
            End If
            Dim date2 As DateTime = StartDateTimeUtc.AddSeconds(UtcOffset)
            dateIter = New DateTime(dateIter.Year, dateIter.Month, dateIter.Day, date2.Hour, date2.Minute, dateIter.Second).AddSeconds(-UtcOffset)

            While CheckEndDate(EndDateUtc_recurrence, dateIter, EndDateUtc_interval)
                If (CheckEndDate(EndDateUtc_recurrence, dateIter, EndDateUtc_interval)) Then
                    DateTimeList.Add(dateIter)
                Else
                    Exit While
                End If

                dateIter = dateIter.AddYears(1)
                dateIter.AddSeconds(UtcOffset)
                Dim date3 As DateTime = IdoToDate(DayOfMonth, DayOfWeekMaskUtc, MonthOfYear, dateIter.Year)

                date3 = New DateTime(date3.Year, date3.Month, date3.Day, dateIter.Hour, dateIter.Minute, date3.Second)
                dateIter = date3.AddSeconds(-UtcOffset)
            End While

            Return DateTimeList
        End Function

        Public Function listCalendarByRange(ByVal sd As String, ByVal ed As String) As Dictionary(Of String, Object)
            Dim ret As Dictionary(Of String, Object) = New Dictionary(Of String, Object)()

            Try
                Dim ObjOutlook As New COutlook

                ' Convert string dates into DateTime type
                Dim dtSd As DateTime = DateTime.Parse(sd)
                Dim dtEd As DateTime = DateTime.Parse(ed)

                ObjOutlook.StartDateTimeUtc = dtSd.AddMinutes(CCommon.ToDouble(HttpContext.Current.Session("ClientMachineUTCTimeOffset")))
                ObjOutlook.EndDateUtc = dtEd.AddMinutes(CCommon.ToDouble(HttpContext.Current.Session("ClientMachineUTCTimeOffset")))
                ObjOutlook.ResourceName = HttpContext.Current.Session("ResourceIDEmployee")

                Dim dtRecurrence As DataTable
                dtRecurrence = ObjOutlook.getRecurrence()
                dtRecurrence.PrimaryKey = New DataColumn() {dtRecurrence.Columns("RecurrenceID")}

                Dim dtCalandar As New DataTable
                dtCalandar = ObjOutlook.FetchActivity()

                Dim evtList As List(Of Object) = New List(Of Object)

                For Each Row As DataRow In dtCalandar.Rows

                    Dim sAde As Boolean = CCommon.ToBool(Row("AllDayEvent"))
                    Dim evtData(0 To 11) As Object
                    Dim dtSt, dtEt As DateTime
                    Dim StartDateTimeUtc As DateTime = CDate(Row.Item("StartDateTimeUtc"))
                    Dim recurrenceEndDateUtc As DateTime

                    If Row.Item("RecurrenceID") > 0 Then
                        Dim dRowFound As DataRow = dtRecurrence.Rows.Find(CCommon.ToLong(Row.Item("RecurrenceID")))
                        If dRowFound IsNot Nothing Then
                            recurrenceEndDateUtc = CDate(dRowFound.Item("EndDateUtc"))

                            Dim DateTimeList As ArrayList
                            Select Case dRowFound("Period")
                                Case "D"
                                    If (dRowFound("DayOfWeekMaskUtc") <> 127) AndAlso (dRowFound("DayOfWeekMaskUtc") <> 0) Then

                                        DateTimeList = DailyRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                            dRowFound("DayOfWeekMaskUtc"), dRowFound("UtcOffset"), dRowFound("DayOfMonth"),
                                                                            dRowFound("MonthOfYear"), dRowFound("PeriodMultiple"), dRowFound("Period"))

                                    Else
                                        DateTimeList = BasicRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                           dRowFound("DayOfWeekMaskUtc"), dRowFound("UtcOffset"), dRowFound("DayOfMonth"),
                                                                           dRowFound("MonthOfYear"), dRowFound("PeriodMultiple"), dRowFound("Period"))
                                    End If

                                Case "W"
                                    If dRowFound("DayOfWeekMaskUtc") = 0 Then
                                        DateTimeList = BasicRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                          dRowFound("DayOfWeekMaskUtc"), dRowFound("UtcOffset"), dRowFound("DayOfMonth"),
                                                                          dRowFound("MonthOfYear"), dRowFound("PeriodMultiple"), dRowFound("Period"))
                                    Else
                                        DateTimeList = WeeklyRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                          dRowFound("DayOfWeekMaskUtc"), dRowFound("UtcOffset"), dRowFound("DayOfMonth"),
                                                                          dRowFound("MonthOfYear"), dRowFound("PeriodMultiple"), dRowFound("Period"))

                                    End If

                                Case "M"
                                    DateTimeList = MonthlyRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                          dRowFound("DayOfWeekMaskUtc"), dRowFound("UtcOffset"), dRowFound("DayOfMonth"),
                                                                          dRowFound("MonthOfYear"), dRowFound("PeriodMultiple"), dRowFound("Period"))


                                Case "Y"
                                    Dim DayOfMonth, DayOfWeekMaskUtc, MonthOfYear As Integer
                                    DayOfMonth = dRowFound("DayOfMonth")
                                    MonthOfYear = dRowFound("MonthOfYear")
                                    DayOfWeekMaskUtc = dRowFound("DayOfWeekMaskUtc")

                                    If (DayOfMonth = 29) AndAlso (MonthOfYear = 2) Then
                                        DayOfMonth = -5
                                        DayOfWeekMaskUtc = 127
                                    End If

                                    DateTimeList = YearlyRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                        DayOfWeekMaskUtc, dRowFound("UtcOffset"), DayOfMonth,
                                                                        MonthOfYear, dRowFound("PeriodMultiple"), dRowFound("Period"))

                            End Select

                            Dim dtVariance As New DataTable

                            If Not IsDBNull(Row.Item("VarianceID")) Then
                                ObjOutlook.RecurrenceKey = Row.Item("RecurrenceID")
                                ObjOutlook.VarianceKey = Row.Item("VarianceID").ToString

                                dtVariance = ObjOutlook.FetchVariance()
                            End If

                            If DateTimeList IsNot Nothing Then
                                For Each StartDate As DateTime In DateTimeList
                                    dtSt = New DateTime(StartDate.Year, StartDate.Month, StartDate.Day, StartDateTimeUtc.Hour, StartDateTimeUtc.Minute, StartDateTimeUtc.Second).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset") * -1)

                                    If dtVariance.Rows.Count > 0 Then
                                        For Each drVariance As DataRow In dtVariance.Rows
                                            Dim StartDateTimeUtc_variance As DateTime = CDate(drVariance("StartDateTimeUtc")).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset") * -1)

                                            If StartDateTimeUtc_variance.Date = dtSt.Date Then
                                                Dim evtData2(0 To 11) As Object
                                                ' Assign values to evtData object
                                                evtData2(0) = Convert.ToInt32(drVariance("ActivityID")) 'Activity Id
                                                evtData2(1) = IIf(IsDBNull(drVariance.Item("subject")), "", drVariance.Item("subject")) 'Subject
                                                evtData2(2) = StartDateTimeUtc_variance.ToString("MM/dd/yyyy HH:mm") 'Start TIme
                                                evtData2(3) = StartDateTimeUtc_variance.AddSeconds(drVariance.Item("Duration")).ToString("MM/dd/yyyy HH:mm") 'End Time
                                                evtData2(4) = IIf(sAde = True, 1, 0) 'All Day Event
                                                evtData2(5) = 0 'more than one day event
                                                evtData2(6) = 1 'Recurring event
                                                evtData2(7) = drVariance("Color").ToString 'Color
                                                evtData2(8) = 1 'Enable Drag
                                                evtData2(9) = IIf(IsDBNull(drVariance.Item("Location")), "", drVariance.Item("Location")) 'Location
                                                evtData2(10) = String.Empty
                                                evtData2(11) = Convert.ToInt32(Row("ActivityID")) 'Parent Activity Id

                                                evtList.Add(evtData2)

                                                GoTo EndVariance_Loop
                                            End If
                                        Next
                                    End If

                                    dtEt = dtSt.AddSeconds(Row.Item("Duration"))

                                    Dim evtData1(0 To 11) As Object
                                    ' Assign values to evtData object
                                    evtData1(0) = Convert.ToInt32(Row("ActivityID")) 'Activity Id
                                    evtData1(1) = IIf(IsDBNull(Row.Item("subject")), "", Row.Item("subject")) 'Subject
                                    evtData1(2) = dtSt.ToString("MM/dd/yyyy HH:mm") 'Start TIme
                                    evtData1(3) = dtEt.ToString("MM/dd/yyyy HH:mm") 'End Time
                                    evtData1(4) = IIf(sAde = True, 1, 0) 'All Day Event
                                    evtData1(5) = 0 'more than one day event
                                    evtData1(6) = 1 'Recurring event
                                    evtData1(7) = Row("Color").ToString 'Color
                                    evtData1(8) = 0 'Enable Drag
                                    evtData1(9) = IIf(IsDBNull(Row.Item("Location")), "", Row.Item("Location")) 'Location
                                    evtData1(10) = String.Empty
                                    evtData1(11) = Convert.ToInt32(Row("ActivityID")) 'Activity Id

                                    evtList.Add(evtData1)
EndVariance_Loop:

                                Next
                            End If
                        End If
                    Else
                        dtSt = CDate(Row.Item("StartDateTimeUtc")).AddMinutes(CCommon.ToDouble(HttpContext.Current.Session("ClientMachineUTCTimeOffset")) * -1)
                        dtEt = dtSt.AddSeconds(Row.Item("Duration"))

                        ' Assign values to evtData object
                        evtData(0) = Convert.ToInt32(Row("ActivityID")) 'Activity Id
                        evtData(1) = IIf(IsDBNull(Row.Item("subject")), "", Row.Item("subject")) 'Subject
                        evtData(2) = dtSt.ToString("MM/dd/yyyy HH:mm") 'Start TIme
                        evtData(3) = dtEt.ToString("MM/dd/yyyy HH:mm") 'End Time
                        evtData(4) = IIf(sAde = True, 1, 0) 'All Day Event
                        evtData(5) = 0 'more than one day event
                        evtData(6) = 0 'Recurring event
                        evtData(7) = Row("Color").ToString 'Color
                        evtData(8) = 1 'Enable Drag
                        evtData(9) = IIf(IsDBNull(Row.Item("Location")), "", Row.Item("Location")) 'Location
                        evtData(10) = String.Empty
                        evtData(11) = Convert.ToInt32(Row("ActivityID")) 'Activity Id

                        evtList.Add(evtData)
                    End If

                    evtData = Nothing
                Next

                ret.Add("events", evtList)
                ret.Add("issort", True)
                ret.Add("start", dtSd.ToString("MM/dd/yyyy HH:mm"))
                ret.Add("end", dtEd.ToString("MM/dd/yyyy HH:mm"))
                ret.Add("error", Nothing)

            Catch ex As Exception
                ret.Add("IsSuccess", False)
                ret.Add("error", "list calendar by range failed: " + ex.Message)
            End Try

            Return ret

        End Function

        Public Function listCalendar(ByVal day As String, ByVal type As String) As Dictionary(Of String, Object)

            Dim st, et As String
            Dim dtDay As DateTime = DateTime.Parse(day)
            Dim fn As New wdCalendar.Code.Functions

            st = dtDay.ToString("MM/dd/yyyy HH:mm")
            et = dtDay.AddDays(1).AddSeconds(-1).ToString("MM/dd/yyyy HH:mm")

            Select Case type

                Case "month"

                    st = fn.GetFirstDayOfMonth(dtDay).ToString("MM/dd/yyyy HH:mm")
                    et = fn.GetLastDayOfMonth(dtDay).AddDays(1).AddSeconds(-1).ToString("MM/dd/yyyy HH:mm")

                Case "week"

                    st = fn.GetFirstDayOfWeek(dtDay).ToString("MM/dd/yyyy HH:mm")
                    et = fn.GetLastDayOfWeek(dtDay).AddDays(1).AddSeconds(-1).ToString("MM/dd/yyyy HH:mm")

                Case "day"

                    st = dtDay.ToString("MM/dd/yyyy HH:mm")
                    et = dtDay.AddDays(1).AddSeconds(-1).ToString("MM/dd/yyyy HH:mm")

                Case "reminders"

                    st = dtDay.ToString("MM/dd/yyyy HH:mm")
                    et = dtDay.AddDays(2).AddSeconds(-1).ToString("MM/dd/yyyy HH:mm")

            End Select

            Return listCalendarByRange(st, et)

        End Function

        Public Function listCalendarReminders() As Dictionary(Of String, Object)
            Dim ret As Dictionary(Of String, Object) = New Dictionary(Of String, Object)()

            Try
                Dim sd, ed As DateTime

                sd = DateTime.Now
                ed = DateTime.Now.AddDays(1).AddSeconds(-1)

                Dim ObjOutlook As New COutlook

                ' Convert string dates into DateTime type
                Dim dtSd As DateTime = DateTime.Parse(sd)
                Dim dtEd As DateTime = DateTime.Parse(ed)

                ObjOutlook.StartDateTimeUtc = dtSd.AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                ObjOutlook.EndDateUtc = dtEd.AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                ObjOutlook.ResourceName = HttpContext.Current.Session("ResourceId")

                Dim dtRecurrence As DataTable
                dtRecurrence = ObjOutlook.getRecurrence()
                dtRecurrence.PrimaryKey = New DataColumn() {dtRecurrence.Columns("RecurrenceID")}

                Dim dtCalandar As New DataTable
                dtCalandar = ObjOutlook.GetReminder()

                Dim dtReminderList As DataTable = New DataTable


                Dim drRow As DataRow

                dtReminderList.Columns.Add("ActivityId")
                dtReminderList.Columns.Add("Type")
                dtReminderList.Columns.Add("Subject")
                dtReminderList.Columns.Add("Organization")
                dtReminderList.Columns.Add("StartDateTimeUTC", GetType(DateTime))
                dtReminderList.Columns.Add("StartTime")
                dtReminderList.Columns.Add("Location")
                dtReminderList.Columns.Add("RecurrenceID")
                dtReminderList.Columns.Add("bitCurrentEvent")
                dtReminderList.Columns.Add("ReminderDate")


                Dim isCurrentEventSet As Boolean = False

                For Each Row As DataRow In dtCalandar.Rows

                    Dim evtData(0 To 11) As Object
                    Dim dtSt, dtEt As DateTime
                    Dim dtSt1 As DateTime
                    Dim StartDateTimeUtc As DateTime = CDate(Row.Item("StartDateTimeUtc"))
                    Dim LastSnoozDateTimeUtc As DateTime
                    If Not IsDBNull(Row.Item("LastSnoozDateTimeUtc")) Then
                        LastSnoozDateTimeUtc = CDate(Row.Item("LastSnoozDateTimeUtc"))
                    Else
                        LastSnoozDateTimeUtc = DateTime.MinValue
                    End If

                    Dim recurrenceEndDateUtc As DateTime

                    If Row.Item("RecurrenceID") > 0 Then
                        Dim dRowFound As DataRow = dtRecurrence.Rows.Find(CCommon.ToLong(Row.Item("RecurrenceID")))
                        If dRowFound IsNot Nothing Then
                            recurrenceEndDateUtc = CDate(dRowFound.Item("EndDateUtc"))

                            Dim DateTimeList As ArrayList
                            Select Case dRowFound("Period")
                                Case "D"
                                    If (dRowFound("DayOfWeekMaskUtc") <> 127) AndAlso (dRowFound("DayOfWeekMaskUtc") <> 0) Then

                                        DateTimeList = DailyRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                            dRowFound("DayOfWeekMaskUtc"), dRowFound("UtcOffset"), dRowFound("DayOfMonth"),
                                                                            dRowFound("MonthOfYear"), dRowFound("PeriodMultiple"), dRowFound("Period"))

                                    Else
                                        DateTimeList = BasicRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                           dRowFound("DayOfWeekMaskUtc"), dRowFound("UtcOffset"), dRowFound("DayOfMonth"),
                                                                           dRowFound("MonthOfYear"), dRowFound("PeriodMultiple"), dRowFound("Period"))
                                    End If

                                Case "W"
                                    If dRowFound("DayOfWeekMaskUtc") = 0 Then
                                        DateTimeList = BasicRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                          dRowFound("DayOfWeekMaskUtc"), dRowFound("UtcOffset"), dRowFound("DayOfMonth"),
                                                                          dRowFound("MonthOfYear"), dRowFound("PeriodMultiple"), dRowFound("Period"))
                                    Else
                                        DateTimeList = WeeklyRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                          dRowFound("DayOfWeekMaskUtc"), dRowFound("UtcOffset"), dRowFound("DayOfMonth"),
                                                                          dRowFound("MonthOfYear"), dRowFound("PeriodMultiple"), dRowFound("Period"))

                                    End If

                                Case "M"
                                    DateTimeList = MonthlyRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                          dRowFound("DayOfWeekMaskUtc"), dRowFound("UtcOffset"), dRowFound("DayOfMonth"),
                                                                          dRowFound("MonthOfYear"), dRowFound("PeriodMultiple"), dRowFound("Period"))


                                Case "Y"
                                    Dim DayOfMonth, DayOfWeekMaskUtc, MonthOfYear As Integer
                                    DayOfMonth = dRowFound("DayOfMonth")
                                    MonthOfYear = dRowFound("MonthOfYear")
                                    DayOfWeekMaskUtc = dRowFound("DayOfWeekMaskUtc")

                                    If (DayOfMonth = 29) AndAlso (MonthOfYear = 2) Then
                                        DayOfMonth = -5
                                        DayOfWeekMaskUtc = 127
                                    End If

                                    DateTimeList = YearlyRecurrenceEngine(StartDateTimeUtc, ObjOutlook.StartDateTimeUtc, ObjOutlook.EndDateUtc, recurrenceEndDateUtc,
                                                                        DayOfWeekMaskUtc, dRowFound("UtcOffset"), DayOfMonth,
                                                                        MonthOfYear, dRowFound("PeriodMultiple"), dRowFound("Period"))

                            End Select

                            Dim dtVariance As New DataTable

                            If Not IsDBNull(Row.Item("VarianceID")) Then
                                ObjOutlook.RecurrenceKey = Row.Item("RecurrenceID")
                                ObjOutlook.VarianceKey = Row.Item("VarianceID").ToString

                                dtVariance = ObjOutlook.FetchVariance()
                            End If

                            If DateTimeList IsNot Nothing Then
                                For Each StartDate As DateTime In DateTimeList
                                    dtSt1 = New DateTime(StartDate.Year, StartDate.Month, StartDate.Day, StartDateTimeUtc.Hour, StartDateTimeUtc.Minute, StartDateTimeUtc.Second)
                                    dtSt = dtSt1.AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset") * -1)

                                    If dtVariance.Rows.Count > 0 Then
                                        For Each drVariance As DataRow In dtVariance.Rows
                                            Dim StartDateTimeUtc_variance As DateTime = CDate(drVariance("StartDateTimeUtc")).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset") * -1)

                                            If StartDateTimeUtc_variance.Date = dtSt.Date Then
                                                If CCommon.ToBool(drVariance("EnableReminder")) = True AndAlso CDate(drVariance("StartDateTimeUtc")).AddSeconds(-CCommon.ToInteger(drVariance("ReminderInterval"))) < DateTime.UtcNow AndAlso LastSnoozDateTimeUtc.AddMinutes(-1) < DateTime.UtcNow Then
                                                    drRow = dtReminderList.NewRow
                                                    drRow("ActivityId") = Convert.ToInt32(drVariance("ActivityID")) 'Activity Id
                                                    drRow("Type") = 1
                                                    drRow("ReminderDate") = drVariance("ReminderDate")
                                                    drRow("Organization") = drVariance("Organization")
                                                    drRow("Subject") = IIf(IsDBNull(drVariance.Item("subject")), "", drVariance.Item("subject")) 'Subject
                                                    drRow("StartDateTimeUTC") = CDate(drVariance("StartDateTimeUtc"))
                                                    drRow("StartTime") = DateTime.ParseExact(StartDateTimeUtc_variance.ToString("HHmm"), "HHmm", CultureInfo.CurrentCulture).ToString("hh:mm tt") 'Start TIme
                                                    drRow("Location") = IIf(IsDBNull(drVariance.Item("Location")), "", drVariance.Item("Location")) 'Location
                                                    drRow("RecurrenceID") = -999 'Recurrence ID
                                                    If Not isCurrentEventSet Then
                                                        drRow("bitCurrentEvent") = 1
                                                        isCurrentEventSet = True
                                                    Else
                                                        drRow("bitCurrentEvent") = 0
                                                    End If

                                                    dtReminderList.Rows.Add(drRow)
                                                End If
                                                GoTo EndVariance_Loop
                                            End If
                                        Next
                                    End If

                                    Dim LastReminderDateTimeUtc As DateTime = dRowFound("LastReminderDateTimeUtc")

                                    If CCommon.ToBool(Row("EnableReminder")) = True AndAlso dtSt1.AddSeconds(-CCommon.ToInteger(Row("ReminderInterval"))) < DateTime.UtcNow AndAlso LastReminderDateTimeUtc < DateTime.UtcNow AndAlso LastSnoozDateTimeUtc.AddMinutes(-1) < DateTime.UtcNow Then
                                        drRow = dtReminderList.NewRow
                                        drRow("ActivityId") = Convert.ToInt32(Row("ActivityID")) 'Activity Id
                                        drRow("Type") = Convert.ToInt32(Row("numType"))
                                        drRow("ReminderDate") = Row.Item("ReminderDate")
                                        drRow("Organization") = IIf(IsDBNull(Row.Item("Organization")), "", Row.Item("Organization")) 'Organization
                                        drRow("Subject") = IIf(IsDBNull(Row.Item("subject")), "", Row.Item("subject")) 'Subject
                                        drRow("StartDateTimeUTC") = dtSt1
                                        drRow("StartTime") = DateTime.ParseExact(dtSt.ToString("HHmm"), "HHmm", CultureInfo.CurrentCulture).ToString("hh:mm tt")
                                        drRow("Location") = IIf(IsDBNull(Row.Item("Location")), "", Row.Item("Location")) 'Location
                                        drRow("RecurrenceID") = Convert.ToInt32(Row("RecurrenceID")) 'Recurrence ID
                                        If Not isCurrentEventSet Then
                                            drRow("bitCurrentEvent") = 1
                                            isCurrentEventSet = True
                                        Else
                                            drRow("bitCurrentEvent") = 0
                                        End If


                                        dtReminderList.Rows.Add(drRow)
                                    Else
                                        Row("StartDateTimeUTC") = dtSt1
                                    End If
EndVariance_Loop:

                                Next
                            End If
                        End If
                    Else
                        dtSt = CDate(Row.Item("StartDateTimeUtc")).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset") * -1)

                        'WE ARE NOW FETCHING ALL ACTION ITEMS AND CALENDER ITEMS OF CURRENT DATE
                        If CCommon.ToBool(Row("EnableReminder")) = True AndAlso StartDateTimeUtc.AddSeconds(-CCommon.ToInteger(Row("ReminderInterval"))) < DateTime.UtcNow AndAlso LastSnoozDateTimeUtc.AddMinutes(-1) < DateTime.UtcNow Then
                            drRow = dtReminderList.NewRow
                            drRow("ActivityId") = Convert.ToInt32(Row("ActivityID")) 'Activity Id
                            drRow("Type") = Convert.ToInt32(Row("numType"))
                            drRow("ReminderDate") = Row("ReminderDate")
                            drRow("Subject") = IIf(IsDBNull(Row.Item("subject")), "", Row.Item("subject")) 'Subject
                            drRow("Organization") = IIf(IsDBNull(Row.Item("Organization")), "", Row.Item("Organization")) 'Subject
                            drRow("StartDateTimeUTC") = StartDateTimeUtc
                            drRow("StartTime") = DateTime.ParseExact(dtSt.ToString("HHmm"), "HHmm", CultureInfo.CurrentCulture).ToString("hh:mm tt") 'Start TIme
                            drRow("Location") = IIf(IsDBNull(Row.Item("Location")), "", Row.Item("Location")) 'Location
                            drRow("RecurrenceID") = Convert.ToInt32(Row("RecurrenceID")) 'Recurrence ID
                            If Not isCurrentEventSet Then
                                drRow("bitCurrentEvent") = 1
                                isCurrentEventSet = True
                            Else
                                drRow("bitCurrentEvent") = 0
                            End If

                            dtReminderList.Rows.Add(drRow)
                        End If
                    End If
                Next

                If dtReminderList.Rows.Count > 0 Then
                    For Each dr As DataRow In dtCalandar.Rows
                        If dtReminderList.Select("ActivityId=" & Convert.ToInt32(dr("ActivityID"))).Count = 0 Then
                            drRow = dtReminderList.NewRow
                            drRow("ActivityId") = Convert.ToInt32(dr("ActivityID")) 'Activity Id
                            drRow("Type") = Convert.ToInt32(dr("numType"))
                            drRow("ReminderDate") = dr("ReminderDate")
                            drRow("Subject") = IIf(IsDBNull(dr.Item("subject")), "", dr.Item("subject")) 'Subject
                            drRow("Organization") = IIf(IsDBNull(dr.Item("Organization")), "", dr.Item("Organization")) 'Subject
                            drRow("StartDateTimeUTC") = dr("StartDateTimeUTC")
                            drRow("StartTime") = DateTime.ParseExact(CDate(dr.Item("StartDateTimeUtc")).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset") * -1).ToString("HHmm"), "HHmm", CultureInfo.CurrentCulture).ToString("hh:mm tt") 'Start TIme
                            drRow("Location") = IIf(IsDBNull(dr.Item("Location")), "", dr.Item("Location")) 'Location
                            drRow("RecurrenceID") = Convert.ToInt32(dr("RecurrenceID")) 'Recurrence ID
                            drRow("bitCurrentEvent") = 0

                            dtReminderList.Rows.Add(drRow)
                        End If
                    Next


                    Dim dv As DataView
                    dv = dtReminderList.AsDataView()
                    dv.Sort = "StartDateTimeUTC asc"

                    HttpContext.Current.Session("dtReminderList") = dv.ToTable()
                    ret.Add("IsSuccess", True)
                Else
                    ret.Add("IsSuccess", False)
                End If

            Catch ex As Exception
                ret.Add("IsSuccess", False)
            End Try

            Return ret
        End Function


        Public Function updateCalendar(ByVal ActivityID As Integer, ByVal StartDateTimeUtc As DateTime, ByVal EndDateTimeUtc As DateTime) As Dictionary(Of String, Object)

            Dim ret As Dictionary(Of String, Object) = New Dictionary(Of String, Object)()
            Dim sqlReturn As Integer = 0

            Try
                Dim ObjOutlook As New COutlook
                ObjOutlook.ResourceID = HttpContext.Current.Session("ResourceIDEmployee")

                ObjOutlook.ActivityID = ActivityID

                Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                strSplitStartTimeDate = CType(StartDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                strSplitEndTimeDate = CType(EndDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC

                ObjOutlook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                ObjOutlook.StartDateTimeUtc = strSplitStartTimeDate

                ObjOutlook.ModeType = 1

                sqlReturn = ObjOutlook.UpdateActivity()

                ret.Add("IsSuccess", True)
                ret.Add("Msg", "update success")
                ret.Add("Data", sqlReturn.ToString)
            Catch ex As Exception
                ret.Add("IsSuccess", False)
                ret.Add("Msg", "update failed")
                ret.Add("Data", sqlReturn.ToString)
            End Try

            Return ret

        End Function

        Public Function AddVariance(ByVal StartDateTimeUtc As DateTime, ByVal EndDateTimeUtc As DateTime, ByVal subject As String,
                                              ByVal AllDayEvent As Boolean, ByVal ActivityDescription As String, ByVal Location As String, ByVal color As Integer,
                                              ByVal tz As String, ByVal ShowTimeAs As Integer, ByVal Importance As Integer, ByVal EnableReminder As Boolean,
                                              ByVal ReminderInterval As Long, ByVal RecurrenceID As Long, ByVal VarianceID As String) As Dictionary(Of String, Object)

            Dim ret As Dictionary(Of String, Object) = New Dictionary(Of String, Object)()
            Dim sqlReturn As Integer = 0

            Try
                Dim objoutlook As New COutlook
                Dim rootActivityID As Integer = -999
                objoutlook.AllDayEvent = AllDayEvent
                objoutlook.ActivityDescription = ActivityDescription

                Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                strSplitStartTimeDate = CType(StartDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                strSplitEndTimeDate = CType(EndDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC

                objoutlook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                objoutlook.StartDateTimeUtc = strSplitStartTimeDate

                objoutlook.Location = Location
                objoutlook.Subject = subject
                objoutlook.EnableReminder = EnableReminder
                objoutlook.ReminderInterval = ReminderInterval
                objoutlook.ShowTimeAs = ShowTimeAs
                objoutlook.Importance = Importance
                objoutlook.Status = 0
                objoutlook.RecurrenceKey = RecurrenceID

                objoutlook.Color = color
                ' objoutlook.OriginalStartDateTimeUtc = CDate(DateToString(appointment..OriginalStartDateTime))
                objoutlook.ResourceName = HttpContext.Current.Session("ResourceIDEmployee")
                'context.OrganizerName
                'If strDesr.Length = 3 Then
                '    objoutlook.ItemId = strDesr(1)
                '    objoutlook.ChangeKey = strDesr(2)
                'Else
                '    objoutlook.ItemId = " "
                '    objoutlook.ChangeKey = " "
                'End If

                If VarianceID = "0" Then
                    VarianceID = Guid.NewGuid.ToString
                End If
                objoutlook.VarianceKey = VarianceID

                objoutlook.OriginalStartDateTimeUtc = strSplitStartTimeDate


                sqlReturn = objoutlook.AddVarience()

                ret.Add("IsSuccess", True)
                ret.Add("Msg", "update success")
                ret.Add("Data", sqlReturn.ToString)
            Catch ex As Exception
                ret.Add("IsSuccess", False)
                ret.Add("Msg", "update failed")
                ret.Add("Data", sqlReturn.ToString)
            End Try

            Return ret
        End Function

        Public Function UpdateVariance(ByVal ActivityID As Long, ByVal StartDateTimeUtc As DateTime, ByVal EndDateTimeUtc As DateTime, ByVal subject As String,
                                             ByVal AllDayEvent As Boolean, ByVal ActivityDescription As String, ByVal Location As String, ByVal color As Integer,
                                             ByVal tz As String, ByVal ShowTimeAs As Integer, ByVal Importance As Integer, ByVal EnableReminder As Boolean,
                                             ByVal ReminderInterval As Long, ByVal RecurrenceID As Long) As Dictionary(Of String, Object)

            Dim ret As Dictionary(Of String, Object) = New Dictionary(Of String, Object)()
            Dim sqlReturn As Integer = 0

            Try
                Dim RecurringId As Integer = -999

                '' Adding apponitment data to DB
                Dim ObjOutlook As New COutlook
                ObjOutlook.ActivityID = ActivityID
                ObjOutlook.AllDayEvent = AllDayEvent
                ObjOutlook.Location = Location
                ObjOutlook.ActivityDescription = ActivityDescription

                Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                strSplitStartTimeDate = CType(StartDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                strSplitEndTimeDate = CType(EndDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC

                ObjOutlook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                ObjOutlook.StartDateTimeUtc = strSplitStartTimeDate

                ObjOutlook.Subject = subject
                ObjOutlook.EnableReminder = EnableReminder
                ObjOutlook.ReminderInterval = ReminderInterval
                ObjOutlook.ShowTimeAs = ShowTimeAs
                ObjOutlook.Importance = Importance
                ObjOutlook.Status = 0
                ObjOutlook.RecurrenceKey = RecurrenceID
                ObjOutlook.OriginalStartDateTimeUtc = strSplitStartTimeDate

                ObjOutlook.Color = color
                ObjOutlook.UpdateVariance()

                ret.Add("IsSuccess", True)
                ret.Add("Msg", "update success")
                ret.Add("Data", sqlReturn.ToString)
            Catch ex As Exception
                ret.Add("IsSuccess", False)
                ret.Add("Msg", "update failed")
                ret.Add("Data", sqlReturn.ToString)
            End Try

            Return ret
        End Function

        Public Function updateDetailedCalendar(ByVal ActivityID As Long, ByVal StartDateTimeUtc As DateTime, ByVal EndDateTimeUtc As DateTime, ByVal subject As String,
                                               ByVal AllDayEvent As Boolean, ByVal ActivityDescription As String, ByVal Location As String, ByVal color As Integer,
                                               ByVal tz As String, ByVal ShowTimeAs As Integer, ByVal Importance As Integer, ByVal EnableReminder As Boolean,
                                               ByVal ReminderInterval As Long, ByVal RecurrenceID As Long, ByVal VarianceID As String) As Dictionary(Of String, Object)

            Dim ret As Dictionary(Of String, Object) = New Dictionary(Of String, Object)()
            Dim sqlReturn As Integer = 0

            Try
                Dim ObjOutlook As New COutlook
                ObjOutlook.ResourceID = HttpContext.Current.Session("ResourceIDEmployee")

                Dim RecurringId As Integer = -999
                ObjOutlook.ActivityID = ActivityID
                ObjOutlook.AllDayEvent = AllDayEvent
                ObjOutlook.Location = Location
                ObjOutlook.ActivityDescription = ActivityDescription

                Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                strSplitStartTimeDate = CType(StartDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                strSplitEndTimeDate = CType(EndDateTimeUtc, DateTime).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC

                ObjOutlook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                ObjOutlook.StartDateTimeUtc = strSplitStartTimeDate

                ObjOutlook.Subject = subject

                ObjOutlook.EnableReminder = EnableReminder
                ObjOutlook.ReminderInterval = ReminderInterval
                ObjOutlook.ShowTimeAs = ShowTimeAs
                ObjOutlook.Importance = Importance
                ObjOutlook.Status = 0
                ObjOutlook.Color = color
                If RecurrenceID > 0 Then
                    ObjOutlook.RecurrenceKey = RecurrenceID
                Else
                    ObjOutlook.RecurrenceKey = -999
                End If

                ObjOutlook.VarianceKey = VarianceID

                sqlReturn = ObjOutlook.UpdateActivity()

                ret.Add("IsSuccess", True)
                ret.Add("Msg", "update success")
                ret.Add("Data", sqlReturn.ToString)
            Catch ex As Exception
                ret.Add("IsSuccess", False)
                ret.Add("Msg", "update failed")
                ret.Add("Data", sqlReturn.ToString)
            End Try

            Return ret

        End Function

        Public Function removeCalendar(ByVal ActivityID As Long) As Dictionary(Of String, Object)

            Dim ret As Dictionary(Of String, Object) = New Dictionary(Of String, Object)()
            Dim sqlReturn As Integer = 0

            Try

                Dim ObjOutlook As New COutlook
                ObjOutlook.ActivityID = ActivityID
                ObjOutlook.mode = 0
                Dim dtTable As DataTable
                dtTable = ObjOutlook.getActivityByActId()

                If HttpContext.Current.Session("BtoGCalendar") = True Then
                    If dtTable.Rows.Count > 0 Then
                        If Not IsDBNull(dtTable.Rows(0).Item("GoogleEventId")) Then
                            Dim objCalendar As New GoogleCalendar
                            objCalendar.GoogleActivityDelete(dtTable.Rows(0).Item("numContactID"), dtTable.Rows(0).Item("numDomainID"), dtTable.Rows(0).Item("ActivityID"))
                        End If
                    End If
                End If

                sqlReturn = ObjOutlook.RemoveActivity()

                ret.Add("IsSuccess", True)
                ret.Add("Msg", "remove success")
                ret.Add("Data", sqlReturn.ToString)

            Catch ex As Exception

                ret.Add("IsSuccess", False)
                ret.Add("Msg", "remove failed")
                ret.Add("Data", sqlReturn.ToString)

            End Try

            Return ret

        End Function

        Public Class ReminderItem
            Public Property ActivityID As Long
            Public Property Type As Short
            Public Property Subject As String
            Public Property StartDateTimeUTC As DateTime
            Public Property StartTime As String
            Public Property Location As String
            Public Property RecurrenceID As Long
            Public Property bitCurrentEvent As Short
            Public Property ReminderInterval As Int32
        End Class

    End Class


End Namespace


