﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Reminder.aspx.vb" Inherits=".Reminder"
    MasterPageFile="~/common/Popup.Master" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .hs th {
    padding: 7px !important;
}
        .SnoozeButton{
                color: #fff !important;
    background: none;
    background-color: #1e66be;
        border-radius: 0px;
    border: 1px solid;
        }
        input#btnDismiss {
    border-radius: 0px;
    border: 1px solid;
    background: none;
    background-color: #da790b;
    color: #fff;
}
    </style>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }

        $(document).ready(function () {
            var chkBox = $("input[id$='chkAll']");
            chkBox.click(
          function () {
              $("INPUT[type='checkbox']").prop('checked', chkBox.is(':checked'));
          });
            // To deselect CheckAll when a GridView CheckBox        // is unchecked
            $("INPUT[type='checkbox']").click(
        function (e) {
            if (!$(this)[0].checked) {
                chkBox.prop("checked", false);
            }
        });
        });

        function SetTextRemider(id, type, time) {
            window.opener.SetTextRemider(id, type, time);
        }

        function ClearTextRemider() {
            window.opener.ClearTextRemider();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table  cellspacing="0" cellpadding="0"  width="600px">
        <tr>
            <td style="text-align:center; padding:10px; vertical-align:middle">
                <asp:Button ID="btnSnooze" runat="server" CssClass="ImageButton SnoozeButton" Text="Snooze" style="padding-left: 5px; text-align: center " />&nbsp;

                <asp:DropDownList ID="ddSnoozTime" runat="server" style="width:80px !important">
                    <asp:ListItem Value="300">5 minutes</asp:ListItem>
                    <asp:ListItem Value="600">10 minutes</asp:ListItem>
                    <asp:ListItem Value="900">15 minutes</asp:ListItem>
                    <asp:ListItem Value="1200">20 minutes</asp:ListItem>
                    <asp:ListItem Value="1800">30 minutes</asp:ListItem>
                    <asp:ListItem Value="2700">45 minutes</asp:ListItem>
                    <asp:ListItem Value="3600">60 minutes</asp:ListItem>
                </asp:DropDownList>
                
                <asp:Button ID="btnDismiss" runat="server" CssClass="ImageButton" Text="Dismiss" style="float:right;padding-left:5px;" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvReminder" runat="server" AutoGenerateColumns="False" CssClass="dg"
                    Width="100%" DataKeyNames="ActivityId,RecurrenceID,Type,bitCurrentEvent,StartTime,StartDateTimeUtc">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                         <asp:TemplateField>
                            <HeaderTemplate>
                                <b>Organization</b>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Organization")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <b>Subject</b>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Subject")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <b>Type</b>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Location")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Due" ItemStyle-Width="75">
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "ReminderDate")%>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Time" ItemStyle-Width="75">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "StartTime")%>&nbsp;
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/accept.png" style="vertical-align:middle; padding-bottom:2px;" Visible="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No reminder event found.
                    </EmptyDataTemplate>
                </asp:GridView>

            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID ="PageTitle" runat ="server">
    <asp:Label ID="lblTitle" runat="server">Reminder List</asp:Label>
</asp:Content>
