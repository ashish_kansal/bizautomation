﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data
Imports System.Text

Namespace wdCalendar.Code

    Public Class Functions

        Public Function GetFirstDayOfMonth(ByVal dtDate As DateTime) As DateTime

            Dim dtFrom As DateTime = dtDate

            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1))

            Return dtFrom

        End Function

        Public Function GetFirstDayOfMonth(ByVal iMonth As Integer) As DateTime

            Dim dtFrom As DateTime = New DateTime(DateTime.Now.Year, iMonth, 1)

            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1))

            Return dtFrom

        End Function

        Public Function GetLastDayOfMonth(ByVal dtDate As DateTime) As DateTime

            Dim dtTo = dtDate

            dtTo = dtTo.AddMonths(1)

            Return dtTo.AddDays(-(dtTo.Day))

        End Function

        Public Function GetLastDayOfMonth(ByVal iMonth As Integer) As DateTime

            Dim dtTo = New DateTime(DateTime.Now.Year, iMonth, 1)

            dtTo = dtTo.AddMonths(1)

            Return dtTo.AddDays(-(dtTo.Day))

        End Function

        Public Function GetFirstDayOfWeek(ByVal dtDate As DateTime) As DateTime
            ' Monday is the first day of week
            Return dtDate.AddDays(1 - dtDate.DayOfWeek)
        End Function

        Public Function GetLastDayOfWeek(ByVal dtDate As DateTime) As DateTime
            ' Sunday is last day of week
            Return dtDate.AddDays(7 - Convert.ToInt16(dtDate.DayOfWeek))
        End Function

        Public Function ConvertToUnixTimestamp(ByVal dtDate As DateTime) As Integer

            Dim origin As DateTime = New DateTime(1970, 1, 1, 0, 0, 0, 0)
            Dim diff As TimeSpan = dtDate - origin

            Return Convert.ToInt16(diff.TotalSeconds)

        End Function

        Public Function ConvertFromUnixTimestamp(ByVal timestamp As Double) As DateTime

            Dim origin As DateTime = New DateTime(1970, 1, 1, 0, 0, 0, 0)

            Return origin.AddSeconds(timestamp)

        End Function

    End Class

End Namespace


