﻿function cbReminder_Clicked() {
    var reminder = document.getElementById("cbReminder");
    var ddreminder = document.getElementById("ddReminder");
    ddreminder.disabled = !reminder.checked;
}

function IsRecurring_Clicked() {
    var Recurring = document.getElementById("IsRecurring");

    if (Recurring.checked) {
        document.getElementById("trRecurrencePattern").style.display = "";
        document.getElementById("trRangeRecurrence").style.display = "";
    }
    else {
        document.getElementById("trRecurrencePattern").style.display = "none";
        document.getElementById("trRangeRecurrence").style.display = "none";
    }
}

function recurrenceLoad() {
    var Recurring = document.getElementById("IsRecurring");

    if (Recurring.checked) {
        InitializeRecurrencePattern();
        InitializeRangeOfRecurrence();
    }
}

function InitializeRecurrencePattern() {
    //Recurrence Pattern
    var period;
    if (document.getElementById("hfPeriod").value == 'D')
        period = 0;
    else if (document.getElementById("hfPeriod").value == 'W')
        period = 1;
    else if (document.getElementById("hfPeriod").value == 'M')
        period = 2;
    else if (document.getElementById("hfPeriod").value == 'Y')
        period = 3;

    var periodMultiple = document.getElementById("hfPeriodMultiple").value;
    var periodElements = document.getElementsByName("radioList");


    // Setup Default Values
    setDefaultDailyValues();
    setDefaultWeeklyValues();
    setDefaultMonthlyValues();
    setDefaultYearlyValues();

    if ($.browser.msie)
        periodElements[period + 1].checked = true;
    else {
        periodElements[period].checked = true;
        firefoxRadioChanged(periodElements[period]);
    }

    if (period == 0) // Daily
    {
        var daily = document.getElementsByName("dailyRadioList");
        if ($.browser.msie) {
            if (document.getElementById("hfDayOfWeekMaskUtc").value == 62)
                daily[2].checked = true;
            else
                daily[1].checked = true;
        }
        else {
            if (document.getElementById("hfDayOfWeekMaskUtc").value == 62)
                daily[1].checked = true;
            else
                daily[0].checked = true;

        }

        document.getElementById("inputEveryXDays").value = periodMultiple;
    }
    else if (period == 1) // Weekly
    {
        var dayofweeks = document.getElementsByName("dayOfWeekCb");
        var mask = parseInt(document.getElementById("hfDayOfWeekMaskUtc").value);
        while (mask != 0) {
            for (var i = dayofweeks.length - 1; i >= 0; i--) {
                if ((mask - dayofweeks[i].getAttribute("val")) >= 0) {
                    mask = mask - dayofweeks[i].getAttribute("val");
                    dayofweeks[i].checked = true;
                }
            }
        }
        document.getElementById("inputRecursOn").value = periodMultiple;
    }
    else if (period == 2) // Monthly
    {
        var doM = parseInt(document.getElementById("hfDayOfMonth").value);
        if (doM > 0) {
            document.getElementById("input1DayOf").value = doM;
            document.getElementById("input2DayOf").value = periodMultiple;
            if ($.browser.msie)
                document.getElementsByName("monthlyRadioList")[1].checked = true;
            else
                document.getElementsByName("monthlyRadioList")[0].checked = true;
        }
        else {
            if ($.browser.msie)
                document.getElementsByName("monthlyRadioList")[2].checked = true;
            else
                document.getElementsByName("monthlyRadioList")[1].checked = true;
            var domOccurrence = document.getElementById("select1XOf");
            doM *= (-1);
            doM -= 1;
            domOccurrence.options[doM].selected = true;

            var dayofweeks = document.getElementById("select2XOf");
            var mask = parseInt(document.getElementById("hfDayOfWeekMaskUtc").value);
            for (var i = 0; i < dayofweeks.options.length; i++) {
                if (dayofweeks.options[i].getAttribute("val") == mask) {
                    dayofweeks.options[i].selected = true;
                    break;
                }
            }

            document.getElementById("inputXOf").value = periodMultiple
        }
    }
    else // Yearly
    {
        var moY = document.getElementById("hfMonthOfYear").value;
        var doM = parseInt(document.getElementById("hfDayOfMonth").value);
        var index = 0;
        var monthList = document.getElementById("select1DateOf");
        var monthList2 = document.getElementById("select3XOfX");
        for (var i = 0; i < monthList.options.length; i++) {
            if (monthList.options[i].value == moY) {
                index = i;
                break;
            }
        }
        if (doM > 0) {
            document.getElementById("inputDateOf").value = doM;
            monthList.options[index].selected = true;
            if ($.browser.msie)
                document.getElementsByName("yearlyRadioList")[1].checked = true;
            else
                document.getElementsByName("yearlyRadioList")[0].checked = true;
        }
        else {
            var dowMask = document.getElementById("hfDayOfWeekMaskUtc").value;
            var dayList = document.getElementById("select2XOfX");
            for (var i = 0; i < dayList.options.length; i++) {
                if (dayList.options[i].getAttribute("val") == dowMask) {
                    dayList.options[i].selected = true;
                    break;
                }
            }
            monthList2.options[index].selected = true;
            doM *= (-1);
            doM -= 1;
            var domOccurrence = document.getElementById("select1XOfX");
            domOccurrence.options[doM].selected = true;
            if ($.browser.msie)
                document.getElementsByName("yearlyRadioList")[2].checked = true;
            else
                document.getElementsByName("yearlyRadioList")[1].checked = true;

        }
    }
}

function setDefaultDailyValues() {
    var daily = document.getElementsByName("dailyRadioList");
    if (!$.browser.msie)
        daily[0].checked = true;
    else
        daily[1].checked = true;
    document.getElementById("inputEveryXDays").value = document.getElementById("hfPeriodMultiple").value;
}

function setDefaultWeeklyValues() {
    var dayofweeks = document.getElementsByName("dayOfWeekCb");
    var d = new Date((new Date(document.getElementById("hfStartDateTime").value)).getTime());
    var mask = Math.pow(2, d.getDay());
    for (var i = dayofweeks.length - 1; i >= 0; i--) {
        if (mask == dayofweeks[i].getAttribute("val")) {
            dayofweeks[i].checked = true;
            break;
        }
    }
    document.getElementById("inputRecursOn").value = document.getElementById("hfPeriodMultiple").value;
}
function setDefaultMonthlyValues() {
    var d = new Date((new Date(document.getElementById("hfStartDateTime").value)).getTime());
    var day = d.getDay();
    var monthRadioList = document.getElementsByName("monthlyRadioList");
    monthRadioList[monthRadioList.length - 1].checked = true;
    document.getElementById("input1DayOf").value = d.getDate();
    document.getElementById("input2DayOf").value = document.getElementById("hfPeriodMultiple").value;
    var dayDropDown = document.getElementById("select2XOf");
    for (var i = 0; i < dayDropDown.options.length; i++) {
        if (dayDropDown.options[i].value == day) {
            dayDropDown.options[i].selected = true;
            break;
        }
    }
    document.getElementById("inputXOf").value = document.getElementById("hfPeriodMultiple").value;

    var originalMonth = d.getMonth();
    var count = -1;
    while (originalMonth == d.getMonth()) {
        count++;
        d.setDate(d.getDate() - 7);
    }
    var dowOccurrence = document.getElementById("select1XOf");
    dowOccurrence.options[count].selected = true;

}
function setDefaultYearlyValues() {
    var d = new Date((new Date(document.getElementById("hfStartDateTime").value)).getTime());
    var month = d.getMonth();
    var day = d.getDay();
    var yearRadioList = document.getElementsByName("yearlyRadioList");
    if ($.browser.msie)
        yearRadioList[1].checked = true;
    else
        yearRadioList[0].checked = true;
    var monthList = document.getElementById("select1DateOf");
    var monthList2 = document.getElementById("select3XOfX");
    for (var i = 0; i < monthList.options.length; i++) {
        if (monthList.options[i].value == month + 1) {
            monthList.options[i].selected = true;
            monthList2.options[i].selected = true;
            break;
        }
    }
    var dayList = document.getElementById("select2XOfX");
    for (var i = 0; i < dayList.options.length; i++) {
        if (dayList.options[i].value == day) {
            dayList.options[i].selected = true;
            break;
        }
    }
    document.getElementById("inputDateOf").value = d.getDate();
}
function InitializeRangeOfRecurrence() {
    var rangeElements = document.getElementsByName("rangeRadioList");
    var rangeType = 0;
    if (rangeType > 0 || document.getElementById("hfEndDateUtc").value != '12/31/9999')
        rangeType = 2;
    //    if (rangeType > 0 && document.getElementById("hfEndDateUtc").value  == null)
    //        rangeType = 1;

    if ($.browser.msie)
        rangeElements[rangeType + 1].checked = true;
    else
        rangeElements[rangeType].checked = true;
    if (rangeType == 0)
        document.getElementById("inputEndAfter").value = "10";
    else {
        var maxOccur = 0;
        if (maxOccur == 0)
            maxOccur = 10;
        document.getElementById("inputEndAfter").value = maxOccur;
        document.getElementById("inputEndBy").value = document.getElementById("hfEndDateUtc").value;
    }
}


var previouslyCheckedElem = null;
function firefoxRadioChanged(elem) {
    if (previouslyCheckedElem == null)
        previouslyCheckedElem = document.getElementById("radWeekly");
    document.getElementById(previouslyCheckedElem.id + "_div").style.display = "none";
    document.getElementById(elem.id + "_div").style.display = "";
    previouslyCheckedElem = elem;
}

function elem_focus(elem) {
    var id = elem.id;
    id = id.replace("input1", "rad");
    id = id.replace("input2", "rad");
    id = id.replace("input", "rad");
    id = id.replace("select1", "rad");
    id = id.replace("select2", "rad");
    id = id.replace("select3", "rad");
    document.getElementById(id).checked = true;
}

function getPeriodValue() {
    var periodElements = document.getElementsByName("radioList");
    var period;
    var x = 1;
    if (!$.browser.msie)
        x = 0;
    for (var i = x; i < periodElements.length; i++) {
        if (periodElements[i].checked) {
            period = i - x;
            break;
        }
    }
    return period;
}

function getDayOfWeekMask(period) {
    var dayOfWeekMask;
    switch (period) {
        case 0:
            {
                var daily = document.getElementsByName("dailyRadioList");
                if ($.browser.msie)
                    dailyOcurChecked = daily[1].checked;
                else
                    dailyOcurChecked = daily[0].checked;

                if (dailyOcurChecked)
                    dayOfWeekMask = 0;
                else
                    dayOfWeekMask = 62;
                break;
            }
        case 1:
            {
                var dayofweeks = document.getElementsByName("dayOfWeekCb");
                var mask = 0
                for (var i = dayofweeks.length - 1; i >= 0; i--) {
                    if (dayofweeks[i].checked)
                        mask += parseInt(dayofweeks[i].getAttribute("val"));
                }
                dayOfWeekMask = mask;
                break;
            }
        case 2:
            {
                var dayofweeks = document.getElementById("select2XOf");
                dayOfWeekMask = dayofweeks.options[dayofweeks.selectedIndex].getAttribute("val");
                break;
            }
        case 3:
            {
                var dayList = document.getElementById("select2XOfX");
                dayOfWeekMask = dayList.options[dayList.selectedIndex].getAttribute("val");
                break;
            }
    }
    return dayOfWeekMask;

}

function getPeriodMultiple(period) {
    var periodMultiple;
    switch (period) {
        case 0:
            {
                periodMultiple = parseInt(document.getElementById("inputEveryXDays").value);
                break;
            }
        case 1:
            {
                periodMultiple = parseInt(document.getElementById("inputRecursOn").value);
                break;
            }
        case 2:
            {
                var monthly = document.getElementsByName("monthlyRadioList");
                var first = 0;
                if ($.browser.msieE)
                    first = 1;
                if (monthly[first].checked)
                    periodMultiple = parseInt(document.getElementById("input2DayOf").value);
                else
                    periodMultiple = parseInt(document.getElementById("inputXOf").value);
                break;
            }
        case 3:
            {
                break;
            }
    }
    return periodMultiple;
}

function getRangeLimitType() {
    var rangeElements = document.getElementsByName("rangeRadioList");
    var rangeLimit = 1;
    var x = 1;
    if (!$.browser.msie)
        x = 0;
    for (var i = x; i < rangeElements.length; i++) {
        if (rangeElements[i].checked == true) {
            rangeLimit = i;
            if (!$.browser.msie)
                rangeLimit += 1;
            break;
        }
    }
    return rangeLimit;
}

function getMaxOccurrences() {
    var occurrences = parseInt(document.getElementById("inputEndAfter").value);
    if (occurrences < 0 || occurrences.toString().toLowerCase().indexOf("nan") > -1) {
        alert(invalidString);
        return null;
    }
    return occurrences;
}

function validateDayOfMonth(month, dayOfMonth) {
    var date = new Date();
    if (month == 1 && dayOfMonth == 29)
        return true;
    date.setMonth(month);
    date.setDate(dayOfMonth);
    if (date.getMonth() != month) {
        alert(invalidString);
        return false;
    }
    else
        return true;
}

function okClicked() {
    var period = getPeriodValue();

    var setDayOfWeekMask = 0, setperiodMultiple = 0, setDayOfMonth = 0, setMonthOfYear = 0;

    if (period == 0 || period == 1)		// Daily || Weekly
    {
        setDayOfWeekMask = getDayOfWeekMask(period);
        setperiodMultiple = getPeriodMultiple(period);

        document.getElementById("hfPeriod").value = (period == 0 ? "D" : "W");
    }
    else if (period == 2)// Monthly
    {
        var monthly = document.getElementsByName("monthlyRadioList");

        if (!$.browser.msie)
            firstChecked = monthly[0].checked;
        else
            firstChecked = monthly[1].checked;

        if (firstChecked) {
            setDayOfMonth = parseInt(document.getElementById("input1DayOf").value);
            setperiodMultiple = getPeriodMultiple(period);
            setDayOfWeekMask = 0;
        }
        else {
            var domOccurrence = document.getElementById("select1XOf");
            setDayOfMonth = -domOccurrence.options[domOccurrence.selectedIndex].value;

            setDayOfWeekMask = getDayOfWeekMask(period);

            setperiodMultiple = getPeriodMultiple(period);
        }

        document.getElementById("hfPeriod").value = "M";
    }
    else if (period == 3)// Yearly
    {
        var yearly = document.getElementsByName("yearlyRadioList");

        if (!$.browser.msie)
            firstChecked = yearly[0].checked;
        else
            firstChecked = yearly[1].checked;

        if (firstChecked) {
            var dayOfMonth = document.getElementById("inputDateOf").value;
            var monthList = document.getElementById("select1DateOf");
            var month = monthList.options[monthList.selectedIndex].value;
            if (!validateDayOfMonth(month - 1, dayOfMonth))
                return;

            setDayOfMonth = dayOfMonth;
            setMonthOfYear = month;
            setDayOfWeekMask = 0;
        }
        else {
            var monthList = document.getElementById("select3XOfX");
            setMonthOfYear = monthList.options[monthList.selectedIndex].value;

            setDayOfWeekMask = getDayOfWeekMask(period);

            var domOccurrence = document.getElementById("select1XOfX");
            setDayOfMonth = -domOccurrence.options[domOccurrence.selectedIndex].value;
        }

        document.getElementById("hfPeriod").value = "Y";
    }

    var rangeLimit = getRangeLimitType();

    var setEndDate = '9999-12-31 23:59:59.000', setMaxOccurrences = 0;
    if (rangeLimit == 1)// No End Date
    {
        setMaxOccurrences = 0;
    }
    else if (rangeLimit == 2) // Max Occurences
    {
        var occurrences = getMaxOccurrences();
        if (occurrences == null)
            return;

        setMaxOccurrences = occurrences;
    }
    else if (rangeLimit == 3) // EndDate
    {
        setEndDate = document.getElementById("inputEndBy").value;
    }

    document.getElementById("hfEndDateUtc").value = setEndDate;
    document.getElementById("hfDayOfWeekMaskUtc").value = setDayOfWeekMask;
    document.getElementById("hfDayOfMonth").value = setDayOfMonth;
    document.getElementById("hfMonthOfYear").value = setMonthOfYear;

    if (isNaN(setperiodMultiple)) {
        document.getElementById("hfPeriodMultiple").value = 0;
    }
    else {
        document.getElementById("hfPeriodMultiple").value = setperiodMultiple;
    }

}


