﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditCalendar.aspx.vb"
    Inherits=".EditCalendar" ValidateRequest="false" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Event -
        <%=Request("Subject")%></title>
    <link href="jqCalender/main.css" rel="stylesheet" type="text/css" />
    <link href="jqCalender/Error.css" rel="stylesheet" type="text/css" />
    <link href="jqCalender/dropdown.css" rel="stylesheet" type="text/css" />
    <link href="jqCalender/dp.css" rel="stylesheet" type="text/css" />
    <link href="jqCalender/dailog.css" rel="stylesheet" type="text/css" />
    <link href="jqCalender/colorselect.css" rel="stylesheet" type="text/css" />
    <link href="jqCalender/calendar.css" rel="stylesheet" type="text/css" />
    <link href="jqCalender/alert.css" rel="stylesheet" type="text/css" />
    <script src="wdCalendar/src/jquery.js" type="text/javascript"></script>
    <script src="wdCalendar/src/Plugins/Common.js" type="text/javascript"></script>
    <script src="wdCalendar/src/Plugins/jquery.form.js" type="text/javascript"></script>
    <script src="wdCalendar/src/Plugins/jquery.validate.js" type="text/javascript"></script>
    <script src="wdCalendar/src/Plugins/datepicker_lang_US.js" type="text/javascript"></script>
    <script src="wdCalendar/src/Plugins/jquery.datepicker.js" type="text/javascript"></script>
    <script src="wdCalendar/src/Plugins/jquery.dropdown.js" type="text/javascript"></script>
    <script src="wdCalendar/src/Plugins/jquery.colorselect.js" type="text/javascript"></script>
    <script src="wdCalendar/src/jquery.getQueryParam.min.js" type="text/javascript"></script>
    <script src="wdCalendar/src/recurrance.js" type="text/javascript"></script>
    <script type="text/javascript">
        if (!DateAdd || typeof (DateDiff) != "function") {
            var DateAdd = function (interval, number, idate) {
                number = parseInt(number);
                var date;
                if (typeof (idate) == "string") {
                    date = idate.split(/\D/);
                    eval("var date = new Date(" + date.join(",") + ")");
                }
                if (typeof (idate) == "object") {
                    date = new Date(idate.toString());
                }
                switch (interval) {
                    case "y": date.setFullYear(date.getFullYear() + number); break;
                    case "m": date.setMonth(date.getMonth() + number); break;
                    case "d": date.setDate(date.getDate() + number); break;
                    case "w": date.setDate(date.getDate() + 7 * number); break;
                    case "h": date.setHours(date.getHours() + number); break;
                    case "n": date.setMinutes(date.getMinutes() + number); break;
                    case "s": date.setSeconds(date.getSeconds() + number); break;
                    case "l": date.setMilliseconds(date.getMilliseconds() + number); break;
                }
                return date;
            }
        }
        function getHM(date) {
            var hour = date.getHours();
            var minute = date.getMinutes();
            var ret = (hour > 9 ? hour : "0" + hour) + ":" + (minute > 9 ? minute : "0" + minute);
            return ret;
        }
        $(document).ready(function () {
            //debugger;
            var DATA_FEED_URL = "DataFeedCalendar.aspx";
            var arrT = [];
            var tt = "{0}:{1}";
            for (var i = 0; i < 24; i++) {
                arrT.push({ text: StrFormat(tt, [i >= 10 ? i : "0" + i, "00"]) }, { text: StrFormat(tt, [i >= 10 ? i : "0" + i, "30"]) });
            }
            $("#timezone").val(new Date().getTimezoneOffset() / 60 * -1);
            $("#stparttime").dropdown({
                dropheight: 200,
                dropwidth: 60,
                selectedchange: function () { },
                items: arrT
            });
            $("#etparttime").dropdown({
                dropheight: 200,
                dropwidth: 60,
                selectedchange: function () { },
                items: arrT
            });
            var check = $("#IsAllDayEvent").click(function (e) {
                if (this.checked) {
                    $("#stparttime").val("00:00").hide();
                    $("#etparttime").val("00:00").hide();
                }
                else {
                    var d = new Date();
                    var p = 60 - d.getMinutes();
                    if (p > 30) p = p - 30;
                    d = DateAdd("n", p, d);
                    $("#stparttime").val(getHM(d)).show();
                    $("#etparttime").val(getHM(DateAdd("h", 1, d))).show();
                }
            });
            if (check[0].checked) {
                $("#stparttime").val("00:00").hide();
                $("#etparttime").val("00:00").hide();
            }
            $("#hypViewLead").click(function () {
                if ($("#hfMode").val() == "1") {
                    window.close();
                }
                else {
                    CloseModelWindow();
                }
            });
            $("#Savebtn").click(function () {
                var Recurring = document.getElementById("IsRecurring");

                if (Recurring.checked) {
                    okClicked();
                }

                $("#fmEdit").submit();
            });
            $("#Closebtn").click(function () {
                if ($("#hfMode").val() == "1") {
                    window.close();
                }
                else {
                    CloseModelWindow();
                }
            });
            $("#Deletebtn").click(function () {
                var calID = $("#hfActivityID").val();

                if (confirm("Are you sure to remove this event")) {
                    var param = [{ "name": "calendarId", value: calID}];
                    $.post(DATA_FEED_URL + "?method=remove",
                        param,
                        function (data) {
                            if (data.IsSuccess) {
                                if ($("#hfMode").val() == "1") {
                                    window.close();
                                }
                                else {
                                    alert(data.Msg);
                                    CloseModelWindow(null, true);
                                }
                            }
                            else {
                                alert("Error occurs.\r\n" + data.Msg);
                            }
                        }
                    , "json");
                }
            });

            $("#stpartdate,#etpartdate,#inputEndBy").datepicker({ picker: "<button class='calpick'></button>" });
            var cv = $("#colorvalue").val();
            if (cv == "") {
                cv = "-1";
            }
            $("#calendarcolor").colorselect({ title: "Color", index: cv, hiddenid: "colorvalue" });
            //to define parameters of ajaxform
            var options = {
                beforeSubmit: function () {
                    return true;
                },
                dataType: "json",
                success: function (data) {
                    //alert(data.Msg);
                    if (data.IsSuccess) {
                        CloseModelWindow(null, true);
                    }
                }
            };
            $.validator.addMethod("date", function (value, element) {
                var arrs = value.split(i18n.datepicker.dateformat.separator);
                var year = arrs[i18n.datepicker.dateformat.year_index];
                var month = arrs[i18n.datepicker.dateformat.month_index];
                var day = arrs[i18n.datepicker.dateformat.day_index];
                var standvalue = [year, month, day].join("-");
                return this.optional(element) || /^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1,3-9]|1[0-2])[\/\-\.](?:29|30))(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1,3,5,7,8]|1[02])[\/\-\.]31)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])[\/\-\.]0?2[\/\-\.]29)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:16|[2468][048]|[3579][26])00[\/\-\.]0?2[\/\-\.]29)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1-9]|1[0-2])[\/\-\.](?:0?[1-9]|1\d|2[0-8]))(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?:\d{1,3})?)?$/.test(standvalue);
            }, "Invalid date format");
            $.validator.addMethod("time", function (value, element) {
                return this.optional(element) || /^([0-1]?[0-9]|2[0-3]):([0-5][0-9])$/.test(value);
            }, "Invalid time format");
            $.validator.addMethod("safe", function (value, element) {
                return this.optional(element) || /^[^$\<\>]+$/.test(value);
            }, "$<> not allowed");
            $("#fmEdit").validate({
                submitHandler: function (form) { $("#fmEdit").ajaxSubmit(options); },
                errorElement: "div",
                errorClass: "cusErrorPanel",
                errorPlacement: function (error, element) {
                    showerror(error, element);
                }
            });
            function showerror(error, target) {
                var pos = target.position();
                var height = target.height();
                var newpos = { left: pos.left, top: pos.top + height + 2 }
                var form = $("#fmEdit");
                error.appendTo(form).css(newpos);
            }
        });
    </script>
</head>
<body>
    <div>
        <div class="toolBotton">
            <a id="Savebtn" class="imgbtn" href="javascript:void(0);"><span class="Save" title="Save the calendar">
                Save</span></a>
            <%If ActivityID > 0 Then%>
            <a id="Deletebtn" runat="server" class="imgbtn" href="javascript:void(0);"><span class="Delete"
                title="Cancel the calendar">Delete</span></a>
            <%End If%>
            <a id="Closebtn" class="imgbtn" href="javascript:void(0);"><span class="Close" title="Close the window">
                Close</span> </a>
        </div>
        <div style="clear: both">
        </div>
        <div class="infocontainer">
            <form action='DataFeedCalendar.aspx?method=adddetails&Id=<%= ActivityID %>' class="fform"
            id="fmEdit" method="post">
            <label>
                <span>*Subject:</span>
                <div id="calendarcolor">
                </div>
                <input maxlength="200" class="required safe" id="Subject" name="Subject" style="width: 85%;"
                    type="text" value='<%=dtRow("Subject") %>' />
                <input id="colorvalue" name="colorvalue" type="hidden" value='<%=dtRow("Color") %>' />
            </label>
            <%-- <label>
                <span>Lead:</span>
                <asp:HyperLink ID="hypViewLead" runat="server" Text="View Lead" NavigateUrl="#" />
            </label>--%>
            <label>
                <span>*Time: </span>
                <div>
                    <input maxlength="10" class="required date" id="stpartdate" name="stpartdate" style="padding-left: 2px;
                        width: 90px;" type="text" value='<%=sStartDate %>' />
                    <input maxlength="5" class="required time" id="stparttime" name="stparttime" style="width: 40px;"
                        type="text" value='<%=sStartTime %>' />
                    To
                    <input maxlength="10" class="required date" id="etpartdate" name="etpartdate" style="padding-left: 2px;
                        width: 90px;" type="text" value='<%=sEndDate %>' />
                    <input maxlength="50" class="required time" id="etparttime" name="etparttime" style="width: 40px;"
                        type="text" value='<%=sEndTime %>' />
                    <label class="checkp">
                        <input id="IsAllDayEvent" runat="server" name="IsAllDayEvent" type="checkbox" value="1" />All
                        Day Event
                    </label>
                </div>
            </label>
            <label>
                <span>Location:</span>
                <input maxlength="200" id="Location" name="Location" style="width: 95%;" type="text"
                    value='<%=dtRow("Location") %>' />
            </label>
            <label>
                <span>Description:</span>
                <textarea cols="20" id="Description" name="Description" rows="2" style="width: 95%;
                    height: 70px"><%=dtRow("ActivityDescription")%></textarea>
            </label>
            <table width="100%">
                <tr>
                    <td>
                        Show me as:
                        <select id="ddShowTimeAs" name="ddShowTimeAs" runat="server">
                            <option value="0">Free</option>
                            <option value="1">Tentative</option>
                            <option value="2">Out of Office</option>
                            <option value="3">Busy</option>
                        </select>
                    </td>
                    <td>
                        Importance:
                        <select id="ddImportance" name="ddImportance" runat="server">
                            <option value="0">Low</option>
                            <option value="1">Medium</option>
                            <option value="2">High</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="cbReminder" type="checkbox" name="cbReminder" runat="server" onclick="cbReminder_Clicked()" /><label
                            for="cbReminder" style="display: inline">Reminder</label>
                    </td>
                    <td>
                        Reminder Interval:<select id="ddReminder" name="ddReminder" runat="server">
                            <option value="0">0 minutes</option>
                            <option value="300">5 minutes</option>
                            <option value="600">10 minutes</option>
                            <option value="900">15 minutes</option>
                            <option value="1800">30 minutes</option>
                            <option value="3600">1 hour</option>
                            <option value="7200">2 hours</option>
                            <option value="14400">4 hours</option>
                            <option value="28800">8 hours</option>
                            <option value="43200">0.5 days</option>
                            <option value="86400">1 day</option>
                            <option value="172800">2 days</option>
                        </select>
                    </td>
                </tr>
                <tr id="trIsRecurring" runat="server">
                    <td colspan="2">
                        <input id="IsRecurring" runat="server" name="IsRecurring" type="checkbox" onclick="IsRecurring_Clicked()" /><label
                            for="IsRecurring" style="display: inline">Recurrence
                        </label>
                    </td>
                </tr>
                <tr id="trRecurrencePattern">
                    <td valign="top" width="100%" colspan="2">
                        <fieldset style="padding-top: 5px">
                            <legend align="left" class="Fonts">Recurrence pattern </legend>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="border-right: black 1px solid; width: 120px" nowrap valign="top">
                                        <table style="width: 100%" id="radioList" border="0" cellpadding="0" cellspacing="0"
                                            class="Fonts">
                                            <tr>
                                                <td>
                                                    <input id="radDaily" type="radio" value="Daily" name="radioList" onchange="firefoxRadioChanged(this)"
                                                        onpropertychange="radioChanged(this)"><label for="radDaily" style="display: inline">Daily</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="radWeekly" type="radio" value="Weekly" name="radioList" onchange="firefoxRadioChanged(this)"
                                                        onpropertychange="radioChanged(this)" checked><label for="radWeekly" style="display: inline">Weekly</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="radMonthly" type="radio" value="Monthly" name="radioList" onchange="firefoxRadioChanged(this)"
                                                        onpropertychange="radioChanged(this)"><label for="radMonthly" style="display: inline">Monthly</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="radYearly" type="radio" value="Yearly" name="radioList" onchange="firefoxRadioChanged(this)"
                                                        onpropertychange="radioChanged(this)"><label for="radYearly" style="display: inline">Yearly</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="border-left: white 1px solid" valign="top">
                                        <div id="radDaily_div" style="display: none">
                                            <table id="dailyRadioList" border="0" cellpadding="0" cellspacing="0" class="Fonts">
                                                <tr>
                                                    <td nowrap>
                                                        <input id="radEveryXDays" type="radio" value="Every" name="dailyRadioList" onpropertychange="">
                                                        <label for="radEveryXDays" style="display: inline">
                                                            Every</label>
                                                        <input class="Fonts" onfocus="elem_focus(this)" style="width: 37px" type="text" id="inputEveryXDays">
                                                        <label for="radEveryXDays" style="display: inline">
                                                            day(s)</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td nowrap>
                                                        <input id="radEveryWeekDay" type="radio" value="Every WeekDay" name="dailyRadioList"
                                                            onpropertychange="" checked>
                                                        <label for="radEveryWeekDay" style="display: inline">
                                                            Every Weekday</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="radWeekly_div" style="width: 100%">
                                            <table border="0" cellpadding="0" cellspacing="0" class="Fonts" style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <label style="display: inline">
                                                            &nbsp; Recurs every</label>
                                                        <input class="Fonts" style="width: 37px" value="1" type="text" id="inputRecursOn">
                                                        <label style="display: inline">
                                                            week(s) on</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100%">
                                                        <table cellpadding="0" cellspacing="0" class="Fonts" style="width: 100%">
                                                            <tr>
                                                                <td align="left" nowrap>
                                                                    <input val="1" id="cbSunday" type="checkbox" value="Sunday" name="dayOfWeekCb" onpropertychange="">
                                                                    <label for="cbSunday" style="display: inline">
                                                                        Sunday</label>
                                                                </td>
                                                                <td align="left" nowrap>
                                                                    <input val="2" id="cbMonday" type="checkbox" value="Monday" name="dayOfWeekCb" onpropertychange="">
                                                                    <label for="cbMonday" style="display: inline">
                                                                        Monday</label>
                                                                </td>
                                                                <td align="left" nowrap>
                                                                    <input val="4" id="cbTuesday" type="checkbox" value="Tuesday" name="dayOfWeekCb"
                                                                        onpropertychange="">
                                                                    <label for="cbTuesday" style="display: inline">
                                                                        Tuesday</label>
                                                                </td>
                                                                <td align="left" nowrap>
                                                                    <input val="8" id="cbWednesday" type="checkbox" value="Wednesday" name="dayOfWeekCb"
                                                                        onpropertychange="">
                                                                    <label for="cbWednesday" style="display: inline">
                                                                        Wednesday</label>
                                                                </td>
                                                                <td align="right" nowrap>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" nowrap>
                                                                    <input val="16" id="cbThursday" type="checkbox" value="Thursday" name="dayOfWeekCb"
                                                                        onpropertychange="">
                                                                    <label for="cbThursday" style="display: inline">
                                                                        Thursday</label>
                                                                </td>
                                                                <td align="left" nowrap>
                                                                    <input val="32" id="cbFriday" type="checkbox" value="Friday" name="dayOfWeekCb" onpropertychange="">
                                                                    <label for="cbFriday" style="display: inline">
                                                                        Friday</label>
                                                                </td>
                                                                <td align="left" nowrap>
                                                                    <input val="64" id="cbSaturday" type="checkbox" value="Saturday" name="dayOfWeekCb"
                                                                        onpropertychange="">
                                                                    <label for="cbSaturday" style="display: inline">
                                                                        Saturday</label>
                                                                </td>
                                                                <td align="left" nowrap>
                                                                    &nbsp;
                                                                </td>
                                                                <td align="right" nowrap>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="radMonthly_div" style="display: none">
                                            <table id="monthlyRadioList" border="0" cellpadding="0" cellspacing="0" class="Fonts">
                                                <tr>
                                                    <td>
                                                        <input id="radDayOf" type="radio" value="Every" name="monthlyRadioList" onpropertychange="">
                                                        <label for="radDayOf" style="display: inline">
                                                            Day</label>
                                                        <input onfocus="elem_focus(this)" class="Fonts" style="width: 37px" type="text" id="input1DayOf">
                                                        <label for="radDayOf" style="display: inline">
                                                            of every</label>
                                                        <input onfocus="elem_focus(this)" class="Fonts" style="width: 37px" type="text" id="input2DayOf">
                                                        <label for="radDayOf" style="display: inline">
                                                            month(s)</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td nowrap>
                                                        <input id="radXOf" type="radio" value="Every" name="monthlyRadioList" onpropertychange=""
                                                            checked>
                                                        <label for="radXOf" style="display: inline">
                                                            The</label>
                                                        <select id="select1XOf" onfocus="elem_focus(this)" class="Fonts" style="width: 58px">
                                                            <option selected value="1">first</option>
                                                            <option value="2">second</option>
                                                            <option value="3">third</option>
                                                            <option value="4">fourth</option>
                                                            <option value="5">last</option>
                                                        </select>
                                                        <select id="select2XOf" onfocus="elem_focus(this)" class="Fonts" style="width: 90px">
                                                            <option selected val="127">day</option>
                                                            <option val="62">weekday</option>
                                                            <option val="65">weekend day</option>
                                                            <option value="0" val="1">Sunday</option>
                                                            <option value="1" val="2">Monday</option>
                                                            <option value="2" val="4">Tuesday</option>
                                                            <option value="3" val="8">Wednesday</option>
                                                            <option value="4" val="16">Thursday</option>
                                                            <option value="5" val="32">Friday</option>
                                                            <option value="6" val="64">Saturday</option>
                                                        </select>
                                                        <label for="radXOf" style="display: inline">
                                                            of every</label>
                                                        <input onfocus="elem_focus(this)" class="Fonts" style="width: 37px" type="text" id="inputXOf">
                                                        <label for="radXOf" style="display: inline">
                                                            month(s)</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="radYearly_div" style="display: none">
                                            <table id="yearlyRadioList" border="0" cellpadding="0" cellspacing="0" class="Fonts">
                                                <tr>
                                                    <td>
                                                        <input id="radDateOf" type="radio" value="Every" name="yearlyRadioList" onpropertychange=""
                                                            checked>
                                                        <label for="radDateOf" style="display: inline">
                                                            Every</label>
                                                        <select id="select1DateOf" onfocus="elem_focus(this)" class="Fonts" style="width: 80px">
                                                            <option selected value="1">January</option>
                                                            <option value="2">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">August</option>
                                                            <option value="9">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                        <input onfocus="elem_focus(this)" class="Fonts" style="width: 37px" type="text" id="inputDateOf">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td nowrap>
                                                        <input id="radXOfX" type="radio" value="Every" name="yearlyRadioList" onpropertychange="">
                                                        <label for="radXOfX" style="display: inline">
                                                            The</label>
                                                        <select id="select1XOfX" onfocus="elem_focus(this)" class="Fonts" style="width: 58px">
                                                            <option selected value="1">first</option>
                                                            <option value="2">second</option>
                                                            <option value="3">third</option>
                                                            <option value="4">fourth</option>
                                                            <option value="5">last</option>
                                                        </select>
                                                        <select id="select2XOfX" onfocus="elem_focus(this)" class="Fonts" style="width: 90px">
                                                            <option selected val="127">day</option>
                                                            <option val="62">weekday</option>
                                                            <option val="65">weekend day</option>
                                                            <option value="0" val="1">Sunday</option>
                                                            <option value="1" val="2">Monday</option>
                                                            <option value="2" val="4">Tuesday</option>
                                                            <option value="3" val="8">Wednesday</option>
                                                            <option value="4" val="16">Thursday</option>
                                                            <option value="5" val="32">Friday</option>
                                                            <option value="6" val="64">Saturday</option>
                                                        </select>
                                                        <label for="radXOfX" style="display: inline">
                                                            of
                                                        </label>
                                                        <select id="select3XOfX" onfocus="elem_focus(this)" class="Fonts" style="width: 80px">
                                                            <option selected value="1">January</option>
                                                            <option value="2">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">August</option>
                                                            <option value="9">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr id="trRangeRecurrence">
                    <td valign="top" width="100%" colspan="2">
                        <fieldset style="padding-top: 5px">
                            <legend align="left" class="Fonts">Range of recurrence </legend>
                            <div style="width: 100%; height: 100%">
                                <table id="rangeRadioList" cellpadding="0" cellspacing="0" style="width: 100%" class="Fonts">
                                    <tr>
                                        <td align="left" nowrap>
                                            <input id="radNoEndDate" type="radio" name="rangeRadioList" onpropertychange="" checked>
                                            <label for="radNoEndDate" style="display: inline">
                                                No end date</label>
                                        </td>
                                        <td align="left" style="display: none;">
                                            <input id="radEndAfter" type="radio" name="rangeRadioList" onpropertychange="">
                                            <label for="radEndAfter" style="display: inline">
                                                End after:</label>
                                            <input class="Fonts" onfocus="elem_focus(this)" style="width: 37px" type="text" value="10"
                                                id="inputEndAfter">
                                            <label for="radEndAfter" style="display: inline">
                                                occurrences</label>
                                        </td>
                                        <td align="left">
                                            <input id="radEndBy" type="radio" name="rangeRadioList" onpropertychange="">
                                            <label for="radEndBy" style="display: inline">
                                                End by: &nbsp;</label>
                                            <input maxlength="10" id="inputEndBy" name="inputEndBy" style="padding-left: 2px;
                                                width: 90px;" type="text" class="Fonts" onfocus="elem_focus(this)" />
                                        </td>
                                </table>
                            </div>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <input id="timezone" name="timezone" type="hidden" value="" />
            <input id="hfMode" name="hfMode" runat="server" type="hidden" value="" />
            <input id="hfActivityID" name="hfActivityID" runat="server" type="hidden" value="" />
            <input id="hfRecurrenceID" name="hfRecurrenceID" runat="server" type="hidden" value="" />
            <input id="hfVarianceID" name="hfVarianceID" runat="server" type="hidden" value="" />
            <input id="hfRecurrenceSeries" name="hfRecurrenceSeries" runat="server" type="hidden"
                value="" />
            <input id="hfRecurrenceOccurNew" name="hfRecurrenceOccurNew" runat="server" type="hidden"
                value="" />
            <input id="hfEndDateUtc" name="hfEndDateUtc" runat="server" type="hidden" value="" />
            <input id="hfDayOfWeekMaskUtc" name="hfDayOfWeekMaskUtc" runat="server" type="hidden"
                value="" />
            <input id="hfDayOfMonth" name="hfDayOfMonth" runat="server" type="hidden" value="" />
            <input id="hfMonthOfYear" name="hfMonthOfYear" runat="server" type="hidden" value="" />
            <input id="hfPeriodMultiple" name="hfPeriodMultiple" runat="server" type="hidden"
                value="" />
            <input id="hfPeriod" name="hfPeriod" runat="server" type="hidden" value="" />
            <input id="hfStartDateTime" name="hfStartDateTime" runat="server" type="hidden" value="" />
            </form>
            <script type="text/javascript" language="javascript">
                cbReminder_Clicked();
                IsRecurring_Clicked();
                recurrenceLoad();
            </script>
        </div>
    </div>
</body>
</html>
