﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Admin
Imports wdCalender.Code
Public Class Reminder
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("dtReminderList") IsNot Nothing Then
                bindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindData()
        Try
            Dim isEventAvailable As Boolean = False
            Dim dtReminderList As DataTable
            dtReminderList = Session("dtReminderList")

            If Not dtReminderList Is Nothing AndAlso dtReminderList.Rows.Count > 0 Then
                gvReminder.DataSource = dtReminderList
                gvReminder.DataBind()

                For Each row As GridViewRow In gvReminder.Rows
                    If CCommon.ToInteger(gvReminder.DataKeys(row.RowIndex).Values("bitCurrentEvent")) = 1 Then
                        DirectCast(row.FindControl("ImageButton1"), ImageButton).Visible = True
                        row.BackColor = Color.LightYellow
                        isEventAvailable = True
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SetTextRemider", "SetTextRemider(" & gvReminder.DataKeys(row.RowIndex).Values("ActivityId") & "," & gvReminder.DataKeys(row.RowIndex).Values("Type") & ",'" & gvReminder.DataKeys(row.RowIndex).Values("StartTime") & "');", True)
                    ElseIf CCommon.ToSqlDate(gvReminder.DataKeys(row.RowIndex).Values("StartDateTimeUtc")).ToShortDateString() = DateTime.UtcNow.ToShortDateString() Then
                        row.BackColor = Color.GhostWhite
                    End If
                Next

                If Not isEventAvailable Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ClearTextRemider", "ClearTextRemider();", True)
                End If

                Session("dtReminderList") = Nothing
            Else
                Session("dtReminderList") = Nothing
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ClearTextRemider", "ClearTextRemider();", True)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "CloseWindow", "Close();", True)
            End If
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub btnDismiss_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDismiss.Click
        Try
            Dim ObjOutlook As New COutlook

            For Each row As GridViewRow In gvReminder.Rows
                If CCommon.ToInteger(gvReminder.DataKeys(row.RowIndex).Values("bitCurrentEvent")) = 1 Then
                    Dim ActivityId As Long = gvReminder.DataKeys(row.RowIndex).Values("ActivityId")

                    If gvReminder.DataKeys(row.RowIndex).Values("Type") = "1" Then
                        'Calender
                        Dim RecurrenceID As Long = gvReminder.DataKeys(row.RowIndex).Values("RecurrenceID")

                        If RecurrenceID > 0 Then
                            ObjOutlook.LastReminderDateTimeUtc = DateTime.UtcNow.AddDays(1)
                            ObjOutlook.RecurrenceKey = RecurrenceID
                            ObjOutlook.UpdateReminderByReccurence()
                        Else
                            ObjOutlook.ActivityID = ActivityId
                            ObjOutlook.Status = 0
                            ObjOutlook.LastReminderDateTimeUtc = DateTime.UtcNow
                            ObjOutlook.UpdateReminderByActivityId()
                        End If
                    ElseIf gvReminder.DataKeys(row.RowIndex).Values("Type") = "2" Then
                        'Action Item / Communication
                        Dim objActionItem As New ActionItem
                        objActionItem.CommID = ActivityId
                        objActionItem.DismissReminder()
                    End If
                End If
            Next

            Dim df As DataFeedClass = New DataFeedClass
            df.listCalendarReminders()
            bindData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSnooze_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSnooze.Click
        Try
            Dim ObjOutlook As New COutlook

            For Each row As GridViewRow In gvReminder.Rows
                If CCommon.ToInteger(gvReminder.DataKeys(row.RowIndex).Values("bitCurrentEvent")) = 1 Then
                    Dim ActivityId As Long = gvReminder.DataKeys(row.RowIndex).Values("ActivityId")

                    If gvReminder.DataKeys(row.RowIndex).Values("Type") = "1" Then
                        'Calender
                        ObjOutlook.ActivityID = ActivityId
                        ObjOutlook.LastReminderDateTimeUtc = DateTime.UtcNow.AddSeconds(ddSnoozTime.SelectedValue)
                        ObjOutlook.UpdateReminderSnoozeByActivityId()
                    ElseIf gvReminder.DataKeys(row.RowIndex).Values("Type") = "2" Then
                        'Action Item / Communication
                        Dim objActionItem As New ActionItem
                        objActionItem.CommID = ActivityId
                        objActionItem.SnoozeReminder(DateTime.UtcNow.AddSeconds(ddSnoozTime.SelectedValue))
                    End If
                End If
            Next

            Dim df As DataFeedClass = New DataFeedClass
            df.listCalendarReminders()
            bindData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class