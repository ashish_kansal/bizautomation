﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common

Public Class EditCalendar
    Inherits BACRMPage

    Public dtEvents As DataTable = New DataTable
    Public dtRow As DataRow
    Public sStartTime As String
    Public sEndTime As String
    Public sStartDate As String = DateTime.Now.ToShortDateString
    Public sEndDate As String = DateTime.Now.ToShortDateString
    Public ActivityID As Long = 0
    Public ActivityPID As Long = 0
    Public dtSt, dtEt As DateTime

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                GetEventDetail()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub GetEventDetail()
        Try
            If GetQueryStringVal( "isallday") = "1" Then
                IsAllDayEvent.Attributes.Add("checked", "")
            End If

            ActivityID = CCommon.ToLong(GetQueryStringVal( "id"))
            ActivityPID = CCommon.ToLong(GetQueryStringVal( "PId"))

            hfMode.Value = CCommon.ToInteger(GetQueryStringVal( "Mode"))

            If hfMode.Value = 1 Then
                Deletebtn.Visible = False
            End If
            If ActivityID > 0 Then
                Dim ObjOutlook As New COutlook

                If CCommon.ToInteger(GetQueryStringVal( "rs")) = 1 Then
                    ObjOutlook.ActivityID = ActivityPID
                Else
                    ObjOutlook.ActivityID = ActivityID
                End If


                ObjOutlook.mode = True
                dtEvents = ObjOutlook.getActivityByActId()

                If dtEvents.Rows.Count > 0 Then
                    dtRow = dtEvents.Rows(0)

                    If dtRow("EnableReminder") = True Then
                        cbReminder.Attributes.Add("checked", "")
                    End If

                    If dtRow("AllDayEvent") = True Then
                        IsAllDayEvent.Attributes.Add("checked", "")
                    End If

                    If ddShowTimeAs.Items.FindByValue(dtRow("ShowTimeAs")) IsNot Nothing Then
                        ddShowTimeAs.Items.FindByValue(dtRow("ShowTimeAs")).Selected = True
                    End If

                    If ddImportance.Items.FindByValue(dtRow("Importance")) IsNot Nothing Then
                        ddImportance.Items.FindByValue(dtRow("Importance")).Selected = True
                    End If

                    If ddReminder.Items.FindByValue(dtRow("ReminderInterval")) IsNot Nothing Then
                        ddReminder.Items.FindByValue(dtRow("ReminderInterval")).Selected = True
                    End If

                    hfActivityID.Value = dtRow("ActivityID")
                    hfRecurrenceID.Value = dtRow("RecurrenceID")


                    If Not IsDBNull(dtRow("VarianceID")) Then
                        hfVarianceID.Value = dtRow("VarianceID").ToString
                    Else
                        hfVarianceID.Value = "0"
                    End If

                    If CCommon.ToLong(dtRow("RecurrenceID")) > 0 Then
                        hfRecurrenceSeries.Value = CCommon.ToInteger(GetQueryStringVal( "rs"))

                        If CCommon.ToInteger(GetQueryStringVal( "rs")) = 1 Then
                            dtSt = CDate(dtRow("StartDateTimeUtc")).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset") * -1)
                            dtEt = dtSt.AddSeconds(CCommon.ToLong(dtRow("Duration")))

                            IsRecurring.Attributes.Add("checked", "")

                            hfEndDateUtc.Value = CDate(dtRow("EndDateUtc")).ToString("M/dd/yyyy")
                            hfDayOfWeekMaskUtc.Value = dtRow("DayOfWeekMaskUtc")
                            hfDayOfMonth.Value = dtRow("DayOfMonth")
                            hfMonthOfYear.Value = dtRow("MonthOfYear")
                            hfPeriodMultiple.Value = dtRow("PeriodMultiple")
                            hfPeriod.Value = dtRow("Period")
                            hfStartDateTime.Value = dtSt
                        Else
                            Dim StartDate As String = GetQueryStringVal("start").ToString
                            Dim EndDate As String = GetQueryStringVal("end").ToString

                            dtSt = DateTime.Parse(StartDate.Replace("%2F", "/").Replace("%3A", ":"))
                            dtEt = DateTime.Parse(EndDate.Replace("%2F", "/").Replace("%3A", ":"))

                            hfRecurrenceOccurNew.Value = IIf(ActivityID = ActivityPID, 1, 0)
                            trIsRecurring.Attributes.Add("style", "display:none")
                        End If
                    Else
                        dtSt = CDate(dtRow("StartDateTimeUtc")).AddMinutes(HttpContext.Current.Session("ClientMachineUTCTimeOffset") * -1)
                        dtEt = dtSt.AddSeconds(CCommon.ToLong(dtRow("Duration")))
                    End If

                    ActivityID = ObjOutlook.ActivityID
                End If
            Else
                'Dim strDate As String = GetQueryStringVal( "start").ToString
                'Dim dtToday As DateTime = DateTime.Parse(strDate.Replace("%2F", "/").Replace("%3A", ":"))
                Dim StartDate As String = GetQueryStringVal( "start").ToString
                Dim EndDate As String = GetQueryStringVal( "end").ToString

                dtSt = DateTime.Parse(StartDate.Replace("%2F", "/").Replace("%3A", ":"))
                dtEt = DateTime.Parse(EndDate.Replace("%2F", "/").Replace("%3A", ":"))

                dtEvents.Columns.Add("Subject")
                dtEvents.Columns.Add("Color")
                dtEvents.Columns.Add("Location")
                dtEvents.Columns.Add("ActivityDescription")

                dtRow = dtEvents.NewRow
                dtRow("Subject") = If(Request("Subject") Is Nothing, "New Event", Request("Subject"))
                dtRow("Color") = "6"

                hfActivityID.Value = "0"
                hfRecurrenceID.Value = "0"
                hfVarianceID.Value = "0"
            End If

            sStartTime = dtSt.ToString("HH:mm")
            sEndTime = dtEt.ToString("HH:mm")
            sStartDate = dtSt.ToString("M/dd/yyyy")
            sEndDate = dtEt.ToString("M/dd/yyyy")
        Catch ex As Exception
            Throw (ex)
        End Try

    End Sub
End Class
