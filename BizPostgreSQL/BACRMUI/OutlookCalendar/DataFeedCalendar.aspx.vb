﻿Imports Newtonsoft.Json
Imports wdCalendar.Code
Imports wdCalender.Code
Imports BACRM.BusinessLogic.Common

Public Class DataFeedCalendar
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then


            Dim fn As Functions = New Functions
            Dim df As DataFeedClass = New DataFeedClass

            Dim method As String = Request("method")
            Dim json As String = String.Empty

            Response.Clear()
            Response.ContentType = "text/javascript;charset=UTF-8"

            Select Case method

                Case "add"

                    json = JsonConvert.SerializeObject(df.addCalender(Request("CalendarStartTime"), Request("CalendarEndTime"), Request("CalendarTitle"), Request("IsAllDayEvent")), Formatting.None)

                Case "list"

                    json = JsonConvert.SerializeObject(df.listCalendar(Request("showdate"), Request("viewtype")), Formatting.None)

                Case "update"

                    json = JsonConvert.SerializeObject(df.updateCalendar(CCommon.ToLong(Request("calendarID")), Request("CalendarStartTime"), Request("CalendarEndTime")), Formatting.None)

                Case "remove"

                    json = JsonConvert.SerializeObject(df.removeCalendar(CCommon.ToLong(Request("calendarID"))), Formatting.None)

                Case "adddetails"

                    Dim st As String = Request("stpartdate") + " " + Request("stparttime")
                    Dim et As String = Request("etpartdate") + " " + Request("etparttime")
                    Dim ade As Boolean = IIf(Request("IsAllDayEvent") Is Nothing, "0", "1")
                    Dim EnableReminder As Boolean = IIf(Request("cbReminder") Is Nothing, "0", "1")

                    Dim IsRecurring As Boolean = IIf(Request("IsRecurring") Is Nothing, "0", "1")

                    If CCommon.ToLong(Request("id")) > 0 Then
                        Dim RecurrenceID As Long = CCommon.ToLong(Request("hfRecurrenceID"))

                        If RecurrenceID > 0 Then

                            If CCommon.ToInteger(Request("hfRecurrenceSeries")) = 0 Then
                                If CCommon.ToInteger(Request("hfRecurrenceOccurNew")) = 1 Then
                                    json = JsonConvert.SerializeObject(df.AddVariance(st, et, Request("Subject"), ade, Request("Description"), Request("Location"), Request("colorvalue"), Request("timezone"), Request("ddShowTimeAs"), Request("ddImportance"), EnableReminder, Request("ddReminder"), RecurrenceID, Request("hfVarianceID")), Formatting.None)
                                Else
                                    json = JsonConvert.SerializeObject(df.UpdateVariance(CCommon.ToLong(Request("id")), st, et, Request("Subject"), ade, Request("Description"), Request("Location"), Request("colorvalue"), Request("timezone"), Request("ddShowTimeAs"), Request("ddImportance"), EnableReminder, Request("ddReminder"), RecurrenceID), Formatting.None)
                                End If
                            Else
                                df.UpdateRecurrence(RecurrenceID, IIf(IsRecurring, Request("hfEndDateUtc"), Nothing), CCommon.ToInteger(Request("hfDayOfWeekMaskUtc")), CCommon.ToInteger(Request("hfDayOfMonth")), CCommon.ToInteger(Request("hfMonthOfYear")), IIf(IsRecurring, Request("hfPeriod"), ""), CCommon.ToInteger(Request("hfPeriodMultiple")), 0)
                                json = JsonConvert.SerializeObject(df.updateDetailedCalendar(CCommon.ToLong(Request("id")), st, et, Request("Subject"), ade, Request("Description"), Request("Location"), Request("colorvalue"), Request("timezone"), Request("ddShowTimeAs"), Request("ddImportance"), EnableReminder, Request("ddReminder"), RecurrenceID, IIf(Request("hfVarianceID") <> "0", Request("hfVarianceID"), Nothing)), Formatting.None)
                            End If

                        ElseIf IsRecurring Then
                            RecurrenceID = df.AddRecurrence(IIf(IsRecurring, Request("hfEndDateUtc"), Nothing), CCommon.ToInteger(Request("hfDayOfWeekMaskUtc")), CCommon.ToInteger(Request("hfDayOfMonth")), CCommon.ToInteger(Request("hfMonthOfYear")), IIf(IsRecurring, Request("hfPeriod"), ""), CCommon.ToInteger(Request("hfPeriodMultiple")), 0)
                            json = JsonConvert.SerializeObject(df.updateDetailedCalendar(CCommon.ToLong(Request("id")), st, et, Request("Subject"), ade, Request("Description"), Request("Location"), Request("colorvalue"), Request("timezone"), Request("ddShowTimeAs"), Request("ddImportance"), EnableReminder, Request("ddReminder"), RecurrenceID, Nothing), Formatting.None)
                        Else
                            json = JsonConvert.SerializeObject(df.updateDetailedCalendar(CCommon.ToLong(Request("id")), st, et, Request("Subject"), ade, Request("Description"), Request("Location"), Request("colorvalue"), Request("timezone"), Request("ddShowTimeAs"), Request("ddImportance"), EnableReminder, Request("ddReminder"), RecurrenceID, Nothing), Formatting.None)
                        End If

                    Else
                        json = JsonConvert.SerializeObject(df.addDetailedCalendar(st, et, Request("Subject"), ade, Request("Description"), Request("Location"), Request("colorvalue"), Request("timezone"), Request("ddShowTimeAs"), Request("ddImportance"), EnableReminder, Request("ddReminder"),
                                                                                        IsRecurring, IIf(IsRecurring, Request("hfEndDateUtc"), Nothing), CCommon.ToInteger(Request("hfDayOfWeekMaskUtc")), CCommon.ToInteger(Request("hfDayOfMonth")), CCommon.ToInteger(Request("hfMonthOfYear")), IIf(IsRecurring, Request("hfPeriod"), ""), CCommon.ToInteger(Request("hfPeriodMultiple")), 0), Formatting.None)

                    End If
                Case "reminders"

                    json = JsonConvert.SerializeObject(df.listCalendarReminders(), Formatting.None)

            End Select

            Response.Write(json)
            Response.End()

        End If

    End Sub

End Class