﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Outlook

Public Class CalendarList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1)
                
                GetUserRightsForPage(33, 9)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                End If
                Dim objOutlook As New COutlook

                objOutlook.UserCntID = Session("UserContactId")
                objOutlook.DomainId = Session("DomainId")
                objOutlook.TeamType = 1
                objOutlook.GroupId = Session("UserGroupID")
                ddlItems.DataSource = objOutlook.GetUsers
                ddlItems.DataTextField = "Name"
                ddlItems.DataValueField = "resourceId"
                ddlItems.DataBind()
            End If

            If ddlItems.Items.Count > 0 Then
                Dim activeuser As String
                activeuser = ddlItems.SelectedItem.Text
                If activeuser = "Myself" Then activeuser = Session("ContactName").ToString.Trim()

                lblUserName.Text = activeuser

                Session("ResourceIDEmployee") = ddlItems.SelectedItem.Value
            End If

            hdnTimeFormat.Value = Session("CalendarTimeFormat")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class