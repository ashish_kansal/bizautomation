﻿Imports System.Data
Imports BACRM.BusinessLogic.Common

Public Class AddCalendar
    Inherits BACRMPage

    Public sStartTime As String
    Public sEndTime As String
    Public sStartDate As String = DateTime.Now.ToShortDateString
    Public sEndDate As String = DateTime.Now.ToShortDateString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Dim dtToday As DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0)

            sStartDate = dtToday.ToString("M/dd/yyyy")
            sEndDate = dtToday.ToString("M/dd/yyyy")
            sStartTime = dtToday.ToString("HH:mm")
            sEndTime = dtToday.AddHours(1).ToString("HH:mm")

        End If

    End Sub

End Class