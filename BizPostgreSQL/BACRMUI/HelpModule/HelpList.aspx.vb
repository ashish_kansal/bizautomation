﻿Imports System.Data
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Help
Partial Public Class HelpList
    Inherits BACRMPage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                
                BindCategories()
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindData()
        Try
            Dim objhelp As New Help
            Dim objdt As New DataTable
            objhelp.CategoryID = ddlCategories.SelectedValue
            objdt = objhelp.GethelpDataByCategory()
            dgHelp.DataSource = objdt
            dgHelp.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgHelp_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgHelp.ItemCommand
        Try
            If e.CommandName = "Edit" Then Response.Redirect("../HelpModule/HelpMgr.aspx?HelpID=" & e.Item.Cells(0).Text)
            If e.CommandName = "Delete" Then
                Dim objhelp As New Help
                objhelp.HelpID = e.Item.Cells(0).Text
                objhelp.DeleteHelpData()
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgHelp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgHelp.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveOrder.Click
        Try
            Dim objhelp As New Help
            objhelp.StrItems = GetItems()
            objhelp.SaveHelpDisplayOrder()
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Function GetItems() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numHelpID")
            dt.Columns.Add("intDisplayOrder")
            Dim txtbox As New TextBox
            For Each Item As DataGridItem In dgHelp.Items
                txtbox = CType(Item.FindControl("txtOrder"), TextBox)
                If CCommon.ToInteger(txtbox.Text) > 0 Then
                    Dim dr As DataRow = dt.NewRow
                    dr("numHelpID") = Item.Cells(0).Text
                    dr("intDisplayOrder") = txtbox.Text.Trim()
                    dt.Rows.Add(dr)
                End If
            Next
            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "Item"
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub BindCategories()
        Try
            Dim objHelp As New Help
            Dim dtHelpCategories As DataTable
            objHelp.CategoryID = 0
            objHelp.byteMode = 0
            dtHelpCategories = objHelp.GethelpCategories()
            ddlCategories.DataTextField = "vcCatecoryName"
            ddlCategories.DataValueField = "numHelpCategoryID"
            ddlCategories.DataSource = dtHelpCategories
            ddlCategories.DataBind()
            ddlCategories.Items.Insert(0, New ListItem("--Select One--"))
            If CCommon.ToInteger(Session("HelpCategoryID")) > 0 Then
                ddlCategories.ClearSelection()
                ddlCategories.SelectedValue = CCommon.ToInteger(Session("HelpCategoryID"))
            Else
                If ddlCategories.Items.Count > 1 Then
                    ddlCategories.ClearSelection()
                    ddlCategories.SelectedIndex = 1
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlCategories_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategories.SelectedIndexChanged
        Try
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class