﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewCategory.aspx.vb"
    Inherits=".frmNewCategory" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript" language="javascript">
        function Save() {
            if (document.form1.txtCategoryName.value == "") {
                alert("Enter Category Name.")
                document.form1.txtCategoryName.focus()
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" Text="Save &amp; Close" CssClass="button"
                    OnClientClick="javascript:return Save();"></asp:Button>
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                </asp:Button>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblCategory" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
        runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table width="100%" border="0">
                    <tr>
                        <td class="text" align="right">
                            Category Name<font color="#ff0000">*</font>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCategoryName" runat="server" MaxLength="100" CssClass="signup"
                                Width="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right" valign="top">
                        </td>
                        <td>
                            <asp:CheckBox ID="chkLinkPage" runat="server" CssClass="signup" Text="Link Page" />
                            <asp:DropDownList ID="ddlHelp" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="text" align="right">
                            Parent Category
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlParentCategory" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
