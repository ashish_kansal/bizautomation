﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Help
Imports System.Data
Imports System.IO
Imports Infragistics.WebUI.WebHtmlEditor
Imports BACRM.BusinessLogic.Marketing
Imports Telerik.Web.UI

Partial Public Class HelpMgr
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not IsPostBack) Then
            
            BindCategory()
            If GetQueryStringVal( "helpID") <> "" Then
                Dim strmyval As String
                strmyval = GetQueryStringVal( "helpID")
                ViewState("helpID") = GetQueryStringVal( "helpID")
                BindHelp(Convert.ToInt32(strmyval))
                Dim viewImages As String() = New String() {"~/Images"}
                hdnStaticFileHostedOn.Value = ConfigurationManager.AppSettings("StaticFileHostedOn")
            End If
        End If
        'Set Image Manage and Template Manager
        SetRadEditorPath(RadEditor1, CCommon.ToLong(Session("DomainID")), Page)
        'Dim strPath As String = "~/" & ConfigurationManager.AppSettings("PortalDocumentsVirtualDirectory") & "/Help"

    End Sub
    Private Sub SetRadEditorPath(ByVal RadEditor As Telerik.Web.UI.RadEditor, ByVal lngDomainID As Long, ByVal page As System.Web.UI.Page)
        'Dim strPath As String = "~/" & ConfigurationManager.AppSettings("PortalDocumentsVirtualDirectory") & "/" & lngDomainID.ToString
        Dim strPath As String = "~/" & ConfigurationManager.AppSettings("PortalDocumentsVirtualDirectory") & "/Help"
        hdnViewPath.Value = strPath
        'RadEditor.ImageManager.ViewPaths = New String() {strPath}
        'RadEditor.ImageManager.UploadPaths = New String() {strPath}
        'RadEditor.ImageManager.DeletePaths = New String() {strPath}
        RadEditor1.ImageManager.ViewPaths = New String() {strPath}
        RadEditor1.ImageManager.UploadPaths = New String() {strPath}
        RadEditor1.ImageManager.DeletePaths = New String() {strPath}
        RadEditor1.MediaManager.ViewPaths = New String() {strPath}
        RadEditor1.MediaManager.UploadPaths = New String() {strPath}
        RadEditor1.MediaManager.DeletePaths = New String() {strPath}
        RadEditor1.MediaManager.SearchPatterns = New String() {"*.mp4", "*.flv"}
        RadEditor1.MediaManager.MaxUploadFileSize = 50000000

        'RadEditor1.MediaManager.ContentProviderTypeNa
        strPath = "~/" & ConfigurationManager.AppSettings("EMailTemplatesVirtualDirectory")
        'RadEditor.TemplateManager.ViewPaths = New String() {strPath}
        'page.ClientScript.RegisterHiddenField("hdnViewPath", ConfigurationManager.AppSettings("PortalDocumentsVirtualDirectory"))
        'page.ClientScript.RegisterHiddenField("hdnStaticFileHostedOn", ConfigurationManager.AppSettings("StaticFileHostedOn"))
        'Dim sb As New System.Text.StringBuilder
        'sb.AppendLine(" //www.telerik.com/help/aspnet-ajax/onclientpastehtml.html")
        'sb.AppendLine("function OnClientPasteHtml(sender, args) {")
        'sb.AppendLine("var commandName = args.get_commandName();")
        'sb.AppendLine("var value = args.get_value();")
        'sb.AppendLine("if (commandName == ""ImageManager"") {")
        'sb.AppendLine("//See if an img has an alt tag set)")
        'sb.AppendLine("var div = document.createElement(""DIV"");")
        'sb.AppendLine("//Do not use div.innerHTML as in IE this would cause the image's src or the link's href to be converted to absolute path.")
        'sb.AppendLine("//This is a severe IE quirk.")
        'sb.AppendLine("Telerik.Web.UI.Editor.Utils.setElementInnerHtml(div, value);")
        'sb.AppendLine("//Now check if there is alt attribute")
        'sb.AppendLine("var img = div.firstChild;")
        'sb.AppendLine("if (img.src) {")
        'sb.AppendLine("var viewPath = document.getElementById('hdnViewPath').value;")
        'sb.AppendLine("var ImagePath = img.src;")
        'sb.AppendLine("var PathToAppend = ImagePath.substring(ImagePath.indexOf(viewPath) + viewPath.length);")
        'sb.AppendLine("img.setAttribute(""src"", document.getElementById('hdnStaticFileHostedOn').value + PathToAppend);")
        'sb.AppendLine("//Set new content to be pasted into the editor")
        'sb.AppendLine("args.set_value(div.innerHTML);")
        'sb.AppendLine("}")
        'sb.AppendLine("else  { ")
        'sb.AppendLine("var name = args.get_value();")
        'sb.AppendLine("var val = <a id=""player"" style=""display:block;width:520px;height:330px; border:solid 1px;""   href =""http://localhost/BACRMUI/ "" + name></a>';")
        'sb.AppendLine("args.set_value(val);")
        'sb.AppendLine("}")
        'sb.AppendLine("}")
        'sb.AppendLine("}")

        ' page.ClientScript.RegisterClientScriptBlock(page.GetType, "ImageManagerScript", sb.ToString, True)
    End Sub
    Sub BindCategory()
        Try
            Dim objhelp As New Help
            Dim dtCategory As DataTable
            objhelp.CategoryID = 0
            objhelp.byteMode = 1
            dtCategory = objhelp.GethelpCategories()

            ddlCategory.DataSource = dtCategory
            ddlCategory.DataTextField = "vcCatecoryName"
            ddlCategory.DataValueField = "numHelpCategoryID"
            ddlCategory.DataBind()
            ddlCategory.Items.Insert(0, "--Select One--")
            ddlCategory.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindHelp(ByVal ihelpID As Integer)
        Dim objhelp As New Help
        Dim objdt As New DataTable
        objdt = objhelp.GethelpDataByID(ihelpID)
        If (objdt.Rows.Count > 0) Then
            ddlCategory.SelectedValue = objdt.Rows(0)("helpCategory").ToString()
            txttitle.Text = objdt.Rows(0)("helpHeader").ToString()
            txtmetatag.Text = objdt.Rows(0)("helpMetatag").ToString()

            RadEditor1.Content = objdt.Rows(0)("helpDescription").ToString()
            txtpageurl.Text = objdt.Rows(0)("helpPageUrl").ToString()
            txtpageurl.Enabled = False
            txtLinkingPage.Text = objdt.Rows(0)("helpLinkingPageURL").ToString()
            RadEditor1.ImageManager.ViewPaths = New String() {"~/"}
        End If
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim objhelp As New Help
        Try
            Session("HelpCategoryID") = ddlCategory.SelectedValue
            If (Not ViewState("helpID") Is Nothing) Then
                objhelp.HelpID = Convert.ToInt32(ViewState("helpID").ToString())
                objhelp.HelpCategory = CCommon.ToLong(ddlCategory.SelectedValue)
                objhelp.HelpHeader = txttitle.Text.Trim()
                objhelp.HelpMetatag = txtmetatag.Text.Trim()
                objhelp.HelpDescription = RadEditor1.Content.Trim()
                objhelp.HelpLinkingPageURL = txtLinkingPage.Text.Trim
                If (RadEditor1.Text.Length > 400) Then
                    objhelp.HelpShortDescription = RadEditor1.Text.Replace(Char.Parse(vbCr), "").Replace(Char.Parse(vbLf), "").Trim().Substring(0, 399) 'RadEditor1.Content.Substring(0, 200)
                Else
                    objhelp.HelpShortDescription = RadEditor1.Text.Replace(Char.Parse(vbCr), "").Replace(Char.Parse(vbLf), "").Trim()
                End If
                objhelp.HelpPageUrl = RewriteUrl(txtpageurl.Text.Trim())
                If (objhelp.UpdateHelpData()) Then
                    Response.Redirect("~/HelpModule/HelpList.aspx")
                End If
            Else
                objhelp.HelpCategory = CCommon.ToLong(ddlCategory.SelectedValue)
                objhelp.HelpHeader = txttitle.Text.Trim()
                objhelp.HelpMetatag = txtmetatag.Text.Trim()
                objhelp.HelpDescription = RadEditor1.Content.Trim()
                objhelp.HelpPageUrl = RewriteUrl(txtpageurl.Text.Trim())
                objhelp.HelpLinkingPageURL = txtLinkingPage.Text.Trim
                If (RadEditor1.Text.Length > 400) Then
                    objhelp.HelpShortDescription = RadEditor1.Text.Replace(Char.Parse(vbCr), "").Replace(Char.Parse(vbLf), "").Trim().Substring(0, 399) 'RadEditor1.Content.Substring(0, 200)
                Else
                    objhelp.HelpShortDescription = RadEditor1.Text.Replace(Char.Parse(vbCr), "").Replace(Char.Parse(vbLf), "").Trim()
                End If
                If (objhelp.InsertHelpData()) Then
                    Response.Redirect("~/HelpModule/HelpList.aspx")
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Sub clearData()
        txtmetatag.Text = ""
        txtpageurl.Text = ""
        txttitle.Text = ""
        RadEditor1.Content = ""
        txtLinkingPage.Text = ""
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../HelpModule/HelpList.aspx")
    End Sub
    Public Function RewriteUrl(ByVal input As String) As String
        Dim strInput As String = input.Trim().Replace("&", "").Replace("#", "").Replace(" ", "-").Replace("%", "").Replace("&", "").Replace("/", "").Replace("\", "").Replace(":", "").Replace(";", "").Replace(".", "").Replace("^", "").Trim()
        If strInput.StartsWith("-") Then
            strInput = strInput.Substring(1, strInput.Length - 1)
        End If
        Return strInput
    End Function
End Class