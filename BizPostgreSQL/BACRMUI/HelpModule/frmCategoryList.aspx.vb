﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Help
Partial Public Class frmCategoryList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindData()
                
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindData()
        Try
            Dim objHelp As New Help
            Dim dtHelpCategories As DataTable
            objHelp.CategoryID = 0
            objHelp.byteMode = 0
            dtHelpCategories = objHelp.GethelpCategories()
            dgHelpCategories.DataSource = dtHelpCategories
            dgHelpCategories.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub dgHelpCategories_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgHelpCategories.ItemCommand
        Try
            If e.CommandName = "Edit" Then Response.Redirect("../HelpModule/frmNewCategory.aspx?CategoryID=" & e.Item.Cells(0).Text, False)
            Dim objHelp As New Help
            If e.CommandName = "Delete" Then
                objHelp.CategoryID = e.Item.Cells(0).Text
                objHelp.DeleteHelpCategory()
            End If
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub dgHelpCategories_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgHelpCategories.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


End Class