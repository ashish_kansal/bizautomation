﻿Imports System.Data
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Help
Partial Public Class LeftMenu
    Inherits System.Web.UI.UserControl

    Private _helpmode As String
    Public Property HelpMode() As String
        Get
            Return _helpmode
        End Get
        Set(ByVal value As String)
            _helpmode = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If HelpMode = "Admin" Then
                tradmin.Visible = True
                trmain.Visible = False
            ElseIf HelpMode = "MainHelp" Then


                tradmin.Visible = False
                trmain.Visible = True
            Else
                tradmin.Visible = True
                trmain.Visible = True

            End If
            BindContentByAdmin()
            BindContentByMainApp()
        End If

    End Sub
    Sub BindContentByAdmin()
        Dim objhelp As New Help
        Dim objdt As New DataTable
        objdt = objhelp.GethelpDataByCategory("Admin")
        grvadmin.DataSource = objdt
        grvadmin.DataBind()



    End Sub
    Sub BindContentByMainApp()

        Dim objhelp As New Help
        Dim objdt As New DataTable
        objdt = objhelp.GethelpDataByCategory("MainHelp")
        grvmain.DataSource = objdt
        grvmain.DataBind()


    End Sub


    Protected Sub grvadmin_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvadmin.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim lnkClientHelpCategory As HyperLink = CType(e.Row.FindControl("lnkhelp"), HyperLink)
            lnkClientHelpCategory.Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpHeader"))
            If (lnkClientHelpCategory.Text.Length > 15) Then
                lnkClientHelpCategory.ToolTip = lnkClientHelpCategory.Text
                lnkClientHelpCategory.Text = (lnkClientHelpCategory.Text.Substring(0, 15) + "...")
            End If
            lnkClientHelpCategory.NavigateUrl = "~/HelpModule/HelpContent.aspx?pageurl=" & Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpPageUrl"))
            'lnkClientHelpCategory.NavigateUrl = "~/HelpFile/" & Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpPageUrl"))


            'lnkClientHelpCategory.NavigateUrl = ("~/" + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "url")))
            If GetQueryStringVal(Request.QueryString("enc"), "pageurl") <> "" Then
                Dim strmyval As String
                strmyval = GetQueryStringVal(Request.QueryString("enc"), "pageurl")

                If strmyval = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpPageUrl")) Then
                    lnkClientHelpCategory.Font.Bold = True
                End If
            End If

            'If Not (Request.QueryString("pageurl")) Is Nothing Then
            '    If Request.QueryString("pageurl").ToString = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpPageUrl")) Then

            '        lnkClientHelpCategory.Font.Bold = True

            '    End If
            'End If
        End If



    End Sub

    Protected Sub grvmain_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvmain.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim lnkClientHelpCategory As HyperLink = CType(e.Row.FindControl("lnkhelp"), HyperLink)
            lnkClientHelpCategory.Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpHeader"))
            If (lnkClientHelpCategory.Text.Length > 15) Then
                lnkClientHelpCategory.ToolTip = lnkClientHelpCategory.Text
                lnkClientHelpCategory.Text = (lnkClientHelpCategory.Text.Substring(0, 15) + "...")
            End If
            lnkClientHelpCategory.NavigateUrl = "~/HelpModule/HelpContent.aspx?pageurl=" & Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpPageUrl"))
            'lnkClientHelpCategory.NavigateUrl = "~/HelpFile/" & Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpPageUrl"))

            'lnkClientHelpCategory.NavigateUrl = ("~/" + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "url")))


            If GetQueryStringVal(Request.QueryString("enc"), "pageurl") <> "" Then
                Dim strmyval As String
                strmyval = GetQueryStringVal(Request.QueryString("enc"), "pageurl")

                If strmyval = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpPageUrl")) Then
                    lnkClientHelpCategory.Font.Bold = True
                End If
            End If

            'If Not (Request.QueryString("pageurl")) Is Nothing Then
            '    If Request.QueryString("pageurl").ToString = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpPageUrl")) Then
            '        lnkClientHelpCategory.ForeColor = Color.Azure
            '        lnkClientHelpCategory.Font.Bold = True
            '    End If
            'End If
        End If
    End Sub
End Class