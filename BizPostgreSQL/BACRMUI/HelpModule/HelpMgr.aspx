﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HelpMgr.aspx.vb" Inherits=".HelpMgr"
    MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Help Management Page</title>
    <script language="javascript">
        //        function OnClientCommandExecuting(editor, args) {
        //            if (args.get_commandName() == "Add Video") {
        //                var url = prompt("Enter video URL (.FLV format)", "Type you name here");
        //                var strRegex = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
        //                var re = new RegExp(strRegex);
        //                var isValid = re.test(url);
        //                if (isValid == false) {
        //                    alert("Invalid url format");
        //                    args.set_cancel(true);
        //                    return false;
        //                }
        //                var name = args.get_name();
        //                var val = "<a id='player' style='display:block;width:520px;height:330px; border:solid 1px;'   href ='" + url + "'></a>";
        //                if (name == "Add Video") {
        //                    editor.pasteHtml(val);
        //                    //Cancel the further execution of the command as such a command does not exist in the editor command list
        //                    args.set_cancel(true);
        //                }
        //            }

        //        }
        function OnClientPasteHtml(sender, args) {
            var commandName = args.get_commandName();
            var value = args.get_value();
            if (commandName == "ImageManager") {
                //See if an img has an alt tag set)
                var div = document.createElement("DIV");
                //Do not use div.innerHTML as in IE this would cause the image's src or the link's href to be converted to absolute path.
                //This is a severe IE quirk.
                Telerik.Web.UI.Editor.Utils.setElementInnerHtml(div, value);
                //Now check if there is alt attribute
                var img = div.firstChild;
                if (img.src) {
                    var viewPath = document.getElementById('hdnViewPath').value;
                    var ImagePath = img.src;
                    var PathToAppend = ImagePath.substring(ImagePath.indexOf(viewPath) + viewPath.length + 1);
                    img.setAttribute("src", document.getElementById('hdnStaticFileHostedOn').value + PathToAppend);
                    //Set new content to be pasted into the editor
                    args.set_value(div.innerHTML);
                }
            }
            else if (commandName = "MediaManager") {
                var div = document.createElement("DIV");
                Telerik.Web.UI.Editor.Utils.setElementInnerHtml(div, value);
                var img = div.firstChild;
                var name = args.get_name();
                var url = document.getElementById('hdnStaticFileHostedOn').value + img.firstChild.attributes.value.nodeValue;
                url = url.replace("Portaldoc/", "");
                var val = "<a id='player' style='display:block;width:520px;height:330px; border:solid 1px black;'   href ='" + url + "'></a>";
                args.set_value(val);

            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Help Management Page
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div>
        <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
            runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <br>
                    <asp:HiddenField ID="hdvalue" runat="server" />
                    <table width="100%" border="0">
                        <tr>
                            <td>
                            </td>
                            <td class="text" align="right">
                                <asp:Button ID="btnSubmit" runat="server" Text="Save" CssClass="button" />&nbsp;<asp:Button
                                    ID="btnCancel" runat="server" Text="Close" CssClass="button" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Category: <font color="#ff0000">*</font>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCategory" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Title: <font color="#ff0000">*</font>
                            </td>
                            <td>
                                <asp:TextBox ID="txttitle" runat="server" CssClass="signup" Width="400"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                MetaTag: <font color="#ff0000">*</font>
                            </td>
                            <td>
                                <asp:TextBox ID="txtmetatag" runat="server" CssClass="signup" Width="400"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Page Url: <font color="#ff0000">*</font>
                            </td>
                            <td>
                                <asp:TextBox ID="txtpageurl" runat="server" CssClass="signup" Width="400"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Linking Page URL: <font color="#ff0000">*</font>
                            </td>
                            <td class="text">
                                <asp:TextBox ID="txtLinkingPage" runat="server" CssClass="signup" Width="400"></asp:TextBox>
                                e.g.frmABC.aspx
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right" valign="top">
                                Description Of Page
                            </td>
                            <td colspan="3">
                                <telerik:RadEditor ID="RadEditor1" runat="server" Height="515px" Width="670px" ToolsFile="EditorTools.xml"
                                    OnClientPasteHtml="OnClientPasteHtml">
                                    <content>
                                    </content>
                                </telerik:RadEditor>
                                <%--  <telerik:RadEditor runat="server" ID="RadEditor2" ToolbarMode="Default" Height="515"   OnClientPasteHtml="OnClientPasteHtml" OnClientCommandExecuting="OnClientCommandExecuting" >
                            <ImageManager ViewPaths="~/">
                            </ImageManager>
                        </telerik:RadEditor>--%>
                            </td>
                        </tr>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
    <asp:HiddenField ID="hdnStaticFileHostedOn" runat="server" />
    <asp:HiddenField ID="hdnViewPath" runat="server" />
</asp:Content>
