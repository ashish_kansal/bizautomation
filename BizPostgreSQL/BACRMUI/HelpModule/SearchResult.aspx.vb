﻿Imports System.Data
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Help
Partial Public Class SearchResult
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If GetQueryStringVal(Request.QueryString("enc"), "strdata") <> "" Then
                Dim strmyval As String
                strmyval = GetQueryStringVal(Request.QueryString("enc"), "strdata")
                BindResult(strmyval)




            End If
            'If (Not Request.QueryString("strdata") Is Nothing) Then
            '    BindResult()
            'End If
        End If
    End Sub
    Sub BindResult(ByVal strdata As String)
        Dim objHelp As New Help
        Dim objdata As New DataTable

        objdata = objHelp.GethelpDataBysearch(strdata)
        If (objdata.Rows.Count > 0) Then
            grvdataresult.DataSource = objdata
            grvdataresult.DataBind()
        Else
            grvdataresult.EmptyDataText = "No search Record"

        End If

    End Sub

    Protected Sub grvdataresult_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvdataresult.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim lnkClientHelpCategory As HyperLink = CType(e.Row.FindControl("lnkhelp"), HyperLink)
            Dim lbldescription As Label = CType(e.Row.FindControl("lbldesc"), Label)
            lnkClientHelpCategory.Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpHeader"))
            lbldescription.Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpshortdesc"))
            If (lnkClientHelpCategory.Text.Length > 100) Then
                lnkClientHelpCategory.ToolTip = lnkClientHelpCategory.Text
                lnkClientHelpCategory.Text = (lnkClientHelpCategory.Text.Substring(0, 100) + "...")
            End If
            lnkClientHelpCategory.NavigateUrl = "~/HelpModule/HelpContent.aspx?pageurl=" & Convert.ToString(DataBinder.Eval(e.Row.DataItem, "helpPageUrl"))
        End If
    End Sub

End Class