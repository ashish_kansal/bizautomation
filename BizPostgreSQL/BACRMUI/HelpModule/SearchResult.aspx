﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SearchResult.aspx.vb" Inherits=".SearchResult" %>
<%@ Register src="Control/HelpSearch.ascx" tagname="HelpSearch" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SearchResult Page</title>
    <link id="lnkStyleSheet" runat="server" href="~/HelpModule/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="100%">
            <tr>
                <td>
                    
                    <uc2:HelpSearch ID="HelpSearch1" runat="server" />
                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="grvdataresult" runat="server"  CssClass="GridStart"  Width="100%" ShowHeader="false" AutoGenerateColumns="false" EmptyDataText="No search record found"
                         DataKeyNames="helpID">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <img src="~/images/bullet01.gif" runat="server" id="imgclienthelp" align="absmiddle"
                                        alt="" width="19" height="20" />
                                    <asp:HyperLink ID="lnkhelp" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "helpHeader")%>'>HelpTitle</asp:HyperLink>
                                    <br />
                                    <asp:Label ID="lbldesc" runat="server"></asp:Label>
                                    
                                    
                                    
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
