﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HelpList.aspx.vb" Inherits=".HelpList "
    MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Help List Page</title>
    <script type="text/javascript" language="javascript">

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>Categories:</label>
                    <asp:DropDownList runat="server" ID="ddlCategories" CssClass="form-control" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="pull-right">
                <asp:HyperLink ID="hplCategories" runat="server" CssClass="btn btn-primary" NavigateUrl="~/HelpModule/frmCategoryList.aspx">Manage Help Categories</asp:HyperLink>
                <asp:HyperLink ID="hplNew" runat="server" CssClass="btn btn-primary" NavigateUrl="~/HelpModule/HelpMgr.aspx">Add Help Page</asp:HyperLink>
                <asp:Button ID="btnSaveOrder" runat="server" CssClass="btn btn-primary" Text="Save Display Order"></asp:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Help Pages
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:DataGrid ID="dgHelp" AllowSorting="false" runat="server" Width="100%" UseAccessibleHeader="true" CssClass="table table-bordered table-striped" AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundColumn Visible="false" DataField="numHelpID" HeaderText="numHelpID"></asp:BoundColumn>
                                <asp:BoundColumn DataField="row" HeaderText="" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="helpHeader" SortExpression="" HeaderText="Title" CommandName="Edit"></asp:ButtonColumn>
                                <asp:BoundColumn DataField="vcCatecoryName" SortExpression="" HeaderText="Category"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Display Order">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtOrder" runat="server" CssClass="signup" Width="20" Text='<%# Eval("intDisplayOrder") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                                    <ItemTemplate>
                                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" Text="X" CommandName="Delete"></asp:Button>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="dgHelp" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
