﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Help
Partial Public Class frmNewCategory
    Inherits BACRMPage
    Dim lngCategoryID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GetQueryStringVal( "CategoryID") <> "" Then
                lngCategoryID = GetQueryStringVal( "CategoryID")
            End If
            If Not IsPostBack Then
                
                LoadDropDowns()
                If lngCategoryID > 0 Then
                    LoadDetails()
                    lblCategory.Text = "Edit Category"
                Else
                    lblCategory.Text = "New Category"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDropDowns()
        Try
            Dim objhelp As New Help
            Dim dt As New DataTable
            dt = objhelp.GethelpDataByCategory()
            ddlHelp.DataSource = dt
            ddlHelp.DataTextField = "helpHeader"
            ddlHelp.DataValueField = "numHelpID"
            ddlHelp.DataBind()
            ddlHelp.Items.Insert(0, "--Select One--")
            ddlHelp.Items.FindByText("--Select One--").Value = 0

            Dim dtCategory As DataTable
            objhelp.CategoryID = lngCategoryID
            objhelp.byteMode = 1
            dtCategory = objhelp.GethelpCategories()

            ddlParentCategory.DataSource = dtCategory
            ddlParentCategory.DataTextField = "vcCatecoryName"
            ddlParentCategory.DataValueField = "numHelpCategoryID"
            ddlParentCategory.DataBind()
            ddlParentCategory.Items.Insert(0, "--Select One--")
            ddlParentCategory.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objCategory As New Help
        Try
            objCategory.CategoryID = lngCategoryID
            objCategory.CategoryName = txtCategoryName.Text.Trim()
            objCategory.LinkPage = chkLinkPage.Checked
            objCategory.HelpID = ddlHelp.SelectedValue
            objCategory.ParentCatID = ddlParentCategory.SelectedValue
            objCategory.Level = 0

            If objCategory.ManageHelpCategory() Then
                Response.Redirect("../HelpModule/frmCategoryList.aspx", False)
            Else
                litMessage.Text = "Category name already exists."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../HelpModule/frmCategoryList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objCategory As New Help
            Dim dtCategory As DataTable
            objCategory.CategoryID = lngCategoryID
            objCategory.byteMode = 0
            dtCategory = objCategory.GethelpCategories()
            txtCategoryName.Text = dtCategory.Rows(0)("vcCatecoryName")
            chkLinkPage.Checked = dtCategory.Rows(0)("bitLinkPage")
            If Not IsDBNull(dtCategory.Rows(0)("numPageID")) Then
                If Not ddlHelp.Items.FindByValue(dtCategory.Rows(0)("numPageID")) Is Nothing Then
                    ddlHelp.Items.FindByValue(dtCategory.Rows(0)("numPageID")).Selected = True
                End If
            End If
            If Not IsDBNull(dtCategory.Rows(0)("numParentCatID")) Then
                If Not ddlParentCategory.Items.FindByValue(dtCategory.Rows(0)("numParentCatID")) Is Nothing Then
                    ddlParentCategory.Items.FindByValue(dtCategory.Rows(0)("numParentCatID")).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


End Class