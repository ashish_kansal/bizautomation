﻿Public Class Globals



    Public Shared Function FormatQueryString(ByVal Value As String) As String

        Value = Value.Replace("%20", " ")
        Value = Value.Replace("%22", """")
        Value = Value.Replace("%3C", "<")
        Value = Value.Replace("%3E", ">")
        Value = Value.Replace("%27", "'")
        Value = Value.Replace("%23", "#")
        Return Value
    End Function

End Class
