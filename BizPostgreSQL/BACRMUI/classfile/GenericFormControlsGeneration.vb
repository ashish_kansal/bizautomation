Imports System.Text
Imports System.IO
Imports System.Web.Mail
Imports System.Web.UI.WebControls
Imports BACRM.BusinessLogic.Admin
Imports System.Reflection
Imports BACRM.BusinessLogic.Common

Module GenericFormControlsGeneration

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Represents the DomainID.
    ''' </summary>
    ''' <remarks>
    '''     This holds the domain id.
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/09/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private _DomainID As Long
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the Domain id.
    ''' </summary>
    ''' <value>Returns the domain id as long.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the Domain id. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/09/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Property DomainID() As Long
        Get
            Return _DomainID
        End Get
        Set(ByVal Value As Long)
            _DomainID = Value
        End Set
    End Property
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Represents the userID.
    ''' </summary>
    ''' <remarks>
    '''     This holds the user id.
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/19/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private _UserCntID As Long
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the User id.
    ''' </summary>
    ''' <value>Returns the user id as long.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the User id. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/19/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Property UserCntID() As Long
        Get
            Return _UserCntID
        End Get
        Set(ByVal Value As Long)
            _UserCntID = Value
        End Set
    End Property
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Represents the Authentication Group ID.
    ''' </summary>
    ''' <remarks>
    '''     This holds the user Group id.
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/30/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private _AuthGroupId As Long
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the User's authenticaiton group id.
    ''' </summary>
    ''' <value>Returns the user authentication group id as long.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the User authentiation group id. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/30/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Property AuthGroupId() As Long
        Get
            Return _AuthGroupId
        End Get
        Set(ByVal Value As Long)
            _AuthGroupId = Value
        End Set
    End Property
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Represents the form id.
    ''' </summary>
    ''' <remarks>
    '''     This holds the form id.
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/09/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private _FormID As Long
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the Domain id.
    ''' </summary>
    ''' <value>Returns the domain id as long.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the Domain id. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/09/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Property FormID() As Long
        Get
            Return _FormID
        End Get
        Set(ByVal Value As Long)
            _FormID = Value
        End Set
    End Property
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the sXMLFilePath whihc is path to the xml file.
    ''' </summary>
    ''' <value>Returns the sXMLFilePath as String.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the sXMLFilePath. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private _sXMLFilePath As String
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the sXMLFilePath whihc is path to the xml file.
    ''' </summary>
    ''' <value>Returns the sXMLFilePath as String.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the sXMLFilePath. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Property sXMLFilePath() As String
        Get
            Return _sXMLFilePath
        End Get
        Set(ByVal Value As String)
            _sXMLFilePath = Value
        End Set
    End Property
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Represents the indicator for AOI field.
    ''' </summary>
    ''' <remarks>
    '''     This holds the flag to the AOI field.
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/09/2005	Created
    ''' </history>
    Private _boolAOIField As Int16
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the boolAOIField which indicates if the field is a AOI or not.
    ''' </summary>
    ''' <value>Returns the boolAOIField as Boolean.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the boolAOIField. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Property boolAOIField() As Int16
        Get
            Return _boolAOIField
        End Get
        Set(ByVal Value As Int16)
            _boolAOIField = Value
        End Set
    End Property
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This subroutine finds the controls on the form by parsing the xml file which is created from BizForm Wizard
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Function getFormControlConfig() As DataTable
        Dim dtFormConfig As DataTable                                                       'declare the datatable which will contain the form config
        Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
        objConfigWizard.FormID = FormID                                                     'Set the Form ids
        objConfigWizard.AuthGroupId = AuthGroupId                                           'sets teh user's authenticaiton group id
        objConfigWizard.DomainID = DomainID                                                 'sets the user's Domain id
        dtFormConfig = objConfigWizard.getDynamicFormConfig(sXMLFilePath)                   'calls to get the form config
        Return dtFormConfig                                                                 'return the datatable
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This subroutine creates the controls on the form 
    ''' </summary>
    ''' <param name="dtFormConfig">The form config as stored in the BizForm Wizard.</param>
    ''' <param name="FormID">added to disable search criteria for Projects and cases advance search </param>
    ''' <value>Returns the controls in a table.</value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Function createFormControls(ByVal dtFormConfig As DataTable, ByVal tblDynamicFormControls As HtmlTable, Optional ByVal lngFormID As Long = 0, Optional ByVal lngDomainID As Long = 0, Optional ByVal lngCntID As Long = 0) As Table

        'tblDynamicFormControls.BorderColor = Color.Black                                            'Border Color is Black
        'tblDynamicFormControls.BorderWidth = Unit.Pixel(1)                                          'Border Width is 1 pixel
        tblDynamicFormControls.Width = "100%"                                        'Width of the table is 100%
        'tblDynamicFormControls.ID = "tblFormLeadBoxNonAOITable"                                     'give a name to the table
        tblDynamicFormControls.Border = 0                                        'There are no GridLines
        tblDynamicFormControls.EnableViewState = True                                               'Enable view state for the table
        tblDynamicFormControls.CellPadding = 0                                                      'Set the cell padding
        tblDynamicFormControls.CellSpacing = 0                                                      'Set the cell spacing
        'Dim boolCountryListBoxPresent As Boolean = False                                            'A flag which indicates if a country drop down is present or not
        If dtFormConfig.Rows.Count > 0 Then                                                         'determine that the XML file exists and there is atleast one field
            Dim iMaxRows As Integer                                                                 'declare a variable which will contain the max nos of rows
            dtFormConfig.Select("boolAOIField = " & boolAOIField)                                   'filter out the records as per AOI/ NOn AOI flag
            iMaxRows = CInt(dtFormConfig.Compute("Max(intRowNumVirtual)", ""))                      'Get the max value of the row number
            Dim iMaxCols As Integer                                                                 'declare a variable which will contain the max nos of rows
            iMaxCols = CInt(dtFormConfig.Compute("Max(intColumnNum)", ""))                          'Get the max value of the Columns
            Dim dvFormConfig As DataView                                                            'declare a dataview object
            Dim iRowIndex, iColumnIndex As Integer                                                  'declare a row index variable
            Dim iColNum, iRowNum As Integer                                                         'declare a column index variable
            Dim tblRow As HtmlTableRow                                                                  'declare a table row object
            Dim litLableText As Literal                                                             'declare a literal
            Dim tblCell, tblControlCell, tblLiteralCell As HtmlTableCell                                'declare a table cell object
            Dim litRequired As New Literal
            Dim strControlType, strFieldName As String
            Dim dr As DataRowView
            Dim intSearchCriteria As Integer
            Dim strSearchCriteriaName As String
            litRequired.Text = "<span class=normal4>*</span>"

            Dim UserSearchCriteria As New Hashtable
            Dim objSearch As New FormGenericAdvSearch
            objSearch.FormID = lngFormID
            objSearch.DomainID = lngDomainID
            objSearch.UserCntID = lngCntID
            objSearch.byteMode = 0
            Dim dt As DataTable = objSearch.ManageSearchCriteria()
            For Each dr1 As DataRow In dt.Rows
                UserSearchCriteria.Add(dr1("vcFormFieldName"), dr1("intSearchOperator"))
            Next


            For iRowIndex = 0 To iMaxRows                                                           'loop through the max rows to create rows for the table
                iRowNum = iRowIndex                                                                 'find the row number
                tblRow = New HtmlTableRow                                                               'instantiate a new table row
                tblRow.Height = 23                                                    'set the height attribute
                For iColumnIndex = 0 To iMaxCols * 2                                                'loop through the cell in the table row
                    dvFormConfig = New DataView(dtFormConfig)                                       'store the dataview as the dataview of the form config
                    tblControlCell = New HtmlTableCell                                                  'create a new table literal cell
                    tblControlCell.Attributes.Add("class", "normal1")                                           'set the class attribute for teh tablecell
                    tblLiteralCell = New HtmlTableCell                                                  'create a new table control cell
                    tblLiteralCell.Attributes.Add("class", "normal1")                                          'set the class attribute for teh tablecell
                    tblLiteralCell.Align = "right"                        'align the labels to the right
                    If (iColumnIndex Mod 2) = 0 Then                                                'Odd cells
                        iColNum = (tblRow.Cells.Count / 2) + 1                                      'the column number is dynamic
                        dvFormConfig.RowFilter = "intRowNum = " & iRowNum & " and intColumnNum = " & iColNum & " and boolAOIField = " & _boolAOIField 'set the row filter
                        If dvFormConfig.Count > 0 Then
                            dr = dvFormConfig(0)
                            strControlType = dr("vcAssociatedControlType").ToString.Replace(" ", "") 'replace whitespace with blank 
                            strFieldName = dr("vcDbColumnName").ToString.Trim.Replace(" ", "_") 'remove white space


                            tblLiteralCell.InnerHtml = CStr(dr("vcNewFormFieldName")) & IIf(dr("boolRequired") = 1, " <span class=normal4>*</span>", "") & "&nbsp;" 'get the text as lable


                            intSearchCriteria = 0
                            strSearchCriteriaName = ""
                            If UserSearchCriteria.ContainsKey(strFieldName) Then
                                intSearchCriteria = UserSearchCriteria(strFieldName)
                                strSearchCriteriaName = [Enum].GetName(GetType(enmSearchCriteria), intSearchCriteria).Replace("_", " ")
                            End If





                            'Add Search Operator for All textbox field e.g. =,!=,>,<,null,empty
                            If lngFormID <> 17 And lngFormID <> 18 Then 'disable for cases and projects
                                If strControlType = "TextBox" Or strControlType = "SelectBox" Or strControlType = "DateField" Then

                                    If strSearchCriteriaName.Length = 0 And intSearchCriteria = 0 Then
                                        If strControlType = "TextBox" Then
                                            If dr("vcFieldDataType") = "N" Or dr("vcFieldDataType") = "M" Then
                                                strSearchCriteriaName = "Equal To "
                                                intSearchCriteria = enmSearchCriteria.Equal_To_
                                            Else
                                                strSearchCriteriaName = "Contains "
                                                intSearchCriteria = enmSearchCriteria.Contains_
                                            End If
                                        ElseIf strControlType = "DateField" Then
                                            'If dr("vcFieldDataType") = "D" Then
                                            strSearchCriteriaName = "Equal To "
                                            intSearchCriteria = enmSearchCriteria._Equal_To_
                                            'End If
                                        ElseIf strControlType = "SelectBox" Then
                                            strSearchCriteriaName = "Equal To "
                                            intSearchCriteria = enmSearchCriteria._Equal_To
                                        End If
                                    End If

                                    Dim htmSpan As New HtmlControls.HtmlGenericControl
                                    htmSpan.TagName = "SPAN"
                                    htmSpan.ID = "spn_" & strFieldName
                                    htmSpan.Attributes.Add("class", "SearchOperator")
                                    htmSpan.Attributes.Add("onclick", "SetFilterCriteria('" & strFieldName & "','" & IIf(strControlType = "SelectBox", "S", dr("vcFieldDataType")) & "',event,this);")
                                    htmSpan.InnerHtml = strSearchCriteriaName
                                    tblControlCell.Controls.Add(htmSpan)

                                    'set previous search criteria , and select that one by default when popup is opened
                                    Dim hdnSelectedSearchCriteria As New HiddenField
                                    hdnSelectedSearchCriteria.ID = "hdn_" & strFieldName
                                    hdnSelectedSearchCriteria.Value = intSearchCriteria
                                    tblControlCell.Controls.Add(hdnSelectedSearchCriteria)
                                End If
                            End If


                            'Store Date Range into hidden following field
                            If strControlType = "DateField" Then
                                Dim hdnDateValue As New HiddenField
                                hdnDateValue.ID = "hdn_" & strFieldName & "_value"
                                hdnDateValue.Value = ""
                                tblControlCell.Controls.Add(hdnDateValue)
                            End If

                            'add the dynamic control to the table cell
                            tblControlCell.Controls.Add(getDynamicControlAndData(Replace(Replace(Replace(dr("numFormFieldId"), "R", ""), "C", ""), "D", ""), strFieldName, dr("vcListItemType"), strControlType, dr("numListID"), iRowIndex, iColNum))


                            'Validation control
                            If dr("boolRequired") = 1 Then                        'if mandatory then add an required field validator
                                tblControlCell.Controls.Add(litRequired) 'Add red *
                                tblControlCell.Controls.Add(getDynamicValidationsControlsRequired(dr("vcNewFormFieldName"), strFieldName, dr("boolRequired"))) 'Call to add Validation Controls
                            End If
                            'If strFieldName.IndexOf("Country") > -1 Then  'If its a drop down of country
                            '    boolCountryListBoxPresent = True                                    'Set the flag
                            'End If
                            'Commented by chintan reason: when using search criteria condition between for numeric field it throws error.
                            'tblControlCell.Controls.Add(getDynamicValidationsControlsDataType(dr("vcNewFormFieldName"), strFieldName, dr("vcFieldDataType"))) 'Call to add Validation Controls
                        End If
                    End If
                    tblRow.Cells.Add(tblLiteralCell)                                                'add the literal cell to the row
                    tblRow.Cells.Add(tblControlCell)                                                'add the control cell to the row
                    iColumnIndex += 1                                                               'increment the column number as two cells are added at a time
                Next
                tblDynamicFormControls.Rows.Add(tblRow)                                             'add the new table row to the existing table
            Next
            'Commented by chintan, its being handled in postback event occured by country dropdown
            'If boolCountryListBoxPresent Then                                                       'If a country drop down exists
            '    tblRow = New HtmlTableRow                                                               'instantiate a new table row
            '    tblRow.Height = 1                                                       'set the height attribute
            '    tblControlCell = New HtmlTableCell                                                      'create a new table literal cell
            '    tblControlCell.ColSpan = iMaxCols                                                'Set the column span
            '    tblControlCell.Controls.Add(New LiteralControl("<script language='javascript' src=""" & ConfigurationManager.AppSettings("StateAndCountryList") & "StatesInCountries_" & DomainID & ".js""></script>" & vbCrLf & "<script language='javascript' src=""../javascript/StateAndCountries.js""></script>")) 'Add the script which will filter the States
            '    tblRow.Cells.Add(tblControlCell)                                                    'add the literal cell to the row
            '    tblDynamicFormControls.Rows.Add(tblRow)                                             'add the new table row to the existing table
            'End If
        Else
            Dim tblRow As HtmlTableRow                                                                  'declare a table row onject
            tblRow = New HtmlTableRow                                                                   'instantiate a tablerow object
            Dim tblLiteralCell As HtmlTableCell                                                         'declare a tablecell object
            tblLiteralCell = New HtmlTableCell                                                          'create a new table control cell
            tblLiteralCell.Attributes.Add("class", "normal1")                                                     'set the class attribute for teh tablecell
            Dim litLableText As New LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen.")                                                            'declare a literal
            tblLiteralCell.Controls.Add(litLableText)                                               'Add the literal control to the table cell
            tblRow.Cells.Add(tblLiteralCell)                                                        'Add the table cell to the table row
            tblDynamicFormControls.Rows.Add(tblRow)                                                 'Add the table row to the table
        End If
        'Return teh data table
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to create the array of form elements
    ''' </summary>
    ''' <value>Returns the string of array elements.</value>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    ''' 
    Public Function getJavascriptArray(ByVal dtFormConfig As DataTable) As String
        Dim sDynamicFieldInfo As New System.Text.StringBuilder                              'This will contain the XML fields
        Dim iRowIndex As Int16 = 0                                                          'Declare an integer to hold the row index
        sDynamicFieldInfo.Append(vbCrLf & "<script language='javascript'>")
        sDynamicFieldInfo.Append(vbCrLf & "var arrFieldConfig = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript
        Dim dtRows As DataRow                                                               'Declare a datarow
        For Each dtRows In dtFormConfig.Rows
            If dtRows.Item("boolAOIField") = 1 Then                                         'Since checkbox lsit is use for displayign AOIs
                sDynamicFieldInfo.Append("arrFieldConfig[" & iRowIndex & "] = new classFormFieldConfig('" & "vcInterested_" & CInt(dtRows.Item("intRowNum")) & "','" & Replace(dtRows.Item("vcNewFormFieldName"), "'", "\'") & "','',''," & dtRows.Item("intRowNum") & "," & dtRows.Item("intColumnNum") & ",'" & dtRows.Item("vcAssociatedControlType") & "'," & dtRows.Item("boolAOIField") & ");" & vbCrLf) 'start creating the array elements, each holding field information
            Else                                                                            'db fieldbname is same as control name for non AIOs
                sDynamicFieldInfo.Append("arrFieldConfig[" & iRowIndex & "] = new classFormFieldConfig('" & dtRows.Item("vcDbColumnName") & "','" & Replace(dtRows.Item("vcNewFormFieldName"), "'", "\'") & "','',''," & dtRows.Item("intRowNum") & "," & dtRows.Item("intColumnNum") & ",'" & dtRows.Item("vcAssociatedControlType") & "'," & dtRows.Item("boolAOIField") & ");" & vbCrLf) 'start creating the array elements, each holding field information
            End If
            iRowIndex += 1                                                                  'increment the Array Index
        Next
        sDynamicFieldInfo.Append("</script>")                                               'end the client side javascript block
        Return sDynamicFieldInfo.ToString()                                                 'Return the array as string
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to create the array of form elements of custom field areas
    ''' </summary>
    ''' <value>Returns the string of array elements.</value>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	02/17/2006	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    ''' 
    Public Function getCustomFieldJavascriptArray(ByVal dtFormCustomAreasConfig As DataTable) As String
        Dim sDynamicFieldInfo As New System.Text.StringBuilder                              'This will contain the XML fields
        Dim iRowIndex As Int16 = 0                                                          'Declare an integer to hold the row index
        sDynamicFieldInfo.Append(vbCrLf & "<script language='javascript'>")
        sDynamicFieldInfo.Append(vbCrLf & "var arrCustomAreasFieldConfig = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript
        Dim dtRows As DataRow                                                               'Declare a datarow
        For Each dtRows In dtFormCustomAreasConfig.Rows
            sDynamicFieldInfo.Append("arrCustomAreasFieldConfig[" & iRowIndex & "] = new classCustomAreas(" & dtRows.Item("Loc_Id") & ",'" & dtRows.Item("Loc_Name") & "');" & vbCrLf) 'start creating the array elements, each holding field information
            iRowIndex += 1                                                                  'increment the Array Index
        Next
        sDynamicFieldInfo.Append("</script>")                                               'end the client side javascript block
        Return sDynamicFieldInfo.ToString()                                                 'Return the array as string
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to return the appropriate control to be put into the tablecell
    ''' </summary>
    ''' <param name="vcNewFormFieldName">Represents if the field name.</param>
    ''' <param name="vcDbColumnName">Represents if the database field name.</param>
    ''' <param name="boolRequired">Represents if the field is mandatory or not.</param>
    ''' <value>Returns the controls as an object.</value>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    ''' 
    Public Function getDynamicValidationsControlsRequired(ByVal vcNewFormFieldName As String, ByVal vcDbColumnName As String, ByVal boolRequired As Boolean) As RequiredFieldValidator
        Dim valReqControl As New RequiredFieldValidator                                             'Declare a required field validator
        valReqControl.ID = "val_" & vcDbColumnName
        valReqControl.ControlToValidate = vcDbColumnName                                            'Attach the control to validate
        valReqControl.EnableClientScript = True                                                     'Enable Client script
        valReqControl.InitialValue = ""                                                             'Set the Initial value so that it also validates drop down
        valReqControl.ErrorMessage = "Please enter " & vcNewFormFieldName & "."                     'Specify the error message to be displayed
        valReqControl.Display = ValidatorDisplay.None                                               'Error message never displays inline
        Return valReqControl
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to return the appropriate control to be put into the tablecell
    ''' </summary>
    ''' <param name="vcNewFormFieldName">Represents if the field name.</param>
    ''' <param name="vcDbColumnName">Represents if the database field name.</param>
    ''' <param name="vcDataFieldType">Represents if the field data type (V: Varchar, N: Numeric, B: Boolean, M: Money.</param>
    ''' <value>Returns the controls as an object.</value>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Public Function getDynamicValidationsControlsDataType(ByVal vcNewFormFieldName As String, ByVal vcDbColumnName As String, ByVal vcFieldDataType As Char) As Object

        Select Case vcFieldDataType
            Case "N"                                                                                    'Numeric Data Type
                Dim valDataType As New RangeValidator                                                   'Create an instance of Range Validator Control
                valDataType.ID = "val_" & vcDbColumnName
                valDataType.MinimumValue = 0                                                            'Set the lower limit
                valDataType.MaximumValue = 999999999999999999                                           'Set the upper limit
                valDataType.ControlToValidate = vcDbColumnName                                          'Set the control to validate
                valDataType.EnableClientScript = True                                                   'Enable Client script
                valDataType.Type = ValidationDataType.Double                                            'set the datatype
                valDataType.ErrorMessage = "Invalid value entered for " & vcNewFormFieldName            'Set tge error message
                valDataType.Display = ValidatorDisplay.None                                             'Error message never displays inline
                Return valDataType
                Exit Select
            Case "B"                                                                                    'Boolean Case
                Dim valDataType As New RangeValidator                                                   'Create an instance of Range Validator Control
                valDataType.ID = "val_" & vcDbColumnName
                valDataType.MinimumValue = 0                                                            'Set the lower limit
                valDataType.MaximumValue = 1                                                            'Set the upper limit
                valDataType.ControlToValidate = vcDbColumnName                                          'Set the control to validate
                valDataType.EnableClientScript = True                                                   'Enable Client script
                valDataType.Type = ValidationDataType.Integer                                           'set the datatype
                valDataType.ErrorMessage = "Invalid value entered for " & vcNewFormFieldName            'Set tge error message
                valDataType.Display = ValidatorDisplay.None                                             'Error message never displays inline
                Return valDataType
                Exit Select
            Case "M"                                                                                    'Money Data Type
                Dim valDataType As New RangeValidator                                                   'Create an instance of Range Validator Control
                valDataType.ID = "val_" & vcDbColumnName
                valDataType.MinimumValue = 0                                                            'Set the lower limit
                valDataType.MaximumValue = 999999999999999999                                           'Set the upper limit
                valDataType.ControlToValidate = vcDbColumnName                                          'Set the control to validate
                valDataType.EnableClientScript = True                                                   'Enable Client script
                valDataType.Type = ValidationDataType.Currency                                          'set the datatype
                valDataType.ErrorMessage = "Invalid value entered for " & vcNewFormFieldName            'Set tge error message
                valDataType.Display = ValidatorDisplay.None                                             'Error message never displays inline
                Return valDataType
                Exit Select
            Case "E"                                                                                    'Email
                Dim valDataType As New RegularExpressionValidator                                       'Create an instance of Regular Expression Validator Control
                valDataType.ID = "val_" & vcDbColumnName
                valDataType.ControlToValidate = vcDbColumnName                                          'Set the control to validate
                valDataType.ValidationExpression = "^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$"   'Set the expression for email validaiton
                valDataType.EnableClientScript = True                                                   'Enable Client script
                valDataType.ErrorMessage = "Invalid value entered for " & vcNewFormFieldName            'Set tge error message
                valDataType.Display = ValidatorDisplay.None                                             'Error message never displays inline
                Return valDataType
                Exit Select
            Case Else
                Return New LiteralControl("")                                                           'just return a default control
        End Select
    End Function


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to return the appropriate control to be put into the tablecell
    ''' </summary>
    ''' <param name="vcDbColumnName">Represents the database column name where this field will dump data.</param>
    ''' <param name="vcAssociatedControlType">Represents the associated control type.</param>
    ''' <param name="boolRequired">Represents if the field is mandatory or not.</param>
    ''' <param name="numListID">If the control is a listbox, this links the options of the listbox.</param>
    ''' <value>Returns the controls as an object.</value>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Public Function getDynamicControlAndData(ByVal numFormFieldId As Integer, ByVal vcDbColumnName As String, ByVal vcListItemType As String, ByVal vcAssociatedControlType As String, ByVal numListID As Integer, ByVal numRowNum As Integer, ByVal numColNum As Integer, Optional ByVal MaxLength As Integer = 50) As Object
        'declare a generalized object
        Dim oControlBox As Object
        Select Case vcAssociatedControlType.ToLower.Replace(" ", "")
            Case "textarea"                                                                         'Text Area is selected to be provided
                oControlBox = New TextBox                                                           'Instantiate a TextBox
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                oControlBox.Width = Unit.Pixel(150)                                                 'set the width of the textbox                                                'set the height of the textbox
                oControlBox.TextMode = TextBoxMode.MultiLine                                        'This is a multiline textarea
                oControlBox.CssClass = "signup"                                                     'Set the class attribute for the textbox control
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
            Case "selectbox"                                                                        'If the control to be used is a selectbox
                Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
                objConfigWizard.DomainID = DomainID                                                 'set the domain id
                objConfigWizard.ListItemType = vcListItemType                                       'set the listitem type
                oControlBox = New System.Web.UI.WebControls.DropDownList                                                       'instantiate a drop down
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                Dim dtTable As DataTable                                                            'declare a datatable
                dtTable = objConfigWizard.GetMasterListByListId(numListID)                          'call function to fill the datatable
                Dim dRow As DataRow                                                                 'declare a datarow
                dRow = dtTable.NewRow()                                                             'get a new row object
                dtTable.Rows.InsertAt(dRow, 0)                                                      'insert the row
                dtTable.Rows(0).Item("numItemID") = System.DBNull.Value                             'set the values for the first entry
                dtTable.Rows(0).Item("vcItemName") = "---Select One---"                             'set the group name
                If numListID = 176 Then 'Order status then add New value None (-1) which will search records with orderstatus as 0 or null
                    dRow = dtTable.NewRow()                                                         'get a new row object
                    dRow.Item("numItemID") = "0"
                    dRow.Item("vcItemName") = "---Null---"
                    dtTable.Rows.Add(dRow)                                                          'insert the row
                End If
                oControlBox.DataSource = dtTable                                                    'set the datasource of the drop down
                oControlBox.DataTextField = "vcItemName"                                            'Set the Text property of the drop down
                oControlBox.DataValueField = "numItemID"                                            'Set the value attribute of the textbox
                oControlBox.DataBind()                                                              'Databind the drop down
                oControlBox.Width = Unit.Pixel(156)                                                 'set the width of the drop down
                oControlBox.CssClass = "signup"                                                     'Set the class attribute for the selectbox control
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index

            Case "checkbox" 'Case sensetive                                                        'Checkbox control is requested
                oControlBox = New CheckBox                                                          'instantiate a new checkbox
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                oControlBox.CssClass = "text"                                                       'set the class attribute
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
                'Checkbox of Billing Address Same as Shpping needs scripting/ Requested by Carl on the 1st of Mar 2006
                If vcDbColumnName.IndexOf("bitSameAddr") > -1 Then  'If its a checkbox of Same Address for Billing/ Shiiping
                    oControlBox.Attributes.Add("onclick", "javascript: MakeAddressShippingSameAsBilling(this);") 'Attach Javascript
                End If
            Case "checkbox"                                                                        'Checkbox control is requested
                oControlBox = New CheckBox                                                          'instantiate a new checkbox
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                oControlBox.CssClass = "text"                                                       'set the class attribute
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
                'Checkbox of Billing Address Same as Shpping needs scripting/ Requested by Carl on the 1st of Mar 2006
                If vcDbColumnName.IndexOf("bitSameAddr") > -1 Then  'If its a checkbox of Same Address for Billing/ Shiiping
                    oControlBox.Attributes.Add("onclick", "javascript: MakeAddressShippingSameAsBilling(this);") 'Attach Javascript
                End If
            Case "radiobox"                                                                         'Radio button is requested
                oControlBox = New RadioButton                                                       'instantiate the radio
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                oControlBox.CssClass = "text"                                                       'set the class attribute
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
            Case "datefield"
                Dim bizCalendar As BACRM.Include.calandar
                bizCalendar = CType(CType(HttpContext.Current.Handler, Page).LoadControl("~\include\calandar.ascx"), BACRM.Include.calandar)
                bizCalendar.ID = vcDbColumnName
                'CType(bizCalendar, calandar).SelectedDate = Date.Now.ToShortDateString
                'Dim _myControlType As Type = bizCalendar.GetType()
                'Dim _myUC_DueDate As PropertyInfo
                '_myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                '_myUC_DueDate.SetValue(bizCalendar, Date.Now.ToShortDateString, Nothing)
                Return CType(bizCalendar, Object)
            Case "listbox"                                                                        'If the control to be used is a selectbox
                Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
                objConfigWizard.DomainID = DomainID                                                 'set the domain id
                objConfigWizard.ListItemType = vcListItemType                                       'set the listitem type
                oControlBox = New Telerik.Web.UI.RadComboBox                                                       'instantiate a drop down
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                oControlBox.CheckBoxes = True
                Dim dtTable As DataTable                                                            'declare a datatable
                dtTable = objConfigWizard.GetMasterListByListId(numListID)                          'call function to fill the datatable
                'Dim dRow As DataRow                                                                 'declare a datarow
                'dRow = dtTable.NewRow()                                                             'get a new row object
                'dtTable.Rows.InsertAt(dRow, 0)                                                      'insert the row
                'dtTable.Rows(0).Item("numItemID") = System.DBNull.Value                             'set the values for the first entry
                'dtTable.Rows(0).Item("vcItemName") = "---Select One---"                             'set the group name
                oControlBox.DataSource = dtTable                                                    'set the datasource of the drop down
                oControlBox.DataTextField = "vcItemName"                                            'Set the Text property of the drop down
                oControlBox.DataValueField = "numItemID"                                            'Set the value attribute of the textbox
                oControlBox.DataBind()                                                              'Databind the drop down
                oControlBox.Width = Unit.Pixel(200)                                                 'set the width of the drop down
                oControlBox.Height = Unit.Pixel(100)
                oControlBox.CssClass = "ListBox"                                                     'Set the class attribute for the selectbox control
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
                'Case "website"

                '    oControlBox = New HyperLink
                '    oControlBox.Text = "Google Product Category"
                '    oControlBox.CssClass = "normal1"
                '    oControlBox.Target = "_blank"
                '    oControlBox.NavigateUrl = "www.google.com"

            Case Else                                                                               'Edit Box is to be provided
                oControlBox = New TextBox                                                           'instantiate a new editbox
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                oControlBox.MaxLength = MaxLength                                                          'Set the Maximum character that can be entered
                oControlBox.CssClass = "signup"                                                     'Set the class attribute for the editbox control
                oControlBox.width = 150
                'If vcDbColumnName.IndexOf("PostalCode") > -1 Then                                      'Special Treatment for drop down of Countries, since its a drop down which triggers the filteration of states
                '    oControlBox.Attributes.Add("onkeypress", "return CheckNumber()")
                'ElseIf vcDbColumnName.IndexOf("PostCode") > -1 Then
                '    oControlBox.Attributes.Add("onkeypress", "return CheckNumber()")
                'End If
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
        End Select
        Return CType(oControlBox, Object)
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This subroutine creates the controls on the form for AOIs
    ''' </summary>
    ''' <param name="dtFormConfig">The form config as stored in the BizForm Wizard.</param>
    ''' <value>Returns the controls in a checkboxlist.</value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/30/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Sub createFormControlsAOI(ByVal dtFormConfig As DataTable, ByVal tblAdvSearchAOI As HtmlTable)

        Dim cbListDynamicAOIs As New CheckBoxList                                                   'instantiate a checkboxlist
        cbListDynamicAOIs.DataSource = dtFormConfig                                                 'set a datashource 
        cbListDynamicAOIs.DataTextField = "vcAOIName"                                      'set the text for the checkboxlist
        cbListDynamicAOIs.DataTextFormatString = "&nbsp;{0}"                                        'set the data text format
        cbListDynamicAOIs.DataValueField = "numAOIID"                                         'set the value attribute
        cbListDynamicAOIs.CssClass = "text"                                                         'set the class attribute
        cbListDynamicAOIs.DataBind()                                                                'databind to display the data
        cbListDynamicAOIs.ID = "cbListAOI"                                                       'set the AOI checkboxlist ids
        cbListDynamicAOIs.RepeatColumns = 3                                                         'display 3 columns
        cbListDynamicAOIs.RepeatDirection = RepeatDirection.Horizontal                              'display horizontally
        cbListDynamicAOIs.RepeatLayout = RepeatLayout.Table                                         'display in tabular format
        If cbListDynamicAOIs.Items.Count > 0 Then
            Dim tblRow As HtmlTableRow
            Dim tblCell As HtmlTableCell
            tblRow = New HtmlTableRow
            tblCell = New HtmlTableCell
            tblCell.InnerHtml = "<br/>Areas of Interest"
            tblCell.Align = "Center"
            tblCell.Attributes.Add("class", "text_bold")
            tblRow.Cells.Add(tblCell)
            tblAdvSearchAOI.Rows.Add(tblRow)

            tblRow = New HtmlTableRow
            tblCell = New HtmlTableCell
            tblRow.Cells.Add(tblCell)
            tblCell = New HtmlTableCell
            tblCell.Controls.Add(cbListDynamicAOIs)
            tblCell.ColSpan = 4
            tblRow.Cells.Add(tblCell)
            tblAdvSearchAOI.Rows.Add(tblRow)
        End If
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This subroutine creates the controls on the form for Custom Field Areas
    ''' </summary>
    ''' <param name="dtFormConfig">The form config as stored in the BizForm Wizard.</param>
    ''' <value>Returns the controls in a checkboxlist.</value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	02/17/2006	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Sub createFormControlsCustomFieldAreas(ByVal dtFormConfig As DataTable, ByVal tbl As HtmlTable)
        Dim tblRow As HtmlTableRow
        Dim tblCell As HtmlTableCell
        tblRow = New HtmlTableRow
        tblCell = New HtmlTableCell
        tblCell.InnerHtml = "<br/>Custom Fields"
        tblCell.Align = "center"
        tblCell.Attributes.Add("class", "text_bold")
        tblRow.Cells.Add(tblCell)
        tbl.Rows.Add(tblRow)



        Dim dvFormConfig As DataView                                                                'declare a databview
        dvFormConfig = dtFormConfig.DefaultView                                                     'get the default view
        Dim cbListDynamicCustonFieldAreas As New CheckBoxList                                       'instantiate a checkboxlist
        cbListDynamicCustonFieldAreas.DataSource = dvFormConfig                                     'set a datashource 
        cbListDynamicCustonFieldAreas.DataTextField = "Loc_Name"                                    'set the text for the checkboxlist
        cbListDynamicCustonFieldAreas.DataTextFormatString = "&nbsp;{0}"                            'set the data text format
        cbListDynamicCustonFieldAreas.DataValueField = "Loc_Id"                                     'set the value attribute
        cbListDynamicCustonFieldAreas.CssClass = "text"                                             'set the class attribute
        cbListDynamicCustonFieldAreas.DataBind()                                                    'databind to display the data
        cbListDynamicCustonFieldAreas.ID = "Cfld"                                                   'set the Custom field areas checkboxlist ids
        cbListDynamicCustonFieldAreas.RepeatColumns = 2                                             'display 3 columns
        cbListDynamicCustonFieldAreas.RepeatDirection = RepeatDirection.Horizontal                  'display horizontally
        cbListDynamicCustonFieldAreas.RepeatLayout = RepeatLayout.Table                             'display in tabular format

        tblRow = New HtmlTableRow
        tblCell = New HtmlTableCell
        tblRow.Cells.Add(tblCell)
        tblCell = New HtmlTableCell
        tblCell.ColSpan = 4
        tblCell.Controls.Add(cbListDynamicCustonFieldAreas)
        tblRow.Cells.Add(tblCell)

        tbl.Rows.Add(tblRow)


        tblRow = New HtmlTableRow
        tblCell = New HtmlTableCell
        tblCell.InnerHtml = "Field Search&nbsp;"
        tblCell.Attributes.Add("class", "normal1")
        tblCell.Align = "right"
        tblRow.Cells.Add(tblCell)

        tblCell = New HtmlTableCell
        Dim txt As New TextBox
        txt.ID = "txtCustomKeyword"
        txt.CssClass = "signup"
        tblCell.Controls.Add(txt)
        tblRow.Cells.Add(tblCell)
        tbl.Rows.Add(tblRow)





    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Send an Email notificaiton also attaching the XML data 
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/03/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Public Function sendEmailNotification(ByVal strFileName As String)
        strFileName = sXMLFilePath & "\" & strFileName                                          'Set the file name
        Try
            'Dim msg As New MailMessage                                                          'Create the email message

            'msg.From = "carl@bizautomation.com"                                                 'Set basic properties
            'msg.To = "carl@bizautomation.com"
            'msg.Subject = "The following LeadBox information could not be added to the database"
            'msg.Body = "Sir," & vbCrLf & "The following LeadBox information posted by an user could not be uploaded into the database and it failed to generate a Lead." _
            '& vbCrLf & "You are hence requested to upload it manually." & vbCrLf

            'msg.Attachments.Add(New MailAttachment(strFileName))                                'Add an attachment (as many as needed)

            'SmtpMail.SmtpServer = "localhost"                                                   'Specify the outgoing SMTP mail server

            'SmtpMail.Send(msg)                                                                  'Send it off
            File.Delete(strFileName)                                                            'Delete the file after the email has been sent
        Catch ex As Exception
            'Throw ex
            'Email Sending failed
        End Try
    End Function


    Function createDynamicFormControlsSectionWise(ByVal dsFormConfig As DataSet, ByVal tblDynamicFormControls As HtmlTable, Optional ByVal lngFormID As Long = 0, Optional ByVal lngDomainID As Long = 0, Optional ByVal lngCntID As Long = 0) As Table

        Dim dtFormConfig As DataTable = dsFormConfig.Tables(1)
        Dim dtSection As DataTable = dsFormConfig.Tables(0)

        'tblDynamicFormControls.BorderColor = Color.Black                                            'Border Color is Black
        'tblDynamicFormControls.BorderWidth = Unit.Pixel(1)                                          'Border Width is 1 pixel
        tblDynamicFormControls.Width = "100%"                                        'Width of the table is 100%
        'tblDynamicFormControls.ID = "tblFormLeadBoxNonAOITable"                                     'give a name to the table
        tblDynamicFormControls.Border = 0                                        'There are no GridLines
        tblDynamicFormControls.EnableViewState = True                                               'Enable view state for the table
        tblDynamicFormControls.CellPadding = 0                                                      'Set the cell padding
        tblDynamicFormControls.CellSpacing = 0                                                      'Set the cell spacing
        'Dim boolCountryListBoxPresent As Boolean = False                                            'A flag which indicates if a country drop down is present or not

        Dim tblRow As HtmlTableRow                                                                  'declare a table row object
        Dim tblCell, tblControlCell, tblLiteralCell As HtmlTableCell                                'declare a table cell object

        Dim dvSectionFields As DataView

        If dtFormConfig.Rows.Count > 0 Then                                                         'determine that the XML file exists and there is atleast one field
            For Each drSec As DataRow In dtSection.Rows
                dvSectionFields = New DataView(dtFormConfig)

                If drSec("Loc_Id") > 0 Then
                    dvSectionFields.RowFilter = "GRP_ID = " & drSec("Loc_Id") & " and boolAOIField = " & boolAOIField & ""
                Else
                    dvSectionFields.RowFilter = "intSectionID = " & drSec("intSectionID") & " and boolAOIField = " & boolAOIField & ""
                End If

                dvSectionFields.RowStateFilter = DataViewRowState.CurrentRows

                If dvSectionFields.Count > 0 Then
                    tblRow = New HtmlTableRow
                    tblCell = New HtmlTableCell
                    tblCell.InnerHtml = drSec("vcSectionName")
                    tblCell.Align = "left"
                    tblCell.Attributes.Add("class", "tblrowHeader")
                    tblCell.ColSpan = 7
                    tblRow.Cells.Add(tblCell)
                    tblDynamicFormControls.Rows.Add(tblRow)

                    Dim iMaxRows As Integer                                                                 'declare a variable which will contain the max nos of rows
                    iMaxRows = CInt(dvSectionFields.ToTable().Compute("Max(intRowNum)", ""))                      'Get the max value of the row number

                    Dim iMaxCols As Integer                                                                 'declare a variable which will contain the max nos of rows
                    iMaxCols = CInt(dvSectionFields.ToTable().Compute("Max(intColumnNum)", ""))                          'Get the max value of the Columns

                    Dim dvFormConfig As DataView                                                            'declare a dataview object
                    Dim iRowIndex, iColumnIndex As Integer                                                  'declare a row index variable
                    Dim iColNum, iRowNum As Integer                                                         'declare a column index variable
                    Dim litLableText As Literal                                                             'declare a literal
                    Dim litRequired As New Literal
                    Dim strControlType, strFieldName, strControlID As String
                    Dim dr As DataRowView
                    Dim intSearchCriteria As Integer
                    Dim strSearchCriteriaName As String
                    litRequired.Text = "<span class=normal4>*</span>"

                    Dim UserSearchCriteria As New Hashtable
                    Dim objSearch As New FormGenericAdvSearch
                    objSearch.FormID = lngFormID
                    objSearch.DomainID = lngDomainID
                    objSearch.UserCntID = lngCntID
                    objSearch.byteMode = 0
                    Dim dt As DataTable = objSearch.ManageSearchCriteria()
                    For Each dr1 As DataRow In dt.Rows
                        UserSearchCriteria.Add(dr1("vcFormFieldName"), dr1("intSearchOperator"))
                    Next

                    For iRowIndex = 0 To iMaxRows                                                           'loop through the max rows to create rows for the table
                        iRowNum = iRowIndex                                                                 'find the row number
                        tblRow = New HtmlTableRow                                                               'instantiate a new table row
                        tblRow.Height = 23                                                    'set the height attribute
                        For iColumnIndex = 0 To iMaxCols * 2                                                'loop through the cell in the table row
                            dvFormConfig = New DataView(dvSectionFields.ToTable())                                       'store the dataview as the dataview of the form config
                            tblControlCell = New HtmlTableCell                                                  'create a new table literal cell
                            tblControlCell.Attributes.Add("class", "normal1")                                           'set the class attribute for teh tablecell
                            tblLiteralCell = New HtmlTableCell                                                  'create a new table control cell
                            tblLiteralCell.Attributes.Add("class", "normal1")                                          'set the class attribute for teh tablecell
                            tblLiteralCell.Align = "right"                        'align the labels to the right
                            If (iColumnIndex Mod 2) = 0 Then                                                'Odd cells
                                iColNum = (tblRow.Cells.Count / 2) + 1                                      'the column number is dynamic
                                dvFormConfig.RowFilter = "intRowNum = " & iRowNum & " and intColumnNum = " & iColNum & " and boolAOIField = " & _boolAOIField 'set the row filter
                                If dvFormConfig.Count > 0 Then
                                    dr = dvFormConfig(0)

                                    If dr("vcAssociatedControlType").ToString.ToLower = "checkbox" Then
                                        dr("vcAssociatedControlType") = "SelectBox"
                                        dr("vcListItemType") = "CHK"
                                        dr("numListID") = "0"
                                    End If

                                    strControlType = dr("vcAssociatedControlType").ToString.Replace(" ", "") 'replace whitespace with blank 
                                    strFieldName = dr("vcDbColumnName").ToString.Trim.Replace(" ", "_") 'remove white space
                                    strControlID = dr("numFormFieldID").ToString & "_" & dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")

                                    tblLiteralCell.InnerHtml = CStr(dr("vcNewFormFieldName")) & "&nbsp;" 'get the text as lable

                                    intSearchCriteria = 0
                                    strSearchCriteriaName = ""
                                    If UserSearchCriteria.ContainsKey(strControlID) Then
                                        intSearchCriteria = UserSearchCriteria(strControlID)
                                        strSearchCriteriaName = [Enum].GetName(GetType(enmSearchCriteria), intSearchCriteria).Replace("_", " ")
                                    End If

                                    'Add Search Operator for All textbox field e.g. =,!=,>,<,null,empty
                                    If lngFormID <> 17 And lngFormID <> 18 Then 'disable for cases and projects
                                        If strControlType = "TextBox" Or strControlType = "SelectBox" Or strControlType = "DateField" Then

                                            If strSearchCriteriaName.Length = 0 And intSearchCriteria = 0 Then
                                                If strControlType = "TextBox" Then
                                                    If dr("vcFieldDataType") = "N" Or dr("vcFieldDataType") = "M" Then
                                                        strSearchCriteriaName = "Equal To "
                                                        intSearchCriteria = enmSearchCriteria.Equal_To_
                                                    Else
                                                        strSearchCriteriaName = "Contains "
                                                        intSearchCriteria = enmSearchCriteria.Contains_
                                                    End If
                                                ElseIf strControlType = "DateField" Then
                                                    'If dr("vcFieldDataType") = "D" Then
                                                    strSearchCriteriaName = "Equal To "
                                                    intSearchCriteria = enmSearchCriteria._Equal_To_
                                                    'End If
                                                ElseIf strControlType = "SelectBox" Then
                                                    strSearchCriteriaName = "Equal To "
                                                    intSearchCriteria = enmSearchCriteria._Equal_To
                                                End If
                                            End If

                                            Dim htmSpan As New HtmlControls.HtmlGenericControl
                                            htmSpan.TagName = "SPAN"
                                            htmSpan.ID = "spn_" & strControlID
                                            htmSpan.Attributes.Add("class", "SearchOperator")
                                            htmSpan.Attributes.Add("onclick", "SetFilterCriteria('" & strControlID & "','" & IIf(strControlType = "SelectBox", "S", dr("vcFieldDataType")) & "',event,this);")
                                            htmSpan.InnerHtml = strSearchCriteriaName
                                            tblControlCell.Controls.Add(htmSpan)

                                            'set previous search criteria , and select that one by default when popup is opened
                                            Dim hdnSelectedSearchCriteria As New HiddenField
                                            hdnSelectedSearchCriteria.ID = "hdn_" & strControlID
                                            hdnSelectedSearchCriteria.Value = intSearchCriteria
                                            tblControlCell.Controls.Add(hdnSelectedSearchCriteria)
                                        End If
                                    End If


                                    'Store Date Range into hidden following field
                                    If strControlType = "DateField" Then
                                        Dim hdnDateValue As New HiddenField
                                        hdnDateValue.ID = "hdn_" & strControlID & "_value"
                                        hdnDateValue.Value = ""
                                        tblControlCell.Controls.Add(hdnDateValue)
                                    End If

                                    'add the dynamic control to the table cell
                                    tblControlCell.Controls.Add(getDynamicControlAndData(Replace(Replace(Replace(dr("numFormFieldId"), "R", ""), "C", ""), "D", ""), strControlID, dr("vcListItemType"), strControlType, dr("numListID"), iRowIndex, iColNum))


                                    'Validation control
                                    'If dr("boolRequired") = 1 Then                        'if mandatory then add an required field validator
                                    '    tblControlCell.Controls.Add(litRequired) 'Add red *
                                    '    tblControlCell.Controls.Add(getDynamicValidationsControlsRequired(dr("vcNewFormFieldName"), strFieldName, dr("boolRequired"))) 'Call to add Validation Controls
                                    'End If
                                    'If strFieldName.IndexOf("Country") > -1 Then  'If its a drop down of country
                                    '    boolCountryListBoxPresent = True                                    'Set the flag
                                    'End If
                                    'Commented by chintan reason: when using search criteria condition between for numeric field it throws error.
                                    'tblControlCell.Controls.Add(getDynamicValidationsControlsDataType(dr("vcNewFormFieldName"), strFieldName, dr("vcFieldDataType"))) 'Call to add Validation Controls
                                End If
                            End If
                            tblRow.Cells.Add(tblLiteralCell)                                                'add the literal cell to the row
                            tblRow.Cells.Add(tblControlCell)                                                'add the control cell to the row
                            iColumnIndex += 1                                                               'increment the column number as two cells are added at a time
                        Next
                        tblDynamicFormControls.Rows.Add(tblRow)                                             'add the new table row to the existing table
                    Next
                End If
            Next
        Else
            tblRow = New HtmlTableRow                                                                   'instantiate a tablerow object
            tblLiteralCell = New HtmlTableCell                                                          'create a new table control cell
            tblLiteralCell.Attributes.Add("class", "normal1")                                                     'set the class attribute for teh tablecell
            Dim litLableText As New LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen.")                                                            'declare a literal
            tblLiteralCell.Controls.Add(litLableText)                                               'Add the literal control to the table cell
            tblRow.Cells.Add(tblLiteralCell)                                                        'Add the table cell to the table row
            tblDynamicFormControls.Rows.Add(tblRow)                                                 'Add the table row to the table
        End If
        'Return teh data table
    End Function


    Function createDynamicFormControlsSectionWise(ByVal dsFormConfig As DataSet, ByVal plhControls As PlaceHolder, Optional ByVal lngFormID As Long = 0, Optional ByVal lngDomainID As Long = 0, Optional ByVal lngCntID As Long = 0) As Table

        Dim dtFormConfig As DataTable = dsFormConfig.Tables(1)
        Dim dtSection As DataTable = dsFormConfig.Tables(0)

        Dim dvSectionFields As DataView

        If dtFormConfig.Rows.Count > 0 Then
            For Each drSec As DataRow In dtSection.Rows
                dvSectionFields = New DataView(dtFormConfig)

                If drSec("Loc_Id") > 0 Then
                    dvSectionFields.RowFilter = "GRP_ID = " & drSec("Loc_Id") & " and boolAOIField = " & boolAOIField & ""
                Else
                    dvSectionFields.RowFilter = "intSectionID = " & drSec("intSectionID") & " and boolAOIField = " & boolAOIField & ""
                End If

                dvSectionFields.RowStateFilter = DataViewRowState.CurrentRows

                If dvSectionFields.Count > 0 Then
                    Dim divBox As New HtmlGenericControl("div")
                    divBox.Attributes.Add("class", "box box-primary box-solid")

                    Dim divBoxHeader As New HtmlGenericControl("div")
                    divBoxHeader.Attributes.Add("class", "box-header with-border")
                    divBoxHeader.InnerHtml = "<h3 class=""box-title"">" & drSec("vcSectionName") & "</h3>"

                    divBox.Controls.Add(divBoxHeader)

                    Dim divBoxBody As New HtmlGenericControl("div")
                    divBoxBody.Attributes.Add("class", "box-body searchcontrols")


                    Dim iMaxRows As Integer                                                                 'declare a variable which will contain the max nos of rows
                    iMaxRows = CInt(dvSectionFields.ToTable().Compute("Max(intRowNum)", ""))                      'Get the max value of the row number

                    Dim iMaxCols As Integer                                                                 'declare a variable which will contain the max nos of rows
                    iMaxCols = CInt(dvSectionFields.ToTable().Compute("Max(intColumnNum)", ""))                          'Get the max value of the Columns

                    Dim dvFormConfig As DataView                                                            'declare a dataview object
                    Dim iRowIndex, iColumnIndex As Integer                                                  'declare a row index variable
                    Dim iColNum, iRowNum As Integer                                                         'declare a column index variable
                    Dim litLableText As Literal                                                             'declare a literal
                    Dim litRequired As New Literal
                    Dim strControlType, strFieldName, strControlID As String
                    Dim dr As DataRowView
                    Dim intSearchCriteria As Integer
                    Dim strSearchCriteriaName As String
                    litRequired.Text = "<span class=normal4>*</span>"

                    Dim UserSearchCriteria As New Hashtable
                    Dim objSearch As New FormGenericAdvSearch
                    objSearch.FormID = lngFormID
                    objSearch.DomainID = lngDomainID
                    objSearch.UserCntID = lngCntID
                    objSearch.byteMode = 0
                    Dim dt As DataTable = objSearch.ManageSearchCriteria()
                    For Each dr1 As DataRow In dt.Rows
                        UserSearchCriteria.Add(dr1("vcFormFieldName"), dr1("intSearchOperator"))
                    Next

                    For iRowIndex = 0 To iMaxRows                                                           'loop through the max rows to create rows for the table
                        iRowNum = iRowIndex + 1                                                              'find the row number

                        Dim divRow As New HtmlGenericControl("div")
                        divRow.Attributes.Add("class", "row")



                        For iColumnIndex = 0 To iMaxCols
                            iColNum = iColumnIndex + 1

                            dvFormConfig = New DataView(dvSectionFields.ToTable())                                       'store the dataview as the dataview of the form config

                            Dim divColumn As New HtmlGenericControl("div")

                            If iMaxCols = 3 Then
                                divColumn.Attributes.Add("class", "col-sm-12 col-md-4")
                            ElseIf iMaxCols = 2 Then
                                divColumn.Attributes.Add("class", "col-sm-12 col-md-6")
                            Else
                                divColumn.Attributes.Add("class", "col-xs-12")
                            End If

                            'the column number is dynamic
                            dvFormConfig.RowFilter = "intRowNum = " & iRowNum & " and intColumnNum = " & iColNum & " and boolAOIField = " & _boolAOIField 'set the row filter
                            If dvFormConfig.Count > 0 Then
                                Dim divForm As New HtmlGenericControl("div")
                                divForm.Attributes.Add("class", "form-group")

                                Dim label As New HtmlGenericControl("label")

                                dr = dvFormConfig(0)

                                If dr("vcAssociatedControlType").ToString.ToLower = "checkbox" Then
                                    dr("vcAssociatedControlType") = "SelectBox"
                                    dr("vcListItemType") = "CHK"
                                    dr("numListID") = "0"
                                End If

                                strControlType = dr("vcAssociatedControlType").ToString.Replace(" ", "") 'replace whitespace with blank 
                                strFieldName = dr("vcDbColumnName").ToString.Trim.Replace(" ", "_") 'remove white space
                                strControlID = dr("numFormFieldID").ToString & "_" & dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")

                                label.InnerHtml = CStr(dr("vcNewFormFieldName"))
                                divForm.Controls.Add(Label)

                                intSearchCriteria = 0
                                strSearchCriteriaName = ""
                                If UserSearchCriteria.ContainsKey(strControlID) Then
                                    intSearchCriteria = UserSearchCriteria(strControlID)
                                    strSearchCriteriaName = [Enum].GetName(GetType(enmSearchCriteria), intSearchCriteria).Replace("_", " ")
                                End If

                                'Add Search Operator for All textbox field e.g. =,!=,>,<,null,empty
                                If lngFormID <> 17 And lngFormID <> 18 Then 'disable for cases and projects
                                    If strControlType = "TextBox" Or strControlType = "SelectBox" Or strControlType = "DateField" Then

                                        If strSearchCriteriaName.Length = 0 And intSearchCriteria = 0 Then
                                            If strControlType = "TextBox" Then
                                                If dr("vcFieldDataType") = "N" Or dr("vcFieldDataType") = "M" Then
                                                    strSearchCriteriaName = "Equal To "
                                                    intSearchCriteria = enmSearchCriteria.Equal_To_
                                                Else
                                                    strSearchCriteriaName = "Contains "
                                                    intSearchCriteria = enmSearchCriteria.Contains_
                                                End If
                                            ElseIf strControlType = "DateField" Then
                                                'If dr("vcFieldDataType") = "D" Then
                                                strSearchCriteriaName = "Equal To "
                                                intSearchCriteria = enmSearchCriteria._Equal_To_
                                                'End If
                                            ElseIf strControlType = "SelectBox" Then
                                                strSearchCriteriaName = "Equal To "
                                                intSearchCriteria = enmSearchCriteria._Equal_To
                                            End If
                                        End If

                                        Dim htmSpan As New HtmlControls.HtmlGenericControl
                                        htmSpan.TagName = "SPAN"
                                        htmSpan.ID = "spn_" & strControlID
                                        htmSpan.Attributes.Add("class", "SearchOperator")
                                        htmSpan.Attributes.Add("onclick", "SetFilterCriteria('" & strControlID & "','" & IIf(strControlType = "SelectBox", "S", dr("vcFieldDataType")) & "',event,this);")
                                        htmSpan.InnerHtml = strSearchCriteriaName
                                        divForm.Controls.Add(htmSpan)

                                        'set previous search criteria , and select that one by default when popup is opened
                                        Dim hdnSelectedSearchCriteria As New HiddenField
                                        hdnSelectedSearchCriteria.ID = "hdn_" & strControlID
                                        hdnSelectedSearchCriteria.Value = intSearchCriteria
                                        divForm.Controls.Add(hdnSelectedSearchCriteria)
                                    End If
                                End If


                                'Store Date Range into hidden following field
                                If strControlType = "DateField" Then
                                    Dim hdnDateValue As New HiddenField
                                    hdnDateValue.ID = "hdn_" & strControlID & "_value"
                                    hdnDateValue.Value = ""
                                    divForm.Controls.Add(hdnDateValue)
                                End If

                                'add the dynamic control to the table cell
                                divForm.Controls.Add(getDynamicControlAndDataBootstrap(Replace(Replace(Replace(dr("numFormFieldId"), "R", ""), "C", ""), "D", ""), strControlID, dr("vcListItemType"), strControlType, dr("numListID"), iRowIndex, iColNum))
                                divColumn.Controls.Add(divForm)
                            End If



                            divRow.Controls.Add(divColumn)
                        Next


                        divBoxBody.Controls.Add(divRow)                                             'add the new table row to the existing table
                    Next

                    divBox.Controls.Add(divBoxBody)
                    plhControls.Controls.Add(divBox)
                End If
            Next
        Else
            Dim divRow As New HtmlGenericControl("div")
            divRow.Attributes.Add("class", "row")

            Dim divColumn As New HtmlGenericControl("div")
            divColumn.Attributes.Add("class", "col-xs-12 text-aqua")
            divColumn.InnerText = "The screen is not created. Please use BizForm Wizard to create this screen."

            divRow.Controls.Add(divColumn)

            plhControls.Controls.Add(divRow)
        End If
        'Return teh data table
    End Function

    Public Function getDynamicControlAndDataBootstrap(ByVal numFormFieldId As Integer, ByVal vcDbColumnName As String, ByVal vcListItemType As String, ByVal vcAssociatedControlType As String, ByVal numListID As Integer, ByVal numRowNum As Integer, ByVal numColNum As Integer, Optional ByVal MaxLength As Integer = 50) As Object
        'declare a generalized object
        Dim oControlBox As Object

        If vcDbColumnName = "5R_vcProfile" Then
            vcAssociatedControlType = "CheckBoxList"
        End If


        Select Case vcAssociatedControlType.ToLower.Replace(" ", "")
            Case "textarea"                                                                         'Text Area is selected to be provided
                oControlBox = New TextBox                                                           'Instantiate a TextBox
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox                                             'set the height of the textbox
                oControlBox.TextMode = TextBoxMode.MultiLine                                        'This is a multiline textarea
                oControlBox.CssClass = "form-control"                                                     'Set the class attribute for the textbox control
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
            Case "selectbox"                                                                        'If the control to be used is a selectbox
                Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
                objConfigWizard.DomainID = DomainID                                                 'set the domain id
                objConfigWizard.ListItemType = vcListItemType                                       'set the listitem type
                oControlBox = New System.Web.UI.WebControls.DropDownList                                                       'instantiate a drop down
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                Dim dtTable As DataTable                                                            'declare a datatable
                dtTable = objConfigWizard.GetMasterListByListId(numListID)                          'call function to fill the datatable
                Dim dRow As DataRow                                                                 'declare a datarow
                dRow = dtTable.NewRow()                                                             'get a new row object
                dtTable.Rows.InsertAt(dRow, 0)                                                      'insert the row
                dtTable.Rows(0).Item("numItemID") = System.DBNull.Value                             'set the values for the first entry
                dtTable.Rows(0).Item("vcItemName") = "---Select One---"                             'set the group name
                If numListID = 176 Then 'Order status then add New value None (-1) which will search records with orderstatus as 0 or null
                    dRow = dtTable.NewRow()                                                         'get a new row object
                    dRow.Item("numItemID") = "0"
                    dRow.Item("vcItemName") = "---Null---"
                    dtTable.Rows.Add(dRow)                                                          'insert the row
                End If
                oControlBox.DataSource = dtTable                                                    'set the datasource of the drop down
                oControlBox.DataTextField = "vcItemName"                                            'Set the Text property of the drop down
                oControlBox.DataValueField = "numItemID"                                            'Set the value attribute of the textbox
                oControlBox.DataBind()                                                              'Databind the drop down
                oControlBox.CssClass = "form-control"                                                     'Set the class attribute for the selectbox control
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
            Case "checkboxlist"
                Dim div As New HtmlGenericControl("div")

                Dim chkbl As New CheckBoxList
                chkbl.ID = vcDbColumnName

                Dim objConfigWizard As New FormGenericFormContents
                objConfigWizard.DomainID = DomainID
                objConfigWizard.ListItemType = vcListItemType
                Dim dtTable As DataTable = objConfigWizard.GetMasterListByListId(numListID)

                For Each tempDr As DataRow In dtTable.Rows
                    Dim chkblItem As New ListItem
                    chkblItem.Text = CCommon.ToString(tempDr(dtTable.Columns(0).ColumnName))
                    chkblItem.Value = CCommon.ToString(tempDr(dtTable.Columns(1).ColumnName))
                    chkbl.Items.Add(chkblItem)
                Next

                chkbl.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))

                div.Controls.Add(chkbl)

                oControlBox = div
            Case "checkbox" 'Case sensetive                                                        'Checkbox control is requested
                oControlBox = New CheckBox                                                          'instantiate a new checkbox
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                oControlBox.CssClass = "text"                                                       'set the class attribute
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
                'Checkbox of Billing Address Same as Shpping needs scripting/ Requested by Carl on the 1st of Mar 2006
                If vcDbColumnName.IndexOf("bitSameAddr") > -1 Then  'If its a checkbox of Same Address for Billing/ Shiiping
                    oControlBox.Attributes.Add("onclick", "javascript: MakeAddressShippingSameAsBilling(this);") 'Attach Javascript
                End If
            Case "checkbox"                                                                        'Checkbox control is requested
                oControlBox = New CheckBox                                                          'instantiate a new checkbox
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                oControlBox.CssClass = "text"                                                       'set the class attribute
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
                'Checkbox of Billing Address Same as Shpping needs scripting/ Requested by Carl on the 1st of Mar 2006
                If vcDbColumnName.IndexOf("bitSameAddr") > -1 Then  'If its a checkbox of Same Address for Billing/ Shiiping
                    oControlBox.Attributes.Add("onclick", "javascript: MakeAddressShippingSameAsBilling(this);") 'Attach Javascript
                End If
            Case "radiobox"                                                                         'Radio button is requested
                oControlBox = New RadioButton                                                       'instantiate the radio
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                oControlBox.CssClass = "text"                                                       'set the class attribute
                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
            Case "datefield"
                Dim bizCalendar As BACRM.Include.calandar
                bizCalendar = CType(CType(HttpContext.Current.Handler, Page).LoadControl("~\include\calandar.ascx"), BACRM.Include.calandar)
                bizCalendar.ID = vcDbColumnName
                Return CType(bizCalendar, Object)
            Case "listbox"                                                                        'If the control to be used is a selectbox

                Dim div As New HtmlGenericControl("div")

                Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
                objConfigWizard.DomainID = DomainID                                                 'set the domain id
                objConfigWizard.ListItemType = vcListItemType                                       'set the listitem type
                Dim radComboBox As New Telerik.Web.UI.RadComboBox                                                       'instantiate a drop down
                radComboBox.ID = vcDbColumnName                                                     'set the name of the textbox
                radComboBox.CheckBoxes = True
                Dim dtTable As DataTable                                                            'declare a datatable
                dtTable = objConfigWizard.GetMasterListByListId(numListID)                          'call function to fill the datatable

                radComboBox.DataSource = dtTable                                                    'set the datasource of the drop down
                radComboBox.DataTextField = "vcItemName"                                            'Set the Text property of the drop down
                radComboBox.DataValueField = "numItemID"                                            'Set the value attribute of the textbox
                radComboBox.DataBind()                                                              'Databind the drop down
                radComboBox.CssClass = "ListBox"
                radComboBox.Width = New Unit(100, UnitType.Percentage)
                radComboBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index

                div.Controls.Add(radComboBox)

                oControlBox = div
            Case Else                                                                               'Edit Box is to be provided
                oControlBox = New TextBox                                                           'instantiate a new editbox
                oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                oControlBox.MaxLength = MaxLength                                                          'Set the Maximum character that can be entered
                oControlBox.CssClass = "form-control"                                                     'Set the class attribute for the editbox control

                oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
        End Select

        Return CType(oControlBox, Object)
    End Function


    Sub createFormControlsAOI(ByVal dtFormConfig As DataTable, ByVal div As HtmlGenericControl)

        Dim cbListDynamicAOIs As New CheckBoxList                                                   'instantiate a checkboxlist
        cbListDynamicAOIs.DataSource = dtFormConfig                                                 'set a datashource 
        cbListDynamicAOIs.DataTextField = "vcAOIName"                                      'set the text for the checkboxlist
        cbListDynamicAOIs.DataTextFormatString = "&nbsp;{0}"                                        'set the data text format
        cbListDynamicAOIs.DataValueField = "numAOIID"                                         'set the value attribute
        cbListDynamicAOIs.CssClass = "text"                                                         'set the class attribute
        cbListDynamicAOIs.DataBind()                                                                'databind to display the data
        cbListDynamicAOIs.ID = "cbListAOI"                                                       'set the AOI checkboxlist ids
        cbListDynamicAOIs.RepeatColumns = 3                                                         'display 3 columns
        cbListDynamicAOIs.RepeatDirection = RepeatDirection.Horizontal                              'display horizontally
        cbListDynamicAOIs.RepeatLayout = RepeatLayout.Table                                         'display in tabular format
        If cbListDynamicAOIs.Items.Count > 0 Then
            Dim divRow As New HtmlGenericControl("div")
            divRow.Attributes.Add("class", "row")

            Dim divColumn As New HtmlGenericControl("div")
            divColumn.Attributes.Add("class", "col-xs-12")

            Dim divFormGroup As New HtmlGenericControl("div")
            divFormGroup.Attributes.Add("class", "form-inline")

            Dim label As New HtmlGenericControl("label")
            label.InnerText = "Areas of Interest"

            divFormGroup.Controls.Add(label)

            divFormGroup.Controls.Add(cbListDynamicAOIs)

            div.Controls.Add(divRow)
        End If
    End Sub
End Module
