Imports MailBee.SmtpMail
Imports MailBee
Imports MailBee.Mime
Imports MailBee.Security
Imports BACRM.BusinessLogic.Common
Public Class clsSendEmail
    Private mailer As Smtp = Nothing
    Dim objCommon As New CCommon
    Public Function SendEmail(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String, ByVal dtTable As DataTable) As Boolean
        Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
        Try

            If HttpContext.Current.Session("SMTPServerIntegration") = False Then
                Exit Function
            End If

            Dim server As SmtpServer = New SmtpServer
            mailer = New Smtp
            mailer.Log.Enabled = True
            mailer.Log.Filename = "C:\aspNetEmail.log"
            mailer.Log.DisableOnException = False
            server.Name = HttpContext.Current.Session("SMTPServer")

            If HttpContext.Current.Session("SMTPAuth") = True Then
                server.AuthMethods = AuthenticationMethods.Auto
                server.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                server.AccountName = HttpContext.Current.Session("UserEmail")
                Dim strPassword As String
                strPassword = objCommon.Decrypt(HttpContext.Current.Session("SMTPPassword"))
                server.Password = strPassword
            End If
            If HttpContext.Current.Session("SMTPPort") <> 0 Then
                server.Port = HttpContext.Current.Session("SMTPPort")
            End If
            If HttpContext.Current.Session("bitSMTPSSL") = True Then
                server.SslMode = SslStartupMode.UseStartTls
            End If



            mailer.SmtpServers.Add(server)
            mailer.From.AsString = strFrom
            mailer.To.AsString = strTo
            If strCC <> "" Then
                mailer.Cc.AsString = strCC
            End If

            mailer.Subject = strSubject

            mailer.BodyHtmlText = strBody



            mailer.SendMailMerge(strFrom, Nothing, dtTable)
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function SendSimpleEmail(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String) As Boolean
        Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
        Try
            If HttpContext.Current.Session("SMTPServerIntegration") = False Then
                Exit Function
            End If
            Dim server As SmtpServer = New SmtpServer
            mailer = New Smtp
            mailer.Log.Enabled = True
            mailer.Log.Filename = "C:\aspNetEmail.log"
            mailer.Log.DisableOnException = False
            server.Name = HttpContext.Current.Session("SMTPServer")
            If HttpContext.Current.Session("SMTPAuth") = True Then
                server.AuthMethods = AuthenticationMethods.Auto
                server.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                server.AccountName = HttpContext.Current.Session("UserEmail")
                Dim strPassword As String
                strPassword = objCommon.Decrypt(HttpContext.Current.Session("SMTPPassword"))
                server.Password = strPassword
            End If
            If HttpContext.Current.Session("SMTPPort") <> 0 Then
                server.Port = HttpContext.Current.Session("SMTPPort")
            End If
            If HttpContext.Current.Session("bitSMTPSSL") = True Then
                server.SslMode = SslStartupMode.UseStartTls
            End If



            mailer.SmtpServers.Add(server)
            mailer.From.AsString = strFrom

            mailer.To.AsString = strTo
            If strCC <> "" Then
                mailer.Cc.AsString = strCC
            End If

            mailer.Subject = strSubject
            mailer.BodyHtmlText = strBody
            mailer.Send()

        Catch ex As Exception
            Return False
        End Try

    End Function

    'This event is raised before each row is merged into the EmailMessage object
   






End Class
