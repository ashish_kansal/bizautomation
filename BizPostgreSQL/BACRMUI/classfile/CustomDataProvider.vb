Imports System.Globalization
Imports System.Xml
Imports System.Xml.XPath
Imports Infragistics.WebUI.data
Imports Infragistics.WebUI.WebSchedule
Imports Infragistics.WebUI.Shared
Imports WebReference
Imports System.net
Imports System.DateTime
Imports System.Collections
Imports Microsoft.VisualBasic.DateAndTime
Imports System.Data
Imports BACRM.BusinessLogic.Outlook

Public Class CustomDataProvider
    Inherits WebScheduleDataProviderBase
    Implements IDataFetch, IDataUpdate
    Dim RecurId As Integer = -999
    Dim maxActivityId As Integer = 0
    Dim maxRecurrenceId As Integer = 0
    Dim ResourceId As Integer = 0
    Dim dtCalandar As New DataTable
    Dim esb As New ExchangeServiceBinding()

    Private dateFormat As New CultureInfo("en-GB", True)
    'Function esbCredentials() As ExchangeServiceBinding
    '    ServicePointManager.CertificatePolicy = New MyCertificateValidation
    '    esb.Url = HttpContext.Current.Session("ExchURL") & "/EWS/exchange.asmx"
    '    'esb.Credentials = New NetworkCredential(HttpContext.Current.Session("ExchUserName"), HttpContext.Current.Session("ExchPassword"), HttpContext.Current.Session("ExchDomain"))

    '    If HttpContext.Current.Session("AccessMethod") = True Then
    '        Dim ExchImpersonateAccc As New ExchangeImpersonationType
    '        Dim connectionSid As New ConnectingSIDType
    '        connectionSid.PrimarySmtpAddress = HttpContext.Current.Session("UserEmail")
    '        'connectionSid.PrimarySmtpAddress = "administrator@bizdemo.com"
    '        ExchImpersonateAccc.ConnectingSID = connectionSid
    '        esb.ExchangeImpersonation = ExchImpersonateAccc
    '    End If


    '    Return esb
    'End Function

    Public Sub Fetch(ByVal context As Infragistics.WebUI.WebSchedule.DataContext) Implements Infragistics.WebUI.WebSchedule.IDataFetch.Fetch

        Try

            ' Take action based on the operation provided
            Select Case context.Operation
                Case "FetchVariances"
                    FetchVariences(CType(context, FetchVariancesContext))
                Case "FetchResource"
                    FetchResources(CType(context, FetchResourcesContext))
                Case "FetchActivities"
                    FetchActivities(CType(context, FetchActivitiesContext))
                Case "FetchRecurrences"
                    FetchRecurrence(CType(context, FetchRecurrencesContext))
                Case "FetchReminders"
                    FetchRemainder(CType(context, FetchRemindersContext))
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub Update(ByVal context As Infragistics.WebUI.WebSchedule.DataContext) Implements Infragistics.WebUI.WebSchedule.IDataUpdate.Update

        Select Case (Context.Operation)


            Case "AddVariance"
                AddVariance(CType(Context, AddVarianceContext))
            Case "UpdateVariance"
                UpdateVariance(CType(Context, UpdateVarianceContext))
            Case "RemoveVariance"
                RemoveVariance(CType(Context, RemoveVarianceContext))
            Case "AddActivity"
                AddActivity(CType(Context, AddActivityContext))
            Case "DeleteActivity"
                RemoveActivity(CType(Context, RemoveActivityContext))
            Case "UpdateActivity"
                UpdateActivity(CType(Context, UpdateActivityContext))
            Case "AddRecurrence"
                AddRecurrence(CType(Context, AddRecurrenceContext))
            Case "UpdateRecurrence"
                UpdateRecurrence(CType(Context, UpdateRecurrenceContext))
            Case "RemoveRecurrence"
                RemoveRecurrence(CType(context, RemoveRecurrenceContext))
            Case "DeleteRecurrence"
                RemoveRecurrence(CType(context, RemoveRecurrenceContext))

            Case "UpdateSnooze"
                UpdateSnooze(CType(Context, UpdateSnoozeContext))
            Case "UpdateReminder"
                UpdateReminder(CType(Context, UpdateRemindersContext))

        End Select

    End Sub
    Private Sub FetchVariences(ByVal context As FetchVariancesContext)
        Try

        
            context.Variances.Clear()
            Dim ObjOutlook As New COutlook
            ObjOutlook.EndDateUtc = context.EndDate.Value
            ObjOutlook.ResourceName = context.Resource.Name
            ObjOutlook.StartDateTimeUtc = context.StartDate.Value
            Dim activity As Appointment
            For Each activity In context.Activities
             

                If activity.IsRecurring Then
                    ObjOutlook.RecurrenceKey = activity.RecurrenceKey
                    ObjOutlook.VarianceKey = activity.VarianceKey.ToString
                    
                    Dim dtTable As DataTable

                    dtTable = ObjOutlook.FetchVariance()

                    For Each row As DataRow In dtTable.Rows

                        Dim AppointmentVar As AppointmentVariance
                        'create varience context
                        AppointmentVar = activity.CreateVariance(New SmartDate(CDate(row.Item("OriginalStartDateTimeUtc"))), New SmartDate(CDate(row.Item("StartDateTimeUtc"))))

                        Dim description As String = IIf(IsDBNull(row.Item("ActivityDescription")), "", row.Item("ActivityDescription").ToString.Replace("~", "")) & "~" & row.Item("ItemId") & "~" & row.Item("ChangeKey")
                        AppointmentVar.Key = row.Item("ActivityID")
                        AppointmentVar.Subject = IIf(IsDBNull(row.Item("subject")), "", row.Item("subject"))
                        AppointmentVar.StartDateTimeUtc = New SmartDate(CDate(row.Item("StartDateTimeUtc")))
                        AppointmentVar.EndDateTimeUtc = New SmartDate(CDate(row.Item("StartDateTimeUtc")).AddSeconds(row.Item("Duration")))
                        AppointmentVar.Location = IIf(IsDBNull(row.Item("Location")), "", row.Item("Location"))
                        AppointmentVar.Description = description
                        AppointmentVar.Duration = New TimeSpan(0, 0, row.Item("Duration"))
                        AppointmentVar.ShowTimeAs = row.Item("ShowTimeAs")
                        AppointmentVar.AllDayEvent = row.Item("AllDayEvent")
                        AppointmentVar.EnableReminder = row.Item("EnableReminder")
                        AppointmentVar.ReminderInterval = New TimeSpan(0, 0, row.Item("ReminderInterval"))
                        AppointmentVar.Importance = row.Item("Importance")
                        AppointmentVar.ShowTimeAs = row.Item("ShowTimeAs")
                        AppointmentVar.Status = row.Item("Status")
                        AppointmentVar.RecurrenceKey = row.Item("RecurrenceID")
                        AppointmentVar.ResourceKey = context.Resource.DataKey
                        If row.Item("VarianceID").ToString() <> String.Empty Then
                            AppointmentVar.VarianceKey = row.Item("VarianceId")
                        End If
                        AppointmentVar.OriginalStartDateTimeUtc = New SmartDate(CDate(row.Item("OriginalStartDateTimeUtc")))
                        context.Variances.Add(AppointmentVar)
                    Next

                End If
            Next
        Catch ex As Exception
            Throw ex
        Finally
            Me.DataBindings.Clear()
        End Try
    End Sub

    Private Sub FetchResources(ByVal context As FetchResourcesContext)
        Try
            context.Resources.Clear()
            Dim ObjOutlook As New COutlook
            ObjOutlook.ResourceName = Me.WebScheduleInfo.ActiveResourceName
            Dim dtTable As DataTable
            dtTable = ObjOutlook.GetResourceByName()
            If dtTable.Rows.Count > 0 Then
                Dim Resource As New Resource
                ResourceId = dtTable.Rows(0).Item("ResourceID")
                Resource.Name = dtTable.Rows(0).Item("ResourceID")
                Resource.DataKey = dtTable.Rows(0).Item("ResourceID")
                Resource.Key = dtTable.Rows(0).Item("ResourceID")

                Resource.EnableReminders = DefaultableBoolean.True
                Resource.EmailAddress = "12@12.com"
                context.Resources.Add(Resource)
                context.ActiveResource = Resource
                HttpContext.Current.Session("ResourceID") = dtTable.Rows(0).Item("ResourceID")
                'Me.WebScheduleInfo.ActiveResourceName = dtTable.Rows(0).Item("ResourceName")
            End If
        Catch ex As Exception
            Throw ex
        Finally
            Me.DataBindings.Clear()
        End Try

    End Sub

    Private Sub FetchActivities(ByVal context As FetchActivitiesContext)
        Try

            Dim ObjOutlook As New COutlook

            ObjOutlook.StartDateTimeUtc = context.FrameInterval.StartDateUTC.Value
            ObjOutlook.EndDateUtc = context.FrameInterval.EndDateUTC.Value
            ObjOutlook.ResourceName = ResourceId
            'context.Activities.ScheduleInfo.ActiveResource.Key

            dtCalandar = ObjOutlook.FetchActivity()

            For Each Row As DataRow In dtCalandar.Rows
                Dim description As String = IIf(IsDBNull(Row.Item("ActivityDescription")), "", Row.Item("ActivityDescription").ToString.Replace("~", "")) & "~" & Row.Item("ItemId") & "~" & Row.Item("ChangeKey")
                Dim appointment As Appointment = New Appointment(Me.WebScheduleInfo)
                appointment.Key = Row.Item("ActivityID")
                appointment.Subject = IIf(IsDBNull(Row.Item("subject")), "", Row.Item("subject"))
                appointment.StartDateTimeUtc = New SmartDate(CDate(Row.Item("StartDateTimeUtc")))
                appointment.EndDateTimeUtc = New SmartDate(CDate(Row.Item("StartDateTimeUtc")).AddSeconds(Row.Item("Duration")))
                appointment.Location = IIf(IsDBNull(Row.Item("Location")), "", Row.Item("Location"))
                appointment.Description = description
                appointment.Duration = New TimeSpan(0, 0, Row.Item("Duration"))
                appointment.ShowTimeAs = Row.Item("ShowTimeAs")
                appointment.AllDayEvent = Row.Item("AllDayEvent")
                appointment.EnableReminder = Row.Item("EnableReminder")
                appointment.ReminderInterval = New TimeSpan(0, 0, Row.Item("ReminderInterval"))
                appointment.Importance = Row.Item("Importance")
                appointment.ShowTimeAs = Row.Item("ShowTimeAs")
                appointment.Status = Row.Item("Status")
                appointment.RecurrenceKey = Row.Item("RecurrenceID")
                appointment.ResourceKey = ResourceId
                If Not IsDBNull(Row.Item("VarianceId")) Then
                    appointment.VarianceKey = Row.Item("VarianceId")
                End If
                CType(context.Activities, IList).Add(appointment)
            Next
        Catch ex As Exception
            Throw ex
        Finally
            Me.DataBindings.Clear()
        End Try
    End Sub

    Private Sub FetchRecurrence(ByVal context As FetchRecurrencesContext)
        Try

     
            Dim ObjOutlook As New COutlook
            Dim dtRecurrence As DataTable
            ObjOutlook.StartDateTimeUtc = context.StartDate.Value
            ObjOutlook.EndDateUtc = context.EndDate.Value
            ObjOutlook.ResourceName = ResourceId
            'context.Resource.Name
            dtRecurrence = ObjOutlook.getRecurrence()
            For Each rowRecurrence As DataRow In dtRecurrence.Rows

                Dim RecurrenceObject As New Recurrence(Nothing)
                RecurrenceObject.Key = rowRecurrence.Item("RecurrenceID")
                RecurrenceObject.EndDateUtc = New SmartDate(CDate(rowRecurrence.Item("EndDateUtc")))
                RecurrenceObject.DayOfWeekMaskUtc = rowRecurrence.Item("DayOfWeekMaskUtc")
                Dim period As String
                period = rowRecurrence.Item("Period")
                Select Case period
                    Case "D"
                        RecurrenceObject.Period = RecurrencePeriod.Daily
                    Case "W"
                        RecurrenceObject.Period = RecurrencePeriod.Weekly
                    Case "M"
                        RecurrenceObject.Period = RecurrencePeriod.Monthly
                    Case "Y"
                        RecurrenceObject.Period = RecurrencePeriod.Yearly
                End Select
                RecurrenceObject.PeriodMultiple = rowRecurrence.Item("PeriodMultiple")
                RecurrenceObject.EditType = rowRecurrence.Item("EditType")
                RecurrenceObject.DayOfMonth = rowRecurrence.Item("DayOfMonth")
                RecurrenceObject.MonthOfYear = rowRecurrence.Item("MonthOfYear")
                RecurrenceObject.LastReminderDateTimeUtc = New SmartDate(CDate(rowRecurrence.Item("LastReminderDateTimeUtc")))
                RecurrenceObject.UtcOffset = New TimeSpan(0, 0, rowRecurrence.Item("UtcOffset"))

                context.Recurrences.Add(RecurrenceObject)
            Next
        Catch ex As Exception
            Throw ex
        Finally
            Me.DataBindings.Clear()
        End Try
    End Sub

    Private Sub FetchRemainder(ByVal context As FetchRemindersContext)
        Try

            Dim ObjOutlook As New COutlook
            Dim dtRemainder As DataTable
            ObjOutlook.ResourceID = context.Resource.Key
            ObjOutlook.LookAheadWindowEndTime = CType(context.ExpireTime.Value, Date).Add(context.LookAheadInterval)
            dtRemainder = ObjOutlook.getRemainders()

            Dim ResourceId As Integer = context.Resource.Key

         

            context.Activities.Clear()

            For Each row As DataRow In dtRemainder.Rows
                Dim description As String = IIf(IsDBNull(row.Item("ActivityDescription")), "", row.Item("ActivityDescription").ToString.Replace("~", "")) & "~" & row.Item("ItemId") & "~" & row.Item("ChangeKey")
                Dim appointment As Appointment = New Appointment(Me.WebScheduleInfo)

                appointment.ResourceKey = context.Resource.Key
                appointment.Key = row.Item("ActivityID")
                appointment.DataKey = row.Item("ActivityID").ToString
                appointment.Subject = IIf(IsDBNull(row.Item("subject")), "", row.Item("subject"))
                appointment.StartDateTimeUtc = New SmartDate(CDate(row.Item("StartDateTimeUtc")))
                appointment.EndDateTimeUtc = New SmartDate(CDate(row.Item("StartDateTimeUtc")).AddSeconds(row.Item("Duration")))
                appointment.Location = IIf(IsDBNull(row.Item("Location")), "", row.Item("Location"))
                appointment.Description = description
                appointment.Duration = New TimeSpan(0, 0, row.Item("Duration"))
                appointment.ShowTimeAs = row.Item("ShowTimeAs")
                appointment.AllDayEvent = row.Item("AllDayEvent")
                appointment.EnableReminder = row.Item("EnableReminder")
                appointment.ReminderInterval = New TimeSpan(0, 0, row.Item("ReminderInterval"))
                appointment.Importance = row.Item("Importance")
                appointment.ShowTimeAs = row.Item("ShowTimeAs")
                appointment.Status = row.Item("Status")
                appointment.Resource.Name = context.Resource.Key
                'appointment.RecurrenceKey = row.Item("RecurrenceID")

                '                context.Resource.EnableReminders = DefaultableBoolean.True

                appointment.OnItemBound()
                If (appointment.StartDateTimeUtc.Subtract(appointment.ReminderInterval) < SmartDate.Now.ToUniversalTime()) Then

                    
                    CType(context.Activities, IList).Add(appointment)

                End If
            Next
    
        Catch ex As Exception
            Throw ex
        Finally
            If Me.SnoozePersistenceType = Infragistics.WebUI.WebSchedule.SnoozePersistenceType.Cookie Then
                'Dim cookie As HttpCookie = Me.Page.Request.Cookies("Snuz_" + context.Resource.Name + "_" + Me.ID)
                Dim strSnzVals As String = HttpContext.Current.Session("Snuz_" + context.Resource.Name)
                If Not IsNothing(strSnzVals) Then
                    Me.EnlistSnoozesFromCookie(strSnzVals, context.SnoozeList)
                End If
            End If
        End Try
    End Sub
    Public Sub EnlistSnoozesFromCookie(ByVal str As String, ByVal SnoozeList As IList)
        Try
            Dim pieces As String() = str.Split("?")
            SnoozeList.Clear()
            Dim i As Integer = 0
            While i < pieces.Length - 1
                Dim chunk As String = pieces(i)
                Dim crumbs As String() = chunk.Split(":")
                If crumbs.Length = 3 Then
                    Dim snore As New Snooze
                    snore.ActivityKey = crumbs(0)
                    snore.Interval = TimeSpan.FromTicks(Long.Parse(crumbs(1)))
                    snore.LastSnooze = TimeSpan.FromTicks(Long.Parse(crumbs(2)))
                    SnoozeList.Add(snore)
                End If
                i += 1
            End While
        Catch ex As Exception

        End Try
    End Sub

   


    Private Sub AddVariance(ByVal context As AddVarianceContext)
        Try
            Dim appointment As Appointment = CType(context.Variance, AppointmentVariance)
            Dim ItemIdOccur As String = ""
            If Not appointment Is Nothing Then
                If Not appointment.RecurrenceKey = Nothing Then
                    If appointment.RecurrenceKey.Length > 0 Then
                        Dim objoutlook As New COutlook
                        Dim rootActivityID As Integer = -999
                        Dim strDesr As String() = appointment.Description.Split("~")
                        objoutlook.AllDayEvent = appointment.AllDayEvent
                        objoutlook.ActivityDescription = strDesr(0)
                        objoutlook.Duration = appointment.Duration.TotalSeconds
                        objoutlook.Location = appointment.Location
                        objoutlook.StartDateTimeUtc = appointment.StartDateTimeUtc.Value
                        objoutlook.Subject = appointment.Subject
                        objoutlook.EnableReminder = appointment.EnableReminder
                        objoutlook.ReminderInterval = appointment.ReminderInterval.TotalSeconds
                        objoutlook.ShowTimeAs = appointment.ShowTimeAs
                        objoutlook.Importance = appointment.Importance
                        objoutlook.Status = appointment.Status
                        objoutlook.RecurrenceKey = appointment.RecurrenceKey

                        ' objoutlook.OriginalStartDateTimeUtc = CDate(DateToString(appointment..OriginalStartDateTime))
                        objoutlook.ResourceName = HttpContext.Current.Session("ResourceID")
                        'context.OrganizerName
                        If strDesr.Length = 3 Then
                            objoutlook.ItemId = strDesr(1)
                            objoutlook.ChangeKey = strDesr(2)
                        Else
                            objoutlook.ItemId = " "
                            objoutlook.ChangeKey = " "
                        End If
                        


                        Dim correlationID As Guid = appointment.Recurrence.RootActivity.VarianceKey
                        If correlationID = Guid.Empty Then
                            correlationID = Guid.NewGuid
                            appointment.VarianceKey = correlationID
                            rootActivityID = Integer.Parse(appointment.Recurrence.RootActivity.Key)
                        Else
                            appointment.VarianceKey = correlationID
                        End If

                        objoutlook.VarianceKey = appointment.VarianceKey.ToString

                        objoutlook.OriginalStartDateTimeUtc = IIf(appointment.OriginalStartDateTimeUtc = Nothing, DBNull.Value, appointment.OriginalStartDateTimeUtc.Value)


                        Dim newActivityId As Integer

                     

                      
                        objoutlook.ItemIdOccur = ItemIdOccur
                        newActivityId = objoutlook.AddVarience()
                        If Not newActivityId = Nothing Then
                            appointment.DataKey = CInt(newActivityId)
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        Finally
            Me.DataBindings.Clear()
        End Try
    End Sub

    Private Sub UpdateVariance(ByVal context As UpdateVarianceContext)
        Try

            If TypeOf context.Variance Is AppointmentVariance Then
                '' Get a reference to the appointment being added
                Dim appointment As Appointment = CType(context.Variance, AppointmentVariance)
                Dim RecurringId As Integer = -999

                '' Adding apponitment data to DB
                Dim ObjOutlook As New COutlook
                Dim strDesr As String() = appointment.Description.Split("~")
                ObjOutlook.ActivityID = appointment.Key
                ObjOutlook.AllDayEvent = appointment.AllDayEvent
                ObjOutlook.Location = appointment.Location
                ObjOutlook.ActivityDescription = strDesr(0)
                ObjOutlook.Duration = appointment.Duration.TotalSeconds
                ObjOutlook.StartDateTimeUtc = appointment.StartDateTimeUtc.Value
                ObjOutlook.Subject = appointment.Subject
                ObjOutlook.EnableReminder = appointment.EnableReminder
                ObjOutlook.ReminderInterval = appointment.ReminderInterval.TotalSeconds
                ObjOutlook.ShowTimeAs = appointment.ShowTimeAs
                ObjOutlook.Importance = appointment.Importance
                ObjOutlook.Status = appointment.Status
                ObjOutlook.RecurrenceKey = appointment.RecurrenceKey
                ObjOutlook.OriginalStartDateTimeUtc = Parse(appointment.OriginalStartDateTimeUtc.ToString)
                ObjOutlook.UpdateVariance()

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub RemoveVariance(ByVal context As RemoveVarianceContext)

        Dim ObjOutlook As New COutlook
        If context.IsDeleteAll Then
            ObjOutlook.ActivityID = context.RootActivityKey()

            ObjOutlook.RemoveAllVarience()
        Else

            ' Dim ObjOutlook As New COutlook

            ObjOutlook.ActivityID = context.DataKey
            ObjOutlook.mode = 1


            ObjOutlook.Status = -1
            ObjOutlook.ActivityID = context.DataKey
            ObjOutlook.RemoveVarience()
        End If

    End Sub

    Private Sub UpdateReminder(ByVal context As UpdateRemindersContext)
        Try
            Dim ObjOutlook As New COutlook
            'ObjOutlook.ResourceID = context.ActiveResourceName

            'dv.RowFilter = "ActivityId = " & context.DataKey
            If context.LastReminderStartDateTimeUtc > New DateTime(1900, 1, 2) Then
                ObjOutlook.LastReminderDateTimeUtc = context.LastReminderStartDateTimeUtc
                ObjOutlook.RecurrenceKey = context.DataKey
                ObjOutlook.UpdateReminderByReccurence()
            Else
                ObjOutlook.ActivityID = context.DataKey
                ObjOutlook.Status = context.Status
                ObjOutlook.LastReminderDateTimeUtc = context.LastReminderStartDateTimeUtc
                ObjOutlook.UpdateReminderByActivityId()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UpdateSnooze(ByVal context As UpdateSnoozeContext)
        Dim snooze As Snooze = context.Snooze
        Dim snoozeList As IList = context.SnoozeList
        Dim sleepyType As SnoozePersistenceType = MyBase.SnoozePersistenceType
        If sleepyType = SnoozePersistenceType.Cookie Then
            'Dim cookie As HttpCookie = MyBase.Page.Request.Cookies("Snuz_" + context.ActiveResourceName + "_" + MyBase.ID)
            Dim str As String = HttpContext.Current.Session("Snuz_" + context.ActiveResourceName)
            Dim created As Boolean = False
            If str Is Nothing Then
                ' Cookie = New HttpCookie("Snuz_" + context.ActiveResourceName + "_" + MyBase.ID)
                created = True
            End If
            Dim snoozeString As String = snooze.ActivityKey + ":" + snooze.Interval.Ticks.ToString() + ":" + snooze.LastSnooze.Ticks.ToString() + "?"
            If Not created Then

                Dim index As Integer = str.IndexOf(snooze.ActivityKey)
                While index <> -1
                    Dim endIndex As Integer = str.IndexOf("?", index)
                    str = str.Remove(index, endIndex - index + 1)
                    index = str.IndexOf(snooze.ActivityKey)
                End While
            End If
            str += snoozeString
            HttpContext.Current.Session("Snuz_" + context.ActiveResourceName) = str
            'cookie.Expires = DateTime.Now.AddDays(14)
            'MyBase.Page.Response.Cookies.Add(cookie)
        ElseIf sleepyType = SnoozePersistenceType.ControlState Then
            snoozeList.Add(snooze)
        End If
    End Sub 'UpdateSnooze

    Sub AddRecurrence(ByVal context As AddRecurrenceContext)
        Try

            Dim appointment As Recurrence = context.Recurrence
            Dim ObjOutlook As New COutlook

            'Dim RecurringId As Integer
            ObjOutlook.EndDateUtc = appointment.EndDateUtc.Value
            ObjOutlook.DayOfWeekMaskUtc = appointment.DayOfWeekMaskUtc
            ObjOutlook.UtcOffset = appointment.UtcOffset.TotalSeconds
            ObjOutlook.DayOfMonth = appointment.DayOfMonth
            ObjOutlook.MonthOfYear = appointment.MonthOfYear
            ObjOutlook.Period = appointment.Period.ToString
            ObjOutlook.EditType = appointment.EditType

            ' Dim ObjOutlook As New COutlook

            ObjOutlook.ActivityID = context.Recurrence.RootActivity.DataKey
            ObjOutlook.mode = 1
           
            appointment.DataKey = ObjOutlook.AddRecurring
            context.DataKey = appointment.DataKey

            context.Recurrence.RootActivity.RecurrenceKey = context.DataKey

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Sub UpdateRecurrence(ByVal context As UpdateRecurrenceContext)
        Try
            Dim ObjOutlook As New COutlook

            ObjOutlook.EndDateUtc = context.Activity.EndDateTimeUtc.Value
            ObjOutlook.DayOfWeekMaskUtc = context.Activity.Recurrence.DayOfWeekMaskUtc
            ObjOutlook.UtcOffset = context.Activity.Recurrence.UtcOffset.TotalSeconds
            ObjOutlook.DayOfMonth = context.Activity.Recurrence.DayOfMonth
            ObjOutlook.MonthOfYear = context.Activity.Recurrence.MonthOfYear
            ObjOutlook.PeriodMultiple = context.Activity.Recurrence.PeriodMultiple
            Select Case context.Activity.Recurrence.Period
                Case "Daily"
                    ObjOutlook.Period = "D"
                Case "Weekly"
                    ObjOutlook.Period = "W"
                Case "Monthly"
                    ObjOutlook.Period = "M"
                Case "Yearly"
                    ObjOutlook.Period = "Y"
            End Select
            ObjOutlook.EditType = context.Activity.Recurrence.EditType
            ObjOutlook.LastReminderDateTimeUtc = context.Activity.Recurrence.LastReminderDateTimeUtc.Value
            ObjOutlook.RecurrenceKey = context.Activity.Recurrence.RecurrenceId
            ObjOutlook.UpdateRecurrence()
            ObjOutlook.Status = context.Activity.Status
            ObjOutlook.UpdateReminderByReccurence()

          
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub RemoveRecurrence(ByVal context As RemoveRecurrenceContext)
        Try

            Dim ObjOutlook As New COutlook

            ObjOutlook.ActivityID = context.ActivityKey
            ObjOutlook.RecurrenceKey = context.RecurrenceKey
            ObjOutlook.RemoveReccurence()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub AddActivity(ByVal context As AddActivityContext)
        Try

            If TypeOf context.Activity Is Appointment Then
                '' Get a reference to the appointment being added
                Dim appointment As Appointment = CType(context.Activity, Appointment)
                Dim RecurringId As Integer = -999
                Dim strDesr As String() = appointment.Description.Split("~")
                '' Adding apponitment data to DB
                Dim ObjOutlook As New COutlook
                If appointment.IsRecurring = True Then '' If appointment is recurring
                    ObjOutlook.EndDateUtc = Parse(appointment.Recurrence.EndDateUtc.ToString)
                    ObjOutlook.DayOfWeekMaskUtc = appointment.Recurrence.DayOfWeekMaskUtc
                    ObjOutlook.UtcOffset = appointment.Recurrence.UtcOffset.TotalSeconds
                    ObjOutlook.DayOfMonth = appointment.Recurrence.DayOfMonth
                    ObjOutlook.MonthOfYear = appointment.Recurrence.MonthOfYear
                    ObjOutlook.Period = appointment.Recurrence.Period.ToString
                    ObjOutlook.EditType = appointment.Recurrence.EditType
                    RecurringId = ObjOutlook.AddRecurring
                End If

                ObjOutlook.ResourceName = HttpContext.Current.Session("ResourceID")
                ObjOutlook.AllDayEvent = appointment.AllDayEvent
                ObjOutlook.Location = appointment.Location
                ObjOutlook.ActivityDescription = strDesr(0)
                ObjOutlook.Duration = appointment.Duration.TotalSeconds
                ObjOutlook.StartDateTimeUtc = appointment.StartDateTimeUtc.Value
                ObjOutlook.Subject = appointment.Subject
                ObjOutlook.EnableReminder = appointment.EnableReminder
                ObjOutlook.ReminderInterval = appointment.ReminderInterval.TotalSeconds
                ObjOutlook.ShowTimeAs = appointment.ShowTimeAs
                ObjOutlook.Importance = appointment.Importance
                ObjOutlook.Status = appointment.Status
                ObjOutlook.RecurrenceKey = RecurringId
                Dim i As Boolean = ObjOutlook.AddActivity()
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub RemoveActivity(ByVal context As RemoveActivityContext)
        Try

            Dim ObjOutlook As New COutlook
            ObjOutlook.ActivityID = context.DataKey
            ObjOutlook.mode = 0
            Dim dtTable As DataTable
            dtTable = ObjOutlook.getActivityByActId()

            If HttpContext.Current.Session("BtoGCalendar") = True Then
                If dtTable.Rows.Count > 0 Then
                    If Not IsDBNull(dtTable.Rows(0).Item("GoogleEventId")) Then
                        Dim objCalendar As New GoogleCalendar
                        objCalendar.GoogleActivityDelete(dtTable.Rows(0).Item("numContactID"), dtTable.Rows(0).Item("numDomainID"), dtTable.Rows(0).Item("ActivityID"))
                    End If
                End If
            End If

            ObjOutlook.RemoveActivity()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub UpdateActivity(ByVal context As UpdateActivityContext)
        Try

            If TypeOf context.Activity Is Appointment Then
                ' Get a reference to the Appointment that has been changed
                Dim appointment As Appointment = CType(context.Activity, Appointment)

                Dim ObjOutlook As New COutlook
                ObjOutlook.ResourceID = context.Activity.ResourceKey
                ' Dim max As Integer = context.Activity.Recurrence.RootActivity.Recurrence.MaxOccurrences




                Dim strDesr As String() = appointment.Description.Split("~")
                Dim RecurringId As Integer = -999
                ObjOutlook.ActivityID = appointment.Key
                ObjOutlook.AllDayEvent = appointment.AllDayEvent
                ObjOutlook.Location = appointment.Location
                ObjOutlook.ActivityDescription = strDesr(0)
                ObjOutlook.Duration = appointment.Duration.TotalSeconds
                ObjOutlook.StartDateTimeUtc = appointment.StartDateTimeUtc.Value
                ObjOutlook.Subject = appointment.Subject

                ObjOutlook.EnableReminder = appointment.EnableReminder
                ObjOutlook.ReminderInterval = appointment.ReminderInterval.TotalSeconds
                ObjOutlook.ShowTimeAs = appointment.ShowTimeAs
                ObjOutlook.Importance = appointment.Importance
                ObjOutlook.Status = appointment.Status
                If Not appointment.RecurrenceKey Is Nothing Then
                    ObjOutlook.RecurrenceKey = appointment.RecurrenceKey
                Else
                    ObjOutlook.RecurrenceKey = -999
                End If


                If Not appointment.VarianceKey = Guid.Empty Then
                    ObjOutlook.VarianceKey = appointment.VarianceKey.ToString
                End If
              
                ObjOutlook.UpdateActivity()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Helper methods"
    Public Function fn_GetDateTimeFromNumber(ByVal dtDate As Date, ByVal strFormat As String) As String
        Dim str4Yr As String = Microsoft.VisualBasic.DateAndTime.Year(dtDate)
        'Set the Year in 2 Digits to a Variable
        Dim str2Yr As String = Mid(Microsoft.VisualBasic.DateAndTime.Year(dtDate), 3, 2)
        'Set the Month in 2 Digits to a Variable
        Dim strIntMonth As String = Microsoft.VisualBasic.DateAndTime.Month(dtDate)
        'Set the Date in 2 Digits to a Variable
        Dim strDate As String = Microsoft.VisualBasic.DateAndTime.Day(dtDate)
        'Set the Abbrivated Month Name to a Variable
        Dim str3Month As String = MonthName(strIntMonth, True)
        'Set the Full Month Name to a Variable
        Dim strFullMonth As String = MonthName(strIntMonth, False)
        'Set the Nos of Hrs to a Variable
        Dim strHrs As String = Microsoft.VisualBasic.DateAndTime.Hour(dtDate)
        'Set the Nos of Mins to a Variable
        Dim strMins As String = Microsoft.VisualBasic.DateAndTime.Minute(dtDate)
        Dim strSecs As String = Microsoft.VisualBasic.DateAndTime.Second(dtDate)

        'As the Date Format will be one of the above mentioned formats,
        'we need to replace the required string to get the formatted date.
        strFormat = Replace(strFormat, "DD", strDate)
        strFormat = Replace(strFormat, "YYYY", str4Yr)
        strFormat = Replace(strFormat, "YY", str2Yr)
        strFormat = Replace(strFormat, "MM", strIntMonth)
        strFormat = Replace(strFormat, "MONTH", strFullMonth)
        strFormat = Replace(strFormat, "MON", str3Month)
        strFormat &= " " & IIf(strHrs > 12, CInt(strHrs) - 12, strHrs) & ":" & strMins & ":" & strSecs & " " & IIf(strHrs > 12, "PM", "AM")

        Return strFormat
    End Function

    Private Function StringToDate(ByVal dateString As String) As SmartDate
        Return New SmartDate(DateTime.Parse(dateString, dateFormat))
    End Function

    Private Function DateToString(ByVal dateValue As SmartDate) As String
        Return dateValue.Value.ToString(dateFormat)
    End Function
#End Region
End Class
