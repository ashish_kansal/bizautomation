Imports System
Imports System.Collections
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D

Namespace ASPNET.StarterKit.Chart

    '*********************************************************************
    '
    ' PieChart Class
    '
    ' This class uses GDI+ to render Pie Chart.
    '
    '*********************************************************************

    Public Class PieChart
        Inherits Chart
        Private _bufferSpace As Integer = 125
        Private _chartItems As ArrayList
        Private _perimeter As Integer
        Private _backgroundColor As Color
        Private _borderColor As Color
        Private _total As Single
        Private _legendWidth As Integer
        Private _legendHeight As Integer
        Private _legendFontHeight As Integer
        Private _legendFontStyle As String
        Private _legendFontSize As Single

        Public Sub New()
            _chartItems = New ArrayList()
            _perimeter = 200
            _backgroundColor = Color.White
            _borderColor = Color.FromArgb(63, 63, 63)
            _legendFontSize = 8
            _legendFontStyle = "normal"
        End Sub 'New

        Public Sub New(ByVal bgColor As Color)
            _chartItems = New ArrayList()
            _perimeter = 200
            _backgroundColor = bgColor
            _borderColor = Color.FromArgb(63, 63, 63)
            _legendFontSize = 8
            _legendFontStyle = "normal"
        End Sub 'New

        '*********************************************************************
        '
        ' This method collects all data points and calculate all the necessary dimensions 
        ' to draw the chart.  It is the first method called before invoking the Draw() method.
        '
        '*********************************************************************

        Public Sub CollectDataPoints(ByVal xValues() As String, ByVal yValues() As String)
            _total = 0.0F

            Dim i As Integer
            For i = 0 To xValues.Length - 1
                Dim ftemp As Single = Convert.ToSingle(yValues(i))
                _chartItems.Add(New ChartItem(xValues(i), xValues.ToString(), ftemp, 0, 0, Color.AliceBlue))
                _total += ftemp
            Next i

            Dim nextStartPos As Single = 0.0F
            Dim counter As Integer = 0
            Dim item As ChartItem
            For Each item In _chartItems
                item.StartPos = nextStartPos
                item.SweepSize = item.Value / _total * 360
                nextStartPos = item.StartPos + item.SweepSize
                counter = counter + 1
                item.ItemColor = GetColor(counter)
            Next
            CalculateLegendWidthHeight()
        End Sub 'CollectDataPoints

        '*********************************************************************
        '
        ' This method returns a bitmap to the calling function.  This is the method
        ' that actually draws the pie chart and the legend with it.
        '
        '*********************************************************************

        Public Overrides Function Draw() As Bitmap
            Dim perimeter As Integer = _perimeter
            Dim pieRect As New Rectangle(0, 80, perimeter, perimeter - 1)
            Dim bmp As New Bitmap(60 + _legendWidth, perimeter + 100)
            '' Dim bmp As New Bitmap(perimeter + _legendWidth, perimeter)
            Dim grp As Graphics = Nothing
            Dim sf As StringFormat = Nothing

            Try
                grp = Graphics.FromImage(bmp)
                sf = New StringFormat()

                'Paint Back ground
                grp.FillRectangle(New SolidBrush(_backgroundColor), 0, 0, perimeter + _legendWidth, perimeter + 200)
                'Align text to the right
                sf.Alignment = StringAlignment.Near

                'Draw all wedges and legends
                Dim i As Integer
                For i = 0 To _chartItems.Count - 1
                    Dim item As ChartItem = CType(_chartItems(i), ChartItem)
                    Dim brs As SolidBrush = Nothing
                    Try
                        brs = New SolidBrush(item.ItemColor)
                        grp.FillPie(brs, pieRect, item.StartPos, item.SweepSize)
                        grp.FillRectangle(brs, 5, i * _legendFontHeight + 10, 10, 10)
                        If i = 1 Then
                            ''grp.DrawString(item.Label & " (" & IIf(_total.ToString("G") > 0, Math.Round(item.Value.ToString("G") / _total.ToString("G") * 100, 2), 0) & "%)", New Font(_legendFontStyle, _legendFontSize), New SolidBrush(Color.Black), perimeter + 20, i * _legendFontHeight + 13)
                            grp.DrawString(item.Label & " (" & IIf(_total.ToString("G") > 0, Math.Round(item.Value.ToString("G") / _total.ToString("G") * 100, 2), 0) & "%)", New Font(_legendFontStyle, _legendFontSize), New SolidBrush(Color.Black), 15, i * _legendFontHeight + 10)
                        Else

                            '' grp.DrawString(item.Label, New Font(_legendFontStyle, _legendFontSize), New SolidBrush(Color.Black), perimeter + 20, i * _legendFontHeight + 13)
                            grp.DrawString(item.Label, New Font(_legendFontStyle, _legendFontSize), New SolidBrush(Color.Black), 15, i * _legendFontHeight + 10)
                        End If
                    Finally
                        If Not (brs Is Nothing) Then
                            brs.Dispose()
                        End If
                    End Try
                Next i
                'draws the border around Pie
                grp.DrawEllipse(New Pen(_borderColor, 2), pieRect)

                'draw border around legend
                ''  grp.DrawRectangle(New Pen(_borderColor, 1), perimeter, 10, 390, _chartItems.Count * _legendFontHeight + 10)
                grp.DrawRectangle(New Pen(_borderColor, 1), 0, 0, 380, _chartItems.Count * _legendFontHeight + 10)
                'Draw Total under legend
                ''grp.DrawString("Total", New Font(_legendFontStyle, _legendFontSize, FontStyle.Bold), New SolidBrush(Color.Black), perimeter + 10, (_chartItems.Count + 1) * _legendFontHeight, sf)
                ''grp.DrawString(_total.ToString("G") & "%", New Font(_legendFontStyle, _legendFontSize, FontStyle.Bold), New SolidBrush(Color.Black), perimeter + _bufferSpace + 210, (_chartItems.Count + 1) * _legendFontHeight, sf)

                grp.SmoothingMode = SmoothingMode.AntiAlias
            Finally
                If Not (sf Is Nothing) Then
                    sf.Dispose()
                End If
                If Not (grp Is Nothing) Then
                    grp.Dispose()
                End If
            End Try
            Return bmp
        End Function 'Draw

        '*********************************************************************
        '
        '	This method calculates the space required to draw the chart legend.
        '
        '*********************************************************************

        Private Sub CalculateLegendWidthHeight()
            Dim fontLegend As New Font(_legendFontStyle, _legendFontSize)
            _legendFontHeight = fontLegend.Height + 5
            _legendHeight = fontLegend.Height * (_chartItems.Count + 1)
            If _legendHeight > _perimeter Then
                _perimeter = _legendHeight
            End If
            _legendWidth = _perimeter + _bufferSpace
        End Sub 'CalculateLegendWidthHeight
    End Class 'PieChart
End Namespace 'ASPNET.StarterKit.Chart