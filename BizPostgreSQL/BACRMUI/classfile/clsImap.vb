Imports System.Web.HttpContext
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.admin
Imports BACRM.BusinessLogic.Outlook

Imports MailBee
Imports MailBee.Mime
Imports MailBee.Security
Imports MailBee.ImapMail
Public Class clsImap

    Private _ShowError As Boolean = False
    Private _ErrorMessage As String = ""

    Public Property ErrorMessage() As String
        Get
            Return _ErrorMessage
        End Get
        Set(ByVal Value As String)
            _ErrorMessage = Value
        End Set
    End Property

    Public Property ShowError() As Boolean
        Get
            Return _ShowError
        End Get
        Set(ByVal Value As Boolean)
            _ShowError = Value
        End Set
    End Property




    Public Function FetchRecent(ByVal UserContactId As Long, ByVal numDomainId As Long) As String
        Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
        Dim i As Long
        '"MN200-8C44445C440544494405E4C94F93-DC85"
        Dim imp As New Imap
        Try
            Dim lastUID As Long
            Dim dtTable As New DataTable
            Dim dtCredientials As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.UserCntID = UserContactId
            objUserAccess.DomainID = numDomainId
            dtCredientials = objUserAccess.GetImapDtls()
            Dim success As Boolean
            Dim ServerUrl As String
            Dim emailId As String
            Dim Password As String
            Dim UseUserName As Boolean

            If dtCredientials.Rows.Count > 0 Then
                If dtCredientials.Rows(0).Item("bitImap") = True Then
                    Dim strPassword As String
                    Dim objCommon As New CCommon
                    strPassword = objCommon.Decrypt(dtCredientials.Rows(0).Item("vcImapPassword"))
                    ServerUrl = dtCredientials.Rows(0).Item("vcImapServerUrl")
                    UseUserName = dtCredientials.Rows(0).Item("bitUseUserName")
                    emailId = dtCredientials.Rows(0).Item("vcEmailID")
                    Password = strPassword
                    lastUID = dtCredientials.Rows(0).Item("LastUID")
                    If dtCredientials.Rows(0).Item("bitSSl") = True Then
                        imp.SslMode = SslStartupMode.OnConnect
                    End If
                Else
                    Return "Imap Is not Integrated"
                    Exit Function
                End If
            Else
                Return "Imap Is not Integrated"
                Exit Function
            End If

            imp.Log.Enabled = True
            imp.Log.Filename = "C:\log.txt"
            Try
                success = imp.Connect(ServerUrl, dtCredientials.Rows(0).Item("numPort"))
            Catch ex1 As Exception
                _ShowError = True
                _ErrorMessage = ex1.Message
                Exit Function
            End Try

            If (success <> True) Then
                Return "Could Not Connect To the Server"
                Exit Function
            End If
            Try


                If UseUserName = True Then
                    success = imp.Login(emailId.Split("@")(0), Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)

                Else
                    success = imp.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)

                End If
            Catch ex2 As Exception
                _ShowError = True
                _ErrorMessage = ex2.Message
            End Try
            If (success <> True) Then
                Return "Invalid Credentials"
                Exit Function
            End If

            imp.ExamineFolder("Inbox")
            'Dim noofmessages As Long = imp.MessageCount
            'Dim strcondition As String = ""
            'If noofmessages - 20 > 0 Then
            '    strcondition = (noofmessages - 20) & ":" & noofmessages
            'Else
            '    strcondition = "1:" & noofmessages
            'End If

            Dim envelopes As EnvelopeCollection = imp.DownloadEnvelopes((lastUID + 1).ToString() + ":*", True, EnvelopeParts.MailBeeEnvelope Or EnvelopeParts.BodyStructure, 0)




            Dim objDR As System.Data.DataRow
            dtTable.Columns.Add("Id", GetType(Long))
            dtTable.Columns.Add("UId", GetType(Long))
            dtTable.Columns.Add("FromEmail", GetType(String))
            dtTable.Columns.Add("FromName", GetType(String))
            dtTable.Columns.Add("Subject", GetType(String))
            dtTable.Columns.Add("Sent", GetType(Date))
            dtTable.Columns.Add("Size", GetType(String))
            dtTable.Columns.Add("IsRead", GetType(Boolean))
            dtTable.Columns.Add("HasAttachments", GetType(Boolean))
            dtTable.Columns.Add("ToName", GetType(String))
            dtTable.Columns.Add("ToAddress", GetType(String))
            dtTable.Columns.Add("CCName", GetType(String))
            dtTable.Columns.Add("CCAddress", GetType(String))
            dtTable.Columns.Add("AttachmentName", GetType(String))
            dtTable.Columns.Add("AttachmentType", GetType(String))


            Dim j As Integer = 0

            For i = 0 To envelopes.Count - 1
                Dim env As Envelope = envelopes(i)
                Dim strBuffer As New System.Text.StringBuilder


                Dim filename As String = ""
                Dim filetype As String = ""
                Dim HasAttachments As Boolean = False
                Try
                    Dim parts As ImapBodyStructureCollection = env.BodyStructure.GetAllParts()
                    For Each part As ImapBodyStructure In parts
                        ' Detect if this part is attachment.
                        If (Not part.Disposition Is Nothing AndAlso part.Disposition.ToLower() = "attachment") OrElse _
                            (Not part.Filename Is Nothing AndAlso part.Filename <> String.Empty) OrElse _
                            (Not part.ContentType Is Nothing AndAlso part.ContentType.ToLower() = "message/rfc822") Then
                            If Not part.Filename Is Nothing Then
                                If part.Filename.Length > 0 Then
                                    If filename = "" Then
                                        filename = part.Filename
                                        filetype = part.ContentType
                                    Else
                                        filename = filename & "," & part.Filename
                                        filetype = filetype & "," & part.ContentType
                                    End If
                                    HasAttachments = True
                                End If
                            End If
                        End If
                    Next
                Catch ex As Exception
                    ExceptionModule.ExceptionPublish(ex, HttpContext.Current.Session("DomainID"), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                End Try

                Dim ToName As String = ""
                Dim ToAddress As String = ""
                Dim CCName As String = ""
                Dim CCAddress As String = ""


                For j = 0 To env.To.Count - 1
                    If ToName = "" Then
                        ToName = env.To(j).DisplayName
                    Else
                        ToName = ToName & "," & env.To(j).DisplayName
                    End If
                    If ToAddress = "" Then
                        ToAddress = env.To(j).Email
                    Else
                        ToAddress = ToAddress & "," & env.To(j).Email
                    End If

                Next
                j = 0
                For j = 0 To env.Cc.Count - 1
                    If CCName = "" Then
                        CCName = env.Cc(j).DisplayName
                    Else
                        CCName = CCName & env.Cc(j).DisplayName
                    End If
                    If CCAddress = "" Then
                        CCAddress = env.Cc(j).Email
                    Else
                        CCAddress = CCAddress & env.Cc(j).Email
                    End If
                Next
                objDR = dtTable.NewRow
                objDR("Id") = i + 1
                objDR("uid") = env.Uid
                objDR("FromEmail") = env.From.Email
                objDR("FromName") = env.From.DisplayName
                objDR("Subject") = env.Subject
                objDR("Sent") = env.DateReceived
                objDR("Size") = env.Size
                objDR("IsRead") = IIf(env.Flags.ToString.Contains("\Seen") = True, True, False)
                objDR("HasAttachments") = HasAttachments
                objDR("ToName") = ToName
                objDR("ToAddress") = ToAddress
                objDR("CCName") = CCName
                objDR("CCAddress") = CCAddress
                objDR("AttachmentName") = filename
                objDR("AttachmentType") = filetype

                dtTable.Rows.Add(objDR)
            Next



            Dim ds As New DataSet
            ds.Tables.Add(dtTable)
            ds.Tables(0).TableName = "Table"
            Dim strxml As String = ds.GetXml()
            ds.Tables.RemoveAt(0)
            imp.Disconnect()
            Dim ObjOutlook As New COutlook

            ObjOutlook.UserCntID = UserContactId
            ObjOutlook.DomainId = numDomainId
            ObjOutlook.strXml = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & strxml
            ObjOutlook.InsertImapNewMails()

            Return "Emails Fetched"
        Catch ex As Exception
            Throw ex
        Finally
            If imp.IsConnected Then
                imp.Disconnect()
            End If


        End Try
    End Function
    Public Function DeleteImap(ByVal numEmailHstrId As Long, ByVal numDomainID As Long) As Boolean
        Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
        Dim imp As New Imap
        Dim dtTableDtl As New DataTable
        Try
            Dim ObjOutlook As New COutlook
            Dim dtTable As DataTable

            ObjOutlook.DomainId = numDomainID
            ObjOutlook.numEmailHstrID = numEmailHstrId
            dtTable = ObjOutlook.getMail()
            If dtTable.Rows.Count > 0 Then
                If dtTable.Rows(0).Item("Source") = "I" Then
                    Dim objUserAccess As New UserAccess
                    Dim dtCredientials As DataTable
                    objUserAccess.UserCntID = dtTable.Rows(0).Item("numUserCntId")
                    objUserAccess.DomainID = numDomainID
                    dtCredientials = objUserAccess.GetImapDtls()
                    Dim success As Boolean
                    Dim ServerUrl As String
                    Dim emailId As String
                    Dim Password As String
                    If dtCredientials.Rows.Count > 0 Then
                        If dtCredientials.Rows(0).Item("bitImap") = True Then
                            Dim strPassword As String
                            Dim objCommon As New CCommon
                            strPassword = objCommon.Decrypt(dtCredientials.Rows(0).Item("vcImapPassword"))

                            ServerUrl = dtCredientials.Rows(0).Item("vcImapServerUrl")
                            emailId = dtCredientials.Rows(0).Item("vcEmailID")
                            Password = strPassword
                            If dtCredientials.Rows(0).Item("bitSSl") = True Then
                                imp.SslMode = SslStartupMode.OnConnect
                            End If
                        Else
                            Return False
                            Exit Function
                        End If
                    Else
                        Return False
                        Exit Function
                    End If
                    success = imp.Connect(ServerUrl, dtCredientials.Rows(0).Item("numPort"))
                    If (success <> True) Then
                        Return False
                        Exit Function
                    End If
                    success = imp.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                    imp.SelectFolder("Inbox")

                    If (success <> True) Then
                        Return False
                        Exit Function
                    End If
                    Return imp.DeleteMessages(dtTable.Rows(0).Item("numUId").ToString, True)
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function FetchSingle(ByVal numEmailHstrId As Long, ByVal numDomainID As Long) As DataTable

        Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
        Dim imp As New Imap
        Dim dtTableDtl As New DataTable
        Try
            Dim ObjOutlook As New COutlook
            Dim dtTable As DataTable

            ObjOutlook.DomainId = numDomainID
            ObjOutlook.numEmailHstrID = numEmailHstrId
            dtTable = ObjOutlook.getMail()
            If dtTable.Rows.Count > 0 Then
                Dim objUserAccess As New UserAccess
                Dim dtCredientials As DataTable
                objUserAccess.UserCntID = dtTable.Rows(0).Item("numUserCntId")
                objUserAccess.DomainID = numDomainID
                dtCredientials = objUserAccess.GetImapDtls()
                Dim success As Boolean
                Dim ServerUrl As String
                Dim emailId As String
                Dim Password As String
                If dtCredientials.Rows.Count > 0 Then
                    If dtCredientials.Rows(0).Item("bitImap") = True Then

                        Dim strPassword As String
                        Dim objCommon As New CCommon
                        strPassword = objCommon.Decrypt(dtCredientials.Rows(0).Item("vcImapPassword"))

                        ServerUrl = dtCredientials.Rows(0).Item("vcImapServerUrl")
                        emailId = dtCredientials.Rows(0).Item("vcEmailID")
                        Password = strPassword
                        If dtCredientials.Rows(0).Item("bitSSl") = True Then
                            imp.SslMode = SslStartupMode.OnConnect
                        End If
                    Else
                        Return dtTableDtl
                        Exit Function
                    End If
                Else
                    Return dtTableDtl
                    Exit Function
                End If

                success = imp.Connect(ServerUrl, dtCredientials.Rows(0).Item("numPort"))
                If (success <> True) Then
                    Return dtTableDtl
                    Exit Function
                End If
                success = imp.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                If (success <> True) Then
                    Return dtTableDtl
                    Exit Function
                End If

                imp.ExamineFolder("Inbox")

                Dim msg As MailMessage

                If imp.MessageCount > 0 Then
                    msg = imp.DownloadEntireMessage(dtTable.Rows(0).Item("numUId").ToString, True)
                    msg.Parser.HeadersAsHtml = True
                    msg.Parser.PlainToHtmlMode = PlainToHtmlAutoConvert.IfNoHtml
                    msg.Parser.PlainToHtmlOptions = PlainToHtmlConvertOptions.UriToLink
                    msg.Parser.Apply()
                End If

                ' Dim msg As MailMessage = imp.DownloadEntireMessage(dtTable.Rows(0).Item("numUId").ToString, True)


                If Not msg Is Nothing Then

                    ' imp.SetMessageFlags(dtTable.Rows(0).Item("numUId").ToString, True, SystemMessageFlags.Seen, MessageFlagAction.Replace, True)
                    Dim objDR As System.Data.DataRow
                    dtTableDtl.Columns.Add("FromEmail", GetType(String))
                    dtTableDtl.Columns.Add("FromName", GetType(String))
                    dtTableDtl.Columns.Add("Subject", GetType(String))
                    dtTableDtl.Columns.Add("Sent", GetType(Date))
                    dtTableDtl.Columns.Add("Size", GetType(String))
                    dtTableDtl.Columns.Add("HasAttachments", GetType(Boolean))
                    dtTableDtl.Columns.Add("ToName", GetType(String))
                    dtTableDtl.Columns.Add("ToAddress", GetType(String))
                    dtTableDtl.Columns.Add("CCName", GetType(String))
                    dtTableDtl.Columns.Add("CCAddress", GetType(String))
                    dtTableDtl.Columns.Add("AttachmentName", GetType(String))
                    dtTableDtl.Columns.Add("Body", GetType(String))
                    dtTableDtl.Columns.Add("BodyText", GetType(String))
                    Dim i As Long
                    Dim j As Integer = 0

                    objDR = dtTableDtl.NewRow


                    objDR("FromEmail") = msg.From.Email
                    objDR("FromName") = msg.From.DisplayName
                    objDR("Subject") = msg.Subject

                    'msg.Parser.HeadersAsHtml = True
                    'msg.Parser.PlainToHtmlMode = PlainToHtmlAutoConvert.IfNoHtml
                    'msg.Parser.PlainToHtmlOptions = PlainToHtmlConvertOptions.UriToLink
                    'msg.Parser.PlainToHtmlMode = PlainToHtmlAutoConvert.IfPlain
                    'msg.Parser.Apply()


                    objDR("Body") = msg.BodyHtmlText
                    objDR("BodyText") = msg.BodyPlainText
                    objDR("Sent") = msg.DateReceived
                    objDR("Size") = msg.Size

                    Dim ToName As String = ""
                    Dim ToAddress As String = ""
                    Dim CCName As String = ""
                    Dim CCAddress As String = ""

                    For j = 0 To msg.To.Count - 1
                        If ToName = "" Then
                            ToName = msg.To(j).DisplayName
                        Else
                            ToName = ToName & "," & msg.To(j).DisplayName
                        End If
                        If ToAddress = "" Then
                            ToAddress = msg.To(j).Email
                        Else
                            ToAddress = ToAddress & "," & msg.To(j).Email
                        End If

                    Next
                    j = 0
                    For j = 0 To msg.Cc.Count - 1
                        If CCName = "" Then
                            CCName = msg.Cc(j).DisplayName
                        Else
                            CCName = CCName & msg.Cc(j).DisplayName
                        End If
                        If CCAddress = "" Then
                            CCAddress = msg.Cc(j).Email
                        Else
                            CCAddress = CCAddress & msg.Cc(j).Email
                        End If
                    Next
                    objDR("HasAttachments") = IIf(msg.Attachments.Count = 0, False, True)

                    Dim strAttachment As String = ""
                    Dim strAttachmentType As String = ""
                    If msg.Attachments.Count > 0 Then
                        msg.Attachments.SaveAll(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\")
                        For j = 0 To msg.Attachments.Count - 1
                            Dim attach As Attachment = msg.Attachments(j)
                            If strAttachment = "" Then
                                strAttachment = attach.Filename
                                strAttachmentType = attach.ContentType
                            Else
                                strAttachment = strAttachment & "," & attach.Filename
                                strAttachmentType = strAttachmentType & "," & attach.ContentType
                            End If
                        Next

                    End If
                    objDR("ToName") = ToName
                    objDR("ToAddress") = ToAddress
                    objDR("CCName") = CCName
                    objDR("CCAddress") = CCAddress
                    objDR("AttachmentName") = strAttachment

                    dtTableDtl.Rows.Add(objDR)
                End If

            End If
            If imp.IsConnected Then
                imp.Disconnect()
            End If
            Return dtTableDtl
        Catch ex As Exception
            If imp.IsConnected Then
                imp.Disconnect()
            End If
            Throw ex
        End Try
    End Function

End Class
