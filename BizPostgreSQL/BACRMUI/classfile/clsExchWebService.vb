Imports WebReference
Imports System.net
Imports System.Reflection
Imports System.IO
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common
Public Class clsExchWebService

    Public Function Service(ByVal esb As ExchangeServiceBinding) As DataTable
        Dim dtNewMail As New DataTable
        Dim subscribeRequest As New SubscribeType()
        Dim pullSubscription As New PullSubscriptionRequestType()

        ' Identify the folders monitored for events.
        Dim folders(1) As BaseFolderIdType
        Dim folderId As DistinguishedFolderIdType
        folderId = New DistinguishedFolderIdType
        folderId.Id = DistinguishedFolderIdNameType.inbox

        folders(0) = folderId
        folderId = New DistinguishedFolderIdType
        folderId.Id = DistinguishedFolderIdNameType.sentitems
        folders(1) = folderId
        pullSubscription.FolderIds = folders

        ' Identify the events monitored for the subscription.
        Dim eventTypes(1) As NotificationEventTypeType
        eventTypes(0) = NotificationEventTypeType.NewMailEvent
        pullSubscription.EventTypes = eventTypes
        eventTypes(1) = NotificationEventTypeType.CreatedEvent
        pullSubscription.EventTypes = eventTypes

        ' Define the timeout period for the subscription.
        pullSubscription.Timeout = 10

        subscribeRequest.Item = pullSubscription

        ' Send the Subscribe request and receive the Subscribe response.
        Dim subscribeResponse As SubscribeResponseType = esb.Subscribe(subscribeRequest)

        ' Check the results.
        If (subscribeResponse.ResponseMessages.Items.Length > 0 And subscribeResponse.ResponseMessages.Items(0).ResponseClass = ResponseClassType.Success) Then

            Dim subscribeResponseMessage As SubscribeResponseMessageType = subscribeResponse.ResponseMessages.Items(0)
            ' Response.Write("Subscribed for Pull notifications: " & subscribeResponseMessage.SubscriptionId)

            ' Wait 30 seconds before receiving event notifications.
            'Thread.Sleep(30000)

            ' Create a new GetEvents request.
            Dim getEventsRequest As New GetEventsType()

            ' Identify the subscription identifier and watermark for the subscription 
            ' that will be polled for changes in the Exchange store.
            getEventsRequest.SubscriptionId = subscribeResponseMessage.SubscriptionId
            getEventsRequest.Watermark = subscribeResponseMessage.Watermark

            ' Send the GetEvents request and receive the GetEvents response.
            Dim eventsResponse As GetEventsResponseType = esb.GetEvents(getEventsRequest)

            ' Check the results.
            If (eventsResponse.ResponseMessages.Items.Length > 0 And eventsResponse.ResponseMessages.Items(0).ResponseClass = ResponseClassType.Success) Then
                Dim eventsResponseMessage As GetEventsResponseMessageType = eventsResponse.ResponseMessages.Items(0)
                For Each type As ItemsChoiceType In eventsResponseMessage.Notification.ItemsElementName
                    If type = ItemsChoiceType.NewMailEvent Then
                        Try
                            Dim dt As DataTable
                            dt = GetNewMails(eventsResponseMessage, esb)
                            dtNewMail.Merge(dt)
                        Catch ex As Exception
                            Throw ex
                        End Try

                    End If
                    If type = ItemsChoiceType.CreatedEvent Then

                    End If
                Next
            End If

            ' Create an Unsubscribe request.
            Dim unsubscribeRequest As New UnsubscribeType()

            ' Identify the subscription to unsubscribe from.
            unsubscribeRequest.SubscriptionId = subscribeResponseMessage.SubscriptionId

            ' Send the Unsubscribe request and receive the Unsubscribe response.
            Dim unsubscribeResponse As UnsubscribeResponseType = esb.Unsubscribe(unsubscribeRequest)

            ' Check the results
            If (unsubscribeResponse.ResponseMessages.Items.Length > 0 And unsubscribeResponse.ResponseMessages.Items(0).ResponseClass = ResponseClassType.Success) Then
                ' Response.Write("Subscribtionunsubscribed successfully: " & unsubscribeRequest.SubscriptionId)
            End If
        End If
        Return dtNewMail
    End Function

    ' Function To get a Details Of a Message From ItemId and ChangeKey
    Public Function GetMails(ByVal esb As ExchangeServiceBinding, ByVal ItemId As String, ByVal ChangeKey As String) As DataTable

        Dim dtTable As New DataTable
        Dim objDR As System.Data.DataRow
        Dim destinationPath As String = "c:\InetPub\wwwroot\BacrmPortal\Documents\Docs\"
        dtTable.Columns.Add("ToEmail", GetType(String))
        dtTable.Columns.Add("ToName", GetType(String))
        dtTable.Columns.Add("FromEmail", GetType(String))
        dtTable.Columns.Add("FromName", GetType(String))
        dtTable.Columns.Add("CCEmail", GetType(String))
        dtTable.Columns.Add("ccName", GetType(String))
        dtTable.Columns.Add("Subject", GetType(String))
        dtTable.Columns.Add("Body", GetType(String))
        dtTable.Columns.Add("Sent", GetType(Date))
        dtTable.Columns.Add("IsRead", GetType(Boolean))
        dtTable.Columns.Add("Size", GetType(String))
        dtTable.Columns.Add("HasAttachments", GetType(Boolean))
        dtTable.Columns.Add("AttachmentType", GetType(String))
        dtTable.Columns.Add("AttachmentPath", GetType(String))
        dtTable.Columns.Add("Source", GetType(Char))
        dtTable.Columns.Add("ItemId", GetType(String))
        dtTable.Columns.Add("Changekey", GetType(String))

        Dim getItemRequest As New GetItemType
        Dim getItemIDType(0) As ItemIdType

        Dim gettemResponseShapeType As New ItemResponseShapeType
        gettemResponseShapeType.BaseShape = DefaultShapeNamesType.AllProperties
        gettemResponseShapeType.IncludeMimeContent = True
        getItemRequest.ItemShape = gettemResponseShapeType

        getItemIDType(0) = New ItemIdType
        getItemIDType(0).Id = ItemId
        getItemIDType(0).ChangeKey = ChangeKey
        getItemRequest.ItemIds = getItemIDType

        Dim getItemresponse As GetItemResponseType
        getItemresponse = esb.GetItem(getItemRequest)

        For Each message As ItemInfoResponseMessageType In getItemresponse.ResponseMessages.Items
            If Not message.Items Is Nothing Then
                Dim item As ItemType = message.Items.Items(0)
                Dim cc As String = ""
                Dim to1 As String = ""
                Dim ccname As String = ""
                Dim toname As String = ""
                Dim z1 As MessageType
                z1 = CType(item, MessageType)
                Dim i As Integer = 0

                If z1.DisplayTo <> "" Then
                    For i = 0 To z1.ToRecipients.Length - 1
                        If Not i = z1.ToRecipients.Length - 1 Then
                            to1 = to1 & z1.ToRecipients(i).EmailAddress & ","
                        Else
                            to1 = to1 & z1.ToRecipients(i).EmailAddress
                        End If

                        If Not i = z1.ToRecipients.Length - 1 Then
                            toname = toname & z1.ToRecipients(i).Name & ","
                        Else
                            toname = toname & z1.ToRecipients(i).Name
                        End If
                    Next i
                End If
                If z1.DisplayCc <> "" Then

                    For i = 0 To z1.CcRecipients.Length - 1
                        If Not i = z1.CcRecipients.Length - 1 Then
                            cc = cc & z1.CcRecipients(i).EmailAddress & ","
                        Else
                            cc = cc & z1.CcRecipients(i).EmailAddress
                        End If

                        If Not i = z1.CcRecipients.Length - 1 Then
                            ccname = ccname & z1.CcRecipients(i).Name & ","
                        Else
                            ccname = ccname & z1.CcRecipients(i).Name
                        End If
                        'i = i + 1
                    Next i
                End If


                objDR = dtTable.NewRow

                If item.HasAttachments = True Then
                    If item.Attachments.Length > 0 Then
                        If Not item.Attachments.Equals(Nothing) Then
                            Dim attachmentIds As New List(Of RequestAttachmentIdType)
                            Dim attachmentIndex As Integer = 0
                            Dim strAttachmentType As String = ""
                            For attachmentIndex = 0 To item.Attachments.Length - 1
                                Dim almostAnAttachment As New FileAttachmentType
                                almostAnAttachment = item.Attachments(attachmentIndex)
                                If Not almostAnAttachment.Equals(Nothing) Then
                                    Dim requestId As RequestAttachmentIdType = New RequestAttachmentIdType
                                    requestId.Id = almostAnAttachment.AttachmentId.Id
                                    strAttachmentType = strAttachmentType & "," & almostAnAttachment.ContentType
                                    objDR("AttachmentType") = strAttachmentType
                                    attachmentIds.Add(requestId)
                                End If
                            Next
                            Dim getAttachmentRequest As GetAttachmentType = New GetAttachmentType
                            getAttachmentRequest.AttachmentShape = New AttachmentResponseShapeType
                            getAttachmentRequest.AttachmentIds = attachmentIds.ToArray
                            Dim getAttachmentResponse As GetAttachmentResponseType = esb.GetAttachment(getAttachmentRequest)
                            Dim AttachmentPath As String
                            Dim AttachmentName As String = ""
                            For Each attachmentResponseMessage As AttachmentInfoResponseMessageType In getAttachmentResponse.ResponseMessages.Items

                                If attachmentResponseMessage.ResponseCode = ResponseCodeType.NoError Then
                                    Dim fileAttachment As New FileAttachmentType
                                    fileAttachment = attachmentResponseMessage.Attachments(0)
                                    Dim file As New FileStream(Path.Combine(destinationPath, fileAttachment.Name), FileMode.Create)
                                    Try
                                        file.Write(fileAttachment.Content, 0, fileAttachment.Content.Length)
                                        file.Flush()
                                        file.Close()
                                        AttachmentName = AttachmentName & fileAttachment.Name & ","
                                        'objDR("AttachmentPath") = fileAttachment.Name
                                    Finally
                                        CType(file, IDisposable).Dispose()
                                    End Try
                                End If
                            Next
                            objDR("AttachmentPath") = AttachmentName
                        End If
                    End If
                End If
                objDR("ToEmail") = to1
                objDR("ToName") = toname
                objDR("CCEmail") = cc
                objDR("ccName") = ccname
                objDR("FromEmail") = z1.From.Item.EmailAddress
                objDR("FromName") = z1.From.Item.Name
                objDR("Subject") = item.Subject
                objDR("Body") = item.Body.Value
                objDR("Sent") = item.DateTimeSent
                objDR("IsRead") = z1.IsRead
                objDR("Size") = SetBytes(item.Size)
                objDR("Source") = "O"
                objDR("HasAttachments") = item.HasAttachments
                objDR("Itemid") = item.ItemId.Id
                objDR("Changekey") = item.ItemId.ChangeKey
                dtTable.Rows.Add(objDR)
            End If
        Next
        Return dtTable

    End Function

    Public Function GetNewMails(ByVal eventsResponseMessage As GetEventsResponseMessageType, ByVal esb As ExchangeServiceBinding) As DataTable
        Try
            Dim dtTable As New DataTable
            Dim objDR As System.Data.DataRow
            Dim destinationPath As String = "E:\inbox\"
            dtTable.Columns.Add("To", GetType(String))
            dtTable.Columns.Add("ToName", GetType(String))
            dtTable.Columns.Add("cc", GetType(String))
            dtTable.Columns.Add("ccName", GetType(String))
            dtTable.Columns.Add("Subject", GetType(String))
            dtTable.Columns.Add("Body", GetType(String))
            dtTable.Columns.Add("created", GetType(Date))
            dtTable.Columns.Add("IsRead", GetType(Boolean))
            dtTable.Columns.Add("size", GetType(Integer))
            dtTable.Columns.Add("HasAttachments", GetType(Boolean))
            dtTable.Columns.Add("itemid", GetType(String))
            dtTable.Columns.Add("changekey", GetType(String))
            Dim notification As NotificationType
            notification = eventsResponseMessage.Notification
            Dim eventChangedTye As BaseObjectChangedEventType
            eventChangedTye = notification.Items(0)
            Dim MailItemIdType As ItemIdType
            MailItemIdType = eventChangedTye.Item
            'Response.Write(MailItemIdType.Id)

            Dim getItemRequest As New GetItemType
            Dim getItemIDType(0) As ItemIdType

            Dim gettemResponseShapeType As New ItemResponseShapeType
            gettemResponseShapeType.BaseShape = DefaultShapeNamesType.AllProperties
            gettemResponseShapeType.IncludeMimeContent = True
            getItemRequest.ItemShape = gettemResponseShapeType

            getItemIDType(0) = New ItemIdType
            getItemIDType(0).Id = MailItemIdType.Id
            getItemIDType(0).ChangeKey = MailItemIdType.ChangeKey
            getItemRequest.ItemIds = getItemIDType

            Dim getItemresponse As GetItemResponseType
            getItemresponse = esb.GetItem(getItemRequest)

            For Each message As ItemInfoResponseMessageType In getItemresponse.ResponseMessages.Items
                Dim item As ItemType = message.Items.Items(0)
                Dim cc As String = ""
                Dim to1 As String = ""
                Dim ccname As String = ""
                Dim toname As String = ""
                Dim z As MessageType
                z = CType(item, MessageType)
                Dim i As Integer = 0

                For i = 0 To z.CcRecipients.Length - 1
                    cc = cc & z.CcRecipients(i).EmailAddress & ","
                    ccname = ccname & z.CcRecipients(i).Name & ","

                Next i
                For i = 0 To z.ToRecipients.Length - 1
                    to1 = to1 & z.ToRecipients(i).EmailAddress & ","
                    toname = toname & z.ToRecipients(i).Name & ","
                    'Response.Write("to mail id :" & z.ToRecipients(i).EmailAddress & "<br>")
                    'Response.Write("to name :" & z.ToRecipients(i).Name & "<br>")

                Next i
                objDR = dtTable.NewRow
                objDR("to") = to1
                objDR("ToName") = toname
                objDR("cc") = cc
                objDR("ccName") = ccname
                objDR("Subject") = z.Subject
                objDR("Body") = item.Body.Value
                objDR("created") = z.DateTimeReceived
                objDR("IsRead") = z.IsRead
                objDR("size") = z.Size
                objDR("HasAttachments") = z.HasAttachments
                objDR("itemid") = z.ItemId.Id
                objDR("changekey") = z.ItemId.ChangeKey
                dtTable.Rows.Add(objDR)
                'If item.HasAttachments = True Then
                '    If item.Attachments.Length > 0 Then
                '        If Not item.Attachments.Equals(Nothing) Then
                '            Dim attachmentIds As New List(Of RequestAttachmentIdType)
                '            Dim attachmentIndex As Integer = 0
                '            For attachmentIndex = 0 To item.Attachments.Length - 1
                '                Dim almostAnAttachment As New FileAttachmentType
                '                almostAnAttachment = item.Attachments(attachmentIndex)
                '                If Not almostAnAttachment.Equals(Nothing) Then
                '                    Dim requestId As RequestAttachmentIdType = New RequestAttachmentIdType
                '                    requestId.Id = almostAnAttachment.AttachmentId.Id
                '                    attachmentIds.Add(requestId)
                '                End If
                '            Next
                '            Dim getAttachmentRequest As GetAttachmentType = New GetAttachmentType
                '            getAttachmentRequest.AttachmentShape = New AttachmentResponseShapeType
                '            getAttachmentRequest.AttachmentIds = attachmentIds.ToArray
                '            Dim getAttachmentResponse As GetAttachmentResponseType = esb.GetAttachment(getAttachmentRequest)
                '            For Each attachmentResponseMessage As AttachmentInfoResponseMessageType In getAttachmentResponse.ResponseMessages.Items
                '                If attachmentResponseMessage.ResponseCode = ResponseCodeType.NoError Then
                '                    Dim fileAttachment As New FileAttachmentType
                '                    fileAttachment = attachmentResponseMessage.Attachments(0)
                '                    Dim file As New FileStream(Path.Combine(destinationPath, fileAttachment.Name), FileMode.Create)
                '                    Try
                '                        file.Write(fileAttachment.Content, 0, fileAttachment.Content.Length)
                '                        file.Flush()
                '                        file.Close()
                '                    Finally
                '                        CType(file, IDisposable).Dispose()
                '                    End Try
                '                End If
                '            Next
                '        End If
                '    End If
                'End If
            Next
            Return dtTable
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function GetSentItems(ByVal esb As ExchangeServiceBinding, ByVal frmdate As String, ByVal Email As String, ByVal DateFormat As String) As DataTable
        Try

            Dim dtTable As New DataTable
            Dim objDR As System.Data.DataRow
            Dim destinationPath As String = "c:\InetPub\wwwroot\BacrmPortal\Documents\Docs\"
            dtTable.Columns.Add("Count", GetType(String))
            dtTable.Columns.Add("ToEmail", GetType(String))
            dtTable.Columns.Add("ToName", GetType(String))
            dtTable.Columns.Add("FromEmail", GetType(String))
            dtTable.Columns.Add("FromName", GetType(String))
            dtTable.Columns.Add("CCEmail", GetType(String))
            dtTable.Columns.Add("ccName", GetType(String))
            dtTable.Columns.Add("Subject", GetType(String))
            dtTable.Columns.Add("Category", GetType(String))
            dtTable.Columns.Add("Body", GetType(String))
            dtTable.Columns.Add("Sent", GetType(Date))
            dtTable.Columns.Add("IsRead", GetType(Boolean))
            dtTable.Columns.Add("Size", GetType(String))
            dtTable.Columns.Add("HasAttachments", GetType(Boolean))
            dtTable.Columns.Add("AttachmentType", GetType(String))
            dtTable.Columns.Add("AttachmentPath", GetType(String))
            dtTable.Columns.Add("SentFrom", GetType(Char))
            dtTable.Columns.Add("ItemId", GetType(String))
            dtTable.Columns.Add("Changekey", GetType(String))

            ''Adding a header field to SOAP request to identify the user
            'Dim ExchImpersonateAccc As New ExchangeImpersonationType
            'Dim connectionSid As New ConnectingSIDType
            'connectionSid.PrimarySmtpAddress = Email
            'ExchImpersonateAccc.ConnectingSID = connectionSid
            'esb.ExchangeImpersonation = ExchImpersonateAccc


            Dim findItemRequest As New FindItemType()
            findItemRequest.Traversal = ItemQueryTraversalType.Shallow


            Dim itemProperties As New ItemResponseShapeType()
            itemProperties.BaseShape = DefaultShapeNamesType.AllProperties


            Dim folderIDArray(1) As DistinguishedFolderIdType
            folderIDArray(0) = New DistinguishedFolderIdType()

            folderIDArray(0).Id = DistinguishedFolderIdNameType.sentitems
            folderIDArray(0).Mailbox = New EmailAddressType()
            folderIDArray(0).Mailbox.EmailAddress = Email

            findItemRequest.ParentFolderIds = folderIDArray

            findItemRequest.ItemShape = itemProperties

            'Applying restriction of datetime Created
            Dim restriction As New RestrictionType

            Dim FieldURIOrConstantType As New FieldURIOrConstantType
            Dim contstantvalue As New ConstantValueType
            Dim frmdate1 As DateTime = CDate(frmdate)

            contstantvalue.Value = frmdate1.ToUniversalTime().ToString 'constantvalue of date time
            FieldURIOrConstantType.Item = contstantvalue

            Dim URI As New PathToUnindexedFieldType
            URI.FieldURI = UnindexedFieldURIType.itemDateTimeSent                  'path of the Property on which restriction had to be applied

            Dim IsGreaterThanOrEqualToType As New IsGreaterThanOrEqualToType
            IsGreaterThanOrEqualToType.FieldURIOrConstant = FieldURIOrConstantType
            IsGreaterThanOrEqualToType.Item = URI
            restriction.Item = CType(IsGreaterThanOrEqualToType, SearchExpressionType)

            findItemRequest.Restriction = restriction


            Dim findItemResponse As FindItemResponseType = esb.FindItem(findItemRequest)

            Dim count As Integer = 1
            Dim findItemMsgResponse As New FindItemResponseMessageType
            findItemMsgResponse = findItemResponse.ResponseMessages.Items(0)
            If findItemResponse.ResponseMessages.Items(0).ResponseClass = ResponseClassType.Success Then
                Dim ItemDtls As ArrayOfRealItemsType = findItemMsgResponse.RootFolder.Item
                Dim ItemDtlsCount As Integer = 1
                If Not IsNothing(ItemDtls.Items) Then
                    For ItemDtlsCount = 0 To ItemDtls.Items.Length - 1
                        Dim z As MessageType
                        z = CType(ItemDtls.Items(ItemDtlsCount), MessageType)


                        Dim i As Integer = 0

                        Dim getItemRequest As New GetItemType
                        Dim getItemIDType(0) As ItemIdType

                        Dim gettemResponseShapeType As New ItemResponseShapeType
                        gettemResponseShapeType.BaseShape = DefaultShapeNamesType.AllProperties
                        gettemResponseShapeType.IncludeMimeContent = True
                        getItemRequest.ItemShape = gettemResponseShapeType

                        getItemIDType(0) = New ItemIdType
                        getItemIDType(0).Id = z.ItemId.Id
                        getItemIDType(0).ChangeKey = z.ItemId.ChangeKey
                        getItemRequest.ItemIds = getItemIDType

                        Dim getItemresponse As GetItemResponseType
                        getItemresponse = esb.GetItem(getItemRequest)
                        For Each message As ItemInfoResponseMessageType In getItemresponse.ResponseMessages.Items
                            Dim item As ItemType = message.Items.Items(0)
                            Dim cc As String = ""
                            Dim to1 As String = ""
                            Dim ccname As String = ""
                            Dim toname As String = ""
                            Dim z1 As MessageType
                            z1 = CType(item, MessageType)
                            Dim i1 As Integer = 0
                            For i = 0 To z1.ToRecipients.Length - 1
                                If Not i = z1.ToRecipients.Length - 1 Then
                                    to1 = to1 & z1.ToRecipients(i).EmailAddress & ","
                                Else
                                    to1 = to1 & z1.ToRecipients(i).EmailAddress
                                End If

                                If Not i = z1.ToRecipients.Length - 1 Then
                                    toname = toname & z1.ToRecipients(i).Name & ","
                                Else
                                    toname = toname & z1.ToRecipients(i).Name
                                End If
                            Next i

                            If z1.DisplayCc <> "" Then

                                For i = 0 To z1.CcRecipients.Length - 1
                                    If Not i = z1.CcRecipients.Length - 1 Then
                                        cc = cc & z1.CcRecipients(i).EmailAddress & ","
                                    Else
                                        cc = cc & z1.CcRecipients(i).EmailAddress
                                    End If

                                    If Not i = z1.CcRecipients.Length - 1 Then
                                        ccname = ccname & z1.CcRecipients(i).Name & ","
                                    Else
                                        ccname = ccname & z1.CcRecipients(i).Name
                                    End If
                                    'i = i + 1
                                Next i
                            End If


                            objDR = dtTable.NewRow


                            If item.HasAttachments = True Then
                                If item.Attachments.Length > 0 Then
                                    If Not item.Attachments.Equals(Nothing) Then
                                        Dim attachmentIds As New List(Of RequestAttachmentIdType)
                                        Dim attachmentIndex As Integer = 0
                                        Dim strAttachmentType As String = ""
                                        For attachmentIndex = 0 To item.Attachments.Length - 1
                                            Dim almostAnAttachment As New FileAttachmentType
                                            almostAnAttachment = item.Attachments(attachmentIndex)
                                            If Not almostAnAttachment.Equals(Nothing) Then
                                                Dim requestId As RequestAttachmentIdType = New RequestAttachmentIdType
                                                requestId.Id = almostAnAttachment.AttachmentId.Id
                                                strAttachmentType = strAttachmentType & "," & almostAnAttachment.ContentType
                                                objDR("AttachmentType") = strAttachmentType
                                                attachmentIds.Add(requestId)
                                            End If
                                        Next
                                        Dim getAttachmentRequest As GetAttachmentType = New GetAttachmentType
                                        getAttachmentRequest.AttachmentShape = New AttachmentResponseShapeType
                                        getAttachmentRequest.AttachmentIds = attachmentIds.ToArray
                                        Dim getAttachmentResponse As GetAttachmentResponseType = esb.GetAttachment(getAttachmentRequest)
                                        Dim AttachmentPath As String
                                        Dim AttachmentName As String = ""
                                        For Each attachmentResponseMessage As AttachmentInfoResponseMessageType In getAttachmentResponse.ResponseMessages.Items

                                            If attachmentResponseMessage.ResponseCode = ResponseCodeType.NoError Then
                                                Dim fileAttachment As New FileAttachmentType
                                                fileAttachment = attachmentResponseMessage.Attachments(0)
                                                Dim file As New FileStream(Path.Combine(destinationPath, fileAttachment.Name), FileMode.Create)
                                                Try
                                                    file.Write(fileAttachment.Content, 0, fileAttachment.Content.Length)
                                                    file.Flush()
                                                    file.Close()
                                                    AttachmentName = AttachmentName & fileAttachment.Name & ","
                                                    'objDR("AttachmentPath") = fileAttachment.Name
                                                Finally
                                                    CType(file, IDisposable).Dispose()
                                                End Try
                                            End If
                                        Next
                                        objDR("AttachmentPath") = AttachmentName
                                    End If
                                End If
                            End If
                            objDR("Count") = count
                            objDR("ToEmail") = to1
                            objDR("ToName") = toname
                            objDR("CCEmail") = cc
                            objDR("ccName") = ccname
                            objDR("FromEmail") = z1.From.Item.EmailAddress
                            objDR("FromName") = z1.From.Item.Name
                            objDR("Subject") = z.Subject
                            objDR("Body") = item.Body.Value
                            If Not IsNothing(z1.Categories) Then
                                Dim strCategory As String = ""
                                Dim CategoryIterate As Integer = 0
                                For CategoryIterate = 0 To z1.Categories.Length - 1
                                    strCategory = strCategory & z1.Categories(CategoryIterate) & ","
                                Next
                                objDR("Category") = strCategory
                            Else
                                objDR("Category") = ""
                            End If
                            objDR("Sent") = FormatDate(z.DateTimeSent, DateFormat)
                            objDR("IsRead") = z.IsRead
                            objDR("Size") = SetBytes(z.Size)
                            objDR("SentFrom") = "O"
                            objDR("HasAttachments") = z.HasAttachments
                            objDR("Itemid") = z.ItemId.Id
                            objDR("Changekey") = z.ItemId.ChangeKey
                            dtTable.Rows.Add(objDR)
                        Next
                        count = count + 1
                    Next
                End If
            End If
            Return dtTable
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetDeletedItems(ByVal esb As ExchangeServiceBinding, ByVal frmDate As String, ByVal Email As String, ByVal DateFormat As String) As DataTable
        Try

            Dim dtTable As New DataTable
            Dim objDR As System.Data.DataRow
            Dim destinationPath As String = "c:\InetPub\wwwroot\BacrmPortal\Documents\Docs\"
            dtTable.Columns.Add("Count", GetType(String))
            dtTable.Columns.Add("ToEmail", GetType(String))
            dtTable.Columns.Add("ToName", GetType(String))
            dtTable.Columns.Add("CCEmail", GetType(String))
            dtTable.Columns.Add("ccName", GetType(String))
            dtTable.Columns.Add("FromEmail", GetType(String))
            dtTable.Columns.Add("FromName", GetType(String))
            dtTable.Columns.Add("Subject", GetType(String))
            dtTable.Columns.Add("Body", GetType(String))
            dtTable.Columns.Add("Sent", GetType(String))
            dtTable.Columns.Add("IsRead", GetType(Boolean))
            dtTable.Columns.Add("Size", GetType(String))
            dtTable.Columns.Add("Category", GetType(String))
            dtTable.Columns.Add("HasAttachments", GetType(Boolean))
            dtTable.Columns.Add("AttachmentType", GetType(String))
            dtTable.Columns.Add("AttachmentPath", GetType(String))
            dtTable.Columns.Add("SentFrom", GetType(Char))
            dtTable.Columns.Add("ItemId", GetType(String))
            dtTable.Columns.Add("Changekey", GetType(String))

            ''Adding a header field to SOAP request to identify the user
            'Dim ExchImpersonateAccc As New ExchangeImpersonationType
            'Dim connectionSid As New ConnectingSIDType
            'connectionSid.PrimarySmtpAddress = Email
            'ExchImpersonateAccc.ConnectingSID = connectionSid
            'esb.ExchangeImpersonation = ExchImpersonateAccc


            Dim findItemRequest As New FindItemType()
            findItemRequest.Traversal = ItemQueryTraversalType.Shallow

            Dim itemProperties As New ItemResponseShapeType()
            itemProperties.BaseShape = DefaultShapeNamesType.AllProperties

            Dim folderIDArray(1) As DistinguishedFolderIdType
            folderIDArray(0) = New DistinguishedFolderIdType()

            folderIDArray(0).Id = DistinguishedFolderIdNameType.deleteditems
            folderIDArray(0).Mailbox = New EmailAddressType()
            folderIDArray(0).Mailbox.EmailAddress = Email

            findItemRequest.ParentFolderIds = folderIDArray

            findItemRequest.ItemShape = itemProperties

            'Applying restriction of datetime Created
            Dim restriction As New RestrictionType

            Dim FieldURIOrConstantType As New FieldURIOrConstantType
            Dim contstantvalue As New ConstantValueType

            contstantvalue.Value = CDate(frmDate).ToUniversalTime().ToString 'constantvalue of date time
            FieldURIOrConstantType.Item = contstantvalue

            Dim URI As New PathToUnindexedFieldType
            URI.FieldURI = UnindexedFieldURIType.itemDateTimeSent                  'path of the Property on which restriction had to be applied

            Dim IsGreaterThanOrEqualToType As New IsGreaterThanOrEqualToType
            IsGreaterThanOrEqualToType.FieldURIOrConstant = FieldURIOrConstantType
            IsGreaterThanOrEqualToType.Item = URI
            restriction.Item = CType(IsGreaterThanOrEqualToType, SearchExpressionType)
            findItemRequest.Restriction = restriction

            Dim findItemResponse As FindItemResponseType = esb.FindItem(findItemRequest)

            Dim count As Integer = 1
            Dim findItemMsgResponse As New FindItemResponseMessageType
            findItemMsgResponse = findItemResponse.ResponseMessages.Items(0)
            If findItemResponse.ResponseMessages.Items(0).ResponseClass = ResponseClassType.Success Then
                Dim ItemDtls As ArrayOfRealItemsType = findItemMsgResponse.RootFolder.Item
                Dim ItemDtlsCount As Integer = 1
                If Not IsNothing(ItemDtls.Items) Then
                    For ItemDtlsCount = 0 To ItemDtls.Items.Length - 1
                        If ItemDtls.Items(ItemDtlsCount).ToString = "WebReference.MessageType" Then
                            Dim z As MessageType
                            z = CType(ItemDtls.Items(ItemDtlsCount), MessageType)

                            Dim i As Integer = 0

                            Dim getItemRequest As New GetItemType
                            Dim getItemIDType(0) As ItemIdType

                            Dim gettemResponseShapeType As New ItemResponseShapeType
                            gettemResponseShapeType.BaseShape = DefaultShapeNamesType.AllProperties
                            gettemResponseShapeType.IncludeMimeContent = True
                            getItemRequest.ItemShape = gettemResponseShapeType

                            getItemIDType(0) = New ItemIdType
                            getItemIDType(0).Id = z.ItemId.Id
                            getItemIDType(0).ChangeKey = z.ItemId.ChangeKey
                            getItemRequest.ItemIds = getItemIDType

                            Dim getItemresponse As GetItemResponseType
                            getItemresponse = esb.GetItem(getItemRequest)
                            For Each message As ItemInfoResponseMessageType In getItemresponse.ResponseMessages.Items
                                Dim item As ItemType = message.Items.Items(0)
                                Dim cc As String = ""
                                Dim to1 As String = ""
                                Dim ccname As String = ""
                                Dim toname As String = ""

                                Dim z1 As MessageType



                                z1 = CType(item, MessageType)
                                Dim fromEmail As String = z1.From.Item.EmailAddress
                                Dim fromName As String = z1.From.Item.Name
                                Dim i1 As Integer = 0
                                For i = 0 To z1.ToRecipients.Length - 1
                                    If Not i = z1.ToRecipients.Length - 1 Then
                                        to1 = to1 & z1.ToRecipients(i).EmailAddress & ","
                                    Else
                                        to1 = to1 & z1.ToRecipients(i).EmailAddress
                                    End If

                                    If Not i = z1.ToRecipients.Length - 1 Then
                                        toname = toname & z1.ToRecipients(i).Name & ","
                                    Else
                                        toname = toname & z1.ToRecipients(i).Name
                                    End If
                                Next i

                                If z1.DisplayCc <> "" Then

                                    For i = 0 To z1.CcRecipients.Length - 1
                                        If Not i = z1.CcRecipients.Length - 1 Then
                                            cc = cc & z1.CcRecipients(i).EmailAddress & ","
                                        Else
                                            cc = cc & z1.CcRecipients(i).EmailAddress
                                        End If

                                        If Not i = z1.CcRecipients.Length - 1 Then
                                            ccname = ccname & z1.CcRecipients(i).Name & ","
                                        Else
                                            ccname = ccname & z1.CcRecipients(i).Name
                                        End If
                                        'i = i + 1
                                    Next i
                                End If


                                objDR = dtTable.NewRow


                                If Not z1.From.Item.Name = "Microsoft Exchange" Then
                                    If item.HasAttachments = True Then
                                        If item.Attachments.Length > 0 Then
                                            If Not item.Attachments.Equals(Nothing) Then
                                                Dim attachmentIds As New List(Of RequestAttachmentIdType)
                                                Dim attachmentIndex As Integer = 0
                                                Dim strAttachmentType As String = ""
                                                For attachmentIndex = 0 To item.Attachments.Length - 1
                                                    Dim almostAnAttachment As New FileAttachmentType
                                                    almostAnAttachment = item.Attachments(attachmentIndex)
                                                    If Not almostAnAttachment.Equals(Nothing) Then
                                                        Dim requestId As RequestAttachmentIdType = New RequestAttachmentIdType
                                                        requestId.Id = almostAnAttachment.AttachmentId.Id
                                                        strAttachmentType = strAttachmentType & "," & almostAnAttachment.ContentType
                                                        objDR("AttachmentType") = strAttachmentType
                                                        attachmentIds.Add(requestId)
                                                    End If
                                                Next
                                                Dim getAttachmentRequest As GetAttachmentType = New GetAttachmentType
                                                getAttachmentRequest.AttachmentShape = New AttachmentResponseShapeType
                                                getAttachmentRequest.AttachmentIds = attachmentIds.ToArray
                                                Dim getAttachmentResponse As GetAttachmentResponseType = esb.GetAttachment(getAttachmentRequest)
                                                Dim AttachmentPath As String
                                                Dim AttachmentName As String = ""
                                                For Each attachmentResponseMessage As AttachmentInfoResponseMessageType In getAttachmentResponse.ResponseMessages.Items

                                                    If attachmentResponseMessage.ResponseCode = ResponseCodeType.NoError Then
                                                        Dim fileAttachment As New FileAttachmentType
                                                        fileAttachment = attachmentResponseMessage.Attachments(0)
                                                        Dim file As New FileStream(Path.Combine(destinationPath, fileAttachment.Name), FileMode.Create)
                                                        Try
                                                            file.Write(fileAttachment.Content, 0, fileAttachment.Content.Length)
                                                            file.Flush()
                                                            file.Close()
                                                            AttachmentName = AttachmentName & fileAttachment.Name & ","
                                                            'objDR("AttachmentPath") = fileAttachment.Name
                                                        Finally
                                                            CType(file, IDisposable).Dispose()
                                                        End Try
                                                    End If
                                                Next
                                                objDR("AttachmentPath") = AttachmentName
                                            End If
                                        End If
                                    End If
                                End If
                                objDR("Count") = count
                                objDR("ToEmail") = to1
                                objDR("ToName") = toname
                                objDR("CCEmail") = cc
                                objDR("ccName") = ccname
                                objDR("FromEmail") = z1.From.Item.EmailAddress
                                objDR("FromName") = z1.From.Item.Name
                                objDR("Subject") = z.Subject
                                objDR("Body") = item.Body.Value
                                objDR("Sent") = FormatDate(z.DateTimeSent, DateFormat)

                                If Not IsNothing(z1.Categories) Then
                                    Dim strCategory As String = ""
                                    Dim CategoryIterate As Integer = 0
                                    For CategoryIterate = 0 To z1.Categories.Length - 1
                                        strCategory = strCategory & z1.Categories(CategoryIterate) & ";"
                                    Next
                                    objDR("Category") = strCategory
                                Else
                                    objDR("Category") = ""
                                End If
                                'objDR("Category") = IIf(z1.Categories.Length > 0, "", z.Categories)
                                objDR("IsRead") = z.IsRead
                                objDR("Size") = SetBytes(z.Size)
                                objDR("SentFrom") = "O"
                                If Not z1.From.Item.Name = "Microsoft Exchange" Then
                                    objDR("HasAttachments") = z.HasAttachments
                                Else
                                    objDR("HasAttachments") = False
                                End If

                                objDR("Itemid") = z.ItemId.Id
                                objDR("Changekey") = z.ItemId.ChangeKey
                                dtTable.Rows.Add(objDR)

                            Next
                            count = count + 1

                        End If
                    Next
                End If
            End If
            Return dtTable

        Catch ex As Exception
            'Throw ex
        End Try
    End Function

    Function FormatDate(ByVal EntryDate, ByVal DateFormat) As String
        Try
            Return FormattedDateFromDate(EntryDate, DateFormat)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Function to Move a mail to Specfied Folder
    'delete ---- deleteditems Folder
    'inbox  ---- inbox Folder
    'Sent   ---- sentitems Folder
    Public Function MoveToFld(ByVal esb As ExchangeServiceBinding, ByVal folder As String, ByVal strItemId As String, ByVal strChangeKey As String)
        Try
            Dim distFolder As DistinguishedFolderIdType = New DistinguishedFolderIdType
            Select Case folder
                Case "delete"
                    distFolder.Id = DistinguishedFolderIdNameType.deleteditems
                Case "inbox"
                    distFolder.Id = DistinguishedFolderIdNameType.inbox
                Case "Sent"
                    distFolder.Id = DistinguishedFolderIdNameType.sentitems
            End Select
            Dim targetID As TargetFolderIdType = New TargetFolderIdType
            targetID.Item = distFolder
            Dim ItemId1(1) As ItemIdType
            ItemId1(0) = New ItemIdType
            ItemId1(0).Id = strItemId
            ItemId1(0).ChangeKey = strChangeKey
            Dim MoveItem As New MoveItemType
            MoveItem.ToFolderId = targetID
            MoveItem.ItemIds = ItemId1
            Dim response As MoveItemResponseType = esb.MoveItem(MoveItem)
            response = response
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Function To Mark a message To Read
    Public Function UpdateMessage(ByVal esb As ExchangeServiceBinding, ByVal strItemId As String, ByVal strChangeKey As String)
        Try
            'Creating the ITemId
            Dim ItemId1 As ItemIdType
            ItemId1 = New ItemIdType
            ItemId1.Id = strItemId
            ItemId1.ChangeKey = strChangeKey

            Dim ItemChangeType As ItemChangeType = New ItemChangeType
            ItemChangeType.Item = ItemId1
            ItemChangeType.Updates = New ItemChangeDescriptionType(0) {}

            Dim setItem As SetItemFieldType = New SetItemFieldType
            Dim indexedField As PathToUnindexedFieldType = New PathToUnindexedFieldType
            indexedField.FieldURI = UnindexedFieldURIType.messageIsRead
            setItem.Item = indexedField

            Dim Message As MessageType = New MessageType
            Message.IsRead = True
            Message.IsReadSpecified = True
            setItem.Item1 = Message

            ItemChangeType.Updates(0) = setItem

            Dim updateItem As New UpdateItemType
            updateItem.ItemChanges = New ItemChangeType(0) {}
            updateItem.ItemChanges(0) = ItemChangeType
            updateItem.MessageDisposition = MessageDispositionType.SaveOnly
            updateItem.MessageDispositionSpecified = True
            updateItem.ConflictResolution = ConflictResolutionType.AutoResolve

            esb.UpdateItem(updateItem)


        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Function HardDelete(ByVal esb As ExchangeServiceBinding, ByVal ItemId() As ItemIdType)
        Try
            Dim deleteItem As DeleteItemType
            deleteItem = New DeleteItemType
            deleteItem.ItemIds = ItemId
            deleteItem.DeleteType = DisposalType.HardDelete

            Dim response As DeleteItemResponseType = esb.DeleteItem(deleteItem)
            response = response



        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function HardDeleteCal(ByVal esb As ExchangeServiceBinding, ByVal ItemId() As ItemIdType)
        Try
            Dim deleteItem As DeleteItemType
            deleteItem = New DeleteItemType
            deleteItem.ItemIds = ItemId
            deleteItem.DeleteType = DisposalType.HardDelete
            deleteItem.SendMeetingCancellations = CalendarItemCreateOrDeleteOperationType.SendToNone
            deleteItem.SendMeetingCancellationsSpecified = True
            Dim response As DeleteItemResponseType = esb.DeleteItem(deleteItem)
            response = response



        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCalendarItems(ByVal esb As ExchangeServiceBinding, ByVal maxActivityId As Integer, ByVal maxRecurrenceId As Integer, ByVal ResourceId As Integer) As DataTable
        Try

            Dim dtTable As New DataTable
            Dim objDR As System.Data.DataRow
            Dim destinationPath As String = "c:\InetPub\wwwroot\BacrmPortal\Documents\Docs\"

            dtTable.Columns.Add("AllDayEvent", GetType(Boolean))
            dtTable.Columns.Add("ActivityDescription", GetType(String))
            dtTable.Columns.Add("Duration", GetType(Integer))
            dtTable.Columns.Add("Location", GetType(String))
            dtTable.Columns.Add("ActivityID", GetType(String))
            dtTable.Columns.Add("StartDateTimeUtc", GetType(Date))
            dtTable.Columns.Add("Subject", GetType(String))
            dtTable.Columns.Add("EnableReminder", GetType(Boolean))
            dtTable.Columns.Add("ReminderInterval", GetType(Integer))
            dtTable.Columns.Add("ShowTimeAs", GetType(Integer))
            dtTable.Columns.Add("Importance", GetType(Integer))
            dtTable.Columns.Add("Status", GetType(Integer))
            dtTable.Columns.Add("RecurrenceKey", GetType(Integer))
            dtTable.Columns.Add("ResourceKey", GetType(Integer))
            dtTable.Columns.Add("EndTime", GetType(Date))
            dtTable.Columns.Add("ItemId", GetType(String))
            dtTable.Columns.Add("ChangeKey", GetType(String))


            Dim findItemRequest As New FindItemType()
            findItemRequest.Traversal = ItemQueryTraversalType.Shallow


            Dim itemProperties As New ItemResponseShapeType()
            itemProperties.BaseShape = DefaultShapeNamesType.AllProperties

            Dim folderIDArray(1) As DistinguishedFolderIdType
            folderIDArray(0) = New DistinguishedFolderIdType()

            folderIDArray(0).Id = DistinguishedFolderIdNameType.calendar

            findItemRequest.ParentFolderIds = folderIDArray

            findItemRequest.ItemShape = itemProperties


            Dim findItemResponse As FindItemResponseType = esb.FindItem(findItemRequest)

            Dim count As Integer = 1
            Dim findItemMsgResponse As New FindItemResponseMessageType
            findItemMsgResponse = findItemResponse.ResponseMessages.Items(0)
            Dim ItemDtls As ArrayOfRealItemsType = findItemMsgResponse.RootFolder.Item
            Dim ItemDtlsCount As Integer = 1
            Dim activityId As Integer = maxActivityId + 1  ' adding 1 to max attivityId and using them for activities of Exchange calandaractivities as activityid
            Dim RecurrenceId As Integer = maxRecurrenceId + 1
            If Not IsNothing(ItemDtls.Items) Then


                For ItemDtlsCount = 0 To ItemDtls.Items.Length - 1

                    Dim calndarType As CalendarItemType
                    calndarType = CType(ItemDtls.Items(ItemDtlsCount), CalendarItemType)

                    Dim i As Integer = 0

                    Dim getItemRequest As New GetItemType
                    Dim getItemIDType(0) As ItemIdType

                    Dim gettemResponseShapeType As New ItemResponseShapeType
                    gettemResponseShapeType.BaseShape = DefaultShapeNamesType.AllProperties
                    gettemResponseShapeType.IncludeMimeContent = True
                    getItemRequest.ItemShape = gettemResponseShapeType

                    getItemIDType(0) = New ItemIdType
                    getItemIDType(0).Id = calndarType.ItemId.Id
                    getItemIDType(0).ChangeKey = calndarType.ItemId.ChangeKey
                    getItemRequest.ItemIds = getItemIDType

                    Dim getItemresponse As GetItemResponseType
                    getItemresponse = esb.GetItem(getItemRequest)

                    For Each message As ItemInfoResponseMessageType In getItemresponse.ResponseMessages.Items
                        Dim item As ItemType = message.Items.Items(0)
                        Dim calndarType1 As CalendarItemType
                        calndarType1 = CType(item, CalendarItemType)
                        objDR = dtTable.NewRow
                        'Dim strData As String
                        'strData = StripTags(calndarType1.Body.Value)
                        objDR("Subject") = calndarType1.Subject
                        objDR("ActivityDescription") = calndarType1.Body.Value
                        'objDR("ActivityID") = calndarType1.ItemId.Id
                        objDR("ActivityID") = activityId
                        objDR("StartDateTimeUtc") = calndarType1.Start
                        objDR("EndTime") = calndarType1.End
                        objDR("Duration") = DateDiff(DateInterval.Second, CDate(calndarType1.Start), CDate(calndarType1.End))
                        objDR("Location") = calndarType1.Location
                        objDR("AllDayEvent") = calndarType1.IsAllDayEvent
                        objDR("Location") = calndarType1.Location
                        objDR("EnableReminder") = calndarType1.ReminderIsSet
                        objDR("ReminderInterval") = CInt(calndarType1.ReminderMinutesBeforeStart) * 60
                        Select Case calndarType1.LegacyFreeBusyStatus
                            Case 0
                                objDR("ShowTimeAs") = calndarType1.LegacyFreeBusyStatus
                            Case 1
                                objDR("ShowTimeAs") = calndarType1.LegacyFreeBusyStatus
                            Case 2
                                objDR("ShowTimeAs") = 3
                            Case 3
                                objDR("ShowTimeAs") = 2
                        End Select
                        objDR("Importance") = calndarType1.Importance
                        objDR("Status") = calndarType1.AppointmentState
                        If IsNothing(calndarType1.Recurrence) Then
                            objDR("RecurrenceKey") = "-999"
                        Else
                            objDR("RecurrenceKey") = RecurrenceId
                        End If

                        objDR("ResourceKey") = ResourceId
                        objDR("ItemId") = calndarType1.ItemId.Id
                        objDR("ChangeKey") = calndarType1.ItemId.ChangeKey
                        dtTable.Rows.Add(objDR)
                        If Not IsNothing(calndarType1.Recurrence) Then

                            Dim basetype As RecurrencePatternBaseType
                            basetype = CType(calndarType1.Recurrence.Item, RecurrencePatternBaseType)

                            Dim RangeBase As RecurrenceRangeBaseType
                            RangeBase = CType(calndarType1.Recurrence.Item1, RecurrenceRangeBaseType)
                            Dim NumberedRecurrence As NumberedRecurrenceRangeType
                            Dim EndDateRecurrence As EndDateRecurrenceRangeType
                            Dim Occurrences As Integer

                            If RangeBase.ToString = "WebReference.EndDateRecurrenceRangeType" Then
                                EndDateRecurrence = CType(RangeBase, EndDateRecurrenceRangeType)
                            ElseIf RangeBase.ToString = "WebReference.NumberedRecurrenceRangeType" Then
                                NumberedRecurrence = CType(RangeBase, NumberedRecurrenceRangeType)
                                Occurrences = NumberedRecurrence.NumberOfOccurrences
                            End If


                            Dim startdate As Date = RangeBase.StartDate
                            Dim period As String
                            Dim Interval As Integer
                            Dim daysofweek As String
                            Dim DayOfMonth As Integer
                            Dim month1 As MonthNamesType
                            Select Case basetype.ToString
                                Case "WebReference.DailyRecurrencePatternType"
                                    period = "D"
                                    Dim daily As New DailyRecurrencePatternType
                                    daily = CType(basetype, DailyRecurrencePatternType)
                                    Interval = daily.Interval
                                Case "WebReference.WeeklyRecurrencePatternType"
                                    period = "W"
                                    Dim weekly As New WeeklyRecurrencePatternType
                                    weekly = CType(basetype, WeeklyRecurrencePatternType)
                                    Interval = weekly.Interval
                                    daysofweek = weekly.DaysOfWeek
                                Case "WebReference.AbsoluteMonthlyRecurrencePatternType"
                                    period = "M"
                                    Dim monthly As New AbsoluteMonthlyRecurrencePatternType
                                    monthly = CType(basetype, AbsoluteMonthlyRecurrencePatternType)
                                    DayOfMonth = monthly.DayOfMonth
                                    Interval = monthly.Interval

                                Case "WebReference.AbsoluteYearlyRecurrencePatternType"
                                    period = "Y"
                                    Dim yearly As New AbsoluteYearlyRecurrencePatternType
                                    yearly = CType(basetype, AbsoluteYearlyRecurrencePatternType)
                                    DayOfMonth = yearly.DayOfMonth
                                    month1 = yearly.Month

                            End Select
                            If Not IsNothing(calndarType1.FirstOccurrence) Then
                                Dim Forginalstart As Date = calndarType1.FirstOccurrence.OriginalStart
                                Dim Fstart As Date = calndarType1.FirstOccurrence.Start
                                Dim Fend As Date = calndarType1.FirstOccurrence.End
                            End If
                            If Not IsNothing(calndarType1.LastOccurrence) Then
                                Dim Lorginalstart As Date = calndarType1.LastOccurrence.OriginalStart
                                Dim Lstart As Date = calndarType1.LastOccurrence.Start
                                Dim Lend As Date = calndarType1.LastOccurrence.End
                            End If
                            RecurrenceId = RecurrenceId + 1
                        End If
                        activityId = activityId + 1
                    Next
                    count = count + 1

                Next
            End If
            Return dtTable

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub GetAttachments(ByVal esb As ExchangeServiceBinding, ByVal AttachmentItemId As String())
        Dim destinationPath As String = "c:\InetPub\wwwroot\BacrmPortal\Documents\Docs\"
        Dim attachmentIds As New List(Of RequestAttachmentIdType)
        Dim attachmentIndex As Integer = 0
        Dim strAttachmentType As String = ""
        For attachmentIndex = 0 To AttachmentItemId.Length - 1
            Dim requestId As RequestAttachmentIdType = New RequestAttachmentIdType
            requestId.Id = AttachmentItemId(attachmentIndex)
            attachmentIds.Add(requestId)
        Next
        Dim getAttachmentRequest As GetAttachmentType = New GetAttachmentType
        getAttachmentRequest.AttachmentShape = New AttachmentResponseShapeType
        getAttachmentRequest.AttachmentIds = attachmentIds.ToArray
        Dim getAttachmentResponse As GetAttachmentResponseType = esb.GetAttachment(getAttachmentRequest)
        Dim AttachmentPath As String
        Dim AttachmentName As String = ""
        For Each attachmentResponseMessage As AttachmentInfoResponseMessageType In getAttachmentResponse.ResponseMessages.Items
            If attachmentResponseMessage.ResponseCode = ResponseCodeType.NoError Then
                Dim fileAttachment As New FileAttachmentType
                fileAttachment = attachmentResponseMessage.Attachments(0)
                Dim file As New FileStream(Path.Combine(destinationPath, fileAttachment.Name), FileMode.Create)
                Try
                    file.Write(fileAttachment.Content, 0, fileAttachment.Content.Length)
                    file.Flush()
                    file.Close()
                    ' AttachmentName = AttachmentName & fileAttachment.Name & ";"
                    'objDR("AttachmentPath") = fileAttachment.Name
                Finally
                    CType(file, IDisposable).Dispose()
                End Try
            End If
        Next
    End Sub

    Public Sub UpdateCalandarActivity(ByVal esb As ExchangeServiceBinding, ByVal strItemId As String, ByVal strChangeKey As String, ByVal subject As String, ByVal Body As String, ByVal StartTime As DateTime, ByVal EndTime As DateTime, ByVal location As String, ByVal Importance As Integer, ByVal IsAllDayEvent As Boolean, ByVal EnableReminder As Boolean, ByVal Showtime As Integer, ByVal status As Integer, ByVal ReminderInteraval As Integer)
        Try


            Dim ItemId1 As ItemIdType
            ItemId1 = New ItemIdType
            ItemId1.Id = strItemId.Replace(" ", "+")
            ItemId1.ChangeKey = strChangeKey.Replace(" ", "+")

            Dim ItemChangeType As ItemChangeType = New ItemChangeType
            ItemChangeType.Item = ItemId1
            ItemChangeType.Updates = New ItemChangeDescriptionType(13) {}

            Dim setItem(13) As SetItemFieldType
            Dim i As Integer = 0
            For i = 0 To 12
                setItem(i) = New SetItemFieldType
            Next


            Dim indexedField(10) As PathToUnindexedFieldType

            indexedField(0) = New PathToUnindexedFieldType
            indexedField(0).FieldURI = UnindexedFieldURIType.itemSubject
            setItem(0).Item = indexedField(0)

            indexedField(1) = New PathToUnindexedFieldType
            indexedField(1).FieldURI = UnindexedFieldURIType.itemBody
            setItem(1).Item = indexedField(1)

            indexedField(2) = New PathToUnindexedFieldType
            indexedField(2).FieldURI = UnindexedFieldURIType.calendarLocation
            setItem(2).Item = indexedField(2)

            indexedField(3) = New PathToUnindexedFieldType
            indexedField(3).FieldURI = UnindexedFieldURIType.calendarIsAllDayEvent
            setItem(3).Item = indexedField(3)

            indexedField(4) = New PathToUnindexedFieldType
            indexedField(4).FieldURI = UnindexedFieldURIType.itemImportance
            setItem(4).Item = indexedField(4)

            indexedField(5) = New PathToUnindexedFieldType
            indexedField(5).FieldURI = UnindexedFieldURIType.itemReminderIsSet
            setItem(5).Item = indexedField(5)

            indexedField(6) = New PathToUnindexedFieldType
            indexedField(6).FieldURI = UnindexedFieldURIType.calendarLegacyFreeBusyStatus
            setItem(6).Item = indexedField(6)

            indexedField(7) = New PathToUnindexedFieldType
            indexedField(7).FieldURI = UnindexedFieldURIType.calendarAppointmentState
            setItem(7).Item = indexedField(7)

            indexedField(8) = New PathToUnindexedFieldType
            indexedField(8).FieldURI = UnindexedFieldURIType.calendarStart
            setItem(8).Item = indexedField(8)


            indexedField(9) = New PathToUnindexedFieldType
            indexedField(9).FieldURI = UnindexedFieldURIType.calendarEnd
            setItem(9).Item = indexedField(9)

            indexedField(10) = New PathToUnindexedFieldType
            indexedField(10).FieldURI = UnindexedFieldURIType.itemReminderMinutesBeforeStart
            setItem(10).Item = indexedField(10)



            Dim Activity(13) As CalendarItemType

            i = 0
            For i = 0 To 12
                Activity(i) = New CalendarItemType
            Next



            Activity(0).Subject = subject
            setItem(0).Item1 = Activity(0)


            Dim bodytype As New BodyType
            bodytype.Value = Body
            Activity(1).Body = bodytype
            setItem(1).Item1 = Activity(1)

            Activity(2).Location = location
            setItem(2).Item1 = Activity(2)

            Activity(3).IsAllDayEvent = IsAllDayEvent
            Activity(3).IsAllDayEventSpecified = True
            setItem(3).Item1 = Activity(3)


            Activity(4).Importance = Importance
            Activity(4).ImportanceSpecified = True
            setItem(4).Item1 = Activity(4)

            Activity(5).ReminderIsSet = EnableReminder
            Activity(5).ReminderIsSetSpecified = True
            setItem(5).Item1 = Activity(5)

            Activity(6).LegacyFreeBusyStatus = Showtime
            Activity(6).LegacyFreeBusyStatusSpecified = True
            setItem(6).Item1 = Activity(6)


            Activity(7).AppointmentState = status
            Activity(7).AppointmentStateSpecified = True
            setItem(7).Item1 = Activity(7)


            Activity(8).Start = StartTime
            Activity(8).StartSpecified = True

            setItem(8).Item1 = Activity(8)

            Activity(9).End = EndTime
            Activity(9).EndSpecified = True
            setItem(9).Item1 = Activity(9)

            Activity(10).ReminderMinutesBeforeStart = CStr(ReminderInteraval)
            setItem(10).Item1 = Activity(10)


            ItemChangeType.Updates(0) = setItem(0)
            ItemChangeType.Updates(1) = setItem(1)
            ItemChangeType.Updates(2) = setItem(2)
            ItemChangeType.Updates(3) = setItem(3)
            ItemChangeType.Updates(4) = setItem(4)
            ItemChangeType.Updates(5) = setItem(5)
            ItemChangeType.Updates(6) = setItem(6)
            'ItemChangeType.Updates(7) = setItem(7)
            ItemChangeType.Updates(8) = setItem(8)
            ItemChangeType.Updates(9) = setItem(9)
            ItemChangeType.Updates(10) = setItem(10)

            Dim updateItem As New UpdateItemType
            updateItem.ItemChanges = New ItemChangeType(0) {}
            updateItem.ItemChanges(0) = ItemChangeType
            'updateItem.MessageDisposition = MessageDispositionType.SaveOnly
            'updateItem.MessageDispositionSpecified = True
            updateItem.SendMeetingInvitationsOrCancellations = CalendarItemUpdateOperationType.SendToNone
            updateItem.SendMeetingInvitationsOrCancellationsSpecified = True
            updateItem.ConflictResolution = ConflictResolutionType.AutoResolve

            Dim up As New UpdateItemResponseType
            up = esb.UpdateItem(updateItem)
            up = up
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function CalendarOccuranceDelete(ByVal esb As ExchangeServiceBinding, ByVal strItemId As String, ByVal strChangeKey As String, ByVal StartTime As DateTime)
        Try
            Dim ItemId1 As ItemIdType
            ItemId1 = New ItemIdType
            ItemId1.Id = strItemId.Replace(" ", "+")
            ItemId1.ChangeKey = strChangeKey.Replace(" ", "+")

            Dim occuranceItemId As OccurrenceItemIdType
            occuranceItemId.RecurringMasterId = strItemId.Replace(" ", "+")
            occuranceItemId.ChangeKey = strChangeKey.Replace(" ", "+")


            Dim OccuranceDel(0) As DeletedOccurrenceInfoType
            OccuranceDel(0) = New DeletedOccurrenceInfoType
            OccuranceDel(0).Start = StartTime


            Dim setItem(0) As SetItemFieldType

            setItem(0) = New SetItemFieldType

            Dim indexedField(0) As PathToUnindexedFieldType

            indexedField(0) = New PathToUnindexedFieldType
            indexedField(0).FieldURI = UnindexedFieldURIType.calendarDeletedOccurrences

            setItem(0).Item = indexedField(0)

            Dim ItemChangeType As ItemChangeType = New ItemChangeType
            ItemChangeType.Item = ItemId1

            ItemChangeType.Updates = New ItemChangeDescriptionType(0) {}


            Dim Calendar(0) As CalendarItemType
            Calendar(0) = New CalendarItemType
            Calendar(0).DeletedOccurrences = OccuranceDel
            setItem(0).Item1 = Calendar(0)
            ItemChangeType.Updates(0) = setItem(0)

            Dim updateItem As New UpdateItemType
            updateItem.ItemChanges = New ItemChangeType(0) {}
            updateItem.ItemChanges(0) = ItemChangeType
            updateItem.SendMeetingInvitationsOrCancellations = CalendarItemUpdateOperationType.SendToNone
            updateItem.SendMeetingInvitationsOrCancellationsSpecified = True
            updateItem.ConflictResolution = ConflictResolutionType.AutoResolve

            Dim up As New UpdateItemResponseType
            up = esb.UpdateItem(updateItem)
            up = up
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Sub UpdateCalandarReminder(ByVal esb As ExchangeServiceBinding, ByVal strItemId As String, ByVal strChangeKey As String)
        Try


            Dim ItemId1 As ItemIdType
            ItemId1 = New ItemIdType
            ItemId1.Id = strItemId.Replace(" ", "+")
            ItemId1.ChangeKey = strChangeKey.Replace(" ", "+")

            Dim ItemChangeType As ItemChangeType = New ItemChangeType
            ItemChangeType.Item = ItemId1
            ItemChangeType.Updates = New ItemChangeDescriptionType(0) {}

            Dim setItem As SetItemFieldType = New SetItemFieldType
            Dim indexedField As PathToUnindexedFieldType = New PathToUnindexedFieldType
            indexedField.FieldURI = UnindexedFieldURIType.itemReminderIsSet
            setItem.Item = indexedField

            Dim Activity As CalendarItemType = New CalendarItemType
            Activity.ReminderIsSet = False
            Activity.ReminderIsSetSpecified = True
            setItem.Item1 = Activity


            ItemChangeType.Updates(0) = setItem

            Dim updateItem As New UpdateItemType
            updateItem.ItemChanges = New ItemChangeType(0) {}
            updateItem.ItemChanges(0) = ItemChangeType
            updateItem.SendMeetingInvitationsOrCancellations = CalendarItemUpdateOperationType.SendToNone
            updateItem.SendMeetingInvitationsOrCancellationsSpecified = True
            updateItem.ConflictResolution = ConflictResolutionType.AutoResolve
            Dim up As New UpdateItemResponseType
            up = esb.UpdateItem(updateItem)
            up = up
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub AddRecurrence(ByVal esb As ExchangeServiceBinding, ByVal strItemId As String, ByVal strChangeKey As String, ByVal RecurrenceType As RecurrenceType)
        Try
            Dim ItemId1 As ItemIdType
            ItemId1 = New ItemIdType
            ItemId1.Id = strItemId.Replace(" ", "+")
            ItemId1.ChangeKey = strChangeKey.Replace(" ", "+")
          
            Dim ItemChangeType As ItemChangeType = New ItemChangeType
            ItemChangeType.Item = ItemId1
            ItemChangeType.Updates = New ItemChangeDescriptionType(0) {}

            Dim setItem As SetItemFieldType = New SetItemFieldType
            Dim indexedField As PathToUnindexedFieldType = New PathToUnindexedFieldType
            indexedField.FieldURI = UnindexedFieldURIType.calendarRecurrence
            setItem.Item = indexedField

            Dim Activity As CalendarItemType = New CalendarItemType
            Activity.Recurrence = RecurrenceType
            setItem.Item1 = Activity


            ItemChangeType.Updates(0) = setItem

            Dim updateItem As New UpdateItemType
            updateItem.ItemChanges = New ItemChangeType(0) {}
            updateItem.ItemChanges(0) = ItemChangeType
            updateItem.SendMeetingInvitationsOrCancellations = CalendarItemUpdateOperationType.SendToNone
            updateItem.SendMeetingInvitationsOrCancellationsSpecified = True
            updateItem.ConflictResolution = ConflictResolutionType.AutoResolve
            Dim up As New UpdateItemResponseType
            up = esb.UpdateItem(updateItem)
            up = up
        Catch ex As Exception

        End Try
    End Sub

    Sub RemoveCalandarRecurrence(ByVal esb As ExchangeServiceBinding, ByVal strItemId As String, ByVal strChangeKey As String)
        Try


            Dim ItemId1 As ItemIdType
            ItemId1 = New ItemIdType
            ItemId1.Id = strItemId.Replace(" ", "+")
            ItemId1.ChangeKey = strChangeKey.Replace(" ", "+")

            Dim ItemChangeType As ItemChangeType = New ItemChangeType
            ItemChangeType.Item = ItemId1
            ItemChangeType.Updates = New ItemChangeDescriptionType(0) {}


            Dim setItem As SetItemFieldType = New SetItemFieldType

            Dim indexedField As PathToUnindexedFieldType = New PathToUnindexedFieldType
            indexedField.FieldURI = UnindexedFieldURIType.calendarRecurrence
            'setItem.Item = indexedField

            Dim DeleteItemFieldType As DeleteItemFieldType = New DeleteItemFieldType
            DeleteItemFieldType.Item = indexedField

            'Dim RecurrenceType As New RecurrenceType
            ''RecurrenceType = Nothing


            'RecurrenceType.Item = Nothing
            'RecurrenceType.Item1 = Nothing

            'Dim Activity As CalendarItemType = New CalendarItemType
            'Activity.Recurrence = RecurrenceType
            ' setItem.Item1 = Activity


            ItemChangeType.Updates(0) = DeleteItemFieldType

            Dim updateItem As New UpdateItemType
            updateItem.ItemChanges = New ItemChangeType(0) {}
            updateItem.ItemChanges(0) = ItemChangeType
            updateItem.SendMeetingInvitationsOrCancellations = CalendarItemUpdateOperationType.SendToNone
            updateItem.SendMeetingInvitationsOrCancellationsSpecified = True
            updateItem.ConflictResolution = ConflictResolutionType.AutoResolve
            Dim up As New UpdateItemResponseType
            up = esb.UpdateItem(updateItem)
            up = up
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function StripTags(ByVal HTML As String) As String
        ' Removes tags from passed HTML
        Dim objRegEx As _
            System.Text.RegularExpressions.Regex
        Return objRegEx.Replace(HTML, "<[^>]*>", "")
    End Function



    Function SetBytes(ByVal Bytes) As String

        If Bytes >= 1073741824 Then
            SetBytes = Format(Bytes / 1024 / 1024 / 1024, "#0.00") & " GB"
        ElseIf Bytes >= 1048576 Then
            SetBytes = Format(Bytes / 1024 / 1024, "#0.00") & " MB"
        ElseIf Bytes >= 1024 Then
            SetBytes = Format(Bytes / 1024, "#0.00") & " KB"
        ElseIf Bytes < 1024 Then
            SetBytes = Fix(Bytes) & " B"
        End If

        Exit Function
    End Function

End Class
