Public Class ExportToExcel
    Inherits System.ComponentModel.Component


    Public Shared Sub DataGridToExcel(ByVal dgExport As DataGrid, ByVal response As HttpResponse)
        response.Clear()
        response.AddHeader("content-disposition", "attachment;filename=FileName" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".xls")
        response.Charset = ""
        'response.Cache.SetCacheability(HttpCacheability.NoCache)
        response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)
        ClearControls(dgExport)
        dgExport.GridLines = GridLines.Both
        dgExport.HeaderStyle.Font.Bold = True
        dgExport.RenderControl(htmlWrite)
        response.Write(stringWrite.ToString())
        response.End()
    End Sub

    Public Shared Sub DataGridToExcel(ByVal gView As GridView, ByVal response As HttpResponse)
        response.Clear()
        response.AddHeader("content-disposition", "attachment;filename=FileName" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".xls")
        response.Charset = ""
        'response.Cache.SetCacheability(HttpCacheability.NoCache)
        response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)
        'Dim gv As New GridView
        'gv = gView
        ClearControls(gView)
        gView.GridLines = GridLines.Both
        gView.HeaderStyle.Font.Bold = True
        gView.RenderControl(htmlWrite)
        response.Write(stringWrite.ToString())
        response.End()
    End Sub
    Public Shared Sub ExportToHTML(ByVal strHTML As String, ByVal response As HttpResponse)
        response.Clear()
        response.AddHeader("content-disposition", "attachment;filename=Products" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".html")
        response.Charset = ""
        'response.Cache.SetCacheability(HttpCacheability.NoCache)
        response.ContentType = "text/html"
        'Dim stringWrite As New System.IO.StringWriter
        'Dim htmlWrite As New HtmlTextWriter(stringWrite)
        'Dim gv As New GridView
        'gv = gView
        'ClearControls(radGrid)
        'radGrid.GridLines = GridLines.Both
        'radGrid.HeaderStyle.Font.Bold = True
        'radGrid.RenderControl(htmlWrite)
        response.Write(strHTML)
        response.End()
    End Sub

    Public Shared Sub RepeaterToExcel(ByVal rptr As Repeater, ByVal response As HttpResponse)
        response.Clear()
        response.AddHeader("content-disposition", "attachment;filename=Export" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".xls")
        response.Charset = ""
        'response.Cache.SetCacheability(HttpCacheability.NoCache)
        response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)
        'Dim gv As New GridView
        'gv = gView
        ClearControls(rptr)
        'rptr.GridLines = GridLines.Both
        'rptr.HeaderStyle.Font.Bold = True
        rptr.RenderControl(htmlWrite)
        response.Write(stringWrite.ToString())
        response.End()
    End Sub


    Public Shared Sub aspTableToExcel(ByVal aspTable As HtmlTable, ByVal response As HttpResponse)
        response.Clear()
        response.AddHeader("content-disposition", "attachment;filename=Export" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".xls")
        response.Charset = ""
        'response.Cache.SetCacheability(HttpCacheability.NoCache)
        response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)
        aspTable.RenderControl(htmlWrite)
        response.Write(stringWrite.ToString())
        response.End()
    End Sub

    Public Shared Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i

        If TypeOf control Is System.Web.UI.WebControls.Image Then
            control.Parent.Controls.Remove(control)
        End If

        If (Not TypeOf control Is TableCell) Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    literal.Text = Replace(literal.Text, "white", "black")
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
        Return
    End Sub
End Class

