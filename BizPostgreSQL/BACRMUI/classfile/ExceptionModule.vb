﻿Imports MailBee
Imports MailBee.Security
Imports MailBee.Mime
Imports MailBee.SmtpMail
Imports QueryStringVal
Imports BACRM.BusinessLogic.Common
Module ExceptionModule


    Public Function ExceptionPublish(ByVal ex As Exception, ByVal DomainID As Long, ByVal UserContactID As Long, ByVal req As System.Web.HttpRequest) As Boolean
        Try

    
            If ex.Message = "Thread was being aborted." Then Exit Function
            Dim objQueryString As New GetQueryStringVal
            Dim objExceptionPublisher As New ExceptionPublisher
            objExceptionPublisher.DomainID = DomainID
            objExceptionPublisher.Message = ex.Message & "<br>" & ex.Source & "<br>" & ex.HelpLink & "<br>" & ex.StackTrace & "<br>" & ex.TargetSite.ToString
            Dim strURL As String
            Dim strReferrer As String
            strURL = req.Url.AbsolutePath
            If Not req.QueryString("enc") Is Nothing Then
                strURL = strURL + "?" + objQueryString.Decrypt(req.QueryString("enc").Trim)
            End If

            strReferrer = req.UrlReferrer.PathAndQuery

            objExceptionPublisher.URL = strURL
            objExceptionPublisher.Referrer = strReferrer
            objExceptionPublisher.UserContactID = UserContactID
            objExceptionPublisher.Browser = req.Browser.Type
            objExceptionPublisher.BrowserAgent = req.UserAgent & req.UserHostAddress
            objExceptionPublisher.ManageException()

            If ConfigurationManager.AppSettings("SendExceptionMail") = 1 Then
                Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")


                Dim svr As SmtpServer = New SmtpServer
                Dim m As New Smtp
                'm.Log.Enabled = True
                'm.Log.Filename = "C:\aspNetEmail.log"
                'm.Log.DisableOnException = False

                svr.AuthMethods = AuthenticationMethods.Auto
                svr.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                svr.AccountName = "anoop@bizautomation.com"
                Dim strPassword As String
                strPassword = "Anoop1213"
                svr.Password = strPassword

                svr.Port = "587"
                svr.SslMode = SslStartupMode.UseStartTls
                svr.Name = "smtp.gmail.com"

                m.SmtpServers.Add(svr)

                '    Dim m As New EmailMessage(IIf(Session("SMTPServer") = "", ConfigurationManager.AppSettings("SMTPServer"), Session("SMTPServer")))
                ' If Session("SMTPPort") <> 0 Then
                '  m.Port = Session("SMTPPort")
                'End If
                'demonstrate the BeforeRowMerge event
                ' AddHandler m.BeforeRowMerge, AddressOf OnBeforeRowMerge

                'demonstrate the MergedRowSent event
                ' AddHandler m.MergedRowSent, AddressOf OnRowSent
                m.From.AsString = "anoop@bizautomation.com"
                m.From.DisplayName = "Anoop Jayaraj"
                ' m.FromAddress = Session("UserEmail")
                '  m.FromName = Session("ContactName")
                ' m.ThrowException = False

                ' If File.Exists("c:\aspNetEmail.log") = True Then
                'm.LogOverwrite = True
                '  m.Logging = True
                '  m.LogPath = "c:\aspNetEmail.log"
                ' End If


                'try and reconnect up to 3 times, if the connection is dropped
                ' m.MailMergeReconnectAttempts = 3

                'wait 100ms between connection attempts
                '  m.MailMergeReconnectDelay = 100

                'matches a column name in the tabled named 'fldEmail'
                m.To.AsString = ConfigurationManager.AppSettings("SendExceptionMailAddress")
                m.Cc.AsString = ""
                'If chkVCard.Checked = True Then
                '    Dim vc As vCardWriter.vCard = LoadvCard()
                '    m.AddvCard(vc)
                'End If

                'matches a column name in the tabled named 'fldFirstName'
                m.Subject = ex.Message

                '  m.BodyFormat = MailFormat.Html

                m.BodyHtmlText = ex.Message & "<br>" & ex.Source & "<br>" & ex.HelpLink & "<br>" & ex.StackTrace & "<br>" & ex.TargetSite.ToString & _
                 "<br>User:" & HttpContext.Current.Session("ContactName") & "<br>UserEmail:" & HttpContext.Current.Session("UserEmail") & _
                 "<br>Company:" & HttpContext.Current.Session("CompanyName") & "<br>DomainName:" & HttpContext.Current.Session("DomainName") & _
                 "<br>URL:" & strURL & "<br>Referrer:" & strReferrer & "<br>Browser:" & req.Browser.Type & "<br>Browser Agent:" & req.UserAgent

                m.Send()
            End If

            Return True
        Catch exNon As Exception

        End Try
    End Function

End Module