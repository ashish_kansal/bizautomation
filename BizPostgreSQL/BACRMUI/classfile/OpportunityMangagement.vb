﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Common

Public Class OpportunityMangagement

    'Public Shared Function AddEditItemtoDataTable(ByRef objCommon As BACRM.BusinessLogic.Common.CCommon, _
    '                               ByRef dsTemp As DataSet, _
    '                               ByVal AddMode As Boolean, _
    '                               ByVal ItemTypeValue As Char, _
    '                               ByVal ItemTypeText As String, _
    '                               ByVal DropShip As Boolean, _
    '                               ByVal IsKit As String, _
    '                               ByVal ItemCode As Long, _
    '                               ByVal Units As Integer, _
    '                               ByVal Price As Decimal, _
    '                               ByVal Desc As String, _
    '                               ByVal WareHouseItemID As Long, _
    '                               ByVal ItemName As String, _
    '                               ByVal Warehouse As String, _
    '                               ByRef dgKitItems As System.Web.UI.WebControls.DataGrid, _
    '                               ByVal OppItemID As Long, _
    '                               ByVal ModelId As String) As DataSet
    '    Try




    '        Dim dtItem As DataTable
    '        Dim dtSerItem As DataTable
    '        Dim dtChildItems As DataTable

    '        dtItem = dsTemp.Tables(0)
    '        dtSerItem = dsTemp.Tables(1)
    '        dtChildItems = dsTemp.Tables(2)

    '        Dim dr, dr2 As DataRow
    '        Dim lngOppItemId As Long

    '        If AddMode = True Then
    '            dr = dtItem.NewRow
    '            If IsKit = "True" Then
    '                Dim objOpportunity As New MOpportunity
    '                objOpportunity.ItemCode = ItemCode
    '                objOpportunity.ItemType = ItemTypeValue
    '                lngOppItemId = objOpportunity.CreateOppItem
    '                dr("numoppitemtCode") = lngOppItemId
    '                dr("Op_Flag") = 2
    '            Else
    '                dr("Op_Flag") = 1
    '                dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1 'dtItem.Rows.Count + 1 
    '            End If
    '            dr("numItemCode") = ItemCode
    '            dr("numUnitHour") = Units
    '            dr("monPrice") = Price
    '            dr("bitDiscountType") = 0
    '            dr("fltDiscount") = 0
    '            dr("monTotAmtBefDiscount") = Units * Price
    '            dr("monTotAmount") = dr("monTotAmtBefDiscount")
    '            dr("vcItemDesc") = Replace(Desc, "'", "''")
    '            dr("vcModelID") = ModelId
    '            dr("ItemType") = ItemTypeText
    '            dr("vcItemName") = ItemName
    '            If ItemTypeValue = "P" Then
    '                If DropShip = False Then
    '                    Dim objItems As New BACRM.BusinessLogic.Item.CItems
    '                    objItems.WareHouseItemID = WareHouseItemID
    '                    dr("Warehouse") = Warehouse
    '                    dr("numWarehouseID") = objItems.GetWarehouseID
    '                    dr("numWarehouseItmsID") = WareHouseItemID
    '                    dr("Attributes") = objCommon.GetAttributesForWarehouseItem(WareHouseItemID, False)
    '                    dr("DropShip") = False
    '                Else
    '                    dr("numWarehouseID") = DBNull.Value
    '                    dr("Warehouse") = ""
    '                    dr("numWarehouseItmsID") = DBNull.Value
    '                    dr("Attributes") = ""
    '                    dr("DropShip") = True
    '                End If
    '            Else
    '                dr("numWarehouseID") = DBNull.Value
    '                dr("numWarehouseItmsID") = DBNull.Value
    '            End If
    '            dtItem.Rows.Add(dr)
    '            If IsKit = "True" Then
    '                For Each dgGridKitItem As System.Web.UI.WebControls.DataGridItem In dgKitItems.Items
    '                    dr2 = dtChildItems.NewRow
    '                    dr2("numOppChildItemID") = CType(IIf(IsDBNull(dtChildItems.Compute("MAX(numOppChildItemID)", "")), 0, dtChildItems.Compute("MAX(numOppChildItemID)", "")), Integer) + 1 'dtChildItems.Rows.Count + 1  
    '                    dr2("numoppitemtCode") = lngOppItemId
    '                    dr2("numItemCode") = dgGridKitItem.Cells(0).Text
    '                    dr2("numQtyItemsReq") = CType(dgGridKitItem.FindControl("txtQuantity"), System.Web.UI.WebControls.TextBox).Text
    '                    dr2("vcItemName") = dgGridKitItem.Cells(4).Text
    '                    dr2("monListPrice") = dgGridKitItem.Cells(8).Text.Split(CChar("/"))(0)
    '                    dr2("UnitPrice") = dgGridKitItem.Cells(8).Text
    '                    dr2("charItemType") = dgGridKitItem.Cells(7).Text
    '                    dr2("ItemType") = dgGridKitItem.Cells(10).Text
    '                    dr2("txtItemDesc") = dgGridKitItem.Cells(6).Text
    '                    If dgGridKitItem.Cells(10).Text = "P" And DropShip = False Then
    '                        dr2("numWarehouseItmsID") = IIf(CType(dgGridKitItem.FindControl("radKitWareHouse"), RadComboBox).SelectedValue = "", 0, CType(dgGridKitItem.FindControl("radKitWareHouse"), RadComboBox).SelectedValue)
    '                        dr2("Attr") = CType(dgGridKitItem.FindControl("radKitWareHouse"), RadComboBox).Text
    '                    End If
    '                    dr2("Op_Flag") = 1
    '                    dtChildItems.Rows.Add(dr2)
    '                Next
    '            End If
    '            dsTemp.AcceptChanges()
    '        Else

    '            dr = dtItem.Rows.Find(OppItemID)
    '            lngOppItemId = dr("numoppitemtCode")
    '            dr("numUnitHour") = Units
    '            dr("monPrice") = Price
    '            dr("monTotAmtBefDiscount") = Units * Price
    '            If dr("bitDiscountType") = False Then
    '                dr("monTotAmount") = dr("monTotAmtBefDiscount") - dr("monTotAmtBefDiscount") * dr("fltDiscount") / 100
    '            Else
    '                dr("monTotAmount") = dr("monTotAmtBefDiscount") - dr("fltDiscount")
    '            End If
    '            dr("vcItemDesc") = Replace(Desc, "'", "''")
    '            dr("Op_Flag") = 2
    '            If ItemTypeValue = "P" Then
    '                If DropShip = False Then
    '                    Dim objItems As New CItems
    '                    objItems.WareHouseItemID = WareHouseItemID
    '                    dr("numWarehouseID") = objItems.GetWarehouseID
    '                    dr("Warehouse") = Warehouse
    '                    dr("numWarehouseItmsID") = WareHouseItemID
    '                    dr("Attributes") = objCommon.GetAttributesForWarehouseItem(dr("numWarehouseItmsID"), False)
    '                    dr("DropShip") = False
    '                Else
    '                    dr("numWarehouseID") = DBNull.Value
    '                    dr("numWarehouseItmsID") = DBNull.Value
    '                    dr("Warehouse") = ""
    '                    dr("Attributes") = ""
    '                    dr("DropShip") = True
    '                End If
    '            End If
    '            If IsKit = "True" Then
    '                For Each dgGridKitItem As DataGridItem In dgKitItems.Items
    '                    If dgGridKitItem.Cells(1).Text = 0 Then
    '                        dr2 = dtChildItems.NewRow
    '                        dr2("numOppChildItemID") = CType(IIf(IsDBNull(dtChildItems.Compute("MAX(numOppChildItemID)", "")), 0, dtChildItems.Compute("MAX(numOppChildItemID)", "")), Integer) + 1 'dtChildItems.Rows.Count + 1
    '                        dr2("numoppitemtCode") = lngOppItemId
    '                        dr2("numItemCode") = dgGridKitItem.Cells(0).Text
    '                        dr2("numQtyItemsReq") = CType(dgGridKitItem.FindControl("txtQuantity"), TextBox).Text
    '                        dr2("vcItemName") = dgGridKitItem.Cells(4).Text
    '                        dr2("monListPrice") = dgGridKitItem.Cells(8).Text.Split("/")(0)
    '                        dr2("UnitPrice") = dgGridKitItem.Cells(8).Text
    '                        dr2("charItemType") = dgGridKitItem.Cells(7).Text
    '                        dr2("ItemType") = dgGridKitItem.Cells(10).Text
    '                        dr2("txtItemDesc") = dgGridKitItem.Cells(6).Text
    '                        If dgGridKitItem.Cells(10).Text = "P" And DropShip = False Then
    '                            dr2("numWarehouseItmsID") = IIf(CType(dgGridKitItem.FindControl("radKitWareHouse"), RadComboBox).SelectedValue = "", 0, CType(dgGridKitItem.FindControl("radKitWareHouse"), RadComboBox).SelectedValue)
    '                            dr2("Attr") = CType(dgGridKitItem.FindControl("radKitWareHouse"), RadComboBox).Text
    '                        End If
    '                        dr2("Op_Flag") = 1
    '                        dtChildItems.Rows.Add(dr2)
    '                    Else
    '                        If dtChildItems.PrimaryKey.Length = 0 Then
    '                            dtChildItems.PrimaryKey = New DataColumn() {dsTemp.Tables(2).Columns("numOppChildItemID")}
    '                        End If
    '                        dr2 = dtChildItems.Rows.Find(dgGridKitItem.Cells(1).Text)
    '                        dr2("numQtyItemsReq") = CType(dgGridKitItem.FindControl("txtQuantity"), TextBox).Text
    '                        If dgGridKitItem.Cells(10).Text = "P" And DropShip = False Then
    '                            dr2("numWarehouseItmsID") = IIf(CType(dgGridKitItem.FindControl("radKitWareHouse"), RadComboBox).SelectedValue = "", 0, CType(dgGridKitItem.FindControl("radKitWareHouse"), RadComboBox).SelectedValue)
    '                            dr2("Attr") = CType(dgGridKitItem.FindControl("radKitWareHouse"), Telerik.Web.UI.RadComboBox).Text
    '                        End If
    '                        If dr2("Op_Flag") = 0 Then dr2("Op_Flag") = 2
    '                    End If
    '                Next
    '            End If
    '            dr.AcceptChanges()
    '            dsTemp.AcceptChanges()
    '            If dtSerItem.ParentRelations.Count = 0 Then dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
    '        End If
    '        Return dsTemp
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Shared Function AddEditItemtoDataTable(ByRef objCommon As BACRM.BusinessLogic.Common.CCommon,
                            ByRef dsTemp As DataSet,
                            ByVal AddMode As Boolean,
                            ByVal ItemTypeValue As Char,
                            ByVal ItemTypeText As String,
                            ByVal DropShip As Boolean,
                            ByVal IsKit As String,
                            ByVal ItemCode As Long,
                            ByVal Units As Decimal,
                            ByVal Price As Decimal,
                            ByVal Desc As String,
                            ByVal WareHouseItemID As Long,
                            ByVal ItemName As String,
                            ByVal Warehouse As String,
                            ByVal OppItemID As Long,
                            ByVal ModelId As String,
                            ByVal UOM As Long,
                            ByVal vcUOMName As String,
                            ByVal UOMConversionFactor As Decimal,
                            ByVal PageType As String,
                            ByVal IsDiscountInPer As Boolean,
                            ByVal Discount As Decimal,
                            ByVal Taxable As String,
                            ByVal Tax As String,
                            ByVal CustomerPartNo As String,
                            Optional ByRef chkTaxItems As CheckBoxList = Nothing,
                            Optional ByVal bitWorkOrder As Boolean = False,
                            Optional ByVal vcInstruction As String = "",
                            Optional ByVal bintCompliationDate As String = "",
                            Optional ByVal numWOAssignedTo As Long = 0,
                            Optional ByVal numVendorWareHouse As Long = 0,
                            Optional ByVal numShipmentMethod As Long = 0,
                            Optional ByVal numSOVendorId As Long = 0,
                            Optional ByVal numProjectID As Long = 0,
                            Optional ByVal numProjectStageID As Long = 0,
                            Optional ByVal bDirectAdd As Boolean = False,
                            Optional ByVal ToWarehouseItemID As Long = 0,
                            Optional ByVal IsPOS As Boolean = False,
                            Optional ByVal vcBaseUOMName As String = "",
                            Optional ByVal numPrimaryVendorID As Long = 0,
                            Optional ByVal fltItemWeight As Double = 0,
                            Optional ByVal IsFreeShipping As Boolean = False,
                            Optional ByVal fltHeight As Double = 0,
                            Optional ByVal fltWidth As Double = 0,
                            Optional ByVal fltLength As Double = 0,
                            Optional ByVal strSKU As String = "",
                            Optional ByVal dtItemColumnTableInfo As DataTable = Nothing,
                            Optional ByVal objItem As CItems = Nothing,
                            Optional ByVal primaryVendorCost As Decimal = 0,
                            Optional ByVal salePrice As Decimal = 0,
                            Optional ByVal numMaxWOQty As Integer = 0,
                            Optional ByVal vcAttributes As String = "",
                            Optional ByVal vcAttributeIDs As String = "",
                            Optional ByVal numContainer As Integer = -1,
                            Optional ByVal numContainerQty As Integer = -1,
                            Optional ByVal numItemClassification As Long = 0,
                            Optional ByVal numPromotionID As Long = 0,
                            Optional ByVal IsPromotionTriggered As Boolean = False,
                            Optional ByVal vcPromotionDetail As String = "",
                            Optional ByVal numSortOrder As Long = 0,
                            Optional ByVal numCost As Double = 0,
                            Optional ByVal vcVendorNotes As String = "",
                            Optional ByVal itemReleaseDate As Date? = Nothing,
                            Optional ByVal ShipFromLocation As String = "",
                            Optional ByVal numShipToAddressID As Long = 0,
                            Optional ByVal ShipToFullAddress As String = "",
                            Optional ByVal InclusionDetail As String = "",
                            Optional ByVal OnHandAllocation As String = "",
                            Optional ByVal bitMarkupDiscount As String = "0",
                            Optional ByVal numSelectedPromotionID As Long = 0,
                            Optional ByVal bitAlwaysCreateWO As Boolean = False,
                            Optional ByVal numWOQty As Double = 0,
                            Optional ByVal dtPlannedStart As String = "",
                            Optional ByVal itemRequiredDate As Date? = Nothing,
                            Optional ByVal isDisablePromotion As Boolean = False
                             ) As DataSet
        Try

            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            Dim dtChildItems As DataTable

            dtItem = dsTemp.Tables(0)
            dtSerItem = dsTemp.Tables(1)
            dtChildItems = dsTemp.Tables(2)

            'dr("fltItemWeight") = fltItemWeight
            'dr("IsFreeShipping") = IsFreeShipping
            'dr("fltHeight") = fltHeight
            'dr("fltWidth") = fltWidth
            'dr("fltLength") = fltLength
            If Not dtItem.Columns.Contains("fltItemWeight") Then dtItem.Columns.Add("fltItemWeight")

            If Not dtItem.Columns.Contains("IsFreeShipping") Then dtItem.Columns.Add("IsFreeShipping")
            If Not dtItem.Columns.Contains("fltHeight") Then dtItem.Columns.Add("fltHeight")
            If Not dtItem.Columns.Contains("fltWidth") Then dtItem.Columns.Add("fltWidth")
            If Not dtItem.Columns.Contains("fltLength") Then dtItem.Columns.Add("fltLength")
            If Not dtItem.Columns.Contains("numSortOrder") Then dtItem.Columns.Add("numSortOrder")


            Dim dr, dr2 As DataRow
            Dim lngOppItemId As Long

            If AddMode = True Then
                If Not CCommon.ToBool(HttpContext.Current.Session("IsAllowDuplicateLineItems")) AndAlso (dsTemp.Tables(0).Select("numItemCode='" & ItemCode & "'" & IIf(WareHouseItemID > 0, " and numWarehouseItmsID = '" & WareHouseItemID & "'", "") & " and DropShip ='" & DropShip & "'").Length > 0) Then
                    If (HttpContext.Current.Session("ShippingItem") IsNot Nothing) Then
                    Else
                        Return dsTemp
                    End If

                End If

                dr = dtItem.NewRow
                dr("numSortOrder") = numSortOrder
                dr("Op_Flag") = 1

                Dim objOpportunity As New MOpportunity
                Dim maxOppItemID As Long = objOpportunity.GetMaxOppItemID()

                Dim rnd As New Random
                Dim tempOppItemID As Long
                Do
                    tempOppItemID = rnd.Next(maxOppItemID, maxOppItemID + 20000)
                Loop While dtItem.Select("numoppitemtCode=" & tempOppItemID).Length > 0

                dr("numoppitemtCode") = tempOppItemID
                dr("numItemCode") = ItemCode
                dr("vcModelID") = ModelId

                ' dr("CustomerPartNo") = CustomerPartNo

                dr("ItemType") = ItemTypeText
                dr("vcItemName") = ItemName

                If numProjectID > 0 AndAlso numProjectStageID > 0 Then
                    dr("numProjectID") = numProjectID
                    dr("numProjectStageID") = numProjectStageID
                Else
                    dr("numProjectID") = 0
                    dr("numProjectStageID") = 0
                End If

                dr("charItemType") = ItemTypeValue
                dr("bitIsAuthBizDoc") = False
                dr("numUnitHourReceived") = 0
                dr("numQtyShipped") = 0
                dr("numItemClassification") = numItemClassification

                If dtItem.Columns.Contains("numSelectedPromotionID") Then
                    dr("numSelectedPromotionID") = numSelectedPromotionID
                End If

                If dtItem.Columns.Contains("bitAlwaysCreateWO") Then
                    dr("bitAlwaysCreateWO") = bitAlwaysCreateWO
                End If

                If dtItem.Columns.Contains("numWOQty") Then
                    dr("numWOQty") = numWOQty
                End If

                If dtItem.Columns.Contains("bitDisablePromotion") Then
                    dr("bitDisablePromotion") = isDisablePromotion
                End If
            ElseIf IsPOS = True Then
                    If OppItemID > 0 Then
                        dr = dtItem.Rows.Find(OppItemID)
                        lngOppItemId = dr("numoppitemtCode")

                    End If

                Else
                    If OppItemID > 0 Then
                    dr = dtItem.Rows.Find(OppItemID)
                    lngOppItemId = dr("numoppitemtCode")
                End If
            End If

            If dtItem.Columns.Contains("ItemReleaseDate") Then
                If itemReleaseDate Is Nothing Then
                    dr("ItemReleaseDate") = DBNull.Value
                Else
                    dr("ItemReleaseDate") = itemReleaseDate
                End If
            End If

            If dtItem.Columns.Contains("ItemRequiredDate") Then
                If itemRequiredDate Is Nothing Then
                    dr("ItemRequiredDate") = DBNull.Value
                Else
                    dr("ItemRequiredDate") = itemRequiredDate
                End If
            End If

            If dtItem.Columns.Contains("CustomerPartNo") Then
                dr("CustomerPartNo") = CustomerPartNo
            End If

            If dtItem.Columns.Contains("numOnHand") AndAlso Not objItem Is Nothing Then
                dr("numOnHand") = objItem.OnHand
            End If

            If dtItem.Columns.Contains("numMaxWorkOrderQty") Then
                dr("numMaxWorkOrderQty") = numMaxWOQty
            End If

            If dtItem.Columns.Contains("Location") Then
                dr("Location") = ShipFromLocation
            End If

            If dtItem.Columns.Contains("numShipToAddressID") Then
                dr("numShipToAddressID") = numShipToAddressID
            End If

            If dtItem.Columns.Contains("ShipToFullAddress") Then
                dr("ShipToFullAddress") = ShipToFullAddress
            End If

            If dtItem.Columns.Contains("InclusionDetail") Then
                dr("InclusionDetail") = InclusionDetail
            End If

            If dtItem.Columns.Contains("OnHandAllocation") Then
                Try
                    If dr("OnHandAllocation") = "" AndAlso OnHandAllocation <> "" Then
                        dr("OnHandAllocation") = OnHandAllocation
                    End If
                Catch ex As Exception
                    dr("OnHandAllocation") = OnHandAllocation
                End Try
            End If

            'Strat - Changed By Sandeep
            'Reason - Following code is removed from above if condition because it is also required when item is updated.
            dr("bitWorkOrder") = bitWorkOrder

            If bitWorkOrder = True Then
                dr("vcInstruction") = vcInstruction
                dr("bintCompliationDate") = bintCompliationDate
                dr("numWOAssignedTo") = numWOAssignedTo
                dr("dtPlannedStart") = dtPlannedStart
            End If
            'End - Changed By Sandeep

            If objItem IsNot Nothing Then
                dr("fltItemWeight") = If(fltItemWeight = 0, CCommon.ToDecimal(objItem.Weight), fltItemWeight)
                dr("IsFreeShipping") = IsFreeShipping
                dr("fltHeight") = If(fltHeight = 0, CCommon.ToDecimal(objItem.Height), fltHeight)
                dr("fltWidth") = If(fltWidth = 0, CCommon.ToDecimal(objItem.Width), fltWidth)
                dr("fltLength") = If(fltLength = 0, CCommon.ToDecimal(objItem.Length), fltLength)

                If dtItem.Columns.Contains("bitHasKitAsChild") Then
                    dr("bitHasKitAsChild") = objItem.bitHasKitAsChild
                End If

                If dtItem.Columns.Contains("KitChildItems") Then
                    dr("KitChildItems") = objItem.vcKitChildItems
                End If
            End If

            dr("vcItemDesc") = Replace(Desc, "'", "''")

            If dtItem.Columns.Contains("vcVendorNotes") Then
                dr("vcVendorNotes") = vcVendorNotes
            End If
            dr("numUnitHour") = Units

            If dtItem.Columns.Contains("numOrigUnitHour") Then
                dr("numOrigUnitHour") = Units
            End If

            dr("monPrice") = Price

            If dtItem.Columns.Contains("monSalePrice") Then
                dr("monSalePrice") = salePrice
            Else
                salePrice = Price
            End If

            dr("numUOM") = UOM
            dr("vcUOMName") = vcUOMName
            dr("UOMConversionFactor") = UOMConversionFactor
            dr("monTotAmtBefDiscount") = Math.Truncate(10000 * CCommon.ToDouble((Units * UOMConversionFactor) * Price)) / 10000
            dr("vcBaseUOMName") = vcBaseUOMName

            If dtItem.Columns.Contains("bitItemPriceApprovalRequired") Then
                dr("bitItemPriceApprovalRequired") = False
            End If

            dr("bitDiscountType") = IIf(IsDiscountInPer = True, 0, 1)
            dr("fltDiscount") = Discount

            If dtItem.Columns.Contains("bitMarkupDiscount") Then
                dr("bitMarkupDiscount") = bitMarkupDiscount
            End If

            If IsDiscountInPer = True Then
                If Discount = 0 Then
                    dr("monTotAmount") = Math.Truncate(10000 * CCommon.ToDouble((Units * UOMConversionFactor) * salePrice)) / 10000
                Else
                    If (dtItem.Columns.Contains("bitMarkupDiscount") AndAlso dr("bitMarkupDiscount") = "1") Then
                        dr("monTotAmount") = Math.Truncate(10000 * (CCommon.ToDouble(dr("monTotAmtBefDiscount")) + CCommon.ToDouble(dr("monTotAmtBefDiscount") * dr("fltDiscount") / 100))) / 10000
                    Else
                        dr("monTotAmount") = Math.Truncate(10000 * (CCommon.ToDouble(dr("monTotAmtBefDiscount")) - CCommon.ToDouble(dr("monTotAmtBefDiscount") * dr("fltDiscount") / 100))) / 10000
                    End If
                End If
            Else
                If Discount = 0 Then
                    dr("monTotAmount") = Math.Truncate(10000 * CCommon.ToDouble((Units * UOMConversionFactor) * salePrice)) / 10000
                Else
                    If (dtItem.Columns.Contains("bitMarkupDiscount") AndAlso dr("bitMarkupDiscount") = "1") Then
                        dr("monTotAmount") = CCommon.ToDouble(dr("monTotAmtBefDiscount") + dr("fltDiscount"))
                    Else
                        dr("monTotAmount") = CCommon.ToDouble(dr("monTotAmtBefDiscount") - dr("fltDiscount"))
                    End If

                End If
            End If

            If dtItem.Columns.Contains("TotalDiscountAmount") Then
                If Not IsDiscountInPer Then
                    dr("TotalDiscountAmount") = dr("fltDiscount")
                Else
                    If dr("monTotAmount") > dr("monTotAmtBefDiscount") Then
                        dr("TotalDiscountAmount") = 0
                    Else
                        dr("TotalDiscountAmount") = dr("monTotAmtBefDiscount") - dr("monTotAmount")
                    End If
                End If
            End If

            dr("numVendorWareHouse") = numVendorWareHouse
            dr("numShipmentMethod") = numShipmentMethod
            dr("numSOVendorId") = numSOVendorId
            dr("vcSKU") = strSKU

            If PageType = "Sales" AndAlso chkTaxItems IsNot Nothing Then
                Dim k As Integer
                Dim strApplicable(), strTax() As String
                strApplicable = Taxable.Split(",")
                strTax = Tax.Split(",")

                For k = 0 To chkTaxItems.Items.Count - 1
                    If strApplicable.Length > k Then
                        If CCommon.ToBool(strApplicable(k)) = True Then
                            Dim decTaxValue As Decimal = CCommon.ToDecimal(strTax(k).Split("#")(0))
                            Dim tintTaxType As Short = CCommon.ToShort(strTax(k).Split("#")(1))

                            If tintTaxType = 2 Then 'FLAT AMOUNT
                                dr("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * (dr("numUnitHour") * dr("UOMConversionFactor"))
                            Else 'PERCENTAGE
                                dr("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * dr("monTotAmount") / 100
                            End If

                            dr("bitTaxable" & chkTaxItems.Items(k).Value) = True
                        Else
                            dr("Tax" & chkTaxItems.Items(k).Value) = 0
                            dr("bitTaxable" & chkTaxItems.Items(k).Value) = False
                        End If
                    End If
                Next
            End If

            If ItemTypeValue = "P" Then
                If DropShip = False Then
                    Dim objItems As New CItems
                    objItems.WareHouseItemID = WareHouseItemID
                    dr("numWarehouseID") = objItems.GetWarehouseID
                    dr("Warehouse") = Warehouse
                    dr("numWarehouseItmsID") = WareHouseItemID
                    If dtItem.Columns.Contains("Attributes") Then
                        dr("Attributes") = objCommon.GetAttributesForWarehouseItem(dr("numWarehouseItmsID"), False)
                    End If
                    dr("DropShip") = False
                    dr("numToWarehouseItemID") = ToWarehouseItemID
                    dr("numVendorID") = 0
                Else
                    dr("numWarehouseID") = DBNull.Value
                    dr("numWarehouseItmsID") = DBNull.Value
                    dr("Warehouse") = ""
                    If dtItem.Columns.Contains("Attributes") Then
                        dr("Attributes") = ""
                    End If
                    dr("DropShip") = True
                    dr("numVendorID") = numPrimaryVendorID
                End If
            Else
                dr("numWarehouseID") = DBNull.Value
                dr("numWarehouseItmsID") = DBNull.Value
                dr("numVendorID") = numPrimaryVendorID
                dr("DropShip") = DropShip
                If AddMode Then
                    If dtItem.Columns.Contains("Attributes") Then
                        dr("Attributes") = vcAttributes
                    End If

                    If dtItem.Columns.Contains("AttributeIDs") Then
                        dr("AttributeIDs") = vcAttributeIDs
                    End If
                End If
            End If
            If numContainer <> -1 Then
                If dtItem.Columns.Contains("numContainer") Then
                    dr("numContainer") = numContainer
                End If
            End If
            If numContainerQty <> -1 Then
                If dtItem.Columns.Contains("numContainerQty") Then
                    dr("numContainerQty") = numContainerQty

                End If
            End If

            If dtItem.Columns.Contains("numCost") Then
                dr("numCost") = numCost
            End If

            If AddMode = True Then
                dtItem.Rows.Add(dr)
            End If

            If bDirectAdd = False Then
                If dtChildItems.Rows.Count > 0 Then
                    For i As Int32 = 0 To dtChildItems.Rows.Count - 1
                        If (dsTemp.Tables(0).Select("numItemCode='" & dtChildItems.Rows(i)("numItemCode") & "'").Length = 0) Then
                            dr = dtItem.NewRow

                            dr("Op_Flag") = 1
                            dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1 'dtItem.Rows.Count + 1
                            dr("numItemCode") = dtChildItems.Rows(i)("numItemCode")

                            If IsDBNull(dtChildItems.Rows(i)("numQtyItemsReq")) Then
                                dr("numUnitHour") = 0
                            ElseIf dtChildItems.Rows(i)("numQtyItemsReq").ToString.Length = 0 Then
                                dr("numUnitHour") = 0
                            Else
                                dr("numUnitHour") = Math.Abs(CCommon.ToDecimal(dtChildItems.Rows(i)("numQtyItemsReq").ToString()))
                            End If

                            dr("monPrice") = CCommon.ToDecimal(dtChildItems.Rows(i)("monListPrice").ToString())
                            If dtItem.Columns.Contains("monSalePrice") Then
                                dr("monSalePrice") = CCommon.ToDecimal(dtChildItems.Rows(i)("monListPrice").ToString())
                            End If

                            dr("bitDiscountType") = False
                            dr("fltDiscount") = 0
                            dr("monTotAmtBefDiscount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))

                            dr("monTotAmount") = dr("monTotAmtBefDiscount") - dr("fltDiscount")

                            If PageType = "Sales" Then
                                dr("Tax0") = 0
                                dr("bitTaxable0") = False
                            End If

                            dr("vcItemDesc") = dtChildItems.Rows(i)("txtItemDesc")

                            If dtChildItems.Rows(i)("numWarehouseItmsID") <> 0 Then
                                dr("numWarehouseItmsID") = dtChildItems.Rows(i)("numWarehouseItmsID")
                                dr("Warehouse") = dtChildItems.Rows(i)("vcWareHouse")
                            Else
                                dr("numWarehouseItmsID") = DBNull.Value
                                dr("Warehouse") = DBNull.Value
                            End If

                            dr("DropShip") = False
                            dr("bitIsAuthBizDoc") = False
                            dr("numUnitHourReceived") = 0
                            dr("numQtyShipped") = 0

                            dr("vcItemName") = dtChildItems.Rows(i)("vcItemName")
                            dr("ItemType") = dtChildItems.Rows(i)("charItemType")

                            dr("UOMConversionFactor") = 1
                            dr("numUOM") = 0
                            dr("vcUOMName") = ""
                            dr("bitWorkOrder") = False
                            dr("vcBaseUOMName") = ""

                            dr("numVendorWareHouse") = "0"
                            dr("numShipmentMethod") = "0"
                            dr("numSOVendorId") = "0"

                            If numProjectID > 0 AndAlso numProjectStageID > 0 Then
                                dr("numProjectID") = numProjectID
                                dr("numProjectStageID") = numProjectStageID
                            Else
                                dr("numProjectID") = 0
                                dr("numProjectStageID") = 0
                            End If
                            dr("charItemType") = dtChildItems.Rows(i)("charItemType")

                            dtItem.Rows.Add(dr)
                        End If
                    Next
                End If
            End If

            dr.AcceptChanges()
            dsTemp.AcceptChanges()

            If PageType = "Sales" Or AddMode = False Then
                If dtSerItem.ParentRelations.Count = 0 Then dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
            End If

            Return dsTemp
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class
