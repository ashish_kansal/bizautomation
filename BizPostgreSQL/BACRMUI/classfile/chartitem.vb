Imports System
Imports System.Collections
Imports System.Diagnostics
Imports System.Drawing

Namespace ASPNET.StarterKit.Chart

    '*********************************************************************
    '
    ' ChartItem Class
    '
    ' This class represents a data point in a chart
    '
    '*********************************************************************

    Public Class ChartItem
        Private _label As String
        Private _description As String
        Private _value As Single
        Private _color As Color
        Private _startPos As Single
        Private _sweepSize As Single

        Private Sub New()
        End Sub 'New

        Public Sub New(ByVal label As String, ByVal desc As String, ByVal data As Single, ByVal start As Single, ByVal sweep As Single, ByVal clr As Color)
            _label = label
            _description = desc
            _value = data
            _startPos = start
            _sweepSize = sweep
            _color = clr
        End Sub 'New

        Public Property Label() As String
            Get
                Return _label
            End Get
            Set(ByVal Value As String)
                _label = Value
            End Set
        End Property

        Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal Value As String)
                _description = Value
            End Set
        End Property

        Public Property Value() As Single
            Get
                Return _value
            End Get
            Set(ByVal Value As Single)
                _value = Value
            End Set
        End Property

        Public Property ItemColor() As Color
            Get
                Return _color
            End Get
            Set(ByVal Value As Color)
                _color = Value
            End Set
        End Property

        Public Property StartPos() As Single
            Get
                Return _startPos
            End Get
            Set(ByVal Value As Single)
                _startPos = Value
            End Set
        End Property

        Public Property SweepSize() As Single
            Get
                Return _sweepSize
            End Get
            Set(ByVal Value As Single)
                _sweepSize = Value
            End Set
        End Property
    End Class 'ChartItem

    '*********************************************************************
    '
    ' Custom Collection for ChartItems
    '
    '*********************************************************************

    Public Class ChartItemsCollection
        Inherits CollectionBase

        Default Public Property Item(ByVal index As Integer) As ChartItem
            Get
                Return CType(List(index), ChartItem)
            End Get
            Set(ByVal Value As ChartItem)
                List(index) = Value
            End Set
        End Property

        Public Function Add(ByVal value As ChartItem) As Integer
            Return List.Add(value)
        End Function 'Add

        Public Function IndexOf(ByVal value As ChartItem) As Integer
            Return List.IndexOf(value)
        End Function 'IndexOf

        Public Function Contains(ByVal value As ChartItem) As Boolean
            Return List.Contains(value)
        End Function 'Contains

        Public Sub Remove(ByVal value As ChartItem)
            List.Remove(value)
        End Sub 'Remove
    End Class 'ChartItemsCollection
End Namespace 'ASPNET.StarterKit.Chart

