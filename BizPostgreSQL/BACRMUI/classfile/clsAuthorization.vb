
Imports BACRM.BusinessLogic.Common

Public Class clsAuthorization

    Public Shared Function fn_GetPageListUserRights_old(ByVal objCommon As CCommon, ByVal PageName As String, ByVal lngUserID As Long, ByVal numModuleID As Long, ByVal numPageID As Long) As Array

        'Check page access rights from Cached XML for current auth group

        If objCommon Is Nothing Then
            objCommon = New CCommon
        End If
        objCommon.UserID = lngUserID
        objCommon.ModuleID = CInt(numModuleID)
        objCommon.PageID = CInt(numPageID)
        Return objCommon.PageLevelUserRights
    End Function



End Class
