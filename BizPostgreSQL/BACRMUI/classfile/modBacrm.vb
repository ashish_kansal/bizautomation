'================================================================================
'
' Module Name:      modBacrm
' Description:      This module contains the main common functions & procedures 
'                   which will be used by most of the pages.
' Created By:       Anup Jishnu
' Creation Date:    06/02/2003http://localhost/Files_1/marketingdateselection.aspx
'================================================================================

Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Web
Imports QueryStringVal

Module modBacrm

    'This enum is created for the rights on a specific page on Page Level.
    Public Enum RIGHTSTYPE
        VIEW = 0
        ADD = 1
        UPDATE = 2
        DELETE = 3
        EXPORT = 4
    End Enum

    'The following Enum is for rthe Rights values.
    Public Enum RIGHTSVALUE
        NO_RIGHTS = 0
        ONLY_OWNER = 1
        ONLY_TERRITORY = 2
        ALL_RECORDS = 3
    End Enum

    Function AddQuotes(ByVal strValue)
        Dim QUOTE As String
        QUOTE = """"
        AddQuotes = QUOTE & Replace(strValue, QUOTE, QUOTE & QUOTE) & QUOTE
    End Function

    Sub FillState(ByVal ddl As DropDownList, ByVal lngCountry As Long, ByVal lngDomainID As Long)
        Dim dtTable As DataTable
        Dim objUserAccess As New UserAccess
        objUserAccess.DomainID = lngDomainID
        objUserAccess.Country = lngCountry
        dtTable = objUserAccess.SelState
        ddl.DataSource = dtTable
        ddl.DataTextField = "vcState"
        ddl.DataValueField = "numStateID"
        ddl.DataBind()
        ddl.Items.Insert(0, "--Select One--")
        ddl.Items.FindByText("--Select One--").Value = 0
    End Sub


    Sub FillAllState(ByVal ddl As DropDownList, ByVal lngDomainID As Long)
        Dim dtTable As DataTable
        Dim objUserAccess As New UserAccess
        objUserAccess.DomainID = lngDomainID
        dtTable = objUserAccess.SelAllStates
        ddl.DataSource = dtTable
        ddl.DataTextField = "vcState"
        ddl.DataValueField = "numStateID"
        ddl.DataBind()
    End Sub


    Sub FillState(ByVal lst As ListBox, ByVal lngCountry As Long, ByVal lngDomainID As Long)
        Dim dtTable As DataTable
        Dim objUserAccess As New UserAccess
        objUserAccess.DomainID = lngDomainID
        objUserAccess.Country = lngCountry
        dtTable = objUserAccess.SelState
        lst.DataSource = dtTable
        lst.DataTextField = "vcState"
        lst.DataValueField = "numStateID"
        lst.DataBind()
    End Sub

    Public Function FormattedDateFromDate(ByVal dtDate As Date, ByVal strFormat As String) As String
        Dim str4Yr As String = Year(dtDate)
        'Set the Year in 2 Digits to a Variable
        Dim str2Yr As String = Mid(Year(dtDate), 3, 2)
        'Set the Month in 2 Digits to a Variable
        Dim strIntMonth As String = Month(dtDate)

        If strIntMonth <= 9 Then
            strIntMonth = "0" & strIntMonth
        End If
        'Set the Date in 2 Digits to a Variable
        Dim strDate As String = Day(dtDate)

        If strDate <= 9 Then
            strDate = "0" & strDate
        End If
        'Set the Abbrivated Month Name to a Variable
        Dim str3Month As String = MonthName(strIntMonth, True)
        'Set the Full Month Name to a Variable
        Dim strFullMonth As String = MonthName(strIntMonth, False)
        'Set the Nos of Hrs to a Variable
        Dim strHrs As String = Hour(dtDate)
        'Set the Nos of Mins to a Variable
        Dim strMins As String = Minute(dtDate)

        'As the Date Format will be one of the above mentioned formats,
        'we need to replace the required string to get the formatted date.
        strFormat = Replace(strFormat, "DD", strDate)
        strFormat = Replace(strFormat, "YYYY", str4Yr)
        strFormat = Replace(strFormat, "YY", str2Yr)
        strFormat = Replace(strFormat, "MM", strIntMonth)
        strFormat = Replace(strFormat, "MONTH", strFullMonth)
        strFormat = Replace(strFormat, "MON", str3Month)

        Return strFormat
    End Function


    Public Function FormattedDateTimeFromDate(ByVal dtDate As Date, ByVal strFormat As String) As String
        Dim str4Yr As String = Year(dtDate)
        'Set the Year in 2 Digits to a Variable
        Dim str2Yr As String = Mid(Year(dtDate), 3, 2)
        'Set the Month in 2 Digits to a Variable
        Dim strIntMonth As String = Month(dtDate)
        If strIntMonth <= 9 Then
            strIntMonth = "0" & strIntMonth
        End If
        'Set the Date in 2 Digits to a Variable
        Dim strDate As String = Day(dtDate)

        If strDate <= 9 Then
            strDate = "0" & strDate
        End If
        Dim str3Month As String = MonthName(strIntMonth, True)
        'Set the Full Month Name to a Variable
        Dim strFullMonth As String = MonthName(strIntMonth, False)
        'Set the Nos of Hrs to a Variable
        Dim strHrs As String = Hour(dtDate)
        'Set the Nos of Mins to a Variable
        Dim strMins As String = Minute(dtDate)
        If strMins <= 9 Then
            strMins = "0" & strMins
        End If

        strFormat = Replace(strFormat, "DD", strDate)
        strFormat = Replace(strFormat, "YYYY", str4Yr)
        strFormat = Replace(strFormat, "YY", str2Yr)
        strFormat = Replace(strFormat, "MM", strIntMonth)
        strFormat = Replace(strFormat, "MONTH", strFullMonth)
        strFormat = Replace(strFormat, "MON", str3Month)


        Dim offset As Integer = -(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
        Dim strNow As Date
        strNow = DateAdd(DateInterval.Minute, offset, Date.UtcNow)


        If Format(dtDate, "yyyyMMdd") = Format(strNow, "yyyyMMdd") Then
            strFormat = "<b><font color=red>Today</font></b>"
        ElseIf Format(dtDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, strNow), "yyyyMMdd") Then
            strFormat = "<b><font color=orange>" & strFormat & "</font></b>"
        ElseIf Format(dtDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, -1, strNow), "yyyyMMdd") Then
            strFormat = "<b><font color=purple>" & strFormat & "</font></b>"
        End If
        strFormat &= " " & IIf(strHrs > 12, CInt(strHrs) - 12, strHrs) & ":" & strMins & " " & IIf(strHrs >= 12, "PM", "AM")

        'Return the formatted date.        
        Return strFormat
    End Function

    Public Function fn_GetDateTimeFromNumber(ByVal dtDate As Date, ByVal strFormat As String) As String
        Dim str4Yr As String = Year(dtDate)
        'Set the Year in 2 Digits to a Variable
        Dim str2Yr As String = Mid(Year(dtDate), 3, 2)
        'Set the Month in 2 Digits to a Variable
        Dim strIntMonth As String = Month(dtDate)
        'Set the Date in 2 Digits to a Variable
        Dim strDate As String = Day(dtDate)
        'Set the Abbrivated Month Name to a Variable
        Dim str3Month As String = MonthName(strIntMonth, True)
        'Set the Full Month Name to a Variable
        Dim strFullMonth As String = MonthName(strIntMonth, False)
        'Set the Nos of Hrs to a Variable
        Dim strHrs As String = Hour(dtDate)
        'Set the Nos of Mins to a Variable
        Dim strMins As String = Minute(dtDate)

        'As the Date Format will be one of the above mentioned formats,
        'we need to replace the required string to get the formatted date.
        strFormat = Replace(strFormat, "DD", strDate)
        strFormat = Replace(strFormat, "YYYY", str4Yr)
        strFormat = Replace(strFormat, "YY", str2Yr)
        strFormat = Replace(strFormat, "MM", strIntMonth)
        strFormat = Replace(strFormat, "MONTH", strFullMonth)
        strFormat = Replace(strFormat, "MON", str3Month)
        strFormat &= " " & IIf(strHrs > 12, CInt(strHrs) - 12, strHrs) & ":" & strMins & " " & IIf(strHrs > 12, "PM", "AM")

        Return strFormat
    End Function

    'Public Function DateFromFormattedDate(ByVal strDate As String, ByVal strCrtFormat As String) As Date
    '    If strCrtFormat = "MM/DD/YYYY" Then
    '        DateFromFormattedDate = Mid(strDate, 4, 2) & "/" & Left(strDate, 2) & "/" & Right(strDate, 4)
    '    ElseIf strCrtFormat = "MM-DD-YYYY" Then
    '        DateFromFormattedDate = Mid(strDate, 4, 2) & "/" & Left(strDate, 2) & "/" & Right(strDate, 4)
    '    ElseIf strCrtFormat = "DD/MON/YYYY" Then
    '        DateFromFormattedDate = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD-MON-YYYY" Then
    '        DateFromFormattedDate = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD/MONTH/YYYY" Then
    '        DateFromFormattedDate = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD-MONTH-YYYY" Then
    '        DateFromFormattedDate = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD/MM/YYYY" Then
    '        DateFromFormattedDate = Left(strDate, 2) & "/" & Mid(strDate, 4, 2) & "/" & Right(strDate, 4)
    '    ElseIf strCrtFormat = "DD-MM-YYYY" Then
    '        DateFromFormattedDate = Left(strDate, 2) & "/" & Mid(strDate, 4, 2) & "/" & Right(strDate, 4)
    '    End If
    'End Function

    'Public Function GetDateFrmFormattedDateAddDays(ByVal strDate As String, ByVal strCrtFormat As String) As Date
    '    Dim str As Date
    '    If strCrtFormat = "MM/DD/YYYY" Then
    '        str = Mid(strDate, 4, 2) & "/" & Left(strDate, 2) & "/" & Right(strDate, 4)
    '    ElseIf strCrtFormat = "MM/DD/YY" Then
    '        str = Mid(strDate, 4, 2) & "/" & "20" & Left(strDate, 2) & "/" & Right(strDate, 2)
    '    ElseIf strCrtFormat = "MM-DD-YYYY" Then
    '        str = Mid(strDate, 4, 2) & "/" & Left(strDate, 2) & "/" & Right(strDate, 4)
    '    ElseIf strCrtFormat = "MM-DD-YY" Then
    '        str = Mid(strDate, 4, 2) & "/" & Left(strDate, 2) & "/" & "20" & Right(strDate, 2)
    '    ElseIf strCrtFormat = "DD/MON/YYYY" Then
    '        str = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD/MON/YY" Then
    '        str = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD-MON-YYYY" Then
    '        str = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD-MON-YY" Then
    '        str = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD/MONTH/YYYY" Then
    '        str = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD/MONTH/YY" Then
    '        str = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD-MONTH-YYYY" Then
    '        str = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD-MONTH-YY" Then
    '        str = IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & Year(strDate)
    '    ElseIf strCrtFormat = "DD/MM/YYYY" Then
    '        str = Left(strDate, 2) & "/" & Mid(strDate, 4, 2) & "/" & Right(strDate, 4)
    '    ElseIf strCrtFormat = "DD/MM/YY" Then
    '        str = Left(strDate, 2) & "/" & Mid(strDate, 4, 2) & "/" & "20" & Right(strDate, 2)
    '    ElseIf strCrtFormat = "DD-MM-YYYY" Then
    '        str = Left(strDate, 2) & "/" & Mid(strDate, 4, 2) & "/" & Right(strDate, 4)
    '    ElseIf strCrtFormat = "DD-MM-YY" Then
    '        str = Left(strDate, 2) & "/" & Mid(strDate, 4, 2) & "/" & "20" & Right(strDate, 2)
    '    End If
    '    str = DateAdd(DateInterval.Day, 1, str)
    '    Return str
    'End Function

    Public Function DateFromFormattedDate(ByVal strDate As String, ByVal strCrtFormat As String) As Date
        Dim dtDate As Date
        If strCrtFormat = "MM/DD/YYYY" Then
            dtDate = New Date(Right(strDate, 4), Left(strDate, 2), Mid(strDate, 4, 2))
        ElseIf strCrtFormat = "MM-DD-YYYY" Then
            ''DateFromFormattedDate = Left(strDate, 2) & "/" & Mid(strDate, 4, 2) & "/" & Right(strDate, 4)
            dtDate = New Date(Right(strDate, 4), Left(strDate, 2), Mid(strDate, 4, 2))
        ElseIf strCrtFormat = "DD/MON/YYYY" Then
            '' DateFromFormattedDate = IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & Year(strDate)
            dtDate = New Date(Year(strDate), IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)), IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)))
        ElseIf strCrtFormat = "DD-MON-YYYY" Then
            ''DateFromFormattedDate = IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & Year(strDate)
            dtDate = New Date(Year(strDate), IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)), IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)))
        ElseIf strCrtFormat = "DD/MONTH/YYYY" Then
            ''DateFromFormattedDate = IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & Year(strDate)
            dtDate = New Date(Year(strDate), IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)), IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)))
        ElseIf strCrtFormat = "DD-MONTH-YYYY" Then
            ''DateFromFormattedDate = IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & Year(strDate)
            dtDate = New Date(Year(strDate), IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)), IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)))
        ElseIf strCrtFormat = "DD/MM/YYYY" Then
            ''DateFromFormattedDate = Mid(strDate, 4, 2) & "/" & Left(strDate, 2) & "/" & Right(strDate, 4)
            dtDate = New Date(Right(strDate, 4), Mid(strDate, 4, 2), Left(strDate, 2))
        ElseIf strCrtFormat = "DD-MM-YYYY" Then
            ''DateFromFormattedDate = Mid(strDate, 4, 2) & "/" & Left(strDate, 2) & "/" & Right(strDate, 4)
            dtDate = New Date(Right(strDate, 4), Mid(strDate, 4, 2), Left(strDate, 2))
        End If
        Return dtDate
    End Function

    Public Function GetDateFrmFormattedDateAddDays(ByVal strDate As String, ByVal strCrtFormat As String) As Date

        Dim dtDate As Date
        If strCrtFormat = "MM/DD/YYYY" Then
            ''str = Left(strDate, 2) & "/" & Mid(strDate, 4, 2) & "/" & Right(strDate, 4)
            dtDate = New Date(Right(strDate, 4), Left(strDate, 2), Mid(strDate, 4, 2))
        ElseIf strCrtFormat = "MM-DD-YYYY" Then
            ''str = Left(strDate, 2) & "/" & Mid(strDate, 4, 2) & "/" & Right(strDate, 4)
            dtDate = New Date(Right(strDate, 4), Left(strDate, 2), Mid(strDate, 4, 2))
        ElseIf strCrtFormat = "DD/MON/YYYY" Then
            ''str = IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & Year(strDate)
            dtDate = New Date(Year(strDate), IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)), IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)))
        ElseIf strCrtFormat = "DD-MON-YYYY" Then
            ''str = IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & Year(strDate)
            dtDate = New Date(Year(strDate), IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)), IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)))
        ElseIf strCrtFormat = "DD/MONTH/YYYY" Then
            ''str = IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & Year(strDate)
            dtDate = New Date(Year(strDate), IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)), IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)))
        ElseIf strCrtFormat = "DD-MONTH-YYYY" Then
            ''str = IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)) & "/" & IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)) & "/" & Year(strDate)
            dtDate = New Date(Year(strDate), IIf(Month(strDate) > 9, Month(strDate), "0" & Month(strDate)), IIf(Day(strDate) > 9, Day(strDate), "0" & Day(strDate)))
        ElseIf strCrtFormat = "DD/MM/YYYY" Then
            ''str = Mid(strDate, 4, 2) & "/" & Left(strDate, 2) & "/" & Right(strDate, 4)
            dtDate = New Date(Right(strDate, 4), Mid(strDate, 4, 2), Left(strDate, 2))
        ElseIf strCrtFormat = "DD-MM-YYYY" Then
            ''str = Mid(strDate, 4, 2) & "/" & Left(strDate, 2) & "/" & Right(strDate, 4)
            dtDate = New Date(Right(strDate, 4), Mid(strDate, 4, 2), Left(strDate, 2))
        End If
        dtDate = DateAdd(DateInterval.Day, 1, dtDate)
        Return dtDate
    End Function


    Public Function GetQueryStringVal(ByVal querystring As String, ByVal fldkey As String)
        If HttpContext.Current.Session("objQSV") Is Nothing Then
            Dim objQSV As New GetQueryStringVal
            HttpContext.Current.Session("objQSV") = objQSV
        End If
        Return HttpContext.Current.Session("objQSV").GetQueryStringVal(querystring, fldkey)
    End Function
    Public Function GetLocalDate(ByVal strDate As String) As Date
        Dim dtDate As Date
        Dim offset As Integer = -(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
        dtDate = DateAdd(DateInterval.Minute, offset, CDate(strDate))
        Return dtDate
    End Function


End Module
