Imports System
Imports System.Drawing
Imports System.Collections

Namespace ASPNET.StarterKit.Chart

    '*********************************************************************
    '
    ' Chart Class
    '
    ' Base class implementation for BarChart and PieChart
    '
    '*********************************************************************

    Public MustInherit Class Chart
        Private _colorLimit As Integer = 12

        Private _color As Color() = {Color.Chocolate, Color.FromArgb(188, 197, 205), Color.FromArgb(119, 136, 153), Color.DarkKhaki, Color.Sienna, Color.PaleGoldenrod, Color.Peru, Color.Tan, Color.Khaki, Color.Blue, Color.Maroon, Color.OliveDrab}

        ' Represent collection of all data points for the chart
        Private _dataPoints As New ChartItemsCollection

        ' The implementation of this method is provided by derived classes
        Public MustOverride Function Draw() As Bitmap

        Public Property DataPoints() As ChartItemsCollection
            Get
                Return _dataPoints
            End Get
            Set(ByVal Value As ChartItemsCollection)
                _dataPoints = Value
            End Set
        End Property

        Public Sub SetColor(ByVal index As Integer, ByVal NewColor As Color)
            If index < _colorLimit Then
                _color(index) = NewColor
            Else
                Throw New Exception("Color Limit is " + _colorLimit)
            End If
        End Sub 'SetColor

        Public Function GetColor(ByVal index As Integer) As Color
            If index < _colorLimit Then
                Return _color(index)
            Else
                Throw New Exception("Color Limit is " + _colorLimit)
            End If
        End Function 'GetColor
    End Class 'Chart
End Namespace 'ASPNET.StarterKit.Chart

