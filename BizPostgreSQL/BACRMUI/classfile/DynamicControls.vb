Imports BACRM.BusinessLogic.Admin
Imports System.Reflection
Imports System.web.ui.TemplateControl
Module DynamicControls


    Public Function CreateTexBox(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal strText As String)
     
        Dim txt As New TextBox
        txt.CssClass = "signup"
        txt.ID = strId
        If strText <> "0" Then
            txt.Text = strText
        Else
            txt.Text = ""
        End If
        txt.Width = Unit.Pixel(180)
        tblCell.Controls.Add(txt)
        tblRow.Cells.Add(tblCell)
    End Function

    Public Function CreateDropdown(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal intSelValue As Integer, ByVal intListId As Integer) As DropDownList

        Dim ddl As New DropDownList
        ddl.CssClass = "signup"
        Dim dtTable As DataTable
        Dim ObjCusfld As New CustomFields
        ObjCusfld.ListId = CInt(intListId)
        dtTable = ObjCusfld.GetMasterListByListId
        ddl.ID = strId
        ddl.DataSource = dtTable
        ddl.DataTextField = "vcData"
        ddl.DataValueField = "numListItemID"
        ddl.DataBind()
        ddl.Items.Insert(0, "--Select One--")
        ddl.Items.FindByText("--Select One--").Value = 0
        ddl.Width = Unit.Pixel(180)

        tblCell.Controls.Add(ddl)
        tblRow.Cells.Add(tblCell)
        If intSelValue > 0 AndAlso Not ddl.Items.FindByValue(intSelValue) Is Nothing Then
            ddl.Items.FindByValue(intSelValue).Selected = True
        End If

        Return ddl
    End Function

    Public Function CreateTextArea(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal strText As String)
      

        Dim txt As New TextBox
        txt.CssClass = "signup"
        txt.Width = Unit.Pixel(200)
        txt.Height = Unit.Pixel(40)
        txt.TextMode = TextBoxMode.MultiLine
        txt.ID = strId
        If strText <> "0" Then
            txt.Text = strText
        Else
            txt.Text = ""
        End If
        tblCell.Controls.Add(txt)
        tblRow.Cells.Add(tblCell)
    End Function


    Public Function CreateChkBox(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal intSel As Integer) As CheckBox

        Dim chk As New CheckBox
        chk.ID = strId
        If intSel = 1 Then
            chk.Checked = True
        Else
            chk.Checked = False
        End If
        tblCell.Controls.Add(chk)
        tblRow.Cells.Add(tblCell)

        Return chk
    End Function


    Public Function CreateLink(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal URL As String, ByVal RecID As Long, ByVal Label As String)
        Dim hpl As New HyperLink
        hpl.ID = "hpl" & strId
        hpl.Text = Label
        URL = URL.Replace("RecordID", RecID)
        hpl.NavigateUrl = URL
        hpl.Target = "_blank"
        hpl.CssClass = "hyperlink"
        tblCell.Controls.Add(hpl)
        tblRow.Cells.Add(tblCell)
    End Function

    Public Function CreateCheckBoxList(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal intSelValue As String, ByVal intListId As Integer) As CheckBoxList

        Dim chkbl As New CheckBoxList
        chkbl.CssClass = "signup"
        Dim dtTable As DataTable
        Dim ObjCusfld As New CustomFields
        ObjCusfld.ListId = CInt(intListId)
        dtTable = ObjCusfld.GetMasterListByListId
        chkbl.ID = strId

        Dim tempValue As String()
        If Not String.IsNullOrEmpty(intSelValue) Then
            tempValue = intSelValue.Split(CChar(","))
        End If

        For Each tempDr As DataRow In dtTable.Rows
            Dim chkblItem As New ListItem
            chkblItem.Text = Convert.ToString(tempDr("vcData"))
            chkblItem.Value = Convert.ToString(tempDr("numListItemID"))

            If Not tempValue Is Nothing AndAlso tempValue.Contains(Convert.ToString(tempDr("numListItemID"))) Then
                chkblItem.Selected = True
            End If

            chkbl.Items.Add(chkblItem)
        Next

        tblCell.Controls.Add(chkbl)
        tblRow.Cells.Add(tblCell)

        Return chkbl
    End Function

    Public Function CreateRadComboBox(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal intSelValue As Integer, ByVal intListId As Integer) As Telerik.Web.UI.RadComboBox
        Dim radComboBox As Telerik.Web.UI.RadComboBox = New Telerik.Web.UI.RadComboBox

        Try
            radComboBox.Skin = "Default"
            radComboBox.Attributes.Add("ListID", intListId)
            radComboBox.AllowCustomText = True
            radComboBox.ShowMoreResultsBox = True
            radComboBox.ClientIDMode = System.Web.UI.ClientIDMode.Static
            radComboBox.ID = strId
            radComboBox.EnableLoadOnDemand = True
            radComboBox.ItemsPerRequest = 20

            tblCell.Controls.Add(radComboBox)
            tblRow.Cells.Add(tblCell)
        Catch ex As Exception
            Throw
        End Try

        Return radComboBox
    End Function

#Region "Bootstrap Controls"
    Public Sub CreateTexBox(ByVal div As HtmlGenericControl, ByVal strId As String, ByVal strText As String)
        Dim txt As New TextBox
        txt.CssClass = "form-control"
        txt.ID = strId

        If strText <> "0" Then
            txt.Text = strText
        Else
            txt.Text = ""
        End If

        div.Controls.Add(txt)
    End Sub

    Public Function CreateDropdown(ByVal div As HtmlGenericControl, ByVal strId As String, ByVal intSelValue As Integer, ByVal intListId As Integer) As DropDownList

        Dim ddl As New DropDownList
        ddl.CssClass = "form-control"
        Dim dtTable As DataTable
        Dim ObjCusfld As New CustomFields
        ObjCusfld.ListId = CInt(intListId)
        dtTable = ObjCusfld.GetMasterListByListId
        ddl.ID = strId
        ddl.DataSource = dtTable
        ddl.DataTextField = "vcData"
        ddl.DataValueField = "numListItemID"
        ddl.DataBind()
        ddl.Items.Insert(0, "--Select One--")
        ddl.Items.FindByText("--Select One--").Value = 0

        If intSelValue > 0 AndAlso Not ddl.Items.FindByValue(intSelValue) Is Nothing Then
            ddl.Items.FindByValue(intSelValue).Selected = True
        End If

        div.Controls.Add(ddl)

        Return ddl
    End Function

    Public Sub CreateChkBox(ByVal div As HtmlGenericControl, ByVal strId As String, ByVal intSel As Integer)
        Dim divInner As New HtmlGenericControl("div")

        Dim chk As New CheckBox
        chk.ID = strId

        If intSel = 1 Then
            chk.Checked = True
        Else
            chk.Checked = False
        End If

        divInner.Controls.Add(chk)
        div.Controls.Add(divInner)
    End Sub

    Public Sub CreateTextArea(ByVal div As HtmlGenericControl, ByVal strId As String, ByVal strText As String)
        Dim txt As New TextBox
        txt.CssClass = "form-control"
        txt.TextMode = TextBoxMode.MultiLine
        txt.ID = strId
        If strText <> "0" Then
            txt.Text = strText
        Else
            txt.Text = ""
        End If
        div.Controls.Add(txt)
    End Sub

    Public Function CreateLink(ByVal div As HtmlGenericControl, ByVal strId As String, ByVal URL As String, ByVal RecID As Long, ByVal Label As String)
        Dim divInner As New HtmlGenericControl("div")

        Dim hpl As New HyperLink
        hpl.ID = "hpl" & strId
        hpl.Text = Label
        URL = URL.Replace("RecordID", RecID)
        hpl.NavigateUrl = URL
        hpl.Target = "_blank"

        divInner.Controls.Add(hpl)
        div.Controls.Add(divInner)
    End Function

    Public Sub CreateCheckBoxList(ByVal div As HtmlGenericControl, ByVal strId As String, ByVal intSelValue As String, ByVal intListId As Integer)
        Dim divInner As New HtmlGenericControl("div")

        Dim chkbl As New CheckBoxList

        Dim dtTable As DataTable
        Dim ObjCusfld As New CustomFields
        ObjCusfld.ListId = CInt(intListId)
        dtTable = ObjCusfld.GetMasterListByListId
        chkbl.ID = strId

        Dim tempValue As String()
        If Not String.IsNullOrEmpty(intSelValue) Then
            tempValue = intSelValue.Split(CChar(","))
        End If

        For Each tempDr As DataRow In dtTable.Rows
            Dim chkblItem As New ListItem
            chkblItem.Text = Convert.ToString(tempDr("vcData"))
            chkblItem.Value = Convert.ToString(tempDr("numListItemID"))

            If Not tempValue Is Nothing AndAlso tempValue.Contains(Convert.ToString(tempDr("numListItemID"))) Then
                chkblItem.Selected = True
            End If

            chkbl.Items.Add(chkblItem)
        Next

        divInner.Controls.Add(chkbl)
        div.Controls.Add(divInner)
    End Sub
#End Region


End Module
