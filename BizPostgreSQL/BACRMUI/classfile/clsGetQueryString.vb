Imports System
Imports System.IO
Imports System.Web
Imports System.Text
Imports System.Security.Cryptography
Namespace QueryStringVal
    Public Class GetQueryStringVal
        Private Const PARAMETER_NAME As String = "enc="
        Private Const ENCRYPTION_KEY As String = "tOtAlbIzV1"
        Private Shared ReadOnly SALT As Byte() = Encoding.ASCII.GetBytes(ENCRYPTION_KEY.Length.ToString)
        Public Function GetQueryStringVal(ByVal Qstr As String, ByVal Field As String) As String
            Try
                Dim retval As String = Nothing
                If Not Qstr Is Nothing Then
                    Qstr = Decrypt(Qstr)
                    Dim strKeyValue As String() = Qstr.Split("&")
                    Dim strValue As String()
                    Dim i As Integer
                    For i = 0 To strKeyValue.Length - 1
                        strValue = strKeyValue(i).Split("=")
                        'strValue(0)    Contains Key
                        'strValue(1)    Contains Value
                        If strValue(0).ToLower = Field.ToLower Then
                            If strValue(0) = "ItemId" Or strValue(0) = "ChangeKey" Then
                                retval = strKeyValue(i).ToString.Replace(strValue(0) & "=", "")
                            Else
                                retval = strValue(1)
                            End If

                            Exit For
                        End If
                    Next
                End If
                If Not retval Is Nothing Then
                    retval = retval.Replace("%20", " ")
                End If
                Return retval
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function Encrypt(ByVal inputText As String) As String
            Dim rijndaelCipher As RijndaelManaged = New RijndaelManaged
            Dim plainText As Byte() = Encoding.Unicode.GetBytes(inputText)
            Dim SecretKey As PasswordDeriveBytes = New PasswordDeriveBytes(ENCRYPTION_KEY, SALT)
            ' Using
            Dim encryptor As ICryptoTransform = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16))
            Try
                ' Using
                Dim memoryStream As MemoryStream = New MemoryStream
                Try
                    ' Using
                    Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
                    Try
                        cryptoStream.Write(plainText, 0, plainText.Length)
                        cryptoStream.FlushFinalBlock()
                        Return Convert.ToBase64String(memoryStream.ToArray)
                    Finally
                        CType(cryptoStream, IDisposable).Dispose()
                    End Try
                Finally
                    CType(memoryStream, IDisposable).Dispose()
                End Try
            Finally
                CType(encryptor, IDisposable).Dispose()
            End Try
        End Function


        Public Function Decrypt(ByVal inputText As String) As String
            inputText = inputText.Replace(" ", "+")
            Dim rijndaelCipher As RijndaelManaged = New RijndaelManaged
            Dim encryptedData As Byte() = Convert.FromBase64String(inputText)
            Dim secretKey As PasswordDeriveBytes = New PasswordDeriveBytes(ENCRYPTION_KEY, SALT)
            ' Using
            Dim decryptor As ICryptoTransform = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
            Try
                ' Using
                Dim memoryStream As MemoryStream = New MemoryStream(encryptedData)
                Try
                    ' Using
                    Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)
                    Try
                        Dim plainText(encryptedData.Length) As Byte
                        Dim decryptedCount As Integer = cryptoStream.Read(plainText, 0, plainText.Length)
                        Return Encoding.Unicode.GetString(plainText, 0, decryptedCount)
                    Finally
                        CType(cryptoStream, IDisposable).Dispose()
                    End Try
                Finally
                    CType(memoryStream, IDisposable).Dispose()
                End Try
            Finally
                CType(decryptor, IDisposable).Dispose()
            End Try
        End Function

    End Class
End Namespace