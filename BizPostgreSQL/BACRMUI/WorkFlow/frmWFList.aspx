﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmWFList.aspx.vb" Inherits="BACRM.UserInterface.Workflows.frmWFList" %>

<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Workflow & Business Process Automation (BPA)</title>
    <script type="text/javascript">
        function NewWorkFlow() {
            window.location.href = "../WorkFlow/frmWFManage.aspx";
            return false;
        }
        function OpenHistory() {
            window.location.href = "../WorkFlow/frmWFHistory.aspx";
            return false;
        }
        function OpenSMTPPopUp(a, b) {
            window.open('../admin/frmSMTPPopup.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&UserId=' + a + '&Mode=' + b, '', 'toolbar=no,titlebar=no,left=200,top=250,width=900,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected WorkFlow?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function pageLoaded() {
            $('#btnNewWF').click(function (e) {
                var win = window.open('../WorkFlow/frmWFManage.aspx', 'New WorkFlow Rule', 'toolbar=no,titlebar=no,left=50,top=50,width=' + (window.screen.width - 600) + ',height=' + (window.screen.height - 600) + ',scrollbars=yes,resizable=no');

                win.focus();
                e.eventPreventDefault();
            });
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });
        function EditWorkFlow(a) {
            var win = window.open('../WorkFlow/frmWFManage.aspx?WFID=' + a, 'New WorkFlow Rule', 'toolbar=no,titlebar=no,left=50,top=50,width=' + (window.screen.width - 600) + ',height=' + (window.screen.height - 600) + ',scrollbars=yes,resizable=yes');

            win.focus();
            return false;
        }

           function HideEmailDescPopup(divId) {           
            document.getElementById(divId).style.display = "none";
        }

    </script>
    <style type="text/css">
        .tempcls {
            display: none;
        }

         .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            bottom: 0px;
            width: 100%;
            height: 100%;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        .overlayContentPopUp {
            z-index: 99;
            margin: 0 auto;
            position: relative;
            top: 25%;
            top: 50%;
            left: 50%;
            margin: -50px 0 0 -100px;
        }

          .overlayComMessage {
            z-index: 99;
            margin: 0 auto;
            position: relative;
            top: 50%;
            left: 50%;
            margin: -277px 0 0 -340px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-7">
            <div class="form-inline">
                <div class="form-group">
                    <label>
                        Module
                    </label>
                    <asp:DropDownList AutoPostBack="true" Width="240" ID="ddlForm" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
                <div class="form-group">
                    <label>
                        Name or Description
                    </label>
                    <div class="input-group">
                        <asp:TextBox ID="txtWFName" runat="server" CssClass="form-control" Width="180" MaxLength="25"></asp:TextBox>
                        <span class="input-group-btn">
                            <asp:Button runat="server" CssClass="btn btn-primary btn-flat" Text="Go" ID="btnSearch" />
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5 text-right">
            <asp:Button ID="btnNewWF" runat="server" Text="New WorkFlow" CssClass="btn btn-primary" UseSubmitBehavior="true" />
            <asp:Button ID="btnRefresh" runat="server" Text="ReFresh" CssClass="btn btn-primary" UseSubmitBehavior="true" Visible="false" />
            <asp:Button ID="btnOPenHistory" runat="server" Text="Automation Logs" CssClass="btn btn-primary" UseSubmitBehavior="true" OnClientClick="return OpenHistory()" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Workflow & Business Process Automation (BPA)&nbsp;<a href="#" onclick="return OpenHelpPopUp('WorkFlow/frmWFList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:Button ID="btnRunService" Text="Run Service" runat="server" Visible="true" CssClass="tempcls" />
    <div class="table-responsive">
        <asp:GridView ID="gvWFList" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="false" Width="100%" DataKeyNames="numWFID" EnableViewState="true" ShowHeaderWhenEmpty="true">
            <Columns>
                 <asp:BoundField DataField="RowNo" ItemStyle-Width="3%" ></asp:BoundField>
                <asp:BoundField DataField="vcWFName" HeaderText="Rule Name" ItemStyle-Width="12%"></asp:BoundField>
                <asp:BoundField DataField="vcFormName" HeaderText="Module" ItemStyle-Width="7%"></asp:BoundField>
                <asp:BoundField DataField="TriggeredOn" HeaderText="Event" ItemStyle-Width="8%"></asp:BoundField>
                <asp:BoundField DataField="vcWFDescription" HeaderText="Description" ItemStyle-Width="10%"></asp:BoundField>
                <asp:BoundField DataField="vcWFAction" Visible="false" HeaderText="Actions & Conditions"></asp:BoundField>

                <asp:TemplateField HeaderText="Conditions" ItemStyle-Width="27%">
                        <ItemTemplate>
                            <asp:Repeater ID="rprConditions" runat="server" >                               
                                 <ItemTemplate>
                                <table style="width:100%;">
                                    <tr style="background-color: #ffffff; height: 25px;">
                                        <td style="border: 1px solid #A1A1A1; padding-left: 10px !important; padding-right: 10px !important; white-space: nowrap">
                                            <asp:Label ID="lblDislayNumber" runat="server" Text='<%#Eval("RowNumber") %>' Visible="false"></asp:Label>
                                            <%#Container.ItemIndex+1%>
                                        </td>
                                        <td  style="width:100%; border: 1px solid #A1A1A1; padding-left: 5px;">
                                            <asp:Label ID="lblActionText" runat="server" Text='<%#Eval("vcMessge") %>' ForeColor="maroon"></asp:Label>
                                        </td>                                       
                                    </tr>
                                </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Actions" ItemStyle-Width="25%">
                    <ItemTemplate>
                        <asp:Repeater ID="rprActions" runat="server" OnItemDataBound="rprActions_ItemDataBound">                           
                             <ItemTemplate>
                                <table style="width:100%;">
                                <tr style="background-color: #ffffff; height: 30px;">
                                    <td style="border: 1px solid #A1A1A1; padding-left: 10px !important; padding-right: 10px !important; white-space: nowrap">
                                        <asp:Label ID="lblDislayNumber" runat="server" Text='<%#Eval("RowNumber") %>' Visible="false"></asp:Label>
                                        <%#Container.ItemIndex+1%>
                                    </td>
                                    <td style="width:100%; border: 1px solid #A1A1A1; padding-left: 5px;">
                                        <asp:Label ID="lblActionText" runat="server" Text='<%#Eval("vcMessge") %>' ForeColor="navy"></asp:Label>
                                        <asp:ImageButton ID="imgOpenTemplate"  runat="server" ImageUrl="~/images/Document-48.gif" Height="15px" Width="15px" OnClick="imgOpenTemplate_Click" />
                                        <asp:Label ID="lblEmailTemplateId" runat="server" Text='<%#Eval("numGenericDocID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblBizDocId" runat="server" Text='<%#Eval("numBizDocTemplateID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblAction" runat="server" Text='<%#Eval("vcAction") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblDocName" runat="server" Text='<%#Eval("vcDocName") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblTemplateName" runat="server" Text='<%#Eval("TemplateName") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblUpdateFields" runat="server" Text='<%#Eval("vcUpdateFields") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblAltertMessage" runat="server" Text='<%#Eval("vcAlertMessage") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblReceiverName" runat="server" Text='<%#Eval("vcReceiverName")%>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblAssignTo" runat="server" Text='<%#Eval("vcAssignTo")%>' Visible="false"></asp:Label>
                                    </td>                                   
                                </tr>
                             </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Status" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" Text='<%# Eval("Status")%>' Visible="false" runat="server"></asp:Label>

                        <asp:ImageButton ID="imImageBtn" runat="server" Width="16px" Height="16px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="5%">
                    <ItemTemplate>
                        <div style="white-space: nowrap;">
                            <asp:LinkButton ID="hplEdit" runat="server" CssClass="btn btn-xs btn-info"><i class="fa fa-pencil"></i></asp:LinkButton>
                            <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-xs btn-danger" OnClientClick="javascript:return DeleteRecord();" CommandName="DeleteWF"><i class="fa fa-trash"></i></asp:LinkButton>
                            <asp:LinkButton ID="lnkdelete" runat="server" Visible="false"><font color="#730000">*</asp:LinkButton>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <div class="overlay" id="divShowEmailDesc" runat="server" style="display: none">
    <div style="border-color: #8A8A8A; border-width: 2px; border-style: solid;  height: 320px; width: 520px; background-color: aliceblue; " class="overlayComMessage">
        <table border="0" width="100%" class="dialog-body">
            <tr>
                <td  style="float:right; " >
                    <asp:Button ID="btnCloseEmailDesc" OnClientClick="javascript: HideEmailDescPopup('divShowEmailDesc'); return false;" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Close" />                           
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y:scroll;  height: 270px; width: 500px;" >
                    <table>
                        <tr>
                            <td style="vertical-align:top; width:10%; text-align:right;">
                                <asp:Label ID="lblEmailSubject" Font-Bold="true" Text="Subject: " runat="server"></asp:Label>
                            </td>
                            <td style="width:90%">
                                <asp:Label ID="lblSubject" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top; text-align:right;">
                                <asp:Label ID="lblEmailDesc" Font-Bold="true"  Text="Description: " runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Literal ID="litDesc" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </div>
                </td>                       
            </tr>                  
        </table>
    </div>
</div>
    </div>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" CssClass="button" runat="server" Style="display: none" Text="Go"></asp:Button>

    

</asp:Content>
