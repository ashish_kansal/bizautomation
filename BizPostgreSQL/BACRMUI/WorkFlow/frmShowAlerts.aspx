﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShowAlerts.aspx.vb" Inherits=".frmShowAlerts" MasterPageFile="~/common/Popup.Master" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }

        $(document).ready(function () {
            var chkBox = $("input[id$='chkAll']");
            chkBox.click(
          function () {
              $("INPUT[type='checkbox']").prop('checked', chkBox.is(':checked'));
          });
            // To deselect CheckAll when a GridView CheckBox        // is unchecked
            $("INPUT[type='checkbox']").click(
        function (e) {
            if (!$(this)[0].checked) {
                chkBox.prop("checked", false);
            }
        });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table cellspacing="0" cellpadding="0" width="500px">
        <tr>

            <td align="right" class="text">


                <asp:Button ID="btnDismiss" runat="server" CssClass="button" Text="Dismiss" />&nbsp;
                <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close()"
                    Text="Close" />
            </td>
        </tr>
    </table>
    <asp:Table ID="Table9" Width="100%" runat="server" Height="350" GridLines="None"
        BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:GridView ID="gvAlerts" runat="server" AutoGenerateColumns="False" CssClass="dg"
                    Width="100%" DataKeyNames="numWFAlertID">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:BoundField DataField="Number" HeaderText="No."></asp:BoundField>
                        <asp:BoundField HeaderText="Message" DataField="vcAlertMessage"></asp:BoundField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No Alert found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="lblTitle" runat="server">Alerts</asp:Label>
</asp:Content>
