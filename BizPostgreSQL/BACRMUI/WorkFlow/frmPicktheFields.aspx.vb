﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Projects
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Web.UI.WebControls
Imports System.Text
Namespace BACRM.UserInterface.Workflows
    Public Class frmPicktheFields
        Inherits  BACRMPage
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not IsPostBack Then
            
                GetWFFormFieldList(CCommon.ToLong(Session("numFormID")))
              
            End If
        End Sub
        Sub GetWFFormFieldList(ByVal numFormID As Long)
            Try
                Dim objWF As New BACRM.BusinessLogic.Workflow.Workflow
                objWF.DomainID = Session("DomainID")
                objWF.FormID = numFormID

                Dim dtFieldsList As DataTable
                dtFieldsList = objWF.GetWorkFlowFormFieldMaster()
                radFields.DataSource = dtFieldsList
                radFields.DataTextField = "vcFieldName"
                radFields.DataValueField = "ID"
                radFields.DataBind()


              


            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'btnSelect_OnClick
        Protected Sub btnSelect_OnClick(sender As Object, e As EventArgs)
            Try
                Dim sbValue As New StringBuilder()
                Dim sbValueText As New StringBuilder()
                Dim collection As IList(Of RadListBoxItem) = radFields.CheckedItems
                For Each item As RadListBoxItem In collection
                    sbValueText.Append(item.Text & ",")
                Next
                Session("PickedFields") = CCommon.ToString(sbValueText)
               
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace