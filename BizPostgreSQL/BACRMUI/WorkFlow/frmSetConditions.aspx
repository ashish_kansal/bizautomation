﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSetConditions.aspx.vb" Inherits="BACRM.UserInterface.Workflows.frmSetConditions" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Set Conditions</title>
    <style type="text/css">
        .formLayout {
            background-color: #e8edf1;
            /*border: solid 1px #a1a1a1;*/
            padding: 10px;
            width: 414px;
            float: left;
            height: 150px;
        }

            .formLayout label, .formLayout input {
                display: block;
                width: 170px;
                float: left;
                margin-bottom: 10px;
            }

            .formLayout label, .formLayout textarea {
                display: block;
                width: 166px;
                float: left;
                margin-bottom: 10px;
            }

            .formLayout label, .formLayout select {
                display: block;
                width: 170px;
                float: left;
                margin-bottom: 10px;
            }

        input[type='radio'] {
            display: block;
            width: 15px;
            float: left;
            margin-bottom: 10px;
        }

       

        .formLayout label {
            text-align: right;
            padding-right: 20px;
        }

        .dvBoxHeader {
            background-color: #d3e3fc;
            border: solid 1px #a1a1a1;
            padding: 10px;
            width: 660px;
            float: left;
            height: 30px;
        }

        .dvBox {
            background-color: #ffffff;
            border: solid 1px #a1a1a1;
            width: 680px;
            float: left;
            height: 500px;
        }

        .dvBoxInner {
            background-color: #ffffff;
            border: solid 1px #a1a1a1;
            padding-top: 10px;
            width: 668px;
            float: left;
            height: 100px;
            padding-left: 10px;
        }

        .dvBoxInnerChild {
            background-color: #ffffff;
            padding-top: 10px;
            width: 680px;
            float: left;
        }

        .dvRptrChild {
            background-color: #ffffff;
            /*padding-top: 10px;*/
            width: 680px;
            float: left;
            height: 25px;
            border: 1px solid #A1A1A1;
        }

        .dvLine {
            border: solid 1px #a1a1a1;
            width: 680px;
        }

        br {
            clear: left;
        }
    </style>
    <script type="text/javascript">
        function Close() {
            window.close()
            return false;
        }

    </script>

    <script src="../reports/jqTree/underscore.js" type="text/javascript"></script>
    <script src="../reports/jqTree/ui.dropdownchecklist.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/dateFormat.js" type="text/javascript"></script>
    <script src="js/WorkFlow.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Set Conditions
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <div style="background-color: #fff">
        <div style="width: 680px; float: left;height:555px">

            <asp:ScriptManager ID="smListBox" runat="server"></asp:ScriptManager>
            <div class="dvBoxHeader">
                <span style="font-weight: 500; font-size: x-large; float: left">Set Conditions:</span>
                <div style="float: right;">
                    <asp:Button ID="btnCloseConditions" runat="server" Text="Save & Close" BackColor="#4689e2" BorderColor="Black" Font-Bold="true" BorderStyle="Double" OnClientClick="Close();" />
                </div>
            </div>
            <div class="dvBox">
                <div class="dvBoxInner">
                    <div style="width: 40px; float: left">
                        <asp:Label ID="lblActionNo" runat="server" Text="1" Visible="false"></asp:Label>
                        When
                    </div>
                    <div style="width: 60px; float: left;display:none" id="dvANDOR" runat="server">
                        <asp:DropDownList ID="ddlANDORCondition" runat="server" Width="50px"></asp:DropDownList>
                    </div>
                    <div style="width: 140px; float: left">
                        <asp:DropDownList ID="ddlCondition" runat="server" Width="130px" AutoPostBack="true" OnSelectedIndexChanged="ddlCondition_OnSelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div style="width: 120px; float: left;" id="dvUpdateFields" runat="server">
                        <asp:DropDownList ID="ddlSign" runat="server" Width="110px"></asp:DropDownList>
                    </div>
                    <div style="width: 220px; float: left;" id="Div1" runat="server">
                        <telerik:RadListBox ID="radValue" runat="server" CheckBoxes="true" showcheckall="true" Width="210px"
                            Height="60px">
                        </telerik:RadListBox>
                        <asp:TextBox ID="txtValue" runat="server" Width="160px" Visible="false" autocomplete="off"></asp:TextBox>
                       <BizCalendar:Calendar ID="calDate" runat="server" Visible="false" />
                        <asp:TextBox ID="txtValueMoney" runat="server" Width="160px" Visible="false" autocomplete="off" ></asp:TextBox>
                        <asp:TextBox ID="txtNvalue" runat="server" Width="160px" Visible="false" autocomplete="off"></asp:TextBox>
                    </div>
                    <div style="width: 50px; float: left; text-align: right;">
                        <asp:Button ID="btnAddAction" runat="server" Text="+" BackColor="BlueViolet" Width="50px" BorderColor="Black" Font-Bold="true" BorderStyle="Double"  OnClick="ButtonAdd_Click"/>
                    </div>





                </div>
                <asp:HiddenField ID="hfWFCodeForEdit" runat="server" Value="0" />
                <asp:Repeater ID="rprConditions" runat="server" OnItemCommand="rprConditions_ItemCommand">
                    <ItemTemplate>
                        <div class="dvRptrChild" id="dvAssignItems" runat="server">
                            <div style="width: 14px; height: 22px; float: left; border-right: 1px solid #A1A1A1; padding-top: 3px">
                                <asp:Label ID="lblDislayNumber" runat="server" Text='<%#Eval("RowNumber") %>' Visible="false"></asp:Label>
                                <%#Container.ItemIndex+1%>
                            </div>
                            <div style="width: 664px; float: left; padding-top: 3px">
                                <asp:Label ID="lblActionText" runat="server" Text='<%#Eval("vcMessge") %>' ForeColor="maroon"></asp:Label>
                                <%--<asp:Label ID="lblAction" runat="server" Text='<%#Eval("vcAction") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblDocName" runat="server" Text='<%#Eval("vcDocName") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblTemplateName" runat="server" Text='<%#Eval("TemplateName") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblUpdateFields" runat="server" Text='<%#Eval("vcUpdateFields") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblAltertMessage" runat="server" Text='<%#Eval("vcAlertMessage") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblReceiverName" runat="server" Text='<%#Eval("vcReceiverName")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lblAssignTo" runat="server" Text='<%#Eval("vcAssignTo")%>' Visible="false"></asp:Label>--%>
                                <div class="tdRepeaterNormalR" style="width: 100px; float: right">
                                    <asp:ImageButton runat="server" ID="ImageButton1" CommandArgument='<%# Eval("RowNumber")%>'
                                        ToolTip="Click Here to Edit Record." CommandName="Edit" AlternateText="Edit"
                                        ImageUrl="~/images/edit.gif"></asp:ImageButton>
                                    &nbsp; <span class="vAlignMiddle">| </span>&nbsp;
                                                                    <asp:ImageButton runat="server" ID="ImageButton2" CommandArgument='<%# Eval("RowNumber") %>'
                                                                        ToolTip="Click Here to Delete Record." CommandName="Delete" AlternateText="Delete"
                                                                        ImageUrl="~/images/delete2.gif" OnClientClick="return confirm(&quot;Are you sure you want to delete?&quot;)"></asp:ImageButton>
                                </div>
                            </div>

                        </div>
                    </ItemTemplate>

                </asp:Repeater>
            </div>

        </div>



        <asp:HiddenField ID="hfDateFormat" runat="server" />
        <asp:HiddenField ID="hfWFID" runat="server" />
        <asp:HiddenField ID="hfValidDateFormat" runat="server" />
    </div>
</asp:Content>
