﻿
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Admin
Imports System.Drawing
Imports BACRM.BusinessLogic.Opportunities

Namespace BACRM.UserInterface.Workflows
    Public Class frmWFHistory
        Inherits BACRMPage

        Dim m_aryRightsForOrders() As Integer
        Dim objWF As New Workflow

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                If Not IsPostBack Then
                    LoadDropDowns()

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))

                        txtWFName.Text = CCommon.ToString(PersistTable(txtWFName.ID))

                        If ddlForm.Items.FindByValue(CCommon.ToString(PersistTable(ddlForm.ID))) IsNot Nothing Then
                            ddlForm.ClearSelection()
                            ddlForm.Items.FindByValue(CCommon.ToString(PersistTable(ddlForm.ID))).Selected = True
                        End If
                    End If

                    If (CCommon.ToInteger(txtCurrrentPage.Text) = 0) Then
                        txtCurrrentPage.Text = "1"
                    End If

                    BindDataGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub LoadDropDowns()
            Try
                Dim objConfigWizard As New FormConfigWizard             'Create an object of form config wizard
                objConfigWizard.DomainID = Session("DomainID")          'Set value of domain id
                objConfigWizard.tintType = 3

                Dim dtFormsList As DataTable
                dtFormsList = objConfigWizard.getFormList()

                ddlForm.DataSource = dtFormsList
                ddlForm.DataTextField = "vcFormName"
                ddlForm.DataValueField = "numFormId"
                ddlForm.DataBind()

                ddlForm.Items.Insert(0, New ListItem("--All--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindDataGrid()
            Try
                Dim dtTable As DataSet
                With objWF
                    .DomainID = Session("DomainId")
                    .SortCharacter = txtSortChar.Text.Trim()

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()

                    objWF.PageSize = Session("PagingRows")
                    objWF.TotalHistoryRecords = 0

                    If txtSortColumn.Text <> "" Then
                        .columnName = txtSortColumn.Text.Trim()
                    Else : .columnName = "dtCreatedDate"
                    End If

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                        txtSortOrder.Text = "A"
                    Else
                        .columnSortOrder = "Asc"
                        txtSortOrder.Text = "D"
                    End If

                    objWF.FormID = ddlForm.SelectedValue
                    objWF.WFName = txtWFName.Text
                End With

                dtTable = objWF.ShowHistory()

                PersistTable.Clear()
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtTable.Tables(0).Rows.Count > 0, txtCurrrentPage.Text, "1"))
                If txtSortOrder.Text = "A" Then
                    PersistTable.Add(PersistKey.SortOrder, "D")
                End If
                If txtSortOrder.Text = "D" Then
                    PersistTable.Add(PersistKey.SortOrder, "A")
                End If



                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())

                PersistTable.Add(txtWFName.ID, txtWFName.Text.Trim())
                PersistTable.Add(ddlForm.ID, ddlForm.SelectedValue)

                PersistTable.Save()

                bizPager.PageSize = Session("PagingRows")

                bizPager.RecordCount = objWF.TotalHistoryRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                gvHistory.DataSource = dtTable
                gvHistory.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                'txtCurrrentPage.Text = 1
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("domainid"), Session("usercontactid"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlform_selectedindexchanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlForm.SelectedIndexChanged
            Try
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("domainid"), Session("usercontactid"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#Region "History"

        Protected Sub gvHistory_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvHistory.RowDataBound
            If e.Row.DataItem IsNot Nothing Then
                Dim lblItemOrderNo As Label = DirectCast(e.Row.FindControl("lblItemOrderNo"), Label)
                Dim llb As Label = DirectCast(e.Row.FindControl("llb"), Label)
                Dim hplItemOrderNo As HyperLink = DirectCast(e.Row.FindControl("hplItemOrderNo"), HyperLink)
                m_aryRightsForOrders = GetUserRightsForPage(10, 3)
                If m_aryRightsForOrders(RIGHTSTYPE.VIEW) = 0 Then
                    lblItemOrderNo.Visible = True
                    hplItemOrderNo.Visible = False
                Else
                    lblItemOrderNo.Visible = False
                    hplItemOrderNo.Visible = True
                    hplItemOrderNo.Attributes.Add("onclick", "return OpenOpp('" & llb.Text & "','" & llb.Text & "')")
                End If
                Dim lblStatus As Label = DirectCast(e.Row.FindControl("lblStatus"), Label)
                Dim lblStatusText As Label = DirectCast(e.Row.FindControl("lblStatusText"), Label)
                Dim iStatus As Integer = CCommon.ToInteger(lblStatus.Text)
                If iStatus = 1 Then 'Pending Execution
                    lblStatusText.CssClass = "label label-warning"
                ElseIf iStatus = 2 Then 'in progress
                    lblStatusText.CssClass = "label label-primary"
                ElseIf iStatus = 3 Then 'Suceess
                    lblStatusText.CssClass = "label label-success"
                Else
                    lblStatusText.CssClass = "label label-danger"
                End If
            End If
        End Sub
#End Region
        Function ReturnDateTime(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) And IsDate(CloseDate) = True Then
                    strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                End If
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function getBizDocDetails(ByVal DomainId As Long, ByVal lngOppBizDocID As Long, ByVal lngOppId As Long, ByVal BizDocTemplateID As Long) As String
            Try
                Dim strBizDocUI As String = Nothing
                Dim dtOppBiDocDtl As DataTable
                Dim objOppBizDocs As New OppBizDocs
                'objOppBizDocs.OppBizDocId = lngOppBizDocID
                'objOppBizDocs.OppId = lngOppId
                'objOppBizDocs.DomainID = Session("DomainID")
                'objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.OppBizDocId = lngOppBizDocID
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.DomainID = DomainId
                objOppBizDocs.UserCntID = 1
                objOppBizDocs.ClientTimeZoneOffset = -330
                dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
                If dtOppBiDocDtl.Rows.Count = 0 Then Exit Function

                Dim tblBizDocSumm As New HtmlTable
                Dim lblAmountPaid As String
                Dim lblPending As String
                Dim lblApproved As String
                Dim lblDeclined As String
                Dim hplAmountPaid As String
                Dim txtComments As String
                Dim hplTrackingNumbers As String

                Dim lblBizDocIDValue As String
                Dim lblcreated As String
                Dim lblModifiedby As String
                Dim lblDate As String
                Dim lblOrganizationName As String
                Dim lblOrganizationContactName As String
                Dim hdnDivID As String
                Dim lblBillingTerms As String
                Dim lblBillingTermsName As String
                Dim lblDiscount As String
                Dim lblStatus As String
                Dim lblComments As String
                Dim lblNo As String
                Dim hdShipAmt As String
                Dim hdLateCharge As String
                Dim hdDisc As String
                Dim hdSubTotal As String
                Dim hdGrandTotal As String
                Dim hdTaxAmt As String
                Dim hdCRVTaxAmt As String
                Dim hdnCreditAmount As String

                Dim hdnOppType As String
                Dim hdnBizDocId As String
                Dim lblBalance As String

                Dim hplBillto As New System.Web.UI.WebControls.HyperLink
                Dim hplShipTo As New System.Web.UI.WebControls.HyperLink
                Dim hplOppID As String
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monAmountPaid")) Then
                    'lblAmountPaid = CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows(0).Item("monAmountPaid"))
                    lblAmountPaid = dtOppBiDocDtl.Rows(0).Item("monAmountPaid")
                Else : lblAmountPaid = "0.00"
                End If
                'If dtOppBiDocDtl.Rows(0).Item("AppReq") = 1 Then
                '    pnlApprove.Visible = True
                'Else : pnlApprove.Visible = False
                'End If

                lblPending = dtOppBiDocDtl.Rows(0).Item("Pending")
                lblApproved = dtOppBiDocDtl.Rows(0).Item("Approved")
                lblDeclined = dtOppBiDocDtl.Rows(0).Item("Declined")
                'hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill','" & lngOppId & "'," & lngReturnID & ")")
                'hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship','" & lngOppId & "'," & lngReturnID & ")")

                If dtOppBiDocDtl.Rows(0).Item("tintDeferred") = 1 Then
                    hplAmountPaid = "Amount Paid: (Deferred)"
                End If
                Dim imgFooter As New System.Web.UI.WebControls.Image
                If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("BizdocFooter")) Then
                        imgFooter.Visible = True
                        imgFooter.ImageUrl = CCommon.GetDocumentPath(DomainId) & dtOppBiDocDtl.Rows(0).Item("BizdocFooter")
                    Else : imgFooter.Visible = False
                    End If
                    ' lblPONo.Text = "P.O"

                Else
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter")) Then
                        imgFooter.Visible = True
                        imgFooter.ImageUrl = CCommon.GetDocumentPath(DomainId) & dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter")
                    Else : imgFooter.Visible = False
                    End If
                    'lblPONo.Text = "Invoice"

                    '  hplAmountPaid.Attributes.Add("onclick", "#")
                End If
                Dim imgLogo As New System.Web.UI.WebControls.Image
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")) Then

                    imgLogo.Visible = True
                    imgLogo.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")
                Else : imgLogo.Visible = False
                End If



                txtComments = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcComments")), "", dtOppBiDocDtl.Rows(0).Item("vcComments"))
                hplTrackingNumbers = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTrackingNo"))
                'hplTrackingNumbers.NavigateUrl = IIf(CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTrackingURL")) = "", "javascript:void(0)", String.Format(CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTrackingURL")), CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTrackingNo"))))


                'If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocName")) AndAlso dtOppBiDocDtl.Rows(0).Item("vcBizDocName").ToString.Trim.Length > 0 Then
                '    lblBizDocIDValue.Text = dtOppBiDocDtl.Rows(0).Item("vcBizDocName")
                'Else



                lblBizDocIDValue = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocID")), "", dtOppBiDocDtl.Rows(0).Item("vcBizDocID"))
                'End If

                lblcreated = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numCreatedby")), "", dtOppBiDocDtl.Rows(0).Item("numCreatedby")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")) & "  &nbsp;" & IIf(CCommon.ToBool(dtOppBiDocDtl.Rows(0).Item("bitAutoCreated")), "WorkFlow", "")
                lblModifiedby = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numModifiedBy")), "", dtOppBiDocDtl.Rows(0).Item("numModifiedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtModifiedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtModifiedDate"))
                'lblviewwedby.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numViewedBy")), "", dtOppBiDocDtl.Rows(0).Item("numViewedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtViewedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtViewedDate"))
                lblDate = FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"), Session("DateFormat"))
                lblOrganizationName = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationName"))
                lblOrganizationContactName = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactName"))
                hdnDivID = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numDivisionID"))
                'SetWebControlAttributes()
                Dim strDate As Date
                Dim lblDuedate As String
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtFromDate")) Then
                    strDate = DateAdd(DateInterval.Day, CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("numBillingDaysName")), dtOppBiDocDtl.Rows(0).Item("dtFromDate"))
                    lblDuedate = FormattedDateFromDate(strDate, Session("DateFormat"))
                End If

                If dtOppBiDocDtl.Rows(0).Item("tintBillingTerms") = True Then
                    lblBillingTerms = "Net " & dtOppBiDocDtl.Rows(0).Item("numBillingDaysName") & " , " & IIf(dtOppBiDocDtl.Rows(0).Item("tintInterestType") = False, "-", "+") & dtOppBiDocDtl.Rows(0).Item("fltInterest") & " %"
                    lblBillingTermsName = dtOppBiDocDtl.Rows(0).Item("vcBillingTermsName") & " , " & IIf(dtOppBiDocDtl.Rows(0).Item("tintInterestType") = False, "-", "+") & dtOppBiDocDtl.Rows(0).Item("fltInterest") & " %"

                Else
                    lblBillingTerms = "-"
                    lblBillingTermsName = "-"

                End If


                Dim hplDueDate As New System.Web.UI.WebControls.HyperLink
                If Not strDate = Nothing Then
                    hplDueDate.Text = "Due Date"
                    ' hplDueDate.Attributes.Add("onclick", "return ChangeDate('" & lngOppBizDocID & "','" & strDate.ToString("yyyyMMdd") & "','" & lngOppId & "')")
                End If



                If dtOppBiDocDtl.Rows(0).Item("fltDiscount") > 0 Then
                    If dtOppBiDocDtl.Rows(0).Item("bitDiscountType") = False Then

                        lblDiscount = dtOppBiDocDtl.Rows(0).Item("fltDiscount") & " %"
                    Else
                        lblDiscount = dtOppBiDocDtl.Rows(0).Item("fltDiscount")
                    End If
                End If
                'If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("numShipDoc")) Then
                '    If Not ddlShipDoc.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numShipDoc")) Is Nothing Then
                '        ddlShipDoc.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numShipDoc")).Selected = True
                '    End If
                'End If
                'If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("numShipVia")) Then
                '    If Not ddlShipCompany.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numShipVia")) Is Nothing Then
                '        ddlShipCompany.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numShipVia")).Selected = True
                '    End If
                'End If
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("numBizDocStatus")) Then
                    'If Not ddlBizDocStatus.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocStatus")) Is Nothing Then
                    '    ddlBizDocStatus.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocStatus")).Selected = True
                    'End If
                End If
                lblStatus = dtOppBiDocDtl.Rows(0).Item("BizDocStatus")
                'lblComments.Text = "<pre class='normal1'>" & dtOppBiDocDtl.Rows(0).Item("vcComments") & "</pre>"

                lblComments = "<pre class=""WordWrap"">" & dtOppBiDocDtl.Rows(0).Item("vcComments") & "</pre>"
                lblNo = dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo")
                'txtTrackingURL.Text = dtOppBiDocDtl.Rows(0).Item("vcTrackingURL")
                'hdShipAmt = CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows(0).Item("monShipCost"))
                hdShipAmt = dtOppBiDocDtl.Rows(0).Item("monShipCost")
                'Show capture amount button
                'If dtOppBiDocDtl.Rows(0).Item("bitAuthoritativeBizDocs") = 1 Then btnCaptureAmount.Visible = IsCardAuthorized()

                Dim ds As New DataSet
                Dim dtOppBiDocItems As DataTable
                objOppBizDocs.DomainID = DomainId
                objOppBizDocs.OppBizDocId = lngOppBizDocID
                objOppBizDocs.OppId = lngOppId
                ds = objOppBizDocs.GetOppInItems
                dtOppBiDocItems = ds.Tables(0)

                Dim objCalculateDealAmount As New CalculateDealAmount
                objCalculateDealAmount.CalculateDealAmount(lngOppId, lngOppBizDocID, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), DomainId, dtOppBiDocItems, FromBizInvoice:=True)





                hdLateCharge = objCalculateDealAmount.TotalLateCharges

                'Commented by chintan, reason: line item discount is display purpose only. take aggregate discount from BizDoc edit ->Discount Field
                hdDisc = objCalculateDealAmount.TotalDiscount + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0)

                hdSubTotal = objCalculateDealAmount.TotalAmount + (objCalculateDealAmount.TotalDiscount + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0))

                hdGrandTotal = objCalculateDealAmount.GrandTotal - objCalculateDealAmount.CreditAmount

                hdTaxAmt = objCalculateDealAmount.TotalTaxAmount - objCalculateDealAmount.TotalCRVTaxAmount

                hdCRVTaxAmt = objCalculateDealAmount.TotalCRVTaxAmount

                hdnCreditAmount = IIf(objCalculateDealAmount.CreditAmount > 0, objCalculateDealAmount.CreditAmount * -1, 0)

                '''Add columns to datagrid
                Dim i As Integer
                Dim dgBizDocs As New System.Web.UI.WebControls.DataGrid
                dgBizDocs.AutoGenerateColumns = False
                dgBizDocs.Font.Size = 10
                dgBizDocs.BackColor = Color.White
                dgBizDocs.Width = New Unit("100%")

                dgBizDocs.GridLines = GridLines.Vertical
                dgBizDocs.HorizontalAlign = HorizontalAlign.Center
                dgBizDocs.ItemStyle.CssClass = "ItemStyle"
                dgBizDocs.HeaderStyle.CssClass = "ItemHeader"
                dgBizDocs.AlternatingItemStyle.CssClass = "AltItemStyle"
                dgBizDocs.AlternatingItemStyle.VerticalAlign = VerticalAlign.Middle
                dgBizDocs.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                dgBizDocs.ItemStyle.VerticalAlign = VerticalAlign.Middle

                ''<AlternatingItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="AltItemStyle"></AlternatingItemStyle>
                '                           <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="ItemStyle"></ItemStyle>
                '                           <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="ItemHeader"></HeaderStyle>

                ' ForeColor="" Font-Size="10px" Width="100%"
                '                            BorderStyle="None" BackColor="White" GridLines="Vertical" HorizontalAlign="Center"
                'AutoGenerateColumns = "False"
                Dim dtdgColumns As DataTable
                dtdgColumns = ds.Tables(2)
                Dim bColumn As BoundColumn
                For i = 0 To dtdgColumns.Rows.Count - 1
                    bColumn = New BoundColumn
                    bColumn.HeaderText = dtdgColumns.Rows(i).Item("vcFormFieldName")
                    bColumn.DataField = dtdgColumns.Rows(i).Item("vcDbColumnName")
                    If dtdgColumns.Rows(i).Item("vcDbColumnName") = "SerialLotNo" Then
                        bColumn.ItemStyle.CssClass = "WordWrapSerialNo"
                    End If
                    If dtdgColumns.Rows(i).Item("vcFieldDataType") = "M" Then bColumn.DataFormatString = CCommon.GetDataFormatString()
                    dgBizDocs.Columns.Add(bColumn)
                Next
                Dim dtItems As New DataTable
                dtItems = dtOppBiDocItems
                dgBizDocs.DataSource = dtItems
                dgBizDocs.DataBind()
                'BindItems()
                Dim objConfigWizard As New FormConfigWizard
                Dim objCommon As New CCommon
                'If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                '    ibtnFooter.Attributes.Add("onclick", "return Openfooter(1)")
                '    objConfigWizard.FormID = 7
                'Else
                '    objConfigWizard.FormID = 8
                '    ibtnFooter.Attributes.Add("onclick", "return Openfooter(2)")
                'End If




                hdnOppType = dtOppBiDocDtl.Rows(0).Item("tintOppType")
                hdnBizDocId = dtOppBiDocDtl.Rows(0).Item("numBizDocId")

                ' BindBizDocsTemplate()

                'If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")) Then
                '    If Not ddlBizDocTemplate.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")) Is Nothing Then
                '        ddlBizDocTemplate.ClearSelection()
                '        ddlBizDocTemplate.SelectedValue = dtOppBiDocDtl.Rows(0).Item("numBizDocTempID").ToString
                '    End If
                'End If

                objConfigWizard.DomainID = DomainId
                objConfigWizard.BizDocID = dtOppBiDocDtl.Rows(0).Item("numBizDocId")
                objConfigWizard.BizDocTemplateID = dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")

                objOppBizDocs.BizDocId = dtOppBiDocDtl.Rows(0).Item("numBizDocId")
                objOppBizDocs.DomainID = DomainId
                objOppBizDocs.BizDocTemplateID = BizDocTemplateID
                'hplBizDocAtch.Text = "Attachments(" & IIf(objOppBizDocs.GetBizDocAttachments.Rows.Count = 0, 1, objOppBizDocs.GetBizDocAttachments.Rows.Count) & ")"

                Dim dsNew As DataSet
                Dim dtTable As DataTable
                dsNew = objConfigWizard.GetFieldFormListForBizDocsSumm
                dtTable = dsNew.Tables(1)
                'commented by chintan , reason : bug id 392
                'If dtTable.Rows.Count = 0 Then dtTable = dsNew.Tables(0)
                Dim tblrow As HtmlTableRow
                Dim tblCell As HtmlTableCell

                Dim strLabelCellID As String

                For Each dr As DataRow In dtTable.Rows
                    tblrow = New HtmlTableRow
                    tblCell = New HtmlTableCell
                    tblCell.Attributes.Add("class", "normal1")
                    tblCell.InnerText = dr("vcFormFieldName") & ": "
                    tblrow.Cells.Add(tblCell)

                    tblCell = New HtmlTableCell
                    tblCell.Attributes.Add("class", "normal1")
                    If dr("vcDbColumnName") = "SubTotal" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdSubTotal
                    ElseIf dr("vcDbColumnName") = "ShippingAmount" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdShipAmt
                    ElseIf dr("vcDbColumnName") = "TotalSalesTax" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdTaxAmt
                    ElseIf dr("vcDbColumnName") = "TotalCRVTax" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdCRVTaxAmt
                    ElseIf dr("vcDbColumnName") = "LateCharge" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdLateCharge
                    ElseIf dr("vcDbColumnName") = "Discount" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdDisc
                    ElseIf dr("vcDbColumnName") = "CreditApplied" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdnCreditAmount
                    ElseIf dr("vcDbColumnName") = "GrandTotal" Then
                        tblCell.ID = "tdGradTotal"
                        strLabelCellID = tblCell.ID
                        'tblCell.InnerText = hdGrandTotal.Value
                    Else
                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                            Dim taxAmt As Decimal
                            taxAmt = IIf(IsDBNull(dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0")), 0, dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0"))
                            tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + CCommon.GetDecimalFormat(taxAmt)
                        End If
                    End If

                    tblrow.Cells.Add(tblCell)
                    tblBizDocSumm.Rows.Add(tblrow)
                Next
                If strLabelCellID <> "" Then
                    CType(tblBizDocSumm.FindControl(strLabelCellID), HtmlTableCell).InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdGrandTotal
                End If
                'sachinew
                Dim dtOppBizAddDtl As DataTable

                objOppBizDocs.OppBizDocId = lngOppBizDocID
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.DomainID = DomainId
                dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail
                Dim lblBillTo As String
                Dim lblShipTo As String
                hplOppID = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("OppName"))
                lblBillTo = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("BillAdd"))
                lblShipTo = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("ShipAdd"))

                Dim hdnBalance As String
                Dim lblAmountPaidCurrency As String
                Dim lblBalanceDueCurrency As String
                Dim lblBizDoc As String
                lblBalance = Convert.ToDecimal(hdGrandTotal - lblAmountPaid)
                hdnBalance = lblBalance
                lblAmountPaidCurrency = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString()
                lblBalanceDueCurrency = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString()
                '  btnEdit.Attributes.Add("onclick", "return OpenEdit(" & lngOppId.ToString & "," & lngOppBizDocID.ToString & "," & dtOppBiDocDtl.Rows(0).Item("tintOppType").ToString & ")")

                If dtOppBiDocDtl.Rows(0)("bitEnabled") = True And dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString().Length > 0 Then
                    'BizDoc UI modification 
                    Dim strCss As String = "<style>" & HttpUtility.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtCSS").ToString()) & "</style>"
                    ' Dim strCss As String = "<style></style>"
                    strBizDocUI = strCss & HttpUtility.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString())
                    If strBizDocUI.Length > 0 Then
                        strBizDocUI = strBizDocUI.Replace("#Logo#", CCommon.RenderControl(imgLogo))
                        'strBizDocUI = strBizDocUI.Replace("#Signature#", CCommon.RenderControl(imgSignature))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationComments#", dtOppBiDocDtl.Rows(0)("vcOrganizationComments").ToString())
                        lblBizDoc = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("BizDcocName"))
                        strBizDocUI = strBizDocUI.Replace("#FooterImage#", CCommon.RenderControl(imgFooter))
                        strBizDocUI = strBizDocUI.Replace("#BizDocType#", lblBizDoc)
                        'strBizDocUI = strBizDocUI.Replace("#Customer/VendorBill-toAddress#", lblBillTo.Text)
                        'strBizDocUI = strBizDocUI.Replace("#Customer/VendorShip-toAddress#", lblShipTo.Text)

                        Dim dsAddress As DataSet
                        objOppBizDocs.OppId = lngOppId
                        objOppBizDocs.DomainID = DomainId
                        objOppBizDocs.OppBizDocId = lngOppBizDocID
                        dsAddress = objOppBizDocs.GetOPPGetOppAddressDetails

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        'Customer/Vendor Information
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationName")))
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationPhone")))

                        If dsAddress.Tables(0).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", "")
                            strBizDocUI = strBizDocUI.Replace("#BillingContact#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcAddressName")))
                            strBizDocUI = strBizDocUI.Replace("#BillingContact#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcContact")))
                        End If

                        If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 4 AndAlso dsAddress.Tables(4).Rows.Count > 0 Then
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcAddressName")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", "")
                        End If

                        If dsAddress.Tables(1).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", "")
                            strBizDocUI = strBizDocUI.Replace("#ShippingContact#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcAddressName")))
                            strBizDocUI = strBizDocUI.Replace("#ShippingContact#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcContact")))
                        End If

                        If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 5 AndAlso dsAddress.Tables(5).Rows.Count > 0 Then
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcAddressName")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", "")
                        End If

                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                            If strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") + "#OppOrderChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") - "#OppOrderChangeBillToHeader(".Length)
                                hplBillto.Text = "Bill To" '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader(" & strLink & ")#", CCommon.RenderControl(hplBillto))
                            Else
                                hplBillto.Text = "Bill To"
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader#", CCommon.RenderControl(hplBillto))
                            End If

                            If strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") + "#OppOrderChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") - "#OppOrderChangeShipToHeader(".Length)
                                hplShipTo.Text = "Ship To"  '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader(" & strLink & ")#", CCommon.RenderControl(hplShipTo))
                            Else
                                hplShipTo.Text = "Ship To"
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader#", CCommon.RenderControl(hplShipTo))
                            End If
                        Else
                            If strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") + "#OppOrderChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") - "#OppOrderChangeBillToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader#", "Bill To")
                            End If

                            If strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") + "#OppOrderChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") - "#OppOrderChangeShipToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader#", "Ship To")
                            End If
                        End If

                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationComments#", dtOppBiDocDtl.Rows(0)("vcOrganizationComments").ToString())

                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactName#", lblOrganizationContactName)
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactEmail")))
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactPhone")))
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        'Employer Information
                        strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("EmployerOrganizationName")))
                        strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("EmployerOrganizationPhone")))

                        If dsAddress.Tables(2).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", dsAddress.Tables(2).Rows(0)("vcCompanyName"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", dsAddress.Tables(2).Rows(0)("vcFullAddress"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", dsAddress.Tables(2).Rows(0)("vcStreet"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", dsAddress.Tables(2).Rows(0)("vcCity"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", dsAddress.Tables(2).Rows(0)("vcPostalCode"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", dsAddress.Tables(2).Rows(0)("vcState"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", dsAddress.Tables(2).Rows(0)("vcCountry"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", dsAddress.Tables(2).Rows(0)("vcAddressName"))
                        End If

                        If dsAddress.Tables(3).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", dsAddress.Tables(3).Rows(0)("vcCompanyName"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", dsAddress.Tables(3).Rows(0)("vcFullAddress"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", dsAddress.Tables(3).Rows(0)("vcStreet"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", dsAddress.Tables(3).Rows(0)("vcCity"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", dsAddress.Tables(3).Rows(0)("vcPostalCode"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", dsAddress.Tables(3).Rows(0)("vcState"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", dsAddress.Tables(3).Rows(0)("vcCountry"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", dsAddress.Tables(3).Rows(0)("vcAddressName"))
                        End If

                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 2 Then
                            If strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") + "#EmployerChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") - "#EmployerChangeBillToHeader(".Length)
                                hplBillto.Text = "Bill To" '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader(" & strLink & ")#", CCommon.RenderControl(hplBillto))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader#", CCommon.RenderControl(hplBillto))
                            End If

                            If strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") + "#EmployerChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") - "#EmployerChangeShipToHeader(".Length)
                                hplShipTo.Text = "Ship To" '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader(" & strLink & ")#", CCommon.RenderControl(hplShipTo))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader#", CCommon.RenderControl(hplShipTo))
                            End If
                        Else
                            If strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") + "#EmployerChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") - "#EmployerChangeBillToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader(" & strLink & ")#", strBizDocUI)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader#", "Bill To")
                            End If

                            If strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") + "#EmployerChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") - "#EmployerChangeShipToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader#", "Ship To")
                            End If
                        End If

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        strBizDocUI = strBizDocUI.Replace("#BizDocStatus#", lblStatus)
                        strBizDocUI = strBizDocUI.Replace("#Currency#", lblBalanceDueCurrency)
                        strBizDocUI = strBizDocUI.Replace("#AmountPaid#", lblAmountPaid)
                        strBizDocUI = strBizDocUI.Replace("#BalanceDue#", lblBalance) 'used by amt paid js
                        strBizDocUI = strBizDocUI.Replace("#Discount#", lblDiscount)
                        strBizDocUI = strBizDocUI.Replace("#BillingTerms#", lblBillingTerms)
                        If CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("dtFromDate")).Length > 3 Then
                            strBizDocUI = strBizDocUI.Replace("#BillingTermFromDate#", FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate"), Session("DateFormat")))
                        End If
                        strBizDocUI = strBizDocUI.Replace("#BillingTermsName#", lblBillingTermsName)

                        strBizDocUI = strBizDocUI.Replace("#DueDate#", lblDuedate)
                        strBizDocUI = strBizDocUI.Replace("#BizDocID#", lblBizDocIDValue)
                        strBizDocUI = strBizDocUI.Replace("#P.O.NO#", lblNo)
                        strBizDocUI = strBizDocUI.Replace("#OrderID#", hplOppID)
                        strBizDocUI = strBizDocUI.Replace("#Comments#", lblComments)
                        strBizDocUI = strBizDocUI.Replace("#Products#", CCommon.RenderControl(dgBizDocs))
                        strBizDocUI = strBizDocUI.Replace("#BizDocSummary#", CCommon.RenderControl(tblBizDocSumm))
                        strBizDocUI = strBizDocUI.Replace("#BizDocCreatedDate#", lblDate)

                        strBizDocUI = strBizDocUI.Replace("#AmountPaidPopUp#", hplAmountPaid)
                        strBizDocUI = strBizDocUI.Replace("#ChangeDueDate#", CCommon.RenderControl(hplDueDate))
                        strBizDocUI = strBizDocUI.Replace("#AssigneeName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeName")))
                        strBizDocUI = strBizDocUI.Replace("#AssigneeEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeEmail")))
                        strBizDocUI = strBizDocUI.Replace("#AssigneePhoneNo#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneePhone")))
                        strBizDocUI = strBizDocUI.Replace("#PartnerSource#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcPartner")))
                        strBizDocUI = strBizDocUI.Replace("#DropShip#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcDropShip")))
                        strBizDocUI = strBizDocUI.Replace("#ReleaseDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcReleaseDate")))
                        strBizDocUI = strBizDocUI.Replace("#RequiredDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcRequiredDate")))
                        strBizDocUI = strBizDocUI.Replace("#OrderRecOwner#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OnlyOrderRecOwner")))
                        strBizDocUI = strBizDocUI.Replace("#InventoryStatus#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcInventoryStatus")))
                        strBizDocUI = strBizDocUI.Replace("#ShippingCompany#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("ShipVia")))
                        strBizDocUI = strBizDocUI.Replace("#ShippingService#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcShippingService")))
                        strBizDocUI = strBizDocUI.Replace("#PackingSlipID#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcPackingSlip")))
                        strBizDocUI = strBizDocUI.Replace("#BizDocTemplateName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTemplateName")))
                        If dtOppBiDocItems IsNot Nothing AndAlso dtOppBiDocItems.Rows.Count > 0 Then
                            If dtOppBiDocItems.Select("numItemCode <>" & CCommon.ToDouble(Session("DiscountServiceItem")) & " AND numItemCode <> " & CCommon.ToDouble(Session("ShippingServiceItem"))).Length > 0 Then
                                strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", CCommon.ToDecimal(dtOppBiDocItems.Select("numItemCode <>" & CCommon.ToDouble(Session("DiscountServiceItem")) & " AND numItemCode <> " & CCommon.ToDouble(Session("ShippingServiceItem"))).CopyToDataTable().Compute("sum(numUnitHour)", ""))))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", 0))
                            End If
                        Else
                            strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", 0))
                        End If
                        strBizDocUI = strBizDocUI.Replace("#TrackingNo#", hplTrackingNumbers)
                        strBizDocUI = strBizDocUI.Replace("#CustomerPO##", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcCustomerPO#")))
                        strBizDocUI = strBizDocUI.Replace("#SOComments#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcSOComments")))
                        strBizDocUI = strBizDocUI.Replace("#ParcelShippingAccount#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcShippersAccountNo")))
                        strBizDocUI = strBizDocUI.Replace("#OrderCreatedDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrderCreatedDate")))
                        strBizDocUI = strBizDocUI.Replace("#VendorInvoice#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcVendorInvoice")))

                        Dim lNumericToWord As New NumericToWord

                        strBizDocUI = strBizDocUI.Replace("#GrandTotalinWords#", lNumericToWord.SpellNumber(Replace(hdGrandTotal, ",", "")))

                        If dtOppBiDocDtl.Columns.Contains("vcTotalQtybyUOM") Then
                            strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTotalQtybyUOM")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", "")
                        End If

                        'Replace custom field values
                        Dim objCustomFields As New CustomFields
                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                            objCustomFields.locId = 2
                        Else
                            objCustomFields.locId = 6
                        End If
                        objCustomFields.RelId = 0
                        objCustomFields.DomainID = DomainId
                        objCustomFields.RecordId = lngOppId
                        ds = objCustomFields.GetCustFlds
                        For Each drow As DataRow In ds.Tables(0).Rows
                            If CCommon.ToLong(drow("numListID")) > 0 Then
                                strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", objCommon.GetListItemValue(CCommon.ToString(("Value")), DomainId))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", drow("Value"))
                            End If
                        Next
                    End If
                Else
                    strBizDocUI = CCommon.RenderControl(tblBizDocSumm)
                End If
                Return strBizDocUI
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnTest_CLick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTest.Click
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = 113161
            objOppBizDocs.DomainID = 1
            Dim dsOppData As DataSet = objOppBizDocs.GetBizDocsDetails

            Dim htmlCodeToConvert As String = getBizDocDetails(1, 113197, 95057, 387)
            Dim i As Integer
            Dim strFileName As String = ""
            Dim strFilePhysicalLocation As String = ""
            Dim objHTMLToPDF As New HTMLToPDF
            strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(1))
            strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(1)) & strFileName
        End Sub
        Private Sub gvItemsBought_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs) Handles gvHistory.Sorting
            Try
                txtSortColumn.Text = e.SortExpression
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class


End Namespace