﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Workflow

Public Class frmShowAlerts
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindAlerts()

        End If
    End Sub
    Public Sub BindAlerts()

        Dim objWF As New WorkFlow
        objWF.DomainID = Session("DomainID")
        
        Dim dtResult As DataSet = objWF.GetAlertMessageData()
        If dtResult.Tables(0).Rows.Count > 0 Then
            gvAlerts.DataSource = dtResult
            gvAlerts.DataBind()
        Else
            gvAlerts.DataSource = Nothing
            gvAlerts.DataBind()
        End If

    End Sub
    Private Sub btnDismiss_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDismiss.Click
        Try
            Dim objWF As New Workflow

            For Each row As GridViewRow In gvAlerts.Rows
                If CType(row.FindControl("chkSelect"), CheckBox).Checked = True Then

                    Dim AlertID As Long = gvAlerts.DataKeys(row.RowIndex).Values("numWFAlertID")

                    If AlertID > 0 Then
                        objWF.numWFAlertID = AlertID
                        objWF.intAlertStatus = 2
                        objWF.UpdateWorkFlowAlertStatus()

                    End If
                End If
            Next
            BindAlerts()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class