﻿'Author:Sachin Sadhu'
'Date:14thFeb2014
'Purpose:Save WorkFlow details
'Future Enhancements: SMS alerts is under BETA release.need Domain wise Twilio registration

Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing
Imports System.Text
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Web.UI.WebControls
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Case
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Web.Services
Imports System.Web.Script.Services
Namespace BACRM.UserInterface.Workflows
    Public Class frmWFManage
        Inherits BACRMPage
        Dim lngWFID As Long
        Dim strCondition As String = Nothing
        Dim objAdmin As CAdmin
        Shared dtEmailTemplates As DataTable
        Shared lngSelectedEmailTemplate As Long = 0
#Region "Page Load"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngWFID = CCommon.ToLong(GetQueryStringVal("WFID"))
                'If CCommon.ToString(Session("PickedFields")) IsNot Nothing Then
                '    lblshowFields.Text = CCommon.ToString(Session("PickedFields"))
                'End If

                If Not IsPostBack Then
                    hfWFID.Value = lngWFID
                    BindModules()
                    SetInitialRow()
                    SetInitialRowUF()
                    SetInitialRowCondition()
                    BindActionDdl()

                    BindConditionsDdl()
                    BindBefore()

                    hfDateFormat.Value = CCommon.GetDateFormat()
                    hfValidDateFormat.Value = CCommon.GetValidationDateFormat()
                    GetWFAData(lngWFID)
                    'Dim script As String = "$(document).ready(function () { $('[id*=btnRefesh]').click(); });"
                    'ClientScript.RegisterStartupScript(Me.[GetType](), "load", script, True)
                    LoadMergeField()
                    radComposeMail.Content = Session("Signature")
                    radComposeMail.DisableFilter(Telerik.Web.UI.EditorFilters.ConvertFontToSpan)

                End If
                btnSave.Attributes.Add("onclick", "return Save()")
                btnRefesh.Attributes.Add("onclick", "return Save()")
                'txtNvalue.Attributes.Add("onkeypress", "CheckNumber2(3,event)")
                ' txtValueMoney.Attributes.Add("onkeypress", "CheckNumber2(3,event)")


                ' txtValue.Attributes.Add("onkeypress", "CheckNumber2(3,event)")
                btnAddAction.Attributes.Add("onclick", "return AddAction()")
                If (ddlUpdateFields.SelectedIndex > 0) Then
                    divUpdateFields.Style.Add("display", "block")
                Else
                    divUpdateFields.Style.Add("display", "none")

                End If

                'If (hdfPickedFields.Value = "0" Or lngWFID <> 0) Then
                '    dvPickfields.Style.Add("display", "block")
                'Else
                '    dvPickfields.Style.Add("display", "none")

                'End If
                dvPickfields.Style.Add("display", "none")
                dvComposeMessage.Style.Add("display", "none")
                divShowEmailDesc.Style.Add("display", "none")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
#End Region
#Region "Bind Dropdowns & Events"
        Sub BindModules()
            Try
                Dim objConfigWizard As New FormConfigWizard
                objConfigWizard.DomainID = Session("DomainID")
                objConfigWizard.tintType = 3

                Dim dtFormsList As DataTable
                dtFormsList = objConfigWizard.getFormList()
                'Commendted for PRD Update: 
                ddlForm.DataSource = dtFormsList

                ddlForm.DataTextField = "vcFormName"
                ddlForm.DataValueField = "numFormId"
                'Commendted for PRD Update: ddlForm.DataBind()

                Dim dv As DataView = dtFormsList.DefaultView

                'dv.RowFilter = " numFormId IN (68,70,71,94)"
                ddlUpdateFields.Items.Clear()
                ddlUpdateFields.DataSource = dv

                'Uncommnet for Production update
                ' ddlForm.DataSource = dv
                ddlForm.DataBind()
                ddlForm.Items.Insert(0, New ListItem("--Select--", "0"))

                'comment for Production update


            Catch ex As Exception
                Throw
            End Try
        End Sub
        Sub BindArAging()
            Try
                Dim newListItem As ListItem
                newListItem = New ListItem("-- Select # of days past due date --", "0")
                ddlArAging.Items.Clear()
                ddlArAging.Items.Add(newListItem)
                newListItem = New ListItem("1 day past-due", "1")
                ddlArAging.Items.Add(newListItem)
                newListItem = New ListItem("2 days past-due", "2")
                ddlArAging.Items.Add(newListItem)
                newListItem = New ListItem("5 days past-due", "3")
                ddlArAging.Items.Add(newListItem)
                newListItem = New ListItem("7 days past-due", "4")
                ddlArAging.Items.Add(newListItem)
                newListItem = New ListItem("14 days past-due", "5")
                ddlArAging.Items.Add(newListItem)
                newListItem = New ListItem("21 days past-due", "6")
                ddlArAging.Items.Add(newListItem)
                newListItem = New ListItem("30 days past-due", "7")
                ddlArAging.Items.Add(newListItem)

            Catch ex As Exception
                Throw
            End Try
        End Sub
        Sub BindBizDoc()
            Try
                objCommon.DomainID = Session("DomainID")
                'objCommon.BizDocType = ddlOppType.SelectedValue
                objCommon.BizDocType = ddlBizDocType.SelectedValue
                'ddlBizDocs.DataSource = objCommon.GetBizDocType
                ' ddlBizDocs.DataTextField = "vcData"
                ' ddlBizDocs.DataValueField = "numListItemID"
                ' ddlBizDocs.DataBind()
                ddlCreateBizDoc.Items.Clear()
                ddlCreateBizDoc.DataSource = objCommon.GetBizDocType
                ddlCreateBizDoc.DataTextField = "vcData"
                ddlCreateBizDoc.DataValueField = "numListItemID"
                ddlCreateBizDoc.DataBind()
                'Added on 23Aug||Saturday
                ddlBizTemplate.Items.Clear()
                ddlCreateBizDoc.Items.Insert(0, New ListItem("--Select--", 0))
                ddlBizTemplate.Items.Insert(0, New ListItem("--Select--", 0))
                'end of new
                If ddlCreateBizDoc.SelectedIndex > 0 Then
                    BindBizDocsTemplate()
                End If


            Catch ex As Exception
                Throw
            End Try
        End Sub
        Sub BindBizDocsTemplate()
            Try
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = Session("DomainID")
                objOppBizDoc.BizDocId = ddlCreateBizDoc.SelectedValue
                objOppBizDoc.OppType = ddlBizDocType.SelectedValue 'opptype
                objOppBizDoc.byteMode = 0

                Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()
                ddlBizTemplate.Items.Clear()
                ddlBizTemplate.DataSource = dtBizDocTemplate
                ddlBizTemplate.DataTextField = "vcTemplateName"
                ddlBizTemplate.DataValueField = "numBizDocTempID"
                ddlBizTemplate.DataBind()

                ddlBizTemplate.Items.Insert(0, New ListItem("--Select--", 0))
                objOppBizDoc.byteMode = 1

                Dim dtDefaultBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                If Not IsDBNull(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID")) Then
                    If Not ddlBizTemplate.Items.FindByValue(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID")) Is Nothing Then

                        ddlBizTemplate.ClearSelection()
                        ddlBizTemplate.SelectedValue = dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID").ToString
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub BindBizDocType()
            Try
                Dim arr As New ArrayList()
                arr.Add(New ListItem("Sales", "1"))
                arr.Add(New ListItem("Purchase", "2"))

                ddlBizDocType.Items.Clear()
                For Each item As ListItem In arr
                    ddlBizDocType.Items.Add(item)
                Next
                'If ddlBizDocType.SelectedIndex > 0 Then
                BindBizDoc()
                'End If


            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindActionItemTemplateList()
            Try
                Dim objActionItem As New ActionItem
                Dim dtTable As DataTable
                objActionItem.DomainID = Context.Session("DomainID")
                objActionItem.UserCntID = 0
                dtTable = objActionItem.LoadActionItemTemplateData()
                'If rprActions1.Items.Count > 0 Then
                '    For Each repeaterItem As RepeaterItem In rprActions1.Items
                '        Dim ddlActionItemTemplate As DropDownList = DirectCast(repeaterItem.FindControl("ddlActionItemTemplate"), DropDownList)
                'If ddlActionItemTemplate.Items.Count <= 0 Then
                ddlActionItemTemplate.DataSource = dtTable
                ddlActionItemTemplate.DataTextField = "TemplateName"
                ddlActionItemTemplate.DataValueField = "RowID"
                ddlActionItemTemplate.DataBind()
                'End If
                '    Next
                'End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Sub BindActionDdl()
            Try
                Dim arr As New ArrayList()

                arr.Add(New ListItem("--Select One--", "0"))
                arr.Add(New ListItem("Send Alerts", "1"))
                If CCommon.ToInteger(ddlForm.SelectedValue) = 124 Then 'Action Ticklers
                    'nothing
                ElseIf CCommon.ToInteger(ddlForm.SelectedValue) <> 138 Then 'A/R Aging
                    arr.Add(New ListItem("Assign Action Items", "2"))
                End If

                If ddlForm.SelectedValue = 138 Then  'A/R Aging
                    arr.Add(New ListItem("Update Fields", "3"))
                Else
                    arr.Add(New ListItem("Update Fields", "3"))
                    arr.Add(New ListItem("Display Alert Message", "4"))
                    arr.Add(New ListItem("Send SMS Alerts", "9"))
                End If

                If ddlForm.SelectedValue = 71 Then
                    arr.Add(New ListItem("Bizdoc Approval", "5"))
                ElseIf ddlForm.SelectedValue = 70 Then
                    arr.Add(New ListItem("Create BizDoc", "6"))
                ElseIf ddlForm.SelectedValue = 68 Then
                    arr.Add(New ListItem("Promote Organization", "7"))
                    arr.Add(New ListItem("Demote Organization", "8"))
                End If

                ddlAction.Items.Clear()
                For Each item As ListItem In arr
                    ddlAction.Items.Add(item)
                Next

            Catch ex As Exception
                Throw
            End Try
        End Sub
        Sub BindBefore()
            Try
                Dim arr As New ArrayList()
                arr.Add(New ListItem("Before", "1"))
                arr.Add(New ListItem("After", "2"))
                ddlBefore.Items.Clear()
                For Each item As ListItem In arr
                    ddlBefore.Items.Add(item)
                Next
                'If rprActions1.Items.Count > 0 Then
                '    For Each repeaterItem As RepeaterItem In rprActions1.Items
                '        Dim ddlAction As DropDownList = DirectCast(repeaterItem.FindControl("ddlAction"), DropDownList)
                '        If ddlAction.Items.Count <= 0 Then
                '            ddlAction.Items.Clear()
                '            For Each item As ListItem In arr
                '                ddlAction.Items.Add(item)
                '            Next
                '        End If


                '    Next
                'End If

            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindAssignTo()
            Try
                Dim arr As New ArrayList()
                arr.Add(New ListItem("Owner of trigger record", "1"))
                arr.Add(New ListItem("Assignee of trigger record", "2"))
                'If rprActions1.Items.Count > 0 Then
                '    For Each repeaterItem As RepeaterItem In rprActions1.Items
                '        Dim ddlAssignTo As DropDownList = DirectCast(repeaterItem.FindControl("ddlAssignTo"), DropDownList)
                '        If ddlAssignTo.Items.Count <= 0 Then
                '            ddlAssignTo.Items.Clear()
                '            For Each item As ListItem In arr
                '                ddlAssignTo.Items.Add(item)
                '            Next
                '        End If
                '    Next
                'End If
                ddlAssignTo.Items.Clear()
                For Each item As ListItem In arr
                    ddlAssignTo.Items.Add(item)
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Sub GetEmailTemplateList()
            Try
                Dim objCampaign As New Campaign
                Dim dtTable As DataTable

                objCampaign.DomainID = Session("DomainID")
                dtTable = objCampaign.GetUserEmailTemplates
                dtEmailTemplates = dtTable

                ddlEmailTemplate.DataSource = dtTable
                ddlEmailTemplate.DataTextField = "vcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()

                ' ddlEmailTemplate.Items.Insert(9999, New ListItem("Compose Message", "9999"))

                Dim lst As New ListItem("Compose Message", "99999")
                ddlEmailTemplate.Items.Insert(ddlEmailTemplate.Items.Count, lst)
                'If rprActions1.Items.Count > 0 Then
                '    For Each repeaterItem As RepeaterItem In rprActions1.Items
                '        Dim ddlEmailTemplate As DropDownList = DirectCast(repeaterItem.FindControl("ddlEmailTemplate"), DropDownList)
                '        If ddlEmailTemplate.Items.Count <= 0 Then
                '            ddlEmailTemplate.DataSource = dtTable
                '            ddlEmailTemplate.DataTextField = "vcDocName"
                '            ddlEmailTemplate.DataValueField = "numGenericDocID"

                '            ddlEmailTemplate.DataBind()
                '        End If
                '    Next
                'End If

                lnkEmailTemplate.Text = "<b>Preview: </b> " + ddlEmailTemplate.SelectedItem.Text
                lngSelectedEmailTemplate = ddlEmailTemplate.SelectedValue

            Catch ex As Exception
                Throw
            End Try

        End Sub
        Sub BindSendTo()
            Try

                Dim dt As New DataTable()
                Dim dr As DataRow = Nothing
                CCommon.AddColumnsToDataTable(dt, "vcEmailToType,vcReceiverName")

                If ddlProcessType.SelectedValue = "3" Then 'Projects

                    dr = dt.NewRow()
                    dr("vcEmailToType") = 2
                    dr("vcReceiverName") = "Assignee of trigger record(Stage)"
                    dt.Rows.Add(dr)

                    dr = dt.NewRow()
                    dr("vcEmailToType") = 7
                    dr("vcReceiverName") = "Assignee of trigger record(Project)"
                    dt.Rows.Add(dr)

                    dr = dt.NewRow()
                    dr("vcEmailToType") = 4
                    dr("vcReceiverName") = "Internal Project Manager"
                    dt.Rows.Add(dr)

                    dr = dt.NewRow()
                    dr("vcEmailToType") = 5
                    dr("vcReceiverName") = "External Project Manager"
                    dt.Rows.Add(dr)

                    dr = dt.NewRow()
                    dr("vcEmailToType") = 6
                    dr("vcReceiverName") = "Assignee of Next Stage"
                    dt.Rows.Add(dr)
                Else

                    If ddlForm.SelectedValue = "73" Then 'Projects
                        dr = dt.NewRow()
                        dr("vcEmailToType") = 3
                        dr("vcReceiverName") = "Primary contact of trigger record"
                        dt.Rows.Add(dr)
                        dr = dt.NewRow()
                        dr("vcEmailToType") = 7
                        dr("vcReceiverName") = "Assignee of trigger record"
                        dt.Rows.Add(dr)

                        dr = dt.NewRow()
                        dr("vcEmailToType") = 4
                        dr("vcReceiverName") = "Internal Project Manager"
                        dt.Rows.Add(dr)

                        dr = dt.NewRow()
                        dr("vcEmailToType") = 5
                        dr("vcReceiverName") = "External Project Manager"
                        dt.Rows.Add(dr)

                    Else
                        dr = dt.NewRow()
                        dr("vcEmailToType") = 1
                        dr("vcReceiverName") = "Owner of trigger record"
                        dt.Rows.Add(dr)

                        dr = dt.NewRow()
                        dr("vcEmailToType") = 2
                        dr("vcReceiverName") = "Assignee of trigger record"
                        dt.Rows.Add(dr)

                        dr = dt.NewRow()
                        dr("vcEmailToType") = 3
                        dr("vcReceiverName") = "Primary contact of trigger record"
                        dt.Rows.Add(dr)

                    End If

                    If CCommon.ToInteger(ddlAction.SelectedValue) = 1 Then 'Send Email alerts
                        dr = dt.NewRow()
                        dr("vcEmailToType") = 8
                        dr("vcReceiverName") = ""
                        dt.Rows.Add(dr)
                    End If
                End If

                radlstEmailTo.Items.Clear()
                radlstEmailTo.DataSource = dt
                radlstEmailTo.DataValueField = "vcEmailToType"
                radlstEmailTo.DataTextField = "vcReceiverName"

                radlstEmailTo.DataBind()

                'Send SMS Alerts

                radSendSMS.Items.Clear()
                radSendSMS.DataSource = dt
                radSendSMS.DataValueField = "vcEmailToType"
                radSendSMS.DataTextField = "vcReceiverName"
                radSendSMS.DataBind()

                'arr.Add(New ListItem("Owner of trigger record", 1))
                'arr.Add(New ListItem("Assignee of trigger record", 2))
                'arr.Add(New ListItem("Primary contact of trigger record", 3))
                'arr.Add(New ListItem("1", "Owner of trigger record"))
                'arr.Add(New ListItem("2", "Assignee of trigger record"))
                'arr.Add(New ListItem("3", "Primary contact of trigger record"))

            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub BindWebsitePages()
            Try
                Dim objTrackingVisitorsDTL As New TrackingVisitorsDTL
                objTrackingVisitorsDTL.DomainID = CCommon.ToLong(Session("DomainID"))
                rcbWebsitePages.DataSource = objTrackingVisitorsDTL.GetWebsitePagesForWorkflow()
                rcbWebsitePages.DataTextField = "vcPageName"
                rcbWebsitePages.DataValueField = "vcPageName"
                rcbWebsitePages.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Protected Sub ddlAction_OnSelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                If ddlAction.SelectedIndex > 0 Then
                    If ddlAction.SelectedValue = "1" Then 'send alert
                        BindSendTo()
                        dvSendMail.Style.Add("display", "block")
                        displayAlert.Style.Add("display", "none")
                        dvAssignItems.Style.Add("display", "none")
                        dvUpdateFields.Style.Add("display", "none")
                        dvBizDocapproval.Style.Add("display", "none")
                        dvCreateBizDoc.Style.Add("display", "none")
                        dvSelectBizDoc.Style.Add("display", "none")
                        dvSendSMS.Style.Add("display", "none")
                        If ddlForm.SelectedValue = 71 Then
                            dvAttachBizDoc.Style.Add("display", "block")
                        Else
                            dvAttachBizDoc.Style.Add("display", "none")
                        End If


                        GetEmailTemplateList()

                    ElseIf ddlAction.SelectedValue = "2" Then 'Assign Action Items
                        dvSendMail.Style.Add("display", "none")
                        displayAlert.Style.Add("display", "none")
                        dvAssignItems.Style.Add("display", "block")
                        dvUpdateFields.Style.Add("display", "none")
                        dvAttachBizDoc.Style.Add("display", "none")
                        dvBizDocapproval.Style.Add("display", "none")
                        dvCreateBizDoc.Style.Add("display", "none")
                        dvSelectBizDoc.Style.Add("display", "none")
                        dvSendSMS.Style.Add("display", "none")
                        BindActionItemTemplateList()
                        BindAssignTo()
                    ElseIf ddlAction.SelectedValue = "3" Then 'update fields
                        dvSendMail.Style.Add("display", "none")
                        displayAlert.Style.Add("display", "none")
                        dvAssignItems.Style.Add("display", "none")
                        dvUpdateFields.Style.Add("display", "block")
                        dvBizDocapproval.Style.Add("display", "none")
                        dvAttachBizDoc.Style.Add("display", "none")
                        dvCreateBizDoc.Style.Add("display", "none")
                        dvSelectBizDoc.Style.Add("display", "none")
                        dvSendSMS.Style.Add("display", "none")
                    ElseIf ddlAction.SelectedValue = "4" Then 'display Alert
                        dvSendMail.Style.Add("display", "none")
                        displayAlert.Style.Add("display", "block")
                        dvAssignItems.Style.Add("display", "none")
                        dvUpdateFields.Style.Add("display", "none")
                        dvAttachBizDoc.Style.Add("display", "none")
                        dvBizDocapproval.Style.Add("display", "none")
                        dvCreateBizDoc.Style.Add("display", "none")
                        dvSelectBizDoc.Style.Add("display", "none")
                        dvSendSMS.Style.Add("display", "none")
                    ElseIf ddlAction.SelectedValue = "5" Then 'BizDoc approval
                        BindApprovers()
                        dvBizDocapproval.Style.Add("display", "block")
                        dvSendMail.Style.Add("display", "none")
                        displayAlert.Style.Add("display", "none")
                        dvAssignItems.Style.Add("display", "none")
                        dvUpdateFields.Style.Add("display", "none")
                        dvAttachBizDoc.Style.Add("display", "none")
                        dvCreateBizDoc.Style.Add("display", "none")
                        dvSelectBizDoc.Style.Add("display", "none")

                        dvSendSMS.Style.Add("display", "none")
                    ElseIf ddlAction.SelectedValue = "6" Then 'Create BizDoc
                        BindBizDocType()
                        dvSendSMS.Style.Add("display", "none")
                        dvSendMail.Style.Add("display", "none")
                        displayAlert.Style.Add("display", "none")
                        dvAssignItems.Style.Add("display", "none")
                        dvUpdateFields.Style.Add("display", "none")
                        dvAttachBizDoc.Style.Add("display", "none")
                        dvBizDocapproval.Style.Add("display", "none")
                        dvCreateBizDoc.Style.Add("display", "block")
                        dvSelectBizDoc.Style.Add("display", "block")
                    ElseIf ddlAction.SelectedValue = "9" Then 'send SMS alerts
                        BindSendTo()
                        dvSendSMS.Style.Add("display", "block")
                        displayAlert.Style.Add("display", "none")
                        dvAssignItems.Style.Add("display", "none")
                        dvUpdateFields.Style.Add("display", "none")
                        dvBizDocapproval.Style.Add("display", "none")
                        dvCreateBizDoc.Style.Add("display", "none")
                        dvSelectBizDoc.Style.Add("display", "none")
                        dvSendMail.Style.Add("display", "none")
                        dvAttachBizDoc.Style.Add("display", "none")

                    ElseIf ddlAction.SelectedValue = "7" Then 'Promote

                        dvSendSMS.Style.Add("display", "none")
                        displayAlert.Style.Add("display", "none")
                        dvAssignItems.Style.Add("display", "none")
                        dvUpdateFields.Style.Add("display", "none")
                        dvBizDocapproval.Style.Add("display", "none")
                        dvCreateBizDoc.Style.Add("display", "none")
                        dvSelectBizDoc.Style.Add("display", "none")
                        dvSendMail.Style.Add("display", "none")
                        dvAttachBizDoc.Style.Add("display", "none")
                    ElseIf ddlAction.SelectedValue = "8" Then 'Demote

                        dvSendSMS.Style.Add("display", "none")
                        displayAlert.Style.Add("display", "none")
                        dvAssignItems.Style.Add("display", "none")
                        dvUpdateFields.Style.Add("display", "none")
                        dvBizDocapproval.Style.Add("display", "none")
                        dvCreateBizDoc.Style.Add("display", "none")
                        dvSelectBizDoc.Style.Add("display", "none")
                        dvSendMail.Style.Add("display", "none")
                        dvAttachBizDoc.Style.Add("display", "none")
                    End If

                Else
                    dvSendSMS.Style.Add("display", "none")
                    dvSendMail.Style.Add("display", "none")
                    displayAlert.Style.Add("display", "none")
                    dvBizDocapproval.Style.Add("display", "none")
                    dvAssignItems.Style.Add("display", "none")
                    dvUpdateFields.Style.Add("display", "none")
                    dvAttachBizDoc.Style.Add("display", "none")
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub
        Protected Sub ddlForm_OnSelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                lblDateRange.Text = ""
                If ddlForm.SelectedIndex > 0 Then
                    BindActionDdl()
                    GetUpdateFieldList(ddlForm.SelectedValue)
                    GetWFFormFieldList(CCommon.ToLong(ddlForm.SelectedValue))
                    GetWFFormFieldListCond(CCommon.ToLong(ddlForm.SelectedValue))
                    Session("numFormID") = ddlForm.SelectedValue

                    tblOrganizationFilter.Style.Add("display", If(ddlForm.SelectedValue = "68", "", "none")) 'Organization
                    tblContactType.Style.Add("display", If(ddlForm.SelectedValue = "69", "", "none")) 'Contact
                    tblOppOrder.Style.Add("display", If(ddlForm.SelectedValue = "70", "", "none")) 'Opportunities & Orders
                    tblBizDocs.Style.Add("display", If(ddlForm.SelectedValue = "71", "", "none")) 'BizDocs
                    tblCases.Style.Add("display", If(ddlForm.SelectedValue = "72", "", "none")) 'Cases
                    tblProjects.Style.Add("display", If(ddlForm.SelectedValue = "73", "", "none")) 'Projects
                    dvProcessTypeAndName.Style.Add("display", If(ddlForm.SelectedValue = "94", "", "none")) 'Sales/Purchase/project Process
                    tblActionItems.Style.Add("display", If(ddlForm.SelectedValue = "124", "", "none")) 'Action Items
                    tblARAging.Style.Add("display", If(ddlForm.SelectedValue = "138", "", "none")) 'A/R (Aging)
                    tblWebsite.Style.Add("display", If(ddlForm.SelectedValue = "148", "", "none"))

                    ddlCondition.Visible = True
                    ddlSign.Visible = True
                    tblrdbs.Style.Add("display", "block")

                    If ddlForm.SelectedValue = "68" Then 'Organization
                        ddlRelationship.Items.Clear()
                        ddlProfile.Items.Clear()
                        ddlFollowup.Items.Clear()

                        objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                        objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))
                        objCommon.sb_FillComboFromDBwithSel(ddlFollowup, 30, Session("DomainID"))

                        ddlRelationship.Items(0).Text = "-- Select Relationship --"
                        ddlRelationshipType.SelectedValue = "-1"
                        ddlProfile.Items(0).Text = "-- Select Profile --"
                        ddlFollowup.Items(0).Text = "-- Select Follow-up Status --"
                    ElseIf ddlForm.SelectedValue = "69" Then 'Contact
                        ddlContactType.Items.Clear()
                        objCommon.sb_FillComboFromDBwithSel(ddlContactType, 8, Session("DomainID"))
                        ddlContactType.Items(0).Text = "-- Select Contact Type --"
                    ElseIf ddlForm.SelectedValue = "70" Then 'Opportunities & Orders
                        ddlOppOrOrder.SelectedValue = "0"
                    ElseIf ddlForm.SelectedValue = "71" Then 'BizDocs
                        ddlOppOrOrderBizDocs.SelectedValue = "0"
                    ElseIf ddlForm.SelectedValue = "72" Then 'Cases
                        ddlCasesType.Items.Clear()
                        ddlCasesReason.Items.Clear()
                        ddlCasesStatus.Items.Clear()
                        ddlCasesPriority.Items.Clear()

                        objCommon.sb_FillComboFromDBwithSel(ddlCasesType, 16, Session("DomainID"))
                        objCommon.sb_FillComboFromDBwithSel(ddlCasesReason, 17, Session("DomainID"))
                        objCommon.sb_FillComboFromDBwithSel(ddlCasesStatus, 14, Session("DomainID"))
                        objCommon.sb_FillComboFromDBwithSel(ddlCasesPriority, 13, Session("DomainID"))

                        ddlCasesType.Items(0).Text = "-- Select Case Type --"
                        ddlCasesReason.Items(0).Text = "-- Select Case Reason --"
                        ddlCasesStatus.Items(0).Text = "-- Select Case Status --"
                        ddlCasesPriority.Items(0).Text = "-- Select Case Priority --"
                    ElseIf ddlForm.SelectedValue = "73" Then 'Projects
                        ddlProjectsType.Items.Clear()
                        ddlProjectsStatus.Items.Clear()

                        objCommon.sb_FillComboFromDBwithSel(ddlProjectsType, 380, Session("DomainID"))
                        objCommon.sb_FillComboFromDBwithSel(ddlProjectsStatus, 439, Session("DomainID"))

                        ddlProjectsType.Items(0).Text = "-- Select Project Type --"
                        ddlProjectsStatus.Items(0).Text = "-- Select Project Status --"
                    ElseIf ddlForm.SelectedValue = "94" Then 'Salese/Purchase/project Process
                        ddlProcessType.SelectedValue = "0"
                        ddlStageProgress.SelectedValue = "0"
                    ElseIf ddlForm.SelectedValue = "124" Then 'Action Items
                        ddlActionItemType.Items.Clear()
                        ddlActionItemTaskType.Items.Clear()
                        ddlActionItemPriority.Items.Clear()

                        objCommon.sb_FillComboFromDBwithSel(ddlActionItemType, 32, Session("DomainID"))
                        objCommon.sb_FillComboFromDBwithSel(ddlActionItemTaskType, 73, Session("DomainID"))
                        objCommon.sb_FillComboFromDBwithSel(ddlActionItemPriority, 447, Session("DomainID"))

                        ddlActionItemType.Items(0).Text = "-- Select Action Item Type --"
                        ddlActionItemTaskType.Items(0).Text = "-- Select Action Item Task --"
                        ddlActionItemPriority.Items(0).Text = "-- Select Action Item Priority --"
                    ElseIf ddlForm.SelectedValue = "138" Then 'A/R (Aging)
                        BindArAging()
                        ddlCondition.Visible = False
                        ddlSign.Visible = False
                        tblrdbs.Style.Add("display", "none")
                    ElseIf ddlForm.SelectedValue = "148" Then 'Website Page
                        BindWebsitePages()
                        tblrdbs.Style.Add("display", "none")
                    End If
                Else
                    tblOrganizationFilter.Style.Add("display", "none") 'Organization
                    tblContactType.Style.Add("display", "none") 'Contact
                    tblOppOrder.Style.Add("display", "none") 'Opportunities & Orders
                    tblBizDocs.Style.Add("display", "none") 'BizDocs
                    tblCases.Style.Add("display", "none") 'Cases
                    tblProjects.Style.Add("display", "none") 'Projects
                    dvProcessTypeAndName.Style.Add("display", "none") 'Salese/Purchase/project Process
                    tblActionItems.Style.Add("display", "none") 'Action Items
                    tblARAging.Style.Add("display", "none") 'A/R (Aging)
                    tblWebsite.Style.Add("display", "none")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub ddlBizDocType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocType.SelectedIndexChanged
            Try
                BindBizDoc()

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub


        Private Sub ddlCreateBizDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCreateBizDoc.SelectedIndexChanged
            Try
                BindBizDocsTemplate()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlProcessType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProcessType.SelectedIndexChanged
            Try
                If CCommon.ToLong(ddlProcessType.SelectedValue) > 0 Then
                    If objAdmin Is Nothing Then objAdmin = New CAdmin
                    objAdmin.DomainID = Session("DomainId")
                    If ddlProcessType.SelectedValue = "1" Then
                        objAdmin.Mode = 0
                    ElseIf ddlProcessType.SelectedValue = "2" Then
                        objAdmin.Mode = 2
                    ElseIf ddlProcessType.SelectedValue = "3" Then
                        objAdmin.Mode = 1
                    End If

                    ddlProcessName.DataSource = objAdmin.LoadProcessList()
                    ddlProcessName.DataTextField = "Slp_Name"
                    ddlProcessName.DataValueField = "Slp_Id"
                    ddlProcessName.DataBind()
                End If

                ddlProcessName.Items.Insert(0, New ListItem("-- Select Process Name --", "0"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub radlstEmailTo_ItemDataBound(sender As Object, e As RadListBoxItemEventArgs)
            Try
                Dim lblValue As Label = DirectCast(e.Item.FindControl("lblValue"), Label)
                Dim txtSendTo As TextBox = DirectCast(e.Item.FindControl("txtSendTo"), TextBox)
                If lblValue.Text = "8" Then
                    txtSendTo.Visible = True
                    txtSendTo.Style.Add("display", "block")
                Else
                    txtSendTo.Style.Add("display", "none")
                    txtSendTo.Visible = False
                End If
                'e.Item.ToolTip = (string)DataBinder.Eval(e.Item.DataItem, "ToolTipColumn");
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub
        Protected Sub ddlEmailTemplate_OnSelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                If ddlEmailTemplate.SelectedIndex > 0 Then
                    If ddlEmailTemplate.SelectedValue = "99999" Then
                        dvComposeMessage.Style.Add("display", "block")
                        divShowEmailDesc.Style.Add("display", "none")
                    Else
                        dvComposeMessage.Style.Add("display", "none")
                        lnkEmailTemplate.Text = "<b>Preview: </b> " + ddlEmailTemplate.SelectedItem.Text
                        lngSelectedEmailTemplate = ddlEmailTemplate.SelectedValue
                    End If

                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub

        Protected Sub ddlSign_OnSelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                If ddlSign.SelectedIndex > 0 Then

                    If ddlSign.SelectedValue = "ceq" Or ddlSign.SelectedValue = "cne" Or ddlSign.SelectedValue = "cgt" Or ddlSign.SelectedValue = "cge" Or ddlSign.SelectedValue = "clt" Or ddlSign.SelectedValue = "cle" Then
                        ddlCompareWith.Visible = True
                        Dim objWF As New BACRM.BusinessLogic.Workflow.Workflow

                        objWF.DomainID = Session("DomainID")
                        If CCommon.ToLong(ddlForm.SelectedValue) = 71 Then 'BizDocs
                            objWF.FormID = 49
                        Else
                            objWF.FormID = CCommon.ToLong(ddlForm.SelectedValue)
                        End If
                        Dim strDdlValue As String() = ddlCondition.SelectedValue.Split("_")

                        objWF.FieldID = CCommon.ToLong(strDdlValue(0))
                        Dim dtCompareFieldsList As DataTable
                        dtCompareFieldsList = objWF.GetWorkFlowCompareFields()
                        ddlCompareWith.DataSource = dtCompareFieldsList
                        ddlCompareWith.DataTextField = "vcFieldName"
                        ddlCompareWith.DataValueField = "vcOrigDbColumnName"
                        ddlCompareWith.DataBind()
                        ddlCompareWith.Items.Insert(0, New ListItem("--Select--", "0"))
                        radValue.Visible = False
                        txtValue.Visible = False
                        txtValueMoney.Visible = False
                        txtNvalue.Visible = False
                        calDate.Visible = False
                    Else

                        ddlCompareWith.Visible = False

                        Dim dtFieldsList As DataTable
                        dtFieldsList = DirectCast(ViewState("Fields"), DataTable)
                        Dim drFormRow As DataRow
                        Dim strDdlValue As String() = ddlCondition.SelectedValue.Split("_")
                        drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))
                        If drFormRow("vcAssociatedControlType") = "SelectBox" Then
                            radValue.Visible = True
                            calDate.Visible = False
                            txtNvalue.Visible = False
                            txtValueMoney.Visible = False
                            txtValue.Visible = False

                            If CCommon.ToInteger(ddlForm.SelectedValue) = 94 Then 'Sales/Purchase/Process
                                GetDropDownDataCond(CCommon.ToInteger(ddlProcessName.SelectedValue), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))

                            Else
                                GetDropDownDataCond(drFormRow("numListID"), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))

                            End If


                        ElseIf drFormRow("vcAssociatedControlType") = "CheckBox" Then
                            radValue.Visible = True
                            calDate.Visible = False
                            txtNvalue.Visible = False
                            txtValueMoney.Visible = False
                            txtValue.Visible = False

                            If CCommon.ToInteger(ddlForm.SelectedValue) = 94 Then 'Sales/Purchase/Process
                                GetDropDownDataCond(CCommon.ToInteger(ddlProcessName.SelectedValue), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))

                            Else
                                GetDropDownDataCond(drFormRow("numListID"), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))

                            End If
                        ElseIf drFormRow("vcAssociatedControlType") = "DateField" Then
                            radValue.Visible = False

                            txtNvalue.Visible = False
                            txtValueMoney.Visible = False
                            txtValue.Visible = False
                            calDate.Visible = True



                        ElseIf drFormRow("vcFieldDataType") = "M" Then
                            radValue.Visible = False
                            calDate.Visible = False
                            txtNvalue.Visible = False
                            txtValueMoney.Visible = True
                            txtValue.Visible = False

                            ' txtValueMoney.Attributes.Add("onkeypress", "CheckNumber(3,event)")
                        ElseIf drFormRow("vcFieldDataType") = "N" Then
                            radValue.Visible = False
                            calDate.Visible = False
                            txtNvalue.Visible = True
                            ' txtNvalue.Attributes.Add("onkeypress", "CheckNumber(3,event)")
                            txtValueMoney.Visible = False
                            txtValue.Visible = False

                        Else
                            radValue.Visible = False
                            calDate.Visible = False
                            txtNvalue.Visible = False
                            txtValueMoney.Visible = False
                            txtValue.Visible = True

                        End If



                    End If

                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub
        Private Sub LoadMergeField()
            Try
                Dim objDocument As New DocumentList
                objDocument.BindEmailTemplateMergeFields(radComposeMail, 1)
                ''***************************************************
                ''	USING CUSTOM TAG INSERTION FEATURE
                ''***************************************************
                ''This custom tag will be utilised by mail merge only when contact id is present for given email id
                'RadEditor1.Snippets.Clear()
                'RadEditor1.Snippets.Add("<b>Insert</b>", "")
                'RadEditor1.Snippets.Add("First Name", "##ContactFirstName##")
                'RadEditor1.Snippets.Add("Last Name", "##ContactLastName##")
                'RadEditor1.Snippets.Add("Company Name", "##OrganizationName##")
                'RadEditor1.Snippets.Add("Shipping Address", "##Ship##")
                If CCommon.ToLong(GetQueryStringVal("CaseID")) > 0 Then
                    radComposeMail.Snippets.Add("Case Number", "##vcCaseNumber##")
                    radComposeMail.Snippets.Add("Item Name", "##vcItemName##")
                    radComposeMail.Snippets.Add("Model", "##vcModelID##")
                    radComposeMail.Snippets.Add("Item Description", "##vcItemDesc##")
                    radComposeMail.Snippets.Add("Units", "##numUnitHour##")
                    radComposeMail.Snippets.Add("Serial No", "##vcSerialNo##")

                    'Add First Solution added to case into Email Body
                    Dim objSolution As New Solution
                    Dim dtSolution As DataTable
                    objSolution.CaseID = CCommon.ToLong(GetQueryStringVal("CaseID"))
                    dtSolution = objSolution.GetSolutionForCases
                    If dtSolution.Rows.Count > 0 Then
                        radComposeMail.Content = CCommon.ToString(dtSolution.Rows(0)("Solution Descrition"))
                    End If
                End If
                'Commented by chinta Bug id 612
                'If CCommon.ToInteger(GetQueryStringVal("PickAtch")) = 1 And GetQueryStringVal("BizDocID") <> "" Then
                '    Dim ddl As Telerik.Web.UI.EditorDropDown = CType(RadEditor1.FindTool("MergeField"), Telerik.Web.UI.EditorDropDown)
                '    ddl.Items.Add("BizDocID", "##BizDocID##")
                'End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region
#Region "Repeater"
        Private Sub SetInitialRow()
            Try
                Dim dt As New DataTable()
                Dim dr As DataRow = Nothing
                CCommon.AddColumnsToDataTable(dt, "RowNumber,numAction,vcAction,numGenericDocID,vcDocName,vcEmailToType,tintEmailFrom,vcReceiverName,TemplateName,RowID,vcAssignTo,numAssignTo,vcUpdateFields,vcAlertMessage,vcMessge,tintIsAttachment,vcApproval,tintSendMail,numBizDocTemplateID,vcBizDocTemplate,numBizDocTypeID,vcBizDocType,vcBizDoc,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject,vcIndividualAction")

                'Store the DataTable in ViewState for future reference

                ViewState("CurrentTable") = dt
                'Bind the Gridview
                'rprActions1.DataSource = dt
                ' rprActions1.DataBind()

                BindActionDdl()
                BindActionItemTemplateList()
                GetEmailTemplateList()
                'After binding the gridview, we can then extract and fill the DropDownList with Data
            Catch ex As Exception
                Throw
            End Try

        End Sub
        Private Sub AddNewRowToGrid()
            Try
                'GenerateConditionText()

                If ViewState("CurrentTable") IsNot Nothing Then

                    Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentTable"), DataTable)
                    Dim drCurrentRow As DataRow = Nothing

                    If hfWFCodeForEdit.Value = "0" Then
                        drCurrentRow = dtCurrentTable.NewRow()
                        drCurrentRow("RowNumber") = dtCurrentTable.Rows.Count + 1

                        If dtCurrentTable.Rows.Count > 0 Then
                            For i = 0 To dtCurrentTable.Rows.Count - 1
                                If (ddlAction.SelectedValue = dtCurrentTable.Rows(i)("numAction").ToString) Then
                                    If (ddlAction.SelectedValue = 6) Then 'Create BizDoc
                                        If (dtCurrentTable.Rows(i)("vcBizDoc").ToString = ddlCreateBizDoc.SelectedItem.Text) Then
                                            lblMessage.Text = "This action already exists,Please choose another action or update existing one"
                                            dvError.Style.Add("display", "block")
                                            dvSuccess.Style.Add("display", "none")
                                            lblMessage.ForeColor = Color.Red
                                            Return
                                        Else
                                            dvError.Style.Add("display", "none")
                                            dvSuccess.Style.Add("display", "none")
                                            lblMessage.Text = ""
                                        End If
                                    End If
                                    If (ddlAction.SelectedValue = 1) Then 'Send Alerts
                                        If (dtCurrentTable.Rows(i)("vcDocName").ToString = ddlEmailTemplate.SelectedItem.Text) Then
                                            lblMessage.Text = "This action already exists,Please choose another action or update existing one"
                                            dvError.Style.Add("display", "block")
                                            dvSuccess.Style.Add("display", "none")
                                            lblMessage.ForeColor = Color.Red
                                            Return
                                        Else
                                            dvError.Style.Add("display", "none")
                                            dvSuccess.Style.Add("display", "none")
                                            lblMessage.Text = ""
                                        End If
                                    End If

                                    If (ddlAction.SelectedValue = 2) Then 'Assign Action Items
                                        If (dtCurrentTable.Rows(i)("TemplateName").ToString = ddlActionItemTemplate.SelectedItem.Text) Then
                                            lblMessage.Text = "This action already exists,Please choose another action or update existing one"
                                            dvError.Style.Add("display", "block")
                                            dvSuccess.Style.Add("display", "none")
                                            lblMessage.ForeColor = Color.Red
                                            Return
                                        Else
                                            dvError.Style.Add("display", "none")
                                            dvSuccess.Style.Add("display", "none")
                                            lblMessage.Text = ""

                                        End If
                                    End If

                                Else
                                    dvError.Style.Add("display", "none")
                                    dvSuccess.Style.Add("display", "none")
                                    lblMessage.Text = ""
                                End If

                            Next
                        End If

                    Else
                        drCurrentRow = dtCurrentTable.Rows(dtCurrentTable.Rows.IndexOf(dtCurrentTable.Select("RowNumber=" + hfWFCodeForEdit.Value)(0)))
                        drCurrentRow("RowNumber") = Convert.ToInt32(hfWFCodeForEdit.Value)
                    End If
                    'For i As Integer = 0 To dtCurrentTable.Rows.Count - 1
                    'TemplateName, RowID, vcAssignTo, numAssignTo,vcAlertMessage
                    If ddlAction.SelectedValue = 1 Then 'Send Alerts
                        Dim sbEmailTo As New StringBuilder()
                        Dim sbEmailToValue As New StringBuilder()
                        Dim StrSendTo As String = Nothing
                        Dim collection As IList(Of RadListBoxItem) = radlstEmailTo.CheckedItems
                        For Each item As RadListBoxItem In collection
                            If (item.Value <> "8") Then
                                sbEmailTo.Append(item.Text & ",")
                                sbEmailToValue.Append(item.Value & ",")
                            End If
                            For Each item1 As RadListBoxItem In radlstEmailTo.Items
                                Dim txtSendTo As TextBox = DirectCast(item1.FindControl("txtSendTo"), TextBox)
                                If txtSendTo.Text IsNot Nothing Then
                                    StrSendTo = txtSendTo.Text
                                End If
                            Next
                        Next
                        ' Dim strPickingFields As String = sbEmailToValue.ToString.Remove(sbEmailToValue.ToString.Length - 1, 1)
                        drCurrentRow("numAction") = ddlAction.SelectedValue
                        drCurrentRow("vcAction") = ddlAction.SelectedItem
                        drCurrentRow("numGenericDocID") = ddlEmailTemplate.SelectedValue
                        drCurrentRow("vcDocName") = ddlEmailTemplate.SelectedItem
                        If (StrSendTo.ToString = "") Then
                            drCurrentRow("vcReceiverName") = If((sbEmailTo.ToString = ""), "", sbEmailTo.ToString().Remove(sbEmailTo.ToString.Length - 1, 1))
                        Else
                            drCurrentRow("vcReceiverName") = If((sbEmailTo.ToString = ""), "", sbEmailTo.ToString())
                        End If

                        drCurrentRow("vcEmailToType") = If((sbEmailToValue.ToString = ""), "", sbEmailToValue.ToString().Remove(sbEmailToValue.ToString.Length - 1, 1))
                        drCurrentRow("tintEmailFrom") = rblFrom.SelectedValue
                        drCurrentRow("vcEmailSendTo") = If((StrSendTo.ToString = ""), "", StrSendTo.ToString)
                        drCurrentRow("vcMailSubject") = If((txtMailSubject.Text = ""), "", txtMailSubject.Text)

                        drCurrentRow("vcMailBody") = radComposeMail.Content
                        drCurrentRow("RowID") = Nothing
                        drCurrentRow("TemplateName") = Nothing
                        drCurrentRow("numAssignTo") = Nothing
                        drCurrentRow("vcAssignTo") = Nothing
                        drCurrentRow("vcAlertMessage") = Nothing
                        drCurrentRow("vcUpdateFields") = Nothing

                        If ddlForm.SelectedValue = 71 Then
                            If chkAttachBizDoc.Checked Then
                                drCurrentRow("tintIsAttachment") = 1
                            Else
                                drCurrentRow("tintIsAttachment") = 0
                            End If
                        Else
                            drCurrentRow("tintIsAttachment") = 0
                        End If

                        drCurrentRow("vcApproval") = Nothing
                        drCurrentRow("tintSendMail") = Nothing
                        drCurrentRow("numBizDocTemplateID") = Nothing
                        drCurrentRow("vcBizDocTemplate") = Nothing
                        drCurrentRow("vcBizDocType") = Nothing
                        drCurrentRow("vcBizDoc") = Nothing
                        drCurrentRow("numBizDocTypeID") = Nothing
                        drCurrentRow("vcSMSText") = Nothing

                        Dim vcFrom As String = ""
                        Select Case drCurrentRow("tintEmailFrom")
                            Case "1"
                                vcFrom = "Owner of trigger record"
                            Case "2"
                                vcFrom = "Assignee of trigger record"
                            Case Else
                                vcFrom = "Global Messaging & Calendaring"
                        End Select

                        If CCommon.ToString(Session("strCondition")) = Nothing Then
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; &nbsp;" & drCurrentRow("vcAction") & "&nbsp; Using Email Template <b>" & drCurrentRow("vcDocName") & "</b> To <b>" & drCurrentRow("vcReceiverName") & StrSendTo.ToString & "</b> From <b>" & vcFrom & "</b>"
                        Else
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & " Then " & drCurrentRow("vcAction") & " Using Email Template <b>" & drCurrentRow("vcDocName") & "</b> To <b>" & drCurrentRow("vcReceiverName") & StrSendTo.ToString & "</b> From <b>" & vcFrom & "</b>"
                        End If

                        drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "&nbsp; Using Email Template <b>" & drCurrentRow("vcDocName") & "</b> To <b>" & drCurrentRow("vcReceiverName") & StrSendTo.ToString & "</b> From <b>" & vcFrom & "</b>"

                    ElseIf ddlAction.SelectedValue = 2 Then 'Assign Action Items
                        drCurrentRow("numAction") = ddlAction.SelectedValue
                        drCurrentRow("vcAction") = ddlAction.SelectedItem
                        drCurrentRow("numGenericDocID") = Nothing
                        drCurrentRow("vcDocName") = Nothing
                        drCurrentRow("vcReceiverName") = Nothing
                        drCurrentRow("vcEmailToType") = Nothing
                        drCurrentRow("tintEmailFrom") = Nothing
                        drCurrentRow("vcEmailSendTo") = Nothing
                        drCurrentRow("vcMailSubject") = Nothing
                        drCurrentRow("vcMailBody") = Nothing
                        drCurrentRow("RowID") = ddlActionItemTemplate.SelectedValue
                        drCurrentRow("TemplateName") = ddlActionItemTemplate.SelectedItem
                        drCurrentRow("numAssignTo") = ddlAssignTo.SelectedValue
                        drCurrentRow("vcAssignTo") = ddlAssignTo.SelectedItem
                        drCurrentRow("vcAlertMessage") = Nothing
                        drCurrentRow("vcUpdateFields") = Nothing
                        drCurrentRow("tintIsAttachment") = 0
                        drCurrentRow("vcApproval") = Nothing
                        drCurrentRow("tintSendMail") = Nothing
                        drCurrentRow("numBizDocTemplateID") = Nothing
                        drCurrentRow("vcBizDocTemplate") = Nothing
                        drCurrentRow("vcBizDocType") = Nothing
                        drCurrentRow("vcBizDoc") = Nothing
                        drCurrentRow("numBizDocTypeID") = Nothing
                        drCurrentRow("vcSMSText") = Nothing
                        If CCommon.ToString(Session("strCondition")) = Nothing Then
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; &nbsp;" & drCurrentRow("vcAction") & "&nbsp; Using Action Template <b>" & drCurrentRow("TemplateName") & "</b> To <b>" & drCurrentRow("vcAssignTo") & "</b>"
                        Else
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & " Then " & drCurrentRow("vcAction") & " Using Action Template <b>" & drCurrentRow("TemplateName") & "</b> To <b>" & drCurrentRow("vcAssignTo") & "</b>"
                        End If

                        drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "&nbsp; Using Action Template <b>" & drCurrentRow("TemplateName") & "</b> To <b>" & drCurrentRow("vcAssignTo") & "</b>"

                    ElseIf ddlAction.SelectedValue = "3" Then 'update fields
                        Dim sbUpdateFields As New StringBuilder()
                        Dim sbUpdateFieldsText As New StringBuilder()
                        Dim strUpdateFields As String = Nothing
                        Dim dtUFActions As DataTable
                        dtUFActions = DirectCast(ViewState("UFCurrentTable"), DataTable)

                        If dtUFActions.Rows.Count > 0 Then
                            For i = 0 To dtUFActions.Rows.Count - 1
                                strUpdateFields = dtUFActions.Rows(i)("vcFieldName").ToString & "," & strUpdateFields
                            Next

                        Else
                            lblMessage.Text = "Please Choose Update Fields"
                        End If

                        drCurrentRow("numAction") = ddlAction.SelectedValue
                        drCurrentRow("vcAction") = ddlAction.SelectedItem
                        drCurrentRow("numGenericDocID") = Nothing
                        drCurrentRow("vcDocName") = Nothing
                        drCurrentRow("vcReceiverName") = Nothing
                        drCurrentRow("vcEmailToType") = Nothing
                        drCurrentRow("tintEmailFrom") = Nothing
                        drCurrentRow("vcEmailSendTo") = Nothing
                        drCurrentRow("vcMailBody") = Nothing
                        drCurrentRow("vcMailSubject") = Nothing
                        drCurrentRow("RowID") = Nothing
                        drCurrentRow("TemplateName") = Nothing
                        drCurrentRow("numAssignTo") = Nothing
                        drCurrentRow("vcAssignTo") = Nothing
                        drCurrentRow("vcAlertMessage") = Nothing
                        drCurrentRow("vcUpdateFields") = CCommon.ToString(strUpdateFields)
                        drCurrentRow("tintIsAttachment") = 0
                        drCurrentRow("vcApproval") = Nothing
                        drCurrentRow("tintSendMail") = Nothing
                        drCurrentRow("numBizDocTemplateID") = Nothing
                        drCurrentRow("vcBizDocTemplate") = Nothing
                        drCurrentRow("vcBizDocType") = Nothing
                        drCurrentRow("vcBizDoc") = Nothing
                        drCurrentRow("numBizDocTypeID") = Nothing
                        drCurrentRow("vcSMSText") = Nothing
                        If CCommon.ToString(Session("strCondition")) = Nothing Then
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; &nbsp; &nbsp; Updates the fields: <b>" & CCommon.ToString(strUpdateFields) & "</b>"
                        Else
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & " Then Updates the fields: <b>" & CCommon.ToString(strUpdateFields) & "</b>"
                        End If

                        drCurrentRow("vcIndividualAction") = "Updates the fields: <b>" & CCommon.ToString(strUpdateFields) & "</b>"

                    ElseIf ddlAction.SelectedValue = 5 Then 'BizDoc Approval
                        'vcApproval ,tintSendMail 
                        Dim sbApprovalAuthority As New StringBuilder()
                        Dim sbApprovalAuthorityText As New StringBuilder()
                        Dim collection As IList(Of RadListBoxItem) = radApprovalAuthority.CheckedItems
                        For Each item As RadListBoxItem In collection
                            sbApprovalAuthority.Append(item.Value & ",")
                            sbApprovalAuthorityText.Append(item.Text & ",")
                        Next
                        drCurrentRow("numAction") = ddlAction.SelectedValue
                        drCurrentRow("vcAction") = ddlAction.SelectedItem
                        drCurrentRow("numGenericDocID") = Nothing
                        drCurrentRow("vcDocName") = Nothing
                        drCurrentRow("vcReceiverName") = Nothing
                        drCurrentRow("vcEmailToType") = Nothing
                        drCurrentRow("tintEmailFrom") = Nothing
                        drCurrentRow("vcEmailSendTo") = Nothing
                        drCurrentRow("vcMailSubject") = Nothing
                        drCurrentRow("vcMailBody") = Nothing
                        drCurrentRow("RowID") = Nothing
                        drCurrentRow("TemplateName") = Nothing
                        drCurrentRow("numAssignTo") = Nothing
                        drCurrentRow("vcAssignTo") = Nothing
                        drCurrentRow("vcAlertMessage") = Nothing
                        drCurrentRow("vcUpdateFields") = Nothing
                        drCurrentRow("tintIsAttachment") = 0
                        drCurrentRow("vcApproval") = If((sbApprovalAuthority.ToString = ""), "", sbApprovalAuthority.ToString().Remove(sbApprovalAuthority.ToString.Length - 1, 1))
                        drCurrentRow("tintSendMail") = Nothing
                        drCurrentRow("numBizDocTemplateID") = Nothing
                        drCurrentRow("vcBizDocTemplate") = Nothing
                        drCurrentRow("vcBizDocType") = Nothing
                        drCurrentRow("vcBizDoc") = Nothing
                        drCurrentRow("numBizDocTypeID") = Nothing
                        drCurrentRow("vcSMSText") = Nothing
                        If CCommon.ToString(Session("strCondition")) = Nothing Then
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; &nbsp;" & drCurrentRow("vcAction") & "&nbsp; by <b>" & CCommon.ToString(sbApprovalAuthorityText) & "</b>"
                        Else
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & " Then " & drCurrentRow("vcAction") & "&nbsp; by <b>" & CCommon.ToString(sbApprovalAuthorityText) & "</b>"
                        End If

                        drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "&nbsp; by <b>" & CCommon.ToString(sbApprovalAuthorityText) & "</b>"

                    ElseIf ddlAction.SelectedValue = 6 Then 'Create BizDoc
                        'numBizDocTemplateID,vcBizDocTemplate,numBizDocType,numBizDoc
                        Dim sbApprovalAuthority As New StringBuilder()
                        Dim sbApprovalAuthorityText As New StringBuilder()
                        Dim collection As IList(Of RadListBoxItem) = radApprovalAuthority.CheckedItems
                        For Each item As RadListBoxItem In collection
                            sbApprovalAuthority.Append(item.Value & ",")
                            sbApprovalAuthorityText.Append(item.Text & ",")
                        Next
                        drCurrentRow("numAction") = ddlAction.SelectedValue
                        drCurrentRow("vcAction") = ddlAction.SelectedItem
                        drCurrentRow("numGenericDocID") = Nothing
                        drCurrentRow("vcDocName") = Nothing
                        drCurrentRow("vcReceiverName") = Nothing
                        drCurrentRow("vcEmailToType") = Nothing
                        drCurrentRow("tintEmailFrom") = Nothing
                        drCurrentRow("vcEmailSendTo") = Nothing
                        drCurrentRow("vcMailSubject") = Nothing
                        drCurrentRow("vcMailBody") = Nothing
                        drCurrentRow("RowID") = Nothing
                        drCurrentRow("TemplateName") = Nothing
                        drCurrentRow("numAssignTo") = Nothing
                        drCurrentRow("vcAssignTo") = Nothing
                        drCurrentRow("vcAlertMessage") = Nothing
                        drCurrentRow("vcUpdateFields") = Nothing
                        drCurrentRow("tintIsAttachment") = 0
                        drCurrentRow("vcApproval") = Nothing
                        drCurrentRow("tintSendMail") = Nothing
                        drCurrentRow("numBizDocTemplateID") = CCommon.ToLong(ddlBizTemplate.SelectedValue)
                        drCurrentRow("vcBizDocTemplate") = CCommon.ToString(ddlBizTemplate.SelectedItem)
                        drCurrentRow("vcBizDocType") = CCommon.ToString(ddlBizDocType.SelectedItem)
                        drCurrentRow("vcBizDoc") = CCommon.ToString(ddlCreateBizDoc.SelectedItem)
                        drCurrentRow("numBizDocTypeID") = CCommon.ToLong(ddlCreateBizDoc.SelectedValue)
                        drCurrentRow("vcSMSText") = Nothing
                        If CCommon.ToString(Session("strCondition")) = Nothing Then
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; &nbsp;" & drCurrentRow("vcAction") & "&nbsp; using Template <b>" & CCommon.ToString(drCurrentRow("vcBizDocTemplate")) & "</b>"
                        Else
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & " Then " & drCurrentRow("vcAction") & "&nbsp; using template <b>" & CCommon.ToString(drCurrentRow("vcBizDocTemplate")) & "</b>"
                        End If
                        drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "&nbsp; using Template <b>" & CCommon.ToString(drCurrentRow("vcBizDocTemplate")) & "</b>"
                    ElseIf ddlAction.SelectedValue = 9 Then 'Send SMS Alerts
                        Dim sbSMSTo As New StringBuilder()
                        Dim sbSMSToValue As New StringBuilder()
                        Dim collection As IList(Of RadListBoxItem) = radSendSMS.CheckedItems
                        For Each item As RadListBoxItem In collection
                            sbSMSTo.Append(item.Text & ",")
                            sbSMSToValue.Append(item.Value & ",")
                        Next

                        drCurrentRow("numAction") = ddlAction.SelectedValue
                        drCurrentRow("vcAction") = ddlAction.SelectedItem
                        drCurrentRow("numGenericDocID") = Nothing
                        drCurrentRow("vcDocName") = Nothing
                        drCurrentRow("vcReceiverName") = If((sbSMSTo.ToString = ""), "", sbSMSTo.ToString().Remove(sbSMSTo.ToString.Length - 1, 1))
                        drCurrentRow("vcEmailToType") = If((sbSMSToValue.ToString = ""), "", sbSMSToValue.ToString().Remove(sbSMSToValue.ToString.Length - 1, 1))
                        drCurrentRow("tintEmailFrom") = Nothing
                        drCurrentRow("vcEmailSendTo") = Nothing
                        drCurrentRow("vcMailSubject") = Nothing
                        drCurrentRow("vcMailBody") = Nothing
                        drCurrentRow("RowID") = Nothing
                        drCurrentRow("TemplateName") = Nothing
                        drCurrentRow("numAssignTo") = Nothing

                        drCurrentRow("vcAssignTo") = Nothing
                        drCurrentRow("vcAlertMessage") = Nothing
                        drCurrentRow("vcUpdateFields") = Nothing
                        drCurrentRow("tintIsAttachment") = 0
                        drCurrentRow("vcApproval") = Nothing
                        drCurrentRow("tintSendMail") = Nothing
                        drCurrentRow("numBizDocTemplateID") = Nothing
                        drCurrentRow("vcBizDocTemplate") = Nothing
                        drCurrentRow("vcBizDocType") = Nothing
                        drCurrentRow("vcBizDoc") = Nothing
                        drCurrentRow("numBizDocTypeID") = Nothing
                        drCurrentRow("vcSMSText") = txtSMSText.Text.Trim()
                        If CCommon.ToString(Session("strCondition")) = Nothing Then
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; &nbsp;" & drCurrentRow("vcAction") & " : <b>" & drCurrentRow("vcSMSText") & "</b> To <b>" & drCurrentRow("vcReceiverName").ToString & "</b>"
                        Else
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & " Then " & drCurrentRow("vcAction") & " : <b>" & drCurrentRow("vcSMSText") & "</b> To <b>" & drCurrentRow("vcReceiverName").ToString & "</b>"
                        End If
                        drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & " : <b>" & drCurrentRow("vcSMSText") & "</b> To <b>" & drCurrentRow("vcReceiverName").ToString & "</b>"

                    Else 'Display alert Message
                        drCurrentRow("numAction") = ddlAction.SelectedValue
                        drCurrentRow("vcAction") = ddlAction.SelectedItem
                        drCurrentRow("numGenericDocID") = Nothing
                        drCurrentRow("vcDocName") = Nothing
                        drCurrentRow("vcReceiverName") = Nothing
                        drCurrentRow("vcEmailToType") = Nothing
                        drCurrentRow("tintEmailFrom") = Nothing
                        drCurrentRow("vcEmailSendTo") = Nothing
                        drCurrentRow("vcMailSubject") = Nothing
                        drCurrentRow("vcMailBody") = Nothing
                        drCurrentRow("RowID") = Nothing
                        drCurrentRow("TemplateName") = Nothing
                        drCurrentRow("numAssignTo") = Nothing
                        drCurrentRow("vcAssignTo") = Nothing
                        drCurrentRow("vcAlertMessage") = txtAlertMessage.Text
                        drCurrentRow("vcUpdateFields") = Nothing
                        drCurrentRow("tintIsAttachment") = 0
                        drCurrentRow("vcApproval") = Nothing
                        drCurrentRow("tintSendMail") = Nothing
                        drCurrentRow("numBizDocTemplateID") = Nothing
                        drCurrentRow("vcBizDocTemplate") = Nothing
                        drCurrentRow("vcBizDocType") = Nothing
                        drCurrentRow("vcBizDoc") = Nothing
                        drCurrentRow("numBizDocTypeID") = Nothing
                        drCurrentRow("vcSMSText") = Nothing
                        If CCommon.ToString(Session("strCondition")) = Nothing Then
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; &nbsp;" & drCurrentRow("vcAction") & " : <b>" & drCurrentRow("vcAlertMessage") & "</b>"
                        Else
                            drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & " Then " & drCurrentRow("vcAction") & " : <b>" & drCurrentRow("vcAlertMessage") & "</b>"
                        End If
                        drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & " : <b>" & drCurrentRow("vcAlertMessage") & "</b>"

                    End If

                    If rdbCreate.Checked Then
                        Session("vcWFAction") = CCommon.ToString(drCurrentRow("vcMessge"))
                    ElseIf rdbDateFieldCond.Checked Then
                        Session("vcWFAction") = ddlBefore.SelectedItem.ToString & txtDays.Text & " Days of- " & ddldateField.SelectedItem.ToString & "- &nbsp; ," & CCommon.ToString(drCurrentRow("vcMessge"))
                    Else
                        Session("vcWFAction") = CCommon.ToString(drCurrentRow("vcMessge"))
                    End If

                    If hfWFCodeForEdit.Value = "0" Then
                        dtCurrentTable.Rows.Add(drCurrentRow)
                    Else
                        dtCurrentTable.AcceptChanges()
                        hfWFCodeForEdit.Value = "0"
                    End If
                    'Store the current data to ViewState for future reference
                    ViewState("CurrentTable") = dtCurrentTable
                    'Rebind the Grid with the current data to reflect changes
                    rprActions1.DataSource = dtCurrentTable
                    rprActions1.DataBind()
                    ddlAction.Enabled = True
                    hfActionCount.Value = dtCurrentTable.Rows.Count

                Else
                    Response.Write("ViewState is null")
                End If

            Catch ex As Exception
                Throw
            End Try

        End Sub
        Private Sub SetPreviousData()
            Try
                Dim rowIndex As Integer = 0
                If ViewState("CurrentTable") IsNot Nothing Then

                    Dim dt As DataTable = DirectCast(ViewState("CurrentTable"), DataTable)
                    If dt.Rows.Count > 0 Then

                        For i As Integer = 0 To dt.Rows.Count - 1
                            If i < dt.Rows.Count - 1 Then
                                Dim ddlAction As DropDownList = DirectCast(rprActions1.Items(i).FindControl("ddlAction"), DropDownList)
                                Dim ddlEmailTemplate As DropDownList = DirectCast(rprActions1.Items(i).FindControl("ddlEmailTemplate"), DropDownList)
                                Dim txtReceiver As TextBox = DirectCast(rprActions1.Items(i).FindControl("txtReceiver"), TextBox)
                                BindActionDdl()
                                BindActionItemTemplateList()
                                GetEmailTemplateList()
                                ' numGenericDocID,vcDocName,vcReceiverName
                                'Assign the value from DataTable to the TextBox
                                ddlAction.ClearSelection()
                                ddlAction.Items.FindByValue(dt.Rows(i)("numAction").ToString()).Selected = True
                                ddlAction.ClearSelection()
                                ddlAction.Items.FindByText(dt.Rows(i)("vcAction").ToString()).Selected = True

                                ddlEmailTemplate.ClearSelection()
                                ddlEmailTemplate.Items.FindByValue(dt.Rows(i)("numGenericDocID").ToString()).Selected = True
                                ddlEmailTemplate.ClearSelection()
                                ddlEmailTemplate.Items.FindByText(dt.Rows(i)("vcDocName").ToString()).Selected = True

                                txtReceiver.Text = CCommon.ToString(dt.Rows(i)("vcReceiverName"))

                            End If

                            rowIndex += 1
                        Next
                    End If
                End If

            Catch ex As Exception
                Throw
            End Try

        End Sub
        ' btnAddAction
        Protected Sub ButtonAdd_Click(sender As Object, e As EventArgs)
            Try
                AddNewRowToGrid()

                ddlForm.Enabled = False
                ddlAction.SelectedValue = "0"
                dvUpdateFields.Style.Add("display", "none")
                dvBizDocapproval.Style.Add("display", "none")
                dvAttachBizDoc.Style.Add("display", "none")
                dvSelectBizDoc.Style.Add("display", "none")
                dvSendMail.Style.Add("display", "none")
                displayAlert.Style.Add("display", "none")
                dvSendSMS.Style.Add("display", "none")
                dvAssignItems.Style.Add("display", "none")
                dvCreateBizDoc.Style.Add("display", "none")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try


        End Sub
        Protected Sub rprActions1_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try

                If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                    Dim lblActionText As Label = DirectCast(e.Item.FindControl("lblActionText"), Label)
                    Dim lblAction As Label = DirectCast(e.Item.FindControl("lblAction"), Label)
                    Dim lblDocName As Label = DirectCast(e.Item.FindControl("lblDocName"), Label)
                    Dim lblTemplateName As Label = DirectCast(e.Item.FindControl("lblTemplateName"), Label)
                    Dim lblUpdateFields As Label = DirectCast(e.Item.FindControl("lblUpdateFields"), Label)
                    Dim lblAltertMessage As Label = DirectCast(e.Item.FindControl("lblAltertMessage"), Label)
                    Dim lblReceiverName As Label = DirectCast(e.Item.FindControl("lblReceiverName"), Label)
                    Dim lblAssignTo As Label = DirectCast(e.Item.FindControl("lblAssignTo"), Label)
                    'TemplateName, RowID, vcAssignTo, numAssignTo,vcAlertMessage
                    If ddlAction.SelectedValue = 1 Then 'Send Alerts
                        lblActionText.Text = lblAction.Text & "&nbsp; Using Email Template <b>" & lblDocName.Text & "</b> To <b>" & lblReceiverName.Text & "</b>"
                    ElseIf ddlAction.SelectedValue = 2 Then 'Assign Action Items
                        lblActionText.Text = lblAction.Text & "&nbsp; Using Email Template <b>" & lblTemplateName.Text & "</b> To <b>" & lblAssignTo.Text & "</b>"
                    ElseIf ddlAction.SelectedValue = "3" Then 'update fields
                        lblActionText.Text = lblAction.Text & "&nbsp; Updates fields: <b>" & lblUpdateFields.Text & "</b>"
                    Else
                        lblActionText.Text = lblAction.Text & "&nbsp; Using Email Template <b>" & lblAltertMessage.Text & "</b> To <b>" & lblAltertMessage.Text & "</b>"
                    End If

                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Protected Sub rprActions1_ItemCommand(src As Object, e As RepeaterCommandEventArgs)
            Try
                'ss//ClearSampleControls();
                Dim dtActions As DataTable
                If e.CommandName = "Edit" Then
                    'add Sample

                    dtActions = DirectCast(ViewState("CurrentTable"), DataTable)

                    Dim drCurrentRow As DataRow = dtActions.Rows(dtActions.Rows.IndexOf(dtActions.Select("RowNumber=" + e.CommandArgument)(0)))
                    hfWFCodeForEdit.Value = drCurrentRow("RowNumber").ToString()

                    ddlAction.SelectedValue = CCommon.ToString(drCurrentRow("numAction"))
                    ddlAction_OnSelectedIndexChanged(ddlAction, New EventArgs())
                    ddlAction.Enabled = False
                    If ddlAction.SelectedValue = 1 Then 'Send Alerts

                        ddlEmailTemplate.SelectedValue = drCurrentRow("numGenericDocID")
                        lnkEmailTemplate.Text = "<b>Preview: </b> " + ddlEmailTemplate.SelectedItem.Text
                        lngSelectedEmailTemplate = ddlEmailTemplate.SelectedValue

                        Dim strReceiverName As String() = drCurrentRow("vcReceiverName").Split(",")
                        Dim strEmailTo As String() = drCurrentRow("vcEmailToType").Split(",")
                        For k = 0 To strEmailTo.Length - 1
                            For Each radItem As RadListBoxItem In radlstEmailTo.Items
                                If radItem.Value = strEmailTo(k) Then
                                    radItem.Checked = True
                                End If
                            Next
                        Next
                        'For k = 0 To strReceiverName.Length - 1
                        '    For Each radItem As RadListBoxItem In radlstEmailTo.Items
                        '        If radItem.Text = strReceiverName(k) Then
                        '            radItem.Checked = True
                        '        End If
                        '    Next
                        'Next
                        For Each item1 As RadListBoxItem In radlstEmailTo.Items
                            Dim txtSendTo As TextBox = DirectCast(item1.FindControl("txtSendTo"), TextBox)
                            If txtSendTo.Text IsNot Nothing And drCurrentRow("vcEmailSendTo").ToString <> "" Then
                                txtSendTo.Text = CCommon.ToString(drCurrentRow("vcEmailSendTo").ToString)
                                radlstEmailTo.FindItemByValue("8").Checked = True
                            End If
                        Next
                        If ddlEmailTemplate.SelectedValue = "99999" Then
                            dvComposeMessage.Style.Add("display", "block")
                            radComposeMail.Content = drCurrentRow("vcMailBody").ToString
                            txtMailSubject.Text = drCurrentRow("vcMailSubject").ToString
                        End If


                        'txtSendTo.Text = CCommon.ToString(drCurrentRow("vcEmailSendTo").ToString)
                        If ddlForm.SelectedValue = 71 Then
                            If CCommon.ToShort(drCurrentRow("tintIsAttachment")) = 1 Then
                                chkAttachBizDoc.Checked = True
                            Else
                                chkAttachBizDoc.Checked = False
                            End If
                        End If

                    ElseIf ddlAction.SelectedValue = 2 Then 'Assign Action Items
                        ddlActionItemTemplate.SelectedValue = drCurrentRow("RowID")

                        ddlAssignTo.SelectedValue = drCurrentRow("numAssignTo")


                    ElseIf ddlAction.SelectedValue = "3" Then 'update fields

                        Dim strUpdateFields As String() = drCurrentRow("vcUpdateFields").Split(",")
                        dvUpdateFields.Style.Add("display", "block")
                        ddlAction.Enabled = False
                        'For k = 0 To strUpdateFields.Length - 1
                        '    For Each radItem As RadListBoxItem In radUpdateFields.Items
                        '        If radItem.Value = strUpdateFields(k) Then
                        '            radItem.Checked = True
                        '        End If
                        '    Next
                        'Next

                    ElseIf ddlAction.SelectedValue = 5 Then 'BizDoc Approval
                        Dim strBizDocApproval As String() = drCurrentRow("vcApproval").Split(",")
                        For k = 0 To strBizDocApproval.Length - 1
                            For Each radItem As RadListBoxItem In radApprovalAuthority.Items
                                If radItem.Value = strBizDocApproval(k) Then
                                    radItem.Checked = True
                                End If
                            Next
                        Next
                        'space for tintSendMail
                    ElseIf ddlAction.SelectedValue = 6 Then 'Create  BizDoc
                        'ddlAction_OnSelectedIndexChanged(ddlAction, New EventArgs())
                        ddlBizDocType.SelectedIndex = ddlBizDocType.Items.IndexOf(ddlBizDocType.Items.FindByText(drCurrentRow("vcBizDocType")))

                        ddlBizDocType_SelectedIndexChanged(ddlBizDocType, New EventArgs())

                        ddlCreateBizDoc.SelectedIndex = ddlCreateBizDoc.Items.IndexOf(ddlCreateBizDoc.Items.FindByText(drCurrentRow("vcBizDoc")))
                        ddlCreateBizDoc_SelectedIndexChanged(ddlCreateBizDoc, New EventArgs())

                        ddlBizTemplate.SelectedIndex = ddlBizTemplate.Items.IndexOf(ddlBizTemplate.Items.FindByText(drCurrentRow("vcBizDocTemplate")))

                    ElseIf ddlAction.SelectedValue = 9 Then 'Send SMS Alerts

                        dvAttachBizDoc.Style.Add("display", "none")
                        Dim strReceiverName As String() = drCurrentRow("vcReceiverName").Split(",")
                        For k = 0 To strReceiverName.Length - 1
                            For Each radItem As RadListBoxItem In radSendSMS.Items
                                If radItem.Text = strReceiverName(k) Then
                                    radItem.Checked = True
                                End If
                            Next
                        Next
                        txtSMSText.Text = drCurrentRow("vcSMSText")
                    Else
                        txtAlertMessage.Text = drCurrentRow("vcAlertMessage")

                    End If
                ElseIf e.CommandName = "Delete" Then
                    'delete selected row

                    dtActions = DirectCast(ViewState("CurrentTable"), DataTable)

                    Dim drCurrentRow As DataRow = dtActions.Rows(dtActions.Rows.IndexOf(dtActions.Select("RowNumber=" + e.CommandArgument)(0)))

                    ddlAction.SelectedValue = CCommon.ToString(drCurrentRow("numAction"))
                    If ddlAction.SelectedValue = 3 Then
                        SetInitialRowUF()
                        rprUpdateFields.DataSource = Nothing
                        rprUpdateFields.DataBind()
                    End If

                    dtActions.Rows.RemoveAt(dtActions.Rows.IndexOf(dtActions.Select("RowNumber=" + e.CommandArgument)(0)))
                    dtActions.AcceptChanges()

                    ViewState("CurrentTable") = dtActions

                    rprActions1.DataSource = dtActions
                    rprActions1.DataBind()
                    'ltrTempEntry.Text = "Record is deleted."
                    ' hfLabourCode.Value = (Convert.ToInt32(hfLabourCode.Value) - 1).ToString()
                    'do nothing

                    SetModuleVisibility()
                Else

                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub
#End Region
#Region "Button/methods:Save,Get data(WFA)"
        Protected Sub btnSave_OnClick(sender As Object, e As EventArgs)
            Try
                SaveWFA(lngWFID)
            Catch ex As Exception
                If ex.Message.Contains("MASS SALES FULLFILLMENT RULE EXISTS") Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "AutomationRuleError", "alert('You can not set this condition(s) because it is used in Mass Sales Fulfillment Rules.');", True)
                ElseIf ex.Message.Contains("DUPLICATE_WORKFLOW") Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "DuplicateAutomationRule", "alert('A workflow rule that uses this module and these conditions already exists');", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub
        'Sachin
        Public Sub SaveWFA(ByVal lngWFID As Long)
            Try
                Dim objWF As New WorkFlowObject
                Dim objWFManage As New Workflow

                objWFManage.WFID = lngWFID
                objWFManage.DomainID = Session("DomainID")
                objWFManage.UserCntID = Session("UserContactID")
                objWFManage.WFName = txtWFName.Text
                objWFManage.WFDescription = txtDescription.Text
                objWFManage.boolActive = chkStatus.Checked
                objWFManage.FormID = CCommon.ToLong(ddlForm.SelectedValue)

                If rdbCreate.Checked Then
                    If (objWFManage.FormID = 138) Then
                        objWFManage.WFTriggerOn = 6 ' A/R Aging
                    Else
                        objWFManage.WFTriggerOn = 1 'create
                    End If
                ElseIf rdbFieldsUpdate.Checked Then
                    objWFManage.WFTriggerOn = 4 'field update
                ElseIf rdbDateFieldCond.Checked Then
                    objWFManage.WFTriggerOn = 3 'Date field Condition(Time Based Action)
                Else
                    If (objWFManage.FormID = 138) Then
                        objWFManage.WFTriggerOn = 6 ' A/R Aging
                    ElseIf objWFManage.FormID = 94 Then
                        objWFManage.WFTriggerOn = 2
                    End If
                    'Else
                    '    If CCommon.ToLong(ddlForm.SelectedValue) = 94 Then
                    '        objWFManage.WFTriggerOn = 2 'Date field Condition(Time Based Action)
                    '    Else
                    '        objWFManage.WFTriggerOn = 3 'Date field Condition(Time Based Action) //On record Create or Edit
                    '    End If

                End If

                If (objWFManage.FormID = 148) Then
                    objWFManage.WFTriggerOn = 1 'create
                End If

                If rdbDateFieldCond.Checked Then
                    'Custom Field support in Time based action ||
                    Dim dtCFields As DataTable
                    dtCFields = DirectCast(ViewState("CFields"), DataTable)
                    Dim dv As DataView = dtCFields.DefaultView
                    dv.RowFilter = "ID='" + CCommon.ToString(ddldateField.SelectedValue) + "'"

                    Dim dtCustFields As DataTable = dv.ToTable

                    objWFManage.vcDateField = CCommon.ToString(ddldateField.SelectedValue)
                    objWFManage.intDays = CCommon.ToInteger(txtDays.Text)
                    objWFManage.intActionOn = CCommon.ToInteger(ddlBefore.SelectedValue)
                    objWFManage.boolCustom = CCommon.ToBool(dtCustFields.Rows(0)("bitCustom").ToString)
                    objWFManage.numFieldID = CCommon.ToLong(dtCustFields.Rows(0)("numFieldID").ToString)
                ElseIf ddlForm.SelectedValue = 138 Then    'A/R Aging

                    objWFManage.vcDateField = Nothing

                    If (ddlArAging.SelectedValue = "1") Then
                        objWFManage.intDays = 1
                    ElseIf (ddlArAging.SelectedValue = "2") Then
                        objWFManage.intDays = 2
                    ElseIf (ddlArAging.SelectedValue = "3") Then
                        objWFManage.intDays = 5
                    ElseIf (ddlArAging.SelectedValue = "4") Then
                        objWFManage.intDays = 7
                    ElseIf (ddlArAging.SelectedValue = "5") Then
                        objWFManage.intDays = 14
                    ElseIf (ddlArAging.SelectedValue = "6") Then
                        objWFManage.intDays = 21
                    ElseIf (ddlArAging.SelectedValue = "7") Then
                        objWFManage.intDays = 30
                    End If

                    objWFManage.intActionOn = 0
                Else
                    objWFManage.vcDateField = Nothing
                    objWFManage.intDays = 0
                    objWFManage.intActionOn = 0
                End If

                If ddlForm.SelectedValue = 148 Then
                    Dim selectedPages As String = "["
                    For Each objItem As RadComboBoxItem In rcbWebsitePages.CheckedItems
                        selectedPages = selectedPages & "{""vcWebsitePage"":""" & objItem.Value & """}"
                    Next
                    selectedPages = selectedPages & "]"
                    objWFManage.WebsitePage = selectedPages
                    objWFManage.WebsiteDays = CCommon.ToInteger(txtWebsiteDays.Text)
                End If


                objWFManage.vcWFAction = CCommon.ToString(Session("vcWFAction")) & "..." 'CCommon.ToString(Session("strCondition")) & CCommon.ToString(Session("vcWFAction")) & "..."         

                Dim dr As DataRow
                'yet pending
                Dim dtWFTriggerFieldList As New DataTable
                CCommon.AddColumnsToDataTable(dtWFTriggerFieldList, "numFieldID,bitCustom")
                dtWFTriggerFieldList.TableName = "WorkFlowTriggerFieldList"
                'yes

                Dim strBeforeTrim As String = hdfPickedFields.Value
                If hdfPickedFields.Value <> "0" Then
                    Dim strPickingFields As String = strBeforeTrim.Remove(strBeforeTrim.Length - 1, 1)
                    Dim strpickFields() = strPickingFields.Split(",")
                    For Each str As String In strpickFields
                        dr = dtWFTriggerFieldList.NewRow
                        dr("numFieldID") = CCommon.ToLong(str.Split("_")(0))
                        dr("bitCustom") = CCommon.ToBool(str.Split("_")(1))
                        dtWFTriggerFieldList.Rows.Add(dr)
                    Next
                End If

                Dim dtConditions As DataTable
                'dtConditions = DirectCast(Session("CurrentCondTable"), DataTable)
                dtConditions = DirectCast(ViewState("CurrentCondTable"), DataTable)

                Dim dtConditionList As New DataTable
                CCommon.AddColumnsToDataTable(dtConditionList, "numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare")
                dtConditionList.TableName = "WorkFlowConditionList"

                For i As Integer = 0 To dtConditions.Rows.Count - 1
                    dr = dtConditionList.NewRow
                    Dim strID As String() = dtConditions.Rows(i)("ID").ToString().Split("_")
                    dr("numFieldID") = CCommon.ToLong(strID(0)) 'ID
                    dr("bitCustom") = CCommon.ToBool(strID(1)) 'ID
                    dr("vcFilterValue") = dtConditions.Rows(i)("numID").ToString().Trim 'VcData(only text),numID(Value)
                    dr("vcFilterOperator") = dtConditions.Rows(i)("numEquation").ToString().Trim 'numEquation
                    dr("vcFilterANDOR") = dtConditions.Rows(i)("vcANDOR").ToString().Trim 'vcANDOR
                    dr("vcFilterData") = dtConditions.Rows(i)("VcData").ToString().Trim
                    dr("intCompare") = CCommon.ToInteger(dtConditions.Rows(i)("intCompare").ToString())
                    dtConditionList.Rows.Add(dr)
                Next

                'CCommon.AddColumnsToDataTable(dt, "RowNumber,numAction,vcAction,numGenericDocID,vcDocName,vcReceiverName,TemplateName,RowID,vcAssignTo,numAssignTo,vcUpdateFields,vcAlertMessage,vcMessge")
                Dim dtActions As DataTable
                dtActions = DirectCast(ViewState("CurrentTable"), DataTable)
                Dim dtWFActionList As New DataTable
                ' numBizDocTemplateID,vcBizDocTemplate,vcBizDocType,vcBizDoc
                CCommon.AddColumnsToDataTable(dtWFActionList, "tintActionType,numTemplateID,vcEmailToType,tintEmailFrom,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject")
                dtWFActionList.TableName = "WorkFlowActionList"

                Dim dtUFActions As DataTable
                dtUFActions = DirectCast(ViewState("UFCurrentTable"), DataTable)
                Dim dtWFActionUpdateFields As New DataTable
                CCommon.AddColumnsToDataTable(dtWFActionUpdateFields, "tintModuleType,numFieldID,bitCustom,vcValue,numWFActionID,vcData")
                dtWFActionUpdateFields.TableName = "WorkFlowActionUpdateFields"
                For k As Integer = 0 To dtActions.Rows.Count - 1
                    dr = dtWFActionList.NewRow
                    dr("tintActionType") = dtActions.Rows(k)("numAction").ToString() 'numAction
                    If CCommon.ToShort(dtActions.Rows(k)("numAction")) = 1 Then 'Send Email Alerts
                        dr("numTemplateID") = CCommon.ToLong(dtActions.Rows(k)("numGenericDocID")) 'numGenericDocID,RowID
                    Else
                        dr("numTemplateID") = CCommon.ToLong(dtActions.Rows(k)("RowID")) 'numGenericDocID,RowID
                    End If

                    dr("vcEmailToType") = CCommon.ToString(dtActions.Rows(k)("vcEmailToType")) 'vcReceiverName
                    dr("tintEmailFrom") = CCommon.ToString(dtActions.Rows(k)("tintEmailFrom")) 'vcReceiverName
                    dr("vcEmailSendTo") = CCommon.ToString(dtActions.Rows(k)("vcEmailSendTo")) 'vcEmailSendTo(Optional)
                    dr("vcMailBody") = CCommon.ToString(dtActions.Rows(k)("vcMailBody")) 'Compose mail(Optional)

                    dr("vcMailSubject") = CCommon.ToString(dtActions.Rows(k)("vcMailSubject")) 'Compose mail subject(Optional)
                    dr("tintTicklerActionAssignedTo") = CCommon.ToShort(dtActions.Rows(k)("numAssignTo")) 'numAssignTo
                    dr("vcAlertMessage") = CCommon.ToString(dtActions.Rows(k)("vcAlertMessage")) 'vcAlertMessage
                    dr("tintIsAttachment") = CCommon.ToShort(dtActions.Rows(k)("tintIsAttachment"))
                    ' vcApproval, tintSendMail
                    dr("vcApproval") = CCommon.ToString(dtActions.Rows(k)("vcApproval")) 'vcApproval-On BizDoc Approval
                    dr("tintSendMail") = CCommon.ToShort(dtActions.Rows(k)("tintSendMail"))
                    dr("numBizDocTypeID") = CCommon.ToLong(dtActions.Rows(k)("numBizDocTypeID"))
                    dr("numBizDocTemplateID") = CCommon.ToLong(dtActions.Rows(k)("numBizDocTemplateID"))
                    dr("vcSMSText") = CCommon.ToString(dtActions.Rows(k)("vcSMSText"))
                    dr("tintActionOrder") = k

                    If CCommon.ToShort(dr("tintActionType")) = 3 Then
                        For j As Integer = 0 To dtUFActions.Rows.Count - 1
                            Dim dr1 As DataRow = dtWFActionUpdateFields.NewRow
                            Dim strID As String() = dtUFActions.Rows(j)("ID").ToString().Split("_")
                            dr1("numWFActionID") = k
                            dr1("tintModuleType") = dtUFActions.Rows(j)("tintModuleType").ToString()
                            dr1("numFieldID") = CCommon.ToLong(strID(0)) 'ID
                            dr1("bitCustom") = CCommon.ToBool(strID(1)) 'ID
                            'dr1("vcValue") = CCommon.ToString(dtUFActions.Rows(j)("vcValue"))numID
                            dr1("vcValue") = CCommon.ToString(dtUFActions.Rows(j)("numID"))
                            dr1("vcData") = CCommon.ToString(dtUFActions.Rows(j)("vcValue"))
                            dtWFActionUpdateFields.Rows.Add(dr1)
                        Next
                    End If

                    dtWFActionList.Rows.Add(dr)
                Next

                Dim ds As New DataSet
                ds.Tables.Add(dtWFTriggerFieldList)
                ds.Tables.Add(dtConditionList)
                ds.Tables.Add(dtWFActionList)
                ds.Tables.Add(dtWFActionUpdateFields)

                'objReportManage.textQuery = Sql.ToString()
                'objWFManage.strText = ds.GetXml.Replace("'", "''")
                objWFManage.strText = ds.GetXml
                objWFManage.ManageWorkFlowMaster()
                If lngWFID <> 0 Then
                    dvError.Style.Add("display", "none")
                    dvSuccess.Style.Add("display", "block")
                    lblMessage.ForeColor = Color.Green
                    lblMessage.Text = "WorkFlow Rule has been Updated successfully...!"
                Else
                    lblMessage.Text = "WorkFlow Rule has been Saved successfully...!"
                    dvError.Style.Add("display", "none")
                    dvSuccess.Style.Add("display", "block")
                    lblMessage.ForeColor = Color.Black
                End If

            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Sub GetWFAData(ByVal lngWFID As Long)
            Try
                If lngWFID > 0 Then
                    Dim objWFManage As New Workflow
                    Dim ds As DataSet

                    objWFManage.WFID = lngWFID
                    objWFManage.DomainID = Session("DomainID")
                    objWFManage.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    ds = objWFManage.GetWorkFlowMasterDetail
                    If ds.Tables.Count > 0 Then
                        If (ds.Tables(0).Rows.Count > 0) Then 'Work Flow Master
                            txtWFName.Text = CCommon.ToString(ds.Tables(0).Rows(0)("vcWFName"))
                            ddlForm.SelectedValue = CCommon.ToString(ds.Tables(0).Rows(0)("numFormID"))
                            ddlForm_OnSelectedIndexChanged(ddlForm, New EventArgs())
                            ddlForm.Enabled = False
                            txtDescription.Text = CCommon.ToString(ds.Tables(0).Rows(0)("vcWFDescription"))
                            chkStatus.Checked = CCommon.ToBool(ds.Tables(0).Rows(0)("bitActive")) 'tintWFTriggerOn
                            ' rdbCreate.Checked = CCommon.ToShort(ds.Tables(0).Rows(0)("tintWFTriggerOn"))
                            Dim iTriggeredOn As Short = CCommon.ToShort(ds.Tables(0).Rows(0)("tintWFTriggerOn"))
                            ddldateField.SelectedValue = CCommon.ToString(ds.Tables(0).Rows(0)("vcDateField"))

                            lblCreatedBy.Text = ds.Tables(0).Rows(0).Item("CreatedBy")
                            lblModifiedBy.Text = ds.Tables(0).Rows(0).Item("ModifiedBy")

                            Dim ARAgingValue As Long = ds.Tables(0).Rows(0).Item("intDays")

                            If Not String.IsNullOrEmpty(CCommon.ToString(ds.Tables(0).Rows(0)("vcWebsitePage"))) And CCommon.ToString(ds.Tables(0).Rows(0)("vcWebsitePage")) <> "[]" Then
                                Dim dtResult As DataTable = Newtonsoft.Json.JsonConvert.DeserializeObject(Of DataTable)(CCommon.ToString(ds.Tables(0).Rows(0)("vcWebsitePage")))

                                If Not dtResult Is Nothing AndAlso dtResult.Rows.Count > 0 Then
                                    For Each objItem As RadComboBoxItem In rcbWebsitePages.Items
                                        For Each drPage As DataRow In dtResult.Rows
                                            If Not rcbWebsitePages.Items.FindItemByValue(drPage("vcWebsitePage")) Is Nothing Then
                                                rcbWebsitePages.Items.FindItemByValue(drPage("vcWebsitePage")).Checked = True
                                            End If
                                        Next
                                    Next
                                End If
                            End If

                            txtWebsiteDays.Text = CCommon.ToString(ds.Tables(0).Rows(0)("intWebsiteDays"))

                            If iTriggeredOn = 1 Then 'Create
                                rdbCreate.Checked = True
                            ElseIf iTriggeredOn = 2 Or iTriggeredOn = 3 Then 'Edit
                                'If CCommon.ToLong(ddlForm.SelectedValue) = 94 Then
                                If ddldateField.SelectedIndex > 0 Then
                                    rdbDateFieldCond.Checked = True
                                    txtDays.Text = CCommon.ToInteger(ds.Tables(0).Rows(0)("intDays"))
                                    ddlBefore.SelectedValue = CCommon.ToInteger(ds.Tables(0).Rows(0)("intActionOn"))
                                End If
                                ' End If
                            ElseIf iTriggeredOn = 4 Then 'Fields Update
                                rdbFieldsUpdate.Checked = True
                            ElseIf iTriggeredOn = 6 Then
                                If (ARAgingValue = 1) Then
                                    ddlArAging.SelectedValue = "1"
                                ElseIf (ARAgingValue = 2) Then
                                    ddlArAging.SelectedValue = "2"
                                ElseIf (ARAgingValue = 5) Then
                                    ddlArAging.SelectedValue = "3"
                                ElseIf (ARAgingValue = 7) Then
                                    ddlArAging.SelectedValue = "4"
                                ElseIf (ARAgingValue = 14) Then
                                    ddlArAging.SelectedValue = "5"
                                ElseIf (ARAgingValue = 21) Then
                                    ddlArAging.SelectedValue = "6"
                                ElseIf (ARAgingValue = 30) Then
                                    ddlArAging.SelectedValue = "7"

                                End If
                            End If
                        End If

                        If rdbFieldsUpdate.Checked Then
                            Dim strFieldName As String = Nothing
                            Dim strFieldID As String = Nothing
                            If (ds.Tables(1).Rows.Count > 0) Then 'WorkFlowTriggerFieldList
                                For i = 0 To ds.Tables(1).Rows.Count - 1
                                    Dim strFields As String = ds.Tables(1).Rows(i)("FieldID").ToString

                                    strFieldName = ds.Tables(1).Rows(i)("FieldName").ToString & "," & strFieldName
                                    strFieldID = ds.Tables(1).Rows(i)("FieldID").ToString & "," & strFieldID
                                    For Each radItem As RadListBoxItem In radFields.Items
                                        If radItem.Value = strFields Then
                                            radItem.Checked = True
                                        End If
                                        hdfPickedFields.Value = CCommon.ToString(strFieldID)
                                    Next
                                Next
                            End If
                        End If
                        If (ds.Tables(2).Rows.Count > 0) Then 'WorkFlowConditionList

                            Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentCondTable"), DataTable)
                            Dim drCurrentRow As DataRow = Nothing
                            For i = 0 To ds.Tables(2).Rows.Count - 1

                                If hdfConditions.Value = "0" Then
                                    drCurrentRow = dtCurrentTable.NewRow()
                                    drCurrentRow("RowNumber") = dtCurrentTable.Rows.Count + 1
                                Else
                                    drCurrentRow = dtCurrentTable.Rows(dtCurrentTable.Rows.IndexOf(dtCurrentTable.Select("RowNumber=" + hdfConditions.Value)(0)))
                                    drCurrentRow("RowNumber") = Convert.ToInt32(hdfConditions.Value)
                                End If
                                drCurrentRow("ID") = CCommon.ToString(ds.Tables(2).Rows(i)("FieldID"))
                                If (CCommon.ToLong(ds.Tables(0).Rows(0)("numFormID")) = 138) Then    'A/R Aging
                                    drCurrentRow("vcFieldName") = "A/R Aging"
                                Else
                                    drCurrentRow("vcFieldName") = CCommon.ToString(ds.Tables(2).Rows(i)("FieldName"))
                                End If

                                drCurrentRow("numEquation") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterOperator"))

                                If ddlCondition.SelectedIndex >= 0 Then
                                    Dim dtFieldsList As DataTable
                                    dtFieldsList = DirectCast(ViewState("Fields"), DataTable)
                                    Dim drFormRow As DataRow
                                    If (CCommon.ToLong(ds.Tables(0).Rows(0)("numFormID")) <> 138) Then    'A/R Aging
                                        drCurrentRow("vcEquation") = CCommon.ToString(ds.Tables(2).Rows(i)("FilterOperator"))
                                        Dim strDdlValue As String() = ddlCondition.SelectedValue.Split("_")
                                        drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(CCommon.ToString(ds.Tables(2).Rows(i)("numFieldID"))))(0)))
                                    Else
                                        drCurrentRow("vcEquation") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterOperator"))
                                        drFormRow = dtFieldsList.Rows(0)
                                    End If

                                    drCurrentRow("intCompare") = CCommon.ToString(ds.Tables(2).Rows(i)("intCompare"))

                                    If drFormRow("vcAssociatedControlType") = "SelectBox" Then
                                        drCurrentRow("numID") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterValue"))
                                        drCurrentRow("VcData") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterData"))
                                    ElseIf drFormRow("vcAssociatedControlType") = "DateField" Then
                                        drCurrentRow("numID") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterValue"))
                                        drCurrentRow("VcData") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterData"))
                                    ElseIf drFormRow("vcFieldDataType") = "M" Then
                                        drCurrentRow("numID") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterValue"))
                                        drCurrentRow("VcData") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterData"))
                                        drCurrentRow("intCompare") = 1
                                    ElseIf drFormRow("vcFieldDataType") = "N" Then
                                        drCurrentRow("numID") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterValue"))
                                        drCurrentRow("VcData") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterData"))
                                    Else
                                        drCurrentRow("numID") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterValue"))
                                        drCurrentRow("VcData") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterData"))
                                    End If

                                End If

                                If dtCurrentTable.Rows.Count > 0 Then
                                    drCurrentRow("vcANDOR") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterANDOR"))
                                    drCurrentRow("vcMessge") = "<b>" & drCurrentRow("vcANDOR") & "  </b>  <b>  " & drCurrentRow("vcFieldName") & "</b>  " & drCurrentRow("vcEquation") & " <b>" & drCurrentRow("VcData") & "</b>"
                                Else
                                    drCurrentRow("vcANDOR") = Nothing
                                    drCurrentRow("vcMessge") = "When <b>" & drCurrentRow("vcFieldName") & " </b>  " & drCurrentRow("vcEquation") & " <b>" & drCurrentRow("VcData") & "</b>"
                                End If
                                strCondition = CCommon.ToString(Session("strCondition"))
                                strCondition = strCondition & "&nbsp;" & drCurrentRow("vcMessge")
                                Session("strCondition") = strCondition
                                dtCurrentTable.Rows.Add(drCurrentRow)
                                If dtCurrentTable.Rows.Count > 0 Then
                                    dvANDOR.Style.Add("display", "block")
                                Else
                                    dvANDOR.Style.Add("display", "none")
                                End If
                            Next
                            'Store the current data to ViewState for future reference
                            ViewState("CurrentCondTable") = dtCurrentTable

                            'Rebind the Grid with the current data to reflect changes
                            rprConditions.DataSource = dtCurrentTable
                            rprConditions.DataBind()
                        End If
                        If (ds.Tables(3).Rows.Count > 0) Then 'WorkFlowActionList
                            '  If ViewState("CurrentTable") Is Nothing Then
                            Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentTable"), DataTable)
                            Dim drCurrentRow As DataRow = Nothing

                            For i = 0 To ds.Tables(3).Rows.Count - 1
                                If hfWFCodeForEdit.Value = "0" Then
                                    drCurrentRow = dtCurrentTable.NewRow()
                                    drCurrentRow("RowNumber") = dtCurrentTable.Rows.Count + 1

                                Else
                                    drCurrentRow = dtCurrentTable.Rows(dtCurrentTable.Rows.IndexOf(dtCurrentTable.Select("RowNumber=" + hfWFCodeForEdit.Value)(0)))
                                    drCurrentRow("RowNumber") = Convert.ToInt32(hfWFCodeForEdit.Value)
                                End If
                                drCurrentRow("numAction") = CCommon.ToString(ds.Tables(3).Rows(i)("tintActionType"))
                                Dim numActiontype As Integer = CCommon.ToInteger(ds.Tables(3).Rows(i)("tintActionType"))
                                If numActiontype = 1 Then 'Send Alerts
                                    drCurrentRow("vcAction") = "Send Alerts"
                                    drCurrentRow("numGenericDocID") = CCommon.ToString(ds.Tables(3).Rows(i)("numTemplateID"))
                                    drCurrentRow("vcDocName") = CCommon.ToString(ds.Tables(3).Rows(i)("EmailTemplate"))
                                    drCurrentRow("vcReceiverName") = CCommon.ToString(ds.Tables(3).Rows(i)("EmailToType"))
                                    drCurrentRow("vcEmailToType") = CCommon.ToString(ds.Tables(3).Rows(i)("vcEmailToType"))
                                    drCurrentRow("tintEmailFrom") = CCommon.ToString(ds.Tables(3).Rows(i)("tintEmailFrom"))
                                    drCurrentRow("vcEmailSendTo") = CCommon.ToString(ds.Tables(3).Rows(i)("vcEmailSendTo"))
                                    drCurrentRow("vcMailBody") = CCommon.ToString(ds.Tables(3).Rows(i)("vcMailBody"))
                                    drCurrentRow("vcMailSubject") = CCommon.ToString(ds.Tables(3).Rows(i)("vcMailSubject"))

                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = CCommon.ToShort(ds.Tables(3).Rows(i)("tintIsAttachment"))
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing

                                    If CCommon.ToLong(drCurrentRow("numGenericDocID")) = 99999 Then
                                        drCurrentRow("vcDocName") = "Compose Message"
                                    End If
                                    If CCommon.ToString(Session("strCondition")) = "" Then
                                        drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; Using Email Template <b>" & drCurrentRow("vcDocName") & "</b> To <b>" & drCurrentRow("vcReceiverName") & CCommon.ToString(drCurrentRow("vcEmailSendTo")) & "</b>"
                                    Else
                                        drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "Then &nbsp;" & drCurrentRow("vcAction") & "&nbsp; Using Email Template <b>" & drCurrentRow("vcDocName") & "</b> To <b>" & drCurrentRow("vcReceiverName") & CCommon.ToString(drCurrentRow("vcEmailSendTo")) & "</b>"
                                    End If
                                    Dim vcFrom As String = ""

                                    Select Case drCurrentRow("tintEmailFrom")
                                        Case "1"
                                            vcFrom = "Owner of trigger record"
                                        Case "2"
                                            vcFrom = "Assignee of trigger record"
                                        Case Else
                                            vcFrom = "Global Messaging & Calendaring"
                                    End Select

                                    drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") &
                                    "&nbsp; Using Email Template <b>" &
                                    drCurrentRow("vcDocName") &
                                    "</b> To <b>" &
                                    drCurrentRow("vcReceiverName") &
                                    CCommon.ToString(drCurrentRow("vcEmailSendTo")) &
                                    "</b>" &
                                    " From <b>" & vcFrom & "</b>"

                                ElseIf numActiontype = 2 Then 'Assign Action Items

                                    drCurrentRow("vcAction") = "Assign Action Items"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("tintEmailFrom") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = CCommon.ToString(ds.Tables(3).Rows(i)("numTemplateID"))
                                    drCurrentRow("TemplateName") = CCommon.ToString(ds.Tables(3).Rows(i)("EmailTemplate"))
                                    drCurrentRow("numAssignTo") = CCommon.ToString(ds.Tables(3).Rows(i)("tintTicklerActionAssignedTo"))
                                    drCurrentRow("vcAssignTo") = CCommon.ToString(ds.Tables(3).Rows(i)("TicklerAssignedTo"))
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing

                                    If CCommon.ToString(Session("strCondition")) = "" Then
                                        drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; Using Action Template <b>" & drCurrentRow("TemplateName") & "</b> To <b>" & drCurrentRow("vcAssignTo") & "</b>"
                                    Else
                                        drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; Then &nbsp;" & drCurrentRow("vcAction") & "&nbsp; Using Action Template <b>" & drCurrentRow("TemplateName") & "</b> To <b>" & drCurrentRow("vcAssignTo") & "</b>"
                                    End If

                                    drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "&nbsp; Using Action Template <b>" & drCurrentRow("TemplateName") & "</b> To <b>" & drCurrentRow("vcAssignTo") & "</b>"

                                ElseIf numActiontype = "3" Then 'update fields

                                    Dim dtUpdateFields As DataTable = DirectCast(ViewState("UFCurrentTable"), DataTable)
                                    Dim drUFCurrentRow As DataRow = Nothing
                                    Dim strFields As String = Nothing
                                    If (ds.Tables(4).Rows.Count > 0) Then 'WorkFlowActionUpdateFields
                                        For f = 0 To ds.Tables(4).Rows.Count - 1


                                            If hdfUpdateFields.Value = "0" Then
                                                drUFCurrentRow = dtUpdateFields.NewRow()
                                                drUFCurrentRow("RowNumber") = dtUpdateFields.Rows.Count + 1
                                            Else
                                                drUFCurrentRow = dtCurrentTable.Rows(dtUpdateFields.Rows.IndexOf(dtCurrentTable.Select("RowNumber=" + hdfUpdateFields.Value)(0)))
                                                drUFCurrentRow("RowNumber") = Convert.ToInt32(hdfUpdateFields.Value)
                                            End If
                                            drUFCurrentRow("tintModuleType") = 0
                                            drUFCurrentRow("ID") = CCommon.ToString(ds.Tables(4).Rows(f)("FieldID"))
                                            drUFCurrentRow("vcFieldName") = CCommon.ToString(ds.Tables(4).Rows(f)("FieldName"))
                                            strFields = CCommon.ToString(ds.Tables(4).Rows(f)("FieldName")) & "," & strFields
                                            drUFCurrentRow("vcValue") = CCommon.ToString(ds.Tables(4).Rows(f)("vcData"))
                                            drUFCurrentRow("numID") = CCommon.ToString(ds.Tables(4).Rows(f)("vcValue"))
                                            'If ddlUpdateFields.SelectedIndex >= 0 Then
                                            '    Dim dtFieldsList As DataTable
                                            '    dtFieldsList = DirectCast(ViewState("UFFields"), DataTable)
                                            '    Dim drFormRow As DataRow
                                            '    Dim strDdlValue As String() = ddlUpdateFields.SelectedValue.Split("_")
                                            '    drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))
                                            '    If drFormRow("vcAssociatedControlType") = "SelectBox" Or drFormRow("vcAssociatedControlType") = "CheckBox" Then
                                            '        drUFCurrentRow("numID") = CCommon.ToString(ddlUFValue.SelectedValue)
                                            '        drUFCurrentRow("vcValue") = CCommon.ToString(ddlUFValue.SelectedItem)
                                            '    ElseIf drFormRow("vcAssociatedControlType") = "DateField" Then
                                            '        drUFCurrentRow("numID") = calUFDate.SelectedDate
                                            '        drUFCurrentRow("vcValue") = calUFDate.SelectedDate
                                            '    Else
                                            '        drUFCurrentRow("numID") = txtUFValue.Text
                                            '        drUFCurrentRow("vcValue") = txtUFValue.Text

                                            '    End If
                                            'End If
                                            drUFCurrentRow("vcMessge") = CCommon.ToString(drUFCurrentRow("vcFieldName")) & " to " & drUFCurrentRow("vcValue").ToString()
                                            dtUpdateFields.Rows.Add(drUFCurrentRow)
                                        Next
                                        'Store the current data to ViewState for future reference
                                        ViewState("UFCurrentTable") = dtUpdateFields
                                        'Rebind the Grid with the current data to reflect changes
                                        rprUpdateFields.DataSource = dtUpdateFields
                                        rprUpdateFields.DataBind()
                                        hfUFCount.Value = dtUpdateFields.Rows.Count()
                                    End If

                                    drCurrentRow("vcAction") = ""
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("tintEmailFrom") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = CCommon.ToString(strFields).Remove(strFields.Length - 1, 1)
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    'vcApproval,tintSendMail
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing
                                    If CCommon.ToString(Session("strCondition")) = "" Then
                                        drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "Updates fields: <b>" & CCommon.ToString(drCurrentRow("vcUpdateFields")) & "</b>"
                                    Else
                                        drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; Then &nbsp;" & drCurrentRow("vcAction") & "&nbsp; Updates fields: <b>" & CCommon.ToString(drCurrentRow("vcUpdateFields")) & "</b>"
                                    End If

                                    drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "Updates fields: <b>" & CCommon.ToString(drCurrentRow("vcUpdateFields")) & "</b>"

                                ElseIf numActiontype = 5 Then 'Biz Doc Approval
                                    drCurrentRow("vcAction") = "BizDoc Approval"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("tintEmailFrom") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    'vcApproval,tintSendMail
                                    drCurrentRow("vcApproval") = CCommon.ToString(ds.Tables(3).Rows(i)("vcApproval"))
                                    drCurrentRow("tintSendMail") = CCommon.ToShort(ds.Tables(3).Rows(i)("tintSendMail"))
                                    If CCommon.ToString(Session("strCondition")) = "" Then
                                        drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; by <b>" & CCommon.ToString(ds.Tables(3).Rows(i)("vcApprovalName")) & "</b> "
                                    Else
                                        drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "Then &nbsp;" & drCurrentRow("vcAction") & "&nbsp; by <b>" & CCommon.ToString(ds.Tables(3).Rows(i)("vcApprovalName")) & "</b> "
                                    End If

                                    drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "&nbsp; by <b>" & CCommon.ToString(ds.Tables(3).Rows(i)("vcApprovalName")) & "</b> "

                                ElseIf numActiontype = 6 Then 'Create BizDoc

                                    drCurrentRow("vcAction") = "Create bizDoc"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("tintEmailFrom") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    'vcApproval,tintSendMail
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing
                                    drCurrentRow("numBizDocTypeID") = CCommon.ToLong(ds.Tables(3).Rows(i)("numBizDocTypeID"))
                                    drCurrentRow("numBizDocTemplateID") = CCommon.ToLong(ds.Tables(3).Rows(i)("numBizDocTemplateID"))
                                    drCurrentRow("vcBizDocTemplate") = CCommon.ToString(ds.Tables(3).Rows(i)("vcBizDocTemplate"))
                                    drCurrentRow("vcBizDocType") = CCommon.ToString(ds.Tables(3).Rows(i)("vcBizDocType"))
                                    drCurrentRow("vcBizDoc") = CCommon.ToString(ds.Tables(3).Rows(i)("vcBizDoc"))

                                    If CCommon.ToString(Session("strCondition")) = "" Then
                                        drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; Using Template <b>" & drCurrentRow("vcBizDocTemplate") & "</b>"
                                    Else
                                        drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; Then &nbsp;" & drCurrentRow("vcAction") & "&nbsp; Using Template <b>" & drCurrentRow("vcBizDocTemplate") & "</b>"
                                    End If

                                    drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "&nbsp; Using Template <b>" & drCurrentRow("vcBizDocTemplate") & "</b>"

                                ElseIf numActiontype = 7 Then 'Promote Organization

                                    drCurrentRow("vcAction") = "Promote Organization"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("tintEmailFrom") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    'vcApproval,tintSendMail
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing
                                    drCurrentRow("numBizDocTemplateID") = Nothing
                                    drCurrentRow("vcBizDocTemplate") = Nothing
                                    drCurrentRow("vcBizDocType") = Nothing
                                    drCurrentRow("vcBizDoc") = Nothing
                                    If CCommon.ToString(Session("strCondition")) = "" Then
                                        drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "</b>"
                                    Else
                                        drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; Then &nbsp;" & drCurrentRow("vcAction") & "</b>"
                                    End If

                                    drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "</b>"

                                ElseIf numActiontype = 8 Then 'Demote Organization

                                    drCurrentRow("vcAction") = "Demote Organization"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("tintEmailFrom") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    'vcApproval,tintSendMail
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing
                                    drCurrentRow("numBizDocTemplateID") = Nothing
                                    drCurrentRow("vcBizDocTemplate") = Nothing
                                    drCurrentRow("vcBizDocType") = Nothing
                                    drCurrentRow("vcBizDoc") = Nothing
                                    drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; Then &nbsp;" & drCurrentRow("vcAction") & "</b>"

                                    drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "</b>"

                                ElseIf numActiontype = 9 Then ''Send SMS Alerts
                                    drCurrentRow("vcAction") = "Send SMS Alerts"
                                    drCurrentRow("numGenericDocID") = CCommon.ToString(ds.Tables(3).Rows(i)("numTemplateID"))
                                    drCurrentRow("vcDocName") = CCommon.ToString(ds.Tables(3).Rows(i)("EmailTemplate"))
                                    drCurrentRow("vcReceiverName") = CCommon.ToString(ds.Tables(3).Rows(i)("EmailToType"))
                                    drCurrentRow("vcEmailToType") = CCommon.ToString(ds.Tables(3).Rows(i)("vcEmailToType"))
                                    drCurrentRow("tintEmailFrom") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = CCommon.ToShort(ds.Tables(3).Rows(i)("tintIsAttachment"))
                                    'vcApproval,tintSendMail
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("vcSMSText") = CCommon.ToString(ds.Tables(3).Rows(i)("vcSMSText"))
                                    drCurrentRow("tintSendMail") = Nothing
                                    drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "Then &nbsp;" & drCurrentRow("vcAction") & "&nbsp; <b>" & drCurrentRow("vcSMSText") & "</b> To <b>" & drCurrentRow("vcReceiverName") & "</b>"

                                    drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "&nbsp; <b>" & drCurrentRow("vcSMSText") & "</b> To <b>" & drCurrentRow("vcReceiverName") & "</b>"

                                Else
                                    drCurrentRow("vcAction") = "Display Alert Message"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("tintEmailFrom") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = CCommon.ToString(ds.Tables(3).Rows(i)("vcAlertMessage"))
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    'vcApproval,tintSendMail
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing
                                    drCurrentRow("vcMessge") = CCommon.ToString(Session("strCondition")) & "&nbsp; Then &nbsp;" & drCurrentRow("vcAction") & "&nbsp; <b>" & drCurrentRow("vcAlertMessage") & "</b> "

                                    drCurrentRow("vcIndividualAction") = drCurrentRow("vcAction") & "&nbsp; <b>" & drCurrentRow("vcAlertMessage") & "</b> "
                                End If

                                dtCurrentTable.Rows.Add(drCurrentRow)
                            Next

                            If lngWFID > 0 Then 'In Edit mode-maintain Rule Details
                                Session("vcWFAction") = CCommon.ToString(drCurrentRow("vcMessge"))
                                objWFManage.vcWFAction = CCommon.ToString(Session("vcWFAction")) & "..."
                            End If

                            ViewState("CurrentTable") = dtCurrentTable
                            'Rebind the Grid with the current data to reflect changes
                            rprActions1.DataSource = dtCurrentTable
                            rprActions1.DataBind()
                            hfActionCount.Value = dtCurrentTable.Rows.Count

                        End If

                    End If

                    Dim objWF As New WorkFlowObject
                    If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        objWF = objWFManage.GetDatasetToWFObject(ds)
                    End If
                End If

            Catch ex As Exception
                Throw
            End Try
        End Sub

        Protected Sub btnRefresh_OnClick(sender As Object, e As EventArgs)
            Try
                SaveWFA(lngWFID)
                ClearControls()

                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Reload", "window.opener.location=window.opener.location;close();self.close();", True)
            Catch ex As Exception
                If ex.Message.Contains("MASS SALES FULLFILLMENT RULE EXISTS") Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "AutomationRuleError", "alert('You can not set this actions until you deselect Create Invoice and Create Packing Slip rule from Administration | Mass Sales Fulfillment Rules | Sales Order Document Automation.');", True)
                ElseIf ex.Message.Contains("DUPLICATE_WORKFLOW") Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "DuplicateAutomationRule", "alert('A workflow rule that uses this module and these conditions already exists');", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Protected Sub btnCancel_OnClick(sender As Object, e As EventArgs)
            Try

                ClearControls()

                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "clientScript", "self.close();", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Protected Sub ClearControls()
            ddlForm.Enabled = True
            ddlForm.SelectedIndex = 0
            SetInitialRow()
            SetInitialRowCondition()
            SetInitialRowUF()
            hdfConditions.Value = "0"
            hdfPickedFields.Value = "0"
            hdfUpdateFields.Value = "0"
            hfWFCodeForEdit.Value = "0"
            rprActions1.DataSource = Nothing
            rprActions1.DataBind()
            rprConditions.DataSource = Nothing
            rprConditions.DataBind()
            rprUpdateFields.DataSource = Nothing
            rprUpdateFields.DataBind()

            txtDescription.Text = ""
            txtWFName.Text = ""
            chkStatus.Checked = False
            rdbCreate.Checked = False
            lblMessage.Text = ""
            dvError.Style.Add("display", "none")
            dvSuccess.Style.Add("display", "none")

            rdbFieldsUpdate.Checked = False
            ddlAction.Enabled = True
            ddlAction.SelectedIndex = 0
            ddlCondition.Enabled = True
            ddlAction_OnSelectedIndexChanged(ddlAction, New EventArgs())
        End Sub

        Private Sub AddDateFieldCondition(ByVal numFieldID As String, ByVal vcFieldName As String, ByVal itemText As String, ByVal itemValue As Long)
            Try
                If ViewState("CurrentCondTable") IsNot Nothing Then
                    Dim isNewCondition As Boolean = True

                    Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentCondTable"), DataTable)
                    Dim drCurrentRow As DataRow = Nothing

                    If hdfConditions.Value = "0" And dtCurrentTable.Select("ID='" & numFieldID & "'").Length = 0 Then
                        drCurrentRow = dtCurrentTable.NewRow()
                        drCurrentRow("RowNumber") = dtCurrentTable.Rows.Count + 1

                        If dtCurrentTable.Rows.Count > 0 Then
                            For i = 0 To dtCurrentTable.Rows.Count - 1
                                If (ddlCondition.SelectedValue = dtCurrentTable.Rows(i)("ID").ToString) Then
                                    lblConditionMessage.Text = "This Condition  already exists,Please choose another Condition or update existing one"
                                    lblConditionMessage.ForeColor = Color.Red
                                    Return
                                Else

                                    lblConditionMessage.Text = ""
                                End If

                            Next
                        End If
                        isNewCondition = True
                    Else
                        drCurrentRow = dtCurrentTable.Rows(dtCurrentTable.Rows.IndexOf(dtCurrentTable.Select("RowNumber=" + hdfConditions.Value)(0)))
                        drCurrentRow("RowNumber") = Convert.ToInt32(hdfConditions.Value)
                        isNewCondition = False
                    End If

                    'If  Then
                    '    drCurrentRow = dtCurrentTable.NewRow()
                    '    drCurrentRow("RowNumber") = dtCurrentTable.Rows.Count + 1
                    '    isNewCondition = True
                    'Else
                    '    isNewCondition = False
                    '    drCurrentRow = dtCurrentTable.Select("ID='" & numFieldID & "'")(0)
                    'End If

                    'RowNumber, vcANDOR, ID, vcFieldName, numEquation, vcEquation, numID, VcData, vcMessge
                    Dim sbValue As New StringBuilder()
                    sbValue.Append(itemValue)
                    Dim sbValueText As New StringBuilder()
                    sbValueText.Append(itemText)

                    drCurrentRow("ID") = numFieldID
                    drCurrentRow("vcFieldName") = vcFieldName
                    drCurrentRow("numEquation") = ddlBefore.SelectedItem.Text
                    drCurrentRow("vcEquation") = "is"
                    drCurrentRow("numID") = sbValue.ToString()
                    drCurrentRow("VcData") = "'" & txtDays.Text & "'" & " days " & ddlBefore.SelectedItem.Text & " '" & "Today" & "'"
                    drCurrentRow("intCompare") = 0

                    If isNewCondition AndAlso dtCurrentTable.Rows.Count > 0 Then
                        drCurrentRow("vcANDOR") = ddlANDORCondition.SelectedValue
                    Else
                        drCurrentRow("vcANDOR") = Nothing
                    End If

                    'If isNewCondition Then
                    '    dtCurrentTable.Rows.Add(drCurrentRow)
                    'Else
                    '    dtCurrentTable.AcceptChanges()
                    'End If

                    If hdfConditions.Value = "0" Then
                        dtCurrentTable.Rows.Add(drCurrentRow)
                    Else
                        dtCurrentTable.AcceptChanges()
                        hdfConditions.Value = "0"
                    End If

                    If Not dtCurrentTable Is Nothing AndAlso dtCurrentTable.Rows.Count > 0 Then
                        Dim vcMessage As String = ""

                        For Each dr As DataRow In dtCurrentTable.Rows
                            If String.IsNullOrEmpty(vcMessage) Then
                                vcMessage = "When <b>" & dr("vcFieldName") & " </b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                dr("vcMessge") = "When <b>" & dr("vcFieldName") & " </b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                            Else
                                vcMessage = vcMessage & " " & "<b>" & dr("vcANDOR") & "  </b>  <b>  " & dr("vcFieldName") & "</b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                dr("vcMessge") = "<b>" & dr("vcANDOR") & "  </b>  <b>  " & dr("vcFieldName") & "</b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                            End If
                        Next

                        dtCurrentTable.AcceptChanges()
                        Session("strCondition") = vcMessage
                    Else
                        Session("strCondition") = ""
                    End If

                    'Store the current data to ViewState for future reference
                    ViewState("CurrentCondTable") = dtCurrentTable

                    'Rebind the Grid with the current data to reflect changes
                    rprConditions.DataSource = dtCurrentTable
                    rprConditions.DataBind()

                Else
                    Response.Write("ViewState is null")
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Protected Sub btnClosePickFields_OnClick(sender As Object, e As EventArgs) Handles btnClosePickFields.Click
            Try
                dvPickfields.Style.Add("display", "none")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub btnCancelDate_OnClick(sender As Object, e As EventArgs) Handles btnCancelDate.Click
            Try
                dvDatefieldCondition.Style.Add("display", "none")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub
        Protected Sub btnCloseDate_OnClick(sender As Object, e As EventArgs) Handles btnCloseDate.Click
            Try
                dvDatefieldCondition.Style.Add("display", "none")
                If Not ddldateField.SelectedItem Is Nothing Then
                    lblDateRange.Text = "When " & ddldateField.SelectedItem.Text & " is " & txtDays.Text & " days " & ddlBefore.SelectedItem.Text & " today."
                    AddDateFieldCondition(ddldateField.SelectedValue, ddldateField.SelectedItem.Text, ddldateField.SelectedItem.Text, txtDays.Text)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Protected Sub rdbDateFieldCond_CheckedChanged(sender As Object, e As EventArgs)
            Try
                dvDatefieldCondition.Style.Add("display", "block")

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Protected Sub btnCloseUpdateFields_OnClick(sender As Object, e As EventArgs) Handles btnCloseUpdateFields.Click
            Try
                If ddlUpdateFields.Items.Count > 0 Then
                    ddlUpdateFields.SelectedIndex = 0
                End If

                rprActions1.DataSource = DirectCast(ViewState("CurrentTable"), DataTable)
                rprActions1.DataBind()

                divUpdateFields.Style.Add("display", "none")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub

        Protected Sub selectbtn_OnClick(sender As Object, e As EventArgs) Handles selectbtn.Click
            Try
                divUpdateFields.Style.Add("display", "block")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub AddDefaultFilterCondition(ByVal isDelete As Boolean, ByVal numFieldID As String, ByVal isCustomField As Boolean, ByVal vcFieldName As String, ByVal itemText As String, ByVal itemValue As Long)
            Try
                If isDelete Then
                    If Not ViewState("CurrentCondTable") Is Nothing Then
                        Dim dtConditions As DataTable = DirectCast(ViewState("CurrentCondTable"), DataTable)

                        If Not dtConditions Is Nothing AndAlso dtConditions.Select("ID='" & numFieldID & "'").Length > 0 Then
                            dtConditions.Rows.Remove(dtConditions.Select("ID='" & numFieldID & "'")(0))

                            If Not dtConditions Is Nothing AndAlso dtConditions.Rows.Count > 0 Then
                                Dim vcMessage As String = ""

                                For Each dr As DataRow In dtConditions.Rows
                                    If String.IsNullOrEmpty(vcMessage) Then
                                        vcMessage = "When <b>" & dr("vcFieldName") & " </b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                        dr("vcMessge") = "When <b>" & dr("vcFieldName") & " </b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                    Else
                                        vcMessage = vcMessage & " " & "<b>" & dr("vcANDOR") & "  </b>  <b>  " & dr("vcFieldName") & "</b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                        dr("vcMessge") = "<b>" & dr("vcANDOR") & "  </b>  <b>  " & dr("vcFieldName") & "</b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                    End If
                                Next
                                Session("strCondition") = vcMessage
                            Else
                                Session("strCondition") = ""
                            End If

                            dtConditions.AcceptChanges()
                            'Store the current data to ViewState for future reference
                            ViewState("CurrentCondTable") = dtConditions

                            'Rebind the Grid with the current data to reflect changes
                            rprConditions.DataSource = dtConditions
                            rprConditions.DataBind()
                        End If
                    End If
                Else
                    AddCondition(numFieldID, isCustomField, vcFieldName, itemText, itemValue)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub AddCondition(ByVal numFieldID As String, ByVal isCustomField As Boolean, ByVal vcFieldName As String, ByVal itemText As String, ByVal itemValue As Long)
            Try
                If ViewState("CurrentCondTable") IsNot Nothing Then
                    Dim isNewCondition As Boolean = True

                    Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentCondTable"), DataTable)
                    Dim drCurrentRow As DataRow = Nothing

                    If dtCurrentTable.Select("ID='" & numFieldID & "'").Length = 0 Then
                        drCurrentRow = dtCurrentTable.NewRow()
                        drCurrentRow("RowNumber") = dtCurrentTable.Rows.Count + 1
                        isNewCondition = True
                    Else
                        isNewCondition = False
                        drCurrentRow = dtCurrentTable.Select("ID='" & numFieldID & "'")(0)
                    End If

                    'RowNumber, vcANDOR, ID, vcFieldName, numEquation, vcEquation, numID, VcData, vcMessge
                    Dim sbValue As New StringBuilder()
                    sbValue.Append(itemValue)
                    Dim sbValueText As New StringBuilder()
                    sbValueText.Append(itemText)

                    drCurrentRow("ID") = numFieldID
                    drCurrentRow("vcFieldName") = vcFieldName
                    drCurrentRow("numEquation") = "eq"
                    drCurrentRow("vcEquation") = "equals"
                    drCurrentRow("numID") = sbValue.ToString()
                    drCurrentRow("VcData") = sbValueText.ToString()
                    drCurrentRow("intCompare") = 0

                    If isNewCondition AndAlso dtCurrentTable.Rows.Count > 0 Then
                        drCurrentRow("vcANDOR") = ddlANDORCondition.SelectedValue
                    Else
                        drCurrentRow("vcANDOR") = Nothing
                    End If

                    If isNewCondition Then
                        dtCurrentTable.Rows.Add(drCurrentRow)
                    Else
                        dtCurrentTable.AcceptChanges()
                    End If

                    If Not dtCurrentTable Is Nothing AndAlso dtCurrentTable.Rows.Count > 0 Then
                        Dim vcMessage As String = ""

                        For Each dr As DataRow In dtCurrentTable.Rows
                            If String.IsNullOrEmpty(vcMessage) Then
                                vcMessage = "When <b>" & dr("vcFieldName") & " </b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                dr("vcMessge") = "When <b>" & dr("vcFieldName") & " </b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                            Else
                                vcMessage = vcMessage & " " & "<b>" & dr("vcANDOR") & "  </b>  <b>  " & dr("vcFieldName") & "</b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                dr("vcMessge") = "<b>" & dr("vcANDOR") & "  </b>  <b>  " & dr("vcFieldName") & "</b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                            End If
                        Next

                        dtCurrentTable.AcceptChanges()
                        Session("strCondition") = vcMessage
                    Else
                        Session("strCondition") = ""
                    End If

                    'Store the current data to ViewState for future reference
                    ViewState("CurrentCondTable") = dtCurrentTable

                    'Rebind the Grid with the current data to reflect changes
                    rprConditions.DataSource = dtCurrentTable
                    rprConditions.DataBind()
                Else
                    Response.Write("ViewState is null")
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region
#Region "Update Fields"
#Region "Repeater"
        Private Sub SetInitialRowUF()
            Try
                Dim dtUpdateFields As New DataTable()
                Dim dr As DataRow = Nothing
                CCommon.AddColumnsToDataTable(dtUpdateFields, "RowNumber,tintModuleType,ID,vcFieldName,numID,vcValue,vcMessge")
                ViewState("UFCurrentTable") = dtUpdateFields
            Catch ex As Exception
                Throw
            End Try

        End Sub
        Private Sub AddNewRowToGridUF()
            Try
                If ViewState("UFCurrentTable") IsNot Nothing Then

                    Dim dtCurrentTable As DataTable = DirectCast(ViewState("UFCurrentTable"), DataTable)
                    If dtCurrentTable.Select("ID='" & ddlUpdateFields.SelectedValue & "'").Length = 0 Then
                        Dim drCurrentRow As DataRow = Nothing

                        'If dtCurrentTable.Rows.Count > 0 Then

                        If hdfUpdateFields.Value = "0" Then
                            drCurrentRow = dtCurrentTable.NewRow()
                            drCurrentRow("RowNumber") = dtCurrentTable.Rows.Count + 1
                        Else
                            drCurrentRow = dtCurrentTable.Rows(dtCurrentTable.Rows.IndexOf(dtCurrentTable.Select("RowNumber=" + hdfUpdateFields.Value)(0)))
                            drCurrentRow("RowNumber") = Convert.ToInt32(hdfUpdateFields.Value)
                        End If

                        'RowNumber,tintModuleType,ID,vcFieldName,vcValue,vcMessge

                        drCurrentRow("tintModuleType") = 0
                        drCurrentRow("ID") = ddlUpdateFields.SelectedValue
                        drCurrentRow("vcFieldName") = ddlUpdateFields.SelectedItem


                        If ddlUpdateFields.SelectedIndex >= 0 Then
                            Dim sbValue As New StringBuilder()
                            Dim sbValueText As New StringBuilder()


                            Dim dtFieldsList As DataTable
                            dtFieldsList = DirectCast(ViewState("UFFields"), DataTable)
                            Dim drFormRow As DataRow
                            Dim strDdlValue As String() = ddlUpdateFields.SelectedValue.Split("_")
                            drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))

                            If drFormRow("vcAssociatedControlType") = "SelectBox" Or drFormRow("vcAssociatedControlType") = "CheckBox" Then
                                drCurrentRow("numID") = CCommon.ToString(ddlUFValue.SelectedValue)
                                drCurrentRow("vcValue") = CCommon.ToString(ddlUFValue.SelectedItem)
                            ElseIf drFormRow("vcAssociatedControlType") = "CheckBoxList" Then

                                For Each item As ListItem In chklstUFValue.Items
                                    If (item.Selected = True) Then
                                        sbValue.Append(item.Value & ",")
                                        sbValueText.Append(item.Text & ",")
                                    End If

                                Next
                                drCurrentRow("numID") = If((sbValue.ToString = ""), "", sbValue.ToString().Remove(sbValue.ToString.Length - 1, 1))
                                drCurrentRow("vcValue") = If((sbValueText.ToString = ""), "", sbValueText.ToString().Remove(sbValueText.ToString.Length - 1, 1))

                            ElseIf drFormRow("vcAssociatedControlType") = "DateField" Then
                                drCurrentRow("numID") = calUFDate.SelectedDate
                                drCurrentRow("vcValue") = calUFDate.SelectedDate
                            Else
                                drCurrentRow("numID") = txtUFValue.Text
                                drCurrentRow("vcValue") = txtUFValue.Text

                            End If
                        End If
                        drCurrentRow("vcMessge") = CCommon.ToString(drCurrentRow("vcFieldName")) & " to " & drCurrentRow("vcValue").ToString()
                        'drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; Using Email Template <b>" & drCurrentRow("vcDocName") & "</b> To <b>" & drCurrentRow("vcReceiverName") & "</b>"
                        If hdfUpdateFields.Value = "0" Then
                            dtCurrentTable.Rows.Add(drCurrentRow)
                        Else
                            dtCurrentTable.AcceptChanges()
                            hdfUpdateFields.Value = "0"
                        End If

                        'Store the current data to ViewState for future reference
                        ViewState("UFCurrentTable") = dtCurrentTable

                        'Rebind the Grid with the current data to reflect changes
                        rprUpdateFields.DataSource = dtCurrentTable
                        rprUpdateFields.DataBind()
                        hfUFCount.Value = dtCurrentTable.Rows.Count()
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UpdateFieldAlreadyAdded", "alert('Field selected for update is already added.');", True)
                    End If

                    'End If
                Else
                    Response.Write("ViewState is null")
                End If
                'Set Previous Data on Postbacks
                'SetPreviousData()
            Catch ex As Exception
                Throw
            End Try

        End Sub

        Protected Sub rprUpdateFields_ItemCommand(src As Object, e As RepeaterCommandEventArgs)
            Try
                'ss//ClearSampleControls();
                Dim dtUpdateFields As DataTable
                If e.CommandName = "Edit" Then
                    'add Sample
                    divUpdateFields.Style.Add("display", "block")
                    dtUpdateFields = DirectCast(ViewState("UFCurrentTable"), DataTable)

                    Dim drCurrentRow As DataRow = dtUpdateFields.Rows(dtUpdateFields.Rows.IndexOf(dtUpdateFields.Select("RowNumber=" + e.CommandArgument)(0)))
                    hdfUpdateFields.Value = drCurrentRow("RowNumber").ToString()
                    ddlUpdateFields.SelectedValue = CCommon.ToString(drCurrentRow("ID"))
                    ddlUpdateFields_OnSelectedIndexChanged(ddlUpdateFields, New EventArgs())
                    Dim dtFieldsList As DataTable
                    dtFieldsList = DirectCast(ViewState("UFFields"), DataTable)
                    Dim drFormRow As DataRow
                    Dim strDdlValue As String() = ddlUpdateFields.SelectedValue.Split("_")

                    drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))
                    If drFormRow("vcAssociatedControlType") = "SelectBox" Or drFormRow("vcAssociatedControlType") = "CheckBox" Then

                        ddlUFValue.SelectedValue = CCommon.ToString(drCurrentRow("numID"))
                    Else
                        txtUFValue.Text = CCommon.ToString(drCurrentRow("vcValue"))

                    End If
                ElseIf e.CommandName = "Delete" Then
                    'delete selected row

                    dtUpdateFields = DirectCast(ViewState("UFCurrentTable"), DataTable)

                    dtUpdateFields.Rows.RemoveAt(dtUpdateFields.Rows.IndexOf(dtUpdateFields.Select("RowNumber=" + e.CommandArgument)(0)))
                    dtUpdateFields.AcceptChanges()

                    ViewState("UFCurrentTable") = dtUpdateFields

                    rprUpdateFields.DataSource = dtUpdateFields
                    rprUpdateFields.DataBind()
                    'ltrTempEntry.Text = "Record is deleted."
                    ' hfLabourCode.Value = (Convert.ToInt32(hfLabourCode.Value) - 1).ToString()
                    'do nothing
                    SetModuleVisibility()
                Else

                End If

                dvUpdateFields.Style.Add("display", "")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub
#End Region
#Region "DropDowns"
        Public Sub GetUpdateFieldList(ByVal numFormID As Long)
            Try
                Dim objWF As New BACRM.BusinessLogic.Workflow.Workflow

                objWF.DomainID = Session("DomainID")
                If numFormID = 71 Then 'BizDocs
                    objWF.FormID = 49
                Else
                    objWF.FormID = numFormID
                End If

                Dim dtFieldsList As DataTable
                dtFieldsList = objWF.GetWorkFlowFormFieldMaster()
                Dim dv As DataView = dtFieldsList.DefaultView
                dv.RowFilter = "bitAllowEdit='True'"
                ddlUpdateFields.Items.Clear()
                ddlUpdateFields.DataSource = dv
                ddlUpdateFields.DataTextField = "vcFieldName"
                ddlUpdateFields.DataValueField = "ID"
                ddlUpdateFields.DataBind()
                Dim dtUFFields As DataTable = dv.ToTable
                ViewState("UFFields") = dtUFFields

                'Grouping DDL
                For i As Integer = 0 To ddlUpdateFields.Items.Count - 1
                    ddlUpdateFields.Items(i).Attributes("OptionGroup") = CStr(dtUFFields.Rows(i).Item("vcGroup"))
                Next
                'end of Grouping DDL

                ddlUpdateFields.Items.Insert("0", New ListItem("--Select--", 0))
                ddlUFValue.Items.Clear()
                ddlUFValue.Items.Insert("0", New ListItem("--Select--", 0))

            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Sub GetDropDownData(ByVal numListID As Long, ByVal vcListItemType As String, vcDbColumnName As String, vcAssociatedControlType As String)
            Try
                Dim strData As String = ""
                Dim dtData As New DataTable
                Dim objCommon As New CCommon
                objCommon.DomainID = Session("DomainID")

                If vcAssociatedControlType = "CheckBox" Then
                    dtData = New DataTable
                    dtData.Columns.Add("numID")
                    dtData.Columns.Add("vcData")
                    Dim dr1 As DataRow
                    dr1 = dtData.NewRow
                    dr1("numID") = "1"
                    dr1("vcData") = "Yes"
                    dtData.Rows.Add(dr1)

                    dr1 = dtData.NewRow
                    dr1("numID") = "0"
                    dr1("vcData") = "No"
                    dtData.Rows.Add(dr1)
                ElseIf vcDbColumnName = "tintSource" Then
                    dtData = objCommon.GetOpportunitySource()
                    dtData.Columns(0).ColumnName = "numID"
                ElseIf vcDbColumnName = "numProjectID" Then
                    Dim objProject As New Project
                    objProject.DomainID = Session("DomainID")
                    dtData = objProject.GetOpenProject()

                    dtData.Columns(0).ColumnName = "numID"
                    dtData.Columns(1).ColumnName = "vcData"

                    Dim dr1 As DataRow
                    dr1 = dtData.NewRow
                    dr1("numID") = "0"
                    dr1("vcData") = "--None--"
                    dtData.Rows.Add(dr1)
                Else
                    dtData = objCommon.GetDropDownValue(numListID, vcListItemType, vcDbColumnName)
                End If
                ddlUFValue.DataSource = dtData
                ddlUFValue.DataValueField = "numID"
                ddlUFValue.DataTextField = "vcData"
                ddlUFValue.DataBind()
                ddlUFValue.Items.Insert("0", New ListItem("--Select--", 0))
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Protected Sub ddlUpdateFields_OnSelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                If ddlUpdateFields.SelectedIndex > 0 Then

                    Dim dtFieldsList As DataTable
                    dtFieldsList = DirectCast(ViewState("UFFields"), DataTable)
                    Dim drFormRow As DataRow
                    Dim strDdlValue As String() = ddlUpdateFields.SelectedValue.Split("_")
                    drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))
                    If drFormRow("vcAssociatedControlType") = "SelectBox" Or drFormRow("vcAssociatedControlType") = "CheckBox" Then
                        ddlUFValue.Visible = True
                        calUFDate.Visible = False
                        txtUFValue.Visible = False
                        chklstUFValue.Visible = False
                        divChkLst.Style.Add("display", "none")
                        If CCommon.ToInteger(ddlForm.SelectedValue) = 94 Then 'Sales/Purchase/Process
                            GetDropDownData(CCommon.ToInteger(ddlProcessName.SelectedValue), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))
                        ElseIf drFormRow("vcDbColumnName").ToString = "vcSignatureType" Then
                            ddlUFValue.Items.Clear()
                            ddlUFValue.Items.Add(New ListItem("-- All --", "-1"))
                            ddlUFValue.Items.Add(New ListItem("Service Default", "0"))
                            ddlUFValue.Items.Add(New ListItem("Adult Signature Required", "1"))
                            ddlUFValue.Items.Add(New ListItem("Direct Signature", "2"))
                            ddlUFValue.Items.Add(New ListItem("InDirect Signature", "3"))
                            ddlUFValue.Items.Add(New ListItem("No Signature Required", "4"))
                        Else
                            GetDropDownData(drFormRow("numListID"), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))
                        End If
                    ElseIf drFormRow("vcAssociatedControlType") = "CheckBoxList" Then
                        ddlUFValue.Visible = False
                        calUFDate.Visible = False
                        txtUFValue.Visible = False
                        divChkLst.Style.Add("display", "")
                        chklstUFValue.Visible = True
                        Dim dtData As DataTable
                        dtData = objCommon.GetMasterListItems(drFormRow("numListID"), Session("DomainID"))
                        chklstUFValue.DataSource = dtData
                        chklstUFValue.DataTextField = "vcData"
                        chklstUFValue.DataValueField = "numListItemID"
                        chklstUFValue.DataBind()
                    ElseIf drFormRow("vcAssociatedControlType") = "DateField" Then
                        ddlUFValue.Visible = False
                        calUFDate.Visible = True
                        txtUFValue.Visible = False
                        divChkLst.Style.Add("display", "none")
                        chklstUFValue.Visible = False
                    Else
                        ddlUFValue.Visible = False
                        calUFDate.Visible = False
                        txtUFValue.Visible = True
                        divChkLst.Style.Add("display", "none")
                        chklstUFValue.Visible = False
                    End If
                Else

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try


        End Sub
#End Region
#Region "Button:Add"

        Protected Sub btnAddUpdateFields_OnClick(sender As Object, e As EventArgs)
            Try
                If ddlUpdateFields.Items.Count > 0 Then
                    AddNewRowToGridUF()
                End If

                divUpdateFields.Style.Add("display", "")

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
#End Region
#End Region
#Region "Pick Fields"
#Region "DropDowns"
        Sub GetWFFormFieldList(ByVal numFormID As Long)
            Try
                Dim objWF As New BACRM.BusinessLogic.Workflow.Workflow
                objWF.DomainID = Session("DomainID")
                If numFormID = 71 Then
                    objWF.FormID = 49
                Else
                    objWF.FormID = numFormID
                End If

                Dim dtFieldsList As DataTable
                dtFieldsList = objWF.GetWorkFlowFormFieldMaster()

                Dim dv As DataView = dtFieldsList.DefaultView

                If numFormID = 68 Then 'Organization
                    dv.RowFilter = "(bitCustom='False' AND vcLookBackTableName='DivisionMaster') OR (bitCustom='True' AND vcLookBackTableName='CFW_FLD_Values')"
                ElseIf numFormID = 69 Then
                    dv.RowFilter = "(bitCustom='False' AND vcLookBackTableName='AdditionalContactsInformation') OR (bitCustom='True' AND vcLookBackTableName='CFW_FLD_Values_Cont')" '

                ElseIf numFormID = 70 Then 'OpportunityMaster
                    dv.RowFilter = "(bitCustom='False' AND vcLookBackTableName='OpportunityMaster') OR (bitCustom='True' AND vcLookBackTableName='CFW_Fld_Values_Opp') "
                ElseIf numFormID = 71 Then 'BizDocs
                    dv.RowFilter = "bitCustom='False' AND vcLookBackTableName='OpportunityBizDocs'"
                ElseIf numFormID = 73 Then 'Projects
                    dv.RowFilter = "(bitCustom='False' AND vcLookBackTableName='ProjectsMaster') OR (bitCustom='True' AND vcLookBackTableName='CFW_FLD_Values_Pro')"
                ElseIf numFormID = 72 Then 'Cases
                    dv.RowFilter = "(bitCustom='False' AND vcLookBackTableName='Cases') OR (bitCustom='True' AND vcLookBackTableName='CFW_FLD_Values_Case')"
                ElseIf numFormID = 124 Then 'Action items
                    dv.RowFilter = "bitCustom='False' AND vcLookBackTableName='Communication'"
                Else
                    dv.RowFilter = " bitCustom='False' AND  bitAllowFiltering='True'"
                End If
                dv.Sort = "vcFieldName ASC"
                'dv.RowFilter = " bitCustom='False' AND bitAllowFiltering='True' "
                radFields.DataSource = dv
                radFields.DataTextField = "vcFieldName"
                radFields.DataValueField = "ID"
                radFields.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
#End Region
#Region "Buttons"
        Protected Sub btnSelect_OnClick(sender As Object, e As EventArgs)
            Try
                dvPickfields.Style.Add("display", "block")
                Dim sbValue As New StringBuilder()
                Dim sbValueText As New StringBuilder()
                Dim collection As IList(Of RadListBoxItem) = radFields.CheckedItems
                For Each item As RadListBoxItem In collection
                    sbValueText.Append(item.Text & ",")
                    sbValue.Append(item.Value & ",")
                Next
                hdfPickedFields.Value = CCommon.ToString(sbValue.ToString)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
#End Region

#End Region
#Region "Set Conditions"
#Region "DropDowns"
        Sub BindConditionsDdl()
            Try
                Dim arr As New ArrayList()

                arr.Add(New ListItem("AND", "AND"))
                arr.Add(New ListItem("OR", "OR"))

                ddlANDORCondition.Items.Clear()
                For Each item As ListItem In arr
                    ddlANDORCondition.Items.Add(item)
                Next
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Public Sub GetDropDownDataCond(ByVal numListID As Long, ByVal vcListItemType As String, vcDbColumnName As String, vcAssociatedControlType As String)
            Try
                Dim strData As String = ""
                Dim dtData As New DataTable
                Dim objCommon As New CCommon
                objCommon.DomainID = Session("DomainID")

                If vcAssociatedControlType = "CheckBox" Then
                    dtData = New DataTable
                    dtData.Columns.Add("numID")
                    dtData.Columns.Add("vcData")
                    Dim dr1 As DataRow
                    dr1 = dtData.NewRow
                    dr1("numID") = "1"
                    dr1("vcData") = "Yes"
                    dtData.Rows.Add(dr1)

                    dr1 = dtData.NewRow
                    dr1("numID") = "0"
                    dr1("vcData") = "No"
                    dtData.Rows.Add(dr1)
                ElseIf vcDbColumnName = "tintSource" Then
                    dtData = objCommon.GetOpportunitySource()
                    dtData.Columns(0).ColumnName = "numID"
                ElseIf vcDbColumnName = "numProjectID" Then
                    Dim objProject As New Project
                    objProject.DomainID = Session("DomainID")
                    dtData = objProject.GetOpenProject()

                    dtData.Columns(0).ColumnName = "numID"
                    dtData.Columns(1).ColumnName = "vcData"

                    Dim dr1 As DataRow
                    dr1 = dtData.NewRow
                    dr1("numID") = "0"
                    dr1("vcData") = "--None--"
                    dtData.Rows.Add(dr1)
                ElseIf vcDbColumnName = "numPartner" Then
                    objCommon.DomainID = Session("DomainID")
                    dtData = objCommon.GetPartnerSource()

                    dtData.Columns(0).ColumnName = "numID"
                    dtData.Columns(1).ColumnName = "vcData"
                ElseIf vcDbColumnName = "vcInventoryStatus" Then
                    dtData = New DataTable
                    dtData.Columns.Add("numID")
                    dtData.Columns.Add("vcData")

                    Dim row As DataRow = dtData.NewRow
                    row("numID") = 0
                    row("vcData") = "--None--"
                    dtData.Rows.Add(row)

                    row = dtData.NewRow
                    row("numID") = 1
                    row("vcData") = "Not Applicable"
                    dtData.Rows.Add(row)

                    row = dtData.NewRow
                    row("numID") = 2
                    row("vcData") = "Shipped"
                    dtData.Rows.Add(row)

                    row = dtData.NewRow
                    row("numID") = 3
                    row("vcData") = "Back Order"
                    dtData.Rows.Add(row)

                    row = dtData.NewRow
                    row("numID") = 4
                    row("vcData") = "Shippable"
                    dtData.Rows.Add(row)
                    dtData.AcceptChanges()
                ElseIf vcDbColumnName = "numPartenerContact" Then
                    Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentCondTable"), DataTable)

                    If dtCurrentTable.Select("ID='743_False'").Length > 0 Then '743_False
                        objCommon.DomainID = Session("DomainID")
                        objCommon.DivisionID = dtCurrentTable.Select("ID='743_False'")(0)("numID")
                        dtData = objCommon.GetPartnerContacts()

                        dtData.Columns(0).ColumnName = "numID"
                        dtData.Columns(1).ColumnName = "vcData"
                    End If
                ElseIf vcDbColumnName = "numReleaseStatus" Then
                    dtData = New DataTable
                    dtData.Columns.Add("numID")
                    dtData.Columns.Add("vcData")

                    Dim row As DataRow = dtData.NewRow
                    row("numID") = 1
                    row("vcData") = "Open"
                    dtData.Rows.Add(row)

                    row = dtData.NewRow
                    row("numID") = 2
                    row("vcData") = "Purchased"
                    dtData.Rows.Add(row)
                    dtData.AcceptChanges()
                ElseIf vcDbColumnName = "monAmountPaid" Then
                    dtData = New DataTable
                    dtData.Columns.Add("numID")
                    dtData.Columns.Add("vcData")

                    Dim row As DataRow = dtData.NewRow
                    row("numID") = 0
                    row("vcData") = "--All--"
                    dtData.Rows.Add(row)

                    row = dtData.NewRow
                    row("numID") = 1
                    row("vcData") = "Fully Paid"
                    dtData.Rows.Add(row)

                    row = dtData.NewRow
                    row("numID") = 2
                    row("vcData") = "Partially Paid"
                    dtData.Rows.Add(row)

                    row = dtData.NewRow
                    row("numID") = 3
                    row("vcData") = "Not Paid"
                    dtData.Rows.Add(row)

                    dtData.AcceptChanges()
                Else
                    dtData = objCommon.GetDropDownValue(numListID, vcListItemType, vcDbColumnName)
                End If
                radValue.DataSource = dtData
                radValue.DataValueField = "numID"
                radValue.DataTextField = "vcData"
                radValue.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindEquations()
            Try
                Dim arr As New ArrayList()
                Dim dtFieldsList As DataTable
                dtFieldsList = DirectCast(ViewState("Fields"), DataTable)
                Dim drFormRow As DataRow
                Dim strDdlValue As String() = ddlCondition.SelectedValue.Split("_")
                drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))
                If drFormRow("vcAssociatedControlType") = "SelectBox" Then

                    arr.Add(New ListItem("equals", "eq"))
                    arr.Add(New ListItem("not equal to", "ne"))

                ElseIf drFormRow("vcAssociatedControlType") = "DateField" Then
                    arr.Add(New ListItem("equals", "eq"))
                    arr.Add(New ListItem("not equal to", "ne"))
                    arr.Add(New ListItem("greater than", "gt"))
                    arr.Add(New ListItem("greater or equal", "ge"))
                    arr.Add(New ListItem("less than", "lt"))
                    arr.Add(New ListItem("less or equal", "le"))

                ElseIf drFormRow("vcFieldDataType") = "M" Then

                    Dim item1 As New ListItem("equals", "eq")
                    ' item1.Attributes("OptionGroup") = ""

                    Dim item2 As New ListItem("not equal to", "ne")
                    'item2.Attributes("OptionGroup") = ""

                    Dim item3 As New ListItem("greater than", "gt")
                    'item3.Attributes("OptionGroup") = "Compare"

                    Dim item4 As New ListItem("greater or equal", "ge")
                    'item4.Attributes("OptionGroup") = "Compare"

                    Dim item5 As New ListItem("less than", "lt")
                    'item5.Attributes("OptionGroup") = "Compare"

                    Dim item6 As New ListItem("less or equal", "le")
                    'item6.Attributes("OptionGroup") = "Compare"
                    arr.Add(item1)
                    arr.Add(item2)
                    arr.Add(item3)
                    arr.Add(item4)
                    arr.Add(item5)
                    arr.Add(item6)

                    'arr.Add(New ListItem("equals", "eq"))
                    'arr.Add(New ListItem("not equal to", "ne"))
                    'arr.Add(New ListItem("greater than", "gt"))
                    'arr.Add(New ListItem("greater or equal", "ge"))
                    'arr.Add(New ListItem("less than", "lt"))
                    'arr.Add(New ListItem("less or equal", "le"))

                    If CCommon.ToInteger(drFormRow("intWFCompare")) = 1 Then
                        'arr.Add(New ListItem("Compare: equals", "ceq"))
                        'arr.Add(New ListItem("Compare: not equal to", "cne"))
                        'arr.Add(New ListItem("Compare: greater than", "cgt"))
                        'arr.Add(New ListItem("Compare: greater or equal", "cge"))
                        'arr.Add(New ListItem("Compare: less than", "clt"))
                        'arr.Add(New ListItem("Compare: less or equal", "cle"))
                        Dim item11 As New ListItem("equals", "ceq")
                        item11.Attributes("OptionGroup") = "Compare with Field on"

                        Dim item22 As New ListItem("not equal to", "cne")
                        item22.Attributes("OptionGroup") = "Compare with Field on"

                        Dim item33 As New ListItem("greater than", "cgt")
                        item33.Attributes("OptionGroup") = "Compare with Field on"

                        Dim item44 As New ListItem("greater or equal", "cge")
                        item44.Attributes("OptionGroup") = "Compare with Field on"

                        Dim item55 As New ListItem("less than", "clt")
                        item55.Attributes("OptionGroup") = "Compare with Field on"

                        Dim item66 As New ListItem("less or equal", "cle")
                        item66.Attributes("OptionGroup") = "Compare with Field on"

                        arr.Add(item11)
                        arr.Add(item22)
                        arr.Add(item33)
                        arr.Add(item44)
                        arr.Add(item55)
                        arr.Add(item66)

                    End If

                ElseIf drFormRow("vcFieldDataType") = "N" Then
                    arr.Add(New ListItem("equals", "eq"))
                    arr.Add(New ListItem("not equal to", "ne"))
                    arr.Add(New ListItem("greater than", "gt"))
                    arr.Add(New ListItem("greater or equal", "ge"))
                    arr.Add(New ListItem("less than", "lt"))
                    arr.Add(New ListItem("less or equal", "le"))
                    '  arr.Add(New ListItem("co", "contains"))
                    ' arr.Add(New ListItem("sw", "starts with"))
                    ' arr.Add(New ListItem("nc", "does not contain"))

                Else
                    arr.Add(New ListItem("equals", "eq"))
                    arr.Add(New ListItem("not equal to", "ne"))

                End If

                ddlSign.Items.Clear()
                For Each item As ListItem In arr
                    ddlSign.Items.Add(item)
                Next
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Sub BindApprovers()
            Try
                Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
                objConfigWizard.DomainID = Session("DomainID")                                                 'set the domain id
                objConfigWizard.ListItemType = "U"                                     'set the listitem type
                Dim dtTable As DataTable                                                            'declare a datatable
                dtTable = objConfigWizard.GetMasterListByListId(0)                          'call function to fill the datatable
                radApprovalAuthority.DataSource = dtTable
                radApprovalAuthority.DataValueField = "numItemID"
                radApprovalAuthority.DataTextField = "vcItemName"
                radApprovalAuthority.DataBind()
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Sub GetWFFormFieldListCond(ByVal numFormID As Long)
            Try
                Dim objWF As New BACRM.BusinessLogic.Workflow.Workflow
                objWF.DomainID = Session("DomainID")

                If numFormID = 71 Then
                    objWF.FormID = 49
                ElseIf numFormID = 148 Then
                    objWF.FormID = 68
                Else
                    objWF.FormID = numFormID
                End If

                Dim dtFieldsList As DataTable
                dtFieldsList = objWF.GetWorkFlowFormFieldMaster()

                ddlCondition.DataSource = dtFieldsList
                ddlCondition.DataTextField = "vcFieldName"
                ddlCondition.DataValueField = "ID"
                ViewState("Fields") = dtFieldsList
                ddlCondition.DataBind()
                'Grouping DDL
                For i As Integer = 0 To ddlCondition.Items.Count - 1
                    ddlCondition.Items(i).Attributes("OptionGroup") = CStr(dtFieldsList.Rows(i).Item("vcGroup"))
                Next
                'end of Grouping DDL
                ddlCondition.Items.Insert("0", New ListItem("--Select--", 0))

                ddlSign.Items.Clear()
                ddlSign.Items.Insert("0", New ListItem("--Select--", 0))
                radValue.Items.Clear()
                radValue.Items.Insert("0", New RadListBoxItem("--Select--", 0))

                Dim dView As New DataView(dtFieldsList)

                If numFormID = 68 Then 'Organization
                    dView.RowFilter = "vcAssociatedControlType = 'DateField' AND (vcLookBackTableName='DivisionMaster_TempDateFields' OR bitCustom=1)" 'To include Custom Fields
                ElseIf numFormID = 69 Then 'Contacts
                    dView.RowFilter = "vcAssociatedControlType = 'DateField' AND (vcLookBackTableName='AdditionalContactsInformation_TempDateFields' OR bitCustom=1)" 'To include Custom Fields
                ElseIf numFormID = 70 Then 'Orders
                    dView.RowFilter = "vcAssociatedControlType = 'DateField' AND (vcLookBackTableName='OpportunityMaster_TempDateFields' OR bitCustom=1)" 'To include Custom Fields
                ElseIf numFormID = 71 Then 'BizDocs
                    dView.RowFilter = "vcAssociatedControlType = 'DateField' AND (vcLookBackTableName='OpportunityBizDocs_TempDateFields' OR bitCustom=1)" 'To include Custom Fields
                ElseIf numFormID = 72 Then 'Cases
                    dView.RowFilter = "vcAssociatedControlType = 'DateField' AND (vcLookBackTableName='Cases_TempDateFields' OR bitCustom=1)" 'To include Custom Fields
                ElseIf numFormID = 73 Then 'Projects
                    dView.RowFilter = "vcAssociatedControlType = 'DateField' AND (vcLookBackTableName='ProjectsMaster_TempDateFields' OR bitCustom=1)" 'To include Custom Fields
                ElseIf numFormID = 94 Then 'Business Process
                    dView.RowFilter = "vcAssociatedControlType = 'DateField' AND (vcLookBackTableName='StagePercentageDetails_TempDateFields' OR bitCustom=1)" 'To include Custom Fields
                ElseIf numFormID = 124 Then 'Action Items
                    dView.RowFilter = "vcAssociatedControlType = 'DateField' AND (vcLookBackTableName='Communication_TempDateFields' OR bitCustom=1)" 'To include Custom Fields
                Else ' worst case
                    dView.RowFilter = "vcAssociatedControlType = 'DateField'"
                End If
                ' dView.RowFilter = "vcAssociatedControlType = 'DateField'"
                ddldateField.DataSource = dView
                ddldateField.DataTextField = "vcFieldName"
                ddldateField.DataValueField = "ID"
                ddldateField.DataBind()

                ddldateField.Items.Insert("0", New ListItem("--Select--", 0))

                Dim dTable As DataTable
                dTable = dView.ToTable

                ViewState("CFields") = dTable

                Dim drFormRow As DataRow

                If ddlCondition.SelectedIndex > 0 Then
                    'GetDropDownData()
                    Dim strDdlValue As String() = ddlCondition.SelectedValue.Split("_")
                    drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))

                    If CCommon.ToInteger(ddlForm.SelectedValue) = 94 Then 'Sales/Purchase/Process
                        GetDropDownDataCond(CCommon.ToInteger(ddlProcessName.SelectedValue), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))
                    Else
                        GetDropDownDataCond(drFormRow("numListID"), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Protected Sub ddlCondition_OnSelectedIndexChanged(sender As Object, e As EventArgs)
            If ddlCondition.SelectedIndex > 0 Then
                BindEquations()
                Dim dtFieldsList As DataTable
                dtFieldsList = DirectCast(ViewState("Fields"), DataTable)
                Dim drFormRow As DataRow
                Dim strDdlValue As String() = ddlCondition.SelectedValue.Split("_")
                ddlCompareWith.Visible = False
                ddlSign.Visible = True
                drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))
                If drFormRow("vcAssociatedControlType") = "SelectBox" Or drFormRow("vcDbColumnName") = "vcInventoryStatus" Or drFormRow("vcDbColumnName") = "monAmountPaid" Then
                    radValue.Visible = True
                    calDate.Visible = False
                    txtNvalue.Visible = False
                    txtValueMoney.Visible = False
                    txtValue.Visible = False

                    If CCommon.ToInteger(ddlForm.SelectedValue) = 94 Then 'Sales/Purchase/Process
                        GetDropDownDataCond(CCommon.ToInteger(ddlProcessName.SelectedValue), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))
                    Else
                        GetDropDownDataCond(drFormRow("numListID"), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))
                    End If
                ElseIf drFormRow("vcAssociatedControlType") = "CheckBox" Then
                    radValue.Visible = True
                    calDate.Visible = False
                    txtNvalue.Visible = False
                    txtValueMoney.Visible = False
                    txtValue.Visible = False

                    If CCommon.ToInteger(ddlForm.SelectedValue) = 94 Then 'Sales/Purchase/Process
                        GetDropDownDataCond(CCommon.ToInteger(ddlProcessName.SelectedValue), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))
                    Else
                        GetDropDownDataCond(drFormRow("numListID"), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))
                    End If
                ElseIf drFormRow("vcAssociatedControlType") = "DateField" Then
                    radValue.Visible = False
                    txtNvalue.Visible = False
                    txtValueMoney.Visible = False
                    txtValue.Visible = False
                    calDate.Visible = False
                    ddlSign.Visible = False
                    dvDatefieldCondition.Style.Add("display", "block")
                ElseIf drFormRow("vcFieldDataType") = "M" Then
                    radValue.Visible = False
                    calDate.Visible = False
                    txtNvalue.Visible = False
                    txtValueMoney.Visible = True
                    txtValue.Visible = False
                ElseIf drFormRow("vcFieldDataType") = "N" Then
                    radValue.Visible = False
                    calDate.Visible = False
                    txtNvalue.Visible = True
                    txtValueMoney.Visible = False
                    txtValue.Visible = False
                Else
                    radValue.Visible = False
                    calDate.Visible = False
                    txtNvalue.Visible = False
                    txtValueMoney.Visible = False
                    txtValue.Visible = True
                End If
            Else
                radValue.Visible = False
                txtValue.Visible = False
                calDate.Visible = False
                txtValueMoney.Visible = False
                txtNvalue.Visible = False
                ddlCompareWith.Visible = False
            End If

        End Sub

#End Region
#Region "Repeater"
        Private Sub SetInitialRowCondition()
            Try
                Dim dtConditions As New DataTable()
                Dim dr As DataRow = Nothing
                CCommon.AddColumnsToDataTable(dtConditions, "RowNumber,vcANDOR,ID,vcFieldName,numEquation,vcEquation,numID,VcData,vcMessge,intCompare")
                ViewState("CurrentCondTable") = dtConditions

                Session("strCondition") = Nothing
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Private Sub AddNewRowToGridCondition()
            Try

                If ViewState("CurrentCondTable") IsNot Nothing Then

                    Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentCondTable"), DataTable)
                    Dim drCurrentRow As DataRow = Nothing

                    'If dtCurrentTable.Rows.Count > 0 Then

                    If hdfConditions.Value = "0" Then
                        drCurrentRow = dtCurrentTable.NewRow()
                        drCurrentRow("RowNumber") = dtCurrentTable.Rows.Count + 1

                        If dtCurrentTable.Rows.Count > 0 Then
                            For i = 0 To dtCurrentTable.Rows.Count - 1
                                If (ddlCondition.SelectedValue = dtCurrentTable.Rows(i)("ID").ToString) Then
                                    lblConditionMessage.Text = "This Condition  already exists,Please choose another Condition or update existing one"
                                    lblConditionMessage.ForeColor = Color.Red
                                    Return
                                Else

                                    lblConditionMessage.Text = ""
                                End If

                            Next
                        End If
                    Else
                        drCurrentRow = dtCurrentTable.Rows(dtCurrentTable.Rows.IndexOf(dtCurrentTable.Select("RowNumber=" + hdfConditions.Value)(0)))
                        drCurrentRow("RowNumber") = Convert.ToInt32(hdfConditions.Value)
                    End If

                    'RowNumber, vcANDOR, ID, vcFieldName, numEquation, vcEquation, numID, VcData, vcMessge
                    Dim sbValue As New StringBuilder()
                    Dim sbValueText As New StringBuilder()
                    Dim collection As IList(Of RadListBoxItem) = radValue.CheckedItems
                    For Each item As RadListBoxItem In collection
                        sbValue.Append(item.Value & ",")
                        sbValueText.Append(item.Text & ",")
                    Next

                    drCurrentRow("ID") = ddlCondition.SelectedValue
                    drCurrentRow("vcFieldName") = ddlCondition.SelectedItem
                    drCurrentRow("numEquation") = ddlSign.SelectedValue
                    drCurrentRow("vcEquation") = ddlSign.SelectedItem

                    If ddlCondition.SelectedIndex > 0 Then
                        Dim dtFieldsList As DataTable
                        dtFieldsList = DirectCast(ViewState("Fields"), DataTable)
                        Dim drFormRow As DataRow
                        Dim strDdlValue As String() = ddlCondition.SelectedValue.Split("_")
                        drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))
                        If drFormRow("vcAssociatedControlType") = "SelectBox" Or drFormRow("vcDbColumnName") = "monAmountPaid" Then
                            drCurrentRow("numID") = If((sbValue.ToString = ""), "", sbValue.ToString().Remove(sbValue.ToString.Length - 1, 1))
                            drCurrentRow("VcData") = If((sbValueText.ToString = ""), "", sbValueText.ToString().Remove(sbValueText.ToString.Length - 1, 1))
                            drCurrentRow("intCompare") = 0
                        ElseIf drFormRow("vcAssociatedControlType") = "CheckBox" Then
                            drCurrentRow("numID") = If((sbValue.ToString = ""), "", sbValue.ToString().Remove(sbValue.ToString.Length - 1, 1))
                            drCurrentRow("VcData") = If((sbValueText.ToString = ""), "", sbValueText.ToString().Remove(sbValueText.ToString.Length - 1, 1))
                            drCurrentRow("intCompare") = 0
                        ElseIf drFormRow("vcAssociatedControlType") = "DateField" Then
                            drCurrentRow("numID") = calDate.SelectedDate
                            drCurrentRow("VcData") = calDate.SelectedDate
                            drCurrentRow("intCompare") = 0
                        ElseIf drFormRow("vcFieldDataType") = "M" Then

                            If CCommon.ToInteger(drFormRow("intWFCompare")) = 1 Then 'compare Enable
                                If ddlCompareWith.Visible = True Then
                                    drCurrentRow("numID") = ddlCompareWith.SelectedValue
                                    drCurrentRow("VcData") = ddlCompareWith.SelectedItem
                                    drCurrentRow("intCompare") = 1
                                Else
                                    drCurrentRow("numID") = txtValueMoney.Text
                                    drCurrentRow("VcData") = txtValueMoney.Text
                                    drCurrentRow("intCompare") = 0
                                End If
                            Else
                                drCurrentRow("numID") = txtValueMoney.Text
                                drCurrentRow("VcData") = txtValueMoney.Text
                                drCurrentRow("intCompare") = 0

                            End If

                        ElseIf drFormRow("vcFieldDataType") = "N" Then
                            drCurrentRow("numID") = txtNvalue.Text
                            drCurrentRow("VcData") = txtNvalue.Text
                            drCurrentRow("intCompare") = 0

                        Else
                            drCurrentRow("numID") = txtValue.Text
                            drCurrentRow("VcData") = txtValue.Text
                            drCurrentRow("intCompare") = 0
                        End If
                    End If

                    If hdfConditions.Value = "0" Then
                        If dtCurrentTable.Rows.Count > 0 Then
                            drCurrentRow("vcANDOR") = ddlANDORCondition.SelectedValue
                            drCurrentRow("vcMessge") = "<b>" & drCurrentRow("vcANDOR") & "  </b>  <b>  " & drCurrentRow("vcFieldName") & "</b>  " & drCurrentRow("vcEquation") & " <b>" & drCurrentRow("VcData") & "</b>"
                        Else
                            drCurrentRow("vcANDOR") = Nothing
                            drCurrentRow("vcMessge") = "When <b>" & drCurrentRow("vcFieldName") & " </b>  " & drCurrentRow("vcEquation") & " <b>" & drCurrentRow("VcData") & "</b>"
                        End If
                    Else

                        If CCommon.ToInteger(hdfConditions.Value) <> 1 Then
                            drCurrentRow("vcANDOR") = ddlANDORCondition.SelectedValue
                            drCurrentRow("vcMessge") = "<b>" & drCurrentRow("vcANDOR") & "  </b>  <b>  " & drCurrentRow("vcFieldName") & "</b>  " & drCurrentRow("vcEquation") & " <b>" & drCurrentRow("VcData") & "</b>"
                        Else
                            drCurrentRow("vcANDOR") = Nothing
                            drCurrentRow("vcMessge") = "When <b>" & drCurrentRow("vcFieldName") & " </b>  " & drCurrentRow("vcEquation") & " <b>" & drCurrentRow("VcData") & "</b>"
                        End If
                    End If


                    strCondition = CCommon.ToString(Session("strCondition"))
                    strCondition = strCondition & "&nbsp;" & drCurrentRow("vcMessge")
                    Session("strCondition") = strCondition
                    'drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; Using Email Template <b>" & drCurrentRow("vcDocName") & "</b> To <b>" & drCurrentRow("vcReceiverName") & "</b>"
                    If hdfConditions.Value = "0" Then
                        dtCurrentTable.Rows.Add(drCurrentRow)
                    Else
                        dtCurrentTable.AcceptChanges()
                        hdfConditions.Value = "0"
                    End If

                    If dtCurrentTable.Rows.Count > 0 Then
                        dvANDOR.Style.Add("display", "block")
                    Else
                        dvANDOR.Style.Add("display", "none")
                    End If

                    'Store the current data to ViewState for future reference
                    ViewState("CurrentCondTable") = dtCurrentTable

                    'Rebind the Grid with the current data to reflect changes
                    rprConditions.DataSource = dtCurrentTable
                    rprConditions.DataBind()

                    ddlCondition.Enabled = True
                    'End If
                Else
                    Response.Write("ViewState is null")
                End If
                'Set Previous Data on Postbacks
                'SetPreviousData()
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Private Sub GenerateConditionText()
            Try
                If ViewState("CurrentCondTable") IsNot Nothing Then
                    Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentCondTable"), DataTable)

                    If Not dtCurrentTable Is Nothing AndAlso dtCurrentTable.Rows.Count > 0 Then
                        Dim vcMessage As String = ""

                        For Each dr As DataRow In dtCurrentTable.Rows
                            If String.IsNullOrEmpty(vcMessage) Then
                                vcMessage = "When <b>" & dr("vcFieldName") & " </b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                dr("vcMessage") = "When <b>" & dr("vcFieldName") & " </b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                            Else
                                vcMessage = vcMessage & "&nbsp;" & "<b>" & dr("vcANDOR") & "  </b>  <b>  " & dr("vcFieldName") & "</b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                dr("vcMessage") = "<b>" & dr("vcANDOR") & "  </b>  <b>  " & dr("vcFieldName") & "</b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                            End If
                        Next
                        dtCurrentTable.AcceptChanges()
                        Session("strCondition") = vcMessage
                    Else
                        Session("strCondition") = ""
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Protected Sub rprConditions_ItemCommand(src As Object, e As RepeaterCommandEventArgs)
            Try
                'ss//ClearSampleControls();
                Dim dtConditions As DataTable
                If e.CommandName = "Edit" Then
                    'add Sample
                    'chkCondition.Checked = True
                    dtConditions = DirectCast(ViewState("CurrentCondTable"), DataTable)
                    Dim drCurrentRow As DataRow = dtConditions.Rows(dtConditions.Rows.IndexOf(dtConditions.Select("RowNumber=" + e.CommandArgument)(0)))
                    hdfConditions.Value = drCurrentRow("RowNumber").ToString()

                    'RowNumber, vcANDOR, ID, vcFieldName, numEquation, vcEquation, numID, VcData, vcMessge

                    If (drCurrentRow("numEquation").ToString <> "Before" And drCurrentRow("numEquation").ToString <> "After") Then
                        If CCommon.ToInteger(hdfConditions.Value) <> 1 Then
                            ddlANDORCondition.SelectedValue = drCurrentRow("vcANDOR")
                        Else
                            dvANDOR.Style.Add("display", "none")
                        End If
                        ddlCondition.SelectedValue = drCurrentRow("ID")
                        ddlCondition_OnSelectedIndexChanged(ddlCondition, New EventArgs())
                        ddlCondition.Enabled = False
                        ddlSign.SelectedValue = drCurrentRow("numEquation")

                        ddlSign_OnSelectedIndexChanged(ddlSign, e)

                        Dim strValueText As String() = drCurrentRow("VcData").Split(",")
                        For k = 0 To strValueText.Length - 1
                            For Each radItem As RadListBoxItem In radValue.Items
                                If radItem.Text = strValueText(k) Then
                                    radItem.Checked = True
                                End If
                            Next
                        Next
                        txtNvalue.Text = drCurrentRow("VcData")
                        txtValue.Text = drCurrentRow("VcData")
                        txtValueMoney.Text = drCurrentRow("VcData")
                        ddlCompareWith.SelectedValue = drCurrentRow("numID")
                        calDate.SelectedDate = CCommon.GetDateFormat(drCurrentRow("VcData"))
                    Else
                        dvDatefieldCondition.Style.Add("display", "block")

                    End If
                ElseIf e.CommandName = "Delete" Then
                    'delete selected row

                    dtConditions = DirectCast(ViewState("CurrentCondTable"), DataTable)

                    dtConditions.Rows.RemoveAt(dtConditions.Rows.IndexOf(dtConditions.Select("RowNumber=" + e.CommandArgument)(0)))
                    dtConditions.AcceptChanges()
                    If dtConditions.Rows.Count > 0 Then
                        dvANDOR.Style.Add("display", "block")
                    Else
                        dvANDOR.Style.Add("display", "none")
                    End If

                    ViewState("CurrentCondTable") = dtConditions

                    rprConditions.DataSource = dtConditions
                    rprConditions.DataBind()

                    SetModuleVisibility()
                Else

                End If

            Catch ex As Exception
            End Try

        End Sub
#End Region
#Region "Buttons:Add Condition"
        Protected Sub btnAddConditions_Click(sender As Object, e As EventArgs)
            Try
                'If (chkCondition.Checked) Then
                AddNewRowToGridCondition()

                ddlForm.Enabled = False
                ddlCondition.SelectedValue = "0"
                radValue.Visible = False
                txtValue.Visible = False
                calDate.Visible = False
                txtValueMoney.Visible = False
                txtNvalue.Visible = False
                ddlCompareWith.Visible = False
                'End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
#End Region
#End Region

        Private Sub SetModuleVisibility()
            Try
                If rprActions1.Items.Count = 0 AndAlso rprConditions.Items.Count = 0 AndAlso rprUpdateFields.Items.Count = 0 Then
                    ddlForm.Enabled = True
                Else
                    ddlForm.Enabled = False
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub rdbCreate_CheckedChanged(sender As Object, e As EventArgs) Handles rdbCreate.CheckedChanged
            Try
                lblDateRange.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rdbFieldsUpdate_CheckedChanged(sender As Object, e As EventArgs) Handles rdbFieldsUpdate.CheckedChanged
            Try
                lblDateRange.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlProcessName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProcessName.SelectedIndexChanged
            Try
                ddlStage.Items.Clear()

                If CCommon.ToLong(ddlProcessName.SelectedValue) > 0 Then
                    If objAdmin Is Nothing Then objAdmin = New CAdmin
                    Dim dtTable As DataTable
                    objAdmin.Mode = 0
                    objAdmin.DomainID = Session("DomainId")
                    objAdmin.SalesProsID = ddlProcessName.SelectedValue
                    dtTable = objAdmin.StageItemDetails()

                    ddlStage.DataSource = dtTable
                    ddlStage.DataTextField = "vcStageName"
                    ddlStage.DataValueField = "numStageDetailsId"
                    ddlStage.DataBind()
                End If

                ddlStage.Items.Insert(0, New ListItem("-- Select Stage Name --", "0"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlOppOrOrderBizDocs_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlOppOrOrderBizDocs.SelectedIndexChanged
            Try
                ddlBizDocsType.Items.Clear()

                If CCommon.ToLong(ddlOppOrOrderBizDocs.SelectedValue) > 0 Then
                    objCommon.DomainID = Session("DomainID")
                    objCommon.BizDocType = If(ddlOppOrOrderBizDocs.SelectedValue = "1" Or ddlOppOrOrderBizDocs.SelectedValue = "3", 1, 2)

                    ddlBizDocsType.DataSource = objCommon.GetBizDocType
                    ddlBizDocsType.DataTextField = "vcData"
                    ddlBizDocsType.DataValueField = "numListItemID"
                    ddlBizDocsType.DataBind()
                End If

                ddlBizDocsType.Items.Insert(0, New ListItem("-- Select BizDoc Type --", "0"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlRelationship_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRelationship.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlRelationship.SelectedValue) > 0, False, True), "6_False", False, "Relationship", ddlRelationship.SelectedItem.Text, ddlRelationship.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlRelationshipType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRelationshipType.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlRelationshipType.SelectedValue) >= 0, False, True), "451_False", False, "Relationship Type", ddlRelationshipType.SelectedItem.Text, ddlRelationshipType.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlProfile_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProfile.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlProfile.SelectedValue) > 0, False, True), "5_False", False, "Organization Profile", ddlProfile.SelectedItem.Text, ddlProfile.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlFollowup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFollowup.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlFollowup.SelectedValue) > 0, False, True), "18_False", False, "Follow-up Status", ddlFollowup.SelectedItem.Text, ddlFollowup.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlContactType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlContactType.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlContactType.SelectedValue) > 0, False, True), "56_False", False, "Contact Type", ddlContactType.SelectedItem.Text, ddlContactType.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlOppOrOrder_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlOppOrOrder.SelectedIndexChanged
            Try
                ddlOppOrOrderStatus.Items.Clear()

                If CCommon.ToLong(ddlOppOrOrder.SelectedValue) > 0 Then
                    Dim dtData As DataTable = objCommon.GetMasterListItems(176, Session("DomainID"), If(ddlOppOrOrder.SelectedValue = "1" Or ddlOppOrOrder.SelectedValue = "3", 1, 2), If(ddlOppOrOrder.SelectedValue = "1" Or ddlOppOrOrder.SelectedValue = "2", 2, 1))

                    ddlOppOrOrderStatus.Items.Clear()
                    ddlOppOrOrderStatus.DataSource = dtData
                    ddlOppOrOrderStatus.DataValueField = dtData.Columns(0).ColumnName
                    ddlOppOrOrderStatus.DataTextField = dtData.Columns(1).ColumnName
                    ddlOppOrOrderStatus.DataBind()
                End If

                ddlOppOrOrderStatus.Items.Insert(0, New ListItem("-- Select Opportunity or Order Status --", "0"))
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlOppOrOrder.SelectedValue) > 0, False, True), "118_False", False, "Opp Type", ddlOppOrOrder.SelectedItem.Text, ddlOppOrOrder.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlOppOrOrderStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlOppOrOrderStatus.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlOppOrOrderStatus.SelectedValue) > 0, False, True), "101_False", False, "Order Status", ddlOppOrOrderStatus.SelectedItem.Text, ddlOppOrOrderStatus.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlBizDocsType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBizDocsType.SelectedIndexChanged
            Try
                ddlBizDocsTemplate.Items.Clear()
                ddlBizDocsStatus.Items.Clear()

                If CCommon.ToLong(ddlBizDocsType.SelectedValue) > 0 Then
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = Session("DomainID")
                    objOppBizDoc.BizDocId = ddlBizDocsType.SelectedValue
                    objOppBizDoc.byteMode = 0
                    Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                    ddlBizDocsTemplate.Items.Clear()
                    ddlBizDocsTemplate.DataSource = dtBizDocTemplate
                    ddlBizDocsTemplate.DataTextField = "vcTemplateName"
                    ddlBizDocsTemplate.DataValueField = "numBizDocTempID"
                    ddlBizDocsTemplate.DataBind()

                    objCommon.sb_FillComboFromDBwithSel(ddlBizDocsStatus, 11, Session("DomainID"), CCommon.ToLong(ddlBizDocsType.SelectedValue))
                End If

                ddlBizDocsTemplate.Items.Insert(0, New ListItem("-- Select BizDoc Template --", 0))
                ddlBizDocsStatus.Items.Insert(0, New ListItem("-- Select BizDoc Status --", "0"))

                AddDefaultFilterCondition(If(CCommon.ToLong(ddlBizDocsType.SelectedValue) > 0, False, True), "266_False", False, "BizDoc Type", ddlBizDocsType.SelectedItem.Text, ddlBizDocsType.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlBizDocsTemplate_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBizDocsTemplate.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlBizDocsTemplate.SelectedValue) > 0, False, True), "667_False", False, "BizDoc Template", ddlBizDocsTemplate.SelectedItem.Text, ddlBizDocsTemplate.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlBizDocsStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBizDocsStatus.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlBizDocsStatus.SelectedValue) > 0, False, True), "267_False", False, "BizDoc Status", ddlBizDocsStatus.SelectedItem.Text, ddlBizDocsStatus.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlCasesType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCasesType.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlCasesType.SelectedValue) > 0, False, True), "133_False", False, "Type", ddlCasesType.SelectedItem.Text, ddlCasesType.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlCasesReason_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCasesReason.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlCasesReason.SelectedValue) > 0, False, True), "136_False", False, "Reason", ddlCasesReason.SelectedItem.Text, ddlCasesReason.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlCasesStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCasesStatus.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlCasesStatus.SelectedValue) > 0, False, True), "137_False", False, "Status", ddlCasesStatus.SelectedItem.Text, ddlCasesStatus.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlCasesPriority_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCasesPriority.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlCasesPriority.SelectedValue) > 0, False, True), "129_False", False, "Priority", ddlCasesPriority.SelectedItem.Text, ddlCasesPriority.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlProjectsType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProjectsType.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlProjectsType.SelectedValue) > 0, False, True), "164_False", False, "Project Type", ddlProjectsType.SelectedItem.Text, ddlProjectsType.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlProjectsStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProjectsStatus.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlProjectsType.SelectedValue) > 0, False, True), "165_False", False, "Project Status", ddlProjectsType.SelectedItem.Text, ddlProjectsType.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlStage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStage.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlStage.SelectedValue) > 0, False, True), "520_False", False, "Stage", ddlStage.SelectedItem.Text, ddlStage.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlStageProgress_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStageProgress.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlStageProgress.SelectedValue) >= 0, False, True), "517_False", False, "Stage Progress", ddlStageProgress.SelectedItem.Text, ddlStageProgress.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlActionItemType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlActionItemType.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlActionItemType.SelectedValue) > 0, False, True), "542_False", False, "Activity", ddlActionItemType.SelectedItem.Text, ddlActionItemType.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlActionItemTaskType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlActionItemTaskType.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlActionItemTaskType.SelectedValue) > 0, False, True), "544_False", False, "Task", ddlActionItemTaskType.SelectedItem.Text, ddlActionItemTaskType.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlActionItemPriority_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlActionItemPriority.SelectedIndexChanged
            Try
                AddDefaultFilterCondition(If(CCommon.ToLong(ddlActionItemPriority.SelectedValue) > 0, False, True), "543_False", False, "Priority", ddlActionItemPriority.SelectedItem.Text, ddlActionItemPriority.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub btnCloseEditViewCondition_Click(sender As Object, e As EventArgs)
            ClearConditions()
        End Sub

        Protected Sub ClearConditions()
            If ViewState("CurrentCondTable") IsNot Nothing Then

                Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentCondTable"), DataTable)

                If dtCurrentTable.Rows.Count > 0 Then
                    dvANDOR.Style.Add("display", "block")
                    ddlANDORCondition.SelectedValue = "AND"
                Else
                    dvANDOR.Style.Add("display", "none")
                End If
            End If

            ddlCondition.SelectedValue = "0"
            ddlCondition.Enabled = True
            radValue.Visible = False
            txtValue.Visible = False
            calDate.Visible = False
            txtValueMoney.Visible = False
            txtNvalue.Visible = False
            ddlCompareWith.Visible = False
            ddlSign.Items.Clear()
            ddlSign.Items.Insert("0", New ListItem("--Select--", 0))
            'chkCondition.Checked = False
        End Sub

        Protected Sub btnCloseEditViewAction_Click(sender As Object, e As EventArgs)
            ClearActions()
        End Sub

        Protected Sub ClearActions()
            ddlAction.Enabled = True
            ddlAction.SelectedValue = "0"
            dvUpdateFields.Style.Add("display", "none")
            dvBizDocapproval.Style.Add("display", "none")
            dvAttachBizDoc.Style.Add("display", "none")
            dvSelectBizDoc.Style.Add("display", "none")
            dvSendMail.Style.Add("display", "none")
            displayAlert.Style.Add("display", "none")
            dvSendSMS.Style.Add("display", "none")
            dvAssignItems.Style.Add("display", "none")
            dvCreateBizDoc.Style.Add("display", "none")
        End Sub

        Protected Sub lnkEmailTemplate_Click(sender As Object, e As EventArgs)
            divShowEmailDesc.Style.Add("display", "block")

            If (dtEmailTemplates IsNot Nothing And dtEmailTemplates.Rows.Count > 0) Then
                For Each drrow As DataRow In dtEmailTemplates.Rows
                    If lngSelectedEmailTemplate = CCommon.ToLong(drrow("numGenericDocID")) Then
                        lblSubject.Text = drrow("vcSubject").ToString
                        litDesc.Text = drrow("vcDocdesc").ToString
                    End If
                Next
            End If
        End Sub

        Protected Sub ddlArAging_SelectedIndexChanged(sender As Object, e As EventArgs)
            AddARAgingCondition("0_0", "A/R Aging", ddlArAging.SelectedItem.Text, ddlArAging.SelectedValue)
        End Sub

        Private Sub AddARAgingCondition(ByVal numFieldID As String, ByVal vcFieldName As String, ByVal itemText As String, ByVal itemValue As Long)
            Try
                If ViewState("CurrentCondTable") IsNot Nothing Then
                    Dim isNewCondition As Boolean = True

                    Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentCondTable"), DataTable)
                    Dim drCurrentRow As DataRow = Nothing

                    If dtCurrentTable.Select("ID='" & numFieldID & "'").Length = 0 Then
                        drCurrentRow = dtCurrentTable.NewRow()
                        drCurrentRow("RowNumber") = dtCurrentTable.Rows.Count + 1
                        isNewCondition = True
                    Else
                        isNewCondition = False
                        drCurrentRow = dtCurrentTable.Select("ID='" & numFieldID & "'")(0)
                    End If

                    'RowNumber, vcANDOR, ID, vcFieldName, numEquation, vcEquation, numID, VcData, vcMessge
                    Dim sbValue As New StringBuilder()
                    sbValue.Append(itemValue)
                    Dim sbValueText As New StringBuilder()
                    sbValueText.Append(itemText)

                    drCurrentRow("ID") = numFieldID
                    drCurrentRow("vcFieldName") = vcFieldName
                    drCurrentRow("numEquation") = "is"
                    drCurrentRow("vcEquation") = "is"
                    drCurrentRow("numID") = sbValue.ToString()
                    drCurrentRow("VcData") = "'" & ddlArAging.SelectedItem.Text & "'"
                    drCurrentRow("intCompare") = 0

                    If isNewCondition AndAlso dtCurrentTable.Rows.Count > 0 Then
                        drCurrentRow("vcANDOR") = ddlANDORCondition.SelectedValue
                    Else
                        drCurrentRow("vcANDOR") = Nothing
                    End If

                    If isNewCondition Then
                        dtCurrentTable.Rows.Add(drCurrentRow)
                    Else
                        dtCurrentTable.AcceptChanges()
                    End If

                    If Not dtCurrentTable Is Nothing AndAlso dtCurrentTable.Rows.Count > 0 Then
                        Dim vcMessage As String = ""

                        For Each dr As DataRow In dtCurrentTable.Rows
                            If String.IsNullOrEmpty(vcMessage) Then
                                vcMessage = "When <b>" & dr("vcFieldName") & " </b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                dr("vcMessge") = "When <b>" & dr("vcFieldName") & " </b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                            Else
                                vcMessage = vcMessage & " " & "<b>" & dr("vcANDOR") & "  </b>  <b>  " & dr("vcFieldName") & "</b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                                dr("vcMessge") = "<b>" & dr("vcANDOR") & "  </b>  <b>  " & dr("vcFieldName") & "</b>  " & dr("vcEquation") & " <b>" & dr("VcData") & "</b>"
                            End If
                        Next

                        dtCurrentTable.AcceptChanges()
                        Session("strCondition") = vcMessage
                    Else
                        Session("strCondition") = ""
                    End If

                    'Store the current data to ViewState for future reference
                    ViewState("CurrentCondTable") = dtCurrentTable

                    'Rebind the Grid with the current data to reflect changes
                    rprConditions.DataSource = dtCurrentTable
                    rprConditions.DataBind()
                Else
                    Response.Write("ViewState is null")
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace