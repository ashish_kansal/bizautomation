﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Admin
Imports System.Drawing
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents

Namespace BACRM.UserInterface.Workflows
    Public Class frmWFList
        Inherits BACRMPage
        Dim m_aryRightsForOrders() As Integer
        Dim objWF As New Workflow

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                If Not IsPostBack Then
                    LoadDropDowns()

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))

                        txtWFName.Text = CCommon.ToString(PersistTable(txtWFName.ID))

                        If ddlForm.Items.FindByValue(CCommon.ToString(PersistTable(ddlForm.ID))) IsNot Nothing Then
                            ddlForm.ClearSelection()
                            ddlForm.Items.FindByValue(CCommon.ToString(PersistTable(ddlForm.ID))).Selected = True
                        End If
                    End If

                    If (CCommon.ToInteger(txtCurrrentPage.Text) = 0) Then
                        txtCurrrentPage.Text = "1"
                    End If

                    BindDataGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub LoadDropDowns()
            Try
                Dim objConfigWizard As New FormConfigWizard             'Create an object of form config wizard
                objConfigWizard.DomainID = Session("DomainID")          'Set value of domain id
                objConfigWizard.tintType = 3

                Dim dtFormsList As DataTable
                dtFormsList = objConfigWizard.getFormList()

                ddlForm.DataSource = dtFormsList
                ddlForm.DataTextField = "vcFormName"
                ddlForm.DataValueField = "numFormId"
                ddlForm.DataBind()

                ddlForm.Items.Insert(0, New ListItem("--All--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindDataGrid()
            Try
                Dim dtTable As DataTable
                With objWF
                    .DomainID = Session("DomainId")
                    .SortCharacter = txtSortChar.Text.Trim()

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()

                    objWF.PageSize = Session("PagingRows")
                    objWF.TotalRecords = 0

                    If txtSortColumn.Text <> "" Then
                        .columnName = txtSortColumn.Text.Trim()
                    Else : .columnName = "numWFID"
                    End If

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    objWF.FormID = ddlForm.SelectedValue
                    objWF.WFName = txtWFName.Text
                End With

                dtTable = objWF.GetWorkFlowMaster()

                PersistTable.Clear()
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtTable.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())

                PersistTable.Add(txtWFName.ID, txtWFName.Text.Trim())
                PersistTable.Add(ddlForm.ID, ddlForm.SelectedValue)

                PersistTable.Save()

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objWF.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                gvWFList.DataSource = dtTable
                gvWFList.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
            Try
                'txtCurrrentPage.Text = 1
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                'txtCurrrentPage.Text = 1
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvWFList_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvWFList.RowCommand
            Try
                If e.CommandName = "DeleteWF" Then
                    Dim gvRow As GridViewRow
                    gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                    objWF.DomainID = Session("DomainId")
                    objWF.WFID = gvWFList.DataKeys(gvRow.DataItemIndex).Value
                    objWF.DeleteWorkFlowMaster()

                    txtCurrrentPage.Text = 1

                    BindDataGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvWFList_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvWFList.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim hplEdit As LinkButton = e.Row.FindControl("hplEdit")
                    hplEdit.Attributes.Add("onclick", "return EditWorkFlow(" & DataBinder.Eval(e.Row.DataItem, "numWFID") & ")")

                    Dim lblStatus As Label = e.Row.FindControl("lblStatus")
                    Dim imImageBtn As ImageButton = e.Row.FindControl("imImageBtn")
                    If lblStatus.Text = "Active" Then
                        imImageBtn.ImageUrl = "~/images/active.png"
                    Else
                        imImageBtn.ImageUrl = "~/images/inactive.png"
                    End If

                    Dim lngWFID As Long = CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numWFID"))
                    Dim rtConditons As Repeater = CType(e.Row.FindControl("rprConditions"), Repeater)
                    Dim rtActions As Repeater = CType(e.Row.FindControl("rprActions"), Repeater)

                    Dim objWFManage As New Workflow
                    Dim ds As DataSet

                    objWFManage.WFID = lngWFID
                    objWFManage.DomainID = Session("DomainID")
                    ds = objWFManage.GetWorkFlowMasterDetail

                    If ds.Tables.Count > 0 Then
                        If (ds.Tables(2).Rows.Count > 0) Then 'WorkFlowConditionList

                            Dim dtConditions As New DataTable()

                            CCommon.AddColumnsToDataTable(dtConditions, "RowNumber,vcANDOR,ID,vcFieldName,numEquation,vcEquation,numID,VcData,vcMessge,intCompare")

                            '  Dim dtCurrentTable As DataTable = dtConditions
                            Dim drCurrentRow As DataRow = Nothing
                            For i = 0 To ds.Tables(2).Rows.Count - 1

                                drCurrentRow = dtConditions.NewRow()
                                drCurrentRow("ID") = CCommon.ToString(ds.Tables(2).Rows(i)("FieldID"))

                                If (CCommon.ToLong(ds.Tables(0).Rows(0)("numFormID")) = 138) Then   'A/R Aging
                                    drCurrentRow("vcFieldName") = "A/R Aging"
                                    drCurrentRow("vcEquation") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterOperator"))
                                Else
                                    drCurrentRow("vcFieldName") = CCommon.ToString(ds.Tables(2).Rows(i)("FieldName"))
                                    drCurrentRow("vcEquation") = CCommon.ToString(ds.Tables(2).Rows(i)("FilterOperator"))
                                End If
                                drCurrentRow("numEquation") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterOperator"))
                                drCurrentRow("VcData") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterData"))
                                If dtConditions.Rows.Count > 0 Then
                                    drCurrentRow("vcANDOR") = CCommon.ToString(ds.Tables(2).Rows(i)("vcFilterANDOR"))
                                    drCurrentRow("vcMessge") = "<b>" & drCurrentRow("vcANDOR") & "  </b>  <b>  " & drCurrentRow("vcFieldName") & "</b>  " & drCurrentRow("vcEquation") & " <b>" & drCurrentRow("VcData") & "</b>"
                                Else
                                    drCurrentRow("vcANDOR") = Nothing
                                    drCurrentRow("vcMessge") = "When <b>" & drCurrentRow("vcFieldName") & " </b>  " & drCurrentRow("vcEquation") & " <b>" & drCurrentRow("VcData") & "</b>"
                                End If

                                dtConditions.Rows.Add(drCurrentRow)
                            Next

                            'bind the repeater 
                            rtConditons.DataSource = dtConditions
                            rtConditons.DataBind()
                        End If

                        If (ds.Tables(3).Rows.Count > 0) Then 'WorkFlowActionList

                            Dim dtActions As New DataTable()
                            CCommon.AddColumnsToDataTable(dtActions, "RowNumber,numAction,vcAction,numGenericDocID,vcDocName,vcEmailToType,vcReceiverName,TemplateName,RowID,vcAssignTo,numAssignTo,vcUpdateFields,vcAlertMessage,vcMessge,tintIsAttachment,vcApproval,tintSendMail,numBizDocTemplateID,vcBizDocTemplate,numBizDocTypeID,vcBizDocType,vcBizDoc,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject")

                            Dim drCurrentRow As DataRow = Nothing

                            For i = 0 To ds.Tables(3).Rows.Count - 1

                                drCurrentRow = dtActions.NewRow()
                                drCurrentRow("numAction") = CCommon.ToString(ds.Tables(3).Rows(i)("tintActionType"))
                                Dim numActiontype As Integer = CCommon.ToInteger(ds.Tables(3).Rows(i)("tintActionType"))
                                If numActiontype = 1 Then 'Send Alerts
                                    drCurrentRow("vcAction") = "Send Alerts"
                                    drCurrentRow("numGenericDocID") = CCommon.ToString(ds.Tables(3).Rows(i)("numTemplateID"))
                                    drCurrentRow("vcDocName") = CCommon.ToString(ds.Tables(3).Rows(i)("EmailTemplate"))
                                    drCurrentRow("vcReceiverName") = CCommon.ToString(ds.Tables(3).Rows(i)("EmailToType"))
                                    drCurrentRow("vcEmailToType") = CCommon.ToString(ds.Tables(3).Rows(i)("vcEmailToType"))
                                    drCurrentRow("vcEmailSendTo") = CCommon.ToString(ds.Tables(3).Rows(i)("vcEmailSendTo"))
                                    drCurrentRow("vcMailBody") = CCommon.ToString(ds.Tables(3).Rows(i)("vcMailBody"))
                                    drCurrentRow("vcMailSubject") = CCommon.ToString(ds.Tables(3).Rows(i)("vcMailSubject"))

                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = CCommon.ToShort(ds.Tables(3).Rows(i)("tintIsAttachment"))
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing

                                    If CCommon.ToLong(drCurrentRow("numGenericDocID")) = 99999 Then
                                        drCurrentRow("vcDocName") = "Compose Message"
                                    End If

                                    If drCurrentRow("vcEmailSendTo").ToString <> "" AndAlso drCurrentRow("vcReceiverName").ToString <> "" Then
                                        drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; Using Email Template <b>" & drCurrentRow("vcDocName") & "</b> To <b>" & drCurrentRow("vcReceiverName") & "," & CCommon.ToString(drCurrentRow("vcEmailSendTo")) & "</b>"
                                    Else
                                        drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; Using Email Template <b>" & drCurrentRow("vcDocName") & "</b> To <b>" & drCurrentRow("vcReceiverName") & CCommon.ToString(drCurrentRow("vcEmailSendTo")) & "</b>"
                                    End If

                                ElseIf numActiontype = 2 Then 'Assign Action Items

                                    drCurrentRow("vcAction") = "Assign Action Items"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = CCommon.ToString(ds.Tables(3).Rows(i)("numTemplateID"))
                                    drCurrentRow("TemplateName") = CCommon.ToString(ds.Tables(3).Rows(i)("EmailTemplate"))
                                    drCurrentRow("numAssignTo") = CCommon.ToString(ds.Tables(3).Rows(i)("tintTicklerActionAssignedTo"))
                                    drCurrentRow("vcAssignTo") = CCommon.ToString(ds.Tables(3).Rows(i)("TicklerAssignedTo"))
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing

                                    drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; Using Action Template <b>" & drCurrentRow("TemplateName") & "</b> To <b>" & drCurrentRow("vcAssignTo") & "</b>"

                                ElseIf numActiontype = "3" Then 'update fields

                                    Dim strFields As String = Nothing
                                    If (ds.Tables(4).Rows.Count > 0) Then 'WorkFlowActionUpdateFields
                                        For f = 0 To ds.Tables(4).Rows.Count - 1
                                            strFields = CCommon.ToString(ds.Tables(4).Rows(f)("FieldName")) & "," & strFields
                                        Next
                                    End If

                                    drCurrentRow("vcAction") = ""
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = CCommon.ToString(strFields).Remove(strFields.Length - 1, 1)
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing

                                    drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; Updates fields: <b>" & CCommon.ToString(drCurrentRow("vcUpdateFields")) & "</b>"

                                ElseIf numActiontype = 5 Then 'Biz Doc Approval
                                    drCurrentRow("vcAction") = "BizDoc Approval"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    drCurrentRow("vcApproval") = CCommon.ToString(ds.Tables(3).Rows(i)("vcApproval"))
                                    drCurrentRow("tintSendMail") = CCommon.ToShort(ds.Tables(3).Rows(i)("tintSendMail"))

                                    drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; by <b>" & CCommon.ToString(ds.Tables(3).Rows(i)("vcApprovalName")) & "</b> "

                                ElseIf numActiontype = 6 Then 'Create BizDoc

                                    drCurrentRow("vcAction") = "Create bizDoc"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing
                                    drCurrentRow("numBizDocTypeID") = CCommon.ToLong(ds.Tables(3).Rows(i)("numBizDocTypeID"))
                                    drCurrentRow("numBizDocTemplateID") = CCommon.ToLong(ds.Tables(3).Rows(i)("numBizDocTemplateID"))
                                    drCurrentRow("vcBizDocTemplate") = CCommon.ToString(ds.Tables(3).Rows(i)("vcBizDocTemplate"))
                                    drCurrentRow("vcBizDocType") = CCommon.ToString(ds.Tables(3).Rows(i)("vcBizDocType"))
                                    drCurrentRow("vcBizDoc") = CCommon.ToString(ds.Tables(3).Rows(i)("vcBizDoc"))

                                    drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; Using Template <b>" & drCurrentRow("vcBizDocTemplate") & "</b>"

                                ElseIf numActiontype = 7 Then 'Promote Organization

                                    drCurrentRow("vcAction") = "Promote Organization"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing
                                    drCurrentRow("numBizDocTemplateID") = Nothing
                                    drCurrentRow("vcBizDocTemplate") = Nothing
                                    drCurrentRow("vcBizDocType") = Nothing
                                    drCurrentRow("vcBizDoc") = Nothing

                                    drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "</b>"

                                ElseIf numActiontype = 8 Then 'Demote Organization

                                    drCurrentRow("vcAction") = "Demote Organization"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing
                                    drCurrentRow("numBizDocTemplateID") = Nothing
                                    drCurrentRow("vcBizDocTemplate") = Nothing
                                    drCurrentRow("vcBizDocType") = Nothing
                                    drCurrentRow("vcBizDoc") = Nothing
                                    drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "</b>"  ' CCommon.ToString(Session("strCondition")) & "&nbsp; Then &nbsp;" &

                                ElseIf numActiontype = 9 Then 'Send SMS Alerts
                                    drCurrentRow("vcAction") = "Send SMS Alerts"
                                    drCurrentRow("numGenericDocID") = CCommon.ToString(ds.Tables(3).Rows(i)("numTemplateID"))
                                    drCurrentRow("vcDocName") = CCommon.ToString(ds.Tables(3).Rows(i)("EmailTemplate"))
                                    drCurrentRow("vcReceiverName") = CCommon.ToString(ds.Tables(3).Rows(i)("EmailToType"))
                                    drCurrentRow("vcEmailToType") = CCommon.ToString(ds.Tables(3).Rows(i)("vcEmailToType"))
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = Nothing
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = CCommon.ToShort(ds.Tables(3).Rows(i)("tintIsAttachment"))
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("vcSMSText") = CCommon.ToString(ds.Tables(3).Rows(i)("vcSMSText"))
                                    drCurrentRow("tintSendMail") = Nothing
                                    drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; <b>" & drCurrentRow("vcSMSText") & "</b> To <b>" & drCurrentRow("vcReceiverName") & "</b>"  ' CCommon.ToString(Session("strCondition")) & "Then &nbsp;" & 

                                Else
                                    drCurrentRow("vcAction") = "Display Alert Message"
                                    drCurrentRow("numGenericDocID") = Nothing
                                    drCurrentRow("vcDocName") = Nothing
                                    drCurrentRow("vcReceiverName") = Nothing
                                    drCurrentRow("vcEmailToType") = Nothing
                                    drCurrentRow("vcEmailSendTo") = Nothing
                                    drCurrentRow("RowID") = Nothing
                                    drCurrentRow("TemplateName") = Nothing
                                    drCurrentRow("numAssignTo") = Nothing
                                    drCurrentRow("vcAssignTo") = Nothing
                                    drCurrentRow("vcAlertMessage") = CCommon.ToString(ds.Tables(3).Rows(i)("vcAlertMessage"))
                                    drCurrentRow("vcUpdateFields") = Nothing
                                    drCurrentRow("tintIsAttachment") = Nothing
                                    drCurrentRow("vcApproval") = Nothing
                                    drCurrentRow("tintSendMail") = Nothing
                                    drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; <b>" & drCurrentRow("vcAlertMessage") & "</b> "   'CCommon.ToString(Session("strCondition")) & "&nbsp; Then &nbsp;" &

                                End If

                                dtActions.Rows.Add(drCurrentRow)
                            Next
                            'bind the Repeater 
                            rtActions.DataSource = dtActions
                            rtActions.DataBind()

                        End If

                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlForm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlForm.SelectedIndexChanged
            Try
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnRunService_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRunService.Click
            Try
                Dim objWFM As New Workflow

                Dim objWF As New WorkFlowExecution
                objWF.WFExecution()

                'Dim objOA As New OpportunityAutomation
                'objOA.OpportunityAutomationExecution()

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try

        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Protected Sub imgOpenTemplate_Click(sender As Object, e As ImageClickEventArgs)
            divShowEmailDesc.Style.Add("display", "block")

            Dim imgTemplate As RepeaterItem = TryCast((TryCast(sender, ImageButton)).NamingContainer, RepeaterItem)
            Dim lblDocId As String = TryCast(imgTemplate.FindControl("lblEmailTemplateId"), Label).Text

            Dim lblBizDocId As String = TryCast(imgTemplate.FindControl("lblBizDocId"), Label).Text

            If (lblDocId <> "") Then
                Dim dtDocDetails As DataTable
                Dim objDocuments As New DocumentList
                With objDocuments
                    .GenDocID = lblDocId
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtDocDetails = .GetDocByGenDocID
                End With

                lblSubject.Text = dtDocDetails.Rows(0)("vcSubject").ToString
                litDesc.Text = dtDocDetails.Rows(0)("vcDocdesc").ToString

            Else
                If (lblBizDocId <> "") Then
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = Session("DomainID")
                    objOppBizDoc.TemplateType = 0

                    objOppBizDoc.BizDocTemplateID = lblBizDocId

                    Dim dt As DataTable = objOppBizDoc.GetBizDocTemplate()
                    If dt.Rows.Count > 0 Then
                        lblSubject.Text = dt.Rows(0)("vcTemplateName").ToString
                        litDesc.Text = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtBizDocTemplate")))
                    End If

                End If

            End If


        End Sub

        Protected Sub rprActions_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim lblBizDocId As Label = DirectCast(e.Item.FindControl("lblBizDocId"), Label)
                Dim lblDocId As Label = DirectCast(e.Item.FindControl("lblEmailTemplateId"), Label)
                Dim imgOpenTemplate As ImageButton = DirectCast(e.Item.FindControl("imgOpenTemplate"), ImageButton)

                If (lblDocId.Text <> "") Then
                    imgOpenTemplate.Visible = True
                Else
                    If (lblBizDocId.Text <> "") Then
                        imgOpenTemplate.Visible = True
                    Else
                        imgOpenTemplate.Visible = False
                    End If

                End If

            End If

        End Sub
    End Class
End Namespace