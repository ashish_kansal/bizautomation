﻿<%@ Page Language="vb" MasterPageFile="~/common/GridMasterRegular.Master" AutoEventWireup="false" CodeBehind="frmWFHistory.aspx.vb" Inherits="BACRM.UserInterface.Workflows.frmWFHistory" %>


<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Workflow & Business Process Automation (BPA)</title>
    <script type="text/javascript">
        function NewWorkFlow() {
            window.location.href = "../WorkFlow/frmWFManage.aspx";
            return false;
        }

        function OpenOpp(a, b) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" + a;
            document.location.href = str;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>
                        Module
                    </label>
                    <asp:DropDownList AutoPostBack="true" Width="240" ID="ddlForm" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <label>
                        Name or Description
                    </label>
                    <div class="input-group">
                        <asp:TextBox ID="txtWFName" runat="server" CssClass="form-control" Width="180" MaxLength="25"></asp:TextBox>
                        <span class="input-group-btn">
                            <asp:Button runat="server" CssClass="btn btn-primary btn-flat" Text="Go" ID="btnSearch" />
                        </span>
                    </div>
                </div>
                <asp:Button ID="btnTest" runat="server" Text="Test" CssClass="button" Visible="false" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Workflow & Business Process Automation (BPA)
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
    
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="false"
                    CssClass="table table-bordered table-striped" Width="100%" DataKeyNames="numWFID" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label ID="lblSequnece" runat="server" Text='<%#Eval("RowNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="vcWFName" HeaderText="Rule Name" ItemStyle-Width="10%" SortExpression="vcWFName"></asp:BoundField>
                        <asp:BoundField DataField="vcFormName" HeaderText="Module" SortExpression="vcFormName" ItemStyle-Wrap="false"></asp:BoundField>
                        <asp:BoundField DataField="TriggeredOn" HeaderText="Event" SortExpression="TriggeredOn" ItemStyle-Wrap="false"></asp:BoundField>
                        <asp:TemplateField ItemStyle-Width="10%" HeaderText="Requested On" SortExpression="dtCreatedDate">
                            <ItemTemplate>
                                <%#String.Format("{0:m}", Convert.ToDateTime(Eval("dtCreatedDate")).AddMinutes(Session("ClientMachineUTCTimeOffset") * -1)) & "," & String.Format("{0:hh:mm tt}", Convert.ToDateTime(Eval("dtCreatedDate")).AddMinutes(Session("ClientMachineUTCTimeOffset") * -1))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%" HeaderText="Executed On" SortExpression="dtExecutionDate">
                            <ItemTemplate>
                                <%# If(Eval("dtExecutionDate") Is DBNull.Value, "", String.Format("{0:m}", Convert.ToDateTime(Eval("dtExecutionDate")).AddMinutes(Session("ClientMachineUTCTimeOffset") * -1)) & "," & String.Format("{0:hh:mm tt}", Convert.ToDateTime(Eval("dtExecutionDate")).AddMinutes(Session("ClientMachineUTCTimeOffset") * -1)))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="vcUserName" HeaderText="Created by" SortExpression="vcUserName" ItemStyle-Wrap="false" />
                        <asp:TemplateField HeaderText="Record/Order Name" SortExpression="vcPOppName" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplItemOrderNo" runat="server" Text='<%# Eval("vcPOppName")%>'></asp:HyperLink>
                                <asp:Label runat="server" ID="lblItemOrderNo" Text='<%# Eval("vcPOppName")%>' Visible="false"></asp:Label>
                                <asp:Label runat="server" ID="llb" Text='<%# Eval("numOppId")%>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" SortExpression="Status">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblStatusText" Text='<%# Eval("Status")%>' Visible="True" Font-Bold="true"></asp:Label>
                                <asp:Label runat="server" ID="lblStatus" Text='<%# Eval("tintProcessStatus")%>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Exec.Details" SortExpression="vcDescription">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblExecutionDetails" Text='<%# Eval("vcDescription")%>' Visible="True" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" CssClass="button" runat="server" Style="display: none" Text="Go"></asp:Button>
</asp:Content>
