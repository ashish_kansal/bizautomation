﻿//***filter Condition Operators & Mapping with Controls Type***//
filterConditionOperators = {
    "eq": "equals", "ne": "not equal to",
    "gt": "greater than", "ge": "greater or equal",
    "lt": "less than", "le": "less or equal",
    "co": "contains", "sw": "starts with", "nc": "does not contain"
    //,"ex": "excludes", "wi": "within", "in": "includes", 
};

typeFilterOperatorsMap = {
    //TextBox_V,TextBox_M,SelectBox_N,DateField_V
    "TextBox_V": ["eq", "ne", "co", "nc", "sw"],
    "TextBox_M": ["eq", "ne", "lt", "gt", "le", "ge"],
    "TextBox_N": ["eq", "ne", "lt", "gt", "le", "ge"],
    "SelectBox_N": ["eq", "ne"],
    "SelectBox_V": ["eq", "ne"],
    "CheckBox_V": ["eq", "ne"],
    "CheckBox_Y": ["eq", "ne"],
    "DateField_D": ["eq", "ne", "lt", "gt", "le", "ge"],
    "DateField_V": ["eq", "ne", "lt", "gt", "le", "ge"],
};

arrActionList =
          [{ "vcActionType": "--Select One--", "ID": "0" },
           { "vcActionType": "Send Alerts", "ID": "1" },
           { "vcActionType": "Assign Action Items", "ID": "2" },
           { "vcActionType": "Update Fields", "ID": "3" },
           { "vcActionType": "Display Alert Message", "ID": "4" }]

function findInColumnList(FieldName, FieldValue) {

    var new_arr = $.grep(arrFormFields, function (ColumnList, i) {
        //return (ColumnList["numFormFieldId"] == FieldValue.split('_')[0] && ColumnList["bitCustomField"] == FieldValue.split('_')[1]);
        return ColumnList[FieldName] == FieldValue;
    });

    return new_arr[0];
}

function SelectTabByValue(Id) {
    //var tabStrip = $find("radWFTab");
    //var tab = tabStrip.findTabByValue(Id);
    //if (tab) {
    //    tab.select();
    //}
    $("#Step1").hide();
    $("#Step2").hide();
    $("#Step3").hide();
    $("#Step4").hide();

    $("#" + Id).show();
}

//***Get DropDown Data***//
function GetFormFieldList() {
    $.ajax({
        type: "POST",
        async: false,
        url: "../common/Common.asmx/GetWFFormFieldList",
        data: "{numFormID:'" + $('#ddlForm').val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log($.parseJSON(response.d));
            result = $.parseJSON(response.d);
        },
        failure: function (response) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        },
        complete: function () {
        }
    });

    return result;
}

//***Get DropDown Data***//
function GetDropDownData(numListID, vcListItemType, vcDbColumnName, vcAssociatedControlType) {
    var result = "";

    $.ajax({
        type: "POST",
        async: false,
        url: "../common/CustomReportsService.asmx/GetDropDownData",
        data: "{numListID:'" + numListID + "',vcListItemType:'" + vcListItemType + "',vcDbColumnName:'" + vcDbColumnName + "',vcAssociatedControlType:'" + vcAssociatedControlType + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log($.parseJSON(response.d));
            result = $.parseJSON(response.d);
        },
        failure: function (response) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        },
        complete: function () {
        }
    });

    return result;
}

//***Get Action Template List***//
function GetActionItemTemplateList() {
    $.ajax({
        type: "POST",
        async: false,
        url: "../common/Common.asmx/GetActionItemTemplateList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log($.parseJSON(response.d));
            result = $.parseJSON(response.d);
        },
        failure: function (response) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        },
        complete: function () {
        }
    });

    return result;
}

//***Get Email Template List***//
function GetEmailTemplateList() {
    $.ajax({
        type: "POST",
        async: false,
        url: "../common/Common.asmx/GetEmailTemplateList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log($.parseJSON(response.d));
            result = $.parseJSON(response.d);
        },
        failure: function (response) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        },
        complete: function () {
        }
    });

    return result;
}

//***Get Report Response***//
function GetWFData() {
    $.ajax({
        type: "POST",
        async: false,
        url: "../common/WorkFlowService.asmx/GetWFData",
        data: "{lngWFID:'" + $("#hfWFID").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log(response.d);
            responseJson = $.parseJSON(response.d);

            if (parseInt($("#hfWFID").val()) == 0)
                return;

            $("#txtWFName").val(responseJson.WFName);
            $("#txtWFDescription").val(responseJson.WFDescription);
            $('#chkActive').prop('checked', responseJson.Active);

            $('#ddlForm').find("option[value='" + responseJson.FormID + "']").prop("selected", true);
            iSelectedForm = responseJson.FormID;

            $('input:radio[name=gnTriggerEvents]').filter('[value=' + responseJson.WFTriggerOn + ']').prop('checked', true);

            arrFormFields = GetFormFieldList();
            changeTriggerEvents();

            //Condition List
            $.each(responseJson.Conditions, function (i, selValue) {
                AddRuleCondition();

                if (i != 0) {
                    $("#ddlANDORCondition_" + i).val(selValue.FilterANDOR);
                }

                $("#ddlFilterEditModeColumnList_" + i).val(selValue.Column);
                $("#ddlFilterEditModeColumnList_" + i).change();
                $("#ddlFilterEditModeOperator_" + i).val(selValue.FilterOperator);

                SelectColumn = findInColumnList('ID', selValue.Column);

                if (SelectColumn.vcAssociatedControlType == 'SelectBox' || SelectColumn.vcAssociatedControlType == 'CheckBox') {
                    var separated = selValue.FilterValue.split(",");

                    $("#ddlFilterEditMode_" + i).dropdownchecklist("destroy");

                    $.each(separated, function (k, selValue) {
                        $("#ddlFilterEditMode_" + i).find("option[value='" + selValue + "']").prop("selected", true);
                    });

                    $("#ddlFilterEditMode_" + i).dropdownchecklist({ width: 180, maxDropHeight: 350 });
                }
                else if (SelectColumn.vcAssociatedControlType == 'DateField') {
                    $("#txtFilter_calDate_" + i).val(selValue.FilterValue);
                }
                else {
                    $("#txtFilterEditModeValue_" + i).val(selValue.FilterValue);
                }
            });

            //Action List
            $.each(responseJson.Actions, function (i, selValue) {
                AddRuleAction();

                $("#ddlActionsList_" + i).val(selValue.ActionType);
                $("#ddlActionsList_" + i).change();

                switch (selValue.ActionType) {
                    case 1: //Email Alerts
                        $("#ddlEmailTemplate_" + i).val(selValue.TemplateID);
                        var separated = selValue.vcEmailToType.split(",");

                        $("#ddlEmailTo_" + i).dropdownchecklist("destroy");

                        $.each(separated, function (k, selValue) {
                            $("#ddlEmailTo_" + i).find("option[value='" + selValue + "']").prop("selected", true);
                        });

                        $("#ddlEmailTo_" + i).dropdownchecklist({ width: 180, maxDropHeight: 350 });

                        break;
                    case 2: //Assign Action Items
                        $("#ddlActionItemTemplate_" + i).val(selValue.TemplateID);
                        $("#ddlActionItemAssignTo_" + i).val(selValue.TicklerActionAssignedTo);

                        break;
                    case 3: //Update Fields
                        $.each(selValue.ActionUpdateFields, function (j, objUF) {
                            if ($("#dvActionSetting_" + i).find("#ddlUFColumns_" + j).length == 0)
                                AddActionUF($("#dvActionSetting_" + i));                            $("#dvActionSetting_" + i).find("#ddlUFColumns_" + j).val(objUF.Column);
                            $("#dvActionSetting_" + i).find("#ddlUFColumns_" + j).trigger('change');

                            SelectColumn = findInColumnList('ID', objUF.Column);

                            if (SelectColumn.vcAssociatedControlType == 'SelectBox' || SelectColumn.vcAssociatedControlType == 'CheckBox') {
                                $("#dvActionSetting_" + i).find("#ddlUFValue_" + j).find("option[value='" + objUF.vcValue + "']").prop("selected", true);
                            }
                            else if (SelectColumn.vcAssociatedControlType == 'DateField') {
                                $("#dvActionSetting_" + i).find("#txtUFValue_calDate_" + j).val(objUF.vcValue);
                            }
                            else {
                                $("#dvActionSetting_" + i).find("#txtUFValue_" + j).val(objUF.vcValue);
                            }
                        });

                        break;
                    case 4: //Alert Message
                        $("#txtAlertMessage_" + i).val(selValue.vcAlertMessage);
                        break;
                }
            });
        },
        failure: function (response) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        },
        complete: function () {
        }
    });
}

function SaveWFData() {

    responseJson = { "WFName": '', "Active": false, "WFDescription": '', "FormID": 0, "WFTriggerOn": 0, "TriggerFieldList": [], "Conditions": [], "Actions": [] };

    responseJson.WFName = $("#txtWFName").val();
    responseJson.WFDescription = $("#txtWFDescription").val();
    responseJson.Active = $('#chkActive').prop('checked');
    responseJson.FormID = $('#ddlForm').val();
    responseJson.WFTriggerOn = $('input:radio[name=gnTriggerEvents]:checked').val();

    //Trigger Field List
    responseJson.TriggerFieldList = [];

    if ($('#rbFieldUpdate').is(':checked')) {
        $('#ddlFieldUpdate1').find('option:selected').each(function (i, selected) {
            responseJson.TriggerFieldList.push($(selected).val());
        });
    }

    //Condition List
    responseJson.Conditions = [];
    $('#dvRuleCriteria').find('.filterItemContainer').each(function (i) {
        var IdIndex = i + 1;

        var FilterANDOR = "";

        if (IdIndex != 1) {
            FilterANDOR = $(this).find('[id*=ddlANDORCondition]').val();
        }

        var Column = $(this).find('[id*=ddlFilterEditModeColumnList]').val();
        var FilterOperator = $(this).find('[id*=ddlFilterEditModeOperator]').val();

        var FilterValue = "";

        SelectColumn = findInColumnList('ID', Column);

        if (SelectColumn.vcAssociatedControlType == 'SelectBox' || SelectColumn.vcAssociatedControlType == 'CheckBox') {
            $(this).find("#ddlFilterEditMode_" + i).find('option:selected').each(function (i, selected) {
                if (FilterValue != "") {
                    FilterValue += ",";
                }
                FilterValue += $(selected).val();
            });
        }
        else if (SelectColumn.vcAssociatedControlType == 'DateField') {
            var DateValue = $.trim($(this).find('[id*=txtFilter_calDate]').val());

            if (!isDate(DateValue, $('#hfValidDateFormat').val())) {
                return false;
            }

            FilterValue = DateValue;
        }
        else {
            FilterValue = $(this).find('[id*=txtFilterEditModeValue]').val();
        }

        responseJson.Conditions.push({ 'Column': Column, 'FilterANDOR': FilterANDOR, 'FilterOperator': FilterOperator, 'FilterValue': FilterValue });
    });

    //Action List
    responseJson.Actions = [];
    $('#dvRuleAction').find('.actionContainer').each(function (i) {
        var ActionType = parseInt($(this).find('[id*=ddlActionsList]').val());
        var IdIndex = i + 1;

        var TemplateID = 0, vcEmailToType = '', TicklerActionAssignedTo = 0, vcAlertMessage = ''

        var ActionUpdateFields = [];

        switch (ActionType) {
            case 1: //Email Alerts
                var TemplateID = parseInt($(this).find('[id*=ddlEmailTemplate]').val());

                $(this).find('[id*=ddlEmailTo]').find('option:selected').each(function (i, selected) {
                    if (vcEmailToType != "") {
                        vcEmailToType += ",";
                    }
                    vcEmailToType += $(selected).val();
                });

                break;
            case 2: //Assign Action Items
                TemplateID = parseInt($(this).find('[id*=ddlActionItemTemplate]').val());
                TicklerActionAssignedTo = parseInt($(this).find('[id*=ddlActionItemAssignTo]').val());

                break;
            case 3: //Update Fields

                $(this).find('.UpdateFields').each(function (j) {
                    var Column = $(this).find('[id*=ddlUFColumns]').val();

                    var FilterValue = "";

                    SelectColumn = findInColumnList('ID', Column);

                    if (SelectColumn.vcAssociatedControlType == 'SelectBox' || SelectColumn.vcAssociatedControlType == 'CheckBox') {
                        FilterValue = $(this).find("[id*=ddlUFValue]").val();
                    }
                    else if (SelectColumn.vcAssociatedControlType == 'DateField') {
                        var DateValue = $.trim($(this).find('[id*=txtUFValue_calDate]').val());

                        if (!isDate(DateValue, $('#hfValidDateFormat').val())) {
                            return false;
                        }

                        FilterValue = DateValue;
                    }
                    else {
                        FilterValue = $(this).find('[id*=txtUFValue]').val();
                    }

                    ActionUpdateFields.push({ 'tintModuleType': 0, 'Column': Column, 'vcValue': FilterValue });
                });

                break;
            case 4: //Alert Message
                vcAlertMessage = $.trim($(this).find('[id*=txtAlertMessage]').val());

                break;
        }

        responseJson.Actions.push({ 'ActionType': ActionType, 'TemplateID': TemplateID, 'vcEmailToType': vcEmailToType, 'TicklerActionAssignedTo': TicklerActionAssignedTo, 'vcAlertMessage': vcAlertMessage, 'ActionUpdateFields': ActionUpdateFields });
    });


    console.log(responseJson);

    $.ajax({
        type: "POST",
        async: false,
        url: "../common/WorkFlowService.asmx/SaveWFData",
        data: "{lngWFID:'" + $("#hfWFID").val() + "',strReportJson:'" + JSON.stringify(responseJson) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            return true;
        },
        failure: function (response) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            return false;
        },
        complete: function () {
        }
    });

    return true;
}

function changeTriggerEvents() {
    if ($('#rbFieldUpdate').is(':checked')) {
        $('#dvFieldUpdateList').show();

        var spnFilterEditMode = $("#dvFieldUpdateList");
        $(spnFilterEditMode).text('');

        var ddlFieldUpdate1 = $('<select id="ddlFieldUpdate1" multiple="multiple"></select>');


        $.each(arrFormFields, function (i, data) {
            console.log('sachin-Allowfitering');
            console.log(data.bitAllowFiltering)
            console.log('sachin-bitCustom');
            console.log(data.bitCustom);
            if (!data.bitCustom && data.bitAllowFiltering)
                console.log('loop-sachin-Allowfitering');
            console.log(data.bitAllowFiltering)
            console.log('loop-sachin-bitCustom');
            console.log(data.bitCustom);
            ddlFieldUpdate1.append('<option value="' + data.ID + '">' + data.vcFieldName + '</option>')
        });

        if (responseJson.TriggerFieldList != null) {
            $.each(responseJson.TriggerFieldList, function (i, selValue) {
                ddlFieldUpdate1.find("option[value='" + selValue + "']").prop("selected", true);
            });
        }

        spnFilterEditMode.append(ddlFieldUpdate1);
        $("#ddlFieldUpdate1").dropdownchecklist({ width: 300, maxDropHeight: 250 });
    }
    else {
        $('#dvFieldUpdateList').hide();
    }
}


function JQueryOnLoad() {
    $("[name='gnTriggerEvents']").change(function () {
        //console.log($(this).attr('id'));
        changeTriggerEvents();
    });

    //changeTriggerEvents();

    $("#dvRuleCriteria").delegate('.filterItemContainer [id*=ddlFilterEditModeColumnList]', 'change', function (e) {

        var filteOperator = $(this).parent().find('[id*=ddlFilterEditModeOperator]');
        $(filteOperator).empty();

        SelectColumn = findInColumnList('ID', $(this).val());

        var filterOperatorList = typeFilterOperatorsMap[SelectColumn.vcAssociatedControlType + "_" + SelectColumn.vcFieldDataType]
        $.each(filterOperatorList, function (i, OperatorType) {
            $(filteOperator).append('<option value="' + OperatorType + '">' + filterConditionOperators[OperatorType] + '</option>')
        });


        var spnFilterEditMode = $(this).parent().find('[id*=spnFilterEditMode]');
        $(spnFilterEditMode).text('');

        var IdIndex = $(this).parent().attr("id");

        if (SelectColumn.vcAssociatedControlType == 'SelectBox' || SelectColumn.vcAssociatedControlType == 'CheckBox') {
            var ddlData = GetDropDownData(SelectColumn.numListID, SelectColumn.vcListItemType, SelectColumn.vcDbColumnName, SelectColumn.vcAssociatedControlType);

            var ddlFilterEditMode = $('<select multiple="multiple"></select>').attr("id", "ddlFilterEditMode_" + IdIndex);

            $.each(ddlData, function (i, data) {
                ddlFilterEditMode.append('<option value="' + data.numID + '">' + data.vcData + '</option>')
            });

            spnFilterEditMode.append(ddlFilterEditMode);
            $("#ddlFilterEditMode_" + IdIndex).dropdownchecklist({ width: 180, maxDropHeight: 350 });
        }
        else if (SelectColumn.vcAssociatedControlType == 'DateField') {
            var txtFilter_calDate = $('<input type="text" autocomplete="off"/>').attr("id", "txtFilter_calDate_" + IdIndex);

            spnFilterEditMode.append(txtFilter_calDate);
            spnFilterEditMode.append('&nbsp;&nbsp;');

            var imgFilter_calDate = $('<img class="hyperlink" style="border-width:0px;" src="../Images/Calender.gif">').attr("id", "imgFilter_calDate_" + IdIndex);

            spnFilterEditMode.append(imgFilter_calDate);
            setupCal('imgFilter_calDate_' + IdIndex, 'txtFilter_calDate_' + IdIndex, $('#hfDateFormat').val(), false);
        }
        else if (SelectColumn.vcFieldDataType == 'M') {
            var txtFilterEditModeValue = $('<input type="text" autocomplete="off" class="required_money {required:false ,number:true, maxlength:12, messages:{number:\'provide valid value!\'}} money"/>').attr("id", "txtFilterEditModeValue_" + IdIndex);

            spnFilterEditMode.append(txtFilterEditModeValue);
        }
        else if (SelectColumn.vcFieldDataType == 'N') {
            var txtFilterEditModeValue = $('<input type="text" autocomplete="off" class="required_integer {required:false ,number:true, maxlength:12, messages:{number:\'provide valid value!\'}} money"/>').attr("id", "txtFilterEditModeValue_" + IdIndex);

            spnFilterEditMode.append(txtFilterEditModeValue);
        }
        else {
            var txtFilterEditModeValue = $('<input type="text" autocomplete="off"/>').attr("id", "txtFilterEditModeValue_" + IdIndex);

            spnFilterEditMode.append(txtFilterEditModeValue);
        }
    });

    $("#dvRuleCriteria").delegate('.filterItemContainer [id*=lnkDelete]', 'click', function (e) {
        $(this).parent().remove();

        $('#dvRuleCriteria').find('.filterItemContainer').each(function (i) {
            $(this).find('.spnRowNo').text((i + 1) + '.');
            //$(this).find('[id*=ddlANDORCondition]').attr("id", "ddlANDORCondition_" + i);
            //$(this).find('[id*=ddlFilterEditModeColumnList]').attr("id", "ddlFilterEditModeColumnList_" + i);
            //$(this).find('[id*=lnkDelete]').attr("id", "lnkDelete_" + i);
        });
    });

    $("#dvRuleAction").delegate('.actionContainer [id*=ddlActionsList]', 'change', function (e) {
        ActionType = parseInt($(this).val());
        var IdIndex = $(this).parent().attr("id");

        var dvActionSetting = $(this).parent().find('[id*=dvActionSetting]');
        $(dvActionSetting).text('');

        switch (ActionType) {
            case 1: //Email Alerts

                dvActionSetting.append("Email Template:");

                var ddlEmailTemplate = $('<select></select>').attr("id", "ddlEmailTemplate_" + IdIndex);
                ddlEmailTemplate.append('<option value="0">--Select--</option>')

                arrEmailTemplate = GetEmailTemplateList();
                $.each(arrEmailTemplate, function (i, data) {
                    ddlEmailTemplate.append('<option value="' + data.numGenericDocID + '">' + data.vcDocName + '</option>')
                });

                dvActionSetting.append(ddlEmailTemplate);

                dvActionSetting.append('&nbsp;&nbsp;');

                dvActionSetting.append("To:");

                var ddlEmailTo = $('<select multiple="multiple"></select>').attr("id", "ddlEmailTo_" + IdIndex);
                ddlEmailTo.append('<option value="1">Owner of trigger record</option>')
                ddlEmailTo.append('<option value="2">Assignee of trigger record</option>')
                ddlEmailTo.append('<option value="3">Primary contact of trigger record</option>')
                dvActionSetting.append(ddlEmailTo);

                $("#ddlEmailTo_" + IdIndex).dropdownchecklist({ width: 185, maxDropHeight: 65 });

                break;
            case 2: //Assign Action Items
                dvActionSetting.append("Action Item Template:");

                var ddlActionItemTemplate = $('<select></select>').attr("id", "ddlActionItemTemplate_" + IdIndex);
                ddlActionItemTemplate.append('<option value="0">--Select--</option>')

                arrActionItem = GetActionItemTemplateList();
                $.each(arrActionItem, function (i, data) {
                    ddlActionItemTemplate.append('<option value="' + data.RowID + '">' + data.TemplateName + '</option>')
                });

                dvActionSetting.append(ddlActionItemTemplate);

                dvActionSetting.append('&nbsp;&nbsp;');

                dvActionSetting.append("Assign To:");

                var ddlActionItemAssignTo = $('<select></select>').attr("id", "ddlActionItemAssignTo_" + IdIndex);
                ddlActionItemAssignTo.append('<option value="1">Owner of trigger record</option>')
                ddlActionItemAssignTo.append('<option value="2">Assignee of trigger record</option>')
                dvActionSetting.append(ddlActionItemAssignTo);

                break;
            case 3: //Update Fields
                var lnkAddUF = $('<a href="#">Add Fields</a>').attr("id", "lnkAddUF_" + IdIndex);
                dvActionSetting.append(lnkAddUF);
                dvActionSetting.append('<br/>');

                AddActionUF(dvActionSetting);
                break;
            case 4: //Alert Message
                var txtAlertMessage = $('<textarea type="text" cols="20" rows="2"/>').attr("id", "txtAlertMessage_" + IdIndex);
                dvActionSetting.append(txtAlertMessage);
                dvActionSetting.append("<font>The message will display in a pop-up when the rule criteria for the trigger event occurs.</font>");

                break;
        }
    });

    $("#dvRuleAction").delegate('.actionContainer [id*=lnkDeleteAction]', 'click', function (e) {
        $(this).parent().remove();

        $('#dvRuleAction').find('.actionContainer').each(function (i) {
            $(this).find('.spnRowNo').text((i + 1) + '.');
        });
    });

    $("#dvRuleAction").delegate('.actionContainer [id*=lnkAddUF]', 'click', function (e) {
        dvActionSetting = $(this).parent();
        AddActionUF(dvActionSetting);
    });


    $("#dvRuleAction").delegate('.UpdateFields [id*=lnkDeleteUF]', 'click', function (e) {
        var dvActionSetting = $(this).parent().parent();

        $(this).parent().remove();

        $(dvActionSetting).find('.UpdateFields').each(function (i) {
            $(this).find('.spnUFRowNo').text((i + 1) + '.');
        });
    });

    $("#dvRuleAction").delegate('.UpdateFields [id*=ddlUFColumns]', 'change', function (e) {

        SelectColumn = findInColumnList('ID', $(this).val());

        var spnValueUF = $(this).parent().find('[id*=spnValueUF]');
        $(spnValueUF).text('');

        var IdIndex = $(this).parent().attr("id");//.index() - 2;

        if (SelectColumn.vcAssociatedControlType == 'SelectBox' || SelectColumn.vcAssociatedControlType == 'CheckBox') {
            var ddlData = GetDropDownData(SelectColumn.numListID, SelectColumn.vcListItemType, SelectColumn.vcDbColumnName, SelectColumn.vcAssociatedControlType);

            var ddlUFValue = $('<select></select>').attr("id", "ddlUFValue_" + IdIndex);

            $.each(ddlData, function (i, data) {
                if (data.numID == '0' && data.vcData == '--None--')
                { }
                else { ddlUFValue.append('<option value="' + data.numID + '">' + data.vcData + '</option>'); }
            });

            spnValueUF.append(ddlUFValue);
        }
        else if (SelectColumn.vcAssociatedControlType == 'DateField') {
            var txtUFValue_calDate = $('<input type="text" autocomplete="off"/>').attr("id", "txtUFValue_calDate_" + IdIndex);

            spnValueUF.append(txtUFValue_calDate);
            spnValueUF.append('&nbsp;&nbsp;');

            var imgUFValue_calDate = $('<img class="hyperlink" style="border-width:0px;" src="../Images/Calender.gif">').attr("id", "imgUFValue_calDate_" + IdIndex);

            spnValueUF.append(imgUFValue_calDate);
            setupCal('imgUFValue_calDate_' + IdIndex, 'txtUFValue_calDate_' + IdIndex, $('#hfDateFormat').val(), false);
        }
        else if (SelectColumn.vcFieldDataType == 'M') {
            var txtUFValue = $('<input type="text" autocomplete="off" class="required_money {required:false ,number:true, maxlength:12, messages:{number:\'provide valid value!\'}} money"/>').attr("id", "txtUFValue_" + IdIndex);

            spnValueUF.append(txtUFValue);
        }
        else if (SelectColumn.vcFieldDataType == 'N') {
            var txtUFValue = $('<input type="text" autocomplete="off" class="required_integer {required:false ,number:true, maxlength:12, messages:{number:\'provide valid value!\'}} money"/>').attr("id", "txtUFValue_" + IdIndex);

            spnValueUF.append(txtUFValue);
        }
        else {
            var txtUFValue = $('<input type="text" autocomplete="off"/>').attr("id", "txtUFValue_" + IdIndex);

            spnValueUF.append(txtUFValue);
        }
    });
}

function AddActionUF(dvActionSetting) {
    var TotalUF = $(dvActionSetting).find(".UpdateFields").length;

    if (TotalUF == 5) {
        alert("Allowed to add mximum 5 Update Fields.");
        return false;
    }

    ControlID = 0;
    if (TotalUF != 0) {
        ControlID = Number($(dvActionSetting).find(".UpdateFields:last-child").attr("id")) + 1;
    }

    var UpdateFields = $('<div></div>').addClass('UpdateFields').attr("id", ControlID);
    UpdateFields.append("<span class='spnUFRowNo'>" + (TotalUF + 1) + ".</span>");

    UpdateFields.append('&nbsp;&nbsp;');

    var ddlUFColumns = $('<select></select>').attr("id", "ddlUFColumns_" + ControlID);

    $.each(arrFormFields, function (i, data) {
        if (data.bitAllowEdit)
            ddlUFColumns.append('<option value="' + data.ID + '">' + data.vcFieldName + '</option>')
    });

    UpdateFields.append(ddlUFColumns);

    var spnValueUF = $('<span></span>').attr("id", "spnValueUF_" + ControlID);    UpdateFields.append('&nbsp;&nbsp;');
    UpdateFields.append(spnValueUF);

    var lnkDeleteUF = $('<a href="#">Delete</a>').attr("id", "lnkDeleteUF_" + ControlID);
    UpdateFields.append('&nbsp;&nbsp;');
    UpdateFields.append(lnkDeleteUF);

    dvActionSetting.append(UpdateFields);

    $(dvActionSetting).find("#ddlUFColumns_" + ControlID).trigger('change');
}


function Step1Validate() {
    if (document.getElementById("ddlForm").value == 0) {
        alert("Select Workflow Module");
        document.getElementById("ddlForm").focus();
        return false;
    }

    if ($.trim(document.getElementById("txtWFName").value) == '') {
        alert("Enter Workflow Name");
        document.getElementById("txtWFName").focus();
        return false;
    }

    SelectTabByValue("Step2");

    if (iSelectedForm != document.getElementById("ddlForm").value) {
        arrFormFields = GetFormFieldList();

        $("#dvRuleCriteria").text('');
        $("#dvRuleAction").text('');
        $('#rbCreate').prop('checked', true);
    }

    iSelectedForm = document.getElementById("ddlForm").value;
    return false;
}

function Step2Validate() {
    if ($('#rbFieldUpdate').is(':checked')) {
        var colValue = "";
        var colText = "";

        $('#ddlFieldUpdate1').find('option:selected').each(function (i, selected) {
            //console.log($(selected).val());
            //console.log($(selected).text());

            if (colValue != "") {
                colValue += ",";
                colText += ",";
            }
            colValue += $(selected).val();
            colText += $(selected).text();
        });

        if (colValue == "") {
            alert("No Field is selected for Trigger Events.");
            return false;
        }
    }

    SelectTabByValue("Step3");

    return false;
}

function Step3Validate() {
    SelectTabByValue("Step4");

    return false;
}

function Step4Validate() {
    var ActionCount = 0;
    var boolActionError = false;
    $('#dvRuleAction').find('.actionContainer').each(function (i) {
        var ActionType = parseInt($(this).find('[id*=ddlActionsList]').val());
        var IdIndex = i + 1;

        switch (ActionType) {
            case 1: //Email Alerts

                var ddlEmailTemplate = parseInt($(this).find('[id*=ddlEmailTemplate]').val());

                if (ddlEmailTemplate == 0) {
                    boolActionError = true;
                    alert("Select Email Template for action #" + IdIndex);
                    return false;
                }

                var colValue = "";
                var colText = "";

                $(this).find('[id*=ddlEmailTo]').find('option:selected').each(function (i, selected) {
                    if (colValue != "") {
                        colValue += ",";
                        colText += ",";
                    }
                    colValue += $(selected).val();
                    colText += $(selected).text();
                });

                if (colValue == "") {
                    boolActionError = true;
                    alert("Select Email To for action #" + IdIndex);
                    return false;
                }

                ActionCount += 1;
                break;
            case 2: //Assign Action Items
                var ddlActionItemTemplate = parseInt($(this).find('[id*=ddlActionItemTemplate]').val());

                if (ddlActionItemTemplate == 0) {
                    boolActionError = true;
                    alert("Select Action Item Template for action #" + IdIndex);
                    return false;
                }

                ActionCount += 1;
                break;
            case 3: //Update Fields

                ActionCount += 1;
                break;
            case 4: //Alert Message
                var txtAlertMessage = $(this).find('[id*=txtAlertMessage]').val();

                if ($.trim(txtAlertMessage) == '') {
                    boolActionError = true;
                    alert("Enter alert message for action #" + IdIndex);
                    return false;
                }

                ActionCount += 1;
                break;
        }
    });

    if (boolActionError == false && ActionCount == 0) {
        alert("No Action is addedd for Workflow.");
        return false;
    }
    else if (boolActionError == true)
        return false;
    else
        return SaveWFData();
}

function AddRuleCondition() {

    var TotalCondition = $("#dvRuleCriteria .filterItemContainer").length;

    if (TotalCondition == 5) {
        alert("Allowed to add mximum 5 condition");
        return false;
    }

    ControlID = 0;
    if (TotalCondition != 0) {
        ControlID = Number($("#dvRuleCriteria .filterItemContainer:last-child").attr("id")) + 1;
    }

    var filterItemContainer = $('<div></div>').addClass('filterItemContainer').attr("id", ControlID);

    filterItemContainer.append("<span class='spnRowNo'>" + (TotalCondition + 1) + ".</span>");

    filterItemContainer.append('&nbsp;&nbsp;');

    filterItemContainer.append('&nbsp;&nbsp;');

    if (ControlID > 0) {
        var ddlANDORCondition = $('<select></select>').attr("id", "ddlANDORCondition_" + (ControlID));
        ddlANDORCondition.append('<option value="AND">AND</option>');
        ddlANDORCondition.append('<option value="OR">OR</option>');

        filterItemContainer.append(ddlANDORCondition);
    }

    var ddlFilterEditModeColumnList = $('<select></select>').attr("id", "ddlFilterEditModeColumnList_" + (ControlID));

    $.each(arrFormFields, function (i, data) {
        ddlFilterEditModeColumnList.append('<option value="' + data.ID + '">' + data.vcFieldName + '</option>')
    });

    filterItemContainer.append('&nbsp;&nbsp;');
    filterItemContainer.append(ddlFilterEditModeColumnList);

    var filteOperator = $('<select></select>').attr("id", "ddlFilterEditModeOperator_" + (ControlID));
    filterItemContainer.append('&nbsp;&nbsp;');
    filterItemContainer.append(filteOperator);

    var spnFilterEditMode = $('<span></span>').attr("id", "spnFilterEditMode_" + (ControlID));

    filterItemContainer.append('&nbsp;&nbsp;');
    filterItemContainer.append(spnFilterEditMode);

    var lnkDelete = $('<a href="#">Delete</a>').attr("id", "lnkDelete_" + (ControlID));
    filterItemContainer.append('&nbsp;&nbsp;');
    filterItemContainer.append(lnkDelete);

    $("#dvRuleCriteria").append(filterItemContainer);

    $("#dvRuleCriteria").find("#ddlFilterEditModeColumnList_" + (ControlID)).trigger('change');

    return false;
}


function AddRuleAction() {

    var TotalCondition = $("#dvRuleAction .actionContainer").length;

    if (TotalCondition == 5) {
        alert("Allowed to add mximum 5 action");
        return false;
    }

    ControlID = 0;
    if (TotalCondition != 0) {
        ControlID = Number($("#dvRuleAction .actionContainer:last-child").attr("id")) + 1;
    }

    var actionContainer = $('<div></div>').addClass('actionContainer').attr("id", ControlID);

    actionContainer.append("<span class='spnRowNo'>" + (TotalCondition + 1) + ".</span>");

    actionContainer.append('&nbsp;&nbsp;');

    var ddlActionsList = $('<select></select>').attr("id", "ddlActionsList_" + (ControlID));

    $.each(arrActionList, function (i, data) {
        ddlActionsList.append('<option value="' + data.ID + '">' + data.vcActionType + '</option>')
    });

    actionContainer.append('&nbsp;&nbsp;');
    actionContainer.append(ddlActionsList);

    var lnkDeleteAction = $('<a href="#">Delete</a>').attr("id", "lnkDeleteAction_" + (ControlID));
    actionContainer.append('&nbsp;&nbsp;');
    actionContainer.append(lnkDeleteAction);

    var dvActionSetting = $('<div style="padding-left:25px;"></div>').attr("id", "dvActionSetting_" + (ControlID));
    actionContainer.append('&nbsp;&nbsp;');
    actionContainer.append(dvActionSetting);

    $("#dvRuleAction").append(actionContainer);

    return false;
}