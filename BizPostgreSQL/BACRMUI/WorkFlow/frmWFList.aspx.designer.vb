﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Workflows
    
    Partial Public Class frmWFList
        
        '''<summary>
        '''ddlForm control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlForm As Global.System.Web.UI.WebControls.DropDownList
        
        '''<summary>
        '''txtWFName control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtWFName As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''btnSearch control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnSearch As Global.System.Web.UI.WebControls.Button
        
        '''<summary>
        '''btnNewWF control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnNewWF As Global.System.Web.UI.WebControls.Button
        
        '''<summary>
        '''btnRefresh control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnRefresh As Global.System.Web.UI.WebControls.Button
        
        '''<summary>
        '''btnOPenHistory control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnOPenHistory As Global.System.Web.UI.WebControls.Button
        
        '''<summary>
        '''bizPager control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents bizPager As Global.Wuqi.Webdiyer.AspNetPager
        
        '''<summary>
        '''UpdateProgress control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress
        
        '''<summary>
        '''frmBizSorting2 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents frmBizSorting2 As Global.frmBizSorting
        
        '''<summary>
        '''btnRunService control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnRunService As Global.System.Web.UI.WebControls.Button
        
        '''<summary>
        '''gvWFList control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents gvWFList As Global.System.Web.UI.WebControls.GridView
        
        '''<summary>
        '''divShowEmailDesc control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divShowEmailDesc As Global.System.Web.UI.HtmlControls.HtmlGenericControl
        
        '''<summary>
        '''btnCloseEmailDesc control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnCloseEmailDesc As Global.System.Web.UI.WebControls.Button
        
        '''<summary>
        '''lblEmailSubject control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblEmailSubject As Global.System.Web.UI.WebControls.Label
        
        '''<summary>
        '''lblSubject control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblSubject As Global.System.Web.UI.WebControls.Label
        
        '''<summary>
        '''lblEmailDesc control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblEmailDesc As Global.System.Web.UI.WebControls.Label
        
        '''<summary>
        '''litDesc control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents litDesc As Global.System.Web.UI.WebControls.Literal
        
        '''<summary>
        '''txtSortColumn control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSortColumn As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''txtSortOrder control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSortOrder As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''txtTotalPage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtTotalPage As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''txtTotalRecords control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtTotalRecords As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''txtSortChar control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSortChar As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''txtCurrrentPage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtCurrrentPage As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''btnGo1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnGo1 As Global.System.Web.UI.WebControls.Button
    End Class
End Namespace
