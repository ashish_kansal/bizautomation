﻿'Author:Sachin Sadhu'
'Date:17thFeb2014
'Purpose:Save WorkFlow details

Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Projects
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Web.UI.WebControls
Imports System.Text

Namespace BACRM.UserInterface.Workflows
    Public Class frmSetConditions
        Inherits BACRMPage
        Public dtConditions As New DataTable()
        Dim strCondition As String = Nothing
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not IsPostBack Then
                ViewState("CurrentCondTable") = Nothing
                SetInitialRow()
                BindConditionsDdl()
                GetWFFormFieldList(CCommon.ToLong(Session("numFormID")))
                'hfDateFormat.Value = CCommon.GetDateFormat()
                ' hfValidDateFormat.Value = CCommon.GetValidationDateFormat()
            End If

        End Sub
#Region "Repeater"
        Private Sub SetInitialRow()
            Try
                'Dim dtConditions As New DataTable()
                Dim dr As DataRow = Nothing
                CCommon.AddColumnsToDataTable(dtConditions, "RowNumber,vcANDOR,ID,vcFieldName,numEquation,vcEquation,numID,VcData,vcMessge")
                ViewState("CurrentCondTable") = dtConditions
                Session("CurrentCondTable") = dtConditions
                Session("strCondition") = Nothing
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Private Sub AddNewRowToGrid()
            Try

                If ViewState("CurrentCondTable") IsNot Nothing Then

                    Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentCondTable"), DataTable)
                    Dim drCurrentRow As DataRow = Nothing

                    'If dtCurrentTable.Rows.Count > 0 Then

                    If hfWFCodeForEdit.Value = "0" Then
                        drCurrentRow = dtCurrentTable.NewRow()
                        drCurrentRow("RowNumber") = dtCurrentTable.Rows.Count + 1
                    Else
                        drCurrentRow = dtCurrentTable.Rows(dtCurrentTable.Rows.IndexOf(dtCurrentTable.Select("RowNumber=" + hfWFCodeForEdit.Value)(0)))
                        drCurrentRow("RowNumber") = Convert.ToInt32(hfWFCodeForEdit.Value)
                    End If

                    'RowNumber, vcANDOR, ID, vcFieldName, numEquation, vcEquation, numID, VcData, vcMessge
                    Dim sbValue As New StringBuilder()
                    Dim sbValueText As New StringBuilder()
                    Dim collection As IList(Of RadListBoxItem) = radValue.CheckedItems
                    For Each item As RadListBoxItem In collection
                        sbValue.Append(item.Value & ",")
                        sbValueText.Append(item.Text & ",")
                    Next

                    drCurrentRow("ID") = ddlCondition.SelectedValue
                    drCurrentRow("vcFieldName") = ddlCondition.SelectedItem
                    drCurrentRow("numEquation") = ddlSign.SelectedValue
                    drCurrentRow("vcEquation") = ddlSign.SelectedItem
                   
                    If ddlCondition.SelectedIndex >= 0 Then
                        Dim dtFieldsList As DataTable
                        dtFieldsList = DirectCast(ViewState("Fields"), DataTable)
                        Dim drFormRow As DataRow
                        Dim strDdlValue As String() = ddlCondition.SelectedValue.Split("_")
                        drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))
                        If drFormRow("vcAssociatedControlType") = "SelectBox" Then
                             drCurrentRow("numID") = CCommon.ToString(sbValue)
                            drCurrentRow("VcData") = CCommon.ToString(sbValueText)
                        ElseIf drFormRow("vcAssociatedControlType") = "DateField" Then
                            drCurrentRow("numID") = calDate.SelectedDate
                            drCurrentRow("VcData") = calDate.SelectedDate
                        ElseIf drFormRow("vcFieldDataType") = "M" Then
                            drCurrentRow("numID") = txtValueMoney.Text
                            drCurrentRow("VcData") = txtValueMoney.Text
                        ElseIf drFormRow("vcFieldDataType") = "N" Then
                            drCurrentRow("numID") = txtNvalue.Text
                            drCurrentRow("VcData") = txtNvalue.Text

                        Else
                            drCurrentRow("numID") = txtValue.Text
                            drCurrentRow("VcData") = txtValue.Text

                        End If
                    End If

                    If dtCurrentTable.Rows.Count > 0 Then
                        drCurrentRow("vcANDOR") = ddlANDORCondition.SelectedValue
                        drCurrentRow("vcMessge") = "<b>" & drCurrentRow("vcANDOR") & "  </b>  <b>  " & drCurrentRow("vcFieldName") & "</b>  " & drCurrentRow("vcEquation") & " <b>" & drCurrentRow("VcData") & "</b>"


                    Else
                        drCurrentRow("vcANDOR") = Nothing
                        drCurrentRow("vcMessge") = "When <b>" & drCurrentRow("vcFieldName") & " </b>  " & drCurrentRow("vcEquation") & " <b>" & drCurrentRow("VcData") & "</b>"
                    End If
                    strCondition = CCommon.ToString(Session("strCondition"))
                    strCondition = strCondition & "&nbsp;" & drCurrentRow("vcMessge")
                    Session("strCondition") = strCondition
                    'drCurrentRow("vcMessge") = drCurrentRow("vcAction") & "&nbsp; Using Email Template <b>" & drCurrentRow("vcDocName") & "</b> To <b>" & drCurrentRow("vcReceiverName") & "</b>"
                    If hfWFCodeForEdit.Value = "0" Then
                        dtCurrentTable.Rows.Add(drCurrentRow)
                    Else
                        dtCurrentTable.AcceptChanges()
                        hfWFCodeForEdit.Value = "0"
                    End If

                    If dtCurrentTable.Rows.Count > 0 Then
                        dvANDOR.Style.Add("display", "block")
                    Else
                        dvANDOR.Style.Add("display", "none")
                    End If

                    'Store the current data to ViewState for future reference
                    ViewState("CurrentCondTable") = dtCurrentTable
                    Session("CurrentCondTable") = dtCurrentTable
                    'Rebind the Grid with the current data to reflect changes
                    rprConditions.DataSource = dtCurrentTable
                    rprConditions.DataBind()
                    'End If
                Else

                    Response.Write("ViewState is null")
                End If
                'Set Previous Data on Postbacks
                'SetPreviousData()
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Protected Sub rprConditions_ItemCommand(src As Object, e As RepeaterCommandEventArgs)
            Try
                'ss//ClearSampleControls();
                Dim dtConditions As DataTable
                If e.CommandName = "Edit" Then
                    'add Sample

                    dtConditions = DirectCast(ViewState("CurrentCondTable"), DataTable)
                    Dim drCurrentRow As DataRow = dtConditions.Rows(dtConditions.Rows.IndexOf(dtConditions.Select("RowNumber=" + e.CommandArgument)(0)))
                    hfWFCodeForEdit.Value = drCurrentRow("RowNumber").ToString()
                    'RowNumber, vcANDOR, ID, vcFieldName, numEquation, vcEquation, numID, VcData, vcMessge

                    ddlANDORCondition.SelectedValue = drCurrentRow("vcANDOR")
                    ddlCondition.SelectedValue = drCurrentRow("ID")
                    ddlCondition_OnSelectedIndexChanged(ddlCondition, New EventArgs())
                    ddlSign.SelectedValue = drCurrentRow("numEquation")


                    Dim strValueText As String() = drCurrentRow("VcData").Split(",")
                    For k = 0 To strValueText.Length - 1
                        For Each radItem As RadListBoxItem In radValue.Items
                            If radItem.Text = strValueText(k) Then
                                radItem.Checked = True
                            End If
                        Next
                    Next
                    txtNvalue.Text = drCurrentRow("VcData")
                    txtValue.Text = drCurrentRow("VcData")
                    txtValueMoney.Text = drCurrentRow("VcData")
                    calDate.SelectedDate = CCommon.GetDateFormat(drCurrentRow("VcData"))
                ElseIf e.CommandName = "Delete" Then
                    'delete selected row

                    dtConditions = DirectCast(ViewState("CurrentCondTable"), DataTable)

                    dtConditions.Rows.RemoveAt(dtConditions.Rows.IndexOf(dtConditions.Select("RowNumber=" + e.CommandArgument)(0)))
                    dtConditions.AcceptChanges()
                    If dtConditions.Rows.Count > 0 Then
                        dvANDOR.Style.Add("display", "block")
                    Else
                        dvANDOR.Style.Add("display", "none")
                    End If

                    ViewState("CurrentCondTable") = dtConditions
                    Session("CurrentCondTable") = dtConditions
                    rprConditions.DataSource = dtConditions
                    rprConditions.DataBind()
                    'ltrTempEntry.Text = "Record is deleted."
                    ' hfLabourCode.Value = (Convert.ToInt32(hfLabourCode.Value) - 1).ToString()
                    'do nothing
                Else

                End If

            Catch ex As Exception
            End Try

        End Sub
#End Region
#Region "Button Events:Add"
        Protected Sub ButtonAdd_Click(sender As Object, e As EventArgs)
            Try
                AddNewRowToGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
#End Region

#Region "DropDownLists"

        Sub BindConditionsDdl()
            Try
                Dim arr As New ArrayList()

                arr.Add(New ListItem("AND", "AND"))
                arr.Add(New ListItem("OR", "OR"))

                ddlANDORCondition.Items.Clear()
                For Each item As ListItem In arr
                    ddlANDORCondition.Items.Add(item)
                Next
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Public Sub GetDropDownData(ByVal numListID As Long, ByVal vcListItemType As String, vcDbColumnName As String, vcAssociatedControlType As String)
            Try
                Dim strData As String = ""
                Dim dtData As New DataTable
                Dim objCommon As New CCommon
                objCommon.DomainID = Session("DomainID")

                If vcAssociatedControlType = "CheckBox" Then
                    dtData = New DataTable
                    dtData.Columns.Add("numID")
                    dtData.Columns.Add("vcData")
                    Dim dr1 As DataRow
                    dr1 = dtData.NewRow
                    dr1("numID") = "1"
                    dr1("vcData") = "Yes"
                    dtData.Rows.Add(dr1)

                    dr1 = dtData.NewRow
                    dr1("numID") = "0"
                    dr1("vcData") = "No"
                    dtData.Rows.Add(dr1)
                ElseIf vcDbColumnName = "tintSource" Then
                    dtData = objCommon.GetOpportunitySource()
                    dtData.Columns(0).ColumnName = "numID"
                ElseIf vcDbColumnName = "numProjectID" Then
                    Dim objProject As New Project
                    objProject.DomainID = Session("DomainID")
                    dtData = objProject.GetOpenProject()

                    dtData.Columns(0).ColumnName = "numID"
                    dtData.Columns(1).ColumnName = "vcData"

                    Dim dr1 As DataRow
                    dr1 = dtData.NewRow
                    dr1("numID") = "0"
                    dr1("vcData") = "--None--"
                    dtData.Rows.Add(dr1)
                Else
                    dtData = objCommon.GetDropDownValue(numListID, vcListItemType, vcDbColumnName)
                End If
                radValue.DataSource = dtData
                radValue.DataValueField = "numID"
                radValue.DataTextField = "vcData"
                radValue.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindEquations()
            Try
                Dim arr As New ArrayList()



                '            "TextBox_V": ["eq", "ne", "co", "nc", "sw"],

                Dim dtFieldsList As DataTable
                dtFieldsList = DirectCast(ViewState("Fields"), DataTable)
                Dim drFormRow As DataRow
                Dim strDdlValue As String() = ddlCondition.SelectedValue.Split("_")
                drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))
                If drFormRow("vcAssociatedControlType") = "SelectBox" Then

                    arr.Add(New ListItem("equals", "eq"))
                    arr.Add(New ListItem("not equal to", "ne"))

                ElseIf drFormRow("vcAssociatedControlType") = "DateField" Then
                    arr.Add(New ListItem("equals", "eq"))
                    arr.Add(New ListItem("not equal to", "ne"))
                    arr.Add(New ListItem("greater than", "gt"))
                    arr.Add(New ListItem("greater or equal", "ge"))
                    arr.Add(New ListItem("less than", "lt"))
                    arr.Add(New ListItem("less or equal", "le"))
                ElseIf drFormRow("vcFieldDataType") = "M" Then
                    arr.Add(New ListItem("equals", "eq"))
                    arr.Add(New ListItem("not equal to", "ne"))
                    arr.Add(New ListItem("greater than", "gt"))
                    arr.Add(New ListItem("greater or equal", "ge"))
                    arr.Add(New ListItem("less than", "lt"))
                    arr.Add(New ListItem("less or equal", "le"))
                ElseIf drFormRow("vcFieldDataType") = "N" Then
                    arr.Add(New ListItem("equals", "eq"))
                    arr.Add(New ListItem("not equal to", "ne"))
                    arr.Add(New ListItem("greater than", "gt"))
                    arr.Add(New ListItem("greater or equal", "ge"))
                    arr.Add(New ListItem("less than", "lt"))
                    arr.Add(New ListItem("less or equal", "le"))
                    '  arr.Add(New ListItem("co", "contains"))
                    ' arr.Add(New ListItem("sw", "starts with"))
                    ' arr.Add(New ListItem("nc", "does not contain"))

                Else
                    arr.Add(New ListItem("equals", "eq"))
                    arr.Add(New ListItem("not equal to", "ne"))


                End If
                ddlSign.Items.Clear()
                For Each item As ListItem In arr
                    ddlSign.Items.Add(item)
                Next
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Sub GetWFFormFieldList(ByVal numFormID As Long)
            Try
                Dim objWF As New BACRM.BusinessLogic.Workflow.Workflow
                objWF.DomainID = Session("DomainID")
                objWF.FormID = numFormID

                Dim dtFieldsList As DataTable
                dtFieldsList = objWF.GetWorkFlowFormFieldMaster()

                ddlCondition.DataSource = dtFieldsList
                ddlCondition.DataTextField = "vcFieldName"
                ddlCondition.DataValueField = "ID"
                ViewState("Fields") = dtFieldsList
                ddlCondition.DataBind()
                Dim drFormRow As DataRow

                If ddlCondition.SelectedIndex >= 0 Then
                    'GetDropDownData()
                    Dim strDdlValue As String() = ddlCondition.SelectedValue.Split("_")
                    drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))
                    GetDropDownData(drFormRow("numListID"), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))
                End If


            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Protected Sub ddlCondition_OnSelectedIndexChanged(sender As Object, e As EventArgs)
            If ddlCondition.SelectedIndex >= 0 Then
                BindEquations()
                Dim dtFieldsList As DataTable
                dtFieldsList = DirectCast(ViewState("Fields"), DataTable)
                Dim drFormRow As DataRow
                Dim strDdlValue As String() = ddlCondition.SelectedValue.Split("_")
                drFormRow = dtFieldsList.Rows(dtFieldsList.Rows.IndexOf(dtFieldsList.Select("numFieldID=" + CCommon.ToString(strDdlValue(0)))(0)))
                If drFormRow("vcAssociatedControlType") = "SelectBox" Then
                    radValue.Visible = True
                    calDate.Visible = False
                    txtNvalue.Visible = False
                    txtValueMoney.Visible = False
                    txtValue.Visible = False
                    GetDropDownData(drFormRow("numListID"), drFormRow("vcListItemType"), drFormRow("vcDbColumnName"), drFormRow("vcAssociatedControlType"))
                ElseIf drFormRow("vcAssociatedControlType") = "DateField" Then
                    radValue.Visible = False
                    calDate.Visible = True
                    txtNvalue.Visible = False
                    txtValueMoney.Visible = False
                    txtValue.Visible = False
                ElseIf drFormRow("vcFieldDataType") = "M" Then
                    radValue.Visible = False
                    calDate.Visible = False
                    txtNvalue.Visible = False
                    txtValueMoney.Visible = True
                    txtValue.Visible = False
                    txtValueMoney.Attributes.Add("onkeypress", "CheckNumber(3,event)")
                ElseIf drFormRow("vcFieldDataType") = "N" Then
                    radValue.Visible = False
                    calDate.Visible = False
                    txtNvalue.Visible = True
                    txtNvalue.Attributes.Add("onkeypress", "CheckNumber(3,event)")
                    txtValueMoney.Visible = False
                    txtValue.Visible = False

                Else
                    radValue.Visible = False
                    calDate.Visible = False
                    txtNvalue.Visible = False
                    txtValueMoney.Visible = False
                    txtValue.Visible = True

                End If


            End If

        End Sub

#End Region

    End Class
End Namespace