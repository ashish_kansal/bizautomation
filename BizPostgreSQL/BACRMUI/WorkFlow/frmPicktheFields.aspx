﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPicktheFields.aspx.vb" Inherits="BACRM.UserInterface.Workflows.frmPicktheFields" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Pick the Fields</title>
    <style type="text/css">
        .formLayout {
            background-color: #e8edf1;
            /*border: solid 1px #a1a1a1;*/
            padding: 10px;
            width: 414px;
            float: left;
            height: 150px;
        }

            .formLayout label, .formLayout input {
                display: block;
                width: 170px;
                float: left;
                margin-bottom: 10px;
            }

            .formLayout label, .formLayout textarea {
                display: block;
                width: 166px;
                float: left;
                margin-bottom: 10px;
            }

            .formLayout label, .formLayout select {
                display: block;
                width: 170px;
                float: left;
                margin-bottom: 10px;
            }

        input[type='radio'] {
            display: block;
            width: 15px;
            float: left;
            margin-bottom: 10px;
        }



        .formLayout label {
            text-align: right;
            padding-right: 20px;
        }

        .dvBoxHeader {
            background-color: #d3e3fc;
            border: solid 1px #a1a1a1;
            padding: 10px;
            width: 660px;
            float: left;
            height: 30px;
        }

        .dvBox {
            background-color: #ffffff;
            border: solid 1px #a1a1a1;
            width: 680px;
            float: left;
            height: 345px;
        }

        .dvBoxInner {
            background-color: #ffffff;
            border: solid 1px #a1a1a1;
            padding-top: 10px;
            width: 668px;
            float: left;
            height: 333px;
            padding-left: 10px;
        }

        .dvBoxInnerChild {
            background-color: #ffffff;
            padding-top: 10px;
            width: 680px;
            float: left;
        }

        .dvRptrChild {
            background-color: #ffffff;
            /*padding-top: 10px;*/
            width: 680px;
            float: left;
            height: 25px;
            border: 1px solid #A1A1A1;
        }

        .dvLine {
            border: solid 1px #a1a1a1;
            width: 680px;
        }

        br {
            clear: left;
        }
    </style>
    <script type="text/javascript">
        function Close() {
            window.close()
            return false;
        }
        window.onunload = function () {
            window.opener.location.href = window.opener.location;
        }
    </script>

    <script src="../reports/jqTree/underscore.js" type="text/javascript"></script>
    <script src="../reports/jqTree/ui.dropdownchecklist.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/dateFormat.js" type="text/javascript"></script>
    <script src="js/WorkFlow.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Pick the Fields
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <div style="background-color: #fff">
        <div style="width: 680px; float: left; height: 400px">

            <asp:ScriptManager ID="smListBox" runat="server"></asp:ScriptManager>
            <div class="dvBoxHeader">
                <span style="font-weight: 500; font-size: x-large; float: left">Choose Fields:</span>
                <div style="float: right;">
                     <asp:Button ID="btnSelect" runat="server" Text="Select" BackColor="#4689e2" BorderColor="Black" Font-Bold="true" BorderStyle="Double" OnClick="btnSelect_OnClick" />
                    <asp:Button ID="btnCloseConditions" runat="server" Text="Close" BackColor="#4689e2" BorderColor="Black" Font-Bold="true" BorderStyle="Double" OnClientClick="Close();" />
                </div>
            </div>
            <div class="dvBox">
                <div class="dvBoxInner">
                    <div style="width: 100px; float: left">
                        <asp:Label ID="lblParameterName" runat="server" Text="Choose Fields" Visible="true"></asp:Label>

                    </div>

                    <div style="width: 565px; float: left;" id="Div1" runat="server">
                        <telerik:RadListBox ID="radFields" runat="server" CheckBoxes="true" showcheckall="true" Width="210px"
                            Height="300px">
                        </telerik:RadListBox>

                    </div>





                </div>


            </div>

        </div>




    </div>
</asp:Content>
