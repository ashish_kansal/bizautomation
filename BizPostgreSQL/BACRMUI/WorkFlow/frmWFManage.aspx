﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmWFManage.aspx.vb" Inherits="BACRM.UserInterface.Workflows.frmWFManage" MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
    <title>Workflow & Business Process Automation (BPA)</title>
    <style type="text/css">       

        #resizeDiv {
            padding: 0px !important;
            margin: 0px !important;
        }

        .tblWFDetail td {
            text-align: left;
        }

            .tblWFDetail td:first-child {
                font-weight: bold;
                white-space: normal;
                text-align: right;
                min-width: 150px;
            }

        .tblActionDetail td {
            text-align: left;
        }

            .tblActionDetail td:first-child {
                font-weight: bold;
                white-space: normal;
                text-align: right;
                min-width: 100px;
            }

        .radCSS {
            display: none;
            width: 180px;
        }

        .RadListBox .rlbTemplate {
            display: inline-table!important;
        }

        .box-body {
            padding: 0px;
        }

        div.RadComboBoxDropDown_Vista .rcbSeparator {
            background: none;
            font-family: Arial;
            font-weight: bold;
            font-style: italic;
            font-size: 10pt;
            padding-left: 6px !important;
        }

        div.RadComboBoxDropDown_Vista .rcbItem, div.RadComboBoxDropDown_Vista .rcbHovered {
            padding-left: 20px;
            font-family: Arial;
            font-size: 10pt;
        }

        #table-main {
            background: #fff;
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            bottom: 0px;
            width: 100%;
            height: 100%;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        .overlayContentPopUp {
            z-index: 99;
            margin: 0 auto;
            position: relative;
            top: 25%;
            top: 50%;
            left: 50%;
            margin: -50px 0 0 -100px;
        }

        .overlayUpdateFields {
            z-index: 99;
            margin: 0 auto;
            position: relative;
            top: 50%;
            left: 50%;
            margin: -200px 0 0 -200px;
        }

        .overlayPickFields {
            z-index: 99;
            margin: 0 auto;
            position: relative;
            top: 50%;
            left: 50%;
            margin: -200px 0 0 -200px;
        }

        .overlayDateFields {
            z-index: 99;
            margin: 0 auto;
            position: relative;
            top: 50%;
            left: 50%;
            margin: -55px 0 0 -307px;
        }

        .overlayComMessage {
            z-index: 99;
            margin: 0 auto;
            position: relative;
            top: 50%;
            left: 50%;
            margin: -277px 0 0 -340px;
        }

        .modal {
            position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
        }

        .customerdetail {
            min-height: 0px;
            height: 0px;
        }

        .loading {
            font-family: Arial;
            font-size: 10pt;
            border: 5px solid #67CFF5;
            width: 200px;
            height: 100px;
            display: none;
            position: fixed;
            background-color: White;
            z-index: 999;
        }

        .formLayout {
            background-color: #e8edf1;
            padding: 10px;
            float: left;
        }

            .formLayout label, .formLayout input {
                display: block;
                white-space: nowrap;
                float: left;
                margin-bottom: 10px;
            }

            .formLayout label, .formLayout textarea {
                display: block;
                /*width: 197px;*/
                float: left;
                margin-bottom: 10px;
            }

            .formLayout label, .formLayout select {
                display: block;
                white-space: nowrap;
                float: left;
                margin-bottom: 10px;
            }

        input[type='radio'] {
            background-color: #FFFF99;
            color: Navy;
            position: relative;
            bottom: 3px;
            vertical-align: middle;
        }

        #chkStatus {
            display: block;
            width: 15px;
            float: left;
            margin-bottom: 10px;
        }

        .formLayout label {
            text-align: right;
            padding-right: 20px;
            font-weight: bold;
        }

        .dvHeader {
            background-color: #d3e3fc;
            border-bottom: solid 1px #a1a1a1;
            padding: 10px;
            height: 22px;
        }

        .dvBoxHeader {
            background-color: #d3e3fc;
            border: solid 1px #a1a1a1;
            padding-top: 10px;
            padding-bottom: 10px;
            width: 99.9%;
            float: left;
            height: 22px;
        }

        .dvBox {
            background-color: #ffffff;
            border: solid 1px #a1a1a1;
            width: 99.9%;
            float: left;
            height: 320px;
        }

        .dvBoxInner {
            background-color: #ffffff;
            border: solid 1px #a1a1a1;
            padding-top: 10px;
            width: 99.9%;
            float: left;
            height: 119px;
        }

        .dvBoxInnerChild {
            background-color: #ffffff;
            padding-top: 5px;
            width: 99.9%;
            float: left;
            height: 67px;
        }

        .dvRptrChild {
            background-color: #ffffff;
            /*padding-top: 10px;*/
            width: 99.9%;
            float: left;
            height: 25px;
            border: 1px solid #A1A1A1;
        }

        .dvLine {
            border: solid 1px #a1a1a1;
            width: 850px;
        }

        br {
            clear: left;
        }

        .TriggerGroup {
            font-weight: bold;
            font-size: 12px;
        }

        .actionContainer, .filterItemContainer {
            padding-top: 5px;
        }
        /*Update Fields_On Select*/
        .dvBoxHeaderUF {
            background-color: #d3e3fc;
            border: solid 1px #a1a1a1;
            padding: 10px;
            width: 379px;
            float: left;
            height: 22px;
        }

        .dvBoxUF {
            background-color: #ffffff;
            border: solid 1px #a1a1a1;
            width: 398px;
            float: left;
            height: 354px;
        }

        .dvBoxInnerUF {
            background-color: #ffffff;
            border: solid 1px #a1a1a1;
            padding-top: 10px;
            width: 388px;
            float: left;
            height: 54px;
            padding-left: 10px;
        }

         .dvBoxInnerUFDefault {
            background-color: #ffffff;
            border: solid 1px #a1a1a1;
            padding-top: 10px;
            width: 388px;
            float: left;
            padding-left: 10px;
        }

        .dvBoxInnerChildUF {
            background-color: #ffffff;
            padding-top: 10px;
            width: 390px;
            float: left;
        }

        .dvRptrChildUF {
            background-color: #ffffff;
            /*padding-top: 10px;*/
            width: 426px;
            float: left;
            height: 25px;
            border: 1px solid #A1A1A1;
        }

        .ConddvBoxHeader {
            background-color: #d3e3fc;
            border: solid 1px #a1a1a1;
            padding: 10px;
            width: 850px;
            float: left;
            height: 22px;
            display: none;
        }

        .ConddvBoxInner {
            background-color: #ffffff;
            border: solid 1px #a1a1a1;
            padding-top: 10px;
            width: 860px;
            float: left;
            height: 88px;
            padding-left: 10px;
            display: none;
        }

        .ConddvBox {
            background-color: #ffffff;
            border: solid 1px #a1a1a1;
            width: 870px;
            float: left;
            height: 88px;
            display: none;
        }

        .chk {
            vertical-align: middle;
            position: relative;
            bottom: 4px;
        }

        input[type=checkbox] {
            vertical-align: middle;
            position: relative;
            top: 2px;
        }

        #tblWebsite .textWebsite {
            height:20px !important;
        }
    </style>
    <script type="text/javascript">
        //function ShowProgress() {
        //    setTimeout(function () {
        //        var modal = $('<div />');
        //        modal.addClass("modal");
        //        $('body').append(modal);
        //        var loading = $(".loading");
        //        loading.show();
        //        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        //        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        //        loading.css({ top: top, left: left });
        //    }, 200);
        //    console.log("sachin");
        //}
        //$('#btnRefresh').click( function () {
        //    console.log('shruti');
        //    ShowProgress();
        //});

        $(function () {
            $('#btnSaveMail').click(function () {
                console.log('dkfslfdasjfjasflsjfklsjdfklsjfkldsjfkl');

                $("#dvComposeMessage").css({ "display": "none" });
            });
        });
        function OpenSetConditions() {
            //Added By Sachin Sadhu||Date:12Dec13
            //Added Company Querystring param to filter comp wise contacts

            window.open('../WorkFlow/frmSetConditions.aspx', '', 'width=650,height=350,status=no,titlebar=no,scrollbars=1,resizable=true,top=110,left=250')

            return false;
        }

        //function OpenRules() {
        //    window.location.href = "../WorkFlow/frmWFList.aspx";
        //    return false;
        //}
        function OpenPickUpFields() {
            //Added By Sachin Sadhu||Date:12Dec13
            //Added Company Querystring param to filter comp wise contacts

            window.open('../WorkFlow/frmPicktheFields.aspx', '', 'width=650,height=350,status=no,titlebar=no,scrollbars=1,resizable=true,top=110,left=250')

            return false;
        }
        function CheckNumber2(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }



        //www.telerik.com/help/aspnet-ajax/onclientpastehtml.html
        function SetFocusOnRadEditor() {
            var editor = $find("RadEditor1"); //get a reference to RadEditor client object          
            editor.setFocus(); //set the focus on the the editor
        }
        function OnClientLoad(editor, args) {
            $("#token-input-txtTo").focus();
        }
        function OnClientPasteHtml(sender, args) {
            var commandName = args.get_commandName();
            var value = args.get_value();
            if (commandName == "ImageManager") {
                //See if an img has an alt tag set
                var div = document.createElement("DIV");
                //Do not use div.innerHTML as in IE this would cause the image's src or the link's href to be converted to absolute path.
                //This is a severe IE quirk.
                Telerik.Web.UI.Editor.Utils.setElementInnerHtml(div, value);
                //Now check if there is alt attribute
                var img = div.firstChild;
                if (img.src) {
                    img.setAttribute("src", img.src);
                    //Set new content to be pasted into the editor
                    args.set_value(div.innerHTML);
                }
            }
        }
        function OnClientCommandExecuting(editor, args) {
            var name = args.get_name();
            var val = args.get_value();
            if (name == "MergeField") {
                editor.pasteHtml(val);
                //Cancel the further execution of the command as such a command does not exist in the editor command list
                args.set_cancel(true);
            }
        }
    </script>


    <%--    <script src="../reports/jqTree/underscore.js" type="text/javascript"></script>
    <script src="../reports/jqTree/ui.dropdownchecklist.js" type="text/javascript"></script>--%>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
   <%--  <link rel="stylesheet" type="text/css" href="../CSS/bootstrap.min.css" />--%>
    <link rel="stylesheet" type="text/css" href="../CSS/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/biz.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/dateFormat.js" type="text/javascript"></script>
    <%--   <script src="js/WorkFlow.js" type="text/javascript"></script>--%>


    <script language="javascript" type="text/javascript">

        //var arrFormFields = [];
        //var iSelectedForm = 0;
        //var responseJson = null;
        /*For make Div Movable*/
        //(function ($) {
        //    $.fn.drags = function (opt) {

        //        opt = $.extend({ handle: "", cursor: "move" }, opt);

        //        if (opt.handle === "") {
        //            var $el = this;
        //        } else {
        //            var $el = this.find(opt.handle);
        //        }

        //        return $el.css('cursor', opt.cursor).on("mousedown", function (e) {
        //            if (opt.handle === "") {
        //                var $drag = $(this).addClass('draggable');
        //            } else {
        //                var $drag = $(this).addClass('active-handle').parent().addClass('draggable');
        //            }
        //            var z_idx = $drag.css('z-index'),
        //                drg_h = $drag.outerHeight(),
        //                drg_w = $drag.outerWidth(),
        //                pos_y = $drag.offset().top + drg_h - e.pageY,
        //                pos_x = $drag.offset().left + drg_w - e.pageX;
        //            $drag.css('z-index', 1000).parents().on("mousemove", function (e) {
        //                $('.draggable').offset({
        //                    top: e.pageY + pos_y - drg_h,
        //                    left: e.pageX + pos_x - drg_w
        //                }).on("mouseup", function () {
        //                    $(this).removeClass('draggable').css('z-index', z_idx);
        //                });
        //            });
        //            //  e.preventDefault(); // disable selection
        //        }).on("mouseup", function () {
        //            if (opt.handle === "") {
        //                $(this).removeClass('draggable');
        //            } else {
        //                $(this).removeClass('active-handle').parent().removeClass('draggable');
        //            }
        //        });

        //    }
        //})(jQuery);
        /*ended here*/
        function Save() {
            
            if ($.trim(document.getElementById("txtWFName").value) == '') {
                alert("Enter Workflow Name");
                document.getElementById("txtWFName").focus();
                return false;
            }

            if (document.getElementById("ddlForm").value == 0) {
                alert("Select Workflow Module");
                document.getElementById("ddlForm").focus();
                return false;
            }

            if (document.getElementById("hfActionCount").value == "0") {
                alert("Enter Action");
                document.getElementById("ddlAction").focus();
                return false;
            }

            if (document.getElementById("ddlForm").value == "148" && ($("[id$=txtWebsiteDays]").val() == "" || parseInt($("[id$=txtWebsiteDays]").val()) == 0)) {
                alert("Enter days");
                document.getElementById("txtWebsiteDays").focus();
                return false;
            }

        }
        function AddAction() {
            if (document.form1.txtWFName.value == '') {
                alert("Enter Work Flow Name")
                document.form1.txtWFName.focus()
                return false;
            }
            if ($.trim(document.getElementById("txtWFName").value) == '') {
                alert("Enter Workflow Name");
                document.getElementById("txtWFName").focus();
                return false;
            }

            if (document.getElementById("ddlForm").value == 0) {
                alert("Select Workflow Module");
                document.getElementById("ddlForm").focus();
                return false;
            }

            if ($('#rdbDateFieldCond').is(':checked')) {
                if (document.getElementById('ddldateField').value == "0") {
                    $("#dvDatefieldCondition").css("display", "block");
                    alert("Select date");

                    document.getElementById("ddldateField").focus();
                    return false;
                }
                if ((parseInt(document.getElementById('txtDays').value) == 0) || ($.trim(document.getElementById('txtDays').value) == '')) {
                    $("#dvDatefieldCondition").css("display", "block");
                    alert("Enter Days");

                    document.getElementById('txtDays').focus();
                    return false;
                }
                if ((parseInt(document.getElementById('txtDays').value) > 365)) {
                    alert("No.Of Days must be less than or equal to 365");
                    document.getElementById('txtDays').focus();
                    return false;
                }
            }
            if (document.getElementById("ddlAction").value == 0) {
                alert("Select Action");
                document.getElementById("ddlAction").focus();
                return false;
            }
            if (document.getElementById("ddlAction").value == 3) {//Update Fields
                if (document.getElementById("hfUFCount").value == "0") {
                    $("#divUpdateFields").css("display", "block");
                    alert("Enter Update Fields");
                    document.getElementById("ddlAction").focus();
                    return false;
                }
            }
            if (document.getElementById("ddlAction").value == 1) {
                if (!validate()) {
                    return false;
                }
            }
            if (document.getElementById("ddlAction").value == 4) {//Display Alert
                if (document.getElementById("txtAlertMessage").value == "") {
                    alert("Enter Alert Message");
                    document.getElementById("txtAlertMessage").focus();
                    return false;
                }
            }
        }
        function AddCondition() {
            //if (document.form1.txtWFName.value == '') {
            //    alert("Enter Work Flow Name")
            //    document.form1.txtWFName.focus()
            //    return false;
            //}
            if ($.trim(document.getElementById("txtWFName").value) == '') {
                alert("Enter Workflow Name");
                document.getElementById("txtWFName").focus();
                return false;
            }

            if (document.getElementById("ddlForm").value == 0) {
                alert("Select Workflow Module");
                document.getElementById("ddlForm").focus();
                return false;
            }

            if ($('#rdbDateFieldCond').is(':checked')) {
                if (document.getElementById('ddldateField').value == "0") {
                    $("#dvDatefieldCondition").css("display", "block");
                    alert("Select date");

                    document.getElementById("ddldateField").focus();
                    return false;
                }
                if ((parseInt(document.getElementById('txtDays').value) == 0) || ($.trim(document.getElementById('txtDays').value) == '')) {
                    $("#dvDatefieldCondition").css("display", "block");
                    alert("Enter Days");

                    document.getElementById('txtDays').focus();
                    return false;
                }
                if ((parseInt(document.getElementById('txtDays').value) > 365)) {
                    alert("No.Of Days must be less than or equal to 365");
                    document.getElementById('txtDays').focus();
                    return false;
                }
            }

            if (document.getElementById("ddlCondition").value == 0) {
                alert("Select Condition");
                document.getElementById("ddlCondition").focus();
                return false;
            }

            //if ($('#chkCondition').is(':checked') == false) {
            //    alert("Please Check to Add Condition");
            //      return false;
            ////}
        }
        function validate() {
            var chks = document.getElementsByClassName('rlbCheck');
            var checkCount = 0;
            for (var i = 0; i < chks.length; i++) {
                if (chks[i].checked) {
                    checkCount++;
                }
            }

            if (checkCount == 0) {
                alert("select at least one - to field .");
                return false;
            }
            return true;
        }
        $(document).ready(function () {

            //For Action: Update Fields
            //$('#selectbtn').click(function () {
            //    $("#divUpdateFields").css("display", "block");
            //    //$('#divUpdateFields').drags(); 

            //});

            //$('#btnCloseUpdateFields').click(function () {
            //    $('#ddlUpdateFields').val('0');
            //    $("#divUpdateFields").css("display", "none");
            //});

            $('#ddlUpdateFields').change(function () {
                $("#divUpdateFields").css("display", "block");
                //$('#divUpdateFields').drags(); 

            });
            $('#btnAddUpdateFields').click(function () {
                $("#divUpdateFields").css("display", "block");
                //$('#divUpdateFields').drags(); 

            });
            //For Pick Fields

            //$('#lnbPickFields').click(function () {
            //    $("#dvPickfields").css("display", "block");
            //    //$('#divUpdateFields').drags(); 

            //});
            //$('#btnClosePickFields').click(function () {
            //    $("#dvPickfields").css("display", "none");
            //    //$('#divUpdateFields').drags(); 

            //});

            //For Setting Conditions

            //$('#btnSetConditions').click(function () {
            //    $("#dvConditions").css("display", "block");
            //    //$('#divUpdateFields').drags(); 

            //});

            //$('#btnCloseConditions').click(function () {
            //    $('#ddlCondition').val('0');
            //    $("#dvConditions").css("display", "none");
            //    //$('#divUpdateFields').drags(); 

            //});

            //date Field Condition
            //dvDatefieldConditionbtnCloseDate
            //$('#rdbDateFieldCond').click(function () {
            //    $("#dvDatefieldCondition").css("display", "block");

            //});
            $('#btnCloseDate').click(function () {

                if (document.getElementById('ddldateField').value == "0") {
                    alert("Select date");
                    document.getElementById("ddldateField").focus();
                    return false;
                }
                if ((parseInt(document.getElementById('txtDays').value) == 0) || ($.trim(document.getElementById('txtDays').value) == '')) {
                    alert("Enter Days");

                    document.getElementById('txtDays').focus();
                    return false;
                }
                if ((parseInt(document.getElementById('txtDays').value) > 365)) {
                    alert("No.Of Days must be less than or equal to 365");
                    document.getElementById('txtDays').focus();
                    return false;
                }
                //$("#dvDatefieldCondition").css("display", "none");
                //$('#divUpdateFields').drags(); btnCancelDate

            });
            //$('#btnCancelDate').click(function () {

            //    $("#dvDatefieldCondition").css("display", "none");
            //    //$('#divUpdateFields').drags(); btnCancelDate

            //});
        });

        function Close() {
            window.location.href = "../WorkFlow/frmWFList.aspx";
            return false;
        }

         function HideEmailDescPopup(divId) {           
            document.getElementById(divId).style.display = "none";
        }

        function ClearConditionControls() {
            var listbox = $find("<%=radValue.ClientID %>");

            if (listbox != null) { 
            for (var i = 0; i < listbox.get_items().get_count(); i++) {
                listbox.get_items().getItem(i).uncheck();
            }
            }
            if (document.getElementById("txtNvalue") != null) {
                document.getElementById("txtNvalue").value = " ";
            }
            if (document.getElementById("txtValueMoney") != null) {
                document.getElementById("txtValueMoney").value = " ";
            }
            if (document.getElementById("txtValue") != null) {
                document.getElementById("txtValue").value = " ";
            }
            if (document.getElementById("txtDate") != null) {
                document.getElementById("txtDate").value = " ";
            }
             if (document.getElementById("ddlCompareWith") != null) {
                document.getElementById("ddlCompareWith").selectedIndex = 0;
            }          
        }
        function ClearActionControls() {
            var listbox = $find("<%=radSendSMS.ClientID %>");
            if (listbox != null) {
                for (var i = 0; i < listbox.get_items().get_count(); i++) {
                    listbox.get_items().getItem(i).uncheck();
                }
            }
            if (document.getElementById("txtSMSText") != null) {
                document.getElementById("txtSMSText").value = " ";
            }

            var listboxEmailbox = $find("<%=radlstEmailTo.ClientID %>");
            if (listboxEmailbox != null) {
                for (var i = 0; i < listboxEmailbox.get_items().get_count(); i++) {
                    listboxEmailbox.get_items().getItem(i).uncheck();
                }
            }
            if (document.getElementById("ddlEmailTemplate") != null) {
                document.getElementById("ddlEmailTemplate").selectedIndex = 0;
            } 
            if (document.getElementById("ddlActionItemTemplate") != null) {
                document.getElementById("ddlActionItemTemplate").selectedIndex = 0;
            } 
            if (document.getElementById("ddlAssignTo") != null) {
                document.getElementById("ddlAssignTo").selectedIndex = 0;
            } 
             if (document.getElementById("txtAlertMessage") != null) {
                document.getElementById("txtAlertMessage").value = " ";
            }
             if (document.getElementById("ddlCreateBizDoc") != null) {
                document.getElementById("ddlCreateBizDoc").selectedIndex = 0;
            } 
            if (document.getElementById("ddlBizTemplate") != null) {
                document.getElementById("ddlBizTemplate").selectedIndex = 0;
            } 
             if (document.getElementById("ddlBizDocType") != null) {
                document.getElementById("ddlBizDocType").selectedIndex = 0;
            } 
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="FiltersAndViews1">
    <div style="float: right">
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSave_OnClick" />
        <asp:Button ID="btnRefesh" runat="server" Text="Save & Close" CssClass="btn btn-primary" OnClick="btnRefresh_OnClick" />
        <asp:Button ID="btnCancel" runat="server" Text="Close" CssClass="btn btn-primary" OnClick="btnCancel_OnClick" />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Workflow & Business Process Automation (BPA)
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="scrptWorkFlow" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdWFa" runat="server"  UpdateMode="Conditional">
        <ContentTemplate>
            <div style="background-color: #e8edf1;" >
                <div style="width: 1200px; min-height: 590px;" >
                    <table style="background-color: #fff; padding: 10px; padding-top:0px; " >
                        <tr>
                            <td></td>
                            <td style="text-align:right; float:right;" >
                                <table  style="padding:0px;  text-align:right;">
                                    <tr>                                        
                                        <td>
                                            <div class="record-small-box">
                                                <a href="#" class="small-box-footer">Created By <i class="fa fa-user"></i></a>
                                                <div class="inner">
                                                    <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="record-small-box bg-yellow">
                                                <a href="#" class="small-box-footer">Last Modified By <i class="fa fa-user"></i></a>
                                                <div class="inner">
                                                    <asp:Label ID="lblModifiedBy" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </td>         
                                    </tr>
                                </table>
                            </td>                           
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="dvSuccess" runat="server"></div>
                                <div id="dvError" runat="server"></div>
                                <asp:Label ID="lblMessage" runat="server" Text="&nbsp;" ForeColor="Navy" Font-Size="8.5pt" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../images/wfHeader.png" alt="" />
                            </td>
                            <td style="width: 100%; padding-left: 40px;">
                                <table style="width: 100%;" >
                                    <tr>
                                        <td style="font-weight: bold; white-space: nowrap; text-align: right">WorkFlow Name<font color="red">*</font>
                                        </td>
                                        <td style="width: 70%">
                                            <asp:TextBox ID="txtWFName" runat="server" CssClass="signup" Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="text-align: right; white-space: nowrap">
                                            <asp:CheckBox ID="chkStatus" runat="server" Style="float: right" />
                                            <label style="float: right; line-height: 30px; font-weight: bold; line-height: 25px; vertical-align: middle;">Engage Workflow</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; white-space: nowrap; text-align: right">Description</td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="54px" CssClass="signup" Width="100%" MaxLength="500"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table  style="background-color: #fff2cc; padding: 20px; width: 100%; border: 1px solid #fbca3a;">
                        <tr style="height: 50px">
                            <td>
                                <table>
                                    <tr>
                                        <td style="vertical-align:top;padding-top:4px;">
                                            <b>When<span style="color: red">*</span></b>
                                            <asp:DropDownList ID="ddlForm" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlForm_OnSelectedIndexChanged"></asp:DropDownList>
                                        </td>
                                        <td>
                                            <table id="tblOrganizationFilter" runat="server" style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlRelationship" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlRelationshipType" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select Relationship Type --" Value="-1"></asp:ListItem>
                                                            <asp:ListItem Text="Lead" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Prospect" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Account" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlProfile" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select Profile --" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlFollowup" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select Follow-up Status --" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tblContactType" runat="server" style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlContactType" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select Contact Type --" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tblOppOrder" runat="server" style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlOppOrOrder" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select Opportunity or Order Type --" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Sales Order" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Purchase Order" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Sales Opportunity" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="Purchase Opportunity" Value="4"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlOppOrOrderStatus" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select Opportunity or Order Status --" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlTotalProgress" runat="server">
                                                            <asp:ListItem Text="-- Select Total Progress --" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tblBizDocs" runat="server" style="display: none">
                                                <tr>
                                                    <td>
                                                        <b>Within a<span style="color: red">*</span></b>
                                                        <asp:DropDownList ID="ddlOppOrOrderBizDocs" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select Opportunity or Order Type --" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Sales Order" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Purchase Order" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Sales Opportunity" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="Purchase Opportunity" Value="4"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlBizDocsType" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select BizDoc Type --" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlBizDocsTemplate" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select BizDoc Template --" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlBizDocsStatus" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select BizDoc Status --" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tblCases" runat="server" style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlCasesType" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlCasesReason" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlCasesStatus" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlCasesPriority" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tblProjects" runat="server" style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlProjectsType" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlProjectsStatus" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="dvProcessTypeAndName" runat="server" style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlProcessType" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select Process Type --" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Sales Process" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Purchase Process" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Projects" Value="3"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlProcessName" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select Process Name --" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlStage" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select Stage Name --" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlStageProgress" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="-- Select Stage Progress --" Value="-1"></asp:ListItem>
                                                            <asp:ListItem Text="0%" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="25%" Value="25"></asp:ListItem>
                                                            <asp:ListItem Text="50%" Value="50"></asp:ListItem>
                                                            <asp:ListItem Text="75%" Value="75"></asp:ListItem>
                                                            <asp:ListItem Text="100%" Value="100"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tblActionItems" runat="server" style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlActionItemType" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlActionItemTaskType" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlActionItemPriority" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tblARAging" runat="server" style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlArAging" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlArAging_SelectedIndexChanged">
                                                        </asp:DropDownList>                                                       
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tblWebsite" runat="server" style="display: none">
                                                <tr>
                                                    <td>
                                                        <telerik:RadComboBox runat="server" CheckBoxes="true" ID="rcbWebsitePages" Width="90%" InputCssClass="textWebsite"></telerik:RadComboBox> is visited by an existing contact AND last email sent was > <asp:TextBox ID="txtWebsiteDays" runat="server" Width="50"></asp:TextBox> days before the visit date 
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td>
                                <table id="tblrdbs" runat="server">
                                    <tr>
                                        <td>
                                            <asp:RadioButton ID="rdbCreate" runat="server" GroupName="Events" Text="Record is created" Font-Bold="true" Checked="true" AutoPostBack="true" Style="padding-bottom: 3px !important;" /></td>
                                        <td>
                                            <asp:RadioButton ID="rdbFieldsUpdate" runat="server" GroupName="Events" AutoPostBack="true" Font-Bold="true" Text="Field(s) is/are updated" />
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rdbDateFieldCond" runat="server" GroupName="Events" Font-Bold="true" AutoPostBack="true" OnCheckedChanged="rdbDateFieldCond_CheckedChanged" Text="Date Field Condition" />
                                            &nbsp;&nbsp;<asp:Label ID="lblDateRange" runat="server" Font-Bold="true" Style="display:none" ForeColor="Green"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" style="height: 400px; border-collapse: collapse; background-color:white;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="50%" style="border: solid 1px #a1a1a1; vertical-align: top; border-collapse: collapse">
                                <asp:Panel runat="server" ID="pnlCondition" DefaultButton="btnAddConditions">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="dvHeader">   
                                                <%--<img src="../images/condition.png" style="float: left" height="24" width="24" />--%>
                                                <span style="font-weight: bold; font-size: 16px; float: left; line-height: 24px">&nbsp; Conditions:</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td class="dvHeader">                                                
                                                <asp:Button ID="btnCloseEditViewCondition" OnClick="btnCloseEditViewCondition_Click"  runat="server" Style="color:white; float: right; margin-right: 5px" Text="Close" CssClass="btn btn-warning" Font-Bold="true" />
                                                <asp:Button ID="btnClearCondition" OnClientClick="javascript: ClearConditionControls(); return false; " runat="server" Style="color:white; float: right; margin-right: 5px" Text="Clear" CssClass="btn btn-warning" Font-Bold="true" />                                              
                                                 <asp:Button ID="btnAddConditions" runat="server" Style="float: right; margin-right: 5px;" Text="Add Condition" OnClientClick="return AddCondition();" CssClass="btn btn-primary" Font-Bold="true" OnClick="btnAddConditions_Click" />                                                  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" cellpadding="5" cellspacing="0" >
                                                    <tr>
                                                        <%--<td style="font-weight: bold">
                                                            <asp:CheckBox ID="chkCondition" runat="server" />
                                                        </td>--%>
                                                      
                                                        <td width="100%" style="text-align:center;" >
                                                            <table  width="85%" align="center"  cellspacing="2" >
                                                                <tr>
                                                                    <td id="dvANDOR" runat="server" style="display: none; width:35%;"  >
                                                                        <asp:DropDownList ID="ddlANDORCondition" runat="server" Width="120"></asp:DropDownList>
                                                                    </td>
                                                                    <td width=" 35%;" >
                                                                        <asp:DropDownList ID="ddlCondition" runat="server" Width="180" AutoPostBack="true" OnSelectedIndexChanged="ddlCondition_OnSelectedIndexChanged"></asp:DropDownList>
                                                                    </td>
                                                                    <td width="35%;">
                                                                        <asp:DropDownList ID="ddlSign" runat="server" Width="150" AutoPostBack="true" OnSelectedIndexChanged="ddlSign_OnSelectedIndexChanged"></asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--<td></td>--%>
                                                        <td width="100%" style="padding-left: 35px;">
                                                            <telerik:RadListBox ID="radValue" runat="server" CheckBoxes="true" showcheckall="true" Width="320"
                                                                Height="280" Visible="false">
                                                            </telerik:RadListBox>
                                                            <asp:TextBox ID="txtValue" runat="server" Width="250px" Visible="false" autocomplete="off"></asp:TextBox>
                                                            <BizCalendar:Calendar ID="calDate" runat="server" Visible="false" />
                                                            <asp:TextBox ID="txtValueMoney" runat="server" Width="250px" Visible="false" MaxLength="12" onkeypress="CheckNumber(1,event)" ClientIDMode="Static"></asp:TextBox>
                                                            <asp:TextBox ID="txtNvalue" runat="server" Width="250px" Visible="false" MaxLength="12" onkeypress="CheckNumber(1,event)"></asp:TextBox>
                                                            <asp:DropDownList ID="ddlCompareWith" runat="server" Width="250px" Visible="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lblConditionMessage" Text="" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdfConditions" runat="server" Value="0" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" valign="top">
                                                <asp:Repeater ID="rprConditions" runat="server" OnItemCommand="rprConditions_ItemCommand">
                                                    <HeaderTemplate>
                                                        <table width="100%" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr style="background-color: #ffffff; height: 30px;">
                                                            <td style="border: 1px solid #A1A1A1; padding-left: 10px !important; padding-right: 10px !important; white-space: nowrap">
                                                                <asp:Label ID="lblDislayNumber" runat="server" Text='<%#Eval("RowNumber") %>' Visible="false"></asp:Label>
                                                                <%#Container.ItemIndex+1%>
                                                            </td>
                                                            <td width="100%" style="border: 1px solid #A1A1A1; padding-left: 5px;">
                                                                <asp:Label ID="lblActionText" runat="server" Text='<%#Eval("vcMessge") %>' ForeColor="maroon"></asp:Label>
                                                            </td>
                                                            <td style="border: 1px solid #A1A1A1; white-space: nowrap; padding-left: 5px;">
                                                                <asp:ImageButton runat="server" ID="ImageButton1" CommandArgument='<%# Eval("RowNumber")%>'
                                                                    ToolTip="Click Here to Edit Record." CommandName="Edit" AlternateText="Edit"
                                                                    ImageUrl="~/images/pencil-24.gif" Width="16px" Height="16px" Style="vertical-align: middle"></asp:ImageButton>
                                                                &nbsp; <span class="vAlignMiddle">| </span>&nbsp;
                                                                    <asp:ImageButton runat="server" ID="ImageButton2" CommandArgument='<%# Eval("RowNumber") %>'
                                                                        ToolTip="Click Here to Delete Record." CommandName="Delete" AlternateText="Delete"
                                                                        ImageUrl="~/images/delete2.gif" OnClientClick="return confirm(&quot;Are you sure you want to delete?&quot;)" Style="vertical-align: middle"></asp:ImageButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                            <td width="50%" style="border: solid 1px #a1a1a1; vertical-align: top; border-collapse: collapse">
                                <asp:Panel runat="server" ID="pnlActions" DefaultButton="btnAddAction">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="dvHeader">
                                                <%--<img src="../images/Actions1.png" style="float: left" height="24" width="24" />--%>
                                                <span style="font-weight: bold; font-size: 16px; float: left; vertical-align: middle; line-height: 24px">&nbsp; Actions:</span>
                                            </td>
                                            <td class="dvHeader">  
                                                <asp:Button ID="btnCloseEditViewAction" OnClick="btnCloseEditViewAction_Click" runat="server" Style="color:white; float: right; margin-right: 5px" Text="Close" CssClass="btn btn-warning" Font-Bold="true" />
                                                <asp:Button ID="btnClearAction" OnClientClick="javascript: ClearActionControls(); return false; " runat="server" Style="color:white; float: right; margin-right: 5px" Text="Clear" CssClass="btn btn-warning" Font-Bold="true" />                                              
                                                 <asp:Button ID="btnAddAction" runat="server" Text="Add Action" Style="float: right; margin-right: 5px;" Font-Bold="true" OnClick="ButtonAdd_Click" CssClass="btn btn-primary" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align:center;">
                                                <table cellpadding="5" cellspacing="0" class="tblActionDetail" style="padding-top: 3px;">
                                                    <tr>
                                                        <td style="vertical-align: top">
                                                            <asp:Label ID="lblActionNo" runat="server" Text="Select Action" Visible="true" Style="line-height: 22px"></asp:Label>
                                                        </td>
                                                        <td style="vertical-align: top">
                                                            <asp:DropDownList ID="ddlAction" runat="server" Width="200" OnSelectedIndexChanged="ddlAction_OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </td>
                                                        <td style="display: none" id="dvUpdateFields" runat="server">
                                                            <asp:Button ID="selectbtn" runat="server" Text="Select" Font-Bold="true" CssClass="btn btn-primary" />
                                                        </td>
                                                        <td id="dvAttachBizDoc" runat="server" style="display: none">
                                                            <asp:CheckBox ID="chkAttachBizDoc" runat="server" CssClass="chk" Font-Bold="true" /><label style="font-weight: bold">Attach BizDoc</label>
                                                            <asp:DropDownList ID="ddlAttachBizdoc" runat="server" Width="140" Visible="false"></asp:DropDownList>
                                                        </td>
                                                        <td id="dvBizDocapproval" runat="server" style="display: none">
                                                            <telerik:RadListBox ID="radApprovalAuthority" runat="server" CheckBoxes="true" ShowCheckAll="true" Width="230" Height="200">
                                                            </telerik:RadListBox>
                                                        </td>
                                                        <td id="dvSelectBizDoc" runat="server" style="display: none">
                                                            <label style="font-weight: bold">Type&nbsp;</label>
                                                            <asp:DropDownList ID="ddlBizDocType" runat="server" Width="190" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellpadding="5" cellspacing="0" style="display: none" id="dvSendMail" runat="server" class="tblActionDetail">
                                                    <tr>
                                                        <td style="vertical-align: top;">
                                                            <asp:Label ID="lblEmailTemplate" runat="server" Text="Email Template" Style="line-height: 22px"></asp:Label>
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                            <asp:DropDownList ID="ddlEmailTemplate" runat="server" Width="200" AutoPostBack="true" OnSelectedIndexChanged="ddlEmailTemplate_OnSelectedIndexChanged"></asp:DropDownList>
                                                          <br /> <asp:LinkButton ID="lnkEmailTemplate" runat="server" OnClick="lnkEmailTemplate_Click" ></asp:LinkButton>
                                                        </td>
                                                        <td style="font-weight: bold; vertical-align: top; white-space: nowrap">
                                                            <asp:Label ID="Label2" runat="server" Text="To" Style="line-height: 22px"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <telerik:RadListBox ID="radlstEmailTo" runat="server" CheckBoxes="true" ShowCheckAll="true" Width="230"
                                                                Height="200" OnItemDataBound="radlstEmailTo_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltext" runat="server" Text='<%# Eval("vcReceiverName") %>'>"></asp:Label>
                                                                    <asp:Label ID="lblValue" runat="server" Visible="false" Text='<%# Eval("vcEmailToType") %>'>"></asp:Label>
                                                                    <asp:TextBox ID="txtSendTo" runat="server" Visible="false" CssClass="radCSS" TextMode="MultiLine" Height="60"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </telerik:RadListBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td style="vertical-align:top"><b>From</b></td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rblFrom" runat="server" Font-Bold="true" RepeatLayout="Flow">
                                                                <asp:ListItem Text="Owner of trigger record" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Assignee of trigger record" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Global Messaging & Calendaring" Value="0" Selected="True"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellpadding="5" cellspacing="0" style="display: none" id="displayAlert" runat="server" class="tblActionDetail">
                                                    <tr>
                                                        <td>Your Message
                                                        </td>
                                                        <td width="100%">
                                                            <asp:TextBox ID="txtAlertMessage" runat="server" TextMode="MultiLine" Width="450" Height="70"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellpadding="5" cellspacing="0" style="display: none" id="dvSendSMS" runat="server" class="tblActionDetail">
                                                    <tr>
                                                        <td style="vertical-align: top">
                                                            <asp:Label ID="Label6" runat="server" Text="Text" Style="line-height: 22px"></asp:Label>
                                                        </td>
                                                        <td style="vertical-align: top">
                                                            <asp:TextBox ID="txtSMSText" runat="server" TextMode="MultiLine" Width="195px" Height="73px"></asp:TextBox>
                                                        </td>
                                                        <td style="font-weight: bold; vertical-align: top">
                                                            <asp:Label ID="lblTo" runat="server" Text="To" Style="line-height: 22px"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <telerik:RadListBox ID="radSendSMS" runat="server" CheckBoxes="true" ShowCheckAll="true" Width="230" Height="200">
                                                            </telerik:RadListBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellpadding="5" cellspacing="0" style="display: none" id="dvAssignItems" runat="server" class="tblActionDetail">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label3" runat="server" Text="Action  Template"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlActionItemTemplate" runat="server" Width="200"></asp:DropDownList>
                                                        </td>
                                                        <td style="font-weight: bold; white-space: nowrap">
                                                            <asp:Label ID="Label4" runat="server" Text="Assign To"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlAssignTo" runat="server" Width="190"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table style="display: none" id="dvCreateBizDoc" runat="server" class="tblActionDetail" cellpadding="5" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblBizDocTemplate" runat="server" Text="Choose BizDoc"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlCreateBizDoc" runat="server" Width="200" AutoPostBack="true"></asp:DropDownList>
                                                        </td>
                                                        <td style="font-weight: bold">
                                                            <asp:Label ID="Label7" runat="server" Text="With"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlBizTemplate" runat="server" Width="190" ToolTip="Select BizDoc Template"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <asp:HiddenField ID="hfWFCodeForEdit" runat="server" Value="0" />
                                                <asp:HiddenField ID="hfActionCount" runat="server" Value="0" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="height: 13px;"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Repeater ID="rprActions1" runat="server" OnItemCommand="rprActions1_ItemCommand">
                                                    <HeaderTemplate>
                                                        <table width="100%" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr style="background-color: #ffffff; height: 30px;">
                                                            <td style="border: 1px solid #A1A1A1; padding-left: 10px !important; padding-right: 10px !important; white-space: nowrap">
                                                                <asp:Label ID="lblDislayNumber" runat="server" Text='<%#Eval("RowNumber") %>' Visible="false"></asp:Label>
                                                                <%#Container.ItemIndex+1%>
                                                            </td>
                                                            <td width="100%" style="border: 1px solid #A1A1A1; padding-left: 5px;">
                                                                
                                                                  <asp:Label ID="lblIndividualAction" runat="server" Text='<%#Eval("vcIndividualAction") %>' ForeColor="navy"></asp:Label>
                                                                <asp:Label ID="lblActionText" runat="server" Text='<%#Eval("vcMessge") %>'  Visible="false" ForeColor="navy"></asp:Label>
                                                                <asp:Label ID="lblAction" runat="server" Text='<%#Eval("vcAction") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblDocName" runat="server" Text='<%#Eval("vcDocName") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblTemplateName" runat="server" Text='<%#Eval("TemplateName") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblUpdateFields" runat="server" Text='<%#Eval("vcUpdateFields") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblAltertMessage" runat="server" Text='<%#Eval("vcAlertMessage") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblReceiverName" runat="server" Text='<%#Eval("vcReceiverName")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblAssignTo" runat="server" Text='<%#Eval("vcAssignTo")%>' Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="border: 1px solid #A1A1A1; white-space: nowrap; padding-left: 5px;">
                                                                <asp:ImageButton runat="server" ID="ImageButton1" CommandArgument='<%# Eval("RowNumber")%>'
                                                                    ToolTip="Click Here to Edit Record." CommandName="Edit" AlternateText="Edit"
                                                                    ImageUrl="~/images/pencil-24.gif" Width="16px" Height="16px" Style="vertical-align: middle"></asp:ImageButton>
                                                                &nbsp; <span class="vAlignMiddle" style="padding-top: 4px!important">| </span>&nbsp;
                                                                    <asp:ImageButton runat="server" ID="ImageButton2" CommandArgument='<%# Eval("RowNumber") %>'
                                                                        ToolTip="Click Here to Delete Record." CommandName="Delete" AlternateText="Delete"
                                                                        ImageUrl="~/images/delete2.gif" OnClientClick="return confirm(&quot;Are you sure you want to delete?&quot;)" Style="vertical-align: middle"></asp:ImageButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>

                <asp:HiddenField ID="hfDateFormat" runat="server" />
                <asp:HiddenField ID="hfWFID" runat="server" />
                <asp:HiddenField ID="hfValidDateFormat" runat="server" />
            </div>
            <div class="overlay" id="divUpdateFields" runat="server" style="display: none">
                <div style="border-color: #8A8A8A; border-width: 2px; border-style: solid; height: 400px; width: 430px; background-color: aliceblue" class="overlayUpdateFields">
                    <asp:Panel ID="pnlUFFields" runat="server" Style="vertical-align: middle">
                        <div class="dvBoxHeaderUF" style="width:408px;">
                            <span style="font-weight: bold; font-size: 16px; float: left">Choose Fields:</span>
                            <div style="float: right;">
                                <%--<input type="button" name="Close" value="Close" id="btnCloseUpdateFields" style="width: 80px;" class="button" runat="server" />--%>
                                <asp:Button ID="btnCloseUpdateFields" runat="server" CssClass="btn btn-primary" Style="width: 80px;" Text="Close" />
                            </div>
                        </div>
                        <div class="dvBoxUF" style="width:428px;">
                            <div class="dvBoxInnerUFDefault" style="width:417px;">
                                <div style="width: 40px; float: left">
                                    <asp:Label ID="lblUFText" runat="server" Text="Fields"></asp:Label>

                                </div>
                                <div style="width: 160px; float: left;">
                                    <asp:DropDownList ID="ddlUpdateFields" runat="server" Width="150px" OnSelectedIndexChanged="ddlUpdateFields_OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                                </div>
                                <div style="width: 155px; float: left;  "  id="Div2" runat="server">
                                    <asp:DropDownList ID="ddlUFValue" runat="server" Width="130px"></asp:DropDownList>
                                    <asp:TextBox ID="txtUFValue" runat="server" Width="130px" Visible="false" autocomplete="off"></asp:TextBox>
                                    <BizCalendar:Calendar ID="calUFDate" runat="server" Visible="false" />
                                    <div id="divChkLst" style="overflow-y:scroll;height:70px;  display:none; width:152px;" runat="server">
                                         <asp:CheckBoxList id="chklstUFValue"  runat="server" ></asp:CheckBoxList>
                                    </div>                                   
                                </div>
                                <div style="width: 50px; float: left; text-align: right;">
                                    <asp:Button ID="btnAddUpdateFields" runat="server" Text="Add" Width="50px" CssClass="btn btn-primary" OnClick="btnAddUpdateFields_OnClick" />
                                </div>
                            </div>
                            <asp:HiddenField ID="hdfUpdateFields" runat="server" Value="0" />
                            <asp:HiddenField ID="hfUFCount" runat="server" Value="0" />
                            <asp:Repeater ID="rprUpdateFields" runat="server" OnItemCommand="rprUpdateFields_ItemCommand">
                                <ItemTemplate>
                                    <div class="dvRptrChildUF" id="dvAssignItems" runat="server">
                                        <div style="width: 14px; height: 22px; float: left; border-right: 1px solid #A1A1A1; padding-top: 3px">
                                            <asp:Label ID="lblDislayNumber" runat="server" Text='<%#Eval("RowNumber") %>' Visible="false"></asp:Label>
                                            <%#Container.ItemIndex+1%>
                                        </div>
                                        <div style="width: 300px; float: left; padding-top: 3px">
                                            <asp:Label ID="lblActionText" runat="server" Text='<%#Eval("vcMessge") %>' ForeColor="maroon"></asp:Label>
                                            <%--<asp:Label ID="lblAction" runat="server" Text='<%#Eval("vcAction") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblDocName" runat="server" Text='<%#Eval("vcDocName") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblTemplateName" runat="server" Text='<%#Eval("TemplateName") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblUpdateFields" runat="server" Text='<%#Eval("vcUpdateFields") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblAltertMessage" runat="server" Text='<%#Eval("vcAlertMessage") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblReceiverName" runat="server" Text='<%#Eval("vcReceiverName")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lblAssignTo" runat="server" Text='<%#Eval("vcAssignTo")%>' Visible="false"></asp:Label>--%>
                                        </div>
                                        <div class="tdRepeaterNormalR" style="width: 70px; float: right; padding-top: 4px">
                                            <asp:ImageButton runat="server" ID="ImageButton1" CommandArgument='<%# Eval("RowNumber")%>'
                                                ToolTip="Click Here to Edit Record." CommandName="Edit" AlternateText="Edit"
                                                ImageUrl="~/images/pencil-24.gif" Width="16px" Height="16px"></asp:ImageButton>
                                            &nbsp; <span class="vAlignMiddle">| </span>&nbsp;
                                            <asp:ImageButton runat="server" ID="ImageButton2" CommandArgument='<%# Eval("RowNumber") %>'
                                                ToolTip="Click Here to Delete Record." CommandName="Delete" AlternateText="Delete"
                                                ImageUrl="~/images/delete2.gif" OnClientClick="return confirm(&quot;Are you sure you want to delete?&quot;)"></asp:ImageButton>
                                        </div>

                                    </div>
                                </ItemTemplate>

                            </asp:Repeater>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="overlay" id="dvPickfields" runat="server" style="display: none">
                <div style="border-color: #8A8A8A; border-width: 2px; border-style: solid; height: 400px; width: 400px; background-color: aliceblue" class="overlayPickFields">
                    <asp:Panel ID="pnlPickFields" runat="server">
                        <div class="dvBoxHeaderUF">
                            <span style="font-weight: bold; font-size: 16px; float: left">Pick the Fields:</span>
                            <div style="float: right;">
                                <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="btnSelect_OnClick" CssClass="btn btn-primary" />
                                <%-- <input type="button" name="Close" value="Close" id="btnClosePickFields" style="width: 80px;" class="button"  />--%>
                                <asp:Button ID="btnClosePickFields" runat="server" Text="Close" Width="80px" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div class="dvBoxUF">
                            <div class="dvBoxInnerUF" style="height: 343px">
                                <div style="width: 120px; float: left">
                                    <asp:Label ID="lblParameterName" runat="server" Text="Choose Fields" Visible="true"></asp:Label><font color="red">*</font>

                                </div>
                                <div style="width: 250px; float: left;">
                                    <telerik:RadListBox ID="radFields" runat="server" CheckBoxes="true" showcheckall="true" Width="240px"
                                        Height="300px">
                                    </telerik:RadListBox>
                                    <asp:HiddenField ID="hdfPickedFields" runat="server" Value="0" />
                                </div>
                            </div>


                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="overlay" id="dvDatefieldCondition" runat="server" style="display: none">
                <div style="border-color: #8A8A8A; border-width: 2px; border-style: solid; height: 150px; width: 613px; background-color: aliceblue" class="overlayDateFields">
                    <asp:Panel ID="Panel1" runat="server">
                        <div class="dvBoxHeaderUF" style="width: 591px; height:35px; padding-top:5px;">
                            <table border="0" width="100%" style="height:100%;">
                                <tr>
                                    <td style="float:left; padding-top:10px;">
                                         <Label style="font-weight:bold; font-size:16px; float:left;" runat="server">Date field condition:</Label>
                                    </td>
                                     <td style=" float:right;">
                                            <asp:Button ID="btnCloseDate" runat="server" CssClass="btn btn-primary" Text="Set & Close" />
                                            <asp:Button ID="btnCancelDate" runat="server" CssClass="btn btn-primary" Text="Cancel" />
                                        </td>
                                </tr>
                            </table>                          
                          
                        </div>
                        <div class="dvBoxUF" style="width: 613px; height: 115px">
                            <div class="dvBoxInnerUF" style="width: 601px;height: 72px;padding-top:40px;">
                                <table>
                                    <tr>
                                        <td style="width: 150px; text-align:right;">
                                             <asp:Label ID="Label1" runat="server" Text="Executes when" ForeColor="#0D680D"></asp:Label>
                                        </td>
                                        <td style="width: 170px;  text-align:right;">
                                             <asp:DropDownList ID="ddldateField" runat="server" Width="150px"></asp:DropDownList><font color="red">*</font>&nbsp;is
                                        </td>
                                        <td style="width: 80px;  text-align:right;">
                                            <asp:TextBox ID="txtDays" runat="server" Width="45px" MaxLength="3" onkeypress="CheckNumber(2,event)"></asp:TextBox><font color="red">*</font>days
                                        </td>
                                        <td style="width: 125px;  text-align:right;">
                                            <asp:DropDownList ID="ddlBefore" runat="server" Width="70px"></asp:DropDownList>
                                            <span style="font-weight: bold; color: #B80000">"Today"</span>
                                        </td>
                                    </tr>
                                </table>  
                                 <div>
                                     <label id="lblNote" runat="server" style="display:none;">Note:</label>
                                </div>
                            </div>
                           

                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="overlay" id="dvComposeMessage" runat="server" style="display: none">
                <div style="border-color: #8A8A8A; border-width: 2px; border-style: solid; height: 553px; width: 680px; background-color: aliceblue" class="overlayComMessage">
                    <asp:Panel ID="pnlComposeMessage" runat="server">
                        <div class="dvBoxHeaderUF" style="width: 660px; border: none;">
                            <span style="font-weight: bold; font-size:16px; float:left;">Compose Message</span>
                            <div style="float: right;">
                                <asp:Button ID="btnSaveMail" runat="server" Text=" Save & Close " CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div class="dvBoxUF" style="width: 680px; height: 510px;">
                            <div class="dvBoxInnerUF" style="width: 668px; height: 499px;">
                                <div style="width: 668px; height: 499px;">
                                    <div style="width: 600px; float: left; height: 35px;">
                                        <div style="width: 50px; float: left">
                                            <span style="color: green">Subject</span>
                                        </div>
                                        <div style="width: 550px; float: left">
                                            <asp:TextBox ID="txtMailSubject" runat="server" CssClass="signup" Width="600px"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div style="width: 600px; float: left; height: 450px">
                                        <telerik:RadEditor ID="radComposeMail" runat="server" ToolsFile="EditorTools.xml" Width="656px"
                                            OnClientLoad="OnClientLoad" Height="450px" OnClientPasteHtml="OnClientPasteHtml"
                                            OnClientCommandExecuting="OnClientCommandExecuting">
                                            <Content>
                                            </Content>
                                            <Tools>
                                                <telerik:EditorToolGroup>
                                                    <telerik:EditorDropDown Name="MergeField" Text="Merge Field">
                                                    </telerik:EditorDropDown>
                                                </telerik:EditorToolGroup>
                                            </Tools>
                                        </telerik:RadEditor>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>

        <div class="overlay" id="divShowEmailDesc" runat="server" style="display: none">
           <div style="border-color: #8A8A8A; border-width: 2px; border-style: solid;  height: 320px; width: 520px; background-color: aliceblue; " class="overlayComMessage">
               <table border="0" width="100%" class="dialog-body">
                   <tr>
                       <td  style="float:right; " >
                            <asp:Button ID="btnCloseEmailDesc" OnClientClick="javascript: HideEmailDescPopup('divShowEmailDesc'); return false;" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Close" />                           
                       </td>
                   </tr>
                   <tr>
                       <td>
                           <div style="overflow-y:scroll;  height: 270px; width: 500px;" >
                           <table>
                               <tr>
                                   <td width="10%" style="vertical-align:top; text-align:right;">
                                        <asp:Label ID="lblEmailSubject" Font-Bold="true" Text="Subject: " runat="server"></asp:Label>
                                    </td>
                                    <td width="90%">
                                        <asp:Label ID="lblSubject" runat="server"></asp:Label>
                                    </td>
                               </tr>
                                <tr>
                                   <td style="vertical-align:top; text-align:right;">
                                        <asp:Label ID="lblEmailDesc" Font-Bold="true"  Text="Description: " runat="server"></asp:Label>
                                   </td>
                                   <td>
                                      <asp:Literal ID="litDesc" runat="server"></asp:Literal>
                                   </td>
                               </tr>
                           </table>
                        </div>
                       </td>                       
                   </tr>                  
               </table>
            </div>
      </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
