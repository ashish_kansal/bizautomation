﻿Imports Microsoft.VisualBasic
Imports Microsoft.Owin
Imports Owin
Imports System.Web.Routing
Imports Microsoft.AspNet.SignalR

'<Assembly: OwinStartup(GetType(SignalRChat.Startup))> 
Namespace SignalRChat
    Public Class Startup
        Public Sub configuration(app As IAppBuilder)
            ' any connection or hub wire up and configuration should go here
            GlobalHost.Configuration.DefaultMessageBufferSize = 500
            app.MapHubs()
            'routetable.routes.maphubs()
        End Sub
    End Class
End Namespace