﻿Imports Microsoft.VisualBasic

Public Class Organization
    Public Property recordId() As Integer
        Get
            Return m_recordId
        End Get
        Set(value As Integer)
            m_recordId = Value
        End Set
    End Property
    Private m_recordId As Integer
    Public Property companyName() As String
        Get
            Return m_companyName
        End Get
        Set(value As String)
            m_companyName = Value
        End Set
    End Property
    Private m_companyName As String
    Public Property createdby() As String
        Get
            Return m_createdby
        End Get
        Set(value As String)
            m_createdby = Value
        End Set
    End Property
    Private m_createdby As String
    Public Property createdDate() As String
        Get
            Return m_createdDate
        End Get
        Set(value As String)
            m_createdDate = value
        End Set
    End Property
    Private m_createdDate As String
    Public Property CRMType() As String
        Get
            Return m_CRMType
        End Get
        Set(value As String)
            m_CRMType = value
        End Set
    End Property
    Private m_CRMType As String
End Class
