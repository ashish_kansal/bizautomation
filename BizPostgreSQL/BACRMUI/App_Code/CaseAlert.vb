﻿Imports Microsoft.VisualBasic

Namespace DatabaseNotification
    Public Class CaseAlert
        Public Property caseId() As Integer
            Get
                Return m_caseId
            End Get
            Set(value As Integer)
                m_caseId = value
            End Set
        End Property
        Private m_caseId As Integer
        Public Property OrganizationName() As String
            Get
                Return m_OrganizationName
            End Get
            Set(value As String)
                m_OrganizationName = value
            End Set
        End Property
        Private m_OrganizationName As String
        Public Property CreatedBy() As String
            Get
                Return m_CreatedBy
            End Get
            Set(value As String)
                m_CreatedBy = value
            End Set
        End Property
        Private m_CreatedBy As String
        Public Property CreatedDate() As String
            Get
                Return m_CreatedDate
            End Get
            Set(value As String)
                m_CreatedDate = value
            End Set
        End Property
        Private m_CreatedDate As String
        Public Property ContactName() As String
            Get
                Return m_ContactName
            End Get
            Set(value As String)
                m_ContactName = value
            End Set
        End Property
        Private m_ContactName As String
        Public Property CaseName() As String
            Get
                Return m_CaseName
            End Get
            Set(value As String)
                m_CaseName = value
            End Set
        End Property
        Private m_CaseName As String

        Public Property OppType() As String
            Get
                Return m_OppType
            End Get
            Set(value As String)
                m_OppType = value
            End Set
        End Property
        Private m_OppType As String

        Public Property OppStatus() As String
            Get
                Return m_OppStatus
            End Get
            Set(value As String)
                m_OppStatus = value
            End Set
        End Property
        Private m_OppStatus As String
    End Class
End Namespace
