Imports System
Imports System.IO
Imports System.Web
Imports System.Text
Imports System.Security.Cryptography



Public Class QueryStringModule
    Implements IHttpModule

    Public Sub Dispose() Implements System.Web.IHttpModule.Dispose
    End Sub

    Public Sub Init(ByVal context As HttpApplication) Implements System.Web.IHttpModule.Init
        AddHandler context.BeginRequest, AddressOf context_BeginRequest
    End Sub

    Private Const PARAMETER_NAME As String = "enc="
    Private Const ENCRYPTION_KEY As String = "tOtAlbIzV1"

    Sub context_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        'Some page does not work when retriving query
        'If Not BACRM.BusinessLogic.Common.CCommon.ToString(System.Configuration.ConfigurationManager.AppSettings("IsDebugMode")) = "true" Then
        Dim context As HttpContext = HttpContext.Current
        Dim RequestedPage = context.Request.Url.Segments(context.Request.Url.Segments.Length - 1)
        If RequestedPage.ToLower() = "webform1.aspx" Or RequestedPage.ToLower() = "frmemailinbox.aspx" Or RequestedPage.ToLower() = "frmmaildtl.aspx" Or RequestedPage = "frmTemplate.aspx" Or RequestedPage.ToLower() = "frmviewattachment.aspx" Or context.Request.Url.ToString.ToLower.Contains("telerik") Or RequestedPage.ToLower() = "googleauthenticatefirst.aspx" Or RequestedPage.ToLower() = "imageuploadresize.ashx" Then
            Exit Sub
        End If
        'Session doesn't work in this event
        'If RequestedPage.ToLower.Contains("aspx") Then
        '    If context.Session("DomainID") Is Nothing Then
        '        context.Response.Redirect("Login.aspx")
        '    End If
        'End If
        If context.Request.Url.OriginalString.Contains("aspx") AndAlso context.Request.RawUrl.Contains("?") Then
            Dim query As String = ExtractQuery(context.Request.RawUrl)
            Dim path As String = GetVirtualPath
            If query.StartsWith(PARAMETER_NAME, StringComparison.OrdinalIgnoreCase) Then
            Else
                If context.Request.HttpMethod = "GET" Then

                    Dim encryptedQuery As String = Encrypt(query)
                    context.Response.Redirect(path + encryptedQuery)
                End If
            End If
        End If
        'End If
    End Sub

    Private Shared Function GetVirtualPath() As String
        Dim path As String = HttpContext.Current.Request.RawUrl
        path = path.Substring(0, path.IndexOf("?"))
        path = path.Substring(path.LastIndexOf("/") + 1)
        Return path
    End Function

    Private Shared Function ExtractQuery(ByVal url As String) As String
        Dim index As Integer = url.IndexOf("?") + 1
        Return url.Substring(index)
    End Function

    Private Shared ReadOnly SALT As Byte() = Encoding.ASCII.GetBytes(ENCRYPTION_KEY.Length.ToString)

    Public Shared Function Encrypt(ByVal inputText As String) As String
        Dim rijndaelCipher As RijndaelManaged = New RijndaelManaged
        Dim plainText As Byte() = Encoding.Unicode.GetBytes(inputText)
        Dim SecretKey As PasswordDeriveBytes = New PasswordDeriveBytes(ENCRYPTION_KEY, SALT)
        ' Using
        Dim encryptor As ICryptoTransform = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16))
        Try
            ' Using
            Dim memoryStream As MemoryStream = New MemoryStream
            Try
                ' Using
                Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
                Try
                    cryptoStream.Write(plainText, 0, plainText.Length)
                    cryptoStream.FlushFinalBlock()
                    Return "?" + PARAMETER_NAME + Convert.ToBase64String(memoryStream.ToArray)
                Finally
                    CType(cryptoStream, IDisposable).Dispose()
                End Try
            Finally
                CType(memoryStream, IDisposable).Dispose()
            End Try
        Finally
            CType(encryptor, IDisposable).Dispose()
        End Try
    End Function

    Public Shared Function Decrypt(ByVal inputText As String) As String
        inputText = inputText.Replace(" ", "+")
        Dim rijndaelCipher As RijndaelManaged = New RijndaelManaged
        Dim encryptedData As Byte() = Convert.FromBase64String(inputText)
        Dim secretKey As PasswordDeriveBytes = New PasswordDeriveBytes(ENCRYPTION_KEY, SALT)
        ' Using
        Dim decryptor As ICryptoTransform = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
        Try
            ' Using
            Dim memoryStream As MemoryStream = New MemoryStream(encryptedData)
            Try
                ' Using
                Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)
                Try
                    Dim plainText(encryptedData.Length) As Byte
                    Dim decryptedCount As Integer = cryptoStream.Read(plainText, 0, plainText.Length)
                    Return Encoding.Unicode.GetString(plainText, 0, decryptedCount)
                Finally
                    CType(cryptoStream, IDisposable).Dispose()
                End Try
            Finally
                CType(memoryStream, IDisposable).Dispose()
            End Try
        Finally
            CType(decryptor, IDisposable).Dispose()
        End Try
    End Function
End Class