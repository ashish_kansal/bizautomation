Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Collections.Generic



Public Class DropDownListAdapter

    Inherits System.Web.UI.WebControls.Adapters.WebControlAdapter

    Public Shared ReadOnly OPTGROUPATTR As String = "OptionGroup"

    Private optionGroups As New Dictionary(Of String, String)()

    Protected Overloads Overrides Sub RenderContents(ByVal writer As HtmlTextWriter)
        Dim list As DropDownList = TryCast(Me.Control, DropDownList)

        Dim currentOptionGroup As String
        Dim renderedOptionGroups As New List(Of String)()

        For Each item As ListItem In list.Items
            Dim keyString As String = item.Value + item.Text
            If item.Attributes(OPTGROUPATTR) Is Nothing AndAlso Not optionGroups.ContainsKey(keyString) Then
                RenderListItem(item, writer)
            Else
                If item.Attributes(OPTGROUPATTR) IsNot Nothing Then
                    currentOptionGroup = item.Attributes(OPTGROUPATTR)
                Else
                    currentOptionGroup = optionGroups(keyString)
                End If

                If renderedOptionGroups.Contains(currentOptionGroup) Then
                    RenderListItem(item, writer)
                Else
                    If renderedOptionGroups.Count > 0 Then
                        RenderOptionGroupEndTag(writer)
                    End If

                    RenderOptionGroupBeginTag(currentOptionGroup, writer)
                    renderedOptionGroups.Add(currentOptionGroup)

                    RenderListItem(item, writer)
                End If
            End If
        Next

        If renderedOptionGroups.Count > 0 Then
            RenderOptionGroupEndTag(writer)
        End If
    End Sub

    Private Sub RenderOptionGroupBeginTag(ByVal name As String, ByVal writer As HtmlTextWriter)
        writer.WriteBeginTag("optgroup")
        writer.WriteAttribute("label", name)
        writer.Write(HtmlTextWriter.TagRightChar)
        writer.WriteLine()
    End Sub

    Private Sub RenderOptionGroupEndTag(ByVal writer As HtmlTextWriter)
        writer.WriteEndTag("optgroup")
        writer.WriteLine()
    End Sub

    Private Sub RenderListItem(ByVal item As ListItem, ByVal writer As HtmlTextWriter)

        Me.Page.ClientScript.RegisterForEventValidation(Me.Control.UniqueID, item.Value)

        writer.WriteBeginTag("option")
        writer.WriteAttribute("value", item.Value, True)

        If item.Selected Then
            writer.WriteAttribute("selected", "selected", False)
        End If

        For Each key As String In item.Attributes.Keys
            writer.WriteAttribute(key, item.Attributes(key))
        Next

        writer.Write(HtmlTextWriter.TagRightChar)
        HttpUtility.HtmlEncode(item.Text, writer)
        writer.WriteEndTag("option")
        writer.WriteLine()
    End Sub

    '
    ' Summary:
    '     Saves view state information for the control adapter.
    '
    ' Returns:
    '     An System.Object that contains the adapter view state information as a System.Web.UI.StateBag.
    Protected Overloads Overrides Function SaveAdapterViewState() As Object
        Dim list As DropDownList = TryCast(Me.Control, DropDownList)
        Dim currentOptionGroup As String
        Dim keyString As String

        For Each item As ListItem In list.Items
            If item.Attributes(OPTGROUPATTR) IsNot Nothing Then
                currentOptionGroup = item.Attributes(OPTGROUPATTR)
                keyString = item.Value + item.Text
                optionGroups(keyString) = currentOptionGroup
            End If
        Next


        Return optionGroups
    End Function


    '
    ' Summary:
    '     Loads adapter view state information that was saved by System.Web.UI.Adapters.ControlAdapter.SaveAdapterViewState()
    '     during a previous request to the page where the control associated with this
    '     control adapter resides.
    '
    ' Parameters:
    '   state:
    '     An System.Object that contains the adapter view state information as a System.Web.UI.StateBag.
    Protected Overloads Overrides Sub LoadAdapterViewState(ByVal state As Object)

        optionGroups = TryCast(state, Dictionary(Of String, String))
    End Sub

End Class

