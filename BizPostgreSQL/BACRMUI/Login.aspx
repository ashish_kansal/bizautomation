<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits=".Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Total BizAutomation - Customer Login</title>
    <link rel="shortcut icon" type="image/ico" href="images/favicon.ico">
    <link rel="stylesheet" href="CSS/Login.css" type="text/css" />
    <link href="CSS/bootstrap.min.css" rel="stylesheet" />
    <link href="CSS/font-awesome.min.css" rel="stylesheet" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .info, .warning {
            border: 1px solid;
            /*margin: 10px 5px 0px 5px;*/
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            font-family: Arial,Verdana;
            font-size: small;
        }

        .info {
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('images/info.png');
        }

        .warning {
            color: #9F6000;
            background-color: #FEEFB3;
            background-image: url('images/warning.png');
        }
    </style>
    <script src="JavaScript/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script type='text/javascript' src='http://platform.linkedin.com/in.js'> api_key: 75xmt0efgvwi1m </script>
    
    <script type="text/javascript">
        function onLinkedInLoad() {
            IN.UI.Authorize().place();
            IN.Event.on(IN, "auth", onLinkedInAuth);
            return false;
        }
        function onLinkedInAuth() {
            IN.API.Profile("me")
            .fields("id", "firstName", "lastName", "industry", "location:(name)", "picture-url", "headline", "summary", "num-connections", "public-profile-url", "distance", "positions", "email-address", "educations", "date-of-birth")
            .result(displayProfiles)
            .error(displayProfilesErrors);
        }
        function displayProfiles(profiles) {
            member = profiles.values[0];
            var dataUrl = "";
            dataUrl = member.publicProfileUrl;
            var dataType = "";
            var dataText = "";
            $("#txtOffset").val(new Date().getTimezoneOffset()); /*  new Date().toLocaleString()*/
            $("#hdnLinkdein").val(dataUrl);
            $("#btnLinkdeinLogin").click();
            //alert(member.publicProfileUrl);
            //$.ajax({
            //    type: 'POST',
            //    async: false,
            //    url: 'Login.aspx/AuthenticateUser',
            //    data: JSON.stringify({ 'Password': dataUrl, 'offset': new Date().getTimezoneOffset() }),
            //    contentType: "application/json; charset=utf-8",
            //    success: function (data) {
            //        dataType = data.d[0];
            //        dataText = data.d[1];
            //    },
            //    error: function (dataError) {
            //        alert(dataError);
            //    }
            //})
            //alert(dataType);
            //alert(dataText);
        }
        function displayProfilesErrors(error) {
            profilesDiv = document.getElementById("profiles");
            profilesDiv.innerHTML = error.message;
            console.log(error);
        }
    </script>
    <script type="text/JavaScript" language="JavaScript">
        var mine = window.open('', '', 'width=1,height=1,left=0,top=0,scrollbars=no');
        if (mine)
            var popUpsBlocked = false
        else
            var popUpsBlocked = true
        mine.close()
    </script>
    <script language="javascript" type="text/javascript">
        function Login() {
            if ($("#txtEmaillAdd").val() == "") {
                alert("Enter Email Address")
                $("#txtEmaillAdd").focus()
                return false;
            }
            if ($("#txtPassword").val() == "") {
                alert("Enter Password")
                $("#txtPassword").focus()
                return false;
            }
            $("#txtOffset").val(new Date().getTimezoneOffset()); /*  new Date().toLocaleString()*/
        }
        function Forgot() {
            if ($("#txtEmailPas").val() == "") {
                alert("Enter Email Address")
                $("#txtEmailPas").focus()
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        function OpenPage(a) {
            //
            //window.location.href='frmRedirectPage.aspx?Page='+a
            window.open(a, '', 'width=776,height=660,status=no,scrollbar=1,resizable=1;top=100,left=100');

            return false;
        }
        function OpenPage1(a) {
            //
            //window.location.href='frmRedirectPage.aspx?Page='+a
            window.open(a, '', 'width=950,height=660,status=no,scrollbar=1,resizable=1;top=100,left=100');

            return false;
        }
        $(document).ready(function () {
            $('body').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    $('#btnLogin').click();
                }
            })
        })
    </script>
</head>
<body class="theme-default biz-signin _<%=hdnTemplateSkin.Value %>" style="">
    <!-- Page background -->
    <div id="biz-signin-bg">
        <!-- Background overlay -->
        <div class="overlay"></div>
        <!-- Replace this with your bg image -->
        <img src="images/signin-bg-1.jpg" alt="" style="">
    </div>
    <!-- / Page background -->


    <!-- Container -->
    <div class="signin-container">
        <div class="notification">
            <% 
                Dim ShowPanel As Boolean = False
                Dim ShowTillDate As Date = Date.Parse("07/15/2015") 'Enter in MM/DD/YYYY format
                'Response.Write(ShowTillDate.ToString())
                'Response.Write(Date.Today.ToString())
                If ShowTillDate < Date.Today Then
                    ShowPanel = False
                End If
                pnlMaintanceInfo.Visible = ShowPanel


            %>
            <asp:Panel runat="server" ID="pnlMaintanceInfo" CssClass="info">
                We will be performing scheduled maintenance and upgrades on <font color="red">Monday,12
                                                        july between 3.30 to 5.30 a.m PST</font>.
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlLogout" CssClass="warning" Visible="false">
                For security reasons, you have been automatically logged out of BizAutomation.com.
                                                    Please login again.
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlServiceStopped" CssClass="warning" Visible="false">
                Biz Automated service isn't started by Biz,Please ask Biz-Support Team to Start...!
            </asp:Panel>
        </div>
        <form id="form1" runat="server" defaultbutton="btnLogin">
            
    <asp:HiddenField ID="hdnTemplateSkin" runat="server" />
            <!-- Left side -->
            <div class="signin-info">
                <a href="#" class="logo">
                    <asp:Image ID="imgLogo" ImageUrl="~/images/bizlogo.png" CssClass="img img-responsive"   style="border-width: 0px;
    margin: auto;
    margin-top: 15%;
    max-height: 120px;
    /* margin: auto; */
    margin-bottom: -26% !important;" runat="server" />
                    &nbsp;
				
                </a>
                <label style="    color: #fff;
    font-weight: 500;
    font-size: 14px;
    margin-top: 30%;
    text-align: center;"><i> <b>Powered by</b> : BizAutomation.com</i></label>
                <asp:Panel runat="server" ID="pnlReleaseNote" Visible="false" Style="color: #fff">
                    <ul>
                        <li id="panel_header">
                            <h6>BizAutomation.com Release Schedule</h6>
                        </li>
                        <div id="panel_content" style="display: block;">
                            <li><i class="fa fa-file-text-o signin-icon"></i>BizAutomation.com was last updated on 03/08/2014</li>
                            <li><i class="fa fa-outdent signin-icon"></i>Refer to this section for information on future release dates and times.</li>
                        </div>
                    </ul>
                </asp:Panel>
            </div>
            <!-- / Left side -->

            <div class="signin-form">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                <div class="signin-text">
                    <span>Sign In to your account</span>
                </div>
                <!-- / .signin-text -->

                <div class="form-group w-icon">
                    <asp:TextBox ID="txtEmaillAdd" placeholder="yourname@email.com" CssClass="form-control input-lg" runat="server"></asp:TextBox>
                    <%--<input type="text" name="signin_username" id="username_id" class="form-control input-lg" placeholder="Username or email">--%>
                    <span class="fa fa-user signin-form-icon"></span>
                </div>
                <!-- / Username -->

                <div class="form-group w-icon">
                    <asp:TextBox ID="txtPassword" placeholder="password" CssClass="form-control input-lg" TextMode="Password" runat="server"></asp:TextBox>
                    <%--<input type="password" name="signin_password" id="password_id" class="form-control input-lg" placeholder="Password">--%>
                    <span class="fa fa-lock signin-form-icon"></span>
                </div>
                <!-- / Password -->

                <div class="form-actions">
                    <asp:Button ID="btnLinkdeinLogin" Style="display: none" CssClass="signin-btn bg-primary" runat="server" Text="Linkdein Login" Width="80"></asp:Button>
                    <asp:HiddenField ID="hdnLinkdein" runat="server" />
                    <asp:Button ID="btnLogin" CssClass="signin-btn bg-primary" runat="server" Text="Login"></asp:Button>
                    <%--<input type="submit" value="SIGN IN" class="signin-btn bg-primary">--%>
                    <a href="javascript:void(0)" class="forgot-password" id="forgot-password-link">Forgot your password?</a>
                </div>
                <!-- / .form-actions -->
                <!-- / Form -->

                <!-- "Sign In with" block -->
                <div class="signin-with" style="display:none">
                    <!-- Facebook -->
                    <a href="javascript:void(0)" id="linkedinLogin" onclick="onLinkedInLoad()" class="signin-with-btn" style="background: #4f6faa; background: #0077B5;"><i style="font-size: 19px;" class="fa fa-linkedin-square"></i>&nbsp;&nbsp;Log In with <span>Linkedin</span></a>
                </div>
                <!-- / "Sign In with" block -->

                <!-- Password reset form -->
                <div class="password-reset-form" id="password-reset-form">
                    <div class="header">
                        <div class="signin-text">
                            <span>Password reset</span>
                            <div class="close">�</div>
                        </div>
                        <!-- / .signin-text -->
                    </div>
                    <!-- / .header -->

                    <!-- Form -->
                    <div class="form-group w-icon">
                        <asp:TextBox ID="txtEmailPas" placeholder="yourname@email.com" CssClass="form-control input-lg" runat="server"></asp:TextBox>
                        <%--<input type="text" name="password_reset_email" id="p_email_id" class="form-control input-lg" placeholder="Enter your email">--%>
                        <span class="fa fa-envelope signin-form-icon"></span>
                    </div>
                    <!-- / Email -->

                    <div class="form-actions">
                        <asp:Button ID="btnSendPwd" CssClass="signin-btn bg-primary" runat="server" Text="Send Password"></asp:Button>
                        <%--<input type="submit" value="SEND PASSWORD RESET LINK" class="signin-btn bg-primary">--%>
                    </div>
                    <!-- / .form-actions -->
                    <!-- / Form -->
                </div>
                <!-- / Password reset form -->







                <div align="center" style="display: none">
                    <table width="1000" border="0" bgcolor="#FFFFFF">
                        <tr>
                            <td colspan="2" align="center">
                                <div align="center">
                                    <table border="0" width="100%" cellspacing="1" bgcolor="#FFFFFF">
                                        <tr>
                                            <td width="968" colspan="2">
                                                <div style="border: solid 1px lightgray;">
                                                    <table width="100%" border="0" cellpadding="0" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"
                                                        cellspacing="0">
                                                        <tr bgcolor="#E2E6FF">
                                                            <td align="center" width="7%" bgcolor="#E2E6EF">&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr bgcolor="">
                                                            <td align="center" bgcolor="">
                                                                <table style="position: relative" border="0">
                                                                    <tr>
                                                                        <td valign="bottom" align="right"></td>
                                                                    </tr>
                                                                </table>
                                                                <div align="center">
                                                                    <div style="float: left; width: 50%">

                                                                        <div style="float: right; padding-left: 5px; padding-bottom: 5px; margin-right: 2px;">
                                                                            <table border="0" style="border: solid 1px lightgray" cellpadding="5">
                                                                                <tr>
                                                                                    <td class="normal4" colspan="2"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" align="center">
                                                                                        <%--<img id="linkedinLogin" onclick="onLinkedInLoad()" src="images/linkedin-button-login.png" style="cursor: pointer" />--%>
                                                                              
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <h2><span>OR</span></h2>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <%-- <td class="normal1" align="right">Email
                                                                            </td>--%>
                                                                                    <td align="left" colspan="2">
                                                                                        <%--<asp:TextBox ID="txtEmaillAdd" placeholder="yourname@email.com" CssClass="signup" runat="server" Width="250"></asp:TextBox>--%>

                                                                                    </td>
                                                                                </tr>

                                                                                <tr id="trPassword" runat="server">
                                                                                    <%--<td class="normal1" align="right">Password
                                                                            </td>--%>
                                                                                    <td align="left" colspan="2">
                                                                                        <%--<asp:TextBox ID="txtPassword" placeholder="password" CssClass="signup" TextMode="Password" Width="250" runat="server"></asp:TextBox>--%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trLogin" runat="server">
                                                                                    <td align="center" colspan="2"></td>
                                                                                </tr>
                                                                                <tr id="trForgotPwd" runat="server" visible="false">
                                                                                    <td align="center" colspan="2">
                                                                                        <%-- <asp:Button ID="btnSendPwd" CssClass="button" runat="server" Text="Send Password"></asp:Button>--%>
                                                                                        <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back"></asp:Button>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trForgotPwdLink" runat="server">
                                                                                    <td class="text_bold" align="center" colspan="2">
                                                                                        <asp:LinkButton ID="lnkForgot" runat="server"><font color="#52658C">Forgot Password</font></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <%--  <tr>
                                                            <td colspan="2">
                                                                <img src="images/New LogoBiz1.JPG">
                                                            </td>
                                                        </tr>--%>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div style="float: right; width: 50%">
                                                                        <div style="float: left; margin-left: 2px;">
                                                                            <% 
                                                                                'Dim ShowPanelRealese As Boolean = False
                                                                                'Dim ShowTillDateRelease As Date = Date.Parse("3/15/2013") 'Enter in MM/DD/YYYY format
                                                                                ''Response.Write(ShowTillDate.ToString())
                                                                                ''Response.Write(Date.Today.ToString())
                                                                                'If ShowTillDateRelease > Date.Today Then
                                                                                '    ShowPanelRealese = True
                                                                                'End If
                                                                                'pnlReleaseNote.Visible = ShowPanelRealese
                                                    
                                                    
                                                                            %>
                                                                        </div>
                                                                    </div>


                                                                </div>

                                                                <table style="position: relative" border="0">
                                                                    <tr>
                                                                        <td valign="bottom" align="right"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" width="7%" bgcolor="#E2E6EF">&nbsp;
                                                            </td>
                                                        </tr>
                                                        <!--CRM-->
                                                        <!-- SFA -->
                                                    </table>

                                                </div>


                                            </td>

                                        </tr>
                                    </table>
                                </div>

                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div align="center">

                <asp:TextBox ID="txtOffset" runat="server" Style="display: none"></asp:TextBox>
                <script type="text/JavaScript" language="JavaScript">
                    if (popUpsBlocked)
                        alert('We have detected that you are using pop-up blocking software.\n Please allow all pop-ups from Bizautomation.com.');
                </script>
            </div>
        </form>

    </div>

    <div class="not-a-member">
        <i><small>BizAutomation.com� 2003 - <%=DateTime.Now.Year%> - All rights reserved.</small></i>
        <%--<small><b>BizAutomation.com is best viewed with the latest version of Mozilla Firefox or Google Chrome at a screen resolution of 1280 x 1024 or higher.</b></small>--%>
    </div>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-503646-1']);
        _gaq.push(['_setDomainName', 'bizautomation.com']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            var $ph = $('#biz-signin-bg'),
                $img = $ph.find('> img');

            $(window).on('resize', function () {
                $img.attr('style', '');
                if ($img.height() < $ph.height()) {
                    $img.css({
                        height: '100%',
                        width: 'auto'
                    });
                }
            });

            $('#forgot-password-link').click(function () {
                $('#password-reset-form').fadeIn(400);
                return false;
            });
            $('#password-reset-form .close').click(function () {
                $('#password-reset-form').fadeOut(400);
                return false;
            });

        });
    </script>
</body>
</html>
