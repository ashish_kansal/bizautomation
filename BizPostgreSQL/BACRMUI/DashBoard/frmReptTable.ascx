<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmReptTable.ascx.vb"
    Inherits=".frmReptTable" %>
<%--<script src="../JavaScript/jquery.min.js" type="text/javascript"></script>--%>
<script type="text/javascript" language="javascript">
    function Redirect(a) {
        document.location.href = '../reports/frmCustomReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ReportId=' + a + '&frm=Dashboard';
    }
    function OpenBizInvoice(a, b) {
        window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
    }

    $(document).ready(function () {
        $('a[href*="frmMailDtl.aspx"]').click(function () {
            window.open($(this).attr('href'), '', 'titlebar=no,top=100,left=250,width=850,height=550,scrollbars=yes,resizable=yes');
            return false;
        });
    });
</script>
<div class="portlet" runat="server" id="dvTable" clientidmode="Static">
    <div class="portlet-header">
        <asp:Label Text="" ID="lblHeader" runat="server" />
    </div>
    <div class="portlet-content">
        <asp:Panel ID="pnlChart" runat="server">
            <table width="100%">
                <tr>
                    <td>
                    </td>
                    <td id="tdEdit" runat="server" align="right" visible="false">
                        <%--<asp:HyperLink ID="hplMoveLeft" runat="server" CssClass="hyperlink"><img border="0" src="../images/move_left.gif" /></asp:HyperLink>
                        <asp:HyperLink ID="hplMoveDown" runat="server" CssClass="hyperlink"><img  border="0" src="../images/move_down.gif" /></asp:HyperLink>
                        <asp:HyperLink ID="hplMoveRight" runat="server" CssClass="hyperlink"><img  border="0" src="../images/move_right.gif" /></asp:HyperLink>
                        <asp:HyperLink ID="hplMoveUp" runat="server" CssClass="hyperlink"><img  border="0" src="../images/move_up.gif" /></asp:HyperLink>--%>
                        <asp:HyperLink ID="hplEdit" CssClass="hyperlink" runat="server">Edit</asp:HyperLink>
                        <asp:HyperLink ID="hplDelete" CssClass="hyperlink" runat="server">Delete</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="dgReport" AutoGenerateColumns="false" runat="server" Width="100%"
                            CssClass="hs">
                            <HeaderStyle CssClass="hs" />
                            <AlternatingRowStyle CssClass="ais" />
                            <RowStyle CssClass="is" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</div>
