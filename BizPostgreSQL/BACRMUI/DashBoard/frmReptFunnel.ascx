<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmReptFunnel.ascx.vb"
    Inherits=".frmReptFunnel" %>
<script type="text/javascript" language="javascript">
    function Redirect(a) {
        document.location.href = '../reports/frmCustomReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ReportId=' + a + '&frm=Dashboard';
    }
</script>
<div class="portlet" runat="server" id="dvTable" clientidmode="Static">
    <div class="portlet-header">
        <asp:Label Text="" ID="lblHeader" runat="server" />
    </div>
    <div class="portlet-content">
        <asp:Panel ID="pnlChart" runat="server">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td id="tdEdit" runat="server" align="right" visible="false">
                        <%--<asp:HyperLink ID="hplMoveLeft" runat="server" CssClass="hyperlink" ><img border="0" src="../images/move_left.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveDown" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_down.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveRight" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_right.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveUp" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_up.gif" /></asp:HyperLink>--%>
                        <asp:HyperLink ID="hplEdit" CssClass="hyperlink" runat="server">Edit</asp:HyperLink>
                        <asp:HyperLink ID="hplDelete" CssClass="hyperlink" runat="server">Delete</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td>
                        <igchart:UltraChart id="UltraChart1" runat="server" ChartType="FunnelChart" Width="400px"
                            Height="300px" Version="6.2">
                            <data>
						<EmptyStyle>
							<PointPE FillOpacity="150" FillStopOpacity="150" Fill="Transparent" StrokeOpacity="150"></PointPE>
							<LineStyle DrawStyle="Dash"></LineStyle>
							<PE FillOpacity="150" FillStopOpacity="150" Fill="Transparent" StrokeOpacity="150"></PE>
						</EmptyStyle>
					</data>
                            <titleleft visible="False" orientation="VerticalLeftFacing" horizontalalign="Near"
                                extent="26" location="Left">
						<Margins Bottom="5" Top="5" Right="5" Left="5"></Margins>
					</titleleft>
                            <funnelchart labels-labelstyle-font="Microsoft Sans Serif, 7.8pt" labels-labelstyle-dx="0"
                                labels-labelstyle-horizontalalign="Near" labels-labelstyle-fontsizebestfit="False"
                                labels-labelstyle-cliptext="True" labels-labelstyle-rotationangle="0" labels-labelstyle-orientation="Horizontal"
                                labels-labelstyle-wraptext="False" labels-labelstyle-flip="False" labels-labelstyle-fontcolor="Black"
                                labels-labelstyle-verticalalign="Center" labels-labelstyle-dy="0"></funnelchart>
                            <colormodel modelstyle="CustomLinear" alphalevel="150">
						<Skin>
							<PEs>
								<igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="108, 162, 36" FillStopColor="148, 244, 17" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="7, 108, 176" FillStopColor="53, 200, 255" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="230, 190, 2" FillStopColor="255, 255, 81" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="215, 0, 5" FillStopColor="254, 117, 16" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="252, 122, 10" FillStopColor="255, 108, 66" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
							</PEs>
						</Skin>
					</colormodel>
                            <legend formatstring="&lt;ITEM_LABEL&gt;">
                                <margins bottom="5" top="5" right="5" left="5"></margins>
                            </legend>
                            <effects>
						<Effects>
							<igchartprop:GradientEffect Style="ForwardDiagonal" Coloring="Darken"></igchartprop:GradientEffect>
						</Effects>
					</effects>
                            <axis>
						<X2 Visible="False" TickmarkInterval="10" LineThickness="1" TickmarkStyle="Smart">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" Fill="Transparent" StrokeOpacity="150"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0"></ScrollScale>
							<Labels SeriesFormatString="" ItemFormatString="" VerticalAlign="Center" Visible="True" HorizontalAlign="Far" Font="Verdana, 7pt" ItemFormat="None" FontColor="Gray" Orientation="VerticalLeftFacing">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="Gray" Orientation="VerticalLeftFacing" Visible="True" FormatString="">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"></MinorGridLines>
						</X2>
						<Z Visible="True" LineThickness="1" TickmarkStyle="Smart">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" Fill="Transparent" StrokeOpacity="150"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0"></ScrollScale>
							<Labels SeriesFormatString="&lt;SERIES_LABEL&gt;" ItemFormatString="" VerticalAlign="Center" Visible="True" HorizontalAlign="Far" Font="Verdana, 7pt" ItemFormat="None" FontColor="DimGray" Orientation="Horizontal">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="DimGray" Orientation="Horizontal" Visible="True" FormatString="&lt;SERIES_LABEL&gt;">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"></MinorGridLines>
						</Z>
						<Z2 Visible="True" LineThickness="1" TickmarkStyle="Smart">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" Fill="Transparent" StrokeOpacity="150"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0"></ScrollScale>
							<Labels SeriesFormatString="&lt;SERIES_LABEL&gt;" ItemFormatString="" VerticalAlign="Center" Visible="True" HorizontalAlign="Near" Font="Verdana, 7pt" ItemFormat="None" FontColor="Gray" Orientation="Horizontal">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="Gray" Orientation="Horizontal" Visible="True" FormatString="&lt;SERIES_LABEL&gt;">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"></MinorGridLines>
						</Z2>
						<X LineEndCapStyle="Flat" Visible="True" TickmarkInterval="10" LineThickness="1" Extent="40" TickmarkStyle="Smart">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" Fill="Transparent" StrokeOpacity="150"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0"></ScrollScale>
							<Labels SeriesFormatString="" ItemFormatString="&lt;ITEM_LABEL&gt;" VerticalAlign="Center" Visible="True" HorizontalAlign="Near" Font="Verdana, 7pt" ItemFormat="ItemLabel" FontColor="DimGray" Orientation="VerticalLeftFacing">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="DimGray" Orientation="VerticalLeftFacing" Visible="True" FormatString="">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"></MinorGridLines>
						</X>
						<Y LineEndCapStyle="Flat" Visible="True" TickmarkInterval="10" LineThickness="1" Extent="30" TickmarkStyle="Smart">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" Fill="Transparent" StrokeOpacity="150"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0"></ScrollScale>
							<Labels SeriesFormatString="" ItemFormatString="&lt;DATA_VALUE:00.##&gt;" VerticalAlign="Center" Visible="True" HorizontalAlign="Far" Font="Verdana, 7pt" ItemFormat="DataValue" FontColor="DimGray" Orientation="Horizontal">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="DimGray" Orientation="Horizontal" Visible="True" FormatString="">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"></MinorGridLines>
						</Y>
						<Y2 Visible="False" TickmarkInterval="10" LineThickness="1" TickmarkStyle="Smart">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" Fill="Transparent" StrokeOpacity="150"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0"></ScrollScale>
							<Labels SeriesFormatString="" ItemFormatString="" VerticalAlign="Center" Visible="True" HorizontalAlign="Near" Font="Verdana, 7pt" ItemFormat="None" FontColor="Gray" Orientation="Horizontal">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="Gray" Orientation="Horizontal" Visible="True" FormatString="">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"></MinorGridLines>
						</Y2>
					</axis>
                            <titleright visible="False" orientation="VerticalRightFacing" horizontalalign="Near"
                                extent="26" location="Right">
						<Margins Bottom="5" Top="5" Right="5" Left="5"></Margins>
					</titleright>
                            <titlebottom visible="False" orientation="Horizontal" horizontalalign="Far" extent="26"
                                location="Bottom">
						<Margins Bottom="5" Top="5" Right="5" Left="5"></Margins>
					</titlebottom>
                            <titletop visible="True" orientation="Horizontal" horizontalalign="Near" extent="33"
                                location="Top">
						<Margins Bottom="5" Top="5" Right="5" Left="5"></Margins>
					</titletop>
                        </igchart:UltraChart>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</div>
