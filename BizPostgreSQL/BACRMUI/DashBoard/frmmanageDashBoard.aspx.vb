Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Partial Public Class frmmanageDashBoard
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                
                Dim objDashboard As New DashBoard
                objDashboard.DashboardReptId = CCommon.ToLong(GetQueryStringVal("DID"))
                objDashboard.MoveDashboard = GetQueryStringVal("Move")
                objDashboard.ArrangeDashboard()
            End If
            Response.Redirect("../DashBoard/frmDashBoard.aspx?frm=" & GetQueryStringVal( "frm") & "&GroupID=" & GetQueryStringVal( "GroupID") & "&Edit=" & GetQueryStringVal( "Edit"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class