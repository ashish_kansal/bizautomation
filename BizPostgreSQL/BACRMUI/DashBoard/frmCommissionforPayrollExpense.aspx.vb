﻿''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI

Public Class frmCommissionforPayrollExpense1
    Inherits BACRMPage

#Region "Variables"
    Dim lngComRuleID As Long
    Dim lngItemCode As Long
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngComRuleID = CCommon.ToLong(GetQueryStringVal("ComRuleID"))
            lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))

            btnCancel.Attributes.Add("onclick", "return Close()")
            If Not IsPostBack Then
                

                LoadCommissionListGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadCommissionListGrid()
        Try
            Dim lobjPayrollExpenses As New PayrollExpenses
            lobjPayrollExpenses.DomainId = Session("DomainId")
            lobjPayrollExpenses.UserCntID = Session("UserContactID")
            lobjPayrollExpenses.ComRuleID = lngComRuleID
            lobjPayrollExpenses.ItemCode = lngItemCode
            lobjPayrollExpenses.bitCommContact = 0

            dgCommission.DataSource = lobjPayrollExpenses.GetUserCommission_CommRule
            dgCommission.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.00}", Money)
            Return String.Empty
        Catch ex As Exception
            Throw ex
        End Try
    End Function


End Class