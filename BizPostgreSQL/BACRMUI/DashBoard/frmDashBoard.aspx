<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDashBoard.aspx.vb"
    Inherits=".frmDashBoard" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../CSS/Master.css" rel="stylesheet" type="text/css" />
    <title>Dashboard</title>
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery-ui-1.8.5.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        
        function OpenDBConf() {
            var DashBoardRptConf = window.open('../dashboard/frmDashBoardRptConf.aspx', 'DashBoardRptConf', 'toolbar=no,titlebar=no,top=100,left=300,scrollbars=yes,resizable=yes');
            DashBoardRptConf.focus()
            return false;
        } 
    </script>
    <script type="text/javascript">
        $(function () {
            $(".column").sortable({
                disable: true,
                connectWith: '.column',
                handle : '.portlet-header',
                stop: function (event, ui) {
                },
                update: function (event, ui) {
                    var Column1;
                    var Column2;
                    var Column3;
                    Column1 = $('#Column1').sortable('toArray');
                    //                    console.log(Column1);
                    Column2 = $('#Column2').sortable('toArray');
                    //                    console.log(Column2);
                    Column3 = $('#Column3').sortable('toArray');
                    //                    console.log(Column3);
                    $.ajax({
                        type: "POST",
                        url: "frmDashBoard.aspx/UpdateOrder",
                        data: '{Column1Values:"' + Column1.toString() + '",Column2Values:"' + Column2.toString() + '",Column3Values:"' + Column3.toString() + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        cache: false,
                        success: function (msg) {
                            if (msg.d == 'True') {
                                $('#dvSucess').text('Widget order saved sucessfully');
                                FadeOut(2000);
                            }
                        }
                    })

                }

            });

            $("#Column1").css("width", $("#tblRept1").css("width"));
            $("#Column2").css("width", $("#tblRept2").css("width"));
            $("#Column3").css("width", $("#tblRept3").css("width"));

            $(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
			.find(".portlet-header")
				.addClass("ui-widget-header ui-corner-all")
				.prepend('<span class="ui-icon ui-icon-minusthick"></span>')
				.end()
			.find(".portlet-content");

            $(".portlet-header .ui-icon").click(function () {
                $(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
                $(this).parents(".portlet:first").find(".portlet-content").toggle();
            });

//             $(".column").disableSelection();
        });

        function FadeOut(NoMSeconds) {
            $('#dvSucess').show();
            setTimeout(function () {
                $("#dvSucess").fadeOut("slow", function () {
                    $("#dvSucess").hide();
                });

            }, NoMSeconds);
        }
    </script>
    <style type="text/css">
        .column
        {
            float: left;
            padding-bottom: 100px;
        }
        .portlet
        {
            margin: 0 1em 1em 0;
            background-color: #F3F4F5;
        }
        .portlet-header
        {
            margin: 0.3em;
            padding-bottom: 4px;
            padding-left: 0.2em;
            background-color: #697496;
            color: White;
            font-family: Sans-Serif, Arial , Tahoma;
            font-size: small;
            cursor: move;
        }
        .portlet-header .ui-icon
        {
            float: right;
        }
        .portlet-content
        {
            padding: 0.4em;
            overflow:scroll;
        }
        .ui-sortable-placeholder
        {
            border: 1px dotted black;
            visibility: visible !important;
            height: 50px !important;
        }
        .ui-sortable-placeholder *
        {
            visibility: hidden;
        }
        .success
        {
            border: 1px solid;
            margin: 10px 0px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            color: #4F8A10;
            background-color: #DFF2BF;
            background-image: url('../images/success.png');
        }
    </style>
    <style>
        .descriptionPanelHeader
        {
            font-family: Arial;
            cursor: move;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <div class="demo" style="margin: 10px 10px 10px 10px;">
        <table border="0">
            <tr>
                <td colspan="2" align="left">
                    <div class="success" id="dvSucess" style="display: none">
                    </div>
                </td>
                <td align="right">
                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="ImageButton Edit" />
                    <asp:Button ID="btnDone" runat="server" Text="Done" CssClass="ImageButton" style="padding-left:5px !important;" />
                </td>
            </tr>
            <tr id="trGroup" runat="server" visible="false">
                <td class="normal1">
                    &nbsp;Group&nbsp;
                    <asp:DropDownList ID="ddlGroup" EnableViewState="true" runat="server" AutoPostBack="true"
                        CssClass="signup" Width="200">
                    </asp:DropDownList>
                    &nbsp;
                    <asp:Button runat="server" ID="btnAllowList" OnClientClick="return OpenDBConf()"
                        CssClass="button" Width="120" Text="Allowed Reports" />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table id="tblRept1" enableviewstate="false" runat="server">
                        <tr>
                            <td class="normal1" runat="server" visible="false" id="td1">
                                Column Size &nbsp;
                                <asp:LinkButton ID="lnkNarrow1" runat="server">Narrow</asp:LinkButton>
                                |
                                <asp:LinkButton ID="lnkMedium1" runat="server">Medium</asp:LinkButton>
                                |
                                <asp:LinkButton ID="lnkWide1" runat="server">Wide</asp:LinkButton>
                            </td>
                        </tr>
                        <tr runat="server" visible="false" id="td2">
                            <td class="normal1">
                                <asp:HyperLink ID="hplAddComponent1" runat="server">Add New Report</asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="column" id="Column1">
                                    <asp:PlaceHolder ID="phColumn1" runat="server"></asp:PlaceHolder>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table id="tblRept2" enableviewstate="false" runat="server">
                        <tr>
                            <td class="normal1" visible="false" runat="server" id="td3">
                                Column Size &nbsp;
                                <asp:LinkButton ID="lnkNarrow2" runat="server">Narrow</asp:LinkButton>
                                |
                                <asp:LinkButton ID="lnkMedium2" runat="server">Medium</asp:LinkButton>
                                |
                                <asp:LinkButton ID="lnkWide2" runat="server">Wide</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" visible="false" runat="server" id="td4">
                                <asp:HyperLink ID="hplAddComponent2" runat="server">Add New Report</asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="column" id="Column2">
                                    <asp:PlaceHolder ID="phColumn2" runat="server"></asp:PlaceHolder>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table id="tblRept3" enableviewstate="false" runat="server">
                        <tr>
                            <td class="normal1" visible="false" runat="server" id="td5">
                                Column Size &nbsp;
                                <asp:LinkButton ID="lnkNarrow3" runat="server">Narrow</asp:LinkButton>
                                |
                                <asp:LinkButton ID="lnkMedium3" runat="server">Medium</asp:LinkButton>
                                |
                                <asp:LinkButton ID="lnkWide3" runat="server">Wide</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" visible="false" runat="server" id="td6">
                                <asp:HyperLink ID="hplAddComponent3" runat="server">Add New Report</asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="column" id="Column3">
                                    <asp:PlaceHolder ID="phColumn3" runat="server"></asp:PlaceHolder>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:TextBox ID="txtEdit" runat="server" Style="display: none"></asp:TextBox>
    </form>
</body>
</html>
