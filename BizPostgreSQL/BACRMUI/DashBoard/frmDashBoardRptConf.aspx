<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDashBoardRptConf.aspx.vb"
    Inherits=".frmDashBoardRptConf" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Untitled Page</title>
    <script type="text/javascript" language="javascript">
        //        function chkAll() {
        //            // alert('yes')
        //            var dgReport = document.getElementById('dgReport')

        //            //alert(dgReport.rows.length)


        //            for (i = 2; i <= document.getElementById('dgReport').rows.length; i++) {
        //                var str;
        //                if (i < 10) {
        //                    str = '0' + i
        //                }
        //                else {
        //                    str = i
        //                }
        //                if (document.getElementById('dgReport_ctl01_chkSelectHdr').checked == true) {
        //                    if (document.getElementById('dgReport_ctl' + str + '_chkSelect') != null) {
        //                        document.getElementById('dgReport_ctl' + str + '_chkSelect').checked = true;
        //                    }
        //                }
        //                else {
        //                    if (document.getElementById('dgReport_ctl' + str + '_chkSelect') != null) {
        //                        document.getElementById('dgReport_ctl' + str + '_chkSelect').checked = false;
        //                    }
        //                }
        //            }
        //        }
        function Save() {
            if (document.getElementById('ddlGroup').value == '0') {
                alert('Select Group')
                return false
            }
            // alert('yes')
            var dgReport = document.getElementById('dgReport')

            //alert(dgReport.rows.length)

            var strVal = ''
            //for (i = 2; i <= document.getElementById('dgReport').rows.length; i++) {
            //    if (i < 10) {
            //        str = '0' + i
            //    }
            //    else {
            //        str = i
            //    }

                $(".dg tr").each(function () {

                    if ($(this).find(".chkSelect input[type = 'checkbox']").is(':checked')) {
                        strVal = $(this).find('#lblRptId').text() + "," + strVal
                    }

                });

//                if (document.getElementById('dgReport_ctl' + str + '_chkSelect') != null) {
//                    if (document.getElementById('dgReport_ctl' + str + '_chkSelect').checked == true) {
//                        if (strVal == '') {
//                            if (document.all) {
//                                strVal = document.getElementById('dgReport_ctl' + str + '_lblRptId').innerText
//                            } else {
//                                strVal = document.getElementById('dgReport_ctl' + str + '_lblRptId').textContent
//                            }
//                        }
//                        else {
//                            if (document.all) {
//                                strVal = strVal + ',' + document.getElementById('dgReport_ctl' + str + '_lblRptId').innerText
//                            } else {
//                                strVal = strVal + ',' + document.getElementById('dgReport_ctl' + str + '_lblRptId').textContent
//                            }
//                        }
//                    }
//                }
            //}

            document.getElementById('txtReportValues').value = strVal
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button runat="server" OnClientClick="return Save()" CssClass="button" Text="Save"
                    ID="btnSave" />
                <asp:Button runat="server" OnClientClick="return Save()" CssClass="button" Text="Save & Close"
                    ID="btnSaveClose" />
                <asp:Button runat="server" OnClientClick="javascript:self.close()" CssClass="button"
                    Text="Close" ID="Button1" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Manage Custom Reports
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr valign="bottom">
            <td align="center" valign="middle" class="normal1">
                Group
                <asp:DropDownList ID="ddlGroup" EnableViewState="true" runat="server" AutoPostBack="true"
                    CssClass="signup" Width="200">
                </asp:DropDownList>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="3" valign="top">
                <asp:DataGrid ID="dgReport" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn SortExpression="vcReportName" ItemStyle-Width="25px" HeaderText=" ">
                            <HeaderTemplate>
                                <asp:CheckBox onclick="SelectAll('chkSelectHdr','chkSelect')" runat="server" ID="chkSelectHdr" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkSelect" CssClass="chkSelect" />
                                <asp:Label runat="server" Style="display: none" ID="lblRptId" Text='<%#Container.DataItem("ReportID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcReportName" SortExpression="vcReportName" HeaderText="Report Name">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="vcReportDescription" SortExpression="vcReportDescription"
                            HeaderText="Report Description"></asp:BoundColumn>
                        <asp:BoundColumn Visible="false" DataField="bitAllowed"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <asp:TextBox runat="server" ID="txtReportValues" Style="display: none"></asp:TextBox>
</asp:Content>
