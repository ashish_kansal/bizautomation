<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmReportDetails.ascx.vb" Inherits=".frmReportDetails" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebChart.v7.1, Version=7.1.20071.40, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebChart" TagPrefix="igchart" %>

<table width="100%">
    <tr>
        <td >
              <asp:Label ID="lblHeader" runat="server" CssClass="text_bold" ></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
           <hr  />
         
        </td>
    </tr>
    <tr>
        <td>
            <igchart:UltraChart ID="UltraChart1" runat="server">
            </igchart:UltraChart>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblFooter" runat="server" CssClass="text_bold" ></asp:Label>
        </td>
    </tr>
</table>

