Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Imports System.Reflection
Imports System.Web.Services

Partial Public Class frmDashBoard
    Inherits BACRMPage
    Dim objDashboard As New DashBoard
    Dim objCustomReports As New CustomReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UserContactID") <> Session("AdminID") Then 'Logged in User is Admin of Domain? yes then override permission and give him access
               
                
                GetUserRightsForPage(13, 27)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnEdit.Visible = False
                    btnDone.Visible = False
                End If
            End If
            If Not IsPostBack Then
                
                If GetQueryStringVal( "Edit") = "True" Then txtEdit.Text = "1"
                If GetQueryStringVal( "frm") = "Admin" Then
                    trGroup.Visible = True
                    Dim objUserGroups As New UserGroups
                    Dim dtGroupName As DataTable
                    objUserGroups.DomainId = Session("DomainID")
                    objUserGroups.SelectedGroupTypes = "1,4"
                    dtGroupName = objUserGroups.GetAutorizationGroup

                    ddlGroup.DataSource = dtGroupName.DataSet.Tables(0).DefaultView
                    ddlGroup.DataTextField = "vcGroupName"
                    ddlGroup.DataValueField = "numGroupId"
                    ddlGroup.DataBind()
                    ddlGroup.Items.Insert(0, "--Select One--")
                    ddlGroup.Items.FindByText("--Select One--").Value = 0

                    If GetQueryStringVal("GroupID") <> "" Then
                        If Not ddlGroup.Items.FindByValue(GetQueryStringVal("GroupID")) Is Nothing Then
                            ddlGroup.Items.FindByValue(GetQueryStringVal("GroupID")).Selected = True
                            LoadDashBoard()
                        End If
                    ElseIf Session("UserGroupID") IsNot Nothing Then
                        If ddlGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                            ddlGroup.ClearSelection()
                            ddlGroup.Items.FindByValue(Session("UserGroupID")).Selected = True
                            LoadDashBoard()
                        End If
                    End If
                Else
                    objDashboard.DomainID = Session("DomainID")
                    objDashboard.UserCntID = Session("UserContactID")
                    objDashboard.GroupId = Session("UserGroupID")
                    Dim ds As DataSet
                    ds = objDashboard.GetDashBoard
                    BuildDashboard(ds)
                    hplAddComponent1.NavigateUrl = "../DashBoard/frmAddDashBoardRPT.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Column=1"
                    hplAddComponent2.NavigateUrl = "../DashBoard/frmAddDashBoardRPT.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Column=2"
                    hplAddComponent3.NavigateUrl = "../DashBoard/frmAddDashBoardRPT.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Column=3"
                End If
            End If

            If Session("UserGroupID") IsNot Nothing Then
                If ddlGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                    ddlGroup.Items.FindByValue(Session("UserGroupID")).Attributes.Add("style", "color:green")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BuildDashboard(ByVal ds As DataSet)
        Try
            Dim dtTable, dtTable1 As DataTable
            dtTable = ds.Tables(0)
            Dim i As Integer
            If dtTable.Rows.Count = 3 Then
                If dtTable.Rows(0).Item("tintColumn") = 1 Then
                    If dtTable.Rows(0).Item("tintSize") = 1 Then
                        tblRept1.Width = "300"
                    ElseIf dtTable.Rows(0).Item("tintSize") = 2 Then
                        tblRept1.Width = "400"
                    ElseIf dtTable.Rows(0).Item("tintSize") = 3 Then
                        tblRept1.Width = "500"
                    Else : tblRept1.Width = "300"
                    End If
                End If
                If dtTable.Rows(1).Item("tintColumn") = 2 Then
                    If dtTable.Rows(1).Item("tintSize") = 1 Then
                        tblRept2.Width = "300"
                    ElseIf dtTable.Rows(1).Item("tintSize") = 2 Then
                        tblRept2.Width = "400"
                    ElseIf dtTable.Rows(1).Item("tintSize") = 3 Then
                        tblRept2.Width = "500"
                    Else : tblRept2.Width = "300"
                    End If
                End If
                If dtTable.Rows(2).Item("tintColumn") = 3 Then
                    If dtTable.Rows(2).Item("tintSize") = 1 Then
                        tblRept3.Width = "300"
                    ElseIf dtTable.Rows(2).Item("tintSize") = 2 Then
                        tblRept3.Width = "400"
                    ElseIf dtTable.Rows(2).Item("tintSize") = 3 Then
                        tblRept3.Width = "500"
                    Else : tblRept3.Width = "300"
                    End If
                End If
            Else
                tblRept1.Width = "300"
                tblRept2.Width = "300"
                tblRept3.Width = "300"
            End If
            dtTable1 = ds.Tables(1)
            Dim tr As HtmlTableRow
            Dim td As HtmlTableCell
            If dtTable1.Rows.Count > 0 Then
                For i = 0 To dtTable1.Rows.Count - 1
                    Dim ReptControl As New Control

                    If dtTable1.Rows(i).Item("tintReportCategory") = 1 Then 'Custom Reports
                        If dtTable1.Rows(i).Item("tintReportType") = 1 Then
                            ReptControl = LoadControl("frmReptTable.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintReportType") = 2 Then
                            If dtTable1.Rows(i).Item("tintChartType") = 1 Then
                                ReptControl = LoadControl("frmReptChart.ascx")
                            ElseIf dtTable1.Rows(i).Item("tintChartType") = 2 Then
                                ReptControl = LoadControl("frmReptBar.ascx")
                            ElseIf dtTable1.Rows(i).Item("tintChartType") = 3 Then
                                ReptControl = LoadControl("frmReptPyramid.ascx")
                            ElseIf dtTable1.Rows(i).Item("tintChartType") = 4 Then
                                ReptControl = LoadControl("frmRept3dPyramid.ascx")
                            ElseIf dtTable1.Rows(i).Item("tintChartType") = 5 Then
                                ReptControl = LoadControl("frmReptFunnel.ascx")
                            ElseIf dtTable1.Rows(i).Item("tintChartType") = 6 Then
                                ReptControl = LoadControl("frmRept3DFunnel.ascx")
                            ElseIf dtTable1.Rows(i).Item("tintChartType") = 7 Then
                                ReptControl = LoadControl("frmRept3DCone.ascx")
                            ElseIf dtTable1.Rows(i).Item("tintChartType") = 8 Then
                                ReptControl = LoadControl("frmReptLine.ascx")
                            ElseIf dtTable1.Rows(i).Item("tintChartType") = 12 Then
                                ReptControl = LoadControl("frmReptPie.ascx")
                            ElseIf dtTable1.Rows(i).Item("tintChartType") = 13 Then
                                ReptControl = LoadControl("frmRept3DPie.ascx")
                            ElseIf dtTable1.Rows(i).Item("tintChartType") = 14 Then
                                ReptControl = LoadControl("frmReptDou.ascx")
                            ElseIf dtTable1.Rows(i).Item("tintChartType") = 15 Then
                                ReptControl = LoadControl("frmRept3DDough.ascx")
                            Else : ReptControl = LoadControl("frmReptChart.ascx")
                            End If
                        End If
                    ElseIf dtTable1.Rows(i).Item("tintReportCategory") = 2 Then 'Commission Item Reports
                        ReptControl = LoadControl("frmReptBar.ascx")
                    End If


                    tr = New HtmlTableRow
                    td = New HtmlTableCell
                    td.ColSpan = 3
                    'td.Controls.Add(ReptControl)

                    'tr.Cells.Add(td)
                    Dim Width As Integer
                    If dtTable1.Rows(i).Item("tintColumn") = 1 Then
                        phColumn1.Controls.Add(ReptControl)
                        'tblRept1.Rows.Add(tr)
                        If dtTable.Rows.Count > 0 Then
                            If dtTable.Rows(0).Item("tintSize") = 1 Then
                                Width = 300
                            ElseIf dtTable.Rows(0).Item("tintSize") = 2 Then
                                Width = 400
                            ElseIf dtTable.Rows(0).Item("tintSize") = 3 Then
                                Width = 500
                            End If
                        Else
                            Width = 300
                        End If
                    ElseIf dtTable1.Rows(i).Item("tintColumn") = 2 Then
                        phColumn2.Controls.Add(ReptControl)
                        'tblRept2.Rows.Add(tr)
                        If dtTable.Rows.Count > 0 Then
                            If dtTable.Rows(1).Item("tintSize") = 1 Then
                                Width = 300
                            ElseIf dtTable.Rows(1).Item("tintSize") = 2 Then
                                Width = 400
                            ElseIf dtTable.Rows(1).Item("tintSize") = 3 Then
                                Width = 500
                            End If
                        Else
                            Width = 300
                        End If

                    ElseIf dtTable1.Rows(i).Item("tintColumn") = 3 Then
                        phColumn3.Controls.Add(ReptControl)
                        'tblRept3.Rows.Add(tr)
                        If dtTable.Rows.Count > 0 Then
                            If dtTable.Rows(2).Item("tintSize") = 1 Then
                                Width = 300
                            ElseIf dtTable.Rows(2).Item("tintSize") = 2 Then
                                Width = 400
                            ElseIf dtTable.Rows(2).Item("tintSize") = 3 Then
                                Width = 500
                            End If
                        Else
                            Width = 300
                        End If

                    End If
                    Dim _myControlType As Type = ReptControl.GetType()
                    Dim _myReptID As PropertyInfo = _myControlType.GetProperty("DashReportID")
                    _myReptID.SetValue(ReptControl, CLng(dtTable1.Rows(i).Item("numDashBoardReptID")), Nothing)
                    If txtEdit.Text = "1" Then
                        Dim _Edit As PropertyInfo = _myControlType.GetProperty("EditRept")
                        _Edit.SetValue(ReptControl, True, Nothing)
                    End If

                    Dim _Width As PropertyInfo = _myControlType.GetProperty("Width")
                    _Width.SetValue(ReptControl, Width, Nothing)

                    Dim _objCustomReport As PropertyInfo = _myControlType.GetProperty("objCustomReport")
                    _objCustomReport.SetValue(ReptControl, objCustomReports, Nothing)

                    Dim _objDashboard As PropertyInfo = _myControlType.GetProperty("objDashboard")
                    _objDashboard.SetValue(ReptControl, objDashboard, Nothing)

                    Dim _myBoolGroup As PropertyInfo = _myControlType.GetProperty("bitGroup")
                    _myBoolGroup.SetValue(ReptControl, IIf(GetQueryStringVal( "frm") = "Admin", True, False), Nothing)
                    Dim _myfunction As System.Reflection.MethodInfo = _myControlType.GetMethod("CreateReport")
                    _myfunction.Invoke(ReptControl, Nothing)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadDashBoard()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkMedium1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMedium1.Click
        Try
            If GetQueryStringVal( "frm") = "Admin" Then
                objDashboard.GroupId = ddlGroup.SelectedValue
                objDashboard.ColumnPos = 1
                objDashboard.RepSize = 2
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            Else
                objDashboard.UserCntID = Session("UserContactID")
                objDashboard.ColumnPos = 1
                objDashboard.RepSize = 2
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkMedium2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMedium2.Click
        Try
            If GetQueryStringVal( "frm") = "Admin" Then
                objDashboard.GroupId = ddlGroup.SelectedValue
                objDashboard.ColumnPos = 2
                objDashboard.RepSize = 2
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            Else
                objDashboard.UserCntID = Session("UserContactID")
                objDashboard.ColumnPos = 2
                objDashboard.RepSize = 2
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkMedium3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMedium3.Click
        Try
            If GetQueryStringVal( "frm") = "Admin" Then
                objDashboard.GroupId = ddlGroup.SelectedValue
                objDashboard.ColumnPos = 3
                objDashboard.RepSize = 2
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            Else
                objDashboard.UserCntID = Session("UserContactID")
                objDashboard.ColumnPos = 3
                objDashboard.RepSize = 2
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNarrow1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNarrow1.Click
        Try
            If GetQueryStringVal( "frm") = "Admin" Then
                objDashboard.GroupId = ddlGroup.SelectedValue
                objDashboard.ColumnPos = 1
                objDashboard.RepSize = 1
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            Else
                objDashboard.UserCntID = Session("UserContactID")
                objDashboard.ColumnPos = 1
                objDashboard.RepSize = 1
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNarrow2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNarrow2.Click
        Try
            If GetQueryStringVal( "frm") = "Admin" Then
                objDashboard.GroupId = ddlGroup.SelectedValue
                objDashboard.ColumnPos = 2
                objDashboard.RepSize = 1
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            Else
                objDashboard.UserCntID = Session("UserContactID")
                objDashboard.ColumnPos = 2
                objDashboard.RepSize = 1
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNarrow3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNarrow3.Click
        Try
            If GetQueryStringVal( "frm") = "Admin" Then
                objDashboard.GroupId = ddlGroup.SelectedValue
                objDashboard.ColumnPos = 3
                objDashboard.RepSize = 1
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            Else
                objDashboard.UserCntID = Session("UserContactID")
                objDashboard.ColumnPos = 3
                objDashboard.RepSize = 1
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkWide1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkWide1.Click
        Try
            If GetQueryStringVal( "frm") = "Admin" Then
                objDashboard.GroupId = ddlGroup.SelectedValue
                objDashboard.ColumnPos = 1
                objDashboard.RepSize = 3
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            Else
                objDashboard.UserCntID = Session("UserContactID")
                objDashboard.ColumnPos = 1
                objDashboard.RepSize = 3
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkWide2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkWide2.Click
        Try
            If GetQueryStringVal( "frm") = "Admin" Then
                objDashboard.GroupId = ddlGroup.SelectedValue
                objDashboard.ColumnPos = 2
                objDashboard.RepSize = 3
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            Else
                objDashboard.UserCntID = Session("UserContactID")
                objDashboard.ColumnPos = 2
                objDashboard.RepSize = 3
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkWide3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkWide3.Click
        Try
            If GetQueryStringVal( "frm") = "Admin" Then
                objDashboard.GroupId = ddlGroup.SelectedValue
                objDashboard.ColumnPos = 3
                objDashboard.RepSize = 3
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            Else
                objDashboard.UserCntID = Session("UserContactID")
                objDashboard.ColumnPos = 3
                objDashboard.RepSize = 3
                objDashboard.ManageDashboardSize()
                LoadDashBoard()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDashBoard()
        Try
            If GetQueryStringVal( "frm") = "Admin" Then
                objDashboard.DomainID = Session("DomainID")
                objDashboard.GroupId = ddlGroup.SelectedValue
                Dim ds As DataSet
                ds = objDashboard.GetDashBoard
                BuildDashboard(ds)
                hplAddComponent1.NavigateUrl = "../DashBoard/frmAddDashBoardRPT.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Column=1&frm=Admin&GroupID=" & ddlGroup.SelectedValue
                hplAddComponent2.NavigateUrl = "../DashBoard/frmAddDashBoardRPT.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Column=2&frm=Admin&GroupID=" & ddlGroup.SelectedValue
                hplAddComponent3.NavigateUrl = "../DashBoard/frmAddDashBoardRPT.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Column=3&frm=Admin&GroupID=" & ddlGroup.SelectedValue
            Else
                objDashboard.DomainID = Session("DomainID")
                objDashboard.UserCntID = Session("UserContactID")
                objDashboard.GroupId = Session("UserGroupID")
                Dim ds As DataSet
                ds = objDashboard.GetDashBoard
                BuildDashboard(ds)
                hplAddComponent1.NavigateUrl = "../DashBoard/frmAddDashBoardRPT.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Column=1"
                hplAddComponent2.NavigateUrl = "../DashBoard/frmAddDashBoardRPT.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Column=2"
                hplAddComponent3.NavigateUrl = "../DashBoard/frmAddDashBoardRPT.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Column=3"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            txtEdit.Text = 1
            td1.Visible = True
            td2.Visible = True
            td3.Visible = True
            td4.Visible = True
            td5.Visible = True
            td6.Visible = True
            LoadDashBoard()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDone.Click
        Try
            txtEdit.Text = ""
            td1.Visible = False
            td2.Visible = False
            td3.Visible = False
            td4.Visible = False
            td5.Visible = False
            td6.Visible = False
            LoadDashBoard()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    <WebMethod()>
    Public Shared Function UpdateOrder(ByVal Column1Values As String, ByVal Column2Values As String, ByVal Column3Values As String) As String
        Try
            Dim dt As New DataTable
            dt.Columns.Add("numDashBoardReptID")
            dt.Columns.Add("tintColumn")
            dt.Columns.Add("tintRow")

            Dim strCol1() As String = Column1Values.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)
            Dim strCol2() As String = Column2Values.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)
            Dim strCol3() As String = Column3Values.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)
            Dim dr As DataRow

            For i As Integer = 0 To strCol1.Length - 1
                If CCommon.ToLong(strCol1(i)) > 0 Then
                    dr = dt.NewRow()
                    dr("numDashBoardReptID") = strCol1(i)
                    dr("tintColumn") = "1"
                    dr("tintRow") = i + 1
                    dt.Rows.Add(dr)
                End If
            Next

            For i As Integer = 0 To strCol2.Length - 1
                If CCommon.ToLong(strCol2(i)) > 0 Then
                    dr = dt.NewRow()
                    dr("numDashBoardReptID") = strCol2(i)
                    dr("tintColumn") = "2"
                    dr("tintRow") = i + 1
                    dt.Rows.Add(dr)
                End If
            Next
            For i As Integer = 0 To strCol3.Length - 1
                If CCommon.ToLong(strCol3(i)) > 0 Then
                    dr = dt.NewRow()
                    dr("numDashBoardReptID") = strCol3(i)
                    dr("tintColumn") = "3"
                    dr("tintRow") = i + 1
                    dt.Rows.Add(dr)
                End If
            Next

            Dim ds As New DataSet
            ds.Tables.Add(dt)
            Dim objDashboard As New DashBoard
            objDashboard.strItems = ds.GetXml
            objDashboard.MoveDashboard = "A"
            Return objDashboard.ArrangeDashboard()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class