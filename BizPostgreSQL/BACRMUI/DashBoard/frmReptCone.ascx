<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmReptCone.ascx.vb" Inherits=".frmReptCone" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebListbar.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebListbar" TagPrefix="iglbar" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebChart.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.UltraChart.Resources.Appearance" TagPrefix="igchartprop" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebChart.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.UltraChart.Data" TagPrefix="igchartdata" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebChart.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebChart" TagPrefix="igchart" %>
    <script type="text/javascript" language="javascript" >
    function Redirect(a)
    {
        document.location.href('../reports/frmCustomReport.aspx?ReportId='+a+'&frm=Dashboard')
    }
    </script>
    <asp:Panel ID="pnlChart" runat="server" >
    
    
<igmisc:WebPanel EnableAppStyling="true" StyleSetName="Default" StyleSetPath="~/styles" ID="WebPanel2" runat="server" ToolTip="Click arrow to the right to expand and close this description." >
     <PanelStyle CssClass="descriptionPanelContent">
     </PanelStyle>
     <Header TextAlignment="Left">
        <ExpandedAppearance >
            <Styles CssClass="descriptionPanelHeader">
            </Styles>
       
        </ExpandedAppearance>
      </Header>
      
      <Template>
      
      <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
              
                <td id="tdEdit" runat="server"   align="right" visible="false"  >
                      <asp:HyperLink ID="hplMoveLeft" runat="server" CssClass="hyperlink" ><img border="0" src="../images/move_left.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveDown" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_down.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveRight" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_right.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveUp" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_up.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplEdit" cssclass="hyperlink" runat="server" >Edit</asp:HyperLink>
                      <asp:HyperLink ID="hplDelete" cssclass="hyperlink" runat="server" >Delete</asp:HyperLink>
                </td>
            </tr>
        
            <tr>
                <td>
                    <igchart:UltraChart id="UltraChart1" Width="100%" runat="server" BackColor="#E9EDF4" BorderColor="#868686" BorderWidth="1px" EmptyChartText="" Version="7.3" BackgroundImageFileName="" >
                    <Axis>
                        <Y Extent="45" LineColor="135, 161, 210" LineThickness="1" TickmarkInterval="40" TickmarkStyle="Smart" Visible="True">
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <MajorGridLines AlphaLevel="255" Color="135, 161, 210" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <Labels Font="Arial, 8.25pt" HorizontalAlign="Far"
                                ItemFormatString="&lt;DATA_VALUE:0.##&gt;" Orientation="Horizontal" VerticalAlign="Center">
                                <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Far" Orientation="VerticalLeftFacing"
                                    VerticalAlign="Center" FormatString="">
                                    <Layout Behavior="Auto">
                                    </Layout>
                                </SeriesLabels>
                            </Labels>
                        </Y>
                        <Y2 LineThickness="1" TickmarkInterval="40" TickmarkStyle="Smart" Visible="False">
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                Visible="True" />
                            <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString="&lt;DATA_VALUE:00.##&gt;"
                                Orientation="Horizontal" VerticalAlign="Center" Visible="False">
                                <Layout Behavior="Auto">
                                </Layout>
                                <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" FormatString="" HorizontalAlign="Near"
                                    Orientation="VerticalLeftFacing" VerticalAlign="Center">
                                    <Layout Behavior="Auto">
                                    </Layout>
                                </SeriesLabels>
                            </Labels>
                        </Y2>
                        <Z LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                Visible="True" />
                            <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString="&lt;ITEM_LABEL&gt;"
                                Orientation="Horizontal" VerticalAlign="Center" Visible="False">
                                <Layout Behavior="Auto">
                                </Layout>
                                <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" Orientation="Horizontal"
                                    VerticalAlign="Center">
                                    <Layout Behavior="Auto">
                                    </Layout>
                                </SeriesLabels>
                            </Labels>
                        </Z>
                        <X Extent="95" LineColor="135, 161, 210" LineThickness="1" TickmarkInterval="1" TickmarkStyle="Smart"
                            Visible="True">
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <Labels Font="Arial, 8.25pt" HorizontalAlign="Near" Orientation="VerticalLeftFacing" VerticalAlign="Center" ItemFormatString="&lt;ITEM_LABEL&gt;">
                                <SeriesLabels Font="Arial, 8.25pt" HorizontalAlign="Center"
                                    Orientation="Horizontal" VerticalAlign="Center" Visible="False">
                                </SeriesLabels>
                            </Labels>
                            <Margin>
                                <Far Value="2.6415094339622645" />
                            </Margin>
                        </X>
                        <X2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                Visible="True" />
                            <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Far" ItemFormatString="&lt;ITEM_LABEL&gt;"
                                Orientation="VerticalLeftFacing" VerticalAlign="Center" Visible="False">
                                <Layout Behavior="Auto">
                                </Layout>
                                <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Center" Orientation="Horizontal"
                                    VerticalAlign="Center">
                                    <Layout Behavior="Auto">
                                    </Layout>
                                </SeriesLabels>
                            </Labels>
                        </X2>
                        <PE Fill="Cornsilk" ElementType="None" />
                        <Z2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                Visible="True" />
                            <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                Orientation="Horizontal" VerticalAlign="Center" Visible="False">
                                <Layout Behavior="Auto">
                                </Layout>
                                <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" Orientation="Horizontal"
                                    VerticalAlign="Center">
                                    <Layout Behavior="Auto">
                                    </Layout>
                                </SeriesLabels>
                            </Labels>
                        </Z2>
                    </Axis>
                        <Border Color="134, 134, 134" CornerRadius="10" />
                        <Effects>
                            <Effects>
                                <igchartprop:gradienteffect style="backwarddiagonal"></igchartprop:gradienteffect>
                                <igchartprop:strokeeffect strokeopacity="255" strokewidth="0"></igchartprop:strokeeffect>
                            </Effects>
                        </Effects>
                        <ColorModel AlphaLevel="150" ColorBegin="79, 129, 189" ModelStyle="CustomLinear">
                            <Skin ApplyRowWise="False">
                                <PEs>
                                    <igchartprop:paintelement elementtype="Gradient" fill="46, 120, 208" fillgradientstyle="Vertical"
                                        fillstopcolor="23, 65, 115" stroke="29, 82, 145" StrokeWidth="0"></igchartprop:paintelement>
                                </PEs>
                            </Skin>
                        </ColorModel>
                        <Legend BackgroundColor="Transparent" BorderThickness="0" Font="Microsoft Sans Serif, 9.75pt, style=Bold" Location="Left"></Legend>
                        <Tooltips Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" />
                        <Data ZeroAligned="True">
                        </Data>
                  
                        <TitleLeft Font="Arial, 8.25pt"  Text="Record Count" Extent="33" Visible="True" Flip="True" HorizontalAlign="Center">
                            <Margins Bottom="0" Left="0" Right="0" Top="0" />
                        </TitleLeft>
				</igchart:UltraChart>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFooter" runat="server" CssClass="text_bold" ></asp:Label>
                </td>
            </tr>
        </table>

      </Template>
</igmisc:WebPanel>
</asp:Panel>
