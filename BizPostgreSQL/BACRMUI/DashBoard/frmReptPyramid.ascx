<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmReptPyramid.ascx.vb"
    Inherits=".frmReptPyramid" %>
<script type="text/javascript" language="javascript">
    function Redirect(a) {
        document.location.href = '../reports/frmCustomReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ReportId=' + a + '&frm=Dashboard';
    }
</script>
<div class="portlet" runat="server" id="dvTable" clientidmode="Static">
    <div class="portlet-header">
        <asp:Label Text="" ID="lblHeader" runat="server" />
    </div>
    <div class="portlet-content">
        <asp:Panel ID="pnlChart" runat="server">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td id="tdEdit" runat="server" align="right" visible="false">
                        <%--<asp:HyperLink ID="hplMoveLeft" runat="server" CssClass="hyperlink" ><img border="0" src="../images/move_left.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveDown" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_down.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveRight" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_right.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveUp" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_up.gif" /></asp:HyperLink>--%>
                        <asp:HyperLink ID="hplEdit" CssClass="hyperlink" runat="server">Edit</asp:HyperLink>
                        <asp:HyperLink ID="hplDelete" CssClass="hyperlink" runat="server">Delete</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td>
                        <igchart:UltraChart id="UltraChart1" runat="server" ChartType="PyramidChart" Width="400px"
                            Height="300px" Version="7.3" BackgroundImageFileName="" Border-Color="Black" Border-Thickness="0"
                            EmptyChartText="Data Not Available. Please call UltraChart.Data.DataBind() after setting valid Data.DataSource">
                            <pyramidchart>
                        <Labels>
                            <LabelStyle Font="Microsoft Sans Serif, 7.8pt" />
                        </Labels>
                    </pyramidchart>
                            <data>
						<EmptyStyle>
							<PointPE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PointPE>
							<LineStyle DrawStyle="Dash"></LineStyle>
							<PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
						</EmptyStyle>
					</data>
                            <colormodel modelstyle="CustomLinear" alphalevel="150">
						<Skin>
							<PEs>
								<igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="108, 162, 36" FillStopColor="148, 244, 17" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="7, 108, 176" FillStopColor="53, 200, 255" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="230, 190, 2" FillStopColor="255, 255, 81" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="215, 0, 5" FillStopColor="254, 117, 16" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="252, 122, 10" FillStopColor="255, 108, 66" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
							</PEs>
						</Skin>
					</colormodel>
                            <effects>
						<Effects>
							<igchartprop:GradientEffect Style="ForwardDiagonal"></igchartprop:GradientEffect>
						</Effects>
					</effects>
                            <axis>
						<X2 Visible="False" TickmarkInterval="10" LineThickness="1" TickmarkStyle="Smart">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
							</StripLines>
							<Labels ItemFormatString="" VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="Gray" Orientation="VerticalLeftFacing">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="Gray" Orientation="VerticalLeftFacing" FormatString="">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
						</X2>
						<Z Visible="True" LineThickness="1" TickmarkStyle="Smart" TickmarkInterval="0">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
							</StripLines>
							<Labels ItemFormatString="" VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="DimGray" Orientation="Horizontal">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="DimGray" Orientation="Horizontal">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
						</Z>
						<Z2 Visible="True" LineThickness="1" TickmarkStyle="Smart" TickmarkInterval="0">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
							</StripLines>
							<Labels ItemFormatString="" VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="Gray" Orientation="Horizontal">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="Gray" Orientation="Horizontal">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
						</Z2>
						<X LineEndCapStyle="Flat" Visible="True" TickmarkInterval="10" LineThickness="1" Extent="40" TickmarkStyle="Smart">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
							</StripLines>
							<Labels ItemFormatString="&lt;ITEM_LABEL&gt;" VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="DimGray" Orientation="VerticalLeftFacing">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="DimGray" Orientation="VerticalLeftFacing" FormatString="">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
						</X>
						<Y LineEndCapStyle="Flat" Visible="True" TickmarkInterval="10" LineThickness="1" Extent="30" TickmarkStyle="Smart">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
							</StripLines>
							<Labels ItemFormatString="&lt;DATA_VALUE:00.##&gt;" VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="DimGray" Orientation="Horizontal">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="DimGray" Orientation="Horizontal" FormatString="">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
						</Y>
						<Y2 Visible="False" TickmarkInterval="10" LineThickness="1" TickmarkStyle="Smart">
							<StripLines>
								<PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
							</StripLines>
							<Labels ItemFormatString="" VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="Gray" Orientation="Horizontal">
								<SeriesLabels VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="Gray" Orientation="Horizontal" FormatString="">
									<Layout Behavior="Auto"></Layout>
								</SeriesLabels>
								<Layout Behavior="Auto"></Layout>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
						</Y2>
                        <PE ElementType="None" Fill="Cornsilk" />
					</axis>
                            <titlebottom visible="False">
					</titlebottom>
                            <tooltips font-bold="False" font-italic="False" font-overline="False" font-strikeout="False"
                                font-underline="False" />
                        </igchart:UltraChart>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</div>
