<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmRept3DPie.ascx.vb"
    Inherits=".frmRept3DPie" %>
<script type="text/javascript" language="javascript">
    function Redirect(a) {
        document.location.href = '../reports/frmCustomReport.aspx?ReportId=' + a + '&frm=Dashboard';
    }
</script>
<div class="portlet" runat="server" id="dvTable" clientidmode="Static">
    <div class="portlet-header">
        <asp:Label Text="" ID="lblHeader" runat="server" />
    </div>
    <div class="portlet-content">
        <asp:Panel ID="pnlChart" runat="server">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td id="tdEdit" runat="server" align="right" visible="false">
                        <%--<asp:HyperLink ID="hplMoveLeft" runat="server" CssClass="hyperlink" ><img border="0" src="../images/move_left.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveDown" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_down.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveRight" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_right.gif" /></asp:HyperLink>
                      <asp:HyperLink ID="hplMoveUp" runat="server" CssClass="hyperlink" ><img  border="0" src="../images/move_up.gif" /></asp:HyperLink>--%>
                        <asp:HyperLink ID="hplEdit" CssClass="hyperlink" runat="server">Edit</asp:HyperLink>
                        <asp:HyperLink ID="hplDelete" CssClass="hyperlink" runat="server">Delete</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td>
                        <igchart:UltraChart id="UltraChart1" runat="server" ChartType="PieChart3D" Transform3D-Perspective="50"
                            Transform3D-ZRotation="0" Transform3D-Scale="80" Transform3D-XRotation="41" Transform3D-YRotation="-18">
                            <border cornerradius="0" drawstyle="Solid" raised="False" color="Black" thickness="1"></border>
                            <piechart3d concentricspacing="0.25" otherscategorypercent="3" breakalternatingslices="False"
                                breakdistancepercentage="10" breakothersslice="False" columnindex="-1" concentric="False"
                                piethickness="20" breakallslices="False" startangle="0" otherscategorytext="Others"
                                radiusfactor="90" showconcentriclegend="True">
						<Labels Font="Verdana, 7pt" Visible="True" FormatString="&lt;PERCENT_VALUE:#0.00&gt;%" LeaderLineThickness="1" FillColor="Transparent" LeaderLinesVisible="True" BorderDrawStyle="Solid" Format="Custom" BorderThickness="0" LeaderEndStyle="ArrowAnchor" FontColor="Black" BorderColor="Black" LeaderDrawStyle="Dot"></Labels>
					</piechart3d>
                            <titleright font="Microsoft Sans Serif, 7.8pt" visible="False" text="" fontsizebestfit="False"
                                orientation="VerticalRightFacing" wraptext="False" extent="26" fontcolor="Black"
                                horizontalalign="Near" verticalalign="Center" location="Right">
						<Margins Bottom="5" Left="5" Top="5" Right="5"></Margins>
					</titleright>
                            <data datamember="" swaprowsandcolumns="False" useminmax="False" userowlabelscolumn="False"
                                minvalue="-1.7976931348623157E+308" rowlabelscolumn="-1" zeroaligned="False"
                                maxvalue="1.7976931348623157E+308">
						<EmptyStyle Text="Empty" EnableLineStyle="False" ShowInLegend="False" EnablePE="False" EnablePoint="False">
							<PointPE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></PointPE>
							<PointStyle CharacterFont="Microsoft Sans Serif, 7.8pt"></PointStyle>
							<LineStyle MidPointAnchors="False" EndStyle="NoAnchor" DrawStyle="Dash" StartStyle="NoAnchor"></LineStyle>
							<PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></PE>
						</EmptyStyle>
					</data>
                            <titleleft font="Microsoft Sans Serif, 7.8pt" visible="False" text="" fontsizebestfit="False"
                                orientation="VerticalLeftFacing" wraptext="False" extent="26" fontcolor="Black"
                                horizontalalign="Near" verticalalign="Center" location="Left">
						<Margins Bottom="5" Left="5" Top="5" Right="5"></Margins>
					</titleleft>
                            <colormodel colorbegin="DarkGoldenrod" colorend="Navy" alphalevel="255" modelstyle="CustomSkin"
                                grayscale="False" scaling="None">
						<Skin ApplyRowWise="True">
							<PEs>
								<igchartprop:PaintElement FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="108, 162, 36" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="7, 108, 176" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="215, 0, 5" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="243, 204, 3" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></igchartprop:PaintElement>
								<igchartprop:PaintElement FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="252, 122, 10" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></igchartprop:PaintElement>
							</PEs>
						</Skin>
					</colormodel>
                            <legend font="Microsoft Sans Serif, 7pt" visible="True" alphalevel="150" borderthickness="1"
                                borderstyle="Solid" spanpercentage="35" bordercolor="Navy" fontcolor="Black"
                                backgroundcolor="FloralWhite" dataassociation="DefaultData" location="Top" formatstring="&lt;ITEM_LABEL&gt;">
                                <margins bottom="5" left="5" top="5" right="5"></margins>
                            </legend>
                            <axis backcolor="Cornsilk">
						<Y LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="False" RangeMin="0" LineColor="Black" RangeType="Automatic" TickmarkInterval="0" LineThickness="2" Extent="80" LogBase="10" RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10" NumericAxisType="Linear">
							<StripLines Interval="2" Visible="False">
								<PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
							<Labels ItemFormatString="&lt;DATA_VALUE:00.##&gt;" VerticalAlign="Center" WrapText="False" FontSizeBestFit="False" SeriesFormatString="" ClipText="True" Font="Microsoft Sans Serif, 7.8pt" Flip="False" ItemFormat="DataValue" FontColor="Black" Orientation="Horizontal" Visible="True" OrientationAngle="0" HorizontalAlign="Far">
								<SeriesLabels Font="Microsoft Sans Serif, 7.8pt" Visible="True" HorizontalAlign="Far" FontSizeBestFit="False" ClipText="True" FormatString="" Orientation="Horizontal" WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0"></SeriesLabels>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
							<TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
							<Margin>
								<Far MarginType="Percentage" Value="0"></Far>
								<Near MarginType="Percentage" Value="0"></Near>
							</Margin>
						</Y>
						<Y2 LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="False" RangeMin="0" LineColor="Black" RangeType="Automatic" TickmarkInterval="0" LineThickness="2" Extent="80" LogBase="10" RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10" NumericAxisType="Linear">
							<StripLines Interval="2" Visible="False">
								<PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
							<Labels ItemFormatString="&lt;DATA_VALUE:00.##&gt;" VerticalAlign="Center" WrapText="False" FontSizeBestFit="False" SeriesFormatString="" ClipText="True" Font="Microsoft Sans Serif, 7.8pt" Flip="False" ItemFormat="DataValue" FontColor="Black" Orientation="Horizontal" Visible="True" OrientationAngle="0" HorizontalAlign="Near">
								<SeriesLabels Font="Microsoft Sans Serif, 7.8pt" Visible="True" HorizontalAlign="Near" FontSizeBestFit="False" ClipText="True" FormatString="" Orientation="Horizontal" WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0"></SeriesLabels>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
							<TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
							<Margin>
								<Far MarginType="Percentage" Value="0"></Far>
								<Near MarginType="Percentage" Value="0"></Near>
							</Margin>
						</Y2>
						<X2 LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="False" RangeMin="0" LineColor="Black" RangeType="Automatic" TickmarkInterval="0" LineThickness="2" Extent="80" LogBase="10" RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10" NumericAxisType="Linear">
							<StripLines Interval="2" Visible="False">
								<PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
							<Labels ItemFormatString="&lt;ITEM_LABEL&gt;" VerticalAlign="Center" WrapText="False" FontSizeBestFit="False" SeriesFormatString="" ClipText="True" Font="Microsoft Sans Serif, 7.8pt" Flip="False" ItemFormat="ItemLabel" FontColor="Black" Orientation="Horizontal" Visible="True" OrientationAngle="0" HorizontalAlign="Far">
								<SeriesLabels Font="Microsoft Sans Serif, 7.8pt" Visible="True" HorizontalAlign="Far" FontSizeBestFit="False" ClipText="True" FormatString="" Orientation="Horizontal" WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0"></SeriesLabels>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
							<TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
							<Margin>
								<Far MarginType="Percentage" Value="0"></Far>
								<Near MarginType="Percentage" Value="0"></Near>
							</Margin>
						</X2>
						<Z2 LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="False" RangeMin="0" LineColor="Black" RangeType="Automatic" TickmarkInterval="0" LineThickness="2" Extent="80" LogBase="10" RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10" NumericAxisType="Linear">
							<StripLines Interval="2" Visible="False">
								<PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
							<Labels ItemFormatString="" VerticalAlign="Center" WrapText="False" FontSizeBestFit="False" SeriesFormatString="&lt;SERIES_LABEL&gt;" ClipText="True" Font="Microsoft Sans Serif, 7.8pt" Flip="False" ItemFormat="None" FontColor="Black" Orientation="Horizontal" Visible="True" OrientationAngle="0" HorizontalAlign="Near">
								<SeriesLabels Font="Microsoft Sans Serif, 7.8pt" Visible="True" HorizontalAlign="Near" FontSizeBestFit="False" ClipText="True" FormatString="&lt;SERIES_LABEL&gt;" Orientation="Horizontal" WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0"></SeriesLabels>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
							<TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
							<Margin>
								<Far MarginType="Percentage" Value="0"></Far>
								<Near MarginType="Percentage" Value="0"></Near>
							</Margin>
						</Z2>
						<Z LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="False" RangeMin="0" LineColor="Black" RangeType="Automatic" TickmarkInterval="0" LineThickness="2" Extent="80" LogBase="10" RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10" NumericAxisType="Linear">
							<StripLines Interval="2" Visible="False">
								<PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
							<Labels ItemFormatString="" VerticalAlign="Center" WrapText="False" FontSizeBestFit="False" SeriesFormatString="&lt;SERIES_LABEL&gt;" ClipText="True" Font="Microsoft Sans Serif, 7.8pt" Flip="False" ItemFormat="None" FontColor="Black" Orientation="Horizontal" Visible="True" OrientationAngle="0" HorizontalAlign="Far">
								<SeriesLabels Font="Microsoft Sans Serif, 7.8pt" Visible="True" HorizontalAlign="Far" FontSizeBestFit="False" ClipText="True" FormatString="&lt;SERIES_LABEL&gt;" Orientation="Horizontal" WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0"></SeriesLabels>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
							<TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
							<Margin>
								<Far MarginType="Percentage" Value="0"></Far>
								<Near MarginType="Percentage" Value="0"></Near>
							</Margin>
						</Z>
						<X LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="False" RangeMin="0" LineColor="Black" RangeType="Automatic" TickmarkInterval="0" LineThickness="2" Extent="80" LogBase="10" RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10" NumericAxisType="Linear">
							<StripLines Interval="2" Visible="False">
								<PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill" Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit" FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1" ImageWrapMode="Tile" TextureApplication="Normal"></PE>
							</StripLines>
							<ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
							<Labels ItemFormatString="&lt;ITEM_LABEL&gt;" VerticalAlign="Center" WrapText="False" FontSizeBestFit="False" SeriesFormatString="" ClipText="True" Font="Microsoft Sans Serif, 7.8pt" Flip="False" ItemFormat="ItemLabel" FontColor="Black" Orientation="Horizontal" Visible="True" OrientationAngle="0" HorizontalAlign="Near">
								<SeriesLabels Font="Microsoft Sans Serif, 7.8pt" Visible="True" HorizontalAlign="Near" FontSizeBestFit="False" ClipText="True" FormatString="" Orientation="Horizontal" WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0"></SeriesLabels>
							</Labels>
							<MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
							<MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
							<TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
							<Margin>
								<Far MarginType="Percentage" Value="0"></Far>
								<Near MarginType="Percentage" Value="0"></Near>
							</Margin>
						</X>
					</axis>
                            <titlebottom font="Microsoft Sans Serif, 7.8pt" visible="False" text="" fontsizebestfit="False"
                                orientation="Horizontal" wraptext="False" extent="26" fontcolor="Black" horizontalalign="Far"
                                verticalalign="Center" location="Bottom">
						<Margins Bottom="5" Left="5" Top="5" Right="5"></Margins>
					</titlebottom>
                            <titletop font="Microsoft Sans Serif, 7.8pt" visible="False" text="" fontsizebestfit="False"
                                orientation="Horizontal" wraptext="False" extent="33" fontcolor="Black" horizontalalign="Near"
                                verticalalign="Center" location="Top">
						<Margins Bottom="5" Left="5" Top="5" Right="5"></Margins>
					</titletop>
                            <tooltips borderthickness="1" overflow="None" formatstring="&lt;DATA_VALUE:00.##&gt;"
                                enablefadingeffect="False" format="DataValue" fontcolor="Black" bordercolor="Black"
                                display="MouseMove" backcolor="AntiqueWhite" padding="0"></tooltips>
                        </igchart:UltraChart>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</div>
