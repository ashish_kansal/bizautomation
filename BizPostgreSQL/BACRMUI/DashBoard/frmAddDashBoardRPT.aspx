<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddDashBoardRPT.aspx.vb"
    Inherits=".frmAddDashBoardRPT" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>New Dashboard Component</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save & Close" />
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    New Dashboard Component
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td colspan="3">
                <asp:Table ID="tbl" runat="server" GridLines="None" Width="100%" CellSpacing="0"
                    CellPadding="0" CssClass="aspTable">
                    <asp:TableRow>
                        <asp:TableCell>
                            <br />
                            <table width="500">
                                <tr>
                                    <td class="normal1" align="right">
                                        Custom Report
                                    </td>
                                    <td>
                                        <asp:DropDownList AutoPostBack="true" ID="ddlCustReport" runat="server" CssClass="signup">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Component Type
                                    </td>
                                    <td class="normal1">
                                        <asp:RadioButton ID="radTable" GroupName="rad" runat="server" Text="Table" />
                                        <asp:RadioButton ID="radChart" GroupName="rad" runat="server" Text="Chart" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Chart Type
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlChartType" CssClass="signup" runat="server">
                                            <asp:ListItem Value="1">Column Chart</asp:ListItem>
                                            <asp:ListItem Value="2">Bar Chart</asp:ListItem>
                                            <asp:ListItem Value="3">Pyramid Chart</asp:ListItem>
                                            <asp:ListItem Value="4">3D Pyramid Chart</asp:ListItem>
                                            <asp:ListItem Value="5">Funnel Chart</asp:ListItem>
                                            <asp:ListItem Value="6">3D Funnel Chart</asp:ListItem>
                                            <asp:ListItem Value="7">3D Cone Chart</asp:ListItem>
                                            <asp:ListItem Value="12">Pie Chart</asp:ListItem>
                                            <asp:ListItem Value="13">3D Pie Chart</asp:ListItem>
                                            <asp:ListItem Value="14">Doughnut Chart</asp:ListItem>
                                            <asp:ListItem Value="15">3D Doughnut Chart</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Header
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtHeader" Width="300" runat="server" CssClass="signup"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Footer
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFooter" runat="server" CssClass="signup" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
</asp:Content>
