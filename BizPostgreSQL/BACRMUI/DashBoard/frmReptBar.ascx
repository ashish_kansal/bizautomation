<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmReptBar.ascx.vb"
    Inherits=".frmReptBar" %>
<script type="text/javascript" language="javascript">
    function Redirect(a) {
        document.location.href = '../reports/frmCustomReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ReportId=' + a + '&frm=Dashboard';
    }
    function OpenCommission(a, b) {
        window.open('../dashboard/frmCommissionforPayrollExpense.aspx?ComRuleID=' + a + '&ItemCode=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=780,height=700,scrollbars=yes,resizable=yes');
        return false;
    }
</script>
<div class="portlet" runat="server" id="dvTable" clientidmode="Static">
    <div class="portlet-header">
        <asp:Label Text="" ID="lblHeader" runat="server" />
    </div>
    <div class="portlet-content">
        <asp:Panel ID="pnlChart" runat="server">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td id="tdEdit" runat="server" align="right" visible="false">
                        <%--<asp:HyperLink ID="hplMoveLeft" runat="server" CssClass="hyperlink"><img border="0" src="../images/move_left.gif" /></asp:HyperLink>
                        <asp:HyperLink ID="hplMoveDown" runat="server" CssClass="hyperlink"><img  border="0" src="../images/move_down.gif" /></asp:HyperLink>
                        <asp:HyperLink ID="hplMoveRight" runat="server" CssClass="hyperlink"><img  border="0" src="../images/move_right.gif" /></asp:HyperLink>
                        <asp:HyperLink ID="hplMoveUp" runat="server" CssClass="hyperlink"><img  border="0" src="../images/move_up.gif" /></asp:HyperLink>--%>
                        <asp:HyperLink ID="hplEdit" CssClass="hyperlink" runat="server">Edit</asp:HyperLink>
                        <asp:HyperLink ID="hplDelete" CssClass="hyperlink" runat="server">Delete</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td>
                        <igchart:UltraChart ID="UltraChart1" runat="server" BackColor="#E9EDF4" Border-Color="#868686"
                            Border-Thickness="1" EmptyChartText="" Version="7.3" BackgroundImageFileName=""
                            ChartType="BarChart">
                            <Axis>
                                <Y Extent="40" LineColor="135, 161, 210" LineThickness="1" TickmarkInterval="1" TickmarkStyle="Smart"
                                    Visible="True">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="135, 161, 210" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <Labels Font="Arial, 8.25pt" HorizontalAlign="Far" ItemFormatString="&lt;ITEM_LABEL&gt;"
                                        Orientation="Horizontal" VerticalAlign="Center">
                                        <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Center" Orientation="Horizontal"
                                            VerticalAlign="Near" FormatString="">
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </SeriesLabels>
                                    </Labels>
                                </Y>
                                <Y2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                        Visible="True" />
                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString="&lt;ITEM_LABEL&gt;"
                                        Orientation="Horizontal" VerticalAlign="Center" Visible="False">
                                        <Layout Behavior="Auto">
                                        </Layout>
                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Center" Orientation="VerticalLeftFacing"
                                            VerticalAlign="Center">
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </SeriesLabels>
                                    </Labels>
                                </Y2>
                                <Z LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                        Visible="True" />
                                    <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString="&lt;ITEM_LABEL&gt;"
                                        Orientation="Horizontal" VerticalAlign="Center" Visible="False">
                                        <Layout Behavior="Auto">
                                        </Layout>
                                        <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" Orientation="Horizontal"
                                            VerticalAlign="Center">
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </SeriesLabels>
                                    </Labels>
                                </Z>
                                <X Extent="33" LineColor="135, 161, 210" LineThickness="1" TickmarkInterval="40"
                                    TickmarkStyle="Smart" Visible="True">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <Labels Font="Arial, 8.25pt" HorizontalAlign="Far" Orientation="Horizontal" VerticalAlign="Center"
                                        ItemFormatString="&lt;DATA_VALUE:0.##&gt;">
                                        <SeriesLabels Font="Arial, 8.25pt" HorizontalAlign="Far" Orientation="VerticalLeftFacing"
                                            VerticalAlign="Center" Visible="False" FormatString="">
                                        </SeriesLabels>
                                    </Labels>
                                    <Margin>
                                        <Far Value="2.6415094339622645" />
                                    </Margin>
                                </X>
                                <X2 LineThickness="1" TickmarkInterval="40" TickmarkStyle="Smart" Visible="False">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                        Visible="True" />
                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Far" ItemFormatString="&lt;DATA_VALUE:00.##&gt;"
                                        Orientation="VerticalLeftFacing" VerticalAlign="Center" Visible="False">
                                        <Layout Behavior="Auto">
                                        </Layout>
                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Far" Orientation="VerticalLeftFacing"
                                            VerticalAlign="Center" FormatString="">
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </SeriesLabels>
                                    </Labels>
                                </X2>
                                <PE Fill="Cornsilk" ElementType="None" />
                                <Z2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                        Visible="True" />
                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                        Orientation="Horizontal" VerticalAlign="Center" Visible="False">
                                        <Layout Behavior="Auto">
                                        </Layout>
                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" Orientation="VerticalLeftFacing"
                                            VerticalAlign="Center">
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </SeriesLabels>
                                    </Labels>
                                </Z2>
                            </Axis>
                            <%--<BarChart>
                                <ChartText>
                                    <igchartprop:ChartTextAppearance ItemFormatString="&lt;ITEM_LABEL&gt;" Visible="true"
                                        HorizontalAlign="Near" VerticalAlign="Center" Column="-2" Row="-2" />
                                </ChartText>
                            </BarChart>--%>
                            <Border Color="134, 134, 134" CornerRadius="10" />
                            <Effects>
                                <Effects>
                                    <igchartprop:gradienteffect style="backwarddiagonal">
                                    </igchartprop:gradienteffect>
                                    <igchartprop:strokeeffect strokeopacity="255" strokewidth="0">
                                    </igchartprop:strokeeffect>
                                </Effects>
                            </Effects>
                            <ColorModel AlphaLevel="150" ColorBegin="79, 129, 189" ModelStyle="CustomLinear">
                                <Skin ApplyRowWise="False">
                                    <PEs>
                                        <igchartprop:PaintElement ElementType="Gradient" Fill="46, 120, 208" FillGradientStyle="Vertical"
                                            FillStopColor="23, 65, 115" Stroke="29, 82, 145" StrokeWidth="0"></igchartprop:PaintElement>
                                    </PEs>
                                </Skin>
                            </ColorModel>
                            <Legend BackgroundColor="Transparent" BorderThickness="0" Font="Microsoft Sans Serif, 9.75pt, style=Bold"
                                Location="Left"></Legend>
                            <Tooltips Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" />
                            <Data ZeroAligned="True">
                            </Data>
                            <TitleLeft Font="Arial, 8.25pt" Extent="33" Visible="True" Flip="True" HorizontalAlign="Center">
                                <Margins Bottom="0" Left="0" Right="0" Top="0" />
                            </TitleLeft>
                        </igchart:UltraChart>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
                    </td>
                </tr>
            </table>
       </asp:Panel>
    </div>
</div>
