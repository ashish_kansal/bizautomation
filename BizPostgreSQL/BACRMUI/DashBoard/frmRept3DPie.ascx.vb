Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Reports
Partial Public Class frmRept3DPie
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private _DashReportID As Long
    Private _EditRept As Boolean
    Private _Width As Integer
    Private _objCustomReport As Object
    Private _objDashboard As Object


    Public Property objDashboard() As Object
        Get
            Try
                Return _objDashboard
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Object)
            Try
                _objDashboard = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Public Property objCustomReport() As Object
        Get
            Try
                Return _objCustomReport
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Object)
            Try
                _objCustomReport = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Public Property Width() As Integer
        Get
            Try
                Return _Width
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Integer)
            Try
                _Width = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Public Property EditRept() As Boolean
        Get
            Try
                Return _EditRept
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Boolean)
            Try
                _EditRept = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Public Property DashReportID() As Long
        Get
            Try
                Return _DashReportID
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Long)
            Try
                _DashReportID = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Private _bitGroup As Boolean

    Public Property bitGroup() As Boolean
        Get
            Try
                Return _bitGroup
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Boolean)
            Try
                _bitGroup = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Public Function CreateReport() As Boolean
        Try
            If _EditRept = True Then tdEdit.Visible = True
            UltraChart1.Width = _Width
            dvTable.ID = _DashReportID
            _objDashboard.DashboardReptId = _DashReportID
            Dim dtTable As DataTable
            dtTable = _objDashboard.GetReptDTL
            lblHeader.Text = dtTable.Rows(0).Item("vcHeader")
            lblFooter.Text = dtTable.Rows(0).Item("vcFooter")

            'hplMoveLeft.NavigateUrl = "../DashBoard/frmmanageDashBoard.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Move=L&DID=" & _DashReportID & "&Edit=" & _EditRept & "&frm=" & GetQueryStringVal( "frm") & "&GroupID=" & dtTable.Rows(0).Item("numGroupUserCntID")
            'hplMoveDown.NavigateUrl = "../DashBoard/frmmanageDashBoard.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Move=D&DID=" & _DashReportID & "&Edit=" & _EditRept & "&frm=" & GetQueryStringVal( "frm") & "&GroupID=" & dtTable.Rows(0).Item("numGroupUserCntID")
            'hplMoveRight.NavigateUrl = "../DashBoard/frmmanageDashBoard.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Move=R&DID=" & _DashReportID & "&Edit=" & _EditRept & "&frm=" & GetQueryStringVal( "frm") & "&GroupID=" & dtTable.Rows(0).Item("numGroupUserCntID")
            'hplMoveUp.NavigateUrl = "../DashBoard/frmmanageDashBoard.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Move=U&DID=" & _DashReportID & "&Edit=" & _EditRept & "&frm=" & GetQueryStringVal( "frm") & "&GroupID=" & dtTable.Rows(0).Item("numGroupUserCntID")
            hplDelete.NavigateUrl = "../DashBoard/frmmanageDashBoard.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Move=X&DID=" & _DashReportID & "&Edit=" & _EditRept & "&frm=" & GetQueryStringVal( "frm") & "&GroupID=" & dtTable.Rows(0).Item("numGroupUserCntID")
            hplEdit.NavigateUrl = "../DashBoard/frmAddDashBoardRPT.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Move=E&DID=" & _DashReportID & "&Edit=" & _EditRept & "&frm=" & GetQueryStringVal( "frm") & "&GroupID=" & dtTable.Rows(0).Item("numGroupUserCntID")
            pnlChart.Attributes.Add("ondblclick", "return Redirect(" & dtTable.Rows(0).Item("numReportID") & ")")
            Try
                dtTable.Rows(0).Item("textQueryGrp") = dtTable.Rows(0).Item("textQueryGrp").split("/*sum*/")(0)
                _objCustomReport.DynamicQuery = dtTable.Rows(0).Item("textQueryGrp")
                _objCustomReport.UserCntID = Session("UserContactID")
                _objCustomReport.DomainID = Session("DomainID")
                Dim ds As DataSet
                ds = _objCustomReport.ExecuteDynamicSql()
                Dim dtTable3 As DataTable
                dtTable3 = ds.Tables(0)

                If dtTable.Rows(0).Item("bitGridType") = False Then
                    If dtTable.Rows(0).Item("varGrpflt") = "6" Then
                        If dtTable3.Columns.Count <= 1 Then Exit Function
                    ElseIf dtTable.Rows(0).Item("varGrpflt") = "9" Then
                        If dtTable3.Columns.Count <= 2 Then Exit Function
                        Dim ThisWeek, LastWeek As Integer
                        ThisWeek = Weeknumber_Entire4DayWeekRule(Now)
                        LastWeek = Weeknumber_Entire4DayWeekRule(DateAdd(DateInterval.Day, -7, Now))
                        Dim dr As DataRow
                        If dtTable3.Rows.Count = 0 Then
                            dr = dtTable3.NewRow()
                            dr(0) = ThisWeek
                            dr(2) = 0
                            dtTable3.Rows.Add(dr)
                            dr = dtTable3.NewRow()
                            dr(0) = LastWeek
                            dr(2) = 0
                            dtTable3.Rows.Add(dr)
                        ElseIf dtTable3.Rows.Count = 1 Then
                            dr = dtTable3.NewRow()
                            dr(2) = 0
                            If dtTable3.Rows(0).Item(0) = ThisWeek Then
                                dr(0) = LastWeek
                            Else : dr(0) = ThisWeek
                            End If
                            dtTable3.Rows.Add(dr)
                        End If
                        If dtTable3.Rows.Count > 0 Then
                            If dtTable3.Rows(0).Item(0) = ThisWeek Then
                                dtTable3.Rows(0).Item(0) = "This Week"
                            ElseIf dtTable3.Rows(0).Item(0) = LastWeek Then
                                dtTable3.Rows(0).Item(0) = "Last Week"
                            End If
                            If dtTable3.Rows.Count > 1 Then
                                If dtTable3.Rows(1).Item(0) = ThisWeek Then
                                    dtTable3.Rows(1).Item(0) = "This Week"
                                ElseIf dtTable3.Rows(1).Item(0) = LastWeek Then
                                    dtTable3.Rows(1).Item(0) = "Last Week"
                                End If
                            End If
                        End If
                        dtTable3.Columns.RemoveAt(1)
                    ElseIf dtTable.Rows(0).Item("varGrpflt") = "10" Then
                        If dtTable3.Columns.Count <= 2 Then Exit Function
                        Dim dr As DataRow
                        If dtTable3.Rows.Count = 0 Then
                            dr = dtTable3.NewRow()
                            dr(0) = Month(Now)
                            dr(2) = 0
                            dtTable3.Rows.Add(dr)
                            dr = dtTable3.NewRow()
                            dr(0) = Month(Now) - 1
                            dr(2) = 0
                            dtTable3.Rows.Add(dr)
                        ElseIf dtTable3.Rows.Count = 1 Then
                            dr = dtTable3.NewRow()
                            dr(2) = 0
                            If dtTable3.Rows(0).Item(0) = Month(Now) Then
                                dr(0) = Month(Now) - 1
                            Else : dr(0) = Month(Now)
                            End If
                            dtTable3.Rows.Add(dr)
                        End If
                        For Each dr In dtTable3.Rows
                            If dr(0) = Month(Now) Then
                                dr(0) = "This Month"
                            Else : dr(0) = "Last Month"
                            End If
                        Next
                        dtTable3.Columns.RemoveAt(1)
                    ElseIf dtTable.Rows(0).Item("varGrpflt") = "11" Then
                        If dtTable3.Columns.Count <= 2 Then Exit Function
                        Dim dr As DataRow
                        If dtTable3.Rows.Count = 0 Then
                            dr = dtTable3.NewRow()
                            dr(0) = Year(Now)
                            dr(2) = 0
                            dtTable3.Rows.Add(dr)
                            dr = dtTable3.NewRow()
                            dr(0) = Year(Now) - 1
                            dr(2) = 0
                            dtTable3.Rows.Add(dr)
                        ElseIf dtTable3.Rows.Count = 1 Then
                            dr = dtTable3.NewRow()
                            dr(2) = 0
                            If dtTable3.Rows(0).Item(0) = Year(Now) Then
                                dr(0) = Year(Now) - 1
                            Else : dr(0) = Year(Now)
                            End If
                            dtTable3.Rows.Add(dr)
                        End If
                        For Each dr In dtTable3.Rows
                            If dr(0) = Year(Now) Then
                                dr(0) = "This Year"
                            Else : dr(0) = "Last Year"
                            End If
                        Next
                        dtTable3.Columns.RemoveAt(1)
                    ElseIf dtTable.Rows(0).Item("varGrpflt") = "12" Then
                        If dtTable3.Columns.Count <= 2 Then Exit Function
                        Dim dr As DataRow
                        Dim ThisQuarter, LastQuarter As Integer
                        ThisQuarter = Getquarter(Now)
                        LastQuarter = Getquarter(DateAdd(DateInterval.Quarter, -1, Now))
                        If dtTable3.Rows.Count = 0 Then
                            dr = dtTable3.NewRow()
                            dr(0) = ThisQuarter
                            dr(2) = 0
                            dtTable3.Rows.Add(dr)
                            dr = dtTable3.NewRow()
                            dr(0) = LastQuarter
                            dr(2) = 0
                            dtTable3.Rows.Add(dr)
                        ElseIf dtTable3.Rows.Count = 1 Then
                            dr = dtTable3.NewRow()
                            dr(2) = 0
                            If dtTable3.Rows(0).Item(0) = ThisQuarter Then
                                dr(0) = LastQuarter
                            Else : dr(0) = ThisQuarter
                            End If
                            dtTable3.Rows.Add(dr)
                        End If
                        For Each dr In dtTable3.Rows
                            If dr(0) = ThisQuarter Then
                                dr(0) = "This Quarter"
                            Else : dr(0) = "Last Quarter"
                            End If
                        Next
                        dtTable3.Columns.RemoveAt(1)
                    Else
                        'If dtTable3.Columns.Count <= 2 Then
                        '    Exit Function
                        'End If
                        If dtTable3.Columns(1).ColumnName = "year" Or dtTable3.Columns(1).ColumnName = "Fstart" Then dtTable3.Columns.RemoveAt(1)
                        If dtTable3.Columns.Count <= 1 Then Exit Function
                    End If
                End If

                UltraChart1.Data.DataSource = dtTable3
                UltraChart1.Data.DataBind()
            Catch ex As Exception

            End Try

            'If dtTable.Rows(0).Item("tintColumn") = 1 Then hplMoveLeft.Visible = False
            'If dtTable.Rows(0).Item("tintRow") = dtTable.Rows(0).Item("MinRow") Then hplMoveUp.Visible = False
            'If dtTable.Rows(0).Item("tintColumn") = 3 Then hplMoveRight.Visible = False
            'If dtTable.Rows(0).Item("tintRow") = dtTable.Rows(0).Item("MaxRow") Then hplMoveDown.Visible = False
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function Weeknumber_Entire4DayWeekRule(ByVal inDate As DateTime) As Integer
        Try
            Const JAN As Integer = 1
            Const DEC As Integer = 12
            Const LASTDAYOFDEC As Integer = 31
            Const FIRSTDAYOFJAN As Integer = 1
            Const THURSDAY As Integer = 4
            Dim ThursdayFlag As Boolean = False

            ' Get the day number since the beginning of the year
            Dim DayOfYear As Integer = inDate.DayOfYear

            ' Get the numeric weekday of the first day of the
            ' year (using sunday as FirstDay)
            Dim StartWeekDayOfYear As Integer = _
               DirectCast(New DateTime(inDate.Year, JAN, FIRSTDAYOFJAN).DayOfWeek, Integer)
            Dim EndWeekDayOfYear As Integer = _
                DirectCast(New DateTime(inDate.Year, DEC, LASTDAYOFDEC).DayOfWeek, Integer)

            ' Compensate for the fact that we are using monday
            ' as the first day of the week
            If StartWeekDayOfYear = 0 Then StartWeekDayOfYear = 7
            If EndWeekDayOfYear = 0 Then EndWeekDayOfYear = 7

            ' Calculate the number of days in the first and last week
            Dim DaysInFirstWeek As Integer = 8 - StartWeekDayOfYear
            Dim DaysInLastWeek As Integer = 8 - EndWeekDayOfYear

            ' If the year either starts or ends on a thursday it will have a 53rd week
            If StartWeekDayOfYear = THURSDAY OrElse EndWeekDayOfYear = THURSDAY Then ThursdayFlag = True

            ' We begin by calculating the number of FULL weeks between the start of the year and
            ' our date. The number is rounded up, so the smallest possible value is 0.
            Dim FullWeeks As Integer = _
                CType(Math.Ceiling((DayOfYear - DaysInFirstWeek) / 7), Integer)

            Dim WeekNumber As Integer = FullWeeks

            ' If the first week of the year has at least four days, then the actual week number for our date
            ' can be incremented by one.
            If DaysInFirstWeek >= THURSDAY Then WeekNumber = WeekNumber + 1

            ' If week number is larger than week 52 (and the year doesn't either start or end on a thursday)
            ' then the correct week number is 1.
            If WeekNumber > 52 AndAlso Not ThursdayFlag Then WeekNumber = 1

            'If week number is still 0, it means that we are trying to evaluate the week number for a
            'week that belongs in the previous year (since that week has 3 days or less in our date's year).
            'We therefore make a recursive call using the last day of the previous year.
            If WeekNumber = 0 Then
                WeekNumber = Weeknumber_Entire4DayWeekRule(New DateTime(inDate.Year - 1, DEC, LASTDAYOFDEC))
            End If
            Return WeekNumber
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Getquarter(ByVal DateIn As DateTime) As SByte
        Try
            Dim sbQuarterNum As SByte = (Month(DateIn) - 1) \ 3 + 1
            Return sbQuarterNum
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class