﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCommissionforPayrollExpense.aspx.vb"
    Inherits=".frmCommissionforPayrollExpense1" MasterPageFile="~/common/Popup.Master"
    ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Commission List</title>
    <script language="JavaScript" src="../javascript/date-picker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close" Width="50">
            </asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Commission List
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:DataGrid ID="dgCommission" AllowSorting="true" runat="server" Width="100%" CssClass="dg"
        AutoGenerateColumns="False" >
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="Name" HeaderText="Deal Won ID"></asp:BoundColumn>
            <asp:BoundColumn DataField="OppStatus" HeaderText="Deal Status"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="BizDoc">
                <ItemTemplate>
                    <span class="hyperlink" onclick="return OpenBizInvoice('<%# DataBinder.Eval(Container.DataItem, "numOppId") %>','<%# DataBinder.Eval(Container.DataItem, "numOppBizDocsId") %>');">
                        <%# DataBinder.Eval(Container.DataItem, "vcBizDocID")%></span>
                    <asp:Label ID="lblBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numBizDocId") %>'>
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn HeaderText="Item" DataField="vcItemName"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="Units" DataField="numUnitHour"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="Amount" DataField="monTotAmount" DataFormatString="{0:##,#00.00}">
            </asp:BoundColumn>
            <asp:BoundColumn HeaderText="Vendor Cost" DataField="VendorCost" DataFormatString="{0:##,#00.00}">
            </asp:BoundColumn>
            <asp:BoundColumn HeaderText="Commission Amount" DataField="CommissionAmt" DataFormatString="{0:##,#00.00}">
            </asp:BoundColumn>
            <asp:BoundColumn HeaderText="Commission" DataField="decCommission" DataFormatString="{0:##,#00.00}">
            </asp:BoundColumn>
            <asp:BoundColumn HeaderText="Based On" DataField="BasedOn"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="Commission Type" DataField="CommissionType"></asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
</asp:Content>
