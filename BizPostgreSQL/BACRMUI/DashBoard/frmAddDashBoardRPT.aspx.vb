Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Partial Public Class frmAddDashBoardRPT
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadDropdown()

                If GetQueryStringVal("DID") <> "" Then LoadDashboardReptDTL()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDashboardReptDTL()
        Try
            Dim objDashboard As New DashBoard
            objDashboard.DashboardReptId = CCommon.ToLong(GetQueryStringVal("DID"))
            Dim dtTable As DataTable
            dtTable = objDashboard.GetReptDTL
            If Not ddlCustReport.Items.FindByValue(dtTable.Rows(0).Item("numReportID")) Is Nothing Then
                ddlCustReport.Items.FindByValue(dtTable.Rows(0).Item("numReportID")).Selected = True
            End If
            If Not ddlChartType.Items.FindByValue(dtTable.Rows(0).Item("tintChartType")) Is Nothing Then
                ddlChartType.Items.FindByValue(dtTable.Rows(0).Item("tintChartType")).Selected = True
            End If
            If dtTable.Rows(0).Item("bitGridType") = False Then
                radChart.Visible = True
            Else : radChart.Visible = False
            End If
            If dtTable.Rows(0).Item("tintReportType") = 1 Then
                radTable.Checked = True
            Else : radChart.Checked = True
            End If
            txtFooter.Text = dtTable.Rows(0).Item("vcFooter")
            txtHeader.Text = dtTable.Rows(0).Item("vcHeader")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDropdown()
        Try
            Dim objCustReport As New CustomReports
            objCustReport.UserCntID = Session("UserContactId")
            objCustReport.DomainID = Session("DomainId")
            If GetQueryStringVal("frm") = "Admin" Then
                objCustReport.GroupId = CCommon.ToLong(GetQueryStringVal("GroupID"))
            Else : objCustReport.GroupId = Session("UserGroupID")
            End If

            objCustReport.GridType = False
            ddlCustReport.DataSource = objCustReport.getCustomReportUser
            ddlCustReport.DataTextField = "vcReportName"
            ddlCustReport.DataValueField = "numReportId"
            ddlCustReport.DataBind()
            Dim listitem As New ListItem
            listitem.Text = "--SelectOne--"
            listitem.Value = "0"
            ddlCustReport.Items.Insert(0, listitem)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Dim objDashboard As New DashBoard
            objDashboard.ReportType = IIf(radTable.Checked = True, 1, 2)
            objDashboard.ReportID = ddlCustReport.SelectedItem.Value
            objDashboard.DashboardReptId = CCommon.ToLong(GetQueryStringVal("DID"))
            objDashboard.ColumnPos = CCommon.ToShort(GetQueryStringVal("Column"))
            If GetQueryStringVal("frm") = "Admin" Then
                objDashboard.GroupId = CCommon.ToLong(GetQueryStringVal("GroupID"))
            Else : objDashboard.UserCntID = Session("UserContactID")
            End If
            objDashboard.Header = txtHeader.Text
            objDashboard.Footer = txtFooter.Text
            objDashboard.ChartType = ddlChartType.SelectedValue
            objDashboard.ManageDashboard()
            Response.Redirect("../DashBoard/frmDashBoard.aspx?frm=" & GetQueryStringVal("frm") & "&GroupID=" & GetQueryStringVal("GroupID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../DashBoard/frmDashBoard.aspx?frm=" & GetQueryStringVal("frm") & "&GroupID=" & GetQueryStringVal("GroupID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCustReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCustReport.SelectedIndexChanged
        Try
            Dim dtTable As DataTable
            Dim objCustomrept As New CustomReports
            objCustomrept.DomainID = Session("DomainID")
            objCustomrept.ReportID = ddlCustReport.SelectedValue
            dtTable = objCustomrept.getReportDetails

            If dtTable.Rows(0).Item("bitGridType") = False Then
                radChart.Visible = True
            Else : radTable.Checked = True
                radChart.Visible = False
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class