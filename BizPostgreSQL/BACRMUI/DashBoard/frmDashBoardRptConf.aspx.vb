Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Public Class frmDashBoardRptConf
    Inherits BACRMPage
    Dim ObjReport As CustomReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                

               
                
                GetUserRightsForPage(13, 27)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnSave.Visible = False
                    btnSaveClose.Visible = False
                End If

                Dim objUserGroups As New UserGroups
                Dim dtGroupName As DataTable
                objUserGroups.DomainId = Session("DomainID")
                objUserGroups.SelectedGroupTypes = "1,4"
                dtGroupName = objUserGroups.GetAutorizationGroup

                ddlGroup.DataSource = dtGroupName.DataSet.Tables(0).DefaultView
                ddlGroup.DataTextField = "vcGroupName"
                ddlGroup.DataValueField = "numGroupId"
                ddlGroup.DataBind()
                ddlGroup.Items.Insert(0, "--Select One--")
                ddlGroup.Items.FindByText("--Select One--").Value = 0

                If Session("UserGroupID") IsNot Nothing Then
                    If ddlGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                        ddlGroup.ClearSelection()
                        ddlGroup.Items.FindByValue(Session("UserGroupID")).Selected = True
                    End If
                End If

                BindDataGrid()
            End If

            If Session("UserGroupID") IsNot Nothing Then
                If ddlGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                    ddlGroup.Items.FindByValue(Session("UserGroupID")).Attributes.Add("style", "color:green")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDataGrid()
        Try
            ObjReport = New CustomReports
            ObjReport.DomainID = Session("DomainId")
            ObjReport.GroupId = ddlGroup.SelectedValue
            dgReport.DataSource = ObjReport.getReportListAll()
            dgReport.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgReport.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim chkSelect As CheckBox
                chkSelect = e.Item.FindControl("ChkSelect")
                chkSelect.Checked = e.Item.Cells(3).Text
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub Save()
        Try
            If ObjReport Is Nothing Then ObjReport = New CustomReports
            ObjReport.strReportIds = txtReportValues.Text
            ObjReport.GroupId = ddlGroup.SelectedValue
            ObjReport.saveAllowedReports()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            Response.Write("<script>self.close()</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class