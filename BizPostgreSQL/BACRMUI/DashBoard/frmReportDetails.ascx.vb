Imports BACRM.BusinessLogic.Reports
Imports Infragistics.UltraChart.Shared.Styles
Imports Infragistics.UltraChart.Resources.Appearance

Partial Public Class frmReportDetails
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Write(Request.QueryString("RepID"))
    End Sub

    Private _DashReportID As Long

    Public Property DashReportID() As Long
        Get
            Return _DashReportID
        End Get
        Set(ByVal Value As Long)
            _DashReportID = Value
        End Set
    End Property

    Private _bitGroup As Boolean

    Public Property bitGroup() As Boolean
        Get
            Return _bitGroup
        End Get
        Set(ByVal Value As Boolean)
            _bitGroup = Value
        End Set
    End Property

    Public Function CreateReport() As Boolean
        Dim objDashboard As New DashBoard
        objDashboard.DashboardReptId = _DashReportID
        Dim dtTable As DataTable
        dtTable = objDashboard.GetReptDTL
        lblHeader.Text = dtTable.Rows(0).Item("vcHeader")
        lblFooter.Text = dtTable.Rows(0).Item("vcFooter")
        Return True
    End Function


End Class