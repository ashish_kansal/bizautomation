<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PagesIP.aspx.vb" Inherits="BACRM.UserInterface.webReports.PagesIP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Page Visited IP's </title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />

    <script language="javascript" type="text/javascript">
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Table ID="Table10" runat="server" GridLines="None" CellPadding="0" CellSpacing="0"
            BackColor="white" Width="100%">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top" CssClass="normal7">
                    <table width="100%">
                        <tr align="center" valign="top">
                            <td>
                                Page Name :
                                <asp:Label runat="server" ID="lblPage" CssClass="normal1"></asp:Label>
                                <br />
                            </td>
                        </tr>
                    </table>
                    <table width="100%" class="TabStyle">
                        <tr>
                            <table class="TabStyle" bordercolor="black" cellspacing="0">
                                <tr>
                                    <td class="Text_bold_White" height="23">
                                        &nbsp;&nbsp;&nbsp;IP Address & User Domain&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataGrid ID="dgIP" runat="server" CssClass="dg" Width="100%"
                                    AllowSorting="True" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="vcUserHostAddress" HeaderText="<font color=white>IP Address</font> ">
                                        </asp:BoundColumn>
                                    </Columns>
                                    <Columns>
                                        <asp:BoundColumn DataField="vcUserDomain" HeaderText="<font color=white>UserDomain</font> ">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
    </form>
</body>
</html>
