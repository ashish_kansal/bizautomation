<%@ Page Language="vb" AutoEventWireup="false" Codebehind="frmPagesVisted.aspx.vb" Inherits="BACRM.UserInterface.webReports.frmPagesVisted"%>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>  
        
		<script language="javascript" type="text/javascript">
		function getIP(pageId)
		{
		   
		  var PagesIP=  window.open('../webreport/PagesIP.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&FromDate='+document.Form1.calFrom$txtDate.value+'&ToDate='+document.Form1.calTo$txtDate.value+'&PageId='+pageId,"PagesIP","width=400,height=500,status=no,scrollbars=yes,left=260,top=200") 		;
		  PagesIP.focus()
		    return false;
		}
		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			
	<table cellSpacing="0" cellpadding="0" width="100%"  border="0">
			
						<tr>
					<td vAlign="top" >
			<table cellPadding="5" width="100%">
				<tr>
					<td class="normal1">     <table class="normal1">
					                    <tr>
					                        <td>
					                             From
					                        </td>
					                        <td>
					                            <BizCalendar:Calendar ID="calFrom" runat="server" />
					                        </td>
					                        <td>
					                            To
					                        </td>
					                        <td>
					                            <BizCalendar:Calendar ID="calTo" runat="server" />
					                        </td>
					                        <td>
					                            <asp:button id="btnGo" CssClass="button" Text="Go" Runat="server"></asp:button>
					                        </td>
					                    </tr>    					    
					                </table>
						</td>
				
					<td class="normal1" align="center" colSpan="5">No of Records:
						<asp:label id="lblRecordCount" runat="server"></asp:label></td>
				</tr>
			</table>
			
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle" borderColor="black" cellSpacing="0" border="0">
							<tr>
								<td class="Text_bold_White" height="23">&nbsp;&nbsp;&nbsp;How often has a page been 
									visited&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td id="hidenav" noWrap align="right" runat="server">
						<table>
							<tr class=normal1>
								<td><asp:label id="lblNext" runat="server" cssclass="Text_bold">Next:</asp:label></td>
								<td class="normal1"><asp:linkbutton id="lnk2" runat="server">2</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk3" runat="server">3</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk4" runat="server">4</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk5" runat="server">5</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkFirst" runat="server"><div class="LinkArrow"><<</div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkPrevious" runat="server"><div class="LinkArrow"><</div>
									</asp:linkbutton></td>
								<td class="normal1"><asp:label id="lblPage" runat="server">Page</asp:label></td>
								<td><asp:textbox id="txtCurrrentPage" runat="server" CssClass="signup" Width="28px" MaxLength="5"
										AutoPostBack="True"></asp:textbox></td>
								<td class="normal1"><asp:label id="lblOf" runat="server">of</asp:label></td>
								<td class="normal1"><asp:label id="lblTotal" runat="server"></asp:label></td>
								<td><asp:linkbutton id="lnkNext" runat="server" CssClass="LinkArrow"><div class="LinkArrow">></div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkLast" runat="server"><div class="LinkArrow">>></div>
									</asp:linkbutton></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<asp:table id="Table1" Height="350" Width="100%" Runat="server" GridLines="None" BorderColor="black"
				BorderWidth="1" CellSpacing="0" CellPadding="0">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
					<asp:datagrid id="dgPagesVisted" runat="server" CssClass="dg" Width="100%"
				AllowSorting="True" AutoGenerateColumns="False">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
				    
					<asp:TemplateColumn SortExpression="vcPageName" HeaderText="<font color=white>Page Name</font> ">
					<ItemTemplate>
					 	 <asp:LinkButton ID="l1" Text='<%#DataBinder.Eval(Container.DataItem, "vcPageName")%>' runat="server"></asp:LinkButton>
					</ItemTemplate>										
				    </asp:TemplateColumn>
					
					<asp:BoundColumn DataField="Times" SortExpression="Times" HeaderText="<font color=white>No of Times</font>"></asp:BoundColumn>
					
				</Columns>
			</asp:datagrid>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table><asp:textbox id="txtTotalPage" style="DISPLAY: none" Runat="server"></asp:textbox><asp:textbox id="txtTotalRecords" style="DISPLAY: none" Runat="server"></asp:textbox><asp:textbox id="txtSortChar" style="DISPLAY: none" Runat="server"></asp:textbox>
			</td>
				</tr>
			</table>
			</form>
	</body>
</HTML>
