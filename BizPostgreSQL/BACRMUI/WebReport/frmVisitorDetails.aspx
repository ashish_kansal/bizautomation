<%@ Page Language="vb" AutoEventWireup="false" Codebehind="frmVisitorDetails.aspx.vb" Inherits="BACRM.UserInterface.webReports.frmVisitorDetails"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
        <script type="text/javascript" language="javascript">
        function openSite(a)
        {
            window.open(a)
            return false
        }
        </script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<br>
			<table width="100%">
				<tr>
					<td align="center" colspan="2" class="normal1">
						<br>
						<font size="4">Visitor details</font>
						<br>
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						User HostAddress :
					</td>
					<td class="normal1">
						<asp:Label ID="lblHostAdd" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						Domain :
					</td>
					<td class="normal1">
						<asp:Label ID="lblDomain" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						User Agent :
					</td>
					<td class="normal1">
						<asp:Label ID="lblUserAgent" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						Browser :
					</td>
					<td class="normal1">
						<asp:Label ID="lblBrowser" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						Crawler :
					</td>
					<td class="normal1">
						<asp:Label ID="lblCrawler" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						URL :
					</td>
					<td class="normal1">
					<asp:HyperLink runat="server" ID="hplURL" CssClass="hyperlink"></asp:HyperLink>
						
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						Referrer :
					</td>
					<td class="normal1">
					<asp:HyperLink runat="server" ID="hplReferrer" CssClass="hyperlink"></asp:HyperLink>
						
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						Orginal URL :
					</td>
					<td class="normal1">
					<asp:HyperLink runat="server" ID="hplOrgURL" CssClass="hyperlink"></asp:HyperLink>
						
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						Orginal Referrer :
					</td>
					<td class="normal1">
					<asp:HyperLink runat="server" ID="hplOrgref" CssClass="hyperlink"></asp:HyperLink>
						
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						No of Pages Visited :
					</td>
					<td class="normal1">
						<asp:Label ID="lblNoofpages" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						Total Time :
					</td>
					<td class="normal1">
						<asp:Label ID="lblTotalTime" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1">
						Date Visited :
					</td>
					<td class="normal1">
						<asp:Label ID="lblDateVisited" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:datagrid id="dgDetails" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="false">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
							    	<asp:TemplateColumn SortExpression="vcPageName" HeaderText="<font color=white>Page Name</font> ">
					                <ItemTemplate>
					 	                 <asp:LinkButton ID="l1" Text='<%#DataBinder.Eval(Container.DataItem, "vcPageName")%>' runat="server"></asp:LinkButton>
					                </ItemTemplate>										
				                    </asp:TemplateColumn>
								<asp:BoundColumn DataField="vcElapsedTime" SortExpression="vcElapsedTime" HeaderText="<font color=white>Elasped Time</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="dtTime" SortExpression="dtTime" HeaderText="<font color=white>Time</font>"></asp:BoundColumn>
							</Columns>
						</asp:datagrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
