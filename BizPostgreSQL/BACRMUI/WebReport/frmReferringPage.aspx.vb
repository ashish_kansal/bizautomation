Imports BACRM.BusinessLogic.Tracking
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.webReports
    Partial Class frmReferringPage : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then

                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    Session("Asc") = 1
                    txtCurrrentPage.Text = 1
                    txtWeek.Text = "0"
                    txtDay.Text = "0"
                    LoadDropDown()
                    BindDatagrid()
                End If
                ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('6');}else{ parent.SelectTabByValue('6'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Function BindDatagrid() As DataTable
            Try
                Dim dtTable As DataTable
                Dim dtDate As DataTable
                Dim objBusinessClass As New BusinessClass
                Dim SortChar As Char
                Dim flag As Boolean = False

                If ViewState("SortChar") <> "" Then
                    SortChar = ViewState("SortChar")
                Else : SortChar = "0"
                End If
                With objBusinessClass
                    .DomainID = Session("DomainId")
                    .Referrer = txtReferrer.Text
                    .SearchTerm = txtSearchTerm.Text
                    .MinimumPages = CInt(ddlMinPagesViewed.SelectedValue)
                    If (txtWeek.Text = "0" And txtDay.Text = "0") Then
                        .FromDate = calFrom.SelectedDate
                        .ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))
                    End If
                    If (txtWeek.Text <> "0" And txtDay.Text = "0" And pnlweek.Visible = True) Then
                        .FromDate = txtWeek.Text
                        '.ToDate = txtWeek.Text
                        .ToDate = DateAdd(DateInterval.Day, 1, CDate(txtWeek.Text))
                        txtWeek.Text = "0"
                    End If
                    If (txtDay.Text <> "0" And txtWeek.Text = "0" And pnlDay.Visible = True) Then
                        .FromDate = txtDay.Text
                        .ToDate = l1.Text
                        l1.Text = "0"
                        txtDay.Text = "0"
                    End If

                    If (txtDay.Text = "0" And txtWeek.Text = "0" And pnlYear.Visible = True And ddlYear.SelectedValue <> 0) Then
                        flag = True
                        .FromDate = DateSerial(CInt(ddlYear.SelectedValue), 1, 1)
                        .ToDate = DateSerial(CInt(ddlYear.SelectedValue), 12, 31)
                        txtDay.Text = "0"
                    End If

                    If txtCurrrentPage.Text.Trim <> "" Then
                        .CurrentPage = txtCurrrentPage.Text
                    Else : .CurrentPage = 1
                    End If
                    .PageSize = 20
                    .TotalRecords = 0
                    If ViewState("Column") <> "" Then
                        .columnName = ViewState("Column")
                    Else : .columnName = "dtCreated"
                    End If
                    If Session("Asc") = 1 Then
                        .columnSortOrder = "Desc"
                    Else
                        .columnSortOrder = "Asc"
                    End If
                    .UserCrawler = ddlCrawler.SelectedValue
                    .ClientZoneOffset = Session("ClientMachineUTCTimeOffset")
                End With

                If (flag = True) Then 'to get the date 
                    dtDate = objBusinessClass.GetVisitorsList(ConfigurationManager.AppSettings("ConnectionString"), True)
                End If

                dtTable = objBusinessClass.GetVisitorsList(ConfigurationManager.AppSettings("ConnectionString"), False)

                If objBusinessClass.TotalRecords = 0 Then
                    hidenav.Visible = False
                    lblRecordCount.Text = 0
                Else
                    hidenav.Visible = True
                    lblRecordCount.Text = String.Format("{0:#,###}", objBusinessClass.TotalRecords)
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblRecordCount.Text / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                        lblTotal.Text = strTotalPage(0)
                        txtTotalPage.Text = strTotalPage(0)
                    Else
                        lblTotal.Text = strTotalPage(0) + 1
                        txtTotalPage.Text = strTotalPage(0) + 1
                    End If
                    txtTotalRecords.Text = lblRecordCount.Text
                End If
                dgReferringPage.DataSource = dtTable
                dgReferringPage.DataBind()
                Return dtDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Public Function binddatagrid2() As DataTable
        '    Try

        '        Dim dr As DataSet


        '        Return dr.Tables(0)
        '    Catch ex As Exception
        '        ' Log exception details
        '        Throw ex
        '    End Try
        'End Function

        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                ddlYear.SelectedValue = "0"
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
            Try
                txtCurrrentPage.Text = txtTotalPage.Text
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
            Try
                If txtCurrrentPage.Text = 1 Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text - 1
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
            Try
                If txtCurrrentPage.Text = txtTotalPage.Text Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 1
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
            Try
                If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 2
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
            Try
                If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 3
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
            Try
                If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 4
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
            Try
                If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 5
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgReferringPage_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgReferringPage.SortCommand
            Try
                If ViewState("Column") <> e.SortExpression.ToString() Then
                    ViewState("Column") = e.SortExpression.ToString()
                    Session("Asc") = 0
                Else
                    If Session("Asc") = 0 Then
                        Session("Asc") = 1
                    Else : Session("Asc") = 0
                    End If
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgReferringPage_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgReferringPage.ItemDataBound
            Try
                If (e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item) Then
                    e.Item.Cells(1).Attributes.Add("onclick", "return OpenDTLPage(" & e.Item.Cells(0).Text & ")")
                    e.Item.Cells(4).ToolTip = e.Item.Cells(9).Text
                    e.Item.Cells(5).ToolTip = e.Item.Cells(10).Text
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlCrawler_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCrawler.SelectedIndexChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlMinPagesViewed_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMinPagesViewed.SelectedIndexChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnWeek_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWeek.Click
            Try
                ddlYear.SelectedValue = "0"
                l1.Text = DateAdd(DateInterval.Day, (CDbl(txtDay.Text) * -1) + 1, Today())
                txtWeek.Text = DateAdd(DateInterval.WeekOfYear, CDbl(txtWeek.Text) * -1, Today())
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnday_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnday.Click
            Try
                ddlYear.SelectedValue = "0"
                l1.Text = DateAdd(DateInterval.Day, (CDbl(txtDay.Text) * -1) + 1, Today())
                txtDay.Text = DateAdd(DateInterval.Day, CDbl(txtDay.Text) * -1, Today())
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReport.SelectedIndexChanged
            Try
                ReportPanel()
                BindDatagrid()
                ddlYear.SelectedValue = "0"
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objBusinessClass As New BusinessClass
                With objBusinessClass
                    .MinimumPages = ddlMinPagesViewed.SelectedValue
                    .ReportType = ddlReport.SelectedValue
                    .SaveSettingsVisitor(ConfigurationManager.AppSettings("ConnectionString"))
                End With
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub LoadDropDown()
            Try
                Dim objBusinessClass As New BusinessClass
                Dim dt As New DataTable
                objBusinessClass.DomainID = Session("DomainId")
                dt = objBusinessClass.LoadDropdown(ConfigurationManager.AppSettings("ConnectionString"))
                ddlMinPagesViewed.SelectedValue = dt.Rows(0).Item("numMinPages")
                ddlReport.SelectedValue = dt.Rows(0).Item("numReportType")
                ddlYear.Items.Add(Year(Now()) - 4)
                ddlYear.Items.Add(Year(Now()) - 3)
                ddlYear.Items.Add(Year(Now()) - 2)
                ddlYear.Items.Add(Year(Now()) - 1)
                ddlYear.Items.Add(Year(Now()))
                ddlYear.Items.Add(Year(Now()) + 1)
                ddlYear.Items.Add(Year(Now()) + 2)
                ddlYear.Items.Add(Year(Now()) + 3)
                ddlYear.Items.Add(Year(Now()) + 4)
                ReportPanel()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ReportPanel()
            Try
                If ddlReport.SelectedValue = "0" Then
                    pnlweek.Visible = False
                    pnlDay.Visible = False
                    pnlYear.Visible = False
                End If
                If ddlReport.SelectedValue = "1" Then
                    pnlweek.Visible = True
                    pnlDay.Visible = False
                    pnlYear.Visible = False
                End If
                If ddlReport.SelectedValue = "2" Then
                    pnlweek.Visible = False
                    pnlDay.Visible = True
                    pnlYear.Visible = False
                End If
                If ddlReport.SelectedValue = "3" Then
                    pnlweek.Visible = False
                    pnlDay.Visible = False
                    pnlYear.Visible = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
            Try
                Dim dtDate As DataTable
                Dim i As Integer = 0
                Dim heighest As Integer
                Dim MonthTotal(11) As Double
                Dim MonthTotalPer(11) As Double
                Dim temp As Integer
                'Declare The DataTable Cell Objects.
                Dim tcColValue As TableCell
                Dim strImage As String
                If (ddlYear.SelectedValue <> 0) Then
                    dtDate = BindDatagrid()
                    While (i < 11)
                        MonthTotal(i) = 0
                        i = i + 1
                    End While

                    If dtDate.Rows.Count > 0 Then                      'looping through the datatable to get the records in month
                        While (i <= dtDate.Rows.Count - 1)

                            temp = Month(CDate(dtDate.Rows(i).Item("dtTime")))
                            MonthTotal(temp - 1) += 1
                            i += 1
                        End While
                    End If

                    i = 0
                    While (i <= 11)                                     'looping through array to find the heighest
                        If MonthTotal(i) > heighest Then
                            heighest = MonthTotal(i)
                        End If
                        i += 1
                    End While

                    i = 0
                    While (i <= 11)                                     'looping through array to percentage
                        If (heighest <> 0) Then
                            MonthTotalPer(i) = (MonthTotal(i) / heighest) * 100
                        End If
                        i += 1
                    End While

                    tcColValue = New TableCell                          'displaying the graph
                    tcColValue.CssClass = "text"
                    tcColValue.VerticalAlign = VerticalAlign.Bottom
                    tcColValue.HorizontalAlign = HorizontalAlign.Left
                    row1.Cells.Add(tcColValue)

                    For i = 0 To 11
                        strImage = "<img src='../images/bottom_line.gif' width='5' height='" & MonthTotalPer(i) & "' > "
                        tcColValue = New TableCell
                        tcColValue.CssClass = "text"
                        tcColValue.VerticalAlign = VerticalAlign.Bottom

                        If (MonthTotal(i) <> 0) Then
                            tcColValue.Controls.Add(New LiteralControl(strImage & "" & Format(MonthTotal(i), "#,###") & ""))
                        Else : tcColValue.Controls.Add(New LiteralControl(strImage & "0"))
                        End If
                        tcColValue.HorizontalAlign = HorizontalAlign.Left
                        row1.Cells.Add(tcColValue)
                    Next
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace