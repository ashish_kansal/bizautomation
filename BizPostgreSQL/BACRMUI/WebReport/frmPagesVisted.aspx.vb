Imports BACRM.BusinessLogic.Tracking
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.webReports
    Partial Class frmPagesVisted : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then

                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    Session("Asc") = 1
                    txtCurrrentPage.Text = 1
                    BindDatagrid()
                End If
                ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('6');}else{ parent.SelectTabByValue('6'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindDatagrid()
            Try
                Dim dtTable As DataTable
                Dim objBusinessClass As New BusinessClass
                Dim SortChar As Char
                If ViewState("SortChar") <> "" Then
                    SortChar = ViewState("SortChar")
                Else : SortChar = "0"
                End If
                With objBusinessClass
                    .FromDate = calFrom.SelectedDate
                    .ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))
                    .DomainID = Session("domainId")
                    If txtCurrrentPage.Text.Trim <> "" Then
                        .CurrentPage = txtCurrrentPage.Text
                    Else : .CurrentPage = 1
                    End If
                    .DomainID = Session("DomainId")
                    .PageSize = 20
                    .TotalRecords = 0
                    If ViewState("Column") <> "" Then
                        .columnName = ViewState("Column")
                    Else : .columnName = "vcPageName"
                    End If
                    If Session("Asc") = 1 Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                    .ClientZoneOffset = Session("ClientMachineUTCTimeOffset")
                End With
                dtTable = objBusinessClass.HowOftenPagesrVisited(ConfigurationManager.AppSettings("ConnectionString"))
                If objBusinessClass.TotalRecords = 0 Then
                    hidenav.Visible = False
                    lblRecordCount.Text = 0
                Else
                    hidenav.Visible = True
                    lblRecordCount.Text = String.Format("{0:#,###}", objBusinessClass.TotalRecords)
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblRecordCount.Text / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (lblRecordCount.Text Mod 20) = 0 Then
                        lblTotal.Text = strTotalPage(0)
                        txtTotalPage.Text = strTotalPage(0)
                    Else
                        lblTotal.Text = strTotalPage(0) + 1
                        txtTotalPage.Text = strTotalPage(0) + 1
                    End If
                    txtTotalRecords.Text = lblRecordCount.Text
                End If
                dgPagesVisted.DataSource = dtTable
                dgPagesVisted.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
            Try
                txtCurrrentPage.Text = txtTotalPage.Text
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
            Try
                If txtCurrrentPage.Text = 1 Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text - 1
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
            Try
                If txtCurrrentPage.Text = txtTotalPage.Text Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 1
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
            Try
                If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 2
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
            Try
                If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 3
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
            Try
                If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 4
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
            Try
                If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
                    Exit Sub
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 5
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgPagesVisted_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPagesVisted.ItemCommand
            Try
                Dim objBusinessClass As New BusinessClass
                Dim dtTable As DataTable
                Dim txtCurrrentPage1 As New TextBox
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgPagesVisted_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPagesVisted.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim lnk As LinkButton
                    lnk = e.Item.FindControl("l1")
                    lnk.Attributes.Add("onclick", "return getIP('" & lnk.Text & "')")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgPagesVisted_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgPagesVisted.SortCommand
            Try
                If ViewState("Column") <> e.SortExpression.ToString() Then
                    ViewState("Column") = e.SortExpression.ToString()
                    Session("Asc") = 0
                Else
                    If Session("Asc") = 0 Then
                        Session("Asc") = 1
                    Else : Session("Asc") = 0
                    End If
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub btnInkTomi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInkTomi.Click
        '    Dim objBusinessClass As New BusinessClass
        '    objBusinessClass.DeleteInkTomi(ConfigurationManager.AppSettings("ConnectionString"))
        'End Sub

    End Class
End Namespace