
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="frmReferringPage.aspx.vb" Inherits="BACRM.UserInterface.webReports.frmReferringPage"%>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>  
        
		<script language="javascript">
		function OpenDTLPage(a)
		{
	
			window.open("../webReport/frmVisitorDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&TrackID="+a,'','toolbar=no,titlebar=no,left=200, top=100,width=600,height=500,scrollbars=yes,resizable=yes')
			return false;
		}
		
		function fnWeek(week)
			{
				document.Form1.txtWeek.value = week;
				
				document.Form1.btnWeek.click();
			}
			
			function fnDay(Day)
			{
				document.Form1.txtDay.value = Day;
				document.Form1.btnday.click();
			}
			
		</script>

	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellpadding="0" width="100%"  border="0">
			
						<tr>
					<td vAlign="top" >
			<table cellPadding="5" width="100%">
							<tr>
								<td class="normal1">
								         <table class="normal1">
					                    <tr>
					                        <td>
					                             From
					                        </td>
					                        <td>
					                            <BizCalendar:Calendar ID="calFrom" runat="server" />
					                        </td>
					                        <td>
					                            To
					                        </td>
					                        <td>
					                            <BizCalendar:Calendar ID="calTo" runat="server" />
					                        </td>					                        
					                    </tr>    					    
					                </table>
								</td>
								<td class="normal1">Search Term&nbsp;<asp:textbox id="txtSearchTerm" Runat="server" CssClass="signup"></asp:textbox>
								</td>
                                <td class="normal1">
                                </td>
								
								<td class="normal1">Referrer&nbsp;<asp:textbox id="txtReferrer" Runat="server" CssClass="signup"></asp:textbox>
								</td>
								<td></td>
								<td>
								<asp:DropDownList ID="ddlCrawler" runat="server" AutoPostBack="true" CssClass="normal1" >
									    <asp:ListItem Value="False">Non-Crawler</asp:ListItem>
								    <asp:ListItem Value="True">Crawler</asp:ListItem>
								    <asp:ListItem Value="">All</asp:ListItem>
								</asp:DropDownList>
								</td>
								<td><asp:button id="btnGo" CssClass="button" Runat="server" Text="Go"></asp:button>
                                    <asp:Button ID="btnSave" runat="server" CssClass="button"  Text="Save Settings"/></td>
							</tr>
							<tr>							
							<td class="normal1" >
							Minimum Number of Pages Viewed : &nbsp; 
							<asp:DropDownList ID="ddlMinPagesViewed" runat="server" AutoPostBack="true" cssclass="normal1">
							<asp:ListItem Text="1" Value="1" Selected="True"></asp:ListItem>
							<asp:ListItem Text="2" Value="2"></asp:ListItem>
							<asp:ListItem Text="3" Value="3"></asp:ListItem>
							<asp:ListItem Text="4" Value="4"></asp:ListItem>
							<asp:ListItem Text="5" Value="5"></asp:ListItem>
							<asp:ListItem Text="6" Value="6"></asp:ListItem>
							<asp:ListItem Text="7" Value="7"></asp:ListItem>
							<asp:ListItem Text="8" Value="8"></asp:ListItem>
							<asp:ListItem Text="9" Value="9"></asp:ListItem>
							<asp:ListItem Text="10" Value="10"></asp:ListItem>
							</asp:DropDownList>
							</td>
							<td class="normal1" >
							Select Report: &nbsp; 
							<asp:DropDownList ID="ddlReport" runat="server" AutoPostBack="true" cssclass="normal1" >
							<asp:ListItem Text="--Select One--" Value="0" Selected="True"></asp:ListItem>
							<asp:ListItem Text="Weeks Ago " Value="1"></asp:ListItem>
							<asp:ListItem Text="Days Ago" Value="2"></asp:ListItem>
							<asp:ListItem Text="Monthly Average" Value="3"></asp:ListItem>
							</asp:DropDownList>
							</td>
                                <td align="left" class="normal1" colspan="1">
                                </td>
							<td class="normal1" align="left" colSpan="5" style="width: 512px">No of Records:
							<asp:label id="lblRecordCount" runat="server"></asp:label>
							</td>
                                <td align="left" class="normal1" colspan="1">
                                </td>
							<td>
                                &nbsp;</td>
							
							</tr>
							
							
							<asp:Panel ID="pnlweek" runat="server"  Visible="false" >
							<asp:table runat="server" id="Table3" Width="60%"  Height="10px" CellPadding="0" CellSpacing="1" 
							                BorderWidth="1px" HorizontalAlign="Center"  >
		                       <asp:TableRow HorizontalAlign="Center" >
		                        <asp:TableCell CssClass = "text">
							         Weeks Ago (From This Day) : 
							        <a id="W1" href="javascript:fnWeek('1')">1</a>&nbsp;
							        <a id="W2" href="javascript:fnWeek('2')">2</a>&nbsp;
							        <a id="W3" href="javascript:fnWeek('3')">3</a>&nbsp;
							        <a id="W4" href="javascript:fnWeek('4')">4<em><asp:Label ID="Label1" Text="(1m)" CssClass="normal1" runat="server"/></em></a>&nbsp;
							        <a id="W5" href="javascript:fnWeek('5')">5</a>&nbsp;
							        <a id="W6" href="javascript:fnWeek('6')">6</a>&nbsp;
							        <a id="W7" href="javascript:fnWeek('7')">7</a>&nbsp;
							        <a id="a8" href="javascript:fnWeek('8')">8<em><asp:Label ID="Label2" Text="(2m)" CssClass="normal1" runat="server"/></em></a>&nbsp;
							        <a id="W12" href="javascript:fnWeek('12')">12<em><asp:Label ID="Label3" Text="(3m)" CssClass="normal1" runat="server"/></em></a>&nbsp;
							        <a id="W18" href="javascript:fnWeek('18')">18<em><asp:Label ID="Label4" Text="(4m)" CssClass="normal1" runat="server"/></em></a>&nbsp;
							        <a id="W22" href="javascript:fnWeek('22')">22<em><asp:Label ID="Label5" Text="(5m)" CssClass="normal1" runat="server"/></em></a>&nbsp;
							        <a id="W26" href="javascript:fnWeek('26')">26<em><asp:Label ID="Label6" Text="(6m)" CssClass="normal1" runat="server"/></em></a>&nbsp;
							        <a id="W40" href="javascript:fnWeek('40')">40<em><asp:Label ID="Label7" Text="(9m)" CssClass="normal1" runat="server"/></em></a>&nbsp;
							        <a id="W52" href="javascript:fnWeek('52')">52<em><asp:Label ID="Label8" Text="(1yr)" CssClass="normal1" runat="server"/></em></a>&nbsp;
							      </asp:TableCell>
							     </asp:TableRow>
						     </asp:table>
						     </asp:Panel>
							
							
							<asp:Panel ID="pnlDay" runat="server" Visible="false">
							 <asp:table runat="server" id="Table2" Width="60%"  Height="10px" CellPadding="0" CellSpacing="1" 
							                BorderWidth="1px" HorizontalAlign="Center"  >
							                <asp:TableRow HorizontalAlign="Center" ><asp:TableCell CssClass = "text">
							                Days Ago (From This Day)							
							                        <a href="javascript:fnDay('1')">1</a>&nbsp;
							                        <a href="javascript:fnDay('2')">2</a>&nbsp;
							                        <a href="javascript:fnDay('3')">3</a>&nbsp;
							                        <a href="javascript:fnDay('4')">4</a>&nbsp;
							                        <a href="javascript:fnDay('5')">5</a>&nbsp;
							                        <a href="javascript:fnDay('6')">6</a>&nbsp;
							                        <a href="javascript:fnDay('7')">7</a>&nbsp;
							                        <a href="javascript:fnDay('8')">8</a>&nbsp;
							                        <a href="javascript:fnDay('9')">9</a>&nbsp;
							                        <a href="javascript:fnDay('10')">10</a>&nbsp;
							                        <a href="javascript:fnDay('11')">11</a>&nbsp;
							                        <a href="javascript:fnDay('12')">12</a>&nbsp;
							                        <a href="javascript:fnDay('13')">13</a>&nbsp;
							                        <a href="javascript:fnDay('14')">14</a>&nbsp;
							                        <a href="javascript:fnDay('15')">15</a>&nbsp;
							                        <a href="javascript:fnDay('16')">16</a>&nbsp;
							                        <a href="javascript:fnDay('17')">17</a>&nbsp;
							                        <a href="javascript:fnDay('18')">18</a>&nbsp;
							                        <a href="javascript:fnDay('19')">19</a>&nbsp;
							                        <a href="javascript:fnDay('20')">20</a>&nbsp;
							                        <a href="javascript:fnDay('21')">21</a>&nbsp;
							                        <a href="javascript:fnDay('22')">22</a>&nbsp;
							                        <a href="javascript:fnDay('23')">23</a>&nbsp;
							                        <a href="javascript:fnDay('24')">24</a>&nbsp;
							                        <a href="javascript:fnDay('25')">25</a>&nbsp;
							                        <a href="javascript:fnDay('26')">26</a>&nbsp;
							                        <a href="javascript:fnDay('27')">27</a>&nbsp;
							                        <a href="javascript:fnDay('28')">28</a>&nbsp;
							                        <a href="javascript:fnDay('29')">29</a>&nbsp;
							                        <a href="javascript:fnDay('30')">30</a>&nbsp;	
							                </asp:TableCell></asp:TableRow></asp:table>
								
							
						
							</asp:Panel>
							</table>
						<br>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td vAlign="bottom">
									<table class="TabStyle" borderColor="black" cellSpacing="0" border="0">
										<tr>
											<td class="Text_bold_White" height="23">&nbsp;&nbsp;&nbsp;Visitor 
												Details&nbsp;&nbsp;&nbsp;
											</td>
										</tr>
									</table>
								</td>
								<td id="hidenav" noWrap align="right" runat="server"><table>
										<tr class=normal1>
											<td><asp:label id="lblNext" runat="server" cssclass="Text_bold">Next:</asp:label></td>
											<td class="normal1"><asp:linkbutton id="lnk2" runat="server">2</asp:linkbutton></td>
											<td class="normal1"><asp:linkbutton id="lnk3" runat="server">3</asp:linkbutton></td>
											<td class="normal1"><asp:linkbutton id="lnk4" runat="server">4</asp:linkbutton></td>
											<td class="normal1"><asp:linkbutton id="lnk5" runat="server">5</asp:linkbutton></td>
											<td><asp:linkbutton id="lnkFirst" runat="server"><div class="LinkArrow"><<</div>
												</asp:linkbutton></td>
											<td><asp:linkbutton id="lnkPrevious" runat="server"><div class="LinkArrow"><</div>
												</asp:linkbutton></td>
											<td class="normal1"><asp:label id="lblPage" runat="server">Page</asp:label></td>
											<td><asp:textbox id="txtCurrrentPage" runat="server" Width="28px" CssClass="signup" AutoPostBack="True"
													MaxLength="5"></asp:textbox></td>
											<td class="normal1"><asp:label id="lblOf" runat="server">of</asp:label></td>
											<td class="normal1"><asp:label id="lblTotal" runat="server"></asp:label></td>
											<td><asp:linkbutton id="lnkNext" runat="server" CssClass="LinkArrow"><div class="LinkArrow">></div>
												</asp:linkbutton></td>
											<td><asp:linkbutton id="lnkLast" runat="server"><div class="LinkArrow">>></div>
												</asp:linkbutton></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<asp:table id="Table1" Width="100%" Height="350" Runat="server" CellPadding="0" CellSpacing="0"
							BorderWidth="1" BorderColor="black" GridLines="None">
							<asp:TableRow>
								<asp:TableCell VerticalAlign="Top">
									<asp:datagrid id="dgReferringPage" runat="server" Width="100%" CssClass="dg" BorderColor="white"
										AutoGenerateColumns="false" AllowSorting="True">
										<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
										<ItemStyle CssClass="is"></ItemStyle>
										<HeaderStyle CssClass="hs"></HeaderStyle>
										<Columns>
											<asp:BoundColumn Visible="False" DataField="numTrackingID"></asp:BoundColumn>
											<asp:ButtonColumn DataTextField="vcUserHostAddress" SortExpression="vcUserHostAddress" HeaderText="<font color=white>User Host Address</font>"></asp:ButtonColumn>
											<asp:BoundColumn DataField="vcUserDomain" SortExpression="vcUserDomain" HeaderText="<font color=white>Domain</font>"></asp:BoundColumn>
											<asp:BoundColumn DataField="vcCountry" SortExpression="vcCountry" HeaderText="<font color=white>Country</font>"></asp:BoundColumn>
											<asp:HyperLinkColumn Target="_blank"  DataNavigateUrlField="vcURL" SortExpression="vcURL" DataTextField="URL" HeaderText="<font color=white>URL</font>"></asp:HyperLinkColumn>
											<asp:HyperLinkColumn Target="_blank" DataNavigateUrlField="vcReferrer" SortExpression="vcReferrer" DataTextField="Referrer" HeaderText="<font color=white>Referrence</font>"></asp:HyperLinkColumn>
											<asp:BoundColumn DataField="vcSearchTerm" SortExpression="vcSearchTerm" HeaderText="<font color=white>KeyWords</font>"></asp:BoundColumn>
											<asp:BoundColumn DataField="numVistedPages" SortExpression="numVistedPages" HeaderText="<font color=white>No of Pages<br> Visited</font>"></asp:BoundColumn>
											<asp:BoundColumn DataField="vcTotalTime" SortExpression="vcTotalTime" HeaderText="<font color=white>Total Time</font>"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="vcURL"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="vcReferrer"></asp:BoundColumn>
										</Columns>
									</asp:datagrid>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table>
						<asp:Panel ID="pnlYear" runat="server" Visible="false" >
							<asp:table id="Table9"  BorderColor="black" CssClass="aspTable"
				                GridLines="None">
				                <asp:TableRow>
					                <asp:TableCell VerticalAlign="Top">
						                <asp:table runat="server" id="tblData" Width="75%"  Height="10px" CellPadding="1" CellSpacing="1" 
							                BorderWidth="1px" HorizontalAlign="Center"  >
							                <asp:TableRow Height="10px" ID="row1"  >                
							                </asp:TableRow>
							                <asp:TableRow   Height="10px" ID="TableRow1" CssClass = "text" >
							                <asp:TableCell ID="t0" runat="server">
							                Monthly Average: Yr: 
							                <asp:DropDownList ID="ddlYear" runat="server" cssclass="normal1" AutoPostBack="true">							
							                <asp:ListItem Text="--Select One--" Value="0"></asp:ListItem>
							                </asp:DropDownList></asp:TableCell>  
							                <asp:TableCell ID="t1" Text="Jan" runat="server"></asp:TableCell>              
							                <asp:TableCell ID="t2" Text="Feb" runat="server"></asp:TableCell>              
							                <asp:TableCell ID="t3" Text="Mar" runat="server"></asp:TableCell>              
							                <asp:TableCell ID="t4" Text="Apr" runat="server"></asp:TableCell>              
							                <asp:TableCell ID="t5" Text="May" runat="server"></asp:TableCell>              
							                <asp:TableCell ID="t6" Text="Jun" runat="server"></asp:TableCell>              
							                <asp:TableCell ID="t7" Text="Jul" runat="server"></asp:TableCell>              
							                <asp:TableCell ID="t8" Text="Aug" runat="server"></asp:TableCell>              
							                <asp:TableCell ID="t9" Text="Sep" runat="server"></asp:TableCell>              
							                <asp:TableCell ID="t10" Text="Oct" runat="server"></asp:TableCell>              
							                <asp:TableCell ID="t11" Text="Nov" runat="server"></asp:TableCell>              
							                <asp:TableCell ID="t12" Text="Dec" runat="server"></asp:TableCell>              
							                </asp:TableRow>
							                
						                </asp:table>
					                </asp:TableCell>
				                </asp:TableRow>
			                </asp:table>
			                </asp:Panel>
						<asp:button id="btnWeek" style="DISPLAY: none" Runat="server" />
						<asp:button id="btnday" style="DISPLAY: none" Runat="server" />
						<asp:textbox id="txtTotalPage" style="DISPLAY: none" Runat="server"></asp:textbox><asp:textbox id="txtTotalRecords" style="DISPLAY: none" Runat="server"></asp:textbox><asp:textbox id="txtSortChar" style="DISPLAY: none" Runat="server"></asp:textbox></td>
						<asp:textbox id="txtWeek"   style="DISPLAY: none"  Runat="server"></asp:textbox><asp:textbox id="txtDay"   style="DISPLAY: none"  Runat="server"></asp:textbox><asp:Label  runat="server" ID="l1" style="DISPLAY: none"></asp:Label></tr>
			</table>
			
		</form>
	</body>
</HTML>
