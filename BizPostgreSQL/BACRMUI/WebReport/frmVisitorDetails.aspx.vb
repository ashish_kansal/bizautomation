Imports BACRM.BusinessLogic.Tracking
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.webReports
    Partial Class frmVisitorDetails : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then

                    Dim objBusinessClass As New BusinessClass
                    Dim dtTable As DataTable
                    objBusinessClass.TrackingID = GetQueryStringVal("TrackID")
                    objBusinessClass.DomainID = Session("DomainId")
                    dtTable = objBusinessClass.GetDetailsOfVisitor(ConfigurationManager.AppSettings("ConnectionString"))
                    If dtTable.Rows.Count > 0 Then
                        lblHostAdd.Text = dtTable.Rows(0).Item("vcUserHostAddress")
                        lblDomain.Text = dtTable.Rows(0).Item("vcUserDomain")
                        lblUserAgent.Text = dtTable.Rows(0).Item("vcUserAgent")
                        lblBrowser.Text = dtTable.Rows(0).Item("vcBrowser")
                        lblCrawler.Text = dtTable.Rows(0).Item("vcCrawler")
                        hplURL.Text = dtTable.Rows(0).Item("vcURL")
                        hplURL.Attributes.Add("onclick", "return openSite('" & hplURL.Text & "')")
                        hplReferrer.Text = dtTable.Rows(0).Item("vcReferrer")
                        hplReferrer.Attributes.Add("onclick", "return openSite('" & hplReferrer.Text & "')")
                        hplOrgURL.Text = dtTable.Rows(0).Item("vcOrginalURL")
                        hplOrgURL.Attributes.Add("onclick", "return openSite('" & hplOrgURL.Text & "')")
                        hplOrgref.Text = dtTable.Rows(0).Item("vcOrginalRef")
                        hplOrgref.Attributes.Add("onclick", "return openSite('" & hplOrgref.Text & "')")
                        lblNoofpages.Text = dtTable.Rows(0).Item("numVistedPages")
                        lblTotalTime.Text = dtTable.Rows(0).Item("vcTotalTime")
                        lblDateVisited.Text = Format(dtTable.Rows(0).Item("dtCreated"), "MM/dd/yyyy hh:mm tt")
                        dgDetails.DataSource = dtTable
                        dgDetails.DataBind()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDetails.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim lnk As LinkButton
                    lnk = e.Item.FindControl("l1")
                    lnk.Attributes.Add("onclick", "return openSite('" & lnk.Text & "')")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace