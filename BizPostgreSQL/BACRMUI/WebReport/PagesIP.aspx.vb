Imports BACRM.BusinessLogic.Tracking
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.webReports
    Partial Public Class PagesIP : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                BindGrid()

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindGrid()
            Try
                Dim dtTable As DataTable
                Dim objBusinessClass As New BusinessClass

                Dim strPage As String = GetQueryStringVal("PageId")
                If Not strPage Is Nothing Then
                    lblPage.Text = strPage
                    Dim fromDate As String = GetQueryStringVal("FromDate")
                    Dim ToDate As String = GetQueryStringVal("ToDate")
                    With objBusinessClass
                        .FromDate = CDate(fromDate)
                        .ToDate = DateAdd(DateInterval.Day, 1, CDate(ToDate))
                        .DomainID = Session("DomainId")
                        .SearchTerm = strPage
                        .ClientZoneOffset = Session("ClientMachineUTCTimeOffset")
                        dtTable = .GetPageIps(ConfigurationManager.AppSettings("ConnectionString"))
                    End With
                    dgIP.DataSource = dtTable
                    dgIP.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace