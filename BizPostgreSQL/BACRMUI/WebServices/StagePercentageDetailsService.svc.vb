﻿Imports System.ServiceModel
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Web
Imports BACRM.BusinessLogic.Common
Imports Newtonsoft.Json

<ServiceContract(Namespace:="StagePercentageDetailsService")>
<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)>
Public Class StagePercentageDetailsService

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetMilestonesByProcess")>
    Public Function GetMilestonesByProcess(ByVal processID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetails As New StagePercentageDetails
                objStagePercentageDetails.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetails.ProcessID = processID
                Return JsonConvert.SerializeObject(objStagePercentageDetails.GetMilestonesByProcess(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)(ex.Message, Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetStagesByProcess")>
    Public Function GetStagesByProcess(ByVal processID As Long, ByVal milestoneName As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetails As New StagePercentageDetails
                objStagePercentageDetails.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetails.ProcessID = processID
                objStagePercentageDetails.MileStoneName = milestoneName
                Return JsonConvert.SerializeObject(objStagePercentageDetails.GetStagesByProcess(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)(ex.Message, Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/ChangeBusinessProcess")>
    Public Sub ChangeBusinessProcess(ByVal processType As Short, ByVal recordID As Long, ByVal processID As Long)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetails As New StagePercentageDetails
                objStagePercentageDetails.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetails.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objStagePercentageDetails.ProcessType = processType
                objStagePercentageDetails.RecordID = recordID
                objStagePercentageDetails.ProcessID = processID
                objStagePercentageDetails.ChangeBusinessProcess()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("TASK_IS_ALREADY_STARTED") Then
                Throw New WebFaultException(Of String)("Can't chnage business process after task is started.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)(ex.Message, Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetCapacityPlanningTasksReportData")>
    Public Function GetCapacityPlanningTasksReportData(ByVal processType As Short, ByVal processID As Long, ByVal milestoneName As String, ByVal stageDetailsID As Long, ByVal taskIds As String, ByVal gradeIds As String, ByVal fromDate As DateTime, ByVal toDate As DateTime, ByVal view As Short) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetails As New StagePercentageDetails
                objStagePercentageDetails.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetails.ProcessType = processType
                objStagePercentageDetails.ProcessID = processID
                objStagePercentageDetails.MileStoneName = milestoneName
                objStagePercentageDetails.StageDetailsID = stageDetailsID
                objStagePercentageDetails.SelectedTasks = taskIds
                objStagePercentageDetails.SelectedGrades = gradeIds
                objStagePercentageDetails.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Dim ds As DataSet = objStagePercentageDetails.GetCapacityPlanningReportData(fromDate, toDate, view)

                Return JsonConvert.SerializeObject(New With {Key .Milestones = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .Stages = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None), Key .Tasks = JsonConvert.SerializeObject(ds.Tables(2), Formatting.None), Key .ReportData = JsonConvert.SerializeObject(ds.Tables(3), Formatting.None), Key .Employees = JsonConvert.SerializeObject(If(ds.Tables.Count > 4, ds.Tables(4), ""), Formatting.None), Key .Teams = JsonConvert.SerializeObject(If(ds.Tables.Count > 5, ds.Tables(5), ""), Formatting.None), Key .Grades = JsonConvert.SerializeObject(If(ds.Tables.Count > 6, ds.Tables(6), ""), Formatting.None)}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)(ex.Message, Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetCapacityPlanningResourceReportData")>
    Public Function GetCapacityPlanningResourceReportData(ByVal teams As String, ByVal employees As String, ByVal processType As Short, ByVal processID As Long, ByVal milestoneName As String, ByVal stageDetailsID As Long, ByVal taskIds As String, ByVal fromDate As DateTime, ByVal toDate As DateTime, ByVal view As Short) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetails As New StagePercentageDetails
                objStagePercentageDetails.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetails.SelectedTeams = teams
                objStagePercentageDetails.SelectedEmployees = employees
                objStagePercentageDetails.ProcessType = processType
                objStagePercentageDetails.ProcessID = processID
                objStagePercentageDetails.MileStoneName = milestoneName
                objStagePercentageDetails.StageDetailsID = stageDetailsID
                objStagePercentageDetails.SelectedTasks = taskIds
                objStagePercentageDetails.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Dim ds As DataSet = objStagePercentageDetails.GetCapacityPlanningResourceReportData(fromDate, toDate, view)

                Return JsonConvert.SerializeObject(New With {Key .Teams = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .Employees = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None), Key .ReportData = JsonConvert.SerializeObject(ds.Tables(2), Formatting.None)}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)(ex.Message, Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetPerformanceEvaluationReportData")>
    Public Function GetPerformanceEvaluationReportData(ByVal processType As Short, ByVal processID As Long, ByVal milestoneName As String, ByVal stageDetailsID As Long, ByVal taskIds As String, ByVal teamIds As String, ByVal employeeIds As String, ByVal fromDate As DateTime, ByVal toDate As DateTime) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetails As New StagePercentageDetails
                objStagePercentageDetails.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetails.ProcessType = processType
                objStagePercentageDetails.ProcessID = processID
                objStagePercentageDetails.MileStoneName = milestoneName
                objStagePercentageDetails.StageDetailsID = stageDetailsID
                objStagePercentageDetails.SelectedTasks = taskIds
                objStagePercentageDetails.SelectedTeams = teamIds
                objStagePercentageDetails.SelectedEmployees = employeeIds
                objStagePercentageDetails.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Dim ds As DataSet = objStagePercentageDetails.GetPerformanceEvaluationReportData(fromDate, toDate)

                Return JsonConvert.SerializeObject(New With {Key .Milestones = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .Stages = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None), Key .Tasks = JsonConvert.SerializeObject(ds.Tables(2), Formatting.None), Key .ReportData = JsonConvert.SerializeObject(ds.Tables(3), Formatting.None), Key .Employees = JsonConvert.SerializeObject(ds.Tables(4), Formatting.None), Key .TotalWorkOrders = CCommon.ToLong(ds.Tables(5).Rows(0)(0))}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)(ex.Message, Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetProductionPlanningReportData")>
    Public Function GetProductionPlanningReportData(ByVal processType As Short, ByVal fromDate As DateTime, ByVal toDate As DateTime, ByVal pageIndex As Integer, ByVal pageSize As Integer) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetails As New StagePercentageDetails
                objStagePercentageDetails.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetails.ProcessType = processType
                objStagePercentageDetails.PageIndex = pageIndex
                objStagePercentageDetails.PageSize = pageSize
                objStagePercentageDetails.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Dim ds As DataSet = objStagePercentageDetails.GetProductionPlanningReportData(fromDate, toDate)

                Return JsonConvert.SerializeObject(New With {Key .data = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .collections = New With {Key .links = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None)}}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)(ex.Message, Net.HttpStatusCode.BadRequest)
        End Try
    End Function
End Class
