﻿Imports System.ServiceModel
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Web
Imports BACRM.BusinessLogic.Common
Imports Newtonsoft.Json
Imports BACRM.BusinessLogic.Opportunities

<ServiceContract(Namespace:="StagePercentageDetailsTaskTimeLogService")>
<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)>
Public Class StagePercentageDetailsTaskTimeLogService

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/Save")>
    Public Sub Save(ByVal id As Long, ByVal taskID As Long, ByVal actionType As Short, ByVal actionTime As DateTime, ByVal processedQty As Double, ByVal reasonForPause As Long, ByVal notes As String, ByVal isManualEntry As Boolean)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetailsTaskTimeLog As New StagePercentageDetailsTaskTimeLog
                objStagePercentageDetailsTaskTimeLog.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetailsTaskTimeLog.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objStagePercentageDetailsTaskTimeLog.ID = id
                objStagePercentageDetailsTaskTimeLog.TaskID = taskID
                objStagePercentageDetailsTaskTimeLog.ActionType = actionType
                objStagePercentageDetailsTaskTimeLog.ActionTime = actionTime
                objStagePercentageDetailsTaskTimeLog.ProcessedQty = processedQty
                objStagePercentageDetailsTaskTimeLog.ReasonForPause = reasonForPause
                objStagePercentageDetailsTaskTimeLog.Notes = notes
                Dim workOrderID As Long = objStagePercentageDetailsTaskTimeLog.Save()

                If workOrderID > 0 Then
                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        Dim objWorkOrder As New WorkOrder
                        objWorkOrder.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objWorkOrder.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                        objWorkOrder.WorkOrderID = workOrderID
                        Dim dt As DataTable = objWorkOrder.UpdateQtyBuilt()

                        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                            Dim itemCode As Long = CCommon.ToLong(dt.Rows(0)("numItemCode"))
                            Dim assetChartAcntId As Long = CCommon.ToLong(dt.Rows(0)("numAssetChartAcntId"))
                            Dim avgCost As Double = CCommon.ToDecimal(dt.Rows(0)("monAverageCost"))
                            Dim quantityBuilt As Double = CCommon.ToDouble(dt.Rows(0)("numQtyBuilt"))

                            Dim lngWIPAccount As Long = BACRM.BusinessLogic.Accounting.ChartOfAccounting.GetDefaultAccount("WP", HttpContext.Current.Session("DomainID"))

                            If assetChartAcntId > 0 AndAlso lngWIPAccount > 0 AndAlso quantityBuilt > 0 Then
                                Dim objItems As New BACRM.BusinessLogic.Item.CItems
                                objItems.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                objItems.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                                objItems.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                                objItems.ItemCode = itemCode

                                Dim journalID As Long = objItems.SaveJournalHeader(quantityBuilt)
                                objItems.SaveDataToGeneralJournalDetails(journalID, assetChartAcntId, lngWIPAccount, quantityBuilt, avgCost, 1, itemCode)
                            End If
                        End If

                        objTransactionScope.Complete()
                    End Using
                End If
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("TASK_DOES_NOT_EXISTS") Then
                Throw New WebFaultException(Of String)("Task doesn't exists.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("TASK_IS_MARKED_AS_FINISHED") Then
                Throw New WebFaultException(Of String)("Change in time entries are not allowed after task is finished.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("TASK_IS_ALREADY_STARTED") Then
                Throw New WebFaultException(Of String)("Task is already started.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetTimeEntries")>
    Public Function GetTimeEntries(ByVal taskID As Long, Optional ByVal bitForProject As Boolean = False) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetailsTaskTimeLog As New StagePercentageDetailsTaskTimeLog
                objStagePercentageDetailsTaskTimeLog.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetailsTaskTimeLog.TaskID = taskID
                objStagePercentageDetailsTaskTimeLog.bitForProject = bitForProject
                objStagePercentageDetailsTaskTimeLog.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Dim dt As DataTable = objStagePercentageDetailsTaskTimeLog.GetByTaskID()

                Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(dt, Formatting.None), Key .TotalTimeSpendOnTask = objStagePercentageDetailsTaskTimeLog.TotalTimeSpendOnTask}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("TASK_DOES_NOT_EXISTS") Then
                Throw New WebFaultException(Of String)("Task doesn't exists.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/Delete")>
    Public Sub Delete(ByVal id As Long, ByVal taskID As Long)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetailsTaskTimeLog As New StagePercentageDetailsTaskTimeLog
                objStagePercentageDetailsTaskTimeLog.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetailsTaskTimeLog.ID = id
                objStagePercentageDetailsTaskTimeLog.TaskID = taskID
                objStagePercentageDetailsTaskTimeLog.Delete()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("TASK_IS_MARKED_AS_FINISHED") Then
                Throw New WebFaultException(Of String)("Change in time entries are not allowed after task is finished.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Sub

End Class
