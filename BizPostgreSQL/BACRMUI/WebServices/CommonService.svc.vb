﻿Imports System.ServiceModel
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Web
Imports BACRM.BusinessLogic.Common
Imports Newtonsoft.Json
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities

<ServiceContract(Namespace:="CommonService")>
<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)>
Public Class CommonService

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetListDetails")>
    Public Function GetListDetails(ByVal listID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objCommon As New CCommon
                Return JsonConvert.SerializeObject(objCommon.GetMasterListItems(listID, CCommon.ToLong(HttpContext.Current.Session("DomainID"))), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetStates")>
    Public Function GetStates(ByVal countryID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objUserAccess As New UserAccess()
                objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objUserAccess.Country = countryID
                Return JsonConvert.SerializeObject(objUserAccess.SelState(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetAddresses")>
    Public Function GetAddresses(ByVal addressFor As Short, ByVal addressType As Short, ByVal recordID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objContact As New BACRM.BusinessLogic.Contacts.CContacts
                objContact.AddressType = addressType
                objContact.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objContact.RecordID = recordID
                objContact.AddresOf = addressFor
                objContact.byteMode = 2
                Return JsonConvert.SerializeObject(objContact.GetAddressDetail(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetShippingServices")>
    Public Function GetShippingServices(ByVal shipVia As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objShippingService As New ShippingService
                objShippingService.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objShippingService.ShipVia = shipVia
                Return JsonConvert.SerializeObject(objShippingService.GetByShipVia(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetEmployees")>
    Public Function GetEmployees() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objCommon As New CCommon
                objCommon.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                Return JsonConvert.SerializeObject(objCommon.GetEmployees(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetProjectedFinish")>
    Public Function GetProjectedFinish(ByVal workOrderID As Long)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objWorkOrder As New BACRM.BusinessLogic.Opportunities.WorkOrder
                objWorkOrder.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objWorkOrder.WorkOrderID = workOrderID
                objWorkOrder.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Return JsonConvert.SerializeObject(objWorkOrder.GetProjectedFinish(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetCapacityLoad")>
    Public Function GetCapacityLoad(ByVal workOrderID As Long)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objWorkOrder As New BACRM.BusinessLogic.Opportunities.WorkOrder
                objWorkOrder.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objWorkOrder.WorkOrderID = workOrderID
                objWorkOrder.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Return JsonConvert.SerializeObject(objWorkOrder.GetCapacityLoad(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetWorkOrderStatus")>
    Public Function GetWorkOrderStatus(ByVal workOrderIDs As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objWorkOrder As New BACRM.BusinessLogic.Opportunities.WorkOrder
                objWorkOrder.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objWorkOrder.Records = workOrderIDs

                Return JsonConvert.SerializeObject(objWorkOrder.GetWorkOrderStatus(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/SaveUserClockInOut")>
    Public Function SaveUserClockInOut()
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objTimeExp.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))

                Dim ApprovalTrans As New ApprovalConfig
                ApprovalTrans.UserId = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                If HttpContext.Current.Session("bitApprovalforTImeExpense") = "1" Then
                    If HttpContext.Current.Session("intTimeExpApprovalProcess") = "1" Then
                        objTimeExp.ApprovalStatus = 6
                        ApprovalTrans.chrAction = "CF"
                        ApprovalTrans.ApprovalStatus = 6
                    ElseIf HttpContext.Current.Session("intTimeExpApprovalProcess") = "2" Then
                        objTimeExp.ApprovalStatus = 1
                        ApprovalTrans.chrAction = "CH"
                        ApprovalTrans.ApprovalStatus = 1
                    End If
                    ApprovalTrans.strOutPut = "INPUT"
                    ApprovalTrans.UpdateApprovalTransaction()
                    If ApprovalTrans.strOutPut <> "VALID" Then
                        Throw New Exception("APPROVAL_PROCESS_NOT_CONFIGURED")
                    End If
                Else
                    objTimeExp.ApprovalStatus = 0
                End If

                HttpContext.Current.Session("IsUserClockedIn") = objTimeExp.SaveUserClockInOut()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetUserClockInOutDetail")>
    Public Function GetUserClockInOutDetail(ByVal fromDate As DateTime) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objTimeExp.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objTimeExp.FromDate = fromDate
                objTimeExp.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Return JsonConvert.SerializeObject(objTimeExp.GetUserClockInOutDetail(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetWorkOrderBOMForPick")>
    Public Function GetWorkOrderBOMForPick(ByVal workOrderIDs As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objWorkOrder As New BACRM.BusinessLogic.Opportunities.WorkOrder
                objWorkOrder.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objWorkOrder.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                objWorkOrder.Records = workOrderIDs

                Return JsonConvert.SerializeObject(objWorkOrder.GetBOMForPick(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetItemWarehouseLocations")>
    Public Function GetItemWarehouseLocations(ByVal itemCode As Long, ByVal warehouseID As Long, ByVal warehouseItemID As Long, ByVal vcWarehouseLocation As String, ByVal pageIndex As Integer, ByVal pageSize As Integer) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objItem As New CItems
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objItem.ItemCode = itemCode
                objItem.WarehouseID = warehouseID
                objItem.WareHouseItemID = warehouseItemID
                objItem.SearchText = vcWarehouseLocation
                objItem.CurrentPage = pageIndex
                objItem.PageSize = pageSize

                Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(objItem.GetWareHousesForPick(), Formatting.None), Key .TotalRecords = objItem.TotalRecords}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/UpdateWOPickedQuantity")>
    Public Function UpdateWOPickedQuantity(ByVal pickedItems As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim ds As New DataSet
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(pickedItems)
                ds.Tables.Add(dt)
                ds.AcceptChanges()

                Dim objWOrkOrder As New WorkOrder
                objWOrkOrder.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objWOrkOrder.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objWOrkOrder.Records = ds.GetXml()

                Return JsonConvert.SerializeObject(objWOrkOrder.PickItems(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("WORKORDER_COMPLETED") Then
                Throw New WebFaultException(Of String)("You are not allowed to change picked items after work order is finished.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Function

    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetInternalLocation")>
    Public Function GetInternalLocation(ByVal warehouseID As Long, ByVal searchText As String, ByVal pageIndex As Long, ByVal pageSize As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objWarehouseLocation As New WarehouseLocation
                objWarehouseLocation.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objWarehouseLocation.WarehouseID = warehouseID
                objWarehouseLocation.SearchText = searchText
                objWarehouseLocation.OffsetRecords = ((pageIndex - 1) * pageSize)
                objWarehouseLocation.PageSize = pageSize
                Dim dt As DataTable = objWarehouseLocation.SearchLocationByLocationName()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(dt, Formatting.None), Key .TotalRecords = CCommon.ToLong(dt.Rows(0)("TotalRecords"))}, Formatting.None)
                Else
                    Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(Nothing, Formatting.None), Key .TotalRecords = 0}, Formatting.None)
                End If
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("WORKORDER_COMPLETED") Then
                Throw New WebFaultException(Of String)("You are not allowed to change picked items after work order is finished.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Function

    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/UpdateSingleFieldValue")>
    Public Sub UpdateSingleFieldValue(ByVal mode As Integer, ByVal updateRecordID As Long, ByVal updateValueID As Long, ByVal comments As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objCommon As New CCommon
                objCommon.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objCommon.UpdateRecordID = updateRecordID
                objCommon.UpdateValueID = updateValueID
                objCommon.Mode = mode
                objCommon.Comments = comments
                objCommon.UpdateSingleFieldValue()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("WORKORDER_COMPLETED") Then
                Throw New WebFaultException(Of String)("You are not allowed to change picked items after work order is finished.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Sub

    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/SaveItemCurrencyPrice")>
    Public Function SaveItemCurrencyPrice(ByVal itemCode As Long, ByVal records As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim ds As New DataSet
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(records)
                ds.Tables.Add(dt)
                ds.AcceptChanges()

                Dim objItemCurrencyPrice As New ItemCurrencyPrice
                objItemCurrencyPrice.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objItemCurrencyPrice.ItemCode = itemCode
                objItemCurrencyPrice.Records = ds.GetXml()
                objItemCurrencyPrice.Save()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetPOForCostOptimizationAndMerge")>
    Public Function GetPOForCostOptimizationAndMerge(ByVal warehouseID As Long, ByVal isOnlyMergable As Boolean, ByVal isExcludePOSendToVendor As Boolean, ByVal isDisplayOnlyCostSaving As Boolean, ByVal costType As Short) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objOpportunity As New COpportunities
                objOpportunity.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objOpportunity.WarehouseID = warehouseID
                objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Dim ds As DataSet = objOpportunity.GetPOForCostOptimizationAndMerge(isOnlyMergable, isExcludePOSendToVendor, isDisplayOnlyCostSaving, costType)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    Return JsonConvert.SerializeObject(New With {Key .Vendors = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .PurchaseOrders = JsonConvert.SerializeObject(IIf(ds.Tables.Count > 1, ds.Tables(1), Nothing), Formatting.None), Key .Items = JsonConvert.SerializeObject(IIf(ds.Tables.Count > 2, ds.Tables(2), Nothing), Formatting.None)}, Formatting.None)
                Else
                    Return JsonConvert.SerializeObject(New With {Key .Vendors = JsonConvert.SerializeObject(Nothing, Formatting.None), Key .PurchaseOrders = JsonConvert.SerializeObject(Nothing, Formatting.None), Key .Items = JsonConvert.SerializeObject(Nothing, Formatting.None)}, Formatting.None)
                End If
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("WORKORDER_COMPLETED") Then
                Throw New WebFaultException(Of String)("You are not allowed to change picked items after work order is finished.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Function

    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/MergePOs")>
    Public Sub MergePOs(ByVal masterPO As Long, ByVal itemsToMerge As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim ds As New DataSet
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(itemsToMerge)
                ds.Tables.Add(dt)
                ds.AcceptChanges()

                Dim objOpp As New COpportunities
                objOpp.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objOpp.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objOpp.MergePurchaseOrders(masterPO, ds.GetXml())
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("MASTER_PO_DOES_NOT_EXISTS") Then
                Throw New WebFaultException(Of String)("Selected master purchase order does not exists.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("BIZ_DOC_EXISTS") Then
                Throw New WebFaultException(Of String)("Bizdoc(s) are added to some selected purchase order(s) needs to be merged with master purchase order.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Sub

    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/UpdateItemVendorCost")>
    Public Sub UpdateItemVendorCost(ByVal selectedRecords As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim ds As New DataSet
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(selectedRecords)
                ds.Tables.Add(dt)
                ds.AcceptChanges()

                Dim objItem As New CItems
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objItem.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objItem.UpdateItemVendorCost(ds.GetXml())
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Sub

    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/UpdatePurchaseOrderCost")>
    Public Sub UpdatePurchaseOrderCost(ByVal selectedRecords As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim ds As New DataSet
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(selectedRecords)
                ds.Tables.Add(dt)
                ds.AcceptChanges()

                Dim objOpp As New COpportunities
                objOpp.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objOpp.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objOpp.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                objOpp.UpdatePurchaseOrderCost(ds.GetXml())
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("BIZ_DOC_EXISTS") Then
                Throw New WebFaultException(Of String)("Not able to update cost as Bizdoc(s) are added to some selected purchase order(s).", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetWarehouseMapping")>
    Public Function GetWarehouseMapping(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal filterSources As String, ByVal filterCountries As String, ByVal filterStates As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objMassSalesFulfillmentWM As New MassSalesFulfillmentWarehouseMapping
                objMassSalesFulfillmentWM.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassSalesFulfillmentWM.PageIndex = pageIndex
                objMassSalesFulfillmentWM.PageSize = pageSize
                Dim dt As DataTable = objMassSalesFulfillmentWM.GetByDomain(filterSources, filterCountries, filterStates)

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(dt, Formatting.None), Key .TotalRecords = CCommon.ToLong(dt.Rows(0)("numTotalRecords"))}, Formatting.None)
                Else
                    Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(Nothing, Formatting.None), Key .TotalRecords = 0}, Formatting.None)
                End If
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/SaveWarehouseMapping")>
    Public Function SaveWarehouseMapping(ByVal orderSource As Long, ByVal sourceType As Short, ByVal country As Long, ByVal states As String, ByVal warehouses As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objMassSalesFulfillmentWM As New MassSalesFulfillmentWarehouseMapping
                objMassSalesFulfillmentWM.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassSalesFulfillmentWM.OrderSource = orderSource
                objMassSalesFulfillmentWM.SourceType = sourceType
                objMassSalesFulfillmentWM.CountryID = country
                objMassSalesFulfillmentWM.StateIDs = states
                objMassSalesFulfillmentWM.WarehousePriorities = warehouses
                objMassSalesFulfillmentWM.Save()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("DUPLICATE_STATE") Then
                Throw New WebFaultException(Of String)("Another record with same order source and state already exists.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/DeleteWarehouseMapping")>
    Public Function DeleteWarehouseMapping(ByVal id As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objMassSalesFulfillmentWM As New MassSalesFulfillmentWarehouseMapping
                objMassSalesFulfillmentWM.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassSalesFulfillmentWM.ID = id
                objMassSalesFulfillmentWM.Delete()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetOtherContactsFromOrganization")>
    Public Function GetOtherContactsFromOrganization(ByVal mode As Short, ByVal contactIds As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objCommon As New CCommon
                objCommon.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objCommon.Mode = mode
                objCommon.Str = contactIds
                Return objCommon.GetOtherContactsFromOrganization()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetTaskNotes")>
    Public Function GetTaskNotes(ByVal taskID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetailsTaskNotes As New StagePercentageDetailsTaskNotes
                objStagePercentageDetailsTaskNotes.TaskID = taskID
                Return JsonConvert.SerializeObject(objStagePercentageDetailsTaskNotes.GetByTaskID(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/SaveTaskNotes")>
    Public Function SaveTaskNotes(ByVal taskID As Long, ByVal notes As String, ByVal isDone As Boolean)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetailsTaskNotes As New StagePercentageDetailsTaskNotes
                objStagePercentageDetailsTaskNotes.TaskID = taskID
                objStagePercentageDetailsTaskNotes.Notes = notes
                objStagePercentageDetailsTaskNotes.IsDone = isDone
                objStagePercentageDetailsTaskNotes.Save()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetOrderExpenseItemsAddBill")>
    Public Function GetOrderExpenseItemsAddBill(ByVal oppID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objCommon As New CCommon
                objCommon.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objCommon.OppID = oppID
                Return JsonConvert.SerializeObject(objCommon.GetOrderExpenseItemsAddBill(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetMatchedPO")>
    Public Function GetMatchedPO(ByVal items As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim ds As New DataSet
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(items)
                ds.Tables.Add(dt)
                ds.AcceptChanges()

                Dim objItem As New CItems
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objItem.strItemCodes = ds.GetXml()
                Return JsonConvert.SerializeObject(objItem.GetMatchedPO(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/DisassociatePO")>
    Public Function DisassociatePO(ByVal oppID As Long, ByVal oppItemIds As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objOpportunity As New OppotunitiesIP
                objOpportunity.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objOpportunity.OpportunityId = oppID
                objOpportunity.DisassociatePO(oppItemIds)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("COMMISSION_PAID") Then
                Throw New WebFaultException(Of String)("You are not allowed to disassociate because commission paid for some item(s).", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetVendorDetail")>
    Public Function GetVendorDetail(ByVal vendorID As Long, ByVal itemID As Long, ByVal attrValues As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objAccounts As New BACRM.BusinessLogic.Account.CAccounts
                objAccounts.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objAccounts.DivisionID = vendorID
                Dim ds As DataSet = objAccounts.GetVendorDetail(itemID, attrValues)

                If Not ds Is Nothing Then
                    Return JsonConvert.SerializeObject(New With {Key .ItemDetails = If(ds.Tables.Count > 0, JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), JsonConvert.SerializeObject(Nothing, Formatting.None)), Key .VendorDetails = If(ds.Tables.Count > 1, JsonConvert.SerializeObject(ds.Tables(1), Formatting.None), JsonConvert.SerializeObject(Nothing, Formatting.None))}, Formatting.None)
                Else
                    Return JsonConvert.SerializeObject(New With {Key .ItemDetails = JsonConvert.SerializeObject(Nothing, Formatting.None), Key .VendorDetails = JsonConvert.SerializeObject(Nothing, Formatting.None)}, Formatting.None)
                End If
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetValidSRG")>
    Public Function GetValidSRG()
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objSRG As New BACRM.BusinessLogic.Reports.ScheduledReportsGroup
                objSRG.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objSRG.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Return JsonConvert.SerializeObject(objSRG.GetAll(1), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetSRG")>
    Public Function GetSRG(ByVal srgID As Long)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objSRG As New BACRM.BusinessLogic.Reports.ScheduledReportsGroup
                objSRG.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objSRG.ID = srgID
                objSRG.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Return JsonConvert.SerializeObject(objSRG.GetByID(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/SaveSRG")>
    Public Function SaveSRG(ByVal srgID As Long, ByVal name As String, ByVal frequency As Short, ByVal datetime As DateTime, ByVal emailTemplate As Long, ByVal selectedTokens As String, ByVal recipientsEmail As String, ByVal recipientsContactId As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objSRG As New BACRM.BusinessLogic.Reports.ScheduledReportsGroup
                objSRG.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objSRG.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objSRG.ID = srgID
                objSRG.Name = name
                objSRG.Frequency = frequency
                objSRG.StartDate = datetime
                objSRG.EmailTemplate = emailTemplate
                objSRG.SelectedTokens = selectedTokens
                objSRG.RecipientsEmail = recipientsEmail
                objSRG.RecipientsContactID = recipientsContactId
                objSRG.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Return JsonConvert.SerializeObject(objSRG.Save(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("MAX_20_REPORT_GROUPS") Then
                Throw New WebFaultException(Of String)("You can add max 20 scheduled reports.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/DeleteSRG")>
    Public Function DeleteSRG(ByVal srgID As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objSRG As New BACRM.BusinessLogic.Reports.ScheduledReportsGroup
                objSRG.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objSRG.SelectedRecords = srgID
                objSRG.Delete()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/TestSRG")>
    Public Function TestSRG(ByVal srgID As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objSRG As New BACRM.BusinessLogic.Reports.ScheduledReportsGroup
                objSRG.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objSRG.ID = srgID
                objSRG.EmailScheduledReportsGroup(True)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetSRGReports")>
    Public Function GetSRGReports(ByVal srgID As Long, ByVal mode As Short, ByVal reportType As Short, ByVal templateID As Long, ByVal searchText As String, ByVal currentPage As Integer, ByVal pageSize As Integer) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objSRGR As New BACRM.BusinessLogic.Reports.ScheduledReportsGroupReports
                objSRGR.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objSRGR.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                Dim dt As DataTable = objSRGR.GetSRGReports(mode, srgID, reportType, templateID, searchText, currentPage, pageSize)

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(dt, Formatting.None), Key .TotalRecords = CCommon.ToInteger(dt.Rows(0)("TotalRecords"))}, Formatting.None)
                Else
                    Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(Nothing, Formatting.None), Key .TotalRecords = 0}, Formatting.None)
                End If
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/SaveSRGReports")>
    Public Function SaveSRGReports(ByVal srgID As Long, ByVal reports As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim ds As New DataSet
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(reports)
                ds.Tables.Add(dt)
                ds.AcceptChanges()

                Dim objSRGR As New BACRM.BusinessLogic.Reports.ScheduledReportsGroupReports
                objSRGR.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objSRGR.SRGID = srgID
                objSRGR.SelectedReports = ds.GetXml()
                objSRGR.Save(1)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("MAX_5_REPORTS") Then
                Throw New WebFaultException(Of String)("You can add max 5 reports.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("REPORT_DOES_NOT_EXISTS") Then
                Throw New WebFaultException(Of String)("Report does not exist.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("REPORT_ALREADY_ADDED") Then
                Throw New WebFaultException(Of String)("Report is all ready added to selected scheduled reports group.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/AddToSRGReports")>
    Public Function AddToSRGReports(ByVal srgID As Long, ByVal reportID As Long)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objSRGR As New BACRM.BusinessLogic.Reports.ScheduledReportsGroupReports
                objSRGR.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objSRGR.SRGID = srgID
                objSRGR.SelectedReports = reportID
                objSRGR.Save(2)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("MAX_5_REPORTS") Then
                Throw New WebFaultException(Of String)("You can add max 5 reports.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/DeleteSRGReports")>
    Public Function DeleteSRGReports(ByVal srgID As Long, ByVal reports As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objSRGR As New BACRM.BusinessLogic.Reports.ScheduledReportsGroupReports
                objSRGR.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objSRGR.SRGID = srgID
                objSRGR.SelectedReports = reports
                objSRGR.Delete()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/UpdateRGReportSortOrder")>
    Public Function UpdateRGReportSortOrder(ByVal srgID As Long, ByVal srgrID As Long, ByVal sortOrder As Integer) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objSRGR As New BACRM.BusinessLogic.Reports.ScheduledReportsGroupReports
                objSRGR.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objSRGR.SRGID = srgID
                objSRGR.ID = srgrID
                objSRGR.SortOrder = sortOrder
                objSRGR.UpdateSortOrder()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetGenericDocument")>
    Public Function GetGenericDocument(ByVal docID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objDocuments As New BACRM.BusinessLogic.Documents.DocumentList
                objDocuments.GenDocID = docID
                objDocuments.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objDocuments.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objDocuments.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Return JsonConvert.SerializeObject(objDocuments.GetDocByGenDocID(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/RemoveActivity")>
    Public Function RemoveActivity(ByVal activityID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim ObjOutlook As New BACRM.BusinessLogic.Outlook.COutlook
                ObjOutlook.Status = -1
                ObjOutlook.ActivityID = activityID
                ObjOutlook.RemoveVarience()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/FinishCommunication")>
    Public Function FinishCommunication(ByVal commID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim ObjActionItem As New ActionItem
                ObjActionItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                ObjActionItem.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                ObjActionItem.CommID = commID
                ObjActionItem.FinishCommunication()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetMarketplaces")>
    Public Function GetMarketplaces() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objeChannelHub As New eChannelHub()
                objeChannelHub.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                Return JsonConvert.SerializeObject(objeChannelHub.GeteChannelHubHtml(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/SaveMarketplaceCustomer")>
    Public Function SaveMarketplaceCustomer(ByVal marketplaceID As Long, ByVal divisionID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objDomainMarketplace As New DomainMarketplace
                objDomainMarketplace.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objDomainMarketplace.MarketplaceID = marketplaceID
                objDomainMarketplace.DivisionID = divisionID
                objDomainMarketplace.Save()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetReorderPointConfiguration")>
    Public Function GetReorderPointConfiguration() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim basedOn As Short = 1
                Dim days As Integer = 30
                Dim percent As Integer = 0
                Dim objCommon As New CCommon
                objCommon.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                Dim dt As DataTable = objCommon.GetDomainSettingValue("tintReorderPointBasedOn")
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    basedOn = CCommon.ToShort(dt.Rows(0)("tintReorderPointBasedOn"))
                End If

                dt = objCommon.GetDomainSettingValue("intReorderPointDays")
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    days = CCommon.ToInteger(dt.Rows(0)("intReorderPointDays"))
                End If

                dt = objCommon.GetDomainSettingValue("intReorderPointPercent")
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    percent = CCommon.ToInteger(dt.Rows(0)("intReorderPointPercent"))
                End If

                Return JsonConvert.SerializeObject(New With {Key .BasedOn = basedOn, Key .Days = days, Key .Percent = percent}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetChildKitsOfKit")>
    Public Function GetChildKitsOfKit(ByVal numItemCode As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objItem As New CItems
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objItem.ItemCode = numItemCode
                Dim ds As DataSet = objItem.GetChildKitsOfKit()

                Return JsonConvert.SerializeObject(New With {Key .Table1 = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .Table2 = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None)}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetKitChildItems")>
    Public Function GetKitChildItems(ByVal numMainKitItemCode As Long, ByVal numChildKitItemCode As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objItem As New CItems
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objItem.ItemCode = numMainKitItemCode
                objItem.ChildItemID = numChildKitItemCode
                Return JsonConvert.SerializeObject(objItem.GetKitChildItems(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetDependedKitChilds")>
    Public Function GetDependedKitChilds(ByVal numMainKitItemCode As Long, ByVal numChildKitItemCode As Long, ByVal vcCurrentKitConfiguration As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objItem As New CItems
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objItem.ItemCode = numMainKitItemCode
                objItem.ChildItemID = numChildKitItemCode
                objItem.KitSelectedChild = vcCurrentKitConfiguration
                Dim ds As DataSet = objItem.GetDependedKitChilds()

                Return JsonConvert.SerializeObject(New With {Key .childKits = If(Not ds Is Nothing AndAlso ds.Tables.Count > 0, ds.Tables(0), Nothing), Key .childKitItems = If(Not ds Is Nothing AndAlso ds.Tables.Count > 1, ds.Tables(1), Nothing)}, Formatting.None)                
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetItemCalculatedPrice")>
    Public Function GetItemCalculatedPrice(ByVal itemCode As Long, ByVal qty As Double, ByVal vcCurrentKitConfiguration As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objItem As New CItems()
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objItem.DivisionID = 0
                objItem.Quantity = qty
                objItem.SiteID = 0
                objItem.ItemCode = itemCode
                objItem.KitSelectedChild = vcCurrentKitConfiguration
                Dim dtCalculatedPrice As DataTable = objItem.GetItemCalculatedPrice()

                Return JsonConvert.SerializeObject(New With {Key .price = CCommon.ToDouble(dtCalculatedPrice.Rows(0)("monPrice")), Key .msrp = CCommon.ToDouble(dtCalculatedPrice.Rows(0)("monMSRP"))}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/UpdateItemDepreciationRate")>
    Public Sub UpdateItemDepreciationRate(ByVal id As Long, ByVal rate As Double)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objItemDepreciation As New ItemDepreciation
                objItemDepreciation.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objItemDepreciation.ID = id
                objItemDepreciation.Rate = rate
                objItemDepreciation.UpdateRate()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("FY_CLOSED") Then
                Throw New WebFaultException(Of String)("You can't change rate once financial year is closed.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("CANT_EDIT_LAST_YEAR") Then
                Throw New WebFaultException(Of String)("You can't change rate of last year of depreciation.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/SavePersistTable")>
    Public Sub SavePersistTable(ByVal isMasterPage As Boolean, ByVal boolOnlyURL As Boolean, ByVal strParam As String, ByVal strPageName As String, ByVal values As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(values)

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim p As New Generic.List(Of PersistAttribute)

                    For Each dr As DataRow In dt.Rows
                        p.Add(New PersistAttribute(Convert.ToString(dr("Key")), Convert.ToString(dr("Value"))))
                    Next

                    Dim strFileFullPath As String = CCommon.GetDocumentPhysicalPath() & "Persistance_"
                    If Not isMasterPage Then
                        If boolOnlyURL Then
                            strFileFullPath = strFileFullPath & (HttpContext.Current.Request.Url.Segments(HttpContext.Current.Request.Url.Segments.Length - 1)).ToLower().GetHashCode()
                        ElseIf strPageName.Length > 0 Then
                            Dim strPageLink As String = HttpContext.Current.Request.Url.ToString()
                            Dim strPageURI As System.Uri

                            strPageLink = HttpContext.Current.Request.Url.ToString()
                            strPageLink = strPageLink.Replace("PersistTab", strPageName)

                            strPageURI = New Uri(strPageLink)
                            strFileFullPath = strFileFullPath & (strPageURI.Segments(strPageURI.Segments.Length - 1)).ToLower().GetHashCode()
                        Else
                            strFileFullPath = strFileFullPath & (HttpContext.Current.Request.Url.Segments(HttpContext.Current.Request.Url.Segments.Length - 1) & "_" & HttpContext.Current.Request.QueryString.ToString()).ToLower().GetHashCode()
                        End If
                    End If

                    If strParam.Length > 0 Then
                        strFileFullPath = strFileFullPath & "_" & strParam
                    End If

                    strFileFullPath = strFileFullPath & "_" & HttpContext.Current.Session("DomainID").ToString & "_" & HttpContext.Current.Session("UserContactID").ToString & ".xml"

                    If isMasterPage AndAlso strParam = "SimpleSearchType" Then
                        HttpContext.Current.Session("SimpleSearchType") = Convert.ToString(dt.Rows(0)("Value"))
                    End If

                    'Serialize object to a text file.
                    Dim objStreamWriter As New IO.StreamWriter(strFileFullPath)
                    Dim x As New System.Xml.Serialization.XmlSerializer(p.GetType)
                    x.Serialize(objStreamWriter, p)
                    objStreamWriter.Close()
                End If
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetPersistTable")>
    Public Function GetPersistTable(ByVal isMasterPage As Boolean, ByVal boolOnlyURL As Boolean, ByVal strParam As String, ByVal strPageName As String, ByVal values As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim json As String = "{"

                Dim strFileFullPath As String = CCommon.GetDocumentPhysicalPath() & "Persistance_"
                If Not isMasterPage Then
                    If boolOnlyURL Then
                        strFileFullPath = strFileFullPath & (HttpContext.Current.Request.Url.Segments(HttpContext.Current.Request.Url.Segments.Length - 1)).ToLower().GetHashCode()
                    ElseIf strPageName.Length > 0 Then
                        Dim strPageLink As String = HttpContext.Current.Request.Url.ToString()
                        Dim strPageURI As System.Uri

                        strPageLink = HttpContext.Current.Request.Url.ToString()
                        strPageLink = strPageLink.Replace("PersistTab", strPageName)

                        strPageURI = New Uri(strPageLink)
                        strFileFullPath = strFileFullPath & (strPageURI.Segments(strPageURI.Segments.Length - 1)).ToLower().GetHashCode()
                    Else
                        strFileFullPath = strFileFullPath & (HttpContext.Current.Request.Url.Segments(HttpContext.Current.Request.Url.Segments.Length - 1) & "_" & HttpContext.Current.Request.QueryString.ToString()).ToLower().GetHashCode()
                    End If
                End If

                If strParam.Length > 0 Then
                    strFileFullPath = strFileFullPath & "_" & strParam
                End If

                strFileFullPath = strFileFullPath & "_" & HttpContext.Current.Session("DomainID").ToString & "_" & HttpContext.Current.Session("UserContactID").ToString & ".xml"

                If System.IO.File.Exists(strFileFullPath) Then
                    Dim objStreamReader As New IO.StreamReader(strFileFullPath)
                    Dim p As New Generic.List(Of PersistAttribute)
                    Dim x As New System.Xml.Serialization.XmlSerializer(p.GetType)
                    p = CType(x.Deserialize(objStreamReader), Generic.List(Of PersistAttribute))
                    objStreamReader.Close()
                    'Dim PersistTable As New Hashtable
                    Dim Enumerator As Generic.IEnumerator(Of PersistAttribute) = p.GetEnumerator()

                    While Enumerator.MoveNext()
                        json = json & If(json.Length > 1, ",", "") & ("""" & Enumerator.Current.Key & """" & ":" & Enumerator.Current.Value)
                    End While
                End If

                json = json & "}"

                Return json
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/CreateNewItemFromKitConfiguration")>
    Public Function CreateNewItemFromKitConfiguration(ByVal itemCode As Long, ByVal vcCurrentKitConfiguration As String, ByVal isAssembly As Boolean, ByVal isKitParent As Boolean) As Long
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objItem As New BACRM.BusinessLogic.Item.CItems
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objItem.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objItem.ItemCode = itemCode
                objItem.KitSelectedChild = vcCurrentKitConfiguration
                objItem.Assembly = isAssembly
                objItem.KitParent = isKitParent
                Return objItem.CreateNewItemFromKitConfiguration()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("Item with name") Then
                Throw New WebFaultException(Of String)(ex.Message.Replace("P0001: ", ""), Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("ITEM_NAME_NOT_SELECTED") Then
                Throw New WebFaultException(Of String)("Item name is reauired.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("ITEM_TYPE_NOT_SELECTED") Then
                Throw New WebFaultException(Of String)("Item type is required.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("INVALID_ASSET_ACCOUNT") Then
                Throw New WebFaultException(Of String)("Asset account is required.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("INVALID_COGS_ACCOUNT") Then
                Throw New WebFaultException(Of String)("Cost of goods sold account is required.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("INVALID_INCOME_ACCOUNT") Then
                Throw New WebFaultException(Of String)("Income account is required.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("INVALID_EXPENSE_ACCOUNT") Then
                Throw New WebFaultException(Of String)("Expense account is required.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("ITEM_GROUP_NOT_SELECTED") Then
                Throw New WebFaultException(Of String)("Item group is required for matrix item.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("ITEM_ATTRIBUTES_NOT_SELECTED") Then
                Throw New WebFaultException(Of String)("Item attributes are required.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS") Then
                Throw New WebFaultException(Of String)("Item with same attributes already exist.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("ITEM_GROUP_NOT_CONFIGURED") Then
                Throw New WebFaultException(Of String)("Item gorup is not configured.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Function

#Region "Vendor LeadTime"
    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/VendorLeadTimes/GetByVendor")>
    Public Function VendorLeadTimesGetByVendor(ByVal vendorID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objVendorLeadTimes As New VendorLeadTimes
                objVendorLeadTimes.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objVendorLeadTimes.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objVendorLeadTimes.VendorID = vendorID
                Return objVendorLeadTimes.GetByVendor()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/VendorLeadTimes/GetByID")>
    Public Function VendorLeadTimesGetByID(ByVal vendorLeadTimesID As String, ByVal vendorID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objVendorLeadTimes As New VendorLeadTimes
                objVendorLeadTimes.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objVendorLeadTimes.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objVendorLeadTimes.VendorLeadTimesID = vendorLeadTimesID
                objVendorLeadTimes.VendorID = vendorID
                Return objVendorLeadTimes.GetByID()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/VendorLeadTimes/Save")>
    Public Sub VendorLeadTimesSave(ByVal vendorLeadTimesID As Long, ByVal vendorID As Long, ByVal data As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objVendorLeadTimes As New VendorLeadTimes
                objVendorLeadTimes.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objVendorLeadTimes.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objVendorLeadTimes.VendorLeadTimesID = vendorLeadTimesID
                objVendorLeadTimes.VendorID = vendorID
                objVendorLeadTimes.data = data
                objVendorLeadTimes.Save()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/VendorLeadTimes/Delete")>
    Public Sub VendorLeadTimesDelete(ByVal vendorLeadTimesIDs As String, ByVal vendorID As Long)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objVendorLeadTimes As New VendorLeadTimes
                objVendorLeadTimes.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objVendorLeadTimes.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objVendorLeadTimes.SelectedIDs = vendorLeadTimesIDs
                objVendorLeadTimes.VendorID = vendorID
                objVendorLeadTimes.Delete()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Sub
#End Region

#Region "Vendor Cost Table"
    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/VendorCostTable/GetByItemVendor")>
    Public Function VendorCostTableGetByItemVendor(ByVal vendorTcode As Long, ByVal vendorID As Long, ByVal itemCode As Long, ByVal currencyID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objVendorCostTable As New VendorCostTable
                objVendorCostTable.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objVendorCostTable.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objVendorCostTable.VendorTcode = vendorTcode
                objVendorCostTable.VendorID = vendorID
                objVendorCostTable.ItemCode = itemCode
                objVendorCostTable.CurrencyID = currencyID
                objVendorCostTable.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Return JsonConvert.SerializeObject(objVendorCostTable.GetByItemVendor(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/VendorCostTable/Save")>
    Public Sub VendorCostTableSave(ByVal vendorTcode As Long, ByVal vendorID As Long, ByVal itemCode As Long, ByVal currencyID As Long, ByVal costRange As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objVendorCostTable As New VendorCostTable
                objVendorCostTable.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objVendorCostTable.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objVendorCostTable.VendorTcode = vendorTcode
                objVendorCostTable.VendorID = vendorID
                objVendorCostTable.ItemCode = itemCode
                objVendorCostTable.CurrencyID = currencyID
                objVendorCostTable.Save(costRange)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/VendorCostTable/UpdateDefaultRange")>
    Public Sub VendorCostTableUpdateDefaultRange(ByVal costRange As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objVendorCostTable As New VendorCostTable
                objVendorCostTable.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objVendorCostTable.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objVendorCostTable.UpdateDefaultRange(costRange)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/VendorCostTable/UpdateCost")>
    Public Sub VendorCostTableUpdateCost(ByVal vendorTcode As Long, ByVal vendorID As Long, ByVal itemCode As Long, ByVal vendorCostTableID As Long, ByVal staticCost As Decimal, ByVal dynamicCost As Decimal)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objVendorCostTable As New VendorCostTable
                objVendorCostTable.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objVendorCostTable.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objVendorCostTable.VendorTcode = vendorTcode
                objVendorCostTable.VendorID = vendorID
                objVendorCostTable.ItemCode = itemCode
                objVendorCostTable.VendorCostTableID = vendorCostTableID
                objVendorCostTable.StaticCost = staticCost
                objVendorCostTable.DynamicCost = dynamicCost
                objVendorCostTable.UpdateCost()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/VendorCostTable/UpdateStaticCost")>
    Public Sub VendorCostTableUpdateStaticCost(ByVal itemGroups As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objVendorCostTable As New VendorCostTable
                objVendorCostTable.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objVendorCostTable.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objVendorCostTable.UpdateStaticCost(itemGroups)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/Orders/UpdateOrderedItemQuantity")>
    Public Sub UpdateOrderedItemQuantity(ByVal mode As Integer, ByVal oppID As Long, ByVal oppItemID As Long, ByVal units As Decimal, ByVal price As Decimal, ByVal notes As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objOpportunity As New MOpportunity
                objOpportunity.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objOpportunity.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objOpportunity.OpportunityId = oppID
                objOpportunity.OppItemCode = oppItemID
                objOpportunity.Mode = mode
                objOpportunity.Units = units
                objOpportunity.UnitPrice = price
                objOpportunity.Notes = notes
                objOpportunity.UpdateOrderedItemQuantity()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                Throw New WebFaultException(Of String)("Selected warehouse is not available for all assembly/kit child items.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY") Then
                Throw New WebFaultException(Of String)("Item quantity can not be less than shipped quantity.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER") Then
                Throw New WebFaultException(Of String)("Item quantity can not be greater than work order quantity.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY") Then
                Throw New WebFaultException(Of String)("Item quantity can not be less than received quantity.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("INVALID_ITEM_WAREHOUSES") Then
                Throw New WebFaultException(Of String)("Invalid warehouse.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY") Then
                Throw New WebFaultException(Of String)("Item quantity can not be less than invoiced/billed quantity.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_SHIPPED_QTY") Then
                Throw New WebFaultException(Of String)("Item quantity can not be less than shipped quantity.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY") Then
                Throw New WebFaultException(Of String)("Item quantity can not be less than packed quantity.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Sub
#End Region
End Class
