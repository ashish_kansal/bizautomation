﻿Imports System.ServiceModel
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Web
Imports BACRM.BusinessLogic.Common
Imports Newtonsoft.Json

<ServiceContract(Namespace:="StagePercentageDetailsTaskService")>
<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)>
Public Class StagePercentageDetailsTaskService

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetTasksByStage")>
    Public Function GetTasksByStage(ByVal stageDetailsID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetailsTask As New StagePercentageDetailsTask
                objStagePercentageDetailsTask.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetailsTask.StageDetailsID = stageDetailsID
                Return JsonConvert.SerializeObject(objStagePercentageDetailsTask.GetByStage(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/ChangeAssignee")>
    Public Sub ChangeAssignee(ByVal taskID As Long, ByVal assignedTo As Long)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetailsTask As New StagePercentageDetailsTask
                objStagePercentageDetailsTask.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetailsTask.TaskID = taskID
                objStagePercentageDetailsTask.AssignedTo = assignedTo
                objStagePercentageDetailsTask.ChangeAssignee()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("TASK_IS_MARKED_AS_FINISHED") Then
                Throw New WebFaultException(Of String)("Can't chnage assignee after task is finished.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("TASK_IS_ALREADY_STARTED") Then
                Throw New WebFaultException(Of String)("Can't chnage assignee after task is started.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/ChangeTime")>
    Public Sub ChangeTime(ByVal taskID As Long, ByVal hours As Long, ByVal minutes As Long, ByVal isMasterUpdateAlso As Boolean)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetailsTask As New StagePercentageDetailsTask
                objStagePercentageDetailsTask.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetailsTask.TaskID = taskID
                objStagePercentageDetailsTask.TaskHours = hours
                objStagePercentageDetailsTask.TaskMinutes = minutes
                objStagePercentageDetailsTask.ChangeTime(isMasterUpdateAlso)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("TASK_IS_MARKED_AS_FINISHED") Then
                Throw New WebFaultException(Of String)("Can't chnage assignee after task is finished.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("TASK_IS_ALREADY_STARTED") Then
                Throw New WebFaultException(Of String)("Can't chnage assignee after task is started.", Net.HttpStatusCode.BadRequest)
            ElseIf ex.Message.Contains("MORE_TIME_SPEND_ON_TASK") Then
                Throw New WebFaultException(Of String)("More time is already spend on task than provided task time.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/ChangeTimeProductionPlanning")>
    Public Sub ChangeTimeProductionPlanning(ByVal taskID As Long, ByVal startDate As DateTime, ByVal endDate As DateTime)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetailsTask As New StagePercentageDetailsTask
                objStagePercentageDetailsTask.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetailsTask.TaskID = taskID
                objStagePercentageDetailsTask.StartTime = startDate
                objStagePercentageDetailsTask.EndTime = endDate
                objStagePercentageDetailsTask.ChangeTimeProductionPlanning()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("INVALID_START_AND_END_TIME") Then
                Throw New WebFaultException(Of String)("Invalid task start and end time.", Net.HttpStatusCode.BadRequest)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
            End If
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/Close")>
    Public Sub Close(ByVal taskID As Long)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objStagePercentageDetailsTask As New StagePercentageDetailsTask
                objStagePercentageDetailsTask.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objStagePercentageDetailsTask.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objStagePercentageDetailsTask.TaskID = taskID
                objStagePercentageDetailsTask.Close()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Throw New WebFaultException(Of String)("Unknown error occurred.", Net.HttpStatusCode.BadRequest)
        End Try
    End Sub
End Class
