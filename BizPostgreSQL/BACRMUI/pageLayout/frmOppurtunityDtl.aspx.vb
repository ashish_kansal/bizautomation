'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  18/2/2005
'***************************************************************************************************************************
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Reflection
Imports Infragistics.WebUI.UltraWebTab

Namespace BACRM.UserInterface.Opportunities
    Public Class frmOppurtunityDtl : Inherits BACRMPage

        Dim dsTemp As DataSet
        Dim ds As DataSet
        Dim objOpportunity As MOpportunity
        Dim objPageLayout As CPageLayout
        Dim objCommon As New CCommon
        Dim objContacts As CContacts
        Dim dtAssignTo As DataTable
        Dim dtSalesProcess As DataTable
        Dim lngDivId As Long
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Dim m_aryRightsForAssContacts(), m_aryRightsForCustFlds(), m_aryRightsForItems(), m_aryRightsForPage() As Integer
        Dim myRow As DataRow
        Dim arrOutPut() As String = New String(2) {}
        Dim lngOppId As Long


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                Response.Redirect("../opportunity/frmOpportunities.aspx?" & GetQueryStringVal(Request.QueryString("enc"), "", True))



                If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                    SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                    SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
                Else : SI1 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
                Else : SI2 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                    frm = ""
                    frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
                Else : frm = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                    frm1 = ""
                    frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
                Else : frm1 = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    frm2 = ""
                    frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
                Else : frm2 = ""
                End If
                lngOppId = GetQueryStringVal(Request.QueryString("enc"), "opid")
                m_aryRightsForAssContacts = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmOpportunities.aspx", Session("userID"), 10, 4)
                m_aryRightsForItems = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmOpportunities.aspx", Session("userID"), 10, 5)
                If (lngOppId <> 0) Then LoadTableInformation()
                txtOppId.Text = lngOppId
                If Not IsPostBack Then
                    If uwOppTab.Tabs.Count > SI Then uwOppTab.SelectedTabIndex = SI
                    objContacts = New CContacts
                    objContacts.RecID = lngOppId
                    objContacts.Type = "O"
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.AddVisiteddetails()
                    Session("Help") = "Opportunity"
                    ''''call function for sub-tab management  - added on 29jul09 by Mohan
                    'objCommon.ManageSubTabs(uwOppTab, Session("DomainID"), 10)
                    '''''
                    Dim strDate As String = FormattedDateFromDate(Now, Session("DateFormat"))
                    If GetQueryStringVal(Request.QueryString("enc"), "SelectedIndex") <> "" Then
                        uwOppTab.SelectedTabIndex = GetQueryStringVal(Request.QueryString("enc"), "SelectedIndex")
                    End If
                    If lngOppId <> 0 Then
                        tblMenu.Visible = True
                    Else : tblMenu.Visible = False
                    End If

                    'IframeBiz.Attributes.Add("src", "../opportunity/frmBizDocs.aspx?OpID=" & lngOppId)
                    CreatMilestone()
                    DisplayDynamicFlds()
                    btnTransfer.Attributes.Add("onclick", "return OpenTransfer('" & "../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Opp&pluYR=" & lngOppId & "')")
                    btnActdelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTableInformation()
            Try
                Dim dtTableInfo As DataTable
                Dim dttablecust As DataTable
                Dim ds As DataSet
                If objPageLayout Is Nothing Then objPageLayout = New CPageLayout
                Dim check As String
                Dim fields() As String
                Dim idcolumn As String = ""
                Dim count1 As Integer
                Dim x As Integer
                Dim type As Char = "O"

                Dim dtDetails As DataTable
                objPageLayout.OpportunityId = lngOppId
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                ViewState("OppType") = dtDetails.Rows(0).Item("tintOppType")
                ddlOppType.Text = dtDetails.Rows(0).Item("tintOppType")
                If dtDetails.Rows(0).Item("tintOppType") = 1 Then
                    type = "O"
                ElseIf dtDetails.Rows(0).Item("tintOppType") = 2 Then
                    type = "u"
                End If
                hplCustomer.Text = dtDetails.Rows(0).Item("vcCompanyName")
                txtDivId.Text = dtDetails.Rows(0).Item("numDivisionID")
                If Not IsDBNull(dtDetails.Rows(0).Item("numParentOppID")) Then
                    hplOppLinked.Text = "Source : " & dtDetails.Rows(0).Item("ParentOppName")
                    hplOppLinked.NavigateUrl = "../opportunity/frmOpportunities.aspx?frm=opportunitylist&OpID=" & dtDetails.Rows(0).Item("numParentOppID")
                End If
                If Not IsDBNull(dtDetails.Rows(0).Item("Source")) Then
                    If (dtDetails.Rows(0).Item("Source").ToString().Trim().Length > 0) Then
                        hplOppLinked.Text = "Source : " & dtDetails.Rows(0).Item("Source")
                        hplOppLinked.NavigateUrl = "#"
                    End If
                End If
                If Not IsDBNull(dtDetails.Rows(0).Item("ChildCount")) Then
                    Dim intCount As Integer = CInt(dtDetails.Rows(0).Item("ChildCount").ToString()) + CInt(dtDetails.Rows(0).Item("RecurringChildCount").ToString())
                    If (intCount > 0) Then
                        hplChildRecords.Text = "Child Records (" & intCount & ")"
                        hplChildRecords.NavigateUrl = "#"
                        hplChildRecords.Visible = True
                        hplChildRecords.Attributes.Add("onclick", "openChild(" & lngOppId.ToString() & ")")
                    End If
                End If


                If dtDetails.Rows(0).Item("tintCRMType") = 0 Then
                    hplCustomer.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=oppdetail&DivID=" & dtDetails.Rows(0).Item("numDivisionID") & "&opId=" & lngOppId & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")

                ElseIf dtDetails.Rows(0).Item("tintCRMType") = 1 Then
                    hplCustomer.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=oppdetail&DivID=" & dtDetails.Rows(0).Item("numDivisionID") & "&opId=" & lngOppId & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")

                ElseIf dtDetails.Rows(0).Item("tintCRMType") = 2 Then
                    hplCustomer.NavigateUrl = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=oppdetail&klds+7kldf=fjk-las&DivId=" & dtDetails.Rows(0).Item("numDivisionID") & "&opId=" & lngOppId & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")

                End If
                If dtDetails.Rows(0).Item("tintOppType") = 1 Then
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmOpportunities.aspx", Session("userID"), 10, 3)
                    m_aryRightsForCustFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmOpportunityList.aspx", Session("userID"), 10, 12)
                    lblCustomerType.Text = "Customer : "
                ElseIf dtDetails.Rows(0).Item("tintOppType") = 2 Then
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmOpportunities.aspx", Session("userID"), 10, 9)
                    m_aryRightsForCustFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmOpportunityList.aspx", Session("userID"), 10, 13)
                    lblCustomerType.Text = "Vendor : "
                End If

                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                Else
                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnEdit.Visible = False
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActdelete.Visible = False
                End If

                btnLayout.Attributes.Add("onclick", "return ShowLayout('" & type & "','" & lngOppId & "');")
                objPageLayout.CoType = type
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngOppId
                objPageLayout.DomainID = Session("DomainID")
                If type = "O" Then
                    objPageLayout.PageId = 2
                ElseIf type = "u" Then
                    objPageLayout.PageId = 6
                End If

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)
                'If ds.Tables.Count = 2 Then
                '    dttablecust = ds.Tables(1)
                '    dtTableInfo.Merge(dttablecust)
                'End If

                txtName.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("VcPoppName")), "", dtDetails.Rows(0).Item("VcPoppName"))
                If Not IsDBNull(dtDetails.Rows(0).Item("monPAmount")) Then
                    lblAmount.Text = String.Format("{0:#,##0.00}", dtDetails.Rows(0).Item("monPAmount"))
                End If
                'Dim dv As DataView
                'dv = New DataView(dtTableInfo)
                Dim numrows As Integer = dtTableInfo.Compute("Max(tintrow)", String.Empty)
                Dim numcells As Integer = dtTableInfo.Compute("Max(intcoulmn)", String.Empty)
                Dim i As Integer = 0
                Dim nr As Integer
                Dim noRowsToLoop As Integer
                noRowsToLoop = (dtTableInfo.Rows.Count - (numrows * numcells)) / numcells
                noRowsToLoop = noRowsToLoop + numrows + 1

                If type = "O" Then
                    For nr = 0 To noRowsToLoop
                        Dim r As New TableRow()
                        Dim nc As Integer
                        Dim ro As Integer = nr
                        For nc = 1 To numcells
                            If dtTableInfo.Rows.Count <> i Then
                                If dtTableInfo.Rows(i).Item("tintrow") = nr + 1 And dtTableInfo.Rows(i).Item("intcoulmn") = nc Then
                                    Dim column1 As New TableCell
                                    Dim column2 As New TableCell
                                    Dim bitDynFld As String
                                    Dim fieldId As Integer
                                    fieldId = CInt(dtTableInfo.Rows(i).Item("numFieldID").ToString)
                                    bitDynFld = dtTableInfo.Rows(i).Item("bitCustomField")
                                    column1.CssClass = "normal7"
                                    If (bitDynFld <> "1") Then
                                        If (fieldId = "113") Then
                                            Dim h As New HyperLink()
                                            'h.CssClass = "hyperlink"
                                            h.NavigateUrl = "#"
                                            h.Text = "Documents"
                                            h.Attributes.Add("onclick", "return OpenDocuments(" & lngOppId & ");")
                                            Dim l As New Label
                                            l.CssClass = "normal7"
                                            l.Text = "(" & IIf(IsDBNull(dtDetails.Rows(0).Item("DocumentCount")), "", dtDetails.Rows(0).Item("DocumentCount")) & ")" & "&nbsp;:"
                                            column1.Controls.Add(h)
                                            column1.Controls.Add(l)
                                        ElseIf (fieldId = "114") Then
                                            Dim h As New HyperLink()
                                            'h.CssClass = "hyperlink"
                                            h.NavigateUrl = "#"
                                            h.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                            h.Attributes.Add("onclick", "ShowlinkedProjects(" & lngOppId & ")")
                                            Dim l As New Label
                                            l.CssClass = "normal7"
                                            l.Text = "(" & IIf(IsDBNull(dtDetails.Rows(0).Item("NoOfProjects")), "", dtDetails.Rows(0).Item("NoOfProjects")) & ")" & "&nbsp;:"
                                            column1.Controls.Add(h)
                                            column1.Controls.Add(l)
                                        Else : column1.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                        End If
                                        If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName").ToString) And dtTableInfo.Rows(i).Item("vcDBColumnName").ToString() <> "" Then
                                            Dim temp As String
                                            temp = dtTableInfo.Rows(i).Item("vcDBColumnName").ToString
                                            fields = temp.Split(",")
                                            Dim j As Integer
                                            j = 0
                                            While (j < fields.Length)
                                                If (fieldId = "103" Or fieldId = "111" Or fieldId = "110" Or fieldId = "113" Or fieldId = "114" Or fieldId = "116") Then
                                                    Dim listvalue As String
                                                    Dim value As String = ""
                                                    listvalue = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))
                                                    Select Case fieldId   ' Must be a primitive data type
                                                        Case 115
                                                            If Not IsDBNull(dtDetails.Rows(0).Item("intpEstimatedCloseDate")) Then
                                                                value = FormattedDateFromDate(IIf(IsDBNull(dtDetails.Rows(0).Item("intpEstimatedCloseDate")), "", dtDetails.Rows(0).Item("intpEstimatedCloseDate")), Session("DateFormat"))
                                                            End If

                                                        Case 103 : value = IIf(dtDetails.Rows(0).Item("varCurrSymbol").ToString().Length > 0, dtDetails.Rows(0).Item("varCurrSymbol") + "&nbsp;", "") + String.Format("{0:#,##0.00}", IIf(IsDBNull(dtDetails.Rows(0).Item("monPAmount")), "", dtDetails.Rows(0).Item("monPAmount")))
                                                        Case 111 : value = IIf(dtDetails.Rows(0).Item("varCurrSymbol").ToString().Length > 0, dtDetails.Rows(0).Item("varCurrSymbol") + "&nbsp;", "") + String.Format("{0:#,##0.00}", IIf(IsDBNull(dtDetails.Rows(0).Item("CalAmount")), "", dtDetails.Rows(0).Item("CalAmount")))
                                                        Case 113, 114
                                                            value = ""
                                                        Case Else
                                                    End Select
                                                    column2.Text = value

                                                ElseIf (fieldId = "112") Then
                                                    Dim l As New Label
                                                    l.CssClass = "cell"
                                                    If Not (IsDBNull(dtDetails.Rows(0).Item("tintActive"))) Then
                                                        l.Text = IIf(dtDetails.Rows(0).Item("tintActive") = "1", "a", "r")
                                                        l.CssClass = "cell1"
                                                    Else
                                                        l.Text = ""
                                                        l.CssClass = "cell"
                                                    End If
                                                    column2.Controls.Add(l)
                                                Else : column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))
                                                End If
                                                j += 1
                                            End While
                                        Else : column1.Text = ""
                                        End If ' end of table cell2
                                    Else
                                        If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then
                                            If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                                column1.Text = "Custom Web Link :"
                                                Dim h As New HyperLink()
                                                h.CssClass = "hyperlink"
                                                Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                                url = url.Replace("RecordID", lngOppId)
                                                h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                                h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                                column2.Controls.Add(h)
                                            ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                                column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                                Dim strDate As String
                                                strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                                If strDate = "0" Then strDate = ""
                                                If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                            Else
                                                column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                                If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                        Dim l As New Label
                                                        l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                                        l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                        column2.Controls.Add(l)
                                                    End If
                                                Else
                                                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                        column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If

                                    column2.CssClass = "normal1"
                                    column1.HorizontalAlign = HorizontalAlign.Right
                                    column2.HorizontalAlign = HorizontalAlign.Left
                                    column1.Width = 250
                                    column2.Width = 300
                                    column2.ColumnSpan = 1
                                    column1.ColumnSpan = 1
                                    r.Cells.Add(column1)
                                    r.Cells.Add(column2)
                                    i += 1
                                ElseIf dtTableInfo.Rows(i).Item("tintrow") = 0 And dtTableInfo.Rows(i).Item("intcoulmn") = 0 Then
                                    Dim column1 As New TableCell
                                    Dim column2 As New TableCell
                                    column1.CssClass = "normal7"
                                    If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then
                                        If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                            column1.Text = "Custom Web Link :"
                                            Dim h As New HyperLink()
                                            h.CssClass = "hyperlink"
                                            Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                            url = url.Replace("RecordID", lngOppId)
                                            h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                            h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                            column2.Controls.Add(h)
                                        ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                            column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                            Dim strDate As String
                                            strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                            If strDate = "0" Then strDate = ""
                                            If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                        Else
                                            column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                            If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                                If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                    Dim l As New Label
                                                    l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                                    l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                    column2.Controls.Add(l)
                                                End If
                                            Else
                                                If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                    column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                                End If
                                            End If
                                        End If
                                    End If
                                    column2.CssClass = "normal1"
                                    column1.HorizontalAlign = HorizontalAlign.Right
                                    column2.HorizontalAlign = HorizontalAlign.Left
                                    column1.Width = 250
                                    column2.Width = 300
                                    column2.ColumnSpan = 1
                                    column1.ColumnSpan = 1
                                    r.Cells.Add(column1)
                                    r.Cells.Add(column2)
                                    i += 1
                                Else
                                    Dim column1 As New TableCell
                                    Dim column2 As New TableCell
                                    column1.Text = ""
                                    r.Cells.Add(column1)
                                    r.Cells.Add(column2)
                                End If
                            End If
                        Next nc
                        tabledetail.Rows.Add(r)
                    Next nr
                ElseIf type = "u" Then
                    For nr = 0 To noRowsToLoop
                        Dim r As New TableRow()
                        Dim nc As Integer
                        Dim ro As Integer = nr
                        For nc = 1 To numcells
                            If dtTableInfo.Rows.Count <> i Then
                                If dtTableInfo.Rows(i).Item("tintrow") = nr + 1 And dtTableInfo.Rows(i).Item("intcoulmn") = nc Then
                                    Dim column1 As New TableCell
                                    Dim column2 As New TableCell
                                    Dim bitDynFld As String
                                    Dim fieldId As Integer
                                    fieldId = CInt(dtTableInfo.Rows(i).Item("numFieldID").ToString)
                                    bitDynFld = dtTableInfo.Rows(i).Item("bitCustomField")
                                    column1.CssClass = "normal7"
                                    If (bitDynFld <> "1") Then
                                        If (fieldId = "269") Then
                                            Dim h As New HyperLink()
                                            'h.CssClass = "hyperlink"
                                            h.NavigateUrl = "#"
                                            h.Text = "Documents"
                                            h.Attributes.Add("onclick", "return OpenDocuments(" & lngOppId & ");")
                                            Dim l As New Label
                                            l.CssClass = "normal7"
                                            l.Text = "(" & IIf(IsDBNull(dtDetails.Rows(0).Item("DocumentCount")), "", dtDetails.Rows(0).Item("DocumentCount")) & ")" & "&nbsp;:"
                                            column1.Controls.Add(h)
                                            column1.Controls.Add(l)
                                        ElseIf (fieldId = "270") Then
                                            Dim h As New HyperLink()
                                            'h.CssClass = "hyperlink"
                                            h.NavigateUrl = "#"
                                            h.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                            h.Attributes.Add("onclick", "ShowlinkedProjects(" & lngOppId & ")")
                                            Dim l As New Label
                                            l.CssClass = "normal7"
                                            l.Text = "(" & IIf(IsDBNull(dtDetails.Rows(0).Item("NoOfProjects")), "", dtDetails.Rows(0).Item("NoOfProjects")) & ")" & "&nbsp;:"
                                            column1.Controls.Add(h)
                                            column1.Controls.Add(l)
                                        Else : column1.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                        End If

                                        If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName").ToString) And dtTableInfo.Rows(i).Item("vcDBColumnName").ToString() <> "" Then
                                            Dim temp As String
                                            temp = dtTableInfo.Rows(i).Item("vcDBColumnName").ToString
                                            fields = temp.Split(",")
                                            Dim j As Integer
                                            j = 0
                                            While (j < fields.Length)
                                                If (fieldId = "271" Or fieldId = "276" Or fieldId = "267" Or fieldId = "269" Or fieldId = "270" Or fieldId = "272") Then
                                                    Dim listvalue As String
                                                    Dim value As String = ""
                                                    listvalue = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))

                                                    Select Case fieldId   ' Must be a primitive data type
                                                        Case 271
                                                            If Not IsDBNull(dtDetails.Rows(0).Item("intpEstimatedCloseDate")) Then
                                                                value = FormattedDateFromDate(IIf(IsDBNull(dtDetails.Rows(0).Item("intpEstimatedCloseDate")), "", dtDetails.Rows(0).Item("intpEstimatedCloseDate")), Session("DateFormat"))
                                                            End If

                                                        Case 267 : value = String.Format("{0:#,##0.00}", IIf(IsDBNull(dtDetails.Rows(0).Item("CalAmount")), "", dtDetails.Rows(0).Item("CalAmount")))
                                                        Case 276 : value = String.Format("{0:#,##0.00}", IIf(IsDBNull(dtDetails.Rows(0).Item("monPAmount")), "", dtDetails.Rows(0).Item("monPAmount")))
                                                        Case 269, 270
                                                            value = ""
                                                        Case Else
                                                    End Select
                                                    column2.Text = value
                                                ElseIf (fieldId = "265") Then
                                                    Dim l As New Label
                                                    l.CssClass = "cell"
                                                    l.Text = "r"
                                                    column2.Controls.Add(l)
                                                ElseIf (fieldId = "268") Then
                                                    Dim l As New Label
                                                    l.CssClass = "cell"
                                                    If Not (IsDBNull(dtDetails.Rows(0).Item("tintActive"))) Then
                                                        l.Text = IIf(dtDetails.Rows(0).Item("tintActive") = "1", "a", "r")
                                                        l.CssClass = "cell1"
                                                    Else
                                                        l.Text = ""
                                                        l.CssClass = "cell"
                                                    End If
                                                    column2.Controls.Add(l)
                                                Else : column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))
                                                End If
                                                j += 1
                                            End While
                                        Else : column1.Text = ""
                                        End If ' end of table cell2
                                    Else
                                        If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then
                                            If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                                column1.Text = "Custom Web Link :"
                                                Dim h As New HyperLink()
                                                h.CssClass = "hyperlink"
                                                Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                                url = url.Replace("RecordID", lngOppId)
                                                h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                                h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                                column2.Controls.Add(h)
                                            ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                                column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                                Dim strDate As String
                                                strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                                If strDate = "0" Then strDate = ""
                                                If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                            Else
                                                column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                                If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                        Dim l As New Label
                                                        l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                                        l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                        column2.Controls.Add(l)
                                                    End If
                                                Else
                                                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                        column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If

                                    column2.CssClass = "normal1"
                                    column1.HorizontalAlign = HorizontalAlign.Right
                                    column2.HorizontalAlign = HorizontalAlign.Left
                                    column1.Width = 250
                                    column2.Width = 300
                                    column2.ColumnSpan = 1
                                    column1.ColumnSpan = 1
                                    r.Cells.Add(column1)
                                    r.Cells.Add(column2)
                                    i += 1
                                ElseIf dtTableInfo.Rows(i).Item("tintrow") = 0 And dtTableInfo.Rows(i).Item("intcoulmn") = 0 Then
                                    Dim column1 As New TableCell
                                    Dim column2 As New TableCell
                                    column1.CssClass = "normal7"
                                    If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then

                                        If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                            column1.Text = "Custom Web Link :"
                                            Dim h As New HyperLink()
                                            h.CssClass = "hyperlink"
                                            Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                            url = url.Replace("RecordID", lngOppId)
                                            h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                            h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                            column2.Controls.Add(h)
                                        ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                            column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                            Dim strDate As String
                                            strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                            If strDate = "0" Then strDate = ""
                                            If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                        Else
                                            column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                            If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                                If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                    Dim l As New Label
                                                    l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                                    l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                    column2.Controls.Add(l)
                                                End If
                                            Else
                                                If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                    column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                                End If
                                            End If
                                        End If
                                    End If
                                    column2.CssClass = "normal1"
                                    column1.HorizontalAlign = HorizontalAlign.Right
                                    column2.HorizontalAlign = HorizontalAlign.Left
                                    column1.Width = 250
                                    column2.Width = 300
                                    column2.ColumnSpan = 1
                                    column1.ColumnSpan = 1
                                    r.Cells.Add(column1)
                                    r.Cells.Add(column2)
                                    i += 1
                                Else
                                    Dim column1 As New TableCell
                                    Dim column2 As New TableCell
                                    column1.Text = ""
                                    r.Cells.Add(column1)
                                    r.Cells.Add(column2)
                                End If
                            End If
                        Next nc
                        tabledetail.Rows.Add(r)
                    Next nr
                End If

                If (dtDetails.Rows.Count > 0) Then
                    Dim column1 As New TableCell
                    Dim column2 As New TableCell
                    Dim r As New TableRow
                    column1.CssClass = "normal7"
                    column2.CssClass = "normal1"
                    column1.HorizontalAlign = HorizontalAlign.Right
                    column2.HorizontalAlign = HorizontalAlign.Justify
                    column2.ColumnSpan = 5
                    Dim l As New Label
                    l.CssClass = "normal7"
                    l.Text = "Comments" & "&nbsp;:"
                    column1.Controls.Add(l)
                    column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("txtComments")), "-", dtDetails.Rows(0).Item("txtComments"))
                    column1.Width = 150
                    r.Cells.Add(column1)
                    r.Cells.Add(column2)
                    tableComment.Rows.Add(r)
                End If
                lblClReason.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("lngPConclAnalysis")), "", dtDetails.Rows(0).Item("lngPConclAnalysis").ToString)
                lblTProgress.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("TProgress")), 0, dtDetails.Rows(0).Item("TProgress").ToString)
                If Not IsPostBack Then LoadSavedInformation(dtDetails)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadSavedInformation(ByVal dtDetails As DataTable)
            Try
                ViewState("OppType") = dtDetails.Rows(0).Item("tintOppType")
                btnTrackAsset.Attributes.Add("onclick", "return openTrackAsset('" & lngOppId & "','" & dtDetails.Rows(0).Item("numDivisionID") & "')")
                If dtDetails.Rows(0).Item("tintOppType") = 1 Then
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmOpportunities.aspx", Session("userID"), 10, 3)
                    lblCustomerType.Text = "Customer : "
                ElseIf dtDetails.Rows(0).Item("tintOppType") = 2 Then
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmOpportunities.aspx", Session("userID"), 10, 9)
                    lblCustomerType.Text = "Vendor : "
                End If

                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                Else
                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnEdit.Visible = False
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActdelete.Visible = False
                End If
                If Not IsDBNull(dtDetails.Rows(0).Item("numRecOwner")) Then txtrecOwner.Text = dtDetails.Rows(0).Item("numRecOwner")
                lblLastModifiedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("ModifiedBy")), "", dtDetails.Rows(0).Item("ModifiedBy"))
                lblCreatedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("CreatedBy")), "", dtDetails.Rows(0).Item("CreatedBy"))
                lblRecordOwner.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("RecordOwner")), "", dtDetails.Rows(0).Item("RecordOwner"))
                If dtDetails.Rows(0).Item("tintOppStatus") = 1 Then
                    ViewState("Deal") = "True"
                    uwOppTab.Tabs(0).Text = "Deal Details"
                    lblDealCompletedDate.Text = "Deal Completed Date : " & FormattedDateFromDate(dtDetails.Rows(0).Item("bintAccountClosingDate"), Session("DateFormat"))
                    btnCreateOpp.Visible = True
                    btnCreateOpp.Attributes.Add("onClick", "return OpenCreateOpp('" & lngOppId & "','" & dtDetails.Rows(0).Item("tintOppType") & "')")
                    If dtDetails.Rows(0).Item("tintOppType") = 1 Then
                        btnCreateOpp.Text = "Create Purchase Deals/Orders"
                    Else : btnCreateOpp.Text = "Create Sales Deals/Orders"
                    End If
                End If
                If dtDetails.Rows(0).Item("tintOppStatus") = 1 And dtDetails.Rows(0).Item("tintshipped") = 1 Then
                    btnActdelete.Enabled = False
                    btnConfSerItems.Attributes.Add("onclick", "return OpenConfSerItem('" & lngOppId & "','" & dtDetails.Rows(0).Item("tintOppType") & "')")
                    btnConfSerItems.Visible = True
                End If
                btnCreateRecurring.Attributes.Add("onclick", "return OpenRecurringDetailPage(" & lngOppId & "," & dtDetails.Rows(0).Item("tintshipped") & ")")
                btnCreateRecurring.Visible = True

                If objOpportunity Is Nothing Then objOpportunity = New MOpportunity
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.DomainID = Session("DomainID")

                dsTemp = objOpportunity.ItemsByOppId
                Dim dtItem As DataTable
                Dim dtSerItem As DataTable
                Dim dtChildItems As DataTable
                dtItem = dsTemp.Tables(0)
                dtSerItem = dsTemp.Tables(1)
                dtChildItems = dsTemp.Tables(2)
                dtItem.TableName = "Item"
                dtSerItem.TableName = "SerialNo"
                dtChildItems.TableName = "ChildItems"
                dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
                'Response.Write(dsTemp.GetXml)
                If dtSerItem.ParentRelations.Count = 0 Then
                    dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
                End If
                dsTemp.AcceptChanges()
                Session("Data") = dsTemp
                If m_aryRightsForItems(RIGHTSTYPE.VIEW) <> 0 Then BindItems()

                Dim dtContactInfo As DataTable
                dtContactInfo = objOpportunity.AssociatedbyOppID
                BindContactInfo(dtContactInfo)

                If objPageLayout Is Nothing Then objPageLayout = New CPageLayout
                objPageLayout.UserCntID = Session("UserContactID")
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.OpportunityId = lngOppId
                dtSalesProcess = objPageLayout.SalesProcessDtlByOppId
                If dtSalesProcess.Rows.Count <= 2 Then ViewState("MileCheck") = 1
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindItems()
            Try
                tblProducts.Visible = True
                RadGrid1.DataSource = dsTemp
                RadGrid1.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindContactInfo(ByVal dtContactInfo As DataTable)
            Try
                If m_aryRightsForAssContacts(RIGHTSTYPE.VIEW) <> 0 Then
                    dgContact.DataSource = dtContactInfo
                    dgContact.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub CreatMilestone()
            Try
                Dim Configuration As Integer
                tblMilestone.Rows.Clear()
                'dtSalesProcess = Session("SalesProcessDetails")
                If dtSalesProcess.Rows.Count <> 0 Then
                    '                    LoadAssignTo()
                    Dim i As Integer
                    Dim tblCell As TableCell
                    Dim tblrow As TableRow
                    Dim chkDlost As CheckBox
                    Dim chkDClosed As CheckBox
                    Dim btnAdd As Button
                    Dim btnDelete As Button
                    Dim boolDealWon, boolDealLost As Boolean
                    Dim strWonComm, strLostComm As String
                    Dim InitialPercentage As Integer = dtSalesProcess.Rows(0).Item(0)
                    If Not dtSalesProcess.Rows(0).Item(0) = 0 Then
                        If Not dtSalesProcess.Rows(0).Item(0) = 100 Then
                            If dtSalesProcess.Rows.Count <> 2 Then
                                If Not dtSalesProcess.Rows(0).Item("Op_Flag") = 1 Then
                                    tblrow = New TableRow
                                    tblCell = New TableCell
                                    tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Milestone - " & InitialPercentage & "%&nbsp;&nbsp;" & dtSalesProcess.Rows(0).Item("vcStagePercentageDtl") & "</font>"
                                    tblCell.Height = Unit.Pixel(20)
                                    tblCell.CssClass = "text_bold"
                                    tblCell.ColumnSpan = 4
                                    tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                                    tblrow.Controls.Add(tblCell)
                                    tblMilestone.Controls.Add(tblrow)
                                End If
                            End If
                        End If
                    End If
                    ViewState("CheckColor") = 0
                    For i = 0 To dtSalesProcess.Rows.Count - 1
                        If dtSalesProcess.Rows(i).Item(0) <> 100 Then
                            If dtSalesProcess.Rows(i).Item(0) <> 0 Then
                                If InitialPercentage = dtSalesProcess.Rows(i).Item(0) Then
                                    If dtSalesProcess.Rows(i).Item("Op_Flag") <> 1 Then
                                        createStages(dtSalesProcess.Rows(i))
                                        InitialPercentage = dtSalesProcess.Rows(i).Item(0)
                                    End If
                                Else
                                    If dtSalesProcess.Rows(i).Item("Op_Flag") <> 1 Then
                                        InitialPercentage = dtSalesProcess.Rows(i).Item(0)
                                        tblrow = New TableRow
                                        tblCell = New TableCell
                                        tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Milestone - " & InitialPercentage & "%&nbsp;&nbsp;" & dtSalesProcess.Rows(i).Item("vcStagePercentageDtl") & "</font>"
                                        tblCell.Height = Unit.Pixel(20)
                                        tblCell.CssClass = "text_bold"
                                        tblCell.ColumnSpan = 4
                                        tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                                        tblrow.Controls.Add(tblCell)
                                        tblMilestone.Controls.Add(tblrow)
                                        ViewState("CheckColor") = 0
                                        createStages(dtSalesProcess.Rows(i))
                                    End If
                                End If
                            End If
                        End If
                        If dtSalesProcess.Rows(i).Item(0) = 100 Then
                            boolDealWon = dtSalesProcess.Rows(i).Item("bitStageCompleted")
                            strWonComm = dtSalesProcess.Rows(i).Item("vcComments")
                        ElseIf dtSalesProcess.Rows(i).Item(0) = 0 Then
                            boolDealLost = dtSalesProcess.Rows(i).Item("bitStageCompleted")
                            strLostComm = dtSalesProcess.Rows(i).Item("vcComments")
                        End If
                    Next

                    ''Deal Conclusion
                    tblrow = New TableRow
                    Dim lblDComments As Label
                    Dim lblComm As Label
                    Dim lblDclosed1 As Label
                    Dim lblDclosed2 As Label
                    Dim lblDlost1 As Label
                    Dim lblDlost2 As Label
                    tblCell = New TableCell
                    tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Deal Conclusion</font>"
                    tblCell.Height = Unit.Pixel(20)
                    tblCell.CssClass = "text_bold"
                    tblCell.ColumnSpan = 4
                    tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                    tblrow.Controls.Add(tblCell)
                    tblMilestone.Controls.Add(tblrow)

                    '' deal Closed
                    tblrow = New TableRow
                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblCell.ColumnSpan = 4
                    lblDclosed1 = New Label
                    lblDclosed2 = New Label
                    lblDclosed1.ID = "lblDClosed"
                    'chkDClosed.Attributes.Add("onclick", "return ValidateCheckBox(1)")
                    lblDclosed1.Text = "Deal Won &nbsp; &nbsp; &nbsp;"
                    lblDclosed1.CssClass = "normal8"
                    lblDclosed2.CssClass = "cell"
                    If boolDealWon = True Then
                        lblDclosed2.CssClass = "cell1"
                        lblDclosed2.Text = "a"
                    Else
                        lblDclosed2.CssClass = "cell"
                        lblDclosed2.Text = "r"
                    End If
                    lblComm = New Label
                    lblComm.Text = "&nbsp;&nbsp; Comments &nbsp; "
                    lblComm.CssClass = "normal8"
                    lblDComments = New Label
                    lblDComments.Text = strWonComm
                    lblDComments.CssClass = "normal1"
                    lblDComments.ID = "txtDCComm"
                    lblDComments.Width = Unit.Pixel(600)
                    tblCell.Controls.Add(lblDclosed1)
                    tblCell.Controls.Add(lblDclosed2)
                    tblCell.CssClass = "normal1"
                    tblCell.Controls.Add(lblComm)
                    tblCell.Controls.Add(lblDComments)
                    tblrow.Controls.Add(tblCell)
                    tblMilestone.Controls.Add(tblrow)

                    '' deal Lost
                    tblrow = New TableRow
                    tblCell = New TableCell
                    tblCell.ColumnSpan = 4
                    tblCell.CssClass = "normal1"
                    lblDlost1 = New Label
                    lblDlost2 = New Label
                    lblDlost1.ID = "chkDlost"

                    If boolDealLost = True Then
                        lblDlost2.CssClass = "cell1"
                        lblDlost2.Text = "a"
                    Else
                        lblDlost2.CssClass = "cell"
                        lblDlost2.Text = "r"
                    End If
                    lblDlost1.Text = "Deal Lost &nbsp; &nbsp; &nbsp; "
                    lblDlost1.CssClass = "normal8"
                    '   chkDlost.Attributes.Add("onclick", "return ValidateCheckBox(2)")
                    lblComm = New Label
                    lblComm.Text = "&nbsp;&nbsp; Comments &nbsp; "
                    lblComm.CssClass = "normal8"

                    lblDComments = New Label
                    lblDComments.ID = "txtDLComm"
                    lblDComments.Text = strLostComm
                    lblDComments.CssClass = "signup"
                    lblDComments.Width = Unit.Pixel(600)
                    tblCell.Controls.Add(lblDlost1)
                    tblCell.Controls.Add(lblDlost2)
                    tblCell.Controls.Add(lblComm)
                    tblCell.Controls.Add(lblDComments)
                    tblrow.Controls.Add(tblCell)
                    tblMilestone.Controls.Add(tblrow)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub createStages(ByVal dr As DataRow)
            Try
                Dim tblCell As TableCell
                Dim tblrow As TableRow
                Dim lbl As Label
                Dim chkStage As CheckBox
                Dim lblStage As Label
                Dim lblchkStage As Label
                Dim lblStageCM As Label
                Dim lblInform As Label
                Dim lblComm As Label
                'Dim ddlStatus As DropDownList
                Dim lblStatus As Label
                Dim lblAssignTo As Label
                Dim lblAlert As Label
                Dim lblCheckKStage As Label
                Dim lblEvent As Label
                Dim lblReminder As Label
                Dim lblET As Label
                Dim lblActivity As Label
                Dim lblStartDay As Label
                Dim lblStartTime As Label
                Dim lblEndtime As Label
                Dim lblBody As Label

                Dim chkAlert As CheckBox
                Dim txtChecKStage As TextBox
                Dim hpkLink As HyperLink

                Dim lblDueDate As Label

                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                'tblCell.CssClass = "normal1"
                tblCell.ColumnSpan = 4
                lblStage = New Label
                If Not IsDBNull(dr.Item("vcstageDetail")) Then lblStage.Text = dr.Item("vcstageDetail")

                lblStage.CssClass = "normal9"
                Dim strImage As String

                strImage = "<img src='../images/mileStone.gif'  > "
                tblCell.Controls.Add(New LiteralControl(strImage))
                tblCell.Controls.Add(lblStage)
                tblCell.CssClass = "normal9"
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                '''First row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell

                lblStageCM = New Label
                lblStageCM.Text = "&nbsp;Stage Status&nbsp;&nbsp;:"
                lblStageCM.CssClass = "normal7"

                lblStageCM.ID = "lblStatus" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

                lblStatus = New Label
                If Not IsDBNull(dr.Item("numStage")) Then
                    lblStatus.Text = dr.Item("numStage")
                    lblStatus.ID = "lblStatus~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                Else : lblStatus.Text = ""
                End If

                tblCell.CssClass = "normal1"
                tblCell.ColumnSpan = 2
                tblCell.Controls.Add(lblStageCM)
                tblCell.Controls.Add(lblStatus)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                lblStageCM = New Label
                lblStageCM.Text = "Last Modified By:  "
                lblStageCM.CssClass = "normal7"
                lblStageCM.ID = "lblStageModifiedBy" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                lblInform = New Label
                If Not IsDBNull(dr.Item("numModifiedBy")) Then lblInform.Text = IIf(dr.Item("numModifiedBy") = 0, "", dr.Item("numModifiedByName"))
                lblInform.ID = "lblInformBy" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                tblCell.CssClass = "normal1"
                tblCell.Controls.Add(lblStageCM)
                tblCell.Controls.Add(lblInform)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                lblStageCM = New Label
                lblStageCM.Text = "Last Modified Date:  "
                lblStageCM.CssClass = "normal7"
                lblStageCM.ID = "lblStageModified" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                lblInform = New Label
                If Not IsDBNull(dr.Item("bintModifiedDate")) Then lblInform.Text = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), dr.Item("bintModifiedDate")), Session("DateFormat"))
                lblInform.ID = "lblInform" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                tblCell.CssClass = "normal1"
                tblCell.Controls.Add(lblStageCM)
                tblCell.Controls.Add(lblInform)
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                'Second Row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                tblCell.ColumnSpan = 4
                lbl = New Label
                lblComm = New Label
                lblComm.ID = "txtComm~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                lblComm.Width = Unit.Pixel(750)
                lblComm.CssClass = "normal1"
                lblComm.Text = dr.Item("vcComments")
                lbl.Text = "Comments &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                tblCell.Controls.Add(lblComm)
                tblCell.CssClass = "normal1"
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                ''Third Row
                If ViewState("MileCheck") = 0 Then
                    tblrow = New TableRow
                    If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    hpkLink = New HyperLink
                    hpkLink.ID = "hpkLinkTime" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    hpkLink.Attributes.Add("onclick", "return OpenTime(" & dr.Item("OppStageID") & "," & lngOppId & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & "," & txtDivId.Text & ");")
                    hpkLink.Text = "<font class='hyperlink'>Time</font>"
                    hpkLink.CssClass = "hyperlink"
                    lbl = New Label
                    lbl.ID = "lblTime" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    If Not IsDBNull(dr.Item("Time")) Then lbl.Text = "<font color=red>&nbsp;" & dr.Item("Time") & "</font>"
                    tblCell.Controls.Add(hpkLink)
                    tblCell.Controls.Add(lbl)
                    tblrow.Controls.Add(tblCell)

                    tblCell = New TableCell
                    hpkLink = New HyperLink
                    hpkLink.ID = "hpkLinkExpense" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    'If Not dr.Item("numOppStageID") Is Nothing Then
                    hpkLink.Attributes.Add("onclick", "return OpenExpense(" & dr.Item("OppStageID") & "," & lngOppId & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & "," & txtDivId.Text & ");")

                    hpkLink.Text = "<font class='hyperlink'>Expense</font>"
                    hpkLink.CssClass = "hyperlink"
                    tblCell.CssClass = "normal1"
                    lbl = New Label
                    lbl.ID = "lblExp" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    If Not IsDBNull(dr.Item("Expense")) Then lbl.Text = "<font color=red>&nbsp;" & String.Format("{0:#,##0.00}", CDec(dr.Item("Expense"))) & "</font>"
                    tblCell.Controls.Add(hpkLink)
                    tblCell.Controls.Add(lbl)
                    tblrow.Controls.Add(tblCell)

                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    hpkLink = New HyperLink
                    hpkLink.ID = "hpkLinkDependency" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    hpkLink.Attributes.Add("onclick", "return OpenDependency(" & dr.Item("OppStageID") & "," & lngOppId & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")
                    'End If
                    hpkLink.Text = "<font class='hyperlink'>Dependency</font>"
                    hpkLink.CssClass = "hyperlink"
                    lbl = New Label
                    '   lbl.ID = "lblDep" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    If Not IsDBNull(dr.Item("Depend")) Then
                        If dr.Item("Depend") = "1" Then lbl.Text = "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>"
                    End If
                    tblCell.Controls.Add(hpkLink)
                    tblCell.Controls.Add(lbl)
                    tblrow.Controls.Add(tblCell)

                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    lbl = New Label
                    hpkLink = New HyperLink
                    ' hpkLink.ID = "hpkLinkSubStage" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    hpkLink.Text = "<font class='hyperlink'>Sub Stages</font>"
                    hpkLink.CssClass = "hyperlink"
                    hpkLink.Attributes.Add("onclick", "return OpenSubStage(" & dr.Item("OppStageID") & "," & lngOppId & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")
                    'End If
                    lbl.ID = "lblStg" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    If Not IsDBNull(dr.Item("SubStg")) Then
                        If dr.Item("SubStg") = "1" Then lbl.Text = "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>"
                    End If
                    tblCell.Controls.Add(hpkLink)
                    tblCell.Controls.Add(lbl)
                    tblrow.Controls.Add(tblCell)
                    tblMilestone.Controls.Add(tblrow)
                End If

                ''''Fourth Row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "&nbsp;Assign To &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)

                lblAssignTo = New Label
                lblAssignTo.Text = IIf(IsDBNull(dr.Item("numAssignTo")), "-", dr.Item("numAssignTo"))

                tblCell.Controls.Add(lblAssignTo)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                chkAlert = New CheckBox
                lblAlert = New Label
                ' lblAlert.ID = "lblAlert~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                'If dr.Item("bitAlert") = 0 Then
                '    chkAlert.Checked = False
                'Else
                '    chkAlert.Checked = True
                'End If
                lbl = New Label
                lbl.Text = "Alert &nbsp;"
                tblCell.Controls.Add(lbl)
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lblAlert)

                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "&nbsp; &nbsp; &nbsp; "
                If dr.Item("bitAlert") = 1 Then
                    lbl.Text = "a"
                    lbl.CssClass = "cell1"
                ElseIf (dr.Item("bitAlert") = 0) Then
                    lbl.Text = "r"
                    lbl.CssClass = "cell"
                End If

                tblCell.Controls.Add(lbl)
                lbl = New Label
                lbl.CssClass = "normal7"
                lbl.Text = "Due Date: &nbsp;: &nbsp;"

                lblDueDate = New Label
                lblDueDate.CssClass = "normal1"
                '  lblDueDate.ID = "txtDueDate" & dr.Item("numStagePercentage") & dr.Item("numstagedetailsID")
                If Not IsDBNull(dr.Item("bintDueDate")) Then lblDueDate.Text = FormattedDateFromDate(dr.Item("bintDueDate"), Session("DateFormat"))

                tblCell.Controls.Add(lbl)
                tblCell.Controls.Add(lblDueDate)
                tblrow.Controls.Add(tblCell)
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Stage Completed : "
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lbl = New Label
                If Not IsDBNull(dr.Item("bintStageComDate")) Then
                    If dr.Item("bitStageCompleted") = True Then lbl.Text = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), dr.Item("bintStageComDate")), Session("DateFormat"))
                End If
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                'chkStage = New CheckBox
                lblchkStage = New Label
                txtChecKStage = New TextBox
                'txtChecKStage.ID = "txtChecKStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                txtChecKStage.Attributes.Add("style", "display:none")
                If dr.Item("bitStageCompleted") = False Then
                    txtChecKStage.Text = 0
                    lbl.Text = "<font color='#006400'><b>Stage Open</b></font>"
                    'lblchkStage.Text = "r"
                    'lblchkStage.CssClass = "cell"
                Else
                    txtChecKStage.Text = 1
                    lbl.Text = "<font color='#CC0000'><b>Stage Closed</b></font> : " & dr.Item("tintPercentage") & "%"
                    'lblchkStage.Text = "a"
                    'lblchkStage.CssClass = "cell"
                End If

                ' lblchkStage.ID = "chkStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                tblCell.Controls.Add(lbl)
                tblCell.Controls.Add(txtChecKStage)
                tblCell.Controls.Add(lblchkStage)
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                ''fifth row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Event &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)

                lblEvent = New Label
                lblEvent.Text = " " & IIf(IsDBNull(dr.Item("numEvent")), "-", dr.Item("numEvent"))

                tblCell.Controls.Add(lblEvent)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Remider &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lblReminder = New Label
                lblReminder.Text = " " & IIf(IsDBNull(dr.Item("numReminder")), "-", dr.Item("numReminder"))
                tblCell.Controls.Add(lblReminder)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Email Template &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lblET = New Label
                lblET.Text = " " & IIf(IsDBNull(dr.Item("numET")), "-", dr.Item("numET"))
                tblCell.Controls.Add(lblET)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell

                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Activity : "
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lbl = New Label
                lbl.Text = " " & IIf(IsDBNull(dr.Item("numActivity")), "-", dr.Item("numActivity"))
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)

                tblMilestone.Controls.Add(tblrow)

                ''Sixth row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Start Date &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)

                lblStartDay = New Label
                lblStartDay.Text = " " & IIf(IsDBNull(dr.Item("numStartDate")), "-", dr.Item("numStartDate"))

                tblCell.Controls.Add(lblStartDay)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Start Time &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lblStartTime = New Label
                lblStartTime.Text = " " & IIf(IsDBNull(dr.Item("numStartTime")), "-", dr.Item("numStartTime")) & " " & IIf(IsDBNull(dr.Item("numStartTimePeriod")), "-", dr.Item("numStartTimePeriod"))
                tblCell.Controls.Add(lblStartTime)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "End Time &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)

                lblEndtime = New Label
                lblEndtime.Text = " " & IIf(IsDBNull(dr.Item("numEndTime")), "-", dr.Item("numEndTime")) & " " & IIf(IsDBNull(dr.Item("numEndTimePeriod")), "-", dr.Item("numEndTimePeriod"))
                tblCell.Controls.Add(lblEndtime)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Action Item Type &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lblEndtime = New Label
                lblEndtime.Text = " " & dr.Item("numType")
                tblCell.Controls.Add(lblEndtime)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                tblrow.Controls.Add(tblCell)

                tblMilestone.Controls.Add(tblrow)

                ''Seventh row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                tblCell.ColumnSpan = 4
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Comments 2 :"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lbl = New Label
                lbl.Text = " " & dr.Item("txtCom")
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                ''Eight row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell

                tblCell.CssClass = "normal1"
                lbl = New Label
                If dr.Item("bitChgStatus") = True Then
                    lbl.Text = "a"
                    lbl.CssClass = "cell1"
                Else
                    lbl.Text = "r"
                    lbl.CssClass = "cell"
                End If
                tblCell.Controls.Add(lbl)
                lbl = New Label
                lbl.CssClass = "normal7"
                lbl.Text = " When done change stage status to "
                tblCell.Controls.Add(lbl)
                lbl = New Label
                lbl.CssClass = "normal1"
                lbl.Text = " " & dr.Item("numChgStatus")
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.ColumnSpan = 3
                lbl = New Label
                lbl.CssClass = "normal7"
                lbl.Text = "When done close stage  "
                tblCell.Controls.Add(lbl)
                lbl = New Label
                If dr.Item("bitClose") = True Then
                    lbl.Text = "a"
                    lbl.CssClass = "cell1"
                Else
                    lbl.Text = "r"
                    lbl.CssClass = "cell"
                End If
                tblCell.Controls.Add(lbl)

                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                If ViewState("CheckColor") = 1 Then
                    ViewState("CheckColor") = 0
                Else : ViewState("CheckColor") = 1
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgContact_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContact.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim btnDelete As Button
                    Dim lnkDelete As LinkButton
                    lnkDelete = e.Item.FindControl("lnkDeleteCnt")
                    btnDelete = e.Item.FindControl("btnDeleteCnt")
                    If m_aryRightsForAssContacts(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                    If e.Item.Cells(1).Text = 1 Then CType(e.Item.FindControl("lblShare"), Label).Text = "a"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                sb_PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub sb_PageRedirect()
            Try
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "contactdetails" Then
                    Response.Redirect("../contact/frmContacts.aspx?frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm1") & "&fdas89iu=098jfd&CntId=" & GetQueryStringVal(Request.QueryString("enc"), "CntID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
               ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "tickler" Then
                    Response.Redirect("../common/frmticklerdisplay.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "accounts" Then
                    objCommon.OppID = lngOppId
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../Account/frmAccounts.aspx?frm=accounts&klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
             ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "prospects" Then
                    objCommon.OppID = lngOppId
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../prospects/frmProspects.aspx?frm=prospects&DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "prospectdtl" Then
                    objCommon.OppID = lngOppId
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../prospects/frmProspects.aspx?frm=prospectdtl&DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "opportunitylist" Then
                    If ViewState("Deal") = "True" Then
                        If ViewState("OppType") = 1 Then
                            Response.Redirect("../opportunity/frmDealList.aspx?type=1&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                        ElseIf ViewState("OppType") = 2 Then
                            Response.Redirect("../opportunity/frmDealList.aspx?type=2&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                        End If
                    ElseIf ViewState("OppType") = 1 Then
                        Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                    ElseIf ViewState("OppType") = 2 Then
                        Response.Redirect("../opportunity/frmOpportunityList.aspx?SI=1&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                    End If
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ProjectList" Then
                    Response.Redirect("../projects/frmProjectList.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "Leads" Then
                    objCommon.OppID = lngOppId
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../Leads/frmLeads.aspx?DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "LeaddtlPl" Then
                    objCommon.OppID = lngOppId
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../pagelayout/frmLeaddtl.asp" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ProductsShipped" Then
                    Response.Redirect("../reports/frmProductShipped.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ContactRole" Then
                    Response.Redirect("../reports/frmContactRole.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CampDetails" Then
                    Response.Redirect("../Marketing/frmCampaignDetails.aspx?CampID=" & GetQueryStringVal(Request.QueryString("enc"), "CampID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "OpportunityOpen" Then
                    Response.Redirect("../reports/frmOpenOpportunities.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "OpportunityStatus" Then
                    Response.Redirect("../reports/frmOpportunityStatus.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "RecurringTransaction" Then
                    Response.Redirect("../Accounting/frmRecurringTransaction.aspx")
             ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "OppSearch" Then
                    Response.Redirect("../Admin/frmItemSearchRes.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "OpenBizDoc" Then
                    Response.Redirect("../Opportunity/frmOpenBizDocs.aspx?type=" + ViewState("OppType").ToString())
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "RecurringTransactionReport" Then
                    Response.Redirect("../Accounting/frmRecurringTransactionReport.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "RecurringTransactionDetailReport" Then
                    Response.Redirect("../Accounting/frmRecurringTransactionDetailReport.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "SalesReturn" Then
                    Response.Redirect("../Opportunity/frmSalesReturns.aspx?type=" + ViewState("OppType").ToString())
                Else
                    If ViewState("Deal") = "True" Then
                        If ViewState("OppType") = 1 Then
                            Response.Redirect("../opportunity/frmDealList.aspx?type=1&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                        ElseIf ViewState("OppType") = 2 Then
                            Response.Redirect("../opportunity/frmDealList.aspx?type=2&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                        End If
                    ElseIf ViewState("OppType") = 1 Then
                        Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                    ElseIf ViewState("OppType") = 2 Then
                        Response.Redirect("../opportunity/frmOpportunityList.aspx?SI=1&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .CompFilter = Trim(strName) & "%"
                    ddlCombo.DataSource = fillCombo.ListCustomer().Tables(0).DefaultView
                    ddlCombo.DataTextField = "vcCompanyname"
                    ddlCombo.DataValueField = "numDivisionID"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActdelete.Click
            Try
                Dim objOpport As New COpportunities
                With objOpport
                    .OpportID = lngOppId
                    .DomainID = Session("DomainID")
                End With
                Dim lintCount As Integer
                lintCount = objOpport.GetAuthoritativeOpportunityCount()
                If lintCount = 0 Then
                    objOpport.DelOpp()
                    If ViewState("OppType") = 1 Then
                        Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                    ElseIf ViewState("OppType") = 2 Then
                        Response.Redirect("../opportunity/frmOpportunityList.aspx?SelectIndex=1" & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                    End If
                Else : litMessage.Text = "You cannot delete Opportunity Details for which Authoritative BizDocs is present"
                End If
                ''  objOpport.DelOpp()
            Catch ex As Exception
                litMessage.Text = "Dependent record exists. Cannot be Deleted."
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            Try
                Response.Redirect("../opportunity/frmOpportunities.aspx?frm=IPOppDTL&OpID=" & lngOppId & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm") & "&type=" & GetQueryStringVal(Request.QueryString("enc"), "type") & "&SI=" & uwOppTab.SelectedTabIndex & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm & "&frm2=" & frm1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                Dim strDate As String
                Dim bizCalendar As UserControl
                Dim _myUC_DueDate As PropertyInfo
                Dim PreviousRowID As Integer = 0
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim i, k, j As Integer
                Dim dtTable As DataTable
                Dim count As Integer = uwOppTab.Tabs.Count
                If objPageLayout Is Nothing Then objPageLayout = New CPageLayout
                If ViewState("OppType") = 1 Then
                    objPageLayout.locId = 2
                ElseIf ViewState("OppType") = 2 Then
                    objPageLayout.locId = 6
                End If
                'objPageLayout.locId = 0
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.RelId = 0
                objPageLayout.RecordId = lngOppId
                dtTable = objPageLayout.GetCustFlds

                If uwOppTab.Tabs.Count > 5 Then
                    Dim iItemcount As Integer
                    iItemcount = uwOppTab.Tabs.Count
                    While uwOppTab.Tabs.Count > 5
                        uwOppTab.Tabs.RemoveAt(iItemcount - 1)
                        iItemcount = iItemcount - 1
                    End While
                End If
                If dtTable.Rows.Count > 0 Then
                    'CustomField Section
                    Dim Tab As Tab
                    ' Dim pageView As PageView
                    Dim aspTable As HtmlTable
                    Dim Table As Table
                    Dim tblcell As TableCell
                    Dim tblRow As TableRow
                    Dim up As UpdatePanel
                    Dim apt As AsyncPostBackTrigger
                    k = 0
                    ViewState("TabId") = dtTable.Rows(0).Item("TabId")
                    ViewState("Check") = 0
                    ViewState("FirstTabCreated") = 0
                    'Tabstrip4.Items.Clear()
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") <> 0 Then
                            If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                                If ViewState("Check") <> 0 Then
                                    aspTable.Rows.Add(objRow)
                                    tblcell.Controls.Add(aspTable)
                                    tblRow.Cells.Add(tblcell)
                                    Table.Rows.Add(tblRow)

                                    up = New UpdatePanel
                                    apt = New AsyncPostBackTrigger
                                    up.ChildrenAsTriggers = True
                                    up.UpdateMode = UpdatePanelUpdateMode.Conditional
                                    apt.ControlID = "btnEdit"
                                    up.ContentTemplateContainer.Controls.Add(Table)
                                    up.Triggers.Add(apt)
                                    Tab.ContentPane.Controls.Add(up)
                                    'pageView.Controls.Add(up)
                                    ' mpages.Controls.Add(pageView)
                                    ' mpages.Controls.Add(pageView)
                                End If
                                k = 0
                                ViewState("Check") = 1
                                'If Not IsPostBack Then
                                ViewState("FirstTabCreated") = 1
                                ViewState("TabId") = dtTable.Rows(i).Item("TabId")
                                Tab = New Tab
                                Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
                                uwOppTab.Tabs.Add(Tab)
                                'End If
                                'pageView = New PageView
                                aspTable = New HtmlTable
                                Table = New Table
                                Table.Width = Unit.Percentage(100)
                                Table.BorderColor = System.Drawing.Color.FromName("black")
                                Table.GridLines = GridLines.None
                                Table.BorderWidth = Unit.Pixel(1)
                                Table.Height = Unit.Pixel(300)
                                Table.CssClass = "aspTable"
                                tblcell = New TableCell
                                tblRow = New TableRow
                                tblcell.VerticalAlign = VerticalAlign.Top
                                aspTable.Width = "100%"
                                objRow = New HtmlTableRow
                                objCell = New HtmlTableCell
                                objCell.InnerHtml = "<br>"
                                objRow.Cells.Add(objCell)
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            If k = 3 Then
                                k = 0
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If
                            objCell = New HtmlTableCell
                            objCell.Align = "right"
                            objCell.Width = 100

                            objCell.Attributes.Add("class", "normal7")
                            If dtTable.Rows(i).Item("fld_type") <> "Frame" Then
                                If dtTable.Rows(i).Item("fld_type") <> "Link" Then
                                    objCell.InnerText = dtTable.Rows(i).Item("fld_label") & " :"
                                End If
                                objRow.Cells.Add(objCell)
                            End If
                            objCell = New HtmlTableCell
                            objCell.Attributes.Add("class", "normal1")
                            objCell.Align = "left"
                            If dtTable.Rows(i).Item("fld_type") = "Link" Then
                                Dim h As New HyperLink
                                h.CssClass = "hyperlink"
                                Dim URL As String = IIf(IsDBNull(dtTable.Rows(i).Item("vcURL")), "", dtTable.Rows(i).Item("vcURL"))
                                URL = URL.Replace("RecordID", lngOppId)
                                h.Text = IIf(IsDBNull(dtTable.Rows(i).Item("fld_label")), "", dtTable.Rows(i).Item("fld_label"))
                                h.Attributes.Add("onclick", "fn_GoToURL('" & "http://" & URL & "')")
                                objCell.Controls.Add(h)
                                'CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngCntID, dtTable.Rows(i).Item("fld_label"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = IIf(dtTable.Rows(i).Item("Value") = "1", "a", "r")
                                    l.CssClass = IIf(dtTable.Rows(i).Item("Value") = "1", "cell1", "cell")
                                    objCell.Controls.Add(l)
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = "r"
                                    l.CssClass = "cell"
                                    objCell.Controls.Add(l)
                                End If
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
                                objCell = New HtmlTableCell
                                Dim strFrame As String
                                Dim URL As String
                                URL = dtTable.Rows(i).Item("vcURL")
                                URL = URL.Replace("RecordID", lngOppId)
                                strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                objCell.Controls.Add(New LiteralControl(strFrame))
                                objRow.Cells.Add(objCell)
                            Else
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = IIf(dtTable.Rows(i).Item("Value") = "0" Or dtTable.Rows(i).Item("Value") = Nothing, "-", dtTable.Rows(i).Item("Value"))
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = "-"
                                End If
                            End If
                            objRow.Cells.Add(objCell)
                            k = k + 1
                        End If
                    Next
                    If ViewState("Check") = 1 Then
                        objRow.Align = "left"
                        aspTable.Rows.Add(objRow)
                        tblcell.Controls.Add(aspTable)
                        tblRow.Cells.Add(tblcell)
                        Table.Rows.Add(tblRow)
                        up = New UpdatePanel
                        apt = New AsyncPostBackTrigger
                        up.ChildrenAsTriggers = True
                        up.UpdateMode = UpdatePanelUpdateMode.Conditional
                        apt.ControlID = "btnEdit"
                        up.ContentTemplateContainer.Controls.Add(Table)
                        up.Triggers.Add(apt)
                        Tab.ContentPane.Controls.Add(up)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub RadGrid1_DetailTableDataBind(ByVal source As Object, ByVal e As Telerik.WebControls.GridDetailTableDataBindEventArgs) Handles RadGrid1.DetailTableDataBind
            Try
                Dim parentItem As Telerik.WebControls.GridDataItem = CType(e.DetailTableView.ParentItem, Telerik.WebControls.GridDataItem)
                If Not parentItem.Edit Then
                    If (e.DetailTableView.DataMember = "SerialNo") Then
                        e.DetailTableView.DataSource = dsTemp.Tables(1)
                        'e.DetailTableView.DataBind()
                        e.DetailTableView.ShowHeadersWhenNoRecords = False
                        e.DetailTableView.NoDetailRecordsText = ""
                    ElseIf (e.DetailTableView.DataMember = "ChildItems") Then
                        e.DetailTableView.DataSource = dsTemp.Tables(2)
                        e.DetailTableView.ShowHeadersWhenNoRecords = False
                        e.DetailTableView.NoDetailRecordsText = ""
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace

