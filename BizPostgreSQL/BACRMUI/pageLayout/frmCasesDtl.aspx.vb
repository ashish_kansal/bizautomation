'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports Infragistics.WebUI.UltraWebTab
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents

Namespace BACRM.UserInterface.Cases
    Public Class frmCasesDtl : Inherits BACRMPage

        Dim objContacts As CContacts
        Dim objPageLayout As CPageLayout
        Dim objContract As CContracts
        Dim objCases As CCases
        Dim objCommon As New CCommon
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim lngCaseId As Long
        Dim lngCntId As Long
        Dim m_aryRightsForEmails(), m_aryRightsForCusFlds(), m_aryRightsForTasks(), m_aryRightsForContacts(), m_aryRightsForLinkSol() As Integer
        Dim strColumn As String

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub





        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Response.Redirect("../cases/frmCases.aspx?" & GetQueryStringVal(Request.QueryString("enc"), "", True))

                If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                    SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                    SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
                Else : SI1 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
                Else : SI2 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                    frm = ""
                    frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
                Else : frm = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                    frm1 = ""
                    frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
                Else : frm1 = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    frm2 = ""
                    frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
                Else : frm2 = ""
                End If
                lngCaseId = GetQueryStringVal(Request.QueryString("enc"), "CaseID")
                m_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCases.aspx", Session("userID"), 7, 5)
                LoadTableInformation()
                If Not IsPostBack Then
                    If objContacts Is Nothing Then objContacts = New CContacts
                    objContacts.RecID = lngCaseId
                    objContacts.Type = "S"
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.AddVisiteddetails()

                    Session("show") = 1
                    Session("Help") = "Support"
                    Dim m_aryRightsForPage() As Integer
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCases.aspx", Session("userID"), 7, 3)
                    ''''call function for sub-tab management  - added on 30jul09 by Mohan
                    'objCommon.ManageSubTabs(uwOppTab, Session("DomainID"), 7)
                    '''''
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else
                        If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False
                        If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnEdit.Visible = False
                    End If
                    Calendar1.SelectedDate = Session("StartDate")
                    Calendar2.SelectedDate = Session("EndDate")

                    calFrom.SelectedDate = Session("StartDate")
                    calTo.SelectedDate = Session("EndDate")
                    ' EmailHistory()
                    'getCorrespondance()
                    m_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCases.aspx", Session("userID"), 7, 5)
                    'LoadTableInformation()
                    LoadTabData()
                    If uwOppTab.Tabs.Count > SI Then uwOppTab.SelectedTabIndex = SI
                    m_aryRightsForLinkSol = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCases.aspx", Session("userID"), 7, 9)
                    If m_aryRightsForLinkSol(RIGHTSTYPE.VIEW) = 0 Then btnLinkSol.Visible = False
                End If

                btnLinkSol.Attributes.Add("onclick", "return openSol(" & lngCaseId & ")")
                btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                btnLayout.Attributes.Add("onclick", "return ShowLayout('S','" & lngCaseId & "');")
                If Session("show") = 1 Then
                    tblComments.Visible = True
                    ShowComments()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTableInformation()
            Try
                Dim dtTableInfo As DataTable
                Dim dtTablecust As DataTable
                Dim ds As New DataSet
                If objPageLayout Is Nothing Then objPageLayout = New CPageLayout
                Dim fields() As String
                Dim idcolumn As String = ""
                ' m_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights("frmCases.aspx", Session("userID"), 7, 5)

                Dim x As Integer
                lngCaseId = GetQueryStringVal(Request.QueryString("enc"), "CaseID")

                objPageLayout.CoType = "S"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngCaseId
                'objPageLayout.ContactID = lngCaseId
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 3
                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)

                Dim dtDetails As DataTable
                objPageLayout.CaseID = lngCaseId
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                'dtDetails = objPageLayout.GetCaseDetails
                hplCustomer.Text = dtDetails.Rows(0).Item("vcCompanyName")
                txtDivId.Text = dtDetails.Rows(0).Item("numDivisionID")
                If dtDetails.Rows(0).Item("tintCRMType") = 0 Then
                    hplCustomer.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseDetail&DivID=" & dtDetails.Rows(0).Item("numDivisionID") & "&CaseID=" & lngCaseId & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")

                ElseIf dtDetails.Rows(0).Item("tintCRMType") = 1 Then
                hplCustomer.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseDetail&DivID=" & dtDetails.Rows(0).Item("numDivisionID") & "&CaseID=" & lngCaseId & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")

                ElseIf dtDetails.Rows(0).Item("tintCRMType") = 2 Then
                hplCustomer.NavigateUrl = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseDetail&klds+7kldf=fjk-las&DivId=" & dtDetails.Rows(0).Item("numDivisionID") & "&CaseID=" & lngCaseId & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")

                End If

                lblcasenumber.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcCaseNumber")), "", dtDetails.Rows(0).Item("vcCaseNumber"))
                lnkbtnNoofCom.Text = "  Comments (" & IIf(IsDBNull(dtDetails.Rows(0).Item("NoofCases")), "", dtDetails.Rows(0).Item("NoofCases")) & " )"
                lblStatus.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("numStatus")), "", dtDetails.Rows(0).Item("numStatus"))
                LoadContractsInfo(dtDetails.Rows(0).Item("numDivisionID"))
                If Not IsPostBack Then
                    If Not ddlContract.Items.FindByValue(dtDetails.Rows(0).Item("numContractId")) Is Nothing Then
                        ddlContract.SelectedItem.Selected = False
                        ddlContract.Items.FindByValue(dtDetails.Rows(0).Item("numContractId")).Selected = True
                    End If
                    BindContractinfo()
                End If

                lngCntId = dtDetails.Rows(0).Item("numContactID")
                btnAddTask.Attributes.Add("onclick", "NewTask('" & lngCntId & "','" & lngCaseId & "')")
                lblCreatedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("CreatedBy")), "", dtDetails.Rows(0).Item("CreatedBy"))
                lblRecordOwner.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("RecOwner")), "", dtDetails.Rows(0).Item("RecOwner"))
                lblLastModifiedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("ModifiedBy")), "", dtDetails.Rows(0).Item("ModifiedBy"))

                Dim numrows As Integer = dtTableInfo.Compute("Max(tintrow)", String.Empty)
                Dim numcells As Integer = dtTableInfo.Compute("Max(intcoulmn)", String.Empty)
                Dim i As Integer = 0
                Dim nr As Integer
                Dim noRowsToLoop As Integer
                noRowsToLoop = (dtTableInfo.Rows.Count - (numrows * numcells)) / numcells
                noRowsToLoop = noRowsToLoop + numrows + 1

                For nr = 0 To noRowsToLoop
                    Dim r As New TableRow()
                    Dim nc As Integer
                    Dim ro As Integer = nr
                    For nc = 1 To numcells
                        If dtTableInfo.Rows.Count <> i Then
                            If dtTableInfo.Rows(i).Item("tintrow") = nr + 1 And dtTableInfo.Rows(i).Item("intcoulmn") = nc Then
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                Dim fieldId As Integer
                                fieldId = CInt(dtTableInfo.Rows(i).Item("numFieldID").ToString)
                                Dim bitDynFld As Boolean
                                bitDynFld = dtTableInfo.Rows(i).Item("bitCustomField")
                                column1.CssClass = "normal7"
                                If (bitDynFld <> True) Then
                                    'column 1 data binding
                                    column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName").ToString) & "&nbsp;:"
                                    'column 2 data binding
                                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName").ToString) And dtTableInfo.Rows(i).Item("vcDBColumnName").ToString() <> "" Then
                                        Dim temp As String
                                        temp = dtTableInfo.Rows(i).Item("vcDBColumnName").ToString
                                        fields = temp.Split(",")
                                        Dim j As Integer
                                        j = 0
                                        While (j < fields.Length)
                                            If (fieldId = "132") Then

                                                Dim listvalue As String
                                                Dim value As String = ""
                                                listvalue = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))

                                                Select Case fieldId   ' Must be a primitive data type
                                                    Case 132
                                                        If Not IsDBNull(dtDetails.Rows(0).Item("intTargetResolveDate")) Then
                                                            value = FormattedDateFromDate(IIf(IsDBNull(dtDetails.Rows(0).Item("intTargetResolveDate")), "", dtDetails.Rows(0).Item("intTargetResolveDate")), Session("DateFormat"))
                                                        End If
                                                    Case Else
                                                End Select
                                                column2.Text = value
                                            ElseIf (fieldId = "135") Then
                                                Dim l As New Label
                                                l.CssClass = "normal1"
                                                l.ForeColor = Color.Red
                                                l.Text = "U" & Format(IIf(IsDBNull(dtDetails.Rows(0).Item("numUniversalSupportKey")), "", dtDetails.Rows(0).Item("numUniversalSupportKey")), "00000000000")
                                                column2.Controls.Add(l)
                                            Else : column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))
                                            End If
                                            j += 1
                                        End While
                                    Else : column1.Text = ""
                                    End If ' end of table cell2
                                Else
                                    If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                        column1.Text = "Custom Web Link :"
                                        Dim h As New HyperLink()
                                        h.CssClass = "hyperlink"
                                        Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                        url = url.Replace("RecordID", lngCaseId)
                                        h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                        column2.Controls.Add(h)
                                    ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                        Dim strDate As String
                                        strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                        If strDate = "0" Then strDate = ""
                                        If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                    Else
                                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                        If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                Dim l As New Label
                                                l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                                l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                column2.Controls.Add(l)
                                            End If
                                        Else
                                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                            End If
                                        End If
                                    End If
                                End If

                                column2.CssClass = "normal1"
                                column1.HorizontalAlign = HorizontalAlign.Right
                                column2.HorizontalAlign = HorizontalAlign.Left
                                column1.Width = 250
                                column2.Width = 300
                                column2.ColumnSpan = 1
                                column1.ColumnSpan = 1
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                                i += 1
                            ElseIf dtTableInfo.Rows(i).Item("tintrow") = 0 And dtTableInfo.Rows(i).Item("intcoulmn") = 0 Then
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                column1.CssClass = "normal7"
                                If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                    column1.Text = "Custom Web Link :"
                                    Dim h As New HyperLink()
                                    h.CssClass = "hyperlink"
                                    Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                    url = url.Replace("RecordID", lngCaseId)
                                    h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                    h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                    column2.Controls.Add(h)
                                ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                    column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                    Dim strDate As String
                                    strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                    If strDate = "0" Then strDate = ""
                                    If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                Else
                                    column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                    If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                        If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                            Dim l As New Label
                                            l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                            l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                            column2.Controls.Add(l)
                                        End If
                                    Else
                                        If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                            column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                        End If
                                    End If
                                End If
                                column2.CssClass = "normal1"
                                column1.HorizontalAlign = HorizontalAlign.Right
                                column2.HorizontalAlign = HorizontalAlign.Left
                                column1.Width = 250
                                column2.Width = 300
                                column2.ColumnSpan = 1
                                column1.ColumnSpan = 1
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                                i += 1
                            Else
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                column1.Text = ""
                                column2.Text = ""
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                            End If
                        End If
                    Next nc
                    tabledetail.Rows.Add(r)
                Next nr

                If (dtDetails.Rows.Count > 0) Then
                    tableComment.Rows.Clear()
                    Dim column1 As TableCell
                    Dim column2 As TableCell
                    Dim r As TableRow
                    column1 = New TableCell
                    column2 = New TableCell
                    r = New TableRow
                    column1.CssClass = "normal7"
                    column2.CssClass = "normal1"
                    column1.HorizontalAlign = HorizontalAlign.Right
                    column2.HorizontalAlign = HorizontalAlign.Justify
                    column2.ColumnSpan = 5
                    Dim l As Label
                    l = New Label
                    l.CssClass = "normal7"
                    l.Text = "Description" & "&nbsp;:"
                    column1.Controls.Add(l)
                    column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("textDesc")), "-", dtDetails.Rows(0).Item("textDesc"))
                    column1.Width = 150
                    r.Cells.Add(column1)
                    r.Cells.Add(column2)
                    tableComment.Rows.Add(r)
                    column1 = New TableCell
                    column2 = New TableCell
                    r = New TableRow
                    column1.CssClass = "normal7"
                    column2.CssClass = "normal1"
                    column1.HorizontalAlign = HorizontalAlign.Right
                    column2.HorizontalAlign = HorizontalAlign.Justify
                    column2.ColumnSpan = 5
                    l = New Label
                    l.CssClass = "normal7"
                    l.Text = "Internal Comments" & "&nbsp;:"
                    column1.Controls.Add(l)
                    column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("textInternalComments")), "-", dtDetails.Rows(0).Item("textInternalComments"))
                    column1.Width = 150
                    r.Cells.Add(column1)
                    r.Cells.Add(column2)
                    tableComment.Rows.Add(r)
                End If

                Dim ObjSolution As New Solution
                Dim dtSolution As DataTable
                ObjSolution.CaseID = lngCaseId
                dtSolution = ObjSolution.GetSolutionForCases
                If dtSolution.Rows.Count > 0 Then
                    pnlKnowledgeBase.Visible = True
                    dgSolution.DataSource = dtSolution
                    dgSolution.DataBind()
                Else : pnlKnowledgeBase.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub AssociatedContacts()
            Try
                If objCases Is Nothing Then objCases = New CCases
                m_aryRightsForContacts = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCases.aspx", Session("userID"), 7, 11)
                If m_aryRightsForContacts(RIGHTSTYPE.VIEW) <> 0 Then
                    objCases.CaseID = lngCaseId
                    objCases.DomainID = Session("DomainID")
                    dgContact.DataSource = objCases.AssCntsByCaseID
                    dgContact.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub EmailHistory()
            Try
                txtEmailPage.Text = 1
                BindEmailData()
                ' LoadTabData()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindEmailData()
            Try
                m_aryRightsForEmails = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCases.aspx", Session("userID"), 7, 4)
                If m_aryRightsForEmails(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                Dim dtEmail As DataTable
                If objCases Is Nothing Then objCases = New CCases
                objCases.DomainID = Session("DomainId")
                objCases.SortOrder = ddlFields.SelectedItem.Value
                objCases.FromDate = calFrom.SelectedDate
                objCases.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))
                objCases.ProCaseNo = lblcasenumber.Text
                objCases.KeyWord = txtSearchEmail.Text
                If txtEmailPage.Text.Trim <> "" Then
                    objCases.CurrentPage = txtEmailPage.Text
                Else : objCases.CurrentPage = 1
                End If
                objCases.PageSize = Session("PagingRows")
                objCases.TotalRecords = 0
                If strColumn <> "" Then
                    objCases.columnName = strColumn
                Else : objCases.columnName = "numEmailHstrID"
                End If
                If Session("Asc") = 1 Then
                    objCases.columnSortOrder = "Desc"
                Else : objCases.columnSortOrder = "Asc"
                End If
                dtEmail = objCases.GetEmailList
                If objCases.TotalRecords = 0 Then
                    tdEmailNav.Visible = False
                    lblEmailNoofRecords.Text = objCases.TotalRecords
                Else
                    tdEmailNav.Visible = True
                    lblEmailNoofRecords.Text = objCases.TotalRecords
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblEmailNoofRecords.Text / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    lblEmailPageNo.Text = strTotalPage(0) + 1
                    txtEmailTotalPage.Text = strTotalPage(0) + 1
                    txtEmailTotalRecords.Text = lblEmailNoofRecords.Text
                End If
                dgEmail.DataSource = dtEmail
                dgEmail.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                If m_aryRightsForCusFlds(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim i, k As Integer
                Dim dtTable As DataTable
                ' Tabstrip3.Items.Clear()

                If objPageLayout Is Nothing Then objPageLayout = New CPageLayout
                objPageLayout.locId = 3
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.RecordId = lngCaseId
                dtTable = objPageLayout.GetCustFlds

                If uwOppTab.Tabs.Count > 4 Then
                    Dim iItemcount As Integer
                    iItemcount = uwOppTab.Tabs.Count
                    While uwOppTab.Tabs.Count > 4
                        uwOppTab.Tabs.RemoveAt(iItemcount - 1)
                        iItemcount = iItemcount - 1
                    End While
                End If

                If dtTable.Rows.Count > 0 Then
                    'CustomField Section
                    Dim Tab As Tab
                    'Dim pageView As PageView
                    Dim aspTable As HtmlTable
                    Dim Table As Table
                    Dim tblcell As TableCell
                    Dim tblRow As TableRow
                    k = 0
                    ViewState("TabId") = dtTable.Rows(0).Item("TabId")
                    ViewState("Check") = 0
                    ViewState("FirstTabCreated") = 0
                    ' Tabstrip3.Items.Clear()
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") <> 0 Then
                            If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                                If ViewState("Check") <> 0 Then
                                    aspTable.Rows.Add(objRow)
                                    tblcell.Controls.Add(aspTable)
                                    tblRow.Cells.Add(tblcell)
                                    Table.Rows.Add(tblRow)
                                    Tab.ContentPane.Controls.Add(Table)
                                End If
                                k = 0
                                ViewState("Check") = 1
                                '  If Not IsPostBack Then
                                ViewState("FirstTabCreated") = 1
                                ViewState("TabId") = dtTable.Rows(i).Item("TabId")
                                Tab = New Tab
                                Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
                                uwOppTab.Tabs.Add(Tab)
                                aspTable = New HtmlTable
                                Table = New Table
                                Table.Width = Unit.Percentage(100)
                                Table.BorderColor = System.Drawing.Color.FromName("black")
                                Table.GridLines = GridLines.None
                                Table.BorderWidth = Unit.Pixel(1)
                                Table.Height = Unit.Pixel(300)
                                Table.CssClass = "aspTable"
                                tblcell = New TableCell
                                tblRow = New TableRow
                                tblcell.VerticalAlign = VerticalAlign.Top
                                aspTable.Width = "100%"
                                objRow = New HtmlTableRow
                                objCell = New HtmlTableCell
                                objCell.InnerHtml = "<br>"
                                objRow.Cells.Add(objCell)
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            'pageView.Controls.Add("")
                            If k = 3 Then
                                k = 0
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If
                            objCell = New HtmlTableCell
                            objCell.Align = "right"
                            objCell.Width = 100
                            objCell.Attributes.Add("class", "normal7")
                            If dtTable.Rows(i).Item("fld_type") <> "Frame" Then
                                If dtTable.Rows(i).Item("fld_type") <> "Link" Then objCell.InnerText = dtTable.Rows(i).Item("fld_label") & " :"
                                objRow.Cells.Add(objCell)
                            End If
                            objCell = New HtmlTableCell
                            objCell.Attributes.Add("class", "normal1")
                            objCell.Align = "left"
                            If dtTable.Rows(i).Item("fld_type") = "Link" Then
                                Dim h As New HyperLink
                                h.CssClass = "hyperlink"
                                Dim URL As String = IIf(IsDBNull(dtTable.Rows(i).Item("vcURL")), "", dtTable.Rows(i).Item("vcURL"))
                                URL = URL.Replace("RecordID", lngCaseId)
                                h.Text = IIf(IsDBNull(dtTable.Rows(i).Item("fld_label")), "", dtTable.Rows(i).Item("fld_label"))
                                h.Attributes.Add("onclick", "fn_GoToURL('" & "http://" & URL & "')")
                                objCell.Controls.Add(h)
                                'CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngCaseId, dtTable.Rows(i).Item("fld_label"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = IIf(dtTable.Rows(i).Item("Value") = "1", "a", "r")
                                    l.CssClass = IIf(dtTable.Rows(i).Item("Value") = "1", "cell1", "cell")
                                    objCell.Controls.Add(l)
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = "r"
                                    l.CssClass = "cell"
                                    objCell.Controls.Add(l)
                                End If
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
                                objCell = New HtmlTableCell
                                Dim strFrame As String
                                Dim URL As String
                                URL = dtTable.Rows(i).Item("vcURL")
                                URL = URL.Replace("RecordID", lngCaseId)
                                strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                objCell.Controls.Add(New LiteralControl(strFrame))
                                objRow.Cells.Add(objCell)
                            Else
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = IIf(dtTable.Rows(i).Item("Value") = "0" Or dtTable.Rows(i).Item("Value") = Nothing, "-", dtTable.Rows(i).Item("Value"))
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = "-"
                                End If
                            End If
                            objRow.Cells.Add(objCell)
                            k = k + 1
                        End If
                    Next
                    If ViewState("Check") = 1 Then
                        aspTable.Rows.Add(objRow)
                        tblcell.Controls.Add(aspTable)
                        tblRow.Cells.Add(tblcell)
                        Table.Rows.Add(tblRow)
                        Tab.ContentPane.Controls.Add(Table)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "Solution" Then
                    Response.Redirect("../cases/frmSolution.aspx?SolId=" & GetQueryStringVal(Request.QueryString("enc"), "SolId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "tickler" Then
                    Response.Redirect("../common/frmticklerdisplay.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "Index") <> "" Then
                    Response.Redirect("../cases/frmCaseList.aspx?Index=" & GetQueryStringVal(Request.QueryString("enc"), "Index") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "accounts" Then
                    Dim objCommon As New CCommon
                    objCommon.CaseID = lngCaseId
                    objCommon.charModule = "S"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../Account/frmAccounts.aspx?klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&CaseID=" & lngCaseId & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CaseReport" Then
                    Response.Redirect("../reports/frmCases.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AdvSer" Then
                    Response.Redirect("../Admin/frmAdvCaseRes.aspx")
                Else : Response.Redirect("../cases/frmCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnEmailGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailGo.Click
            Try
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub linkEmailLast_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkEmailLast.Click
            Try
                txtEmailPage.Text = txtEmailTotalPage.Text
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmailPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmailPrevious.Click
            Try
                If txtEmailPage.Text = 1 Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text - 1
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmailNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmailNext.Click
            Try
                If txtEmailPage.Text = txtEmailTotalPage.Text Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text + 1
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmailFirst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmailFirst.Click
            Try
                txtEmailPage.Text = 1
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmail2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmail2.Click
            Try
                If txtEmailPage.Text + 1 = txtEmailTotalPage.Text Or txtEmailPage.Text + 1 > txtEmailTotalPage.Text Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text + 2
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmail4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmail4.Click
            Try
                If txtEmailPage.Text + 3 = txtEmailTotalPage.Text Or txtEmailPage.Text + 3 > txtEmailTotalPage.Text Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text + 4
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmail5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmail5.Click
            Try
                If txtEmailPage.Text + 4 = txtEmailTotalPage.Text Or txtEmailPage.Text + 4 > txtEmailTotalPage.Text Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text + 5
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmal3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmal3.Click
            Try
                If txtEmailPage.Text + 2 = txtEmailTotalPage.Text Or txtEmailPage.Text + 2 > txtEmailTotalPage.Text Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text + 3
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub txtEmailPage_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmailPage.TextChanged
            Try
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEmail_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgEmail.SortCommand
            Try
                strColumn = e.SortExpression.ToString()
                If Session("Column") <> strColumn Then
                    Session("Column") = strColumn
                    Session("Asc") = 0
                Else
                    If Session("Asc") = 0 Then
                        Session("Asc") = 1
                    Else : Session("Asc") = 0
                    End If
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEmail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgEmail.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    e.Item.Cells(2).Attributes.Add("onclick", "return OpenEmailMessage('" & e.Item.Cells(1).Text & "','" & e.Item.Cells(0).Text & "')")
                    e.Item.Cells(2).Attributes.Add("class", "hyperlink")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub lnkbtnNoofCom_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkbtnNoofCom.Command
            Try
                If e.CommandName = "Show" Then
                    tblComments.Visible = True
                    pnlPostComment.Visible = False
                    ShowComments()
                    Session("show") = 1
                    LoadTabData()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnlbtnPost_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles lnlbtnPost.Command
            Try
                If e.CommandName = "Post" Then
                    tblComments.Visible = False
                    pnlPostComment.Visible = True
                    txtComments.Text = ""
                    txtHeading.Text = ""
                    txtLstUptCom.Text = ""
                    LoadTabData()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub ShowComments()
            Try
                tblComments.Rows.Clear()
                Dim dtCaseComments As DataTable
                If objCases Is Nothing Then objCases = New CCases
                objCases.CaseID = lngCaseId
                objCases.byteMode = 0
                dtCaseComments = objCases.ManageCaseComments
                Dim i As Integer
                If dtCaseComments.Rows.Count > 0 Then
                    Dim tblRow As TableRow
                    Dim tblCell As TableCell
                    Dim lbl As Label
                    Dim btnDelete As Button
                    Dim btnUpdate As Button
                    Dim k As Integer = 0
                    For i = 0 To dtCaseComments.Rows.Count - 1
                        tblRow = New TableRow
                        If k <> 1 Then
                            tblRow.CssClass = "tr1"
                        Else : tblRow.CssClass = "tr2"
                        End If
                        tblCell = New TableCell
                        tblCell.Text = dtCaseComments.Rows(i).Item("vcHeading")
                        tblCell.CssClass = "text_bold"
                        tblRow.Cells.Add(tblCell)
                        tblCell = New TableCell
                        If dtCaseComments.Rows(i).Item("numCommentID") = IIf(txtLstUptCom.Text = "", 0, txtLstUptCom.Text) Then
                            btnUpdate = New Button
                            btnUpdate.Text = "Edit"
                            AddHandler btnUpdate.Click, AddressOf Me.btnUpdate_Click
                            btnUpdate.Width = Unit.Pixel(50)
                            btnUpdate.ID = "btnUpdate~" & dtCaseComments.Rows(i).Item("numCommentID")
                            btnUpdate.CssClass = "button"
                            tblCell.Controls.Add(btnUpdate)
                        End If
                        lbl = New Label
                        lbl.Text = "&nbsp;&nbsp;"
                        tblCell.Controls.Add(lbl)

                        btnDelete = New Button
                        btnDelete.Text = "Delete"
                        AddHandler btnDelete.Click, AddressOf Me.btnDeleteComm_Click
                        btnDelete.Width = Unit.Pixel(50)
                        btnDelete.ID = dtCaseComments.Rows(i).Item("numCommentID") * 100
                        btnDelete.CssClass = "button"
                        tblCell.Controls.Add(btnDelete)
                        lbl = New Label
                        lbl.Text = "&nbsp;&nbsp;"
                        tblCell.Controls.Add(lbl)

                        tblCell.HorizontalAlign = HorizontalAlign.Right
                        tblRow.Cells.Add(tblCell)
                        tblComments.Rows.Add(tblRow)

                        tblRow = New TableRow
                        If k <> 1 Then
                            tblRow.CssClass = "tr1"
                        Else : tblRow.CssClass = "tr2"
                        End If
                        tblCell = New TableCell
                        tblCell.ColumnSpan = 2
                        tblCell.Text = dtCaseComments.Rows(i).Item("txtComments")
                        tblCell.CssClass = "normal1"
                        tblRow.Cells.Add(tblCell)
                        tblComments.Rows.Add(tblRow)
                        If k <> 0 Then
                            k = 0
                        Else : k = 1
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            Try
                pnlPostComment.Visible = False
                tblComments.Visible = True
                If objCases Is Nothing Then objCases = New CCases
                objCases.CaseID = lngCaseId
                objCases.Comments = txtComments.Text
                objCases.Heading = txtHeading.Text
                objCases.ContactID = Session("UserContactID")
                objCases.byteMode = 1
                If txtLstUptCom.Text <> "" Then objCases.CommentID = txtLstUptCom.Text
                txtLstUptCom.Text = objCases.ManageCaseComments().Rows(0).Item(0)
                ShowComments()

                tabledetail.Rows.Clear()
                LoadTableInformation()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub btnDeleteComm_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                If objCases Is Nothing Then objCases = New CCases
                objCases.CaseID = lngCaseId
                objCases.byteMode = 2
                objCases.CommentID = (sender.id) / 100
                objCases.ManageCaseComments()
                ShowComments()

                tabledetail.Rows.Clear()
                LoadTableInformation()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                pnlPostComment.Visible = True
                tblComments.Visible = False
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                If objCases Is Nothing Then objCases = New CCases
                objCases.CaseID = lngCaseId
                objCases.DomainID = Session("DomainID")
                If objCases.DeleteCase() = False Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else : Response.Redirect("../cases/frmCaseList.aspx")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .CompFilter = Trim(strName) & "%"
                    ddlCombo.DataSource = fillCombo.ListCustomer().Tables(0).DefaultView
                    ddlCombo.DataTextField = "vcCompanyname"
                    ddlCombo.DataValueField = "numDivisionID"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContact.ItemCommand

        End Sub

        Private Sub dgContact_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContact.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    If e.Item.Cells(1).Text = 1 Then
                        CType(e.Item.FindControl("lblShare"), Label).Text = "a"
                        CType(e.Item.FindControl("lblShare"), Label).CssClass = "cell1"
                    Else
                        CType(e.Item.FindControl("lblShare"), Label).Text = "r"
                        CType(e.Item.FindControl("lblShare"), Label).CssClass = "cell"
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            Try
                'Response.Redirect("../pagelayout/frmCaseEdit.aspx?frm=Casedtl&CaseID=" & lngCaseId)
                Response.Redirect("../cases/frmCases.aspx?frm=Casedtl&CaseID=" & lngCaseId & "&SI=" & uwOppTab.SelectedTabIndex & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm & "&frm2=" & frm1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadContractsInfo(ByVal intDivId As Integer)
            Try
                If Not IsPostBack Then
                    If objContract Is Nothing Then objContract = New CContracts
                    Dim dtTable As DataTable
                    objContract.DivisionId = txtDivId.Text
                    objContract.UserCntId = Session("UserContactId")
                    objContract.DomainId = Session("DomainId")
                    dtTable = objContract.GetContractDdlList()
                    ddlContract.DataSource = dtTable
                    ddlContract.DataTextField = "vcContractName"
                    ddlContract.DataValueField = "numcontractId"
                    ddlContract.DataBind()
                    ddlContract.Items.Insert(0, "--Select One--")
                    ddlContract.Items.FindByText("--Select One--").Value = "0"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlContract_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContract.SelectedIndexChanged
            Try
                BindContractinfo()
                LoadTabData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindContractinfo()
            Try
                If objContract Is Nothing Then objContract = New CContracts
                Dim dtTable As DataTable
                objContract.ContractID = ddlContract.SelectedValue
                objContract.UserCntId = Session("UserContactId")
                objContract.DomainId = Session("DomainId")
                dtTable = objContract.GetContractDtl()
                If dtTable.Rows.Count > 0 Then
                    lblRemAmount.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemAmount")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemAmount")))
                    lblRemHours.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemHours")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemHours")))
                    lblRemDays.Text = IIf(IsDBNull(dtTable.Rows(0).Item("days")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("days")))
                    If dtTable.Rows(0).Item("bitincidents") = True Then
                        lblRemInci.Text = IIf(IsDBNull(dtTable.Rows(0).Item("Incidents")), 0, dtTable.Rows(0).Item("Incidents"))
                    Else : lblRemInci.Text = 0
                    End If
                Else
                    lblRemAmount.Text = "0"
                    lblRemHours.Text = "0"
                    lblRemDays.Text = "0"
                    lblRemInci.Text = "0"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCorresGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorresGo.Click
            Try
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rptCorr_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptCorr.ItemCommand
            Try
                If e.CommandName = "Sort" Then
                    If e.CommandSource.ID = "lnkDate" Then
                        strColumn = "Date"
                    ElseIf e.CommandSource.ID = "lnkType" Then
                        strColumn = "Type"
                    ElseIf e.CommandSource.ID = "lnkFrom" Then
                        strColumn = "[From]"
                    ElseIf e.CommandSource.ID = "lnkName" Then
                        strColumn = "phone"
                    ElseIf e.CommandSource.ID = "lnkAssigned" Then
                        strColumn = "assignedto"
                    End If
                End If
                If Session("Column") <> strColumn Then
                    Session("Column") = strColumn
                    Session("Asc") = 0
                Else
                    If Session("Asc") = 0 Then
                        Session("Asc") = 1
                    Else : Session("Asc") = 0
                    End If
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlFilterCorr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterCorr.SelectedIndexChanged
            Try
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnCorrDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorrDelete.Click
            Try
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim chk As CheckBox
                Dim lbl As Label
                For Each r As RepeaterItem In rptCorr.Items
                    chk = r.FindControl("chkADelete")
                    If chk.Checked = True Then
                        lbl = r.FindControl("lblDelete")
                        If lbl.Text.Split("~")(1) = 1 Then
                            objContacts.EmailHstrID = lbl.Text.Split("~")(0)
                            objContacts.tinttype = 1
                        Else
                            objContacts.EmailHstrID = lbl.Text.Split("~")(0)
                            objContacts.tinttype = 2
                        End If
                        objContacts.DelCorrespondence()
                    End If
                Next
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub getCorrespondance()
            Try
                m_aryRightsForTasks = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCases.aspx", Session("userID"), 7, 10)
                If m_aryRightsForTasks(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If m_aryRightsForTasks(RIGHTSTYPE.DELETE) = 0 Then btnCorrDelete.Visible = False
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim dttable As DataTable
                objContacts.FromDate = Calendar1.SelectedDate
                objContacts.ToDate = DateAdd(DateInterval.Day, 1, CDate(Calendar2.SelectedDate))
                objContacts.ContactID = 0
                objContacts.CaseID = lngCaseId
                objContacts.MessageFrom = ""
                objContacts.SortOrder1 = ddlFilterCorr.SelectedValue
                objContacts.UserCntID = Session("UserContactID")
                objContacts.SortOrder = ddlSrchCorr.SelectedValue
                objContacts.KeyWord = txtSearchCorr.Text
                objContacts.DomainID = Session("DomainID")
                objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                If txtCurrentPageCorr.Text.Trim <> "" Then
                    objContacts.CurrentPage = txtCurrentPageCorr.Text
                Else : objContacts.CurrentPage = 1
                End If
                objContacts.PageSize = Session("PagingRows")
                objContacts.TotalRecords = 0
                If strColumn <> "" Then
                    objContacts.columnName = strColumn
                Else : objContacts.columnName = "date"
                End If
                If Session("Asc") = 1 Then
                    objContacts.columnSortOrder = "Desc"
                Else : objContacts.columnSortOrder = "Asc"
                End If
                dttable = objContacts.getCorres()
                If objContacts.TotalRecords = 0 Then
                    tdCorr.Visible = False
                    lblNoOfRecordsCorr.Text = objContacts.TotalRecords
                Else
                    tdCorr.Visible = True
                    lblNoOfRecordsCorr.Text = String.Format("{0:#,###}", objContacts.TotalRecords)
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblNoOfRecordsCorr.Text / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (lblNoOfRecordsCorr.Text Mod Session("PagingRows")) = 0 Then
                        lblTotalCorr.Text = strTotalPage(0)
                        txtCorrTotalPage.Text = strTotalPage(0)
                    Else
                        lblTotalCorr.Text = strTotalPage(0) + 1
                        txtCorrTotalPage.Text = strTotalPage(0) + 1
                    End If
                    txtCorrTotalRecords.Text = lblNoOfRecordsCorr.Text
                End If
                rptCorr.DataSource = dttable
                rptCorr.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub lnk2Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2Corr.Click
            Try
                If txtCurrentPageCorr.Text + 1 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 1 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 2
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk3Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3Corr.Click
            Try
                If txtCurrentPageCorr.Text + 2 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 2 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 3
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk4Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4Corr.Click
            Try
                If txtCurrentPageCorr.Text + 3 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 3 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 4
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk5Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5Corr.Click
            Try
                If txtCurrentPageCorr.Text + 4 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 4 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 5
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkFirstCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFirstCorr.Click
            Try
                txtCurrentPageCorr.Text = 1
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkLastCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLastCorr.Click
            Try
                txtCurrentPageCorr.Text = txtCorrTotalPage.Text
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkNextCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextCorr.Click
            Try
                If txtCurrentPageCorr.Text = txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 1
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkPreviousCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreviousCorr.Click
            Try
                If txtCurrentPageCorr.Text = 1 Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text - 1
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rptCorr_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCorr.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Then
                    Dim chk As CheckBox
                    Dim lbl As Label
                    chk = e.Item.FindControl("chkADelete")
                    lbl = e.Item.FindControl("lblDelete")
                    If m_aryRightsForTasks(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If lbl.Text.Split("~")(2) <> Session("UserContactID") Then chk.Visible = False
                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub uwOppTab_TabClick(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebTab.WebTabEvent) Handles uwOppTab.TabClick
            Try
                LoadTabData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTabData()
            Try
                Select Case uwOppTab.SelectedTabIndex
                    Case 0 : DisplayDynamicFlds()
                    Case 3 : EmailHistory()
                    Case 1 : AssociatedContacts()
                    Case 2 : getCorrespondance()
                    Case 4 : DisplayDynamicFlds()
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace

