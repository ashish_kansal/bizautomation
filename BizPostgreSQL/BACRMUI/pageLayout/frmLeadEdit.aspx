<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmLeadEdit.aspx.vb"
    Inherits="BACRM.UserInterface.Leads.frmLeadEdit" %>

<%@ Register TagPrefix="menu1" TagName="Menu" Src="../include/webmenu.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Lead Details</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <link href="../css/lists.css" type="text/css" rel="STYLESHEET">

    <script language="JavaScript" src="../javascript/date-picker.js" type="text/javascript"></script>

    <script language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
        function OpenEmailMessage(a, b) {
            window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email=" + a + "&Date=" + b, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }


        function openFollow(a) {
            window.open("../Leads/frmFollowUpHstr.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenAdd(a) {
            window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
        function fn_GoToURL(varURL) {
            var url = document.getElementById('uwOppTab$_ctl0$' + varURL).value
            if ((url != '') && (url.substr(0, 7) == 'http://') && (url.length > 7)) {
                var LoWindow = window.open(url, "", "");
                LoWindow.focus();
            }
            return false;
        }

        function FillAddress(a) {
            if (document.all) {
                document.getElementById('uwOppTab__ctl0_lblAddress').innerText = a;
            } else {
                document.getElementById('uwOppTab__ctl0_lblAddress').textContent = a;
            }
            return false;
        }
        function fn_SendMail(a) {
            if (document.Form1.uwOppTab$_ctl0$txtEmail.value != '') {
                window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + document.Form1.uwOppTab$_ctl0$txtEmail.value + '&pqwRT=' + a, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            }
            return false;
        }
        function CheckNumber() {
            if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                window.event.keyCode = 0;
            }
        }
        function GoOrgDetails(numDivisionId) {
            frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionEntity').value = 'OrganizationFromDiv';
            frames['IfrOpenOrgContact'].document.getElementById('hdDivisionId').value = numDivisionId;
            frames['IfrOpenOrgContact'].document.forms['frmOrgContactRedirect'].submit();
        }
        function CheckTabSel(a) {
            if (document.getElementById('uwOppTab').selectedIndex == 4 && a == 0) {
                document.Form1.submit()
            }
            return false;
        }
        function Save() {
            if (document.Form1.uwOppTab$_ctl0$ddlGroup.value == 0) {
                alert("Please Select Group")
                document.Form1.uwOppTab$_ctl0$ddlGroup.focus();
                return false;
            }
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:Menu ID="webmenu1" runat="server"></menu1:Menu>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <table id="tblMenu" bordercolor="black" cellspacing="0" cellpadding="0" width="100%"
                            border="0" runat="server">
                            <tr>
                                <td class="tr1" align="center">
                                    <b>Record Owner: </b>
                                    <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Created By: </b>
                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Last Modified By: </b>
                                    <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal1">
                                    Organization ID :
                                    <asp:Label ID="lblCustID" runat="Server"></asp:Label>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblAssociation" runat="Server"></asp:Label>
                                    <iframe id="IfrOpenOrgContact" src="../Marketing/frmOrgContactRedirect.aspx" frameborder="0"
                                        width="10" scrolling="no" height="10" left="0" right="0"></iframe>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button"></asp:Button>
                                    <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close">
                                    </asp:Button>
                                    <asp:Button ID="btnCancel" Text="Close" runat="server" CssClass="button"></asp:Button>
                                    <asp:Button ID="btnActdelete" runat="server" CssClass="Delete" Text="X"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <igtab:UltraWebTab ImageDirectory="" ID="uwOppTab" runat="server" ThreeDEffect="True"
                            BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                            <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                            </DefaultTabStyle>
                            <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                                NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                                FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <SelectedTabStyle Height="23px" ForeColor="white">
                            </SelectedTabStyle>
                            <HoverTabStyle Height="23px" ForeColor="white">
                            </HoverTabStyle>
                            <Tabs>
                                <igtab:Tab Text="&nbsp;&nbsp;Lead Details&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="Table5" runat="server" BorderWidth="1" Width="100%" Height="300" BorderColor="black"
                                            CssClass="aspTableDTL" GridLines="None">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br>
                                                    <table width="100%" id="tblDetails" runat="server">
                                                        <tr>
                                                            <td colspan="2">
                                                                <table>
                                                                    <tr>
                                                                        <td rowspan="30" valign="top">
                                                                            <img src="../images/Building-48.gif" />
                                                                        </td>
                                                                        <td class="normal1" align="right">
                                                                            Organization
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtCompanyName" CssClass="signup" runat="server" Width="180px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Phone/Fax
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtComPhone" runat="server" CssClass="signup" Width="90" TabIndex="7"></asp:TextBox>&nbsp;
                                                                            <asp:TextBox ID="txtComFax" runat="server" CssClass="signup" Width="90" TabIndex="8"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Profile
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlProfile" CssClass="signup" runat="server" Width="180px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Relationship
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlRelationhip" runat="server" AutoPostBack="True" CssClass="signup"
                                                                                Width="180">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Campaign
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlCampaign" runat="Server" CssClass="signup" Width="180px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Territory
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlTerritory" CssClass="signup" runat="server" Width="180px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:Table ID="Table7" runat="server" BorderWidth="1" Width="100%" BorderColor="black"
                                                                    GridLines="None">
                                                                    <asp:TableRow>
                                                                        <asp:TableCell VerticalAlign="Top">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="normal1" align="right" nowrap>
                                                                                        First/Last Name<font color="red">*</font>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtFirstname" CssClass="signup" runat="server" Width="90"></asp:TextBox>&nbsp;
                                                                                        <asp:TextBox ID="txtLastName" CssClass="signup" runat="server" Width="90"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="normal1" align="right">
                                                                                        Phone/Ext
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtPhone" CssClass="signup" runat="server" Width="130"></asp:TextBox>&nbsp;
                                                                                        <asp:TextBox ID="txtExt" CssClass="signup" runat="server" Width="50px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="normal1" align="right">
                                                                                        Email
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtEmail" CssClass="signup" runat="server" Width="145px"></asp:TextBox>&nbsp;
                                                                                        <asp:Button ID="btnEmailGo" CssClass="button" Text="Go" runat="server" Width="25">
                                                                                        </asp:Button>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="normal1" align="right">
                                                                                        Position
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlPosition" CssClass="signup" runat="server" Width="180">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="normal1" align="right">
                                                                                        Title
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtTitle" runat="server" Width="180" TabIndex="20" CssClass="signup"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnContactdetails" runat="server" CssClass="button" Text="Contact Details">
                                                                                        </asp:Button>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:TableCell>
                                                                    </asp:TableRow>
                                                                </asp:Table>
                                                            </td>
                                                            <td colspan="2">
                                                                <table>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Assigned To
                                                                        </td>
                                                                        <td class="normal1">
                                                                            <asp:DropDownList ID="ddlAssignedTo" CssClass="signup" runat="server" Width="180px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Employees
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlNoOfEmp" CssClass="signup" runat="server" Width="180px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Annual Revenue
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlAnnualRev" CssClass="signup" runat="server" Width="180px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Web URL
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtWebURL" Text="http://" CssClass="signup" runat="server" Width="145px"></asp:TextBox>&nbsp;
                                                                            <asp:Button ID="btnWebGo" CssClass="button" Text="Go" runat="server" Width="25">
                                                                            </asp:Button>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Follow-up Status
                                                                        </td>
                                                                        <td class="normal1">
                                                                            <asp:DropDownList ID="ddlFollow" CssClass="signup" runat="server" Width="180px">
                                                                            </asp:DropDownList>
                                                                            <asp:HyperLink ID="hplFollowUpHstr" runat="server" CssClass="hyperlink">
																		Hstr</asp:HyperLink>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Group<font color="red">*</font>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlGroup" CssClass="signup" runat="server" Width="180px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            Info. Source
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlInfoSource" CssClass="signup" runat="server" Width="180px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                <asp:HyperLink ID="hplAddress" runat="server" CssClass="hyperlink">
															Address</asp:HyperLink>
                                                            </td>
                                                            <td class="normal1" colspan="5">
                                                                <asp:Label ID="lblAddress" runat="server" Width="100%" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right" valign="top" width="100">
                                                                Comments
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:TextBox ID="txtComments" runat="server" CssClass="signup" Height="50" Width="400"
                                                                    TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="&nbsp;&nbsp;Areas of Interest&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="Table2" runat="server" BorderWidth="1" Width="100%" GridLines="None"
                                            BorderColor="black" CssClass="aspTable" Height="300">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top" CssClass="normal1">
                                                    <br>
                                                    <asp:CheckBoxList ID="chkAOI" CellSpacing="20" runat="server" RepeatColumns="3" RepeatDirection="Vertical">
                                                    </asp:CheckBoxList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                            </Tabs>
                        </igtab:UltraWebTab>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalPage" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalRecords" runat="server" Style="display: none"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
