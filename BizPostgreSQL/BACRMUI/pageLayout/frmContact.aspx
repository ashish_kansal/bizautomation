<%@ Page Language="vb" EnableEventValidation="false" AutoEventWireup="false" CodeBehind="frmContact.aspx.vb"
    Inherits="BACRM.UserInterface.Contacts.frmContacts1" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Contacts</title>
    <link href="../css/lists.css" type="text/css" rel="STYLESHEET" />

    <script language="JavaScript" src="../javascript/date-picker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email=" + a + "&Date=" + f, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
                return false;
            }
            else {
                window.location.href = "../admin/actionitemdetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetails&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
                return false;
            }

        }
        function chkAll() {
            for (i = 1; i <= 20; i++) {
                var str;
                if (i < 10) {
                    str = '0' + i
                }
                else {
                    str = i
                }
                if (document.getElementById('uwOppTab__ctl4_rptCorr_ctl00_chkDelete').checked == true) {
                    if (document.getElementById('uwOppTab__ctl4_rptCorr_ctl' + str + '_chkADelete') != null) {
                        document.getElementById('uwOppTab__ctl4_rptCorr_ctl' + str + '_chkADelete').checked = true;
                    }
                }
                else {
                    if (document.getElementById('uwOppTab__ctl4_rptCorr_ctl' + str + '_chkADelete') != null) {
                        document.getElementById('uwOppTab__ctl4_rptCorr_ctl' + str + '_chkADelete').checked = false;
                    }
                }
            }
        }
        function fn_GoToURL(varURL) {

            if ((varURL != '') && (varURL.substr(0, 7) == 'http://') && (varURL.length > 7)) {
                var LoWindow = window.open(varURL, "", "");
                LoWindow.focus();
            }
            return false;
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function OpenAdd(CntID) {
            window.open("../contact/frmContactAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pqwRT=" + CntID, '', 'toolbar=no,titlebar=no,top=300,width=850,height=350,scrollbars=no,resizable=no')
            return false;
        }
        function OpenEmailMessage(a, b) {
            window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email=" + a + "&Date=" + b, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
            return false;
        }
        function OpenListView(a) {
            window.open("../admin/frmEmailUsers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ContID=" + a, '', 'width=650,height=350,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
            return false;
        }
        function OpenECamp(a) {
            window.open("../Marketing/frmConECamDtls.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pqwRT=" + a, '', 'width=500,height=300,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
            return false;
        }
        function OpenECampHstr(a) {
            window.open("../Marketing/frmConECampHstr.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pqwRT=" + a, '', 'width=650,height=350,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
            return false;
        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                window.location.reload(true);
                //return false;

            }

        }
        function ShowWindowAddress(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                for (i = 60; i < tblMain.all.length; i++) {
                    if (tblMain.all[i] != null) {
                        if (tblMain.all[i].type == 'select-one') {
                            tblMain.all[i].style.visibility = "hidden";
                        }
                    }

                }
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                for (i = 60; i < tblMain.all.length; i++) {
                    if (tblMain.all[i] != null) {
                        if (tblMain.all[i].type == 'select-one') {
                            tblMain.all[i].style.visibility = "visible";
                        }
                    }

                }
                return false;

            }

        }
        function ShowWindow1(Page, q, att, a) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                if (a == 1) {
                    return true;
                }
                else {
                    return false;
                }


            }

        }
        function fn_SendMail(txtMailAddr, a, b) {

            if (txtMailAddr.value != '') {
                if (a == 1) {

                    window.open('mailto:' + txtMailAddr.value);
                }
                else if (a == 2) {
                    window.open('../common/callemail.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + txtMailAddr.value + '&ContID=' + b, 'mail');
                }

            }

            return false;
        }
        function fn_Mail(txtMailAddr, a, b) {
            if (txtMailAddr != '') {
                if (a == 1) {

                    window.open('mailto:' + txtMailAddr);
                }
                else if (a == 2) {
                    window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + txtMailAddr + '&pqwRT=' + b, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')


                }

            }

            return false;
        }
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=C&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTo(a) {
            window.open("../admin/frmcomAssociationTo.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DivId=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenFrom(a) {
            window.open("../admin/frmCompanyAssociationFrom.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenLast10OpenAct(a) {
            window.open("../admin/DisplayActionItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&cntid=" + a + "&type=1", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenLst10ClosedAct(a) {
            window.open("../admin/DisplayActionItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&cntid=" + a + "&type=2", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function Disableddl() {
            if (Tabstrip2.selectedIndex == 0) {
                document.Form1.ddlOppStatus.style.display = "none";
            }
            else {
                document.Form1.ddlOppStatus.style.display = "";
            }
        }
        ////////////////////////////////////////////////////////////////
        /////////////////////////  SURVEY HISTORY //////////////////////
        ////////////////////////////////////////////////////////////////
        /*
        Purpose:	The processing required to view the Survey History Details
        Created By: Debasish Tapan Nag
        Parameter:	1) numSurId
        Return		1) None
        */
        function EditSurveyResult(numSurId, numRespondentId) {
            var sURL = '../contact/frmContactsSurveyResponses.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numSurID=' + numSurId + '&numRespondentId=' + numRespondentId,
			hndSurveyResponsePopUpURL = window.open(sURL, '', 'toolbar=no,titlebar=no,left=190, top=350,width=800,height=270,scrollbars=yes,resizable=yes');
            hndSurveyResponsePopUpURL.focus();
        }
        function Save() {
            if (document.Form1.txtFirstname.value == "") {
                alert("Enter First Name")
                document.Form1.txtFirstname.focus();
                return false;
            }
            if (document.Form1.txtLastName.value == "") {
                alert("Enter Last Name")
                document.Form1.txtLastName.focus();
                return false;
            }
            if (document.Form1.txtContactType.value == 70) {
                if (document.Form1.ddlType.value != 70) {
                    alert("You can't change the contact type 'Primary Contact' from this record, because there must be  one Primary Contact for every Organization record. Go to another contact record within the same Organization, and change its 'type' value to Primary Contact.")
                    document.Form1.ddlType.focus();
                    return false;
                }
            }
        }
        function Export() {
            if (document.Form1.txtFirstname.value == "") {
                alert("Enter First Name")
                document.Form1.txtFirstname.focus();
                return false;
            }
            if (document.Form1.txtLastName.value == "") {
                alert("Enter Last Name")
                document.Form1.txtLastName.focus();
                return false;
            }
            if (document.Form1.txtEmail.value == "") {
                alert("Enter Email")
                document.Form1.txtEmail.focus();
                return false;
            }
        }
        function FillAddress(a) {
            document.getElementById('uwOppTab__ctl0_lblAddress').innerText = a;
            return false;
        }
        ////////////////////////////////////////////////////////////////
        /////////////////////////  MOVE CONTACTS  //////////////////////
        ////////////////////////////////////////////////////////////////
        /*
        Purpose:	Opens the Contact Merge wscreen in a popup
        Created By: Debasish Tapan Nag
        Parameter:	1) numContactId: The Contact Id
        Return		1) None
        */
        function OpenMergeCopyWindow(numContactId, frmScreen) {
            window.open("../contact/frmMoveContact.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" + frmScreen + '&numContactId=' + numContactId, 'MoveContacts', 'toolbar=no,titlebar=no,top=300,width=500,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTmeAndExp(a, b) {
            window.location.href = "../TimeAndExpense/frmEmpCal.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CntID=" + a + "&frm=" + b;
            return false;
        }
        function ShowLayout(a, b, c) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=" + a + "&type=" + c, '', 'toolbar=no,titlebar=no,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        ////////////////////////////////////////////////// new scripts added by Tarun////////////////////////////////////////////
        function GetSelectAllCheck() {
            var objCheck = document.getElementById('dgEmail_ctl01_selectAllOptions') // .net generated name is dgEmail__ctl1_selectAllOptions
            if (objCheck.checked == true) {
                SelectAllCheckBox();
                return true;
            }
            else {
                UnSelectAllCheckBox();
                return false;
            }
        }

        function SelectAllCheckBox() {
            var objAllCheck = document.getElementsByTagName('input');
            var totalCount = objAllCheck.length
            var i
            for (i = 0; i < totalCount; i++) {
                if (objAllCheck[i] != null && objAllCheck[i].type == 'checkbox') {
                    //if( objAllCheck[i].id.indexOf("listChecks") > = 0  ) 
                    {
                        objAllCheck[i].checked = true;
                    }
                }
            }
        }

        function UnSelectAllCheckBox() {
            var objAllCheck = document.getElementsByTagName('input');
            var totalCount = objAllCheck.length
            var i
            for (i = 0; i < totalCount; i++) {
                if (objAllCheck[i] != null && objAllCheck[i].type == 'checkbox') {
                    //if( objAllCheck[i].id.indexOf("listChecks") > = 0  ) 
                    {
                        objAllCheck[i].checked = false;
                    }
                }
            }
        }

        // use script below to Fire a pertiular button event just you need to pass 
        // event objet ------ we are using to identify key code 
        // Button Id that we have to click 
        function Click_Button(eventObject, buttonID) {
            var objButton = document.getElementById(buttonID);
            if (objButton != null && event.keyCode == 13) {
                objButton.click();
            }
        } // end function


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <table bordercolor="black" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <td class="tr1" align="center">
                                <b>Record Owner : </b>
                                <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                            </td>
                            <td class="td1" width="1" height="18">
                            </td>
                            <td class="tr1" align="center">
                                <b>Created By : </b>
                                <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                            </td>
                            <td class="td1" width="1" height="18">
                            </td>
                            <td class="tr1" align="center">
                                <b>Last Modified By : </b>
                                <asp:Label ID="lblModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                            </td>
                </tr>
            </table>
            </td> </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td class="normal1" align="center">
                                Organization : <u>
                                    <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnTimeExp" runat="server" Visible="false" CssClass="button" Width="105"
                                    Text="Time & Expense"></asp:Button>
                                <asp:Button ID="btnMerge" runat="server" CssClass="button" Text="Move Contact"></asp:Button>
                                <asp:Button ID="btnFav" runat="server" CssClass="button" Width="105" Text="Add to Favorites">
                                </asp:Button>
                                <asp:Button ID="btnActionItem" runat="server" CssClass="ybutton" Width="105" Text="New Action Item">
                                </asp:Button>
                                <asp:Button ID="btnExport" runat="server" CssClass="button" Width="105" Text="Export To Outlook"
                                    Visible="false"></asp:Button>&nbsp;
                                <asp:Button ID="btnEdit" runat="server" CssClass="button" Width="50" Text="Edit" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                                <asp:Button ID="btnActDelete" runat="server" CssClass="Delete" Text="X" ></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <igtab:UltraWebTab AutoPostBack="true" ImageDirectory="" ID="uwOppTab" runat="server"
                        ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                        <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                            NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                            FillStyle="LeftMergedWithCenter"></RoundedImage>
                        <SelectedTabStyle Height="23px" ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white">
                        </HoverTabStyle>
                        <Tabs>
                            <igtab:Tab Text="&nbsp;&nbsp;Contact Details&nbsp;&nbsp;">
                                <ContentTemplate>
                                    <asp:Table ID="tblContacts" runat="server" BorderWidth="1" Width="100%" GridLines="none"
                                        BorderColor="black" CssClass="aspTableDTL" Height="200">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Top">
                                                <asp:Table Width="100%" ID="tbl12" runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">
                                                            <asp:Button ID="btnLayout" runat="server" CssClass="button" Text="Layout"></asp:Button>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell VerticalAlign="Top">
						                    <img src="../images/Contact-32.gif" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Table runat="server" ID="tabledetail" BorderWidth="0" GridLines="none" CellPadding="2"
                                                                CellSpacing="0" HorizontalAlign="Center">
                                                            </asp:Table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell ColumnSpan="2">
                                                            <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%" GridLines="none"
                                                                HorizontalAlign="Center">
                                                            </asp:Table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </igtab:Tab>
                            <igtab:Tab Text="&nbsp;&nbsp;Areas of Interest&nbsp;&nbsp;">
                                <ContentTemplate>
                                    <asp:Table ID="Table2" runat="server" BorderWidth="1" Width="100%" GridLines="None"
                                        BorderColor="black" CssClass="aspTableDTL" Height="300">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Top">
                                                <br>
                                                <asp:Table ID="tblAOI" runat="server" Width="100%">
                                                </asp:Table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </igtab:Tab>
                            <igtab:Tab Text="&nbsp;&nbsp;Opportunities&nbsp;&nbsp;">
                                <ContentTemplate>
                                    <asp:Table ID="Table5" BorderWidth="1" runat="server" Height="300" Width="100%" BorderColor="black"
                                        CssClass="aspTableDTL" GridLines="None">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Top">
                                                <br>
                                                <table cellspacing="0" cellpadding="0" width="70%">
                                                    <tr valign="top">
                                                        <td>
                                                            <table>
                                                                <tr class="normal1">
                                                                    <td>
                                                                        <asp:RadioButton ID="radOppOpen" runat="server" AutoPostBack="true" GroupName="radOpp"
                                                                            Checked="true" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;Open Opportunities&nbsp;&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:RadioButton ID="radOppClose" runat="server" AutoPostBack="true" GroupName="radOpp" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;Closed Deals&nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlOppType" CssClass="signup" runat="server" AutoPostBack="True">
                                                                <asp:ListItem Value="1">Sales Opportunity</asp:ListItem>
                                                                <asp:ListItem Value="2">Purchase Opportunity</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlOppStatus" runat="server" CssClass="signup" AutoPostBack="True">
                                                                <asp:ListItem Value="1">Won</asp:ListItem>
                                                                <asp:ListItem Value="2">Lost</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Table ID="Table7" BorderWidth="1" CellPadding="0" CellSpacing="0" runat="server"
                                                                Width="100%" BorderColor="black" GridLines="None" Height="250">
                                                                <asp:TableRow>
                                                                    <asp:TableCell VerticalAlign="Top">
                                                                        <asp:DataGrid ID="dgOpenOpportunty" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                                                                            BorderColor="white">
                                                                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                                            <ItemStyle CssClass="is"></ItemStyle>
                                                                            <HeaderStyle CssClass="hs"></HeaderStyle>
                                                                            <Columns>
                                                                                <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="numContactID" Visible="false"></asp:BoundColumn>
                                                                                <asp:ButtonColumn HeaderText="Name" DataTextField="vcPOppname" CommandName="Name">
                                                                                </asp:ButtonColumn>
                                                                                <asp:BoundColumn HeaderText="Contact" DataField="vcGivenName"></asp:BoundColumn>
                                                                                <asp:BoundColumn HeaderText="Phone - Ext" DataField="PhoneNo"></asp:BoundColumn>
                                                                                <asp:BoundColumn HeaderText="Milestone, Stage" DataField="status"></asp:BoundColumn>
                                                                                <asp:BoundColumn HeaderText="Amount" DataFormatString="{0:#,###.00}" DataField="monPAmount">
                                                                                </asp:BoundColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                        <asp:DataGrid ID="dgClosedOpp" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                                                                            BorderColor="white">
                                                                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                                            <ItemStyle CssClass="is"></ItemStyle>
                                                                            <HeaderStyle CssClass="hs"></HeaderStyle>
                                                                            <Columns>
                                                                                <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="numContactID" Visible="false"></asp:BoundColumn>
                                                                                <asp:ButtonColumn HeaderText="Name" DataTextField="vcPOppname" CommandName="Name">
                                                                                </asp:ButtonColumn>
                                                                                <asp:BoundColumn HeaderText="Contact" DataField="vcGivenName"></asp:BoundColumn>
                                                                                <asp:BoundColumn HeaderText="Phone - Ext" DataField="PhoneNo"></asp:BoundColumn>
                                                                                <asp:BoundColumn HeaderText="Status" DataField="status"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Completed Date">
                                                                                    <ItemTemplate>
                                                                                        <%#ReturnName(DataBinder.Eval(Container.DataItem, "bintAccountClosingDate"))%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn HeaderText="Amount" DataFormatString="{0:#,###.00}" DataField="monPAmount">
                                                                                </asp:BoundColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </asp:TableCell>
                                                                </asp:TableRow>
                                                            </asp:Table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </igtab:Tab>
                            <igtab:Tab Text="&nbsp;&nbsp;Survey History&nbsp;&nbsp;">
                                <ContentTemplate>
                                    <asp:Table ID="Table8" BorderWidth="1" CellPadding="0" CellSpacing="0" Height="300"
                                        runat="server" CssClass="aspTableDTL" Width="100%" BorderColor="black" GridLines="none">
                                        <asp:TableRow Height="10%" VerticalAlign="Top">
                                            <asp:TableCell Height="30px" VerticalAlign="Top">
                                                <asp:Literal ID="litClientMessageSurveyHistory" runat="server"></asp:Literal>
                                                <table align="right">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblNextSurveyHistory" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:LinkButton ID="lnk2SurveyHistory" runat="server" CausesValidation="False">2</asp:LinkButton>
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:LinkButton ID="lnk3SurveyHistory" runat="server" CausesValidation="False">3</asp:LinkButton>
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:LinkButton ID="lnk4SurveyHistory" runat="server" CausesValidation="False">4</asp:LinkButton>
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:LinkButton ID="lnk5SurveyHistory" runat="server" CausesValidation="False">5</asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="lnkFirstSurveyHistory" runat="server" CausesValidation="False">
															<div class="LinkArrow"><<</div>
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="lnkPreviousSurveyHistory" runat="server" CausesValidation="False">
															<div class="LinkArrow"><</div>
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:Label ID="lblPageSurveyHistory" runat="server">Page</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCurrentPageSurveyHistory" runat="server" Text="1" Width="28px"
                                                                CssClass="signup" MaxLength="5" AutoPostBack="True"></asp:TextBox>
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:Label ID="lblOfSurveyHistory" runat="server">of</asp:Label>
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:Label ID="lblTotalSurveyHistory" runat="server"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="lnkNextSurveyHistory" runat="server" CssClass="LinkArrow" CausesValidation="False">
															<div class="LinkArrow">></div>
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="lnkLastSurveyHistory" runat="server" CausesValidation="False">
															<div class="LinkArrow">>></div>
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                                <br />
                                                <asp:DataGrid ID="dgSurvey" runat="server" CssClass="dg" BorderWidth="1px" Width="100%"
                                                    CellSpacing="0" CellPadding="2" BorderColor="white" AutoGenerateColumns="False"
                                                    ShowHeader="True" DataKeyField="numSurID" HeaderStyle-CssClass="hs" ItemStyle-CssClass="is"
                                                    AlternatingItemStyle-CssClass="ais">
                                                    <Columns>
                                                        <asp:BoundColumn Visible="True" HeaderText="Survey ID" DataField="numSurID" ItemStyle-Width="70">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" HeaderText="Respondent ID" DataField="numRespondantID"
                                                            ItemStyle-Width="70"></asp:BoundColumn>
                                                        <%--<asp:BoundColumn Visible="True" HeaderText="Date Created" DataField="dateCreatedOn" ItemStyle-Width="100"></asp:BoundColumn>--%>
                                                        <asp:TemplateColumn HeaderText="<font color=white>Date Created</font>" SortExpression="dateCreatedOn">
                                                            <ItemTemplate>
                                                                <%#ReturnName(DataBinder.Eval(Container.DataItem, "dateCreatedOn"))%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn Visible="True" ItemStyle-HorizontalAlign="Left" HeaderText="Survey Name"
                                                            DataField="vcSurName" ItemStyle-Width="300"></asp:BoundColumn>
                                                        <asp:HyperLinkColumn ItemStyle-HorizontalAlign="Left" HeaderText="Survey Results"
                                                            DataNavigateUrlField="numContactSurResponseLink" DataNavigateUrlFormatString="javascript:EditSurveyResult({0});"
                                                            Text="See Results" ItemStyle-Width="100"></asp:HyperLinkColumn>
                                                        <asp:BoundColumn Visible="True" HeaderText="Survey Rating" DataField="numSurRating"
                                                            ItemStyle-Width="100" SortExpression="numSurRating"></asp:BoundColumn>
                                                        <asp:TemplateColumn ItemStyle-Width="25" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnSurveyHistoryDelete" runat="server" CssClass="Delete" Text="X" 
                                                                    CommandName="Delete" Visible="True" OnClick="btnSurveyHistoryDeleteAction_Command"
                                                                    CausesValidation="False"></asp:Button>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </igtab:Tab>
                            <igtab:Tab Text="&nbsp;&nbsp;Correspondence&nbsp;&nbsp;">
                                <ContentTemplate>
                                    <asp:Table ID="Table4" CellPadding="0" CellSpacing="0" runat="server" BorderWidth="1"
                                        Width="100%" CssClass="aspTable" GridLines="None" BorderColor="black" Height="300">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Top">
                                                <table width="100%" border="0">
                                                    <tr align="center" valign="top">
                                                        <td align="right">
                                                            <table>
                                                                <tr>
                                                                    <td class="normal1" align="right">
                                                                        From
                                                                    </td>
                                                                    <td align="left">
                                                                        <BizCalendar:Calendar ID="Calendar1" runat="server" />
                                                                    </td>
                                                                    <td class="normal1" align="right">
                                                                        To
                                                                    </td>
                                                                    <td>
                                                                        <BizCalendar:Calendar ID="Calendar2" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="right">
                                                            <table class="normal1">
                                                                <tr>
                                                                    <td>
                                                                        Search
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSearchCorr" runat="server" CssClass="signup"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlSrchCorr" runat="server" CssClass="signup">
                                                                            <asp:ListItem Text="Show All" Value="0"></asp:ListItem>
                                                                            <asp:ListItem Text="Received Messages" Value="1"></asp:ListItem>
                                                                            <asp:ListItem Text="Sent Messages" Value="2"></asp:ListItem>
                                                                            <asp:ListItem Text="Communications by Due Date" Value="3"></asp:ListItem>
                                                                            <asp:ListItem Text="Communications by Created Date" Value="4"></asp:ListItem>
                                                                            <asp:ListItem Text="Tasks by Due Date" Value="5"></asp:ListItem>
                                                                            <asp:ListItem Text="Tasks by Created Date" Value="6"></asp:ListItem>
                                                                            <asp:ListItem Text="Notes by Created Date" Value="7"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnCorresGo" runat="server" CssClass="button" Text="Go"></asp:Button>&nbsp;
                                                                        <asp:Button ID="btnCorrDelete" runat="server" CssClass="button" Text="Delete"></asp:Button>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="right">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="normal1">
                                                                        Filter :
                                                                        <asp:DropDownList ID="ddlFilterCorr" runat="server" AutoPostBack="true" CssClass="signup">
                                                                            <asp:ListItem Text="Show All" Value="0"></asp:ListItem>
                                                                            <asp:ListItem Text="Received Messages" Value="1"></asp:ListItem>
                                                                            <asp:ListItem Text="Sent Messages" Value="2"></asp:ListItem>
                                                                            <asp:ListItem Text="Communications by Due Date" Value="3"></asp:ListItem>
                                                                            <asp:ListItem Text="Communications by Created Date" Value="4"></asp:ListItem>
                                                                            <asp:ListItem Text="Tasks by Due Date" Value="5"></asp:ListItem>
                                                                            <asp:ListItem Text="Tasks by Created Date" Value="6"></asp:ListItem>
                                                                            <asp:ListItem Text="Notes by Created Date" Value="7"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td align="center" class="normal1">
                                                                        &nbsp;&nbsp;&nbsp;No of Records :
                                                                        <asp:Label ID="lblNoOfRecordsCorr" runat="server" CssClass="text"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                                    </td>
                                                                    <td id="tdCorr" runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lblNextCorr" runat="server" CssClass="text_bold">&nbsp;&nbsp;&nbsp;Next:</asp:Label>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:LinkButton ID="lnk2Corr" runat="server" CausesValidation="False">2</asp:LinkButton>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:LinkButton ID="lnk3Corr" runat="server" CausesValidation="False">3</asp:LinkButton>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:LinkButton ID="lnk4Corr" runat="server" CausesValidation="False">4</asp:LinkButton>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:LinkButton ID="lnk5Corr" runat="server" CausesValidation="False">5</asp:LinkButton>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkFirstCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow"><<</div>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkPreviousCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow"><</div>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:Label ID="lblPageCorr" runat="server">Page</asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtCurrentPageCorr" runat="server" Text="1" Width="28px" CssClass="signup"
                                                                                        MaxLength="5" AutoPostBack="true"></asp:TextBox>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:Label ID="lblOfCorr" runat="server">of</asp:Label>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:Label ID="lblTotalCorr" runat="server"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkNextCorr" runat="server" CssClass="LinkArrow" CausesValidation="False">
															                    <div class="LinkArrow">></div>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkLastCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow">>></div>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:Repeater ID="rptCorr" runat="server">
                                                                <HeaderTemplate>
                                                                    <table cellspacing="0" class="dg" width="100%">
                                                                        <tr class="hs">
                                                                            <td style="display: none">
                                                                                numEmailHstrId
                                                                            </td>
                                                                            <td style="display: none">
                                                                                tintType
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:LinkButton CommandName="Sort" ID="lnkDate" runat="server"><font color="white">Date</font></asp:LinkButton>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:LinkButton CommandName="Sort" ID="lnkType" runat="server"><font color="white">Type</font></asp:LinkButton>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:LinkButton CommandName="Sort" ID="lnkFrom" runat="server"><font color="white">From ,To</font></asp:LinkButton>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:LinkButton CommandName="Sort" ID="lnkName" runat="server"><font color="white">Name /Phone ,& Ext.</font></asp:LinkButton>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:LinkButton CommandName="Sort" ID="lnkAssigned" runat="server"><font color="white">Assigned To</font></asp:LinkButton>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:CheckBox ID="chkDelete" onclick="chkAll()" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                </HeaderTemplate>
                                                                <AlternatingItemTemplate>
                                                                    <tr class="ais" align="center">
                                                                        <td style="display: none">
                                                                            <%#Container.DataItem("numEmailHstrId")%>
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <%#Container.DataItem("tintType")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("date")%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:HyperLink ID="hplAType" CssClass="hyperlink" NavigateUrl="#" onclick="openActionItem('<%#Container.DataItem("numEmailHstrId")%>','<%#Container.DataItem("caseid")%>','<%#Container.DataItem("CaseTimeId")%>','<%#Container.DataItem("CaseExpId")%>','<%#Container.DataItem("Type")%>','<%#Container.DataItem("dtCreatedDate")%>')"
                                                                                runat="server"><u style="cursor:hand"><%#Container.DataItem("Type")%></u></asp:HyperLink>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("From")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("Phone")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("assignedto")%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkADelete" Style="color: #C6D3E7;" runat="server" />
                                                                            <asp:Label ID="lblDelete" Visible="false" runat="server" Text='<%#Container.DataItem("DelData")%>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="ais">
                                                                        <td colspan="8">
                                                                            <%#Container.DataItem("Subject")%>
                                                                        </td>
                                                                    </tr>
                                                                </AlternatingItemTemplate>
                                                                <ItemTemplate>
                                                                    <tr class="is">
                                                                        <td style="display: none">
                                                                            <%#Container.DataItem("numEmailHstrId")%>
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <%#Container.DataItem("tintType")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("date")%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:HyperLink ID="hplType" CssClass="hyperlink" NavigateUrl="#" onclick="openActionItem('<%#Container.DataItem("numEmailHstrId")%>','<%#Container.DataItem("caseid")%>','<%#Container.DataItem("CaseTimeId")%>','<%#Container.DataItem("CaseExpId")%>','<%#Container.DataItem("Type")%>','<%#Container.DataItem("dtCreatedDate")%>')"
                                                                                runat="server"><u style="cursor:hand"><%#Container.DataItem("Type")%></u></asp:HyperLink>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("From")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("Phone")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("assignedto")%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkADelete" runat="server" />
                                                                            <asp:Label ID="lblDelete" Visible="false" runat="server" Text='<%#Container.DataItem("DelData")%>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="is">
                                                                        <td colspan="8">
                                                                            <%#Container.DataItem("Subject")%>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </igtab:Tab>
                        </Tabs>
                    </igtab:UltraWebTab>
                </td>
            </tr>
            </table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="hidEml" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="hidCompName" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalRecords" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtContactType" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="type" Style="display: none" runat="server" Text="0"></asp:TextBox>
            <asp:TextBox runat="server" ID="rows" Style="display: none"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtEmail" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtCorrTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtCorrTotalRecords" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="Textbox1" Style="display: none" runat="server"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
