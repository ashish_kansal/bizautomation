<%@ Register TagPrefix="menu1" TagName="Menu" Src="../include/webmenu.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmProjectsdtl.aspx.vb"
    Inherits="BACRM.UserInterface.Projects.frmProjectsdtl" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Projects</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <link href="../css/lists.css" type="text/css" rel="STYLESHEET" />

    <script language="javascript" type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeletMsg() {
            var bln = confirm("You're about to remove the Stage from this Process, all stage data will be deleted")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=P&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function CheckNumber() {
            if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                window.event.keyCode = 0;
            }
        }

        function Save(cint) {

            if (cint == 1) {
                if (document.Form1.ddlIntPrgMgr.selectedIndex == 0) {
                    alert("Select Internal Project Manager");
                    tsVert.selectedIndex = 0;
                    document.Form1.ddlTaskContact.focus();
                    return false;
                }
            }
            if (document.Form1.ddlCustPrjMgr.value == 0) {
                alert("Select Customer Project Manager");
                tsVert.selectedIndex = 0;
                document.Form1.ddlCustPrjMgr.focus();
                return false;
            }

        }
        function AddContact() {
            if (document.Form1.ddlcompany.value == 0) {
                alert("Select Customer");
                tsVert.selectedIndex = 2;
                document.Form1.ddlcompany.focus();
                return false;
            }
            if (document.Form1.ddlAssocContactId.value == 0) {
                alert("Select Contact");
                tsVert.selectedIndex = 2;
                document.Form1.ddlAssocContactId.focus();
                return false;
            }
            var str;
            for (i = 0; i < document.Form1.elements.length; i++) {
                if (i <= 9) {
                    str = '0' + (i + 1)
                }
                else {
                    str = i + 1
                }
                if (document.getElementById('dgContact_ctl' + str + '_txtContactID') != null) {
                    if (document.getElementById('dgContact_ctl' + str + '_txtContactID').value == document.Form1.ddlAssocContactId.value) {
                        alert("Associated contact is already added");
                        return false;
                    }
                }
            }

        }

        function deleteItem() {
            var bln;
            bln = window.confirm("Delete Seleted Row - Are You Sure ?")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }


        function OpenTemplate(a, b) {
            window.open('../common/templates.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pageid=' + a + '&id=' + b, '', 'toolbar=no,titlebar=no,left=500, top=300,width=350,height=200,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenDependency(a, b, c, d) {
            window.open('../projects/frmProDependency.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenExpense(a, b, c, d, e) {
            window.open('../projects/frmProExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&PerID=' + c + '&StgDtlId=' + d + '&DivId=' + e, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenTime(a, b, c, d, e) {
            window.open('../projects/frmProTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&PerID=' + c + '&StgDtlId=' + d + '&DivId=' + e, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenSubStage(a, b, c, d) {
            window.open('../projects/frmprosubstages.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function CheckBoxCon(a, b, c) {
            if (parseInt(c) == 1) {
                document.getElementById('uwOppTab__ctl1_chkStage~' + a + '~' + b).checked = true
            }
            else {
                document.getElementById('uwOppTab__ctl1_chkStage~' + a + '~' + b).checked = false
            }
        }
        function ValidateCheckBox(cint) {
            if (cint == 1) {
                if (document.getElementById('chkDClosed').checked == true) {
                    if (document.getElementById('chkDlost').checked == true) {
                        alert("The Deal is already Lost !")
                        document.getElementById('chkDClosed').checked = false
                        return false;
                    }
                }
            }
            if (cint == 2) {
                if (document.getElementById('chkDlost').checked == true) {
                    if (document.getElementById('chkDClosed').checked == true) {
                        alert("The Deal is already Closed !")
                        document.getElementById('chkDlost').checked = false
                        return false;
                    }
                }
            }
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function ShowWindow1(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                //window.location.reload(true);
                return false;

            }

        }

        function ShowLayout(a, b) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=R", '', 'toolbar=no,titlebar=no,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:Menu ID="webmenu1" runat="server"></menu1:Menu>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <table id="tblMenu" bordercolor="black" cellspacing="0" cellpadding="0" width="100%"
                            border="0" runat="server">
                            <tr>
                                <td class="tr1" align="center">
                                    <b>Record Owner: </b>
                                    <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Created By: </b>
                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Last Modified By: </b>
                                    <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal1" align="left">
                                    Organization : <u>
                                        <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnTransfer" runat="server" Text="Transfer Ownership" CssClass="button"
                                        Width="120"></asp:Button>
                                    <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" Width="50px">
                                    </asp:Button>
                                    <asp:Button ID="btnTCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                                    <asp:Button ID="btnActDelete" runat="server" CssClass="Delete" Text="X" ></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="pnlContract">
                            <table width="100%" class="normal1" align="center">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td width="10%" align="right">
                                                    Contract :
                                                </td>
                                                <td width="20%">
                                                    <asp:DropDownList ID="ddlContract" AutoPostBack="true" Width="180" runat="server"
                                                        CssClass="signup">
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="10%" align="right">
                                                    Amount Balance :
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblRemAmount" CssClass="normal1"></asp:Label>
                                                </td>
                                                <td width="10%" align="right">
                                                    Days Remaining :
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblRemDays" CssClass="normal1"></asp:Label>
                                                </td>
                                                <td width="10%" align="right">
                                                    Incidents Remaining :
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblRemInci" CssClass="normal1"></asp:Label>
                                                </td>
                                                <td width="10%" align="right">
                                                    Hours Remaining :
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblRemHours" CssClass="normal1"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <igtab:UltraWebTab ImageDirectory="" ID="uwOppTab" runat="server" ThreeDEffect="True"
                            BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                            <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                            </DefaultTabStyle>
                            <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                                NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                                FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <SelectedTabStyle Height="23px" ForeColor="white">
                            </SelectedTabStyle>
                            <HoverTabStyle Height="23px" ForeColor="white">
                            </HoverTabStyle>
                            <Tabs>
                                <igtab:Tab Text="&nbsp;&nbsp;Project Details&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="tblOppr" runat="server" BorderWidth="1" Width="100%" GridLines="None"
                                            BorderColor="black" CssClass="aspTableDTL" Height="200">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="top">
                                                    <asp:Table Width="100%" ID="tbl12" runat="server">
                                                        <asp:TableRow>
                                                            <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">
                                                                <asp:Button ID="btnLayout" runat="server" CssClass="button" Text="Layout"></asp:Button>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell VerticalAlign="Top">
                                            <img src="../images/Compass-32.gif" />
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Table runat="server" ID="tabledetail" BorderWidth="0" GridLines="none" CellPadding="2"
                                                                    CellSpacing="0" HorizontalAlign="Center">
                                                                </asp:Table>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell ColumnSpan="2">
                                                                <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%" GridLines="none"
                                                                    HorizontalAlign="Center">
                                                                </asp:Table>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:TableCell></asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="&nbsp;&nbsp;Milestones And Stages&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="tblMile" BorderWidth="1" runat="server" Width="100%" CellPadding="0"
                                            CellSpacing="0" CssClass="aspTableDTL" BorderColor="black" GridLines="None" Height="300">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br>
                                                    <table width="400">
                                                        <tr>
                                                            <td class="normal8" align="right">
                                                                <font color="green">Total Progress :</font>
                                                            </td>
                                                            <td class="normal1">
                                                                <font color="green">
                                                                    <asp:Label ID="lblTProgress" runat="server"></asp:Label>&nbsp;%</font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <br>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td colspan="5">
                                                                <table bordercolor="black" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Table ID="tblMilestone" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                                                                                GridLines="None">
                                                                            </asp:Table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="&nbsp;&nbsp;Associated Contacts&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="tblCont" BorderWidth="1" runat="server" CellPadding="0" CellSpacing="0"
                                            Width="100%" CssClass="aspTableDTL" BorderColor="black" GridLines="None" Height="300">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br />
                                                    <asp:DataGrid ID="dgContact" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                                        AutoGenerateColumns="False">
                                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="is"></ItemStyle>
                                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Organization,Relationship" DataField="Company"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="First & Last Name" DataField="Name"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Email" DataField="Email"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Contact Role" DataField="ContactRole"></asp:BoundColumn>
                                                            <asp:TemplateColumn HeaderText="Share Project via Partner Point ?">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" CssClass="cell1" ID="lblShare"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn Visible="false">
                                                                <HeaderTemplate>
                                                                    <asp:Button ID="btnHdeleteCnt" runat="server" CssClass="Delete" Text="X" ></asp:Button>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtContactID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactId") %>'>
                                                                    </asp:TextBox>
                                                                    <asp:Button ID="btnDeleteCnt" runat="server" CssClass="Delete" Text="X" CommandName="Delete">
                                                                    </asp:Button>
                                                                    <asp:LinkButton ID="lnkDeleteCnt" runat="server" Visible="false">
																<font color="#730000">*</font></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                    <br>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="&nbsp;&nbsp;Email History&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="tblEmail" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
                                            runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br>
                                                    <table width="100%" border="0">
                                                        <tr>
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            From
                                                                        </td>
                                                                        <td align="left">
                                                                            <BizCalendar:Calendar ID="calFrom" runat="server" />
                                                                        </td>
                                                                        <td class="normal1" align="right">
                                                                            To
                                                                        </td>
                                                                        <td>
                                                                            <BizCalendar:Calendar ID="calTo" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="normal1">
                                                                Search
                                                                <asp:TextBox ID="txtSearchEmail" runat="server" CssClass="signup"></asp:TextBox>
                                                                &nbsp;
                                                                <asp:DropDownList ID="ddlFields" runat="server" Width="130" CssClass="signup">
                                                                    <asp:ListItem Value="0">All Email Fields</asp:ListItem>
                                                                    <asp:ListItem Value="1">Message Number</asp:ListItem>
                                                                    <asp:ListItem Value="2">From</asp:ListItem>
                                                                    <asp:ListItem Value="3">To</asp:ListItem>
                                                                    <asp:ListItem Value="4">Subject</asp:ListItem>
                                                                    <asp:ListItem Value="5">Body</asp:ListItem>
                                                                </asp:DropDownList>
                                                                &nbsp;
                                                                <asp:Button ID="btnEmailGo" runat="server" CssClass="button" Text="Go" Width="25">
                                                                </asp:Button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="center" class="normal1">
                                                                No of messages :
                                                                <asp:Label ID="lblEmailNoofRecords" runat="server" CssClass="text"></asp:Label>
                                                            </td>
                                                            <td id="tdEmailNav" runat="server">
                                                                <table align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label1" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                                                        </td>
                                                                        <td class="normal1">
                                                                            <asp:LinkButton ID="lnkEmail2" runat="server" CausesValidation="False">2</asp:LinkButton>
                                                                        </td>
                                                                        <td class="normal1">
                                                                            <asp:LinkButton ID="lnkEmal3" runat="server" CausesValidation="False">3</asp:LinkButton>
                                                                        </td>
                                                                        <td class="normal1">
                                                                            <asp:LinkButton ID="lnkEmail4" runat="server" CausesValidation="False">4</asp:LinkButton>
                                                                        </td>
                                                                        <td class="normal1">
                                                                            <asp:LinkButton ID="lnkEmail5" runat="server" CausesValidation="False">5</asp:LinkButton>
                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton ID="lnkEmailFirst" runat="server" CausesValidation="False">
															<div class="LinkArrow"><<</div>
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton ID="lnkEmailPrevious" runat="server" CausesValidation="False">
															<div class="LinkArrow"><</div>
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                        <td class="normal1">
                                                                            <asp:Label ID="Label2" runat="server">Page</asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtEmailPage" runat="server" Text="1" Width="28px" CssClass="signup"
                                                                                MaxLength="5" AutoPostBack="True"></asp:TextBox>
                                                                        </td>
                                                                        <td class="normal1">
                                                                            <asp:Label ID="Label3" runat="server">of</asp:Label>
                                                                        </td>
                                                                        <td class="normal1">
                                                                            <asp:Label ID="lblEmailPageNo" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton ID="lnkEmailNext" runat="server" CssClass="LinkArrow" CausesValidation="False">
															<div class="LinkArrow">></div>
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton ID="linkEmailLast" runat="server" CausesValidation="False">
															<div class="LinkArrow">>></div>
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:DataGrid ID="dgEmail" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                                                                    BorderColor="white" AllowSorting="True">
                                                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                                    <ItemStyle CssClass="is"></ItemStyle>
                                                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="CreatedOn" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="numEmailHstrID" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="bintCreatedOn" SortExpression="bintCreatedOn" HeaderText="<font color=white>Date/Time</font>">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="vcFromEmail" SortExpression="vcFromEmail" HeaderText="<font color=white>From</font>">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="vcMessageTo" SortExpression="vcEmail" HeaderText="<font color=white>To</font>">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="vcSubject" SortExpression="vcSubject" HeaderText="<font color=white>Subject</font>">
                                                                        </asp:BoundColumn>
                                                                    </Columns>
                                                                    <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages">
                                                                    </PagerStyle>
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                            </Tabs>
                        </igtab:UltraWebTab>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                        <asp:TextBox runat="server" ID="txtDivId" Style="display: none"></asp:TextBox>
                        <asp:TextBox ID="txtEmailTotalPage" Style="display: none" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtEmailTotalRecords" Style="display: none" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtProName" Style="display: none" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
