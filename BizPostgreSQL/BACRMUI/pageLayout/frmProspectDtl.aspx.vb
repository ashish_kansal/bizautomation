Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebTab
Imports System.Reflection

Namespace BACRM.UserInterface.Prospects
    Public Class frmProspectsDTL : Inherits BACRMPage

        Dim objCommon As New CCommon
        Dim objContacts As CContacts
        Dim objItems As CItems
        Dim objPageLayout As CPageLayout
        Dim objProspectdtl As CProspectsDtl
        Dim objprospects As CProspects
        Dim objLeads As CLeads
        Dim strColumn As String
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim lngDivID As Long
        Dim m_aryRightsForPage(), m_aryRightsForActItem(), m_aryRightsForOpp(), m_aryRightsForCustFlds(), m_aryRightsForContacts(), _
        m_aryRightsForTransfer(), m_aryRightsForDemote(), m_aryRightsForFav(), m_aryRightsForAccounting() As Integer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try

                Response.Redirect("../prospects/frmProspects.aspx?" & GetQueryStringVal(Request.QueryString("enc"), "", True))

                If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                    SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                    SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
                Else : SI1 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
                Else : SI2 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                    frm = ""
                    frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
                Else : frm = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                    frm1 = ""
                    frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
                Else : frm1 = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    frm2 = ""
                    frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
                Else : frm2 = ""
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "SurveyRespondents" Or GetQueryStringVal(Request.QueryString("enc"), "frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    'Added by Debasish Nag on 6th Jan 2006
                    Dim objForm As Object                                   'The generic object
                    objForm = Page.FindControl("webmenu1")                  'Get a holder to the web control
                    objForm.visible = False                                 'Hide the web control
                    btnCancel.Attributes.Add("onclick", "javascript: window.close();return false;") 'Write code to close the window
                End If
                lngDivID = GetQueryStringVal(Request.QueryString("enc"), "DivID")

                ' Checking the rights to view Contacts
                ' LoadTableInformation()
                If Not IsPostBack Then
                    If objContacts Is Nothing Then objContacts = New CContacts
                    objContacts.RecID = lngDivID
                    objContacts.Type = "C"
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.AddVisiteddetails()
                    objCommon.DivisionID = lngDivID
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()
                    txtContId.Text = CStr(objCommon.ContactID)
                    Session("Help") = "Organization"
                    ' txtCurrrentPage.Text = 1
                    Calendar1.SelectedDate = Session("StartDate")
                    Calendar2.SelectedDate = Session("EndDate")
                    'checking rights to view the page
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 4)
                    ''''call function for sub-tab management  - added on 30jul09 by Mohan
                    'objCommon.ManageSubTabs(uwOppTab, Session("DomainID"), 3)
                    '''''
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    Else
                        If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnEdit.Visible = False
                        If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False
                    End If

                    Dim dtTab As New DataTable
                    dtTab = Session("DefaultTab")
                    If dtTab.Rows.Count > 0 Then
                        uwOppTab.Tabs(0).Text = IIf(IsDBNull(dtTab.Rows(0).Item("vcProspect")), "Prospect Details", dtTab.Rows(0).Item("vcProspect").ToString & " Details")
                    Else : uwOppTab.Tabs(0).Text = "Prospect Details"
                    End If
                    m_aryRightsForTransfer = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 10)
                    If m_aryRightsForTransfer(RIGHTSTYPE.VIEW) = 0 Then btnTransfer.Visible = False
                    m_aryRightsForDemote = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 11)
                    If m_aryRightsForDemote(RIGHTSTYPE.VIEW) = 0 Then btnDemote.Visible = False
                    m_aryRightsForFav = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 12)
                    If m_aryRightsForFav(RIGHTSTYPE.VIEW) = 0 Then btnFav.Visible = False
                    LoadTabDetails()

                    'Dim m_aryRightsForProject() As Integer
                    'm_aryRightsForProject = clsAuthorization.fn_GetPageListUserRights("frmProspects.aspx", Session("UserID"), 3, 8)
                    'If m_aryRightsForProject(RIGHTSTYPE.VIEW) <> 0 Then '
                    '    sc_GetProjects()
                    'End If

                    If uwOppTab.Tabs.Count > SI Then uwOppTab.SelectedTabIndex = SI

                    'getCorrespondance()
                    ' LoadAssets()
                End If
                'm_aryRightsForCustFlds = clsAuthorization.fn_GetPageListUserRights("frmProspects.aspx", Session("UserID"), 3, 9)
                'If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then
                '    DisplayDynamicFlds()
                'End If
                btnTransfer.Attributes.Add("onclick", "return OpenTransfer('" & "../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects&rtyWR=" & lngDivID & "')")
                btnLayout.Attributes.Add("onclick", "return ShowLayout('P','" & lngDivID & "','" & type.Text & "');")
                If lngDivID = Session("UserDivisionID") Then
                    btnActDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Else : btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadAssets()
            Try
                If objItems Is Nothing Then objItems = New CItems
                Dim dtItems As DataTable
                objItems.DivisionID = lngDivID
                objItems.DomainID = Session("DomainId")
                dtItems = objItems.getCompanyAssets()
                uwItem.DataSource = dtItems
                uwItem.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadTableInformation()
            Try
                Dim dtTableInfo As DataTable
                Dim dttablecust As DataTable
                Dim ds As New DataSet
                If objPageLayout Is Nothing Then objPageLayout = New CPageLayout
                Dim check As String
                Dim fields() As String
                Dim idcolumn As String = ""
                Dim count1 As Integer
                Dim x As Integer
                m_aryRightsForCustFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 9)
                Dim dtContactInfo As DataTable
                If objProspectdtl Is Nothing Then objProspectdtl = New CProspectsDtl
                objProspectdtl.DivisionID = lngDivID
                objProspectdtl.DomainID = Session("DomainID")
                objProspectdtl.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                'dtContactInfo = objProspectdtl.GetCompanyInfoDtl1()            ' getting the details

                type.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numCmptype")), "0", dtContactInfo.Rows(0).Item("numCmptype").ToString)
                uwOppTab.Tabs(0).Text = "&nbsp;&nbsp;" & IIf(IsDBNull(dtContactInfo.Rows(0).Item("numCompanyType")), "Prospect", dtContactInfo.Rows(0).Item("numCompanyType").ToString) & " Detail&nbsp;&nbsp;"
                objPageLayout.CoType = "P"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngDivID
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 1
                objPageLayout.numRelCntType = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numCmptype")), 0, dtContactInfo.Rows(0).Item("numCmptype"))
                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)
                'If ds.Tables.Count = 2 Then
                '    dttablecust = ds.Tables(1)
                '    dtTableInfo.Merge(dttablecust)
                'End If

                'Dim dv As DataView
                'dv = New DataView(dtTableInfo)

                txtROwner.Text = dtContactInfo.Rows(0).Item("numRecOwner")
                lblCustomerId.Text = lngDivID
                lblRecordOwner.Text = dtContactInfo.Rows(0).Item("RecOwner")
                lblCreatedBy.Text = dtContactInfo.Rows(0).Item("vcCreatedBy")
                lblLastModifiedBy.Text = dtContactInfo.Rows(0).Item("vcModifiedBy")

                Dim numrows As Integer = dtTableInfo.Compute("Max(tintrow)", String.Empty)
                Dim numcells As Integer = dtTableInfo.Compute("Max(intcoulmn)", String.Empty)
                Dim i As Integer = 0
                Dim nr As Integer
                Dim noRowsToLoop As Integer
                noRowsToLoop = (dtTableInfo.Rows.Count - (numrows * numcells)) / numcells
                noRowsToLoop = noRowsToLoop + numrows + 1

                For nr = 0 To noRowsToLoop
                    Dim r As New TableRow()
                    Dim nc As Integer
                    Dim ro As Integer = nr
                    For nc = 1 To numcells
                        If dtTableInfo.Rows.Count <> i Then
                            If dtTableInfo.Rows(i).Item("tintrow") = nr + 1 And dtTableInfo.Rows(i).Item("intcoulmn") = nc Then

                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                Dim fieldId As Integer
                                Dim bitDynFld As String
                                fieldId = CInt(dtTableInfo.Rows(i).Item("numFieldID").ToString)
                                bitDynFld = dtTableInfo.Rows(i).Item("bitCustomField")
                                column1.CssClass = "normal7"
                                If (bitDynFld <> "1") Then
                                    If (fieldId = "63") Then
                                        Dim h As New HyperLink()

                                        '  h.CssClass = "hyperlink"
                                        h.NavigateUrl = "#"
                                        h.Text = "Documents"
                                        h.Attributes.Add("onclick", "return OpenDocuments(" & lngDivID & ");")
                                        Dim l As New Label
                                        l.CssClass = "normal7"
                                        l.Text = "(" & dtContactInfo.Rows(0).Item("DocumentCount") & ")" & "&nbsp;:"
                                        column1.Controls.Add(h)
                                        column1.Controls.Add(l)
                                    ElseIf (fieldId = "94") Then
                                        If (IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLabel1")), "", dtContactInfo.Rows(0).Item("vcWebLabel1").ToString) <> "") Then
                                            column1.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLabel1")), "", dtContactInfo.Rows(0).Item("vcWebLabel1")) & "&nbsp;:"
                                        Else : column1.Text = "WebLink 1 :"
                                        End If
                                    ElseIf (fieldId = "95") Then
                                        If (IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLabel2")), "", dtContactInfo.Rows(0).Item("vcWebLabel2").ToString) <> "") Then
                                            column1.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLabel2")), "", dtContactInfo.Rows(0).Item("vcWebLabel2")) & "&nbsp;:"
                                        Else : column1.Text = "WebLink 2 :"
                                        End If
                                    ElseIf (fieldId = "96") Then
                                        If (IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLabel3")), "", dtContactInfo.Rows(0).Item("vcWebLabel3").ToString) <> "") Then
                                            column1.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLabel3")), "", dtContactInfo.Rows(0).Item("vcWebLabel3")) & "&nbsp;:"
                                        Else : column1.Text = "WebLink 3 :"
                                        End If
                                    ElseIf (fieldId = "97") Then
                                        If (IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLabel4")), "", dtContactInfo.Rows(0).Item("vcWebLabel4").ToString) <> "") Then
                                            column1.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLabel4")), "", dtContactInfo.Rows(0).Item("vcWebLabel4")) & "&nbsp;:"
                                        Else : column1.Text = "WebLink 4 :"
                                        End If
                                    ElseIf (fieldId = "48") Then
                                        column1.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numCompanyType")), "Name", dtContactInfo.Rows(0).Item("numCompanyType")) & "&nbsp;:"
                                    ElseIf (fieldId = "252") Then
                                        Dim h As New HyperLink
                                        h.NavigateUrl = "#"
                                        h.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                        h.Attributes.Add("onclick", "openFollow(" & lngDivID & ")")
                                        column1.Controls.Add(h)
                                    ElseIf (fieldId = "54") Then
                                        Dim h As New HyperLink
                                        h.NavigateUrl = "#"
                                        h.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                        h.Attributes.Add("onclick", "OpenAdd(" & lngDivID & ")")
                                        column1.Controls.Add(h)
                                    Else : column1.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                    End If

                                    'cell2 for a table row
                                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName").ToString) Then
                                        Dim temp As String
                                        temp = dtTableInfo.Rows(i).Item("vcDBColumnName").ToString
                                        fields = temp.Split(",")
                                        Dim j As Integer = 0

                                        While (j < fields.Length)
                                            If (fieldId = "69") Then
                                                If (fields(j) = "AssociateCountTo") Then
                                                    Dim h As New HyperLink()
                                                    h.CssClass = "hyperlink"
                                                    'h.NavigateUrl = "#"
                                                    h.Text = "To"
                                                    h.Attributes.Add("onclick", "return OpenTo(" & lngDivID & ");")
                                                    Dim l As New Label
                                                    l.CssClass = "normal1"
                                                    l.Text = "(" & IIf(IsDBNull(dtContactInfo.Rows(0).Item("AssociateCountTo")), "", dtContactInfo.Rows(0).Item("AssociateCountTo")) & ")" & "/"
                                                    column2.Controls.Add(h)
                                                    column2.Controls.Add(l)
                                                ElseIf (fields(j) = "AssociateCountFrom") Then
                                                    Dim h As New HyperLink()
                                                    h.CssClass = "hyperlink"
                                                    ' h.NavigateUrl = "#"
                                                    h.Text = "From"
                                                    h.Attributes.Add("onclick", "return OpenFrom(" & lngDivID & ");")
                                                    Dim l As New Label
                                                    l.CssClass = "normal1"
                                                    l.Text = "(" & dtContactInfo.Rows(0).Item("AssociateCountFrom") & ")"
                                                    l.Text = "(" & IIf(IsDBNull(dtContactInfo.Rows(0).Item("AssociateCountFrom")), "", dtContactInfo.Rows(0).Item("AssociateCountFrom")) & ")"
                                                    column2.Controls.Add(h)
                                                    column2.Controls.Add(l)
                                                End If
                                            ElseIf (fieldId = "60") Then
                                                Dim h As New HyperLink()
                                                h.CssClass = "hyperlink"
                                                'h.NavigateUrl = "http://" & IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebSite")), "", dtContactInfo.Rows(0).Item("vcWebSite"))
                                                h.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebSite")), "", dtContactInfo.Rows(0).Item("vcWebSite"))
                                                'h.Target = "blank"
                                                h.Attributes.Add("onclick", "return fn_GoToURL('" & h.Text & "');")
                                                column2.Controls.Add(h)
                                            ElseIf (fieldId = "94") Then
                                                Dim h As New HyperLink()
                                                h.CssClass = "hyperlink"
                                                'h.NavigateUrl = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLink1")), "", dtContactInfo.Rows(0).Item("vcWebLink1"))
                                                h.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLink1")), "", dtContactInfo.Rows(0).Item("vcWebLink1"))
                                                h.Attributes.Add("onclick", "return fn_GoToURL('" & h.Text & "');")
                                                column2.Controls.Add(h)
                                            ElseIf (fieldId = "95") Then
                                                Dim h As New HyperLink()
                                                h.CssClass = "hyperlink"
                                                ' h.NavigateUrl = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLink2")), "", dtContactInfo.Rows(0).Item("vcWebLink2"))
                                                h.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLink2")), "", dtContactInfo.Rows(0).Item("vcWebLink2"))
                                                h.Attributes.Add("onclick", "return fn_GoToURL('" & h.Text & "');")
                                                column2.Controls.Add(h)
                                            ElseIf (fieldId = "96") Then
                                                Dim h As New HyperLink()
                                                h.CssClass = "hyperlink"
                                                'h.NavigateUrl = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLink3")), "", dtContactInfo.Rows(0).Item("vcWebLink3"))
                                                h.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLink3")), "", dtContactInfo.Rows(0).Item("vcWebLink3"))
                                                h.Attributes.Add("onclick", "return fn_GoToURL('" & h.Text & "');")
                                                column2.Controls.Add(h)

                                            ElseIf (fieldId = "97") Then
                                                Dim h As New HyperLink()
                                                h.CssClass = "hyperlink"
                                                ' h.NavigateUrl = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLink4")), "", dtContactInfo.Rows(0).Item("vcWebLink4"))
                                                h.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebLink4")), "", dtContactInfo.Rows(0).Item("vcWebLink4"))
                                                h.Attributes.Add("onclick", "return fn_GoToURL('" & h.Text & "');")
                                                column2.Controls.Add(h)
                                            ElseIf (fieldId = "62") Then
                                                Dim l As New Label
                                                l.CssClass = "cell"
                                                If Not (IsDBNull(dtContactInfo.Rows(0).Item("bitPublicFlag"))) Then
                                                    l.Text = IIf(dtContactInfo.Rows(0).Item("bitPublicFlag"), "a", "r")
                                                Else : l.Text = "r"
                                                End If
                                                column2.Controls.Add(l)
                                            ElseIf (fieldId = "63" Or fieldId = "252") Then
                                                column2.Text = ""
                                            ElseIf (fieldId = "54") Then
                                                Dim l As New Label
                                                l.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "", dtContactInfo.Rows(0).Item(fields(j)))
                                                l.ID = "lblAddress"
                                                column2.Controls.Add(l)
                                            Else : column2.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "", dtContactInfo.Rows(0).Item(fields(j)))
                                            End If
                                            j += 1
                                        End While
                                    Else : column1.Text = ""
                                    End If ' end of table cell2
                                Else
                                    If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then
                                        If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                            column1.Text = "Custom Web Link :"
                                            Dim h As New HyperLink()
                                            h.CssClass = "hyperlink"
                                            Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                            url = url.Replace("RecordID", lngDivID)
                                            h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                            h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                            column2.Controls.Add(h)
                                        ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                            column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                            Dim strDate As String
                                            strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                            If strDate = "0" Then strDate = ""
                                            If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                        Else
                                            column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                            If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                                If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                    Dim l As New Label
                                                    l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                                    l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                    column2.Controls.Add(l)
                                                End If
                                            Else
                                                If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                    column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                                End If
                                            End If
                                        End If
                                    End If
                                End If

                                column2.CssClass = "normal1"
                                column1.HorizontalAlign = HorizontalAlign.Right
                                column2.HorizontalAlign = HorizontalAlign.Left
                                column1.Width = 250
                                column2.Width = 300
                                column2.ColumnSpan = 1
                                column1.ColumnSpan = 1
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                                i += 1
                            ElseIf dtTableInfo.Rows(i).Item("tintrow") = 0 And dtTableInfo.Rows(i).Item("intcoulmn") = 0 Then
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                column1.CssClass = "normal7"
                                If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then
                                    If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                        column1.Text = "Custom Web Link :"
                                        Dim h As New HyperLink()
                                        h.CssClass = "hyperlink"
                                        Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                        url = url.Replace("RecordID", lngDivID)
                                        h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                        column2.Controls.Add(h)
                                    ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                        Dim strDate As String
                                        strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                        If strDate = "0" Then strDate = ""
                                        If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                    Else
                                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                        If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                Dim l As New Label
                                                l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                                l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                column2.Controls.Add(l)
                                            End If
                                        Else
                                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                            End If
                                        End If
                                    End If
                                End If
                                column2.CssClass = "normal1"
                                column1.HorizontalAlign = HorizontalAlign.Right
                                column2.HorizontalAlign = HorizontalAlign.Left
                                column1.Width = 250
                                column2.Width = 300
                                column2.ColumnSpan = 1
                                column1.ColumnSpan = 1
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                                i += 1
                            Else
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                column1.Text = ""
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                            End If
                        End If
                    Next nc
                    tabledetail.Rows.Add(r)
                Next nr

                If (dtContactInfo.Rows.Count > 0) Then
                    Dim column1 As New TableCell
                    Dim column2 As New TableCell
                    Dim r As New TableRow
                    column1.CssClass = "normal7"
                    column2.CssClass = "normal1"
                    column1.HorizontalAlign = HorizontalAlign.Right
                    column2.HorizontalAlign = HorizontalAlign.Justify
                    column2.ColumnSpan = 5
                    Dim l As New Label
                    l.CssClass = "normal7"
                    l.Text = "Comments" & "&nbsp;:"
                    column1.Controls.Add(l)
                    column2.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("txtComments")), "-", dtContactInfo.Rows(0).Item("txtComments"))
                    column1.Width = 150
                    r.Cells.Add(column1)
                    r.Cells.Add(column2)
                    tableComment.Rows.Add(r)
                End If

                m_aryRightsForAccounting = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 10)
                If m_aryRightsForAccounting(RIGHTSTYPE.VIEW) <> 0 Then '
                    If Not IsDBNull(dtContactInfo.Rows(0).Item("numCompanyCredit")) Then
                        lblCreditLimit.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numCompanyCredit")), "-", dtContactInfo.Rows(0).Item("numCompanyCredit"))
                    End If
                    Dim dtCompanyTaxTypes As DataTable
                    If objprospects Is Nothing Then objprospects = New CProspects
                    objprospects.DomainID = Session("DomainID")
                    objprospects.DivisionID = lngDivID
                    dtCompanyTaxTypes = objprospects.GetCompanyTaxTypes
                    Dim dr As DataRow
                    dr = dtCompanyTaxTypes.NewRow
                    dr("numTaxItemID") = 0
                    dr("vcTaxName") = "Sales Tax(Default)"
                    dr("bitApplicable") = IIf(dtContactInfo.Rows(0).Item("bitNoTax") = True, False, True)
                    dtCompanyTaxTypes.Rows.Add(dr)


                    chkTaxItems.DataTextField = "vcTaxName"
                    chkTaxItems.DataValueField = "numTaxItemID"
                    chkTaxItems.DataSource = dtCompanyTaxTypes
                    chkTaxItems.DataBind()

                    For i = 0 To dtCompanyTaxTypes.Rows.Count - 1
                        If Not IsDBNull(dtCompanyTaxTypes.Rows(i).Item("bitApplicable")) Then
                            If dtCompanyTaxTypes.Rows(i).Item("bitApplicable") = True Then
                                chkTaxItems.Items.FindByValue(dtCompanyTaxTypes.Rows(i).Item("numTaxItemID")).Selected = True
                            Else
                                chkTaxItems.Items.FindByValue(dtCompanyTaxTypes.Rows(i).Item("numTaxItemID")).Selected = False
                            End If
                        End If
                    Next
                    If Not IsDBNull(dtContactInfo.Rows(0).Item("tintBillingTerms")) Then
                        If dtContactInfo.Rows(0).Item("tintBillingTerms") = 0 Then
                            lblBillinTerms.Text = "r"
                        Else
                            lblBillinTerms.Text = "a"
                            lblSummary.Text = "Net " & dtContactInfo.Rows(0).Item("numBillingDays") & " , " & IIf(dtContactInfo.Rows(0).Item("tintInterestType") = 0, "-", "+") & dtContactInfo.Rows(0).Item("fltInterest") & " %"
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub sc_GetOppDetails()
            Try
                m_aryRightsForOpp = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 7)
                If m_aryRightsForOpp(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If objprospects Is Nothing Then objprospects = New CProspects
                objprospects.DivisionID = lngDivID
                objprospects.OppType = ddlOppType.SelectedItem.Value
                If radOppOpen.Checked = True Then
                    objprospects.ByteMode = 0
                    objprospects.DomainID = Session("DomainID")
                    dgOpenOpportunty.DataSource = objprospects.GetOppDetailsForOrg
                    dgOpenOpportunty.DataBind()
                    dgClosedOpp.Visible = False
                    dgOpenOpportunty.Visible = True
                ElseIf radOppClose.Checked = True Then
                    objprospects.ByteMode = 1
                    objprospects.OppStatus = ddlOppStatus.SelectedItem.Value
                    objprospects.DomainID = Session("DomainID")
                    dgClosedOpp.DataSource = objprospects.GetOppDetailsForOrg
                    dgClosedOpp.DataBind()
                    dgClosedOpp.Visible = True
                    dgOpenOpportunty.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub GetContacts()
            Try
                m_aryRightsForContacts = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 5)
                If m_aryRightsForContacts(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If objprospects Is Nothing Then objprospects = New CProspects
                objprospects.DivisionID = lngDivID
                objprospects.DomainID = Session("DomainID")
                dgContacts.DataSource = objprospects.GetContactInfo1
                dgContacts.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub sc_GetProjects()
            Try
                Dim m_aryRightsForProject() As Integer
                m_aryRightsForProject = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 8)
                If m_aryRightsForProject(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If objprospects Is Nothing Then objprospects = New CProspects
                objprospects.DivisionID = lngDivID
                If radPro.Checked = True Then
                    objprospects.ProStatus = 0
                ElseIf radProCmp.Checked = True Then
                    objprospects.ProStatus = 1
                End If
                objprospects.DomainID = Session("DomainID")
                objprospects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dgProjectsOpen.DataSource = objprospects.GetProjectsForOrg
                dgProjectsOpen.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnDemote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDemote.Click
            'Declare the Connection objects
            Try
                If objLeads Is Nothing Then objLeads = New CLeads
                objLeads.DivisionID = lngDivID
                objLeads.byteMode = 0
                objLeads.DomainID = Session("DomainID")
                objLeads.DemoteOrg()

               Response.Redirect("../Leads/frmLeads.aspx?DivID=" & lngDivID)

                LoadTabDetails()
                'Response.Redirect("../Leads/frmLeads.aspx?DivID=" & lngDivID)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If GetQueryStringVal(Request.QueryString("enc"), "frm1") = "ActionItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommId=" & GetQueryStringVal(Request.QueryString("enc"), "CommID") & "&frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm"))
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "ActItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommId=" & GetQueryStringVal(Request.QueryString("enc"), "CommID"))
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "contactlist" Then
                    Response.Redirect("../contact/frmContactList.aspx?ContactType=" & GetQueryStringVal(Request.QueryString("enc"), "ContactType"))
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "opportunitylist" Then
                    Response.Redirect("../opportunity/frmOpportunityList.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "tickler" Then
                    Response.Redirect("../common/frmTicklerDisplay.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "Caselist" Then
                    Response.Redirect("../cases/frmCaseList.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "BestAccounts" Then
                    Response.Redirect("../reports/frmBestAccounts.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ContactRole" Then
                    Response.Redirect("../reports/frmContactRole.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CasesAgent" Then
                    Response.Redirect("../reports/frmCases.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "BizDocReport" Then
                    Response.Redirect("../reports/frmBizDocReport.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "Ecosystem" Then
                    Response.Redirect("../reports/frmEcosystemReport.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "BestPartners" Then
                    Response.Redirect("../reports/frmBestPartners.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ProDetail" Then
                    Response.Redirect("../projects/frmProjects.aspx?frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm1") & "&ProId=" & GetQueryStringVal(Request.QueryString("enc"), "ProId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)

                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "Portal" Then
                    Response.Redirect("../reports/frmSelfServicePortal.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "DealHistory" Then
                    Response.Redirect("../reports/frmRepDealHistory.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CampDetails" Then
                    Response.Redirect("../Marketing/frmCampaignDetails.aspx?CampID=" & GetQueryStringVal(Request.QueryString("enc"), "CampID"))
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "oppdetail" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm1") & "&opId=" & GetQueryStringVal(Request.QueryString("enc"), "opId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)

                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CaseDetail" Then
                    Response.Redirect("../cases/frmCases.aspx?frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm1") & "&CaseID=" & GetQueryStringVal(Request.QueryString("enc"), "CaseID"))

                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "contactdetail" Then
                    Response.Redirect("../contact/frmContacts.aspx?frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm1") & "&fdas89iu=098jfd&CntId=" & GetQueryStringVal(Request.QueryString("enc"), "CntID"))
               ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CaseReport" Then
                    Response.Redirect("../reports/frmCases.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CompanyList" Then
                    Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & GetQueryStringVal(Request.QueryString("enc"), "RelID") & "&profileId=" & GetQueryStringVal(Request.QueryString("enc"), "profileId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AvgSalesCycle1" Then
                    Response.Redirect("../reports/frmAvgSalesCycle1.aspx")
               ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AdvSearch" Then
                    Response.Redirect("../admin/frmAdvancedSearchRes.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsReceivable" Then
                    Response.Redirect("../Accounting/frmAccountsReceivable.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsPayable" Then
                    Response.Redirect("../Accounting/frmAccountsPayable.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AdvSearchSur" Then
                    Response.Redirect("../Admin/FrmAdvSurveyRes.aspx")
                Else
                    If type.Text = "0" Or type.Text = "46" Then
                        Response.Redirect("../prospects/frmProspectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&profileId=" & GetQueryStringVal(Request.QueryString("enc"), "profileid"))
                    Else : Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & type.Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim i, k As Integer
                Dim dtTable As DataTable
                ' Tabstrip3.Items.Clear()
                If objPageLayout Is Nothing Then objPageLayout = New CPageLayout
                objPageLayout.locId = 1
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.RelId = CInt(type.Text)
                objPageLayout.RecordId = lngDivID
                dtTable = objPageLayout.GetCustFlds

                If uwOppTab.Tabs.Count > 8 Then
                    Dim iItemcount As Integer
                    iItemcount = uwOppTab.Tabs.Count
                    While uwOppTab.Tabs.Count > 8
                        uwOppTab.Tabs.RemoveAt(iItemcount - 1)
                        iItemcount = iItemcount - 1
                    End While
                End If

                If dtTable.Rows.Count > 0 Then
                    'CustomField Section
                    Dim Tab As Tab
                    'Dim pageView As PageView
                    Dim aspTable As HtmlTable
                    Dim Table As Table
                    Dim tblcell As TableCell
                    Dim tblRow As TableRow
                    k = 0
                    ViewState("TabId") = dtTable.Rows(0).Item("TabId")
                    ViewState("Check") = 0
                    ViewState("FirstTabCreated") = 0
                    ' Tabstrip3.Items.Clear()
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") <> 0 Then
                            If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                                If ViewState("Check") <> 0 Then
                                    aspTable.Rows.Add(objRow)
                                    tblcell.Controls.Add(aspTable)
                                    tblRow.Cells.Add(tblcell)
                                    Table.Rows.Add(tblRow)
                                    Tab.ContentPane.Controls.Add(Table)
                                End If
                                k = 0
                                ViewState("FirstTabCreated") = 1
                                ViewState("Check") = 1
                                '  If Not IsPostBack Then
                                ViewState("TabId") = dtTable.Rows(i).Item("TabId")
                                Tab = New Tab
                                Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
                                uwOppTab.Tabs.Add(Tab)
                                'End If
                                'pageView = New PageView
                                aspTable = New HtmlTable
                                Table = New Table
                                Table.Width = Unit.Percentage(100)
                                Table.BorderColor = System.Drawing.Color.FromName("black")
                                Table.GridLines = GridLines.None
                                Table.BorderWidth = Unit.Pixel(1)
                                Table.Height = Unit.Pixel(300)
                                Table.CssClass = "aspTable"
                                tblcell = New TableCell
                                tblRow = New TableRow
                                tblcell.VerticalAlign = VerticalAlign.Top
                                aspTable.Width = "100%"
                                objRow = New HtmlTableRow
                                objCell = New HtmlTableCell
                                objCell.InnerHtml = "<br>"
                                objRow.Cells.Add(objCell)
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            'pageView.Controls.Add("")
                            If k = 3 Then
                                k = 0
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If
                            objCell = New HtmlTableCell
                            objCell.Align = "right"
                            objCell.Width = 100
                            objCell.Attributes.Add("class", "normal7")
                            If dtTable.Rows(i).Item("fld_type") <> "Frame" Then
                                If dtTable.Rows(i).Item("fld_type") <> "Link" Then objCell.InnerText = dtTable.Rows(i).Item("fld_label") & " :"
                                objRow.Cells.Add(objCell)
                            End If
                            objCell = New HtmlTableCell
                            objCell.Attributes.Add("class", "normal1")
                            objCell.Align = "left"
                            If dtTable.Rows(i).Item("fld_type") = "Link" Then
                                Dim h As New HyperLink
                                h.CssClass = "hyperlink"
                                Dim URL As String = IIf(IsDBNull(dtTable.Rows(i).Item("vcURL")), "", dtTable.Rows(i).Item("vcURL"))
                                URL = URL.Replace("RecordID", lngDivID)
                                h.Text = IIf(IsDBNull(dtTable.Rows(i).Item("fld_label")), "", dtTable.Rows(i).Item("fld_label"))
                                h.Attributes.Add("onclick", "fn_GoToURL('" & "http://" & URL & "')")
                                objCell.Controls.Add(h)
                                'CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngDivID, dtTable.Rows(i).Item("fld_label"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = IIf(dtTable.Rows(i).Item("Value") = "1", "a", "r")
                                    l.CssClass = IIf(dtTable.Rows(i).Item("Value") = "1", "cell1", "cell")
                                    objCell.Controls.Add(l)
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = "r"
                                    l.CssClass = "cell"
                                    objCell.Controls.Add(l)
                                End If
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
                                objCell = New HtmlTableCell
                                Dim strFrame As String
                                Dim URL As String
                                URL = dtTable.Rows(i).Item("vcURL")
                                URL = URL.Replace("RecordID", lngDivID)
                                strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                objCell.Controls.Add(New LiteralControl(strFrame))
                                objRow.Cells.Add(objCell)
                            Else
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = IIf(dtTable.Rows(i).Item("Value") = "0" Or dtTable.Rows(i).Item("Value") = Nothing, "-", dtTable.Rows(i).Item("Value"))
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = "-"
                                End If
                            End If
                            objRow.Cells.Add(objCell)
                            k = k + 1
                        End If
                    Next
                    If ViewState("Check") = 1 Then
                        aspTable.Rows.Add(objRow)
                        tblcell.Controls.Add(aspTable)
                        tblRow.Cells.Add(tblcell)
                        Table.Rows.Add(tblRow)
                        Tab.ContentPane.Controls.Add(Table)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgContacts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContacts.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    If objprospects Is Nothing Then objprospects = New CProspects
                    objprospects.ContactId = CLng(e.Item.Cells(0).Text)
                    If objprospects.DeleteContactById() = False Then
                        litMessage.Text = "Primary Contact Cannot be Deleted !"
                    Else : GetContacts()
                    End If
                End If
                If e.CommandName = "Name" Then
                    Response.Redirect("../contact/frmContacts.aspx?frm=prospects&fdas89iu=098jfd&CntId=" & CLng(e.Item.Cells(0).Text))
              End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgContacts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContacts.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim hplName, hplActItem, hplEmail, hplNewActItem, hplLast10Opened, hplLst10Closed As HyperLink
                    hplName = e.Item.FindControl("hplName")
                    hplEmail = e.Item.FindControl("hplEmail")
                    hplNewActItem = e.Item.FindControl("hplNewActItem")
                    hplLast10Opened = e.Item.FindControl("hplLast10Opened")
                    hplLst10Closed = e.Item.FindControl("hplLst10Closed")

                    If Session("CompWindow") = 1 Then
                        hplEmail.NavigateUrl = "mailto:" & hplEmail.Text
                    Else
                        'hplEmail.NavigateUrl = "../common/callemail.aspx?LsEmail=" & hplEmail.Text & "&ContID=" & e.Item.Cells(0).Text
                        hplEmail.Attributes.Add("onclick", "return fn_Mail('" & hplEmail.Text & "','" & Session("CompWindow") & "','" & e.Item.Cells(0).Text & "');")
                    End If
                    hplNewActItem.NavigateUrl = "../admin/newaction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Prospects&CntID=" & e.Item.Cells(0).Text
                    hplLast10Opened.CssClass = "hyperlink"
                    hplLst10Closed.CssClass = "hyperlink"
                    hplLast10Opened.Attributes.Add("onclick", "return OpenLst('" & "../ActionItems/frmLast10ActionItems.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&cntid=" & e.Item.Cells(0).Text & "&type=0" & "') ")
                    hplLst10Closed.Attributes.Add("onclick", "return OpenLst('" & " ../ActionItems/frmLast10ActionItems.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&cntid=" & e.Item.Cells(0).Text & "&type=1" & "') ")
                    Dim btnDelete As Button
                    Dim lnkDelete As LinkButton
                    btnDelete = e.Item.FindControl("btnDelete")
                    lnkDelete = e.Item.FindControl("lnkdelete")
                    If e.Item.Cells(0).Text = 1 Then
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                        Exit Sub
                    End If
                    If m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    ElseIf m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If e.Item.Cells(1).Text = Session("UserContactID") Then
                                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                            Else
                                btnDelete.Visible = False
                                lnkDelete.Visible = True
                                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                            End If
                        Catch ex As Exception

                        End Try
                    ElseIf m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 2 Then
                        Try
                            Dim i As Integer
                            Dim dtTerritory As New DataTable
                            dtTerritory = Session("UserTerritory")
                            If e.Item.Cells(2).Text = 0 Then
                                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                            Else
                                Dim chkDelete As Boolean = False
                                For i = 0 To dtTerritory.Rows.Count - 1
                                    If e.Item.Cells(2).Text = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                        chkDelete = True
                                    End If
                                Next
                                If chkDelete = True Then
                                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                                Else
                                    btnDelete.Visible = False
                                    lnkDelete.Visible = True
                                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                    ElseIf m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 3 Then
                        btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgClosedOpp_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgClosedOpp.ItemCommand
            Try
                Dim lngOppID As Long
                lngOppID = e.Item.Cells(0).Text
                If e.CommandName = "Name" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?frm=Prospects&Opid=" & lngOppID)

                End If
                If e.CommandName = "Contact" Then
                    objCommon.OppID = lngOppID
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../contact/frmContacts.aspx?frm=Prospects&fdas89iu=098jfd&CntId=" & objCommon.ContactID)

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgOpenOpportunty_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOpenOpportunty.ItemCommand
            Try
                Dim lngOppID As Long
                lngOppID = e.Item.Cells(0).Text
                If e.CommandName = "Name" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?frm=accounts&Opid=" & lngOppID)

                End If
                If e.CommandName = "Contact" Then
                    objCommon.OppID = lngOppID
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../contact/frmContacts.aspx?frm=Prospects&fdas89iu=098jfd&CntId=" & objCommon.ContactID)

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgProjectsOpen_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgProjectsOpen.ItemCommand
            Try
                Dim lngProID As Long
                lngProID = e.Item.Cells(0).Text
                If e.CommandName = "Name" Then
                    Response.Redirect("../projects/frmProjects.aspx?frm=Prospects&ProID=" & lngProID)

                End If
                If e.CommandName = "Contact" Then
                    objCommon.ProID = lngProID
                    objCommon.charModule = "P"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../contact/frmContacts.aspx?frm=Prospects&fdas89iu=098jfd&CntId=" & objCommon.ContactID)

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlOppType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOppType.SelectedIndexChanged
            Try
                sc_GetOppDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlOppStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOppStatus.SelectedIndexChanged
            Try
                sc_GetOppDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnName(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnFav_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFav.Click
            Try
                If objContacts Is Nothing Then objContacts = New CContacts
                objContacts.byteMode = 0
                objContacts.UserCntID = Session("UserContactID")
                objContacts.ContactID = lngDivID
                objContacts.Type = "O"
                objContacts.ManageFavorites()
                litMessage.Text = "Added to Favorites"
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                Dim objAccount As New CAccounts
                With objAccount
                    .DivisionID = lngDivID
                    .DomainID = Session("DomainID")
                End With
                If objAccount.DeleteOrg = False Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else
                    If type.Text = "0" Or type.Text = "46" Then
                        Response.Redirect("../prospects/frmProspectList.aspx")
                    Else : Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & type.Text)
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub tsVert_SelectedIndexChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsVert.SelectedIndexChange

        'End Sub

        Private Sub dlWebAnlys_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlWebAnlys.ItemCommand
            Try
                If e.CommandName = "Pages" Then
                    Dim cmd As String = CType(e.CommandSource, LinkButton).CommandName
                    dlWebAnlys.SelectedIndex = e.Item.ItemIndex
                    Session("dlIndex") = CInt(e.Item.ItemIndex) + 1
                    createMainLink()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub createMainLink()
            Try
                If objLeads Is Nothing Then objLeads = New CLeads
                objLeads.DivisionID = lngDivID
                objLeads.DomainID = Session("DomainID")
                Dim ds As New DataSet
                ds = objLeads.GetWebAnlysDtl
                If Session("dlIndex") <> 0 Then dlWebAnlys.SelectedIndex = Session("dlIndex") - 1
                dlWebAnlys.DataSource = ds.Tables(1)
                dlWebAnlys.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dlWebAnlys_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlWebAnlys.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.SelectedItem Then
                    If objLeads Is Nothing Then objLeads = New CLeads
                    objLeads.TrackingID = CType(e.Item.FindControl("lblID"), Label).Text
                    Dim dg As DataGrid
                    dg = e.Item.FindControl("dgWebAnlys")
                    dg.DataSource = objLeads.GetPagesVisited
                    dg.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            Try
                Response.Redirect("../pagelayout/frmProspectEdit.aspx?frm=ProspectDtl&DivID=" & lngDivID & "&SI=" & uwOppTab.SelectedTabIndex & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm & "&frm2=" & frm1 & "&profileId=" & GetQueryStringVal(Request.QueryString("enc"), "profileid") & "&ContactType=" & GetQueryStringVal(Request.QueryString("enc"), "ContactType") & "&RelId=" & GetQueryStringVal(Request.QueryString("enc"), "RelID"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub uwItem_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.CellEventArgs) Handles uwItem.ClickCellButton
            Try
                Dim numAItemCode As Integer = CInt(e.Cell.Tag)
                If objItems Is Nothing Then objItems = New CItems
                objItems.DivisionID = lngDivID
                objItems.DomainID = Session("DomainId")
                objItems.ItemCode = numAItemCode
                objItems.DeleteItemFromCmpAsset()
                LoadAssets()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnDateTime(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub uwItem_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwItem.InitializeRow
            Try
                If e.Row.HasParent = False Then
                    e.Row.Cells.FromKey("Action").Tag = e.Row.Cells.FromKey("numAItemCode").Value
                    e.Row.Cells.FromKey("Action").Value = "r"
                    e.Row.Cells.FromKey("Action").Column.CellButtonStyle.CssClass = "Delete"
                    e.Row.Cells.FromKey("Action").Column.Width = Unit.Pixel(20)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub uwOppTab_TabClick(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebTab.WebTabEvent) Handles uwOppTab.TabClick
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTabDetails()
            Try
                Select Case uwOppTab.SelectedTabIndex
                    Case 0
                        m_aryRightsForCustFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 9)
                        LoadTableInformation()
                        DisplayDynamicFlds()
                    Case 1 : GetContacts()
                    Case 2 : sc_GetOppDetails()
                    Case 3 : sc_GetProjects()
                    Case 4
                    Case 5
                        If objLeads Is Nothing Then objLeads = New CLeads
                        objLeads.DivisionID = lngDivID
                        objLeads.DomainID = Session("DomainID")
                        ViewState("Opened") = 1
                        Dim ds As DataSet
                        ds = objLeads.GetWebAnlysDtl
                        If ds.Tables(0).Rows.Count > 0 Then
                            lblDatesvisited.Text = ds.Tables(0).Rows(0).Item("StartDate") & " - " & ds.Tables(0).Rows(0).Item("EndDate")
                            lblReferringPage.Text = ds.Tables(0).Rows(0).Item("vcOrginalRef")
                            lblKeyword.Text = ds.Tables(0).Rows(0).Item("vcSearchTerm")
                            lblNoofTimes.Text = ds.Tables(0).Rows(0).Item("Count")
                        Else
                            lblDatesvisited.Text = "-"
                            lblReferringPage.Text = "-"
                            lblKeyword.Text = "-"
                            lblNoofTimes.Text = "-"
                        End If
                        dlWebAnlys.DataSource = ds.Tables(1)
                        dlWebAnlys.DataBind()
                    Case 6 : getCorrespondance()
                    Case 7 : LoadAssets()
                    Case Else
                        m_aryRightsForCustFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 9)
                        DisplayDynamicFlds()
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub rdbFinancialOverview_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbFinancialOverview.CheckedChanged
            Try
                If rdbFinancialOverview.Checked = True Then
                    PnlFinancialOverview.Visible = True
                    PnlTransaction.Visible = False
                Else
                    PnlTransaction.Visible = True
                    PnlFinancialOverview.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rdbTransaction_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbTransaction.CheckedChanged
            Try
                If rdbTransaction.Checked = True Then
                    PnlTransaction.Visible = True
                    LoadTransactionGrid()
                    PnlFinancialOverview.Visible = False
                Else
                    PnlFinancialOverview.Visible = True
                    PnlTransaction.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub LoadTransactionGrid()
            Try
                If objprospects Is Nothing Then objprospects = New CProspects
                Dim dtTransaction As DataTable
                objprospects.DomainID = Session("DomainId")
                objprospects.DivisionID = lngDivID
                dtTransaction = objprospects.GetTransactionDetails
                dgTransaction.DataSource = dtTransaction
                dgTransaction.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnCorresGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorresGo.Click
            Try
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rptCorr_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptCorr.ItemCommand
            Try
                If e.CommandName = "Sort" Then
                    If e.CommandSource.ID = "lnkDate" Then
                        strColumn = "Date"
                    ElseIf e.CommandSource.ID = "lnkType" Then
                        strColumn = "Type"
                    ElseIf e.CommandSource.ID = "lnkFrom" Then
                        strColumn = "[From]"
                    ElseIf e.CommandSource.ID = "lnkName" Then
                        strColumn = "phone"
                    ElseIf e.CommandSource.ID = "lnkAssigned" Then
                        strColumn = "assignedto"
                    End If
                End If
                If Session("Column") <> strColumn Then
                    Session("Column") = strColumn
                    Session("Asc") = 0
                Else
                    If Session("Asc") = 0 Then
                        Session("Asc") = 1
                    Else : Session("Asc") = 0
                    End If
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlFilterCorr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterCorr.SelectedIndexChanged
            Try
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnCorrDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorrDelete.Click
            Try
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim chk As CheckBox
                Dim lbl As Label
                For Each r As RepeaterItem In rptCorr.Items
                    chk = r.FindControl("chkADelete")
                    If chk.Checked = True Then
                        lbl = r.FindControl("lblDelete")
                        If lbl.Text.Split("~")(1) = 1 Then
                            objContacts.EmailHstrID = lbl.Text.Split("~")(0)
                            objContacts.tinttype = 1
                        Else
                            objContacts.EmailHstrID = lbl.Text.Split("~")(0)
                            objContacts.tinttype = 2
                        End If
                        objContacts.DelCorrespondence()
                    End If
                Next
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub getCorrespondance()
            Try
                m_aryRightsForActItem = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 6)
                If m_aryRightsForActItem(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 0 Then btnCorrDelete.Visible = False
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim dttable As DataTable
                objContacts.FromDate = Calendar1.SelectedDate
                objContacts.ToDate = DateAdd(DateInterval.Day, 1, CDate(Calendar2.SelectedDate))
                objContacts.ContactID = CInt(txtContId.Text)
                objContacts.DivisionID = lngDivID
                objContacts.MessageFrom = ""
                objContacts.SortOrder1 = ddlFilterCorr.SelectedValue
                objContacts.UserCntID = Session("UserContactID")
                objContacts.SortOrder = ddlSrchCorr.SelectedValue
                objContacts.KeyWord = txtSearchCorr.Text
                objContacts.DomainID = Session("DomainID")
                objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                If txtCurrentPageCorr.Text.Trim <> "" Then
                    objContacts.CurrentPage = txtCurrentPageCorr.Text
                Else : objContacts.CurrentPage = 1
                End If
                objContacts.PageSize = Session("PagingRows")
                objContacts.TotalRecords = 0
                If strColumn <> "" Then
                    objContacts.columnName = strColumn
                Else : objContacts.columnName = "date"
                End If
                If Session("Asc") = 1 Then
                    objContacts.columnSortOrder = "Desc"
                Else : objContacts.columnSortOrder = "Asc"
                End If
                dttable = objContacts.getCorres()
                If objContacts.TotalRecords = 0 Then
                    tdCorr.Visible = False
                    lblNoOfRecordsCorr.Text = objContacts.TotalRecords
                Else
                    tdCorr.Visible = True
                    lblNoOfRecordsCorr.Text = String.Format("{0:#,###}", objContacts.TotalRecords)
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblNoOfRecordsCorr.Text / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (lblNoOfRecordsCorr.Text Mod Session("PagingRows")) = 0 Then
                        lblTotalCorr.Text = strTotalPage(0)
                        txtCorrTotalPage.Text = strTotalPage(0)
                    Else
                        lblTotalCorr.Text = strTotalPage(0) + 1
                        txtCorrTotalPage.Text = strTotalPage(0) + 1
                    End If
                    txtCorrTotalRecords.Text = lblNoOfRecordsCorr.Text
                End If
                rptCorr.DataSource = dttable
                rptCorr.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub lnk2Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2Corr.Click
            Try
                If txtCurrentPageCorr.Text + 1 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 1 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 2
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk3Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3Corr.Click
            Try
                If txtCurrentPageCorr.Text + 2 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 2 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 3
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk4Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4Corr.Click
            Try
                If txtCurrentPageCorr.Text + 3 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 3 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 4
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk5Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5Corr.Click
            Try
                If txtCurrentPageCorr.Text + 4 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 4 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 5
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkFirstCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFirstCorr.Click
            Try
                txtCurrentPageCorr.Text = 1
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkLastCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLastCorr.Click
            Try
                txtCurrentPageCorr.Text = txtCorrTotalPage.Text
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkNextCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextCorr.Click
            Try
                If txtCurrentPageCorr.Text = txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 1
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkPreviousCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreviousCorr.Click
            Try
                If txtCurrentPageCorr.Text = 1 Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text - 1
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rptCorr_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCorr.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Then
                    Dim chk As CheckBox
                    Dim lbl As Label
                    chk = e.Item.FindControl("chkADelete")
                    lbl = e.Item.FindControl("lblDelete")
                    If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If lbl.Text.Split("~")(2) <> Session("UserContactID") Then chk.Visible = False
                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub radOppOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOppOpen.CheckedChanged
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub radOppClose_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOppClose.CheckedChanged
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub radPro_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPro.CheckedChanged
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub radProCmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radProCmp.CheckedChanged
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgTransaction_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTransaction.ItemCommand
            Try
                Dim lngID As Long
                lngID = e.Item.Cells(0).Text
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
