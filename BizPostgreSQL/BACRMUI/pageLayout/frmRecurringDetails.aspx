﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRecurringDetails.aspx.vb"
    Inherits=".frmRecurringDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Recurring Transaction Details</title>
    <style type="text/css">
        .style1
        {
            font-family: Tahoma, Verdana, Arial, Helvetica;
            font-size: 8pt;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            color: #000000;
            width: 431px;
        }
        .style2
        {
            width: 830px;
        }
    </style>
</head>
<body>
    <form id="frmRecurringDetails" runat="server">
    <input type="hidden" id="hdnoppId" runat="server" />
    <asp:Literal ID="Literal1" runat="server" EnableViewState="False"></asp:Literal>
    <table cellspacing="0" cellpadding="0" style="width: 72%">
        <tr>
            <td valign="bottom" class="style2" height="23">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Recurring Transaction Details&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td class="style1" align="right" height="23">
                <asp:Button ID="btnSaveandClose" runat="Server" CssClass="button" Text="Save and Close">
                </asp:Button>&nbsp<asp:Button ID="btnClose" runat="Server" CssClass="button" OnClientClick="javascript:CloseMe();"
                    Text="Close"></asp:Button>&nbsp;
            </td>
        </tr>
    </table>
    <asp:Table ID="tblSelectField" Width="72%" GridLines="None" BorderColor="black" BorderWidth="1"
        CssClass="aspTable" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Literal ID="litClientMessage" runat="server" EnableViewState="False"></asp:Literal>
                            

                            <br />
                            <asp:RadioButton ID="rdoRec" runat="server" GroupName="Group" Text="Use this closed sales order as a template from which to clone 'Recurring Sales Orders' as per the following recurring template." />
                            
<asp:DropDownList ID="ddlRec" runat="server" Width="131px">
                            </asp:DropDownList>
&nbsp;&nbsp;&nbsp;&nbsp;
                           &nbsp; <asp:CheckBox ID="chkAmtZero" runat ="server" Text= "Create Sales order with Amount Paid as Zero" /> 
                           
                            
 
                           
                            <br />
                            <asp:RadioButton ID="rdoBizDoc" runat="server" Text="Use this closed sales order as a template from which to create “Recurring Authoritative Sales BizDocs” within THIS sales order as per the following recurring template"
                                GroupName="Group" />
                            
<asp:DropDownList ID="ddlBizDoc" runat="server" Width="131px">
                            </asp:DropDownList>
                        

                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
