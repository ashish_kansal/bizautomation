﻿Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Imports Microsoft.Web.UI.WebControls

Public Class frmCustomTabLayout
    Inherits BACRMPage

    Dim lngFormID, lngTabID, lngRelType, lngPageID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngRelType = GetQueryStringVal("type").ToString
            lngFormID = GetQueryStringVal("FormId").ToString
            lngTabID = GetQueryStringVal("TabId").ToString
            lngPageID = GetQueryStringVal("PageId").ToString

            If Not IsPostBack Then
                getMax()
                binddata()

                GetUserRightsForPage(13, 43)
            End If
            btnUpdate.Attributes.Add("onclick", "getSort()")
            btnUpdate1.Attributes.Add("onclick", "getSort()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            savedata()
            binddata()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub getMax()
        Try
            rows.Text = 2
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub binddata()
        Try
            Dim str As String
            Dim objlayout As New CcustPageLayout
            Dim dttable1 As DataTable
            Dim i, count, x, r As Integer
            Dim strHeader As String = "Customize Tab Layout"

            lblTitle.Text = strHeader

            str = "<br/><table cellSpacing='0' cellPadding='0' width='100%'><tr>"

            str = str & "<td align='right' height='23'>&nbsp;&nbsp;&nbsp;"
            str = str & "<div style='float:right'><input class='button' id='btnsave1' style='width:50' onclick='show()' type='button' value='Save'>&nbsp;<input class='button' id='btnsave1' style='width:100' onclick='show1()' type='button' value='Save & Close'>&nbsp;<input class='button' id='btnClose' style='width:50' onclick='Close()' type='button' value='Close'>&nbsp;</div></td></tr></table>"
            str = str & "<table  class='aspTableDTL' cellpadding='0' cellspacing='0' width='100%' height='500px' border='0'><tr valign='top'><td>"
            str = str & "<br/><table align='center'valign='top' ><tr valign='top'><td class='hs' align='center'>Available Fields</td><td class='hs' align='center'>Fields added to column 1</td><td class='hs' align='center'>Fields added to column 2</td></tr><tr valign='top' >"
            x = 0
            Dim max As String
            max = rows.Text
            Dim maxHeight As Int16 = 0
            While (x <= max)
                objlayout.DomainID = Session("domainId")
                objlayout.UserCntID = Session("UserContactId")
                objlayout.ColumnID = x

                Dim ds As New DataSet
                objlayout.PageId = lngPageID
                objlayout.numRelation = lngRelType

                objlayout.FormId = lngFormID
                objlayout.PageType = 5
                objlayout.TabId = lngTabID

                ds = objlayout.GetCustomTabLayoutInfoDdl()
                dttable1 = ds.Tables(0)

                Dim dv As DataView

                dv = New DataView(dttable1)
                If x <> 0 Then
                    dv.Sort = "tintRow"
                ElseIf x = 0 Then
                    dv.Sort = "vcfieldName"
                End If
                dv.Table.AcceptChanges()
                i = 0
                count = 0
                count = dv.Table.Rows.Count
                str = str & "<td valign='top'>"
                str = str & "<ul id='x" + x.ToString + "' class='sortable boxy' style='height:#MAXHEIGHT#' >"
                While i < count
                    str = str & "<li id='" + dv(i).Item("numFieldId").ToString + "/" + dv(i).Item("bitCustomField").ToString + "' >" + dv(i).Item("vcFieldName") + "</li>"
                    i += 1
                End While
                str = str & "</ul>"
                str = str & " </td>"
                x += 1
                dttable1.Rows.Clear()

                If count > maxHeight Then
                    maxHeight = count
                End If
            End While
            str = str & " </tr></table></td></tr></table>"
            str = str.Replace("#MAXHEIGHT#", CStr(maxHeight * 23) & "px;")
            lblMainContent.Text = str
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub savedata()
        Try
            Dim container(3) As String
            Dim header(10) As String
            Dim headerdata(10) As String
            Dim data1XML As String
            Dim temp(10) As String
            Dim data1(2) As String
            Dim i As Integer = 1
            Dim j As Integer = 0

            Dim objDT As System.Data.DataTable
            Dim objDT1 As System.Data.DataTable
            Dim objDR As System.Data.DataRow
            objDT = New System.Data.DataTable("Table")
            objDT1 = New System.Data.DataTable("Table")

            objDT.Columns.Add("numFieldID", GetType(Integer))
            objDT.Columns.Add("tintRow", GetType(Integer))
            objDT.Columns.Add("intColumn", GetType(Integer))
            objDT.Columns.Add("numUserCntId", GetType(Integer))
            objDT.Columns.Add("numDomainID", GetType(Integer))
            objDT.Columns.Add("bitCustomField", GetType(Integer))
            objDT.Columns.Add("numRelCntType", GetType(Integer))

            Dim ds As New DataSet
            Dim objlayout As New CcustPageLayout
            Dim data As String = order.Value

            container = data.Split(":")

            Dim max As String
            max = rows.Text
            While (i <= max)
                temp = container(i).Split("(")
                header(i) = temp(0)
                temp = temp(1).Split(")")
                headerdata = temp(0).Split(",")
                j = 0
                While (j < headerdata.Length)
                    If (headerdata(j) <> "") Then
                        objDR = objDT.NewRow
                        data1 = headerdata(j).Split("/")
                        objDR("numFieldID") = data1(0)
                        objDR("tintRow") = j + 1
                        objDR("intColumn") = i

                        objDR("numUserCntId") = Session("UserContactId")

                        objDR("numDomainID") = Session("domainId")
                        objDR("bitCustomField") = 1
                        objDR("numRelCntType") = lngRelType

                        objDT.Rows.Add(objDR)
                    End If
                    j += 1
                End While

                ds.Tables.Add(objDT)
                data1XML = ds.GetXml()
                objlayout.DomainID = Session("domainId")
                objlayout.FormId = lngFormID
                objlayout.PageType = 5
                objlayout.TabId = lngTabID

                objlayout.UserCntID = Session("UserContactId")

                'objlayout.tintRow = i
                objlayout.ColumnID = i
                objlayout.RowString = data1XML
                objlayout.numRelCntType = lngRelType
                objlayout.SaveCustomTabLayoutList()
                objDT.Rows.Clear()
                If ds.Tables.Count > 0 Then ds.Tables.Remove(ds.Tables(0))
                i += 1
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub btnBindData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBindData.Click
        Try
            binddata()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnUpdate1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate1.Click
        Try
            savedata()
            binddata()
            ClientScript.RegisterStartupScript(Me.GetType, "close", "opener.location.reload(true);window.close()", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class