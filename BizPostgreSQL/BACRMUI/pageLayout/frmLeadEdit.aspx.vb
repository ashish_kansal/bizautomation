' Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebTab
Imports System.Reflection

Namespace BACRM.UserInterface.Leads
    Public Class frmLeadEdit : Inherits BACRMPage

        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim objContacts As CContacts
        Dim objLeads As CLeads
        Dim objUserAccess As UserAccess
        Dim lngDivID As Long
        Dim m_aryRightsForEmails(), m_aryRightsForActItem(), m_aryRightsForShowAOIs(), m_aryRightsForPromote(), m_aryRightsForCustFlds() As Integer
        Dim strColumn As String
        Dim objCommon As New CCommon

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                    SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                    SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
                Else : SI1 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
                Else : SI2 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                    frm = ""
                    frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
                Else : frm = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                    frm1 = ""
                    frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
                Else : frm1 = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    frm2 = ""
                    frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
                Else : frm2 = ""
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "SurveyRespondents" Or GetQueryStringVal(Request.QueryString("enc"), "frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    'Added by Debasish Nag on 6th Jan 2006
                    Dim objForm As Object                                   'The generic object
                    objForm = Page.FindControl("webmenu1")                  'Get a holder to the web control
                    objForm.visible = False                                 'Hide the web control
                    btnCancel.Attributes.Add("onclick", "javascript: window.close();return false;") 'Write code to close the window
                End If
                lngDivID = GetQueryStringVal(Request.QueryString("enc"), "DivID")
                objCommon.DivisionID = lngDivID
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                If Not IsPostBack Then
                    If objContacts Is Nothing Then objContacts = New CContacts
                    objContacts.RecID = lngDivID
                    objContacts.Type = "C"
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.AddVisiteddetails()
                    LoadAssociationInformation()                            'Added by Debasish for displayign the Association information
                    Session("Help") = "Organization"
                    Dim m_aryRightsForPage() As Integer
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmLeads.aspx", Session("userID"), 2, 3)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else
                        If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                            btnSave.Visible = False
                            btnSaveClose.Visible = False
                        End If
                        If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActdelete.Visible = False
                    End If
                    LoadDropdown()
                    LoadLeadDtls()
                    sb_ShowAOIs(objCommon.ContactID)
                End If
                m_aryRightsForCustFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmLeads.aspx", Session("userID"), 2, 8)
                If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then
                    DisplayDynamicFlds()
                End If
                If Not IsPostBack Then
                    If frm = "LeadDtl" Then
                        If SI > 1 And SI <= 4 Then
                            SI = 0
                        ElseIf SI > 4 Then
                            SI = (SI - 4) + 1
                        End If
                        If uwOppTab.Tabs.Count >= SI Then uwOppTab.SelectedTabIndex = SI
                    End If
                    hplFollowUpHstr.Attributes.Add("onclick", "return openFollow(" & lngDivID & ")")
                    ' hplAddress.Attributes.Add("onclick", "return ShowWindow('Layer1','','show')")
                    hplAddress.Attributes.Add("onclick", "return OpenAdd(" & lngDivID & ");")
                    'btnAddOk.Attributes.Add("onclick", "return ShowWindow('Layer1','','hide')")
                    btnWebGo.Attributes.Add("onclick", "return fn_GoToURL('txtWebURL');")
                    btnSave.Attributes.Add("onclick", "return Save()")
                    btnSaveClose.Attributes.Add("onclick", "return Save()")
                    btnEmailGo.Attributes.Add("onclick", "return fn_SendMail(" & objCommon.ContactID & ");")
                    If lngDivID = Session("UserDivisionID") Then
                        btnActdelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnActdelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadDropdown()
            Try
                objCommon.sb_FillComboFromDBwithSel(ddlPosition, 41, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlRelationhip, 5, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlAnnualRev, 6, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlNoOfEmp, 7, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlCampaign, 24, Session("DomainID"))
                objCommon.sb_FillGroupsFromDBSel(ddlGroup)
                objCommon.sb_FillComboFromDBwithSel(ddlTerritory, 78, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlInfoSource, 18, Session("DomainID"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadLeadDtls()
            Try
                Dim dtLeaddtls As DataTable
                If objLeads Is Nothing Then objLeads = New CLeads
                objLeads.ContactID = objCommon.ContactID
                objLeads.DomainID = Session("DomainID")
                objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtLeaddtls = objLeads.GetLeadInfo
                If dtLeaddtls.Rows.Count > 0 Then
                    lblCustID.Text = lngDivID
                    lblAddress.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("address")), "", dtLeaddtls.Rows(0).Item("address"))
                    txtCompanyName.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("vcCompanyName")), "", dtLeaddtls.Rows(0).Item("vcCompanyName"))
                    txtFirstname.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("vcFirstName")), "", dtLeaddtls.Rows(0).Item("vcFirstName"))
                    txtLastName.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("vcLastName")), "", dtLeaddtls.Rows(0).Item("vcLastName"))
                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("vcWebSite")) Then
                        If Not dtLeaddtls.Rows(0).Item("vcWebSite") = "" Then
                            txtWebURL.Text = dtLeaddtls.Rows(0).Item("vcWebSite")
                        Else : txtWebURL.Text = "http://"
                        End If
                    Else : txtWebURL.Text = "http://"
                    End If
                    ' txtWebURL.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("vcWebSite")), "", dtLeaddtls.Rows(0).Item("vcWebSite"))
                    txtEmail.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("vcEmail")), "", dtLeaddtls.Rows(0).Item("vcEmail"))
                    txtComPhone.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("vcComPhone")), "", dtLeaddtls.Rows(0).Item("vcComPhone"))
                    txtComFax.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("vcComFax")), "", dtLeaddtls.Rows(0).Item("vcComFax"))
                    txtTitle.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("vcTitle")), "", dtLeaddtls.Rows(0).Item("vcTitle"))

                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("numCompanyType")) Then
                        If Not ddlRelationhip.Items.FindByValue(dtLeaddtls.Rows(0).Item("numCompanyType")) Is Nothing Then
                            ddlRelationhip.Items.FindByValue(dtLeaddtls.Rows(0).Item("numCompanyType")).Selected = True
                        End If
                    End If
                    LoadProfile()

                    lblRecordOwner.Text = dtLeaddtls.Rows(0).Item("vcRecordOwner")
                    lblCreatedBy.Text = dtLeaddtls.Rows(0).Item("vcCreatedBy")
                    lblLastModifiedBy.Text = dtLeaddtls.Rows(0).Item("vcModifiedBy")

                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("vcPosition")) Then
                        If Not ddlPosition.Items.FindByValue(dtLeaddtls.Rows(0).Item("vcPosition")) Is Nothing Then
                            ddlPosition.Items.FindByValue(dtLeaddtls.Rows(0).Item("vcPosition")).Selected = True
                        End If
                    End If
                    txtPhone.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("numPhone")), "", dtLeaddtls.Rows(0).Item("numPhone"))
                    txtExt.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("numPhoneExtension")), "", dtLeaddtls.Rows(0).Item("numPhoneExtension"))

                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("numNoOfEmployeesId")) Then
                        If Not ddlNoOfEmp.Items.FindByValue(dtLeaddtls.Rows(0).Item("numNoOfEmployeesId")) Is Nothing Then
                            ddlNoOfEmp.Items.FindByValue(dtLeaddtls.Rows(0).Item("numNoOfEmployeesId")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("numFollowUpStatus")) Then
                        If Not ddlFollow.Items.FindByValue(dtLeaddtls.Rows(0).Item("numFollowUpStatus")) Is Nothing Then
                            ddlFollow.Items.FindByValue(dtLeaddtls.Rows(0).Item("numFollowUpStatus")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("numTerID")) Then
                        If Not ddlTerritory.Items.FindByValue(dtLeaddtls.Rows(0).Item("numTerID")) Is Nothing Then
                            ddlTerritory.Items.FindByValue(dtLeaddtls.Rows(0).Item("numTerID")).Selected = True
                        End If
                    End If

                    If Session("PopulateUserCriteria") = 1 Then
                        ddlTerritory.AutoPostBack = True
                        objCommon.sb_FillConEmpFromTerritories(ddlAssignedTo, Session("DomainID"), 0, 0, dtLeaddtls.Rows(0).Item("numTerID"))
                    ElseIf Session("PopulateUserCriteria") = 2 Then
                        objCommon.sb_FillConEmpFromDBUTeam(ddlAssignedTo, Session("DomainID"), Session("UserContactID"))
                    Else : objCommon.sb_FillConEmpFromDBSel(ddlAssignedTo, Session("DomainID"), 0, 0)
                    End If

                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("vcHow")) Then
                        If Not ddlInfoSource.Items.FindByValue(dtLeaddtls.Rows(0).Item("vcHow")) Is Nothing Then
                            ddlInfoSource.Items.FindByValue(dtLeaddtls.Rows(0).Item("vcHow")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("numAnnualRevID")) Then
                        If Not ddlAnnualRev.Items.FindByValue(dtLeaddtls.Rows(0).Item("numAnnualRevID")) Is Nothing Then
                            ddlAnnualRev.Items.FindByValue(dtLeaddtls.Rows(0).Item("numAnnualRevID")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("vcProfile")) Then
                        If Not ddlProfile.Items.FindByValue(dtLeaddtls.Rows(0).Item("vcProfile")) Is Nothing Then
                            ddlProfile.Items.FindByValue(dtLeaddtls.Rows(0).Item("vcProfile")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("numgrpid")) Then
                        If Not ddlGroup.Items.FindByValue(dtLeaddtls.Rows(0).Item("numgrpid")) Is Nothing Then
                            ddlGroup.Items.FindByValue(dtLeaddtls.Rows(0).Item("numgrpid")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("numAssignedTo")) Then
                        If Not ddlAssignedTo.Items.FindByValue(dtLeaddtls.Rows(0).Item("numAssignedTo")) Is Nothing Then
                            ddlAssignedTo.Items.FindByValue(dtLeaddtls.Rows(0).Item("numAssignedTo")).Selected = True
                        End If
                    End If

                    txtComments.Text = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("txtComments")), "", dtLeaddtls.Rows(0).Item("txtComments"))
                    If Not IsDBNull(dtLeaddtls.Rows(0).Item("numCampaignID")) Then
                        If Not ddlCampaign.Items.FindByValue(dtLeaddtls.Rows(0).Item("numCampaignID")) Is Nothing Then
                            ddlCampaign.Items.FindByValue(dtLeaddtls.Rows(0).Item("numCampaignID")).Selected = True
                        End If
                    End If
                    Dim dtTab As DataTable
                    dtTab = Session("DefaultTab")
                    Dim grpid As Integer = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("numgrpid")), "0", dtLeaddtls.Rows(0).Item("numgrpid"))
                    If dtTab.Rows.Count > 0 Then
                        If grpid = 5 Then
                            uwOppTab.Tabs(0).Text = "My " & IIf(IsDBNull(dtTab.Rows(0).Item("vcLead")), "Lead Details", dtTab.Rows(0).Item("vcLead").ToString & " Details")
                        ElseIf grpid = 1 Then
                            uwOppTab.Tabs(0).Text = "Web " & IIf(IsDBNull(dtTab.Rows(0).Item("vcLead")), "Lead Details", dtTab.Rows(0).Item("vcLead").ToString & " Details")
                        ElseIf grpid = 2 Then
                            uwOppTab.Tabs(0).Text = "Public" & IIf(IsDBNull(dtTab.Rows(0).Item("vcLead")), "Lead Details", dtTab.Rows(0).Item("vcLead").ToString & " Details")
                        ElseIf grpid = 0 Then
                            uwOppTab.Tabs(0).Text = "Lead Details"
                        End If
                    Else
                        uwOppTab.Tabs(0).Text = "Lead Details"
                    End If
                    For Each Item As ListItem In ddlGroup.Items
                        If Item.Text = "My Leads" Then
                            Item.Text = "My " & dtTab.Rows(0).Item("vcLead") & "s"
                        ElseIf Item.Text = "PublicLeads" Then
                            Item.Text = "Public " & dtTab.Rows(0).Item("vcLead") & "s"
                        ElseIf Item.Text = "WebLeads" Then
                            Item.Text = "Web " & dtTab.Rows(0).Item("vcLead") & "s"
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub createHeader(ByVal tbl As Table)
            Try
                Dim tblCell As TableCell
                Dim tblRow As TableRow
                tblRow = New TableRow
                tblRow.CssClass = "hs"

                tblCell = New TableCell
                tblCell.CssClass = "normal5"
                tblCell.Text = "Created Date"
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal5"
                tblCell.Text = "Due Date"
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal5"
                tblCell.Text = "Activity"
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal5"
                tblCell.Text = "Type"
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal5"
                tblCell.Text = "Name"
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal5"
                tblCell.Text = "Phone - Ext"
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal5"
                tblCell.Text = "Assigned To"
                tblRow.Cells.Add(tblCell)

                tbl.Rows.Add(tblRow)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
                SaveAOI(objCommon.ContactID)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Save()
                SaveAOI(objCommon.ContactID)
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If ddlGroup.SelectedItem.Value <> 0 Then
                    Session("grpID") = ddlGroup.SelectedItem.Value
                End If

                If GetQueryStringVal(Request.QueryString("enc"), "frm1") = "ActionItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommId=" & GetQueryStringVal(Request.QueryString("enc"), "CommId") & "&frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm"))
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "tickler" Then
                    Response.Redirect("../common/frmTicklerDisplay.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ActItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommId=" & GetQueryStringVal(Request.QueryString("enc"), "CommID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "admin" Then
                    Response.Redirect("../admin/transferleads.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "search" Then
                    Response.Redirect("../common/searchdisplay.aspx?frm=advancedsearch&srchback=true&typ=" & GetQueryStringVal(Request.QueryString("enc"), "typ") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CaseReport" Then
                    Response.Redirect("../reports/frmCases.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "contactdetail" Then
                    Response.Redirect("../contact/frmContacts.aspx?fda=r43453&CntId=" & GetQueryStringVal(Request.QueryString("enc"), "CntID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "LeadActivity" Then
                    Response.Redirect("../reports/frmLeadActivity.aspx")
               ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsReceivable" Then
                    Response.Redirect("../Accounting/frmAccountsReceivable.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsPayable" Then
                    Response.Redirect("../Accounting/frmAccountsPayable.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                Else : Response.Redirect("../Leads/frmLeadList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub Save()
            Try
                If objLeads Is Nothing Then objLeads = New CLeads
                With objLeads
                    .CompanyID = objCommon.CompID
                    .CompanyName = txtCompanyName.Text
                    .DomainID = Session("DomainID")
                    .DivisionID = lngDivID
                    .AnnualRevenue = ddlAnnualRev.SelectedItem.Value
                    .WebSite = txtWebURL.Text
                    .NumOfEmp = ddlNoOfEmp.SelectedItem.Value
                    .Profile = ddlProfile.SelectedItem.Value
                    .GroupID = ddlGroup.SelectedItem.Value
                    .CampaignID = ddlCampaign.SelectedItem.Value
                    .Comments = txtComments.Text
                    .CompanyType = ddlRelationhip.SelectedItem.Value
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .CRMType = 0
                    .LeadBoxFlg = 1
                    .DivisionName = ""
                    'txtDivisionName.Text()
                    .FirstName = txtFirstname.Text
                    .LastName = txtLastName.Text
                    .Email = txtEmail.Text
                    .FollowUpStatus = ddlFollow.SelectedItem.Value
                    .Position = ddlPosition.SelectedItem.Value
                    .ContactPhone = txtPhone.Text
                    .PhoneExt = txtExt.Text
                    .Comments = txtComments.Text
                    .ContactID = objCommon.ContactID

                    .Title = txtTitle.Text.Trim
                    .ComFax = txtComFax.Text.Trim
                    .ComPhone = txtComPhone.Text.Trim
                    .AssignedTo = ddlAssignedTo.SelectedValue
                    .TerritoryID = ddlTerritory.SelectedItem.Value
                    .InfoSource = ddlInfoSource.SelectedValue
                End With
                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                objLeads.DivisionID = objLeads.ManageCompanyDivisionsInfo1
                objLeads.UpdateAddConInfo()
                SaveCusField()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub sb_ShowAOIs(ByVal lngCntID As Long)
            Try
                m_aryRightsForShowAOIs = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmLeads.aspx", Session("userID"), 2, 4)
                If m_aryRightsForShowAOIs(RIGHTSTYPE.VIEW) = 0 Then chkAOI.Visible = False
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim dtAOI As DataTable
                Dim i As Integer
                objContacts.ContactID = lngCntID
                objContacts.DomainID = Session("DomainID")
                dtAOI = objContacts.GetAOIDetails
                chkAOI.DataSource = dtAOI
                chkAOI.DataTextField = "vcAOIName"
                chkAOI.DataValueField = "numAOIId"
                chkAOI.DataBind()
                For i = 0 To dtAOI.Rows.Count - 1
                    If Not IsDBNull(dtAOI.Rows(i).Item("Status")) Then
                        If dtAOI.Rows(i).Item("Status") = 1 Then
                            chkAOI.Items.FindByValue(dtAOI.Rows(i).Item("numAOIId")).Selected = True
                        Else : chkAOI.Items.FindByValue(dtAOI.Rows(i).Item("numAOIId")).Selected = False
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveAOI(ByVal lngCntID As Long)
            Try
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim dtTable As New DataTable
                dtTable.Columns.Add("numAOIId")
                dtTable.Columns.Add("Status")
                Dim dr As DataRow
                Dim i As Integer
                For i = 0 To chkAOI.Items.Count - 1
                    If chkAOI.Items(i).Selected = True Then
                        dr = dtTable.NewRow
                        dr("numAOIId") = chkAOI.Items(i).Value
                        dr("Status") = 1
                        dtTable.Rows.Add(dr)
                    End If
                Next
                Dim ds As New DataSet
                Dim strdetails As String
                dtTable.TableName = "Table"
                ds.Tables.Add(dtTable)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))
                objContacts.strAOI = strdetails
                objContacts.ContactID = lngCntID
                objContacts.SaveAOI()
                dtTable.Dispose()
                ds.Dispose()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        '    FillState(ddlState, ddlCountry.SelectedItem.Value, Session("DomainID"))
        '    Layer1.Attributes.Add("style", "VISIBILITY: visible;Z-INDEX: 2; LEFT: 200px; WIDTH: 450px; POSITION: absolute; TOP: 360px")
        'End Sub

        Private Sub btnContactDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContactdetails.Click
            Try
               Response.Redirect("../contact/frmContacts.aspx?frm=LeadDetails&fdas89iu=098jfd&CntId=" & objCommon.ContactID & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)

                'Response.Redirect("../Contact/frmContacts.aspx?frm=LeadDetails&CntID=" & objCommon.ContactID)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                Dim strDate As String
                Dim bizCalendar As UserControl
                Dim _myUC_DueDate As PropertyInfo
                Dim PreviousRowID As Integer = 0
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim i, k As Integer
                Dim dtTable As New DataTable
                'Tabstrip3.Items.Clear()
                Dim ObjCus As New CustomFields(Session("userid"))
                ObjCus.locId = 1
                ObjCus.RelId = ddlRelationhip.SelectedItem.Value
                ObjCus.RecordId = lngDivID
                ObjCus.DomainID = Session("DomainID")
                dtTable = ObjCus.GetCustFlds.Tables(0)
                Session("CusFields") = dtTable

                If uwOppTab.Tabs.Count > 2 Then
                    Dim iItemcount As Integer
                    iItemcount = uwOppTab.Tabs.Count
                    While uwOppTab.Tabs.Count > 2
                        uwOppTab.Tabs.RemoveAt(iItemcount - 1)
                        iItemcount = iItemcount - 1
                    End While
                End If

                If dtTable.Rows.Count > 0 Then
                    'Main Detail Section
                    k = 0
                    objRow = New HtmlTableRow
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") = 0 Then
                            If k = 3 Then
                                k = 0
                                tblDetails.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            objCell = New HtmlTableCell
                            objCell.Align = "Right"
                            objCell.Attributes.Add("class", "normal1")
                            If dtTable.Rows(i).Item("fld_type") <> "Link" Then
                                objCell.InnerText = dtTable.Rows(i).Item("fld_label")
                            Else : objCell.InnerText = ""
                            End If
                            objRow.Cells.Add(objCell)
                            If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                                objCell = New HtmlTableCell
                                CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                                objCell = New HtmlTableCell
                                CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                objCell = New HtmlTableCell
                                CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                                objCell = New HtmlTableCell
                                CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
                                objCell = New HtmlTableCell
                                bizCalendar = LoadControl("../include/calandar.ascx")
                                bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
                                objCell.Controls.Add(bizCalendar)
                                objRow.Cells.Add(objCell)
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
                                objCell = New HtmlTableCell
                                CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngDivID, dtTable.Rows(i).Item("fld_label"))
                            End If
                            k = k + 1
                        End If
                    Next
                    tblDetails.Rows.Add(objRow)

                    'CustomField Section
                    Dim Tab As Tab
                    'Dim pageView As PageView
                    Dim aspTable As HtmlTable
                    Dim Table As Table
                    Dim tblcell As TableCell
                    Dim tblRow As TableRow
                    k = 0
                    ViewState("TabId") = dtTable.Rows(0).Item("TabId")
                    ViewState("Check") = 0
                    ViewState("FirstTabCreated") = 0
                    ' Tabstrip3.Items.Clear()
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") <> 0 Then
                            If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                                If ViewState("Check") <> 0 Then
                                    aspTable.Rows.Add(objRow)
                                    tblcell.Controls.Add(aspTable)
                                    tblRow.Cells.Add(tblcell)
                                    Table.Rows.Add(tblRow)
                                    Tab.ContentPane.Controls.Add(Table)
                                End If
                                k = 0
                                ViewState("FirstTabCreated") = 1
                                ViewState("Check") = 1
                                '   If Not IsPostBack Then
                                ViewState("TabId") = dtTable.Rows(i).Item("TabId")
                                Tab = New Tab
                                Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
                                uwOppTab.Tabs.Add(Tab)
                                'End If
                                aspTable = New HtmlTable
                                Table = New Table
                                Table.Width = Unit.Percentage(100)
                                Table.BorderColor = System.Drawing.Color.FromName("black")
                                Table.GridLines = GridLines.None
                                Table.BorderWidth = Unit.Pixel(1)
                                Table.Height = Unit.Pixel(300)
                                Table.CssClass = "aspTable"
                                tblcell = New TableCell
                                tblRow = New TableRow
                                tblcell.VerticalAlign = VerticalAlign.Top
                                aspTable.Width = "100%"
                                objRow = New HtmlTableRow
                                objCell = New HtmlTableCell
                                objCell.InnerHtml = "<br>"
                                objRow.Cells.Add(objCell)
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            'pageView.Controls.Add("")
                            If k = 3 Then
                                k = 0
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If
                            objCell = New HtmlTableCell
                            objCell.Align = "right"
                            objCell.Attributes.Add("class", "normal1")
                            If dtTable.Rows(i).Item("fld_type") <> "Link" Then
                                objCell.InnerText = dtTable.Rows(i).Item("fld_label")
                            End If
                            objRow.Cells.Add(objCell)
                            If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                                objCell = New HtmlTableCell
                                CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                                objCell = New HtmlTableCell
                                CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                objCell = New HtmlTableCell
                                CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                                objCell = New HtmlTableCell
                                CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
                                PreviousRowID = i
                                objCell = New HtmlTableCell
                                bizCalendar = LoadControl("../include/calandar.ascx")
                                bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
                                objCell.Controls.Add(bizCalendar)
                                objRow.Cells.Add(objCell)
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
                                objCell = New HtmlTableCell
                                CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngDivID, dtTable.Rows(i).Item("fld_label"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
                                objCell = New HtmlTableCell
                                Dim strFrame As String
                                Dim URL As String
                                URL = dtTable.Rows(i).Item("vcURL")
                                URL = URL.Replace("RecordID", lngDivID)
                                strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                objCell.Controls.Add(New LiteralControl(strFrame))
                                objRow.Cells.Add(objCell)
                            End If
                            k = k + 1
                        End If
                    Next
                    If ViewState("Check") = 1 Then
                        aspTable.Rows.Add(objRow)
                        tblcell.Controls.Add(aspTable)
                        tblRow.Cells.Add(tblcell)
                        Table.Rows.Add(tblRow)
                        Tab.ContentPane.Controls.Add(Table)
                    End If
                End If
                Dim dvCusFields As DataView
                dvCusFields = dtTable.DefaultView
                dvCusFields.RowFilter = "fld_type='Date Field'"
                Dim iViewCount As Integer
                For iViewCount = 0 To dvCusFields.Count - 1
                    If Not IsDBNull(dvCusFields(iViewCount).Item("Value")) Then
                        bizCalendar = uwOppTab.FindControl("cal" & dvCusFields(iViewCount).Item("fld_id"))
                        Dim _myControlType As Type = bizCalendar.GetType()
                        _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                        strDate = dvCusFields(iViewCount).Item("Value")
                        If strDate = "0" Then strDate = ""
                        If strDate <> "" Then
                            'strDate = DateFromFormattedDate(strDate, Session("DateFormat"))
                            _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveCusField()
            Try
                If Not Session("CusFields") Is Nothing Then
                    Dim dtTable As DataTable
                    Dim i As Integer
                    dtTable = Session("CusFields")
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                            Dim txt As TextBox
                            txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            dtTable.Rows(i).Item("Value") = txt.Text
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                            Dim ddl As DropDownList
                            ddl = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            dtTable.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                            Dim chk As CheckBox
                            chk = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            If chk.Checked = True Then
                                dtTable.Rows(i).Item("Value") = "1"
                            Else : dtTable.Rows(i).Item("Value") = "0"
                            End If
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                            Dim txt As TextBox
                            txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            dtTable.Rows(i).Item("Value") = txt.Text
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
                            Dim BizCalendar As UserControl
                            BizCalendar = uwOppTab.FindControl("cal" & dtTable.Rows(i).Item("fld_id"))
                            Dim strDueDate As String
                            Dim _myControlType As Type = BizCalendar.GetType()
                            Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                            strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
                            If strDueDate <> "" Then
                                dtTable.Rows(i).Item("Value") = strDueDate
                            Else : dtTable.Rows(i).Item("Value") = ""
                            End If
                        End If
                    Next

                    Dim ds As New DataSet
                    Dim strdetails As String
                    dtTable.TableName = "Table"
                    ds.Tables.Add(dtTable.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))
                    Dim ObjCusfld As New CustomFields(Session("userid"))
                    ObjCusfld.strDetails = strdetails
                    ObjCusfld.locId = 1
                    ObjCusfld.RecordId = lngDivID
                    ObjCusfld.SaveCustomFldsByRecId()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to set the Association Information.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the imprints the association information on a screen label.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Sub LoadAssociationInformation()
            Try
                Dim objAssociationInfo As New OrgAssociations                   'Create a new object of association
                objAssociationInfo.DomainID = Session("DomainID")               'Set the Domain Id
                objAssociationInfo.DivisionID = lngDivID                           'Set the Division Id
                Dim dtAssociationInfo As DataTable                              'Declare a new DataTable
                dtAssociationInfo = objAssociationInfo.getParentOrgForCurrentOrg 'Get the Association Info
                If dtAssociationInfo.Rows.Count > 0 Then                        'Check if association exists where there is a parent-child relationship
                    lblAssociation.Text = dtAssociationInfo.Rows(0).Item("vcData") & " of: " & "<a href=""javascript: GoOrgDetails(" & dtAssociationInfo.Rows(0).Item("numDivisionId") & ");""><b>" & dtAssociationInfo.Rows(0).Item("vcCompanyName") & "</b></a>, " & dtAssociationInfo.Rows(0).Item("vcDivisionName")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActdelete.Click
            Try
                Dim objAccount As New CAccounts
                With objAccount
                    .DivisionID = lngDivID
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                End With
                If objAccount.DeleteOrg = False Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else : Response.Redirect("../Leads/frmLeadList.aspx")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlRelationhip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationhip.SelectedIndexChanged
            Try
                LoadProfile()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadProfile()
            Try
                If objUserAccess Is Nothing Then objUserAccess = New UserAccess
                objUserAccess.RelID = ddlRelationhip.SelectedItem.Value
                objUserAccess.DomainID = Session("DomainID")
                ddlProfile.DataSource = objUserAccess.GetRelProfileD
                ddlProfile.DataTextField = "ProName"
                ddlProfile.DataValueField = "numProfileID"
                ddlProfile.DataBind()
                ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))

                objUserAccess.RelID = ddlRelationhip.SelectedItem.Value
                ddlFollow.DataSource = objUserAccess.GetRelFollowD
                ddlFollow.DataTextField = "Follow"
                ddlFollow.DataValueField = "numFollowID"
                ddlFollow.DataBind()
                ddlFollow.Items.Insert(0, New ListItem("---Select One---", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlTerritory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerritory.SelectedIndexChanged
            Try
                Dim lngAssignTo As Long
                lngAssignTo = ddlAssignedTo.SelectedValue
                objCommon.sb_FillConEmpFromTerritories(ddlAssignedTo, Session("DomainID"), 0, 0, ddlTerritory.SelectedValue)
                If Not ddlAssignedTo.Items.FindByValue(lngAssignTo) Is Nothing Then ddlAssignedTo.Items.FindByValue(lngAssignTo).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
