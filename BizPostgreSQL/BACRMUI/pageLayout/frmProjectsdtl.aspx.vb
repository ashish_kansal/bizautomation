
'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  20/3/2005
'***************************************************************************************************************************
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Admin
Imports Infragistics.WebUI.UltraWebTab
Imports System.Reflection

Namespace BACRM.UserInterface.Projects
    Public Class frmProjectsdtl
        Inherits BACRMPage

        Dim objProjects As Project
        Dim objOpportunity As MOpportunity
        Dim objCommon As New CCommon
        Dim dtAssignTo As DataTable
        Dim myRow As DataRow
        Dim arrOutPut() As String = New String(2) {}
        Dim m_aryRightsForAssContacts(), m_aryRightsForPage() As Integer
        Dim lngProID As Long
        Dim intDivId As Long
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim dtMilestone As DataTable
        Dim dtContactInfo As DataTable
        Dim strColumn As String

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Response.Redirect("../projects/frmProjects.aspx?" & GetQueryStringVal(Request.QueryString("enc"), "", True))

                If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                    SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                    SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
                Else : SI1 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
                Else : SI2 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                    frm = ""
                    frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
                Else : frm = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                    frm1 = ""
                    frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
                Else : frm1 = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    frm2 = ""
                    frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
                Else : frm2 = ""
                End If
                lngProID = GetQueryStringVal(Request.QueryString("enc"), "ProId")

                m_aryRightsForAssContacts = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjects.aspx", Session("userID"), 12, 4)
                If Not IsPostBack Then
                    If uwOppTab.Tabs.Count > SI Then uwOppTab.SelectedTabIndex = SI
                    calFrom.SelectedDate = Session("StartDate")
                    calTo.SelectedDate = Session("EndDate")
                    LoadTableInformation()
                    Dim objContacts As New CContacts
                    objContacts.RecID = lngProID
                    objContacts.Type = "P"
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.AddVisiteddetails()
                    ''''call function for sub-tab management  - added on 30jul09 by Mohan
                    'objCommon.ManageSubTabs(uwOppTab, Session("DomainID"), 12)
                    '''''
                    Session("Help") = "Project"
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjects.aspx", Session("userID"), 12, 3)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else
                        If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnEdit.Visible = False
                        If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False
                    End If
                    If lngProID <> 0 Then
                        tblMenu.Visible = True
                    Else : tblMenu.Visible = False
                    End If
                    btnLayout.Attributes.Add("onclick", "return ShowLayout('R','" & lngProID & "');")
                    btnTransfer.Attributes.Add("onclick", "return OpenTransfer('" & "../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Projects&tyrCV=" & lngProID & "')")
                    EmailHistory()
                End If
                Dim strDate As String = FormattedDateFromDate(Now(), Session("DateFormat"))
                If Not dtMilestone Is Nothing Then CreatMilestone()
                DisplayDynamicFlds()
                btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTableInformation()
            Try
                Dim dtTableInfo As DataTable
                Dim ds As New DataSet
                Dim objPageLayout As New CPageLayout
                Dim check As String
                Dim fields() As String
                Dim idcolumn As String = ""
                Dim count1 As Integer
                Dim x As Integer

                objPageLayout.CoType = "R"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngProID
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 11
                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)

                Dim dtDetails As DataTable
                objPageLayout.ProjectId = lngProID
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                'dtDetails = objPageLayout.ProjectDtlPL
                lblTProgress.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("TProgress")), 0, dtDetails.Rows(0).Item("TProgress").ToString)
                hplCustomer.Text = dtDetails.Rows(0).Item("vcCompanyName")
                txtProName.Text = dtDetails.Rows(0).Item("vcprojectName")
                If dtDetails.Rows(0).Item("tintCRMType") = 0 Then
                    hplCustomer.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ProDetail&DivID=" & dtDetails.Rows(0).Item("numDivisionID") & "&ProId=" & lngProID & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")
                ElseIf dtDetails.Rows(0).Item("tintCRMType") = 1 Then
                    hplCustomer.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ProDetail&DivID=" & dtDetails.Rows(0).Item("numDivisionID") & "&ProId=" & lngProID & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")
                ElseIf dtDetails.Rows(0).Item("tintCRMType") = 2 Then
                    hplCustomer.NavigateUrl = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ProDetail&klds+7kldf=fjk-las&DivId=" & dtDetails.Rows(0).Item("numDivisionID") & "&ProId=" & lngProID & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")
                End If

                Dim numrows As Integer = dtTableInfo.Compute("Max(tintrow)", String.Empty)
                Dim numcells As Integer = dtTableInfo.Compute("Max(intcoulmn)", String.Empty)
                Dim i As Integer = 0
                Dim nr As Integer
                Dim noRowsToLoop As Integer
                noRowsToLoop = (dtTableInfo.Rows.Count - (numrows * numcells)) / numcells
                noRowsToLoop = noRowsToLoop + numrows + 1

                For nr = 0 To noRowsToLoop
                    Dim r As New TableRow()
                    Dim nc As Integer
                    Dim ro As Integer = nr
                    For nc = 1 To numcells
                        If dtTableInfo.Rows.Count <> i Then
                            If dtTableInfo.Rows(i).Item("tintrow") = nr + 1 And dtTableInfo.Rows(i).Item("intcoulmn") = nc Then
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                Dim fieldId As Integer
                                Dim bitDynFld As String
                                bitDynFld = dtTableInfo.Rows(i).Item("bitCustomField")
                                fieldId = CInt(dtTableInfo.Rows(i).Item("numFieldID").ToString)
                                column1.CssClass = "normal7"
                                If (bitDynFld <> "1") Then
                                    If (fieldId = "124") Then
                                        Dim h As New HyperLink()

                                        h.NavigateUrl = "#"
                                        h.Text = "Documents"
                                        h.Attributes.Add("onclick", "return OpenDocuments(" & lngProID & ");")
                                        Dim l As New Label
                                        l.CssClass = "normal7"
                                        l.Text = "(" & IIf(IsDBNull(dtDetails.Rows(0).Item("DocumentCount")), "", dtDetails.Rows(0).Item("DocumentCount")) & ")" & "&nbsp;:"
                                        column1.Controls.Add(h)
                                        column1.Controls.Add(l)
                                    Else : column1.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                    End If

                                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName").ToString) And dtTableInfo.Rows(i).Item("vcDBColumnName").ToString() <> "" Then
                                        Dim temp As String
                                        temp = dtTableInfo.Rows(i).Item("vcDBColumnName").ToString
                                        fields = temp.Split(",")
                                        Dim j As Integer
                                        j = 0
                                        While (j < fields.Length)
                                            If (fieldId = "115" Or fieldId = "124") Then
                                                Dim listvalue As String
                                                Dim value As String = ""
                                                listvalue = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))

                                                Select Case fieldId   ' Must be a primitive data type
                                                    Case 115
                                                        If Not IsDBNull(dtDetails.Rows(0).Item("intDueDate")) Then
                                                            value = FormattedDateFromDate(dtDetails.Rows(0).Item("intDueDate"), Session("DateFormat"))
                                                        End If
                                                    Case 124 : value = ""
                                                    Case Else
                                                End Select
                                                column2.Text = value
                                            Else : column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))
                                            End If
                                            j += 1
                                        End While
                                    Else : column1.Text = ""
                                    End If ' end of table cell2
                                Else
                                    If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                        column1.Text = "Custom Web Link :"
                                        Dim h As New HyperLink()
                                        h.CssClass = "hyperlink"
                                        Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                        url = url.Replace("RecordID", lngProID)
                                        h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                        column2.Controls.Add(h)
                                    ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                        Dim strDate As String
                                        strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                        If strDate = "0" Then strDate = ""
                                        If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                    Else
                                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                        If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                Dim l As New Label
                                                l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                                l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                column2.Controls.Add(l)
                                            End If
                                        Else
                                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                            End If
                                        End If
                                    End If
                                End If

                                column2.CssClass = "normal1"
                                column1.HorizontalAlign = HorizontalAlign.Right
                                column2.HorizontalAlign = HorizontalAlign.Left

                                column1.Width = 250
                                column2.Width = 300
                                column2.ColumnSpan = 1
                                column1.ColumnSpan = 1
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                                i += 1
                            ElseIf dtTableInfo.Rows(i).Item("tintrow") = 0 And dtTableInfo.Rows(i).Item("intcoulmn") = 0 Then
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                column1.CssClass = "normal7"
                                If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                    column1.Text = "Custom Web Link :"
                                    Dim h As New HyperLink()
                                    h.CssClass = "hyperlink"
                                    Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                    url = url.Replace("RecordID", lngProID)
                                    h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                    h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                    column2.Controls.Add(h)
                                ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                    column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                    Dim strDate As String
                                    strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                    If strDate = "0" Then strDate = ""
                                    If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                Else
                                    column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                    If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                        If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                            Dim l As New Label
                                            l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                            l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                            column2.Controls.Add(l)
                                        End If
                                    Else
                                        If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                            column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                        End If
                                    End If
                                End If
                                column2.CssClass = "normal1"
                                column1.HorizontalAlign = HorizontalAlign.Right
                                column2.HorizontalAlign = HorizontalAlign.Left
                                column1.Width = 250
                                column2.Width = 300
                                column2.ColumnSpan = 1
                                column1.ColumnSpan = 1
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                                i += 1
                            Else
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                column1.Text = ""
                                column2.Text = ""
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                            End If
                        End If
                    Next nc
                    tabledetail.Rows.Add(r)
                Next nr

                If (dtDetails.Rows.Count > 0) Then
                    Dim column1 As New TableCell
                    Dim column2 As New TableCell
                    Dim r As New TableRow
                    column1.CssClass = "normal7"
                    column2.CssClass = "normal1"
                    column1.HorizontalAlign = HorizontalAlign.Right
                    column2.HorizontalAlign = HorizontalAlign.Justify
                    column2.ColumnSpan = 5
                    Dim l As New Label
                    l.CssClass = "normal7"
                    l.Text = "Comments" & "&nbsp;:"
                    column1.Controls.Add(l)
                    column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("txtComments")), "-", dtDetails.Rows(0).Item("txtComments"))
                    column1.Width = 150
                    r.Cells.Add(column1)
                    r.Cells.Add(column2)
                    tableComment.Rows.Add(r)
                End If
                If objProjects Is Nothing Then objProjects = New Project
                objProjects.ProjectID = lngProID
                objProjects.DomainID = Session("DomainId")
                objProjects.ContactID = Session("UserContactID")
                dtMilestone = objProjects.SalesProcessDtlByProId
                If dtMilestone.Rows.Count = 0 Then ViewState("MileCheck") = 1
                CreatMilestone()
                If Not IsPostBack Then LoadSavedInformation(dtDetails)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadSavedInformation(ByVal dtDetails As DataTable)
            Try
                If objProjects Is Nothing Then objProjects = New Project
                objProjects.ProjectID = lngProID
                objProjects.DomainID = Session("DomainID")

                txtDivId.Text = dtDetails.Rows(0).Item("numDivisionID")
                intDivId = dtDetails.Rows(0).Item("numDivisionID")
                LoadContractsInfo(dtDetails.Rows(0).Item("numDivisionID"))
                If Not IsPostBack Then
                    If Not ddlContract.Items.FindByValue(dtDetails.Rows(0).Item("numcontractId")) Is Nothing Then
                        ddlContract.SelectedItem.Selected = False
                        ddlContract.Items.FindByValue(dtDetails.Rows(0).Item("numcontractId")).Selected = True
                        BindContractinfo()
                    End If
                End If

                lblLastModifiedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcModifiedby")), "", dtDetails.Rows(0).Item("vcModifiedby"))
                lblCreatedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcCreatedBy")), "", dtDetails.Rows(0).Item("vcCreatedBy"))
                lblRecordOwner.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcRecOwner")), "", dtDetails.Rows(0).Item("vcRecOwner"))

                dgContact.DataSource = objProjects.AssCntsByProId
                dgContact.DataBind()

                'objProjects.DomainID = Session("DomainId")
                'objProjects.ContactID = Session("UserContactID")
                'dtMilestone = objProjects.SalesProcessDtlByProId
                'If dtMilestone.Rows.Count = 0 Then
                '    ViewState("MileCheck") = 1
                'End If
                'CreatMilestone()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadContractsInfo(ByVal intDivId As Integer)
            Try
                Dim objContract As New CContracts
                Dim dtTable As DataTable
                objContract.DivisionId = txtDivId.Text
                objContract.UserCntId = Session("UserContactId")
                objContract.DomainId = Session("DomainId")
                dtTable = objContract.GetContractDdlList()
                ddlContract.DataSource = dtTable
                If dtTable.Rows.Count > 0 Then
                    pnlContract.Visible = True
                Else : pnlContract.Visible = False
                End If
                ddlContract.DataTextField = "vcContractName"
                ddlContract.DataValueField = "numcontractId"
                ddlContract.DataBind()
                ddlContract.Items.Insert(0, "--Select One--")
                ddlContract.Items.FindByText("--Select One--").Value = "0"
                lblRemAmount.Text = "0"
                lblRemDays.Text = "0"
                lblRemInci.Text = "0"
                lblRemHours.Text = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlContract_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContract.SelectedIndexChanged
            Try
                BindContractinfo()
                LoadTableInformation()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindContractinfo()
            Try
                Dim objContract As New CContracts
                Dim dtTable As DataTable
                objContract.ContractID = ddlContract.SelectedValue
                objContract.UserCntId = Session("UserContactId")
                objContract.DomainId = Session("DomainId")
                dtTable = objContract.GetContractDtl()
                pnlContract.Visible = True
                If dtTable.Rows.Count > 0 Then
                    lblRemAmount.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemAmount")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemAmount")))
                    lblRemHours.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemHours")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemHours")))
                    lblRemDays.Text = IIf(IsDBNull(dtTable.Rows(0).Item("days")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("days")))
                    If dtTable.Rows(0).Item("bitincidents") = True Then
                        lblRemInci.Text = IIf(IsDBNull(dtTable.Rows(0).Item("Incidents")), 0, dtTable.Rows(0).Item("Incidents"))
                    Else : lblRemInci.Text = 0
                    End If
                Else
                    lblRemAmount.Text = 0
                    lblRemHours.Text = 0
                    lblRemDays.Text = 0
                    lblRemInci.Text = 0
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub EmailHistory()
            Try
                txtEmailPage.Text = 1
                BindEmailData()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindEmailData()
            Try
                Dim dtEmail As DataTable
                Dim objCases As New CCases
                objCases.DomainID = Session("DomainId")
                objCases.SortOrder = ddlFields.SelectedItem.Value
                objCases.FromDate = calFrom.SelectedDate
                objCases.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))
                objCases.ProCaseNo = txtProName.Text
                objCases.KeyWord = txtSearchEmail.Text
                If txtEmailPage.Text.Trim <> "" Then
                    objCases.CurrentPage = txtEmailPage.Text
                Else : objCases.CurrentPage = 1
                End If
                objCases.PageSize = Session("PagingRows")
                objCases.TotalRecords = 0
                If strColumn <> "" Then
                    objCases.columnName = strColumn
                Else : objCases.columnName = "numEmailHstrID"
                End If
                If Session("Asc") = 1 Then
                    objCases.columnSortOrder = "Desc"
                Else : objCases.columnSortOrder = "Asc"
                End If
                dtEmail = objCases.GetEmailList
                If objCases.TotalRecords = 0 Then
                    tdEmailNav.Visible = False
                    lblEmailNoofRecords.Text = objCases.TotalRecords
                Else
                    tdEmailNav.Visible = True
                    lblEmailNoofRecords.Text = objCases.TotalRecords
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblEmailNoofRecords.Text / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    lblEmailPageNo.Text = strTotalPage(0) + 1
                    txtEmailTotalPage.Text = strTotalPage(0) + 1
                    txtEmailTotalRecords.Text = lblEmailNoofRecords.Text
                End If
                dgEmail.DataSource = dtEmail
                dgEmail.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgEmail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgEmail.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    e.Item.Cells(2).Attributes.Add("onclick", "return OpenEmailMessage('" & e.Item.Cells(1).Text & "','" & e.Item.Cells(0).Text & "')")
                    e.Item.Cells(2).Attributes.Add("class", "hyperlink")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgEmail_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgEmail.SortCommand
            Try
                strColumn = e.SortExpression.ToString()
                If Session("Column") <> strColumn Then
                    Session("Column") = strColumn
                    Session("Asc") = 0
                Else
                    If Session("Asc") = 0 Then
                        Session("Asc") = 1
                    Else : Session("Asc") = 0
                    End If
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnEmailGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailGo.Click
            Try
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub linkEmailLast_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkEmailLast.Click
            Try
                txtEmailPage.Text = txtEmailTotalPage.Text
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmailPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmailPrevious.Click
            Try
                If txtEmailPage.Text = 1 Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text - 1
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmailNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmailNext.Click
            Try
                If txtEmailPage.Text = txtEmailTotalPage.Text Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text + 1
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmailFirst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmailFirst.Click
            Try
                txtEmailPage.Text = 1
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmail2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmail2.Click
            Try
                If txtEmailPage.Text + 1 = txtEmailTotalPage.Text Or txtEmailPage.Text + 1 > txtEmailTotalPage.Text Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text + 2
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmail4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmail4.Click
            Try
                If txtEmailPage.Text + 3 = txtEmailTotalPage.Text Or txtEmailPage.Text + 3 > txtEmailTotalPage.Text Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text + 4
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmail5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmail5.Click
            Try
                If txtEmailPage.Text + 4 = txtEmailTotalPage.Text Or txtEmailPage.Text + 4 > txtEmailTotalPage.Text Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text + 5
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkEmal3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmal3.Click
            Try
                If txtEmailPage.Text + 2 = txtEmailTotalPage.Text Or txtEmailPage.Text + 2 > txtEmailTotalPage.Text Then
                    Exit Sub
                Else : txtEmailPage.Text = txtEmailPage.Text + 3
                End If
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub txtEmailPage_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmailPage.TextChanged
            Try
                BindEmailData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub CreatMilestone()
            Try
                tblMilestone.Rows.Clear()
                If dtMilestone.Rows.Count <> 0 Then
                    Dim i As Integer
                    Dim tblCell As TableCell
                    Dim tblrow As TableRow
                    Dim chkDlost As CheckBox
                    Dim chkDClosed As CheckBox
                    Dim chkDclosed1 As Label
                    Dim chkDclosed2 As Label
                    Dim btnAdd As New Button
                    Dim btnDelete As New Button
                    Dim InitialPercentage As Integer = dtMilestone.Rows(0).Item(0)
                    If Not dtMilestone.Rows(0).Item(0) = 100 Then
                        If Not dtMilestone.Rows(0).Item("Op_Flag") = 1 Then
                            tblrow = New TableRow
                            tblCell = New TableCell
                            tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Milestone - " & InitialPercentage & "%&nbsp;&nbsp;" & dtMilestone.Rows(0).Item("vcStagePercentageDtl") & "</font>"
                            tblCell.Height = Unit.Pixel(20)
                            tblCell.CssClass = "text_bold"
                            tblCell.ColumnSpan = 4
                            tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                            tblrow.Controls.Add(tblCell)
                            tblMilestone.Controls.Add(tblrow)
                        End If
                    End If
                    ViewState("CheckColor") = 0
                    For i = 0 To dtMilestone.Rows.Count - 1
                        If dtMilestone.Rows(i).Item(0) <> 100 Then
                            If dtMilestone.Rows(i).Item(0) <> 0 Then
                                If InitialPercentage = dtMilestone.Rows(i).Item(0) Then
                                    If dtMilestone.Rows(i).Item("Op_Flag") <> 1 Then
                                        createStages(dtMilestone.Rows(i))
                                        InitialPercentage = dtMilestone.Rows(i).Item(0)
                                    End If
                                Else
                                    If dtMilestone.Rows(i).Item("Op_Flag") <> 1 Then
                                        InitialPercentage = dtMilestone.Rows(i).Item(0)
                                        tblrow = New TableRow
                                        tblCell = New TableCell
                                        tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Milestone - " & InitialPercentage & "%&nbsp;&nbsp;" & dtMilestone.Rows(i).Item("vcStagePercentageDtl") & "</font>"
                                        tblCell.Height = Unit.Pixel(20)
                                        tblCell.CssClass = "text_bold"
                                        tblCell.ColumnSpan = 4
                                        tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                                        tblrow.Controls.Add(tblCell)
                                        tblMilestone.Controls.Add(tblrow)
                                        ViewState("CheckColor") = 0
                                        createStages(dtMilestone.Rows(i))
                                    End If
                                End If
                            End If
                        End If
                    Next

                    ''Project Conclusion
                    tblrow = New TableRow
                    Dim txtDComments As Label
                    Dim lblComm As Label
                    tblCell = New TableCell
                    tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp; Conclusion</font>"
                    tblCell.Height = Unit.Pixel(20)
                    tblCell.CssClass = "text_bold"
                    tblCell.ColumnSpan = 4
                    tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                    tblrow.Controls.Add(tblCell)
                    tblMilestone.Controls.Add(tblrow)

                    '' Project Completion Details
                    tblrow = New TableRow
                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblCell.ColumnSpan = 4
                    ' chkDClosed = New CheckBox
                    chkDclosed1 = New Label
                    chkDclosed2 = New Label
                    '    chkDClosed.ID = "chkDClosed"
                    chkDclosed1.Text = "Project Completed &nbsp;"
                    chkDclosed1.CssClass = "normal8"
                    If (dtMilestone.Rows(dtMilestone.Rows.Count - 1).Item("bitStageCompleted")) = True Then
                        'chkDclosed2.Text = "a"
                        'chkDclosed2.CssClass = "cell1"
                        chkDclosed1.Text = "&nbsp; Project Completed &nbsp;"
                    Else
                        'chkDclosed2.Text = "r"
                        'chkDclosed2.CssClass = "cell"
                        chkDclosed1.Text = "&nbsp; Project Not Completed &nbsp;"
                    End If
                    lblComm = New Label
                    lblComm.Text = "&nbsp;&nbsp;Comments : &nbsp; "
                    lblComm.CssClass = "normal7"
                    txtDComments = New Label
                    txtDComments.CssClass = "normal1"
                    txtDComments.ID = "txtDCComm"
                    txtDComments.Text = dtMilestone.Rows(dtMilestone.Rows.Count - 1).Item("vcComments")
                    txtDComments.Width = Unit.Pixel(600)
                    tblCell.Controls.Add(chkDclosed1)
                    tblCell.Controls.Add(chkDclosed2)
                    tblCell.CssClass = "normal1"
                    tblCell.Controls.Add(lblComm)
                    tblCell.Controls.Add(txtDComments)
                    tblrow.Controls.Add(tblCell)
                    tblMilestone.Controls.Add(tblrow)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub createStages(ByVal dr As DataRow)
            Try
                Dim tblCell As TableCell
                Dim tblrow As TableRow
                Dim lbl As Label
                Dim chkStage As CheckBox
                Dim chkStage1 As Label
                Dim chkStage2 As Label
                Dim txtStage As Label
                Dim lblStageCM As Label
                Dim lblInform As Label
                Dim txtComm As Label
                Dim lblEvent As Label
                Dim lblReminder As Label
                Dim lblET As Label
                Dim lblActivity As Label
                Dim lblStartDay As Label
                Dim lblStartTime As Label
                Dim lblEndtime As Label
                Dim lblBody As Label
                Dim ddlStatus As Label

                Dim ddlAgginTo As Label
                'Dim chkAlert As Label
                Dim chkAlert1 As Label
                Dim chkAlert2 As Label
                Dim txtChecKStage As Label
                Dim hpkLink As HyperLink
                Dim txtDueDate As Label
                Dim lblAssignTo As Label
                Dim dtSubStages As DataTable
                dtSubStages = Session("SalesProcess")
                dtAssignTo = Session("AssignTo")

                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                tblCell.ColumnSpan = 4
                lbl = New Label
                txtComm = New Label
                'txtComm.ID = "txtComm~" & dr.Item("vcstageDetail") & "~" & dr.Item("vcstageDetail")
                txtComm.Width = Unit.Pixel(750)
                txtComm.CssClass = "normal9"
                txtComm.Text = IIf(IsDBNull(dr.Item("vcstageDetail")), "", dr.Item("vcstageDetail"))
                Dim strImage As String

                strImage = "<img src='../images/mileStone.gif'  > "
                tblCell.Controls.Add(New LiteralControl(strImage))
                tblCell.Controls.Add(txtComm)
                tblCell.CssClass = "normal1"
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                '''First row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                'txtStage = New Label
                'txtStage.ID = "txtStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                'txtStage.Text = "&nbsp;" & dr.Item("vcstageDetail")
                'txtStage.CssClass = "normal8"
                'txtStage.Width = Unit.Pixel(250)
                'tblCell.ColumnSpan = 2
                'tblCell.CssClass = "normal8"
                'tblCell.Controls.Add(txtStage)

                lblStageCM = New Label
                lblStageCM.Text = "&nbsp;Stage Status&nbsp;&nbsp;&nbsp;:"
                lblStageCM.ID = "lblStatus" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                lblStageCM.CssClass = "normal8"
                ddlStatus = New Label

                ddlStatus.Text = IIf(IsDBNull(dr.Item("vcStage")), "", dr.Item("vcStage"))

                ddlStatus.CssClass = "normal1"
                ' ddlStatus.ID = "ddlStatus~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                tblCell.CssClass = "normal1"
                tblCell.Controls.Add(lblStageCM)
                tblCell.Controls.Add(ddlStatus)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                lblStageCM = New Label
                lblStageCM.Text = "Last Modified By :  "
                lblStageCM.CssClass = "normal8"
                lblStageCM.ID = "lblStageModifiedBy" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                lblInform = New Label
                If Not IsDBNull(dr.Item("numModifiedBy")) Then lblInform.Text = IIf(dr.Item("numModifiedBy") = 0, "", dr.Item("numModifiedByName"))
                lblInform.ID = "lblInformBy" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                tblCell.CssClass = "normal1"
                tblCell.Controls.Add(lblStageCM)
                tblCell.Controls.Add(lblInform)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.ColumnSpan = 2
                lblStageCM = New Label
                lblStageCM.Text = "Last Modified Date :  "
                lblStageCM.CssClass = "normal8"
                lblStageCM.ID = "lblStageModified" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                lblInform = New Label
                lblInform.Text = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), dr.Item("bintModifiedDate")), Session("DateFormat"))
                lblInform.ID = "lblInform" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                tblCell.CssClass = "normal1"
                tblCell.Controls.Add(lblStageCM)
                tblCell.Controls.Add(lblInform)
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                'Second Row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                tblCell.ColumnSpan = 4
                lbl = New Label
                txtComm = New Label
                txtComm.ID = "txtComm~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                txtComm.Width = Unit.Pixel(750)
                txtComm.CssClass = "normal1"
                txtComm.Text = dr.Item("vcComments")
                lbl.Text = "&nbsp;Comments &nbsp;:"
                lbl.CssClass = "normal8"
                tblCell.Controls.Add(lbl)
                tblCell.Controls.Add(txtComm)
                tblCell.CssClass = "normal1"
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                ''Third Row
                If ViewState("MileCheck") = 0 Then
                    tblrow = New TableRow
                    If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    hpkLink = New HyperLink
                    hpkLink.ID = "hpkLinkTime" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

                    hpkLink.Attributes.Add("onclick", "return OpenTime(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & "," & txtDivId.Text & ");")

                    hpkLink.Text = "<font class='hyperlink'>&nbsp;Time</font>"
                    hpkLink.CssClass = "hyperlink"
                    lbl = New Label
                    lbl.ID = "lblTime" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    If Not IsDBNull(dr.Item("Time")) Then lbl.Text = "<font color=red>&nbsp;" & dr.Item("Time") & "</font>"

                    tblCell.Controls.Add(hpkLink)
                    tblCell.Controls.Add(lbl)
                    tblrow.Controls.Add(tblCell)

                    tblCell = New TableCell
                    hpkLink = New HyperLink
                    hpkLink.ID = "hpkLinkExpense" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    'If Not dr.Item("numOppStageID") Is Nothing Then
                    hpkLink.Attributes.Add("onclick", "return OpenExpense(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & "," & txtDivId.Text & ");")

                    hpkLink.Text = "<font class='hyperlink'>Expense</font>"
                    hpkLink.CssClass = "hyperlink"
                    tblCell.CssClass = "normal1"
                    lbl = New Label
                    lbl.ID = "lblExp" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    If Not IsDBNull(dr.Item("Expense")) Then
                        lbl.Text = "<font color=red>&nbsp;" & String.Format("{0:#,##0.00}", CDec(dr.Item("Expense"))) & "</font>"
                    End If
                    tblCell.Controls.Add(hpkLink)
                    tblCell.Controls.Add(lbl)
                    tblrow.Controls.Add(tblCell)

                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    hpkLink = New HyperLink
                    hpkLink.ID = "hpkLinkDependency" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

                    hpkLink.Attributes.Add("onclick", "return OpenDependency(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")
                    'End If
                    hpkLink.Text = "<font class='hyperlink'>Dependency</font>"
                    hpkLink.CssClass = "hyperlink"
                    lbl = New Label
                    lbl.ID = "lblDep" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    If Not IsDBNull(dr.Item("Depend")) Then
                        If dr.Item("Depend") = "1" Then
                            lbl.Text = "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>"
                        End If
                    End If
                    tblCell.Controls.Add(hpkLink)
                    tblCell.Controls.Add(lbl)
                    tblrow.Controls.Add(tblCell)

                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    lbl = New Label

                    hpkLink = New HyperLink
                    hpkLink.ID = "hpkLinkSubStage" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

                    hpkLink.Text = "<font  class='hyperlink'>Sub Stages</font>"
                    hpkLink.CssClass = "hyperlink"

                    hpkLink.Attributes.Add("onclick", "return OpenSubStage(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")
                    'End If
                    lbl.ID = "lblStg" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                    If Not IsDBNull(dr.Item("SubStg")) Then
                        If dr.Item("SubStg") = "1" Then
                            lbl.Text = "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>"
                        End If
                    End If

                    tblCell.Controls.Add(hpkLink)
                    tblCell.Controls.Add(lbl)
                    tblrow.Controls.Add(tblCell)
                    tblMilestone.Controls.Add(tblrow)
                End If

                ''''Fourth Row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "&nbsp;Assign To :"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)

                lblAssignTo = New Label
                'If Not IsDBNull(dr.Item("vcAssignTo")) Then
                '    'If Not ddlAgginTo.Items.FindByValue(dr.Item("numAssignTo")) Is Nothing Then
                '    lblAssignTo.Text = dr.Item("vcAssignTo")
                '    'End If
                'End If
                lblAssignTo.Text = IIf(IsDBNull(dr.Item("vcAssignTo")), "", dr.Item("vcAssignTo"))
                tblCell.Controls.Add(lblAssignTo)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                chkAlert1 = New Label
                chkAlert2 = New Label
                chkAlert2.Text = ""
                chkAlert1.ID = "chkAlert~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                chkAlert1.Text = "Alert :"
                chkAlert1.CssClass = "normal8"
                If dr.Item("bitAlert") = 1 Then
                    chkAlert2.Text = "a"
                    chkAlert2.CssClass = "cell1"
                ElseIf (dr.Item("bitAlert") = 0) Then
                    chkAlert2.Text = "r"
                    chkAlert2.CssClass = "cell"
                End If

                tblCell.Controls.Add(chkAlert1)
                tblCell.Controls.Add(chkAlert2)
                tblrow.Controls.Add(tblCell)

                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "&nbsp; &nbsp; &nbsp; "

                tblCell.Controls.Add(lbl)
                lbl = New Label
                lbl.Text = "Due Date: &nbsp;"
                lbl.CssClass = "normal7"
                hpkLink = New HyperLink
                hpkLink.ID = "hpkLinkDueDate" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                txtDueDate = New Label
                txtDueDate.CssClass = "signup"
                txtDueDate.ID = "txtDueDate :" & dr.Item("numStagePercentage") & dr.Item("numstagedetailsID")
                If Not IsDBNull(dr.Item("bintDueDate")) Then
                    txtDueDate.Text = FormattedDateFromDate(dr.Item("bintDueDate"), Session("DateFormat"))
                End If

                tblCell.Controls.Add(lbl)
                tblCell.Controls.Add(txtDueDate)

                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell

                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Stage Completed : "
                lbl.CssClass = "normal8"
                tblCell.Controls.Add(lbl)
                lbl = New Label
                If Not IsDBNull(dr.Item("bintStageComDate")) Then
                    If dr.Item("bitStageCompleted") = True Then
                        lbl.Text = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), dr.Item("bintStageComDate")), Session("DateFormat"))
                    End If
                End If
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.CssClass = "normal7"
                'chkStage = New CheckBox
                'txtChecKStage = New Label
                ' txtChecKStage.ID = "txtChecKStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                ' txtChecKStage.Attributes.Add("style", "display:none")
                If dr.Item("bitStageCompleted") = False Then
                    lbl.Text = "Stage Open"
                Else
                    ' txtChecKStage.Text = 1
                    lbl.Text = "<font color='#CC0000'><b>Stage Closed</b></font> : " & dr.Item("tintPercentage") & " %"
                    'chkStage.Checked = True
                End If

                ' chkStage.ID = "chkStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                tblCell.Controls.Add(lbl)
                ' tblCell.Controls.Add(txtChecKStage)
                '  tblCell.Controls.Add(chkStage)
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)
                ''fifth row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Event &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)

                lblEvent = New Label
                lblEvent.Text = " " & IIf(IsDBNull(dr.Item("vcEvent")), "-", dr.Item("vcEvent"))

                tblCell.Controls.Add(lblEvent)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Remider &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lblReminder = New Label
                lblReminder.Text = " " & IIf(IsDBNull(dr.Item("vcReminder")), "-", dr.Item("vcReminder"))
                tblCell.Controls.Add(lblReminder)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Email Template &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lblET = New Label
                lblET.Text = " " & IIf(IsDBNull(dr.Item("vcET")), "-", dr.Item("vcET"))
                tblCell.Controls.Add(lblET)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell

                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Activity : "
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lbl = New Label
                lbl.Text = " " & IIf(IsDBNull(dr.Item("vcActivity")), "-", dr.Item("vcActivity"))
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)

                tblMilestone.Controls.Add(tblrow)

                ''Sixth row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Start Date &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)

                lblStartDay = New Label
                lblStartDay.Text = " " & IIf(IsDBNull(dr.Item("vcStartDate")), "-", dr.Item("vcStartDate"))

                tblCell.Controls.Add(lblStartDay)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Start Time &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lblStartTime = New Label
                lblStartTime.Text = " " & IIf(IsDBNull(dr.Item("vcStartTime")), "-", dr.Item("vcStartTime")) & " " & IIf(IsDBNull(dr.Item("vcStartTimePeriod")), "-", dr.Item("vcStartTimePeriod"))
                tblCell.Controls.Add(lblStartTime)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "End Time &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lblEndtime = New Label
                lblEndtime.Text = " " & IIf(IsDBNull(dr.Item("vcEndTime")), "-", dr.Item("vcEndTime")) & " " & IIf(IsDBNull(dr.Item("vcEndTimePeriod")), "-", dr.Item("vcEndTimePeriod"))
                tblCell.Controls.Add(lblEndtime)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Action Item Type &nbsp;:"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lblEndtime = New Label
                lblEndtime.Text = " " & IIf(IsDBNull(dr.Item("vcType")), "-", dr.Item("vcType"))
                tblCell.Controls.Add(lblEndtime)
                tblrow.Controls.Add(tblCell)

                'tblCell = New TableCell
                'tblCell.CssClass = "normal1"
                'tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                ''Seventh row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell
                tblCell.ColumnSpan = 4
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.Text = "Comments 2 :"
                lbl.CssClass = "normal7"
                tblCell.Controls.Add(lbl)
                lbl = New Label
                lbl.Text = " " & dr.Item("txtCom")
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)
                ''Eight row
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                tblCell = New TableCell

                tblCell.CssClass = "normal1"
                lbl = New Label
                If dr.Item("bitChgStatus") = True Then
                    lbl.Text = "a"
                    lbl.CssClass = "cell1"
                Else
                    lbl.Text = "r"
                    lbl.CssClass = "cell"
                End If
                tblCell.Controls.Add(lbl)
                lbl = New Label
                lbl.CssClass = "normal7"
                lbl.Text = " When done change stage status to "
                tblCell.Controls.Add(lbl)
                lbl = New Label
                lbl.CssClass = "normal1"
                lbl.Text = " " & dr.Item("vcChgStatus")
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.ColumnSpan = 3
                lbl = New Label
                lbl.CssClass = "normal7"
                lbl.Text = "When done close stage  "
                tblCell.Controls.Add(lbl)
                lbl = New Label
                If dr.Item("bitClose") = True Then
                    lbl.Text = "a"
                    lbl.CssClass = "cell1"
                Else
                    lbl.Text = "r"
                    lbl.CssClass = "cell"
                End If
                tblCell.Controls.Add(lbl)

                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)
                If ViewState("CheckColor") = 1 Then
                    ViewState("CheckColor") = 0
                Else : ViewState("CheckColor") = 1
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnTCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTCancel.Click
            Try
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DomainID = Session("DomainID")
                    .CompanyID = 0
                    .CompFilter = Trim(strName) & "%"
                    ddlCombo.DataSource = fillCombo.ListCustomer().Tables(0).DefaultView
                    ddlCombo.DataTextField = "vcCompanyname"
                    ddlCombo.DataValueField = "numDivisionID"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub PageRedirect()
            Try
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "prospects" Then
                    objCommon.ProID = lngProID
                    objCommon.charModule = "P"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../prospects/frmProspects.aspx?frm=prospects&DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "accounts" Then
                    objCommon.ProID = lngProID
                    objCommon.charModule = "P"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../account/frmAccounts.aspx?frm=accounts&klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "tickler" Then
                    Response.Redirect("../common/frmticklerdisplay.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "contactlist" Then
                    Response.Redirect("../contact/frmContacList.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "search" Then
                    Response.Redirect("../common/searchdisplay.aspx?frm=advancedsearch&srchback=true&typ=" & GetQueryStringVal(Request.QueryString("enc"), "typ") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "opportunitylist" Then
                    Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ProjectStatus" Then
                    Response.Redirect("../reports/frmProjectStatus.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "OPPDetails" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?OpID=" & GetQueryStringVal(Request.QueryString("enc"), "OpID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
               ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AdvSearch" Then
                    Response.Redirect("../Admin/frmAdvProjectsRes.aspx")
                Else : Response.Redirect("../projects/frmProjectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function FillContact(ByVal ddl As DropDownList, ByVal DivisionID As Long)
            Try
                Dim objOpport As New COpportunities
                With objOpport
                    .DivisionID = DivisionID
                End With
                With ddl
                    .DataSource = objOpport.ListContact().Tables(0).DefaultView()
                    .DataTextField = "Name"
                    .DataValueField = "numcontactId"
                    .DataBind()
                    .ClearSelection()
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                If objProjects Is Nothing Then objProjects = New Project
                objProjects.ProjectID = lngProID
                objProjects.DomainID = Session("DomainID")
                If objProjects.DeleteProjects = False Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else : Response.Redirect("../projects/frmProjectList.aspx")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            Try
                Response.Redirect("../Projects/frmProjects.aspx?frm=projectdtl&ProId=" & lngProID & "&SI=" & uwOppTab.SelectedTabIndex & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm & "&frm2=" & frm1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                'If m_aryRightsForCusFlds(RIGHTSTYPE.VIEW) = 0 Then
                '    Exit Sub
                'End If
                Dim strDate As String
                Dim bizCalendar As UserControl
                Dim _myUC_DueDate As PropertyInfo
                Dim PreviousRowID As Integer = 0
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim i, k, j As Integer
                Dim dtTable As DataTable
                Dim count As Integer = uwOppTab.Tabs.Count
                ' Tabstrip4.Items.Clear()

                Dim objPageLayout As New CPageLayout

                objPageLayout.locId = 11
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.RelId = 0
                objPageLayout.RecordId = lngProID
                dtTable = objPageLayout.GetCustFlds

                If uwOppTab.Tabs.Count > 3 Then
                    Dim iItemcount As Integer
                    iItemcount = uwOppTab.Tabs.Count
                    While uwOppTab.Tabs.Count > 3
                        uwOppTab.Tabs.RemoveAt(iItemcount - 1)
                        iItemcount = iItemcount - 1
                    End While
                End If

                If dtTable.Rows.Count > 0 Then
                    'CustomField Section
                    Dim Tab As Tab
                    ' Dim pageView As PageView
                    Dim aspTable As HtmlTable
                    Dim Table As Table
                    Dim tblcell As TableCell
                    Dim tblRow As TableRow
                    Dim up As UpdatePanel
                    Dim apt As AsyncPostBackTrigger
                    k = 0
                    ViewState("TabId") = dtTable.Rows(0).Item("TabId")
                    ViewState("Check") = 0
                    ViewState("FirstTabCreated") = 0
                    'Tabstrip4.Items.Clear()
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") <> 0 Then
                            If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                                If ViewState("Check") <> 0 Then
                                    aspTable.Rows.Add(objRow)
                                    tblcell.Controls.Add(aspTable)
                                    tblRow.Cells.Add(tblcell)
                                    Table.Rows.Add(tblRow)

                                    up = New UpdatePanel
                                    apt = New AsyncPostBackTrigger
                                    up.ChildrenAsTriggers = True
                                    up.UpdateMode = UpdatePanelUpdateMode.Conditional
                                    apt.ControlID = "btnEdit"
                                    up.ContentTemplateContainer.Controls.Add(Table)
                                    up.Triggers.Add(apt)
                                    Tab.ContentPane.Controls.Add(up)
                                    'pageView.Controls.Add(up)
                                    ' mpages.Controls.Add(pageView)
                                    ' mpages.Controls.Add(pageView)
                                End If
                                k = 0
                                ViewState("Check") = 1
                                'If Not IsPostBack Then
                                ViewState("FirstTabCreated") = 1
                                ViewState("TabId") = dtTable.Rows(i).Item("TabId")
                                Tab = New Tab
                                Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
                                uwOppTab.Tabs.Add(Tab)
                                'End If
                                'pageView = New PageView
                                aspTable = New HtmlTable
                                Table = New Table
                                Table.Width = Unit.Percentage(100)
                                Table.BorderColor = System.Drawing.Color.FromName("black")
                                Table.GridLines = GridLines.None
                                Table.BorderWidth = Unit.Pixel(1)
                                Table.Height = Unit.Pixel(300)
                                Table.CssClass = "aspTable"
                                tblcell = New TableCell
                                tblRow = New TableRow
                                tblcell.VerticalAlign = VerticalAlign.Top
                                aspTable.Width = "100%"
                                objRow = New HtmlTableRow
                                objCell = New HtmlTableCell
                                objCell.InnerHtml = "<br>"
                                objRow.Cells.Add(objCell)
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            If k = 2 Then
                                k = 0
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If
                            objCell = New HtmlTableCell
                            objCell.Align = "right"
                            objCell.Width = 100

                            objCell.Attributes.Add("class", "normal7")
                            If dtTable.Rows(i).Item("fld_type") <> "Frame" Then
                                If dtTable.Rows(i).Item("fld_type") <> "Link" Then
                                    objCell.InnerText = dtTable.Rows(i).Item("fld_label") & " :"
                                End If
                                objRow.Cells.Add(objCell)
                            End If
                            objCell = New HtmlTableCell
                            objCell.Attributes.Add("class", "normal1")
                            objCell.Align = "left"
                            If dtTable.Rows(i).Item("fld_type") = "Link" Then

                                Dim h As New HyperLink
                                h.CssClass = "hyperlink"
                                Dim URL As String = IIf(IsDBNull(dtTable.Rows(i).Item("vcURL")), "", dtTable.Rows(i).Item("vcURL"))
                                URL = URL.Replace("RecordID", lngProID)
                                h.Text = IIf(IsDBNull(dtTable.Rows(i).Item("fld_label")), "", dtTable.Rows(i).Item("fld_label"))
                                h.Attributes.Add("onclick", "fn_GoToURL('" & "http://" & URL & "')")
                                objCell.Controls.Add(h)
                                'CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngCntID, dtTable.Rows(i).Item("fld_label"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = IIf(dtTable.Rows(i).Item("Value") = "1", "a", "r")
                                    l.CssClass = IIf(dtTable.Rows(i).Item("Value") = "1", "cell1", "cell")
                                    objCell.Controls.Add(l)
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = "r"
                                    l.CssClass = "cell"
                                    objCell.Controls.Add(l)
                                End If
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
                                objCell = New HtmlTableCell
                                Dim strFrame As String
                                Dim URL As String
                                URL = dtTable.Rows(i).Item("vcURL")
                                URL = URL.Replace("RecordID", lngProID)
                                strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                objCell.Controls.Add(New LiteralControl(strFrame))
                                objRow.Cells.Add(objCell)
                            Else
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = IIf(dtTable.Rows(i).Item("Value") = "0" Or dtTable.Rows(i).Item("Value") = Nothing, "-", dtTable.Rows(i).Item("Value"))
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = "-"
                                End If
                            End If
                            objRow.Cells.Add(objCell)
                            k = k + 1
                        End If

                    Next
                    If ViewState("Check") = 1 Then
                        objRow.Align = "left"
                        aspTable.Rows.Add(objRow)
                        tblcell.Controls.Add(aspTable)
                        tblRow.Cells.Add(tblcell)
                        Table.Rows.Add(tblRow)
                        up = New UpdatePanel
                        apt = New AsyncPostBackTrigger
                        up.ChildrenAsTriggers = True
                        up.UpdateMode = UpdatePanelUpdateMode.Conditional
                        apt.ControlID = "btnEdit"
                        up.ContentTemplateContainer.Controls.Add(Table)
                        up.Triggers.Add(apt)
                        Tab.ContentPane.Controls.Add(up)
                        'pageView.Controls.Add(up)
                        'mpages.Controls.Add(pageView)
                        ' mpages.Controls.Add(pageView)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
