Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.UltraWebTab
Imports System.Reflection

Namespace BACRM.UserInterface.Contacts
    Public Class frmContacts1 : Inherits BACRMPage

        Dim objContacts As CContacts
        Dim objCommon As New CCommon
        Dim objPageLayout As CPageLayout
        Dim objprospects As CProspects
        Dim objSurvey As SurveyAdministration
        Dim strColumn As String
        Dim lngCntID As Long
        Dim m_aryRightsForCusFlds(), m_aryRightsForActItem(), m_aryRightsForEmail(), m_aryRightsForOpp(), m_aryRightsForPage(), m_aryRightsForAOI() As Integer
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Response.Redirect("../contact/frmContacts.aspx?" & GetQueryStringVal(Request.QueryString("enc"), "", True))
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                    SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                    SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
                Else : SI1 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
                Else : SI2 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                    frm = ""
                    frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
                Else : frm = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                    frm1 = ""
                    frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
                Else : frm1 = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    frm2 = ""
                    frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
                Else : frm2 = ""
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "SurveyRespondents" Or GetQueryStringVal(Request.QueryString("enc"), "frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    'Added by Debasish Nag on 6th Jan 2006
                    Dim objForm As Object                                   'The generic object
                    objForm = Page.FindControl("webmenu1")                  'Get a holder to the web control
                    objForm.visible = False                                 'Hide the web control
                    btnCancel.Attributes.Add("onclick", "javascript: window.close();return false;") 'Write code to close the window
                End If
                lngCntID = CInt(GetQueryStringVal(Request.QueryString("enc"), "CntId"))
                'LoadTableInformation()
                'DisplayDynamicFlds()
                If Not IsPostBack Then
                    If uwOppTab.Tabs.Count >= SI Then uwOppTab.SelectedTabIndex = SI
                    Session("Asc") = 1
                    If objContacts Is Nothing Then objContacts = New CContacts
                    objContacts.RecID = lngCntID
                    objContacts.Type = "U"
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.AddVisiteddetails()
                    Session("Help") = "Contacts"

                    Calendar1.SelectedDate = Session("StartDate")
                    Calendar2.SelectedDate = Session("EndDate")
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 3)
                    ''''call function for sub-tab management  - added on 29jul09 by Mohan
                    'objCommon.ManageSubTabs(uwOppTab, Session("DomainID"), 11)
                    '''''
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    Else
                        If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnEdit.Visible = False
                        If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False
                    End If
                    Dim m_aryRightsForExptTooutLook() As Integer
                    m_aryRightsForExptTooutLook = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 10)
                    If m_aryRightsForExptTooutLook(RIGHTSTYPE.VIEW) = 0 Then btnExport.Visible = False
                    m_aryRightsForActItem = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 5)
                    If m_aryRightsForActItem(RIGHTSTYPE.VIEW) = 0 Then btnActionItem.Visible = False
                    Dim dtTab As DataTable
                    dtTab = Session("DefaultTab")
                    If dtTab.Rows.Count > 0 Then
                        uwOppTab.Tabs(0).Text = IIf(IsDBNull(dtTab.Rows(0).Item("vcContact")), "Contact Details", dtTab.Rows(0).Item("vcContact").ToString & " Details")
                    Else : uwOppTab.Tabs(0).Text = "Contact Details"
                    End If
                    LoadTabDetails()
                    btnLayout.Attributes.Add("onclick", "return ShowLayout('c','" & lngCntID & "','" & type.Text & "');")
                    btnMerge.Attributes.Add("onclick", "return OpenMergeCopyWindow('" & lngCntID & "','" & GetQueryStringVal(Request.QueryString("enc"), "frm") & "');")
                    If lngCntID = Session("AdminID") Then
                        btnActDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                    btnExport.Attributes.Add("onclick", "return Export()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTableInformation()
            Try
                Dim dtTableInfo As DataTable
                Dim dtTablecust As DataTable
                Dim ds As New DataSet
                If objPageLayout Is Nothing Then objPageLayout = New CPageLayout

                Dim check As String
                Dim fields() As String
                Dim idcolumn As String = ""
                Dim count1 As Integer
                Dim x As Integer
                m_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 9)
                Dim dtContactInfo As DataTable
                If objContacts Is Nothing Then objContacts = New CContacts
                objPageLayout.ContactID = lngCntID
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                ' dtContactInfo = objPageLayout.GetContactInfoEditIP      ' getting the details

                If dtContactInfo.Rows(0).Item("numUserID") > 0 Then
                    Dim m_aryRightsForTimeExp() As Integer
                    m_aryRightsForTimeExp = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 11)
                    If m_aryRightsForTimeExp(RIGHTSTYPE.VIEW) <> 0 Then
                        btnTimeExp.Visible = True
                        btnTimeExp.Attributes.Add("onclick", "return OpenTmeAndExp(" & lngCntID & ",'" & GetQueryStringVal(Request.QueryString("enc"), "frm") & "')")
                    End If
                End If

                hplCustomer.Text = dtContactInfo.Rows(0).Item("vcCompanyName")
                If dtContactInfo.Rows(0).Item("tintCRMType") = 0 Then
                    hplCustomer.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetail&DivID=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")

                ElseIf dtContactInfo.Rows(0).Item("tintCRMType") = 1 Then
                hplCustomer.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetail&DivID=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")

                ElseIf dtContactInfo.Rows(0).Item("tintCRMType") = 2 Then
                hplCustomer.NavigateUrl = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetail&klds+7kldf=fjk-las&DivId=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")

                End If
                lblRecordOwner.Text = dtContactInfo.Rows(0).Item("RecordOwner")
                lblCreatedBy.Text = dtContactInfo.Rows(0).Item("CreatedBy")
                lblModifiedBy.Text = dtContactInfo.Rows(0).Item("ModifiedBy")

                type.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numContactType")), 0, dtContactInfo.Rows(0).Item("numContactType"))
                txtEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcEmail")), "", dtContactInfo.Rows(0).Item("vcEmail"))
                objPageLayout.CoType = "C"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngCntID
                'objPageLayout.ContactID = lngCntID
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 4
                objPageLayout.numRelCntType = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numContactType")), 0, dtContactInfo.Rows(0).Item("numContactType"))
                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)

                'If ds.Tables.Count = 2 Then
                '    dtTablecust = ds.Tables(1)
                '    dtTableInfo.Merge(dtTablecust)
                'End If
                'Dim dv As DataView
                'dv = New DataView(dtTableInfo)

                Dim numrows As Integer = dtTableInfo.Compute("Max(tintrow)", String.Empty)
                Dim numcells As Integer = dtTableInfo.Compute("Max(intcoulmn)", String.Empty)
                Dim i As Integer = 0
                Dim nr As Integer
                Dim noRowsToLoop As Integer
                noRowsToLoop = (dtTableInfo.Rows.Count - (numrows * numcells)) / numcells
                noRowsToLoop = noRowsToLoop + numrows + 1
                For nr = 0 To noRowsToLoop
                    Dim r As New TableRow()
                    Dim nc As Integer
                    Dim ro As Integer = nr
                    For nc = 1 To numcells
                        If dtTableInfo.Rows.Count <> i Then
                            If dtTableInfo.Rows(i).Item("tintrow") = nr + 1 And dtTableInfo.Rows(i).Item("intcoulmn") = nc Then
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                Dim fieldId As Integer
                                Dim bitDynFld As String
                                fieldId = CInt(dtTableInfo.Rows(i).Item("numFieldID").ToString)
                                bitDynFld = dtTableInfo.Rows(i).Item("bitCustomField")
                                column1.CssClass = "normal7"
                                If (bitDynFld <> "1") Then
                                    If (fieldId = "25") Then
                                        Dim h As New HyperLink()
                                        h.ID = "hplDocuments"
                                        'h.CssClass = "hyperlink"
                                        h.NavigateUrl = "#"
                                        h.Text = "Documents"
                                        h.Attributes.Add("onclick", "return OpenDocuments(" & lngCntID & ");")
                                        Dim l As New Label
                                        l.CssClass = "normal7"
                                        l.Text = "(" & dtContactInfo.Rows(0).Item("DocumentCount") & ")" & "&nbsp;:"
                                        column1.Controls.Add(h)
                                        column1.Controls.Add(l)
                                    ElseIf (fieldId = "9") Then
                                        Dim h As New HyperLink()
                                        ' h.CssClass = "hyperlink"
                                        h.NavigateUrl = "#"
                                        h.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString
                                        h.Attributes.Add("onclick", "return OpenECamp(" & lngCntID & ");")
                                        Dim l As New Label
                                        l.CssClass = "normal7"
                                        l.Text = "&nbsp;:"
                                        column1.Controls.Add(h)
                                        column1.Controls.Add(l)
                                    ElseIf (fieldId = "18") Then
                                        Dim h As New HyperLink()
                                        'h.CssClass = "hyperlink"
                                        h.NavigateUrl = "#"
                                        h.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString
                                        h.Attributes.Add("onclick", "return OpenECampHstr(" & lngCntID & ");")
                                        Dim l As New Label
                                        l.CssClass = "normal7"
                                        l.Text = "&nbsp;:"
                                        column1.Controls.Add(h)
                                        column1.Controls.Add(l)
                                    ElseIf (fieldId = "7") Then
                                        Dim h As New HyperLink
                                        h.NavigateUrl = "#"
                                        h.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                        h.Attributes.Add("onclick", "OpenAdd(" & lngCntID & ")")
                                        column1.Controls.Add(h)
                                    Else : column1.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                    End If

                                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName").ToString) And dtTableInfo.Rows(i).Item("vcDBColumnName").ToString() <> "" Then
                                        Dim temp As String
                                        temp = dtTableInfo.Rows(i).Item("vcDBColumnName").ToString
                                        fields = temp.Split(",")
                                        Dim j As Integer
                                        j = 0

                                        While (j < fields.Length)
                                            If (fieldId = "9" Or fieldId = "18" Or fieldId = "25" Or fieldId = "5" Or fieldId = "23") Then
                                                Dim listvalue As String
                                                Dim value As String = ""
                                                listvalue = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "-", dtContactInfo.Rows(0).Item(fields(j)))
                                                Select Case fieldId   ' Must be a primitive data type
                                                    Case 23
                                                        If listvalue = "M" Then
                                                            value = "Male"
                                                        ElseIf listvalue = "F" Then
                                                            value = "Female"
                                                        Else : value = "-"
                                                        End If
                                                    Case 9, 18, 25
                                                        value = ""
                                                    Case 5
                                                        type.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numContactType")), "0", dtContactInfo.Rows(0).Item("numContactType"))
                                                        value = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "-", dtContactInfo.Rows(0).Item(fields(j)))

                                                    Case Else

                                                End Select
                                                column2.Text = value
                                            ElseIf (fieldId = "2" Or fieldId = "3" Or fieldId = "26") Then
                                                Dim email As String = ""
                                                Dim h As New HyperLink
                                                h.CssClass = "hyperlink"
                                                ' h.NavigateUrl = "#"
                                                h.ID = "email" & fieldId
                                                email = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "-", dtContactInfo.Rows(0).Item(fields(j).ToString))
                                                h.Text = email
                                                h.Attributes.Add("onclick", "return fn_Mail('" & email & "','" & Session("CompWindow") & "','" & lngCntID & "');")
                                                column2.Controls.Add(h)
                                            ElseIf (fieldId = "24") Then
                                                Dim l As New Label
                                                l.CssClass = "cell"
                                                If Not (IsDBNull(dtContactInfo.Rows(0).Item("bitOptOut"))) Then
                                                    l.Text = IIf(dtContactInfo.Rows(0).Item("bitOptOut"), "a", "r")
                                                Else : l.Text = "r"
                                                End If
                                                column2.Controls.Add(l)
                                            ElseIf (fieldId = "7") Then
                                                Dim l As New Label
                                                l.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "", dtContactInfo.Rows(0).Item(fields(j)))
                                                l.ID = "lblAddress"
                                                column2.Controls.Add(l)
                                            Else : column2.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "-", dtContactInfo.Rows(0).Item(fields(j)))
                                            End If
                                            j += 1
                                        End While
                                    Else
                                        column1.Text = ""
                                    End If ' end of table cell2
                                Else
                                    If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                        column1.Text = "Custom Web Link :"
                                        Dim h As New HyperLink()
                                        h.CssClass = "hyperlink"
                                        Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                        url = url.Replace("RecordID", lngCntID)
                                        h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                        column2.Controls.Add(h)
                                    ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                        Dim strDate As String
                                        strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                        If strDate = "0" Then strDate = ""
                                        If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                    Else
                                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                        If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                Dim l As New Label
                                                l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                                l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                column2.Controls.Add(l)
                                            End If
                                        Else
                                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                            End If
                                        End If
                                    End If
                                End If
                                column2.CssClass = "normal1"
                                column1.HorizontalAlign = HorizontalAlign.Right
                                column2.HorizontalAlign = HorizontalAlign.Left
                                column1.Width = 250
                                column2.Width = 300
                                column2.ColumnSpan = 1
                                column1.ColumnSpan = 1
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                                i += 1
                            ElseIf dtTableInfo.Rows(i).Item("tintrow") = 0 And dtTableInfo.Rows(i).Item("intcoulmn") = 0 Then
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                column1.CssClass = "normal7"
                                If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                    column1.Text = "Custom Web Link :"
                                    Dim h As New HyperLink()
                                    h.CssClass = "hyperlink"
                                    Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                    url = url.Replace("RecordID", lngCntID)
                                    h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                    h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                    column2.Controls.Add(h)
                                ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                    column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                    Dim strDate As String
                                    strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                    If strDate = "0" Then strDate = ""
                                    If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                Else
                                    column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                    If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                        If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                            Dim l As New Label
                                            l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                            l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                            column2.Controls.Add(l)
                                        End If
                                    Else
                                        If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                            column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                        End If
                                    End If
                                End If
                                column2.CssClass = "normal1"
                                column1.HorizontalAlign = HorizontalAlign.Right
                                column2.HorizontalAlign = HorizontalAlign.Left
                                column1.Width = 250
                                column2.Width = 300
                                column2.ColumnSpan = 1
                                column1.ColumnSpan = 1
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                                i += 1
                            Else
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                column1.Text = ""
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                            End If
                        End If
                    Next nc
                    tabledetail.Rows.Add(r)
                Next nr

                If (dtContactInfo.Rows.Count > 0) Then
                    Dim column1 As New TableCell
                    Dim column2 As New TableCell
                    Dim r As New TableRow
                    column1.CssClass = "normal7"
                    column2.CssClass = "normal1"
                    column1.HorizontalAlign = HorizontalAlign.Right
                    column2.HorizontalAlign = HorizontalAlign.Justify
                    column2.ColumnSpan = 5
                    Dim l As New Label
                    l.CssClass = "normal7"
                    l.Text = "Comments" & "&nbsp;:"
                    column1.Controls.Add(l)
                    column2.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("txtNotes")), "-", dtContactInfo.Rows(0).Item("txtNotes"))
                    column1.Width = 150
                    r.Cells.Add(column1)
                    r.Cells.Add(column2)
                    tableComment.Rows.Add(r)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub sb_ShowAOIs()
            Try
                m_aryRightsForAOI = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 4)
                If m_aryRightsForAOI(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                Dim dtAOI As DataTable
                Dim objRow As TableRow
                Dim objCell As TableCell
                Dim objCell1 As TableCell
                Dim objCell2 As TableCell
                Dim chk As CheckBox
                Dim i As Integer
                If objContacts Is Nothing Then objContacts = New CContacts
                objContacts.ContactID = lngCntID
                objContacts.DomainID = Session("DomainID")
                dtAOI = objContacts.GetAOIDetails
                Session("AOIForContact") = dtAOI
                Dim cellCount As Integer

                For i = 0 To dtAOI.Rows.Count - 1
                    objRow = New TableRow
                    objCell1 = New TableCell
                    objCell2 = New TableCell
                    objCell1.Text = IIf(IsDBNull(dtAOI.Rows(i).Item("vcAOIName")), "-", dtAOI.Rows(i).Item("vcAOIName"))
                    If Not IsDBNull(dtAOI.Rows(i).Item("Status")) Then
                        Dim l As New Label
                        l.CssClass = "cell"
                        l.Text = IIf(dtAOI.Rows(i).Item("Status") = 0, "", "a")
                        l.CssClass = IIf(dtAOI.Rows(i).Item("Status") = 0, "cell", "cell1")
                        objCell2.Controls.Add(l)
                    End If
                    objCell1.CssClass = "normal7"
                    objCell1.Width = 400
                    objRow.Cells.Add(objCell1)
                    objRow.Cells.Add(objCell2)
                    tblAOI.Rows.Add(objRow)
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub sc_GetOppDetails()
            Try
                m_aryRightsForOpp = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 6)
                If m_aryRightsForOpp(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If objprospects Is Nothing Then objprospects = New CProspects

                objprospects.ContactId = lngCntID
                objprospects.OppType = ddlOppType.SelectedItem.Value
                If radOppOpen.Checked = True Then
                    objprospects.ByteMode = 0
                    objprospects.DomainID = Session("DomainID")
                    dgOpenOpportunty.DataSource = objprospects.GetOppDetailsForOrg
                    dgOpenOpportunty.DataBind()
                    dgClosedOpp.Visible = False
                    dgOpenOpportunty.Visible = True
                ElseIf radOppClose.Checked = True Then
                    objprospects.ByteMode = 1
                    objprospects.OppStatus = ddlOppStatus.SelectedItem.Value
                    objprospects.DomainID = Session("DomainID")
                    dgClosedOpp.DataSource = objprospects.GetOppDetailsForOrg
                    dgClosedOpp.DataBind()
                    dgClosedOpp.Visible = True
                    dgOpenOpportunty.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                If m_aryRightsForCusFlds(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                Dim strDate As String
                Dim bizCalendar As UserControl
                Dim _myUC_DueDate As PropertyInfo
                Dim PreviousRowID As Integer = 0
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim i, k, j As Integer
                Dim dtTable As DataTable
                Dim count As Integer = uwOppTab.Tabs.Count
                ' Tabstrip4.Items.Clear()
                If objPageLayout Is Nothing Then objPageLayout = New CPageLayout
                objPageLayout.locId = 4
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.RelId = CInt(type.Text)
                objPageLayout.RecordId = lngCntID
                dtTable = objPageLayout.GetCustFlds

                If uwOppTab.Tabs.Count > 5 Then
                    Dim iItemcount As Integer
                    iItemcount = uwOppTab.Tabs.Count
                    While uwOppTab.Tabs.Count > 5
                        uwOppTab.Tabs.RemoveAt(iItemcount - 1)
                        iItemcount = iItemcount - 1
                    End While
                End If

                If dtTable.Rows.Count > 0 Then
                    'CustomField Section
                    Dim Tab As Tab
                    ' Dim pageView As PageView
                    Dim aspTable As HtmlTable
                    Dim Table As Table
                    Dim tblcell As TableCell
                    Dim tblRow As TableRow
                    Dim up As UpdatePanel
                    Dim apt As AsyncPostBackTrigger
                    k = 0
                    ViewState("TabId") = dtTable.Rows(0).Item("TabId")
                    ViewState("Check") = 0
                    ViewState("FirstTabCreated") = 0
                    'Tabstrip4.Items.Clear()
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") <> 0 Then
                            If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                                If ViewState("Check") <> 0 Then

                                    aspTable.Rows.Add(objRow)
                                    tblcell.Controls.Add(aspTable)
                                    tblRow.Cells.Add(tblcell)
                                    Table.Rows.Add(tblRow)

                                    up = New UpdatePanel
                                    apt = New AsyncPostBackTrigger
                                    up.ChildrenAsTriggers = True
                                    up.UpdateMode = UpdatePanelUpdateMode.Conditional
                                    apt.ControlID = "btnEdit"
                                    up.ContentTemplateContainer.Controls.Add(Table)
                                    up.Triggers.Add(apt)
                                    Tab.ContentPane.Controls.Add(up)
                                    'pageView.Controls.Add(up)
                                    ' mpages.Controls.Add(pageView)
                                    ' mpages.Controls.Add(pageView)
                                End If
                                k = 0
                                ViewState("Check") = 1
                                'If Not IsPostBack Then
                                ViewState("FirstTabCreated") = 1
                                ViewState("TabId") = dtTable.Rows(i).Item("TabId")
                                Tab = New Tab
                                Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
                                uwOppTab.Tabs.Add(Tab)
                                'End If
                                'pageView = New PageView
                                aspTable = New HtmlTable
                                Table = New Table
                                Table.Width = Unit.Percentage(100)
                                Table.BorderColor = System.Drawing.Color.FromName("black")
                                Table.GridLines = GridLines.None
                                Table.BorderWidth = Unit.Pixel(1)
                                Table.Height = Unit.Pixel(300)
                                Table.CssClass = "aspTable"
                                tblcell = New TableCell
                                tblRow = New TableRow
                                tblcell.VerticalAlign = VerticalAlign.Top
                                aspTable.Width = "100%"
                                objRow = New HtmlTableRow
                                objCell = New HtmlTableCell
                                objCell.InnerHtml = "<br>"
                                objRow.Cells.Add(objCell)
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            If k = 3 Then
                                k = 0
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If
                            objCell = New HtmlTableCell
                            objCell.Align = "right"
                            objCell.Width = 100

                            objCell.Attributes.Add("class", "normal7")
                            If dtTable.Rows(i).Item("fld_type") <> "Frame" Then
                                If dtTable.Rows(i).Item("fld_type") <> "Link" Then objCell.InnerText = dtTable.Rows(i).Item("fld_label") & " :"
                                objRow.Cells.Add(objCell)
                            End If
                            objCell = New HtmlTableCell
                            objCell.Attributes.Add("class", "normal1")
                            objCell.Align = "left"
                            If dtTable.Rows(i).Item("fld_type") = "Link" Then
                                Dim h As New HyperLink
                                h.CssClass = "hyperlink"
                                Dim URL As String = IIf(IsDBNull(dtTable.Rows(i).Item("vcURL")), "", dtTable.Rows(i).Item("vcURL"))
                                URL = URL.Replace("RecordID", lngCntID)
                                h.Text = IIf(IsDBNull(dtTable.Rows(i).Item("fld_label")), "", dtTable.Rows(i).Item("fld_label"))
                                h.Attributes.Add("onclick", "fn_GoToURL('" & "http://" & URL & "')")
                                objCell.Controls.Add(h)
                                'CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngCntID, dtTable.Rows(i).Item("fld_label"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = IIf(dtTable.Rows(i).Item("Value") = "1", "a", "r")
                                    l.CssClass = IIf(dtTable.Rows(i).Item("Value") = "1", "cell1", "cell")
                                    objCell.Controls.Add(l)
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = "r"
                                    l.CssClass = "cell"
                                    objCell.Controls.Add(l)
                                End If
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
                                objCell = New HtmlTableCell
                                Dim strFrame As String
                                Dim URL As String
                                URL = dtTable.Rows(i).Item("vcURL")
                                URL = URL.Replace("RecordID", lngCntID)
                                strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                objCell.Controls.Add(New LiteralControl(strFrame))
                                objRow.Cells.Add(objCell)
                            Else
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = IIf(dtTable.Rows(i).Item("Value") = "0" Or dtTable.Rows(i).Item("Value") = Nothing, "-", dtTable.Rows(i).Item("Value"))
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = "-"
                                End If
                            End If
                            objRow.Cells.Add(objCell)
                            k = k + 1
                        End If
                    Next
                    If ViewState("Check") = 1 Then
                        objRow.Align = "left"
                        aspTable.Rows.Add(objRow)
                        tblcell.Controls.Add(aspTable)
                        tblRow.Cells.Add(tblcell)
                        Table.Rows.Add(tblRow)
                        up = New UpdatePanel
                        apt = New AsyncPostBackTrigger
                        up.ChildrenAsTriggers = True
                        up.UpdateMode = UpdatePanelUpdateMode.Conditional
                        apt.ControlID = "btnEdit"
                        up.ContentTemplateContainer.Controls.Add(Table)
                        up.Triggers.Add(apt)
                        Tab.ContentPane.Controls.Add(up)
                        'pageView.Controls.Add(up)
                        'mpages.Controls.Add(pageView)
                        ' mpages.Controls.Add(pageView)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "SurveyRespondents" Or GetQueryStringVal(Request.QueryString("enc"), "frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    litMessage.Text = "<script language='javascript'>window.close();</script>" 'Write code to close the window
                Else : PageRedirect()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If GetQueryStringVal(Request.QueryString("enc"), "frm1") = "ActionItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommID=" & GetQueryStringVal(Request.QueryString("enc"), "CommID") & "&frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm"))
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "ActItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommId=" & GetQueryStringVal(Request.QueryString("enc"), "CommID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "prospects" Then
                    objCommon.ContactID = lngCntID
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../prospects/frmProspects.aspx?DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "accounts" Then
                    objCommon.ContactID = lngCntID
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../account/frmAccounts.aspx?klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
             ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "tickler" Then
                    Response.Redirect("../common/frmticklerdisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "contactlist" Then
                    Response.Redirect("../contact/frmContactList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&ContactType=" & GetQueryStringVal(Request.QueryString("enc"), "ContactType"))
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "prospectlist" Then
                    Response.Redirect("../prospects/frmProspectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&profileId=" & GetQueryStringVal(Request.QueryString("enc"), "profileId"))
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "accountlist" Then
                    Response.Redirect("../account/frmAccountList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&profileId=" & GetQueryStringVal(Request.QueryString("enc"), "profileId"))
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "LeadsList" Then
                    Response.Redirect("../Leads/frmLeadList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "Caselist" Then
                    Response.Redirect("../cases/frmCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ProjectList" Then
                    Response.Redirect("../projects/frmProjectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "tickler" Then
                    Response.Redirect("../common/frmTicklerDisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "search" Then
                    Response.Redirect("../common/searchdisplay.aspx?frm=advancedsearch&srchback=true&typ=" & GetQueryStringVal(Request.QueryString("enc"), "typ") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "opportunitylist" Then
                    Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ContactRole" Then
                    Response.Redirect("../reports/frmContactRole.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "DealHistory" Then
                    Response.Redirect("../reports/frmRepDealHistory.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CompanyList" Then
                    Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & GetQueryStringVal(Request.QueryString("enc"), "RelID") & "&profileId=" & GetQueryStringVal(Request.QueryString("enc"), "profileId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
               ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "LeadDetails" Then
                    objCommon.ContactID = lngCntID
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../Leads/frmLeads.aspx?DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AdvSearch" Then
                    Response.Redirect("../admin/frmAdvancedSearchRes.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsReceivable" Then
                    Response.Redirect("../Accounting/frmAccountsReceivable.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsPayable" Then
                    Response.Redirect("../Accounting/frmAccountsPayable.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AdvSearchSur" Then
                    Response.Redirect("../Admin/FrmAdvSurveyRes.aspx")
                Else : Response.Redirect("../contact/frmContactList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&ContactType=" & GetQueryStringVal(Request.QueryString("enc"), "ContactType"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgClosedOpp_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgClosedOpp.ItemCommand
            Try
                Dim lngOppID As Long
                lngOppID = e.Item.Cells(0).Text
                If e.CommandName = "Name" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?frm=contactdetails&opID=" & lngOppID & "&SI=" & SI1 & "&SI1=" & uwOppTab.SelectedTabIndex & "&frm=" & frm1 & "&frm1=" & frm2)

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgOpenOpportunty_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOpenOpportunty.ItemCommand
            Try
                Dim lngOppID As Long
                lngOppID = e.Item.Cells(0).Text
                If e.CommandName = "Name" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?frm=contactdetails&opID=" & lngOppID & "&SI=" & SI1 & "&SI1=" & uwOppTab.SelectedTabIndex & "&frm=" & frm1 & "&frm1=" & frm2)

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlOppStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOppStatus.SelectedIndexChanged
            Try
                sc_GetOppDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlOppType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOppType.SelectedIndexChanged
            Try
                sc_GetOppDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the surveys history from the database for the selected contact
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub DisplaySurveyHistoryLists()
            Try
                If objSurvey Is Nothing Then objSurvey = New SurveyAdministration 'Declare and create a object of SurveyAdministration
                objSurvey.DomainId = Session("DomainID")                    'Set the Doamin Id
                objSurvey.ContactId = lngCntID                              'Set the contact ID
                Dim dtSurveyHistorylist As DataTable                        'Declare a datatable
                dtSurveyHistorylist = GetRowsInPageRange(objSurvey.getSurveyHistoryList())  'call function to get the list of available surveys

                dgSurvey.DataSource = dtSurveyHistorylist                   'set the datasource
                dgSurvey.DataBind()                                 'databind the datgrid
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete the Survey History
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Protected Overridable Sub btnSurveyHistoryDeleteAction_Command(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Dim btnSurveyHistoryDelete As Button = CType(sender, Button)                                'typecast to button
                Dim dgContainer As DataGridItem = CType(btnSurveyHistoryDelete.NamingContainer, DataGridItem) 'get the containeer object
                Dim gridIndex As Integer = dgContainer.ItemIndex                                            'Get the row idnex of datagrid
                Dim numSurId As Integer = CInt(dgSurvey.Items(gridIndex).Cells(0).Text)                     'Get the Survey Id from the column
                Dim numRespondentID As Integer = CInt(dgSurvey.Items(gridIndex).Cells(1).Text)              'Get the Respondent Id from the column

                If objSurvey Is Nothing Then objSurvey = New SurveyAdministration 'Declare and create a object of SurveyAdministration
                objSurvey.SurveyId = numSurId                                                               'Get the Survey Id
                objSurvey.SurveyExecution.SurveyExecution().RespondentID = numRespondentID                  'Get the Respondent ID
                objSurvey.DeleteSurveyHistoryForContact()                                                  'Call to delete the Survey History
                DisplaySurveyHistoryLists()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
            'Call to display the list of surveys for the contact
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to pick up a selected range of rows
        ''' </summary>
        ''' <remarks> Returns the table with limited rows
        ''' </remarks>
        ''' <param name="dtSurveyRespondentlist">Table which contains the search result to be displayed in the DataGrid</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function GetRowsInPageRange(ByVal dtContactSurveylist As DataTable) As DataTable
            Try
                Dim numRowIndex As Integer                                                       'Declare an index variable
                Dim dtResultTableCopy As New DataTable                                           'Declare a datatable object and instantiate
                dtResultTableCopy = dtContactSurveylist.Clone()                                  'Clone the results table

                Dim iRowIndex As Integer = (txtCurrentPageSurveyHistory.Text * Session("PagingRows")) - Session("PagingRows") 'Declare a row index and set it to the first row in the table which has ot be displayed
                Dim iLastRowIndex As Integer = (iRowIndex + Session("PagingRows")) - 1 'Nos of records to be traversed
                For numRowIndex = iRowIndex To iLastRowIndex                                     'Loop through the row index from the table
                    If ((numRowIndex >= 0) And (dtContactSurveylist.Rows.Count > numRowIndex)) Then 'Ensure that the row exists
                        dtResultTableCopy.ImportRow(dtContactSurveylist.Rows(numRowIndex))       'import the range of rows
                    End If
                Next
                Return dtResultTableCopy
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to fill the search results for Adv.Search Screen from the XML file (intermediate search) when the page index changes
        ''' </summary>
        ''' <remarks> When the page number is specifically entered in the textbox, it executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub txtCurrentPageSurveyHistory_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrentPageSurveyHistory.TextChanged
            Try
                If IsNumeric(txtCurrentPageSurveyHistory.Text) Then
                    DisplaySurveyHistoryLists()                                                      'Call to execute the common steps to search and display results
                    litClientMessageSurveyHistory.Text = ""                                          'Clean the client side message
                Else : litClientMessageSurveyHistory.Text = "<script language=javascript>alert('Invalid page number entered, please re-enter.');</script>" 'Alert to user about invalid page number
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the last page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkLastSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLastSurveyHistory.Click
            Try
                txtCurrentPageSurveyHistory.Text = lblTotalSurveyHistory.Text                                 'Set the Page Index
                DisplaySurveyHistoryLists()                                                      'Call to execute the common steps to search and display results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the previous page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkPreviousSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPreviousSurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) > 1 Then                               'If the first page is already displayed then exit
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text - 1                               'decrement the page number
                End If
                DisplaySurveyHistoryLists()                                                                 'Call to execute the common steps to search and display results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the first page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkFirstSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirstSurveyHistory.Click
            Try
                txtCurrentPageSurveyHistory.Text = 1                                         'Set the first page index to 1
                DisplaySurveyHistoryLists()                                                  'Call to execute the common steps to search and display results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the next page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkNextSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNextSurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) < Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if this is not the last page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 1 'increment the page number
                End If
                DisplaySurveyHistoryLists()                                                'Call to execute the common steps to search and display results
                litClientMessageSurveyHistory.Text = ""                                    'Clean the client side message
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 2 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk2SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 2 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 2  'increment the page number by 2
                End If
                DisplaySurveyHistoryLists()                                                  'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 3 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk3SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 3 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 3      'increment the page number by 3
                End If
                DisplaySurveyHistoryLists()                                                      'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 4 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk4SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 4 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then  'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 4      'increment the page number by 4
                End If
                DisplaySurveyHistoryLists()                                                      'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 5 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk5SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 5 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 5  'increment the page number by 5
                End If
                DisplaySurveyHistoryLists()                                                  'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnActionItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActionItem.Click
            Try
                Response.Redirect("../Admin/newaction.aspx?frm=contactdetails&CntID=" & lngCntID & "&SI=" & SI1 & "&SI1=" & uwOppTab.SelectedTabIndex & "&frm1=" & frm)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnName(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnFav_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFav.Click
            Try
                If objContacts Is Nothing Then objContacts = New CContacts
                objContacts.byteMode = 0
                objContacts.UserCntID = Session("UserContactID")
                objContacts.ContactID = lngCntID
                objContacts.Type = "C"
                objContacts.ManageFavorites()
                litMessage.Text = "Added to Favorites"
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                If objContacts Is Nothing Then objContacts = New CContacts
                objContacts.ContactID = lngCntID
                objContacts.DomainID = Session("DomainID")
                If objContacts.DelContact > 1 Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else : Response.Redirect("../contact/frmContactList.aspx")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            Try
                Response.Redirect("../pagelayout/frmContactEdit.aspx?frm=ContactDis&CntId=" & lngCntID & "&SI=" & uwOppTab.SelectedTabIndex & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm & "&frm2=" & frm1 & "&RelID=" & GetQueryStringVal(Request.QueryString("enc"), "RelID") & "&profileid=" & GetQueryStringVal(Request.QueryString("enc"), "profileId") & "&ContactType=" & GetQueryStringVal(Request.QueryString("enc"), "ContactType") & "&RelId=" & GetQueryStringVal(Request.QueryString("enc"), "RelID"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnCorresGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorresGo.Click
            Try
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rptCorr_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptCorr.ItemCommand
            Try
                If e.CommandName = "Sort" Then
                    If e.CommandSource.ID = "lnkDate" Then
                        strColumn = "Date"
                    ElseIf e.CommandSource.ID = "lnkType" Then
                        strColumn = "Type"
                    ElseIf e.CommandSource.ID = "lnkFrom" Then
                        strColumn = "[From]"
                    ElseIf e.CommandSource.ID = "lnkName" Then
                        strColumn = "phone"
                    ElseIf e.CommandSource.ID = "lnkAssigned" Then
                        strColumn = "assignedto"
                    End If
                End If
                If Session("Column") <> strColumn Then
                    Session("Column") = strColumn
                    Session("Asc") = 0
                Else
                    If Session("Asc") = 0 Then
                        Session("Asc") = 1
                    Else : Session("Asc") = 0
                    End If
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlFilterCorr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterCorr.SelectedIndexChanged
            Try
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnCorrDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorrDelete.Click
            Try
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim chk As CheckBox
                Dim lbl As Label
                For Each r As RepeaterItem In rptCorr.Items
                    chk = r.FindControl("chkADelete")
                    If chk.Checked = True Then
                        lbl = r.FindControl("lblDelete")
                        If lbl.Text.Split("~")(1) = 1 Then
                            objContacts.EmailHstrID = lbl.Text.Split("~")(0)
                            objContacts.tinttype = 1
                        Else
                            objContacts.EmailHstrID = lbl.Text.Split("~")(0)
                            objContacts.tinttype = 2
                        End If
                        objContacts.DelCorrespondence()
                    End If
                Next
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub getCorrespondance()
            Try
                m_aryRightsForActItem = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 7)
                If m_aryRightsForActItem(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 0 Then btnCorrDelete.Visible = False
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim dttable As DataTable
                objContacts.FromDate = Calendar1.SelectedDate
                objContacts.ToDate = DateAdd(DateInterval.Day, 1, CDate(Calendar2.SelectedDate))
                objContacts.ContactID = lngCntID
                objContacts.DivisionID = 0
                objContacts.MessageFrom = txtEmail.Text
                objContacts.SortOrder1 = ddlFilterCorr.SelectedValue
                objContacts.UserCntID = Session("UserContactID")
                objContacts.SortOrder = ddlSrchCorr.SelectedValue
                objContacts.KeyWord = txtSearchCorr.Text
                objContacts.DomainID = Session("DomainID")
                objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                If txtCurrentPageCorr.Text.Trim <> "" Then
                    objContacts.CurrentPage = txtCurrentPageCorr.Text
                Else : objContacts.CurrentPage = 1
                End If
                objContacts.PageSize = Session("PagingRows")
                objContacts.TotalRecords = 0
                If strColumn <> "" Then
                    objContacts.columnName = strColumn
                Else : objContacts.columnName = "date"
                End If
                If Session("Asc") = 1 Then
                    objContacts.columnSortOrder = "Desc"
                Else : objContacts.columnSortOrder = "Asc"
                End If
                dttable = objContacts.getCorres()
                If objContacts.TotalRecords = 0 Then
                    tdCorr.Visible = False
                    lblNoOfRecordsCorr.Text = objContacts.TotalRecords
                Else
                    tdCorr.Visible = True
                    lblNoOfRecordsCorr.Text = String.Format("{0:#,###}", objContacts.TotalRecords)
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblNoOfRecordsCorr.Text / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (lblNoOfRecordsCorr.Text Mod Session("PagingRows")) = 0 Then
                        lblTotalCorr.Text = strTotalPage(0)
                        txtCorrTotalPage.Text = strTotalPage(0)
                    Else
                        lblTotalCorr.Text = strTotalPage(0) + 1
                        txtCorrTotalPage.Text = strTotalPage(0) + 1
                    End If
                    txtCorrTotalRecords.Text = lblNoOfRecordsCorr.Text
                End If
                rptCorr.DataSource = dttable
                rptCorr.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub lnk2Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2Corr.Click
            Try
                If txtCurrentPageCorr.Text + 1 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 1 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 2
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk3Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3Corr.Click
            Try
                If txtCurrentPageCorr.Text + 2 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 2 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 3
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk4Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4Corr.Click
            Try
                If txtCurrentPageCorr.Text + 3 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 3 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 4
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk5Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5Corr.Click
            Try
                If txtCurrentPageCorr.Text + 4 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 4 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 5
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkFirstCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFirstCorr.Click
            Try
                txtCurrentPageCorr.Text = 1
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkLastCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLastCorr.Click
            Try
                txtCurrentPageCorr.Text = txtCorrTotalPage.Text
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkNextCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextCorr.Click
            Try
                If txtCurrentPageCorr.Text = txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 1
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkPreviousCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreviousCorr.Click
            Try
                If txtCurrentPageCorr.Text = 1 Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text - 1
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rptCorr_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCorr.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Then
                    Dim chk As CheckBox
                    Dim lbl As Label
                    chk = e.Item.FindControl("chkADelete")
                    lbl = e.Item.FindControl("lblDelete")
                    If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If lbl.Text.Split("~")(2) <> Session("UserContactID") Then chk.Visible = False
                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub uwOppTab_TabClick(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebTab.WebTabEvent) Handles uwOppTab.TabClick
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTabDetails()
            Try
                Select Case uwOppTab.SelectedTabIndex
                    Case 0
                        m_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 9)
                        LoadTableInformation()
                        DisplayDynamicFlds()
                    Case 1 : sb_ShowAOIs()
                    Case 2 : sc_GetOppDetails()
                    Case 3 : DisplaySurveyHistoryLists()
                    Case 4 : getCorrespondance()
                    Case Else
                        m_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 9)
                        DisplayDynamicFlds()
                End Select
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Private Sub radOppClose_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOppClose.CheckedChanged
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub radOppOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOppOpen.CheckedChanged
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace

