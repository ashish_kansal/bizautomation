<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCasesDtl.aspx.vb"
    Inherits="BACRM.UserInterface.Cases.frmCasesDtl" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link href="../css/lists.css" type="text/css" rel="stylesheet">
    <title>Case</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">

    <script language="JavaScript" src="../javascript/date-picker.js" type="text/javascript"></script>

    <script>
        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email=" + a + "&Date=" + f, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
                return false;
            }
            else {
                window.location.href = "../admin/actionitemdetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Cases&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
                return false;
            }

        }
        function chkAll() {
            for (i = 1; i <= 20; i++) {
                var str;
                if (i < 10) {
                    str = '0' + i
                }
                else {
                    str = i
                }
                if (document.getElementById('uwOppTab__ctl3_rptCorr_ctl00_chkDelete').checked == true) {
                    if (document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete') != null) {
                        document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete').checked = true;
                    }
                }
                else {
                    if (document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete') != null) {
                        document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete').checked = false;
                    }
                }
            }
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenEmailMessage(a, b) {
            window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email=" + a + "&Date=" + b, '', 'width=650,height=350,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
            return false;
        }
        function OpenItemDtls(OpID) {
            window.open("../Items/frmItemsforCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" + OpID, '', 'width=650,height=350,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
            return false;
        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                //document.Form1.ddlStatus.style.display='none';
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                //document.Form1.ddlStatus.style.display='';
                return false;

            }
        }
        function openSol(a) {
            window.open("../cases/frmCaseLinkSol.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CaseID=" + a, '', 'width=800,height=500,status=no,titlebar=no,scrollbar=yes,top=110,left=150,resizable=yes')
            //			document.getElementById('cntDoc').src="../cases/frmCaseLinkSol.aspx?CaseID="+a
            //			document.getElementById('divSol').style.visibility = "visible";
            return false;

        }
        function AddContact() {
            if (document.Form1.ddlcompany.value == 0) {
                alert("Select Customer");
                tsCases.selectedIndex = 2;
                document.Form1.ddlcompany.focus();
                return false;
            }
            if (document.Form1.ddlAssocContactId.value == 0) {
                alert("Select Contact");
                tsCases.selectedIndex = 2;
                document.Form1.ddlAssocContactId.focus();
                return false;
            }
            var str;
            for (i = 0; i < document.Form1.elements.length; i++) {
                if (i <= 9) {
                    str = '0' + (i + 1)
                }
                else {
                    str = i + 1
                }
                if (document.getElementById('dgContact_ctl' + str + '_txtContactID') != null) {
                    if (document.getElementById('dgContact_ctl' + str + '_txtContactID').value == document.Form1.ddlAssocContactId.value) {
                        alert("Associated contact is already added");
                        return false;
                    }
                }
            }

        }
        function NewTask(a, b) {
            //				alert("cntid : "+a);
            //				alert("caseId : "+b);
            window.location = "../Admin/newaction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Cases&CntID=" + a + "&CaseID=" + b;


        }
        function ShowLayout(a, b) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=S", '', 'toolbar=no,titlebar=no,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table id="tblMenu" bordercolor="black" cellspacing="0" cellpadding="0" width="100%"
                border="0" runat="server">
                <tr>
                    <td class="tr1" align="center">
                        <b>Record Owner: </b>
                        <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                    </td>
                    <td class="td1" width="1" height="18">
                    </td>
                    <td class="tr1" align="center">
                        <b>Created By: </b>
                        <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                    </td>
                    <td class="td1" width="1" height="18">
                    </td>
                    <td class="tr1" align="center">
                        <b>Last Modified By: </b>
                        <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                    </td>
                </tr>
            </table>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal1" align="center">
                                    Organization : <u>
                                        <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink>
                                </td>
                                <td class="normal1" align="right">
                                    Case Number :
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblcasenumber" ForeColor="Red" runat="server"></asp:Label>
                                </td>
                                <td class="normal1" align="right">
                                    Status :
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblStatus" runat="server" CssClass="norrmal1"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnLinkSol" runat="server" CssClass="button" Text="Link Solution">
                                    </asp:Button>
                                    <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" Width="50">
                                    </asp:Button>
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                                    <asp:Button ID="btnActDelete" runat="server" CssClass="Delete" Text="X" ></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:Panel runat="server" ID="pnlContract">
                <table width="100%" class="normal1" align="center">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="10%" align="right">
                                        Contract :
                                    </td>
                                    <td width="20%">
                                        <asp:DropDownList ID="ddlContract" AutoPostBack="true" Width="180" runat="server"
                                            CssClass="signup">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="10%" align="right">
                                        Amount Balance :
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblRemAmount" CssClass="normal1"></asp:Label>
                                    </td>
                                    <td width="10%" align="right">
                                        Days Remaining :
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblRemDays" CssClass="normal1"></asp:Label>
                                    </td>
                                    <td width="10%" align="right">
                                        Incidents Remaining :
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblRemInci" CssClass="normal1"></asp:Label>
                                    </td>
                                    <td width="10%" align="right">
                                        Hours Remaining :
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblRemHours" CssClass="normal1"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <igtab:UltraWebTab AutoPostBack="true" ImageDirectory="" ID="uwOppTab" runat="server"
                ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                <DefaultTabStyle Height="23px" CssClass="InfraDefaultTabStyle">
                </DefaultTabStyle>
                <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                    NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                    FillStyle="LeftMergedWithCenter"></RoundedImage>
                <SelectedTabStyle Height="23px" CssClass="InfraHoverSelTabStyle">
                </SelectedTabStyle>
                <HoverTabStyle Height="23px" CssClass="InfraHoverSelTabStyle">
                </HoverTabStyle>
                <Tabs>
                    <igtab:Tab Text="&nbsp;&nbsp;Case Details&nbsp;&nbsp;">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updatepanel3" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
                                EnableViewState="true">
                                <ContentTemplate>
                                    <asp:Table ID="tblCase" CellPadding="1" CellSpacing="1" BorderWidth="1" Height="300"
                                        runat="server" CssClass="aspTableDTL" Width="100%" BorderColor="black" GridLines="None">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="top">
                                                <asp:Table Width="100%" ID="tbl12" runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">
                                                            <asp:Button ID="btnLayout" runat="server" CssClass="button" Text="Layout"></asp:Button>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell VerticalAlign="top">
                                                <img src="../images/SuitCase-32.gif" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Table runat="server" ID="tabledetail" BorderWidth="0" GridLines="none" CellPadding="2"
                                                                CellSpacing="0" HorizontalAlign="Center">
                                                            </asp:Table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell ColumnSpan="2">
                                                            <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%" GridLines="none"
                                                                HorizontalAlign="Center">
                                                            </asp:Table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Top">
                                                <br>
                                                <table width="100%">
                                                    <asp:Panel ID="pnlKnowledgeBase" runat="server">
                                                        <tr style="width: 100%">
                                                            <td class="normal1" align="right" width="10%">
                                                                Solutions added from<br>
                                                                the Knowledge Base
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:DataGrid ID="dgSolution" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                                                    ShowFooter="False">
                                                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                                    <ItemStyle CssClass="is"></ItemStyle>
                                                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                                                </asp:DataGrid>
                                                                <br>
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                </table>
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td width="10%">
                                                        </td>
                                                        <td colspan="6" class="normal1">
                                                            <asp:LinkButton ID="lnkbtnNoofCom" runat="server" CommandName="Show"></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                            <asp:LinkButton ID="lnlbtnPost" runat="server" CommandName="Post">Post Comment</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8">
                                                            <asp:Table ID="tblComments" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
                                                                Visible="False">
                                                            </asp:Table>
                                                        </td>
                                                    </tr>
                                                    <asp:Panel ID="pnlPostComment" runat="server" Visible="False">
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Heading
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:TextBox ID="txtHeading" runat="server" Width="500" CssClass="signup"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Comment
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:TextBox ID="txtComments" runat="server" Width="500" CssClass="signup" Height="90"
                                                                    TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="6">
                                                                <asp:Button ID="btnSubmit" runat="server" Width="50" CssClass="button" Text="Submit">
                                                                </asp:Button>
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                </table>
                                                <br>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="&nbsp;&nbsp;Associated Contacts&nbsp;&nbsp;">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updatepanel2" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
                                EnableViewState="true">
                                <ContentTemplate>
                                    <asp:Table ID="Table1" BorderWidth="1" Height="300" runat="server" Width="100%" BorderColor="black"
                                        CssClass="aspTableDTL" GridLines="None">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Top">
                                                <br />
                                                <asp:DataGrid ID="dgContact" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                                    AutoGenerateColumns="False">
                                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="is"></ItemStyle>
                                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                                    <Columns>
                                                        <asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Organization,Relationship" DataField="Company"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="First & Last Name" DataField="Name"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Email" DataField="Email"></asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="Contact Role">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" CssClass="normal1" Text='<%# DataBinder.Eval(Container.DataItem, "ContactRole") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Share Opportunity via Partner Point ?">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblShare" runat="server" CssClass="cell"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                                <br>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="&nbsp;&nbsp;Tasks&nbsp;&nbsp;">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updatepanel5" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
                                EnableViewState="true">
                                <ContentTemplate>
                                    <asp:Table ID="Table4" CellPadding="0" CellSpacing="0" runat="server" BorderWidth="1"
                                        Width="100%" CssClass="aspTable" GridLines="None" BorderColor="black" Height="300">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Top">
                                                <table width="100%" border="0">
                                                    <tr align="center" valign="top">
                                                        <td align="right">
                                                            <table>
                                                                <tr>
                                                                    <td class="normal1" align="right">
                                                                        From
                                                                    </td>
                                                                    <td align="left">
                                                                        <BizCalendar:Calendar ID="Calendar1" runat="server" />
                                                                    </td>
                                                                    <td class="normal1" align="right">
                                                                        To
                                                                    </td>
                                                                    <td>
                                                                        <BizCalendar:Calendar ID="Calendar2" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="right">
                                                            <table class="normal1">
                                                                <tr>
                                                                    <td>
                                                                        Search
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSearchCorr" runat="server" CssClass="signup"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlSrchCorr" runat="server" CssClass="signup">
                                                                            <asp:ListItem Text="Tasks by Due Date" Value="5"></asp:ListItem>
                                                                            <asp:ListItem Text="Tasks by Created Date" Value="6"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnCorresGo" runat="server" CssClass="button" Text="Go"></asp:Button>&nbsp;
                                                                        <asp:Button ID="btnAddTask" runat="server" CssClass="button" Text="Add Task"></asp:Button>&nbsp;
                                                                        <asp:Button ID="btnCorrDelete" runat="server" CssClass="button" Text="Delete"></asp:Button>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="right">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="normal1">
                                                                        Filter :
                                                                        <asp:DropDownList ID="ddlFilterCorr" runat="server" AutoPostBack="true" CssClass="signup">
                                                                            <asp:ListItem Text="Tasks by Due Date" Value="5"></asp:ListItem>
                                                                            <asp:ListItem Text="Tasks by Created Date" Value="6"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td align="center" class="normal1">
                                                                        &nbsp;&nbsp;&nbsp;No of Records :
                                                                        <asp:Label ID="lblNoOfRecordsCorr" runat="server" CssClass="text"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                                    </td>
                                                                    <td id="tdCorr" runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lblNextCorr" runat="server" CssClass="text_bold">&nbsp;&nbsp;&nbsp;Next:</asp:Label>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:LinkButton ID="lnk2Corr" runat="server" CausesValidation="False">2</asp:LinkButton>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:LinkButton ID="lnk3Corr" runat="server" CausesValidation="False">3</asp:LinkButton>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:LinkButton ID="lnk4Corr" runat="server" CausesValidation="False">4</asp:LinkButton>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:LinkButton ID="lnk5Corr" runat="server" CausesValidation="False">5</asp:LinkButton>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkFirstCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow"><<</div>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkPreviousCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow"><</div>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:Label ID="lblPageCorr" runat="server">Page</asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtCurrentPageCorr" runat="server" Text="1" Width="28px" CssClass="signup"
                                                                                        MaxLength="5" AutoPostBack="true"></asp:TextBox>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:Label ID="lblOfCorr" runat="server">of</asp:Label>
                                                                                </td>
                                                                                <td class="normal1">
                                                                                    <asp:Label ID="lblTotalCorr" runat="server"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkNextCorr" runat="server" CssClass="LinkArrow" CausesValidation="False">
															                    <div class="LinkArrow">></div>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkLastCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow">>></div>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:Repeater ID="rptCorr" runat="server">
                                                                <HeaderTemplate>
                                                                    <table cellspacing="0" class="dg" width="100%">
                                                                        <tr class="hs">
                                                                            <td style="display: none">
                                                                                numEmailHstrId
                                                                            </td>
                                                                            <td style="display: none">
                                                                                tintType
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:LinkButton CommandName="Sort" ID="lnkDate" runat="server"><font color="white">Date</font></asp:LinkButton>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:LinkButton CommandName="Sort" ID="lnkType" runat="server"><font color="white">Type</font></asp:LinkButton>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:LinkButton CommandName="Sort" ID="lnkFrom" runat="server"><font color="white">From ,To</font></asp:LinkButton>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:LinkButton CommandName="Sort" ID="lnkName" runat="server"><font color="white">Name /Phone ,& Ext.</font></asp:LinkButton>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:LinkButton CommandName="Sort" ID="lnkAssigned" runat="server"><font color="white">Assigned To</font></asp:LinkButton>
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:CheckBox ID="chkDelete" onclick="chkAll()" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                </HeaderTemplate>
                                                                <AlternatingItemTemplate>
                                                                    <tr class="ais" align="center">
                                                                        <td style="display: none">
                                                                            <%#Container.DataItem("numEmailHstrId")%>
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <%#Container.DataItem("tintType")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("date")%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:HyperLink ID="hplAType" CssClass="hyperlink" NavigateUrl="#" onclick="openActionItem('<%#Container.DataItem("numEmailHstrId")%>','<%#Container.DataItem("caseid")%>','<%#Container.DataItem("CaseTimeId")%>','<%#Container.DataItem("CaseExpId")%>','<%#Container.DataItem("Type")%>','<%#Container.DataItem("dtCreatedDate")%>')"
                                                                                runat="server"><u style="cursor:hand"><%#Container.DataItem("Type")%></u></asp:HyperLink>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("From")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("Phone")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("assignedto")%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkADelete" Style="color: #C6D3E7;" runat="server" />
                                                                            <asp:Label ID="lblDelete" Visible="false" runat="server" Text='<%#Container.DataItem("DelData")%>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="ais">
                                                                        <td colspan="8">
                                                                            <%#Container.DataItem("Subject")%>
                                                                        </td>
                                                                    </tr>
                                                                </AlternatingItemTemplate>
                                                                <ItemTemplate>
                                                                    <tr class="is">
                                                                        <td style="display: none">
                                                                            <%#Container.DataItem("numEmailHstrId")%>
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <%#Container.DataItem("tintType")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("date")%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:HyperLink ID="hplType" CssClass="hyperlink" NavigateUrl="#" onclick="openActionItem('<%#Container.DataItem("numEmailHstrId")%>','<%#Container.DataItem("caseid")%>','<%#Container.DataItem("CaseTimeId")%>','<%#Container.DataItem("CaseExpId")%>','<%#Container.DataItem("Type")%>','<%#Container.DataItem("dtCreatedDate")%>')"
                                                                                runat="server"><u style="cursor:hand"><%#Container.DataItem("Type")%></u></asp:HyperLink>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("From")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("Phone")%>
                                                                        </td>
                                                                        <td>
                                                                            <%#Container.DataItem("assignedto")%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkADelete" runat="server" />
                                                                            <asp:Label ID="lblDelete" Visible="false" runat="server" Text='<%#Container.DataItem("DelData")%>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="is">
                                                                        <td colspan="8">
                                                                            <%#Container.DataItem("Subject")%>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <asp:TextBox ID="txtCorrTotalPage" Style="display: none" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtCorrTotalRecords" Style="display: none" runat="server"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="&nbsp;&nbsp;Email History&nbsp;&nbsp;">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updatepanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
                                EnableViewState="true">
                                <ContentTemplate>
                                    <asp:Table ID="tblEmail" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
                                        runat="server" CssClass="aspTableDTL" Width="100%" BorderColor="black" GridLines="None">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Top">
                                                <br>
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td class="normal1" align="right">
                                                            From
                                                        </td>
                                                        <td align="left">
                                                            <BizCalendar:Calendar ID="calFrom" runat="server" />
                                                        </td>
                                                        <td class="normal1" align="right">
                                                            To
                                                        </td>
                                                        <td>
                                                            <BizCalendar:Calendar runat="server" ID="calTo" />
                                                        </td>
                                                        <td class="normal1">
                                                            Search
                                                            <asp:TextBox ID="txtSearchEmail" runat="server" CssClass="signup"></asp:TextBox>
                                                            &nbsp;
                                                            <asp:DropDownList ID="ddlFields" runat="server" Width="130" CssClass="signup">
                                                                <asp:ListItem Value="0">All Email Fields</asp:ListItem>
                                                                <asp:ListItem Value="1">Message Number</asp:ListItem>
                                                                <asp:ListItem Value="2">From</asp:ListItem>
                                                                <asp:ListItem Value="3">To</asp:ListItem>
                                                                <asp:ListItem Value="4">Subject</asp:ListItem>
                                                                <asp:ListItem Value="5">Body</asp:ListItem>
                                                            </asp:DropDownList>
                                                            &nbsp;
                                                            <asp:Button ID="btnEmailGo" runat="server" CssClass="button" Text="Go" Width="25">
                                                            </asp:Button>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="center" class="normal1">
                                                            No of messages :
                                                            <asp:Label ID="lblEmailNoofRecords" runat="server" CssClass="text"></asp:Label>
                                                        </td>
                                                        <td id="tdEmailNav" runat="server">
                                                            <table align="right">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label1" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:LinkButton ID="lnkEmail2" runat="server" CausesValidation="False">2</asp:LinkButton>
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:LinkButton ID="lnkEmal3" runat="server" CausesValidation="False">3</asp:LinkButton>
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:LinkButton ID="lnkEmail4" runat="server" CausesValidation="False">4</asp:LinkButton>
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:LinkButton ID="lnkEmail5" runat="server" CausesValidation="False">5</asp:LinkButton>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lnkEmailFirst" runat="server" CausesValidation="False">
															<div class="LinkArrow"><<</div>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lnkEmailPrevious" runat="server" CausesValidation="False">
															<div class="LinkArrow"><</div>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:Label ID="Label2" runat="server">Page</asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtEmailPage" runat="server" Text="1" Width="28px" CssClass="signup"
                                                                            MaxLength="5" AutoPostBack="True"></asp:TextBox>
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:Label ID="Label3" runat="server">of</asp:Label>
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:Label ID="lblEmailPageNo" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lnkEmailNext" runat="server" CssClass="LinkArrow" CausesValidation="False">
															<div class="LinkArrow">></div>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="linkEmailLast" runat="server" CausesValidation="False">
															<div class="LinkArrow">>></div>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:DataGrid ID="dgEmail" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                                                                BorderColor="white" AllowSorting="True">
                                                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                                <ItemStyle CssClass="is"></ItemStyle>
                                                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="CreatedOn" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="numEmailHstrID" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="bintCreatedOn" SortExpression="bintCreatedOn" HeaderText="<font color=white>Date/Time</font>">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="vcFromEmail" SortExpression="vcFromEmail" HeaderText="<font color=white>From</font>">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="vcMessageTo" SortExpression="vcEmail" HeaderText="<font color=white>To</font>">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="vcSubject" SortExpression="vcSubject" HeaderText="<font color=white>Subject</font>">
                                                                    </asp:BoundColumn>
                                                                </Columns>
                                                                <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages">
                                                                </PagerStyle>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </igtab:Tab>
                </Tabs>
            </igtab:UltraWebTab>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
                    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
                    <asp:TextBox ID="txtDivId" Style="display: none" runat="server"></asp:TextBox>
                </tr>
            </table>
            <asp:TextBox ID="txtLstUptCom" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalRecords" Style="display: none" runat="server"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
