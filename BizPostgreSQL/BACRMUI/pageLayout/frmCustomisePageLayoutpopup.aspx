<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="~/pageLayout/frmCustomisePageLayoutpopup.aspx"
    Inherits=".frmCustomisePageLayoutpopup" MasterPageFile="~/common/DetailPage.Master" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Customize Page Layout</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <link rel="stylesheet" href="../css/lists.css" type="text/css" />
    <script type="text/javascript" src="../javascript/coordinates.js"></script>
    <script type="text/javascript" src="../javascript/drag.js"></script>
    <script type="text/javascript" src="../javascript/dragdrop.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            DragDrop.reset();
            var max = document.getElementById("rows").value;

            for (var x = 0; x <= max; x++) {
                if (document.getElementById("x" + x) != null) {
                    list = document.getElementById("x" + x);
                    DragDrop.makeListContainer(list, 'g2');
                    list.onDragOver = function () { this.style["background"] = "none"; };
                    list.onDragOut = function () { this.style["background"] = "none"; };
                }
            }
        }

        function getSort() {
            order = document.getElementById("order");
            order.value = DragDrop.serData('g2', null);

        }

        function showValue() {
            order = document.getElementById("order");
            alert(order.value);
        }
        function show() {
            document.getElementById("btnUpdate").click();
            return false;

        }
        function show1() {
            document.getElementById("btnUpdate1").click();
            return false;
        }
        function AddColumn() {
            order = document.getElementById("order");
            order.value = DragDrop.serData('g2', null);
            document.getElementById("btnAddColumn").click();
            return false;

        }
        function DeleteColumn() {

            var max = document.getElementById("rows").value;
            if (max > 1) {
                order = document.getElementById("order");
                order.value = DragDrop.serData('g2', null);
                document.getElementById("btnDeleteColumn").click();
                return false;
            }
            else {
                alert("Columns Cannot Be Less Than One");
                return false;
            }
        }
        function Close() {
            opener.location.reload(true);
            window.close()
            return false;
        }
        function dlChange() {
            var mylist = document.getElementById("dlh")
            order = document.getElementById("type");
            order.value = mylist.options[mylist.selectedIndex].value
            document.getElementById("btnBindData").click();
            return false;
        }
        function ddlItemTypeChange() {
            var mylist = document.getElementById("ddlItemType")
            order = document.getElementById("type");
            order.value = mylist.options[mylist.selectedIndex].value
            document.getElementById("btnBindData").click();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
     <asp:Label ID="lblTopAction" runat='server' Width="100%"></asp:Label>
      <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    </asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server" ClientIDMode="Static">
     <asp:Label ID="lblMainContent" runat='server' Width="100%"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server"></asp:Label>
    </asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server" ClientIDMode="Static">
    <asp:HiddenField ID="order" runat="server" />
    <asp:HiddenField ID="hfCtype" runat="server" />
    <asp:HiddenField ID="hfFormId" runat="server" />
    <asp:HiddenField ID="hfPType" runat="server" />
    <asp:TextBox runat="server" ID="rows" Style="display: none"></asp:TextBox>
    <asp:Button ID="maxRow" runat="server" Style="display: none" />
    <asp:Button ID="btnUpdate" Text="" runat="server" Style="display: none" />
    <asp:Button ID="btnUpdate1" Text="" runat="server" Style="display: none" />
    <asp:TextBox ID="type" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:Button ID="btnBindData" Text="update" runat="server" Style="display: none" />
   
    <script type="text/xml-script">
        <page xmlns:script="http://schemas.microsoft.com/xml-script/2005">
            <references>
            </references>
            <components>
            </components>
        </page>
    </script>
</asp:Content>
