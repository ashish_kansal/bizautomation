﻿Imports BACRM.BusinessLogic.Accounting

Partial Public Class frmRecurringDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim intOppId As Integer
            intOppId = GetQueryStringVal(Request.QueryString("enc").ToString, "oppId")
            hdnoppId.Value = intOppId

            Dim dtTempDetails As DataTable
            Dim objRecurringEvents As RecurringEvents
            If Not Page.IsPostBack Then



                If objRecurringEvents Is Nothing Then objRecurringEvents = New RecurringEvents
                objRecurringEvents.DomainId = Session("DomainId")
                dtTempDetails = objRecurringEvents.GetTemplateDetails()
                ddlRec.DataSource = dtTempDetails
                ddlRec.DataTextField = "varRecurringTemplateName"
                ddlRec.DataValueField = "numRecurringId"
                ddlRec.DataBind()
                Dim dtRecurringId As DataTable
                'This code will get the recurring Id for the opportunity and load the drop down and radio 
                objRecurringEvents.OpportunityId = intOppId
                dtRecurringId = objRecurringEvents.GetRecurringTemplateId()
                If dtRecurringId.Rows.Count > 0 Then
                    If IsNumeric(dtRecurringId.Rows(0).Item("numRecurringid")) Then
                        If dtRecurringId.Rows(0).Item("numRecurringid") <> 0 Then
                            rdoRec.Checked = True
                            rdoBizDoc.Checked = False
                            ddlRec.Items.FindByValue(dtRecurringId.Rows(0).Item("numRecurringid")).Selected = True
                            chkAmtZero.Checked = dtRecurringId.Rows(0).Item("bitRecurringZeroAmt")
                        End If
                    End If
                End If


            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Protected Sub btnSaveandClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveandClose.Click
        Try
            If rdoRec.Checked = True Then
                UpdateRecurringDetails(ddlRec.SelectedValue)
            ElseIf rdoBizDoc.Checked = True Then

            End If
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "<script> window.close(); </script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Function UpdateRecurringDetails(ByVal recId As Integer)
        Try
            Dim objRecurringEvents As RecurringEvents
            If objRecurringEvents Is Nothing Then objRecurringEvents = New RecurringEvents
            objRecurringEvents.DomainId = Session("DomainId")
            objRecurringEvents.RecurringId = recId
            objRecurringEvents.OpportunityId = hdnoppId.Value
            objRecurringEvents.UpdateOpportunityMaster(chkAmtZero.Checked)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CloseMe", "<script> window.close(); </script>")
    End Sub
End Class