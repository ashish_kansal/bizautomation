Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebTab
Imports System.Reflection

Namespace BACRM.UserInterface.Prospects
    Public Class frmProspectEdit : Inherits BACRMPage

        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim objContacts As CContacts
        Dim objUserAccess As UserAccess
        Dim dtCompanyTaxTypes As DataTable
        Dim objProspects As CProspects
        Dim objAssociationInfo As OrgAssociations
        Dim objAccount As CAccounts
        Dim objLeads As CLeads
        Dim objCommon As New CCommon
        Dim lngDivID As Long
        Dim m_aryRightsForPage(), m_aryRightsForActItem(), m_aryRightsForOpp(), m_aryRightsForCustFlds(), m_aryRightsForContacts() As Integer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                    SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                    SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
                Else : SI1 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
                Else : SI2 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                    frm = ""
                    frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
                Else : frm = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                    frm1 = ""
                    frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
                Else : frm1 = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    frm2 = ""
                    frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
                Else : frm2 = ""
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "SurveyRespondents" Or GetQueryStringVal(Request.QueryString("enc"), "frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    'Added by Debasish Nag on 6th Jan 2006
                    Dim objForm As Object                                   'The generic object
                    objForm = Page.FindControl("webmenu1")                  'Get a holder to the web control
                    objForm.visible = False                                 'Hide the web control
                    btnCancel.Attributes.Add("onclick", "javascript: window.close();return false;") 'Write code to close the window
                End If
                lngDivID = GetQueryStringVal(Request.QueryString("enc"), "DivID")

                ' Checking the rights to view Contacts
                m_aryRightsForContacts = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 5)
                If Not IsPostBack Then
                    If objContacts Is Nothing Then objContacts = New CContacts
                    objContacts.RecID = lngDivID
                    objContacts.Type = "C"
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.AddVisiteddetails()

                    Session("Help") = "Organization"
                    LoadAssociationInformation()                            'Added by Debasish for displayign the Association information

                    'checking rights to view the page
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 4)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    Else
                        If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                            btnSave.Visible = False
                            btnSaveClose.Visible = False
                        End If
                        If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False
                    End If

                    sb_CompanyInfo()
                    Dim dtTab As DataTable
                    dtTab = Session("DefaultTab")
                    If dtTab.Rows.Count > 0 Then
                        uwOppTab.Tabs(0).Text = IIf(IsDBNull(dtTab.Rows(0).Item("vcProspect")), "Prospect Details", dtTab.Rows(0).Item("vcProspect").ToString & " Details")
                    Else : uwOppTab.Tabs(0).Text = "Prospect Details"
                    End If
                End If
                If ddlRelationhip.SelectedItem.Value <> 0 Then
                    lblCustomer.Text = ddlRelationhip.SelectedItem.Text
                    Session("Relation") = ddlRelationhip.SelectedItem.Value
                Else : Session("Relation") = 0
                End If
                m_aryRightsForCustFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProspects.aspx", Session("UserID"), 3, 9)
                If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then DisplayDynamicFlds()
                If Not IsPostBack Then
                    If frm = "ProspectDtl" Then
                        If SI > 0 And SI <= 7 Then
                            If SI = 4 Then
                                SI = 1
                            Else : SI = 0
                            End If
                        ElseIf SI > 7 Then
                            SI = (SI - 7) + 1
                        End If
                        If uwOppTab.Tabs.Count >= SI Then uwOppTab.SelectedTabIndex = SI
                    End If
                    txtNetDays.Attributes.Add("onkeypress", "CheckNumber(2)")
                    txtInterest.Attributes.Add("onkeypress", "CheckNumber(1)")
                    hplFollowUpHstr.Attributes.Add("onclick", "return openFollow(" & lngDivID & ")")
                    hplAddress.Attributes.Add("onclick", "return OpenAdd(" & lngDivID & ");")
                    hplDocuments.Attributes.Add("onclick", "return OpenDocuments(" & lngDivID & ");")
                    hplTo.Attributes.Add("onclick", "return OpenTo(" & lngDivID & ");")
                    hplFrom.Attributes.Add("onclick", "return OpenFrom(" & lngDivID & ");")
                    btnEditWebLnk.Attributes.Add("onclick", "return fn_EditLabels('" & "../admin/AdminWebLinkLabels.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" & lngDivID & "')")
                    btnWebGo1.Attributes.Add("onclick", "return fn_GoToURL('txtWebLink1');")
                    btnWebGo2.Attributes.Add("onclick", "return fn_GoToURL('txtWebLink2');")
                    btnWebGo3.Attributes.Add("onclick", "return fn_GoToURL('txtWebLink3');")
                    btnWebGo4.Attributes.Add("onclick", "return fn_GoToURL('txtWebLink4');")
                    btnGo.Attributes.Add("onclick", "return fn_GoToURL('txtWeb');")

                    If lngDivID = Session("UserDivisionID") Then
                        btnActDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadProfile()
            Try
                If objUserAccess Is Nothing Then objUserAccess = New UserAccess
                objUserAccess.RelID = ddlRelationhip.SelectedItem.Value
                objUserAccess.DomainID = Session("DomainID")
                ddlProfile.DataSource = objUserAccess.GetRelProfileD
                ddlProfile.DataTextField = "ProName"
                ddlProfile.DataValueField = "numProfileID"
                ddlProfile.DataBind()
                ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))

                objUserAccess.RelID = ddlRelationhip.SelectedItem.Value
                ddlFollow.DataSource = objUserAccess.GetRelFollowD
                ddlFollow.DataTextField = "Follow"
                ddlFollow.DataValueField = "numFollowID"
                ddlFollow.DataBind()
                ddlFollow.Items.Insert(0, New ListItem("---Select One---", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub sb_CompanyInfo()
            Try
                objCommon.sb_FillComboFromDBwithSel(ddlRating, 2, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlStatus, 1, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlRelationhip, 5, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlIndustry, 4, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlInfoSource, 18, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlAnnualRevenue, 6, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlCreditLimit, 3, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlNoOfEmp, 7, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlTerriory, 78, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlCampaign, 24, Session("DomainID"))
                If objProspects Is Nothing Then objProspects = New CProspects
                Dim dtComInfo As DataTable
                objProspects.DivisionID = lngDivID
                objProspects.DomainID = Session("DomainID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtComInfo = objProspects.GetCompanyInfoForEdit
                If dtComInfo.Rows.Count > 0 Then
                    lblAssoCountF.Text = dtComInfo.Rows(0).Item("AssociateCountFrom")
                    lblAssoCountT.Text = dtComInfo.Rows(0).Item("AssociateCountTo")
                    lblDocCount.Text = "(" & dtComInfo.Rows(0).Item("DocumentCount") & ")"
                    txtROwner.Text = dtComInfo.Rows(0).Item("numRecOwner")
                    lblRecordOwner.Text = dtComInfo.Rows(0).Item("RecOwner")
                    lblCreatedBy.Text = dtComInfo.Rows(0).Item("vcCreatedBy")
                    lblLastModifiedBy.Text = dtComInfo.Rows(0).Item("vcModifiedBy")
                    txtRelationShipName.Text = dtComInfo.Rows(0).Item("vcCompanyName")
                    If Not IsDBNull(dtComInfo.Rows(0).Item("numCampaignID")) Then
                        If Not ddlCampaign.Items.FindByValue(dtComInfo.Rows(0).Item("numCampaignID")) Is Nothing Then
                            ddlCampaign.Items.FindByValue(dtComInfo.Rows(0).Item("numCampaignID")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtComInfo.Rows(0).Item("fltInterest")) Then
                        txtInterest.Text = String.Format("{0:#,##0.00}", dtComInfo.Rows(0).Item("fltInterest"))
                    End If
                    If Not IsDBNull(dtComInfo.Rows(0).Item("numBillingDays")) Then
                        txtNetDays.Text = String.Format("{0:#,###}", dtComInfo.Rows(0).Item("numBillingDays"))
                    End If
                    If Not IsDBNull(dtComInfo.Rows(0).Item("tintBillingTerms")) Then
                        If dtComInfo.Rows(0).Item("tintBillingTerms") = 0 Then
                            chkBillinTerms.Checked = False
                        Else
                            chkBillinTerms.Checked = True
                            txtSummary.Text = "Net " & dtComInfo.Rows(0).Item("numBillingDays") & " , " & IIf(dtComInfo.Rows(0).Item("tintInterestType") = 0, "-", "+") & dtComInfo.Rows(0).Item("fltInterest") & " %"
                        End If
                    End If
                    If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyStatus")) Then
                        If Not ddlStatus.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyStatus")) Is Nothing Then
                            ddlStatus.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyStatus")).Selected = True
                        End If
                    End If

                    lblCustomerId.Text = lngDivID

                    ' txtDivision.Text = dtComInfo.Rows(0).Item("vcDivisionName")

                    If Not IsDBNull(dtComInfo.Rows(0).Item("numTerID")) Then
                        If Not ddlTerriory.Items.FindByValue(dtComInfo.Rows(0).Item("numTerID")) Is Nothing Then
                            ddlTerriory.Items.FindByValue(dtComInfo.Rows(0).Item("numTerID")).Selected = True
                        End If
                    End If

                    If Session("PopulateUserCriteria") = 1 Then
                        ddlTerriory.AutoPostBack = True
                        objCommon.sb_FillConEmpFromTerritories(ddlAssignedTo, Session("DomainID"), 0, 0, dtComInfo.Rows(0).Item("numTerID"))
                    ElseIf Session("PopulateUserCriteria") = 2 Then
                        objCommon.sb_FillConEmpFromDBUTeam(ddlAssignedTo, Session("DomainID"), Session("UserContactID"))
                    Else : objCommon.sb_FillConEmpFromDBSel(ddlAssignedTo, Session("DomainID"), 0, 0)
                    End If

                    'Public Flag
                    If dtComInfo.Rows(0).Item("bitPublicFlag") = 0 Then
                        chkPrivate.Checked = False
                    Else : chkPrivate.Checked = True
                    End If

                    'Rating
                    If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyRating")) Then
                        If Not ddlRating.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyRating")) Is Nothing Then
                            ddlRating.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyRating")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyIndustry")) Then
                        If Not ddlIndustry.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyIndustry")) Is Nothing Then
                            ddlIndustry.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyIndustry")).Selected = True
                        End If
                    End If


                    If Not IsDBNull(dtComInfo.Rows(0).Item("numAnnualRevID")) Then
                        If Not ddlAnnualRevenue.Items.FindByValue(dtComInfo.Rows(0).Item("numAnnualRevID")) Is Nothing Then
                            ddlAnnualRevenue.Items.FindByValue(dtComInfo.Rows(0).Item("numAnnualRevID")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyType")) Then
                        If Not ddlRelationhip.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyType")) Is Nothing Then
                            ddlRelationhip.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyType")).Selected = True
                        End If
                    End If
                    If ddlRelationhip.SelectedValue > 0 Then
                        uwOppTab.Tabs(0).Text = "&nbsp;&nbsp;" & ddlRelationhip.SelectedItem.Text & " Detail&nbsp;&nbsp;"
                    End If
                    LoadProfile()
                    If Not IsDBNull(dtComInfo.Rows(0).Item("vcProfile")) Then
                        If Not ddlProfile.Items.FindByValue(dtComInfo.Rows(0).Item("vcProfile")) Is Nothing Then
                            ddlProfile.Items.FindByValue(dtComInfo.Rows(0).Item("vcProfile")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtComInfo.Rows(0).Item("numAssignedTo")) Then
                        If Not ddlAssignedTo.Items.FindByValue(dtComInfo.Rows(0).Item("numAssignedTo")) Is Nothing Then
                            ddlAssignedTo.Items.FindByValue(dtComInfo.Rows(0).Item("numAssignedTo")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyCredit")) Then
                        If Not ddlCreditLimit.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyCredit")) Is Nothing Then
                            ddlCreditLimit.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyCredit")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtComInfo.Rows(0).Item("vcWebSite")) Then
                        If Not dtComInfo.Rows(0).Item("vcWebSite") = "" Then
                            txtWeb.Text = dtComInfo.Rows(0).Item("vcWebSite")
                        Else : txtWeb.Text = "http://"
                        End If
                    Else : txtWeb.Text = "http://"
                    End If
                    'txtWeb.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebSite")), "http://", dtComInfo.Rows(0).Item("vcWebSite"))
                    If Not IsDBNull(dtComInfo.Rows(0).Item("numNoOfEmployeesID")) Then
                        If Not ddlNoOfEmp.Items.FindByValue(dtComInfo.Rows(0).Item("numNoOfEmployeesID")) Is Nothing Then
                            ddlNoOfEmp.Items.FindByValue(dtComInfo.Rows(0).Item("numNoOfEmployeesID")).Selected = True
                        End If
                    End If
                    lblAddress.Text = IIf(dtComInfo.Rows(0).Item("vcBillStreet") = "", "", dtComInfo.Rows(0).Item("vcBillStreet") & ", ") & IIf(dtComInfo.Rows(0).Item("vcBillCity") = "", "", dtComInfo.Rows(0).Item("vcBillCity") & ", ") & IIf(dtComInfo.Rows(0).Item("vcBilState") = "", "", dtComInfo.Rows(0).Item("vcBilState") & ", ") & IIf(dtComInfo.Rows(0).Item("vcBillPostCode") = "", "", dtComInfo.Rows(0).Item("vcBillPostCode") & ", ") & dtComInfo.Rows(0).Item("vcBillCountry")
                    If Not IsDBNull(dtComInfo.Rows(0).Item("numFollowUpStatus")) Then
                        If Not ddlFollow.Items.FindByValue(dtComInfo.Rows(0).Item("numFollowUpStatus")) Is Nothing Then
                            ddlFollow.Items.FindByValue(dtComInfo.Rows(0).Item("numFollowUpStatus")).Selected = True
                        End If
                    End If

                    txtComments.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("txtComments")), "", dtComInfo.Rows(0).Item("txtComments"))
                    If Not IsDBNull(dtComInfo.Rows(0).Item("vcHow")) Then
                        If Not ddlInfoSource.Items.FindByValue(dtComInfo.Rows(0).Item("vcHow")) Is Nothing Then
                            ddlInfoSource.Items.FindByValue(dtComInfo.Rows(0).Item("vcHow")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtComInfo.Rows(0).Item("numFollowUpStatus")) Then
                        If Not ddlFollow.Items.FindByValue(dtComInfo.Rows(0).Item("numFollowUpStatus")) Is Nothing Then
                            ddlFollow.Items.FindByValue(dtComInfo.Rows(0).Item("numFollowUpStatus")).Selected = True
                        End If
                    End If

                    txtPhone.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcComPhone")), "", dtComInfo.Rows(0).Item("vcComPhone"))
                    txtFax.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcComFax")), "", dtComInfo.Rows(0).Item("vcComFax"))

                    lblWebLink1.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLabel1")), "", dtComInfo.Rows(0).Item("vcWebLabel1"))
                    lblWebLink2.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLabel2")), "", dtComInfo.Rows(0).Item("vcWebLabel2"))
                    lblWebLink3.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLabel3")), "", dtComInfo.Rows(0).Item("vcWebLabel3"))
                    lblWebLink4.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWeblabel4")), "", dtComInfo.Rows(0).Item("vcWeblabel4"))
                    txtWebLink1.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLink1")), "", dtComInfo.Rows(0).Item("vcWebLink1"))
                    txtWebLink2.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLink2")), "", dtComInfo.Rows(0).Item("vcWebLink2"))
                    txtWebLink3.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLink3")), "", dtComInfo.Rows(0).Item("vcWebLink3"))
                    txtWebLink4.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLink4")), "", dtComInfo.Rows(0).Item("vcWebLink4"))

                    objProspects.DomainID = Session("DomainID")
                    dtCompanyTaxTypes = objProspects.GetCompanyTaxTypes
                    Dim dr As DataRow
                    dr = dtCompanyTaxTypes.NewRow
                    dr("numTaxItemID") = 0
                    dr("vcTaxName") = "Sales Tax(Default)"
                    dr("bitApplicable") = IIf(dtComInfo.Rows(0).Item("bitNoTax") = True, False, True)
                    dtCompanyTaxTypes.Rows.Add(dr)


                    chkTaxItems.DataTextField = "vcTaxName"
                    chkTaxItems.DataValueField = "numTaxItemID"
                    chkTaxItems.DataSource = dtCompanyTaxTypes
                    chkTaxItems.DataBind()
                    Dim i As Integer
                    For i = 0 To dtCompanyTaxTypes.Rows.Count - 1
                        If Not IsDBNull(dtCompanyTaxTypes.Rows(i).Item("bitApplicable")) Then
                            If dtCompanyTaxTypes.Rows(i).Item("bitApplicable") = True Then
                                chkTaxItems.Items.FindByValue(dtCompanyTaxTypes.Rows(i).Item("numTaxItemID")).Selected = True
                            Else
                                chkTaxItems.Items.FindByValue(dtCompanyTaxTypes.Rows(i).Item("numTaxItemID")).Selected = False
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Sub SaveTaxTypes()
            Try
                dtCompanyTaxTypes = New DataTable
                dtCompanyTaxTypes.Columns.Add("numTaxItemID")
                dtCompanyTaxTypes.Columns.Add("bitApplicable")
                Dim dr As DataRow
                Dim i As Integer
                For i = 0 To chkTaxItems.Items.Count - 1
                    If chkTaxItems.Items(i).Selected = True Then
                        dr = dtCompanyTaxTypes.NewRow
                        dr("numTaxItemID") = chkTaxItems.Items(i).Value
                        dr("bitApplicable") = 1
                        dtCompanyTaxTypes.Rows.Add(dr)
                    End If
                Next
                Dim ds As New DataSet
                Dim strdetails As String
                dtCompanyTaxTypes.TableName = "Table"
                ds.Tables.Add(dtCompanyTaxTypes)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))
                If objProspects Is Nothing Then
                    objProspects = New CProspects
                End If
                objProspects.DivisionID = lngDivID
                objProspects.strCompanyTaxTypes = strdetails
                objProspects.ManageCompanyTaxTypes()
                ds.Dispose()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                objCommon.DivisionID = lngDivID
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                SaveProspects(objCommon.CompID)
                SaveTaxTypes()
                sb_CompanyInfo()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                objCommon.DivisionID = lngDivID
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                SaveProspects(objCommon.CompID)
                SaveTaxTypes()
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If GetQueryStringVal(Request.QueryString("enc"), "frm1") = "ActionItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommId=" & GetQueryStringVal(Request.QueryString("enc"), "CommID") & "&frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm"))
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "ActItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommId=" & GetQueryStringVal(Request.QueryString("enc"), "CommID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "contactlist" Then
                    Response.Redirect("../contact/frmContactList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&ContactType=" & GetQueryStringVal(Request.QueryString("enc"), "ContactType"))
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "opportunitylist" Then
                    Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "tickler" Then
                    Response.Redirect("../common/frmTicklerDisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "Caselist" Then
                    Response.Redirect("../cases/frmCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "BestAccounts" Then
                    Response.Redirect("../reports/frmBestAccounts.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ContactRole" Then
                    Response.Redirect("../reports/frmContactRole.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CasesAgent" Then
                    Response.Redirect("../reports/frmCases.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "BizDocReport" Then
                    Response.Redirect("../reports/frmBizDocReport.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "Ecosystem" Then
                    Response.Redirect("../reports/frmEcosystemReport.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "BestPartners" Then
                    Response.Redirect("../reports/frmBestPartners.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "Portal" Then
                    Response.Redirect("../reports/frmSelfServicePortal.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "DealHistory" Then
                    Response.Redirect("../reports/frmRepDealHistory.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CampDetails" Then
                    Response.Redirect("../Marketing/frmCampaignDetails.aspx?CampID=" & GetQueryStringVal(Request.QueryString("enc"), "CampID") & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "oppdetail" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?opId=" & GetQueryStringVal(Request.QueryString("enc"), "opId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CaseDetail" Then
                    Response.Redirect("../cases/frmCases.aspx?CaseID=" & GetQueryStringVal(Request.QueryString("enc"), "CaseID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)

                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "contactdetail" Then
                    Response.Redirect("../contact/frmContacts.aspx?CntId=" & GetQueryStringVal(Request.QueryString("enc"), "CntID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CaseReport" Then
                    Response.Redirect("../reports/frmCases.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CompanyList" Then
                    Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & GetQueryStringVal(Request.QueryString("enc"), "RelID") & "&profileId=" & GetQueryStringVal(Request.QueryString("enc"), "profileId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AvgSalesCycle1" Then
                    Response.Redirect("../reports/frmAvgSalesCycle1.aspx")
               ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsReceivable" Then
                    Response.Redirect("../Accounting/frmAccountsReceivable.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsPayable" Then
                    Response.Redirect("../Accounting/frmAccountsPayable.aspx")
                Else : Response.Redirect("../prospects/frmProspectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&RelId=" & GetQueryStringVal(Request.QueryString("enc"), "RelID") & "&profileId=" & GetQueryStringVal(Request.QueryString("enc"), "profileid"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveProspects(ByVal CompID As Long)
            Try
                If objLeads Is Nothing Then objLeads = New CLeads
                With objLeads
                    .CompanyID = CompID
                    .CompanyName = txtRelationShipName.Text
                    .InfoSource = ddlInfoSource.SelectedItem.Value
                    .DomainID = Session("DomainID")
                    .TerritoryID = ddlTerriory.SelectedItem.Value
                    .CompanyType = ddlRelationhip.SelectedItem.Value
                    .PublicFlag = chkPrivate.Checked
                    .DivisionID = lngDivID
                    .CompanyRating = ddlRating.SelectedItem.Value
                    .AnnualRevenue = ddlAnnualRevenue.SelectedItem.Value
                    .CompanyIndustry = ddlIndustry.SelectedItem.Value
                    .AnnualRevenue = ddlAnnualRevenue.SelectedItem.Value
                    .CompanyCredit = ddlCreditLimit.SelectedItem.Value
                    .WebSite = txtWeb.Text
                    .NumOfEmp = ddlNoOfEmp.SelectedItem.Value
                    .Profile = ddlProfile.SelectedItem.Value
                    .FollowUpStatus = ddlFollow.SelectedItem.Value
                    .CampaignID = ddlCampaign.SelectedItem.Value
                    .Comments = txtComments.Text
                    .WebLink1 = txtWebLink1.Text
                    .WebLink2 = txtWebLink2.Text
                    .WebLink3 = txtWebLink3.Text
                    .WebLink4 = txtWebLink4.Text
                    .WebLabel1 = lblWebLink1.Text
                    .WebLabel2 = lblWebLink2.Text
                    .WebLabel3 = lblWebLink3.Text
                    .WebLabel4 = lblWebLink4.Text
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .CRMType = 1
                    .LeadBoxFlg = 1
                    .DivisionName = ""
                    ' txtDivision.Text()
                    .StatusID = ddlStatus.SelectedItem.Value
                    .BillingTerms = IIf(chkBillinTerms.Checked = True, 1, 0)
                    .BillingDays = IIf(txtNetDays.Text = "", 0, txtNetDays.Text)
                    .InterestType = IIf(radPlus.Checked = True, 1, 0)
                    .Interest = IIf(Replace(txtInterest.Text, ",", "") = "", 0, Replace(txtInterest.Text, ",", ""))
                    .ComPhone = txtPhone.Text.Trim
                    .ComFax = txtFax.Text.Trim
                    .AssignedTo = ddlAssignedTo.SelectedItem.Value
                    .NoTax = IIf(chkTaxItems.Items.FindByValue(0).Selected = True, False, True)
                End With
                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                objLeads.DivisionID = objLeads.ManageCompanyDivisionsInfo1
                SaveCusField()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                Dim strDate As String
                Dim bizCalendar As UserControl
                Dim _myUC_DueDate As PropertyInfo
                Dim PreviousRowID As Integer = 0
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim i, k As Integer
                Dim dtTable As DataTable
                'Tabstrip3.Items.Clear()
                Dim ObjCus As New CustomFields
                ObjCus.locId = 1
                ObjCus.RelId = ddlRelationhip.SelectedItem.Value
                ObjCus.RecordId = lngDivID
                ObjCus.DomainID = Session("DomainID")
                dtTable = ObjCus.GetCustFlds.Tables(0)
                Session("CusFields") = dtTable

                If uwOppTab.Tabs.Count > 2 Then
                    Dim iItemcount As Integer
                    iItemcount = uwOppTab.Tabs.Count
                    While uwOppTab.Tabs.Count > 2
                        uwOppTab.Tabs.RemoveAt(iItemcount - 1)
                        iItemcount = iItemcount - 1
                    End While
                End If

                If dtTable.Rows.Count > 0 Then
                    'Main Detail Section
                    k = 0
                    objRow = New HtmlTableRow
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") = 0 Then
                            If k = 3 Then
                                k = 0
                                tblDetails.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            objCell = New HtmlTableCell
                            objCell.Align = "Right"
                            objCell.Attributes.Add("class", "normal1")
                            If dtTable.Rows(i).Item("fld_type") <> "Link" Then
                                objCell.InnerText = dtTable.Rows(i).Item("fld_label")
                            Else : objCell.InnerText = ""
                            End If
                            objRow.Cells.Add(objCell)
                            If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                                objCell = New HtmlTableCell
                                CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                                objCell = New HtmlTableCell
                                CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                objCell = New HtmlTableCell
                                CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                                objCell = New HtmlTableCell
                                CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
                                PreviousRowID = i
                                objCell = New HtmlTableCell
                                bizCalendar = LoadControl("../include/calandar.ascx")
                                bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
                                objCell.Controls.Add(bizCalendar)
                                objRow.Cells.Add(objCell)
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
                                objCell = New HtmlTableCell
                                CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngDivID, dtTable.Rows(i).Item("fld_label"))
                            End If
                            k = k + 1
                        End If
                    Next
                    tblDetails.Rows.Add(objRow)

                    'CustomField Section
                    Dim Tab As Tab
                    'Dim pageView As PageView
                    Dim aspTable As HtmlTable
                    Dim Table As Table
                    Dim tblcell As TableCell
                    Dim tblRow As TableRow
                    k = 0
                    ViewState("TabId") = dtTable.Rows(0).Item("TabId")
                    ViewState("Check") = 0
                    ViewState("FirstTabCreated") = 0
                    ' Tabstrip3.Items.Clear()
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") <> 0 Then
                            If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                                If ViewState("Check") <> 0 Then
                                    aspTable.Rows.Add(objRow)
                                    tblcell.Controls.Add(aspTable)
                                    tblRow.Cells.Add(tblcell)
                                    Table.Rows.Add(tblRow)
                                    Tab.ContentPane.Controls.Add(Table)
                                End If
                                k = 0
                                ViewState("FirstTabCreated") = 1
                                ViewState("Check") = 1
                                '   If Not IsPostBack Then
                                ViewState("TabId") = dtTable.Rows(i).Item("TabId")
                                Tab = New Tab
                                Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"

                                uwOppTab.Tabs.Add(Tab)
                                'End If
                                'pageView = New PageView
                                aspTable = New HtmlTable
                                Table = New Table
                                Table.Width = Unit.Percentage(100)
                                Table.BorderColor = System.Drawing.Color.FromName("black")
                                Table.GridLines = GridLines.None
                                Table.BorderWidth = Unit.Pixel(1)
                                Table.Height = Unit.Pixel(300)
                                Table.CssClass = "aspTable"
                                tblcell = New TableCell
                                tblRow = New TableRow
                                tblcell.VerticalAlign = VerticalAlign.Top
                                aspTable.Width = "100%"
                                objRow = New HtmlTableRow
                                objCell = New HtmlTableCell
                                objCell.InnerHtml = "<br>"
                                objRow.Cells.Add(objCell)
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            'pageView.Controls.Add("")
                            If k = 3 Then
                                k = 0
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If
                            objCell = New HtmlTableCell
                            objCell.Align = "right"
                            objCell.Attributes.Add("class", "normal1")
                            If dtTable.Rows(i).Item("fld_type") <> "Link" Then
                                objCell.InnerText = dtTable.Rows(i).Item("fld_label")
                            End If
                            objRow.Cells.Add(objCell)
                            If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                                objCell = New HtmlTableCell
                                CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                                objCell = New HtmlTableCell
                                CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                objCell = New HtmlTableCell
                                CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                                objCell = New HtmlTableCell
                                CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
                                PreviousRowID = i
                                objCell = New HtmlTableCell
                                bizCalendar = LoadControl("../include/calandar.ascx")
                                bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
                                objCell.Controls.Add(bizCalendar)
                                objRow.Cells.Add(objCell)
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
                                objCell = New HtmlTableCell
                                CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngDivID, dtTable.Rows(i).Item("fld_label"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
                                objCell = New HtmlTableCell
                                Dim strFrame As String
                                Dim URL As String
                                URL = dtTable.Rows(i).Item("vcURL")
                                URL = URL.Replace("RecordID", lngDivID)
                                strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                objCell.Controls.Add(New LiteralControl(strFrame))
                                objRow.Cells.Add(objCell)
                            End If
                            k = k + 1
                        End If
                    Next
                    If ViewState("Check") = 1 Then
                        aspTable.Rows.Add(objRow)
                        tblcell.Controls.Add(aspTable)
                        tblRow.Cells.Add(tblcell)
                        Table.Rows.Add(tblRow)
                        Tab.ContentPane.Controls.Add(Table)
                    End If
                End If
                Dim dvCusFields As DataView
                dvCusFields = dtTable.DefaultView
                dvCusFields.RowFilter = "fld_type='Date Field'"
                Dim iViewCount As Integer
                For iViewCount = 0 To dvCusFields.Count - 1
                    If Not IsDBNull(dvCusFields(iViewCount).Item("Value")) Then
                        bizCalendar = uwOppTab.FindControl("cal" & dvCusFields(iViewCount).Item("fld_id"))
                        Dim _myControlType As Type = bizCalendar.GetType()
                        _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                        strDate = dvCusFields(iViewCount).Item("Value")
                        If strDate = "0" Then strDate = ""
                        If strDate <> "" Then
                            'strDate = DateFromFormattedDate(strDate, Session("DateFormat"))
                            _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveCusField()
            Try
                If Not Session("CusFields") Is Nothing Then
                    Dim dtTable As DataTable
                    Dim i As Integer
                    dtTable = Session("CusFields")
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                            Dim txt As TextBox
                            txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            dtTable.Rows(i).Item("Value") = txt.Text
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                            Dim ddl As DropDownList
                            ddl = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            dtTable.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                            Dim chk As CheckBox
                            chk = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            If chk.Checked = True Then
                                dtTable.Rows(i).Item("Value") = "1"
                            Else : dtTable.Rows(i).Item("Value") = "0"
                            End If
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                            Dim txt As TextBox
                            txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            dtTable.Rows(i).Item("Value") = txt.Text
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
                            Dim BizCalendar As UserControl
                            BizCalendar = uwOppTab.FindControl("cal" & dtTable.Rows(i).Item("fld_id"))
                            Dim strDueDate As String
                            Dim _myControlType As Type = BizCalendar.GetType()
                            Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                            strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
                            If strDueDate <> "" Then
                                dtTable.Rows(i).Item("Value") = strDueDate
                            Else : dtTable.Rows(i).Item("Value") = ""
                            End If
                        End If
                    Next

                    Dim ds As New DataSet
                    Dim strdetails As String
                    dtTable.TableName = "Table"
                    ds.Tables.Add(dtTable.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjCusfld As New CustomFields(Session("userid"))
                    ObjCusfld.strDetails = strdetails
                    ObjCusfld.locId = 1
                    ObjCusfld.RecordId = lngDivID
                    ObjCusfld.SaveCustomFldsByRecId()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to set the Association Information.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the imprints the association information on a screen label.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Sub LoadAssociationInformation()
            Try
                If objAssociationInfo Is Nothing Then objAssociationInfo = New OrgAssociations
                objAssociationInfo.DomainID = Session("DomainID")               'Set the Domain Id
                objAssociationInfo.DivisionID = lngDivID                        'Set the Division Id
                Dim dtAssociationInfo As DataTable                              'Declare a new DataTable
                dtAssociationInfo = objAssociationInfo.getParentOrgForCurrentOrg 'Get the Association Info
                If dtAssociationInfo.Rows.Count > 0 Then                        'Check if association exists where there is a parent-child relationship
                    lblAssociation.Text = dtAssociationInfo.Rows(0).Item("vcData") & " of: " & "<a href=""javascript: GoOrgDetails(" & dtAssociationInfo.Rows(0).Item("numDivisionId") & ");""><b>" & dtAssociationInfo.Rows(0).Item("vcCompanyName") & "</b></a>, " & dtAssociationInfo.Rows(0).Item("vcDivisionName")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                If objAccount Is Nothing Then objAccount = New CAccounts
                With objAccount
                    .DivisionID = lngDivID
                    .DomainID = Session("DomainID")
                End With
                If objAccount.DeleteOrg = False Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else
                    If ddlRelationhip.SelectedValue = "0" Or ddlRelationhip.SelectedValue = "46" Then
                        Response.Redirect("../prospects/frmProspectList.aspx")
                    Else : Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & ddlRelationhip.SelectedValue)
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlRelationhip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationhip.SelectedIndexChanged
            Try
                If ddlRelationhip.SelectedValue > 0 Then
                    uwOppTab.Tabs(0).Text = "&nbsp;&nbsp;" & ddlRelationhip.SelectedItem.Text & " Detail&nbsp;&nbsp;"
                End If
                LoadProfile()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rdbFinancialOverview_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbFinancialOverview.CheckedChanged
            Try
                If rdbFinancialOverview.Checked = True Then
                    PnlFinancialOverview.Visible = True
                    PnlTransaction.Visible = False
                Else
                    PnlTransaction.Visible = True
                    PnlFinancialOverview.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rdbTransaction_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbTransaction.CheckedChanged
            Try
                If rdbTransaction.Checked = True Then
                    PnlTransaction.Visible = True
                    LoadTransactionGrid()
                    PnlFinancialOverview.Visible = False
                Else
                    PnlFinancialOverview.Visible = True
                    PnlTransaction.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub LoadTransactionGrid()
            Try
                If objProspects Is Nothing Then objProspects = New CProspects
                Dim dtTransaction As DataTable
                objProspects.DomainID = Session("DomainId")
                objProspects.DivisionID = lngDivID
                dtTransaction = objProspects.GetTransactionDetails
                dgTransaction.DataSource = dtTransaction
                dgTransaction.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnDateTime(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlTerriory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerriory.SelectedIndexChanged
            Try
                Dim lngAssignTo As Long
                lngAssignTo = ddlAssignedTo.SelectedValue
                objCommon.sb_FillConEmpFromTerritories(ddlAssignedTo, Session("DomainID"), 1, 0, ddlTerriory.SelectedValue)
                If Not ddlAssignedTo.Items.FindByValue(lngAssignTo) Is Nothing Then
                    ddlAssignedTo.Items.FindByValue(lngAssignTo).Selected = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgTransaction_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTransaction.ItemCommand
            Try
                Dim lngID As Long
                lngID = e.Item.Cells(0).Text
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
