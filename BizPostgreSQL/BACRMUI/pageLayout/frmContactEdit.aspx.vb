Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.UltraWebTab.Tab
Imports System.Reflection

Namespace BACRM.UserInterface.Contacts
    Public Class frmContactEdit : Inherits BACRMPage

        Dim objContacts As CContacts
        Dim objCommon As New CCommon
        Dim ObjCus As CustomFields

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Dim strColumn As String
        Dim lngCntID As Long
        Dim m_aryRightsForCusFlds(), m_aryRightsForActItem(), m_aryRightsForEmail(), m_aryRightsForOpp(), m_aryRightsForPage(), m_aryRightsForAOI() As Integer
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                    SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                    SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
                Else : SI1 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
                Else : SI2 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                    frm = ""
                    frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
                Else : frm = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                    frm1 = ""
                    frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
                Else : frm1 = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    frm2 = ""
                    frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
                Else : frm2 = ""
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "SurveyRespondents" Or GetQueryStringVal(Request.QueryString("enc"), "frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    'Added by Debasish Nag on 6th Jan 2006
                    Dim objForm As Object                                   'The generic object
                    objForm = Page.FindControl("webmenu1")                  'Get a holder to the web control
                    objForm.visible = False                                 'Hide the web control
                    btnCancel.Attributes.Add("onclick", "javascript: window.close();return false;") 'Write code to close the window
                End If
                lngCntID = CInt(GetQueryStringVal(Request.QueryString("enc"), "CntId"))
                If Not IsPostBack Then
                    Session("Asc") = 1
                    If objContacts Is Nothing Then objContacts = New CContacts
                    objContacts.RecID = lngCntID
                    objContacts.Type = "U"
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.AddVisiteddetails()
                    Session("Help") = "Contacts"
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 3)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    Else
                        If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                            btnSave.Visible = False
                            btnSaveClose.Visible = False
                        End If
                        If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False
                    End If
                    sb_ShowAOIs()
                    LoadDropDowns()
                    LoadInformation()
                    Dim m_aryRightsForExptTooutLook() As Integer
                    m_aryRightsForExptTooutLook = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 10)
                    If m_aryRightsForExptTooutLook(RIGHTSTYPE.VIEW) = 0 Then btnExport.Visible = False
                    btnExport.Attributes.Add("onclick", "return Export()")
                    Dim dtTab As DataTable
                    dtTab = Session("DefaultTab")
                    If dtTab.Rows.Count > 0 Then
                        uwOppTab.Tabs(0).Text = IIf(IsDBNull(dtTab.Rows(0).Item("vcContact")), "Contact Details", dtTab.Rows(0).Item("vcContact").ToString & " Details")
                    Else : uwOppTab.Tabs(0).Text = "Contact Details"
                    End If
                End If

                DisplayDynamicFlds()
                If Not IsPostBack Then
                    If frm = "ContactDis" Then
                        If SI > 1 And SI <= 4 Then
                            SI = 0
                        ElseIf SI > 4 Then
                            SI = (SI - 4) + 1
                        End If
                        If uwOppTab.Tabs.Count >= SI Then uwOppTab.SelectedTabIndex = SI
                    End If
                    hplDocuments.Attributes.Add("onclick", "return OpenDocuments(" & lngCntID & ");")
                    hplECampaign.Attributes.Add("onclick", "return OpenECamp(" & lngCntID & ");")
                    hplECampHstr.Attributes.Add("onclick", "return OpenECampHstr(" & lngCntID & ");")
                    hplAddress.Attributes.Add("onclick", "return openAddress(" & lngCntID & ")")
                    btnGo.Attributes.Add("onclick", "return fn_SendMail('txtEmail'," & Session("CompWindow") & "," & lngCntID & ");")
                    btnAltEmail.Attributes.Add("onclick", "return fn_SendMail('txtAltEmail'," & Session("CompWindow") & "," & lngCntID & ");")
                    btnAssGo.Attributes.Add("onclick", "return fn_SendMail('txtAsstEmail'," & Session("CompWindow") & "," & 0 & ");")
                    btnExport.Attributes.Add("onclick", "return Export()")
                    If lngCntID = Session("AdminID") Then
                        btnActDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                    btnSave.Attributes.Add("onclick", "return Save()")
                    btnSaveClose.Attributes.Add("onclick", "return Save()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadInformation()
            Try
                Dim dtContactInfo As DataTable
                If objContacts Is Nothing Then objContacts = New CContacts
                objContacts.ContactID = lngCntID
                objContacts.DomainID = Session("DomainID")
                objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtContactInfo = objContacts.GetCntInfoForEdit1

                ''Time And Expense
                If dtContactInfo.Rows(0).Item("numUserID") > 0 Then
                    Dim m_aryRightsForTimeExp() As Integer
                    m_aryRightsForTimeExp = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 11)
                    If m_aryRightsForTimeExp(RIGHTSTYPE.VIEW) <> 0 Then
                        btnTimeExp.Visible = True
                        btnTimeExp.Attributes.Add("onclick", "return OpenTmeAndExp(" & lngCntID & ",'" & GetQueryStringVal(Request.QueryString("enc"), "frm") & "')")
                    End If
                End If

                hplCustomer.Text = dtContactInfo.Rows(0).Item("vcCompanyName")
                If dtContactInfo.Rows(0).Item("tintCRMType") = 0 Then
                   hplCustomer.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetail&DivID=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")

                ElseIf dtContactInfo.Rows(0).Item("tintCRMType") = 1 Then
                hplCustomer.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetail&DivID=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")

                ElseIf dtContactInfo.Rows(0).Item("tintCRMType") = 2 Then
                hplCustomer.NavigateUrl = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetail&klds+7kldf=fjk-las&DivId=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")
                End If
                lblRecordOwner.Text = dtContactInfo.Rows(0).Item("RecordOwner")
                lblCreatedBy.Text = dtContactInfo.Rows(0).Item("CreatedBy")
                lblModifiedBy.Text = dtContactInfo.Rows(0).Item("ModifiedBy")

                txtFirstname.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFirstName")), "", dtContactInfo.Rows(0).Item("vcFirstName"))  'display First Name
                txtLastName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcLastName")), "", dtContactInfo.Rows(0).Item("vcLastName")) ' Display Last Name
                txtPhone.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numPhone")), "", dtContactInfo.Rows(0).Item("numPhone"))  'display Phone
                txtEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcEmail")), "", dtContactInfo.Rows(0).Item("vcEmail"))   'Display Email
                If Not IsDBNull(dtContactInfo.Rows(0).Item("vcCategory")) Then
                    If Not ddlcategory.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCategory")) Is Nothing Then
                        ddlcategory.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCategory")).Selected = True
                    End If
                End If
                If Not IsDBNull(dtContactInfo.Rows(0).Item("vcPosition")) Then
                    If Not ddlPosition.Items.FindByValue(dtContactInfo.Rows(0).Item("vcPosition")) Is Nothing Then
                        ddlPosition.Items.FindByValue(dtContactInfo.Rows(0).Item("vcPosition")).Selected = True
                    End If
                End If

                If Not IsDBNull(dtContactInfo.Rows(0).Item("bintDOB")) Then
                    txtAg.Text = Date.Today.Year - Year(dtContactInfo.Rows(0).Item("bintDOB"))
                    cal.SelectedDate = dtContactInfo.Rows(0).Item("bintDOB")
                End If

                If IsDBNull(dtContactInfo.Rows(0).Item("txtNotes")) Then
                    txtComments.Text = ""   'Dispalay Comments
                Else : txtComments.Text = Server.HtmlDecode(dtContactInfo.Rows(0).Item("txtNotes"))   'Dispalay Comments
                End If

                txtExtension.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numPhoneExtension")), "", dtContactInfo.Rows(0).Item("numPhoneExtension")) 'Display Extension
                txtMobile.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numCell")), "", dtContactInfo.Rows(0).Item("numCell"))
                txtHome.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("NumHomePhone")), "", dtContactInfo.Rows(0).Item("NumHomePhone"))
                txtFax.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFax")), "", dtContactInfo.Rows(0).Item("vcFax"))
                If IsDBNull(dtContactInfo.Rows(0).Item("bitOptOut")) Then
                    optout.Checked = False
                Else
                    If dtContactInfo.Rows(0).Item("bitOptOut") = 0 Then
                        optout.Checked = False
                    Else : optout.Checked = True
                    End If
                End If
                hidCompName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCompanyName")), "", dtContactInfo.Rows(0).Item("vcCompanyName"))
                hidEml.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcGivenName")), "", dtContactInfo.Rows(0).Item("vcGivenName"))
                txtAsstFName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAsstFirstName")), "", dtContactInfo.Rows(0).Item("vcAsstFirstName"))
                txtAsstLName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAsstLastName")), "", dtContactInfo.Rows(0).Item("vcAsstLastName"))
                txtAsstPhone.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numAsstPhone")), "", dtContactInfo.Rows(0).Item("numAsstPhone"))
                txtAsstExt.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numAsstExtn")), "", dtContactInfo.Rows(0).Item("numAsstExtn"))
                txtAsstEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAsstEmail")), "", dtContactInfo.Rows(0).Item("vcAsstEmail"))
                If Not IsDBNull(dtContactInfo.Rows(0).Item("vcpStreet")) Then
                    lblStreet.Text = IIf(dtContactInfo.Rows(0).Item("vcpStreet") = "", "", dtContactInfo.Rows(0).Item("vcpStreet") & ",")
                End If
                If Not IsDBNull(dtContactInfo.Rows(0).Item("vcPCity")) Then
                    lblCity.Text = IIf(dtContactInfo.Rows(0).Item("vcPCity") = "", "", dtContactInfo.Rows(0).Item("vcPCity") & ",")
                End If
                If Not IsDBNull(dtContactInfo.Rows(0).Item("vcPPostalCode")) Then
                    lblPostal.Text = IIf(dtContactInfo.Rows(0).Item("vcPPostalCode") = "", "", dtContactInfo.Rows(0).Item("vcPPostalCode") & ",")
                End If
                lblState.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("State")), "", dtContactInfo.Rows(0).Item("State") & ",")
                lblCountry.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("Country")), "", dtContactInfo.Rows(0).Item("Country"))


                If Not IsDBNull(dtContactInfo.Rows(0).Item("charSex")) Then
                    If Not ddlSex.Items.FindByValue(dtContactInfo.Rows(0).Item("charSex")) Is Nothing Then
                        ddlSex.Items.FindByValue(dtContactInfo.Rows(0).Item("charSex")).Selected = True 'Show the contact type as selected
                    End If
                End If

                If Not IsDBNull(dtContactInfo.Rows(0).Item("numContactType")) Then
                    If Not ddlType.Items.FindByValue(dtContactInfo.Rows(0).Item("numContactType")) Is Nothing Then
                        ddlType.Items.FindByValue(dtContactInfo.Rows(0).Item("numContactType")).Selected = True 'Show the contact type as selected
                        txtContactType.Text = dtContactInfo.Rows(0).Item("numContactType")
                    Else : txtContactType.Text = 0
                    End If
                End If

                If Not IsDBNull(dtContactInfo.Rows(0).Item("numTeam")) Then
                    If Not ddlTeam.Items.FindByValue(dtContactInfo.Rows(0).Item("numTeam")) Is Nothing Then
                        ddlTeam.Items.FindByValue(dtContactInfo.Rows(0).Item("numTeam")).Selected = True 'Show the contact type as selected
                    End If
                End If

                If Not IsDBNull(dtContactInfo.Rows(0).Item("vcDepartment")) Then
                    If Not ddlDepartment.Items.FindByValue(dtContactInfo.Rows(0).Item("vcDepartment")) Is Nothing Then
                        ddlDepartment.Items.FindByValue(dtContactInfo.Rows(0).Item("vcDepartment")).Selected = True 'Show the contact type as selected
                    End If
                End If

                If Not IsDBNull(dtContactInfo.Rows(0).Item("numManagerID")) Then
                    If Not ddlManagers.Items.FindByValue(dtContactInfo.Rows(0).Item("numManagerID")) Is Nothing Then
                        ddlManagers.Items.FindByValue(dtContactInfo.Rows(0).Item("numManagerID")).Selected = True
                    End If
                End If

                txtTitle.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcTitle")), "", dtContactInfo.Rows(0).Item("vcTitle"))
                txtAltEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAltEmail")), "", dtContactInfo.Rows(0).Item("vcAltEmail"))
                'Added by Debasish Nag on 23rd Jan 2006 to incorporate Emp Status of the Contact
                If Not IsDBNull(dtContactInfo.Rows(0).Item("numEmpStatus")) Then
                    If Not ddlEmpStatus.Items.FindByValue(dtContactInfo.Rows(0).Item("numEmpStatus")) Is Nothing Then
                        ddlEmpStatus.Items.FindByValue(dtContactInfo.Rows(0).Item("numEmpStatus")).Selected = True 'Show the contact type as selected
                    End If
                End If
                lblStatus.Text = dtContactInfo.Rows(0).Item("CampStatus")
                lblLActivity.Text = dtContactInfo.Rows(0).Item("Activity")
                lblDocCount.Text = "(" & dtContactInfo.Rows(0).Item("DocumentCount") & ")"
                txtItemId.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcItemId")), "", dtContactInfo.Rows(0).Item("vcItemId"))
                txtChangeKey.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcChangeKey")), "", dtContactInfo.Rows(0).Item("vcChangeKey"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                txtContactType.Text = ddlType.SelectedItem.Value
                saveContactInfo()
                SaveCusField()
                SaveAOI()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub sb_ShowAOIs()
            Try
                m_aryRightsForAOI = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 4)
                If m_aryRightsForAOI(RIGHTSTYPE.VIEW) = 0 Then chkAOI.Visible = False
                Dim dtAOI As DataTable
                Dim i As Integer
                If objContacts Is Nothing Then objContacts = New CContacts
                objContacts.ContactID = lngCntID
                objContacts.DomainID = Session("DomainID")
                dtAOI = objContacts.GetAOIDetails
                chkAOI.DataSource = dtAOI
                chkAOI.DataTextField = "vcAOIName"
                chkAOI.DataValueField = "numAOIId"
                chkAOI.DataBind()
                For i = 0 To dtAOI.Rows.Count - 1
                    If Not IsDBNull(dtAOI.Rows(i).Item("Status")) Then
                        If dtAOI.Rows(i).Item("Status") = 1 Then
                            chkAOI.Items.FindByValue(dtAOI.Rows(i).Item("numAOIId")).Selected = True
                        Else : chkAOI.Items.FindByValue(dtAOI.Rows(i).Item("numAOIId")).Selected = False
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveAOI()
            Try
                Dim dtTable As New DataTable
                dtTable.Columns.Add("numAOIId")
                dtTable.Columns.Add("Status")
                Dim dr As DataRow
                Dim i As Integer
                For i = 0 To chkAOI.Items.Count - 1
                    If chkAOI.Items(i).Selected = True Then
                        dr = dtTable.NewRow
                        dr("numAOIId") = chkAOI.Items(i).Value
                        dr("Status") = 1
                        dtTable.Rows.Add(dr)
                    End If
                Next
                Dim ds As New DataSet
                Dim strdetails As String
                dtTable.TableName = "Table"
                ds.Tables.Add(dtTable)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))
                objContacts.strAOI = strdetails
                objContacts.ContactID = lngCntID
                objContacts.SaveAOI()
                dtTable.Dispose()
                ds.Dispose()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub saveContactInfo()
            Try
                If objContacts Is Nothing Then objContacts = New CContacts
                With objContacts
                    .ContactID = lngCntID
                    .Department = ddlDepartment.SelectedItem.Value
                    .FirstName = txtFirstname.Text
                    .LastName = txtLastName.Text
                    .Email = txtEmail.Text
                    .Sex = ddlSex.SelectedItem.Value
                    .Position = ddlPosition.SelectedItem.Value
                    .ContactPhone = txtPhone.Text
                    .ContactPhoneExt = txtExtension.Text
                    If (cal.SelectedDate <> "") Then .DOB = cal.SelectedDate
                    .Comments = txtComments.Text
                    .UserCntID = Session("UserContactID")
                    .Category = ddlcategory.SelectedItem.Value
                    .CellPhone = txtMobile.Text
                    .HomePhone = txtHome.Text
                    .Fax = txtFax.Text
                    .AssFirstName = txtAsstFName.Text
                    .AssLastName = txtAsstLName.Text
                    .AssPhone = txtAsstPhone.Text
                    .AssPhoneExt = txtAsstExt.Text
                    .AssEmail = txtAsstEmail.Text
                    .ContactType = ddlType.SelectedItem.Value
                    .OptOut = optout.Checked
                    .Manager = ddlManagers.SelectedItem.Value
                    .Team = ddlTeam.SelectedItem.Value
                    .EmpStatus = ddlEmpStatus.SelectedItem.Value
                    .Title = txtTitle.Text.Trim
                    .AltEmail = txtAltEmail.Text.Trim
                End With
                objContacts.ManageContactInfo()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Sub UpdateConatcDetailinOutlook()
        '    Try
        '        sb_UpdateAddContact(hidEml.Text, Replace(txtFirstname.Text, "'", "''"), Replace(txtLastName.Text, "'", "''"), txtPhone.Text, txtEmail.Text, lblStreet.Text, lblCity.Text, lblState.Text, lblPostal.Text, txtHome.Text, txtAsstPhone.Text, txtAsstFName.Text & " " & txtAsstLName.Text, txtFax.Text, lblCountry.Text, txtMobile.Text, lblCustomer.Text, lblweb.Text, IIf(ddlSex.SelectedIndex = 0, "", ddlSex.SelectedItem.Text))
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                saveContactInfo()
                SaveCusField()
                SaveAOI()
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "SurveyRespondents" Or GetQueryStringVal(Request.QueryString("enc"), "frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    litMessage.Text = "<script language='javascript'>window.close();</script>" 'Write code to close the window
                Else : PageRedirect()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadDropDowns()
            Try
                objCommon.ContactID = lngCntID
                objCommon.charModule = "C"
                objCommon.GetCompanySpecificValues1()
                objCommon.sb_FillManagerFromDBSel(ddlManagers, Session("DomainID"), lngCntID, objCommon.DivisionID)
                objCommon.sb_FillComboFromDBwithSel(ddlPosition, 41, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlType, 8, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlTeam, 35, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlDepartment, 19, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlcategory, 25, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlEmpStatus, 44, Session("DomainID"))  'Fill the Contacts Status drop down
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                m_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 9)
                If m_aryRightsForCusFlds(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                Dim strDate As String
                Dim bizCalendar As UserControl
                Dim _myUC_DueDate As PropertyInfo
                Dim PreviousRowID As Integer = 0
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim i, k, j As Integer
                Dim dtTable As DataTable
                Dim count As Integer = uwOppTab.Tabs.Count
                ' Tabstrip4.Items.Clear()
                If ObjCus Is Nothing Then ObjCus = New CustomFields

                ObjCus.locId = 4
                ObjCus.DomainID = Session("DomainID")
                ObjCus.RelId = ddlType.SelectedItem.Value
                ObjCus.RecordId = lngCntID
                dtTable = ObjCus.GetCustFlds.Tables(0)
                Session("CusFields") = dtTable

                If uwOppTab.Tabs.Count > 2 Then
                    Dim iItemcount As Integer
                    iItemcount = uwOppTab.Tabs.Count
                    While uwOppTab.Tabs.Count > 2
                        uwOppTab.Tabs.RemoveAt(iItemcount - 1)
                        iItemcount = iItemcount - 1
                    End While
                End If

                If dtTable.Rows.Count > 0 Then
                    'Main Detail Section
                    k = 0
                    objRow = New HtmlTableRow
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") = 0 Then
                            If k = 3 Then
                                k = 0
                                tblMain.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            objCell = New HtmlTableCell
                            objCell.Align = "Right"
                            objCell.Attributes.Add("class", "normal1")
                            If dtTable.Rows(i).Item("fld_type") <> "Link" Then
                                objCell.InnerText = dtTable.Rows(i).Item("fld_label")
                            Else : objCell.InnerText = ""
                            End If
                            objRow.Cells.Add(objCell)
                            If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                                objCell = New HtmlTableCell
                                CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                                objCell = New HtmlTableCell
                                CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                objCell = New HtmlTableCell
                                CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                                objCell = New HtmlTableCell
                                CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
                                PreviousRowID = i
                                objCell = New HtmlTableCell
                                bizCalendar = LoadControl("../include/calandar.ascx")
                                bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
                                objCell.Controls.Add(bizCalendar)
                                objRow.Cells.Add(objCell)
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
                                objCell = New HtmlTableCell
                                CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngCntID, dtTable.Rows(i).Item("fld_label"))
                            End If
                            k = k + 1
                        End If
                    Next
                    tblMain.Rows.Add(objRow)

                    'CustomField Section
                    Dim Tab As Infragistics.WebUI.UltraWebTab.Tab
                    'Dim pageView As PageView
                    Dim aspTable As HtmlTable
                    Dim Table As Table
                    Dim tblcell As TableCell
                    Dim tblRow As TableRow
                    Dim up As UpdatePanel
                    Dim apt As AsyncPostBackTrigger
                    k = 0
                    ViewState("TabId") = dtTable.Rows(0).Item("TabId")
                    ViewState("Check") = 0
                    ViewState("FirstTabCreated") = 0
                    ' Tabstrip3.Items.Clear()
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") <> 0 Then
                            If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                                If ViewState("Check") <> 0 Then
                                    aspTable.Rows.Add(objRow)
                                    tblcell.Controls.Add(aspTable)
                                    tblRow.Cells.Add(tblcell)
                                    Table.Rows.Add(tblRow)
                                    up = New UpdatePanel
                                    up.ChildrenAsTriggers = True
                                    up.UpdateMode = UpdatePanelUpdateMode.Conditional
                                    apt = New AsyncPostBackTrigger
                                    apt.ControlID = "btnSave"
                                    up.Triggers.Add(apt)
                                    apt = New AsyncPostBackTrigger
                                    apt.ControlID = "btnSaveClose"
                                    up.Triggers.Add(apt)
                                    up.ContentTemplateContainer.Controls.Add(Table)
                                    Tab.ContentPane.Controls.Add(up)
                                End If
                                k = 0
                                ViewState("FirstTabCreated") = 1
                                ViewState("Check") = 1
                                '   If Not IsPostBack Then
                                ViewState("TabId") = dtTable.Rows(i).Item("TabId")
                                Tab = New Infragistics.WebUI.UltraWebTab.Tab
                                Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"

                                uwOppTab.Tabs.Add(Tab)
                                'End If
                                'pageView = New PageView
                                aspTable = New HtmlTable
                                Table = New Table
                                Table.Width = Unit.Percentage(100)
                                Table.BorderColor = System.Drawing.Color.FromName("black")
                                Table.GridLines = GridLines.None
                                Table.BorderWidth = Unit.Pixel(1)
                                Table.Height = Unit.Pixel(300)
                                Table.CssClass = "aspTable"
                                tblcell = New TableCell
                                tblRow = New TableRow
                                tblcell.VerticalAlign = VerticalAlign.Top
                                aspTable.Width = "100%"
                                objRow = New HtmlTableRow
                                objCell = New HtmlTableCell
                                objCell.InnerHtml = "<br>"
                                objRow.Cells.Add(objCell)
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            'pageView.Controls.Add("")
                            If k = 3 Then
                                k = 0
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If
                            objCell = New HtmlTableCell
                            objCell.Align = "right"
                            objCell.Attributes.Add("class", "normal1")
                            If dtTable.Rows(i).Item("fld_type") <> "Link" Then
                                objCell.InnerText = dtTable.Rows(i).Item("fld_label")
                            End If
                            objRow.Cells.Add(objCell)
                            If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                                objCell = New HtmlTableCell
                                CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                                objCell = New HtmlTableCell
                                CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                objCell = New HtmlTableCell
                                CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                                objCell = New HtmlTableCell
                                CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
                                PreviousRowID = i
                                objCell = New HtmlTableCell
                                bizCalendar = LoadControl("../include/calandar.ascx")
                                bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
                                objCell.Controls.Add(bizCalendar)
                                objRow.Cells.Add(objCell)
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
                                objCell = New HtmlTableCell
                                Dim strFrame As String
                                Dim URL As String
                                URL = dtTable.Rows(i).Item("vcURL")
                                URL = URL.Replace("RecordID", lngCntID)
                                strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                objCell.Controls.Add(New LiteralControl(strFrame))
                                objRow.Cells.Add(objCell)
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
                                objCell = New HtmlTableCell
                                CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngCntID, dtTable.Rows(i).Item("fld_label"))
                            End If
                            k = k + 1
                        End If
                    Next
                    If ViewState("Check") = 1 Then
                        aspTable.Rows.Add(objRow)
                        tblcell.Controls.Add(aspTable)
                        tblRow.Cells.Add(tblcell)
                        Table.Rows.Add(tblRow)
                        up = New UpdatePanel
                        up.ChildrenAsTriggers = True
                        up.UpdateMode = UpdatePanelUpdateMode.Conditional
                        apt = New AsyncPostBackTrigger
                        apt.ControlID = "btnSave"
                        up.Triggers.Add(apt)
                        apt = New AsyncPostBackTrigger
                        apt.ControlID = "btnSaveClose"
                        up.Triggers.Add(apt)
                        up.ContentTemplateContainer.Controls.Add(Table)
                        Tab.ContentPane.Controls.Add(up)
                    End If
                End If

                Dim dvCusFields As DataView
                dvCusFields = dtTable.DefaultView
                dvCusFields.RowFilter = "fld_type='Date Field'"
                Dim iViewCount As Integer
                For iViewCount = 0 To dvCusFields.Count - 1
                    If Not IsDBNull(dvCusFields(iViewCount).Item("Value")) Then
                        bizCalendar = uwOppTab.FindControl("cal" & dvCusFields(iViewCount).Item("fld_id"))
                        Dim _myControlType As Type = bizCalendar.GetType()
                        _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                        strDate = dvCusFields(iViewCount).Item("Value")
                        If strDate = "0" Then strDate = ""
                        If strDate <> "" Then
                            'strDate = DateFromFormattedDate(strDate, Session("DateFormat"))
                            _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveCusField()
            Try
                Dim dtTable As DataTable
                Dim i As Integer
                If Not Session("CusFields") Is Nothing Then
                    dtTable = Session("CusFields")
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                            Dim txt As TextBox
                            txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            dtTable.Rows(i).Item("Value") = txt.Text
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                            Dim ddl As DropDownList
                            ddl = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            dtTable.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                            Dim chk As CheckBox
                            chk = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            If chk.Checked = True Then
                                dtTable.Rows(i).Item("Value") = "1"
                            Else : dtTable.Rows(i).Item("Value") = "0"
                            End If
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                            Dim txt As TextBox
                            txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                            dtTable.Rows(i).Item("Value") = txt.Text
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
                            Dim BizCalendar As UserControl
                            BizCalendar = uwOppTab.FindControl("cal" & dtTable.Rows(i).Item("fld_id"))

                            Dim strDueDate As String
                            Dim _myControlType As Type = BizCalendar.GetType()
                            Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                            strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
                            If strDueDate <> "" Then
                                dtTable.Rows(i).Item("Value") = strDueDate
                            Else : dtTable.Rows(i).Item("Value") = ""
                            End If
                        End If
                    Next

                    Dim ds As New DataSet
                    Dim strdetails As String
                    dtTable.TableName = "Table"
                    ds.Tables.Add(dtTable.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))
                    If ObjCus Is Nothing Then ObjCus = New CustomFields
                    ObjCus.strDetails = strdetails
                    ObjCus.locId = 4
                    ObjCus.RecordId = lngCntID
                    ObjCus.SaveCustomFldsByRecId()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "SurveyRespondents" Or GetQueryStringVal(Request.QueryString("enc"), "frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    litMessage.Text = "<script language='javascript'>window.close();</script>" 'Write code to close the window
                Else : PageRedirect()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If GetQueryStringVal(Request.QueryString("enc"), "frm1") = "ActionItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommID=" & GetQueryStringVal(Request.QueryString("enc"), "CommID") & "&frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm"))
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "ActItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommId=" & GetQueryStringVal(Request.QueryString("enc"), "CommID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "prospects" Then
                    objCommon.ContactID = lngCntID
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../prospects/frmProspects.aspx?DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "accounts" Then
                    objCommon.ContactID = lngCntID
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../account/frmAccounts.aspx?klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "tickler" Then
                    Response.Redirect("../common/frmticklerdisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "contactlist" Then
                    Response.Redirect("../contact/frmContactList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "prospectlist" Then
                    Response.Redirect("../prospects/frmProspectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "accountlist" Then
                    Response.Redirect("../account/frmAccountList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "LeadsList" Then
                    Response.Redirect("../Leads/frmLeadList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "Caselist" Then
                    Response.Redirect("../cases/frmCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ProjectList" Then
                    Response.Redirect("../projects/frmProjectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "tickler" Then
                    Response.Redirect("../common/frmTicklerDisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "search" Then
                    Response.Redirect("../common/searchdisplay.aspx?frm=advancedsearch&srchback=true&typ=" & GetQueryStringVal(Request.QueryString("enc"), "typ"))
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "opportunitylist" Then
                    Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ContactRole" Then
                    Response.Redirect("../reports/frmContactRole.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "DealHistory" Then
                    Response.Redirect("../reports/frmRepDealHistory.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CompanyList" Then
                    Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & GetQueryStringVal(Request.QueryString("enc"), "RelID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "LeadDetails" Then
                    objCommon.ContactID = lngCntID
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../Leads/frmLeads.aspx?DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsReceivable" Then
                    Response.Redirect("../Accounting/frmAccountsReceivable.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsPayable" Then
                    Response.Redirect("../Accounting/frmAccountsPayable.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                Else : Response.Redirect("../contact/frmContactList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                If objContacts Is Nothing Then objContacts = New CContacts
                objContacts.ContactID = lngCntID
                objContacts.DomainID = Session("DomainID")
                If objContacts.DelContact > 1 Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else : Response.Redirect("../contact/frmContactList.aspx")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
            Try
                Dim retun As Boolean
                If Trim(txtFirstname.Text) <> "" And Trim(txtLastName.Text) <> "" And Trim(txtEmail.Text) <> "" Then
                    Dim strOriginalHidEmlValue As String
                    strOriginalHidEmlValue = hidEml.Text
                    hidEml.Text = ""
                    retun = UpdateConatcDetailinOutlook()
                    hidEml.Text = strOriginalHidEmlValue
                End If
                If retun = True Then
                    litMessage.Text = "Your contact record was successfully exported to Outlook"
                Else : litMessage.Text = "Your contact record was NOT exported to Outlook"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function UpdateConatcDetailinOutlook() As Boolean
            Try
                If Session("ExchIntegration") = True Then
                    Dim dtTable As DataTable
                    If objContacts Is Nothing Then objContacts = New CContacts
                    objContacts.ContactID = lngCntID
                    objContacts.DomainID = Session("DomainID")
                    dtTable = objContacts.GetBillOrgorContAdd
                    If dtTable.Rows.Count > 0 Then
                        If txtItemId.Text.Length < 5 Then
                            Try
                                clsUpdateExchangeContact.fn_InsertOwacontact(lngCntID, Session("SiteType"), txtFirstname.Text, txtLastName.Text, hidCompName.Text, txtPhone.Text, txtEmail.Text, Session("UserEmail"), Session("ServerName"), hidEml.Text, IIf(ddlDepartment.SelectedIndex = 0, "", ddlDepartment.SelectedItem.Text), txtFax.Text, dtTable.Rows(0).Item("vcPStreet"), dtTable.Rows(0).Item("vcPCity"), dtTable.Rows(0).Item("PState"), dtTable.Rows(0).Item("PCountry"), dtTable.Rows(0).Item("vcWebSite"), dtTable.Rows(0).Item("vcPPostalCode"), dtTable.Rows(0).Item("vcBillstreet"), dtTable.Rows(0).Item("vcBillCity"), dtTable.Rows(0).Item("vcBilState"), dtTable.Rows(0).Item("BillCountry"), dtTable.Rows(0).Item("vcBillPostCode"), txtMobile.Text, txtHome.Text, txtAsstFName.Text, txtAsstPhone.Text, IIf(ddlPosition.SelectedIndex = 0, "", ddlPosition.SelectedItem.Text), cal.SelectedDate, txtComments.Text, IIf(ddlcategory.SelectedIndex = 0, "", ddlcategory.SelectedItem.Text))
                            Catch ex As Exception
                                Throw ex
                            End Try
                        Else
                            Try
                                clsUpdateExchangeContact.fn_UpdateOwacontact(lngCntID, Session("SiteType"), txtFirstname.Text, txtLastName.Text, hidCompName.Text, txtPhone.Text, txtEmail.Text, Session("UserEmail"), Session("ServerName"), hidEml.Text, IIf(ddlDepartment.SelectedIndex = 0, "", ddlDepartment.SelectedItem.Text), txtFax.Text, dtTable.Rows(0).Item("vcPStreet"), dtTable.Rows(0).Item("vcPCity"), dtTable.Rows(0).Item("PState"), dtTable.Rows(0).Item("PCountry"), dtTable.Rows(0).Item("vcWebSite"), dtTable.Rows(0).Item("vcPPostalCode"), dtTable.Rows(0).Item("vcBillstreet"), dtTable.Rows(0).Item("vcBillCity"), dtTable.Rows(0).Item("vcBilState"), dtTable.Rows(0).Item("BillCountry"), dtTable.Rows(0).Item("vcBillPostCode"), txtMobile.Text, txtHome.Text, txtAsstFName.Text, txtAsstPhone.Text, IIf(ddlPosition.SelectedIndex = 0, "", ddlPosition.SelectedItem.Text), cal.SelectedDate, txtComments.Text, IIf(ddlcategory.SelectedIndex = 0, "", ddlcategory.SelectedItem.Text), txtItemId.Text, txtChangeKey.Text)
                            Catch ex As Exception
                                Throw ex
                            End Try
                        End If
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace
