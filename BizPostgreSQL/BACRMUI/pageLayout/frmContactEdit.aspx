<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmContactEdit.aspx.vb"
    Inherits="BACRM.UserInterface.Contacts.frmContactEdit" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Contacts</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <link href="../css/lists.css" type="text/css" rel="STYLESHEET" />

    <script language="JavaScript" src="../javascript/date-picker.js" type="text/javascript"></script>

    <script language="javascript">

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function openAddress(CntID) {
            window.open("../contact/frmContactAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pqwRT=" + CntID, '', 'toolbar=no,titlebar=no,top=300,width=850,height=350,scrollbars=no,resizable=no')
            return false;
        }
        function OpenEmailMessage(a, b) {
            window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email=" + a + "&Date=" + b, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
            return false;
        }
        function OpenListView(a) {
            window.open("../admin/frmEmailUsers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ContID=" + a, '', 'width=650,height=350,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
            return false;
        }
        function OpenECamp(a) {
            window.open("../Marketing/frmConECamDtls.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pqwRT=" + a, '', 'width=500,height=300,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
            return false;
        }
        function OpenECampHstr(a) {
            window.open("../Marketing/frmConECampHstr.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pqwRT=" + a, '', 'width=650,height=350,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
            return false;
        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                window.location.reload(true);
                //return false;

            }

        }
        function ShowWindowAddress(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                for (i = 60; i < tblMain.all.length; i++) {
                    if (tblMain.all[i] != null) {
                        if (tblMain.all[i].type == 'select-one') {
                            tblMain.all[i].style.visibility = "hidden";
                        }
                    }

                }
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                for (i = 60; i < tblMain.all.length; i++) {
                    if (tblMain.all[i] != null) {
                        if (tblMain.all[i].type == 'select-one') {
                            tblMain.all[i].style.visibility = "visible";
                        }
                    }

                }
                return false;

            }

        }
        function ShowWindow1(Page, q, att, a) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                if (a == 1) {
                    return true;
                }
                else {
                    return false;
                }


            }

        }
        function fn_SendMail(txtMailAddr, a, b) {
            var field = document.getElementById('uwOppTab$_ctl0$' + txtMailAddr)
            //alert(field)
            if (field.value != '') {
                if (a == 1) {

                    window.open('mailto:' + field.value);
                }
                else if (a == 2) {
                    window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + field.value + '&pqwRT=' + b, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
                }

            }

            return false;
        }
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=C&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTo(a) {
            window.open("../admin/frmcomAssociationTo.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DivId=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenFrom(a) {
            window.open("../admin/frmCompanyAssociationFrom.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenLast10OpenAct(a) {
            window.open("../admin/DisplayActionItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&cntid=" + a + "&type=1", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenLst10ClosedAct(a) {
            window.open("../admin/DisplayActionItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&cntid=" + a + "&type=2", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function Disableddl() {
            if (Tabstrip2.selectedIndex == 0) {
                document.Form1.ddlOppStatus.style.display = "none";
            }
            else {
                document.Form1.ddlOppStatus.style.display = "";
            }
        }
        ////////////////////////////////////////////////////////////////
        /////////////////////////  SURVEY HISTORY //////////////////////
        ////////////////////////////////////////////////////////////////
        /*
        Purpose:	The processing required to view the Survey History Details
        Created By: Debasish Tapan Nag
        Parameter:	1) numSurId
        Return		1) None
        */
        function EditSurveyResult(numSurId, numRespondentId) {
            var sURL = 'frmContactsSurveyResponses.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numSurID=' + numSurId + '&numRespondentId=' + numRespondentId,
			hndSurveyResponsePopUpURL = window.open(sURL, '', 'toolbar=no,titlebar=no,left=190, top=350,width=800,height=270,scrollbars=yes,resizable=yes');
            hndSurveyResponsePopUpURL.focus();
        }
        function Save() {
            if (document.Form1.uwOppTab$_ctl0$txtFirstname.value == "") {
                alert("Enter First Name")
                document.Form1.uwOppTab$_ctl0$txtFirstname.focus();
                return false;
            }
            if (document.Form1.uwOppTab$_ctl0$txtLastName.value == "") {
                alert("Enter Last Name")
                document.Form1.uwOppTab$_ctl0$txtLastName.focus();
                return false;
            }
            if (document.Form1.txtContactType.value == 70) {
                if (document.Form1.uwOppTab$_ctl0$ddlType.value != 70) {
                    alert("You can't change the contact type 'Primary Contact' from this record, because there must be  one Primary Contact for every Organization record. Go to another contact record within the same Organization, and change its 'type' value to Primary Contact.")
                    document.Form1.uwOppTab$_ctl0$ddlType.focus();
                    return false;
                }
            }
        }
        function Export() {
            if (document.Form1.uwOppTab$_ctl0$txtFirstname.value == "") {
                alert("Enter First Name")
                document.Form1.uwOppTab$_ctl0$txtFirstname.focus();
                return false;
            }
            if (document.Form1.uwOppTab$_ctl0$txtLastName.value == "") {
                alert("Enter Last Name")
                document.Form1.uwOppTab$_ctl0$txtLastName.focus();
                return false;
            }
            if (document.Form1.uwOppTab$_ctl0$txtEmail.value == "") {
                alert("Enter Email")
                document.Form1.uwOppTab$_ctl0$txtEmail.focus();
                return false;
            }
        }
        function FillAddress(a) {
            if (document.all) {
                document.getElementById('uwOppTab__ctl0_lblStreet').innerText = a;
                document.getElementById('uwOppTab__ctl0_lblCity').innerText = "";
                document.getElementById('uwOppTab__ctl0_lblPostal').innerText = "";
                document.getElementById('uwOppTab__ctl0_lblState').innerText = "";
                document.getElementById('uwOppTab__ctl0_lblCountry').innerText = "";
            } else {
                document.getElementById('uwOppTab__ctl0_lblStreet').textContent = a;
                document.getElementById('uwOppTab__ctl0_lblCity').textContent = "";
                document.getElementById('uwOppTab__ctl0_lblPostal').textContent = "";
                document.getElementById('uwOppTab__ctl0_lblState').textContent = "";
                document.getElementById('uwOppTab__ctl0_lblCountry').textContent = "";
            }

            return false;
        }
        ////////////////////////////////////////////////////////////////
        /////////////////////////  MOVE CONTACTS  //////////////////////
        ////////////////////////////////////////////////////////////////
        /*
        Purpose:	Opens the Contact Merge wscreen in a popup
        Created By: Debasish Tapan Nag
        Parameter:	1) numContactId: The Contact Id
        Return		1) None
        */
        function OpenMergeCopyWindow(numContactId, frmScreen) {
            window.open("frmMoveContact.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" + frmScreen + '&numContactId=' + numContactId, 'MoveContacts', 'toolbar=no,titlebar=no,top=300,width=500,height=200,scrollbars=yes,resizable=yes')
        }
        function OpenTmeAndExp(a, b) {
            window.location.href = "../TimeAndExpense/frmEmpCal.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CntID=" + a + "&frm=" + b;
            return false;
        }

        ////////////////////////////////////////////////// new scripts added by Tarun////////////////////////////////////////////
        function GetSelectAllCheck() {
            var objCheck = document.getElementById('dgEmail__ctl1_selectAllOptions') // .net generated name is dgEmail__ctl1_selectAllOptions
            if (objCheck.checked == true) {
                SelectAllCheckBox();
                return true;
            }
            else {
                UnSelectAllCheckBox();
                return false;
            }
        }

        function SelectAllCheckBox() {
            var objAllCheck = document.getElementsByTagName('input');
            var totalCount = objAllCheck.length
            var i
            for (i = 0; i < totalCount; i++) {
                if (objAllCheck[i] != null && objAllCheck[i].type == 'checkbox') {
                    //if( objAllCheck[i].id.indexOf("listChecks") > = 0  ) 
                    {
                        objAllCheck[i].checked = true;
                    }
                }
            }
        }

        function UnSelectAllCheckBox() {
            var objAllCheck = document.getElementsByTagName('input');
            var totalCount = objAllCheck.length
            var i
            for (i = 0; i < totalCount; i++) {
                if (objAllCheck[i] != null && objAllCheck[i].type == 'checkbox') {
                    //if( objAllCheck[i].id.indexOf("listChecks") > = 0  ) 
                    {
                        objAllCheck[i].checked = false;
                    }
                }
            }
        }

        // use script below to Fire a pertiular button event just you need to pass 
        // event objet ------ we are using to identify key code 
        // Button Id that we have to click 
        function Click_Button(eventObject, buttonID) {
            var objButton = document.getElementById(buttonID);
            if (objButton != null && event.keyCode == 13) {
                objButton.click();
            }
        } // end function


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <table bordercolor="black" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <td class="tr1" align="center">
                                <b>Record Owner : </b>
                                <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                            </td>
                            <td class="td1" width="1" height="18">
                            </td>
                            <td class="tr1" align="center">
                                <b>Created By : </b>
                                <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                            </td>
                            <td class="td1" width="1" height="18">
                            </td>
                            <td class="tr1" align="center">
                                <b>Last Modified By : </b>
                                <asp:Label ID="lblModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                            </td>
                </tr>
            </table>
            </td> </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td class="normal1" align="center">
                                Organization : <u>
                                    <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink>
                            </td>
                            <td align="right" valign="top">
                                <asp:Button ID="btnTimeExp" runat="server" Visible="false" CssClass="button" Width="105"
                                    Text="Time & Expense"></asp:Button>
                                <asp:Button ID="btnExport" runat="server" CssClass="button" Width="115" Text="Export To Outlook">
                                </asp:Button>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
                                <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close">
                                </asp:Button>
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                                <asp:Button ID="btnActDelete" runat="server" CssClass="Delete" Text="X"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <igtab:UltraWebTab ImageDirectory="" ID="uwOppTab" runat="server" ThreeDEffect="True"
                        BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                        <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                            NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                            FillStyle="LeftMergedWithCenter"></RoundedImage>
                        <SelectedTabStyle Height="23px" ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white">
                        </HoverTabStyle>
                        <Tabs>
                            <igtab:Tab Text="&nbsp;&nbsp;Contact Details&nbsp;&nbsp;">
                                <ContentTemplate>
                                    <asp:Table ID="tblContacts" runat="server" BorderWidth="1" Width="100%" GridLines="None"
                                        BorderColor="black" CssClass="aspTableDTL" Height="300">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Top">
                                                <br>
                                                <table id="tblMain" width="100%" runat="server">
                                                    <tr>
                                                        <td rowspan="30" valign="top">
                                                            <img src="../images/Contact-32.gif" />
                                                        </td>
                                                        <td class="normal1" valign="middle" align="right">
                                                            First/Last Name <font color="red">*</font>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFirstname" runat="server" CssClass="signup" Width="90" TabIndex="1"></asp:TextBox>
                                                            <asp:TextBox ID="txtLastName" CssClass="signup" Width="90" runat="server" TabIndex="2"></asp:TextBox>
                                                        </td>
                                                        <td class="normal1" align="right">
                                                            Phone/Ext
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPhone" runat="server" CssClass="signup" TabIndex="12" Width="135"></asp:TextBox>
                                                            <asp:TextBox ID="txtExtension" runat="server" CssClass="signup" TabIndex="13" Width="45"></asp:TextBox>
                                                        </td>
                                                        <td class="normal1" valign="middle" align="right">
                                                            Position
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlPosition" CssClass="signup" TabIndex="19" runat="server"
                                                                Width="180">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">
                                                            Email
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtEmail" CssClass="signup" Width="145" runat="server" TabIndex="3"></asp:TextBox>
                                                            &nbsp;
                                                            <asp:Button ID="btnGo" runat="server" CssClass="button" Text="Go" Width="25" TabIndex="4">
                                                            </asp:Button>
                                                        </td>
                                                        <td class="normal1" align="right">
                                                            Cell
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtMobile" runat="server" Width="180" TabIndex="14" CssClass="signup"></asp:TextBox>
                                                        </td>
                                                        <td class="normal1" align="right">
                                                            Title
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTitle" runat="server" Width="180" TabIndex="20" CssClass="signup"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">
                                                            Alternate Email
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAltEmail" CssClass="signup" Width="145" runat="server" TabIndex="5"></asp:TextBox>
                                                            &nbsp;
                                                            <asp:Button ID="btnAltEmail" runat="server" CssClass="button" Text="Go" Width="25"
                                                                TabIndex="6"></asp:Button>
                                                        </td>
                                                        <td class="normal1" align="right">
                                                            Home
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtHome" runat="server" Width="180" TabIndex="15" CssClass="signup"></asp:TextBox>
                                                        </td>
                                                        <td class="normal1" align="right">
                                                            Department
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDepartment" Width="180" TabIndex="21" runat="server" CssClass="signup">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">
                                                            DOB
                                                        </td>
                                                        <td class="normal1">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <BizCalendar:Calendar ID="cal" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;Age
                                                                        <asp:TextBox ID="txtAg" runat="server" CssClass="signup" Width="30" TabIndex="9"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="normal1" align="right">
                                                            Fax
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFax" runat="server" Width="180" TabIndex="16" CssClass="signup"></asp:TextBox>
                                                        </td>
                                                        <td class="normal1" align="right">
                                                            Gender
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSex" CssClass="signup" runat="server" Width="180" TabIndex="22">
                                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                <asp:ListItem Value="M">Male</asp:ListItem>
                                                                <asp:ListItem Value="F">Female</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">
                                                            Contact Type
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlType" CssClass="signup" AutoPostBack="True" runat="server"
                                                                Width="180" TabIndex="10">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="normal1" align="right">
                                                            Category
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlcategory" Width="180" TabIndex="17" runat="server" CssClass="signup">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="normal1" align="right">
                                                            Opt-out
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:CheckBox ID="optout" runat="server" TabIndex="23"></asp:CheckBox>&nbsp;&nbsp;&nbsp;
                                                            Status:
                                                            <asp:DropDownList ID="ddlEmpStatus" CssClass="signup" TabIndex="24" runat="server"
                                                                Width="110">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right" valign="top">
                                                            Manager
                                                        </td>
                                                        <td valign="top">
                                                            <asp:DropDownList ID="ddlManagers" runat="server" Width="180" CssClass="signup" TabIndex="11">
                                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="normal1" align="right" valign="top">
                                                            Team
                                                        </td>
                                                        <td valign="top">
                                                            <asp:DropDownList ID="ddlTeam" runat="server" TabIndex="18" Width="180" CssClass="signup">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="normal1" colspan="2">
                                                            <asp:HyperLink NavigateUrl="#" ID="hplDocuments" runat="server" TabIndex="25" CssClass="hyperlink">
															<font color="#180073">Documents</font>&nbsp;</asp:HyperLink><asp:Label ID="lblDocCount"
                                                                runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">
                                                            <asp:HyperLink ID="hplAddress" runat="server" CssClass="hyperlink">Address</asp:HyperLink>
                                                        </td>
                                                        <td colspan="4" class="normal1">
                                                            <asp:Label ID="lblStreet" runat="server"></asp:Label>
                                                            <asp:Label ID="lblCity" runat="server"></asp:Label>
                                                            <asp:Label ID="lblPostal" runat="server"></asp:Label>
                                                            <asp:Label ID="lblState" runat="server"></asp:Label>
                                                            <asp:Label ID="lblCountry" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%">
                                                    <tr>
                                                        <td class="normal1" align="right">
                                                            Comments
                                                        </td>
                                                        <td colspan="5">
                                                            <asp:TextBox ID="txtComments" runat="server" Width="500" CssClass="signup" Height="50"
                                                                MaxLength="250" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" colspan="6">
                                                            <b>Assistant's Information</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">
                                                            First/Last Name
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAsstFName" CssClass="signup" Width="90" runat="server"></asp:TextBox>&nbsp;
                                                            <asp:TextBox ID="txtAsstLName" runat="server" Width="90" CssClass="signup"></asp:TextBox>
                                                        </td>
                                                        <td class="normal1" align="right">
                                                            Phone/Ext
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAsstPhone" CssClass="signup" Width="135" runat="server"></asp:TextBox>&nbsp;
                                                            <asp:TextBox ID="txtAsstExt" CssClass="signup" runat="server" Width="45px"></asp:TextBox>
                                                            <td class="normal1" align="right">
                                                                Email
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:TextBox ID="txtAsstEmail" runat="server" Width="145" CssClass="signup"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnAssGo" runat="server" CssClass="button" Text="Go" Width="25">
                                                                </asp:Button>
                                                            </td>
                                                    </tr>
                                                </table>
                                                <table width="50%">
                                                    <tr>
                                                        <td class="normal1">
                                                            <asp:HyperLink NavigateUrl="#" ID="hplECampaign" runat="server" CssClass="hyperlink">
															<font color="#180073"><b>Email Campaign</b></font></asp:HyperLink>
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:HyperLink NavigateUrl="#" ID="hplECampHstr" runat="server" CssClass="hyperlink">
															<font color="#180073"><b>Email Campaign Follow-up History</b></font></asp:HyperLink>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1">
                                                            <b>Status : </b>
                                                            <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                        </td>
                                                        <td class="normal1">
                                                            <b>Last Activity : </b>
                                                            <asp:Label ID="lblLActivity" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </igtab:Tab>
                            <igtab:Tab Text="&nbsp;&nbsp;Areas of Interest&nbsp;&nbsp;">
                                <ContentTemplate>
                                    <asp:Table ID="Table2" runat="server" BorderWidth="1" Width="100%" GridLines="None"
                                        BorderColor="black" CssClass="aspTable" Height="300">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Top" CssClass="normal1">
                                                <br>
                                                <asp:CheckBoxList ID="chkAOI" CellSpacing="20" runat="server" RepeatColumns="3" RepeatDirection="Vertical">
                                                </asp:CheckBoxList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </igtab:Tab>
                        </Tabs>
                    </igtab:UltraWebTab>
                </td>
            </tr>
            </table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="hidEml" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="hidCompName" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalRecords" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtContactType" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtItemId" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtChangeKey" Style="display: none" runat="server"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
