' Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebTab

Namespace BACRM.UserInterface.Leads
    Public Class frmLeadDTl : Inherits BACRMPage

        Dim objContacts As CContacts
        Dim objLeads As CLeads
        Dim objCommon As New CCommon
        Dim objItems As CItems
        Dim objAccount As CAccounts
        Dim objAssociationInfo As OrgAssociations
        Dim objPageLayout As CPageLayout
        Dim m_aryRightsForShowAOIs(), m_aryRightsForPromote(), m_aryRightsForCustFlds(), m_aryRightsForTransferOwner(), m_aryRightsForCorr() As Integer
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim lngDivID As Long
        Dim m_aryRightsForEmails(), m_aryRightsForActItem() As Integer
        Dim strColumn As String

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub




        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Response.Redirect("../Leads/frmLeads.aspx?" & GetQueryStringVal(Request.QueryString("enc"), "", True))


                If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                    SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                    SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
                Else : SI1 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
                Else : SI2 = 0
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                    frm = ""
                    frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
                Else : frm = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                    frm1 = ""
                    frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
                Else : frm1 = ""
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                    frm2 = ""
                    frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
                Else : frm2 = ""
                End If

                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "SurveyRespondents" Or GetQueryStringVal(Request.QueryString("enc"), "frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    'Added by Debasish Nag on 6th Jan 2006
                    Dim objForm As Object                                   'The generic object
                    objForm = Page.FindControl("webmenu1")                  'Get a holder to the web control
                    objForm.visible = False                                 'Hide the web control
                    btnCancel.Attributes.Add("onclick", "javascript: window.close();return false;") 'Write code to close the window
                End If
                lngDivID = GetQueryStringVal(Request.QueryString("enc"), "DivID")

                If Not IsPostBack Then
                    If objContacts Is Nothing Then objContacts = New CContacts
                    objContacts.RecID = lngDivID
                    objContacts.Type = "C"
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.AddVisiteddetails()
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.DivisionID = lngDivID
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()
                    LoadAssociationInformation()                            'Added by Debasish for displayign the Association information

                    Session("Help") = "Organization"
                    '  txtCurrrentPage.Text = 1
                    Calendar1.SelectedDate = Session("StartDate")
                    Calendar2.SelectedDate = Session("EndDate")
                    txtContId.Text = CStr(objCommon.ContactID)
                    Dim m_aryRightsForPage() As Integer
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmLeads.aspx", Session("userID"), 2, 3)
                    ''''call function for sub-tab management  - added on 30jul09 by Mohan
                    'objCommon.ManageSubTabs(uwOppTab, Session("DomainID"), 2)
                    '''''
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False
                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnEdit.Visible = False
                    m_aryRightsForPromote = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmLeads.aspx", Session("userID"), 2, 7)
                    If m_aryRightsForPromote(RIGHTSTYPE.VIEW) = 0 Then btnPromote.Visible = False
                    m_aryRightsForTransferOwner = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmLeads.aspx", Session("userID"), 2, 9)
                    If m_aryRightsForTransferOwner(RIGHTSTYPE.VIEW) = 0 Then btnTransfer.Visible = False
                    m_aryRightsForActItem = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmLeads.aspx", Session("userID"), 2, 10)
                    If m_aryRightsForActItem(RIGHTSTYPE.VIEW) = 0 Then btnActionItem.Visible = False
                    If uwOppTab.Tabs.Count > SI Then uwOppTab.SelectedTabIndex = SI
                    LoadTabDetails()
                    btnLayout.Attributes.Add("onclick", "return ShowLayout('L','" & lngDivID & "','" & type.Text & "');")
                    btnTransfer.Attributes.Add("onclick", "return OpenTransfer('" & "../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects&rtyWR=" & lngDivID & "')")

                    If lngDivID = Session("UserDivisionID") Or lngDivID = Session("AdminID") Then
                        btnActDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                    If lngDivID = Session("UserDivisionID") Then
                        btnActDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadAssets()
            Try
                If objItems Is Nothing Then objItems = New CItems
                Dim dtItems As DataTable
                objItems.DivisionID = lngDivID
                objItems.DomainID = Session("DomainId")
                dtItems = objItems.getCompanyAssets()
                uwItem.DataSource = dtItems
                uwItem.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadTableInformation()
            Try
                Dim dtTableInfo As DataTable
                Dim dttablecust As DataTable
                Dim ds As New DataSet
                If objPageLayout Is Nothing Then objPageLayout = New CPageLayout
                Dim fields() As String
                Dim idcolumn As String = ""
                Dim dtLeadInfo As DataTable
                objPageLayout.ContactID = CInt(txtContId.Text)
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtLeadInfo = objPageLayout.GetLeadDtls
                type.Text = IIf(IsDBNull(dtLeadInfo.Rows(0).Item("numCmptype")), 0, dtLeadInfo.Rows(0).Item("numCmptype"))
                objPageLayout.CoType = "L"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngDivID
                'objPageLayout.ContactID = lngDivID
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 1
                objPageLayout.numRelCntType = CInt(type.Text)
                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)
                'If ds.Tables.Count = 2 Then
                '    dttablecust = ds.Tables(1)
                '    dtTableInfo.Merge(dttablecust)
                'End If

                'Dim dv As DataView
                'dv = New DataView(dtTableInfo)
                lblCustID.Text = lngDivID

                txtEmail.Text = IIf(IsDBNull(dtLeadInfo.Rows(0).Item("vcEmail")), "", dtLeadInfo.Rows(0).Item("vcEmail"))
                lblRecordOwner.Text = IIf(IsDBNull(dtLeadInfo.Rows(0).Item("vcRecordOwner")), "", dtLeadInfo.Rows(0).Item("vcRecordOwner"))
                lblCreatedBy.Text = IIf(IsDBNull(dtLeadInfo.Rows(0).Item("vcCreatedBy")), "", dtLeadInfo.Rows(0).Item("vcCreatedBy"))
                lblLastModifiedBy.Text = IIf(IsDBNull(dtLeadInfo.Rows(0).Item("vcModifiedBy")), "", dtLeadInfo.Rows(0).Item("vcModifiedBy"))

                Session("grpID") = IIf(IsDBNull(dtLeadInfo.Rows(0).Item("grpid")), "", dtLeadInfo.Rows(0).Item("grpid"))
                Dim grpid As Integer = IIf(IsDBNull(dtLeadInfo.Rows(0).Item("grpid")), "0", dtLeadInfo.Rows(0).Item("grpid"))
                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")

                If dtTab.Rows.Count > 0 Then
                    If grpid = 5 Then
                        uwOppTab.Tabs(0).Text = "My " & IIf(IsDBNull(dtTab.Rows(0).Item("vcLead")), "Lead Details", dtTab.Rows(0).Item("vcLead").ToString & " Details")
                    ElseIf grpid = 1 Then
                        uwOppTab.Tabs(0).Text = "Web " & IIf(IsDBNull(dtTab.Rows(0).Item("vcLead")), "Lead Details", dtTab.Rows(0).Item("vcLead").ToString & " Details")
                    ElseIf grpid = 2 Then
                        uwOppTab.Tabs(0).Text = "Public" & IIf(IsDBNull(dtTab.Rows(0).Item("vcLead")), "Lead Details", dtTab.Rows(0).Item("vcLead").ToString & " Details")
                    ElseIf grpid = 0 Then
                        uwOppTab.Tabs(0).Text = "Lead Details"
                    End If
                Else : uwOppTab.Tabs(0).Text = "Lead Details"
                End If

                Dim numrows As Integer = dtTableInfo.Compute("Max(tintrow)", String.Empty)
                Dim numcells As Integer = dtTableInfo.Compute("Max(intcoulmn)", String.Empty)
                Dim i As Integer = 0
                Dim nr As Integer
                Dim noRowsToLoop As Integer
                noRowsToLoop = (dtTableInfo.Rows.Count - (numrows * numcells)) / numcells
                noRowsToLoop = noRowsToLoop + numrows + 1

                For nr = 0 To noRowsToLoop
                    Dim r As New TableRow()
                    Dim nc As Integer
                    Dim ro As Integer = nr
                    For nc = 1 To numcells
                        If dtTableInfo.Rows.Count <> i Then
                            If dtTableInfo.Rows(i).Item("tintrow") = nr + 1 And dtTableInfo.Rows(i).Item("intcoulmn") = nc Then
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                Dim fieldId As Integer
                                Dim bitDynFld As String
                                fieldId = CInt(dtTableInfo.Rows(i).Item("numFieldID").ToString)
                                bitDynFld = dtTableInfo.Rows(i).Item("bitCustomField")
                                column1.CssClass = "normal7"
                                If (bitDynFld <> "1") Then
                                    If (fieldId = "83") Then
                                        Dim hpl As New HyperLink
                                        hpl.ID = "hplContact"
                                        hpl.Text = "Contact Details"
                                        hpl.NavigateUrl = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadDTL&CntId=" & CInt(txtContId.Text)


                                        'Dim button As New LinkButton

                                        'button.ID = "btncnt"
                                        'button.Text = "Contact Details"
                                        ''button.CssClass = "normal7"
                                        'AddHandler button.Click, AddressOf lnkButtonshow
                                        Dim l As New Label
                                        l.CssClass = "normal7"
                                        l.Text = "&nbsp;:"
                                        column1.Controls.Add(hpl)
                                        column1.Controls.Add(l)
                                    ElseIf (fieldId = "254") Then
                                        Dim h As New HyperLink
                                        h.NavigateUrl = "#"
                                        h.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                        h.Attributes.Add("onclick", "openFollow(" & lngDivID & ")")
                                        column1.Controls.Add(h)
                                    ElseIf (fieldId = "76") Then
                                        Dim h As New HyperLink
                                        h.NavigateUrl = "#"
                                        h.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
                                        h.Attributes.Add("onclick", "OpenAdd(" & lngDivID & ")")
                                        column1.Controls.Add(h)
                                    Else : column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName").ToString) & "&nbsp;:"
                                    End If

                                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName").ToString) And dtTableInfo.Rows(i).Item("vcDBColumnName").ToString() <> "" Then
                                        Dim temp As String
                                        temp = dtTableInfo.Rows(i).Item("vcDBColumnName").ToString
                                        fields = temp.Split(",")
                                        Dim j As Integer
                                        j = 0

                                        While (j < fields.Length)
                                            If (fieldId = "87") Then
                                                Dim h As New HyperLink
                                                h.CssClass = "hyperlink"
                                                'h.NavigateUrl = "http://" & IIf(IsDBNull(dtLeadInfo.Rows(0).Item(fields(j))), "", dtLeadInfo.Rows(0).Item(fields(j)))
                                                h.Text = IIf(IsDBNull(dtLeadInfo.Rows(0).Item(fields(j))), "", dtLeadInfo.Rows(0).Item(fields(j)))
                                                h.Attributes.Add("onclick", "return fn_GoToURL('" & h.Text & "');")
                                                column2.Controls.Add(h)
                                            ElseIf (fieldId = "80") Then
                                                Dim email As String = ""
                                                Dim h As New HyperLink
                                                'h.NavigateUrl = "#"
                                                h.CssClass = "hyperlink"
                                                h.ID = "email" & fieldId
                                                email = IIf(IsDBNull(dtLeadInfo.Rows(0).Item(fields(j))), "", dtLeadInfo.Rows(0).Item(fields(j)))
                                                h.Text = email
                                                h.Attributes.Add("onclick", "return fn_Mail('" & email & "','" & Session("CompWindow") & "','" & lngDivID & "');")
                                                column2.Controls.Add(h)
                                            ElseIf (fieldId = "83" Or fieldId = "254") Then
                                                column2.Text = ""
                                            ElseIf (fieldId = "76") Then
                                                Dim l As New Label
                                                l.Text = IIf(IsDBNull(dtLeadInfo.Rows(0).Item(fields(j))), "", dtLeadInfo.Rows(0).Item(fields(j)))
                                                l.ID = "lblAddress"
                                                column2.Controls.Add(l)
                                            ElseIf (fieldId = "89") Then
                                                'For Each Item As ListItem In ddlGroup.Items
                                                If IIf(IsDBNull(dtLeadInfo.Rows(0).Item(fields(j))), "", dtLeadInfo.Rows(0).Item(fields(j))) = "My Leads" Then
                                                    column2.Text = "My " & dtTab.Rows(0).Item("vcLead") & "s"
                                                ElseIf IIf(IsDBNull(dtLeadInfo.Rows(0).Item(fields(j))), "", dtLeadInfo.Rows(0).Item(fields(j))) = "PublicLeads" Then
                                                    column2.Text = "Public " & dtTab.Rows(0).Item("vcLead") & "s"
                                                ElseIf IIf(IsDBNull(dtLeadInfo.Rows(0).Item(fields(j))), "", dtLeadInfo.Rows(0).Item(fields(j))) = "WebLeads" Then
                                                    column2.Text = "Web " & dtTab.Rows(0).Item("vcLead") & "s"
                                                End If
                                                '  Next
                                            Else : column2.Text = IIf(IsDBNull(dtLeadInfo.Rows(0).Item(fields(j))), "", dtLeadInfo.Rows(0).Item(fields(j)))
                                            End If
                                            j += 1
                                        End While
                                    Else : column1.Text = ""
                                    End If ' end of table cell2
                                Else
                                    If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then
                                        If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                            column1.Text = "Custom Web Link :"
                                            Dim h As New HyperLink()
                                            h.CssClass = "hyperlink"
                                            Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                            url = url.Replace("RecordID", lngDivID)
                                            h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                            h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                            column2.Controls.Add(h)
                                        ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                            column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                            Dim strDate As String
                                            strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                            If strDate = "0" Then strDate = ""
                                            If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                        Else
                                            column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                            If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                                If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                    Dim l As New Label
                                                    l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                                    l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                    column2.Controls.Add(l)
                                                End If
                                            Else
                                                If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                    column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                                column2.CssClass = "normal1"
                                column1.HorizontalAlign = HorizontalAlign.Right
                                column2.HorizontalAlign = HorizontalAlign.Left
                                column1.Width = 250
                                column2.Width = 300
                                column2.ColumnSpan = 1
                                column1.ColumnSpan = 1
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                                i = i + 1
                            ElseIf dtTableInfo.Rows(i).Item("tintrow") = 0 And dtTableInfo.Rows(i).Item("intcoulmn") = 0 Then
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                column1.CssClass = "normal7"
                                If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then
                                    If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
                                        column1.Text = "Custom Web Link :"
                                        Dim h As New HyperLink()
                                        h.CssClass = "hyperlink"
                                        Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
                                        url = url.Replace("RecordID", lngDivID)
                                        h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName"))
                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                        column2.Controls.Add(h)
                                    ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
                                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                        Dim strDate As String
                                        strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                        If strDate = "0" Then strDate = ""
                                        If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                                    Else
                                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
                                        If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
                                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                Dim l As New Label
                                                l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
                                                l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                column2.Controls.Add(l)
                                            End If
                                        Else
                                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
                                                column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
                                            End If
                                        End If
                                    End If
                                End If
                                column2.CssClass = "normal1"
                                column1.HorizontalAlign = HorizontalAlign.Right
                                column2.HorizontalAlign = HorizontalAlign.Left
                                column1.Width = 250
                                column2.Width = 300
                                column2.ColumnSpan = 1
                                column1.ColumnSpan = 1
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                                i += 1
                            Else
                                Dim column1 As New TableCell
                                Dim column2 As New TableCell
                                column1.Text = ""
                                r.Cells.Add(column1)
                                r.Cells.Add(column2)
                            End If
                        End If
                    Next nc
                    tabledetail.Rows.Add(r)
                Next nr
                If (dtLeadInfo.Rows.Count > 0) Then
                    Dim column1 As New TableCell
                    Dim column2 As New TableCell
                    Dim r As New TableRow
                    column1.CssClass = "normal7"
                    column2.CssClass = "normal1"
                    column1.HorizontalAlign = HorizontalAlign.Right
                    column2.HorizontalAlign = HorizontalAlign.Justify
                    column2.ColumnSpan = 5
                    Dim l As New Label
                    l.CssClass = "normal7"
                    l.Text = "Comments" & "&nbsp;:"
                    column1.Controls.Add(l)
                    column2.Text = IIf(IsDBNull(dtLeadInfo.Rows(0).Item("txtComments")), "-", dtLeadInfo.Rows(0).Item("txtComments"))
                    column1.Width = 150
                    r.Cells.Add(column1)
                    r.Cells.Add(column2)
                    tableComment.Rows.Add(r)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub sb_ShowAOIs(ByVal lngContactID As Long)
            Try
                m_aryRightsForShowAOIs = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmLeads.aspx", Session("userID"), 2, 4)
                If m_aryRightsForShowAOIs(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                Dim dtAOI As DataTable
                Dim objRow As TableRow
                Dim objCell As TableCell
                Dim objCell1 As TableCell
                Dim objCell2 As TableCell
                Dim chk As CheckBox
                Dim i As Integer
                If objContacts Is Nothing Then objContacts = New CContacts
                objContacts.ContactID = lngContactID
                objContacts.DomainID = Session("DomainID")
                dtAOI = objContacts.GetAOIDetails
                Session("AOIForContact") = dtAOI
                Dim cellCount As Integer
                For i = 0 To dtAOI.Rows.Count - 1
                    objRow = New TableRow
                    objCell1 = New TableCell
                    objCell2 = New TableCell
                    objCell1.Text = IIf(IsDBNull(dtAOI.Rows(i).Item("vcAOIName")), "-", dtAOI.Rows(i).Item("vcAOIName"))
                    If Not IsDBNull(dtAOI.Rows(i).Item("Status")) Then
                        Dim l As New Label
                        l.CssClass = "cell"
                        l.Text = IIf(dtAOI.Rows(i).Item("Status") = 0, "", "a")
                        l.CssClass = IIf(dtAOI.Rows(i).Item("Status") = 0, "cell", "cell1")
                        objCell2.Controls.Add(l)
                    End If
                    objCell1.CssClass = "normal7"

                    objCell1.Width = 400
                    objRow.Cells.Add(objCell1)
                    objRow.Cells.Add(objCell2)
                    tblAOI.Rows.Add(objRow)
                Next
                If cellCount > 0 Then tblAOI.Rows.Add(objRow)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If GetQueryStringVal(Request.QueryString("enc"), "frm1") = "ActionItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommId=" & GetQueryStringVal(Request.QueryString("enc"), "CommId") & "&frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm"))
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "tickler" Then
                    Response.Redirect("../common/frmTicklerDisplay.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ActItem" Then
                    Response.Redirect("../admin/actionitemdetails.aspx?CommId=" & GetQueryStringVal(Request.QueryString("enc"), "CommID"))
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "admin" Then
                    Response.Redirect("../admin/transferleads.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "search" Then
                    Response.Redirect("../common/searchdisplay.aspx?frm=advancedsearch&srchback=true&typ=" & GetQueryStringVal(Request.QueryString("enc"), "typ"))
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CaseReport" Then
                    Response.Redirect("../reports/frmCases.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ProDetail" Then
                    Response.Redirect("../projects/frmProjects.aspx?frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm1") & "&ProId=" & GetQueryStringVal(Request.QueryString("enc"), "ProId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)

                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "contactdetail" Then
                    Response.Redirect("../contact/frmContacts.aspx?frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm1") & "&fdas89iu=098jfd&CntId=" & GetQueryStringVal(Request.QueryString("enc"), "CntID"))
               ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "LeadActivity" Then
                    Response.Redirect("../reports/frmLeadActivity.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "oppdetail" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?frm=" & GetQueryStringVal(Request.QueryString("enc"), "frm1") & "&opId=" & GetQueryStringVal(Request.QueryString("enc"), "opId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)

                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AdvSearch" Then
                    Response.Redirect("../admin/frmAdvancedSearchRes.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsReceivable" Then
                    Response.Redirect("../Accounting/frmAccountsReceivable.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AccountsPayable" Then
                    Response.Redirect("../Accounting/frmAccountsPayable.aspx")
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CompanyList" Then
                    Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & GetQueryStringVal(Request.QueryString("enc"), "RelID") & "&profileId=" & GetQueryStringVal(Request.QueryString("enc"), "profileId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "AdvSearchSur" Then
                    Response.Redirect("../Admin/FrmAdvSurveyRes.aspx")
                Else : Response.Redirect("../Leads/frmLeadList.aspx")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnPromote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPromote.Click
            Try
                Dim dtContactInfo As DataTable
                Dim strCompName As String
                If objLeads Is Nothing Then objLeads = New CLeads
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.DivisionID = lngDivID
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                objLeads.UserCntID = Session("UserContactID")
                objLeads.CompanyID = objCommon.CompID
                objLeads.ContactID = CInt(txtContId.Text)
                objLeads.DivisionID = lngDivID
                objLeads.PromoteLead()
                strCompName = objLeads.GetCompanyName
                If objContacts Is Nothing Then objContacts = New CContacts
                dtContactInfo = objLeads.GetContactInfoPromote
                If dtContactInfo.Rows.Count > 0 Then
                    Try
                        ' clsUpdateExchangeContact.fn_InsertOwacontact(Session("SiteType"), IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFirstName")), "", dtContactInfo.Rows(0).Item("vcFirstName")), IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcLastName")), "", dtContactInfo.Rows(0).Item("vcLastName")), strCompName, IIf(IsDBNull(dtContactInfo.Rows(0).Item("numPhone")), "", dtContactInfo.Rows(0).Item("numPhone")), IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcEmail")), "", dtContactInfo.Rows(0).Item("vcEmail")), Session("UserEmail"), Session("ServerName"), IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFirstName")), "", dtContactInfo.Rows(0).Item("vcFirstName")), IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcDepartment")), "", dtContactInfo.Rows(0).Item("vcDepartment")), IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFax")), "", dtContactInfo.Rows(0).Item("vcFax")), IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcStreet")), "", dtContactInfo.Rows(0).Item("vcStreet")), IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCity")), "", dtContactInfo.Rows(0).Item("vcCity")), IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcState")), "", dtContactInfo.Rows(0).Item("vcState")), IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCountry")), "", dtContactInfo.Rows(0).Item("vcCountry")), "", IIf(IsDBNull(dtContactInfo.Rows(0).Item("intPostalCode")), 0, dtContactInfo.Rows(0).Item("intPostalCode")))
                    Catch ex As Exception
                    End Try
                End If
                Response.Redirect("../prospects/frmProspects.aspx?DivID=" & lngDivID)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnActionItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActionItem.Click
            Try
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.DivisionID = lngDivID
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                Response.Redirect("../admin/newaction.aspx?frm=Leadedetails&CntID=" & CInt(txtContId.Text))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim i, k As Integer
                Dim dtTable As DataTable
                ' Tabstrip3.Items.Clear()
                'Dim ObjCus As New CustomFields(Session("userid"))
                'ObjCus.locId = 1
                'ObjCus.RelId = CInt(Type.text)
                'ObjCus.RecordId = lngDivID
                'ObjCus.DomainID = Session("DomainID")
                'dtTable = ObjCus.GetCustFlds
                'Session("CusFields") = dtTable
                If objPageLayout Is Nothing Then objPageLayout = New CPageLayout
                objPageLayout.locId = 1
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.RelId = CInt(type.Text)
                objPageLayout.RecordId = lngDivID
                dtTable = objPageLayout.GetCustFlds

                If uwOppTab.Tabs.Count > 5 Then
                    Dim iItemcount As Integer
                    iItemcount = uwOppTab.Tabs.Count
                    While uwOppTab.Tabs.Count > 5
                        uwOppTab.Tabs.RemoveAt(iItemcount - 1)
                        iItemcount = iItemcount - 1
                    End While
                End If

                If dtTable.Rows.Count > 0 Then
                    'CustomField Section
                    Dim Tab As Tab
                    'Dim pageView As PageView
                    Dim aspTable As HtmlTable
                    Dim Table As Table
                    Dim tblcell As TableCell
                    Dim tblRow As TableRow
                    k = 0
                    ViewState("TabId") = dtTable.Rows(0).Item("TabId")
                    ViewState("Check") = 0
                    ViewState("FirstTabCreated") = 0
                    '  Tabstrip3.Items.Clear()
                    For i = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(i).Item("TabId") <> 0 Then
                            If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                                If ViewState("Check") <> 0 Then
                                    aspTable.Rows.Add(objRow)
                                    tblcell.Controls.Add(aspTable)
                                    tblRow.Cells.Add(tblcell)
                                    Table.Rows.Add(tblRow)
                                    Tab.ContentPane.Controls.Add(Table)
                                End If
                                k = 0
                                ViewState("FirstTabCreated") = 1
                                ViewState("Check") = 1
                                ' If Not IsPostBack Then
                                ViewState("TabId") = dtTable.Rows(i).Item("TabId")
                                Tab = New Tab
                                Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
                                uwOppTab.Tabs.Add(Tab)
                                'End If
                                'pageView = New PageView
                                aspTable = New HtmlTable
                                Table = New Table
                                Table.Width = Unit.Percentage(100)
                                Table.BorderColor = System.Drawing.Color.FromName("black")
                                Table.GridLines = GridLines.None
                                Table.BorderWidth = Unit.Pixel(1)
                                Table.Height = Unit.Pixel(300)
                                Table.CssClass = "aspTable"
                                tblcell = New TableCell
                                tblRow = New TableRow
                                tblcell.VerticalAlign = VerticalAlign.Top
                                aspTable.Width = "100%"
                                objRow = New HtmlTableRow
                                objCell = New HtmlTableCell
                                objCell.InnerHtml = "<br>"
                                objRow.Cells.Add(objCell)
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If

                            'pageView.Controls.Add("")
                            If k = 3 Then
                                k = 0
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If
                            objCell = New HtmlTableCell
                            objCell.Align = "right"
                            objCell.Width = 100
                            objCell.Attributes.Add("class", "normal7")
                            If dtTable.Rows(i).Item("fld_type") <> "Frame" Then
                                If dtTable.Rows(i).Item("fld_type") <> "Link" Then objCell.InnerText = dtTable.Rows(i).Item("fld_label") & " :"
                                objRow.Cells.Add(objCell)
                            End If
                            objCell = New HtmlTableCell
                            objCell.Align = "left"

                            objCell.Attributes.Add("class", "normal1")

                            If dtTable.Rows(i).Item("fld_type") = "Link" Then
                                Dim h As New HyperLink
                                h.CssClass = "hyperlink"
                                Dim URL As String = IIf(IsDBNull(dtTable.Rows(i).Item("vcURL")), "", dtTable.Rows(i).Item("vcURL"))
                                URL = URL.Replace("RecordID", lngDivID)
                                h.Text = IIf(IsDBNull(dtTable.Rows(i).Item("fld_label")), "", dtTable.Rows(i).Item("fld_label"))
                                h.Attributes.Add("onclick", "fn_GoToURL('" & "http://" & URL & "')")
                                objCell.Controls.Add(h)
                                'CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngDivID, dtTable.Rows(i).Item("fld_label"))
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = IIf(dtTable.Rows(i).Item("Value") = "1", "a", "r")
                                    l.CssClass = IIf(dtTable.Rows(i).Item("Value") = "1", "cell1", "cell")
                                    objCell.Controls.Add(l)
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    Dim l As New Label
                                    l.Text = "r"
                                    l.CssClass = "cell"
                                    objCell.Controls.Add(l)
                                End If
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
                                objCell = New HtmlTableCell
                                Dim strFrame As String
                                Dim URL As String
                                URL = dtTable.Rows(i).Item("vcURL")
                                URL = URL.Replace("RecordID", lngDivID)
                                strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                objCell.Controls.Add(New LiteralControl(strFrame))
                                objRow.Cells.Add(objCell)
                            Else
                                If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = IIf(dtTable.Rows(i).Item("Value") = "0" Or dtTable.Rows(i).Item("Value") = Nothing, "-", dtTable.Rows(i).Item("Value"))
                                ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
                                    objCell.InnerText = "-"
                                End If
                            End If
                            objRow.Cells.Add(objCell)
                            k = k + 1
                        End If
                    Next
                    If ViewState("Check") = 1 Then
                        aspTable.Rows.Add(objRow)
                        tblcell.Controls.Add(aspTable)
                        tblRow.Cells.Add(tblcell)
                        Table.Rows.Add(tblRow)
                        Tab.ContentPane.Controls.Add(Table)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to set the Association Information.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the imprints the association information on a screen label.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Sub LoadAssociationInformation()
            Try
                If objAssociationInfo Is Nothing Then objAssociationInfo = New OrgAssociations 'Create a new object of association
                objAssociationInfo.DomainID = Session("DomainID")               'Set the Domain Id
                objAssociationInfo.DivisionID = lngDivID                           'Set the Division Id
                Dim dtAssociationInfo As DataTable                              'Declare a new DataTable
                dtAssociationInfo = objAssociationInfo.getParentOrgForCurrentOrg 'Get the Association Info
                If dtAssociationInfo.Rows.Count > 0 Then                        'Check if association exists where there is a parent-child relationship
                    lblAssociation.Text = dtAssociationInfo.Rows(0).Item("vcData") & " of: " & "<a href=""javascript: GoOrgDetails(" & dtAssociationInfo.Rows(0).Item("numDivisionId") & ");""><b>" & dtAssociationInfo.Rows(0).Item("vcCompanyName") & "</b></a>, " & dtAssociationInfo.Rows(0).Item("vcDivisionName")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                If objAccount Is Nothing Then objAccount = New CAccounts
                With objAccount
                    .DivisionID = lngDivID
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                End With
                If objAccount.DeleteOrg = False Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else : Response.Redirect("../Leads/frmLeadList.aspx")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dlWebAnlys_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlWebAnlys.ItemCommand
            Try
                If e.CommandName = "Pages" Then
                    Dim cmd As String = CType(e.CommandSource, LinkButton).CommandName
                    dlWebAnlys.SelectedIndex = e.Item.ItemIndex
                    Session("dlIndex") = CInt(e.Item.ItemIndex) + 1
                    createMainLink()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub createMainLink()
            Try
                If objLeads Is Nothing Then objLeads = New CLeads
                objLeads.DivisionID = lngDivID
                objLeads.DomainID = Session("DomainID")
                Dim ds As DataSet
                ds = objLeads.GetWebAnlysDtl
                If Session("dlIndex") <> 0 Then dlWebAnlys.SelectedIndex = Session("dlIndex") - 1
                dlWebAnlys.DataSource = ds.Tables(1)
                dlWebAnlys.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dlWebAnlys_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlWebAnlys.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.SelectedItem Then
                    If objLeads Is Nothing Then objLeads = New CLeads
                    objLeads.TrackingID = CType(e.Item.FindControl("lblID"), Label).Text
                    Dim dg As DataGrid
                    dg = e.Item.FindControl("dgWebAnlys")
                    dg.DataSource = objLeads.GetPagesVisited
                    dg.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            Try
                Response.Redirect("../pagelayout/frmLeadEdit.aspx?frm=LeadDtl&DivID=" & lngDivID & "&SI=" & uwOppTab.SelectedTabIndex & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm & "&frm2=" & frm1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub uwItem_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.CellEventArgs) Handles uwItem.ClickCellButton
            Try
                Dim numAItemCode As Integer = CInt(e.Cell.Tag)
                If objItems Is Nothing Then objItems = New CItems
                objItems.DivisionID = lngDivID
                objItems.DomainID = Session("DomainId")
                objItems.ItemCode = numAItemCode
                objItems.DeleteItemFromCmpAsset()
                LoadAssets()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub uwItem_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwItem.InitializeRow
            Try
                If e.Row.HasParent = False Then
                    e.Row.Cells.FromKey("Action").Tag = e.Row.Cells.FromKey("numAItemCode").Value
                    e.Row.Cells.FromKey("Action").Value = "r"
                    e.Row.Cells.FromKey("Action").Column.CellButtonStyle.CssClass = "Delete"
                    e.Row.Cells.FromKey("Action").Column.Width = Unit.Pixel(20)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub uwOppTab_TabClick(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebTab.WebTabEvent) Handles uwOppTab.TabClick
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTabDetails()
            Try
                Select Case uwOppTab.SelectedTabIndex
                    Case 0
                        m_aryRightsForCustFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmLeads.aspx", Session("userID"), 2, 8)
                        LoadTableInformation()
                        DisplayDynamicFlds()
                    Case 1 : sb_ShowAOIs(txtContId.Text)
                    Case 2
                        If objLeads Is Nothing Then objLeads = New CLeads
                        objLeads.DivisionID = lngDivID
                        objLeads.DomainID = Session("DomainID")
                        Dim ds As DataSet
                        ds = objLeads.GetWebAnlysDtl
                        If ds.Tables(0).Rows.Count > 0 Then
                            lblDatesvisited.Text = ds.Tables(0).Rows(0).Item("StartDate") & " - " & ds.Tables(0).Rows(0).Item("EndDate")
                            lblReferringPage.Text = ds.Tables(0).Rows(0).Item("vcOrginalRef")
                            lblKeyword.Text = ds.Tables(0).Rows(0).Item("vcSearchTerm")
                            lblNoofTimes.Text = ds.Tables(0).Rows(0).Item("Count")
                        Else
                            lblDatesvisited.Text = "-"
                            lblReferringPage.Text = "-"
                            lblKeyword.Text = "-"
                            lblNoofTimes.Text = "-"
                        End If
                        dlWebAnlys.DataSource = ds.Tables(1)
                        dlWebAnlys.DataBind()
                    Case 3 : getCorrespondance()
                    Case 4 : LoadAssets()
                    Case Else
                        m_aryRightsForCustFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmLeads.aspx", Session("userID"), 2, 8)
                        DisplayDynamicFlds()
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCorresGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorresGo.Click
            Try
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rptCorr_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptCorr.ItemCommand
            Try
                If e.CommandName = "Sort" Then
                    If e.CommandSource.ID = "lnkDate" Then
                        strColumn = "Date"
                    ElseIf e.CommandSource.ID = "lnkType" Then
                        strColumn = "Type"
                    ElseIf e.CommandSource.ID = "lnkFrom" Then
                        strColumn = "[From]"
                    ElseIf e.CommandSource.ID = "lnkName" Then
                        strColumn = "phone"
                    ElseIf e.CommandSource.ID = "lnkAssigned" Then
                        strColumn = "assignedto"
                    End If
                End If
                If Session("Column") <> strColumn Then
                    Session("Column") = strColumn
                    Session("Asc") = 0
                Else
                    If Session("Asc") = 0 Then
                        Session("Asc") = 1
                    Else : Session("Asc") = 0
                    End If
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlFilterCorr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterCorr.SelectedIndexChanged
            Try
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnCorrDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorrDelete.Click
            Try
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim chk As CheckBox
                Dim lbl As Label
                For Each r As RepeaterItem In rptCorr.Items
                    chk = r.FindControl("chkADelete")
                    If chk.Checked = True Then
                        lbl = r.FindControl("lblDelete")
                        If lbl.Text.Split("~")(1) = 1 Then
                            objContacts.EmailHstrID = lbl.Text.Split("~")(0)
                            objContacts.tinttype = 1
                        Else
                            objContacts.EmailHstrID = lbl.Text.Split("~")(0)
                            objContacts.tinttype = 2
                        End If
                        objContacts.DelCorrespondence()
                    End If
                Next
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub getCorrespondance()
            Try
                m_aryRightsForCorr = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmLeads.aspx", Session("userID"), 2, 5)
                If m_aryRightsForCorr(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If m_aryRightsForCorr(RIGHTSTYPE.DELETE) = 0 Then btnCorrDelete.Visible = False
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim dttable As DataTable
                objContacts.FromDate = Calendar1.SelectedDate
                objContacts.ToDate = DateAdd(DateInterval.Day, 1, CDate(Calendar2.SelectedDate))
                objContacts.ContactID = CInt(txtContId.Text)
                objContacts.DivisionID = lngDivID
                objContacts.MessageFrom = ""
                objContacts.SortOrder1 = ddlFilterCorr.SelectedValue
                objContacts.UserCntID = Session("UserContactID")
                objContacts.SortOrder = ddlSrchCorr.SelectedValue
                objContacts.KeyWord = txtSearchCorr.Text
                objContacts.DomainID = Session("DomainID")
                objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                If txtCurrentPageCorr.Text.Trim <> "" Then
                    objContacts.CurrentPage = txtCurrentPageCorr.Text
                Else : objContacts.CurrentPage = 1
                End If
                objContacts.PageSize = Session("PagingRows")
                objContacts.TotalRecords = 0
                If strColumn <> "" Then
                    objContacts.columnName = strColumn
                Else : objContacts.columnName = "date"
                End If
                If Session("Asc") = 1 Then
                    objContacts.columnSortOrder = "Desc"
                Else : objContacts.columnSortOrder = "Asc"
                End If
                dttable = objContacts.getCorres()
                If objContacts.TotalRecords = 0 Then
                    tdCorr.Visible = False
                    lblNoOfRecordsCorr.Text = objContacts.TotalRecords
                Else
                    tdCorr.Visible = True
                    lblNoOfRecordsCorr.Text = String.Format("{0:#,###}", objContacts.TotalRecords)
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblNoOfRecordsCorr.Text / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (lblNoOfRecordsCorr.Text Mod Session("PagingRows")) = 0 Then
                        lblTotalCorr.Text = strTotalPage(0)
                        txtCorrTotalPage.Text = strTotalPage(0)
                    Else
                        lblTotalCorr.Text = strTotalPage(0) + 1
                        txtCorrTotalPage.Text = strTotalPage(0) + 1
                    End If
                    txtCorrTotalRecords.Text = lblNoOfRecordsCorr.Text
                End If
                rptCorr.DataSource = dttable
                rptCorr.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub lnk2Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2Corr.Click
            Try
                If txtCurrentPageCorr.Text + 1 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 1 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 2
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk3Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3Corr.Click
            Try
                If txtCurrentPageCorr.Text + 2 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 2 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 3
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk4Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4Corr.Click
            Try
                If txtCurrentPageCorr.Text + 3 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 3 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 4
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk5Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5Corr.Click
            Try
                If txtCurrentPageCorr.Text + 4 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 4 > txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 5
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkFirstCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFirstCorr.Click
            Try
                txtCurrentPageCorr.Text = 1
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkLastCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLastCorr.Click
            Try
                txtCurrentPageCorr.Text = txtCorrTotalPage.Text
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkNextCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextCorr.Click
            Try
                If txtCurrentPageCorr.Text = txtCorrTotalPage.Text Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 1
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkPreviousCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreviousCorr.Click
            Try
                If txtCurrentPageCorr.Text = 1 Then
                    Exit Sub
                Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text - 1
                End If
                getCorrespondance()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rptCorr_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCorr.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Then
                    Dim chk As CheckBox
                    Dim lbl As Label
                    chk = e.Item.FindControl("chkADelete")
                    lbl = e.Item.FindControl("lblDelete")
                    If m_aryRightsForCorr(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If lbl.Text.Split("~")(2) <> Session("UserContactID") Then chk.Visible = False
                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
