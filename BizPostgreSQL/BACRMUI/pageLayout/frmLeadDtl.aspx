<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmLeadDtl.aspx.vb"
    Inherits="BACRM.UserInterface.Leads.frmLeadDTl" %>

<%@ Register TagPrefix="menu1" TagName="Menu" Src="../include/webmenu.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link href="../css/lists.css" type="text/css" rel="stylesheet" />
    <title>Lead Details</title>

    <script language="JavaScript" src="../javascript/date-picker.js" type="text/javascript"></script>

    <script language="javascript">
        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email=" + a + "&Date=" + f, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
                return false;
            }
            else {
                window.location.href = "../admin/actionitemdetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Leads&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
                return false;
            }

        }
        function chkAll() {
            for (i = 1; i <= 20; i++) {
                var str;
                if (i < 10) {
                    str = '0' + i
                }
                else {
                    str = i
                }
                if (document.getElementById('uwOppTab__ctl3_rptCorr_ctl00_chkDelete').checked == true) {
                    if (document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete') != null) {
                        document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete').checked = true;
                    }
                }
                else {
                    if (document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete') != null) {
                        document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete').checked = false;
                    }
                }
            }
        }
        function fn_Mail(txtMailAddr, a, b) {
            if (txtMailAddr != '') {
                if (a == 1) {

                    window.open('mailto:' + txtMailAddr);
                }
                else if (a == 2) {
                    window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + txtMailAddr + '&rtyWR=' + b, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')

                }

            }

            return false;
        }
        function ShowLayout(a, b, c) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=" + a + "&type=" + c, '', 'toolbar=no,titlebar=no,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
        function OpenEmailMessage(a, b) {
            window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email=" + a + "&Date=" + b, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
            return false;
        }
        function OpenListView(a) {
            window.open("../admin/frmEmailUsers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ContID=" + a, '', 'width=650,height=350,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }


        function openFollow(a) {
            window.open("../Leads/frmFollowUpHstr.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenAdd(a) {
            window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
        function FillAddress(a) {
            if (document.all) {
                document.getElementById('uwOppTab__ctl0_lblAddress').innerText = a;
            } else {
                document.getElementById('uwOppTab__ctl0_lblAddress').textContent = a;
            }
            return false;
        }
        function fn_GoToURL(varURL) {

            if ((varURL != '') && (varURL.substr(0, 7) == 'http://') && (varURL.length > 7)) {
                var LoWindow = window.open(varURL, "", "");
                LoWindow.focus();
            }
            return false;
        }


        function fn_SendMail(a) {
            if (document.Form1.txtEmail.value != '') {
                window.open('../common/callemail.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + document.Form1.txtEmail.value + '&ContID=' + a, 'mail');

            }
            return false;
        }
        function CheckNumber() {
            if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                window.event.keyCode = 0;
            }
        }
        function GoOrgDetails(numDivisionId) {
            frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionEntity').value = 'OrganizationFromDiv';
            frames['IfrOpenOrgContact'].document.getElementById('hdDivisionId').value = numDivisionId;
            frames['IfrOpenOrgContact'].document.forms['frmOrgContactRedirect'].submit();
        }
        function CheckTabSel(a) {
            if (document.getElementById('uwOppTab').selectedIndex == 4 && a == 0) {
                document.Form1.submit()
            }
            return false;
        }
        function Save() {
            if (document.Form1.ddlGroup.value == 0) {
                alert("Please Select Group")
                document.Form1.ddlGroup.focus();
                return false;
            }
        }
        function ShowLayout(a, b, c) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=" + a + "&type=" + c, '', 'toolbar=no,titlebar=no,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:Menu ID="webmenu1" runat="server"></menu1:Menu>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <table id="tblMenu" bordercolor="black" cellspacing="0" cellpadding="0" width="100%"
                            border="0" runat="server">
                            <tr>
                                <td class="tr1" align="center">
                                    <b>Record Owner: </b>
                                    <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Created By: </b>
                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Last Modified By: </b>
                                    <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal1">
                                    Organization ID :
                                    <asp:Label ID="lblCustID" runat="Server"></asp:Label>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblAssociation" runat="Server"></asp:Label>
                                    <!--<iframe id="IfrOpenOrgContact" src="../Marketing/frmOrgContactRedirect.aspx" frameBorder="0"
										width="10" scrolling="no" height="10" left="0" right="0"></iframe>-->
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnActionItem" Text="New Action Item" runat="server" CssClass="ybutton">
                                    </asp:Button>
                                    <asp:Button ID="btnTransfer" runat="server" Text="Transfer Ownership" CssClass="button">
                                    </asp:Button>
                                    <asp:Button ID="btnPromote" Text="Promote" runat="server" CssClass="button"></asp:Button>
                                    <asp:Button ID="btnEdit" Text="Edit" Width="50" runat="server" CssClass="button">
                                    </asp:Button>
                                    <asp:Button ID="btnCancel" Text="Close" runat="server" CssClass="button"></asp:Button>
                                    <asp:Button ID="btnActDelete" runat="server" CssClass="Delete" Text="X"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <igtab:UltraWebTab AutoPostBack="true" ImageDirectory="" ID="uwOppTab" runat="server"
                            ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                            <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                            </DefaultTabStyle>
                            <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                                NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                                FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <SelectedTabStyle Height="23px" ForeColor="white">
                            </SelectedTabStyle>
                            <HoverTabStyle Height="23px" ForeColor="white">
                            </HoverTabStyle>
                            <Tabs>
                                <igtab:Tab Text="&nbsp;&nbsp;Lead Details&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="Table5" runat="server" BorderWidth="1" Width="100%" GridLines="none"
                                            Height="300" BorderColor="black" CssClass="aspTableDTL">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="top">
                                                    <asp:Table Width="100%" ID="tbl12" runat="server">
                                                        <asp:TableRow>
                                                            <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">
                                                                <asp:Button ID="btnLayout" runat="server" CssClass="button" Text="Layout"></asp:Button>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell VerticalAlign="Top"><img src="../images/Building-48.gif" /></asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Table runat="server" ID="tabledetail" BorderWidth="0" GridLines="none" CellPadding="2"
                                                                    CellSpacing="0" HorizontalAlign="Center">
                                                                </asp:Table>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell ColumnSpan="2">
                                                                <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%" GridLines="none"
                                                                    HorizontalAlign="Center">
                                                                </asp:Table>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:TableCell></asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="&nbsp;&nbsp;Areas of Intrest&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="Table2" runat="server" BorderWidth="1" Width="100%" Height="300" BorderColor="black"
                                            CssClass="aspTableDTL" GridLines="None">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br>
                                                    <asp:Table ID="tblAOI" Width="100%" runat="server">
                                                    </asp:Table>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="&nbsp;&nbsp;Web Analysis&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="Table8" BorderWidth="1" Height="300" runat="server" Width="100%" BorderColor="black"
                                            CssClass="aspTableDTL" GridLines="None">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <table width="700">
                                                        <tr>
                                                            <td class="text_bold" align="right">
                                                                Original Referring Page :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblReferringPage" runat="server" CssClass="normal1"></asp:Label>
                                                            </td>
                                                            <td class="text_bold" align="right">
                                                                Original Referring Key Word Used :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblKeyword" runat="server" CssClass="normal1"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text_bold" align="right">
                                                                First / Last Date visited :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblDatesvisited" runat="server" CssClass="normal1"></asp:Label>
                                                            </td>
                                                            <td class="text_bold" align="right">
                                                                Total Number of times visited :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblNoofTimes" runat="server" CssClass="normal1"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:DataList ID="dlWebAnlys" runat="server" Width="100%" CellPadding="0" CellSpacing="2">
                                                        <ItemTemplate>
                                                            <table width="100%" class="hs">
                                                                <tr>
                                                                    <td align="right">
                                                                        Date Visitied:
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblCreated" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.dtCreated") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td align="right">
                                                                        <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.NoOfTimes") %>'>
                                                                        </asp:Label>&nbsp;Pages Viewed :
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lnk" CommandName="Pages" runat="server" Style="text-decoration: none">
													<font color="white">Click Here</font></asp:LinkButton>
                                                                    </td>
                                                                    <td align="right">
                                                                        Total Time on site :
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblTotalTime" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcTotalTime") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <SelectedItemTemplate>
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr class="hs">
                                                                    <td align="right">
                                                                        <asp:Label ID="lblID" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numTrackingID") %>'
                                                                            Visible="false">
                                                                        </asp:Label>
                                                                        Date Visitied:
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblCreated" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.dtCreated") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td align="right">
                                                                        <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.NoOfTimes") %>'>
                                                                        </asp:Label>&nbsp;Pages Viewed :
                                                                    </td>
                                                                    <td align="right">
                                                                        Total Time on site :
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblTotalTime" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcTotalTime") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:DataGrid ID="dgWebAnlys" runat="server" AllowSorting="True" CssClass="dg" Width="100%"
                                                                            BorderColor="white" AutoGenerateColumns="False">
                                                                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                                            <ItemStyle CssClass="is"></ItemStyle>
                                                                            <HeaderStyle CssClass="hs"></HeaderStyle>
                                                                            <Columns>
                                                                                <asp:BoundColumn Visible="False" DataField="numTracVisitorsHDRID"></asp:BoundColumn>
                                                                                <asp:BoundColumn HeaderText="Page" DataField="vcPageName"></asp:BoundColumn>
                                                                                <asp:BoundColumn HeaderText="Time Spent on the Page" DataField="vcElapsedTime"></asp:BoundColumn>
                                                                                <asp:BoundColumn HeaderText="Time Visited" DataField="TimeVisited"></asp:BoundColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </SelectedItemTemplate>
                                                    </asp:DataList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="&nbsp;&nbsp;Correspondence&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="Table4" CellPadding="0" CellSpacing="0" runat="server" BorderWidth="1"
                                            Width="100%" CssClass="aspTable" GridLines="None" BorderColor="black" Height="300">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <table width="100%" border="0">
                                                        <tr align="center" valign="top">
                                                            <td align="right">
                                                                <table>
                                                                    <tr>
                                                                        <td class="normal1" align="right">
                                                                            From
                                                                        </td>
                                                                        <td align="left">
                                                                            <BizCalendar:Calendar ID="Calendar1" runat="server" />
                                                                        </td>
                                                                        <td class="normal1" align="right">
                                                                            To
                                                                        </td>
                                                                        <td>
                                                                            <BizCalendar:Calendar ID="Calendar2" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="right">
                                                                <table class="normal1">
                                                                    <tr>
                                                                        <td>
                                                                            Search
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtSearchCorr" runat="server" CssClass="signup"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlSrchCorr" runat="server" CssClass="signup">
                                                                                <asp:ListItem Text="Show All" Value="0"></asp:ListItem>
                                                                                <asp:ListItem Text="Received Messages" Value="1"></asp:ListItem>
                                                                                <asp:ListItem Text="Sent Messages" Value="2"></asp:ListItem>
                                                                                <asp:ListItem Text="Communications by Due Date" Value="3"></asp:ListItem>
                                                                                <asp:ListItem Text="Communications by Created Date" Value="4"></asp:ListItem>
                                                                                <asp:ListItem Text="Tasks by Due Date" Value="5"></asp:ListItem>
                                                                                <asp:ListItem Text="Tasks by Created Date" Value="6"></asp:ListItem>
                                                                                <asp:ListItem Text="Notes by Created Date" Value="7"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Button ID="btnCorresGo" runat="server" CssClass="button" Text="Go"></asp:Button>&nbsp;
                                                                            <asp:Button ID="btnCorrDelete" runat="server" CssClass="button" Text="Delete"></asp:Button>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="right">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="normal1">
                                                                            Filter :
                                                                            <asp:DropDownList ID="ddlFilterCorr" runat="server" AutoPostBack="true" CssClass="signup">
                                                                                <asp:ListItem Text="Show All" Value="0"></asp:ListItem>
                                                                                <asp:ListItem Text="Received Messages" Value="1"></asp:ListItem>
                                                                                <asp:ListItem Text="Sent Messages" Value="2"></asp:ListItem>
                                                                                <asp:ListItem Text="Communications by Due Date" Value="3"></asp:ListItem>
                                                                                <asp:ListItem Text="Communications by Created Date" Value="4"></asp:ListItem>
                                                                                <asp:ListItem Text="Tasks by Due Date" Value="5"></asp:ListItem>
                                                                                <asp:ListItem Text="Tasks by Created Date" Value="6"></asp:ListItem>
                                                                                <asp:ListItem Text="Notes by Created Date" Value="7"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="center" class="normal1">
                                                                            &nbsp;&nbsp;&nbsp;No of Records :
                                                                            <asp:Label ID="lblNoOfRecordsCorr" runat="server" CssClass="text"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                                        </td>
                                                                        <td id="tdCorr" runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="lblNextCorr" runat="server" CssClass="text_bold">&nbsp;&nbsp;&nbsp;Next:</asp:Label>
                                                                                    </td>
                                                                                    <td class="normal1">
                                                                                        <asp:LinkButton ID="lnk2Corr" runat="server" CausesValidation="False">2</asp:LinkButton>
                                                                                    </td>
                                                                                    <td class="normal1">
                                                                                        <asp:LinkButton ID="lnk3Corr" runat="server" CausesValidation="False">3</asp:LinkButton>
                                                                                    </td>
                                                                                    <td class="normal1">
                                                                                        <asp:LinkButton ID="lnk4Corr" runat="server" CausesValidation="False">4</asp:LinkButton>
                                                                                    </td>
                                                                                    <td class="normal1">
                                                                                        <asp:LinkButton ID="lnk5Corr" runat="server" CausesValidation="False">5</asp:LinkButton>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:LinkButton ID="lnkFirstCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow"><<</div>
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:LinkButton ID="lnkPreviousCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow"><</div>
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                    <td class="normal1">
                                                                                        <asp:Label ID="lblPageCorr" runat="server">Page</asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtCurrentPageCorr" runat="server" Text="1" Width="28px" CssClass="signup"
                                                                                            MaxLength="5" AutoPostBack="true"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="normal1">
                                                                                        <asp:Label ID="lblOfCorr" runat="server">of</asp:Label>
                                                                                    </td>
                                                                                    <td class="normal1">
                                                                                        <asp:Label ID="lblTotalCorr" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:LinkButton ID="lnkNextCorr" runat="server" CssClass="LinkArrow" CausesValidation="False">
															                    <div class="LinkArrow">></div>
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:LinkButton ID="lnkLastCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow">>></div>
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Repeater ID="rptCorr" runat="server">
                                                                    <HeaderTemplate>
                                                                        <table cellspacing="0" class="dg" width="100%">
                                                                            <tr class="hs">
                                                                                <td style="display: none">
                                                                                    numEmailHstrId
                                                                                </td>
                                                                                <td style="display: none">
                                                                                    tintType
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:LinkButton CommandName="Sort" ID="lnkDate" runat="server"><font color="white">Date</font></asp:LinkButton>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:LinkButton CommandName="Sort" ID="lnkType" runat="server"><font color="white">Type</font></asp:LinkButton>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:LinkButton CommandName="Sort" ID="lnkFrom" runat="server"><font color="white">From ,To</font></asp:LinkButton>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:LinkButton CommandName="Sort" ID="lnkName" runat="server"><font color="white">Name /Phone ,& Ext.</font></asp:LinkButton>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:LinkButton CommandName="Sort" ID="lnkAssigned" runat="server"><font color="white">Assigned To</font></asp:LinkButton>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:CheckBox ID="chkDelete" onclick="chkAll()" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                    </HeaderTemplate>
                                                                    <AlternatingItemTemplate>
                                                                        <tr class="ais" align="center">
                                                                            <td style="display: none">
                                                                                <%#Container.DataItem("numEmailHstrId")%>
                                                                            </td>
                                                                            <td style="display: none">
                                                                                <%#Container.DataItem("tintType")%>
                                                                            </td>
                                                                            <td>
                                                                                <%#Container.DataItem("date")%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:HyperLink ID="hplAType" CssClass="hyperlink" NavigateUrl="#" onclick="openActionItem('<%#Container.DataItem("numEmailHstrId")%>','<%#Container.DataItem("caseid")%>','<%#Container.DataItem("CaseTimeId")%>','<%#Container.DataItem("CaseExpId")%>','<%#Container.DataItem("Type")%>','<%#Container.DataItem("dtCreatedDate")%>')"
                                                                                    runat="server"><u style="cursor:hand"><%#Container.DataItem("Type")%></u></asp:HyperLink>
                                                                            </td>
                                                                            <td>
                                                                                <%#Container.DataItem("From")%>
                                                                            </td>
                                                                            <td>
                                                                                <%#Container.DataItem("Phone")%>
                                                                            </td>
                                                                            <td>
                                                                                <%#Container.DataItem("assignedto")%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkADelete" Style="color: #C6D3E7;" runat="server" />
                                                                                <asp:Label ID="lblDelete" Visible="false" runat="server" Text='<%#Container.DataItem("DelData")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="ais">
                                                                            <td colspan="8">
                                                                                <%#Container.DataItem("Subject")%>
                                                                            </td>
                                                                        </tr>
                                                                    </AlternatingItemTemplate>
                                                                    <ItemTemplate>
                                                                        <tr class="is">
                                                                            <td style="display: none">
                                                                                <%#Container.DataItem("numEmailHstrId")%>
                                                                            </td>
                                                                            <td style="display: none">
                                                                                <%#Container.DataItem("tintType")%>
                                                                            </td>
                                                                            <td>
                                                                                <%#Container.DataItem("date")%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:HyperLink ID="hplType" CssClass="hyperlink" NavigateUrl="#" onclick="openActionItem('<%#Container.DataItem("numEmailHstrId")%>','<%#Container.DataItem("caseid")%>','<%#Container.DataItem("CaseTimeId")%>','<%#Container.DataItem("CaseExpId")%>','<%#Container.DataItem("Type")%>','<%#Container.DataItem("dtCreatedDate")%>')"
                                                                                    runat="server"><u style="cursor:hand"><%#Container.DataItem("Type")%></u></asp:HyperLink>
                                                                            </td>
                                                                            <td>
                                                                                <%#Container.DataItem("From")%>
                                                                            </td>
                                                                            <td>
                                                                                <%#Container.DataItem("Phone")%>
                                                                            </td>
                                                                            <td>
                                                                                <%#Container.DataItem("assignedto")%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkADelete" runat="server" />
                                                                                <asp:Label ID="lblDelete" Visible="false" runat="server" Text='<%#Container.DataItem("DelData")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="is">
                                                                            <td colspan="8">
                                                                                <%#Container.DataItem("Subject")%>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="&nbsp;&nbsp;Assets&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="Table1" CellPadding="0" CellSpacing="0" runat="server" BorderWidth="1"
                                            Width="100%" CssClass="aspTable" GridLines="None" BorderColor="black" Height="300">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <igtbl:UltraWebGrid ID="uwItem" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"
                                                        runat="server" Browser="Xml" Height="100%">
                                                        <DisplayLayout AutoGenerateColumns="false" RowHeightDefault="18" AllowAddNewDefault="Yes"
                                                            Version="3.00" SelectTypeRowDefault="Single" ViewType="Hierarchical" TableLayout="Auto"
                                                            SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
                                                            Name="uwItem" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended"
                                                            AllowUpdateDefault="Yes">
                                                            <HeaderStyleDefault VerticalAlign="Middle" Font-Bold="true" Font-Size="8pt" Font-Names="Arial"
                                                                BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                                <Padding Left="2px" Right="2px"></Padding>
                                                                <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                                                </BorderDetails>
                                                            </HeaderStyleDefault>
                                                            <RowSelectorStyleDefault BackColor="White">
                                                            </RowSelectorStyleDefault>
                                                            <FrameStyle Width="100%" Cursor="Default" BorderWidth="0" Font-Size="8pt" Font-Names="Arial"
                                                                BorderStyle="Double">
                                                            </FrameStyle>
                                                            <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                                <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                                                </BorderDetails>
                                                            </FooterStyleDefault>
                                                            <EditCellStyleDefault BorderWidth="0px" BorderStyle="None">
                                                            </EditCellStyleDefault>
                                                            <SelectedRowStyleDefault ForeColor="White" BackColor="#666666">
                                                            </SelectedRowStyleDefault>
                                                            <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray"
                                                                BorderStyle="Solid" BackColor="White">
                                                                <Padding Left="5px" Right="5px"></Padding>
                                                                <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                            </RowStyleDefault>
                                                            <RowExpAreaStyleDefault BackColor="LightSteelBlue">
                                                            </RowExpAreaStyleDefault>
                                                        </DisplayLayout>
                                                        <Bands>
                                                            <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes">
                                                                <Columns>
                                                                    <igtbl:UltraGridColumn Hidden="true" IsBound="false" BaseColumnName="numAItemCode"
                                                                        Key="numAItemCode">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Item Name" Width="41%" AllowUpdate="No" IsBound="false"
                                                                        BaseColumnName="vcitemName" Key="vcitemName">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Serial No" Width="35%" AllowUpdate="No" IsBound="false"
                                                                        BaseColumnName="vcserialno" Key="vcserialno">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Units" Width="20%" AllowUpdate="No" IsBound="false"
                                                                        Format="###,##0.00" BaseColumnName="unit" Key="unit">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn Width="2%" CellButtonDisplay="Always" Type="Button" Key="Action">
                                                                    </igtbl:UltraGridColumn>
                                                                </Columns>
                                                            </igtbl:UltraGridBand>
                                                        </Bands>
                                                    </igtbl:UltraWebGrid>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                            </Tabs>
                        </igtab:UltraWebTab>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalPage" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalRecords" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtEmail" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="type" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtCorrTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtCorrTotalRecords" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtContactType" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtContId" Style="display: none" runat="server"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
