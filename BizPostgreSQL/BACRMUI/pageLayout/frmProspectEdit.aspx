<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmProspectEdit.aspx.vb"
    Inherits="BACRM.UserInterface.Prospects.frmProspectEdit" %>

<%@ Register TagPrefix="menu1" TagName="Menu" Src="../include/webmenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Prospects</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <link href="../css/lists.css" type="text/css" rel="STYLESHEET" />

    <script language="JavaScript" src="../javascript/date-picker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function openFollow(a) {
            window.open("../Leads/frmFollowUpHstr.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function fn_GoToURL(varURL) {
            var url = document.getElementById('uwOppTab__ctl0_' + varURL).value

            if ((url != '') && (url.substr(0, 7) == 'http://') && (url.length > 7)) {
                var LoWindow = window.open(url, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function fn_EditLabels(URL) {
            window.open(URL, 'WebLinkLabel', 'width=500,height=70,status=no,scrollbar=yes,top=110,left=150');
            return false;
        }
        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
        function OpenLst(url) {
            window.open(url, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenAdd(a) {
            window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=A&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTo(a) {
            window.open("../admin/frmcomAssociationTo.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&boolAssociateDiv=true&numDivisionId=" + a, '', 'toolbar=no,left=200,titlebar=no,top=300,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }


        function OpenFrom(a) {
            window.open("../admin/frmCompanyAssociationFrom.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,left=200,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }


        function Disableddl() {
            if (Tabstrip6.selectedIndex == 0) {
                document.Form1.uwOppTab$_ctl0$ddlOppStatus.style.display = "none";
            }
            else {
                document.Form1.uwOppTab$_ctl0$ddlOppStatus.style.display = "";
            }
        }
        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }
        function FillAddress(a) {
            if (document.all) {
                document.getElementById('uwOppTab__ctl0_lblAddress').innerText = a;
            } else {
                document.getElementById('uwOppTab__ctl0_lblAddress').textContent = a;
            }

            return false;
        }

        /*
        Purpose:	Lead to Organization Details Page
        Created By: Debasish Tapan Nag
        Parameter:	1) numContactId: The Contact Id
        Return		1) None
        */
        function GoOrgDetails(numDivisionId) {
            frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionEntity').value = 'OrganizationFromDiv';
            frames['IfrOpenOrgContact'].document.getElementById('hdDivisionId').value = numDivisionId;
            frames['IfrOpenOrgContact'].document.forms['frmOrgContactRedirect'].submit();
        }
        function CheckTabSel(a) {
            if (document.getElementById('uwOppTab').selectedIndex == 6 && a == 0) {
                document.Form1.submit()
            }
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:Menu ID="webmenu1" runat="server"></menu1:Menu>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%" align="center">
                <tbody>
                    <tr>
                        <td>
                            <table id="tblMenu" bordercolor="black" cellspacing="0" cellpadding="0" width="100%"
                                border="0" runat="server">
                                <tr>
                                    <td class="tr1" align="center">
                                        <b>Record Owner: </b>
                                        <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                                    </td>
                                    <td class="td1" width="1" height="18">
                                    </td>
                                    <td class="tr1" align="center">
                                        <b>Created By: </b>
                                        <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                                    </td>
                                    <td class="td1" width="1" height="18">
                                    </td>
                                    <td class="tr1" align="center">
                                        <b>Last Modified By: </b>
                                        <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td class="normal1" align="left">
                                        Organization ID :
                                        <asp:Label ID="lblCustomerId" runat="server"></asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblAssociation" runat="Server"></asp:Label>
                                        <iframe id="IfrOpenOrgContact" src="../Marketing/frmOrgContactRedirect.aspx" frameborder="0"
                                            width="10" scrolling="no" height="10" left="0" right="0"></iframe>
                                    </td>
                                    <td align="right">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button"></asp:Button>
                                        <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close">
                                        </asp:Button>
                                        <asp:Button ID="btnCancel" runat="server" Text="Close" CssClass="button"></asp:Button>
                                        <asp:Button ID="btnActDelete" runat="server" CssClass="Delete" Text="X"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <igtab:UltraWebTab ImageDirectory="" ID="uwOppTab" runat="server" ThreeDEffect="True"
                            BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                            <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                            </DefaultTabStyle>
                            <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                                NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                                FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <SelectedTabStyle Height="23px" ForeColor="white">
                            </SelectedTabStyle>
                            <HoverTabStyle Height="23px" ForeColor="white">
                            </HoverTabStyle>
                            <Tabs>
                                <igtab:Tab Text="&nbsp;&nbsp;Prospect Details&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="tblProspects" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
                                            runat="server" CssClass="aspTableDTL" Width="100%" BorderColor="black" GridLines="None">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br>
                                                    <table id="tblDetails" cellspacing="0" cellpadding="1" width="100%" border="0" runat="server">
                                                        <tr>
                                                            <td rowspan="30" valign="top">
                                                                <img src="../images/Building-48.gif" />
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                <asp:Label ID="lblCustomer" runat="server">Name</asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRelationShipName" runat="server" CssClass="signup" Width="180"
                                                                    TabIndex="1"></asp:TextBox>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Phone/Fax
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtPhone" runat="server" CssClass="signup" Width="90" TabIndex="7"></asp:TextBox>
                                                                <asp:TextBox ID="txtFax" runat="server" CssClass="signup" Width="90" TabIndex="8"></asp:TextBox>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Assigned To
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:DropDownList ID="ddlAssignedTo" CssClass="signup" runat="server" Width="180px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Industry
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlIndustry" runat="server" CssClass="signup" Width="180" TabIndex="3">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Rating
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlRating" runat="server" CssClass="signup" Width="180" TabIndex="9">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Employees
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlNoOfEmp" runat="server" CssClass="signup" Width="180" TabIndex="16">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Relationship
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlRelationhip" runat="server" CssClass="signup" TabIndex="4"
                                                                    Width="180" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Annual Revenue
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlAnnualRevenue" runat="server" CssClass="signup" Width="180"
                                                                    TabIndex="10">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Org. Status
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="signup" Width="180" TabIndex="17">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Org. Profile
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlProfile" runat="server" CssClass="signup" Width="180" TabIndex="5">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Web
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWeb" runat="server" Text="http://" CssClass="signup" Width="145"
                                                                    TabIndex="11"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="button" Width="25" TabIndex="12">
                                                                </asp:Button>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Campaign
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlCampaign" runat="server" CssClass="signup" Width="180" TabIndex="18">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Info. Source
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlInfoSource" runat="server" CssClass="signup" Width="180"
                                                                    TabIndex="6">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Territory
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlTerriory" runat="server" CssClass="signup" Width="180" TabIndex="13">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Follow-up Status
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:DropDownList ID="ddlFollow" runat="server" CssClass="signup" Width="180" TabIndex="19">
                                                                </asp:DropDownList>
                                                                <asp:HyperLink ID="hplFollowUpHstr" runat="server" CssClass="hyperlink" TabIndex="20">
															<font color="#180073">Hstr</font></asp:HyperLink>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Private
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:CheckBox ID="chkPrivate" runat="server" TabIndex="14"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:HyperLink ID="hplDocuments" runat="server" CssClass="hyperlink" TabIndex="15">
															<font color="#180073">Documents</font></asp:HyperLink><asp:Label runat="server" ID="lblDocCount"
                                                                Text=""></asp:Label>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td class="normal1">
                                                                Associations:&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:HyperLink ID="hplTo" runat="server" CssClass="hyperlink" TabIndex="21">
															<font color="#180073">To</font></asp:HyperLink>(<asp:Label runat="server" Text=""
                                                                ID="lblAssoCountT"></asp:Label>)&nbsp;/&nbsp;
                                                                <asp:HyperLink ID="hplFrom" runat="server" CssClass="hyperlink" TabIndex="22">
															<font color="#180073">From</font></asp:HyperLink>(<asp:Label runat="server" Text=""
                                                                ID="lblAssoCountF"></asp:Label>)
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                <asp:HyperLink ID="hplAddress" runat="server" CssClass="hyperlink">
															<font color="#180073">Address</font></asp:HyperLink>
                                                            </td>
                                                            <td class="normal1" colspan="5">
                                                                <asp:Label ID="lblAddress" runat="server" Text="" Width="100%"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Comments
                                                            </td>
                                                            <td class="normal1" colspan="3">
                                                                <asp:TextBox ID="txtComments" runat="server" CssClass="signup" Width="750" Rows="3"
                                                                    TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text_bold" align="right">
                                                                Web Links
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnEditWebLnk" runat="server" CssClass="button" Text="Edit Web Links">
                                                                </asp:Button>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text" align="right">
                                                                <asp:Label ID="lblWebLink1" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWebLink1" runat="server" value="http://" Width="250px" CssClass="signup"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnWebGo1" runat="server" CssClass="button" Text="Go" Width="25">
                                                                </asp:Button>
                                                            </td>
                                                            <td class="text" align="right">
                                                                <asp:Label ID="lblWebLink2" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWebLink2" runat="server" value="http://" Width="250px" CssClass="signup"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnWebGo2" runat="server" CssClass="button" Text="Go" Width="25">
                                                                </asp:Button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text" align="right">
                                                                <asp:Label ID="lblWebLink3" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWebLink3" runat="server" value="http://" Width="250px" CssClass="signup"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnWebGo3" runat="server" CssClass="button" Text="Go" Width="25">
                                                                </asp:Button>
                                                            </td>
                                                            <td class="text" align="right">
                                                                <asp:Label ID="lblWebLink4" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWebLink4" runat="server" value="http://" Width="250px" CssClass="signup"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnWebGo4" runat="server" CssClass="button" Text="Go" Width="25">
                                                                </asp:Button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="&nbsp;&nbsp;Accounting&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="Table11" BorderWidth="1" Height="300" runat="server" Width="100%"
                                            BorderColor="black" CssClass="aspTableDTL" GridLines="None">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br>
                                                    <table>
                                                        <tr class="normal1">
                                                            <td>
                                                                <asp:RadioButton ID="rdbFinancialOverview" runat="server" AutoPostBack="true" GroupName="radProject"
                                                                    Checked="true" />
                                                            </td>
                                                            <td>
                                                                &nbsp;Financial Overview&nbsp;&nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="rdbTransaction" runat="server" AutoPostBack="true" GroupName="radProject" />
                                                            </td>
                                                            <td>
                                                                &nbsp;Transactions&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:Panel ID="PnlFinancialOverview" runat="server">
                                                        <table border="0">
                                                            <tr>
                                                                <td class="normal1" align="right">
                                                                    Credit Limit
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlCreditLimit" runat="server" CssClass="signup" Width="130">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td class="normal1" align="right">
                                                                </td>
                                                                <td>
                                                                    <asp:CheckBoxList ID="chkTaxItems" CssClass="normal1" runat="server" RepeatDirection="Horizontal"
                                                                        RepeatColumns="3">
                                                                    </asp:CheckBoxList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="normal1" align="right">
                                                                    Billing Terms
                                                                </td>
                                                                <td class="normal1">
                                                                    <asp:CheckBox ID="chkBillinTerms" runat="server"></asp:CheckBox>&nbsp; Msg.
                                                                    <asp:TextBox ID="txtSummary" runat="server" CssClass="signup"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="normal1" align="right">
                                                                    Net
                                                                </td>
                                                                <td class="normal1">
                                                                    <asp:TextBox ID="txtNetDays" runat="server" Width="40" CssClass="signup"></asp:TextBox>
                                                                    days
                                                                    <asp:RadioButton ID="radPlus" GroupName="rad" Checked="True" runat="server" Text="Plus">
                                                                    </asp:RadioButton>
                                                                    <asp:RadioButton ID="radMinus" runat="server" GroupName="rad" Text="Minus"></asp:RadioButton>
                                                                    &nbsp;
                                                                    <asp:TextBox ID="txtInterest" runat="server" Width="40" CssClass="signup"></asp:TextBox>
                                                                    %
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="PnlTransaction" runat="server" Visible="false">
                                                        <asp:Table ID="Table3" BorderWidth="1" CellPadding="0" CellSpacing="0" runat="server"
                                                            Width="100%" CssClass="aspTableDTL" BorderColor="black" GridLines="None" Height="250">
                                                            <asp:TableRow>
                                                                <asp:TableCell VerticalAlign="top">
                                                                    <asp:DataGrid ID="dgTransaction" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                                                                        BorderColor="white">
                                                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                                        <ItemStyle CssClass="is"></ItemStyle>
                                                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                                                        <Columns>
                                                                            <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                                                                            <asp:ButtonColumn DataTextField="Name" HeaderText="Authoritative BizDoc" CommandName="Opportunity">
                                                                            </asp:ButtonColumn>
                                                                            <asp:BoundColumn DataField="vcData" HeaderText="BizDoc Name"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="OppType" HeaderText="Type"></asp:BoundColumn>
                                                                            <asp:TemplateColumn HeaderText="Due Date">
                                                                                <ItemTemplate>
                                                                                    <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "DueDate"))%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Total Amount">
                                                                                <ItemTemplate>
                                                                                    <%#ReturnMoney(DataBinder.Eval(Container.DataItem, "TotalAmt"))%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Amount Paid">
                                                                                <ItemTemplate>
                                                                                    <%#ReturnMoney(DataBinder.Eval(Container.DataItem, "Amount"))%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Date Paid">
                                                                                <ItemTemplate>
                                                                                    <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "datePaid"))%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Deposited to Bank account on">
                                                                                <ItemTemplate>
                                                                                    <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "DepositedDate"))%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Balance Due">
                                                                                <ItemTemplate>
                                                                                    <%#ReturnMoney(DataBinder.Eval(Container.DataItem, "BalanceAmt"))%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                        </Columns>
                                                                        <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages">
                                                                        </PagerStyle>
                                                                    </asp:DataGrid>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:Panel>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                            </Tabs>
                        </igtab:UltraWebTab>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:TextBox ID="txtHidden" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtROwner" Style="display: none" runat="server"></asp:TextBox>
    </form>
</body>
</html>
