﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports System.ComponentModel
Imports System.Net.Http
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Case
Imports System.Text
Imports System.Linq
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Contacts
Imports Newtonsoft.Json
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Workflow
Imports System.IO

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following lincontext.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Common
    Inherits System.Web.Services.WebService

    <WebMethod(EnableSession:=True)> _
    Public Function keepSessionAlive() As String
        If Not Session("DomainID") Is Nothing Then
            Return "OK"
        Else
            Return "EXP" 'expired session
        End If

    End Function
    <WebMethod(EnableSession:=True)> _
    Public Function GetItems(ByVal context As RadComboBoxContext) As IEnumerable
        Try
            Dim items As New List(Of ComboBoxItemData)()
            If Not Session("DomainID") Is Nothing Then
                If context.Text.Trim <> "" Then
                    Dim objItem As New CItems
                    Dim ds As DataSet
                    With objItem
                        .DomainID = Session("DomainID")
                        .str = Trim(context.Text) & "%"
                        .type = 0
                        .ItemType = "P"
                        .Assembly = If(context.ContainsKey("checkAssembly"), Convert.ToBoolean(context("checkAssembly")), False)
                        ds = .GetItemsOrKits
                    End With

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each row As DataRow In ds.Tables(0).Rows
                            Dim itemData As New ComboBoxItemData()
                            itemData.Text = row("vcItemName")
                            itemData.Value = row("numItemCode")
                            items.Add(itemData)
                        Next
                    End If
                End If
            End If
            Return items
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetPromotionItems(ByVal context As RadComboBoxContext) As IEnumerable
        Try
            Dim items As New List(Of ComboBoxItemData)()
            If Not Session("DomainID") Is Nothing Then
                If context.Text.Trim <> "" Then
                    Dim objItem As New CItems
                    Dim ds As DataSet
                    With objItem
                        .DomainID = Session("DomainID")
                        .str = Trim(context.Text) & "%"
                        .type = 0
                        .ItemType = "P"
                        .Assembly = True
                        ds = .GetItemsOrKits
                    End With

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each row As DataRow In ds.Tables(0).Rows
                            Dim itemData As New ComboBoxItemData()
                            itemData.Text = row("vcItemName")
                            itemData.Value = row("numItemCode")
                            items.Add(itemData)
                        Next
                    End If
                End If
            End If
            Return items
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetItemsForKits(ByVal searchText As String, ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal isGetDefaultColumn As Boolean, ByVal isAssembly As Boolean, ByVal itemCode As Long, ByVal searchType As String) As String
        Try
            Dim json As String = String.Empty

            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
            Dim totalCount As Integer = 0
            If searchText <> "" Then
                Dim objCommon As CCommon
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.UserCntID = Session("UserContactID")
                Dim ds As DataSet
                Dim objItem As New CItems
                With objItem
                    .DomainID = Session("DomainID")
                    .str = searchText
                    .type = 1
                    .ItemCode = itemCode
                    .Assembly = isAssembly
                    .CurrentPage = pageIndex
                    .PageSize = pageSize
                    .SearchType = searchType
                    ds = .GetItemsOrKits
                End With

                totalCount = objItem.TotalRecords

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    If isGetDefaultColumn Then
                        json = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None)
                    Else
                        json = JsonConvert.SerializeObject(New With {Key .results = GetJson(ds.Tables(0), isGetDefaultColumn), Key .Total = totalCount}, Formatting.None)
                    End If
                End If
            End If

            Return json
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If

        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetKitChildsFirstLevel(ByVal searchText As String, ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal isGetDefaultColumn As Boolean, ByVal isAssembly As Boolean, ByVal itemCode As Long) As String
        Try
            Dim json As String = String.Empty

            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
            Dim totalCount As Integer = 0
            If searchText <> "" Then
                Dim objCommon As CCommon
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.UserCntID = Session("UserContactID")
                Dim ds As DataSet
                Dim objItem As New CItems
                With objItem
                    .DomainID = Session("DomainID")
                    .str = searchText
                    .type = 1
                    .ItemCode = itemCode
                    .Assembly = isAssembly
                    .CurrentPage = pageIndex
                    .PageSize = pageSize
                    ds = .GetKitChildsFirstLevel
                End With

                totalCount = objItem.TotalRecords

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    If isGetDefaultColumn Then
                        json = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None)
                    Else
                        json = JsonConvert.SerializeObject(New With {Key .results = GetJson(ds.Tables(0), isGetDefaultColumn), Key .Total = totalCount}, Formatting.None)
                    End If
                End If
            End If

            Return json
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If

        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetKitAssemblyCalculatedPrice(ByVal itemCode As Long, ByVal divisionID As Long, ByVal quantity As Double, ByVal priceBasedOn As Short) As String
        Try
            Dim objItem As New CItems
            objItem.DomainID = CCommon.ToLong(Session("DomainID"))
            objItem.DivisionID = divisionID
            objItem.Quantity = quantity
            objItem.ItemCode = itemCode
            objItem.KitAssemblyPriceBasedOn = priceBasedOn
            Dim dt As DataTable = objItem.GetKitAssemblyCalculatedPrice()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                Return CCommon.GetDecimalFormat(dt.Rows(0)("monPrice"))
            Else
                Return ""
            End If
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
                Context.Response.StatusCode = 404
                Return ex.Message
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetCompanies(ByVal context As RadComboBoxContext) As RadComboBoxData
        Dim items As New List(Of ComboBoxItemData)()
        If Not Session("DomainID") Is Nothing Then
            If context.Text.Trim <> "" And Len(context.Text.Trim) >= IIf(Session("ChrForCompSearch") = 0, 1, Session("ChrForCompSearch")) Then
                Dim ds As DataSet
                Dim objCommon As New CCommon

                Dim excludeEmployer As Boolean = False

                If context.ContainsKey("excludeEmployer") Then
                    excludeEmployer = CCommon.ToBool(context("excludeEmployer"))
                End If

                ds = objCommon.SearchOrganization(Session("DomainID"), Session("UserContactID"), If(CCommon.ToInteger(Session("OrganizationSearchCriteria")) = 1, True, False), context.Text.Trim, excludeemployer:=excludeEmployer)
                Return AddOrganizations(ds, context.NumberOfItems)
            End If
        End If
    End Function
    Private Function AddOrganizations(ByVal ds As DataSet, ByVal NoOfItemsRequested As Integer) As RadComboBoxData
        Try
            Dim radcmbdata As New RadComboBoxData

            Dim organizationsPerRequest As Integer = 10
            Dim itemOffset As Integer = NoOfItemsRequested
            Dim endOffset As Integer = itemOffset + organizationsPerRequest
            If endOffset > ds.Tables(0).Rows.Count Then
                endOffset = ds.Tables(0).Rows.Count
            End If
            If endOffset = ds.Tables(0).Rows.Count Then
                radcmbdata.EndOfItems = True
            Else
                radcmbdata.EndOfItems = False
            End If

            Dim result As New List(Of RadComboBoxItemData)(endOffset - itemOffset)
            'For Each row As DataRow In ds.Tables(0).Rows
            Dim organizationData As RadComboBoxItemData
            Dim row As DataRow
            For index As Int32 = itemOffset To endOffset - 1
                row = ds.Tables(0).Rows(index)
                organizationData = New RadComboBoxItemData()
                organizationData.Text = row("vcCompanyName")
                organizationData.Value = row("numDivisionID")
                For Each col As DataColumn In ds.Tables(0).Columns
                    organizationData.Attributes(col.ColumnName) = IIf(row(col.ColumnName).ToString.Length = 0, "-", row(col.ColumnName))
                Next
                result.Add(organizationData)
            Next

            radcmbdata.Items = result.ToArray()

            If (ds.Tables(0).Rows.Count > 0) Then
                radcmbdata.Message = String.Format("Customers <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset.ToString(), ds.Tables(0).Rows.Count.ToString())
            Else
                radcmbdata.Message = "No matches"
            End If

            Return radcmbdata
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <WebMethod(EnableSession:=True)>
    Public Function GetCompaniesWithClass(ByVal context As RadComboBoxContext) As RadComboBoxData
        Dim items As New List(Of ComboBoxItemData)()
        If Not Session("DomainID") Is Nothing Then
            If context.Text.Trim <> "" And Len(context.Text.Trim) >= IIf(Session("ChrForCompSearch") = 0, 1, Session("ChrForCompSearch")) Then
                Dim ds As DataSet
                Dim objCommon As New CCommon
                ds = objCommon.SearchOrganizationWithClass(Session("DomainID"), Session("UserContactID"), If(CCommon.ToInteger(Session("OrganizationSearchCriteria")) = 1, True, False), context.Text.Trim, CCommon.ToLong(context("ClassID")))
                Return AddOrganizations(ds, context.NumberOfItems)
            End If
        End If
    End Function
    <WebMethod(EnableSession:=True)>
    Public Function UpdateEmailStatus(ByVal intUpdateRecordId As Integer, ByVal intUpdatevalue As Integer)
        Try
            Dim objCommon As New CCommon
            objCommon.Mode = 15
            objCommon.UpdateRecordID = intUpdateRecordId
            objCommon.UpdateValueID = intUpdatevalue
            objCommon.UpdateSingleFieldValue()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <WebMethod(EnableSession:=True)>
    Public Function UpdateMultipleEmailStatus(ByVal strUpdateRecordId As String, ByVal intUpdatevalue As Integer)
        Try
            Dim objCommon As New CCommon
            objCommon.Mode = 16
            objCommon.Comments = strUpdateRecordId
            objCommon.UpdateValueID = intUpdatevalue
            objCommon.UpdateSingleFieldValue()

            Return ("Selected email status updated successfully!")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function DeleteMultipleEmail(ByVal strUpdateRecordId As String, ByVal NodeId As Integer)
        Try
            Dim m_aryRightsForPage() As Integer
            Dim objCommon As New CCommon
            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights_old(objCommon, "frmInboxItems.aspx", Context.Session("userID"), 33, 1)

            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                Return ("Error : You are not Authorized to Delete!")
            End If
            Dim objOutlook As New COutlook
            objOutlook.DomainID = CCommon.ToLong(Context.Session("DomainID"))
            objOutlook.UserCntID = CCommon.ToLong(Context.Session("UserContactID"))

            Dim strMailIds As String() = strUpdateRecordId.Trim(",").Split(",")
            objOutlook.mode = True

            Dim dt As DataTable
            dt = objOutlook.GetEmailHistryByIDs(strUpdateRecordId.Trim(","))


            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    Try
                        'objEmails.DeleteEmail will move selected mail to trash in gmail
                        Dim objEmails As New Email
                        objEmails.DomainID = CCommon.ToLong(Context.Session("DomainId"))
                        objEmails.UserID = CCommon.ToLong(Context.Session("UserContactID"))
                        objEmails.DeleteEmail(CCommon.ToLong(dr("numEmailHstrID")), CCommon.ToString(dr("numUid")), CCommon.ToString(dr("vcNodeName")), CCommon.ToString(dr("chrSource")))
                    Catch ex1 As Exception
                        ' litMessage.Text = ex1.Message
                    End Try


                Next
            End If

            Return ("Selected email deleted successfully!")

            'Dim str As New System.Text.StringBuilder
            'str.Append("{")
            'str.Append("""id"" : ""1"",")
            'str.Append("""value"" : ""1""")
            'str.Append("},")
            'str.Remove(str.ToString.Length - 1, 1)
            'Return ("[" & str.ToString & "]")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function SpamMultipleEmail(ByVal strUpdateRecordId As String, ByVal NodeId As Integer)
        Try
            Dim m_aryRightsForPage() As Integer
            Dim objCommon As New CCommon
            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights_old(objCommon, "frmInboxItems.aspx", Context.Session("userID"), 33, 1)

            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                Return ("Error : You are not Authorized to Spam!")
            End If

            Dim objOutlook As New COutlook
            Dim strMailIds As String() = strUpdateRecordId.Trim(",").Split(",")
            For i As Integer = 0 To strMailIds.Length - 1
                objOutlook.numEmailHstrID = CCommon.ToLong(strMailIds(i)) 'GetQueryStringVal( "numEmailHstrId")
                objOutlook.mode = True
                objOutlook.NodeId = 191
                objOutlook.MoveCopy()
            Next

            Return ("Selected email spam successfully!")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetItemNames(ByVal context As RadComboBoxContext) As RadComboBoxData
        Try

            If context.Text <> "" And Len(context.Text) >= IIf(Session("ChrForItemSearch") = 0, 1, Session("ChrForItemSearch")) Then
                Dim objCommon As CCommon
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.UserCntID = Session("UserContactID")

                Dim OppType As Short = context("OppType").ToString()
                Dim strDiv As String = context("Division").ToString()
                Dim ds As DataSet
                'If OppType = 1 Or (Session("RemoveVendorPOValidation") = True AndAlso OppType = 2) Then 'Remove Vendor validation when creating new Purchase Orders
                If OppType = 1 Then
                    If strDiv <> "" Then
                        ds = objCommon.NewOppItems(Session("DomainID"), strDiv, 1, context.Text, context("SearchOrderCustomerHistory").ToString())
                        Return AddItems(ds, context.NumberOfItems)
                    Else
                        ds = objCommon.NewOppItems(Session("DomainID"), 0, 1, context.Text, context("SearchOrderCustomerHistory").ToString())
                        Return AddItems(ds, context.NumberOfItems)
                    End If
                ElseIf OppType = 2 Then
                    If strDiv <> "" Then
                        ds = objCommon.NewOppItems(Session("DomainID"), strDiv, 2, context.Text, context("SearchOrderCustomerHistory").ToString())
                        Return AddItems(ds, context.NumberOfItems)
                    Else
                        ds = objCommon.NewOppItems(Session("DomainID"), 0, 2, context.Text, context("SearchOrderCustomerHistory").ToString()) '1 is replaced by 2-reason:Not allowing one to create PO for which vendor is not mapped.
                        Return AddItems(ds, context.NumberOfItems)
                    End If
                End If
            Else
                Dim radcmbdata As New RadComboBoxData
                Dim itemData As RadComboBoxItemData
                itemData = New RadComboBoxItemData
                Dim result As New List(Of RadComboBoxItemData)(1)
                itemData.Text = ""
                itemData.Value = ""
                result.Add(itemData)
                radcmbdata.Items = result.ToArray()
                Return radcmbdata
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function AddItems(ByVal ds As DataSet, ByVal NoOfItemsRequested As Integer) As RadComboBoxData
        Try
            Dim radcmbdata As New RadComboBoxData

            Dim itemsPerRequest As Integer = 10
            Dim itemOffset As Integer = NoOfItemsRequested
            Dim endOffset As Integer = itemOffset + itemsPerRequest
            If endOffset > ds.Tables(0).Rows.Count Then
                endOffset = ds.Tables(0).Rows.Count
            End If
            If endOffset = ds.Tables(0).Rows.Count Then
                radcmbdata.EndOfItems = True
            Else
                radcmbdata.EndOfItems = False
            End If


            Dim result As New List(Of RadComboBoxItemData)(endOffset - itemOffset)
            'For Each row As DataRow In ds.Tables(0).Rows
            Dim itemData As RadComboBoxItemData
            Dim row As DataRow
            For index As Int32 = itemOffset To endOffset - 1
                row = ds.Tables(0).Rows(index)
                itemData = New RadComboBoxItemData()
                itemData.Text = row("vcItemName")
                itemData.Value = row("numItemCode")
                For Each col As DataColumn In ds.Tables(0).Columns
                    itemData.Attributes(col.ColumnName) = IIf(row(col.ColumnName).ToString.Length = 0, "-", row(col.ColumnName))
                Next
                'itemData.Attributes("vcModelID") = IIf(row("vcModelID").ToString.Length = 0, "-", row("vcModelID"))
                'itemData.Attributes("monListPrice") = IIf(row("monListPrice").ToString.Length = 0, "-", row("monListPrice"))
                'itemData.Attributes("vcSKU") = IIf(row("vcSKU").ToString.Length = 0, "-", row("vcSKU"))
                'itemData.Attributes("txtItemDesc") = IIf(row("txtItemDesc").ToString.Length = 0, "-", row("txtItemDesc"))
                result.Add(itemData)
            Next
            'Next


            radcmbdata.Items = result.ToArray()

            If (ds.Tables(0).Rows.Count > 0) Then
                radcmbdata.Message = String.Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset.ToString(), ds.Tables(0).Rows.Count.ToString())
            Else
                radcmbdata.Message = "No matches"
            End If

            Return radcmbdata
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <WebMethod(EnableSession:=True)>
    Public Function FillOpenRecords(ByVal lngDivisionID As Long, ByVal lngDomainID As Long, ByVal Type As Short)
        Dim objOpp As New MOpportunity
        objOpp.DomainID = lngDomainID
        objOpp.DivisionID = lngDivisionID
        Dim dt As DataTable
        Select Case Type
            Case 1
                objOpp.Mode = 1 'select only sales opportunity
                dt = objOpp.GetOpenRecords()
                'ddl.DataTextField = "vcPOppName"
                'ddl.DataValueField = "numOppId"
                'ddl.DataBind()
                'ddl.Items.Insert(0, "--Select One--")
                'ddl.Items.FindByText("--Select One--").Value = "0"
            Case 2
                objOpp.Mode = 2
                dt = objOpp.GetOpenRecords()
                'ddl.DataTextField = "vcPOppName"
                'ddl.DataValueField = "numOppId"
                'ddl.DataBind()
                'ddl.Items.Insert(0, "--Select One--")
                'ddl.Items.FindByText("--Select One--").Value = "0"
            Case 3
                objOpp.Mode = 3
                dt = objOpp.GetOpenRecords()
                'ddl.DataTextField = "vcPOppName"
                'ddl.DataValueField = "numOppId"
                'ddl.DataBind()
                'ddl.Items.Insert(0, "--Select One--")
                'ddl.Items.FindByText("--Select One--").Value = "0"
            Case 4
                objOpp.Mode = 4
                dt = objOpp.GetOpenRecords()
                'ddl.DataTextField = "vcPOppName"
                'ddl.DataValueField = "numOppId"
                'ddl.DataBind()
                'ddl.Items.Insert(0, "--Select One--")
                'ddl.Items.FindByText("--Select One--").Value = "0"
            Case 5
                Dim objProject As New Project
                objProject.DomainID = lngDomainID
                objProject.DivisionID = lngDivisionID
                'ddl.DataTextField = "vcProjectName"
                'ddl.DataValueField = "numProId"
                dt = objProject.GetOpenProject()
                'ddl.DataBind()
                'ddl.Items.Insert(0, "--Select One--")
                'ddl.Items.FindByText("--Select One--").Value = "0"
            Case 6
                Dim ObjCases As New BACRM.BusinessLogic.Case.CCases
                Dim dtCaseNo As DataTable
                ObjCases.DivisionID = lngDivisionID
                dtCaseNo = ObjCases.GetOpenCases
                'ddl.DataTextField = "vcCaseNumber"
                'ddl.DataValueField = "numCaseId1"
                dt = dtCaseNo
                'ddl.DataBind()
                'ddl.Items.Insert(0, "--Select One--")
                'ddl.Items.FindByText("--Select One--").Value = 0
        End Select
        Dim intCount As Integer = 0
        Dim str As New System.Text.StringBuilder
        str.Append("[")
        For intCount = 0 To dt.Rows.Count - 1
            str.Append("{")
            str.Append("""id"" : """ & dt.Rows(intCount)(dt.Columns(0).ColumnName) & """,")
            str.Append("""value"" : """ & dt.Rows(intCount)(dt.Columns(1).ColumnName) & """")
            str.Append("},")
        Next
        str.Remove(str.ToString.Length - 1, 1)
        str.Append("]")
        If dt.Rows.Count <= 0 Then
            Return ("[]")
        End If

        str.Remove(0, 1)
        str.Remove(str.Length - 1, 1)



        'strJson = strJson.ToString.Remove(0, 1)
        'strJson = strJson.ToString.Remove(strJson.ToString.Length - 1, 1)
        Return ("[" & str.ToString & "]")
    End Function
    <WebMethod(EnableSession:=True)>
    Public Function ManageCorrespondence(ByVal lngOpenRecord As Long, ByVal lngEmailHistoryid As Long, ByVal lngAssignval As Long)
        Try
            ''Add To Correspondense if Open record is selected
            If CCommon.ToLong(lngOpenRecord) > 0 Then
                Dim objActionItem As New ActionItem
                objActionItem.CorrespondenceID = 0
                objActionItem.CommID = 0
                objActionItem.EmailHistoryID = lngEmailHistoryid
                objActionItem.CorrType = lngAssignval
                objActionItem.OpenRecordID = lngOpenRecord
                objActionItem.DomainID = Session("DomainID")
                objActionItem.ManageCorrespondence()
            End If
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    '<WebMethod(Enablesession:=True)> _
    'Public Function SaveFilter(ByVal Panel1 As String, ByVal Panel2 As String, ByVal CustStage As String, ByVal BaseOn As String)
    '    Try

    '        Dim objContact As New CContacts
    '        objContact.DomainID = Session("DomainID")
    '        objContact.UserCntID = Session("UserContactID")
    '        objContact.FormId = 47
    '        objContact.GroupID = 2
    '        objContact.ContactType = 0
    '        objContact.bitDefault = False
    '        objContact.blnFlag = True
    '        Dim str As String = Panel1 & "~" & Panel2 & "~" & CustStage & "~" & BaseOn
    '        objContact.strFilter = str.ToString
    '        objContact.SaveDefaultFilter()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    <WebMethod(EnableSession:=True)>
    Public Function GetItemWareHouse(ByVal lngItemCode As Long)
        Try
            Dim objItems As New CItems
            Dim ds As DataSet

            objItems.ItemCode = lngItemCode
            ds = objItems.GetItemWareHouses()

            Dim dtTable, dtTable1 As DataTable
            dtTable = ds.Tables(0)

            Dim json As String = String.Empty

            json = JsonConvert.SerializeObject(ds, Formatting.None)

            Return json
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function SaveGridColumnWidth(ByVal str As String)
        Try
            If str.Length > 0 Then
                Dim ds As New DataSet
                Dim dtTable As New DataTable
                Dim dr As DataRow

                dtTable.TableName = "GridColumnWidth"

                dtTable.Columns.Add("numFormId")
                dtTable.Columns.Add("numFieldId")
                dtTable.Columns.Add("bitCustom")
                dtTable.Columns.Add("intColumnWidth")

                Dim strValues() As String = str.Trim(";").Split(";")
                Dim strIDWidth(), strID() As String

                For i As Integer = 0 To strValues.Length - 1
                    dr = dtTable.NewRow
                    strIDWidth = strValues(i).Split(":")

                    strID = strIDWidth(0).Split("~")
                    If strID.Length > 1 AndAlso CCommon.ToLong(strID(1)) > 0 Then
                        dr("numFormId") = strID(0)
                        dr("numFieldId") = strID(1)
                        dr("bitCustom") = strID(2)

                        dr("intColumnWidth") = strIDWidth(1)

                        dtTable.Rows.Add(dr)
                    End If
                Next

                ds.Tables.Add(dtTable)

                Dim objCommon As New CCommon

                objCommon.Str = ds.GetXml
                objCommon.UserCntID = Context.Session("UserContactID")
                objCommon.DomainID = Context.Session("DomainID")
                objCommon.SaveGridColumnWidth()
            End If
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function SaveAdvanceSearchGridColumnWidth(ByVal str As String, ByVal viewID As String)
        Try
            If str.Length > 0 Then
                Dim ds As New DataSet
                Dim dtTable As New DataTable
                Dim dr As DataRow

                dtTable.TableName = "GridColumnWidth"

                dtTable.Columns.Add("numFormId")
                dtTable.Columns.Add("numFieldId")
                dtTable.Columns.Add("bitCustom")
                dtTable.Columns.Add("intColumnWidth")

                Dim strValues() As String = str.Trim(";").Split(";")
                Dim strIDWidth(), strID() As String

                For i As Integer = 0 To strValues.Length - 1
                    dr = dtTable.NewRow
                    strIDWidth = strValues(i).Split(":")

                    strID = strIDWidth(0).Split("~")
                    If strID.Length > 1 AndAlso CCommon.ToLong(strID(1)) > 0 Then
                        dr("numFormId") = strID(0)
                        dr("numFieldId") = strID(1)
                        dr("bitCustom") = strID(2)

                        dr("intColumnWidth") = strIDWidth(1)

                        dtTable.Rows.Add(dr)
                    End If
                Next

                ds.Tables.Add(dtTable)

                Dim objCommon As New CCommon

                objCommon.Str = ds.GetXml
                objCommon.UserCntID = Context.Session("UserContactID")
                objCommon.DomainID = Context.Session("DomainID")
                objCommon.SaveAdvanceSearchGridColumnWidth(CCommon.ToInteger(viewID))
            End If
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function EmailRecentCorrespondanceCount(ByVal ContactId As Long)
        Try
            Dim objCommon As New CCommon
            objCommon.UserCntID = Context.Session("UserContactID")
            objCommon.DomainID = Context.Session("DomainID")
            objCommon.Str = ContactId
            objCommon.Mode = 26
            Dim CountTotal As Integer = 0
            CountTotal = CCommon.ToInteger(objCommon.GetSingleFieldValue())

            Return CountTotal
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function EmailAlertPanel(ByVal ContactId As Long)
        Try
            Dim objCommon As New CCommon
            objCommon.UserCntID = Context.Session("UserContactID")
            objCommon.DomainID = Context.Session("DomainID")
            objCommon.Str = ContactId
            objCommon.Mode = 25
            Dim vcMsg As String = ""
            vcMsg = CCommon.ToString(objCommon.GetSingleFieldValue())

            Return vcMsg
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function UpdateBizDocStatus(ByVal lngOppID As Long, ByVal lngOppBizDocsId As Long, ByVal lngBizDocStatus As Long)
        Try
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = lngOppBizDocsId
            objOppBizDocs.DomainID = Context.Session("DomainID")
            objOppBizDocs.OppId = lngOppID
            objOppBizDocs.UserCntID = Context.Session("UserContactID")
            objOppBizDocs.BizDocStatus = lngBizDocStatus
            objOppBizDocs.OpportunityBizDocStatusChange()

            ''Added By Sachin Sadhu||Date:5thMay2014
            ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
            ''          Using Change tracking
            Dim objWfA As New Workflow()
            objWfA.DomainID = Context.Session("DomainID")
            objWfA.UserCntID = Context.Session("UserContactID")
            objWfA.RecordID = lngOppBizDocsId
            objWfA.SaveWFBizDocQueue()
            'end of code

            Return ("Success : BizDic Status updated successfully!")
        Catch ex As Exception
            If ex.Message = "NOT_ALLOWED_ChangeBizDicStatus" Then
                Return "Error : Already in Automation Process"
            Else
                Return (ex)
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function UpdateOrderStatus(ByVal lngOppID As Long, ByVal lngBizDocStatus As Long)
        Try
            Dim objOpp As New MOpportunity
            objOpp.DomainID = Context.Session("DomainID")
            objOpp.OpportunityId = lngOppID
            objOpp.UserCntID = Context.Session("UserContactID")
            objOpp.OrderStatus = lngBizDocStatus
            objOpp.UpdateOrderStatus()

            ''Added By Sachin Sadhu||Date:5thMay2014
            ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
            ''          Using Change tracking
            Dim objWfA As New Workflow()
            objWfA.DomainID = Context.Session("DomainID")
            objWfA.UserCntID = Context.Session("UserContactID")
            objWfA.RecordID = lngOppID
            objWfA.SaveWFOrderQueue()
            'end of code

            Return ("Success : Order Status updated successfully!")
        Catch ex As Exception
            Return (ex)
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function UpdateBankReconStatus(ByVal TransactionId As Long, ByVal Status As String)
        Try
            Dim objCommon As New CCommon
            objCommon.Mode = 28
            objCommon.UpdateRecordID = TransactionId
            objCommon.Comments = Status
            objCommon.DomainID = Context.Session("DomainID")
            objCommon.UpdateSingleFieldValue()

            Return ("Bank Recon Status updated successfully!")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetWFFormFieldList(ByVal numFormID As Long) As String
        Try
            Dim objWF As New BACRM.BusinessLogic.Workflow.Workflow
            objWF.DomainID = Session("DomainID")
            objWF.FormID = numFormID

            Dim dtFieldsList As DataTable
            dtFieldsList = objWF.GetWorkFlowFormFieldMaster()

            Dim strData As String = ""
            strData = JsonConvert.SerializeObject(dtFieldsList, Formatting.None)

            Return strData
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetActionItemTemplateList() As String
        Try
            Dim objActionItem As New ActionItem
            Dim dtTable As DataTable
            objActionItem.DomainID = Context.Session("DomainID")
            objActionItem.UserCntID = 0
            dtTable = objActionItem.LoadActionItemTemplateData()

            Dim strData As String = ""
            strData = JsonConvert.SerializeObject(dtTable, Formatting.None)

            Return strData
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetEmailTemplateList() As String
        Try
            Dim objCampaign As New Campaign
            Dim dtTable As DataTable

            objCampaign.DomainID = Session("DomainID")
            dtTable = objCampaign.GetUserEmailTemplates

            Dim strData As String = ""
            strData = JsonConvert.SerializeObject(dtTable, Formatting.None)

            Return strData
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Added by sandeep to search items in new sales order form
    ''' <summary>
    ''' Gets the list of item based on serch text
    ''' </summary>
    ''' <param name="seachText">search term</param>
    ''' <param name="pageIndex">current page</param>
    ''' <param name="pageSize">page size</param>
    ''' <param name="divisionId">division id</param>
    ''' <param name="isGetDefaultColumn">If is ture then return value will be json of default column needs to displyed in search dropdown otherwise return searched items in json format</param>
    ''' <returns>json</returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetSearchedItems(ByVal searchText As String, ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal divisionId As Integer, ByVal isGetDefaultColumn As Boolean, ByVal searchOrdCusHistory As Integer, ByVal warehouseID As Integer, ByVal oppType As Integer, ByVal isTransferTo As Boolean, ByVal IsCustomerPartSearch As Boolean, ByVal searchType As String) As String
        Try
            Dim json As String = String.Empty

            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
            Dim totalCount As Integer = 0
            If searchText <> "" Then
                Dim objCommon As CCommon
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.UserCntID = Session("UserContactID")
                If IsCustomerPartSearch = True Then
                    searchOrdCusHistory = 1
                End If
                Dim strDiv As String = divisionId.ToString()
                Dim ds As DataSet

                If strDiv <> "" Then
                    ds = objCommon.SearchItem(Session("DomainID"), strDiv, oppType, searchText, pageIndex, pageSize, totalCount, warehouseID, isTransferTo, searchOrdCusHistory.ToString(), IsCustomerPartSearch, searchType:=searchType)
                Else
                    ds = objCommon.SearchItem(Session("DomainID"), 0, oppType, searchText, pageIndex, pageSize, totalCount, warehouseID, isTransferTo, searchOrdCusHistory.ToString(), IsCustomerPartSearch, searchType:=searchType)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    If isGetDefaultColumn Then
                        json = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None)
                    Else
                        json = JsonConvert.SerializeObject(New With {Key .results = GetJson(ds.Tables(0), isGetDefaultColumn), Key .Total = totalCount}, Formatting.None)
                    End If
                End If
            End If

            Return json
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If

        End Try
    End Function
    Private Function GetJson(ByRef dt As DataTable, ByVal isGetDefaultColumn As Boolean) As String
        Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim rows As New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object) = Nothing
        If Not isGetDefaultColumn Then
            dt.Columns.Add("id")
            dt.Columns.Add("text")
        End If
        For Each dr As DataRow In dt.Rows
            If Not isGetDefaultColumn Then

                dr("id") = CCommon.ToString(dr("numItemCode")) + "-" + CCommon.ToString(dr("numWareHouseItemID"))
                dr("text") = dr("vcItemName")

                If Not dr("vcPathForTImage") Is DBNull.Value AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dr("vcPathForTImage"))) Then
                    dr("vcPathForTImage") = CCommon.GetDocumentPath(Session("DomainID")) + dr("vcPathForTImage")
                End If
            End If
            row = New Dictionary(Of String, Object)()
            For Each col As DataColumn In dt.Columns
                row.Add(col.ColumnName.Trim(), dr(col))
            Next
            rows.Add(row)
        Next
        Return serializer.Serialize(rows)
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function ArchiveEmail(ByVal strUpdateRecordId As String)
        Try
            Dim m_aryRightsForPage() As Integer
            Dim objCommon As New CCommon

            Dim objOutlook As New COutlook
            objOutlook.DomainID = CCommon.ToLong(Session("DomainID"))
            objOutlook.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objOutlook.ArchiveEmail(strUpdateRecordId.Trim(","))

            Return ("Selected emails are archived successfully!")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function Restore(ByVal strUpdateRecordId As String)
        Try
            Dim m_aryRightsForPage() As Integer
            Dim objCommon As New CCommon

            Dim objOutlook As New COutlook
            objOutlook.DomainID = CCommon.ToLong(Session("DomainID"))
            objOutlook.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objOutlook.Restore(strUpdateRecordId.Trim(","))

            Return ("Selected emails are restored!")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function DeleteArchivedMails(ByVal fromDate As Date, ByVal toDate As Date)
        Try
            Dim m_aryRightsForPage() As Integer
            Dim objCommon As New CCommon

            Dim objOutlook As New COutlook
            objOutlook.DomainID = CCommon.ToLong(Session("DomainID"))
            objOutlook.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objOutlook.DeleteArchived(fromDate, toDate)

            Return ("Archived emails are delete successfully!")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function MarkAsSpam(ByVal strUpdateRecordId As String)
        Try
            Dim m_aryRightsForPage() As Integer
            Dim objCommon As New CCommon

            Dim objOutlook As New COutlook
            objOutlook.DomainID = CCommon.ToLong(Session("DomainID"))
            objOutlook.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objOutlook.MarkAsSpam(strUpdateRecordId.Trim(","))

            Return ("Selected emails are removed from biz and moved to spam in gmail!")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetPOAvailableForMerge()
        Try
            Dim objOpp As New COpportunities
            objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dt As DataTable = objOpp.GetPOAvailableForMerge(0, 0, 0)

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                Return JsonConvert.SerializeObject(1, Formatting.None)
            Else
                Return JsonConvert.SerializeObject(0, Formatting.None)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Returns all item except serial and lot because we are not allowing direct return of serial/lot item without sales order
    ''' </summary>
    ''' <param name="context"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True)>
    Public Function GetItemNamesForReturn(ByVal context As RadComboBoxContext) As RadComboBoxData
        Try

            If context.Text <> "" And Len(context.Text) >= IIf(Session("ChrForItemSearch") = 0, 1, Session("ChrForItemSearch")) Then
                Dim objCommon As CCommon
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.UserCntID = Session("UserContactID")

                Dim OppType As Short = context("OppType").ToString()
                Dim strDiv As String = context("Division").ToString()
                Dim ds As DataSet
                'If OppType = 1 Or (Session("RemoveVendorPOValidation") = True AndAlso OppType = 2) Then 'Remove Vendor validation when creating new Purchase Orders
                If OppType = 1 Then
                    If strDiv <> "" Then
                        ds = objCommon.NewOppItemsForReturn(Session("DomainID"), strDiv, 1, context.Text, context("SearchOrderCustomerHistory").ToString())
                        Return AddItems(ds, context.NumberOfItems)
                    Else
                        ds = objCommon.NewOppItemsForReturn(Session("DomainID"), 0, 1, context.Text, context("SearchOrderCustomerHistory").ToString())
                        Return AddItems(ds, context.NumberOfItems)
                    End If
                ElseIf OppType = 2 Then
                    If strDiv <> "" Then
                        ds = objCommon.NewOppItemsForReturn(Session("DomainID"), strDiv, 2, context.Text, context("SearchOrderCustomerHistory").ToString())
                        Return AddItems(ds, context.NumberOfItems)
                    Else
                        ds = objCommon.NewOppItemsForReturn(Session("DomainID"), 0, 2, context.Text, context("SearchOrderCustomerHistory").ToString()) '1 is replaced by 2-reason:Not allowing one to create PO for which vendor is not mapped.
                        Return AddItems(ds, context.NumberOfItems)
                    End If
                End If
            Else
                Dim radcmbdata As New RadComboBoxData
                Dim itemData As RadComboBoxItemData
                itemData = New RadComboBoxItemData
                Dim result As New List(Of RadComboBoxItemData)(1)
                itemData.Text = ""
                itemData.Value = ""
                result.Add(itemData)
                radcmbdata.Items = result.ToArray()
                Return radcmbdata
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function ClearDemandForecastSession()
        Try
            Session("DFItemsToPurchase") = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SimpleElasticSearch(ByVal searchText As String, ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal startsWithSearch As String, ByVal emailSearch As Boolean) As String
        Try
            searchText = CCommon.ToString(searchText).Replace("/", "\/")
            'searchText = searchText.Replace(" ", "\\ ")
            Dim totalRecords As Integer = 0
            Dim containsSearch As Boolean = True
            Dim json As String = String.Empty
            Dim index As String = Session("DomainID") & "_*"
            Dim isModuleSearch As Boolean = False
            Dim tempSearchText As String = searchText
            Dim dateRange As String
            Dim fromDate As Date
            Dim toDate As Date
            Dim fromNumber As Double
            Dim toNumber As Double
            Dim isDate As Boolean = False
            Dim isNumaber As Boolean = False

            If searchText.Length > 0 AndAlso searchText.IndexOf("[") > 0 AndAlso searchText.IndexOf("]") > 0 AndAlso (searchText.IndexOf("]") > searchText.IndexOf("[")) Then
                dateRange = searchText.Substring(searchText.IndexOf("["), searchText.IndexOf("]") - searchText.IndexOf("[") + 1)
                searchText = searchText.Replace(dateRange, "").Trim()
                dateRange = dateRange.ToUpper()

                If dateRange.ToUpper().Split(" TO ").Length = 3 Then
                    If Date.TryParse(dateRange.ToUpper().Split(" TO ")(0).Trim().Replace("[", ""), fromDate) AndAlso Date.TryParse(dateRange.ToUpper().Split(" TO ")(2).Trim().Replace("]", ""), toDate) Then
                        isDate = True
                    ElseIf Double.TryParse(dateRange.ToUpper().Split(" TO ")(0).Trim().Replace("[", ""), fromNumber) AndAlso Double.TryParse(dateRange.ToUpper().Split(" TO ")(2).Trim().Replace("]", ""), toNumber) Then
                        isNumaber = True
                    End If
                End If
            End If

            If CCommon.ToLong(Session("DomainID")) > 0 Then
                If emailSearch Then
                    If CCommon.ToString(searchText).Length > 0 Or CCommon.ToString(dateRange).Length > 0 Then
                        index = Session("DomainID") & "_email"
                        Dim settings = New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL"))).EnableDebugMode()
                        Dim client = New Nest.ElasticClient(settings)
                        Dim funcMust = New List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer))()
                        If Not dateRange Is Nothing AndAlso dateRange.Length > 0 Then
                            If isDate Then
                                funcMust.Add(Function(sh) sh.QueryString(Function(x) x.Fields("SearchDate_*").Query(dateRange)))
                            ElseIf isNumaber Then
                                funcMust.Add(Function(sh) sh.QueryString(Function(x) x.Fields("SearchNumber_*").Query(dateRange)))
                            End If
                        End If

                        funcMust.Add(Function(sh) sh.Term("numUserCntId", Session("UserContactID")))

                        Dim fields() As String = {"id", "text", "displaytext", "url", "module", "Search_vcSubject", "Search_vcBodyText", "Search_vcFromEmail", "Search_vcToEmail", "SearchDate_dtReceivedOn"}

                        Dim resp = Nothing
                        If startsWithSearch = "2" Then
                            resp = client.Search(Of Object)(Function(s) s.Index(index).IgnoreUnavailable(True) _
                            .Query(Function(qry) qry.QueryString(Function(qs) qs.Fields("Search_*").Analyzer("standard").Query(searchText & "*").DefaultOperator(Nest.Operator.And))) _
                            .Sort(Function(qry) qry.Descending(New Nest.Field("dtReceivedOn"))) _
                            .PostFilter(Function(pf) pf.Bool(Function(b) b.Must(funcMust))).From((pageIndex - 1) * pageSize).Take(pageSize).Source(Function(x) x.Includes(Function(f) f.Fields(fields))))
                        ElseIf startsWithSearch = "3" Then
                            resp = client.Search(Of Object)(Function(s) s.Index(index).IgnoreUnavailable(True) _
                            .Query(Function(qry) qry.QueryString(Function(qs) qs.Fields("Search_*").Analyzer("standard").Query("*" & searchText).DefaultOperator(Nest.Operator.And))) _
                            .Sort(Function(qry) qry.Descending(New Nest.Field("dtReceivedOn"))) _
                            .PostFilter(Function(pf) pf.Bool(Function(b) b.Must(funcMust))).From((pageIndex - 1) * pageSize).Take(pageSize).Source(Function(x) x.Includes(Function(f) f.Fields(fields))))
                        Else
                            resp = client.Search(Of Object)(Function(s) s.Index(index).IgnoreUnavailable(True) _
                            .Query(Function(qry) qry.QueryString(Function(qs) qs.Fields("Search_*").Analyzer("standard").Query("*" & searchText & "*").DefaultOperator(Nest.Operator.And))) _
                            .Sort(Function(qry) qry.Descending(New Nest.Field("dtReceivedOn"))) _
                            .PostFilter(Function(pf) pf.Bool(Function(b) b.Must(funcMust))).From((pageIndex - 1) * pageSize).Take(pageSize).Source(Function(x) x.Includes(Function(f) f.Fields(fields))))
                        End If

                        json = JsonConvert.SerializeObject(resp.Documents, Formatting.None)
                        totalRecords = resp.Total
                    End If

                    'json = File.ReadAllText(Server.MapPath("~/sample.json"))

                    Return JsonConvert.SerializeObject(New With {Key .results = json, Key .Total = totalRecords}, Formatting.None)
                Else
                    Dim func = New List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer))()
                    Dim funcMust = New List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer))()
                    Dim funcMustNot = New List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer))()
                    If searchText.ToLower().StartsWith("or:") Or searchText.ToLower().StartsWith("cu:") Or searchText.ToLower().StartsWith("ve:") Then
                        isModuleSearch = True
                        index = Session("DomainID") & "_organization"
                        searchText = searchText.Substring(3, searchText.Length - 3).Trim()
                    ElseIf searchText.ToLower().StartsWith("it:") Then
                        isModuleSearch = True
                        index = Session("DomainID") & "_item"
                        searchText = searchText.Substring(3, searchText.Length - 3).Trim()
                    ElseIf searchText.ToLower().StartsWith("so:") Or searchText.ToLower().StartsWith("po:") Or searchText.ToLower().StartsWith("op:") Then
                        isModuleSearch = True
                        index = Session("DomainID") & "_opporder"
                        searchText = searchText.Substring(3, searchText.Length - 3).Trim()
                    ElseIf searchText.ToLower().StartsWith("co:") Then
                        isModuleSearch = True
                        index = Session("DomainID") & "_contact"
                        searchText = searchText.Substring(3, searchText.Length - 3).Trim()
                    ElseIf searchText.ToLower().StartsWith("rm:") Then
                        isModuleSearch = True
                        index = Session("DomainID") & "_return"
                        searchText = searchText.Substring(3, searchText.Length - 3).Trim()
                    ElseIf searchText.ToLower().StartsWith("bd:") Then
                        isModuleSearch = True
                        index = Session("DomainID") & "_bizdoc"
                        searchText = searchText.Substring(3, searchText.Length - 3).Trim()
                    ElseIf searchText.ToLower().StartsWith("ca:") Then
                        isModuleSearch = True
                        index = Session("DomainID") & "_case"
                        searchText = searchText.Substring(3, searchText.Length - 3).Trim()
                    ElseIf searchText.ToLower().StartsWith("kb:") Then
                        isModuleSearch = True
                        index = Session("DomainID") & "_knowledgebase"
                        searchText = searchText.Substring(3, searchText.Length - 3).Trim()
                    ElseIf searchText.ToLower().StartsWith("pj:") Then
                        isModuleSearch = True
                        index = Session("DomainID") & "_project"
                        searchText = searchText.Substring(3, searchText.Length - 3).Trim()
                    ElseIf searchText.ToLower().StartsWith("ai:") Then
                        isModuleSearch = True
                        index = Session("DomainID") & "_actionitem"
                        searchText = searchText.Substring(3, searchText.Length - 3).Trim()
                    ElseIf searchText.ToLower().StartsWith("gl:") Then
                        isModuleSearch = True
                        index = Session("DomainID") & "_generalledger"
                        searchText = searchText.Substring(3, searchText.Length - 3).Trim()
                    End If

                    Dim listTerritory As New List(Of Long)
                    Dim dt As DataTable = Session("UserTerritory")

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        For Each dr As DataRow In dt.Rows
                            listTerritory.Add(dr("numTerritoryId"))
                        Next
                    End If

                    If Not dateRange Is Nothing AndAlso dateRange.Length > 0 Then
                        If isDate Then
                            funcMust.Add(Function(sh) sh.QueryString(Function(x) x.Fields("SearchDate_*").Query(dateRange)))
                        ElseIf isNumaber Then
                            funcMust.Add(Function(sh) sh.QueryString(Function(x) x.Fields("SearchNumber_*").Query(dateRange)))
                        End If
                    End If

                    SetOrganizationFilters(isModuleSearch, listTerritory, tempSearchText, func, funcMust, funcMustNot)
                    SetContactFilters(isModuleSearch, listTerritory, tempSearchText, func, funcMust, funcMustNot)
                    SetOppOrderFilters(isModuleSearch, listTerritory, tempSearchText, func, funcMust, funcMustNot)
                    SetReturnFilters(isModuleSearch, listTerritory, tempSearchText, func, funcMust, funcMustNot)
                    SetBizDocFilters(isModuleSearch, listTerritory, tempSearchText, func, funcMust, funcMustNot)
                    SetItemFilters(isModuleSearch, listTerritory, tempSearchText, func, funcMust, funcMustNot)
                    SetCaseFilters(isModuleSearch, listTerritory, tempSearchText, func, funcMust, funcMustNot)
                    SetKnowledgeBaseFilters(isModuleSearch, listTerritory, tempSearchText, func, funcMust, funcMustNot)
                    SetProjectFilters(isModuleSearch, listTerritory, tempSearchText, func, funcMust, funcMustNot)
                    SetGeneralLedgerFilters(isModuleSearch, listTerritory, tempSearchText, func, funcMust, funcMustNot)
                    SetActionItemFilters(isModuleSearch, listTerritory, tempSearchText, func, funcMust, funcMustNot)


                    If CCommon.ToString(searchText).Length > 0 Or CCommon.ToString(dateRange).Length > 0 Then


                        Dim settings = New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                        Dim client = New Nest.ElasticClient(settings.EnableDebugMode())

                        Dim indexOrganization As New Nest.IndexName
                        indexOrganization.Name = Session("DomainID") & "_organization"

                        Dim indexContact As New Nest.IndexName
                        indexContact.Name = Session("DomainID") & "_contact"

                        Dim indexItem As New Nest.IndexName
                        indexItem.Name = Session("DomainID") & "_item"

                        Dim indexOppOrder As New Nest.IndexName
                        indexOppOrder.Name = Session("DomainID") & "_opporder"

                        Dim indexReturn As New Nest.IndexName
                        indexReturn.Name = Session("DomainID") & "_return"

                        Dim indexBizDoc As New Nest.IndexName
                        indexBizDoc.Name = Session("DomainID") & "_bizdoc"

                        Dim indexCase As New Nest.IndexName
                        indexCase.Name = Session("DomainID") & "_case"

                        Dim indexKnowledgeBase As New Nest.IndexName
                        indexKnowledgeBase.Name = Session("DomainID") & "_knowledgebase"

                        Dim indexProject As New Nest.IndexName
                        indexProject.Name = Session("DomainID") & "_project"

                        Dim indexGeneralLedger As New Nest.IndexName
                        indexGeneralLedger.Name = Session("DomainID") & "_generalledger"

                        Dim indexActionItem As New Nest.IndexName
                        indexActionItem.Name = Session("DomainID") & "_actionitem"

                        Dim fields() As String = {"id", "text", "displaytext", "url", "module", "numOppId"}

                        Dim resp = Nothing

                        If startsWithSearch = "2" Then
                            resp = client.Search(Of Object)(Function(s) s.Index(index).IgnoreUnavailable(True).IndicesBoost(Function(b) b.Add(indexOrganization, 11.0).Add(indexContact, 10.0).Add(indexOppOrder, 9.0).Add(indexReturn, 8.0).Add(indexBizDoc, 7.0).Add(indexItem, 6.0).Add(indexCase, 5.0).Add(indexKnowledgeBase, 4.0).Add(indexProject, 3.0).Add(indexGeneralLedger, 2.0).Add(indexActionItem, 1.0)) _
                                        .Query(Function(qry) qry.QueryString(Function(qs) qs.Fields("Search_*").Analyzer("standard").Query(searchText & "*").DefaultOperator(Nest.Operator.And).Escape(True))) _
                                        .PostFilter(Function(pf) pf.Bool(Function(b) b.Must(funcMust).MustNot(funcMustNot).Should(func))).From((pageIndex - 1) * pageSize).Take(pageSize).Source(Function(x) x.Includes(Function(f) f.Fields(fields))))
                        ElseIf startsWithSearch = "3" Then
                            resp = client.Search(Of Object)(Function(s) s.Index(index).IgnoreUnavailable(True).IndicesBoost(Function(b) b.Add(indexOrganization, 11.0).Add(indexContact, 10.0).Add(indexOppOrder, 9.0).Add(indexReturn, 8.0).Add(indexBizDoc, 7.0).Add(indexItem, 6.0).Add(indexCase, 5.0).Add(indexKnowledgeBase, 4.0).Add(indexProject, 3.0).Add(indexGeneralLedger, 2.0).Add(indexActionItem, 1.0)) _
                                        .Query(Function(qry) qry.QueryString(Function(qs) qs.Fields("Search_*").Analyzer("standard").Query("*" & searchText).DefaultOperator(Nest.Operator.And).Escape(True))) _
                                        .PostFilter(Function(pf) pf.Bool(Function(b) b.Must(funcMust).MustNot(funcMustNot).Should(func))).From((pageIndex - 1) * pageSize).Take(pageSize).Source(Function(x) x.Includes(Function(f) f.Fields(fields))))
                        Else
                            resp = client.Search(Of Object)(Function(s) s.Index(index).IgnoreUnavailable(True).IndicesBoost(Function(b) b.Add(indexOrganization, 11.0).Add(indexContact, 10.0).Add(indexOppOrder, 9.0).Add(indexReturn, 8.0).Add(indexBizDoc, 7.0).Add(indexItem, 6.0).Add(indexCase, 5.0).Add(indexKnowledgeBase, 4.0).Add(indexProject, 3.0).Add(indexGeneralLedger, 2.0).Add(indexActionItem, 1.0)) _
                                    .Query(Function(qry) qry.QueryString(Function(qs) qs.Fields("Search_*").Analyzer("standard").Query("*" & searchText & "*").DefaultOperator(Nest.Operator.And).Escape(True))) _
                                    .PostFilter(Function(pf) pf.Bool(Function(b) b.Must(funcMust).MustNot(funcMustNot).Should(func))).From((pageIndex - 1) * pageSize).Take(pageSize).Source(Function(x) x.Includes(Function(f) f.Fields(fields))))
                            'ExceptionModule.ExceptionPublish(resp.ApiCall.DebugInformation, 0, 0, 0, Nothing)
                        End If

                        'If Session("DomainID") = "226" Then
                        '    ExceptionModule.ExceptionPublish(resp.ApiCall.DebugInformation, 0, 0, 0, Nothing)
                        'End If

                        json = JsonConvert.SerializeObject(resp.Documents, Formatting.None)
                        totalRecords = resp.Total
                    End If

                    Return JsonConvert.SerializeObject(New With {Key .results = json, Key .Total = totalRecords}, Formatting.None)
                End If
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If
        End Try
    End Function


    Private Sub SetOrganizationFilters(ByVal isModuleSearch As Boolean, ByVal listTerritory As List(Of Long), ByVal tempSearchText As String, ByRef func As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMust As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMustNot As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)))
        Try
            If isModuleSearch = False Or (isModuleSearch AndAlso (tempSearchText.ToLower().StartsWith("or:") Or tempSearchText.ToLower().StartsWith("cu:") Or tempSearchText.ToLower().StartsWith("ve:"))) Then
                Dim leadsRights As Short = CCommon.ToShort(Session("tintLeadRights"))
                Dim prospectsRights As Short = CCommon.ToShort(Session("tintProspectRights"))
                Dim accountRights As Short = CCommon.ToShort(Session("tintAccountRights"))

                If leadsRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 0))
                ElseIf leadsRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 0) AndAlso (sh.Term("numOrgAssignedTo", Session("UserContactID")) Or sh.Term("numOrgRecOwner", Session("UserContactID"))))
                ElseIf leadsRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 0) AndAlso (sh.Term("numOrgAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numOrgTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 0))
                End If

                If prospectsRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 1))
                ElseIf prospectsRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 1) AndAlso (sh.Term("numOrgAssignedTo", Session("UserContactID")) Or sh.Term("numOrgRecOwner", Session("UserContactID"))))
                ElseIf prospectsRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 1) AndAlso (sh.Term("numOrgAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numOrgTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 1))
                End If

                If accountRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 2))
                ElseIf accountRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 2) AndAlso (sh.Term("numOrgAssignedTo", Session("UserContactID")) Or sh.Term("numOrgRecOwner", Session("UserContactID"))))
                ElseIf accountRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 2) AndAlso (sh.Term("numOrgAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numOrgTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("tintOrgCRMType", 2))
                End If

                'GET RELATIONSHIPS
                Dim objCommon As New CCommon
                Dim dtRelationships As DataTable = objCommon.GetMasterListItems(5, Session("DomainID"))
                If Not dtRelationships Is Nothing AndAlso dtRelationships.Rows.Count > 0 Then
                    For Each drRelationship As DataRow In dtRelationships.Rows
                        If CCommon.ToLong(drRelationship("numListItemID")) <> 46 Then
                            objCommon.UserID = CCommon.ToLong(Session("UserID"))
                            objCommon.ModuleID = 32
                            objCommon.PageID = CCommon.ToLong(drRelationship("numListItemID"))
                            Dim m_aryRights() As Integer = objCommon.PageLevelUserRights()

                            If m_aryRights.Length > 0 Then
                                If m_aryRights(RIGHTSTYPE.VIEW) = 0 Then 'Have No Rights To View
                                    funcMustNot.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("numOrgCompanyType", CCommon.ToLong(drRelationship("numListItemID"))))
                                ElseIf m_aryRights(RIGHTSTYPE.VIEW) = 1 Then
                                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("numOrgCompanyType", CCommon.ToLong(drRelationship("numListItemID"))) AndAlso (sh.Term("numOrgAssignedTo", Session("UserContactID")) Or sh.Term("numOrgRecOwner", Session("UserContactID"))))
                                ElseIf m_aryRights(RIGHTSTYPE.VIEW) = 2 Then
                                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("numOrgCompanyType", CCommon.ToLong(drRelationship("numListItemID"))) AndAlso (sh.Term("numOrgAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numOrgTerID").Terms(listTerritory))))
                                Else
                                    func.Add(Function(sh) sh.Term("module", "organization") AndAlso sh.Term("numOrgCompanyType", CCommon.ToLong(drRelationship("numListItemID"))))
                                End If
                            End If
                        End If
                    Next
                End If

                If tempSearchText.ToLower().StartsWith("cu:") Then
                    funcMust.Add(Function(sh) sh.Term("numOrgCompanyType", 46))
                ElseIf tempSearchText.ToLower().StartsWith("ve:") Then
                    funcMust.Add(Function(sh) sh.Term("numOrgCompanyType", 47))
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetContactFilters(ByVal isModuleSearch As Boolean, ByVal listTerritory As List(Of Long), ByVal tempSearchText As String, ByRef func As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMust As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMustNot As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)))
        Try
            If isModuleSearch = False Or (isModuleSearch AndAlso tempSearchText.ToLower().StartsWith("co:")) Then
                Dim contactListRights As Short = CCommon.ToShort(Session("tintContactListRights"))
                Dim leadsRights As Short = CCommon.ToShort(Session("tintLeadRights"))
                Dim prospectsRights As Short = CCommon.ToShort(Session("tintProspectRights"))
                Dim accountRights As Short = CCommon.ToShort(Session("tintAccountRights"))

                If contactListRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "contact"))
                ElseIf contactListRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "contact") AndAlso sh.Term("numRecOwner", Session("UserContactID")))
                ElseIf contactListRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "contact") AndAlso sh.Terms(Function(ts) ts.Field("numTerID").Terms(listTerritory)))
                Else
                    func.Add(Function(sh) sh.Term("module", "contact"))
                End If

                If leadsRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "contact") AndAlso sh.Term("tintOrgCRMType", 0))
                End If

                If prospectsRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "contact") AndAlso sh.Term("tintOrgCRMType", 1))
                End If

                If accountRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "contact") AndAlso sh.Term("tintOrgCRMType", 2))
                End If

                'GET RELATIONSHIPS
                Dim objCommon As New CCommon
                Dim dtRelationships As DataTable = objCommon.GetMasterListItems(5, Session("DomainID"))
                If Not dtRelationships Is Nothing AndAlso dtRelationships.Rows.Count > 0 Then
                    For Each drRelationship As DataRow In dtRelationships.Rows
                        If CCommon.ToLong(drRelationship("numListItemID")) <> 46 Then
                            objCommon.UserID = CCommon.ToLong(Session("UserID"))
                            objCommon.ModuleID = 32
                            objCommon.PageID = CCommon.ToLong(drRelationship("numListItemID"))
                            Dim m_aryRights() As Integer = objCommon.PageLevelUserRights()

                            If m_aryRights.Length > 0 Then
                                If m_aryRights(RIGHTSTYPE.VIEW) = 0 Then 'Have No Rights To View
                                    funcMustNot.Add(Function(sh) sh.Term("module", "contact") AndAlso sh.Term("numOrgCompanyType", CCommon.ToLong(drRelationship("numListItemID"))))
                                End If
                            End If
                        End If
                    Next
                End If

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetItemFilters(ByVal isModuleSearch As Boolean, ByVal listTerritory As List(Of Long), ByVal tempSearchText As String, ByRef func As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMust As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMustNot As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)))
        Try
            If isModuleSearch = False Or (isModuleSearch AndAlso tempSearchText.ToLower().StartsWith("it:")) Then
                func.Add(Function(sh) sh.Term("module", "item"))
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetOppOrderFilters(ByVal isModuleSearch As Boolean, ByVal listTerritory As List(Of Long), ByVal tempSearchText As String, ByRef func As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMust As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMustNot As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)))
        Try
            If isModuleSearch = False Or (isModuleSearch AndAlso (tempSearchText.ToLower().StartsWith("so:") Or tempSearchText.ToLower().StartsWith("po:") Or tempSearchText.ToLower().StartsWith("op:"))) Then
                Dim salesOrderListRights As Short = CCommon.ToShort(Session("tintSalesOrderListRights"))
                Dim purchaseOrderListRights As Short = CCommon.ToShort(Session("tintPurchaseOrderListRights"))
                Dim salesOpportunityListRights As Short = CCommon.ToShort(Session("tintSalesOpportunityListRights"))
                Dim purchaseOpportunityListRights As Short = CCommon.ToShort(Session("tintPurchaseOpportunityListRights"))


                If salesOrderListRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 1))
                ElseIf salesOrderListRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 1) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Term("numRecOwner", Session("UserContactID"))))
                ElseIf salesOrderListRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 1) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 1))
                End If

                If purchaseOrderListRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 1))
                ElseIf purchaseOrderListRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 1) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Term("numRecOwner", Session("UserContactID"))))
                ElseIf purchaseOrderListRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 1) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 1))
                End If

                If salesOpportunityListRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 0))
                ElseIf salesOpportunityListRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 0) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Term("numRecOwner", Session("UserContactID"))))
                ElseIf salesOpportunityListRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 0) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 0))
                End If

                If purchaseOpportunityListRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 0))
                ElseIf purchaseOpportunityListRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 0) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Term("numRecOwner", Session("UserContactID"))))
                ElseIf purchaseOpportunityListRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 0) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "order") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 0))
                End If


                If tempSearchText.ToLower().StartsWith("so:") Then
                    funcMust.Add(Function(sh) sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 1))
                ElseIf tempSearchText.ToLower().StartsWith("po:") Then
                    funcMust.Add(Function(sh) sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 1))
                ElseIf tempSearchText.ToLower().StartsWith("op:") Then
                    funcMust.Add(Function(sh) sh.Term("tintOppStatus", 0))
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetReturnFilters(ByVal isModuleSearch As Boolean, ByVal listTerritory As List(Of Long), ByVal tempSearchText As String, ByRef func As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMust As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMustNot As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)))
        Try
            If isModuleSearch = False Or (isModuleSearch AndAlso tempSearchText.ToLower().StartsWith("rm:")) Then
                func.Add(Function(sh) sh.Term("module", "return"))
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetBizDocFilters(ByVal isModuleSearch As Boolean, ByVal listTerritory As List(Of Long), ByVal tempSearchText As String, ByRef func As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMust As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMustNot As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)))
        Try
            If isModuleSearch = False Or (isModuleSearch AndAlso tempSearchText.ToLower().StartsWith("bd:")) Then
                Dim salesOrderListRights As Short = CCommon.ToShort(Session("tintSalesOrderListRights"))
                Dim purchaseOrderListRights As Short = CCommon.ToShort(Session("tintPurchaseOrderListRights"))
                Dim salesOpportunityListRights As Short = CCommon.ToShort(Session("tintSalesOpportunityListRights"))
                Dim purchaseOpportunityListRights As Short = CCommon.ToShort(Session("tintPurchaseOpportunityListRights"))


                If salesOrderListRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 1))
                ElseIf salesOrderListRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 1) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Term("numRecOwner", Session("UserContactID"))))
                ElseIf salesOrderListRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 1) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 1))
                End If

                If purchaseOrderListRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 1))
                ElseIf purchaseOrderListRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 1) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Term("numRecOwner", Session("UserContactID"))))
                ElseIf purchaseOrderListRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 1) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 1))
                End If

                If salesOpportunityListRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 0))
                ElseIf salesOpportunityListRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 0) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Term("numRecOwner", Session("UserContactID"))))
                ElseIf salesOpportunityListRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 0) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 1) AndAlso sh.Term("tintOppStatus", 0))
                End If

                If purchaseOpportunityListRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 0))
                ElseIf purchaseOpportunityListRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 0) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Term("numRecOwner", Session("UserContactID"))))
                ElseIf purchaseOpportunityListRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 0) AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "bizdoc") AndAlso sh.Term("tintOppType", 2) AndAlso sh.Term("tintOppStatus", 0))
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetCaseFilters(ByVal isModuleSearch As Boolean, ByVal listTerritory As List(Of Long), ByVal tempSearchText As String, ByRef func As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMust As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMustNot As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)))
        Try
            If isModuleSearch = False Or (isModuleSearch AndAlso tempSearchText.ToLower().StartsWith("ca:")) Then
                Dim caseListRights As Short = CCommon.ToShort(Session("tintCaseListRights"))

                If caseListRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "case"))
                ElseIf caseListRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "case") AndAlso (sh.Term("numRecOwner", Session("UserContactID")) Or sh.Term("numAssignedTo", Session("UserContactID"))))
                ElseIf caseListRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "case") AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "case"))
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetKnowledgeBaseFilters(ByVal isModuleSearch As Boolean, ByVal listTerritory As List(Of Long), ByVal tempSearchText As String, ByRef func As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMust As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMustNot As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)))
        Try
            If isModuleSearch = False Or (isModuleSearch AndAlso tempSearchText.ToLower().StartsWith("kb:")) Then
                func.Add(Function(sh) sh.Term("module", "knowledgebase"))
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetProjectFilters(ByVal isModuleSearch As Boolean, ByVal listTerritory As List(Of Long), ByVal tempSearchText As String, ByRef func As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMust As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMustNot As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)))
        Try
            If isModuleSearch = False Or (isModuleSearch AndAlso tempSearchText.ToLower().StartsWith("pj:")) Then
                Dim projectListRights As Short = CCommon.ToShort(Session("tintProjectListRights"))

                If projectListRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "project"))
                ElseIf projectListRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "project") AndAlso (sh.Term("numRecOwner", Session("UserContactID")) Or sh.Term("numAssignedTo", Session("UserContactID"))))
                ElseIf projectListRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "project") AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Terms(Function(ts) ts.Field("numTerID").Terms(listTerritory))))
                Else
                    func.Add(Function(sh) sh.Term("module", "project"))
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetActionItemFilters(ByVal isModuleSearch As Boolean, ByVal listTerritory As List(Of Long), ByVal tempSearchText As String, ByRef func As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMust As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMustNot As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)))
        Try
            If isModuleSearch = False Or (isModuleSearch AndAlso tempSearchText.ToLower().StartsWith("ai:")) Then
                Dim actionItemRights As Short = CCommon.ToShort(Session("tintActionItemRights"))

                If actionItemRights = 0 Then 'Have No Rights To View
                    funcMustNot.Add(Function(sh) sh.Term("module", "actionitem"))
                ElseIf actionItemRights = 1 Then
                    func.Add(Function(sh) sh.Term("module", "actionitem") AndAlso sh.Term("numAssignedTo", Session("UserContactID")))
                ElseIf actionItemRights = 2 Then
                    func.Add(Function(sh) sh.Term("module", "actionitem") AndAlso sh.Term("numRecOwner", Session("UserContactID")))
                Else
                    func.Add(Function(sh) sh.Term("module", "actionitem") AndAlso (sh.Term("numAssignedTo", Session("UserContactID")) Or sh.Term("numRecOwner", Session("UserContactID"))))
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetGeneralLedgerFilters(ByVal isModuleSearch As Boolean, ByVal listTerritory As List(Of Long), ByVal tempSearchText As String, ByRef func As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMust As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)), ByRef funcMustNot As List(Of Func(Of Nest.QueryContainerDescriptor(Of Object), Nest.QueryContainer)))
        Try
            If isModuleSearch = False Or (isModuleSearch AndAlso tempSearchText.ToLower().StartsWith("gl:")) Then
                func.Add(Function(sh) sh.Term("module", "generalledger"))
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    '<WebMethod(EnableSession:=True)> _
    'Public Function SimpleElasticSearch(ByVal context As RadComboBoxContext) As RadComboBoxData
    '    Try
    '        If CCommon.ToLong(Session("DomainID")) > 0 AndAlso context.Text.Trim <> "" Then
    '            Dim comboData As New RadComboBoxData()
    '            Dim offset As Integer = context.NumberOfItems
    '            Dim pageSize As Integer = CCommon.ToInteger(context("pageSize"))

    '            If pageSize = 1 Then
    '                pageSize = 20
    '            End If

    '            Dim searchText As String = context.Text.Trim

    '            If context("searchType") = 1 Then
    '                searchText = "*" & searchText & "*"
    '            Else
    '                searchText = searchText & "*"
    '            End If

    '            Dim settings = New Nest.ConnectionSettings(New Uri("http://localhost:9200"))
    '            Dim client = New Nest.ElasticClient(settings)
    '            Dim resp = client.Search(Of Object)(Function(s) s.Index(CCommon.ToLong(Session("DomainID")) & "_*").From(offset).Take(pageSize).Query(Function(qry) qry.QueryString(Function(qs) qs.Fields("_all").Query(searchText))))

    '            Dim endOffset As Integer = Math.Min(offset + pageSize, resp.Total)
    '            comboData.EndOfItems = endOffset = resp.Total


    '            Dim result As New List(Of RadComboBoxItemData)(endOffset - offset)

    '            For Each obj As Object In resp.Documents
    '                Dim itemData As New RadComboBoxItemData()
    '                itemData.Attributes.Add("displaytext", CCommon.ToString(obj("displaytext")))
    '                itemData.Attributes.Add("url", CCommon.ToString(obj("url")))
    '                itemData.Text = CCommon.ToString(obj("text"))
    '                itemData.Value = CCommon.ToLong(obj("id"))
    '                result.Add(itemData)
    '            Next

    '            comboData.Message = GetStatusMessage(offset, resp.Total)
    '            comboData.Items = result.ToArray()
    '            Return comboData
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Function
    <WebMethod(EnableSession:=True)>
    Public Function GetSalesOrders(ByVal context As RadComboBoxContext) As RadComboBoxData
        Dim items As New List(Of ComboBoxItemData)()
        If Not Session("DomainID") Is Nothing Then
            If context.Text.Trim <> "" Then
                Dim objCommon As New CCommon
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objCommon.SearchSalesOrder(context.Text.Trim)
                Return AddSalesOrders(dt, context.NumberOfItems)
            End If
        End If
    End Function
    Private Function AddSalesOrders(ByVal dt As DataTable, ByVal NoOfItemsRequested As Integer) As RadComboBoxData
        Try
            Dim radcmbdata As New RadComboBoxData

            Dim organizationsPerRequest As Integer = 10
            Dim itemOffset As Integer = NoOfItemsRequested
            Dim endOffset As Integer = itemOffset + organizationsPerRequest
            If endOffset > dt.Rows.Count Then
                endOffset = dt.Rows.Count
            End If
            If endOffset = dt.Rows.Count Then
                radcmbdata.EndOfItems = True
            Else
                radcmbdata.EndOfItems = False
            End If

            Dim result As New List(Of RadComboBoxItemData)(endOffset - itemOffset)
            'For Each row As DataRow In ds.Tables(0).Rows
            Dim organizationData As RadComboBoxItemData
            Dim row As DataRow
            For index As Int32 = itemOffset To endOffset - 1
                row = dt.Rows(index)
                organizationData = New RadComboBoxItemData()
                organizationData.Text = row("vcPOppName")
                organizationData.Value = row("numOppID")
                result.Add(organizationData)
            Next

            radcmbdata.Items = result.ToArray()

            If (dt.Rows.Count > 0) Then
                radcmbdata.Message = String.Format("Sales orders <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset.ToString(), dt.Rows.Count.ToString())
            Else
                radcmbdata.Message = "No matches"
            End If

            Return radcmbdata
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Shared Function GetStatusMessage(ByVal offset As Integer, ByVal total As Integer) As String
        If total <= 0 Then
            Return "No matches"
        End If

        Return [String].Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", offset, total)
    End Function


    <WebMethod(EnableSession:=True)>
    Public Function UpdateOppItemReleaseDate(ByVal oppID As Long, ByVal oppItemID As Long, ByVal releaseDate As String)
        Try
            Dim dtRelease As Date = Nothing
            If DateTime.TryParseExact(releaseDate, "MM/dd/yyyy", Nothing, System.Globalization.DateTimeStyles.None, dtRelease) Then
                Dim objOpportunityItemsReleaseDates As New OpportunityItemsReleaseDates
                objOpportunityItemsReleaseDates.DomainID = CCommon.ToLong(Session("DomainID"))
                objOpportunityItemsReleaseDates.OppID = oppID
                objOpportunityItemsReleaseDates.OppItemID = oppItemID
                objOpportunityItemsReleaseDates.ReleaseDate = dtRelease
                objOpportunityItemsReleaseDates.Save()
            Else
                Return ("Invalid Date")
            End If
        Catch ex As Exception
            Return ("Error: " & ex.Message)
        End Try
    End Function

    ' Added by Priya (17 Jan 2018)(Follow up campaign details)
    <WebMethod(EnableSession:=True)>
    Public Function FetchAutomatedFollowUpDetails(ByVal numContactId As String) As String()
        Try
            Dim objCommon As New CCommon
            Dim dsFollowupdetails As New DataSet

            Dim strFollowups As String = " " + "," + " " + "," + " "
            Dim ArrFollowValues() As String = strFollowups.Split(",")

            If ((numContactId <> "" And numContactId <> "undefined") And Session("DomainID").ToString() <> "") Then

                Dim DomainId As Long = Session("DomainID")
                dsFollowupdetails = objCommon.FetchFollowUpCampaignDetails(DomainId, CType(numContactId, Long))
                'Dim strFollowUpCampaign As String = ""
                'Dim strLastFollowUp As String = ""
                'Dim strNextFollowUp As String = ""

                Dim dtFollowupTable As DataTable
                dtFollowupTable = dsFollowupdetails.Tables(0)


                If dtFollowupTable.Rows.Count > 0 Then
                    strFollowups = dtFollowupTable.Rows(0).Item("FollowUpCampaign").ToString() + "," + dtFollowupTable.Rows(0).Item("LastFollowUp").ToString() + "," + dtFollowupTable.Rows(0).Item("NextFollowUp").ToString()
                    'strFollowUpCampaign = dtFollowupTable.Rows(0).Item("FollowUpCampaign").ToString()
                    'strLastFollowUp = dtFollowupTable.Rows(0).Item("LastFollowUp").ToString()
                    'strNextFollowUp = dtFollowupTable.Rows(0).Item("NextFollowUp").ToString()
                End If
                ArrFollowValues = strFollowups.Split(",")
                Return ArrFollowValues
                ' Return (strFollowUpCampaign + "," + strLastFollowUp + "," + strNextFollowUp)

            Else
                Return ArrFollowValues
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetAdvancedSearchModuleAndFields() As String
        Try
            Dim objCommon As New CCommon
            objCommon.DomainID = Context.Session("DomainID")
            objCommon.UserCntID = Context.Session("UserContactID")
            Dim ds As DataSet = objCommon.GetAdvancedSearchModuleAndFields()

            If Not ds Is Nothing AndAlso ds.Tables.Count > 1 Then
                Return JsonConvert.SerializeObject(New With {Key .modules = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .fields = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None)}, Formatting.None)
            Else
                Return JsonConvert.SerializeObject(New With {Key .modules = "", Key .fields = ""}, Formatting.None)
            End If
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetAdvancedSearchListDetails(ByVal numListID As Long, ByVal vcListItemType As String, ByVal searchText As String, ByVal pageIndex As Integer, ByVal pageSize As Integer) As String
        Try
            Dim objCommon As New CCommon
            objCommon.DomainID = Context.Session("DomainID")
            Dim dtTable As DataTable = objCommon.GetAdvancedSearchListDetails(numListID, vcListItemType, searchText, pageIndex, pageSize)

            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
            If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 Then
                Return serializer.Serialize(New With {.results = JsonConvert.SerializeObject(dtTable, Formatting.None), .Total = CCommon.ToInteger(dtTable.Rows(0)("numTotalRecords"))})
            Else
                Return serializer.Serialize(New With {.results = "", .Total = 0})
            End If
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function SaveAdvancedSearch(ByVal searchID As Long, ByVal formID As Long, ByVal searchName As String, ByVal searchConditions As String, ByVal shareWithIdsCommaSeperated As String, ByVal displayColumns As String) As String
        Try
            Dim objSearch As New FormGenericAdvSearch
            objSearch.byteMode = 2
            objSearch.SearchID = searchID
            objSearch.DomainID = Context.Session("DomainID")
            objSearch.UserCntID = Context.Session("UserContactID")
            objSearch.SearchName = searchName
            objSearch.FormID = formID
            objSearch.SearchConditions = searchConditions
            objSearch.SharedWithUsers = shareWithIdsCommaSeperated
            objSearch.DisplayColumns = displayColumns
            objSearch.ManageSavedSearch()
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function DeleteAdvancedSearch(ByVal searchID As Long) As String
        Try
            Dim objSearch As New FormGenericAdvSearch
            objSearch.byteMode = 3
            objSearch.SearchID = searchID
            objSearch.DomainID = Context.Session("DomainID")
            objSearch.UserCntID = Context.Session("UserContactID")
            objSearch.ManageSavedSearch()
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetUserSavedAdvancedSearch(ByVal searchID As Long) As String
        Try
            Dim objSearch As New FormGenericAdvSearch
            objSearch.byteMode = 1
            objSearch.SearchID = searchID
            objSearch.DomainID = Context.Session("DomainID")
            objSearch.UserCntID = Context.Session("UserContactID")
            Dim dt As DataTable = objSearch.ManageSavedSearch()

            Return JsonConvert.SerializeObject(dt, Formatting.None)
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function GetVendorDetailPlanningProcurement(ByVal vendorID As Long, ByVal itemCode As Long, ByVal numWarehouseItemID As Long, ByVal numQtyToPurchase As Decimal)
        Try
            Dim objDemandForecast As New BACRM.BusinessLogic.DemandForecast.DemandForecast
            objDemandForecast.DomainID = Context.Session("DomainID")
            Dim ds As DataSet = objDemandForecast.GetVendorDetail(vendorID, itemCode, numWarehouseItemID, numQtyToPurchase)

            If Not ds Is Nothing AndAlso ds.Tables.Count > 1 Then
                Return JsonConvert.SerializeObject(New With {Key .vendors = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .shipmentMethods = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None)}, Formatting.None)
            Else
                Return JsonConvert.SerializeObject(New With {Key .vendors = "", Key .shipmentMethods = ""}, Formatting.None)
            End If
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function ClearWarehouseInternalLocation(ByVal warehouseItemID As Long)
        Try
            Dim objItemWarehouse As New ItemWarehouse
            objItemWarehouse.DomainID = CCommon.ToLong(Context.Session("DomainID"))
            objItemWarehouse.WarehouseItemID = warehouseItemID
            objItemWarehouse.ClearInternalLocation()
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                Context.Response.StatusCode = 404
                Return ex.Message
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function UpdateParentKit(ByVal OppID As Long, ByVal OppItemID As Long, ByVal ParentKitOppItemID As Long)
        Try
            Dim objOpp As New MOpportunity
            objOpp.DomainID = CCommon.ToLong(Context.Session("DomainID"))
            objOpp.OpportunityId = OppID
            objOpp.OppItemCode = OppItemID
            objOpp.ParentOppID = ParentKitOppItemID
            objOpp.UpdateParentKit()

            Return True
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                Context.Response.StatusCode = 404
                Return ex.Message
            Else
                Context.Response.StatusCode = 401
                Return "Session Expired"
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function SetAsWinningBid(ByVal OppID As Long, ByVal OppItemID As Long)
        Try
            Dim objOpp As New MOpportunity
            objOpp.DomainID = CCommon.ToLong(Context.Session("DomainID"))
            objOpp.OpportunityId = OppID
            objOpp.OppItemCode = OppItemID
            objOpp.SetAsWinningBid()

            Return True
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                Context.Response.StatusCode = 404
                Return ex.Message
            Else
                Context.Response.StatusCode = 401
                Return "Session Expired"
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    Public Function CloneOrderLineItem(ByVal OppID As Long, ByVal OppItemID As Long)
        Try
            Dim objOpp As New MOpportunity
            objOpp.DomainID = CCommon.ToLong(Context.Session("DomainID"))
            objOpp.UserCntID = CCommon.ToLong(Context.Session("UserContactID"))
            objOpp.OpportunityId = OppID
            objOpp.OppItemCode = OppItemID
            objOpp.CloneOrderLineItem()

            Return True
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                Context.Response.StatusCode = 404
                Return ex.Message
            Else
                Context.Response.StatusCode = 401
                Return "Session Expired"
            End If
        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetSearchedCustomers(ByVal searchText As String, ByVal pageIndex As Integer, ByVal pageSize As Integer) As String
        Try
            Dim json As String = String.Empty

            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
            Dim totalCount As Integer = 0
            If searchText <> "" Then
                Dim objCustomer As New BACRM.BusinessLogic.Customers.CCustomers
                objCustomer.DomainID = CCommon.ToLong(Session("DomainID"))
                objCustomer.SearchText = searchText
                objCustomer.PageIndex = pageIndex
                objCustomer.PageSize = pageSize
                Dim dtResult As DataTable = objCustomer.SearchCustomerByName()

                If Not dtResult Is Nothing AndAlso dtResult.Rows.Count > 0 Then
                    json = JsonConvert.SerializeObject(New With {Key .results = JsonConvert.SerializeObject(dtResult, Formatting.None), Key .Total = CCommon.ToLong(objCustomer.TotalRecords)}, Formatting.None)
                Else
                    json = JsonConvert.SerializeObject(New With {Key .results = "[]", Key .Total = CCommon.ToLong(objCustomer.TotalRecords)}, Formatting.None)
                End If
            End If

            Return json
        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If

        End Try
    End Function

    <WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function SearchGenericDocuments(ByVal searchText As String, ByVal category As Long, ByVal documentType As Short, ByVal pageIndex As Integer, ByVal pageSize As Integer) As String
        Try
            If CCommon.ToLong(Session("DomainID")) > 0 Then
                Dim json As String = String.Empty

                Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
                Dim totalCount As Integer = 0
                If searchText <> "" Then
                    Dim objDocumentList As New BACRM.BusinessLogic.Documents.DocumentList
                    objDocumentList.DomainID = CCommon.ToLong(Session("DomainID"))
                    objDocumentList.DocCategory = category
                    objDocumentList.DocumentType = documentType
                    objDocumentList.KeyWord = searchText
                    objDocumentList.CurrentPage = pageIndex
                    objDocumentList.PageSize = pageSize
                    Dim dtResult As DataTable = objDocumentList.SearchGenericDocuments()

                    If Not dtResult Is Nothing AndAlso dtResult.Rows.Count > 0 Then
                        json = JsonConvert.SerializeObject(New With {Key .results = JsonConvert.SerializeObject(dtResult, Formatting.None), Key .Total = CCommon.ToLong(objDocumentList.TotalRecords)}, Formatting.None)
                    Else
                        json = JsonConvert.SerializeObject(New With {Key .results = "[]", Key .Total = CCommon.ToLong(objDocumentList.TotalRecords)}, Formatting.None)
                    End If
                End If

                Return json
            Else
                Throw New HttpException(404, "")
            End If


        Catch ex As Exception
            If Not Session("DomainID") Is Nothing Then
                ExceptionModule.ExceptionPublish(ex, Convert.ToInt64(Session("DomainID")), Convert.ToInt64(Session("UserContactID")), req:=Nothing)
                Context.Response.StatusCode = 404
                Return "Error occured"
            Else
                Context.Response.StatusCode = 201
                Return "Session Expired"
            End If

        End Try
    End Function
End Class
Public Class ComboBoxItemData

    Private _text As String
    Public Property Text() As String
        Get
            Return _text
        End Get
        Set(ByVal value As String)
            _text = value
        End Set
    End Property

    Private _value As String
    Public Property Value() As String
        Get
            Return _value
        End Get
        Set(ByVal value As String)
            _value = value
        End Set
    End Property


End Class
