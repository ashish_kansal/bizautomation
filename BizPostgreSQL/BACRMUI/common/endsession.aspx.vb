﻿Imports BACRM.BusinessLogic.Common
Public Class endsession
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If System.Web.HttpContext.Current.Cache("SessionTimeOut" & Session("UserID")) IsNot Nothing Then
            Dim sRet As String = ""

            sRet = DirectCast(System.Web.HttpContext.Current.Cache("SessionTimeOut" & Session("UserID")), String)
            If sRet IsNot Nothing Then
                System.Web.HttpContext.Current.Cache.Remove("SessionTimeOut" & Session("UserID"))
            End If
        End If
        Session.Abandon()
        System.Web.Security.FormsAuthentication.SignOut()
        Response.Write("<script language='javascript'>self.close();</script>")
    End Sub

End Class