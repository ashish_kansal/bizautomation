﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Public Class frmTicklerListFilterSetting
    Inherits BACRMPage

#Region "Enum"
    Private Enum CriteriaEnum As Integer
        Assigned = 1
        Owned = 2
        AssignedOrOwned = 3
    End Enum

    Private Enum AssignedStagesEnum As Integer
        SalesOpportunities = 1
        PurchaseOpportunities = 2
        SalesOrders = 3
        PurchaseOrders = 4
        Projects = 5
    End Enum

#End Region

#Region "Member Variables"

    Private objTickler As Tickler
    Private vcEmployees() As String
    Private vcEmployeesOpp() As String
    Private vcEmployeesCases() As String
    Private vcActionTypes() As String
    Private vcAssignedStages() As String
    Private numCriteria, numCriteriaCases As Integer

#End Region

#Region "Constructor"

    Sub New()
        objTickler = New Tickler
        numCriteria = 0
        numCriteriaCases = 0
    End Sub

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                GetTicklerFilterSetting()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub SetCriteria()
        Try
            Select Case numCriteria
                Case CriteriaEnum.Assigned
                    rdbAssigned.Checked = True
                Case CriteriaEnum.Owned
                    rdbOwned.Checked = True
                Case CriteriaEnum.AssignedOrOwned
                    rdbAssignedOrOwned.Checked = True
            End Select

            Select Case numCriteriaCases
                Case CriteriaEnum.Assigned
                    rdbCasesAssigned.Checked = True
                Case CriteriaEnum.Owned
                    rdbCasesOwned.Checked = True
                Case CriteriaEnum.AssignedOrOwned
                    rdbCasesAssignedOrOwned.Checked = True
            End Select
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindEmployees()
        Try
            Dim dtEmployees As DataTable
            objTickler.DomainID = Session("DomainID")
            objTickler.UserCntID = Session("UserContactID")
            objTickler.TeamType = 1
            dtEmployees = objTickler.GetEmployees

            If Not dtEmployees Is Nothing AndAlso dtEmployees.Rows.Count > 0 Then
                Dim listItem As ListItem

                For Each dr As DataRow In dtEmployees.Rows
                    listItem = New ListItem
                    listItem.Text = CCommon.ToString(dr("Name"))
                    listItem.Value = CCommon.ToString(dr("numUserId"))

                    If Not vcEmployees Is Nothing AndAlso vcEmployees.Contains(listItem.Value.Split(",")(1)) Then
                        lbSelectedFields.Items.Add(listItem)
                    Else
                        lbAvailableFields.Items.Add(listItem)
                    End If

                    If Not vcEmployeesOpp Is Nothing AndAlso vcEmployeesOpp.Contains(listItem.Value.Split(",")(1)) Then
                        lbSelectedFieldsOpp.Items.Add(listItem)
                    Else
                        lbAvailableFieldsOpp.Items.Add(listItem)
                    End If

                    If Not vcEmployeesCases Is Nothing AndAlso vcEmployeesCases.Contains(listItem.Value.Split(",")(1)) Then
                        lbSelectedFieldsCases.Items.Add(listItem)
                    Else
                        lbAvailableFieldsCases.Items.Add(listItem)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindActionTypes()
        Try
            Dim dt As DataTable = objCommon.GetMasterListItems(73, Session("DomainID"))

            Dim listItem As ListItem

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    listItem = New ListItem
                    listItem.Text = CCommon.ToString(dr("vcData"))
                    listItem.Value = CCommon.ToString(dr("numListItemID"))

                    If Not vcActionTypes Is Nothing AndAlso vcActionTypes.Contains(listItem.Value) Then
                        listItem.Selected = True
                    End If

                    If CCommon.ToLong(dr("numListItemID")) <> 974 Then
                        chblAction.Items.Add(listItem)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindAssignedStages()
        Try
            If Not vcAssignedStages Is Nothing AndAlso vcAssignedStages.Contains(AssignedStagesEnum.SalesOpportunities) Then
                chkSalesOpportunities.Checked = True
            End If
            If Not vcAssignedStages Is Nothing AndAlso vcAssignedStages.Contains(AssignedStagesEnum.SalesOrders) Then
                chkSalesOrders.Checked = True
            End If
            If Not vcAssignedStages Is Nothing AndAlso vcAssignedStages.Contains(AssignedStagesEnum.PurchaseOpportunities) Then
                chkPurchaseOpportunities.Checked = True
            End If
            If Not vcAssignedStages Is Nothing AndAlso vcAssignedStages.Contains(AssignedStagesEnum.PurchaseOrders) Then
                chkPurchaseOrders.Checked = True
            End If
            If Not vcAssignedStages Is Nothing AndAlso vcAssignedStages.Contains(AssignedStagesEnum.Projects) Then
                chkProjects.Checked = True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub GetTicklerFilterSetting()
        Try
            objTickler.DomainID = Session("DomainID")
            objTickler.UserCntID = Session("UserContactID")
            Dim dt As DataTable = objTickler.GetListFilterSetting()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                vcEmployees = CCommon.ToString(dt.Rows(0)("vcSelectedEmployee")).Split(",")
                vcActionTypes = CCommon.ToString(dt.Rows(0)("vcActionTypes")).Split(",")
                numCriteria = CCommon.ToInteger(dt.Rows(0)("numCriteria"))
                vcEmployeesOpp = CCommon.ToString(dt.Rows(0)("vcSelectedEmployeeOpp")).Split(",")
                vcEmployeesCases = CCommon.ToString(dt.Rows(0)("vcSelectedEmployeeCases")).Split(",")
                vcAssignedStages = CCommon.ToString(dt.Rows(0)("vcAssignedStages")).Split(",")
                numCriteriaCases = CCommon.ToInteger(dt.Rows(0)("numCriteriaCases"))
            End If

            SetCriteria()
            BindEmployees()
            BindActionTypes()
            BindAssignedStages()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub AddEmployee(ByVal lbTempAvailableFields As ListBox, ByVal lbTempSelectedFields As ListBox)
        Try
            If Not lbTempAvailableFields.SelectedItem Is Nothing Then
                lbTempSelectedFields.Items.Add(lbTempAvailableFields.SelectedItem)
                lbTempAvailableFields.Items.Remove(lbTempAvailableFields.SelectedItem)

                If lbTempAvailableFields.Items.Count > 0 Then
                    lbTempAvailableFields.SelectedIndex = 0
                End If

                If lbTempSelectedFields.Items.Count > 0 Then
                    lbTempSelectedFields.SelectedIndex = 0
                End If
            Else
                Page.ClientScript.RegisterStartupScript(GetType(Page), "AddEmployee", "alert('Please select employee!');", True)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub RemoveEmployee(ByVal lbTempAvailableFields As ListBox, ByVal lbTempSelectedFields As ListBox)
        Try
            If Not lbTempSelectedFields.SelectedItem Is Nothing Then
                lbTempAvailableFields.Items.Add(lbTempSelectedFields.SelectedItem)
                lbTempSelectedFields.Items.Remove(lbTempSelectedFields.SelectedItem)

                If lbTempAvailableFields.Items.Count > 0 Then
                    lbTempAvailableFields.SelectedIndex = 0
                End If

                If lbTempSelectedFields.Items.Count > 0 Then
                    lbTempSelectedFields.SelectedIndex = 0
                End If
            Else
                Page.ClientScript.RegisterStartupScript(GetType(Page), "RemoveEmployee", "alert('Please select employee!');", True)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Private Sub btnSaveAndClose_Click(sender As Object, e As EventArgs) Handles btnSaveAndClose.Click
        Try
            Dim vcActionTypes As String = ""
            Dim vcEmployees As String = ""
            Dim vcEmployeesOpp As String = ""
            Dim vcEmployeesCases As String = ""
            Dim vcAssignedStages As String = ""
            numCriteria = 0
            numCriteriaCases = 0


            'START - 1 - Action Items & Meetings
            For Each item As ListItem In lbSelectedFields.Items
                vcEmployees = vcEmployees + "," + item.Value.Split(",")(1)
            Next
            vcEmployees = If(vcEmployees.Length > 0, vcEmployees.Substring(1, vcEmployees.Length - 1), "")

            For Each item As ListItem In chblAction.Items
                If item.Selected = True Then
                    vcActionTypes = vcActionTypes + "," + item.Value
                End If
            Next
            vcActionTypes = If(vcActionTypes.Length > 0, vcActionTypes.Substring(1, vcActionTypes.Length - 1), "")

            If rdbAssigned.Checked Then
                numCriteria = CriteriaEnum.Assigned
            ElseIf rdbOwned.Checked Then
                numCriteria = CriteriaEnum.Owned
            ElseIf rdbAssignedOrOwned.Checked Then
                numCriteria = CriteriaEnum.AssignedOrOwned
            End If
            'END - 1 - Action Items & Meetings

            'START - 2 - Opportunities & Projects
            For Each item As ListItem In lbSelectedFieldsOpp.Items
                vcEmployeesOpp = vcEmployeesOpp + "," + item.Value.Split(",")(1)
            Next
            vcEmployeesOpp = If(vcEmployeesOpp.Length > 0, vcEmployeesOpp.Substring(1, vcEmployeesOpp.Length - 1), "")

            If chkSalesOpportunities.Checked Then
                vcAssignedStages = vcAssignedStages + "," + CCommon.ToString(CCommon.ToInteger(AssignedStagesEnum.SalesOpportunities))
            End If
            If chkPurchaseOpportunities.Checked Then
                vcAssignedStages = vcAssignedStages + "," + CCommon.ToString(CCommon.ToInteger(AssignedStagesEnum.PurchaseOpportunities))
            End If
            If chkSalesOrders.Checked Then
                vcAssignedStages = vcAssignedStages + "," + CCommon.ToString(CCommon.ToInteger(AssignedStagesEnum.SalesOrders))
            End If
            If chkPurchaseOrders.Checked Then
                vcAssignedStages = vcAssignedStages + "," + CCommon.ToString(CCommon.ToInteger(AssignedStagesEnum.PurchaseOrders))
            End If
            If chkProjects.Checked Then
                vcAssignedStages = vcAssignedStages + "," + CCommon.ToString(CCommon.ToInteger(AssignedStagesEnum.Projects))
            End If

            vcAssignedStages = If(vcAssignedStages.Length > 0, vcAssignedStages.Substring(1, vcAssignedStages.Length - 1), "")
            'END - 2 - Opportunities & Projects

            'START - 3 - Cases
            For Each item As ListItem In lbSelectedFieldsCases.Items
                vcEmployeesCases = vcEmployeesCases + "," + item.Value.Split(",")(1)
            Next
            vcEmployeesCases = If(vcEmployeesCases.Length > 0, vcEmployeesCases.Substring(1, vcEmployeesCases.Length - 1), "")

            If rdbCasesAssigned.Checked Then
                numCriteriaCases = CriteriaEnum.Assigned
            ElseIf rdbCasesOwned.Checked Then
                numCriteriaCases = CriteriaEnum.Owned
            ElseIf rdbCasesAssignedOrOwned.Checked Then
                numCriteriaCases = CriteriaEnum.AssignedOrOwned
            End If
            'END - 3 - Cases

            objTickler.DomainID = Session("DomainID")
            objTickler.UserCntID = Session("UserContactID")
            objTickler.SaveListFilterSetting(vcEmployees, numCriteria, vcActionTypes, vcEmployeesOpp, vcEmployeesCases, vcAssignedStages, numCriteriaCases)

            Page.ClientScript.RegisterStartupScript(GetType(Page), "clientScript", "opener.location.reload(true); self.close();", True)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            AddEmployee(lbAvailableFields, lbSelectedFields)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        Try
            RemoveEmployee(lbAvailableFields, lbSelectedFields)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAddOpp_Click(sender As Object, e As EventArgs) Handles btnAddOpp.Click
        Try
            AddEmployee(lbAvailableFieldsOpp, lbSelectedFieldsOpp)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnRemoveOpp_Click(sender As Object, e As EventArgs) Handles btnRemoveOpp.Click
        Try
            RemoveEmployee(lbAvailableFieldsOpp, lbSelectedFieldsOpp)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAddCases_Click(sender As Object, e As EventArgs) Handles btnAddCases.Click
        Try
            AddEmployee(lbAvailableFieldsCases, lbSelectedFieldsCases)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnRemoveCases_Click(sender As Object, e As EventArgs) Handles btnRemoveCases.Click
        Try
            RemoveEmployee(lbAvailableFieldsCases, lbSelectedFieldsCases)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

End Class