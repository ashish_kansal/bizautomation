﻿Imports Infragistics.WebUI.UltraWebTab
Imports BACRM.BusinessLogic
Public Class SubTabsManager
    Public Shared Sub ManageSubTabs(ByVal p_uwOppTab As UltraWebTab, ByVal p_numDomainId As Long, ByVal p_numModuleId As Long)
        Dim objTabs As New Admin.Tabs
        Dim dtblSubTab As DataTable
        objTabs.DomainID = p_numDomainId
        objTabs.ModuleId = p_numModuleId
        dtblSubTab = objTabs.GetSubTabsControlInformation()
        If Not dtblSubTab Is Nothing Then
            Dim intCounter As Integer = dtblSubTab.Rows.Count
            If p_uwOppTab.Tabs.Count < intCounter Then
                intCounter = p_uwOppTab.Tabs.Count
            End If
            Dim i As Integer
            For i = 0 To intCounter - 1
                p_uwOppTab.Tabs(i).Text = dtblSubTab.Rows(i)("numTabName").ToString()
                If Not dtblSubTab.Rows(i)("IsVisible") Then
                    p_uwOppTab.Tabs(i).Visible = False
                End If
            Next
        End If
    End Sub
End Class
