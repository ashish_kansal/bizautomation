Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Common

Public Class frmAssignedStages
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents tblDetails As System.Web.UI.WebControls.Table
        Protected WithEvents btnOk As System.Web.UI.WebControls.Button
        Protected WithEvents tblstg As System.Web.UI.WebControls.Table

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                Dim intOppId, TicklerType, i As Integer
                Dim startDate, EndDate As Date
                Dim objCell As TableCell
                Dim objRow As TableRow
                Dim dtAssignedStages As DataTable
                intOppId = CCommon.ToInteger(GetQueryStringVal("OppID"))
                startDate = GetQueryStringVal( "startdate")
                EndDate = GetQueryStringVal( "enddate")
                TicklerType = CCommon.ToInteger(GetQueryStringVal("TicklerType"))
                Dim objTickler As New Tickler
                objTickler.UserCntID = Session("UserContactID")
                objTickler.DomainID = Session("DomainID")
                objTickler.StartDate = startDate
                objTickler.EndDate = EndDate
                objTickler.OppID = intOppId
                If GetQueryStringVal( "OppType") = "Project" Then
                    objTickler.OppType = 2
                Else : objTickler.OppType = 1
                End If
                dtAssignedStages = objTickler.GetAssingedStages
                For i = 0 To dtAssignedStages.Rows.Count - 1
                    objRow = New TableRow
                    objCell = New TableCell
                    objCell.Attributes.Add("class", "text_bold")

                    objCell.Text = i + 1 & ".&nbsp;"
                    objCell.HorizontalAlign = HorizontalAlign.Right
                    objRow.Cells.Add(objCell)
                    objCell = New TableCell
                    objCell.Attributes.Add("class", "normal1")
                    objCell.Text = dtAssignedStages.Rows(i).Item("vcStageDetail")
                    objRow.Cells.Add(objCell)
                    tblDetails.Rows.Add(objRow)
                Next
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
