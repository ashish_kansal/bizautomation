﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class BizMaster

    '''<summary>
    '''head control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents head As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''hlpBiz control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hlpBiz As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''imgThemeLogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgThemeLogo As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''chkEmailSimpleSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkEmailSimpleSearch As Global.System.Web.UI.HtmlControls.HtmlInputCheckBox

    '''<summary>
    '''hdnSimpleSearchText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSimpleSearchText As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''btnSimpleSearchPostback control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSimpleSearchPostback As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''aUserClockInOut control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents aUserClockInOut As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''btnUserClockInOut control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUserClockInOut As Global.System.Web.UI.HtmlControls.HtmlInputButton

    '''<summary>
    '''lblReminder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReminder As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblReminderTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReminderTime As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnDismiss control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDismiss As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hdnActionItemID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnActionItemID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnType As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''OrganizationList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents OrganizationList As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Image1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image1 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''orgAlertCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents orgAlertCount As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''CaseList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CaseList As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Image2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image2 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''caseAlertCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents caseAlertCount As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''oppList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents oppList As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Image3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image3 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''oppAlertCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents oppAlertCount As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''orderList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents orderList As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Image4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image4 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''orderAlertCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents orderAlertCount As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''projectList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents projectList As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Image5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image5 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''projectAlertCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents projectAlertCount As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''emailList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents emailList As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Image6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image6 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''emailAlertCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents emailAlertCount As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''TicklerList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TicklerList As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Image7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image7 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''ticklerAlertCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ticklerAlertCount As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ProcessList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ProcessList As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Image8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image8 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''processAlertCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents processAlertCount As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''liAdvanceSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liAdvanceSearch As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''hplAdvanceSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplAdvanceSearch As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''liBizAdvanceSearchMenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liBizAdvanceSearchMenu As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''liSubscription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liSubscription As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''HyperLink1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HyperLink1 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''imgUser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgUser As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''lblUserName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUserName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''imgUserLarge control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgUserLarge As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''lblUserNameDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUserNameDetail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMemmberSince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMemmberSince As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSubMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSubMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lkbSignOut control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lkbSignOut As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''liAdmin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liAdmin As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''hplAdministration control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplAdministration As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''liBizAdminMenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liBizAdminMenu As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''divBizNavMenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divBizNavMenu As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''webmenu1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents webmenu1 As Global.webmenu

    '''<summary>
    '''UpdatePanelError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelError As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''divError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divError As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblError As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''UpdatePanelMain control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanelMain As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''MainContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents MainContent As Global.System.Web.UI.WebControls.ContentPlaceHolder
End Class
