﻿Imports BACRM.BusinessLogic.Common
Public Class frmManageVendorLeadTimes
    Inherits BACRMPage

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                hdnDivisionID.Value = CCommon.ToLong(GetQueryStringVal("VendorID"))
                hdnVendorLeadTimesID.Value = CCommon.ToLong(GetQueryStringVal("VendorLeadTimesID"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"
    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

#End Region


End Class