﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Public Class frmManageVendorCostTable
    Inherits BACRMPage

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                hdnVendorTCode.Value = CCommon.ToLong(GetQueryStringVal("VendorTCode"))
                hdnDivisionID.Value = CCommon.ToLong(GetQueryStringVal("VendorID"))
                hdnItemID.Value = CCommon.ToLong(GetQueryStringVal("ItemID"))
                hdnIsUpdateDefaultVendorCostRange.Value = CCommon.ToShort(GetQueryStringVal("IsUpdateDefaultVendorCostRange"))

                Dim objCommon As New CCommon
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objCommon.GetDomainSettingValue("vcDefaultCostRange")
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    hdnDefaultVendorCostRange.Value = CCommon.ToString(dt.Rows(0)(0))
                End If

                Dim objCurrency As New CurrencyRates
                objCurrency.DomainID = Session("DomainID")
                objCurrency.GetAll = 0
                ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                ddlCurrency.DataTextField = "vcCurrencyDesc"
                ddlCurrency.DataValueField = "numCurrencyID"
                ddlCurrency.DataBind()

                If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                    ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                    ddlCurrency.SelectedItem.Text = "Base Currency"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"
    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

#End Region

End Class