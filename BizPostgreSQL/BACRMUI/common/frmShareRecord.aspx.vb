﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts

Namespace BACRM.UserInterface.Admin
    Public Class frmShareRecord
        Inherits BACRMPage

        Dim lngRecordID As Long
        Dim lngModuleID As Long
        Dim objContact As CContacts

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngRecordID = CCommon.ToLong(GetQueryStringVal("RecordID"))
                lngModuleID = CCommon.ToLong(GetQueryStringVal("ModuleID"))

                btnSave.Attributes.Add("onclick", "Save()")

                If Not IsPostBack Then
                    objCommon.sb_FillComboFromDBwithSel(ddlContactRole, 26, Session("DomainID"))

                    BindLists()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindLists()
            Try
                If objContact Is Nothing Then objContact = New CContacts

                objContact.DomainID = Session("DomainId")
                objContact.RecordID = lngRecordID
                objContact.ModuleID = lngModuleID
                objContact.tinttype = 0

                Dim ds As DataSet
                ds = objContact.GetShareRecordList()

                lstAvailablefld.DataSource = ds.Tables(0)
                lstAvailablefld.DataTextField = "vcUserName"
                lstAvailablefld.DataValueField = "numContactID"
                lstAvailablefld.DataBind()

                lstSelectedfld.DataSource = ds.Tables(1)
                lstSelectedfld.DataTextField = "vcUserName"
                lstSelectedfld.DataValueField = "numContactID"
                lstSelectedfld.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If objContact Is Nothing Then objContact = New CContacts
                Dim dsNew As New DataSet
                Dim dtTable As New DataTable
                dtTable.Columns.Add("numAssignedTo")
                dtTable.Columns.Add("numContactType")

                Dim i As Integer
                Dim dr As DataRow
                Dim str As String()
                str = hdnCol.Value.Split(",")
                For i = 0 To str.Length - 2
                    dr = dtTable.NewRow
                    dr("numAssignedTo") = str(i).Split("~")(0)
                    dr("numContactType") = str(i).Split("~")(1)
                    dtTable.Rows.Add(dr)
                Next

                dtTable.TableName = "Table"
                dsNew.Tables.Add(dtTable.Copy)

                objContact.DomainID = Session("DomainId")
                objContact.RecordID = lngRecordID
                objContact.ModuleID = lngModuleID
                objContact.tinttype = 1
                objContact.strXml = dsNew.GetXml

                objContact.SaveShareRecord()

                dsNew.Tables.Remove(dsNew.Tables(0))

                BindLists()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace