﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmChangeOrganization.aspx.vb" Inherits=".frmChangeOrganization" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function RefereshAndClose() {
            if (window.opener != null) {
                window.opener.location.href = window.opener.location.href;
            }

            window.close();
        }

        $(document).ready(function () {
            $('input[type=radio][name$=OrgGroup]').click(function () {
                $('input[type=radio][name$=OrgGroup]').each(function () {
                    $(this).prop('checked', false);
                });

                $(this).prop('checked', true);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnSave" runat="server" Text="Save & Close" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary" OnClientClick="return Close();" />
                <asp:HiddenField ID="hdnOppID" runat="server" />
                <asp:HiddenField ID="hdnProjectID" runat="server" />
                <asp:HiddenField ID="hdnCaseID" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Change Organization
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <ul class="list-inline">
                <li>
                    <asp:RadioButton runat="server" ID="rbCustomer" GroupName="OrgGroup" Checked="true" CssClass="rbOrgGroup" />
                </li>
                <li>
                    <telerik:RadComboBox AccessKey="C" Width="300" ID="radCmbCompany" DropDownWidth="600px"
                        Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" ShowMoreResultsBox="true" EnableLoadOnDemand="True"
                        OnClientItemsRequested="OnClientItemsRequestedOrganization" EmptyMessage="Select Organization"
                        ClientIDMode="Static" TabIndex="1" OnSelectedIndexChanged="radCmbCompany_SelectedIndexChanged">
                        <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                    </telerik:RadComboBox>
                </li>
                <li>
                    <telerik:RadComboBox ID="radcmbContact" Width="300" runat="server" EmptyMessage="Select Contact" AutoPostBack="true" TabIndex="2" ></telerik:RadComboBox>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <asp:Repeater runat="server" ID="rptAssociatedContacts">
            <ItemTemplate>
                <div class="col-xs-12">
                    <ul class="list-inline">
                        <li>
                            <asp:RadioButton runat="server" ID="rbAssociatedContact" GroupName="OrgGroup"  CssClass="rbOrgGroup" /></li>
                        <li>
                            <%# Eval("TargetCompany")%> (<%# Eval("ContactName")%>) <%# Eval("vcData")%>
                            <asp:HiddenField ID="hdnDivisionID" runat="server" Value='<%# Eval("numDivisionID") %>' />
                            <asp:HiddenField ID="hdnContactID" runat="server" Value='<%# Eval("numContactID") %>' />
                        </li>
                    </ul>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
