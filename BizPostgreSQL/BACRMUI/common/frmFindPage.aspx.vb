Imports BACRM.BusinessLogic.Admin
Namespace BACRM.UserInterface.Common

    Partial Public Class frmFindPage
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim objTab As New Tabs
            objTab.GroupID = Session("UserGroupID")
            objTab.RelationshipID = 0
            objTab.DomainID = Session("DomainID")
            Server.Transfer("../" & objTab.GetFirstPage)
        End Sub

    End Class

End Namespace