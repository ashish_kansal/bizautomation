<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TicklerDisplay.aspx.vb"
    Inherits="BACRM.UserInterface.Common.TicklerDisplay" %>

<%@ Register TagPrefix="menu1" TagName="Menu" Src="../include/webmenu.ascx" %>
<%@ Register TagPrefix="Biz" TagName="SimpleSearch" Src="../include/SimpleSearchControl.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Total BizAutomation</title>
    <link rel="stylesheet" href="../CSS/jscal2.css" type="text/css" />
    <link rel="stylesheet" href="~/CSS/NewUiStyle.css" type="text/css" />
    <link rel="stylesheet" href="~/images/BizSkin/TabStrip.css" type="text/css" />

    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <link rel="shortcut icon" type="image/ico" href="../images/favicon.ico" />
    <script type="text/javascript" src="../JavaScript/jquery.hotkeys.js"></script>

    <%-- Alert Panel Related files--%>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script>var jq = $.noConflict();</script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../Scripts/jquery.signalR-1.1.4.min.js"></script>
    <%--<script src="Scripts/jquery.signalR-1.2.2.min.js"></script>--%>
    <script src="http://localhost/BACRMUI/signalr/hubs"></script>
    <link href="../css/animate.min.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/custom.css" rel="stylesheet" />
    <link href="../css/floatexamples.css" rel="stylesheet" />
    <link href="../fonts/css/font-awesome.min.css" rel="stylesheet" />
    <%--<script src="../js/jquery.min.js"></script>--%>

    <script src="../js/custom.js"></script>
    <script src="../js/enscroll-0.6.0.min.js"></script>

    <script>
        var PageTitleNotification = {
            Vars: {
                OriginalTitle: document.title,
                Interval: null
            },
            On: function (notification, intervalSpeed) {
                var _this = this;
                _this.Vars.Interval = setInterval(function () {
                    document.title = (_this.Vars.OriginalTitle == document.title)
                                        ? notification
                                        : _this.Vars.OriginalTitle;
                }, (intervalSpeed) ? intervalSpeed : 1000);
            },
            Off: function () {
                clearInterval(this.Vars.Interval);
                document.title = this.Vars.OriginalTitle;
            }
        }
        //Organization Module -1
        //Case Module - 2
        //Purchase Opportunity - 3
        //Sales Opportunity - 4
        //Purchase Order - 5
        //Sales Order - 6
        //Project - 7
        //Email - 8
        //Tickler - 9
        //Process - 10
        function DataLoaded() {
            $("#orgMenu").empty();
            $.ajax({
                type: 'POST',
                async: false,
                contentType: "application/json; charset=utf-8",
                url: 'TicklerDisplay.aspx/OrgnizationAlert',
                data: "",
                success: function (data) {
                    $(".orgazCount").empty();
                    if (parseInt(data.d.length) > 99) {
                        $(".orgazCount").html("99+");
                    } else {
                        $(".orgazCount").html(data.d.length);
                    }
                    $.each(data.d, function (index, value) {
                        $("#orgMenu").append("<li><a href=\"#\" onclick=Delete(1," + value.recordId + "); id=" + value.recordId + " module=1 class=\"btnDelete\"><span><span>" + value.CRMType + "</span><span class=\"time\">" + value.createdDate + "</span></span><span class=\"message\"><b>" + value.companyName + "</b> Assigned By " + value.createdby + "</span></a><span id=" + value.recordId + " module=1 class=\"close btnDelete\" onclick=DeleteClose(1," + value.recordId + ");><i class=\"fa fa-close\"></i></span></li>");
                    })
                }
            });
        }
        function CaseDataLoaded() {
            $("#caseMenu").empty();
            $.ajax({
                type: 'POST',
                async: false,
                contentType: "application/json; charset=utf-8",
                url: 'TicklerDisplay.aspx/CaseNAlert',
                data: "",
                success: function (data) {
                    $(".caseCount").empty();
                    if (parseInt(data.d.length) > 99) {
                        $(".caseCount").html("99+");
                    } else {
                        $(".caseCount").html(data.d.length);
                    }
                    $.each(data.d, function (index, value) {
                        $("#caseMenu").append("<li><a href=\"#\" onclick=Delete(2," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>" + value.CreatedBy + "</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\">New Case from <b>" + value.OrganizationName + "</b> By " + value.ContactName + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteClose(2," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                    })
                }
            });
        }
        function OpportunityDataLoaded() {
            $("#opportunityMenu").empty();
            $("#orderMenu").empty();
            var order = 0;
            var opportunity = 0;
            $.ajax({
                type: 'POST',
                async: false,
                contentType: "application/json; charset=utf-8",
                url: 'TicklerDisplay.aspx/POSAlert',
                data: "",
                success: function (data) {

                    $.each(data.d, function (index, value) {
                        if (value.OppType == "2" && value.OppStatus == "0") {
                            opportunity = opportunity + 1;
                            $("#opportunityMenu").append("<li><a href=\"#\" onclick=Delete(3," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>Purchase Opportunity</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> By " + value.ContactName + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteClose(3," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                        }
                        if (value.OppType == "1" && value.OppStatus == "0") {
                            opportunity = opportunity + 1;
                            $("#opportunityMenu").append("<li><a href=\"#\" onclick=Delete(4," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>Sales Opportunity</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> By " + value.ContactName + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteClose(4," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                        }
                        if (value.OppType == "2" && value.OppStatus == "1") {
                            order = order + 1;
                            $("#orderMenu").append("<li><a href=\"#\" onclick=Delete(5," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>Purchase Order</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> By " + value.ContactName + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteClose(5," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                        }
                        if (value.OppType == "1" && value.OppStatus == "1") {
                            order = order + 1;
                            $("#orderMenu").append("<li><a href=\"#\" onclick=Delete(6," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>Sales Order</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> By " + value.ContactName + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteClose(6," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                        }
                    })
                    $(".opportunityCount").empty();
                    if (parseInt(opportunity) > 99) {
                        $(".opportunityCount").html("99+");
                    } else {
                        $(".opportunityCount").html(opportunity);
                    }
                    $(".orderCount").empty();
                    if (parseInt(order) > 99) {
                        $(".orderCount").html("99+");
                    } else {
                        $(".orderCount").html(order);
                    }
                }
            });
        }

        function ProjectDataLoaded() {
            $("#projectMenu").empty();
            $(".projectCount").empty();
            var order = 0;
            var opportunity = 0;
            $.ajax({
                type: 'POST',
                async: false,
                contentType: "application/json; charset=utf-8",
                url: 'TicklerDisplay.aspx/ProjectAlert',
                data: "",
                success: function (data) {
                    if (parseInt(data.d.length) > 99) {
                        $(".projectCount").html("99+");
                    } else {
                        $(".projectCount").html(data.d.length);
                    }
                    $.each(data.d, function (index, value) {
                        $("#projectMenu").append("<li><a href=\"#\" onclick=Delete(7," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>" + value.CreatedBy + "</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> Assigned By " + value.CreatedBy + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteClose(7," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                    })
                }
            });
        }
        function EmailDataLoaded() {
            $("#EmailMenu").empty();
            $(".EmailCount").empty();

            var order = 0;
            var opportunity = 0;
            $.ajax({
                type: 'POST',
                async: false,
                contentType: "application/json; charset=utf-8",
                url: 'TicklerDisplay.aspx/EmailAlert',
                data: "",
                success: function (data) {
                    if (parseInt(data.d.length) > 99) {
                        $(".EmailCount").html("99+");
                    } else {
                        $(".EmailCount").html(data.d.length);
                    }
                    $.each(data.d, function (index, value) {
                        $("#EmailMenu").append("<li><a href=\"#\" onclick=Delete(8," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>" + value.CreatedBy + "</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.CreatedBy + "</b> </span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteClose(8," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                    })
                }
            });
        }
        function TicklerDataLoaded() {
            $("#TicklerMenu").empty();
            $(".TicklerCount").empty();
            $.ajax({
                type: 'POST',
                async: false,
                contentType: "application/json; charset=utf-8",
                url: 'TicklerDisplay.aspx/TicklerAlert',
                data: "",
                success: function (data) {
                    if (parseInt(data.d.length) > 99) {
                        $(".TicklerCount").html("99+");
                    } else {
                        $(".TicklerCount").html(data.d.length);
                    }
                    $.each(data.d, function (index, value) {
                        $("#TicklerMenu").append("<li><a href=\"#\" onclick=Delete(9," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>" + value.CreatedBy + "</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\">A new Tickler from <b>" + value.CaseName + "</b> through <b>" + value.OppStatus + "</b> </span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteClose(9," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                    })
                }
            });
        }
        
        function ProcessDataLoaded() {
            $("#ProcessMenu").empty();
            $(".ProcessCount").empty();

            var order = 0;
            var opportunity = 0;
            $.ajax({
                type: 'POST',
                async: false,
                contentType: "application/json; charset=utf-8",
                url: 'TicklerDisplay.aspx/ProcessAlert',
                data: "",
                success: function (data) {
                    if (parseInt(data.d.length) > 99) {
                        $(".ProcessCount").html("99+");
                    } else {
                        $(".ProcessCount").html(data.d.length);
                    }
                    $.each(data.d, function (index, value) {
                        $("#ProcessMenu").append("<li><a href=\"#\" onclick=Delete(10," + value.caseId + "," + value.OppType + "," + value.OppStatus + "," + value.OrganizationName + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>" + value.CreatedBy + "</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> Assigned By " + value.CreatedBy + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteClose(10," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                    })
                }
            });
        }
        //Delete Records
        function DeleteClose(mod, id) {
            var recordid = id;
            var module = mod;
            DeleteRecord(recordid, module);
            if (module == 1) {
                DataLoaded();
            }
            if (module == 2) {
                CaseDataLoaded();
            }
            if (module == 3) {
                OpportunityDataLoaded();
            }
            if (module == 4) {
                OpportunityDataLoaded();
            }
            if (module == 5) {
                OpportunityDataLoaded();
            }
            if (module == 6) {
                OpportunityDataLoaded();
            }
            if (module == 7) {
                ProjectDataLoaded();
            }
            if (module == 8) {
                EmailDataLoaded();
            }
            if (module == 9) {
                TicklerDataLoaded();
            }
        }

        //Delete View Records
        function Delete(mod, id,opptype,oppstatus,OppID) {
            var recordid = id;
            var module = mod;
            DeleteRecord(recordid, module);
            if (module == 1) {
                DataLoaded();
                $("#mainframe")[0].contentWindow.location.href = '../Leads/frmLeads.aspx?DivID=' + recordid + '';
            }
            if (module == 2) {
                CaseDataLoaded();
                $("#mainframe")[0].contentWindow.location.href = '../cases/frmCases.aspx?CaseID=' + recordid + '';
            }
            if (module == 3) {
                OpportunityDataLoaded();
                $("#mainframe")[0].contentWindow.location.href = '../opportunity/frmOpportunities.aspx?opId=' + recordid + '';
            }
            if (module == 4) {
                OpportunityDataLoaded();
                $("#mainframe")[0].contentWindow.location.href = '../opportunity/frmOpportunities.aspx?opId=' + recordid + '';
            }
            if (module == 5) {
                OpportunityDataLoaded();
                $("#mainframe")[0].contentWindow.location.href = '../opportunity/frmOpportunities.aspx?opId=' + recordid + '';
            }
            if (module == 6) {
                OpportunityDataLoaded();
                $("#mainframe")[0].contentWindow.location.href = '../opportunity/frmOpportunities.aspx?opId=' + recordid + '';
            }
            if (module == 7) {
                ProjectDataLoaded();
                $("#mainframe")[0].contentWindow.location.href = '../projects/frmProjects.aspx?ProId=' + recordid + '';
            }
            if (module == 8) {
                EmailDataLoaded();
                $("#mainframe")[0].contentWindow.location.href = '../projects/frmProjects.aspx?ProId=' + recordid + '';
            }
            if (module == 9) {
                TicklerDataLoaded();
                $("#mainframe")[0].contentWindow.location.href = '../admin/ActionItemDetailsOld.aspx?CommId=' + recordid + '&CaseId=0&CaseTimeId=0&CaseExpId=0';
            }
            if (module == 10) {
                ProcessDataLoaded();
                if (opptype == -1 && oppstatus == -1) {
                    $("#mainframe")[0].contentWindow.location.href = '../projects/frmProjects.aspx?ProId=' + OppID + '';
                } else {
                    $("#mainframe")[0].contentWindow.location.href = '../opportunity/frmOpportunities.aspx?opId=' + OppID + '';
                }
            }
        }
        function DeleteRecord(RecordId, ModuleId) {
            $("#newData").empty();
            $.ajax({
                type: 'POST',
                async: false,
                contentType: "application/json; charset=utf-8",
                url: 'TicklerDisplay.aspx/DeleteRecord',
                data: JSON.stringify({ "ModuleId": ModuleId, "RecordId": RecordId }),
                success: function (data) {
                    if (data.d == "1") {
                    }
                }
            });
        }
        $(document).ready(function () {
            DataLoaded();
            CaseDataLoaded();
            OpportunityDataLoaded();
            ProjectDataLoaded();
            EmailDataLoaded();
            TicklerDataLoaded();
            ProcessDataLoaded();
            jq('.inner-menu').enscroll({
                showOnHover: false,
                verticalTrackClass: 'track3',
                verticalHandleClass: 'handle3'
            });
            $(".inner-menu").css("width", "100%");
            $(".inner-menu").css("padding-right", "0px");
        })

    </script>
    <script src="../Scripts/jquery-1.6.4.min.js"></script>
    <script>var jq1 = $.noConflict();</script>
    <script>
        $(function () {

            //Current Counts Status
            var COrg = $(".orgazCount").html();
            var CCase = $(".caseCount").html();
            var COpp = $(".opportunityCount").html();
            var COrder = $(".orderCount").html();
            var CProject = $(".projectCount").html();
            var CEmail = $(".EmailCount").html();
            var CTickler = $(".TicklerCount").html();
            var CProcess = $(".ProcessCount").html();

            //New Notifcation Status
            var NOrga = 0;
            var NCase = 0;
            var NOpportunity = 0;
            var NOrder = 0;
            var NProject = 0;
            var NEmail = 0;
            var NTickler = 0;
            var NProcess = 0;
            //var notify = $.connection.notificationsHub;
            $("#caseLi").click(function () {
                NCase = 0;
            })
            $("#orgLi").click(function () {
                NOrga = 0;
            })
            $("#oppLi").click(function () {
                NOpportunity = 0;
            })
            $("#orderLi").click(function () {
                NOrder = 0;
            })
            $("#projectLi").click(function () {
                NProject = 0;
            })
            $("#emailLi").click(function () {
                NEmail = 0;
            })
            $("#ticklerLi").click(function () {
                NTickler = 0;
            })
            $("#processLi").click(function () {
                NProcess = 0;
            })
            jq.connection.notificationsHub.client.displayNotification = function (msg) {
                var data = "";
                DataLoaded();
                if (COrg != $(".orgazCount").html()) {
                    NOrga = 1;
                    COrg = $(".orgazCount").html();
                }
                var OrganizationIntrvlId = setInterval(function () {
                    if (NOrga == "1") {
                        $(".iorgazCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
                    } else {
                        $(".iorgazCount").stop().fadeTo('slow', 1);
                        $(".iorgazCount").css('opacity', '1');
                    }
                }, 2);
            };
            jq.connection.caseHub.client.displayCaseNotification = function (msg) {
                var data = "";
                CaseDataLoaded();
                if (CCase != $(".caseCount").html()) {
                    NCase = 1;
                    CCase = $(".caseCount").html();
                }
                var CaseIntrvlId = setInterval(function () {
                    if (NCase == "1") {
                        $(".icaseCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
                    } else {
                        $(".icaseCount").stop().fadeTo('slow', 1);
                        $(".icaseCount").css('opacity', '1');
                    }
                    },2);
            };
            jq.connection.opportunityHub.client.displayOpportunityNotification = function (msg) {
                var data = "";
                OpportunityDataLoaded();
                if (COpp != $(".opportunityCount").html()) {
                    NOpportunity = 1;
                    COpp = $(".opportunityCount").html();
                }
                var OppIntrvlId = setInterval(function () {
                    if (NOpportunity == "1") {
                        $(".iopportunityCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
                    } else {
                        $(".iopportunityCount").stop().fadeTo('slow', 1);
                        $(".iopportunityCount").css('opacity', '1');
                    }
                }, 2);
                if (COrder != $(".orderCount").html()) {
                    NOrder = 1;
                    COrder = $(".orderCount").html();
                }
                var OrderIntrvlId = setInterval(function () {
                    if (NOrder == "1") {
                        $(".iorderCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
                    } else {
                        $(".iorderCount").stop().fadeTo('slow', 1);
                        $(".iorderCount").css('opacity', '1');
                    }
                }, 2);
            };
            jq.connection.projectHub.client.displayProjectNotification = function (msg) {
                var data = "";
                ProjectDataLoaded();
                if (CProject != $(".projectCount").html()) {
                    NProject = 1;
                    CProject = $(".projectCount").html();
                }
                var ProjectIntrvlId = setInterval(function () {
                    if (NProject == "1") {
                        $(".iprojectCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
                    } else {
                        $(".iprojectCount").stop().fadeTo('slow', 1);
                        $(".iprojectCount").css('opacity', '1');
                    }
                }, 2);
            };
            jq.connection.emailHub.client.displayEmailNotification = function (msg) {
                var data = "";
                EmailDataLoaded();
                if (CEmail != $(".EmailCount").html()) {
                    NEmail = 1;
                    CEmail = $(".EmailCount").html();
                }
                var EmailIntrvlId = setInterval(function () {
                    if (NEmail == "1") {
                        $(".iEmailCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
                    } else {
                        $(".iEmailCount").stop().fadeTo('slow', 1);
                        $(".iEmailCount").css('opacity', '1');
                    }
                }, 2);
            };
            jq.connection.ticklerHub.client.displayTicklerNotification = function (msg) {
                var data = "";
                TicklerDataLoaded();
                if (CTickler != $(".TicklerCount").html()) {
                    NTickler = 1;
                    CTickler = $(".TicklerCount").html();
                }
                var TicklerIntrvlId = setInterval(function () {
                    if (NTickler == "1") {
                        $(".iTicklerCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
                    } else {
                        $(".iTicklerCount").stop().fadeTo('slow', 1)
                        $(".iTicklerCount").css('opacity', '1');
                    }
                }, 2);
            };
            jq.connection.processHub.client.displayProcessNotification = function (msg) {
                var data = "";
                ProcessDataLoaded();
                if (CProcess != $(".ProcessCount").html()) {
                    NProcess = 1;
                    CProcess = $(".ProcessCount").html();
                }
                var CaseIntrvlId = setInterval(function () {
                    if (NProcess == "1") {
                        $(".iProcessCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
                    } else {
                        $(".iProcessCount").stop().fadeTo('slow', 1)
                        $(".iProcessCount").css('opacity', '1');
                    }
                }, 2);
            };
            jq.connection.hub.start().done(function () {
                //alert("Connection Started");
            });
        });
    </script>
    <style>
        .close {
            float: right;
            margin-top: -9px;
            font-size: 13px;
        }

        .inner-menu {
            width: 100%;
            max-height: 400px;
        }

        .track3 {
            width: 10px;
            background: rgba(0, 0, 0, 0);
            margin-right: 2px;
            border-radius: 10px;
            -webkit-transition: background 250ms linear;
            transition: background 250ms linear;
        }

            .track3:hover,
            .track3.dragging {
                background: #d9d9d9;
                background: rgba(0, 0, 0, 0.15);
            }

        .handle3 {
            width: 7px;
            right: 0;
            background: #999;
            background: rgba(0, 0, 0, 0.4);
            border-radius: 7px;
            -webkit-transition: width 250ms;
            transition: width 250ms;
        }

        .track3:hover .handle3,
        .track3.dragging .handle3 {
            width: 10px;
        }
    </style>
    <%-- End Alert Panel Related files--%>
    <script type="text/javascript">


        //function pageLoad() {
        //    BindKeyPress();
        //}

        window.onunload = unloadPage;

        function unloadPage() {
            window.open('endsession.aspx', 'SessionTImeOut', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=450,height=305,left=200,top=200');
        }

        //window.onload = BindKeyPress

        //function BindKeyPress() {
        //    if ($("#mainframe")[0].contentWindow != null){
        //        $($("#mainframe")[0].contentWindow.document).bind('keydown', 'alt+`', FocusSearchType);
        //    }

        //    if ($("#mainframe")[0].contentWindow != null && $("#mainframe")[0].contentWindow.frames['rightFrame'] != null) {
        //        $($("#mainframe")[0].contentWindow.frames['rightFrame'].document).bind('keydown', 'alt+`', FocusSearchType);
        //    }

        //    if ($("#mainframe")[0].contentWindow != null && $("#mainframe")[0].contentWindow.frames['leftFrame'] != null) {
        //        $($("#mainframe")[0].contentWindow.frames['leftFrame'].document).bind('keydown', 'alt+`', FocusSearchType);
        //    }
        //}

        $(document).ready(function () {
            $(document).bind('keydown', 'alt+`', FocusSearchType);

            $('iframe#mainframe').load(function () {
                if ($("#mainframe")[0] != null && $("#mainframe")[0].contentWindow != null && $("#mainframe")[0].contentWindow.document != null) {
                    $($("#mainframe")[0].contentWindow.document).bind('keydown', 'alt+`', FocusSearchType);

                    if ($("#mainframe")[0].contentWindow.document.getElementsByName("rightFrame")[0] != null &&
                        $("#mainframe")[0].contentWindow.document.getElementsByName("rightFrame")[0].contentWindow != null &&
                        $("#mainframe")[0].contentWindow.document.getElementsByName("rightFrame")[0].contentWindow.document != null) {

                        $($("#mainframe")[0].contentWindow.document.getElementsByName("rightFrame")[0]).load(function () {
                            $($("#mainframe")[0].contentWindow.document.getElementsByName("rightFrame")[0].contentWindow.document).bind('keydown', 'alt+`', FocusSearchType);
                        });

                        $($("#mainframe")[0].contentWindow.document.getElementsByName("rightFrame")[0].contentWindow.document).bind('keydown', 'alt+`', FocusSearchType);
                    }

                    if ($("#mainframe")[0].contentWindow.document.getElementsByName("leftFrame")[0] != null &&
                        $("#mainframe")[0].contentWindow.document.getElementsByName("leftFrame")[0].contentWindow != null &&
                        $("#mainframe")[0].contentWindow.document.getElementsByName("leftFrame")[0].contentWindow.document != null) {
                        $($("#mainframe")[0].contentWindow.document.getElementsByName("leftFrame")[0].contentWindow.document).bind('keydown', 'alt+`', FocusSearchType);
                    }
                }
            });
        });

        function FocusSearchType() {
            FocusSimpleSearchType();
        }
    </script>
    <script type="text/javascript" language="javascript">
        function SelectTabByValue(Id) {
            var tabStrip = $find("RadTabStrip1");
            var tab = tabStrip.findTabByValue(Id);
            if (tab) {
                tab.select();
            }
        }
        function UnSelectTabs() {
            var tabStrip = $find("RadTabStrip1");

            var tab = tabStrip.get_selectedTab();
            if (tab) {
                tab.unselect();
            }
        }


        //myFunc is being called from frmTicklerdisplay.aspx
        function myFunc(a) {
            var scheduleInfo = ig_getWebScheduleInfoById("WebScheduleInfo1");

            if (scheduleInfo.getActivities().getItemFromKey(a) == null) {
                document.form1.btnGo1.click();
                setTimeout('ig_getWebScheduleInfoById("WebScheduleInfo1").showUpdateAppointmentDialog(' + a + ',"")', 2500)
            }
            else {
                ig_getWebScheduleInfoById("WebScheduleInfo1").showUpdateAppointmentDialog(a, "")
            }

            return false
        }

        function myFuncOut(a) {
            var scheduleInfo = ig_getWebScheduleInfoById("WebScheduleInfo1");
            if (scheduleInfo.getActivities().getItemFromKey(a) == null) {
                setTimeout('', 5000);
                document.form1.btnGo2.click();

            }

        }
        function WebScheduleInfo1_ActivityUpdating(oScheduleInfo, oEvent, oActivityUpdateProps, oActivity, id) {

        }
        function OpenConf() {
            var tabStrip = $find("RadTabStrip1");
            var selectedTab = tabStrip.get_selectedTab();

            var tab = 0;
            if (selectedTab != null)
                tab = selectedTab.get_value();

            window.open("../admin/frmShortcutMngUsr.aspx?tab=" + tab, '', 'toolbar=no,titlebar=no,left=150,top=150,scrollbars=yes,resizable=yes')
            return false;
        }
        function Click_Button() {
            document.form1.btnGo1.click();
        }
        function OpenInMainFrame(url) {
            $("#mainframe")[0].contentWindow.location.href = url;
            return false;
        }

        var PopupURL;
        PopupURL = '';
        var Popup;
        var sOppType;
        sOppType = 0;
        function Goto(target, frame, div, a, b) {
            var Oper;
            var checkURL;
            checkURL = target;

            if (target.split('?').length != 1) {
                Oper = '&';
            }
            else {
                Oper = '?';
            }
            if ($("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtDivision') == null) {
                if ($("#mainframe")[0].contentWindow.frames['rightFrame'] != null) {
                    if ($("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                        target = target + Oper                            + 'rtyWR=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision').value                            + '&uihTR=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtContact').value                            + '&tyrCV=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtPro').value                            + '&pluYR=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtOpp').value                            + '&fghTY=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtCase').value
                    }
                    else {
                        target = target + Oper + 'I=1'
                    }
                }

                else {
                    target = target + Oper + 'I=1'
                }
            }
            else {
                if ($("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                    target = target + Oper + 'rtyWR=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtDivision').value                        + '&uihTR=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtContact').value                        + '&tyrCV=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtPro').value                        + '&pluYR=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtOpp').value                        + '&fghTY=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtCase').value
                }
            }
            sOppType = 0;
            if (checkURL == '../opportunity/frmNewSalesOrder.aspx') {
                sOppType = 1;
            }
            else if (checkURL == '../Opportunity/frmNewPurchaseOrder.aspx') {
                sOppType = 2;
            }
            if (checkURL == '../opportunity/frmNewSalesOrder.aspx' || checkURL == '../Opportunity/frmNewPurchaseOrder.aspx') {
                PopupURL = target;
                Popup = window.open(target, "popNewOrder", 'toolbar=no,titlebar=no,left=150, top=150,width=' + a + ',height=' + b + ',scrollbars=yes,resizable=yes')
                $('#hdnPopupURL').val(Popup);
            }
            else {
                window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=' + a + ',height=' + b + ',scrollbars=yes,resizable=yes')
            }
        }
        function gotoOpp(target, frame, div, a, b) {
            var Oper;
            if (target.split('?').length != 1) {
                Oper = '&';
            }
            else {
                Oper = '?';
            }

            if ($("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtDivision') == null) {
                if ($("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                    target = target + Oper                        + 'rtyWR=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision').value                        + '&uihTR=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtContact').value                        + '&tyrCV=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtPro').value                        + '&pluYR=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtOpp').value                        + '&fghTY=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtCase').value
                }

            }
            else {
                if ($("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                    target = target + Oper                        + 'rtyWR=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtDivision').value                        + '&uihTR=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtContact').value                        + '&tyrCV=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtPro').value                        + '&pluYR=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtOpp').value                        + '&fghTY=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtCase').value
                }
            }

            window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=' + a + ',height=' + b + ',scrollbars=yes,resizable=yes')
        }
        var pop;
        var RPPopup;

        function CloseReceivePayment(a) {
            if (a.split('frmAmtPaid').length > 1) {
                if ($("#mainframe") != undefined && $("#mainframe")[0].contentWindow.frames['rightFrame'] != undefined) {
                    if ($("#mainframe")[0].contentWindow.frames['rightFrame'].RPPopup != undefined) {
                        pop = $("#mainframe")[0].contentWindow.frames['rightFrame'].RPPopup;
                    }
                    else if ($("#mainframe")[0].contentWindow.frames['rightFrame'].document.RPPopup != undefined) {
                        pop = $("#mainframe")[0].contentWindow.frames['rightFrame'].document.RPPopup;
                    }
                    else if (document.RPPopup != undefined) {
                        pop = document.RPPopup;
                    }
                    else {
                        return;
                    }
                }
                else {
                    return;
                }

                console.log(pop);
                if (pop == undefined) {
                    return;
                }
                else {
                    console.log(pop.name);
                    pop.close();
                }
            }
        }

        function reDirect(a, b) {
            var target = '';
            var Oper;

            CloseReceivePayment(a);

            if (b == 1) {
                if (a.split('?').length != 1) {
                    Oper = '&';
                }
                else {
                    Oper = '?';
                }
                if (parent.frames['mainframe'].frames.length == 0) {
                    if (($("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtDivision') != undefined && $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtDivision').value != null) ||
                        ($("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtContact') != undefined && $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtContact').value != null) ||
                        ($("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtPro') != undefined && $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtPro').value != null) ||
                        ($("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtOpp') != undefined && $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtOpp').value != null) ||
                        ($("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtCase') != undefined && $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtCase').value != null)) {
                        target = Oper + 'DivID=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtDivision').value
                        + '&CntID=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtContact').value
                        + '&ProID=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtPro').value
                        + '&OpID=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtOpp').value
                        + '&CaseID=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtCase').value

                    } else {
                        target = ''
                    }
                }
                else {
                    if ($("#mainframe") != undefined && $("#mainframe")[0].contentWindow.frames['rightFrame'] != null) {
                        if ($("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                            target = Oper                                + 'DivID=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision').value                                + '&CntID=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtContact').value                                + '&ProID=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtPro').value                                + '&OpID=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtOpp').value                                + '&CaseID=' + $("#mainframe")[0].contentWindow.frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtCase').value
                        }
                        else {
                            target = ''
                        }
                    }
                    else {
                        if ($("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                            target = Oper                                    + 'DivID=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtDivision').value                                    + '&CntID=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtContact').value                                    + '&ProID=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtPro').value                                    + '&OpID=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtOpp').value                                    + '&CaseID=' + $("#mainframe")[0].contentWindow.document.getElementById('ctl00_webmenu1_txtCase').value
                        }
                    }
                }
            }

            //if (a.split('frmAmtPaid').length > 1) {
            //    target = '';
            //}

            if (a.split('TimeAndExpense').length == 2) {
                $("#mainframe")[0].contentWindow.location.href = a;
            }
            else {

                if (target == "") {
                    $("#mainframe")[0].contentWindow.location.href = a;
                }
                else {
                    $("#mainframe")[0].contentWindow.location.href = a + target;
                }
            }

            return false;
        }
        function OpenPop(target) {
            window.open(target + '?R=1', '', 'toolbar=no,titlebar=no,left=150, top=150,width=750,height=450,scrollbars=yes,resizable=yes')
        }

        function GenericPopup(target, settings) {
            window.open(target, '', settings);
            return;
        }

        function Login() {
            if (parent.parent.frames.length > 0) {
                parent.parent.document.location.href = "../Login.aspx"
            }
            else if (parent.frames.length > 0) {
                parent.document.location.href = "../Login.aspx"
            }
            else {
                document.location.href = "../Login.aspx"
            }

            return false;
        }
        function reDirectPage(a) {
            $("#mainframe")[0].contentWindow.location.href = a;
        }
        var reminderPopUp;
        function OpenRemindersWindow() {
            if (reminderPopUp && !reminderPopUp.closed) {
                reminderPopUp.close();
            }
            reminderPopUp = window.open("../OutlookCalendar/Reminder.aspx", 'BizAutomation_Reminder_Popup', 'width=800,height=600,status=no,scrollbar=yes,top=110,left=150');
            reminderPopUp.focus();
        }
        function OpenAlertsWindow() {
            window.open("../WorkFlow/frmShowAlerts.aspx", '', 'width=800,height=600,status=no,scrollbar=yes,top=110,left=150')
        }
        function SetTextRemider(id, type, time) {
            $("#divReminder").show()
            $("#hdnActionItemID").val(id);
            $("#hdnType").val(type);
            $("#lblReminderTime").text(time);
        }
        function ClearTextRemider() {
            $("#divReminder").hide()
            $("#hdnActionItemID").val("");
            $("#hdnType").val("");
            $("#lblReminderTime").text("");
        }
    </script>
    <script src="../JavaScript/TabMenu.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.idletimer.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.idletimeout.js" type="text/javascript"></script>
    <style type="text/css">
        #idletimeout {
            background: #515E81;
            border: 3px solid #8B96B6;
            color: #fff;
            font-family: arial, sans-serif;
            text-align: center;
            font-size: 12px;
            padding: 10px;
            position: relative;
            top: 0px;
            left: 0;
            right: 0;
            z-index: 100000;
            display: none;
        }

            #idletimeout a {
                color: #fff;
                font-weight: bold;
            }

            #idletimeout span {
                font-weight: bold;
            }

        .alignbottom {
            vertical-align: text-bottom;
        }

        .navnext {
            width: 18px;
            height: 18px;
            background: url("../images/BizSkin/TabStrip/TabStripStates.png") -18px -208px;
            position: absolute;
            top: 0px;
            right: 0px;
            margin-top: -8px;
        }

        .navprev {
            width: 18px;
            height: 18px;
            background: url("../images/BizSkin/TabStrip/TabStripStates.png") 0px -208px;
            position: absolute;
            top: 0px;
            right: 18px;
            margin-top: -8px;
        }

        html, body {
            height: 100%;
        }
    </style>
</head>
<body>
    <div style="margin: 0; padding: 0">
        <div style="height: 105px">
            <form id="form1" runat="server">
                <div id="idletimeout">
                    You will be logged off in <span>
                        <!-- countdown place holder -->
                    </span>&nbsp;seconds due to inactivity. <a id="idletimeout-resume" href="#">Click here
            to continue using BizAutomation.com</a>.
                </div>
                <telerik:RadScriptManager ID="ScriptManager" runat="server" />
                <asp:Button runat="server" ID="btnLoadCal" Style="display: none" />
                <asp:Button runat="server" ID="btnGo1" Style="display: none" />
                <asp:Button runat="server" ID="btnGo2" Style="display: none" />
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <img src="../images/New LogoBiz1.JPG" alt="BizAutomation.com" />
                        </td>
                        <td style="white-space: nowrap; text-align: center" width="100%">
                            <div id="divReminder" style="display: none;">
                                <asp:Label ID="lblReminder" ForeColor="Red" runat="server" Text="You have an action item at: "></asp:Label>&nbsp;<asp:Label ID="lblReminderTime" ForeColor="Red" runat="server"></asp:Label>&nbsp;<asp:Button ID="btnDismiss" runat="server" CssClass="button" Style="padding-left: 5px !important;" Text="Dismiss" />
                            </div>
                            <asp:HiddenField ID="hdnActionItemID" runat="server" />
                            <asp:HiddenField ID="hdnType" runat="server" />
                        </td>
                        <td align="right" style="white-space: nowrap">
                            <table>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td id="tdEditHelp" runat="server" visible="false">
                                                    <asp:HyperLink NavigateUrl="#" CssClass="text_bold" ID="hplEditHelp" runat="server" Visible="false">Edit Help</asp:HyperLink>
                                                </td>
                                                <td id="tdSubscription" runat="server" visible="false">
                                                    <asp:HyperLink NavigateUrl="#" CssClass="text_bold" ID="hplSubscription" runat="server">Subscription</asp:HyperLink>
                                                    |
                                                </td>
                                                <td id="tdAdmin" runat="server">
                                                    <asp:HyperLink NavigateUrl="#" CssClass="text_bold" ID="hplAdministration" runat="server">Administration</asp:HyperLink>
                                                    |
                                                </td>
                                                <td id="tdAdvanced" runat="server">
                                                    <asp:HyperLink NavigateUrl="#" CssClass="text_bold" ID="hplAdvanced" runat="server">Advanced Search</asp:HyperLink>
                                                </td>
                                                <td>
                                                    <asp:HyperLink CssClass="text_bold" NavigateUrl="#" ID="hplHelp" runat="server" Visible="false">Help</asp:HyperLink>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <img height="16" src="../images/search_icon.gif" width="16" alt="" />
                                                </td>
                                                <td style="white-space: nowrap" align="left">
                                                    <Biz:SimpleSearch ID="SimpleSearch1" runat="server" />
                                                </td>

                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <div style="float: right; margin-top: 15px;">
                                          
                                            <asp:Label ID="lblLoggedin" runat="server"></asp:Label>
                                            <asp:LinkButton ID="lnkbtnLogOut" runat="server" Text="Logout" CssClass="PurpleText"></asp:LinkButton>&nbsp;&nbsp;<br />
                                              <asp:Label ID="lblSubMessage" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;<br />
                                <asp:Label ID="lblAlertMessage" runat="server" ForeColor="Red">&nbsp;&nbsp;&nbsp;</asp:Label>
                                        </div>
                                        <nav class="" role="navigation" style="float: right">

            <ul class="nav navbar-nav navbar-right">
                <li role="presentation" class="dropdown" runat="server" id="OrganizationList" visible="false">
                    <a href="javascript:;" id="orgLi" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <%--<i class="fa fa-envelope-o"></i>--%>
                        <img src="../images/Building-24.gif" />
                        <span class="badge bg-red orgazCount iorgazCount">6</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                        <li style="
    background: #f7f7f7;
    margin-top: 0px;
">
                            <div class="text-center">
                                <a>
                                    <a href="javascript:void(0)">You have&nbsp;<strong><span class="orgazCount"></span></strong>&nbsp;New Organization Added
                                    &nbsp;<i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                        <li style="padding: 0px;border-bottom:0px;">
                            <div id="orgMenu" class="inner-menu">

                            </div>
                        </li>
                    </ul>
                </li>
                    <li role="presentation" class="dropdown" runat="server" id="CaseList" visible="false"> 
                    <a href="javascript:;" id="caseLi" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <img src="../images/icons/headphone_mic_large.png" width="28px" />
                        <span class="badge bg-red caseCount icaseCount">6</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                        <li style="
    background: #f7f7f7;
    margin-top: 0px;
">
                            <div class="text-center">
                                <a>
                                    <a href="javascript:void(0)">You have&nbsp;<strong><span class="caseCount"></span></strong>&nbsp;New Case Assigned
                                    &nbsp;<i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                        <li style="padding: 0px;border-bottom:0px;">
                            <div id="caseMenu" class="inner-menu">

                            </div>
                        </li>
                      
                    </ul>
                </li>
                 <li role="presentation" class="dropdown" runat="server" id="oppList" visible="false">
                    <a href="javascript:;" id="oppLi" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <img src="../images/opportunity.png" width="27px" style="padding-top: 4px;" />
                        <span class="badge bg-red opportunityCount iopportunityCount">0</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                        <li style="
    background: #f7f7f7;
    margin-top: 0px;
">
                            <div class="text-center">
                                <a>
                                    <a href="javascript:void(0)">You have&nbsp;<strong><span class="opportunityCount"></span></strong>&nbsp;New Opportunity Assigned
                                    &nbsp;<i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                        <li style="padding: 0px;border-bottom:0px;">
                            <div id="opportunityMenu" class="inner-menu">

                            </div>
                        </li>
                      
                    </ul>
                </li>
                <li role="presentation" class="dropdown" id="orderList" runat="server" visible="false">
                    <a href="javascript:;" id="orderLi" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <img src="../images/icons/cart_large.png" width="30px" />
                        <span class="badge bg-red orderCount iorderCount">0</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                        <li style="
    background: #f7f7f7;
    margin-top: 0px;
">
                            <div class="text-center">
                                <a>
                                    <a href="javascript:void(0)">You have&nbsp;<strong><span class="orderCount"></span></strong>&nbsp;New Order Assigned
                                    &nbsp;<i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                        <li style="padding: 0px;border-bottom:0px;">
                            <div id="orderMenu" class="inner-menu">

                            </div>
                        </li>
                      
                    </ul>
                </li>
                <li role="presentation" class="dropdown" id="projectList" runat="server" visible="false">
                    <a href="javascript:;" id="projectLi" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <img src="../images/Compass-32.gif" width="32px" />
                        <span class="badge bg-red projectCount iprojectCount" style="right: 11px;">0</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                        <li style="
    background: #f7f7f7;
    margin-top: 0px;
">
                            <div class="text-center">
                                <a>
                                    <a href="javascript:void(0)">You have&nbsp;<strong><span class="projectCount"></span></strong>&nbsp;New Projects Assigned
                                    &nbsp;<i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                        <li style="padding: 0px;border-bottom:0px;">
                            <div id="projectMenu" class="inner-menu">

                            </div>
                        </li>
                      
                    </ul>
                </li>
                 <li role="presentation" class="dropdown" id="emailList" runat="server" visible="false">
                    <a href="javascript:;" id="emailLi" class="dropdown-toggle info-number" data-toggle="dropdown"style="padding-top: 22px;" aria-expanded="false">
                        <img src="../images/EMailSearch.png" width="24px"  />
                        <span class="badge bg-red EmailCount iEmailCount">0</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                        <li style="
    background: #f7f7f7;
    margin-top: 0px;
">
                            <div class="text-center">
                                <a>
                                    <a href="javascript:void(0)">You have&nbsp;<strong><span class="EmailCount"></span></strong>&nbsp;New Email
                                    &nbsp;<i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                        <li style="padding: 0px;border-bottom:0px;">
                            <div id="EmailMenu" class="inner-menu">

                            </div>
                        </li>
                      
                    </ul>
                </li>
                <li role="presentation" class="dropdown" id="TicklerList" runat="server" visible="false">
                    <a href="javascript:;" id="ticklerLi" class="dropdown-toggle info-number" data-toggle="dropdown"style="padding-top: 18px;" aria-expanded="false">
                        <img src="../images/ActionItem-32.gif" width="24px"  />
                        <span class="badge bg-red TicklerCount iTicklerCount">0</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                        <li style="
    background: #f7f7f7;
    margin-top: 0px;
">
                            <div class="text-center">
                                <a>
                                    <a href="javascript:void(0)">You have&nbsp;<strong><span class="TicklerCount"></span></strong>&nbsp;New Tickler Assigned
                                    &nbsp;<i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                        <li style="padding: 0px;border-bottom:0px;">
                            <div id="TicklerMenu" class="inner-menu">

                            </div>
                        </li>
                      
                    </ul>
                </li>
                <li role="presentation" class="dropdown" id="ProcessList" runat="server" visible="false">
                    <a href="javascript:;" id="processLi" class="dropdown-toggle info-number" data-toggle="dropdown"style="padding-top: 18px;" aria-expanded="false">
                        <img src="../images/process.png" width="24px"  />
                        <span class="badge bg-red ProcessCount iProcessCount">0</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                        <li style="
    background: #f7f7f7;
    margin-top: 0px;
">
                            <div class="text-center">
                                <a>
                                    <a href="javascript:void(0)">You have&nbsp;<strong><span class="ProcessCount"></span></strong>&nbsp;New Process Updated
                                    &nbsp;<i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                        <li style="padding: 0px;border-bottom:0px;">
                            <div id="ProcessMenu" class="inner-menu">

                            </div>
                        </li>
                      
                    </ul>
                </li>
            </ul>
        </nav>
                                        <div>
                                            <span id="newData"></span>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td>
                            <telerik:RadTabStrip ID="RadTabStrip1" runat="server" EnableEmbeddedSkins="false"
                                Skin="BizSkin" SelectedIndex="0" ScrollChildren="true" OnClientLoad="ClientTabstripLoad">
                                <Tabs>
                                </Tabs>
                            </telerik:RadTabStrip>
                        </td>
                    </tr>
                    <tr class="SearchStyle">
                        <td height="20px" valign="top">
                            <table class="ShortCutBar" runat="server" id="Table1" cellspacing="0" cellpadding="0"
                                width="100%" border="0" height="20px">
                                <tr>
                                    <td class="Text_W" runat="server" align="left" id="tdLink" width="94%" style="white-space: nowrap">
                                        <div id="divTop" style="overflow: hidden; white-space: nowrap; display: block;">
                                            <div id="divLink" style="float: left; vertical-align: bottom;">
                                            </div>
                                            <div id="divFavourite" runat="server" style="vertical-align: bottom;">
                                            </div>
                                        </div>
                                    </td>
                                    <td align="left">
                                        <div style="position: absolute;">
                                            <a href="#" onmouseover="scrollDivRight('divTop')" onmouseout="stopMe()" class='navprev'></a><a href="#" onmouseover="scrollDivLeft('divTop')" onmouseout="stopMe()" class="navnext"></a>
                                        </div>
                                    </td>
                                    <td class="Text_WB" align="right" width="6%">
                                        <asp:HyperLink runat="server" ID="hypConfMenu" ToolTip="Manage shortcut bar">
                                    <img src="../images/Settings.png" style="cursor: pointer;" alt=""/>
                                        </asp:HyperLink>&nbsp;&nbsp;
                                <img style="cursor: pointer; margin-right: 5px;" title="submit feedback" src="../images/UserFeeback.png" border="0" onclick="reDirectPage('../common/frmReportBug.aspx?Mode=1')" alt="" />
                                        <img style="cursor: pointer; margin-right: 5px;" title="Report a Bug" src="../images/bug1.png" border="0" onclick="reDirectPage('../common/frmReportBug.aspx?Mode=0')" alt="" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hdnSessionTimeout" runat="server" />
                <asp:HiddenField ID="hdnPopupURL" runat="server" />

                <script type="text/javascript">
                    setMenuWidth();
                    RemindCall();

                    $(window).resize(function () {
                        setMenuWidth();
                    });

                    function setMenuWidth() {
                        document.getElementById('RadTabStrip1').style.width = $(window).width() - 30 + "px";
                        document.getElementById('RadTabStrip1').style.height = "26px";
                        document.getElementById('divTop').style.width = $(window).width() - 130 + "px";
                    }
                    $(function () {
                        jq1.idleTimeout('#idletimeout', '#idletimeout a', {
                            warningLength: 300, //5min show warning before 5 mins
                            idleAfter: $("#hdnSessionTimeout").val(), //45 mins=2700secs
                            pollingInterval: 600, //10 mins
                            keepAliveURL: '../Common/Common.asmx/keepSessionAlive',
                            serverResponseEquals: 'OK',
                            onTimeout: function () {
                                $(this).slideUp();
                                parent.window.location = "../Login.aspx?Msg=1";
                            },
                            onIdle: function () {
                                $(this).slideDown(); // show the warning bar
                            },
                            onCountdown: function (counter) {
                                $(this).find("span").html(counter); // update the counter
                            },
                            onResume: function () {
                                $(this).slideUp(); // hide the warning bar
                            }
                        });
                    });

                    setInterval("RemindCall()", 300000);
                    function RemindCall() {
                        $.ajax({
                            type: "POST", //
                            url: "../OutlookCalendar/DataFeedCalendar.aspx?method=reminders",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",

                            success: function (data) {
                                if (data.IsSuccess) {
                                    OpenRemindersWindow();
                                }
                            }
                        });
                    }
                    function AlertCall() {
                        $.ajax({
                            type: "POST", //
                            url: "../common/WorkFlowService.asmx/GetAlertData",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                if ($.isEmptyObject(data)) {
                                    console.log("data is empty");
                                }
                                else {
                                    console.log("sachin" + data);
                                    console.log("sachin empty" + data.length);
                                    OpenAlertsWindow();
                                }


                            }
                        });
                    }
                </script>
            </form>
        </div>
        <div style="top: 163px; bottom: 0px; width: 100%; position: absolute">
            <iframe id="mainframe" runat="server" name="mainframe" frameborder="0" style="width: 100%; left: 0; position: absolute; height: 100%"></iframe>
        </div>
    </div>
</body>
<%--<frameset frameborder="0">
		        <frameset id="frmTickler" rows="*"  framespacing="0" frameborder="0" >
			        <frame id="mainframe" runat="server" name="mainframe" frameborder="0" />
		        </frameset>
	        </frameset>--%>
</html>
