﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Collections.Generic
Imports DatabaseNotification
Imports System.Web.Services
Imports System.Data.SqlClient
Imports BACRM.BusinessLogic.Outlook

Public Class BizMaster
    Inherits BACRMMasterPage

#Region "Member Variables"
    Private objCommon As CCommon
    Private strApplicationPath As String
    Private dtSubMenu As DataTable
    Private arrAddedMenuIds As ArrayList
    Private arrAdminAddedMenuIds As ArrayList
    Private arrAdvanceSearchAddedMenuIds As ArrayList
    Private m_aryRightsForAdmin() As Integer
    Private m_aryRightsForAdvanceSearch() As Integer
    Private objAdmin As ActionItem
#End Region

#Region "Constructor"
    Sub New()
        objCommon = New CCommon
        objAdmin = New ActionItem
    End Sub
#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            strApplicationPath = If(Request.ApplicationPath.Length > 1, Request.ApplicationPath, "")

            If Not Page.IsPostBack Then
                Dim strFilePath As String
                If CCommon.ToBool(Session("bitLogoAppliedToBizTheme")) = True Then
                    If CCommon.ToString(Session("vcLogoForBizTheme")) <> "" Then
                        Dim FilePath = CCommon.GetDocumentPath(Session("DomainID"))
                        strFilePath = FilePath & CCommon.ToString(Session("vcLogoForBizTheme"))
                        imgThemeLogo.ImageUrl = strFilePath
                    Else
                        strFilePath = "~/images/BizBlack.png"
                        imgThemeLogo.ImageUrl = strFilePath
                    End If
                End If

                If Session("UserContactID") = "1" Then
                    liSubscription.Visible = True
                Else
                    liSubscription.Visible = False
                End If
                If Convert.ToString(Session("ProfilePic")) = "" Then
                    imgUserLarge.ImageUrl = "~/images/ContactPic.png"
                    imgUser.ImageUrl = "~/images/ContactPic.png"
                Else
                    imgUserLarge.ImageUrl = "~/profile/" & Convert.ToString(Session("ProfilePic"))
                    imgUser.ImageUrl = "~/profile/" & Convert.ToString(Session("ProfilePic"))
                End If
                If Session("Trial") = True Then
                    lblSubMessage.Text = "Trial Version"
                ElseIf Session("NoOfDaysLeft") < 30 Then
                    lblSubMessage.Text = "Subscription will expire in " & Session("NoOfDaysLeft") & " days"
                End If

                lblUserName.Text = Session("ContactName")
                lblUserNameDetail.Text = Session("ContactName")
                lblMemmberSince.Text = CCommon.ToSqlDate(Session("UserCeatedDate")).ToString("MMM yyyy")

                BindMenu()
                ShowHideAdminLinks() 'Keep This After BindMenu
                BindCalendar()
                ShowHideAlertPanel()

                If Not CCommon.ToBool(Session("DisplayedAlertIntially")) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "AlertMain", "RemindCall()", True)
                    Session("DisplayedAlertIntially") = True
                End If

                If CCommon.ToShort(Session("PayrollType")) <> 2 Then
                    aUserClockInOut.Visible = True
                    btnUserClockInOut.Visible = True

                    If CCommon.ToBool(Session("IsUserClockedIn")) Then
                        btnUserClockInOut.Style.Add("color", "#ff0000")
                        btnUserClockInOut.Value = "Out"
                    Else
                        btnUserClockInOut.Style.Add("color", "#00b050")
                        btnUserClockInOut.Value = "In"
                    End If
                End If
            ElseIf Not ScriptManager.GetCurrent(Me.Page).IsInAsyncPostBack Then
                BindMenu()
                ShowHideAdminLinks() 'Keep This After BindMenu
                ShowHideAlertPanel()
            End If
        Catch ex As Exception
            DisplayError(ex.Message)
        Finally
            Page.Header.DataBind()
        End Try
    End Sub

#End Region

#Region "Event Handlers"
    Private Sub lkbSignOut_Click(sender As Object, e As EventArgs) Handles lkbSignOut.Click
        Try
            System.Web.Security.FormsAuthentication.SignOut()
            Session.Abandon()
            Response.Redirect("~/Login.aspx", True)
        Catch exThread As Threading.ThreadAbortException
            'Do not log
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnDismiss_Click(sender As Object, e As EventArgs) Handles btnDismiss.Click
        Try
            If Not String.IsNullOrEmpty(hdnActionItemID.Value) AndAlso Not String.IsNullOrEmpty(hdnType.Value) Then
                If hdnType.Value = "1" Then
                    'Calender
                    Dim ObjOutlook As New COutlook
                    ObjOutlook.ActivityID = CCommon.ToLong(hdnActionItemID.Value)
                    ObjOutlook.Status = 0
                    ObjOutlook.LastReminderDateTimeUtc = DateTime.UtcNow
                    ObjOutlook.UpdateReminderByActivityId()
                ElseIf hdnType.Value = "2" Then
                    'Action Item / Communication
                    Dim objActionItem As New ActionItem
                    objActionItem.CommID = CCommon.ToLong(hdnActionItemID.Value)
                    objActionItem.DismissReminder()
                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "RemindCall()", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

#Region "Private Methods"


#Region "Alert Panel"



#End Region

    Private Sub BindMenu()
        arrAddedMenuIds = New ArrayList

        m_aryRightsForAdmin = GetUserRightsForPage_Other(13, 46)
        m_aryRightsForAdvanceSearch = GetUserRightsForPage_Other(13, 48)

        If Cache("BizMenu" & Session("UserContactId")) Is Nothing _
            Or Cache("BizAdminMenu" & Session("UserContactId")) Is Nothing _
            Or (Session("UserContactID") = Session("AdminID") AndAlso (Cache("BizAdminMenu" & Session("UserContactId")) Is Nothing Or Cache("BizAdvanceSearchMenu" & Session("UserContactId")) Is Nothing)) _
            Or (m_aryRightsForAdmin(RIGHTSTYPE.VIEW) <> 0 AndAlso Cache("BizAdminMenu" & Session("UserContactId")) Is Nothing) _
            Or (m_aryRightsForAdvanceSearch(RIGHTSTYPE.VIEW) <> 0 AndAlso Cache("BizAdvanceSearchMenu" & Session("UserContactId")) Is Nothing) Then


            Dim strMenu As New System.Text.StringBuilder
            strMenu.Append("<ul id=""main-menu"" class=""nav navbar-nav main-navigation"" >")


            Try
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                objCommon.GroupID = CCommon.ToLong(Session("UserGroupID"))
                Dim ds As DataSet = objCommon.GetNavigation()

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Select("numTabID<>-1").Count > 0 Then
                    Dim dtMain As DataTable = ds.Tables(0)
                    dtSubMenu = ds.Tables(1)

                    For Each dr As DataRow In dtMain.Select("numTabID<>-1").OrderBy(Function(x) x("numOrder"))
                        GenerateOuterLI(dr, strMenu)
                    Next
                End If

                strMenu.Append("</ul>")
                Cache("BizMenu" & Session("UserContactId")) = strMenu.ToString()
            Catch ex As Exception
                strMenu.Append("</ul>")
                Throw
            Finally
                divBizNavMenu.InnerHtml = strMenu.ToString()
            End Try
        Else
            divBizNavMenu.InnerHtml = CCommon.ToString(Cache("BizMenu" & Session("UserContactId")))
        End If
    End Sub

    Private Sub BindCalendar()
        Try
            If Session("DomainID") > 0 Then
                If Session("ResourceId") Is Nothing Then
                    Dim objOutlook As New COutlook
                    Dim dtTable As DataTable
                    objOutlook.UserCntID = Session("UserContactId")
                    objOutlook.DomainID = Session("DomainId")
                    dtTable = objOutlook.GetResourceId()
                    If Not dtTable Is Nothing Then
                        If dtTable.Rows.Count > 0 Then
                            ' Dim activeuser As String = Session("ContactName").ToString
                            Session("ResourceId") = dtTable.Rows(0).Item("ResourceId")
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindAdminMenu()
        Try
            If Cache("BizAdminMenu" & Session("UserContactId")) Is Nothing Then
                If Not dtSubMenu Is Nothing AndAlso dtSubMenu.Select("numTabID=-1").Count > 0 Then
                    arrAdminAddedMenuIds = New ArrayList

                    Dim strAdminMenu As New System.Text.StringBuilder
                    strAdminMenu.Append("<div class=""col-xs-12 nav-admin"">")

                    Dim strdiv1 As New System.Text.StringBuilder
                    strdiv1.Append("<div class=""col-xs-12 col-sm-6"">")
                    strdiv1.Append("<ul class=""mega-menu-ul"">")

                    Dim strdiv2 As New System.Text.StringBuilder
                    strdiv2.Append("<div class=""col-xs-12 col-sm-6"">")
                    strdiv2.Append("<ul class=""mega-menu-ul"">")



                    Dim i As Int16 = 1

                    For Each dr As DataRow In dtSubMenu.Select("numTabID=-1")
                        If Not arrAdminAddedMenuIds.Contains(dr("ID")) Then
                            arrAdminAddedMenuIds.Add(dr("ID"))

                            Dim strInnerLI As String = GenerateAdminLI(dr)
                            strInnerLI = strInnerLI.Replace("##PLACEHOLDER-FOR-LI-CLASS##", "class=""dropdown-header""")

                            If i <= 5 Then
                                strdiv1.Append(strInnerLI)
                                GenerateAdminMenuRecursively(-1, dr("ID"), strdiv1)
                            ElseIf i <= 10 Then
                                strdiv2.Append(strInnerLI)
                                GenerateAdminMenuRecursively(-1, dr("ID"), strdiv2)
                            Else
                                If i Mod 2 = 0 Then
                                    strdiv2.Append(strInnerLI)
                                    GenerateAdminMenuRecursively(-1, dr("ID"), strdiv2)
                                Else
                                    strdiv1.Append(strInnerLI)
                                    GenerateAdminMenuRecursively(-1, dr("ID"), strdiv1)
                                End If
                            End If

                            i += 1
                        End If
                    Next

                    strdiv1.Append("</ul>")
                    strdiv2.Append("</ul>")

                    strdiv1.Append("</div>")
                    strdiv2.Append("</div>")

                    strAdminMenu.Append(strdiv1)
                    strAdminMenu.Append(strdiv2)

                    liBizAdminMenu.InnerHtml = strAdminMenu.ToString()
                    Cache("BizAdminMenu" & Session("UserContactId")) = strAdminMenu.ToString()
                End If
            Else
                liBizAdminMenu.InnerHtml = CCommon.ToString(Cache("BizAdminMenu" & Session("UserContactId")))
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindAlertPanelCount()
        Try
            If Cache("Alert_Company_" & Session("UserContactId")) Is Nothing _
                Or Cache("Alert_Company_" & Session("UserContactId")) Is Nothing _
                Or Cache("Alert_Company_" & Session("UserContactId")) Is Nothing _
                Or Cache("Alert_Company_" & Session("UserContactId")) Is Nothing _
                Or Cache("Alert_Company_" & Session("UserContactId")) Is Nothing _
                Or Cache("Alert_Company_" & Session("UserContactId")) Is Nothing _
                Or Cache("Alert_Company_" & Session("UserContactId")) Is Nothing _
                Or Cache("Alert_Company_" & Session("UserContactId")) Is Nothing Then

                Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
                'Dim org As New Organization()
                Dim ds As New DataSet
                Using connection As New Npgsql.NpgsqlConnection(conStr)
                    'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                    Using command As New Npgsql.NpgsqlCommand()
                        Dim da As Npgsql.NpgsqlDataAdapter
                        command.CommandType = CommandType.StoredProcedure
                        command.Connection = connection
                        command.CommandText = "usp_countalertpanel"

                        command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "ALLC"})
                        command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                        command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                        command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 1})
                        command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                        command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                        Dim j As Int32 = 0
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            command.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        ds.Tables.Add(parm.Value.ToString())
                                        da.Fill(ds.Tables(j))
                                        j += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            orgAlertCount.InnerText = CCommon.ToInteger(ds.Tables(0).Rows(0)("company"))
                            caseAlertCount.InnerText = CCommon.ToInteger(ds.Tables(0).Rows(0)("case"))
                            oppAlertCount.InnerText = CCommon.ToInteger(ds.Tables(0).Rows(0)("opp"))
                            orderAlertCount.InnerText = CCommon.ToInteger(ds.Tables(0).Rows(0)("order"))
                            projectAlertCount.InnerText = CCommon.ToInteger(ds.Tables(0).Rows(0)("project"))
                            emailAlertCount.InnerText = CCommon.ToInteger(ds.Tables(0).Rows(0)("Email"))
                            ticklerAlertCount.InnerText = CCommon.ToInteger(ds.Tables(0).Rows(0)("tickler"))
                            processAlertCount.InnerText = CCommon.ToInteger(ds.Tables(0).Rows(0)("process"))
                        Else
                            orgAlertCount.InnerText = "0"
                            caseAlertCount.InnerText = "0"
                            oppAlertCount.InnerText = "0"
                            orderAlertCount.InnerText = "0"
                            projectAlertCount.InnerText = "0"
                            emailAlertCount.InnerText = "0"
                            ticklerAlertCount.InnerText = "0"
                            processAlertCount.InnerText = "0"
                        End If

                        Cache("Alert_Company_" & Session("UserContactId")) = orgAlertCount.InnerText
                        Cache("Alert_Case_" & Session("UserContactId")) = caseAlertCount.InnerText
                        Cache("Alert_Opp_" & Session("UserContactId")) = oppAlertCount.InnerText
                        Cache("Alert_Order_" & Session("UserContactId")) = orderAlertCount.InnerText
                        Cache("Alert_Project_" & Session("UserContactId")) = projectAlertCount.InnerText
                        Cache("Alert_Email_" & Session("UserContactId")) = emailAlertCount.InnerText
                        Cache("Alert_Tickler_" & Session("UserContactId")) = ticklerAlertCount.InnerText
                        Cache("Alert_Process_" & Session("UserContactId")) = processAlertCount.InnerText
                    End Using
                End Using
            Else
                orgAlertCount.InnerText = Cache("Alert_Company_" & Session("UserContactId"))
                caseAlertCount.InnerText = Cache("Alert_Case_" & Session("UserContactId"))
                oppAlertCount.InnerText = Cache("Alert_Opp_" & Session("UserContactId"))
                orderAlertCount.InnerText = Cache("Alert_Order_" & Session("UserContactId"))
                projectAlertCount.InnerText = Cache("Alert_Project_" & Session("UserContactId"))
                emailAlertCount.InnerText = Cache("Alert_Email_" & Session("UserContactId"))
                ticklerAlertCount.InnerText = Cache("Alert_Tickler_" & Session("UserContactId"))
                processAlertCount.InnerText = Cache("Alert_Process_" & Session("UserContactId"))
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ShowHideAdminLinks()
        Try
            ''Checking The User Rights
            If Session("UserContactID") <> Session("AdminID") Then 'Logged in User is Admin of Domain? yes then override permission and give him access


                If m_aryRightsForAdmin(RIGHTSTYPE.VIEW) = 0 Then
                    liAdmin.Visible = False
                Else
                    BindAdminMenu()
                End If

                If m_aryRightsForAdvanceSearch(RIGHTSTYPE.VIEW) = 0 Then
                    liAdvanceSearch.Visible = False
                Else
                    BindAdvanceSearchMenu()
                End If
            Else
                BindAdminMenu()
                BindAdvanceSearchMenu()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ShowHideAlertPanel()
        Try
            Dim m_aryRightsForAlertOrganization() As Integer = GetUserRightsForPage_Other(42, 118)
            Dim m_aryRightsForAlertCase() As Integer = GetUserRightsForPage_Other(42, 119)
            Dim m_aryRightsForAlertOpportunity() As Integer = GetUserRightsForPage_Other(42, 120)
            Dim m_aryRightsForAlertOrder() As Integer = GetUserRightsForPage_Other(42, 121)
            Dim m_aryRightsForAlertProject() As Integer = GetUserRightsForPage_Other(42, 122)
            Dim m_aryRightsForAlertEmail() As Integer = GetUserRightsForPage_Other(42, 123)
            Dim m_aryRightsForAlertTickler() As Integer = GetUserRightsForPage_Other(42, 124)
            Dim m_aryRightsForAlertProcess() As Integer = GetUserRightsForPage_Other(42, 125)

            If m_aryRightsForAlertOrganization(RIGHTSTYPE.VIEW) <> 0 Then
                OrganizationList.Visible = True
            End If

            If m_aryRightsForAlertCase(RIGHTSTYPE.VIEW) <> 0 Then
                CaseList.Visible = True
            End If

            If m_aryRightsForAlertOpportunity(RIGHTSTYPE.VIEW) <> 0 Then
                oppList.Visible = True
            End If

            If m_aryRightsForAlertOrder(RIGHTSTYPE.VIEW) <> 0 Then
                orderList.Visible = True
            End If

            If m_aryRightsForAlertProject(RIGHTSTYPE.VIEW) <> 0 Then
                projectList.Visible = True
            End If

            If m_aryRightsForAlertEmail(RIGHTSTYPE.VIEW) <> 0 Then
                emailList.Visible = True
            End If

            If m_aryRightsForAlertTickler(RIGHTSTYPE.VIEW) <> 0 Then
                TicklerList.Visible = True
            End If

            If m_aryRightsForAlertProcess(RIGHTSTYPE.VIEW) <> 0 Then
                ProcessList.Visible = True
            End If

            If OrganizationList.Visible Or _
                CaseList.Visible Or _
                oppList.Visible Or _
                orderList.Visible Or _
                projectList.Visible Or _
                emailList.Visible Or _
                TicklerList.Visible Or _
                ProcessList.Visible Then
                BindAlertPanelCount()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindAdvanceSearchMenu()
        Try
            If Cache("BizAdvanceSearchMenu" & Session("UserContactId")) Is Nothing Then
                If Not dtSubMenu Is Nothing AndAlso dtSubMenu.Select("numTabID=-3").Count > 0 Then
                    arrAdvanceSearchAddedMenuIds = New ArrayList

                    Dim strAdvanceSearchMenu As New System.Text.StringBuilder
                    strAdvanceSearchMenu.Append("<div class=""col-xs-12 nav-advancesearch"">")
                    strAdvanceSearchMenu.Append("<ul class=""mega-menu-ul"">")


                    For Each dr As DataRow In dtSubMenu.Select("numTabID=-3")
                        If Not arrAdvanceSearchAddedMenuIds.Contains(dr("ID")) Then
                            arrAdvanceSearchAddedMenuIds.Add(dr("ID"))

                            Dim strliInner As String = GenerateAdminLI(dr)
                            strliInner = strliInner.Replace("##PLACEHOLDER-FOR-LI-CLASS##", "class=""dropdown-header""")
                            strAdvanceSearchMenu.Append(strliInner)

                            GenerateAdvanceSearchMenuRecursively(-3, dr("ID"), strAdvanceSearchMenu)
                        End If
                    Next

                    strAdvanceSearchMenu.Append("</ul>")
                    strAdvanceSearchMenu.Append("</div>")

                    liBizAdvanceSearchMenu.InnerHtml = strAdvanceSearchMenu.ToString()
                    Cache("BizAdvanceSearchMenu" & Session("UserContactId")) = strAdvanceSearchMenu.ToString()
                End If
            Else
                liBizAdvanceSearchMenu.InnerHtml = CCommon.ToString(Cache("BizAdvanceSearchMenu" & Session("UserContactId")))
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function GenerateOuterLI(ByVal dr As DataRow, ByRef strMenu As System.Text.StringBuilder) As HtmlGenericControl
        Try
            strMenu.Append("<li ##PLACEHOLDER-FOR-OUTER-LI##>")
            strMenu.Append("<div style=""white-space:nowrap"">")


            If Not String.IsNullOrEmpty(dr("vcAddURL")) Then
                Dim aAdd As New HtmlGenericControl("a")

                If CCommon.ToBool(dr("bitAddIsPopUp")) Then
                    strMenu.Append("<a style=""cursor:pointer"" class=""pull-left popupIconClass"" onclick=""OpenPopUp('" & strApplicationPath & "/" & dr("vcAddURL") & "')"">")
                Else
                    strMenu.Append("<a style=""cursor:pointer"" class=""pull-left popupIconClass"" href=""" & strApplicationPath & "/" & dr("vcAddURL") & """>")
                End If


                strMenu.Append("<span><i class=""fa fa-plus""></i></span>")
                strMenu.Append("</a>")

            End If


            If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcURL"))) Then
                strMenu.Append("<a class=""pull-left linkClass"" href=""" & strApplicationPath & "/" & dr("vcURL") & """>")
            Else
                strMenu.Append("<a class=""linkClass"">")
            End If

            strMenu.Append("<span>" & dr("vcTabName") & "</span>")
            Dim span As New HtmlGenericControl("span")

            strMenu.Append("</a>")




            Dim arrRows As DataRow() = dtSubMenu.Select("numTabID=" & dr("numTabID"))
            If Not arrRows Is Nothing AndAlso arrRows.Length > 0 Then
                strMenu.Append("<label class=""pull-right paddingRightOnly""><b class=""caret""></b></label>")
            End If

            strMenu.Append("</div>")

            GenerateInnerMenu(CCommon.ToLong(dr("numTabID")), strMenu)

            strMenu.Append("</li>")
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function GenerateInnerMenu(ByVal numTabID As Long, ByRef strMenu As System.Text.StringBuilder)
        Try
            Dim arrRows As DataRow() = dtSubMenu.Select("numTabID=" & numTabID).OrderBy(Function(x) x("numParentID")).ToArray()

            If Not arrRows Is Nothing AndAlso arrRows.Length > 0 Then
                strMenu.Replace("##PLACEHOLDER-FOR-OUTER-LI##", "class=""dropdown""")
                strMenu.Append("<ul class=""dropdown-menu"">")

                For Each dr As DataRow In arrRows
                    If Not arrAddedMenuIds.Contains(dr("ID")) Then
                        arrAddedMenuIds.Add(dr("ID"))

                        GenerateInnerLI(dr, strMenu)
                        GenerateInnerMenuRecursively(numTabID, dr("ID"), strMenu)

                        strMenu.Append("</li>") 'Closing li started from GenerateInnerLI method
                    End If
                Next

                strMenu.Append("</ul>")
            Else
                strMenu.Replace("##PLACEHOLDER-FOR-OUTER-LI##", "")
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function GenerateInnerMenuRecursively(ByVal numTabID As Long, ByVal ID As Long, ByRef strMenu As System.Text.StringBuilder) As HtmlGenericControl
        Try
            Dim arrRows As DataRow() = dtSubMenu.Select("numTabID=" & numTabID & " AND numParentID=" & ID).OrderBy(Function(x) x("numParentID")).ToArray()

            If Not arrRows Is Nothing AndAlso arrRows.Length > 0 Then
                strMenu.Replace("##PLACEHODER-FOR-LI-CLASS-CHANGE##", "class=""dropdown-submenu""")

                strMenu.Append("<ul class=""dropdown-menu"">")

                For Each drInner As DataRow In arrRows
                    If Not arrAddedMenuIds.Contains(drInner("ID")) Then
                        arrAddedMenuIds.Add(drInner("ID"))

                        GenerateInnerLI(drInner, strMenu)
                        GenerateInnerMenuRecursively(numTabID, drInner("ID"), strMenu)

                        strMenu.Append("</li>") 'Closing li started from GenerateInnerLI method
                    End If
                Next

                strMenu.Append("</ul>")
            Else
                strMenu.Replace("##PLACEHODER-FOR-LI-CLASS-CHANGE##", "")
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub GenerateInnerLI(ByVal dr As DataRow, ByRef strMenu As System.Text.StringBuilder, Optional ByVal isMegaMenu As Boolean = False)
        Try
            If dr("numTabID") = "6" Then
                strMenu.Append("<li style=""height:40px"" ##PLACEHODER-FOR-LI-CLASS-CHANGE##>")
            Else
                strMenu.Append("<li ##PLACEHODER-FOR-LI-CLASS-CHANGE##>")
            End If


            Dim arrRows As DataRow() = dtSubMenu.Select("numTabID=" & dr("numTabID") & " AND numParentID=" & dr("ID")).OrderBy(Function(x) x("numParentID")).ToArray()
            If Not arrRows Is Nothing AndAlso arrRows.Length > 0 AndAlso isMegaMenu Then
                strMenu.Replace("##PLACEHODER-FOR-LI-CLASS-CHANGE##", "class=""dropdown-header""")
            End If

            strMenu.Append("<div style=""white-space:nowrap"">")


            If Not String.IsNullOrEmpty(dr("vcAddURL")) Then
                If CCommon.ToBool(dr("bitAddIsPopUp")) Then
                    strMenu.Append("<a style=""cursor:pointer"" class=""pull-left popupIconClass"" onclick=""" & "OpenPopUp('" & strApplicationPath & dr("vcAddURL").Replace("~/", "/") & "')" & """>")
                Else
                    strMenu.Append("<a class=""pull-left popupIconClass"" href=""" & strApplicationPath & CCommon.ToString(dr("vcAddURL")).Replace("~/", "/") & """>")
                End If

                strMenu.Append("<span><i class="" fa fa-plus""></i></span>")
            Else
                If String.IsNullOrEmpty(CCommon.ToString(dr("vcImageURL"))) Then
                    If Not arrRows Is Nothing AndAlso arrRows.Length > 0 AndAlso Not isMegaMenu Then
                        strMenu.Append("<a style=""cursor:pointer;width: 24px;""  class=""pull-left popupIconClass"">&nbsp;</a>")
                    Else
                        strMenu.Append("<a style=""cursor:pointer;width: 21px;""  class=""pull-left popupIconClass"">&nbsp;</a>")
                    End If
                End If
            End If


            If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcURL"))) Then
                If Not arrRows Is Nothing AndAlso arrRows.Length > 0 AndAlso Not isMegaMenu Then
                    strMenu.Append("<a class=""pull-left linkClass"" style=""padding-right:25px"" href=""" & strApplicationPath & CCommon.ToString(dr("vcURL")).Replace("../", "/") & """>")
                Else
                    strMenu.Append("<a class=""pull-left linkClass"" href=""" & strApplicationPath & CCommon.ToString(dr("vcURL")).Replace("../", "/") & """>")
                End If
            Else
                If Not arrRows Is Nothing AndAlso arrRows.Length > 0 AndAlso Not isMegaMenu Then
                    strMenu.Append("<a class=""pull-left linkClass"" style=""padding-right:25px"">")
                Else
                    strMenu.Append("<a class=""pull-left linkClass"">")
                End If

            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcImageURL"))) Then
                If dr("numTabID") = "6" Then
                    strMenu.Append("<img style=""height:30px;margin-right:5px"" src=""" & strApplicationPath & CCommon.ToString(dr("vcImageURL")).Replace("../", "/") & """/>")
                Else
                    strMenu.Append("<img style=""height:16px;width:16px;margin-right:5px"" src=""" & strApplicationPath & CCommon.ToString(dr("vcImageURL")).Replace("../", "/") & """/>")
                End If
            End If

            If dr("numTabID") = "6" Then
                strMenu.Append("<span style=""line-height:40px"">" & dr("vcNodeName") & "</span>")
            Else
                strMenu.Append("<span>" & dr("vcNodeName") & "</span>")
            End If
            strMenu.Append("</a>")

            If Not arrRows Is Nothing AndAlso arrRows.Length > 0 AndAlso Not isMegaMenu Then
                strMenu.Append("<label class=""pull-right paddingRightOnly""><b class=""caret""></b></label>")
            End If


            strMenu.Append("</div>")

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function GenerateAdminLI(ByVal dr As DataRow) As String
        Try
            Dim strBuilder As New System.Text.StringBuilder

            Dim arrRows As DataRow() = dtSubMenu.Select("numTabID=" & dr("numTabID") & " AND numParentID=" & dr("ID")).OrderBy(Function(x) x("numParentID")).ToArray()

            If Not arrRows Is Nothing AndAlso arrRows.Length > 0 Then
                strBuilder.Append("<li class=""dropdown-header"">")
            Else
                strBuilder.Append("<li ##PLACEHOLDER-FOR-LI-CLASS## >")
            End If


            Dim a As New HtmlGenericControl("a")

            If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcURL"))) Then
                strBuilder.Append("<a href=""" & strApplicationPath & CCommon.ToString(dr("vcURL")).Replace("../", "/") & """>")
            Else
                strBuilder.Append("<a>")
            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcImageURL"))) Then
                strBuilder.Append("<img style=""margin-right:5px"" src=""" & strApplicationPath & CCommon.ToString(dr("vcImageURL")).Replace("../", "/").Replace("~/", "/") & """/>")
            End If


            strBuilder.Append("<span>" & dr("vcNodeName") & "</span>")

            strBuilder.Append("</a>")
            strBuilder.Append("</li>")

            Return strBuilder.ToString()
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function GenerateAdminMenuRecursively(ByVal numTabID As Long, ByVal ID As Long, ByRef strDiv As System.Text.StringBuilder) As HtmlGenericControl
        Try
            Dim arrRows As DataRow() = dtSubMenu.Select("numTabID=" & numTabID & " AND numParentID=" & ID).OrderBy(Function(x) x("numParentID")).ToArray()

            If Not arrRows Is Nothing AndAlso arrRows.Length > 0 Then
                For Each drInner As DataRow In arrRows
                    If Not arrAdminAddedMenuIds.Contains(drInner("ID")) Then
                        arrAdminAddedMenuIds.Add(drInner("ID"))

                        Dim strLiInner As String = GenerateAdminLI(drInner)
                        strDiv.Append(strLiInner)

                        GenerateAdminMenuRecursively(numTabID, drInner("ID"), strDiv)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function GenerateAdvanceSearchMenuRecursively(ByVal numTabID As Long, ByVal ID As Long, ByRef strDiv As System.Text.StringBuilder) As HtmlGenericControl
        Try
            Dim arrRows As DataRow() = dtSubMenu.Select("numTabID=" & numTabID & " AND numParentID=" & ID).OrderBy(Function(x) x("numParentID")).ToArray()

            If Not arrRows Is Nothing AndAlso arrRows.Length > 0 Then
                For Each drInner As DataRow In arrRows
                    If Not arrAdvanceSearchAddedMenuIds.Contains(drInner("ID")) Then
                        arrAdvanceSearchAddedMenuIds.Add(drInner("ID"))

                        Dim strLiInner As String = GenerateAdminLI(drInner)
                        strLiInner = strLiInner.Replace("##PLACEHOLDER-FOR-LI-CLASS##", "class=""dropdown-header""")
                        strDiv.Append(strLiInner)

                        GenerateAdminMenuRecursively(numTabID, drInner("ID"), strDiv)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

#End Region

#Region "Public Methods"
    Public Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception

        End Try
    End Sub
#End Region

    'Private Sub btnCheckReminder_Click(sender As Object, e As EventArgs) Handles btnCheckReminder.Click
    '    Try
    '        If Session("LastReminderPopupTime") <= DateTime.Now.AddMinutes(-5) Then
    '            ScriptManager.RegisterClientScriptBlock(UpdatePanelReminder, UpdatePanelReminder.GetType(), "Alert", "RemindCall()", True)
    '            Session("LastReminderPopupTime") = DateTime.Now
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(CCommon.ToString(ex))
    '    End Try
    'End Sub

    Protected Sub btnSimpleSearchPostback_Click(sender As Object, e As EventArgs)
        Try
            If CCommon.ToString(hdnSimpleSearchText.Value).Length > 0 AndAlso CCommon.ToString(hdnSimpleSearchText.Value).IndexOf("[") > 0 AndAlso CCommon.ToString(hdnSimpleSearchText.Value).IndexOf("]") > 0 AndAlso (CCommon.ToString(hdnSimpleSearchText.Value).IndexOf("]") > CCommon.ToString(hdnSimpleSearchText.Value).IndexOf("[")) Then
                Dim dateRange As String = CCommon.ToString(hdnSimpleSearchText.Value).Substring(CCommon.ToString(hdnSimpleSearchText.Value).IndexOf("["), CCommon.ToString(hdnSimpleSearchText.Value).IndexOf("]") - CCommon.ToString(hdnSimpleSearchText.Value).IndexOf("[") + 1)
                hdnSimpleSearchText.Value = CCommon.ToString(hdnSimpleSearchText.Value).Replace(dateRange, "").Trim()
            End If

            Dim strScript As String
            Dim vcSearchText As String
            If CCommon.ToString(hdnSimpleSearchText.Value).ToLower().StartsWith("or:") Then
                Session("WhereCondition") = " and vcCompanyName ilike '%" & Replace(hdnSimpleSearchText.Value.Replace("or:", ""), "'", "''") & "%'"
                strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmAdvancedSearchRes.aspx") & "'"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                Exit Sub
            ElseIf CCommon.ToString(hdnSimpleSearchText.Value).ToLower().StartsWith("ve:") Then
                Session("WhereCondition") = " and numCompanyType = 47 and vcCompanyName ilike '%" & Replace(hdnSimpleSearchText.Value.Replace("ve:", ""), "'", "''") & "%'"
                strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmAdvancedSearchRes.aspx") & "'"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                Exit Sub
            ElseIf CCommon.ToString(hdnSimpleSearchText.Value).ToLower().StartsWith("cu:") Then
                Session("WhereCondition") = " and numCompanyType = 46 and vcCompanyName ilike'%" & Replace(hdnSimpleSearchText.Value.Replace("cu:", ""), "'", "''") & "%'"
                strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmAdvancedSearchRes.aspx") & "'"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                Exit Sub
            ElseIf CCommon.ToString(hdnSimpleSearchText.Value).ToLower().StartsWith("co:") Then
                vcSearchText = hdnSimpleSearchText.Value.Replace("co:", "")
                Session("WhereCondition") = " and (vcFirstName ilike '%" & Replace(vcSearchText, "'", "''") & "%' Or vcLastName ilike '%" & Replace(vcSearchText, "'", "''") & "%' Or vcCompanyName ilike '%" & Replace(vcSearchText, "'", "''") & "%' Or vcEmail ilike '%" & Replace(vcSearchText, "'", "''") & "%' Or numCell ilike '%" & Replace(vcSearchText, "'", "''") & "%' Or numHomePhone ilike '%" & Replace(vcSearchText, "'", "''") & "%' Or numPhone ilike '%" & Replace(vcSearchText, "'", "''") & "%')"
                strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmAdvancedSearchRes.aspx") & "'"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                Exit Sub
            ElseIf CCommon.ToString(hdnSimpleSearchText.Value).ToLower().StartsWith("so:") Then
                vcSearchText = hdnSimpleSearchText.Value.Replace("so:", "")
                Session("WhereContditionOpp") = " AND COALESCE(tintOppType,0)=1 AND COALESCE(tintOppStatus,0) = 1 AND vcpOppName ilike '%" & Replace(vcSearchText, "'", "''") & "%'"
                strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmItemSearchRes.aspx") & "'"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                Exit Sub
            ElseIf CCommon.ToString(hdnSimpleSearchText.Value).ToLower().StartsWith("po:") Then
                vcSearchText = hdnSimpleSearchText.Value.Replace("po:", "")
                Session("WhereContditionOpp") = " AND COALESCE(tintOppType,0)=2 AND COALESCE(tintOppStatus,0) = 1 AND vcpOppName ilike '%" & Replace(vcSearchText, "'", "''") & "%'"
                strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmItemSearchRes.aspx") & "'"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                Exit Sub
            ElseIf CCommon.ToString(hdnSimpleSearchText.Value).ToLower().StartsWith("op:") Then
                vcSearchText = hdnSimpleSearchText.Value.Replace("op:", "")
                Session("WhereContditionOpp") = " AND COALESCE(tintOppStatus,0)=0 AND vcpOppName ilike '%" & Replace(vcSearchText, "'", "''") & "%'"
                strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmItemSearchRes.aspx") & "'"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                Exit Sub
            ElseIf CCommon.ToString(hdnSimpleSearchText.Value).ToLower().StartsWith("ca:") Then
                vcSearchText = hdnSimpleSearchText.Value.Replace("ca:", "")
                Session("WhereContditionCase") = " AND (vcCaseNumber ilike '%" & Replace(vcSearchText, "'", "''") & "%' OR textSubject ilike '%" & Replace(vcSearchText, "'", "''") & "%')"
                strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmAdvCaseRes.aspx") & "'"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                Exit Sub
            ElseIf CCommon.ToString(hdnSimpleSearchText.Value).ToLower().StartsWith("pj:") Then
                vcSearchText = hdnSimpleSearchText.Value.Replace("pj:", "")
                Session("WhereContditionPro") = " AND (vcProjectID ilike '%" & Replace(vcSearchText, "'", "''") & "%' OR vcProjectName ilike '%" & Replace(vcSearchText, "'", "''") & "%')"
                strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmAdvProjectsRes.aspx") & "'"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                Exit Sub
            ElseIf CCommon.ToString(hdnSimpleSearchText.Value).ToLower().StartsWith("it:") Then
                vcSearchText = hdnSimpleSearchText.Value.Replace("it:", "")
                Session("WhereConditionItem") = " AND (vcItemName ilike '%" & Replace(vcSearchText, "'", "''") & "%' OR vcSKU ilike '%" & Replace(vcSearchText, "'", "''") & "%' OR numBarCodeId ilike '%" & Replace(vcSearchText, "'", "''") & "%' OR vcModelID ilike '%" & Replace(vcSearchText, "'", "''") & "%' OR vcManufacturer ilike '%" & Replace(vcSearchText, "'", "''") & "%')"
                strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmAdvSerInvItemsRes.aspx") & "'"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                Exit Sub
            ElseIf CCommon.ToBool(chkEmailSimpleSearch.Checked) Then
                Session("EmailSearchText") = hdnSimpleSearchText.Value
                Response.Redirect("~/Outlook/frmInboxItems.aspx", False)
                Exit Sub
            End If
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub
End Class