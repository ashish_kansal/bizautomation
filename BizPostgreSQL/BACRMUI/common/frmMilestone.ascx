﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmMilestone.ascx.vb"
    Inherits=".Milestone" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>

<script language="javascript">
    function OpenEvent(a, b) {
        if (a == 1) {
            window.open('../outlook/frmMailDtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=inbox&numEmailHstrID=' + b, '', 'width=800,height=600,status=no,scrollbar=yes,top=110,left=150');
        }
        else if (a == 2) {
            window.open('../admin/ActionItemDetailsOld.aspx?Popup=True&CommID=' + b ,'','width=800,height=600,status=no,scrollbar=yes,top=110,left=150');
        }
        else if (a == 3) {
    }
    return false;
    }
</script>

<asp:Repeater ID="rptMilestone" runat="server">
    <HeaderTemplate>
        <table width="100%">
    </HeaderTemplate>
    <AlternatingItemTemplate>
        <tr id="trHeader" runat="server" class="hs">
            <td align="left" colspan="7">
                Milestone -
                <asp:Label ID="lblStagePercentage" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "numStagePercentage")%>'></asp:Label>
                %
                <%#DataBinder.Eval(Container.DataItem, "vcStagePercentageDtl")%>
            </td>
            <td align="right">
                <asp:Button ID="btnAddStage" runat="server" CommandName="Add" Text="Add" CssClass="button" />
            </td>
        </tr>
        <tr class="tr1">
            <td style="white-space: nowrap;" class="normal1" align="right">
                Stage Name
            </td>
            <td colspan="3">
                <asp:TextBox CssClass="signup" ID="txtStageName" Width="500" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcstageDetail") %>'></asp:TextBox>
            </td>
            <td style="white-space: nowrap;" class="normal1" align="right">
                Last Modified By:
            </td>
            <td style="white-space: nowrap;" class="normal1">
                <asp:Label ID="lblStageModifiedBy" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "numModifiedByName") %>'></asp:Label>
            </td>
            <td style="white-space: nowrap;" class="normal1" align="right">
                Last Modified Date:
            </td>
            <td style="white-space: nowrap;" class="normal1">
                <asp:Label ID="lblStageModifiedDate" Text='<%# FormmattedDate(DataBinder.Eval(Container.DataItem, "bintModifiedDate")) %>'
                    runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="tr1">
            <td style="white-space: nowrap;" class="normal1" align="right">
                Stage Status
            </td>
            <td>
                <asp:DropDownList CssClass="signup" ID="ddlStageStatus" Width="200" runat="server">
                </asp:DropDownList>
            </td>
            <td style="white-space: nowrap;" class="normal1" align="right">
                This stage comprises
            </td>
            <td style="white-space: nowrap;" class="normal1">
                <asp:TextBox CssClass="signup" ID="txtStagePercentage" Width="20" runat="server"
                    Text='<%# DataBinder.Eval(Container.DataItem, "tintPercentage") %>'></asp:TextBox>
                % of completion
            </td>
            <td style="white-space: nowrap;" class="normal1" align="right">
                Due Date
            </td>
            <td>
                <BizCalendar:Calendar ID="calStageDueDate" SelectedDate='<%# DataBinder.Eval(Container.DataItem, "bintDueDate") %>'
                    runat="server" />
            </td>
            <td style="white-space: nowrap;" class="normal1" align="right">
                Stage Completed Date
            </td>
            <td style="white-space: nowrap;" class="normal1">
                <asp:Label ID="lblStageCompletedDate" runat="server" Text='<%# FormmattedDate(DataBinder.Eval(Container.DataItem, "bintStageComDate")) %>'></asp:Label>
            </td>
        </tr>
        <tr class="tr1">
            <td style="white-space: nowrap;" class="normal1" align="right">
                Assign To
            </td>
            <td>
                <asp:DropDownList CssClass="signup" ID="ddlAssignTo" Width="200" runat="server">
                </asp:DropDownList>
            </td>
            <td class="normal1">
                <asp:CheckBox ID="chkAlert" Text="Alert" runat="server" />
            </td>
            <td class="normal1">
                <asp:CheckBox ID="chkStageClosed" Checked='<%# DataBinder.Eval(Container.DataItem, "bitStageCompleted") %>'
                    Text="Stage Closed" runat="server" />
            </td>
            <td colspan="4">
                <asp:HyperLink ID="hplStageTime" CssClass="hyperlink" runat="server">Time:</asp:HyperLink>
                <asp:Label ID="lblStageTime" class="normal1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Time") %>'></asp:Label>
                <asp:HyperLink ID="hplStageExpense" CssClass="hyperlink" runat="server">Expense:</asp:HyperLink>
                <asp:Label ID="lblStageExpense" class="normal1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Expense") %>'></asp:Label>
                <asp:HyperLink ID="hplStageDependency" CssClass="hyperlink" runat="server">Dependency</asp:HyperLink>
                :
                <asp:Label ID="lblStageDependency" class="normal1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Depend") %>'></asp:Label>
                <asp:HyperLink ID="hplStageSubStages" CssClass="hyperlink" runat="server">Sub Stages</asp:HyperLink>
                :
                <asp:Label ID="lblSubStages" class="normal1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SubStg") %>'></asp:Label>
            </td>
        </tr>
        <tr class="tr1">
            <td class="normal1" align="right" rowspan="2">
                Comments
            </td>
            <td colspan="3" rowspan="2">
                <asp:TextBox ID="txtStageComment" Text='<%# DataBinder.Eval(Container.DataItem, "vcComments") %>'
                    CssClass="signup" Width="500" Height="30" TextMode="MultiLine" runat="server"></asp:TextBox>
            </td>
            <td align="right">
                <asp:HyperLink ID="hplEvent" CssClass="hyperlink" runat="server">Event</asp:HyperLink> : 
            </td>
            <td colspan="3">
                <asp:Label CssClass="signup" ID="lblStageEvent" runat="server">No Event</asp:Label>
            </td>
        </tr>
        <tr class="tr1">
            <td colspan="4" align="right">
                <asp:HiddenField ID="hdOppStageID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "StageID") %>' />
                <asp:HiddenField ID="hdStageDetail" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "vcstageDetail") %>' />
                <asp:HiddenField ID="hdModifiedBy" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numModifiedBy") %>' />
                <asp:HiddenField ID="hdDueDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bintDueDate") %>' />
                <asp:HiddenField ID="hdStageComDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bintStageComDate") %>' />
                <asp:HiddenField ID="hdStageCompleted" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bitStageCompleted") %>' />
                <asp:HiddenField ID="hdTCategoryHDRID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numTCategoryHDRID") %>' />
                <asp:HiddenField ID="hdECategoryHDRID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numECategoryHDRID") %>' />
                <asp:HiddenField ID="hdDepend" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Depend") %>' />
                <asp:HiddenField ID="hdExpense" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Expense") %>' />
                <asp:HiddenField ID="hdTime" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Time") %>' />
                <asp:HiddenField ID="hdStage" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numStage") %>' />
                <asp:HiddenField ID="hdTemplateId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numTemplateId") %>' />
                <asp:HiddenField ID="hdPercentage" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "tintPercentage") %>' />
                <asp:HiddenField ID="hdEvent" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numEvent") %>' />
                <asp:HiddenField ID="hdReminder" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numReminder") %>' />
                <asp:HiddenField ID="hdComments" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "vcComments") %>' />
                <asp:HiddenField ID="hdStagePercentageDtl" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "vcStagePercentageDtl") %>' />
                <asp:HiddenField ID="hdET" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numET") %>' />
                <asp:HiddenField ID="hdActivity" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numActivity") %>' />
                <asp:HiddenField ID="hdStartDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numStartDate") %>' />
                <asp:HiddenField ID="hdStartTime" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numStartTime") %>' />
                <asp:HiddenField ID="hdStartTimePeriod" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numStartTimePeriod") %>' />
                <asp:HiddenField ID="hdEndTime" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numEndTime") %>' />
                <asp:HiddenField ID="hdEndTimePeriod" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numEndTimePeriod") %>' />
                <asp:HiddenField ID="hdCom" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "txtCom") %>' />
                <asp:HiddenField ID="hdChgStatus" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numChgStatus") %>' />
                <asp:HiddenField ID="hdOnClose" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bitClose") %>' />
                <asp:HiddenField ID="hdType" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numType") %>' />
                <asp:HiddenField ID="hdCommActId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numCommActId") %>' />
                <asp:HiddenField ID="hdAssignTo" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numAssignTo") %>' />
                <asp:HiddenField ID="hdOp_Flag" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Op_Flag") %>' />
                <asp:HiddenField ID="hdModifiedDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bintModifiedDate") %>' />
                <asp:HiddenField ID="hdCreatedDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bintCreatedDate") %>' />
                <asp:Button ID="btnStageDelete" CommandName="Delete" runat="server" CssClass="button Delete" Text="X" />
            </td>
        </tr>
    </AlternatingItemTemplate>
    <ItemTemplate>
        <tr id="trHeader" runat="server" class="hs">
            <td align="left" colspan="7">
                Milestone -
                <asp:Label ID="lblStagePercentage" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "numStagePercentage")%>'></asp:Label>
                %
                <%#DataBinder.Eval(Container.DataItem, "vcStagePercentageDtl")%>
            </td>
            <td align="right">
                <asp:Button ID="btnAddStage" runat="server" CommandName="Add" Text="Add" CssClass="button" />
            </td>
        </tr>
        <tr>
            <td style="white-space: nowrap;" class="normal1" align="right">
                Stage Name
            </td>
            <td colspan="3">
                <asp:TextBox CssClass="signup" ID="txtStageName" Width="500" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcstageDetail") %>'></asp:TextBox>
            </td>
            <td style="white-space: nowrap;" class="normal1" align="right">
                Last Modified By:
            </td>
            <td style="white-space: nowrap;" class="normal1">
                <asp:Label ID="lblStageModifiedBy" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "numModifiedByName") %>'></asp:Label>
            </td>
            <td style="white-space: nowrap;" class="normal1" align="right">
                Last Modified Date:
            </td>
            <td style="white-space: nowrap;" class="normal1">
                <asp:Label ID="lblStageModifiedDate" Text='<%# FormmattedDate(DataBinder.Eval(Container.DataItem, "bintModifiedDate")) %>'
                    runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="white-space: nowrap;" class="normal1" align="right">
                Stage Status
            </td>
            <td>
                <asp:DropDownList CssClass="signup" ID="ddlStageStatus" Width="200" runat="server">
                </asp:DropDownList>
            </td>
            <td style="white-space: nowrap;" class="normal1" align="right">
                This stage comprises
            </td>
            <td style="white-space: nowrap;" class="normal1">
                <asp:TextBox CssClass="signup" ID="txtStagePercentage" Width="20" runat="server"
                    Text='<%# DataBinder.Eval(Container.DataItem, "tintPercentage") %>'></asp:TextBox>
                % of completion
            </td>
            <td style="white-space: nowrap;" class="normal1" align="right">
                Due Date
            </td>
            <td>
                <BizCalendar:Calendar ID="calStageDueDate" SelectedDate='<%# DataBinder.Eval(Container.DataItem, "bintDueDate") %>'
                    runat="server" />
            </td>
            <td style="white-space: nowrap;" class="normal1" align="right">
                Stage Completed Date
            </td>
            <td style="white-space: nowrap;" class="normal1">
                <asp:Label ID="lblStageCompletedDate" runat="server" Text='<%# FormmattedDate(DataBinder.Eval(Container.DataItem, "bintStageComDate")) %>'></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="white-space: nowrap;" class="normal1" align="right">
                Assign To
            </td>
            <td>
                <asp:DropDownList CssClass="signup" ID="ddlAssignTo" Width="200" runat="server">
                </asp:DropDownList>
            </td>
            <td class="normal1">
                <asp:CheckBox ID="chkAlert" Text="Alert" runat="server" />
            </td>
            <td class="normal1">
                <asp:CheckBox ID="chkStageClosed" Checked='<%# DataBinder.Eval(Container.DataItem, "bitStageCompleted") %>'
                    Text="Stage Closed" runat="server" />
            </td>
            <td colspan="4">
                <asp:HyperLink ID="hplStageTime" CssClass="hyperlink" runat="server">Time:</asp:HyperLink>
                <asp:Label ID="lblStageTime" class="normal1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Time") %>'></asp:Label>
                <asp:HyperLink ID="hplStageExpense" CssClass="hyperlink" runat="server">Expense:</asp:HyperLink>
                <asp:Label ID="lblStageExpense" class="normal1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Expense") %>'></asp:Label>
                <asp:HyperLink ID="hplStageDependency" CssClass="hyperlink" runat="server">Dependency</asp:HyperLink>
                :
                <asp:Label ID="lblStageDependency" class="normal1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Depend") %>'></asp:Label>
                <asp:HyperLink ID="hplStageSubStages" CssClass="hyperlink" runat="server">Sub Stages</asp:HyperLink>
                :
                <asp:Label ID="lblSubStages" class="normal1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SubStg") %>'></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right" rowspan="2">
                Comments
            </td>
            <td colspan="3" rowspan="2">
                <asp:TextBox ID="txtStageComment" Text='<%# DataBinder.Eval(Container.DataItem, "vcComments") %>'
                    CssClass="signup" Width="500" Height="30" TextMode="MultiLine" runat="server"></asp:TextBox>
            </td>
            <td align="right">
                <asp:HyperLink ID="hplEvent" CssClass="hyperlink" runat="server">Event</asp:HyperLink> : 
            </td>
            <td colspan="3">
                <asp:Label  CssClass="signup" ID="lblStageEvent" runat="server">No Event</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="right">
                <asp:HiddenField ID="hdOppStageID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "StageID") %>' />
                <asp:HiddenField ID="hdStageDetail" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "vcstageDetail") %>' />
                <asp:HiddenField ID="hdModifiedBy" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numModifiedBy") %>' />
                <asp:HiddenField ID="hdDueDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bintDueDate") %>' />
                <asp:HiddenField ID="hdStageComDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bintStageComDate") %>' />
                <asp:HiddenField ID="hdStageCompleted" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bitStageCompleted") %>' />
                <asp:HiddenField ID="hdTCategoryHDRID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numTCategoryHDRID") %>' />
                <asp:HiddenField ID="hdECategoryHDRID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numECategoryHDRID") %>' />
                <asp:HiddenField ID="hdDepend" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Depend") %>' />
                <asp:HiddenField ID="hdExpense" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Expense") %>' />
                <asp:HiddenField ID="hdTime" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Time") %>' />
                <asp:HiddenField ID="hdStage" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numStage") %>' />
                <asp:HiddenField ID="hdTemplateId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numTemplateId") %>' />
                <asp:HiddenField ID="hdPercentage" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "tintPercentage") %>' />
                <asp:HiddenField ID="hdEvent" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numEvent") %>' />
                <asp:HiddenField ID="hdReminder" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numReminder") %>' />
                <asp:HiddenField ID="hdComments" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "vcComments") %>' />
                <asp:HiddenField ID="hdStagePercentageDtl" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "vcStagePercentageDtl") %>' />
                <asp:HiddenField ID="hdET" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numET") %>' />
                <asp:HiddenField ID="hdActivity" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numActivity") %>' />
                <asp:HiddenField ID="hdStartDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numStartDate") %>' />
                <asp:HiddenField ID="hdStartTime" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numStartTime") %>' />
                <asp:HiddenField ID="hdStartTimePeriod" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numStartTimePeriod") %>' />
                <asp:HiddenField ID="hdEndTime" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numEndTime") %>' />
                <asp:HiddenField ID="hdEndTimePeriod" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numEndTimePeriod") %>' />
                <asp:HiddenField ID="hdCom" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "txtCom") %>' />
                <asp:HiddenField ID="hdChgStatus" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numChgStatus") %>' />
                <asp:HiddenField ID="hdOnClose" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bitClose") %>' />
                <asp:HiddenField ID="hdType" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numType") %>' />
                <asp:HiddenField ID="hdCommActId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numCommActId") %>' />
                <asp:HiddenField ID="hdAssignTo" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numAssignTo") %>' />
                <asp:HiddenField ID="hdOp_Flag" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Op_Flag") %>' />
                <asp:HiddenField ID="hdModifiedDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bintModifiedDate") %>' />
                <asp:HiddenField ID="hdCreatedDate" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bintCreatedDate") %>' />
                <asp:Button ID="btnStageDelete" CommandName="Delete" runat="server" CssClass="button Delete" Text="X" />
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        <tr>
            <td class="normal1" colspan="8">
                <asp:HiddenField ID="hdWonOppStageID" runat="server" />
                <asp:HiddenField ID="hdDealWon" runat="server" />
                <asp:HiddenField ID="hdWonOp_Flag" runat="server" />
                <asp:CheckBox ID="chkDealWon" runat="server" Text="Deal Won" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comment
                <asp:TextBox Width="600" ID="txtDealWonComment" runat="server"></asp:TextBox>
            </td>
        </tr>
        <asp:Panel ID="pnlDealLost" runat="server"  >
        <tr>
            <td class="normal1" colspan="8">
                <asp:HiddenField ID="hdLostOppStageID" runat="server" />
                <asp:HiddenField ID="hdDealLost" runat="server" />
                <asp:HiddenField ID="hdLostOp_Flag" runat="server" />
                <asp:CheckBox ID="chkDealLost" runat="server" Text="Deal Lost" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comment
                <asp:TextBox Width="600" ID="txtDealLostComment" runat="server"></asp:TextBox>
            </td>
        </tr>
        </asp:Panel>
        </table>
    </FooterTemplate>
</asp:Repeater>
