﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmBizSorting.ascx.vb"
    Inherits=".frmBizSorting" %>
<%@ OutputCache Duration="88400" VaryByParam="none" %>
<script type="text/javascript">
    function pageLoad() {
        $('#listSort li a').on("click", function (event) {
            fnSortByChar(this.id);
            setActive();
        });

        setActive();
    }

    function fnSortByChar(varSortChar) {
        var SortChar = $('#txtSortChar');
        SortChar.val(varSortChar);

        if (typeof ($('#txtCurrrentPage')) != 'undefined') {
            $('#txtCurrrentPage').val(1);
        }
        $('#btnGo1').click();
    }

    function setActive() {
        //Reset active Element
        $('#listSort li a').each(function () {
            $(this).parent().removeClass("active");
        });

        if ($('#txtSortChar').val() == "")
            $('#txtSortChar').val("0")

        //Apply Active class to clicked td
        $('#' + $('#txtSortChar').val().toLowerCase()).parent().attr("class", "paginate_button active");
    }

</script>
<style type="text/css">
    #listSort {
        margin:0px;
    }

    #listSort li {
        cursor:pointer;
    }
    #listSort > li > a {
        margin-right: 5px;
        border-radius: 4px;
    }
</style>

<ul id="listSort" class="pagination">
    <li class="paginate_button"><a id="a">A</a></li>
    <li class="paginate_button"><a id="b">B</a></li>
    <li class="paginate_button"><a id="c">C</a></li>
    <li class="paginate_button"><a id="d">D</a></li>
    <li class="paginate_button"><a id="e">E</a></li>
    <li class="paginate_button"><a id="f">F</a></li>
    <li class="paginate_button"><a id="g">G</a></li>
    <li class="paginate_button"><a id="h">H</a></li>
    <li class="paginate_button"><a id="i">I</a></li>
    <li class="paginate_button"><a id="j">J</a></li>
    <li class="paginate_button"><a id="k">K</a></li>
    <li class="paginate_button"><a id="l">L</a></li>
    <li class="paginate_button"><a id="m">M</a></li>
    <li class="paginate_button"><a id="n">N</a></li>
    <li class="paginate_button"><a id="o">O</a></li>
    <li class="paginate_button"><a id="p">P</a></li>
    <li class="paginate_button"><a id="q">Q</a></li>
    <li class="paginate_button"><a id="r">R</a></li>
    <li class="paginate_button"><a id="s">S</a></li>
    <li class="paginate_button"><a id="t">T</a></li>
    <li class="paginate_button"><a id="u">U</a></li>
    <li class="paginate_button"><a id="v">V</a></li>
    <li class="paginate_button"><a id="w">W</a></li>
    <li class="paginate_button"><a id="x">X</a></li>
    <li class="paginate_button"><a id="y">Y</a></li>
    <li class="paginate_button"><a id="z">Z</a></li>
    <li class="paginate_button"><a id="0">ALL</a></li>
</ul>
