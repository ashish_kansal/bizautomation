﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Common

Public Class ECommerceMenuMaster
    Inherits System.Web.UI.MasterPage
    Private _HideWebMenuUserControl As Boolean
    Private _SelectedTabValue As String

    Private isErrorThrownFromChild As Boolean

    Public Property SelectedTabValue() As String
        Get
            Return _SelectedTabValue
        End Get
        Set(ByVal value As String)
            _SelectedTabValue = value
        End Set
    End Property

    Public Property HideWebMenuUserControl() As Boolean
        Get
            Return _HideWebMenuUserControl
        End Get
        Set(ByVal value As Boolean)
            _HideWebMenuUserControl = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If isErrorThrownFromChild Then
                isErrorThrownFromChild = False
            Else
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            End If

            If Not IsPostBack Then
                BindMenu()
            End If

            If Request.Url.PathAndQuery.Contains("frmCssList.aspx") Or
               Request.Url.PathAndQuery.Contains("frmCss.aspx") Or
               Request.Url.PathAndQuery.Contains("frmTemplate.aspx") Or
               Request.Url.PathAndQuery.Contains("frmSitePage.aspx") Or
               Request.Url.PathAndQuery.Contains("frmMenu.aspx") Or
               Request.Url.PathAndQuery.Contains("frmBreadCrumb.aspx") Or
               Request.Url.PathAndQuery.Contains("frmSite.aspx") Then
                If Not radTabStripEcommerce.FindTabByValue(_SelectedTabValue) Is Nothing Then
                    radTabStripEcommerce.FindTabByValue(_SelectedTabValue).Selected = True
                    radTabStripEcommerce.FindTabByValue(_SelectedTabValue).SelectParents()
                End If
            Else
                Dim currentItem As RadTab = radTabStripEcommerce.FindTabByUrl(Request.Url.PathAndQuery)

                If currentItem IsNot Nothing Then
                    currentItem.Selected = True
                    If Not currentItem.Parent Is Nothing Then
                        currentItem.SelectParents()
                    End If
                Else
                    If Request.Url.PathAndQuery.Contains("frmCategories.aspx") Or
                        Request.Url.PathAndQuery.Contains("frmCategoryAdd.aspx") Or
                        Request.Url.PathAndQuery.Contains("frmAssignCategories.aspx") Then
                        If Not radTabStripEcommerce.FindTabByValue("103") Is Nothing Then
                            radTabStripEcommerce.FindTabByValue("103").Selected = True
                        End If
                    ElseIf Request.Url.PathAndQuery.Contains("frmShippingRule.aspx") Then
                        If Not radTabStripEcommerce.FindTabByValue("193") Is Nothing Then
                            radTabStripEcommerce.FindTabByValue("193").Selected = True
                            radTabStripEcommerce.FindTabByValue("193").SelectParents()
                        End If
                    ElseIf Request.Url.PathAndQuery.Contains("frmPromotionOfferManage.aspx") Or
                        Request.Url.PathAndQuery.Contains("frmPromotionOfferList.aspx") Then
                        If Not radTabStripEcommerce.FindTabByValue("187") Is Nothing Then
                            radTabStripEcommerce.FindTabByValue("187").Selected = True
                        End If
                    Else
                        If radTabStripEcommerce.Tabs.Count > 0 Then
                            radTabStripEcommerce.SelectedIndex = 0
                        End If
                    End If

                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Public Sub BindMenu()
        Try
            Dim objCommon As New CCommon
            objCommon.ModuleID = 13
            objCommon.DomainID = Session("DomainID")
            objCommon.GroupID = Session("UserGroupID")
            objCommon.lngParentID = 73
            objCommon.iMode = 0
            Dim dt As New DataTable()
            dt = objCommon.GetEcommerceNavigationDTLs()

            radTabStripEcommerce.DataValueField = "numPageNavID"
            radTabStripEcommerce.DataTextField = "vcPageNavName"
            radTabStripEcommerce.DataNavigateUrlField = "vcNavURL"
            radTabStripEcommerce.DataFieldID = "numPageNavID"
            radTabStripEcommerce.DataFieldParentID = "numParentID"

            radTabStripEcommerce.DataSource = dt
            radTabStripEcommerce.DataBind()

        Catch ex As Exception
            Throw
        End Try

    End Sub

    Private Sub radTabStripEcommerce_TabClick(sender As Object, e As RadTabStripEventArgs) Handles radTabStripEcommerce.TabClick
        Try
            If e.Tab.Tabs.Count > 0 Then
                Response.Redirect(e.Tab.Tabs(0).NavigateUrl)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ThrowError(ByVal ex As Exception)
        isErrorThrownFromChild = True
        DisplayError(CCommon.ToString(ex))
    End Sub

End Class