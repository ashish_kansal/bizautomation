﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmReportBug.aspx.vb"
    Inherits=".frmReportBug" MasterPageFile="~/common/GridMasterRegular.Master" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript" language="javascript">
        function validate() {
            var Upload_file = document.getElementById('<%= fupload.ClientID %>');
            var myfile = Upload_file.value;
            if (myfile.length > 0 && myfile.length <= 524288) {
                if (myfile.indexOf("jpg") > 0 || myfile.indexOf("jpeg") > 0 || myfile.indexOf("png") > 0) {
                    return true
                }
                else {
                    alert('Only jpeg,png files allowed. ');
                    return false;
                }
            }
            return true;
        } //End of function validate.
  
        

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
            </td>
            <td align="right">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button" OnClientClick="javascript:return validate();">
                </asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label Text="Report a bug" runat="server" ID="lblTitle" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
&nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table width="100%" border="0">
        <tr id="trFeedback" runat="server" visible="false">
            <td>
            </td>
            <td style="font-family: 'Segoe UI' Arial Sans-Serif; font-size: 13px;" align="left">
                <span style="width: 500px; display: block;">Send us your ideas, requests or comments
                    about BizAutomation. Although we can't directly respond to your feedback, we'll
                    use it to improve our product and services.</span>
            </td>
        </tr>
        <tr id="trSummary" runat="server">
            <td class="text" align="right">
                Summary<font color="#ff0000">*</font>
            </td>
            <td>
                <asp:TextBox ID="txtDesc" runat="server" CssClass="signup" Width="500" Text="Enter one-line summary"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">
                <asp:Label Text="Description" runat="server" ID="lblDescription" /><font color="#ff0000">*</font>
            </td>
            <td>
                <asp:TextBox ID="txtComments" runat="server" CssClass="signup" Height="300" Width="500"
                    TextMode="MultiLine" Text="What steps will reproduce the problem?
1.
2.
3.

What is the expected output? What do you see instead?


What version of the browser are you using? On what operating system?
##

Please provide any additional information below.
                                "></asp:TextBox>
            </td>
            <td id="tdAlternateMethod" runat="server" style="font-family: 'Segoe UI' Arial Sans-Serif;
                font-size: 13px;" align="center">
                <b>Note:</b> Help us to make Bizautomation bug free! Bugs submitted will be directly
                reviewed by our support engineers,<br />
                and support ticket will be generated. You will be notified via email when bug is
                fixed.<br />
                or
                <br />
                You can also send email with bug description to <b>
                    <asp:Label ID="lblEmail" runat="server"></asp:Label></b>
            </td>
        </tr>
        <tr id="trAttachment" runat="server">
            <td class="text" align="right">
                Attachment
            </td>
            <td>
                <asp:FileUpload ID="fupload" runat="server" CssClass="signup" />
            </td>
        </tr>
        <tr>
        </tr>
    </table>
</asp:Content>
