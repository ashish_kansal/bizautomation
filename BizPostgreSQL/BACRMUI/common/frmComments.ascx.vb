﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Case

Public Class frmComments
    Inherits BACRMUserControl
    

    Private _CaseID As Long
    Public Property CaseID() As Long
        Get
            Return _CaseID
        End Get
        Set(ByVal value As Long)
            _CaseID = value
        End Set
    End Property
    Private _StageID As Long
    Public Property StageID() As Long
        Get
            Return _StageID
        End Get
        Set(ByVal value As Long)
            _StageID = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If StageID <> 0 Or CaseID <> 0 Then
                If Session("show") = 1 Then
                    tblComments.Visible = True
                    ShowComments()
                End If
                btnSubmit.Attributes.Add("onclick", "return SaveComment()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try

    End Sub
    Private Sub lnkbtnNoofCom_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkbtnNoofCom.Command
        Try
            If e.CommandName = "Show" Then
                tblComments.Visible = True
                pnlPostComment.Visible = False
                ShowComments()
                Session("show") = 1
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lnlbtnPost_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles lnlbtnPost.Command
        Try
            If e.CommandName = "Post" Then
                tblComments.Visible = False
                pnlPostComment.Visible = True
                txtComments.Text = ""
                txtHeading.Text = ""
                txtLstUptCom.Text = ""
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub ShowComments()
        Try
            tblComments.Rows.Clear()
            Dim dtCaseComments As DataTable
            objCommon.CaseID = _CaseID
            objCommon.StageID = _StageID
            objCommon.byteMode = 0
            objCommon.DomainID = Session("DomainId")
            dtCaseComments = objCommon.ManageComments
            hdnCommentCount.Value = dtCaseComments.Rows.Count
            lnkbtnNoofCom.Text = "Comments (" & hdnCommentCount.Value.ToString & ")"
            Dim i As Integer
            If dtCaseComments.Rows.Count > 0 Then
                Dim tblRow As TableRow
                Dim tblCell As TableCell
                Dim lbl As Label
                Dim btnDelete As LinkButton
                Dim btnUpdate As LinkButton
                Dim k As Integer = 0
                For i = 0 To dtCaseComments.Rows.Count - 1
                    tblRow = New TableRow
                    tblCell = New TableCell

                    Dim label As New Label
                    label.Font.Bold = True
                    label.Text = dtCaseComments.Rows(i).Item("vcHeading")
                    tblCell.Controls.Add(label)

                    Dim div As New HtmlGenericControl("div")
                    div.Attributes.Add("class", "pull-right")

                    If dtCaseComments.Rows(i).Item("numCommentID") = IIf(txtLstUptCom.Text = "", 0, txtLstUptCom.Text) Then
                        btnUpdate = New LinkButton
                        btnUpdate.Text = "<i class=""fa fa-pencil""></i>"
                        AddHandler btnUpdate.Click, AddressOf Me.btnUpdate_Click
                        btnUpdate.ID = "btnUpdate~" & dtCaseComments.Rows(i).Item("numCommentID")
                        btnUpdate.CssClass = "btn btn-xs btn-info"
                        div.Controls.Add(btnUpdate)
                    End If

                    btnDelete = New LinkButton
                    btnDelete.Text = "<i class=""fa fa-trash""></i>"
                    AddHandler btnDelete.Click, AddressOf Me.btnDeleteComm_Click
                    btnDelete.ID = dtCaseComments.Rows(i).Item("numCommentID") * 100
                    btnDelete.CssClass = "btn btn-xs btn-danger"
                    div.Controls.Add(btnDelete)

                  
                    tblCell.Controls.Add(div)
                    tblRow.Cells.Add(tblCell)
                    tblComments.Rows.Add(tblRow)

                    tblRow = New TableRow
                    tblCell = New TableCell
                    tblCell.Text = dtCaseComments.Rows(i).Item("txtComments")
                    tblRow.Cells.Add(tblCell)
                    tblComments.Rows.Add(tblRow)
                    If k <> 0 Then
                        k = 0
                    Else : k = 1
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub btnDeleteComm_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            objCommon.CaseID = _CaseID
            objCommon.StageID = _StageID
            objCommon.byteMode = 2
            objCommon.DomainID = Session("DomainId")
            objCommon.CommentID = (sender.id) / 100
            objCommon.ManageComments()
            ShowComments()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            pnlPostComment.Visible = False
            tblComments.Visible = True
            objCommon.CaseID = _CaseID
            objCommon.StageID = _StageID
            objCommon.Comments = txtComments.Text
            objCommon.Heading = txtHeading.Text
            objCommon.ContactID = Session("UserContactID")
            objCommon.CommentID = 0
            objCommon.byteMode = 1
            objCommon.DomainID = Session("DomainId")
            If txtLstUptCom.Text <> "" Then objCommon.CommentID = txtLstUptCom.Text
            txtLstUptCom.Text = objCommon.ManageComments().Rows(0).Item(0)
            ShowComments()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            pnlPostComment.Visible = True
            tblComments.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
End Class