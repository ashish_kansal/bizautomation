﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart

Public Class frmHelp
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            hdnContentKey.Value = GetQueryStringVal("contentKey")

            If Not Page.IsPostBack Then
                PersistTable.Load(strParam:="frmHelp.aspx", isMasterPage:=True)
                If PersistTable.Count > 0 Then
                    hdnSelectedTab.Value = CCommon.ToBool(PersistTable("IsSpecificContent"))
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(CCommon.ToString(ex))
        End Try

    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        Try
            Dim objCM As New ContentManagement

            objCM.bitIsSpecificContent = hdnSpecificContent.Value
            objCM.numDomainID = Session("DomainId")
            objCM.vcContentKey = hdnContentKey.Value
            objCM.vcContent = hdnContentPublish.Value
            objCM.bitIsActive = True
            Dim dtCM As String = objCM.ManageBizHelpContents
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class