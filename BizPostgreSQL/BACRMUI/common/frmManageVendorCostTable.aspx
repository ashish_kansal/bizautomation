﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmManageVendorCostTable.aspx.vb" Inherits=".frmManageVendorCostTable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($("[id$=hdnIsUpdateDefaultVendorCostRange]").val() == "1") {
                LoadDefaultVendorCostRange();
            } else {
                LoadVendorCostTable();
            }

            $("[id$=ddlCurrency]").change(function () {
                LoadVendorCostTable();
            });
        });

        function LoadDefaultVendorCostRange() {
            try {
                $("[id$=divLoader]").show();
                $("#tblVendorCost thead").html("");
                $("#tblVendorCost tbody").html("");
                var i = 1;

                $("#tblVendorCost thead").append('<tr><th style="width:180px;">Qty From(Purchase UOM)</th><th style="width:180px;">To (Purchase UOM)</th></tr>');

                if ($("[id$=hdnDefaultVendorCostRange]").val() != "") {
                    var objVendorCostTable = $.parseJSON($("[id$=hdnDefaultVendorCostRange]").val());

                    if (objVendorCostTable != null && objVendorCostTable.length > 0) {
                        objVendorCostTable.forEach(function (obj) {
                            var html = "<tr>";
                            html += '<td><input type="number" class="txtFromQty form-control" value="' + replaceNull(obj.intfromqty) + '" /></td>';
                            html += '<td><input type="number" class="txtToQty form-control" value="' + replaceNull(obj.inttoqty) + '" /></td>';
                            html += "</tr>";
                            $("#tblVendorCost tbody").append(html);
                            i += 1;
                        });
                    }
                }

                while (i <= 20) {
                    var html = "<tr>";
                    html += '<td><input type="number" class="txtFromQty form-control"/></td>';
                    html += '<td><input type="number" class="txtToQty form-control"/></td>';
                    html += "</tr>";
                    $("#tblVendorCost tbody").append(html);

                    i += 1;
                }
            } catch (e) {
                $("[id$=divLoader]").hide();
                alert("Unknown error occurred while loading default cost range");
            }

            $("[id$=divLoader]").hide();
        }

        function LoadVendorCostTable() {
            try {
                $("[id$=divLoader]").show();
                $("#tblVendorCost thead").html("");
                $("#tblVendorCost tbody").html("");
                $("#tblVendorCost thead").append('<tr><th style="width:180px;">Qty From(Purchase UOM)</th><th style="width:180px;">To (Purchase UOM)</th><th style="width:200px;">Cost per Base UOM (Static)</th><th>Last Updated</th><th style="width:200px;">Cost per Base UOM (Dynamic)</th><th>Last Updated</th><th style="width:90px"></th></tr>');

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/VendorCostTable/GetByItemVendor',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "vendorTcode": parseInt($("[id$=hdnVendorTCode]").val())
                        , "vendorID": parseInt($("[id$=hdnDivisionID]").val())
                        , "itemCode": parseInt($("[id$=hdnItemID]").val())
                        , "currencyID": parseInt($("[id$=ddlCurrency]").val())
                    }),
                    success: function (data) {
                        try {
                            var objVendorCostTable = null;

                            if (data.VendorCostTableGetByItemVendorResult != null) {
                                objVendorCostTable = $.parseJSON(data.VendorCostTableGetByItemVendorResult);
                            }

                            var i = 1;

                            if (objVendorCostTable != null && objVendorCostTable.length > 0) {
                                objVendorCostTable.forEach(function (obj) {
                                    var html = "<tr>";
                                    html += '<td><input type="number" class="txtFromQty form-control" value="' + replaceNull(obj.intfromqty) + '" /></td>';
                                    html += '<td><input type="number" class="txtToQty form-control" value="' + replaceNull(obj.inttoqty) + '" /></td>';
                                    html += '<td><input type="number" class="txtStaticCost form-control" value="' + replaceNull(obj.monstaticcost) + '" /></td>';
                                    html += '<td><label class="lblLastUpdatedStatic">' + replaceNull(obj.vcstaticcostmodifiedby) + '</label></td>';
                                    html += '<td><input type="number" class="txtDynamicCost form-control" value="' + replaceNull(obj.mondynamiccost) + '" /></td>';
                                    html += '<td><label class="lblLastUpdatedDynamic">' + replaceNull(obj.vcdynamiccostmodifiedby) + '</label></td>';
                                    if (parseInt(obj.numvendorcosttableid || 0) > 0) {
                                        html += '<td><input type="button" class="btn btn-primary" value="Update" onclick="return UpdateCost(this,' + parseInt(obj.numvendorcosttableid || 0) + ');" /></td>';
                                    } else {
                                        html += '<td></td>';
                                    }
                                    html += "</tr>";
                                    $("#tblVendorCost tbody").append(html);
                                    i += 1;
                                });
                            }

                            while (i <= 20) {
                                var html = "<tr>";
                                html += '<td><input type="number" class="txtFromQty form-control" /></td>';
                                html += '<td><input type="number" class="txtToQty form-control" /></td>';
                                html += '<td><input type="number" class="txtStaticCost form-control" /></td>';
                                html += '<td><label class="lblLastUpdatedStatic"></label></td>';
                                html += '<td><input type="number" class="txtDynamicCost form-control" /></td>';
                                html += '<td><label class="lblLastUpdatedDynamic"></label></td>';
                                html += "<td></td>";
                                html += "</tr>";
                                $("#tblVendorCost tbody").append(html);

                                i += 1;
                            }

                            $("[id$=divLoader]").hide();
                        } catch (err) {
                            $("[id$=divLoader]").hide();
                            alert("Unknown error occurred while populating cost table.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("[id$=divLoader]").hide();

                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            alert("Error occurred while loading cost table: " + replaceNull(objError.ErrorMessage));
                        } else {
                            alert("Unknown error ocurred while cost table");
                        }
                    }
                });
            } catch (e) {
                $("[id$=divLoader]").hide();
                alert("Unknown error occurred while loading cost table");
            }
        }

        function Save() {
            if ($("[id$=hdnIsUpdateDefaultVendorCostRange]").val() == "1") {
                SaveDefaultVendorCostRange();
            } else {
                SaveVendorCostTable();
            }

            return false;
        }

        function SaveDefaultVendorCostRange() {
            try {
                
                var isValid = true;
                var vendorCostTable = [];
                var i = 1;

                $("#tblVendorCost tbody > tr").each(function () {
                    if ($(this).find("input.txtFromQty").val() != "" && $(this).find("input.txtToQty").val() != "") {
                        var obj = new Object();
                        obj.intRow = i;
                        obj.intFromQty = parseInt($(this).find("input.txtFromQty").val());
                        obj.intToQty = parseInt($(this).find("input.txtToQty").val());

                        if (obj.intFromQty >= 0 && obj.intToQty <= obj.intFromQty) {
                            alert("Provide valid from and to range");
                            $(this).find("input.txtToQty").focus();
                            isValid = false;
                            return false;
                        }

                        vendorCostTable.push(obj);
                        i += 1;
                    } else {
                        return false;
                    }
                });

                if (isValid) {
                    $("[id$=divLoader]").show();

                    if (vendorCostTable.length > 0) {
                        $.ajax({
                            type: "POST",
                            url: '../WebServices/CommonService.svc/VendorCostTable/UpdateDefaultRange',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "costRange": JSON.stringify(vendorCostTable)
                            }),
                            success: function (data) {
                                Close();
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $("[id$=divLoader]").hide();

                                if (IsJsonString(jqXHR.responseText)) {
                                    var objError = $.parseJSON(jqXHR.responseText)
                                    alert("Error occurred while saving cost table: " + replaceNull(objError.ErrorMessage));
                                } else {
                                    alert("Unknown error occurred while saving default cost range");
                                }
                            }
                        });
                    }
                }
            } catch (e) {
                $("[id$=divLoader]").hide();
                alert("Unknown error occurred while saving default cost range");
            }
        }

        function SaveVendorCostTable() {
            try {
                
                var isValid = true;
                var vendorCostTable = [];
                var i = 1;
                $("#tblVendorCost tbody > tr").each(function () {
                    if ($(this).find("input.txtFromQty").val() != "" && $(this).find("input.txtToQty").val() != "") {
                        if ($(this).find("input.txtStaticCost").val() == "" || $(this).find("input.txtDynamicCost").val() == "") {
                            alert("Provide static and dynamic cost");
                            $(this).find("input.txtStaticCost").focus();
                            isValid = false;
                            return false;
                        } else {
                            var obj = new Object();
                            obj.intRow = i;
                            obj.intFromQty = parseInt($(this).find("input.txtFromQty").val());
                            obj.intToQty = parseInt($(this).find("input.txtToQty").val());
                            obj.monStaticCost = parseFloat($(this).find("input.txtStaticCost").val());
                            obj.monDynamicCost = parseFloat($(this).find("input.txtDynamicCost").val());

                            if (obj.intFromQty >= 0 && obj.intToQty <= obj.intFromQty) {
                                alert("Provide valid from and to range");
                                $(this).find("input.txtToQty").focus();
                                isValid = false;
                                return false;
                            }

                            vendorCostTable.push(obj);
                            i += 1;
                        }
                    } else {
                        return false;
                    }
                });

                if (isValid) {
                    $("[id$=divLoader]").show();

                    if (vendorCostTable.length > 0) {
                        $.ajax({
                            type: "POST",
                            url: '../WebServices/CommonService.svc/VendorCostTable/Save',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "vendorTcode": parseInt($("[id$=hdnVendorTCode]").val())
                                , "vendorID": parseInt($("[id$=hdnDivisionID]").val())
                                , "itemCode": parseInt($("[id$=hdnItemID]").val())
                                , "currencyID": parseInt($("[id$=ddlCurrency]").val())
                                , "costRange": JSON.stringify(vendorCostTable)
                            }),
                            success: function (data) {
                                Close();
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $("[id$=divLoader]").hide();

                                if (IsJsonString(jqXHR.responseText)) {
                                    var objError = $.parseJSON(jqXHR.responseText)
                                    alert("Error occurred while saving cost table: " + replaceNull(objError.ErrorMessage));
                                } else {
                                    alert("Unknown error occurred while saving cost table");
                                }
                            }
                        });
                    }
                }

            } catch (e) {
                $("[id$=divLoader]").hide();
                alert("Unknown error occurred while saving cost table");
            }
        }

        function UpdateCost(btn,vendorCostTableID) {
            try {
                if ($(btn).closest("tr").find("input.txtStaticCost").val() == "") {
                    alert("Static cost is required field");
                    $(btn).closest("tr").find("input.txtStaticCost").focus();
                } else {
                    $("[id$=divLoader]").show();

                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/VendorCostTable/UpdateCost',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "vendorTcode": parseInt($("[id$=hdnVendorTCode]").val())
                            , "vendorID": parseInt($("[id$=hdnDivisionID]").val())
                            , "itemCode": parseInt($("[id$=hdnItemID]").val())
                            , "vendorCostTableID": (vendorCostTableID || 0)
                            , "staticCost": parseFloat($(btn).closest("tr").find("input.txtStaticCost").val())
                            , "dynamicCost": (parseFloat($(btn).closest("tr").find("input.txtDynamicCost").val()) || 0)
                        }),
                        success: function (data) {
                            $("[id$=divLoader]").hide();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $("[id$=divLoader]").hide();

                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred while updating cost: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error occurred while updating cost");
                            }
                        }
                    });
                }
            } catch (e) {
                $("[id$=divLoader]").hide();
                alert("Unknown error occurred while updating cost");
            }

            return false;
        }

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="pull-right">
        <button id="btnSave" class="btn btn-primary" onclick="return Save();">Save & Close</button>
        <button id="btnClose" class="btn btn-primary" onclick="Close();">Close</button>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Vendor Cost Table
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridFilterTools" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:DropDownList runat="server" ID="ddlCurrency" CssClass="form-control"></asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered table-striped" id="tblVendorCost">
                <thead>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="overlay" id="divLoader" style="display: none">
        <div class="overlayContent" style="background-color: #fff; color: #000; text-align: center; width: 280px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Processing Request</h3>
        </div>
    </div>
    <asp:HiddenField ID="hdnItemID" runat="server" />
    <asp:HiddenField ID="hdnDivisionID" runat="server" />
    <asp:HiddenField ID="hdnVendorTCode" runat="server" />
    <asp:HiddenField ID="hdnIsUpdateDefaultVendorCostRange" runat="server" />
    <asp:HiddenField ID="hdnDefaultVendorCostRange" runat="server" />
</asp:Content>
