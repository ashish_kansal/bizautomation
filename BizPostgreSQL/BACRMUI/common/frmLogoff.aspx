<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmLogoff.aspx.vb" Inherits="frmLogoff"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Logoff</title>
		<script language=javascript >
		function Login()
		{
			parent.location.href='../common/ticklerdisplay.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ClientMachineUTCTimeOffset='+new Date().getTimezoneOffset();
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<br>
			<br>
			<asp:table id="Table2" Runat="server" HorizontalAlign="center" CellPadding="0" CellSpacing="0"
				BorderWidth="1" BorderColor="black" GridLines="None" Width=300 >
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<br>
						<table align="center">
							<tr>
								<td class="normal1" align="center">For security reasons, you have
									<br>
									been logged off.
									<br>
									<br>
									Please login again.
									<br>
									<br>
									<asp:Button ID="btnLogin" Runat="server" text="Log Back In" CssClass="Credit"></asp:Button>
								</td>
							</tr>
						</table>
						<br>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
		</form>
	</body>
</HTML>
