﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Projects

Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic
Imports BACRM.BusinessLogic.Marketing
Imports Amazon
Imports Amazon.SimpleEmail
Imports Amazon.SimpleEmail.Model

Public Class frmAssociatedContacts
    Inherits BACRMUserControl


    Private type As String = Nothing
    Dim m_aryRightsForAssContacts() As Integer
    Public lngId As Long
    Public objType As Object
    Public TabIndex As Integer


    Public Property TypeId() As Long
        Get
            Return lngId
        End Get
        Set(ByVal value As Long)
            lngId = value
        End Set
    End Property

    Public Property PageType() As String
        Get
            Return type
        End Get
        Set(ByVal value As String)
            type = value
            Select Case type
                Case "CaseIP"
                    objType = New CaseIP
                    TabIndex = 1
                Case "OppotunitiesIP"
                    objType = New OppotunitiesIP
                    TabIndex = 2
            End Select
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            m_aryRightsForAssContacts = GetUserRightsForPage(10, 4)

            If Not IsPostBack Then

                objCommon.sb_FillComboFromDBwithSel(ddlContactRole, 26, Session("DomainID"))


                If m_aryRightsForAssContacts(RIGHTSTYPE.VIEW) = 0 Then
                    btnAddContact.Visible = False
                ElseIf m_aryRightsForAssContacts(RIGHTSTYPE.ADD) = 0 Then
                    btnAddContact.Visible = False
                End If
                btnAddContact.Attributes.Add("onclick", "return AddContact(" & TabIndex & ")")

                objType.DomainID = Session("DomainID")
                objType.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                If PageType = "CaseIP" Then
                    objType.CaseID = TypeId
                    dgContact.DataSource = objType.AssCntsByCaseID
                ElseIf PageType = "OppotunitiesIP" Then
                    objType.OpportunityId = TypeId
                    dgContact.DataSource = objType.AssociatedbyOppID
                End If

                dgContact.DataBind()
                GetBroadcastConfiguration()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub GetBroadcastConfiguration()
        Try
            Dim objCampaign As New Campaign
            objCampaign.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dt As DataTable = objCampaign.GetBroadcastConfiguration()
            Dim FromEmail As String
            Dim Domain As String
            Dim AWSAcceccKey As String
            Dim AWSSecretKey As String
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                FromEmail = CCommon.ToString(dt.Rows(0)("vcFrom"))
                Domain = CCommon.ToString(dt.Rows(0)("vcAWSDomain"))
                AWSAcceccKey = CCommon.ToString(dt.Rows(0)("vcAWSAccessKey"))
                AWSSecretKey = CCommon.ToString(dt.Rows(0)("vcAWSSecretKey"))

                Try
                    If Not String.IsNullOrEmpty(AWSAcceccKey.Trim()) AndAlso
                        Not String.IsNullOrEmpty(AWSSecretKey.Trim()) AndAlso
                        Not String.IsNullOrEmpty(Domain.Trim()) AndAlso
                        Not String.IsNullOrEmpty(FromEmail.Trim()) Then

                        Dim region As RegionEndpoint = RegionEndpoint.USWest2
                        Dim client As New AmazonSimpleEmailServiceClient(AWSAcceccKey.Trim(), AWSSecretKey.Trim(), region)

                        'Check Domain Verification Status
                        Dim requestIdentity As New GetIdentityVerificationAttributesRequest()
                        requestIdentity.Identities.Add(Domain.Trim())

                        Dim responseIdentity As GetIdentityVerificationAttributesResponse = client.GetIdentityVerificationAttributes(requestIdentity)

                        If Not responseIdentity Is Nothing AndAlso responseIdentity.VerificationAttributes.Count > 0 AndAlso responseIdentity.VerificationAttributes.ContainsKey(Domain.Trim()) Then
                            Dim identityAttriubute As IdentityVerificationAttributes = responseIdentity.VerificationAttributes(Domain.Trim())

                            Dim requestDkimIdentity As GetIdentityDkimAttributesRequest = New GetIdentityDkimAttributesRequest()
                            requestDkimIdentity.Identities.Add(Domain)

                            Dim responseDkimIdentity As GetIdentityDkimAttributesResponse = client.GetIdentityDkimAttributes(requestDkimIdentity)
                            If identityAttriubute.VerificationStatus = "Success" AndAlso responseDkimIdentity.DkimAttributes(Domain.Trim()).DkimVerificationStatus = "Success" AndAlso responseDkimIdentity.DkimAttributes(Domain.Trim()).DkimEnabled Then
                                secBrodcastNotConfigured.Visible = False
                            End If
                        Else
                            secBrodcastNotConfigured.Visible = True
                        End If

                    End If
                Catch ex As Exception
                    secBrodcastNotConfigured.Visible = True
                End Try
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            If radCmbCompany.SelectedValue <> "" Then
                FillContact(ddlAssocContactId, CInt(radCmbCompany.SelectedValue))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Public Sub FillContact(ByVal ddlCombo As DropDownList, ByVal lngDivision As Long)
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = lngDivision
                ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCombo.DataTextField = "Name"
                ddlCombo.DataValueField = "numcontactId"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnAddContact_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddContact.Click
        Try
            If TypeOf objType Is CaseIP Then
                objType.byteMode = 0
            ElseIf TypeOf objType Is OppotunitiesIP Then
                objType.Mode = 0
            End If

            objType.DomainID = Session("DomainID")
            objType.ContactID = CInt(ddlAssocContactId.SelectedItem.Value)
            objType.ContactRole = ddlContactRole.SelectedValue
            objType.bitPartner = 0
            objType.bitSubscribedEmailAlert = rdnYes.Checked
            If PageType = "CaseIP" Then
                objType.CaseID = TypeId
                dgContact.DataSource = objType.AddCaseContacts
            ElseIf PageType = "OppotunitiesIP" Then
                objType.OpportunityId = TypeId
                dgContact.DataSource = objType.AddOppContacts
            End If

            dgContact.DataBind()
            ddlContactRole.SelectedIndex = 0
            'chkShare.Checked = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContact.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                objType.ContactID = e.Item.Cells(0).Text

                If TypeOf objType Is CaseIP Then
                    objType.byteMode = 1
                ElseIf TypeOf objType Is OppotunitiesIP Then
                    objType.Mode = 1
                End If

                objType.DomainID = Session("DomainID")

                If PageType = "CaseIP" Then
                    objType.CaseID = TypeId
                    dgContact.DataSource = objType.AddCaseContacts
                ElseIf PageType = "OppotunitiesIP" Then
                    objType.OpportunityId = TypeId
                    dgContact.DataSource = objType.AddOppContacts
                End If

                dgContact.DataBind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Sub dgContact_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContact.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDeleteCnt")
                btnDelete = e.Item.FindControl("btnDeleteCnt")
                If m_aryRightsForAssContacts(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False
                    lnkDelete.Visible = True
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
                'If e.Item.Cells(1).Text = 1 Then CType(e.Item.FindControl("lblShare"), Label).Text = "a"
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

End Class