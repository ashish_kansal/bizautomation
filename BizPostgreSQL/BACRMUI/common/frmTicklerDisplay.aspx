<%@ Page Language="vb" EnableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master"
    CodeBehind="frmTicklerDisplay.aspx.vb" Inherits="BACRM.UserInterface.Common.frmTicklerDisplay" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Tickler Display</title>

    <link href='<%# ResolveUrl("~/JavaScript/daterangepicker-master/daterangepicker.css")%>' rel="stylesheet" />

    <script src='<%# ResolveUrl("~/JavaScript/daterangepicker-master/moment.min.js")%>'></script>
    <script src='<%# ResolveUrl("~/JavaScript/daterangepicker-master/daterangepicker.js")%>'></script>
    <script type="text/javascript" src='<%# ResolveUrl("~/JavaScript/colResizable-1.6.min.js")%>'></script>
    <script type="text/javascript" src='<%# ResolveUrl("~/JavaScript/biz.workorders.js")%>'></script>
    <script type="text/javascript" src='<%# ResolveUrl("~/JavaScript/biz.projects.js")%>'></script>
    <style>
        a.underLineHyperLink {
            color: #000;
            text-decoration: underline;
        }

        .radLists label {
            font-weight: normal;
            font-size: 14px;
            font-weight: bold;
        }

        .multiselect-group {
            background-color: #3e3e3e !important;
        }

            .multiselect-group a:hover {
                background-color: #3e3e3e !important;
            }

            .multiselect-group a {
                color: #fff !important;
            }

        .multiselect-container > li.multiselect-group label {
            padding: 3px 20px 3px 5px !important;
        }

        .multiselect-container {
            min-width: 250px;
            border: 1px solid #e1e1e1;
        }

        .table-bordered > tbody > tr > th {
            border-bottom-width: 2px;
            overflow: inherit !important;
        }

        #divTaskControlsProject .btn-xs, #divTaskControlsWorkOrder .btn-xs{
            padding-left: 10px !important;
            padding-right: 10px !important;
            font-size: 16px !important;
            padding-top: 10px !important;
            padding-bottom: 10px !important;
        }

         .btn-task-finish {
            background-color: white;
            color: black;
            border: 1px solid black;
            font-weight: bold;
            padding-left: 10px !important;
            padding-right: 10px !important;
            font-size: 16px !important;
            padding-top: 10px !important;
            padding-bottom: 10px !important;
        }

        .btn-comm-followup {
            padding-left: 10px !important;
            padding-right: 10px !important;
            font-size: 16px !important;
            padding-top: 10px !important;
            padding-bottom: 10px !important;
        }
    </style>
    <link href='<%# ResolveUrl("~/JavaScript/MultiSelect/bootstrap-multiselect.css")%>' rel="stylesheet" />
    <script src='<%# ResolveUrl("~/JavaScript/MultiSelect/bootstrap-multiselect.js")%>'></script>
    <style>
        #divTaskControlsWorkOrder .taskTimer, #divTaskControlsWorkOrder .taskTimerInitial {
            font-size: 14px;
            color: #00a65a;
        }

        #divTaskControlsProject .taskTimer, #divTaskControlsProject .taskTimerInitial {
            font-size: 14px;
            color: #00a65a;
        }

        .underline {
            text-decoration: underline;
        }

        .overlay1 {
            position: fixed;
            z-index: 99;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        .btnWarn {
            background-color: #bf9f6f !important;
            color: #fff !important;
            font-weight: 600;
            font-size: 15px;
        }

        .btnPrime {
            background-color: #6b89b5 !important;
            color: #fff !important;
            font-weight: 600;
            font-size: 15px;
        }

        .btnSuccess {
            background-color: #85c47a !important;
            color: #fff !important;
            font-weight: 600;
            font-size: 15px;
        }

        .cursor {
            cursor: pointer !important;
        }

        .FromDateRangePicker {
            border: 1px solid #e1e1e1;
            border-bottom: 0px;
            padding: 2px;
            font-style: italic;
            clear: both;
        }

        .ToDateRangePicker {
            border: 1px solid #e1e1e1;
            border-top: 1px solid #ccc;
            padding: 2px;
            font-style: italic;
        }

        .btn-previous-week {
            background-color: #fff;color: red;border-width: 2px;font-weight: bold;border-color:red;
        }

        .btn-previous-week-selected {
            border-width: 2px;font-weight: bold;border-color:red;
        }

       .btn-previous-week-selected, .btn-previous-week.focus, .btn-previous-week:hover {
            color: #fff;
            background-color:red;
        }

        .btn-current-week {
            background-color: #fff;color: #204d74;border-width: 2px;font-weight: bold;border-color:#204d74;
        }

        .btn-current-week-selected {
            border-width: 2px;font-weight: bold;border-color:#204d74;
        }

        .btn-current-week-selected, .btn-current-week.focus, .btn-current-week:hover {
            color: #fff;
            background-color:#204d74;
        }

        .btn-next-week {
            background-color: #fff;color: #00a65a;border-width: 2px;font-weight: bold;border-color:#00a65a;
        }

        .btn-next-week-selected {
            border-width: 2px;font-weight: bold;border-color:#00a65a;
        }

        .btn-next-week-selected, .btn-next-week.focus, .btn-next-week:hover {
            color: #fff;
            background-color:#00a65a;
        }

        #gvSearch .popover {
            max-width:100%;
            white-space:pre-wrap;
        }
    </style>
    <script type="text/javascript">
        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        $(document).keydown(function (event) { 
            if (event.keyCode == 13) {
                debugger;
                on_filter();
            }
        });
        
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);

            $('[id$=gvSearch] select').change(function () {
                debugger;
                if ($(this).prop("className") == "option-droup-multiSelection-Group") {
                    return false;
                } else {
                    debugger;
                    on_filter();
                }
                CheckAndButtonLoadCount();
            });

            if ($('.option-droup-multiSelection-Group') != null) {
                var prm = Sys.WebForms.PageRequestManager.getInstance();


                prm.add_endRequest(function () {

                    $('.option-droup-multiSelection-Group').multiselect({
                        enableClickableOptGroups: true,
                        onSelectAll: function () {
                            console.log("select-all-nonreq");
                        },
                        optionClass: function (element) {
                            var value = $(element).attr("class");
                            return value;
                        }
                    });
                    $('[id$=gvSearch] select').change(function () {
                        debugger;
                        if ($(this).prop("className") == "option-droup-multiSelection-Group") {
                            return false;
                        } else {
                            debugger;
                            on_filter();
                        }
                    });
                
                });
                $('.option-droup-multiSelection-Group').multiselect({
                    enableClickableOptGroups: true,
                    onSelectAll: function () {
                        console.log("select-all-nonreq");
                    },
                    optionClass: function (element) {
                        var value = $(element).attr("class");
                        return value;
                    }
                });
            }
        });

        function addRemoveTimeProject(domainID, divID, timeInMinutes, taskId, startDateTime, endDateTime, asssignTo, taskName, actualTaskTime, vcEmail, vcName) {
            
            var timeInMinutesId = 2;
            if ($(".chk_" + taskId + " input").prop("checked") == false) {
                timeInMinutesId = 1;
            } else {
                timeInMinutesId = 2;
            }
            var timeSecounds = (timeInMinutes * 60);
            $.ajax({
                type: "POST",
                url: '../projects/frmProjects.aspx/WebMethodAddRemoveTimeFromContract',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "DomainID": domainID,
                    "DivisionId": divID,
                    "TaskId": taskId,
                    "numSecounds": timeSecounds,
                    "Type": timeInMinutesId,
                    "isProjectActivityEmail": 1
                }),
                beforeSend: function () {
                    $("[id$=UpdateProgress]").show();
                },
                complete: function () {
                    $("[id$=UpdateProgress]").hide();

                },
                success: function (data) {
                    $("[id$=UpdateProgressProject]").hide();
                    var dataArr = $.parseJSON(data.d);
                    if (parseInt(dataArr[0]) == 1) {
                        alert("You need to add more time to the contract before attempting to deduct time from this activity / task");
                        $(".chk_" + taskId + " input").removeAttr("checked")
                    } else if (parseInt(dataArr[0]) == 2) {
                        $("#hdnContractBalanceRemaining").val(parseInt(dataArr[1]));
                        $("#hdnStartDateTime").val(startDateTime);
                        $("#hdnAssignToForContract").val(vcEmail);
                        $("#hdnAssignToForContractName").val(vcName);
                        $("#hdnEndDateTime").val(endDateTime);
                        $("#hdnActualTaskCompletionTime").val(actualTaskTime);
                        $("#hdnSelectedTaskTitle").val(taskName);
                        $("#hdnSelectedTaskID").val(taskId);

                        if ($("input.chk_" + taskId).is(":checked")) {
                            $("#btnOpenEmailContract").click();
                        }
                    }
                }
            });
        }

        function addRemoveTime(timeInSecounds, activityID, StartDate, EndDate, Subject, Email, Name, DivisonId) {
            var timeInMinutesId = 2;
            if ($(".chk_" + activityID + "").prop("checked") == false) {
                timeInMinutesId = 1;
            } else {
                timeInMinutesId = 2;
            }
            $.ajax({
                type: "POST",
                url: '../projects/frmProjects.aspx/WebMethodAddRemoveTimeFromContract',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "DomainID": <%=Session("DomainID")%>,
                    "DivisionId": DivisonId,
                    "TaskId": activityID,
                    "numSecounds": timeInSecounds,
                    "Type": timeInMinutesId,
                    "isProjectActivityEmail": 3
                }),
                beforeSend: function () {
                    $("[id$=UpdateProgress]").show();
                },
                complete: function () {
                    $("[id$=UpdateProgress]").hide();
                },
                success: function (data) {
                    $("[id$=UpdateProgress]").hide();
                    var dataArr = $.parseJSON(data.d);
                    if (parseInt(dataArr[0]) == 1) {
                        alert("You need to add more time to the contract before attempting to deduct time from this activity / task");
                        $(".chk_" + taskId + "").removeAttr("checked")
                    } else if (parseInt(dataArr[0]) == 2) {
                        if (confirm("Would you like to send task completion statement including the time it took to complete this task, and how much time is left on the contract ?")) {
                            $("#hdnContractBalanceRemaining").val(parseInt(dataArr[1]));
                            $("#hdnStartDateTime").val(StartDate);
                            $("#hdnAssignToForContract").val(Email);
                            $("#hdnAssignToForContractName").val(Name);
                            $("#hdnEndDateTime").val(EndDate);
                            $("#hdnActualTaskCompletionTime").val(timeInSecounds);
                            $("#hdnSelectedTaskTitle").val(Subject);
                            $("#btnOpenEmailContract").click();
                        }
                    }
                    var hours = Math.floor(parseInt(dataArr[1]) / 60 / 60);
                    var minutes = Math.floor(parseInt(dataArr[1]) / 60) - (hours * 60);
                    if (hours < 10) {
                        hours = "0" + hours;
                    }
                    if (minutes < 10) {
                        minutes = "0" + minutes;
                    }
                    $("#lblPendingTime_" + activityID + "").text(hours + ":" + minutes);
                }
            });
        }
        function pageLoaded() {
            $('#gvSearch [data-toggle="popover"]').popover(); 

            $('body').on('click', function (e) {
                $('#gvSearch [data-toggle=popover]').each(function () {
                    // hide any open popovers when the anywhere else in the body is clicked
                    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                        $(this).popover('hide');
                    }
                });
            });

            $(":checkbox[attrEmailCommId]").each(function (index, element) {
                debugger;

                var recordId = $(element).attr("attrEmailCommId");
                $.ajax({
                    type: "POST",
                    url: '../common/frmTicklerDisplay.aspx/GetEmailActivityDetails',
                    data: "{ActivityId: " + recordId + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    error:
                        function (XMLHttpRequest, textStatus, errorThrown) {
                            console.log("Error");
                        },
                    success:
                        function (result) {
                            var ReturnData = result.d;
                            var dataArr = $.parseJSON(result.d);
                            var activityDetails = dataArr.Table[0];
                            recordId = activityDetails.ActivityID;
                            debugger;
                            $(".chk_" + recordId + "").prop("checked", activityDetails.bitTimeAddedToContract);
                            $("#lblPendingTime_" + recordId + "").text(activityDetails.timeLeft);
                            $(".chk_" + recordId + "").attr("onclick", "addRemoveTime('" + activityDetails.timeInSecounds + "', '" + recordId + "', '" + activityDetails.StartDateTimeUtc + "', '" + activityDetails.EndDateTime + "', '" + activityDetails.Subject + "', '" + activityDetails.AssignEmail + "', '" + activityDetails.AssignName + "', '" + activityDetails.numDivisionId +"')");
                            debugger;

                            if (parseFloat(activityDetails.timeLeftinSec) > 0) {
                                $("#lblPendingTime_" + recordId + "").css("display", "inline");
                                $(".chk_" + recordId + "").css("display", "inline");
                                $("#imgTimeIconn_" + recordId + "").css("display", "inline");
                            } else {
                                $("#lblPendingTime_" + recordId + "").css("display", "none");
                                $(".chk_" + recordId + "").css("display", "none");
                                $("#imgTimeIconn_" + recordId + "").css("display", "none");
                            }

                        }
                });
               
            })
            var start = moment().subtract(29, 'days');
            var end = moment();
            function cb(start, end) {
                debugger;

                document.getElementById("" + this.element[0].id + "").setAttribute("value", start.format('MM/DD/YYYY'))
                var FromId = this.element[0].id;
                FromId = FromId.replace("From", "To");

                document.getElementById("" + FromId + "").setAttribute("value", end.format('MM/DD/YYYY'));
                on_filter();
            }
            $(".FromDateRangePicker").each(function (index, object) {
                var dateRange = $(this).attr("daterange");
                var dateArray = dateRange.split("#");
                var start = moment().subtract(29, 'days').format('MM/DD/YYYY');
                var end = moment().format('MM/DD/YYYY');
                if (dateArray.length == 2) {
                    start = dateArray[0];
                    end = dateArray[1];
                }
                $(this).daterangepicker({
                    "autoApply": true,
                    "autoUpdateInput": false,
                    "showDropdowns": true,
                    "startDate": start,
                    "endDate": end
                }, cb);
            });
            BindJquery()
            CheckAndButtonLoadCount();
           
            var workOrderIds = "";

            $("#gvSearch tr").not(":first").each(function (index, tr) {
                if ($(this).find("label.lblWorkOrderStatus").length > 0) {
                    workOrderIds += ((workOrderIds.length > 0 ? "," : "") + $(this).find("label.lblWorkOrderStatus").attr("id"));
                }
            });

            if (workOrderIds.length > 0) {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetWorkOrderStatus',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "workOrderIDs": workOrderIds
                    }),
                    success: function (data) {
                        var obj = $.parseJSON(data.GetWorkOrderStatusResult);

                        $("#gvSearch > tbody > tr").not(":first").each(function (index, tr) {
                            if ($(this).find("label.lblWorkOrderStatus").length > 0) {
                                var result = $.grep(obj, function (e) { return e.numWOID == $(tr).find("label.lblWorkOrderStatus").attr("id"); });

                                if (result.length === 0) {
                                    $(tr).find("label.lblWorkOrderStatus").replaceWith("<i class='fa fa-exclamation-triangle'></i>");
                                } else {
                                    $(tr).find("label.lblWorkOrderStatus").replaceWith(result[0].vcWorkOrderStatus);
                                }
                            }
                    
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(tr).find("label.lblWorkOrderStatus").closest("td").html("<i class='fa fa-exclamation-triangle'></i>");
                    }
                });
            }
            $("[id$=gvSearch]").colResizable({
                liveDrag: true,
                gripInnerHtml: "<div class='grip2'></div>",
                draggingClass: "dragging",
                minWidth: 30,
                resizeMode: "overflow",
                onResize: onActionGridColumnResized
            });
        }
        function ApproveRejectPORequistion(OrderId, Type) {

            var DomainID = '<%= Session("DomainId")%>';
            var UserContactID = '<%= Session("UserContactID")%>';
            var dataParam = "{DomainID:'" + DomainID + "',UserContactID:'" + UserContactID + "',OrderId:" + OrderId + ",Type:" + Type+"}";
            var arrayToCheck = [];
            $.ajax({
                type: "POST",
                url: "../common/frmTicklerDisplay.aspx/WebMethodApproveRejectPORequistion",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    on_filter();
                    var Jresponse = $.parseJSON(response.d);
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }
        function ApproveRejectPriceMarginApproval(OrderId,Type) {
            var DomainID = '<%= Session("DomainId")%>';
            var UserContactID = '<%= Session("UserContactID")%>';
            var dataParam = "{DomainID:'" + DomainID + "',UserContactID:'" + UserContactID + "',OrderId:" + OrderId + ",Type:" + Type+"}";
            var arrayToCheck = [];
            $.ajax({
                type: "POST",
                url: "../common/frmTicklerDisplay.aspx/WebMethodApproveRejectPriceMarginApproval",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    on_filter();
                    var Jresponse = $.parseJSON(response.d);
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }
        function BindTicklerUserActivity() {
            var DomainID = '<%= Session("DomainId")%>';
            var UserContactID = '<%= Session("UserContactID")%>';
            var dataParam = "{DomainID:'" + DomainID + "',UserContactID:'" + UserContactID + "'}";
            var arrayToCheck = [];
            $.ajax({
                type: "POST",
                url: "../common/frmTicklerDisplay.aspx/WebMethodGetTaskList",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse.length > 0) {
                        $.each(Jresponse, function (index, value) {
                            var IsExist = $.inArray(value.numStageDetailsId, arrayToCheck);
                            if (value.dtmDueDate == null) {
                                value.dtmDueDate = '-';
                            }
                            if (IsExist >= 0) {
                                $("#spanStageDetailsId" + value.numStageDetailsId + "").append("," + "<a class='underline' href='#'  onclick='openTask("+value.numOppID+","+value.numStagePercentageId+","+value.tintPercentage+","+value.tintConfiguration+")'>" + value.vcTaskName + "</a>");
                            } else {
                                arrayToCheck.push(value.numStageDetailsId);
                                $("#tblUserActivity").append("<tr><td>"+value.dtmDueDate+"</td><td id='spanStageDetailsId"+value.numStageDetailsId+"'><a class='underline' href='#' onclick='openTask("+value.numOppID+","+value.numStagePercentageId+","+value.tintPercentage+","+value.tintConfiguration+")'>"+value.vcTaskName+"</a></td><td>"+value.OrderType+"</td><td>"+value.vcMileStoneName+","+value.vcStageName+"</td><td><a class='underline' href='#' onclick='openSalesOrder("+value.numOppID+")'>"+value.vcPOppName+"</a></td></tr>");
                            }
                        });
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }
        function openSalesOrder(oppId) {
            window.open("../opportunity/frmOpportunities.aspx?opId=" + oppId, '_blank');
        }
        function openTask(oppId,numStagePercentageId,tinProgressPercentage,tintConfiguration) {
            window.open("../opportunity/frmOpportunities.aspx?opId=" + oppId + "&MileStoneId=" + tintConfiguration + "_" + numStagePercentageId + "_" + tinProgressPercentage, '_blank');
        }
        function openPurchaseBill(VendorId) {
            var FromDate = $("[id$=hdnFrom]").val();
            var ToDate = $("[id$=hdnTo]").val();
            window.open("../Accounting/frmVendorPayment.aspx?VendorId=" + VendorId + "&FromDate='" + FromDate + "' & ToDate='" + ToDate + "'&FilterFromTickler=1", '_blank');
        }
        function OpenMarginApproval(oppId) {
            window.open("../opportunity/frmOpportunities.aspx?opId=" + oppId + "&ForPriceMarginApproval=1", '_blank');
        }
        function openProjectTask(ProjectId) {
            window.open("../projects/frmProjects.aspx?ProId=" + ProjectId+"", '_blank');
        }
        function openBizDoc(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenAmtPaid(a, b, c) {
            RPPopup = window.open('../opportunity/frmAmtPaid.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizId=' + b + '&DivId=' + c, 'ReceivePayment', 'toolbar=no,titlebar=no,top=300,width=700,height=400,scrollbars=no,resizable=no');
            console.log(RPPopup.name);
            return false;
        }
        // Grid Column Resize
        $(document).ready(function () {
            BindTicklerUserActivity()
            BindJquery();

            $("#hplClearGridCondition").click(function () {
                $('#txtGridColumnFilter').val("");
                $('#txtCurrrentPage').val(1);
                
                $('#btnGo1').trigger('click');
            });
            $("[id$=gvSearch]").colResizable({
                liveDrag: true,
                gripInnerHtml: "<div class='grip2'></div>",
                draggingClass: "dragging",
                minWidth: 30,
                resizeMode: "overflow",
                onResize: onActionGridColumnResized
            });
            ManualStyleTuneUps();
             
        });

        function ManualStyleTuneUps() {
            $('.table-striped .chkSelect').closest('td').css('text-align', 'center');
            $('.table-striped #chkAll').closest('td').css('width', '30px');
        }

        var onActionGridColumnResized = function (e) {
            var columns = $(e.currentTarget).find("th");
            var msg = "";
            columns.each(function () {
                //console.log($(this));

                var thId = $(this).attr("id");
                var thWidth = $(this).width();
                msg += (thId || "anchor") + ':' + thWidth + ';';
            }
            )
            //console.log(msg);

            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/SaveGridColumnWidth",
                data: "{str:'" + msg + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });

            //on_filter();
        };
        function reDirect(a) {
            document.location.href = a;
        }

        function progress(ProgInPercent, Container, progress, prg_width) {
            //Set Total Width
            var OuterDiv = document.getElementById(Container);
            OuterDiv.style.width = prg_width + 'px';
            OuterDiv.style.height = '5px';

            if (ProgInPercent > 100) {
                ProgInPercent = 100;
            }
            //Set Progress Percentage
            var node = document.getElementById(progress);
            node.style.width = parseInt(ProgInPercent) * prg_width / 100 + 'px';
        }
        function OpenOpp(a, b) {

            var str;

            str = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" + a;
            window.open(str, '_blank');
            //document.location.href = str;
        }

        function OpenActionItem(a,b) {
            document.location.href = "../admin/ActionItemDetailsOld.aspx?CommId=" + a + "&lngType=" + b
            return false;
        }
        function OpenDocument(a) {

            document.location.href = "../Documents/frmAddSpecificDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=O&yunWE=12515&tyuTN=" + a + "";
            document.location.href = "../Documents/frmDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocId=" + a + "&frm=Tickler&SI1=0&SI2=0&TabID=1";
            return false;
        }
        function OpenBizDocActionItem(a, b, c) {
            var radTab = $find("radOppTab");
            var selectedIndex = radTab.get_selectedIndex();

            document.location.href = "../admin/BizDocActionDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BizDocActionId=" + a + "&frm=tickler" + "&SI1=" + selectedIndex +

                "&BizDocDate=" + b + "&ContactID=" + c
            return false
        }
        function openCommTask(a,b) {
            window.open("../admin/ActionItemDetailsOld.aspx?CommId=" + a + "&lngType="+b+"", '_blank');
        }
        function openCaseDetails(a) {
            window.open("../cases/frmCases.aspx?CaseID=" + a + "", '_blank');
        }
        function OpenCalItem(a) {//qwwq
            if (a == null || a == "") { window.open("https://calendar.google.com/", '_blank') } else {
                window.open(a, '_blank')
            }
           
        }
        function OpenCompany(a, b, c) {
            var radTab = $find("radOppTab");
            //alert(radTab)
            var selectedIndex = radTab.get_selectedIndex();

            if (b == 0) {
                window.location.href = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=tickler&DivID=" + a + "&SI1=" + selectedIndex
            }

            else if (b == 1) {
                window.location.href = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=tickler&DivID=" + a + "&SI1=" + selectedIndex
            }
            else if (b == 2) {
                window.location.href = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=tickler&klds+7kldf=fjk-las&DivId=" + a + "&SI1=" + selectedIndex
            }

            return false;
        }
        function OpenContact(a, b) {
            var radTab = $find("radOppTab");
            var selectedIndex = radTab.get_selectedIndex();

            window.location.href = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=tickler&fda45s=oijf6d67s&CntId=" + a + "&SI1=" + selectedIndex

            return false;
        }

        function OpenEmail(str) {
            window.open(str)
            return true;
        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }
        function openStage(a, b, c, type, d) {
            document.getElementById('cntDoc').src = "../common/frmAssignedStages.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Oppid=" + a + "&startdate=" + b + "&enddate=" + c + "&TicklerType=" + d + "&OppType=" + type
            document.getElementById('divStg').style.visibility = "visible";
            return false;
        }
        function openAdmin() {
            window.location.href = "../admin/frmAdminSection.aspx";
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function OpenSelTeam() {
            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=1", '',

                'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function openEmpAvailability() {
            window.open("../ActionItems/frmEmpAvailability.aspx?frm=Tickler", '',

                'toolbar=no,titlebar=no,top=0,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.getElementById("btnGo").click();
        }
        function OpemEmail(URL) {
            window.open(URL, 'Compose','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenSetting() {
            window.open('../Items/frmConfItemList.aspx?FormID=43', '',

                'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function EditExpenses(id) {
            window.open('../TimeAndExpense/frmEditTimeExp.aspx?CatID=' + id + '', '', 'toolbar=no,titlebar=no,top=200,left=200,width=750,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenFilterSetting() {
            window.open('../common/frmTicklerListFilterSetting.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function SortColumn(a) {

            if ($("[id$=txtSortColumnActionItems]").val() == a) {
                if ($("[id$=txtSortOrderActionItems]").val() == 'D')
                    $("[id$=txtSortOrderActionItems]").val('A');
                else
                    $("[id$=txtSortOrderActionItems]").val('D');
            }
            else {
                $("[id$=txtSortColumnActionItems]").val(a);
                $("[id$=txtSortOrderActionItems]").val('D');
            }
            $('[id$=btnGo]').trigger('click');
            return false;
        }
        function BindJquery() {
            $("[id$=gvSearch] input:text").keydown(function (event) {
                if (event.keyCode == 13) {
                    console.log($('#txtGridColumnFilter').val());
                    on_filter();
                    return false;
                }
            });

            //$('[id$=gvSearch] select').change(function () {
            //    on_filter();
            //    console.log("Selected");
            //});

            $("#hplClearGridCondition").click(function () {
                $('#txtGridColumnFilter').val("");
                $('#txtCurrrentPage').val(1);
                $('[id$=btnGo]').trigger('click');
            });
        }
       
        function on_filter() {

            var filter_condition = '';
            
            debugger;
            var dropDownControls = $('[id$=gvSearch] tr:first select');
            dropDownControls.each(function () {
                debugger;
                if ($(this).get(0).selectedIndex > 0) {
                    var ddId = $(this).attr("id");
                    var ddValue = $(this).val();
                    filter_condition += ddId + ':' + ddValue + ';';
                }
            }
            )

            var textBoxControls = $('[id$=gvSearch] tr:first input:text');
            textBoxControls.each(function () {
                if ($.trim($(this).val()).length > 0) {
                    var txtId = $(this).attr("id");
                    var txtValue = $.trim($(this).val());
                    filter_condition += txtId + ':' + txtValue + ';';
                }
            }
            )

            $('#txtGridColumnFilter').val(filter_condition);
            //console.log($('#txtGridColumnFilter').val());
            $('#txtCurrrentPage').val('1');
            $('[id$=btnGo]').trigger('click');

        }
        function DeleteActionItemsConfirmation() {

            var gridName;
            var grdValue;
            gridName = 'gvSearch';
            //grdValue = $find("radOppTab").get_selectedTab().get_value();
            //console.log(grdValue);

            //if (grdValue == 'ActionItemsMeetings') {
            //    gridName = 'gvSearch';
            //}
            //else if (grdValue == 'Cases') {
            //    gridName = 'dgCases'
            //}
            //else {
            //    alert('Error in javascript. Please contact System administrator.!');
            //    return false;
            //}

            if (DeleteRecord(gridName) == true) {
                if (confirm('Selected Action Items will be deleted and removed from the list (N/A for BizDoc Approvals). Do you want to proceed ?')) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        function CloseActionItemsConfirmation() {
            if (confirm('Selected Action Items will be closed and removed from the list (N/A for BizDoc Approvals or Calendar items). Do you want to proceed ?')) {
                return true;
            }
            else {
                return false;
            }
        }

        //select all 
        function SelectAll(headerCheckBox, ItemCheckboxClass, gridName) {
            $("[id$=" + gridName + "] ." + ItemCheckboxClass + " input[type = 'checkbox']").each(function () {
                $(this).prop('checked', $('#' + gridName + ' #' + headerCheckBox).is(':checked'))
            });
        }

        function DeleteRecord(gridName) {
            var i = 0;
            $("[id$=" + gridName + "] #chkSelect").each(function () {
                //console.log($(this));
                if ($(this).is(':checked')) {
                    i += 1;
                }
            });

            if (i == 0) {
                alert('Please select atleast one record to delete!');
                return false;
            }
            return true;
        }
        function OpenHelp() {
            window.open('../Help/Tickler_Initial_Detail_Page_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function OnClientTabSelected(sender, eventArgs) {
            var tab = eventArgs.get_tab();
            PersistTab(tab.get_value());
            //Added By:Richa Garg ||Date:25Feb2019
            //Puprose : for persistent tabs
        }
        function CheckAndButtonLoadCount() {
            
            if ($("#<%=hypItemsToPick.ClientID%>").length > 0) {
                LoadDataDetailsCount(1,0);
            }
            if ($("#<%=hypItemsPackShip.ClientID%>").length > 0) {
                LoadDataDetailsCount(2,0);
            }
            if ($("#<%=hypItemToInvoice.ClientID%>").length > 0) {
                LoadDataDetailsCount(3,0);
            }
            if ($("#<%=hypSalesOrderToClose.ClientID%>").length > 0) {
                LoadDataDetailsCount(5,0);
            }
            if ($("#<%=hypItemsToPutAway.ClientID%>").length > 0) {
                LoadDataDetailsCount(2,1);
            }
            if ($("#<%=hypItemsToBill.ClientID%>").length > 0) {
                LoadDataDetailsCount(3,1);
            }
            if ($("#<%=hypPOToClose.ClientID%>").length > 0) {
                LoadDataDetailsCount(4,1);
            }
           <%-- if ($("#<%=hypBOMToPick.ClientID%>").length > 0) {
                LoadDataDetailsCount(1);
            }--%>
        }
        function LoadDataDetailsCount(ViewId,Type) {
            var FromDate = $("[id$=hdnFrom]").val();
            var ToDate = $("[id$=hdnTo]").val();
            $.ajax({
                type: "POST",
                url: '../common/frmTicklerDisplay.aspx/WebMethodLoadCounts',
                data: "{FromDate: '" + FromDate + "', ToDate: '" + ToDate + "', ViewId:" + ViewId + ",TypeId:" + Type+"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async:true,
                error:
                    function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log("Error");
                    },
                success:
                    function (result) {
                        var ReturnData = result.d;
                        var returnArr = ReturnData.split("$");
                        if (returnArr.length == 3) {
                            if (returnArr[1] == "1" && returnArr[2]=="0") {
                                $("#<%=lblItemstoPick.ClientID%>").text(returnArr[0]);
                            }
                            if (returnArr[1] == "2" && returnArr[2] == "0") {
                                $("#<%=lblItemsPackShip.ClientID%>").text(returnArr[0]);
                            }
                            if (returnArr[1] == "3" && returnArr[2] == "0") {
                                $("#<%=lblItemToInvoice.ClientID%>").text(returnArr[0]);
                            }
                            if (returnArr[1] == "5" && returnArr[2] == "0") {
                                $("#<%=lblSalesOrderToClose.ClientID%>").text(returnArr[0]);
                            }
                            if (returnArr[1] == "2" && returnArr[2] == "1") {
                                $("#<%=lblItemsToPutAway.ClientID%>").text(returnArr[0]);
                            }
                            if (returnArr[1] == "3" && returnArr[2] == "1") {
                                $("#<%=lblItemsToBill.ClientID%>").text(returnArr[0]);
                            }
                            if (returnArr[1] == "3" && returnArr[2] == "1") {
                                $("#<%=lblItemsToBill.ClientID%>").text(returnArr[0]);
                            }
                            if (returnArr[1] == "4" && returnArr[2] == "1") {
                                $("#<%=lblPOToClose.ClientID%>").text(returnArr[0]);
                            }
                        }
                    }
            });
            }
            function PersistTab(index) {
                var key;
                var value;
                var formName;

                key = "index";
                value = index;

                var pagePath = window.location.pathname + "/PersistTab";
                var dataString = "{strKey:'" + key + "',strValue:'" + value + "'}";
                IsExists(pagePath, dataString);
            }
            function OpenAddPopUp(Url) {
                window.open(Url, "", "toolbar = no, titlebar = no, left = 100, top = 100, width = 1200, height = 645, scrollbars = yes, resizable = yes");
                return false;
            }
            function IsExists(pagePath, dataString) {
                $.ajax({
                    type: "POST",
                    url: pagePath,
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    error:
                        function (XMLHttpRequest, textStatus, errorThrown) {
                            console.log("Error");
                        },
                    success:
                        function (result) {
                            console.log("success");
                        }
                });
            }

            function TaskClosed(taskID){
                try {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/StagePercentageDetailsTaskService.svc/Close',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "taskID": taskID
                        }),
                        beforeSend: function () {
                            $("[id$=UpdateProgress]").show();
                        },
                        complete: function () {
                            $("[id$=UpdateProgress]").hide();
                        },
                        success: function (data) {
                            document.location.href = document.location.href;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                if (objError.Message != null) {
                                    alert("Error occurred: " + objError.Message);
                                } else {
                                    alert(objError);
                                }
                            } else {
                                alert("Unknown error ocurred");
                            }
                        }
                    });
                } catch (e) {
                    alert("Unknown error occurred.")
                }

                return false;
            }

            function CalendarFinished(activityID){
                try {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/RemoveActivity',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "activityID": activityID
                        }),
                        beforeSend: function () {
                            $("[id$=UpdateProgress]").show();
                        },
                        complete: function () {
                            $("[id$=UpdateProgress]").hide();
                        },
                        success: function (data) {
                            document.location.href = document.location.href;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                if (objError.Message != null) {
                                    alert("Error occurred: " + objError.Message);
                                } else {
                                    alert(objError);
                                }
                            } else {
                                alert("Unknown error ocurred");
                            }
                        }
                    });
                } catch (e) {
                    alert("Unknown error occurred.")
                }

                return false;
            }

            function ActionItemFinished(commID){
                try {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/FinishCommunication',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "commID": commID
                        }),
                        beforeSend: function () {
                            $("[id$=UpdateProgress]").show();
                        },
                        complete: function () {
                            $("[id$=UpdateProgress]").hide();
                        },
                        success: function (data) {
                            document.location.href = document.location.href;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                if (objError.Message != null) {
                                    alert("Error occurred: " + objError.Message);
                                } else {
                                    alert(objError);
                                }
                            } else {
                                alert("Unknown error ocurred");
                            }
                        }
                    });
                } catch (e) {
                    alert("Unknown error occurred.")
                }

                return false;
            }

            function ActionItemFollowup(commID,contactID){
                try {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/FinishCommunication',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "commID": commID
                        }),
                        beforeSend: function () {
                            $("[id$=UpdateProgress]").show();
                        },
                        complete: function () {
                            $("[id$=UpdateProgress]").hide();
                        },
                        success: function (data) {
                            document.location.href = "../admin/ActionItemDetailsOld.aspx?cntid=" + (contactID || 0);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                if (objError.Message != null) {
                                    alert("Error occurred: " + objError.Message);
                                } else {
                                    alert(objError);
                                }
                            } else {
                                alert("Unknown error ocurred");
                            }
                        }
                    });
                } catch (e) {
                    alert("Unknown error occurred.");
                }
            }

            function OpemEmail(a, b) {
                window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
                return false;
            }

            function OpenContact(a, b) {
                window.open("../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactlist&ft6ty=oiuy&CntId=" + a + '&ContactType=1','_blank');
                return false;
            }
    </script>
    <script type="text/javascript">
        var arrFieldConfig = new Array();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>Filter Action Items</label>
                    <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged">
                        <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                        <asp:ListItem Text="My Action Items" Value="1"></asp:ListItem>
                        <asp:ListItem Text="By Team" Value="2"></asp:ListItem>
                        <asp:ListItem Text="By Territory" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                    <telerik:RadComboBox ID="rcbFilterValues" runat="server" CheckBoxes="true" OnSelectedIndexChanged="rcbFilterValues_SelectedIndexChanged" AutoPostBack="true">

                    </telerik:RadComboBox>
                </div>
            </div>
            <div class="pull-right">
                <asp:LinkButton ID="lnkAddNewActionItem" runat="server" OnClientClick="return OpenAddPopUp('../common/frmAddActivity.aspx')" OnClick="lnkAddNewActionItem_Click" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</asp:LinkButton>
                <asp:Button runat="server" ID="btnCloseAction" Visible="false" Style="display: none" CssClass="btn btn-primary" Text="Close Action Item(s)" />
                <asp:LinkButton ID="btnDeleteCases" runat="server" Style="display: none" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
            <asp:HiddenField ID="hdnFrom" runat="server" />
            <asp:HiddenField ID="hdnTo" runat="server" />
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">

            <div class="table-responsive" style="min-height: 600px !important;">
                <div class="pull-right">
                </div>
                <div class="pull-left">
                </div>

                <asp:HiddenField ID="hdnDeleteType" runat="server" />
                <asp:HiddenField ID="hdnlngCommID" runat="server" />
                <asp:HiddenField ID="hdnlngRecOwnID" runat="server" />
                <asp:HiddenField ID="hdnnumContactID" runat="server" />
                <asp:HiddenField ID="hdnlngTerrID" runat="server" />
                <div class="overlay1" id="divActivityDeletepopup" runat="server" visible="false">
                    <div class="overlayContent" style="color: #000; background-color: #FFF; width: 50%; border: 1px solid #ddd; max-height: 500px; overflow: hidden">
                        <div class="dialog-header">
                            <div>

                                <b style="font-size: 14px">Some records have a link to an external Calendar.</b>
                            </div>
                            <br>
                            <div><b style="font-size: 14px; padding-left: 10px;">How would you like the deletion command to be carried out:</b> </div>
                        </div>
                        <br>
                        <div class="dialog-body" style="width: 100%; padding-left: 10px;">
                            <div>

                                <asp:RadioButtonList ID="rblDeleteType" runat="server">
                                    <asp:ListItem Text="Delete the entry only from BizAutomation" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Delete the entry from both BizAutomation & your external calendar" Value="1" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>

                        <div style="padding-left: 240px; padding-bottom: 10px;">
                            <asp:Button ID="btnActivityDelete" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Execute Command & Close" OnClick="btnActivityDelete_Click" />
                        </div>
                    </div>
                </div>

                <asp:UpdatePanel ID="UpdatePanelHiddenField" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="col-md-12" style="padding-left: 0px; margin-bottom: 10px;">
                    <asp:HyperLink CssClass="btn btnPrime" Target="_blank" NavigateUrl="~/opportunity/frmMassSalesFulfillment.aspx?ViewId=1" ID="hypItemsToPick" runat="server">Items to Pick<i>(<asp:Label ID="lblItemstoPick" runat="server" Text="0"></asp:Label>)</i></asp:HyperLink>
                    <asp:HyperLink CssClass="btn btnPrime" Target="_blank" NavigateUrl="~/opportunity/frmMassSalesFulfillment.aspx?ViewId=2" ID="hypItemsPackShip" runat="server">Items to Pack & Ship<i>(<asp:Label ID="lblItemsPackShip" runat="server" Text="0"></asp:Label>)</i></asp:HyperLink>
                    <asp:HyperLink CssClass="btn btnPrime" Target="_blank" NavigateUrl="~/opportunity/frmMassSalesFulfillment.aspx?ViewId=3" ID="hypItemToInvoice" runat="server">Items to Invoice <i>(<asp:Label ID="lblItemToInvoice" runat="server" Text="0"></asp:Label>)</i></asp:HyperLink>
                    <asp:HyperLink CssClass="btn btnPrime" Target="_blank" NavigateUrl="~/opportunity/frmMassSalesFulfillment.aspx?ViewId=5" ID="hypSalesOrderToClose" runat="server">Sales Orders to Close <i>(<asp:Label ID="lblSalesOrderToClose" runat="server" Text="0"></asp:Label>)</i></asp:HyperLink>
                    <asp:HyperLink CssClass="btn btnWarn" Target="_blank" NavigateUrl="~/opportunity/frmMassPurchaseFulfillment.aspx?ViewId=2" ID="hypItemsToPutAway" runat="server">Items to Put-Away <i>(<asp:Label ID="lblItemsToPutAway" runat="server" Text="0"></asp:Label>)</i></asp:HyperLink>
                    <asp:HyperLink CssClass="btn btnWarn" Target="_blank" NavigateUrl="~/opportunity/frmMassPurchaseFulfillment.aspx?ViewId=3" ID="hypItemsToBill" runat="server">Items to Bill <i>(<asp:Label ID="lblItemsToBill" runat="server" Text="0"></asp:Label>)</i></asp:HyperLink>
                    <asp:HyperLink CssClass="btn btnWarn" Target="_blank" NavigateUrl="~/opportunity/frmMassPurchaseFulfillment.aspx?ViewId=4" ID="hypPOToClose" runat="server">POs to Close <i>(<asp:Label ID="lblPOToClose" runat="server" Text="0"></asp:Label>)</i></asp:HyperLink>
                    <asp:HyperLink CssClass="btn btnSuccess" ID="hypBOMToPick" runat="server">BOMs to Pick <i>(<asp:Label ID="lblBOMToPick" runat="server" Text="0"></asp:Label>)</i></asp:HyperLink>
                </div>
                <%--DataKeyNames="ID,CaseId,CaseTimeId,CaseExpId,type,itemid,numContactID,numCreatedBy,numTerId"--%>
                <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                    CssClass="table table-bordered table-striped" Width="100%">
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>
    <asp:Button ID="btnGo" runat="server" Style="display: none" EnableViewState="false" />
    <asp:TextBox ID="txtSortColumnActionItems" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtSortOrderActionItems" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtSortColumnOppPro" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtSortOrderOppPro" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtSortColumnCases" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtSortOrderCases" runat="server" Style="display: none">
    </asp:TextBox>



</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    <div class="radLists">
        <span style="margin-right: 100px;">Activities</span>
        &nbsp;<a href="#" onclick="return OpenHelpPopUp('common/frmticklerdisplay.aspx')"><label class="badge bg-yellow">?</label></a>
        <asp:Button ID="btnPreviousWeek" runat="server" CssClass="btn btn-xs btn-previous-week" Text="Previous Week" OnClick="btnPreviousWeek_Click"></asp:Button>
        <asp:Button ID="btnCurrentWeek" runat="server" CssClass="btn btn-xs btn-current-week" Text="Current Week" OnClick="btnCurrentWeek_Click"></asp:Button>
        <asp:Button ID="btnNextWeek" runat="server" CssClass="btn btn-xs btn-next-week" Text="Next Week" OnClick="btnNextWeek_Click"></asp:Button>
        &nbsp;
        &nbsp;
        &nbsp;
        <asp:RadioButton ID="radOpen" GroupName="rad" AutoPostBack="true" runat="server" Text="Open" />
        <asp:RadioButton ID="radClosed" GroupName="rad" AutoPostBack="true" runat="server" Text="Closed" />
        <asp:RadioButton ID="radAll" GroupName="rad" AutoPostBack="true" runat="server" Text="All" />
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <a href="#" onclick="return OpenFilterSetting()" class="btn btn-xs btn-default" style="display: none">
        <i class="fa fa-gear"></i>&nbsp;<b style="vertical-align: middle">List Filter</b>
    </a>
    <a href="#" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
    <label id="lblTotalPageSize" style="display: none"><%=Session("PagingRows").ToString() %></label>
    <webdiyer:AspNetPager ID="bizPager" Style="float: left;" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        ShowPageIndexBox="Never"
        CustomInfoHTML="Total Records : <span class='lblTotalPageRecords'>%RecordCount%</span>"
        CustomInfoClass="bizpagercustominfo" ShowPageIndex="True" ShowFirstLast="False">
    </webdiyer:AspNetPager>
    <%-- CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "--%>
    <div id="divGridConfiguration" runat="server" style="display: inline">
        <a href="#" onclick="return OpenSetting()" title="Show Grid Settings." class="btn-box-tool"><i class="fa fa-2x fa-gear"></i></a>
    </div>
    <asp:HyperLink ID="hplEmpAvaliability" runat="server" NavigateUrl="#" CssClass="btn btn-xs btn-default">Availability</asp:HyperLink>

    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsDeals" runat="server" Style="display: none"></asp:TextBox>

    <asp:UpdatePanel ID="uplOpenContract" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:Button ID="btnOpenEmailContract" runat="server" Text="OpenEmailContrac" Style="display: none" />
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:HiddenField ID="hdnContractBalanceRemaining" runat="server" />
    <asp:HiddenField ID="hdnStartDateTime" runat="server" />
    <asp:HiddenField ID="hdnAssignToForContract" runat="server" />
    <asp:HiddenField ID="hdnAssignToForContractName" runat="server" />
    <asp:HiddenField ID="hdnEndDateTime" runat="server" />
    <asp:HiddenField ID="hdnActualTaskCompletionTime" runat="server" />
    <asp:HiddenField ID="hdnSelectedTaskTitle" runat="server" />

    <div id="divTaskLogWorkOrder" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width:1180px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Task Time Log</h4>
                </div>
                <div class="modal-body">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                            </div>
                            <div class="pull-right">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>Total Time:</label>
                                        <span id="spnTimeSpendWorkOrder" style="font-size: 14px;" class="badge bg-green"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="tblTaskTimeLogWorkOrder">
                                    <thead>
                                        <tr>
                                            <th style="max-width:200px;min-width:200px;white-space: nowrap">Employee</th>
                                            <th style="white-space: nowrap;max-width:112px;min-width:112px;">Action</th>
                                            <th style="width: 160px; white-space: nowrap">When</th>
                                            <th style="width: 110px; white-space: nowrap">Processed Qty</th>
                                            <th style="white-space: nowrap">Reason for Pause</th>
                                            <th style="width: 30%; white-space: nowrap">Notes</th>
                                            <th style="width: 65px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>
                                                <ul class="list-unstyled">
                                                    <li><input name="TimeEntryTypeWorkOrder" type="radio" value="1" checked="checked" onchange="TimeEntryTypeChangedWorkOrder();" /> Single-Day Task</li>
                                                    <li><input name="TimeEntryTypeWorkOrder" type="radio" value="2" onchange="TimeEntryTypeChangedWorkOrder();" /> Multi-Day Task</li>
                                                </ul>
                                            </td>
                                            <td colspan="5" class="tdSingleDay">
                                                <ul class="list-inline">
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>Date</label>
                                                            <telerik:RadDatePicker ID="rdpTaskStartDateSingleWorkOrder" runat="server" DateInput-DisplayDateFormat="MM/dd/yyyy" Width="105" ShowPopupOnFocus="true" DateInput-CssClass="form-control" DatePopupButton-HoverImageUrl="../Images/calendar25.png" DatePopupButton-ImageUrl="../Images/calendar25.png"></telerik:RadDatePicker>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>Start Time</label>
                                                            <telerik:RadTimePicker ID="rtpStartTimeWorkOrder" runat="server" 
                                                                DateInput-CssClass="form-control" 
                                                                TimeView-Columns="8" 
                                                                EnableScreenBoundaryDetection="false" 
                                                                PopupDirection="BottomLeft" 
                                                                ShowPopupOnFocus="true"
                                                                TimeView-StartTime="00:00:00"
                                                                TimeView-Interval="00:15:00"
                                                                TimeView-EndTime="23:59:59"
                                                                ></telerik:RadTimePicker>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>End Time</label>
                                                            <telerik:RadTimePicker ID="rtpEndTimeWorkOrder" runat="server" 
                                                                DateInput-CssClass="form-control" 
                                                                TimeView-Columns="8" 
                                                                EnableScreenBoundaryDetection="false" 
                                                                PopupDirection="BottomLeft" 
                                                                ShowPopupOnFocus="true"
                                                                TimeView-StartTime="00:00:00"
                                                                TimeView-Interval="00:15:00"
                                                                TimeView-EndTime="23:59:59"
                                                                ></telerik:RadTimePicker>
                                                        </div>
                                                        
                                                    </li>
                                                </ul>
                                            </td>
                                            <td class="tdMultiDayWorkOrder" style="vertical-align: top;display:none">
                                                <select id="ddlActionWorkOrder" class="form-control" tabindex="50" style="width: 95px;" onchange="return TimeEntryActionChangeWorkOrder(this)">
                                                    <option value="1">Start</option>
                                                    <option value="2">Pause</option>
                                                    <option value="3">Resume</option>
                                                    <option value="4">Finish</option>
                                                </select>
                                            </td>
                                            <td class="tdMultiDayWorkOrder" style="vertical-align: top; white-space: nowrap;display:none">
                                                <ul class="list-inline">
                                                    <li>
                                                        <telerik:RadDatePicker ID="rdpTaskStartDateWorkOrder" TabIndex="51" runat="server" DateInput-DisplayDateFormat="MM/dd/yyyy" Width="105" DateInput-CssClass="form-control" DatePopupButton-HoverImageUrl="../Images/calendar25.png" DatePopupButton-ImageUrl="../Images/calendar25.png"></telerik:RadDatePicker>
                                                    </li>
                                                    <li style="vertical-align: top">
                                                        <telerik:RadNumericTextBox ID="txtStartFromHoursWorkOrder" TabIndex="52" CssClass="form-control" runat="server" MaxValue="12" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Hours" Width="58"></telerik:RadNumericTextBox></li>
                                                    <li style="vertical-align: top">
                                                        <telerik:RadNumericTextBox ID="txtStartFromMinutesWorkOrder" TabIndex="53" CssClass="form-control" runat="server" MaxValue="59" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Minutes" Width="68"></telerik:RadNumericTextBox></li>
                                                    <li style="vertical-align: top">
                                                        <asp:RadioButtonList ID="rblStartFromTimeWorkOrder" runat="server" TabIndex="54" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="AM" Value="1" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="PM" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td class="tdMultiDayWorkOrder" style="vertical-align: top;display:none">
                                                <input type="text" class="form-control" id="txtAddProcessedUnitsWorkOrder" tabindex="55" style="display: none" />
                                            </td>
                                            <td class="tdMultiDayWorkOrder" style="vertical-align: top;display:none">
                                                <asp:DropDownList ID="ddlAddReasonForPauseWorkOrder" runat="server" TabIndex="66" CssClass="form-control" Style="display: none">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="tdMultiDayWorkOrder" style="vertical-align: top;display:none">
                                                <textarea class="form-control" rows="3" id="txtAddNotesWorkOrder" tabindex="67" style="display: none"></textarea>
                                            </td>
                                            <td style="vertical-align: top; white-space: nowrap">
                                                <input type="button" class="btn btn-sm btn-success" id="btnAddTimeEntryWorkOrder" tabindex="68" onclick="return AddTimeEntryWorkOrder(0)" value="Finish Task" />&nbsp;
                                                <input type="button" class="btn btn-sm btn-default" id="btnCancelTimeEntryWorkOrder" tabindex="69" onclick="return CancelTimeEntryWorkOrder();" value="Cancel" style="display:none;">
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnTaskLogTaskIDWorkOrder" />
                    <button type="button" class="btn btn-success btn-start-again">Start Again & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="divReasonForPauseWorkOrder" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reason for pause</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label>Reason for Pause</label>
                                <asp:DropDownList ID="ddlPauseReasonWorkOrder" CssClass="form-control" runat="server">
                                    <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label>Notes</label>
                                <textarea id="txtPauseNotesWorkOrder" class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnPausedTaskIDWorkOrder" />
                    <input type="hidden" id="hdnProcessedQtyWorkOrder" />
                    <button type="button" class="btn btn-primary" onclick="return TaskPausedWorkOrder();">Save & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div id="divTaskLogProject" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width:1080px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Task Time Log</h4>
                </div>
                <div class="modal-body">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                            </div>
                            <div class="pull-right">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>Total Time:</label>
                                        <span id="spnTimeSpendProject" style="font-size: 14px;" class="badge bg-green"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="tblTaskTimeLogProject">
                                    <thead>
                                        <tr>
                                            <th style="max-width:200px;min-width:200px;white-space: nowrap">Employee</th>
                                            <th style="white-space: nowrap;max-width:112px;min-width:112px;">Action</th>
                                            <th style="width: 160px; white-space: nowrap">When</th>
                                            <th style="white-space: nowrap">Reason for Pause</th>
                                            <th style="width: 30%; white-space: nowrap">Notes</th>
                                            <th style="width: 65px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>
                                                <ul class="list-unstyled">
                                                    <li><input name="TimeEntryTypeProject" type="radio" value="1" checked="checked" onchange="TimeEntryTypeChangedProject();" /> Single-Day Task</li>
                                                    <li><input name="TimeEntryTypeProject" type="radio" value="2" onchange="TimeEntryTypeChangedProject();" /> Multi-Day Task</li>
                                                </ul>
                                            </td>
                                            <td colspan="4" class="tdSingleDay">
                                                <ul class="list-inline">
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>Date</label>
                                                            <telerik:RadDatePicker ID="rdpTaskStartDateSingleProject" runat="server" DateInput-DisplayDateFormat="MM/dd/yyyy" Width="105" ShowPopupOnFocus="true" DateInput-CssClass="form-control" DatePopupButton-HoverImageUrl="../Images/calendar25.png" DatePopupButton-ImageUrl="../Images/calendar25.png"></telerik:RadDatePicker>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>Start Time</label>
                                                            <telerik:RadTimePicker ID="rtpStartTimeProject" runat="server" 
                                                                DateInput-CssClass="form-control" 
                                                                TimeView-Columns="8" 
                                                                EnableScreenBoundaryDetection="false" 
                                                                PopupDirection="BottomLeft" 
                                                                ShowPopupOnFocus="true"
                                                                TimeView-StartTime="00:00:00"
                                                                TimeView-Interval="00:15:00"
                                                                TimeView-EndTime="23:59:59"
                                                                ></telerik:RadTimePicker>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>End Time</label>
                                                            <telerik:RadTimePicker ID="rtpEndTimeProject" runat="server" 
                                                                DateInput-CssClass="form-control" 
                                                                TimeView-Columns="8" 
                                                                EnableScreenBoundaryDetection="false" 
                                                                PopupDirection="BottomLeft" 
                                                                ShowPopupOnFocus="true"
                                                                TimeView-StartTime="00:00:00"
                                                                TimeView-Interval="00:15:00"
                                                                TimeView-EndTime="23:59:59"
                                                                ></telerik:RadTimePicker>
                                                        </div>
                                                        
                                                    </li>
                                                </ul>
                                            </td>
                                            <td class="tdMultiDayProject" style="vertical-align:top;display:none">
                                                <select id="ddlActionProject" class="form-control" tabindex="50" style="width: 95px;" onchange="return TimeEntryActionChangeProject(this)">
                                                    <option value="1">Start</option>
                                                    <option value="2">Pause</option>
                                                    <option value="3">Resume</option>
                                                    <option value="4">Finish</option>
                                                </select>
                                            </td>
                                            <td class="tdMultiDayProject" style="vertical-align: top; white-space: nowrap;display:none">
                                                <ul class="list-inline">
                                                    <li>
                                                        <telerik:RadDatePicker ID="rdpTaskStartDateProject" TabIndex="51" runat="server" DateInput-DisplayDateFormat="MM/dd/yyyy" Width="105" DateInput-CssClass="form-control" DatePopupButton-HoverImageUrl="../Images/calendar25.png" DatePopupButton-ImageUrl="../Images/calendar25.png"></telerik:RadDatePicker>
                                                    </li>
                                                    <li style="vertical-align: top">
                                                        <telerik:RadNumericTextBox ID="txtStartFromHoursProject" TabIndex="52" CssClass="form-control" runat="server" MaxValue="12" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Hours" Width="58"></telerik:RadNumericTextBox></li>
                                                    <li style="vertical-align: top">
                                                        <telerik:RadNumericTextBox ID="txtStartFromMinutesProject" TabIndex="53" CssClass="form-control" runat="server" MaxValue="59" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Minutes" Width="68"></telerik:RadNumericTextBox></li>
                                                    <li style="vertical-align: top">
                                                        <asp:RadioButtonList ID="rblStartFromTimeProject" runat="server" TabIndex="54" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="AM" Value="1" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="PM" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td class="tdMultiDayProject" style="vertical-align: top;display:none">
                                                <asp:DropDownList ID="ddlAddReasonForPauseProject" runat="server" TabIndex="66" CssClass="form-control" Style="display: none">
                                                </asp:DropDownList>
                                            </td>
                                            <td  class="tdMultiDayProject" style="vertical-align: top;display:none">
                                                <textarea class="form-control" rows="3" id="txtAddNotesProject" tabindex="67" style="display: none"></textarea>
                                            </td>
                                            <td style="vertical-align: top; white-space: nowrap">
                                                <input type="button" class="btn btn-sm btn-success" id="btnAddTimeEntryProject" tabindex="68" onclick="return AddTimeEntryProject(0)" value="Finish Task" />&nbsp;
                                                <input type="button" class="btn btn-sm btn-default" id="btnCancelTimeEntryProject" tabindex="69" onclick="return CancelTimeEntryProject();" value="Cancel" style="display:none;">
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnTaskLogTaskIDProject" />
                    <button type="button" class="btn btn-success btn-start-again">Start Again & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="divReasonForPauseProject" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Reason for pause</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label>Reason for Pause</label>
                                <asp:DropDownList ID="ddlPauseReasonProject" CssClass="form-control" runat="server">
                                    <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label>Notes</label>
                                <textarea id="txtPauseNotesProject" class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnPausedTaskIDProject" />
                    <button type="button" class="btn btn-primary" onclick="return TaskPausedProject();">Save & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
