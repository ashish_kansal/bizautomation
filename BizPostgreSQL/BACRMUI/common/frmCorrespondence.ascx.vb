﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports System.Text.RegularExpressions


Namespace BACRM.UserInterface.Common
    Partial Public Class frmCorrespondence
        Inherits BACRMUserControl
        Dim objContacts As New ContactIP
        Dim lngCntID As Long
        Dim strColumn As String
        Public Mode As Short 'Will be passed from page
        Public lngRecordID As Long 'Will be passed from page
        Public bPaging As Boolean = True 'Will be passed from page
        Public bitOpenCommu As Boolean = False 'Will be passed from page
        Public objCommon As CCommon

        Public m_aryRightsForActItem() As Integer
        Public EditPermission As Integer
        Public Property CorrespondenceModule As Long
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                m_aryRightsForActItem = GetUserRightsForPage_Other(1, 2)
                EditPermission = m_aryRightsForActItem(RIGHTSTYPE.UPDATE)
                If Not IsPostBack Then
                    m_aryRightsForActItem = GetUserRightsForPage_Other(1, 2)
                    EditPermission = m_aryRightsForActItem(RIGHTSTYPE.UPDATE)
                    Calendar1.SelectedDate = Session("StartDate")
                    Calendar2.SelectedDate = Session("EndDate")
                    BindTypes()
                End If

                If CCommon.ToBool(hdnPaging.Value) = False Then
                    divCorrespondence.Visible = False
                    bizPager.Visible = False
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Public Sub getCorrespondance(Optional ByVal isLocalCall As Boolean = False)
            Try
                If Not isLocalCall Then
                    hdnMode.Value = Mode
                    hdnRecordID.Value = lngRecordID
                    hdnPaging.Value = bPaging
                    hdnOpenCommu.Value = bitOpenCommu
                End If


                If Calendar1.SelectedDate = "" Then
                    Calendar1.SelectedDate = Session("StartDate")
                End If
                If Calendar2.SelectedDate = "" Then
                    Calendar2.SelectedDate = Session("EndDate")
                End If
                'Check User Rights
                Dim m_aryRightsForCorrespondense() As Integer

                If CorrespondenceModule = 2 Then 'Leads
                    m_aryRightsForCorrespondense = GetUserRightsForPage_Other(2, 135)
                ElseIf CorrespondenceModule = 3 Then 'Prospects
                    m_aryRightsForCorrespondense = GetUserRightsForPage_Other(3, 135)
                ElseIf CorrespondenceModule = 4 Then 'Accounts
                    m_aryRightsForCorrespondense = GetUserRightsForPage_Other(4, 135)
                ElseIf CorrespondenceModule = 11 Then 'Contacts
                    m_aryRightsForCorrespondense = GetUserRightsForPage_Other(11, 135)
                ElseIf CorrespondenceModule = 7 Then 'cases
                    m_aryRightsForCorrespondense = GetUserRightsForPage_Other(7, 135)
                ElseIf CorrespondenceModule = 12 Then 'Projects
                    m_aryRightsForCorrespondense = GetUserRightsForPage_Other(12, 135)
                ElseIf CorrespondenceModule = 10 Then 'Opportunity/Orders
                    m_aryRightsForCorrespondense = GetUserRightsForPage_Other(10, 135)
                Else
                    m_aryRightsForCorrespondense = GetUserRightsForPage_Other(13, 24) ' Changed by chintan Bug ID 2106
                End If


                If m_aryRightsForCorrespondense(RIGHTSTYPE.VIEW) = 0 Then
                    tblError.Visible = True
                    divMain.Visible = False
                    divCorrespondence.Visible = False
                    Exit Sub
                End If
                If m_aryRightsForCorrespondense(RIGHTSTYPE.DELETE) = 0 Then btnCorrDelete.Visible = False


                If objCommon Is Nothing Then objCommon = New CCommon
                Select Case CCommon.ToShort(hdnMode.Value)
                    Case 1, 2, 3, 4
                        objCommon.OppID = CCommon.ToLong(hdnRecordID.Value)
                        objCommon.charModule = "0"
                    Case 5
                        objCommon.ProID = CCommon.ToLong(hdnRecordID.Value)
                        objCommon.charModule = "P"
                    Case 6
                        objCommon.CaseID = CCommon.ToLong(hdnRecordID.Value)
                        objCommon.charModule = "S"
                    Case 7 'Based on DivisionID
                        objCommon.DivisionID = CCommon.ToLong(hdnRecordID.Value)
                        objCommon.charModule = "D"
                    Case 8 'Based on Contact ID
                        objCommon.ContactID = CCommon.ToLong(hdnRecordID.Value)
                        objCommon.charModule = "C"
                    Case 9 'From frmActionItemList.aspx - Date Filter is not required in this case
                        objCommon.DivisionID = CCommon.ToLong(hdnRecordID.Value)
                        objCommon.charModule = "D"
                End Select
                objCommon.GetCompanySpecificValues1()
                hdnFromEmail.Value = objContacts.Email
                Dim dttable As DataTable
                objContacts.FromDate = Calendar1.SelectedDate
                objContacts.ToDate = DateAdd(DateInterval.Day, 1, CDate(Calendar2.SelectedDate))
                objContacts.ContactID = objCommon.ContactID
                objContacts.DivisionID = IIf(CCommon.ToShort(hdnMode.Value) = 8, 0, objCommon.DivisionID)
                objContacts.MessageFrom = objContacts.Email
                objContacts.UserCntID = Session("UserContactID")
                'objContacts.SortOrder = ddlSrchCorr.SelectedValue
                objContacts.SortOrder = 0
                objContacts.DomainID = Session("DomainID")
                objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objContacts.RecordID = IIf(CCommon.ToShort(hdnMode.Value) = 7 Or CCommon.ToShort(hdnMode.Value) = 8 Or CCommon.ToShort(hdnMode.Value) = 9, 0, CCommon.ToLong(hdnRecordID.Value))
                objContacts.byteMode = CCommon.ToShort(hdnMode.Value)
                objContacts.bitOpenCommu = CCommon.ToBool(hdnOpenCommu.Value)

                If txtCurrentPageCorr1.Text.Trim = "" Then txtCurrentPageCorr1.Text = 1
                objContacts.CurrentPage = txtCurrentPageCorr1.Text.Trim()

                If Session("SortOrder") IsNot Nothing Then
                    objContacts.columnSortOrder = Session("SortOrder")
                Else : objContacts.columnSortOrder = "Desc"
                End If

                If CCommon.ToBool(hdnPaging.Value) = False Then
                    objContacts.PageSize = 4
                    objContacts.columnSortOrder = "Desc"
                Else
                    objContacts.PageSize = Session("PagingRows")
                End If
                objContacts.TotalRecords = 0
                If strColumn <> "" Then
                    objContacts.columnName = strColumn
                Else : objContacts.columnName = "bintCreatedOn"
                End If


                'If Session("Asc") = 1 Then
                '    objContacts.columnSortOrder = "Desc"
                'Else : objContacts.columnSortOrder = "Asc"
                'End If
                objContacts.Type = hdnSelectedTypes.Value
                dttable = objContacts.getCorres()
                bizPager.PageSize = Session("PagingRows")
                bizPager.CurrentPageIndex = txtCurrentPageCorr1.Text
                bizPager.RecordCount = objContacts.TotalRecords

                'If objContacts.TotalRecords = 0 Then
                '    tdCorr.Visible = False
                '    lblNoOfRecordsCorr.Text = objContacts.TotalRecords
                'Else
                '    tdCorr.Visible = True
                '    lblNoOfRecordsCorr.Text = String.Format("{0:#,###}", objContacts.TotalRecords)
                '    Dim strTotalPage As String()
                '    Dim decTotalPage As Decimal
                '    decTotalPage = lblNoOfRecordsCorr.Text / Session("PagingRows")
                '    decTotalPage = Math.Round(decTotalPage, 2)
                '    strTotalPage = CStr(decTotalPage).Split(".")
                '    If (lblNoOfRecordsCorr.Text Mod Session("PagingRows")) = 0 Then
                '        lblTotalCorr.Text = strTotalPage(0)
                '        txtCorrTotalPage.Text = strTotalPage(0)
                '    Else
                '        lblTotalCorr.Text = strTotalPage(0) + 1
                '        txtCorrTotalPage.Text = strTotalPage(0) + 1
                '    End If
                '    txtCorrTotalRecords.Text = lblNoOfRecordsCorr.Text
                'End If

                If CCommon.ToBool(hdnPaging.Value) = True Then
                    Dim strCorrTickler As String = ""
                    m_aryRightsForActItem = GetUserRightsForPage_Other(1, 2)
                    EditPermission = m_aryRightsForActItem(RIGHTSTYPE.UPDATE)
                    For Each dataRowItem As DataRow In dttable.Rows
                        If dataRowItem("type") <> "Email" Then
                            strCorrTickler = strCorrTickler + "," + String.Format("{0}:{1}:{2}:{3}", dataRowItem("numEmailHstrId").ToString(), dataRowItem("caseid").ToString(), dataRowItem("CaseTimeId").ToString(), dataRowItem("CaseExpId").ToString())
                            Dim allowInlineEdit As Boolean = False

                            If EditPermission = 1 AndAlso CCommon.ToLong(dataRowItem("numCreatedBy")) = CCommon.ToLong(HttpContext.Current.Session("UserContactID")) Then
                                allowInlineEdit = True
                            ElseIf EditPermission = 2 Then
                                Dim i As Integer
                                Dim dtTerritory As DataTable
                                dtTerritory = HttpContext.Current.Session("UserTerritory")
                                If CCommon.ToLong(dataRowItem("numOrgTerId")) = 0 Then
                                    allowInlineEdit = False
                                Else
                                    For i = 0 To dtTerritory.Rows.Count - 1
                                        If CCommon.ToLong(dataRowItem("numOrgTerId")) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                            allowInlineEdit = True
                                        End If
                                    Next
                                End If
                            ElseIf EditPermission = 3 Then
                                allowInlineEdit = True
                            End If
                            If allowInlineEdit = False Then
                                dataRowItem("InlineEdit") = ""
                            End If
                        End If
                    Next

                    If strCorrTickler.Length > 0 Then
                        'strCorrTickler = strCorrTickler.Remove(strCorrTickler.Length - 1, 1)
                        strCorrTickler = strCorrTickler.Remove(0, 1)
                        Session("strCorrTickler") = strCorrTickler
                    End If
                End If

                rptCorr.DataSource = dttable
                rptCorr.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub BindTypes()
            Try
                ddlCorrType.CssClass = "option-droup-multiSelection-Group form-control"
                ddlCorrType.ClientIDMode = ClientIDMode.Static
                ddlCorrType.Attributes("multiple") = "multiple"
                Dim dt As New DataTable()
                Dim dtTaskType As New DataTable()
                dtTaskType.Columns.Add("TaskTypeId")
                dtTaskType.Columns.Add("TaskTypeName")
                Dim dr As DataRow
                dr = dtTaskType.NewRow()
                dr("TaskTypeName") = "Action Items"
                dr("TaskTypeId") = "-2"
                dtTaskType.Rows.Add(dr)

                dt = dtTaskType
                Dim item As ListItem = New ListItem("-- All --", "0")
                ddlCorrType.Items.Add(item)
                ddlCorrType.AutoPostBack = False
                Dim dtTaskIndivType As DataTable
                Dim objCommon As New CCommon()
                objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
                objCommon.ListID = CCommon.ToLong(73)
                dtTaskIndivType = objCommon.GetMasterListItemsWithRights()
                For Each drRow As DataRow In dt.Rows
                    item = New ListItem(CCommon.ToString(drRow("TaskTypeName")), CCommon.ToString(drRow("TaskTypeId")))
                    If drRow("TaskTypeId") = -2 Then
                        item.Attributes("OptionGroup") = "Action Items"
                    Else
                        item.Attributes("OptionGroup") = "Others"
                    End If
                    If drRow("TaskTypeId") = -2 Then
                        For Each drActionItem As DataRow In dtTaskIndivType.Rows
                            item = New ListItem(CCommon.ToString(drActionItem("vcData")), CCommon.ToString(drActionItem("numListItemID")))
                            item.Attributes("OptionGroup") = "Action Items"
                            ddlCorrType.Items.Add(item)
                        Next
                    Else
                        ddlCorrType.Items.Add(item)
                    End If
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Function CheckUserExist(ByVal UserContactID As String, ByVal SelectedContactIds As String) As Boolean
            Try
                Dim strArr As String()
                Dim result As Boolean
                result = False
                strArr = SelectedContactIds.Split(",")
                For Each s As String In strArr
                    If UserContactID = s Then
                        result = True
                    End If
                Next
                Return result
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub rptCorr_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptCorr.ItemCommand
            Try
                If e.CommandName = "Sort" Then
                    strColumn = e.CommandArgument
                    SortData(strColumn)
                    getCorrespondance(True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub SortData(ByVal SortExpression As String)
            If Session("SortOrder") Is Nothing Then
                Session("SortOrder") = " ASC"
            Else
                If Session("SortOrder").ToString() = " ASC" Then
                    Session("SortOrder") = " DESC"
                Else
                    Session("SortOrder") = " ASC"
                End If
            End If
        End Sub

        Private Sub rptCorr_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCorr.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim chk As CheckBox
                    Dim lbl As Label
                    chk = e.Item.FindControl("chkADelete")
                    lbl = e.Item.FindControl("lblDelete")
                    'If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 1 Then
                    If lbl.Text.Split("~")(2) <> Session("UserContactID") Then chk.Visible = False
                    'End If

                    If CCommon.ToBool(hdnPaging.Value) = False Then
                        chk.Visible = False
                    End If

                    'If DataBinder.Eval(e.Item.DataItem, "type") <> "Email" Then
                    '    If DataBinder.Eval(e.Item.DataItem, "bitClosedflag") = 0 Then
                    '        CType(e.Item.FindControl("tr"), HtmlTableRow).Attributes.Add("style", "color:red;")
                    '    End If

                    'End If

                    If DataBinder.Eval(e.Item.DataItem, "type") = "Email In" Or DataBinder.Eval(e.Item.DataItem, "type") = "Email Out" Then
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "From")) Then
                            Dim pattern As String = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Dim matches As MatchCollection = Regex.Matches(DataBinder.Eval(e.Item.DataItem, "From"), pattern, RegexOptions.IgnorePatternWhitespace)

                            'Dim email As String = ""
                            Dim fromEmail As String = ""
                            Dim toEmail As String = ""
                            Dim str As String() = New [String](matches.Count - 1) {}
                            For i As Integer = 0 To matches.Count - 1
                                If i = 0 Then
                                    fromEmail = matches(i).Value
                                Else
                                    str(i) = matches(i).Value
                                End If
                            Next

                            CType(e.Item.FindControl("lblFrom"), Label).Text = fromEmail
                            toEmail = String.Join(",", str.Distinct).TrimStart(",")
                            CType(e.Item.FindControl("lblTo"), Label).Text = toEmail
                        End If
                    End If

                    If DataBinder.Eval(e.Item.DataItem, "bitHasAttachments") = 1 Then
                        CType(e.Item.FindControl("imgAttachment"), HtmlImage).Visible = True
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub btnCorrDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorrDelete.Click
            Try
                Dim chk As CheckBox
                Dim lbl As Label
                For Each r As RepeaterItem In rptCorr.Items
                    chk = r.FindControl("chkADelete")
                    If chk.Checked = True Then
                        lbl = r.FindControl("lblDelete")
                        If lbl.Text.Split("~")(1) = 1 Then
                            objContacts.EmailHstrID = lbl.Text.Split("~")(0)
                            objContacts.tinttype = 1
                        Else
                            objContacts.EmailHstrID = lbl.Text.Split("~")(0)
                            objContacts.tinttype = 2
                        End If
                        objContacts.DelCorrespondence()
                    End If
                Next
                getCorrespondance(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub btnCorresGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorresGo.Click
            Try
                getCorrespondance(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrentPageCorr1.Text = bizPager.CurrentPageIndex
                getCorrespondance(True)
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
    End Class
End Namespace
