﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Prospects
Imports Telerik.Web.UI

Public Class frmChangeOrganization
    Inherits BACRMPage

#Region "Page Events"

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                If CCommon.ToLong(GetQueryStringVal("numOppID")) > 0 Then
                    hdnOppID.Value = CCommon.ToLong(GetQueryStringVal("numOppID"))
                    Dim objOpportunity As New MOpportunity
                    objOpportunity.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOpportunity.OpportunityId = hdnOppID.Value
                    LoadAssociatedContacts(CCommon.ToLong(objOpportunity.GetFieldValue("numDivisionID")))
                ElseIf CCommon.ToLong(GetQueryStringVal("numProjectID")) > 0 Then
                    hdnProjectID.Value = CCommon.ToLong(GetQueryStringVal("numProjectID"))
                    Dim objProject As New Project
                    objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                    objProject.ProjectID = hdnProjectID.Value
                    LoadAssociatedContacts(CCommon.ToLong(objProject.GetFieldValue("numDivisionID")))
                ElseIf CCommon.ToLong(GetQueryStringVal("numCaseID")) > 0 Then
                    hdnCaseID.Value = CCommon.ToLong(GetQueryStringVal("numCaseID"))
                    Dim objCase As New CCases
                    objCase.DomainID = CCommon.ToLong(Session("DomainID"))
                    objCase.CaseID = objCase.CaseID
                    LoadAssociatedContacts(CCommon.ToLong(objCase.GetFieldValue("numDivisionID")))
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region
    
#Region "Private Methods"

    Private Sub LoadAssociatedContacts(ByVal divisionID As Long)
        Try
            Dim dr As DataRow
            Dim dt As New DataTable
            dt.Columns.Add("numDivisionID", GetType(System.Int64))
            dt.Columns.Add("numContactID", GetType(System.Int64))
            dt.Columns.Add("TargetCompany", GetType(System.String))
            dt.Columns.Add("ContactName", GetType(System.String))
            dt.Columns.Add("vcData", GetType(System.String))

            Dim objProspects As New CProspects
            objProspects.DivisionID = divisionID
            objProspects.bitAssociatedTo = 1
            Dim dsAssocations As DataSet = objProspects.GetAssociationTo()

            If Not dsAssocations Is Nothing AndAlso dsAssocations.Tables.Count > 0 AndAlso dsAssocations.Tables(0).Rows.Count > 0 Then
                For Each drassociation As DataRow In dsAssocations.Tables(0).Rows
                    dr = dt.NewRow()
                    dr("numDivisionID") = CCommon.ToLong(drassociation("numDivisionID"))
                    dr("numContactID") = CCommon.ToLong(drassociation("numContactID"))
                    dr("TargetCompany") = CCommon.ToString(drassociation("TargetCompany"))
                    dr("ContactName") = CCommon.ToString(drassociation("ContactName"))
                    dr("vcData") = CCommon.ToString(drassociation("vcData"))
                    dt.Rows.Add(dr)
                Next
            End If

            Dim objType As Object
            Dim dtAssContact As DataTable

            If CCommon.ToLong(hdnCaseID.Value) > 0 Then
                objType = New CaseIP
                objType.DomainID = Session("DomainID")
                objType.CaseID = CCommon.ToLong(hdnCaseID.Value)
                dtAssContact = objType.AssCntsByCaseID
            ElseIf CCommon.ToLong(hdnProjectID.Value) > 0 Then
                objType = New ProjectIP
                objType.DomainID = Session("DomainID")
                objType.ProjectID = CCommon.ToLong(hdnProjectID.Value)
                dtAssContact = objType.AssCntsByProId
            ElseIf CCommon.ToLong(hdnOppID.Value) > 0 Then
                objType = New OppotunitiesIP
                objType.DomainID = Session("DomainID")
                objType.OpportunityId = CCommon.ToLong(hdnOppID.Value)
                dtAssContact = objType.AssociatedbyOppID
            End If

            If Not dtAssContact Is Nothing AndAlso dtAssContact.Rows.Count > 0 Then
                For Each drassociation As DataRow In dtAssContact.Rows
                    dr = dt.NewRow()
                    dr("numDivisionID") = CCommon.ToLong(drassociation("numDivisionID"))
                    dr("numContactID") = CCommon.ToLong(drassociation("numContactId"))
                    dr("TargetCompany") = CCommon.ToString(drassociation("Company"))
                    dr("ContactName") = CCommon.ToString(drassociation("Name"))
                    dr("vcData") = CCommon.ToString(drassociation("ContactRole"))
                    dt.Rows.Add(dr)
                Next
            End If

            rptAssociatedContacts.DataSource = dt
            rptAssociatedContacts.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        lblError.Text = ex
        divError.Style.Remove("display")
    End Sub

#End Region

#Region "Events Handler"


    Protected Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        Try
            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                radcmbContact.Items.Clear()

                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = radCmbCompany.SelectedValue
                    radcmbContact.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    radcmbContact.DataTextField = "Name"
                    radcmbContact.DataValueField = "numcontactId"
                    radcmbContact.DataBind()
                End With
                radcmbContact.Items.Insert(0, New RadComboBoxItem("---Select One---", "0"))
                If radcmbContact.Items.Count >= 2 Then
                    radcmbContact.Items(1).Selected = True
                End If
            Else
                radCmbCompany.Items.Clear()
                radcmbContact.Items.Clear()
                rptAssociatedContacts.DataSource = Nothing
                rptAssociatedContacts.DataBind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        Try
            Dim divisionID As Long = 0
            Dim contactID As Long = 0
            Dim isValidData As Boolean = False

            If rbCustomer.Checked AndAlso CCommon.ToLong(radCmbCompany.SelectedValue) > 0 AndAlso CCommon.ToLong(radcmbContact.SelectedValue) > 0 Then
                divisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                contactID = CCommon.ToLong(radcmbContact.SelectedValue)
                isValidData = True
            Else
                For Each item As RepeaterItem In rptAssociatedContacts.Items
                    If CCommon.ToBool(DirectCast(item.FindControl("rbAssociatedContact"), RadioButton).Checked) AndAlso CCommon.ToLong(DirectCast(item.FindControl("hdnDivisionID"), HiddenField).Value) > 0 AndAlso CCommon.ToLong(DirectCast(item.FindControl("hdnContactID"), HiddenField).Value) > 0 Then
                        divisionID = CCommon.ToLong(DirectCast(item.FindControl("hdnDivisionID"), HiddenField).Value)
                        contactID = CCommon.ToLong(DirectCast(item.FindControl("hdnContactID"), HiddenField).Value)
                        isValidData = True
                    End If
                Next
            End If

            If isValidData Then
                If CCommon.ToLong(hdnOppID.Value) > 0 Then
                    Dim objOpportunity As New MOpportunity
                    objOpportunity.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOpportunity.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objOpportunity.OpportunityId = CCommon.ToLong(hdnOppID.Value)
                    objOpportunity.ChangeOrganization(divisionID, contactID)
                ElseIf CCommon.ToLong(hdnProjectID.Value) > 0 Then
                    Dim objProject As New Project
                    objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                    objProject.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objProject.ProjectID = CCommon.ToLong(hdnProjectID.Value)
                    objProject.ChangeOrganization(divisionID, contactID)
                ElseIf CCommon.ToLong(hdnCaseID.Value) > 0 Then
                    Dim objCase As New CCases
                    objCase.DomainID = CCommon.ToLong(Session("DomainID"))
                    objCase.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objCase.CaseID = CCommon.ToLong(hdnCaseID.Value)
                    objCase.ChangeOrganization(divisionID, contactID)
                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "DataUpdated", "RefereshAndClose();", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidData", "alert('Select customer and contact.')", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region

End Class