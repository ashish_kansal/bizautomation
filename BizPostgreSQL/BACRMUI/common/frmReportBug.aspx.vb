﻿Imports System.Net
Imports System.Text
Imports System.IO
Imports BACRM.BusinessLogic.Common

Partial Public Class frmReportBug
    Inherits BACRMPage
    Private Const strBugEmail As String = "Bug@Bizautomation.com"
    Dim Mode As Short
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblEmail.Text = strBugEmail
        Mode = CCommon.ToShort(GetQueryStringVal("Mode"))
        If CCommon.ToLong(Session("DomainID")) = 0 Then
            litMessage.Text = "You need to be Logged-In to submit a bug."
            btnSubmit.Visible = False
        End If

        If Not IsPostBack Then
            If Mode = 1 Then
                lblTitle.Text = "Talk to us about BizAutomation"
                trFeedback.Visible = True
                trSummary.Visible = False
                lblDescription.Text = "Tell us your feedback"
                txtDesc.Text = "Customer Feedback"
                txtComments.Text = ""
                trAttachment.Visible = False
                tdAlternateMethod.Visible = False
            Else
                lblTitle.Text = "Report a bug"
                trAttachment.Visible = False
            End If

            Dim strBrowserConfiguration As New StringBuilder
            strBrowserConfiguration.Append("Browser : " & CCommon.ToString(Request.Browser.Type))
            strBrowserConfiguration.Append(Environment.NewLine)
            strBrowserConfiguration.Append("Browser Agent : " & CCommon.ToString(Request.UserAgent & Request.UserHostAddress))
            txtComments.Text = txtComments.Text.Replace("##", strBrowserConfiguration.ToString)
        End If
    End Sub
    'insert_bug.aspx?username=YOURUSERNAME
    '&password=YOURPASSWORD
    '&short_desc=YOUR+SHORT_DESC
    '&comment=YOUR+COMMENT&projectid=YOUR_PJID
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try

       

            Dim strComment As New System.Text.StringBuilder
            strComment.Append("------------------------------------------" & Environment.NewLine)
            strComment.Append("Domain: " & HttpUtility.UrlEncode(CCommon.ToString(Session("DomainName"))) & Environment.NewLine)
            strComment.Append("User: " & HttpUtility.UrlEncode(CCommon.ToString(Session("ContactName"))) & Environment.NewLine)
            strComment.Append("Email: " & CCommon.ToString(Session("UserEmail")) & Environment.NewLine)
            strComment.Append("Organization: " & HttpUtility.UrlEncode(CCommon.ToString(Session("CompanyName"))) & Environment.NewLine)
            strComment.Append("Contact ID: " & CCommon.ToString(Session("UserContactID")) & Environment.NewLine)
            strComment.Append("Division ID: " & CCommon.ToString(Session("UserDivisionID")) & Environment.NewLine)
            strComment.Append("------------------------------------------" & Environment.NewLine)
            If Mode = 1 Then
                strComment.Append("Feedback: " & Environment.NewLine)
            Else
                strComment.Append("Steps to reproduce bug: " & Environment.NewLine)
            End If

            strComment.Append("------------------------------------------" & Environment.NewLine)
            AddBug(txtDesc.Text.Trim(), HttpUtility.UrlEncode(strComment.ToString()) & HttpUtility.UrlEncode(txtComments.Text.Trim()))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            litMessage.Text = "Unable to submit bug to Bug Tracking System! please submit bug to " & strBugEmail & "."
            'Response.Write(ex)
        End Try
    End Sub
    Private Sub AddBug(ByVal shortDesc As String, ByVal Comment As String)
        Try
            Dim url As String
            Dim Parameter As String = [String].Format("username={0}&password={1}&short_desc={2}&comment={3}&projectid={4}&$CATEGORY$={5}", "customer1", "test", shortDesc, Comment, "1", IIf(Mode = 1, "9", "5"))
            url = ConfigurationManager.AppSettings("BugTrackerURL").ToString() + "insert_bug.aspx?" + Parameter
            Dim req As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
            Dim proxy As String = Nothing
            'If fupload.HasFile = True Then
            '    Dim stream As System.IO.Stream
            '    ' Dim filebytes As System.Byte() = New Byte() {fupload.PostedFile.ContentLength - 1}
            '    Dim filebytes As [Byte]() = New [Byte](fupload.PostedFile.ContentLength - 1) {}
            '    fupload.PostedFile.InputStream.Read(filebytes, 0, fupload.PostedFile.ContentLength - 1)

            '    'Dim intByteCount As Integer = stream.Read(filebytes, 0, CCommon.ToInteger(stream.Length))
            '    Dim fileContent As String = Convert.ToBase64String(filebytes)
            '    ConvertToBase(fileContent)
            '    stream = New System.IO.MemoryStream(filebytes)
            '    ' stream.Close()
            '    'Dim image As System.Drawing.Image = System.Drawing.Image.FromStream(stream)
            '    Parameter = Parameter & "&attachment=" + fileContent + " &attachment_content_type=" + fupload.PostedFile.ContentType + "&attachment_filename=" + fupload.PostedFile.FileName + "&attachment_desc"
            'End If

            Dim buffer As Byte() = Encoding.UTF8.GetBytes(Parameter)

            req.Method = "POST"
            req.ContentType = "application/x-www-form-urlencoded"
            req.ContentLength = buffer.Length
            req.Proxy = New WebProxy(proxy, True)
            ' ignore for local addresses
            req.CookieContainer = New CookieContainer()
            ' enable cookies
            Dim reqst As Stream = req.GetRequestStream()
            ' add form data to request stream
            reqst.Write(buffer, 0, buffer.Length)
            reqst.Flush()
            reqst.Close()

            Dim res As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)

            Dim resst As Stream = res.GetResponseStream()
            Dim sr As New StreamReader(resst)
            Dim response As String = sr.ReadToEnd()
            If Mode = 1 Then
                litMessage.Text = "Thank you for taking the time to tell us about your experience. :)"
            Else
                litMessage.Text = IIf(response.Contains(":"), "Your bug is sucessfully submitted ! Bug ID " & response.Split(":")(1), "Unable to submit bug to Bug tracking system! please submit bug to " & strBugEmail & " .")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ConvertToBase(ByVal base64String As String)
        Dim imageBytes As Byte() = Convert.FromBase64String(base64String)
        Dim ms As New MemoryStream(imageBytes, 0, imageBytes.Length)

        ' Convert byte[] to Image
        ms.Write(imageBytes, 0, imageBytes.Length)
        Dim image__1 As System.Drawing.Image = System.Drawing.Image.FromStream(ms)
        image__1.Save(Server.MapPath("ss.jpeg"))
        ' Return image__1
    End Sub
End Class