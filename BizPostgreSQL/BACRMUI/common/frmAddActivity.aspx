﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmAddActivity.aspx.vb" Inherits="BACRM.UserInterface.Common.frmAddActivity" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
     <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/Common.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            if ($('#<%=chkFollowUp.ClientID%>').is(':checked') == true) {
                $('#tdStartTimeLabel').hide();
                $('#<%=tdStartTimeControl.ClientID%>').hide();
                 $('#<%=tdEndTimeLabel.ClientID%>').hide();
                 $('#tdEndTimeControl').hide();
             }
             else {
                 $('#tdStartTimeLabel').show();
                 $('#<%=tdStartTimeControl.ClientID%>').show();
                 $('#<%=tdEndTimeLabel.ClientID%>').show();
                 $('#tdEndTimeControl').show();
             }
            $('#<%=chkFollowUp.ClientID%>').on("change", function () {
                if ($(this).is(':checked') == true) {
                    $('#tdStartTimeLabel').hide();
                    $('#<%=tdStartTimeControl.ClientID%>').hide();
                    $('#<%=tdEndTimeLabel.ClientID%>').hide();
                    $('#tdEndTimeControl').hide();
                }
                else {
                    $('#tdStartTimeLabel').show();
                    $('#<%=tdStartTimeControl.ClientID%>').show();
                    $('#<%=tdEndTimeLabel.ClientID%>').show();
                    $('#tdEndTimeControl').show();
                }
            });
        });
        function OpenDropDown(intvalue) {
            if (intvalue == "1") {
                var combo = $find("<%= radcmbStartTime.ClientID%>");
                combo.showDropDown();
            } else {
                var combo = $find("<%= radcmbEndTime.ClientID%>");
                 combo.showDropDown();
             }
        }
        function ValidateData() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Company")
                return false;
            }
            if ($("#<%=ddlType.ClientID%>").val() == "0") {
                alert("Select Task Type");
                return false;
            }
        }
        function OpenActionItemWindow() {
            var url = '../ ActionItems / frmActionItemTemplate.aspx';
            if (parseInt($("[id$=ddlActionItemTemplate]").val()) > 0) {
                url = url + '?Mode=edit&ID=' + $("[id$=ddlActionItemTemplate]").val();
            }
            window.open(url, '', 'toolbar=no,titlebar=no,top=100,left=250,width=700,height=350,scrollbars=yes,resizable=yes')
        }
    </script>
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <asp:DropDownList ID="ddlActionItemTemplate" CssClass="form-control" Runat="server" AutoPostBack="true">
					<asp:ListItem Value="0">-- Select Action Item Template --</asp:ListItem>
				</asp:DropDownList>&nbsp;
							    
				<a id='editActionItem' href="javascript:OpenActionItemWindow" style="color:#180073" runat="server">Add</a>
            </div>
            <div class="pull-right">
                <asp:Button ID="btnSave" OnClientClick="return ValidateData();" CssClass="btn btn-primary" runat="server" Text="Save & Close" />
                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" OnClientClick="return Close();" Text="Close" />
               
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    New Action Item
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row">
        <div class="col-md-6">
            <div class="form-inline">
                <div class="col-md-4">
                    <label style="float: right;">
                        Organization&nbsp;&&nbsp;Contact<span style="color: red; font-size: 12px">*</span>
                    </label>
                </div>
                <div class="col-md-8">
                    <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" DropDownWidth="600px"
                        OnClientItemsRequested="OnClientItemsRequestedOrganization"
                        ShowMoreResultsBox="true"
                        Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True" ClientIDMode="Static">
                        <WebServiceSettings Path="~/common/Common.asmx" Method="GetCompanies" />
                    </telerik:RadComboBox>
                    <asp:DropDownList ID="ddlContact" CssClass="form-control" style="width:177px" runat="server" AutoPostBack="true">
                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <label class="col-md-4" style="width: 26%;">Assign To</label>
            <div class="col-md-8">
                <asp:DropDownList ID="ddlUserNames" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-6">
            <div class="form-inline">
                <div class="col-md-4">
                    <label style="float: right;">Due Date</label>
                </div>
                <div class="col-md-8">
                    
                <BizCalendar:Calendar ID="radDueDate" TabIndex="8" runat="server" ClientIDMode="AutoID" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-4"  style="width: 26%;">
                <label>Type&nbsp;<span style="color: red; font-size: 12px">*</span></label>
            </div>
            <div class="col-md-8">
                <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-6">
            <div class="col-md-4" style="text-align: right;">
                <label id="lblFollowUp" runat="server">
                    Follow-up Any Time</label>
            </div>
            <div class="col-md-8">
                <div class="form-inline">

                    <asp:CheckBox ID="chkFollowUp" style="float:left;margin-top: 4px;" runat="server" />
                    
                    <div id="tdStartTimeControl" runat="server" style="float:left">
                        <label style="float:left;    margin-top: 6px;">&nbsp;From&nbsp;</label>
                        <%--OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"--%>
                        <telerik:RadComboBox ID="radcmbStartTime"  style="width:60px;" runat="server" onmouseover="OpenDropDown('1')" >
                            <Items>
                                <telerik:RadComboBoxItem Selected="False" Value="23" Text="12:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="25" Text="12:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="24" Text="12:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="27" Text="12:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="1" Text="1:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="26" Text="1:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="2" Text="1:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="28" Text="1:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="3" Text="2:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="29" Text="2:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="4" Text="2:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="30" Text="2:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="5" Text="3:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="31" Text="3:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="6" Text="3:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="32" Text="3:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="7" Text="4:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="33" Text="4:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="8" Text="4:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="34" Text="4:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="9" Text="5:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="35" Text="5:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="10" Text="5:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="36" Text="5:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="11" Text="6:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="37" Text="6:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="12" Text="6:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="38" Text="6:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="13" Text="7:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="39" Text="7:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="14" Text="7:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="40" Text="7:45" />
                                        <telerik:RadComboBoxItem Selected="true" Value="15" Text="8:00" />
                                        <telerik:RadComboBoxItem Selected="false" Value="41" Text="8:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="16" Text="8:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="42" Text="8:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="17" Text="9:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="43" Text="9:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="18" Text="9:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="44" Text="9:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="19" Text="10:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="45" Text="10:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="20" Text="10:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="46" Text="10:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="21" Text="11:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="47" Text="11:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="22" Text="11:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="48" Text="11:45" />
                            </Items>
                        </telerik:RadComboBox>
                     &nbsp;
            <span class="bold_span" style="vertical-align: middle">AM</span><input id="chkAM" type="radio" value="0" name="AM" runat="server" style="vertical-align: middle" />
                        <span class="bold_span" style="vertical-align: middle">PM</span><input id="chkPM" type="radio" value="1" name="AM" runat="server" style="vertical-align: middle" />
                    </div>
                    <div id="tdEndTimeLabel" runat="server">
                        <label style="float:left;margin-top: 6px;">
                            <span class="bold_span">&nbsp;To&nbsp;</span>
                        </label>
                        <div id="tdEndTimeControl" style="float:left">
                            <telerik:RadComboBox ID="radcmbEndTime" style="width:60px" runat="server" onmouseover="OpenDropDown('2');">
                                <Items>
                                     <telerik:RadComboBoxItem Selected="False" Value="23" Text="12:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="25" Text="12:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="24" Text="12:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="27" Text="12:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="1" Text="1:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="26" Text="1:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="2" Text="1:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="28" Text="1:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="3" Text="2:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="29" Text="2:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="4" Text="2:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="30" Text="2:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="5" Text="3:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="31" Text="3:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="6" Text="3:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="32" Text="3:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="7" Text="4:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="33" Text="4:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="8" Text="4:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="34" Text="4:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="9" Text="5:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="35" Text="5:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="10" Text="5:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="36" Text="5:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="11" Text="6:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="37" Text="6:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="12" Text="6:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="38" Text="6:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="13" Text="7:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="39" Text="7:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="14" Text="7:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="40" Text="7:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="15" Text="8:00" />
                                        <telerik:RadComboBoxItem Selected="true" Value="41" Text="8:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="16" Text="8:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="42" Text="8:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="17" Text="9:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="43" Text="9:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="18" Text="9:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="44" Text="9:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="19" Text="10:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="45" Text="10:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="20" Text="10:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="46" Text="10:45" />
                                        <telerik:RadComboBoxItem Selected="False" Value="21" Text="11:00" />
                                        <telerik:RadComboBoxItem Selected="False" Value="47" Text="11:15" />
                                        <telerik:RadComboBoxItem Selected="False" Value="22" Text="11:30" />
                                        <telerik:RadComboBoxItem Selected="False" Value="48" Text="11:45" />
                                </Items>
                            </telerik:RadComboBox>
                     &nbsp;
                <span class="bold_span" style="vertical-align: middle">AM</span><input style="vertical-align: middle" id="chkEndAM" type="radio" value="0" name="EndAM" runat="server">
                            <span class="bold_span" style="vertical-align: middle">PM</span><input style="vertical-align: middle" id="chkEndPM" type="radio" value="1" name="EndAM" runat="server">
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="col-md-6">
            <label class="col-md-4"  style="width: 26%;">Activity & Priority</label>
            <div class="col-md-8">
                <div class="form-line">
                    <asp:DropDownList ID="ddlActivity" runat="server" CssClass="form-control" style="width: 50%;float: left;">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" style="width: 46%;margin-left: 10px;float: left;">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12" style="width: 97%;">
            <label class="col-md-2" style="text-align:right;width: 16.0%;">Comments</label>
            <div class="col-md-10">
                <asp:TextBox ID="txtcomments" TabIndex="6" runat="server" Height="180" CssClass="form-control" TextMode="MultiLine" Wrap="true" MaxLength="300"></asp:TextBox>
            </div>
        </div>
    </div>
</asp:Content>

