﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmCorrespondence.ascx.vb"
    Inherits="BACRM.UserInterface.Common.frmCorrespondence" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="~/include/Calandar.ascx" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<script src="../JavaScript/CommonInlineEdit.js"></script>
<script type="text/javascript">
    function SelectAll(headerCheckBox, ItemCheckboxClass) {
        $("." + ItemCheckboxClass + " input[type = 'checkbox']").each(function () {
            $(this).prop('checked', $('#' + headerCheckBox).is(':checked'))
        });
    }

    function openActionItem(a, b, c, d, e, f) {
        if (e == 'Email In' || e == 'Email Out') {
            window.open("../outlook/frmMailDtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numEmailHstrID=" + a, '', 'titlebar=no,top=100,left=250,width=850,height=550,scrollbars=yes,resizable=yes');
            return false;
        }
        else {
            window.open("../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accounts&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d);
            return false;
        }

    }
    if (arrFieldConfig == null) {
        var arrFieldConfig = new Array();
    }

    $(document).ready(function () {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(CorrUserControlLoaded);
    });

    function CorrUserControlLoaded() {
        $('[id$=ddlCorrType]').multiselect({
            enableClickableOptGroups: true,
            onSelectAll: function () {
                console.log("select-all-nonreq");
            },
            optionClass: function (element) {
                var value = $(element).attr("class");
                return value;
            },
            onChange: function (option, checked) {
                var selectedValues = "";

                // Get selected options.
                var selectedOptions = $('[id$=ddlCorrType] option:selected');

                if (selectedOptions != null && selectedOptions.length > 0) {
                    selectedOptions.each(function () {
                        if ($(this).val() != "0") {
                            selectedValues = selectedValues + (selectedValues.length > 0 ? "," : "") + $(this).val();
                        }
                    });
                }

                $("[id$=hdnSelectedTypes]").val(selectedValues);
            }
        });

        if ($("[id$=hdnSelectedTypes]").length > 0) {
            var dataarray = $("[id$=hdnSelectedTypes]").val().split(",");
            $('[id$=ddlCorrType]').val(dataarray);
            $('[id$=ddlCorrType]').multiselect("refresh");
        }
    }
</script>
<style>
    .pagn a {
        color: black !important;
    }

    .is td {
        background-color: White !important;
        border-top-width: 0 !important;
        border-right-width: 0 !important;
        border-left-width: 0 !important;
    }

    .ais td {
        background-color: #F2F0F0 !important;
        border-top-width: 0 !important;
        border-right-width: 0 !important;
        border-left-width: 0 !important;
    }

    .blueColorLabel {
        font-weight: 600;
        color: #079dbf;
        font-size: 17px;
    }

    .redColorLabel {
        font-weight: 600;
        color: #bf0707;
        font-size: 17px;
    }

    .greenColorLabel {
        font-weight: 600;
        color: #07bf07;
        font-size: 17px;
    }

    .tblNoBorderCorrespondence tr td {
        border: 0px !important;
        padding: 5px !important;
    }

    table.table.table-bordered.tblNoBorderCorrespondence td {
        font-size: 15px;
    }

    table.table.table-bordered.tblNoBorderCorrespondence.evenRow {
        background-color: #ececec;
    }

        table.table.table-bordered.tblNoBorderCorrespondence.evenRow td {
            background-color: #ececec;
        }
</style>
<div class="row" style="margin-right:0px; margin-left:0px">
    <div class="col-xs-12">
        <div class="pull-left" id="divCorrespondence" runat="server">
            <asp:HiddenField ID="hdnFromEmail" runat="server" />
    <div class="form-inline">
        <div class="form-group">
            <label>From</label>
            <BizCalendar:Calendar ID="Calendar1" runat="server" ClientIDMode="AutoID" />
        </div>
        <div class="form-group">
            <label>To</label>
            <BizCalendar:Calendar ID="Calendar2" runat="server" ClientIDMode="AutoID" />
        </div>
        <div class="form-group">
            <label>Type</label>
            <asp:DropDownList ID="ddlCorrType" runat="server" CssClass="form-control"></asp:DropDownList>
            <asp:HiddenField ID="hdnSelectedTypes" runat="server" />
        </div>
        <asp:LinkButton ID="btnCorresGo" runat="server" CssClass="btn btn-primary"><i class="fa fa-share-square-o"></i>&nbsp;&nbsp;Go</asp:LinkButton>
        &nbsp;
        <asp:LinkButton ID="btnCorrDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
    </div>
        </div>
        <div class="pull-right">
            <webdiyer:AspNetPager ID="bizPager" runat="server"
                        PagingButtonSpacing="0"
                        CssClass="bizgridpager"
                        AlwaysShow="true"
                        CurrentPageButtonClass="active"
                        PagingButtonUlLayoutClass="pagination"
                        PagingButtonLayoutType="UnorderedList"
                        FirstPageText="<<"
                        LastPageText=">>"
                        NextPageText=">"
                        PrevPageText="<"
                        Width="100%"
                        UrlPaging="false"
                        NumericButtonCount="8"
                        ShowPageIndexBox="Never"
                        ShowCustomInfoSection="Left"
                        OnPageChanged="bizPager_PageChanged"
                        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
                        CustomInfoClass="bizpagercustominfo">
                    </webdiyer:AspNetPager>
                    <asp:TextBox ID="txtCurrentPageCorr1" runat="server" Style="display: none"></asp:TextBox>
        </div>
    </div>
</div>
<div class="row" id="divMain" runat="server"  style="margin-right:0px; margin-left:0px">
    <div class="col-xs-12">
        <div class="table-responsive">
            <asp:Repeater ID="rptCorr" runat="server">
                <HeaderTemplate>
                    <table class="table table-bordered table-striped" style="display: none">
                        <thead>
                            <tr>
                                <th style="display: none">numEmailHstrId
                                </th>
                                <th style="display: none">tintType
                                </th>
                                <th>
                                    <asp:LinkButton ID="lnkType" runat="server" CommandName="Sort" CommandArgument="Type">Type</asp:LinkButton>
                                </th>
                                <th>
                                    <asp:LinkButton ID="lnkDate" runat="server" CommandName="Sort" CommandArgument="date">Date</asp:LinkButton>
                                </th>
                                <th>From ,To
                                </th>
                                <th>Name /Phone ,&amp; Ext.
                                </th>
                                <th>
                                    <asp:LinkButton ID="lnkAssigned" runat="server" CommandName="Sort" CommandArgument="assignedto">Assigned To</asp:LinkButton>
                                </th>
                                <th>
                                    <asp:CheckBox ID="chkDelete" runat="server" onclick="javascript:SelectAll('chkDelete', 'chkSelectDelete');"
                                        ClientIDMode="Static" />
                                </th>
                            </tr>
                        </thead>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table class="table table-bordered tblNoBorderCorrespondence <%# IIf((Convert.ToInt32(Container.DataItem("RowNumber")) Mod 2) = 0, "evenRow", "oddRow")%>"" style="margin-bottom: 0px;">
                        <tbody>
                            <tr id="tr" runat="server">
                                <td style="display: none">
                                    <%#Container.DataItem("numEmailHstrId")%>
                                </td>
                                <td style="display: none">
                                    <%#Container.DataItem("tintType")%>
                                </td>
                                <td rowspan="2" style="vertical-align:middle; width:200px;padding:10px !important;">
                                    <ul class="list-inline">
                                        <li>
                                            <a class="hyperlink <%#Container.DataItem("TypeClass")%>" onclick="javascript:openActionItem('<%#Container.DataItem("numEmailHstrId")%>','<%#Container.DataItem("caseid")%>','<%#Container.DataItem("CaseTimeId")%>','<%#Container.DataItem("CaseExpId")%>','<%#Container.DataItem("Type")%>','<%#Container.DataItem("dtCreatedDate")%>')">
                                            <u style="cursor: hand">
                                                <%#Container.DataItem("Type")%></u></a>
                                        </li>
                                        <li>
                                            <%# IIf(Convert.ToString(Container.DataItem("Type")) <> "Email In" AndAlso Convert.ToString(Container.DataItem("Type")) <> "Email Out" AndAlso Not Convert.ToBoolean(Container.DataItem("bitClosedflag")), "<label class='badge' style='background-color:#079dbf;color:#fff;'>Open</lable>", "")%>
                                        </li>
                                    </ul>
                                    
                                </td>
                                <td style="width: 25%;">
                                    <b><%# IIf(Convert.ToString(Container.DataItem("Type")) <> "Email In" AndAlso Convert.ToString(Container.DataItem("Type")) <> "Email Out", "Due", "Sent")%> : </b><%#Container.DataItem("date")%>
                                </td>

                                <td style="width: 20%;">
                                    <span style="display: <%# IIf(Convert.ToString(Container.DataItem("Type")) <> "Email In" AndAlso Convert.ToString(Container.DataItem("Type")) <> "Email Out", "none", "inline")%>">
                                    <b>From : </b> <asp:Label ID="lblFrom"  runat="server" Text='<%#Container.DataItem("From")%>'></asp:Label>
                                    </span>
                                        <span style="display: <%# IIf(Convert.ToString(Container.DataItem("Type")) <> "Email In" AndAlso Convert.ToString(Container.DataItem("Type")) <> "Email Out", "inline", "none")%>"><b>Assign To : </b> <%#Container.DataItem("assignedto")%></span>
                                </td>
                                <td style="width:20%">
                                    <span  style="display: <%# IIf(Convert.ToString(Container.DataItem("Type")) <> "Email In" AndAlso Convert.ToString(Container.DataItem("Type")) <> "Email Out", "none", "inline")%>">
                                    <b>To : </b><asp:Label ID="lblTo" runat="server" Text=''></asp:Label>

                                    </span>
                                </td>
                           <%--     <td>
                                    <%#Container.DataItem("Phone")%>
                                </td>--%>
                                <td style="width:5%">

                                    <img src="../images/msg_attachment_small.gif" id="imgAttachment" runat="server" visible="false" />
                                </td>
                                <td rowspan="2" style="vertical-align:top;">
                                    <asp:CheckBox ID="chkADelete" runat="server" CssClass="chkSelectDelete" />
                                    <asp:Label ID="lblDelete" runat="server" Text='<%#Container.DataItem("DelData")%>'
                                        Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr id="trSubject" style="display: <%# IIf(String.IsNullOrEmpty(Convert.ToString(Container.DataItem("Subject"))), "none", "")%>">
                                <td colspan="5">
                                    <table class="table" style="margin-bottom: 0px">
                                        <tr>
                                            <td style="font-weight: bold; width: <%# IIf(Convert.ToString(Container.DataItem("Type")) <> "Email In" AndAlso Convert.ToString(Container.DataItem("Type")) <> "Email Out", "90%", "28%")%>; border: 0px;padding: 0px !important;" <%#Container.DataItem("InlineEdit")%>><%# Container.DataItem("Subject")%></td>
                                            <td style="border: 0px;padding: 0px !important;"><%# Container.DataItem("vcBody")%></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <%--  <tr style="display: <%# IIf(String.IsNullOrEmpty(Convert.ToString(Container.DataItem("vcBody"))), "none", "")%>">
                        <td align="left" colspan="8" style="color: Gray">
                            <%# Container.DataItem("vcBody")%>
                        </td>
                    </tr>--%>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
<table align="center" runat="server" id="tblError" visible="false">
    <tr>
        <td class="normal13">
            <b>You don’t have permission to use this resource. Contact your administrator</b>
        </td>
    </tr>
</table>
<asp:TextBox ID="txtCorrTotalPage" Style="display: none" runat="server"></asp:TextBox>
<asp:TextBox ID="txtCorrTotalRecords" Style="display: none" runat="server"></asp:TextBox>
<asp:HiddenField runat="server" ID="hdnMode" />
<asp:HiddenField runat="server" ID="hdnRecordID" />
<asp:HiddenField runat="server" ID="hdnPaging" />
<asp:HiddenField runat="server" ID="hdnOpenCommu" />
