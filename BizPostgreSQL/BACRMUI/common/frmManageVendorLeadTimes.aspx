﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmManageVendorLeadTimes.aspx.vb" Inherits=".frmManageVendorLeadTimes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../JavaScript/MultiSelect/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../JavaScript/MultiSelect/bootstrap-multiselect.js" type="text/javascript"></script>
    <script type='text/javascript' src="../JavaScript/biz.VendorLeadTimes.js"></script>
    <script type="text/javascript">
        let objVendorLeadTimes = null;

        $(document).ready(function () {
            $('#ddlState').multiselect({ nonSelectedText: 'All States/Provinces', includeSelectAllOption: false, buttonWidth: '100%', templates: { button: '<button type="button" class="multiselect dropdown-toggle form-control" data-toggle="dropdown"><span class="multiselect-selected-text" style="float:left"></span> <span style="float:right"><i class="glyphicon glyphicon-menu-down" style="font-size: 10px;font-weight: bolder;color: #000;"></i></span></button>' } });
            $('#ddlShippingService').multiselect({ nonSelectedText: 'Select Shipping Carrier Service', includeSelectAllOption: false, buttonWidth: '100%', templates: { button: '<button type="button" class="multiselect dropdown-toggle form-control" data-toggle="dropdown"><span class="multiselect-selected-text" style="float:left"></span> <span style="float:right"><i class="glyphicon glyphicon-menu-down" style="font-size: 10px;font-weight: bolder;color: #000;"></i></span></button>' } });

            if (parseInt($("[id$=hdnVendorLeadTimesID]").val()) > 0) {
                $("#btnSave").html("Update");
            }

            $('#divLoader').show();
            $.when(BindItemGroups(), BindShipVia(), BindCountries(), BindVendorAddresses()).then(function () {
                objVendorLeadTimes = new VendorLeadTimes(parseInt($("[id$=hdnDivisionID]").val()), parseInt($("[id$=hdnVendorLeadTimesID]").val()));

                if (parseInt($("[id$=hdnVendorLeadTimesID]").val()) > 0) {

                    objVendorLeadTimes.GetByID().then(function (result) {
                        try {
                            if (result != null && result.Data != null) {
                                $("#txtFromQty").val((result.Data.FromQuantity === null) ? "" : result.Data.FromQuantity);
                                $("#txtToQty").val((result.Data.ToQuantity === null) ? "" : result.Data.ToQuantity);
                                $("#ddlItemGroup").val(result.Data.ItemGroups);
                                $("#ddlShipVia").val((result.Data.ShipVia === null) ? "0" : result.Data.ShipVia);
                                $("#ddlShipFromLocation").val(result.Data.ShipFromLocations);
                                $("#ddlCountry").val((result.Data.ShipToCountry === null) ? "0" : result.Data.ShipToCountry);
                                $("#txtDays").val((result.Data.DaysToArrive === null) ? "" : result.Data.DaysToArrive);

                                BindShippingServices(result.Data.ShippingServices);
                                BindStates(result.Data.ShipToStates);

                                $("#ddlItemGroup").multiselect("refresh");
                                $("#ddlShippingService").multiselect("refresh");
                                $("#ddlShipFromLocation").multiselect("refresh");
                                
                            }
                        } catch (e) {
                            alert("Error occurred while showing vendor lead times data: " + err.ErrorMessage);
                        }

                        $('#divLoader').hide();
                    }).catch(function (err) {
                        alert("Error occurred while loading vendor lead times data: " + err.ErrorMessage);
                        $('#divLoader').hide();
                    });;
                } else {
                    $('#divLoader').hide();
                }
            }, function () {
                $('#divLoader').hide();
            });

            $("#btnSave").click(function () {
                try {
                    if ($("#txtFromQty").val() == "") {
                        alert("Buying from quanity required");
                        $("#txtFromQty").focus();
                        return false;
                    } else if ($("#txtToQty").val() == "") {
                        alert("Buying to quanity required");
                        $("#txtToQty").focus();
                        return false;
                    } else if ($('#ddlItemGroup').val() == null) {
                        alert("Select item group(s)");
                        $("#ddlItemGroup").focus();
                        return false;
                    } else if (parseInt($('#ddlShipVia').val()) == 0) {
                        alert("Select ship via");
                        $("#ddlShipVia").focus();
                        return false;
                    } else if ($('#ddlShippingService option').length > 0 && $('#ddlShippingService').val() == null) {
                        alert("Select shipping carrier service(s)");
                        $("#ddlShippingService").focus();
                        return false;
                    } else if ($('#ddlShipFromLocation').val() == null) {
                        alert("Select vendor ship-from location(s)");
                        $("#ddlShipFromLocation").focus();
                        return false;
                    } else if (parseInt($('#ddlCountry').val()) == 0) {
                        alert("Select ship-to location country");
                        $("#ddlCountry").focus();
                        return false;
                    } else if ($('#txtDays').val() == "") {
                        alert("Days to arrive is required");
                        $("#txtDays").focus();
                        return false;
                    }

                    $("#divLoader").show();

                    objVendorLeadTimes.fromQty = parseInt($("#txtFromQty").val());
                    objVendorLeadTimes.toQty = parseInt($("#txtToQty").val());
                    objVendorLeadTimes.itemGroups = $("#ddlItemGroup").val().map(i => Number(i));
                    objVendorLeadTimes.shipVia = parseInt($('#ddlShipVia').val());
                    objVendorLeadTimes.shippingServices = $('#ddlShippingService').val() === null ? [] : $('#ddlShippingService').val().map(i => Number(i));
                    objVendorLeadTimes.shipFromLocations = $('#ddlShipFromLocation').val().map(i => Number(i));
                    objVendorLeadTimes.shipToCountry = parseInt($('#ddlCountry').val());
                    objVendorLeadTimes.shipToStates = $('#ddlState').val() === null ? [] : $('#ddlState').val().map(i => Number(i));
                    objVendorLeadTimes.daysToArrive = parseInt($("#txtDays").val());

                    objVendorLeadTimes.Save().then(function (data) {
                        if (window.opener != null) {
                            window.opener.LoadVendorLeadTimes();
                        }
                        Close();
                    }).catch(function (err) {
                        alert(err.ErrorMessage);
                        $("#divLoader").hide();
                    });
                } catch (e) {
                    alert("Unknown error ocurred while saving data.");
                    $("#divLoader").hide();
                }

                return false;
            });

            $("#ddlCountry").change(function () {
                BindStates(null);
            });

            $("#ddlShipVia").change(function () {
                BindShippingServices(null);
            });
        });

        function BindItemGroups() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetListDetails',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "listID": 36
                }),
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetListDetailsResult);
                        if (obj != null && obj.length > 0) {
                            $.each(obj, function (index, value) {
                                $("#ddlItemGroup").append("<option value='" + value.numListItemID.toString() + "'>" + replaceNull(value.vcData) + "</option>");
                            });

                            $('#ddlItemGroup').multiselect({ nonSelectedText: 'Select Item Group(s)', includeSelectAllOption: false, buttonWidth: '100%', templates: { button: '<button type="button" class="multiselect dropdown-toggle form-control" data-toggle="dropdown"><span class="multiselect-selected-text" style="float:left"></span> <span style="float:right"><i class="glyphicon glyphicon-menu-down" style="font-size: 10px;font-weight: bolder;color: #000;"></i></span></button>' } });
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading item groups.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading item groups: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading item groups");
                    }
                }
            });
        }

        function BindShipVia() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetListDetails',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "listID": 82
                }),
                success: function (data) {
                    try {
                        $("#ddlShipVia").append("<option value='0'>Select Ship Via</optin>");

                        var obj = $.parseJSON(data.GetListDetailsResult);
                        if (obj != null && obj.length > 0) {
                            $.each(obj, function (index, value) {
                                $("#ddlShipVia").append("<option value='" + value.numListItemID.toString() + "'>" + replaceNull(value.vcData) + "</option>");
                            });
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading item groups.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading item groups: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading item groups");
                    }
                }
            });
        }

        function BindCountries() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetListDetails',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "listID": 40
                }),
                success: function (data) {
                    try {
                        $("#ddlCountry").append("<option value='0'>Select Country</optin>");

                        var obj = $.parseJSON(data.GetListDetailsResult);
                        if (obj != null && obj.length > 0) {
                            $.each(obj, function (index, value) {
                                $("#ddlCountry").append("<option value='" + value.numListItemID.toString() + "'>" + replaceNull(value.vcData) + "</option>");
                            });
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading countries.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading countries: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading countries");
                    }
                }
            });
        }

        function BindStates(selectedStates) {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetStates',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "countryID": parseInt($("#ddlCountry").val())
                }),
                beforeSend: function () {
                    $("#divLoader").show();
                },
                complete: function () {
                    $("#divLoader").hide();
                },
                success: function (data) {
                    try {
                        $("#ddlState").find("option").remove();

                        var obj = $.parseJSON(data.GetStatesResult);
                        if (obj != null && obj.length > 0) {
                            $.each(obj, function (index, value) {
                                $("#ddlState").append("<option value='" + value.numStateID.toString() + "'>" + replaceNull(value.vcState) + "</option>");
                            });
                        }

                        $('#ddlState').multiselect('destroy');
                        $('#ddlState').multiselect({ nonSelectedText: 'All States/Provinces', includeSelectAllOption: false, buttonWidth: '100%', templates: { button: '<button type="button" class="multiselect dropdown-toggle form-control" data-toggle="dropdown"><span class="multiselect-selected-text" style="float:left"></span> <span style="float:right"><i class="glyphicon glyphicon-menu-down" style="font-size: 10px;font-weight: bolder;color: #000;"></i></span></button>' } });

                        if (selectedStates) {
                            $("#ddlState").val(selectedStates);
                            $("#ddlState").multiselect("refresh");
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading states.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading states: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading states");
                    }
                }
            });
        }

        function BindShippingServices(selectedShippingServices) {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetShippingServices',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "shipVia": parseInt($("#ddlShipVia").val())
                }),
                beforeSend: function () {
                    $("#divLoader").show();
                },
                complete: function () {
                    $("#divLoader").hide();
                },
                success: function (data) {
                    try {
                        $("#ddlShippingService").find("option").remove();

                        var obj = $.parseJSON(data.GetShippingServicesResult);
                        if (obj != null && obj.length > 0) {
                            $.each(obj, function (index, value) {
                                $("#ddlShippingService").append("<option value='" + value.numShippingServiceID.toString() + "'>" + replaceNull(value.vcShipmentService) + "</option>");
                            });
                        }

                        $('#ddlShippingService').multiselect('destroy');
                        $('#ddlShippingService').multiselect({ nonSelectedText: 'Select Shipping Carrier Service', includeSelectAllOption: false, buttonWidth: '100%', templates: { button: '<button type="button" class="multiselect dropdown-toggle form-control" data-toggle="dropdown"><span class="multiselect-selected-text" style="float:left"></span> <span style="float:right"><i class="glyphicon glyphicon-menu-down" style="font-size: 10px;font-weight: bolder;color: #000;"></i></span></button>' } });

                        if (selectedShippingServices) {
                            $("#ddlShippingService").val(selectedShippingServices);
                            $("#ddlShippingService").multiselect("refresh");
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading shipping carrier services.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading shipping carrier services: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading shipping carrier services.");
                    }
                }
            });
        }

        function BindVendorAddresses() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetAddresses',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "addressFor": 2
                    , "addressType" : 2
                    , "recordID": parseInt($("[id$=hdnDivisionID]").val())
                }),
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetAddressesResult);
                        if (obj != null && obj.length > 0) {
                            $.each(obj, function (index, value) {
                                $("#ddlShipFromLocation").append("<option value='" + value.numAddressID.toString() + "'>" + replaceNull(value.vcAddressName) + " (" + replaceNull(value.FullAddress) + ")" + "</option>");
                            });
                        }

                        $('#ddlShipFromLocation').multiselect({ nonSelectedText: 'Select Vendor Ship-From Location(s)', includeSelectAllOption: false, buttonWidth: '100%', templates: { button: '<button type="button" class="multiselect dropdown-toggle form-control" data-toggle="dropdown"><span class="multiselect-selected-text" style="float:left"></span> <span style="float:right"><i class="glyphicon glyphicon-menu-down" style="font-size: 10px;font-weight: bolder;color: #000;"></i></span></button>' } });
                        return true;
                    } catch (err) {
                        alert("Unknown error occurred while loading vendor ship-from locations.");
                        return false;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading vendor ship-from locations: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading vendor ship-from locations.");
                    }

                    return false;
                }
            });
        }

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }
    </script>
    <style type="text/css">
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }

        .multiselect-native-select .btn-default {
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 0px;
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }

            .multiselect-native-select .btn-default.active.focus,
            .multiselect-native-select .btn-default.active:focus,
            .multiselect-native-select .btn-default.active:hover,
            .multiselect-native-select .btn-default:active.focus,
            .multiselect-native-select .btn-default:active:focus,
            .multiselect-native-select .btn-default:active:hover,
            .multiselect-native-select .open > .dropdown-toggle.btn-default.focus,
            .multiselect-native-select .open > .dropdown-toggle.btn-default:focus,
            .multiselect-native-select .open > .dropdown-toggle.btn-default:hover {
                color: #555;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
            }

        .multiselect-native-select .btn-group.open .dropdown-toggle {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .multiselect-native-select .btn-default.active, .multiselect-native-select .btn-default:active, .multiselect-native-select .open > .dropdown-toggle.btn-default {
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="pull-right">
        <button id="btnSave" class="btn btn-primary">Add</button>
        <button id="btnClose" class="btn btn-primary" onclick="Close();">Close</button>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Vendor Lead Times
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridFilterTools" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-inline" style="margin-bottom:15px;">
                <div class="form-group">
                    <label>When buying: </label>
                    <input type="number" class="form-control" id="txtFromQty" style="width: 100px" />
                    <label>to </label>
                    <input type="number" class="form-control" id="txtToQty" style="width: 100px" />
                    <i>(Base UOM)</i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Item Group</label>
                <div>
                    <select id="ddlItemGroup" multiple="multiple" class="form-control"></select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label>Ship Via</label>
                <select id="ddlShipVia" class="form-control"></select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label>Shipping Carrier Service</label>
                <select id="ddlShippingService" multiple="multiple" class="form-control"></select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Vendor Ship-From Location</label>
                <select id="ddlShipFromLocation" multiple="multiple" class="form-control"></select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Ship-To Location</label>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <select id="ddlCountry" class="form-control"></select>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <select id="ddlState" multiple="multiple" class="form-control">
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>It will take approximately </label>
                    <input type="number" class="form-control" id="txtDays" maxlength="3" style="width: 50px" />
                    <label>days for it to arrive</label>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay" id="divLoader" style="display: none">
        <div class="overlayContent" style="background-color: #fff; color: #000; text-align: center; width: 280px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Processing Request</h3>
        </div>
    </div>
    <asp:HiddenField ID="hdnVendorLeadTimesID" runat="server" />
    <asp:HiddenField ID="hdnDivisionID" runat="server" />
</asp:Content>
