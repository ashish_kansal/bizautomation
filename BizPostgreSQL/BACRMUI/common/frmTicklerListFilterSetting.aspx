﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmTicklerListFilterSetting.aspx.vb" Inherits=".frmTicklerListFilterSetting" %>

<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div style="float: right; margin-right: 5px;">
        <asp:Button ID="btnSaveAndClose" CssClass="btn btn-primary" runat="server" Text="Save & Close" />
        <asp:Button ID="btnClose" CssClass="btn btn-primary" runat="server" Text="Close" OnClientClick="return Close();" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Tickler List Filter Setting
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div style="margin: 10px">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <telerik:RadTabStrip ID="radTs" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
                    Skin="Default" ClickSelectedTab="True" AutoPostBack="True" MultiPageID="radMP" SelectedIndex="0">
                    <Tabs>
                        <telerik:RadTab runat="server" PageViewID="radPVTickler" Text="Action Items & Meetings" />
                        <telerik:RadTab runat="server" PageViewID="radPVOpportunity" Text="Opportunities & Projects" />
                        <telerik:RadTab runat="server" PageViewID="radPVCases" Text="Cases" />
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage runat="server" ID="radMP" SelectedIndex="0" CssClass="pageView">
                    <telerik:RadPageView ID="radPVTickler" runat="server" Width="800">
                        <table width="100%">
                            <tr>
                                <td style="vertical-align: top;">
                                    <table width="180">
                                        <tr>
                                            <td style="font-weight: bold; font-size: 12px; text-align: center">Employees available
                                            </td>
                                        </tr>
                                        <tr style="height: 100%">
                                            <td>
                                                <asp:ListBox ID="lbAvailableFields" runat="server" Height="200" Width="180" SelectionMode="Single"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="80" style="text-align: center">
                                    <asp:Button ID="btnAdd" CssClass="button" runat="server" Text="Add >" Style="margin-bottom: 50px" />
                                    <asp:Button ID="btnRemove" CssClass="button" runat="server" Text="< Remove" Style="margin-top: 50px" />
                                </td>
                                <td style="vertical-align: top;">
                                    <table width="180">
                                        <tr>
                                            <td style="font-weight: bold; font-size: 12px; text-align: center">Employees selected
                                            </td>
                                        </tr>
                                        <tr style="height: 100%">
                                            <td>
                                                <asp:ListBox ID="lbSelectedFields" runat="server" Height="200" Width="180" SelectionMode="Single"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10"></td>
                                <td style="vertical-align: top; white-space: nowrap; width: 100%">
                                    <table>
                                        <tr>
                                            <td style="font-weight: bold; font-size: 12px;">Criteria</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="rdbAssigned" runat="server" Text="Action items assigned" GroupName="Criteria" AutoPostBack="true" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="rdbOwned" runat="server" Text="Action items owned" GroupName="Criteria" AutoPostBack="true" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="rdbAssignedOrOwned" runat="server" Text="Action items assigned or owned" GroupName="Criteria" AutoPostBack="true" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; font-size: 12px;">Action Item Types to include</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBoxList ID="chblAction" runat="server" RepeatColumns="3"></asp:CheckBoxList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="radPVOpportunity" runat="server" Width="800">
                        <table width="100%">
                            <tr>
                                <td style="vertical-align: top;">
                                    <table width="180">
                                        <tr>
                                            <td style="font-weight: bold; font-size: 12px; text-align: center">Employees available
                                            </td>
                                        </tr>
                                        <tr style="height: 100%">
                                            <td>
                                                <asp:ListBox ID="lbAvailableFieldsOpp" runat="server" Height="200" Width="180"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="80" style="text-align: center">
                                    <asp:Button ID="btnAddOpp" CssClass="button" runat="server" Text="Add >" Style="margin-bottom: 50px" OnClientClick="return Add('lbAvailableFieldsOpp','lbSelectedFieldsOpp','hdnSelectedEmployeesOpp');" />
                                    <asp:Button ID="btnRemoveOpp" CssClass="button" runat="server" Text="< Remove" Style="margin-top: 50px" OnClientClick="return Remove('lbAvailableFieldsOpp','lbSelectedFieldsOpp','hdnSelectedEmployeesOpp');" />
                                </td>
                                <td style="vertical-align: top;">
                                    <table width="180">
                                        <tr>
                                            <td style="font-weight: bold; font-size: 12px; text-align: center">Employees selected
                                            </td>
                                        </tr>
                                        <tr style="height: 100%">
                                            <td>
                                                <asp:ListBox ID="lbSelectedFieldsOpp" runat="server" Height="200" Width="180"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10"></td>
                                <td style="vertical-align: top; white-space: nowrap; width: 100%">
                                    <table>
                                        <tr>
                                            <td style="font-weight: bold; font-size: 12px;">Assigned stages on</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkSalesOpportunities" runat="server" Text="Sales Opportunities" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkPurchaseOpportunities" runat="server" Text="Purchase Opportunities" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkSalesOrders" runat="server" Text="Sales Orders" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkPurchaseOrders" runat="server" Text="Purchase Orders" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkProjects" runat="server" Text="Projects" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="radPVCases" runat="server" Width="800">
                        <table width="100%">
                            <tr>
                                <td style="vertical-align: top;">
                                    <table width="180">
                                        <tr>
                                            <td style="font-weight: bold; font-size: 12px; text-align: center">Employees available
                                            </td>
                                        </tr>
                                        <tr style="height: 100%">
                                            <td>
                                                <asp:ListBox ID="lbAvailableFieldsCases" runat="server" Height="200" Width="180"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="80" style="text-align: center">
                                    <asp:Button ID="btnAddCases" CssClass="button" runat="server" Text="Add >" Style="margin-bottom: 50px" OnClientClick="return Add('lbAvailableFieldsCases','lbSelectedFieldsCases','hdnSelectedEmployeesCases');" />
                                    <asp:Button ID="btnRemoveCases" CssClass="button" runat="server" Text="< Remove" Style="margin-top: 50px" OnClientClick="return Remove('lbAvailableFieldsCases','lbSelectedFieldsCases','hdnSelectedEmployeesCases');" />
                                </td>
                                <td style="vertical-align: top;">
                                    <table width="180">
                                        <tr>
                                            <td style="font-weight: bold; font-size: 12px; text-align: center">Employees selected
                                            </td>
                                        </tr>
                                        <tr style="height: 100%">
                                            <td>
                                                <asp:ListBox ID="lbSelectedFieldsCases" runat="server" Height="200" Width="180"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10"></td>
                                <td style="vertical-align: top; white-space: nowrap; width: 100%">
                                    <table>
                                        <tr>
                                            <td style="font-weight: bold; font-size: 12px;">Criteria</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="rdbCasesAssigned" runat="server" Text="Cases assigned" GroupName="CriteriaCases" AutoPostBack="true" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="rdbCasesOwned" runat="server" Text="Cases owned" GroupName="CriteriaCases" AutoPostBack="true" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="rdbCasesAssignedOrOwned" runat="server" Text="Cases assigned or owned" GroupName="CriteriaCases" AutoPostBack="true" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
