﻿Imports BACRM.BusinessLogic
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Outlook
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.UserInterface.Common
    Public Class frmAddActivity
        Inherits BACRMPage
        Public objCommon As New CCommon
        Public objActionItem As New ActionItem
        Public objOutLook As New COutlook
        Dim bError As Boolean = False
        Dim dsTemp As DataSet
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim strModuleName, strPermissionName As String
            Dim m_aryRightsAddTickler() As Integer = GetUserRightsForPage_Other(1, 1, strModuleName, strPermissionName)

            If m_aryRightsAddTickler(RIGHTSTYPE.ADD) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AC&Module=" & strModuleName & "&Permission=" & strPermissionName)
                Exit Sub
            End If

            If Not IsPostBack Then
                sb_FillEmpList()
                Sb_FillTypeList()
                Sb_FillActivityList()
                Sb_FillPriorityList()
                LoadActionItemTemplateData()
                radDueDate.SelectedDate = DateTime.Now.Date
                If GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("fghTY") <> "" Or GetQueryStringVal("rtyWR") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Then

                    If GetQueryStringVal("uihTR") <> "" Then
                        objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                        objCommon.charModule = "C"
                    ElseIf GetQueryStringVal("rtyWR") <> "" Then
                        objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                        objCommon.charModule = "D"
                    ElseIf GetQueryStringVal("tyrCV") <> "" Then
                        objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                        objCommon.charModule = "P"
                    ElseIf GetQueryStringVal("pluYR") <> "" Then
                        objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                        objCommon.charModule = "O"
                    ElseIf GetQueryStringVal("fghTY") <> "" Then
                        objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                        objCommon.charModule = "S"
                    End If

                    objCommon.GetCompanySpecificValues1()
                    Dim strCompany, strContactID As String
                    strCompany = objCommon.GetCompanyName
                    strContactID = objCommon.ContactID
                    radCmbCompany.Text = strCompany
                    radCmbCompany.SelectedValue = objCommon.DivisionID

                    LoadContactDetail() 'Added by chintan- Reason:remaining credit wasn't showing

                End If
                radCmbCompany.Focus()
                If CCommon.ToBool(Session("bitFollowupAnytime")) Then
                    chkFollowUp.Checked = True
                End If
            End If
        End Sub
        Private Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                LoadContactDetail()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            End Try
        End Sub
        Private Sub LoadActionItemTemplateData()
            Try
                If objActionItem Is Nothing Then objActionItem = New ActionItem

                Dim dtActionItems As DataTable
                objActionItem.DomainID = Session("DomainId")
                objActionItem.UserCntID = Session("UserContactId")
                dtActionItems = objActionItem.LoadActionItemTemplateData()

                If Not dtActionItems Is Nothing Then
                    ddlActionItemTemplate.DataSource = dtActionItems
                    ddlActionItemTemplate.DataTextField = "TemplateName"
                    ddlActionItemTemplate.DataValueField = "RowID"
                    ddlActionItemTemplate.DataBind()
                    ddlActionItemTemplate.Items.Insert(0, New ListItem("--Select One--", 0))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Private Sub Sb_FillTypeList()
            Try
                Dim objCommon As New CCommon()
                ddlType.DataSource = objCommon.GetMasterListItems(73, Session("DomainID"))
                ddlType.DataTextField = "vcData"
                ddlType.DataValueField = "numListItemID"
                ddlType.DataBind()
                ddlType.Items.Insert(0, "--Select One--")
                ddlType.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception

            End Try
        End Sub
        Private Sub sb_FillEmpList()
            Try
                Dim objContacts As New CContacts
                If objContacts Is Nothing Then objContacts = New CContacts

                Dim dtEmployeeList As DataTable
                objContacts.DomainID = Session("DomainID")
                dtEmployeeList = objContacts.EmployeeList
                ddlUserNames.DataSource = dtEmployeeList
                ddlUserNames.DataTextField = "vcUserName"
                ddlUserNames.DataValueField = "numContactID"
                ddlUserNames.DataBind()
                If Not ddlUserNames.Items.FindByValue(Session("UserContactID")) Is Nothing Then
                    ddlUserNames.ClearSelection()
                    ddlUserNames.Items.FindByValue(Session("UserContactID")).Text = "My Self"
                    ddlUserNames.Items.FindByValue(Session("UserContactID")).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub LoadContactDetail()
            Try
                Dim objOpportunities As New COpportunities
                objOpportunities.DivisionID = radCmbCompany.SelectedValue
                ddlContact.DataSource = objOpportunities.ListContact().Tables(0).DefaultView()
                ddlContact.DataTextField = "ContactType"
                ddlContact.DataValueField = "numcontactId"
                ddlContact.DataBind()
                ddlContact.Items.Insert(0, New ListItem("--Select One--", "0"))
                If ddlContact.Items.Count >= 2 Then
                    ddlContact.Items(1).Selected = True
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Private Sub Sb_FillActivityList()
            Try
                Dim objCommon As New CCommon()
                ddlActivity.DataSource = objCommon.GetMasterListItems(32, Session("DomainID"))
                ddlActivity.DataTextField = "vcData"
                ddlActivity.DataValueField = "numListItemID"
                ddlActivity.DataBind()
                ddlActivity.Items.Insert(0, "--Select One--")
                ddlActivity.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception

            End Try
        End Sub
        Private Sub Sb_FillPriorityList()
            Try
                Dim objCommon As New CCommon()
                ddlStatus.DataSource = objCommon.GetMasterListItems(447, Session("DomainID"))
                ddlStatus.DataTextField = "vcData"
                ddlStatus.DataValueField = "numListItemID"
                ddlStatus.DataBind()
                ddlStatus.Items.Insert(0, "--Select One--")
                ddlStatus.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception

            End Try
        End Sub
        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                InsertCommunication()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub InsertCommunication()
            Try
                Dim intTaskFlg As Integer   'Task flag for identifying whether task or Communication
                Dim lngCommId As Long
                Dim lngActivityId As Long = 0

                'Start and End Time Validation
                If ddlType.SelectedValue <> 973 And ddlType.SelectedValue <> 974 And chkFollowUp.Checked = False Then
                    Dim numSTime, numETime As Double
                    numSTime = Double.Parse(radcmbStartTime.SelectedItem.Text.Trim.Split(":")(0) + "." + radcmbStartTime.SelectedItem.Text.Trim.Split(":")(1))
                    numETime = Double.Parse(radcmbEndTime.SelectedItem.Text.Trim.Split(":")(0) + "." + radcmbEndTime.SelectedItem.Text.Trim.Split(":")(1))

                    If (chkAM.Checked = True And (numSTime = 12 Or numSTime = 12.3)) Then
                        numSTime = 0
                    End If

                    If (chkEndAM.Checked = True And (numETime = 12 Or numETime = 12.3)) Then
                        numETime = 0
                    End If

                    If chkPM.Checked = True And Not (numSTime = 12 Or numSTime = 12.3) Then
                        numSTime = numSTime + 12
                    End If

                    If chkEndPM.Checked = True And Not (numETime = 12 Or numETime = 12.3) Then
                        numETime = numETime + 12
                    End If


                    If (numSTime > numETime) Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "TimeValidation", "alert('End Time must be greater than Start Time.')", True)
                        bError = True
                        Exit Sub
                    End If
                End If

                Dim strStartTime, strEndTime, strDate, strFollowUp As String
                Dim intAssignTo, intEndTime, intTime, intEndChk, intChk As Integer
                strDate = IIf(radDueDate.SelectedDate Is Nothing, DateTime.Now.Date, radDueDate.SelectedDate)

                If chkFollowUp.Checked = False Then
                    strStartTime = strDate.Trim & " " & radcmbStartTime.SelectedItem.Text.Trim & ":00" & IIf(chkAM.Checked = True, " AM", " PM")
                    strEndTime = strDate.Trim & " " & radcmbEndTime.SelectedItem.Text.Trim & ":00" & IIf(chkEndAM.Checked = True, " AM", " PM")
                Else
                    strStartTime = strDate.Trim & " 8:00:00 AM"
                    strEndTime = strDate.Trim & " 8:00:00 AM"
                End If


                If ddlUserNames.Enabled = True Then
                    intAssignTo = ddlUserNames.SelectedItem.Value
                    intEndTime = radcmbEndTime.SelectedItem.Value
                    intEndChk = IIf(chkEndAM.Checked = True, 1, 0)
                    intTime = radcmbStartTime.SelectedItem.Value
                    intChk = IIf(chkAM.Checked = True, 1, 0)
                End If

                If ddlType.SelectedValue <> 973 And ddlType.SelectedValue <> 974 And chkFollowUp.Checked = False Then
                    Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                    Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                    strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                    strSplitEndTimeDate = CType(strEndTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC
                    Dim strDesc As String = "Organization: " & radCmbCompany.Text & Environment.NewLine & "Contact Name: " & ddlContact.SelectedItem.Text & Environment.NewLine & "Assigned To: " & ddlUserNames.SelectedItem.Text & Environment.NewLine & "Comments:" & txtcomments.Text
                End If

                objActionItem = New ActionItem

                With objActionItem
                    .CommID = 0
                    .Task = ddlType.SelectedValue
                    .ContactID = ddlContact.SelectedItem.Value
                    .DivisionID = IIf(radCmbCompany.SelectedValue = "", 0, radCmbCompany.SelectedValue)
                    .Details = txtcomments.Text
                    .AssignedTo = intAssignTo
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    '.BitClosed = IIf(chkClose.Checked = True, 1, 0)
                    If CCommon.ToLong(ddlType.SelectedValue) = 973 Then
                        .StartTime = IIf(radDueDate.SelectedDate Is Nothing, DateTime.Now.Date, radDueDate.SelectedDate)
                        .EndTime = DateAdd(DateInterval.Day, 1, .StartTime)
                    Else
                        .StartTime = strStartTime
                        .EndTime = strEndTime
                    End If
                    .Activity = ddlActivity.SelectedValue
                    .Status = ddlStatus.SelectedValue
                    '.Remainder = ddlMinutes.SelectedValue
                    '.RemainderStatus = CCommon.ToShort(chkPopupRemainder.Checked)
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    '.bitOutlook = IIf(chkOutlook.Checked = True, 1, 0)
                    .ActivityId = ddlActivity.SelectedValue
                    .strContactIds = ""
                    .FollowUpAnyTime = IIf(chkFollowUp.Checked = True, 1, 0)

                    If Session("AttendeeTable") IsNot Nothing Then
                        dsTemp = Session("AttendeeTable")
                        dsTemp.Tables(0).TableName = "AttendeeTable"

                        .strAttendee = dsTemp.GetXml
                    End If
                    .numLinkedOrganization = 0
                    .numLinkedContact = 0
                    '.numLinkedOrganization = IIf(String.IsNullOrEmpty(hdnLinkedOrg.Value), 0, CCommon.ToLong(hdnLinkedOrg.Value))
                    '.numLinkedContact = IIf(String.IsNullOrEmpty(hdnLinkedOrgContact.Value), 0, CCommon.ToLong(hdnLinkedOrgContact.Value))
                End With

                Dim numcommId As Long = objActionItem.SaveCommunicationinfo()

                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = numcommId
                objWfA.SaveWFActionItemsQueue()
                'ss//end of code

                AddToRecentlyViewed(RecetlyViewdRecordType.ActionItem, numcommId)


                'Update FollowUp Status -Bug ID 270
                'Dim objAcc As New BACRM.BusinessLogic.Account.CAccounts
                'objAcc.ContactID = ddlContact.SelectedItem.Value
                'objAcc.FollowUpStatus = ddlFollowUpStatus.SelectedValue
                'objAcc.UpdateFollowUpStatus()

                ''Sending mail to Assigneee
                If ViewState("AssignedTo") Is Nothing Then ViewState("AssignedTo") = 0
                If Session("UserContactID") <> ddlUserNames.SelectedItem.Value Then
                    If ddlUserNames.SelectedValue > 0 And ViewState("AssignedTo") <> ddlUserNames.SelectedValue Then
                        CAlerts.SendAlertToAssignee(6, Session("UserContactID"), ddlUserNames.SelectedValue, numcommId, Session("DomainID")) 'bugid 767
                        ViewState("AssignedTo") = ddlUserNames.SelectedValue
                    End If
                End If
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "window.opener.location.href ='../Common/frmTicklerDisplay.aspx?SelectedIndex=0';window.close();", True)

            Catch ex As Exception
                Throw
            End Try
        End Sub
        'Private Sub UpdateCommunication()
        '    If ddlUserNames.Items.Count = 0 Then Exit Sub
        '    Dim intTaskFlg As Integer
        '    Try

        '        If ddlType.SelectedValue <> 973 And ddlType.SelectedValue <> 974 And chkFollowUp.Checked = False Then
        '            Dim numSTime, numETime As Double
        '            numSTime = Double.Parse(radcmbStartTime.SelectedItem.Text.Trim.Split(":")(0) + "." + radcmbStartTime.SelectedItem.Text.Trim.Split(":")(1))
        '            numETime = Double.Parse(radcmbEndTime.SelectedItem.Text.Trim.Split(":")(0) + "." + radcmbEndTime.SelectedItem.Text.Trim.Split(":")(1))

        '            If (chkAM.Checked = True And (numSTime = 12 Or numSTime = 12.3)) Then
        '                numSTime = 0
        '            End If

        '            If (chkEndAM.Checked = True And (numETime = 12 Or numETime = 12.3)) Then
        '                numETime = 0
        '            End If

        '            If chkPM.Checked = True And Not (numSTime = 12 Or numSTime = 12.3) Then
        '                numSTime = numSTime + 12
        '            End If

        '            If chkEndPM.Checked = True And Not (numETime = 12 Or numETime = 12.3) Then
        '                numETime = numETime + 12
        '            End If


        '            If (numSTime > numETime) Then
        '                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "TimeValidation", "alert('End Time must be greater than Start Time.')", True)
        '                bError = True
        '                Exit Sub
        '            End If
        '        End If
        '        'Communication means in the DB the flag  value is equal to 1. Task means it is equal to 0.
        '        'intTaskFlg = IIf(cmbMainType.Value = 0, 0, 1)
        '        intTaskFlg = ddlType.SelectedItem.Value



        '        Dim strCalendar, strStartTime, strEndTime, strDate As String
        '        strDate = IIf(radDueDate.SelectedDate Is Nothing, DateTime.Now.Date, radDueDate.SelectedDate)

        '        If chkFollowUp.Checked = False Then
        '            strStartTime = strDate.Trim & " " & radcmbStartTime.SelectedItem.Text.Trim & ":00" & IIf(chkAM.Checked = True, " AM", " PM")
        '            strEndTime = strDate.Trim & " " & radcmbEndTime.SelectedItem.Text.Trim & ":00" & IIf(chkEndAM.Checked = True, " AM", " PM")
        '        Else
        '            strStartTime = strDate.Trim & " 8:00:00 AM"
        '            strEndTime = strDate.Trim & " 8:00:00 AM"
        '        End If


        '        If CCommon.ToLong(ddlType.SelectedValue) <> 973 And CCommon.ToLong(ddlType.SelectedValue) <> 974 And chkFollowUp.Checked = False Then
        '            Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
        '            Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
        '            strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
        '            strSplitEndTimeDate = CType(strEndTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))          'Convert the End Date and Time to UTC
        '            Dim strDesc As String = "Organization: " & lnkCompany.Text & Environment.NewLine & "Contact Name: " & lnkContact.Text & Environment.NewLine & "Assigned To: " & ddlUserNames.SelectedItem.Text & Environment.NewLine & "Comments:" & txtcomments.Text
        '            If ddlUserNames.SelectedItem.Text = "My Self" Then
        '                If Len(Trim(GetQueryStringVal("CommId"))) = 0 Then
        '                    If chkOutlook.Checked = True Then
        '                        If objOutLook Is Nothing Then
        '                            objOutLook = New COutlook
        '                        End If
        '                        objOutLook.UserCntID = Session("UserContactId")
        '                        objOutLook.DomainID = Session("DomainId")
        '                        Dim dtTable As DataTable
        '                        dtTable = objOutLook.GetResourceId
        '                        If dtTable.Rows.Count > 0 Then
        '                            objOutLook.AllDayEvent = False
        '                            objOutLook.ActivityDescription = strDesc
        '                            objOutLook.Location = ""
        '                            objOutLook.Subject = ddlType.SelectedItem.Text
        '                            objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
        '                            objOutLook.StartDateTimeUtc = strSplitStartTimeDate
        '                            objOutLook.EnableReminder = True
        '                            objOutLook.ReminderInterval = 900
        '                            objOutLook.ShowTimeAs = 3
        '                            objOutLook.Importance = 2
        '                            objOutLook.RecurrenceKey = -999
        '                            objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
        '                            If txtActivityID.Text = "0" Then
        '                                txtActivityID.Text = CStr(objOutLook.AddActivity())
        '                            Else

        '                                objOutLook.ActivityID = CInt(txtActivityID.Text)
        '                                objOutLook.UpdateActivity()
        '                            End If
        '                        End If


        '                        strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), Session("UserEmail"), txtcomments.Text)
        '                        clsProspects.fn_DeleteOWAActionFromEx(Session("SiteType"), Session("ServerName"), Session("UserEmail"), HidStrEml.Value)
        '                    End If
        '                Else
        '                    If chkClose.Checked = True Then
        '                        If objOutLook Is Nothing Then
        '                            objOutLook = New COutlook
        '                        End If
        '                        objOutLook.ActivityID = CInt(txtActivityID.Text)
        '                        objOutLook.RemoveActivity()
        '                    Else
        '                        If chkOutlook.Checked = True Then

        '                            If objOutLook Is Nothing Then
        '                                objOutLook = New COutlook
        '                            End If
        '                            objOutLook.UserCntID = Session("UserContactId")
        '                            objOutLook.DomainID = Session("DomainId")
        '                            Dim dtTable As DataTable
        '                            dtTable = objOutLook.GetResourceId
        '                            If dtTable.Rows.Count > 0 Then
        '                                objOutLook.AllDayEvent = False
        '                                objOutLook.ActivityDescription = strDesc
        '                                objOutLook.Location = ""
        '                                objOutLook.Subject = ddlType.SelectedItem.Text
        '                                objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
        '                                objOutLook.StartDateTimeUtc = strSplitStartTimeDate
        '                                objOutLook.EnableReminder = True
        '                                objOutLook.ReminderInterval = 900
        '                                objOutLook.ShowTimeAs = 3
        '                                objOutLook.Importance = 2
        '                                objOutLook.RecurrenceKey = -999
        '                                objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
        '                                If txtActivityID.Text = "0" Then
        '                                    txtActivityID.Text = CStr(objOutLook.AddActivity())
        '                                Else
        '                                    objOutLook.ActivityID = CInt(txtActivityID.Text)
        '                                    objOutLook.UpdateActivity()
        '                                End If
        '                            End If
        '                        Else
        '                            If CCommon.ToInteger(txtActivityID.Text) > 0 Then
        '                                If objOutLook Is Nothing Then
        '                                    objOutLook = New COutlook
        '                                End If
        '                                objOutLook.ActivityID = CInt(txtActivityID.Text)
        '                                objOutLook.RemoveActivity()
        '                            End If
        '                        End If
        '                    End If
        '                End If

        '            Else

        '                'If objContacts Is Nothing Then
        '                '    objContacts = New CContacts
        '                'End If
        '                'If Len(Trim(GetQueryStringVal("CommId"))) = 0 Then
        '                '    If chkOutlook.Checked = True Then
        '                '        If objOutLook Is Nothing Then
        '                '            objOutLook = New COutlook
        '                '        End If
        '                '        objOutLook.UserCntID = ddlUserNames.SelectedItem.Value
        '                '        objOutLook.DomainID = Session("DomainId")
        '                '        Dim dtTable As DataTable
        '                '        dtTable = objOutLook.GetResourceId
        '                '        If dtTable.Rows.Count > 0 Then
        '                '            objOutLook.AllDayEvent = False
        '                '            objOutLook.ActivityDescription = strDesc
        '                '            objOutLook.Location = ""
        '                '            objOutLook.Subject = ddlType.SelectedItem.Text
        '                '            objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
        '                '            objOutLook.StartDateTimeUtc = strSplitStartTimeDate
        '                '            objOutLook.EnableReminder = True
        '                '            objOutLook.ReminderInterval = 900
        '                '            objOutLook.ShowTimeAs = 3
        '                '            objOutLook.Importance = 2
        '                '            objOutLook.RecurrenceKey = -999
        '                '            objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
        '                '            If txtActivityID.Text = "0" Then
        '                '                txtActivityID.Text = CStr(objOutLook.AddActivity())
        '                '            Else

        '                '                objOutLook.ActivityID = CInt(txtActivityID.Text)
        '                '                objOutLook.UpdateActivity()
        '                '            End If
        '                '        End If

        '                '        'strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), txtcomments.Text)
        '                '    End If
        '                'Else
        '                '    If chkClose.Checked = True Then
        '                '        If objOutLook Is Nothing Then
        '                '            objOutLook = New COutlook
        '                '        End If
        '                '        objOutLook.ActivityID = CInt(txtActivityID.Text)
        '                '        objOutLook.RemoveActivity()
        '                '        ''For deleting the corresponding calendar entry in the Exchange database.
        '                '        'clsProspects.fn_DeleteOWAActionFromEx(Session("SiteType"), Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), HidStrEml.Value)
        '                '        'strCalendar = HidStrEml.Value
        '                '    Else
        '                '        If chkOutlook.Checked = True Then
        '                '            If objOutLook Is Nothing Then
        '                '                objOutLook = New COutlook
        '                '            End If
        '                '            objOutLook.UserCntID = ddlUserNames.SelectedItem.Value
        '                '            objOutLook.DomainID = Session("DomainId")
        '                '            Dim dtTable As DataTable
        '                '            dtTable = objOutLook.GetResourceId
        '                '            If dtTable.Rows.Count > 0 Then
        '                '                objOutLook.AllDayEvent = False
        '                '                objOutLook.ActivityDescription = strDesc
        '                '                objOutLook.Location = ""
        '                '                objOutLook.Subject = ddlType.SelectedItem.Text
        '                '                objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
        '                '                objOutLook.StartDateTimeUtc = strSplitStartTimeDate
        '                '                objOutLook.EnableReminder = True
        '                '                objOutLook.ReminderInterval = 900
        '                '                objOutLook.ShowTimeAs = 3
        '                '                objOutLook.Importance = 2
        '                '                objOutLook.RecurrenceKey = -999
        '                '                objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
        '                '                If txtActivityID.Text = "0" Then
        '                '                    txtActivityID.Text = CStr(objOutLook.AddActivity())
        '                '                Else

        '                '                    objOutLook.ActivityID = CInt(txtActivityID.Text)
        '                '                    objOutLook.UpdateActivity()
        '                '                End If
        '                '            End If

        '                '            'clsProspects.fn_DeleteOWAActionFromEx(Session("SiteType"), Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), HidStrEml.Value)
        '                '            'strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), txtcomments.Text)
        '                '        End If
        '                '    End If
        '                'End If

        '            End If

        '            If Session("AttendeeTable") IsNot Nothing Then
        '                'If chkOutlook.Checked = True Or chkClose.Checked = True Then
        '                '    objOutLook = New COutlook

        '                '    dsTemp = Session("AttendeeTable")
        '                '    Dim dtAttendee As DataTable
        '                '    dtAttendee = dsTemp.Tables(0)

        '                '    For Each dr As DataRow In dtAttendee.Rows
        '                '        If dr("tinUserType") = "1" Then
        '                '            If ddlUserNames.SelectedItem.Value <> dr("numContactID") Then

        '                '                objOutLook = New COutlook

        '                '                If CCommon.ToLong(hdnCommunicationID.Value) > 0 Then
        '                '                    objActionItem = New ActionItem

        '                '                    objActionItem.CommID = CCommon.ToLong(hdnCommunicationID.Value)
        '                '                    objActionItem.UserCntID = dr("numContactID")

        '                '                    Dim dtAttendeeActivity As DataTable
        '                '                    dtAttendeeActivity = objActionItem.CommunicationAttendees_Detail

        '                '                    If dtAttendeeActivity.Rows.Count > 0 Then
        '                '                        objOutLook.ActivityID = dtAttendeeActivity.Rows(0).Item("ActivityID")
        '                '                    End If
        '                '                End If

        '                '                If chkClose.Checked = True And objOutLook.ActivityID > 0 Then
        '                '                    objOutLook.RemoveActivity()
        '                '                ElseIf chkOutlook.Checked = True Then
        '                '                    objOutLook.UserCntID = dr("numContactID")
        '                '                    objOutLook.DomainID = Session("DomainId")
        '                '                    Dim dtTable As DataTable
        '                '                    dtTable = objOutLook.GetResourceId

        '                '                    If dtTable.Rows.Count > 0 Then
        '                '                        objOutLook.AllDayEvent = False
        '                '                        objOutLook.ActivityDescription = strDesc
        '                '                        objOutLook.Location = ""
        '                '                        objOutLook.Subject = ddlType.SelectedItem.Text
        '                '                        objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
        '                '                        objOutLook.StartDateTimeUtc = strSplitStartTimeDate
        '                '                        objOutLook.EnableReminder = True
        '                '                        objOutLook.ReminderInterval = 900
        '                '                        objOutLook.ShowTimeAs = 3
        '                '                        objOutLook.Importance = 2
        '                '                        objOutLook.RecurrenceKey = -999
        '                '                        objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
        '                '                        If objOutLook.ActivityID = 0 Then
        '                '                            dr("ActivityID") = objOutLook.AddActivity()
        '                '                        Else
        '                '                            dr("ActivityID") = objOutLook.ActivityID
        '                '                            objOutLook.UpdateActivity()
        '                '                        End If
        '                '                    End If
        '                '                End If
        '                '            End If
        '                '        End If
        '                '    Next

        '                '    dsTemp.AcceptChanges()
        '                'End If
        '            End If

        '        End If
        '        Dim intAssignTo, intEndTime, intTime, intEndChk, intChk As Integer
        '        Dim strFollowUp As String


        '        If ddlUserNames.Enabled = True Then
        '            intAssignTo = ddlUserNames.SelectedItem.Value
        '            intEndTime = radcmbStartTime.SelectedItem.Value
        '            intEndChk = IIf(chkEndAM.Checked = True, 1, 0)
        '            intTime = radcmbEndTime.SelectedItem.Value
        '            intChk = IIf(chkAM.Checked = True, 1, 0)
        '        End If

        '        objActionItem = New ActionItem

        '        With objActionItem
        '            .CommID = CCommon.ToLong(hdnCommunicationID.Value)
        '            .Task = intTaskFlg
        '            .Details = txtcomments.Text
        '            .AssignedTo = intAssignTo
        '            .UserCntID = Session("UserContactID")
        '            .DomainID = Session("DomainID")
        '            .BitClosed = IIf(chkClose.Checked = True, 1, 0)
        '            .CalendarName = strCalendar
        '            If CCommon.ToLong(ddlType.SelectedValue) = 973 Then
        '                .StartTime = IIf(radDueDate.SelectedDate Is Nothing, DateTime.Now.Date, radDueDate.SelectedDate)
        '                .EndTime = DateAdd(DateInterval.Day, 1, .StartTime)
        '            Else
        '                .StartTime = strStartTime
        '                .EndTime = strEndTime
        '            End If
        '            .Activity = ddlActivity.SelectedItem.Value
        '            .Status = ddlStatus.SelectedItem.Value
        '            .Remainder = ddlMinutes.SelectedItem.Value
        '            .RemainderStatus = CCommon.ToShort(chkRemindMe.Checked)
        '            .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        '            .bitOutlook = IIf(chkOutlook.Checked = True, 1, 0)
        '            .CaseID = lngCaseid
        '            .CaseTimeId = 0
        '            .CaseExpId = 0
        '            .ActivityId = CInt(txtActivityID.Text)
        '            .FollowUpAnyTime = IIf(chkFollowUp.Checked = True, 1, 0)

        '            If Session("AttendeeTable") IsNot Nothing Then
        '                dsTemp = Session("AttendeeTable")
        '                dsTemp.Tables(0).TableName = "AttendeeTable"
        '                .strAttendee = dsTemp.GetXml
        '            End If

        '            .numLinkedOrganization = IIf(String.IsNullOrEmpty(hdnLinkedOrg.Value), 0, CCommon.ToLong(hdnLinkedOrg.Value))
        '            .numLinkedContact = IIf(String.IsNullOrEmpty(hdnLinkedOrgContact.Value), 0, CCommon.ToLong(hdnLinkedOrgContact.Value))
        '        End With
        '        objActionItem.SaveCommunicationinfo()
        '        ''Add To Correspondense if Open record is selected

        '        'Added By Sachin Sadhu||Date:4thAug2014
        '        'Purpose :To Added Ticker data in work Flow queue based on created Rules
        '        '          Using Change tracking
        '        Dim objWfA As New Workflow()
        '        objWfA.DomainID = Session("DomainID")
        '        objWfA.UserCntID = Session("UserContactID")
        '        objWfA.RecordID = CCommon.ToLong(hdnCommunicationID.Value)
        '        objWfA.SaveWFActionItemsQueue()
        '        'ss//end of code

        '        'Update FollowUp Status -Bug ID 270
        '        Dim objAcc As New BACRM.BusinessLogic.Account.CAccounts
        '        objAcc.ContactID = hdnContactID.Value
        '        objAcc.FollowUpStatus = ddlFollowUpStatus.SelectedValue
        '        objAcc.UpdateFollowUpStatus()

        '        ''Sending mail to Assigneee
        '        If Session("UserContactID") <> ddlUserNames.SelectedValue Then
        '            If ddlUserNames.SelectedValue > 0 And ViewState("AssignedTo") <> ddlUserNames.SelectedValue Then
        '                CAlerts.SendAlertToAssignee(6, Session("UserContactID"), ddlUserNames.SelectedValue, CCommon.ToLong(hdnCommunicationID.Value), Session("DomainID")) 'bugid 767
        '                ViewState("AssignedTo") = ddlUserNames.SelectedValue
        '            End If
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Public Sub AddToRecentlyViewed(ByVal RecordType As RecetlyViewdRecordType, RecordID As Long)
            Try
                Dim objCommon As New CCommon()
                If objCommon Is Nothing Then objCommon = New CCommon
                Select Case RecordType
                    Case RecetlyViewdRecordType.Account_Pospect_Lead
                        objCommon.charModule = "C"
                    Case RecetlyViewdRecordType.ActionItem
                        objCommon.charModule = "A"
                    Case RecetlyViewdRecordType.Document
                        objCommon.charModule = "D"
                    Case RecetlyViewdRecordType.Opportunity
                        objCommon.charModule = "O"
                    Case RecetlyViewdRecordType.Support
                        objCommon.charModule = "S"
                    Case RecetlyViewdRecordType.Project
                        objCommon.charModule = "P"
                    Case RecetlyViewdRecordType.AssetItem
                        objCommon.charModule = "AI"
                    Case RecetlyViewdRecordType.Contact
                        objCommon.charModule = "U"
                    Case RecetlyViewdRecordType.Item
                        objCommon.charModule = "I"
                    Case RecetlyViewdRecordType.Campaign
                        objCommon.charModule = "M"
                    Case RecetlyViewdRecordType.ReturnMA
                        objCommon.charModule = "R"
                    Case RecetlyViewdRecordType.BizDoc
                        objCommon.charModule = "B"
                    Case Else
                End Select
                objCommon.RecordId = RecordID
                objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objCommon.AddVisiteddetails()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlActionItemTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlActionItemTemplate.SelectedIndexChanged
            Try
                If ddlActionItemTemplate.SelectedIndex > 0 Then
                    editActionItem.InnerHtml = "Edit"
                    BindTemplateData(Convert.ToInt32(ddlActionItemTemplate.SelectedValue))
                Else
                    editActionItem.InnerHtml = "Add"
                    Dim strDate As Date = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    txtcomments.Text = ""
                    radDueDate.SelectedDate = strDate
                    txtcomments.Text = ""
                    ddlStatus.SelectedIndex = 0
                    ddlActivity.SelectedIndex = 0
                    ddlType.SelectedIndex = 0
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub BindTemplateData(ByVal actionItemID As Long)
            Try
                If objActionItem Is Nothing Then objActionItem = New ActionItem
                With objActionItem
                    .RowID = actionItemID
                End With
                Dim thisActionItemTable As DataTable = objActionItem.LoadThisActionItemTemplateData()

                If Not thisActionItemTable Is Nothing AndAlso thisActionItemTable.Rows.Count > 0 Then
                    Dim strDate As String = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).AddDays(Convert.ToDouble(thisActionItemTable.Rows(0)("DueDays")))
                    radDueDate.SelectedDate = strDate
                    txtcomments.Text = thisActionItemTable.Rows(0)("Comments")
                    ddlStatus.SelectedItem.Selected = False
                    ddlActivity.SelectedItem.Selected = False
                    ddlType.SelectedItem.Selected = False

                    If Not ddlStatus.Items.FindByValue(thisActionItemTable.Rows(0)("Priority")) Is Nothing Then
                        ddlStatus.Items.FindByValue(thisActionItemTable.Rows(0)("Priority")).Selected = True
                    End If
                    If Not ddlActivity.Items.FindByValue(thisActionItemTable.Rows(0)("Activity")) Is Nothing Then
                        ddlActivity.Items.FindByValue(thisActionItemTable.Rows(0)("Activity")).Selected = True
                    End If
                    If Not ddlType.Items.FindByValue(thisActionItemTable.Rows(0)("numTaskType")) Is Nothing Then
                        ddlType.Items.FindByValue(thisActionItemTable.Rows(0)("numTaskType")).Selected = True
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class

End Namespace