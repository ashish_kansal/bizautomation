﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmHelp.aspx.vb" Inherits=".frmHelp" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Help</title>
    <style type="text/css">
        #divShipVia ul.nav {
            position: relative;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: -moz-flex;
            display: -ms-flex;
            display: flex;
        }
        #divShipVia{
            border-bottom: 1px solid #0073b7;
        }
            #divShipVia ul.nav li {
                border: 1px solid #0073b7;
                -webkit-flex: 1;
                -moz-flex: 1;
                -ms-flex: 1;
                flex: 1;
                text-align: center;
            }

                #divShipVia ul.nav li:not(:last-child) {
                    border-right: none;
                }

                #divShipVia ul.nav li.active {
                    border-top-color: #0073b7;
                    border-bottom: none;
                    background-color: #fff !important;
                }

                #divShipVia ul.nav li a, #divShipVia ul.nav li a:active, #divShipVia ul.nav li a:focus, #divShipVia ul.nav li a:hover {
                    border: none;
                    color: #3c8dbc;
                    border-radius: 0px;
                    margin: 0px;
                }

                  #divShipVia ul.nav li a {
                    color: #FFF;
                }

                #divShipVia ul.nav li.active a {
                    background: none;
                    color: #0073b7;
                    font-weight:bold;
                }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <%--    <script src="../JavaScript/ckEditor/ckeditor.js"></script>--%>
    <script src="https://cdn.ckeditor.com/4.7.3/full/ckeditor.js"></script>
    <link href="../JavaScript/ckEditor/toolbarconfigurator/lib/codemirror/neo.css" rel="stylesheet" />
    <script>
        //CKEDITOR.disableAutoInline = tr;


        var bitAllowToEditHelpFile = '<%=Session("bitAllowToEditHelpFile")%>'
        $(document).ready(function () {
            CKEDITOR.inline('editor1', {
                // Define the toolbar groups as it is a more accessible solution.
                toolbarGroups: [{
                    "name": "basicstyles",
                    "groups": ["basicstyles"]
                },
                {
                    "name": "links",
                    "groups": ["links"]
                },
                {
                    "name": "paragraph",
                    "groups": ["list", "blocks"]
                },
                {
                    "name": "document",
                    "groups": ["mode"]
                },
                {
                    "name": "insert",
                    "groups": ["insert"]
                },
                {
                    "name": "styles",
                    "groups": ["styles"]
                },
                {
                    "name": "about",
                    "groups": ["about"]
                }
                ],
                // Remove the redundant buttons from toolbar groups defined above.
                removeButtons: 'Strike,Subscript,Superscript,Anchor,Styles,Specialchar,PasteFromWord'
            }
            ,{
                filebrowserImageUploadUrl: '../ECommerce/ckEditorImageUpload.ashx?DomainId=<%=Session("DomainID")%>'
            });

            loadContent(true, ($("[id$=hdnSelectedTab]").val() == "True" ? true : false));
        })
    </script>
    <script>
        function loadContent(isInitialLoad, IsSpecificContent) {
            if (!isInitialLoad) {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/SavePersistTable',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "isMasterPage": true
                        , "boolOnlyURL": false
                        , "strParam": "frmHelp.aspx"
                        , "strPageName": ""
                        , "values": "[{\"key\": \"IsSpecificContent\",\"value\" : " + IsSpecificContent + "}]"
                    }),
                    error: function (jqXHR, textStatus, errorThrown) {

                    }
                });
            }
            
            if (IsSpecificContent == false && bitAllowToEditHelpFile == "False") {
                $("#btnPublish").css("display", "none");
                $("#ligeneral").addClass("active");
                $("#lispecific").removeClass("active");
            }
            else if (IsSpecificContent == false && bitAllowToEditHelpFile == "True") {
                $("#btnPublish").css("display", "inline");
                $("#ligeneral").addClass("active");
                $("#lispecific").removeClass("active");
            }
            else {
                $("#btnPublish").css("display", "inline");
                $("#ligeneral").removeClass("active");
                $("#lispecific").addClass("active");
            }
            CKEDITOR.instances['editor1'].setData("Sample Content<br/><br/><br/><br/><br/><br/><br/><br/><br/>");
            $("[id$=hdnSpecificContent]").val(IsSpecificContent ? "true" : "false");
            var ContentKey = $("#<%=hdnContentKey.ClientID%>").val()
            var DomainID = <%= Session("DomainId")%>;
            var dataParam = "{DomainID:'" + DomainID + "',ContentKey:'" + ContentKey + "',IsSpecificContent:" + IsSpecificContent + "}";
            $.ajax({
                type: "POST",
                url: "../Ecommerce/frmContentManagement.aspx/WebMethodGetBizHelpContents",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse != null && Jresponse.length > 0) {
                        CKEDITOR.instances['editor1'].setData(Jresponse[0].vccontent);
                    }
                }
            });
        }
        function saveContent() {
            var ContentKey = $("#<%= hdnContentKey.ClientID%>").val();
            var ckdata = CKEDITOR.instances['editor1'].getData();
            $("#<%= hdnContentPublish.ClientID%>").val(ckdata);
            $("#<%= btnSave.ClientID%>").click();

           <%-- var DomainID = <%= Session("DomainId")%>;
            var dataParam = "{DomainID:'" + DomainID + "',ContentKey:'" + ContentKey + "',IsSpecificContent:" + $("#<%= hdnSpecificContent.ClientID%>").val() + ",vcContent:'" + ckdata+"'}";
            $.ajax({
                type: "POST",
                url: "../Ecommerce/frmContentManagement.aspx/WebMethodSaveBizHelpContents",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);

                }
            });--%>
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:HiddenField ID="hdnSpecificContent" runat="server" />
    <asp:HiddenField ID="hdnContentPublish" runat="server" />
    <asp:HiddenField ID="hdnContentKey" runat="server" />
     <asp:HiddenField ID="hdnSelectedTab" runat="server" />
    <div id="divShipVia">
     <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#listDetails" onclick="loadContent(false,false)">General Help</a></li>
        <li><a data-toggle="tab" href="#listDetails" onclick="loadContent(false,true)">Specific to Us</a></li>
    </ul>
    <div class="tab-content">
        <div id="listDetails" class="tab-pane fade in active">
            <div class="pull-right">
                <br />
                <button class="btn btn-sm btn-primary" id="btnPublish" type="button" onclick="saveContent()">Publish</button>
                <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" style="display:none" />
            </div>
            <div class="col-md-12">
          <%--  <div id="editor1" >
    <h1>Inline Editing in Action!</h1>
    <p>The "div" element that contains this text is now editable.</p>
</div>--%><br />
     <textarea name="editor1" id="editor1">&lt;p&gt;Initial editor content.&lt;/p&gt;<br /><br /><br /><br /><br /><br /><br /><br /><br /></textarea>
                
                </div>
            </div>

            <div id="ddlRelationShip" class="tab-pane fade">
            </div>
        </div>
    </div>
</asp:Content>
