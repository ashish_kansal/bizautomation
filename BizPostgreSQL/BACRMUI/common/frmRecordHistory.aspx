﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmRecordHistory.aspx.vb" Inherits=".frmRecordHistory" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript">
        function fn_doPage(pgno, CurrentPageWebControlClientID, PostbackButtonClientID) {
            var currentPage;
            var postbackButton;

            if (CurrentPageWebControlClientID != '') {
                currentPage = CurrentPageWebControlClientID;
            }
            else {
                currentPage = 'txtCurrentPageCorr';
            }

            if (PostbackButtonClientID != '') {
                postbackButton = PostbackButtonClientID;
            }
            else {
                postbackButton = 'btnCorresGo';
            }

            $('#' + currentPage + '').val(pgno);
            if ($('#' + currentPage + '').val() != 0 || $('#' + currentPage + '').val() > 0) {
                $('#' + postbackButton + '').trigger('click');
            }
        }
        function dateSelected(sender, eventArgs) {
            $("[id$=btnFilter]").click();
        }
        function OnClientSelectedIndexChanged(sender, eventArgs) {
            $("[id$=btnFilter]").click();
            return false;
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            $('.txtFilterOrder').on("keypress", function (e) {
                if (e.keyCode == 13) {
                    $("[id$=btnFilter]").click();
                    return false;
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server" ClientIDMode="Static">
<%--    <div class="pull-left">
        <asp:CheckBox ID="chkOnlyAvgCost" runat="server" Text="Biz Calculated Average Cost" AutoPostBack="true" />
    </div>--%>
    <div class="pull-right">
        <ul class="list-inline">
            <li>
                <webdiyer:AspNetPager
                    ID="bizPager"
                    runat="server"
                    PagingButtonSpacing="0"
                    CssClass="bizgridpager"
                    AlwaysShow="true"
                    CurrentPageButtonClass="active"
                    PagingButtonUlLayoutClass="pagination"
                    PagingButtonLayoutType="UnorderedList"
                    FirstPageText="<<"
                    LastPageText=">>"
                    NextPageText=">"
                    PrevPageText="<"
                    Width="100%"
                    HorizontalAlign="Right"
                    NumericButtonCount="5"
                    CustomInfoSectionWidth="300"
                    LayoutType="div" UrlPaging="false" ShowMoreButtons="true" ShowPageIndexBox="Never"
                    ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
                    CustomInfoStyle="line-height:35px;margin-right:3px;text-align:right !important;font-weight:bold" CurrentPageWebControlClientID="txtCurrrentPage" PostbackButtonClientID="btnGo1">
                </webdiyer:AspNetPager>
            </li>
            <li style="vertical-align: top; margin-top: 5px;">
                <asp:LinkButton runat="server" ID="lbClearGridCondition" OnClick="lbClearGridCondition_Click" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></asp:LinkButton>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server">History</asp:Label>
    <%--  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:Label ID="lblTitle" runat="server">History</asp:Label>
            </td>
            <td align="right" valign="middle" style="font-size: 12px;">
                <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
                    LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
                    Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
                    CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;" CurrentPageWebControlClientID="txtCurrrentPage" PostbackButtonClientID="btnGo1">
                </webdiyer:AspNetPager>
            </td>
        </tr>
    </table>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <telerik:RadScriptManager runat="server"></telerik:RadScriptManager>
    <%--<div style="width: 980px; text-align:center">--%>
    <asp:Label ID="lblNoData" runat="server" Text="No Data" Visible="false" Font-Bold="true"></asp:Label>
    <asp:GridView ID="gvHistory" AutoGenerateColumns="False" runat="server" Width="100%" CssClass="tbl">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" Height="25"></HeaderStyle>
        <Columns>
            <asp:TemplateField HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                <HeaderTemplate>
                    Date / Time 
                    <br />
                    <ul class="list-inline">
                        <li style="padding-right: 0px">
                            <telerik:RadDatePicker ID="radDateFrom" runat="server" Width="65" DateInput-CssClass="form-control" DateInput-AutoCompleteType="Disabled" DateInput-EmptyMessage="From" ShowPopupOnFocus="true" DatePopupButton-Visible="false">
                                 <ClientEvents OnDateSelected="dateSelected" />
                            </telerik:RadDatePicker>
                        </li>
                        <li style="padding-left: 0px">
                            <telerik:RadDatePicker ID="radDateTo" runat="server" Width="65" DateInput-CssClass="form-control" DateInput-AutoCompleteType="Disabled" DateInput-EmptyMessage="To" ShowPopupOnFocus="true" DatePopupButton-Visible="false">
                                 <ClientEvents OnDateSelected="dateSelected" />
                            </telerik:RadDatePicker>
                        </li>
                    </ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("vcAuditActionTime")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="25%" ItemStyle-Width="25%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                <HeaderTemplate>
                    Event
                    <br />
                    <asp:TextBox ID="vcEvent" runat="server" CheckBoxes="true" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"></asp:TextBox>
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("vcEvent")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="25%" ItemStyle-Width="25%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                <HeaderTemplate>
                    By
                    <br />
                    <asp:TextBox ID="vcUserName" runat="server" CheckBoxes="true" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"></asp:TextBox>
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("vcUserName")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="25%" ItemStyle-Width="25%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                <HeaderTemplate>
                    Field Name
                    <br />
                    <asp:TextBox ID="vcFieldName" runat="server" CheckBoxes="true" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"></asp:TextBox>
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("vcFieldName")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="vcOldValue" HeaderText="Old Value"></asp:BoundField>
            <asp:BoundField DataField="vcNewValue" HeaderText="New Value"></asp:BoundField>
            <asp:BoundField DataField="vcDescription" HeaderText="Description"></asp:BoundField>
            <%--<asp:BoundField DataField="vcEvent" HeaderText="Event"></asp:BoundField>
                <asp:BoundField DataField="vcUserName" HeaderText="By"></asp:BoundField>
                <asp:BoundField DataField="vcFieldName" HeaderText="Field Name"></asp:BoundField>
                <asp:BoundField DataField="vcOldValue" HeaderText="Old Value"></asp:BoundField>
                <asp:BoundField DataField="vcNewValue" HeaderText="New Value"></asp:BoundField>
                <asp:BoundField DataField="vcDescription" HeaderText="Description"></asp:BoundField>--%>
        </Columns>
    </asp:GridView>
    <%--   </div>--%>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:Button ID="btnFilter" runat="server" Style="display: none" OnClick="btnFilter_Click"/>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageDeals" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
