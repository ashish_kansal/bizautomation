﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCommunicationDetails.aspx.vb" MasterPageFile="~/common/DetailPage.Master"
    Inherits=".frmCommunicationDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
      <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-12 col-md-3" id="OrderCustomerDetails" runat="server">
            <div class="">
                <div class="form-inline">
                    <div class="pull-left" style="width: 90%">
                        <div class="callout calloutGroup bg-theme">
                            <asp:Label ID="lblCustomerType" CssClass="customerType" Text="Customer" runat="server"></asp:Label>
                            <span>
                                <u>
                                    <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink" Style="font-size: 16px !important; font-weight: bold !important; font-style: italic !important">
                                    </asp:HyperLink></u>

                                <asp:Label ID="lblRelationCustomerType" CssClass="customerType" runat="server"></asp:Label>


                            </span>
                        </div>
                        <div class="record-small-box record-small-group-box">
                            <strong>
                                <asp:Label ID="lblContactName" runat="server" CssClass="text-color-theme" Text=""></asp:Label></strong>
                            <span class="contact">
                                <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label></span>
                            <a id="btnSendEmail" runat="server" href="#">
                                <img src="../images/msg_unread_small.gif" />
                            </a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5">
            <div id="trRecordOwner" runat="server" class="row">

                <div class="record-small-box record-small-group-box createdBySection">
                    <a href="#">Created</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                    </span>
                    <a href="#">Modified</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                    </span>
                    <a href="#">Finish</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblFinishedBy" runat="server"></asp:Label>
                    </span>
                </div>

            </div>
        </div>
        <div class="col-md-4">
            <div class="pull-right createdBySection">
                <asp:LinkButton ID="btnFinish" runat="server" CssClass="btn btn-success" OnClick="btnFinish_Click"><i class="fa fa-flag-checkered"></i>&nbsp;&nbsp;Finish Work Order</asp:LinkButton>
                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger" OnClick="btnDelete_Click"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    Communication&nbsp;&nbsp;(<asp:Label ID="lblCommunicationName" runat="server" ForeColor="Gray" />)
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
     <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="100%" GridLines="none" border="0" runat="server" CssClass="table table-responsive tblNoBorder">
            </asp:Table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>