﻿Imports System.IO
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Public Class frmBizThemeConfg
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            chkCustomizedLogo.Checked = CCommon.ToBool(Session("bitLogoAppliedToBizTheme"))
            chkCustomizedLogoForLogin.Checked = CCommon.ToBool(Session("bitLogoAppliedToLoginBizTheme"))
            rdoColorConfg.SelectedValue = CCommon.ToString(Session("vcThemeClass"))
            txtURLForLoginScreen.Text = CCommon.ToString(Session("vcLoginURL"))
            Dim FilePath = CCommon.GetDocumentPath(Session("DomainID"))
            Dim strFilePath As String
            strFilePath = FilePath & CCommon.ToString(Session("vcLogoForBizTheme"))
            imgLogoFile.ImageUrl = strFilePath
            strFilePath = FilePath & CCommon.ToString(Session("vcLogoForLoginBizTheme"))
            imgLogoFileForLoginScreen.ImageUrl = strFilePath
            Try
                btnCancel.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Sub BizThemeColorUpload()
        Try
            Dim lobjUserAccess As New UserAccess
            Dim strFilePath As String
            If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
            End If
            Dim FilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID"))
            Dim ArrFilePath, ArrFileExt, ArrFileName
            Dim strFileExt, strFileName As String

            If txtMagFile.PostedFile.FileName <> "" Then
                strFilePath = FilePath
                ArrFileExt = Split(txtMagFile.PostedFile.FileName, ".")
                ArrFileName = Split(txtMagFile.PostedFile.FileName, "\")
                strFileExt = ArrFileExt(UBound(ArrFileExt))
                strFileName = "BizTemplateLogoFile" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & "." & strFileExt
                ''strFileName = "Logo.gif"
                strFilePath = strFilePath & strFileName
                txtMagFile.PostedFile.SaveAs(strFilePath)
                lobjUserAccess.vcLogoForBizTheme = strFileName
                Session("vcLogoForBizTheme") = strFileName
            End If
            If fileLogoFIleForLoginScreen.PostedFile.FileName <> "" Then
                strFilePath = FilePath
                ArrFileExt = Split(fileLogoFIleForLoginScreen.PostedFile.FileName, ".")
                ArrFileName = Split(fileLogoFIleForLoginScreen.PostedFile.FileName, "\")
                strFileExt = ArrFileExt(UBound(ArrFileExt))
                strFileName = "BizTemplateLoginLogoFile" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & "." & strFileExt
                ''strFileName = "Logo.gif"
                strFilePath = strFilePath & strFileName
                fileLogoFIleForLoginScreen.PostedFile.SaveAs(strFilePath)
                lobjUserAccess.vcLogoForLoginBizTheme = strFileName
                Session("vcLogoForLoginBizTheme") = strFileName
            End If


            lobjUserAccess.DomainID = Session("DomainID")

            lobjUserAccess.bitLogoAppliedToBizTheme = chkCustomizedLogo.Checked
            lobjUserAccess.bitLogoAppliedToLoginBizTheme = chkCustomizedLogoForLogin.Checked
            lobjUserAccess.vcThemeClass = rdoColorConfg.SelectedValue
            lobjUserAccess.vcLoginURL = txtURLForLoginScreen.Text
            Session("bitLogoAppliedToLoginBizTheme") = chkCustomizedLogoForLogin.Checked
            Session("bitLogoAppliedToBizTheme") = chkCustomizedLogo.Checked
            Session("vcThemeClass") = rdoColorConfg.SelectedValue
            Session("vcLoginURL") = txtURLForLoginScreen.Text
            lobjUserAccess.UpdateBizThemeColorDetails()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'commented by Sachin
            'MagUpload()
            'Added By Sachin Sadhu||Date:24thDec2013
            'Purpose:only BizDoc related pages save Logo in -BizDocTemplate table ,other pages save in  Domain Table
            BizThemeColorUpload()

            Response.Write("<script>opener.location.reload(true);window.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub lnkBtnValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkBtnValidate.Click
        Try
            Dim lobjUserAccess As New UserAccess
            lobjUserAccess.vcLoginURL = txtURLForLoginScreen.Text
            lobjUserAccess.DomainID = Session("DomainID")
            Dim dt As New DataTable()
            dt = lobjUserAccess.GetDomainDetailsByURL()
            If dt.Rows.Count > 0 Then
                lblStatus.Text = "Sorry! URL is not available.please try something different"
                lblStatus.CssClass = "text text-danger"
            Else
                lblStatus.Text = "Congrats! URL is Available to use for your domain login."
                lblStatus.CssClass = "text text-success"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class