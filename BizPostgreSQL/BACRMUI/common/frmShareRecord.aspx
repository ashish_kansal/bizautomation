﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmShareRecord.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmShareRecord" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Share Record</title>
    <script language="javascript" type="text/javascript">
        sortitems = 0;  // 0-False , 1-True
        function move(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    if (document.getElementById("chkAddContactRole").checked == true) {
                        if (document.getElementById("ddlContactRole").selectedIndex != 0) {
                            no.value = fbox.options[i].value.replace('~0', '~' + ddlContactRole.value);
                            no.text = fbox.options[i].text + '(' + ddlContactRole.options[ddlContactRole.selectedIndex].text; +')';
                        }
                        else {
                            no.value = fbox.options[i].value;
                            no.text = fbox.options[i].text;
                        }
                    }
                    else {
                        no.value = fbox.options[i].value;
                        no.text = fbox.options[i].text;
                    }

                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function remove1(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;
                        }
                    }

                    var no = new Option();

                    var splitValue = fbox.options[i].value.split('~');
                    if (splitValue[1] > 0) {
                        no.value = splitValue[0] + '~0';

                        var re = new RegExp("\\(([^\]*)\\)", "g");
                        no.text = fbox.options[i].text.replace(re, "");
                    }
                    else {
                        no.value = fbox.options[i].value;
                        no.text = fbox.options[i].text;
                    }

                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";
                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }


        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1); y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }

        function Save() {
            var str = '';
            for (var i = 0; i < document.getElementById("lstSelectedfld").options.length; i++) {
                var SelectedValue;
                SelectedValue = document.getElementById("lstSelectedfld").options[i].value;
                str = str + SelectedValue + ','
            }
            document.getElementById("hdnCol").value = str;
        }
		
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="input-part">
        <div class="right-input">
            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="50" />
            <input class="button" id="btnClose" style="width: 50" onclick="javascript:opener.location.reload(true);window.close()"
                type="button" value="Close">&nbsp;&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Share Record With
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <br />
    <asp:Table ID="tblShareRecord" runat="server" Width="600" GridLines="None" CssClass="aspTable"
        BorderColor="black" BorderWidth="1">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right" CssClass="normal11">
                Contact Role
                <asp:DropDownList ID="ddlContactRole" runat="server" CssClass="signup">
                </asp:DropDownList>
                <asp:CheckBox ID="chkAddContactRole" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" class="normal1" valign="top">
                            Available<br>
                            &nbsp;&nbsp;
                            <asp:ListBox ID="lstAvailablefld" runat="server" Width="150" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                        </td>
                        <td align="center" class="normal1" valign="middle">
                            <input type="button" id="btnAdd" class="button" value="Add >" onclick="javascript:move(document.getElementById('lstAvailablefld'),document.getElementById('lstSelectedfld'))">
                            <br>
                            <br>
                            <input type="button" id="btnRemove" class="button" value="< Remove" onclick="javascript:remove1(document.getElementById('lstSelectedfld'),document.getElementById('lstAvailablefld'));">
                        </td>
                        <td align="center" class="normal1">
                            Selected<br>
                            <asp:ListBox ID="lstSelectedfld" runat="server" Width="150" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <input id="hdnCol" type="hidden" name="hdXMLString" runat="server" value="" />
</asp:Content>
