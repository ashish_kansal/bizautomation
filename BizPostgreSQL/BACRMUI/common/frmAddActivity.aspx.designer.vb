﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Common

    Partial Public Class frmAddActivity

        '''<summary>
        '''ddlActionItemTemplate control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlActionItemTemplate As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''editActionItem control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents editActionItem As Global.System.Web.UI.HtmlControls.HtmlAnchor

        '''<summary>
        '''btnSave control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnClose control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''ScriptManager1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

        '''<summary>
        '''radCmbCompany control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radCmbCompany As Global.Telerik.Web.UI.RadComboBox

        '''<summary>
        '''ddlContact control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlContact As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''ddlUserNames control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlUserNames As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''radDueDate control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radDueDate As Global.BACRM.Include.calandar

        '''<summary>
        '''ddlType control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlType As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''lblFollowUp control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblFollowUp As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''chkFollowUp control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkFollowUp As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''tdStartTimeControl control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tdStartTimeControl As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''radcmbStartTime control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radcmbStartTime As Global.Telerik.Web.UI.RadComboBox

        '''<summary>
        '''chkAM control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkAM As Global.System.Web.UI.HtmlControls.HtmlInputRadioButton

        '''<summary>
        '''chkPM control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkPM As Global.System.Web.UI.HtmlControls.HtmlInputRadioButton

        '''<summary>
        '''tdEndTimeLabel control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tdEndTimeLabel As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''radcmbEndTime control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radcmbEndTime As Global.Telerik.Web.UI.RadComboBox

        '''<summary>
        '''chkEndAM control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkEndAM As Global.System.Web.UI.HtmlControls.HtmlInputRadioButton

        '''<summary>
        '''chkEndPM control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkEndPM As Global.System.Web.UI.HtmlControls.HtmlInputRadioButton

        '''<summary>
        '''ddlActivity control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlActivity As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''ddlStatus control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlStatus As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''txtcomments control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtcomments As Global.System.Web.UI.WebControls.TextBox
    End Class
End Namespace
