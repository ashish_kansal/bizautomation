﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="custompagelayout.ascx.vb" Inherits=".custompagelayout" %>
 <link rel="stylesheet" href="../css/lists.css" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../javascript/coordinates.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/drag.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/dragdrop.js"></script>
    <script language="JavaScript" type="text/javascript"><!--

    $(document).ready(function () {
        var max = $("#<%#rows.ClientID%>").val();
        
        for (var x = 0; x <= max; x++) {
            if (document.getElementById("x" + x) != null) {
                list = document.getElementById("x" + x);
                DragDrop.makeListContainer(list, 'g2');
                list.onDragOver = function () { this.style["background"] = "none"; };
                list.onDragOut = function () { this.style["background"] = "none"; };
            }
        }
    });

    function getSort() {
        order = $("#<%#order.ClientID%>");
        order.value = DragDrop.serData('g2', null);

    }

    function showValue() {
        order = $("#<%#order.ClientID%>");
        alert(order.value);
    }
    function show() {
        $("#<%#btnUpdate.ClientID%>").click();
        return false;

    }
    function show1() {
        $("#<%#btnUpdate1.ClientID%>").click();
        return false;
    }
   
    function Close() {
        opener.location.reload(true);
        window.close()
        return false;
    }
    function dlChange() {
        var mylist = document.getElementById("dlh")
        order = document.getElementById("type");
        order.value = mylist.options[mylist.selectedIndex].value
        $("#<%#btnBindData.ClientID%>").click();
        return false;
    }
    function ddlItemTypeChange() {
        var mylist = document.getElementById("ddlItemType")
        order = $("#<%#type.ClientID%>");
        order.value = mylist.options[mylist.selectedIndex].value
        $("#<%#btnBindData.ClientID%>").click();
        return false;
    }

    //-->
    </script>
 <asp:Label ID="lblTopAction" runat='server' Width="100%"></asp:Label>
    <asp:ScriptManager runat="server"></asp:ScriptManager>
<asp:Label ID="lblMainContent" runat='server' Width="100%"></asp:Label>


  <asp:Label ID="lblTitle" runat="server"></asp:Label>

  <asp:HiddenField ID="order" runat="server" ></asp:HiddenField>
    <asp:HiddenField ID="hfCtype" runat="server" />
    <asp:HiddenField ID="hfFormId" runat="server" />
    <asp:HiddenField ID="hfPType" runat="server" />
    <asp:TextBox runat="server" ID="rows" Style="display: none"></asp:TextBox>
    <asp:Button ID="maxRow" runat="server" Style="display: none" />
    <asp:Button ID="btnUpdate" Text="" runat="server" Style="display: none" />
    <asp:Button ID="btnUpdate1" Text="" runat="server" Style="display: none" />
    <asp:TextBox ID="type" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:Button ID="btnBindData" Text="update" runat="server" Style="display: none" />
   
    <script type="text/xml-script">
        <page xmlns:script="http://schemas.microsoft.com/xml-script/2005">
            <references>
            </references>
            <components>
            </components>
        </page>
    </script>