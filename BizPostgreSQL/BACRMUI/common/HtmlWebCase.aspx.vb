﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports System.IO
Imports System.Reflection
Imports System.Math
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Tracking
Imports System.Collections.Specialized
Imports BACRM.BusinessLogic.Case

Namespace BACRM.UserInterface.Common
    Public Class HtmlWebCase
        Inherits System.Web.UI.Page

        Private numDomainId As Long
        Private numGroupId As Long
        Private numFormId As Long
        Private vcSuccessURL, vcFailURL As String

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim uri As Uri
                Dim nvc As NameValueCollection = Request.Form

                If Request.UrlReferrer Is Nothing Then
                    If nvc("hfUrlReferrer") Is Nothing Then
                        Throw New Exception("Url Referrer not available.")
                    End If

                    uri = New Uri(nvc("hfUrlReferrer").ToString())
                Else
                    uri = Request.UrlReferrer
                End If

                If nvc("hfForm") Is Nothing Or nvc("hfDomain") Is Nothing Or nvc("hfGroup") Is Nothing Then
                    Response.Redirect(uri.AbsoluteUri & "?Error=Domain Form and group are not found", False)
                    Exit Sub
                End If

                If String.IsNullOrEmpty(nvc("hfForm")) Or String.IsNullOrEmpty(nvc("hfDomain")) Or String.IsNullOrEmpty(nvc("hfGroup")) Then
                    Response.Redirect(uri.AbsoluteUri & "?Error=Domain Form and group are not found", False)
                    Exit Sub
                End If

                Dim objEnc As New QueryStringValues

                numDomainId = objEnc.Decrypt(nvc("hfDomain"))
                numGroupId = objEnc.Decrypt(nvc("hfGroup"))
                numFormId = objEnc.Decrypt(nvc("hfForm"))

                'Validate URL
                Dim objFormWizard As FormConfigWizard
                objFormWizard = New FormConfigWizard
                objFormWizard.DomainID = numDomainId
                objFormWizard.FormID = numFormId
                objFormWizard.AuthenticationGroupID = numGroupId
                objFormWizard.tintType = 2
                Dim dtList As DataTable
                dtList = objFormWizard.ManageHTMLFormURL()
                Dim drBizDocItems() As DataRow = dtList.Select("vcURL  ilike '%" & uri.Host.ToString & "%'")

                If drBizDocItems.Length = 0 Then
                    Response.Redirect(uri.AbsoluteUri & "?Error=From URL is not specified", False)
                    Exit Sub
                Else
                    vcSuccessURL = drBizDocItems(0).Item("vcSuccessURL")
                    vcFailURL = drBizDocItems(0).Item("vcFailURL")
                End If

                Dim objCases As New CCases
                Dim lnDivID, lngCMPID, lnCntID, lngRecOwner As Long
                Dim dRow As DataRow
                Dim ds As New DataSet

                Dim dtGenericFormConfig As New DataTable
                dtGenericFormConfig.Columns.Add("vcDbColumnName")
                dtGenericFormConfig.Columns.Add("vcDbColumnText")

                Dim dtCusTable_C As New DataTable
                dtCusTable_C.Columns.Add("FldDTLID")
                dtCusTable_C.Columns.Add("fld_id")
                dtCusTable_C.Columns.Add("Value")

                Dim dtCusTable_D As New DataTable
                dtCusTable_D = dtCusTable_C.Clone()
                Dim strdetails As String
                Dim nvPairs As String()

                For Each myKey As String In nvc.AllKeys
                    'Response.Write("<br />" & myKey & "-" & nvc(myKey))
                    nvPairs = myKey.Split("_")

                    If nvPairs.Length > 1 Then
                        If nvPairs(0).Contains("cbListAOI") Then
                            dRow = dtGenericFormConfig.NewRow
                            dRow("vcDbColumnName") = "AOI"
                            dRow("vcDbColumnText") = nvc(myKey)
                            dtGenericFormConfig.Rows.Add(dRow)
                        ElseIf nvPairs(1).Contains("C") Then
                            dRow = dtCusTable_C.NewRow
                            dRow("FldDTLID") = 0
                            dRow("fld_id") = nvPairs(1).Replace("C", "")
                            dRow("Value") = nvc(myKey)
                            dtCusTable_C.Rows.Add(dRow)
                        ElseIf nvPairs(1).Contains("_D") Then
                            dRow = dtCusTable_D.NewRow
                            dRow("FldDTLID") = 0
                            dRow("fld_id") = nvPairs(1).Replace("D", "")
                            dRow("Value") = nvc(myKey)
                            dtCusTable_D.Rows.Add(dRow)
                        Else
                            AssignValuesSelectBox(objCases, nvc(myKey), nvPairs(0))
                            AssignValuesTextBox(objCases, nvc(myKey), nvPairs(0))

                            dRow = dtGenericFormConfig.NewRow
                            dRow("vcDbColumnName") = nvPairs(0)
                            dRow("vcDbColumnText") = nvc(myKey)
                            dtGenericFormConfig.Rows.Add(dRow)
                        End If
                    End If
                Next

                objCases.DomainID = numDomainId
                'Ip to country , attach Billing country by default from ip address
                Dim objBusinessClass As New BusinessClass
                objBusinessClass.IPNo = objBusinessClass.GetIPNumberFromIPAddress(Request.UserHostAddress)
                'Response.Write("your country is " + objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString")))
                Dim importWiz As New ImportWizard
                'Dim lngCountryID As Long = importWiz.GetStateAndCountry(0, objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString")), numDomainId)
                'If lngCountryID > 0 Then
                '    objCases.Country = lngCountryID
                'End If

                dtGenericFormConfig.TableName = "Table"


                Dim objAutoRoutRles As New AutoRoutingRules
                objAutoRoutRles.DomainID = numDomainId

                ds.Tables.Add(dtGenericFormConfig.Copy)
                objAutoRoutRles.strValues = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                Dim CaseNo As Long

                CaseNo = objCases.GetMaxCaseID
                objCases.CaseNo = Format(CaseNo, "00000000000")
                objCases.DomainID = numDomainId
                objCases.SaveCasesFromWeb()
                ''Saving CustomFields

                If dtCusTable_C.Rows.Count > 0 Then
                    dtCusTable_C.TableName = "Table"
                    ds.Tables.Add(dtCusTable_C.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjCusfld As New CustomFields
                    ObjCusfld.strDetails = strdetails
                    ObjCusfld.RecordId = lnCntID
                    ObjCusfld.locId = 4
                    ObjCusfld.SaveCustomFldsByRecId()
                End If


                If dtCusTable_D.Rows.Count > 0 Then
                    dtCusTable_D.TableName = "Table"
                    ds.Tables.Add(dtCusTable_D.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjCusfld As New CustomFields
                    ObjCusfld.strDetails = strdetails
                    ObjCusfld.RecordId = lnDivID
                    ObjCusfld.locId = 1
                    ObjCusfld.SaveCustomFldsByRecId()
                End If

                Dim dtTableAOI As New DataTable
                dtTableAOI.Columns.Add("numAOIId")
                dtTableAOI.Columns.Add("Status")
                Dim drAOI() As DataRow = dtGenericFormConfig.Select("vcDbColumnName='AOI'")

                If drAOI.Length > 0 Then
                    For Each dr As DataRow In drAOI
                        dRow = dtTableAOI.NewRow
                        dRow("numAOIId") = dr("vcDbColumnText")
                        dRow("Status") = 1
                        dtTableAOI.Rows.Add(dRow)
                    Next

                    dtTableAOI.TableName = "Table"
                    ds.Tables.Add(dtTableAOI.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjContacts As New CContacts
                    ObjContacts.strAOI = strdetails
                    ObjContacts.ContactID = lnCntID
                    ObjContacts.SaveAOI()
                End If

                Response.Redirect(vcSuccessURL, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                If Not String.IsNullOrEmpty(vcFailURL) Then
                    Response.Redirect(vcFailURL)
                End If
            End Try
        End Sub


        Sub AssignValuesTextBox(ByVal objCase As CCases, ByVal vcValue As String, ByVal vcColumnName As String)
            Select Case vcColumnName
                Case "numContactID"
                    objCase.Email = vcValue
                Case "textSubject"
                    objCase.Subject = vcValue
                Case "textDesc"
                    objCase.Desc = vcValue

            End Select
        End Sub

        Sub AssignValuesSelectBox(ByVal objCase As CCases, ByVal vcValue As String, ByVal vcColumnName As String)
            Try


                Select Case vcColumnName
                    Case "numPriority"
                        objCase.Priority = CLng(vcValue)
                    Case "numOrigin"
                        objCase.Orgin = CLng(vcValue)
                    Case "numType"
                        objCase.CaseType = CLng(vcValue)
                    Case "numReason"
                        objCase.Reason = CLng(vcValue)
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


    End Class
End Namespace