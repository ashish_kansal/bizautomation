﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmCollaborationUserControl.ascx.vb" Inherits="BACRM.UserInterface.Common.frmCollaborationUserControl" %>

<%-------------------------CSS--------------------------%>
<style>
    .commentsBox {
        background-color: #e1e1e1;
        color: #000 !important;
        padding: 2px;
    }

    .boxForCollabaration .box-body {
        padding: 0px !important;
        font-size: 14px !important;
    }

    .boxForCollabaration a {
        font-size: 14px !important;
    }

    .boxForCollabaration .commentsBox .box-title a {
        font-size: 14px !important;
        color: #000 !important;
        padding-top: 0px !important;
        padding-left: 0px !important;
    }

    .boxForCollabaration .box-body a {
        color: #000;
        font-style: italic;
    }

        .boxForCollabaration .box-body a.btn {
            color: #fff !important;
            font-style: normal !important;
            font-weight: bold !important;
        }

        .boxForCollabaration .box-body a.btn-default {
            color: #000 !important;
            font-weight: bold !important;
            font-style: normal !important;
        }

    .boxForCollabaration .box-header {
        padding: 1px !important;
    }

        .boxForCollabaration .box-header a.btn-default {
            font-weight: bold !important;
            background-color: rgb(0 0 0 / 20%);
            border: 1px solid rgb(0 0 0 / 23%);
        }

        .boxForCollabaration .box-header .box-title {
            font-size: 14px !important;
            font-style: italic !important;
        }

    .btnTransparent {
        background-color: transparent !important;
        border: 1px solid transparent !important;
    }

    .repliesHyperLink {
        color: #0b76bd !important;
        font-weight: bold;
    }

    .subCommentSection {
        width: 98%;
        margin-left: 2%;
        display: inline-block;
        border: 1px solid #e1e1e1;
    }

    .messageSection {
        width: 100%;
        display: inline-block;
    }

    .comments {
        width: 100%;
        display: inline-block;
    }

    .boxForCollabaration .internalBoxHeader {
        background-color: #fff !important;
        padding-top: 0px !important;
    }

    .internalBoxHeader p {
        margin: 0px !important;
    }

    .deleteImageIcons {
        height: 13px;
        border-width: 0px;
    }

    .repliesHyperLinkDelete {
        color: #0b76bd !important;
    }

    .CollabarationContent .boxForCollabaration {
        margin-bottom: 5px !important;
    }
</style>
<script src="../Collabaration/Collabaration.js"></script>
<script src="../JavaScript/daterangepicker-master/moment.min.js"></script>
<%--<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/jquery.tinymce.min.js" referrerpolicy="origin"></script>--%>
<script src="../JavaScript/tinymce/jquery.tinymce.min.js"></script>
<script src="../JavaScript/tinymce/tinymce.min.js"></script>
<script>

    $(document).ready(function () {
        LoadTinyMCE();
    });
    if (typeof (Sys) !== 'undefined') {
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler_Page);
    }

    /* fire this event to remove the existing editor and re-initialize it*/
    function EndRequestHandler_Page(sender, args) {
        //1. Remove the existing TinyMCE instance of TinyMCE
        tinymce.remove("#myTextareaCollabaration");
        //2. Re-init the TinyMCE editor
        LoadTinyMCE();
        getTopic();
    }

    function BeforePostback() {
        tinymce.triggerSave();
    }

    function LoadTinyMCE() {
        /* initialize the TinyMCE editor */
        tinymce.init({
            selector: "#myTextareaCollabaration",
            plugins: "link, autolink",
            default_link_target: "_blank",
            toolbar: "undo redo | bold italic | link unlink | cut copy paste | bullist numlist",
            menubar: false,
            statusbar: false,
            height: "150px"
        });
        debugger;
        if ($("#hdnExternalAccess").val() == "1") {
            $("#btnCollabarationSave").text("Post Reply");
            if ($("#hdnIsTopicMessage").val() == "0") {
                $("#commentsBox_" + $("#hdnParentMessageId").val() + "").css("background-color", "rgb(232 209 44 / 38%)");
            }
            $("#divTopicReplyToMessageContent").html(decodeURI($("#hdnSelectedMessageOrTopicExternal").val()));
            $("#divTopicReplyToMessageContentArea").css("display", "block");


        }
        if ($("#hdnRecordType").val() == "1" && $("#hdnExternalAccess").val() == "0") {
            setTimeout(function () {
                if ($find("radOppTab").get_selectedTab().get_text() == "Collaboration") {
                    tinyMCE.get('myTextareaCollabaration').selection.select(tinyMCE.get('myTextareaCollabaration').getBody(), true);
                    tinyMCE.get('myTextareaCollabaration').selection.collapse(false);
                    tinyMCE.get('myTextareaCollabaration').getBody().focus();
                }
            }, 1000);
        } else {
            setTimeout(function () {
                if (tinyMCE.get('myTextareaCollabaration').selection != null) {
                    tinyMCE.get('myTextareaCollabaration').selection.select(tinyMCE.get('myTextareaCollabaration').getBody(), true);
                    tinyMCE.get('myTextareaCollabaration').selection.collapse(false);
                }
                tinyMCE.get('myTextareaCollabaration').getBody().focus();
            }, 1000);
        }
    }

</script>
<div>
    <asp:HiddenField ID="hdnLoggedInUser" ClientIDMode="Static" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRecordId" ClientIDMode="Static" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDomainId" ClientIDMode="Static" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTopicId" ClientIDMode="Static" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRecordType" ClientIDMode="Static" runat="server" Value="0" />

    <asp:HiddenField ID="hdnIsTopicMessage" ClientIDMode="Static" runat="server" Value="0" />
    <asp:HiddenField ID="hdnParentMessageId" ClientIDMode="Static" runat="server" Value="0" />
    <asp:HiddenField ID="hdnCollabarationPortalDocUrl" ClientIDMode="Static" runat="server" Value="0" />
    <asp:HiddenField ID="hdnClientMachineUTCTimeOffset" ClientIDMode="Static" runat="server" Value="0" />
    <asp:HiddenField ID="hdnExternalAccess" ClientIDMode="Static" runat="server" Value="0" />
    <asp:HiddenField ID="hdnMessageId" ClientIDMode="Static" runat="server" Value="0" />

    <asp:HiddenField ID="hdnSelectedMessageOrTopicExternal" ClientIDMode="Static" runat="server" Value="0" />
    <div id="UpdateProgressCollabaration" style="display: none;" role="status" aria-hidden="true">

        <div class="overlay">
            <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                <i class="fa fa-2x fa-refresh fa-spin"></i>
                <h3>Processing Request</h3>
            </div>
        </div>

    </div>

    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline" id="divTopicReplyToMessageContentArea" style="background-color: rgb(232 209 44 / 38%); display: none; padding: 5px;">
                    <b style="float: left;">Replay to :</b>  <span id="divTopicReplyToMessageContent"></span>
                </div>
            </div>
            <div class="pull-right">
                <ul class="list-inline">
                    <li id="divCollabarationInternalThread">
                        <input type="checkbox" id="chkInternal" /><b><i>Internal Thread</i></b>
                    </li>
                    <li style="padding-right: 0px;padding-left: 0px;"><a class="btn btn-primary" id="btnCollabarationSave" href="javasript:void(0)" onclick="SaveTopicMessage();">Post New Topic</a></li>
                    <li style="padding-right: 0px;padding-left: 0px;">
                        <button class="btn btn-primary" id="btnCollabarationCancel" type="button" onclick="CancelTopicMessage();">Cancel</button></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <textarea name="myTextareaCollabaration" id="myTextareaCollabaration"></textarea>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12 text-left">
            <div class="form-inline">
                <input type="file" id="fileCollabarationUpload" onchange="Collabaration_UplodedFiles()" style="display: none" />
                <asp:ImageButton ID="imgOpenDocument" runat="server" ImageUrl="~/images/icons/drawer.png" Style="height: 30px;" />
                <asp:HyperLink ID="hplOpenDocument" runat="server" onclick="Collbaration_hplOpenDocumentClick()" Font-Bold="true" Font-Underline="true" Text="Attach" Style="display: inline-block; vertical-align: top; line-height: normal;"></asp:HyperLink>
                <br />
                <span id="spnCollbarationFilePath"></span>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <br />
            <div class="CollabarationContent">
            </div>
        </div>
    </div>
</div>
