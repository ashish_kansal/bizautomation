﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ECommerceMenuMaster

    '''<summary>
    '''head control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents head As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''radTabStripEcommerce control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents radTabStripEcommerce As Global.Telerik.Web.UI.RadTabStrip

    '''<summary>
    '''FiltersAndViews control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FiltersAndViews As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''FiltersAndViews1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FiltersAndViews1 As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''GridTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridTitle As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''BizPager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BizPager As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''GridSettingPopup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridSettingPopup As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''GridBizSorting control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridBizSorting As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''GridPlaceHolder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridPlaceHolder As Global.System.Web.UI.WebControls.ContentPlaceHolder
End Class
