﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmBizThemeConfg.aspx.vb" Inherits=".frmBizThemeConfg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Logo Upload</title>
    <script language="javascript">
        function Close() {
            window.close();
        }
        function Save() {
            if (document.form1.txtAmount.value == "") {
                alert("Enter Amount")
                document.form1.txtAmount.focus();
                return false;
            }
        }
    </script>
    <style>
        #rdoColorConfg tbody tr td {
            padding:10px;
        }
            #rdoColorConfg tbody tr td img {
                width:300px !important;
                height:40px !important;
            }
            .info {
    color: #00529b;
    background-color: #bde5f8;
    background-image: url(../images/info.png);
    list-style-type: none;
}
.info, .successInfo, .warning, .errorInfo, .validation {
    border: 1px solid;
    padding: 10px 5px 5px 50px;
    background-repeat: no-repeat;
    background-position: 10px center;
    display: list-item;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table width="100%">
                <tr>
                    <td align="right" colspan="3">
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
                        </asp:Button>
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button><br/>
                        <br />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Theme Configuration
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
        <table width="100%">
        <tr>
            <td class="normal1" style="vertical-align:top;width:250px">
             <label>   Select Image For Portal</label>
            </td>
            <td>
                <input id="txtMagFile" type="file" name="txtMagFile" runat="server" style="width: 200px" />
                 <asp:Panel runat="server" ID="Panel1" CssClass="info" >
        Tip: BizAutomation.com recommends resizing your brand logo to maximum dimension
        of 200px × 40px, any logo smaller than 200px × 40px shall work!
    </asp:Panel>
                <asp:Image ID="imgLogoFile" style="height:37px;" CssClass="img-responsive" runat="server" />
            </td>
            
        </tr>

        <tr>
            <td><label>Apply Logo to Portal</label></td>
            <td>
                 <asp:CheckBox ID="chkCustomizedLogo" runat="server" Text="" />
            </td>
        </tr>
            <tr>
            <td class="normal1" style="vertical-align:top;width:200px">
             <label>   Select Image For Login Screen</label>
            </td>
            <td>
                <input id="fileLogoFIleForLoginScreen" type="file" name="fileLogoFIleForLoginScreen" runat="server" style="width: 200px" />
                 <asp:Panel runat="server" ID="Panel2" CssClass="info" >
        Tip: BizAutomation.com recommends resizing your brand logo to maximum dimension
        of 200px × 40px, any logo smaller than 200px × 40px shall work!
    </asp:Panel>
                <asp:Image ID="imgLogoFileForLoginScreen" style="height:37px;" CssClass="img-responsive" runat="server" />
            </td>
            
        </tr>
             <tr>
            <td><label>Apply Logo to Login Screen</label></td>
            <td>
                 <asp:CheckBox ID="chkCustomizedLogoForLogin" runat="server" Text="" />
            </td>
        </tr>
        <tr>
            <td><label>Custom Login URL</label></td>
            <td>
                <div class="form-inline">
                    <span>http://login.bizautomation.com?org=</span>
                <asp:TextBox ID="txtURLForLoginScreen" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnkBtnValidate" runat="server" CssClass="btn btn-sm btn-default">Validate</asp:LinkButton>
                </div>
                <br />
                <asp:Label ID="lblStatus" runat="server" Text="" CssClass="text text-danger"></asp:Label>
            </td>
        </tr>
         
        <tr>
            <td style="vertical-align:top;width:200px"> <label>Please select theme color</label></td>
            <td>
               
                <asp:RadioButtonList ID="rdoColorConfg" runat="server">
                    <asp:ListItem Text='<img src="../images/Theme/LighterBlue.png" style="height:25px;" alt="img1" />' Value="0e99c4"></asp:ListItem>
                    <asp:ListItem Text='<img src="../images/Theme/LightBlue.png" style="height:25px;" alt="img1" />' Value="1473b4" Selected="True"></asp:ListItem>
                    <asp:ListItem Text='<img src="../images/Theme/Blue.png" style="height:25px;" alt="img1" />' Value="274b85"></asp:ListItem>
                    <asp:ListItem Text='<img src="../images/Theme/Red.png" style="height:25px;" alt="img1" />' Value="a80000"></asp:ListItem>
                    <asp:ListItem Text='<img src="../images/Theme/Purple.png" style="height:25px;" alt="img1" />' Value="5e285b"></asp:ListItem>
                    <asp:ListItem Text='<img src="../images/Theme/LightPurple.png" style="height:25px;" alt="img1" />' Value="5f4195"></asp:ListItem>
                    <asp:ListItem Text='<img src="../images/Theme/LightGreen.png" style="height:25px;" alt="img1" />' Value="40966d"></asp:ListItem>
                    <asp:ListItem Text='<img src="../images/Theme/Green.png" style="height:25px;" alt="img1" />' Value="21553c"></asp:ListItem>
                    <asp:ListItem Text='<img src="../images/Theme/LightYellow.png" style="height:25px;" alt="img1" />' Value="c06000"></asp:ListItem>
                    <asp:ListItem Text='<img src="../images/Theme/Yellow.png" style="height:25px;" alt="img1" />' Value="784308"></asp:ListItem>
                    <asp:ListItem Text='<img src="../images/Theme/LightCement.png" style="height:25px;" alt="img1" />' Value="808080"></asp:ListItem>
                    <asp:ListItem Text='<img src="../images/Theme/Cement.png" style="height:25px;" alt="img1" />' Value="3f3f3f"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlLogoTip" CssClass="info" Visible="false">
        Tip: BizAutomation.com recommends resizing your brand logo to maximum dimension
        of 200px × 37px, any logo smaller than 200px × 37px shall work!
    </asp:Panel>
</asp:Content>
