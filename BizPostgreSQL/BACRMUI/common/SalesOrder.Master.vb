﻿Public Class SalesOrder
    Inherits System.Web.UI.MasterPage

    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            Page.DataBind()
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            litScript.Text = "<script type='text/javascript'>windowResize();</script>"
        Else
            litScript.Text = ""
        End If

    End Sub

End Class