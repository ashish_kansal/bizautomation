﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmComments.ascx.vb"
    Inherits=".frmComments" %>
<script type="text/javascript">
    function SaveComment() {

        if (document.getElementById('txtHeading').value == 0) {
            alert("Enter Comment Heading")
            document.getElementById('txtHeading').focus();
            return false;
        }

    }
</script>
<style type="text/css">
    .tr1 {
        font-family: Arial;
        font-size: 8pt;
        font-style: normal;
        font-variant: normal;
        color: #000000;
        background: #F2F0F0;
        text-decoration: none;
    }

    .tr2 {
        font-family: Arial;
        font-size: 8pt;
        font-style: normal;
        font-variant: normal;
        color: #000000;
        background: white;
        text-decoration: none;
    }
</style>
<div class="row padbottom10">
    <div class="col-sm-12">
        <div class="pull-right">
            <asp:LinkButton ID="lnkbtnNoofCom" runat="server" CommandName="Show" CssClass="btn btn-primary"></asp:LinkButton>
            <asp:LinkButton ID="lnlbtnPost" runat="server" CommandName="Post" CssClass="btn btn-primary">Post Comment</asp:LinkButton>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="table-responsive">
            <asp:Table ID="tblComments" CssClass="table table-bordered table-striped" CellPadding="0" CellSpacing="0" runat="server" Width="100%" Visible="False">
            </asp:Table>
        </div>
    </div>
</div>
<asp:Panel ID="pnlPostComment" runat="server" Visible="False">
    <div class="row">
        <div class="col-xs-12">
            <label>Heading</label>
            <asp:TextBox ID="txtHeading" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Comment</label>
                <asp:TextBox ID="txtComments" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-center">
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit"></asp:Button>
        </div>
    </div>
</asp:Panel>
<asp:TextBox ID="txtLstUptCom" Style="display: none" runat="server"></asp:TextBox>
<asp:HiddenField ID="hdnCommentCount" runat="server" />

