﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmAssociatedContacts.ascx.vb"
    Inherits=".frmAssociatedContacts" ClientIDMode="Static" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function AddContact(index) {

        if ($find('radCmbCompany').get_value() == "") {
            alert("Select Customer")
            return false;
        }
        if (document.getElementById("ddlAssocContactId").value == 0) {
            alert("Select Contact");

            var radTab = $find("radOppTab");
            radTab.set_selectedIndex(index);

            document.getElementById("ddlAssocContactId").focus();
            return false;
        }

        var grid = document.getElementById("dgContact");
        if (grid.rows.length > 0) {
            //loop starts from 1. rows[0] points to the header.
            for (i = 1; i < grid.rows.length; i++) {
                //get the reference of first column
                cell = grid.rows[i].cells[5];
                //loop according to the number of childNodes in the cell
                for (j = 0; j < cell.childNodes.length; j++) {
                    //if childNode type is CheckBox

                    if (cell.childNodes[j].type == "text") {
                        if (cell.childNodes[j].value == document.getElementById("ddlAssocContactId").value) {
                            alert("Associated contact is already added");
                            return false;
                        }
                    }
                }
            }
        }


    }
    function OpenContEmailFromAssociate(a, b) {

        window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
        return false;
    }
</script>
<div class="row padbottom10">
    <div class="col-xs-12">
        <div class="form-inline">
            <div class="pull-left">
            <label>Customer</label>&nbsp;
        <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
            OnClientItemsRequested="OnClientItemsRequestedOrganization"
            ClientIDMode="Static"
            ShowMoreResultsBox="true"
            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
        </telerik:RadComboBox>
            <label>Contact</label>
            <asp:DropDownList ID="ddlAssocContactId" runat="server" CssClass="form-control form-group" Width="180">
            </asp:DropDownList>
            <label>Contact Role</label>
            <asp:DropDownList ID="ddlContactRole" runat="server" CssClass="form-control form-group" Width="180">
            </asp:DropDownList>
            <label>Subscribe to Email Alerts : </label>
            <asp:RadioButton ID="rdnYes" Checked="true" Text="Yes"  GroupName="IsSubscribed" runat="server" />
            <asp:RadioButton ID="rdnNo" Text="No"   GroupName="IsSubscribed" runat="server" />
            <asp:Button ID="btnAddContact" runat="server" CssClass="btn btn-primary" Text="Add Contact" />
                </div>
            <div  class="pull-left text-danger" runat="server" style ="padding-left:10px;" id="secBrodcastNotConfigured">
               <label> Note:</label> Alerts are not enabled. To enable, go to: <br />
Marketing | Email Broadcast | Configuration 

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="table-responsive">
            <asp:DataGrid ID="dgContact" runat="server" AutoGenerateColumns="False"
                CssClass="table table-striped table-bordered" Width="100%" UseAccessibleHeader="true">
                <Columns>
                    <asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
                   <%-- <asp:BoundColumn DataField="Company" HeaderText="Organization,Relationship"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Name" HeaderText="First &amp; Last Name"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Phone" HeaderText="Phone - Ext"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Email" HeaderText="Email"></asp:BoundColumn>--%>
                    <asp:TemplateColumn>
                      <ItemTemplate>
                           <strong>
                            <asp:Label ID="lblContactName" runat="server" CssClass="text-color-theme" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'></asp:Label></strong>
                         <span class="contact">
                            <asp:Label ID="lblPhone" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Phone") %>' ></asp:Label></span>
                            <a id="btnSendEmail" href="#" onclick="OpenContEmailFromAssociate('<%# Eval("Email")%>')"><img src="../images/msg_unread_small.gif" /> </a>
                      </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="ContactRole" HeaderText="Role"></asp:BoundColumn>
                    <asp:BoundColumn DataField="SubscribedEmailAlert" HeaderText="Subscribe to Email Alerts"></asp:BoundColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <asp:TextBox ID="txtContactID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactId") %>'>
                            </asp:TextBox>
                            <asp:Button ID="btnDeleteCnt" runat="server" CommandName="Delete" CssClass="btn btn-danger"
                                Text="X" />
                            <asp:LinkButton ID="lnkDeleteCnt" runat="server" Visible="false">
																<font color="#730000">*</font></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</div>

