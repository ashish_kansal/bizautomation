﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Newtonsoft.Json
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Projects

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class WorkFlowService
    Inherits System.Web.Services.WebService

    <WebMethod(EnableSession:=True)> _
    Public Function GetWFData(ByVal lngWFID As Long) As String
        Try
            Dim objWFManage As New Workflow
            Dim ds As DataSet

            objWFManage.WFID = lngWFID
            objWFManage.DomainID = Session("DomainID")

            ds = objWFManage.GetWorkFlowMasterDetail

            Dim objWF As New WorkFlowObject

            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                objWF = objWFManage.GetDatasetToWFObject(ds)
            End If

            Dim strReportJson As String = JsonConvert.SerializeObject(objWF, Formatting.None)

            Return strReportJson
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function SaveWFData(ByVal lngWFID As Long, ByVal strReportJson As String) As String
        Try
            Dim objWF As New WorkFlowObject
            If strReportJson.Trim.Length > 0 Then
                objWF = Newtonsoft.Json.JsonConvert.DeserializeObject(Of WorkFlowObject)(strReportJson)

                If objWF Is Nothing Then
                    'Return "Error: Custom Report."
                    Throw New Exception("Error in Workflow")
                End If
            End If

            Dim objWFManage As New Workflow

            objWFManage.WFID = lngWFID
            objWFManage.DomainID = Session("DomainID")
            objWFManage.UserCntID = Session("UserContactID")
            objWFManage.WFName = objWF.WFName
            objWFManage.WFDescription = objWF.WFDescription
            objWFManage.boolActive = objWF.Active
            objWFManage.FormID = objWF.FormID
            objWFManage.WFTriggerOn = objWF.WFTriggerOn

            Dim dr As DataRow

            Dim dtWFTriggerFieldList As New DataTable
            CCommon.AddColumnsToDataTable(dtWFTriggerFieldList, "numFieldID,bitCustom")
            dtWFTriggerFieldList.TableName = "WorkFlowTriggerFieldList"

            For Each str As String In objWF.TriggerFieldList
                dr = dtWFTriggerFieldList.NewRow
                dr("numFieldID") = CCommon.ToLong(str.Split("_")(0))
                dr("bitCustom") = CCommon.ToBool(str.Split("_")(1))
                dtWFTriggerFieldList.Rows.Add(dr)
            Next


            Dim dtConditionList As New DataTable
            CCommon.AddColumnsToDataTable(dtConditionList, "numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR")
            dtConditionList.TableName = "WorkFlowConditionList"

            For Each str As ConditionObject In objWF.Conditions
                dr = dtConditionList.NewRow

                dr("numFieldID") = CCommon.ToLong(str.Column.Split("_")(0))
                dr("bitCustom") = CCommon.ToBool(str.Column.Split("_")(1))
                dr("vcFilterValue") = str.FilterValue.Trim
                dr("vcFilterOperator") = str.FilterOperator.Trim
                dr("vcFilterANDOR") = str.FilterANDOR.Trim
                dtConditionList.Rows.Add(dr)
            Next

            Dim dtWFActionList As New DataTable
            CCommon.AddColumnsToDataTable(dtWFActionList, "tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder")
            dtWFActionList.TableName = "WorkFlowActionList"

            Dim dtWFActionUpdateFields As New DataTable
            CCommon.AddColumnsToDataTable(dtWFActionUpdateFields, "tintModuleType,numFieldID,bitCustom,vcValue,numWFActionID")
            dtWFActionUpdateFields.TableName = "WorkFlowActionUpdateFields"

            Dim i As Integer = 1

            For Each str As ActionObject In objWF.Actions
                dr = dtWFActionList.NewRow

                dr("tintActionType") = str.ActionType
                dr("numTemplateID") = str.TemplateID
                dr("vcEmailToType") = str.vcEmailToType
                dr("tintTicklerActionAssignedTo") = str.TicklerActionAssignedTo
                dr("vcAlertMessage") = str.vcAlertMessage
                dr("tintActionOrder") = i

                If str.ActionType = 3 Then
                    For Each str1 As ActionUpdateFieldsObject In str.ActionUpdateFields
                        Dim dr1 As DataRow = dtWFActionUpdateFields.NewRow
                        dr1("numWFActionID") = i
                        dr1("tintModuleType") = str1.tintModuleType
                        dr1("numFieldID") = CCommon.ToLong(str1.Column.Split("_")(0))
                        dr1("bitCustom") = CCommon.ToBool(str1.Column.Split("_")(1))
                        dr1("vcValue") = str1.vcValue
                        dtWFActionUpdateFields.Rows.Add(dr1)
                    Next
                End If

                dtWFActionList.Rows.Add(dr)

                i += 1
            Next

            Dim ds As New DataSet
            ds.Tables.Add(dtWFTriggerFieldList)
            ds.Tables.Add(dtConditionList)
            ds.Tables.Add(dtWFActionList)
            ds.Tables.Add(dtWFActionUpdateFields)

            'objReportManage.textQuery = Sql.ToString()
            objWFManage.strText = ds.GetXml
            objWFManage.ManageWorkFlowMaster()

            Return strReportJson
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <WebMethod(EnableSession:=True)> _
    Public Function GetAlertData() As String
        Try
            Dim objWFManage As New Workflow
            Dim ds As DataSet

            objWFManage.DomainID = Session("DomainID")
            ds = objWFManage.GetAlertMessageData()
            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
            Dim rows As New List(Of Dictionary(Of String, Object))()

            Dim row As Dictionary(Of String, Object)

            For Each dr As DataRow In ds.Tables(0).Rows
                row = New Dictionary(Of String, Object)()
                For Each col As DataColumn In ds.Tables(0).Columns
                    row.Add(col.ColumnName, dr(col))
                Next
                rows.Add(row)
            Next




            Return serializer.Serialize(rows)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    
End Class