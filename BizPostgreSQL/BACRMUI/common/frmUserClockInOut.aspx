﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmUserClockInOut.aspx.vb" Inherits=".frmUserClockInOut" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ajaxStart(function () {
            $("#divLoader").show();
        }).ajaxStop(function () {
            $("#divLoader").hide();
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            BindData();
        }

        function BindData() {
            $("#tblData").find('tr').not(':first').remove();
            var datepicker = $find("<%= rdpDate.ClientID %>");
            
            if (datepicker.get_selectedDate() != null) {
                var todayDate = new Date(datepicker.get_selectedDate());
                var fromDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate(), 0, 0, 0, 0);

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetUserClockInOutDetail',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "fromDate": "\/Date(" + fromDate.getTime() + ")\/"
                    }),
                    success: function (data) {
                        try {
                            var obj = $.parseJSON(data.GetUserClockInOutDetailResult);
                            if (obj != null && obj.length > 0) {
                                var html = "";
                                var totalTime = 0;

                                $.each(obj, function (index, item) {
                                    html += "<tr>";
                                    html += "<td>" + (item.vcDate || "") + "</td>";
                                    html += "<td>" + (item.vcCheckedIn || "") + "</td>";
                                    html += "<td>" + (item.vcCheckedOut || "") + "</td>";
                                    html += "<td>" + (item.vcTime || "") + "</td>";
                                    html += "</tr>";

                                    totalTime += parseInt(item.numTimeInMinutes)
                                });

                                $("#tblData").append(html);
                                $("#lblTotalTime").text((Math.floor(totalTime / 60).toString().length === 1 ? "0" : "") + Math.floor(totalTime / 60) + ":" + (Math.floor(totalTime % 60).toString().length === 1 ? "0" : "") + Math.floor(totalTime % 60))
                            } else {
                                $("#tblData").append("<tr><td colspan='4'>No Data</td></tr>");
                            }

                        } catch (err) {
                            alert("Unknown error occurred while loading report data.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            alert("Error occurred while loading report data: " + replaceNull(objError.ErrorMessage));
                        } else {
                            alert("Unknown error ocurred while loading report data.");
                        }
                    }
                });
            }
        }

        function LoadPrevious() {
            try {
                var datepicker = $find("<%= rdpDate.ClientID%>");
                var todayDate = datepicker.get_selectedDate();

                datepicker.set_selectedDate(new Date(todayDate.setDate(todayDate.getDate() - 1)));
            } catch (e) {
                alert("Unknown error occurred.");
            }

            return false;
        }

        function LoadNext() {
            try {
                var datepicker = $find("<%= rdpDate.ClientID%>");
                var todayDate = datepicker.get_selectedDate();

                datepicker.set_selectedDate(new Date(todayDate.setDate(todayDate.getDate() + 1)));
            } catch (e) {
                alert("Unknown error occurred.");
            }

            return false;
        }

        function dateSelected(sender, eventArgs) {
            BindData();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button runat="server" ID="btnClose" OnClientClick="return Close();" Text="Close" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <telerik:RadScriptManager runat="server"></telerik:RadScriptManager>
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Total Time:</label>
                        <label id="lblTotalTime"></label>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <ul class="list-inline" style="margin-bottom: 0px;">
                    <li style="vertical-align: top;">
                        <telerik:RadDatePicker ID="rdpDate" runat="server" DateInput-CssClass="form-control" Width="100" DateInput-Width="100" DatePopupButton-HoverImageUrl="~/images/calendar25.png" DatePopupButton-ImageUrl="~/images/calendar25.png">
                            <ClientEvents OnDateSelected="dateSelected" />
                        </telerik:RadDatePicker>
                    </li>
                    <li style="vertical-align: top;">
                        <button class="btn btn-flat btn-primary" onclick="return LoadPrevious();">Previous</button>
                        <button class="btn btn-flat btn-primary" onclick="return LoadNext();">Next</button>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="tblData" class="table table-bordered table-striped tbl-primary">
                    <tr>
                        <th>Date</th>
                        <th>Checked-in</th>
                        <th>Checked-out</th>
                        <th>Time</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
