﻿Imports System.Web
Imports BACRM.BusinessLogic.Item
Imports System.IO
Imports System.Configuration
Imports System.Drawing
Imports System.Drawing.Imaging
Imports BACRM.BusinessLogic.Common
''' <summary>
''' Summary description for Barcode
''' </summary>
Public Class Barcode
    Implements IHttpHandler

    Public Sub ProcessRequest(context As HttpContext) Implements IHttpHandler.ProcessRequest

        If CCommon.ToString(context.Request("code")).Length > 0 Then
            Dim c39 As New Code39()
            ' Create stream....
            Dim ms As New MemoryStream()
            c39.FontFamilyName = System.Configuration.ConfigurationManager.AppSettings("BarCodeFontFamily")
            c39.FontFileName = context.Request.PhysicalApplicationPath() & "\" & System.Configuration.ConfigurationManager.AppSettings("BarCodeFontFile")
            c39.FontSize = Single.Parse(context.Request("barSize"))
            c39.ShowCodeString = Boolean.Parse(context.Request("ShowCodeString"))

            c39.Title = CCommon.ToString(context.Request("title"))
            Dim objBitmap As Bitmap = c39.GenerateBarcode(CCommon.ToString(context.Request("code")))
            objBitmap.Save(ms, ImageFormat.Png)

            Dim byteArray As [Byte]() = ms.GetBuffer()
            ms.Flush()
            ms.Close()
            'context.Response.BufferOutput = true;
            ' Clear all content output from the buffer stream
            context.Response.Clear()
            context.Response.ClearHeaders()
            context.Response.ContentType = "image/png"
            ' Write the data
            context.Response.BinaryWrite(byteArray)
            context.Response.End()
        Else
            context.Response.End()
        End If

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
End Class
