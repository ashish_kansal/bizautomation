﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.UI
Imports ImageResizer.Configuration
Imports ImageResizer.Plugins.PrettyGifs
Imports ImageResizer
Imports System.Web.HttpServerUtility
Imports System.Diagnostics
Imports ImageResizer.Plugins.FreeImageBuilder
Imports ImageResizer.Util
Imports ImageResizer.Plugins.FreeImageDecoder
Imports System.Drawing
Imports ImageResizer.Plugins.Basic
Imports ImageResizer.Plugins.Watermark
Imports System.IO
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item



Public Class ImageUploadResize
    Implements System.Web.IHttpHandler
    Implements System.Web.SessionState.IRequiresSessionState

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Try
            Dim strMeasure As String = ""
            Dim strItemCode As String = ""
            strMeasure = Convert.ToString(context.Request.QueryString("measure"))
            strItemCode = Convert.ToString(context.Request.QueryString("ItemCode"))
            'TODO:Get Value of ItemCode from query string 

            Dim uploadPath As String = CCommon.GetDocumentPhysicalPath(context.Session("DomainID")) '"E:\Bizautomation_New\CodeBase\BACRMUI\Documents\Docs\1\"

            Dim fileUpload As HttpPostedFile = context.Request.Files(0)

            fileUpload.SaveAs(uploadPath + fileUpload.FileName)

            Dim objItem As New CItems
            Dim result As Boolean

            objItem.DomainID = context.Session("DomainID")
            objItem.ItemCode = strItemCode
            objItem.bitDefault = False
            objItem.PathForImage = fileUpload.FileName

            'Here If uploaded file is image then thumbnail save otherwise there is no need to save thumbnail .
            If fileUpload.ContentType.ToLower() = "image/jpg" Or fileUpload.ContentType.ToLower() = "image/jpeg" Or fileUpload.ContentType.ToLower() = "image/gif" Or fileUpload.ContentType.ToLower() = "image/png" Then
                objItem.PathForTImage = "Thumb_" + fileUpload.FileName
                objItem.IsImage = True
                ImageResizer.ImageBuilder.Current.Build(uploadPath + fileUpload.FileName, uploadPath + "Thumb_" + fileUpload.FileName, New ResizeSettings(strMeasure))
            Else
                objItem.IsImage = False
            End If

            result = objItem.AddUpdateImageItem()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class