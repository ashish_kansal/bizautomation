﻿Imports System.Web
Imports System.Web.Services
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts

Public Class ContactImage
    Implements System.Web.IHttpHandler
    Implements System.Web.SessionState.IRequiresSessionState

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Try
            Dim actionType As String = CCommon.ToString(context.Request.Form("ActionType"))
            Dim contactID As Long = CCommon.ToLong(context.Request.Form("ContactID"))
            Dim uploadPath As String = CCommon.GetDocumentPhysicalPath(context.Session("DomainID"))
            Dim imageName As String = CCommon.ToString(context.Request.Form("ImageName"))

            If actionType = "1" Then 'UPLOAD IMAGE
                If context.Request.Files.Count > 0 Then
                    Dim fileUpload As HttpPostedFile = context.Request.Files(0)
                    Dim fileName As String = "Thumb_" & contactID & "_" & fileUpload.FileName
                    'Save file to disk
                    fileUpload.SaveAs(uploadPath + fileName)

                    'Update image name to database
                    Dim objContact As New CContacts
                    objContact.DomainID = context.Session("DomainID")
                    objContact.ContactID = contactID
                    objContact.vcImageName = fileName
                    objContact.UpdateContactImage()

                    context.Response.Write("Success")

                    'Try to Delete File from disk
                    Try
                        If Not String.IsNullOrEmpty(imageName) AndAlso System.IO.File.Exists(uploadPath & imageName) Then
                            System.IO.File.Delete(uploadPath & imageName)
                        End If
                    Catch ex As Exception
                        'DO NOT THROW ERROR
                    End Try
                End If
            ElseIf actionType = "2" Then 'REMOVE IMAGE


                'Try to Delete File from disk
                Try
                    If Not String.IsNullOrEmpty(imageName) AndAlso System.IO.File.Exists(uploadPath & imageName) Then
                        System.IO.File.Delete(uploadPath & imageName)
                    End If
                Catch ex As Exception
                    'DO NOT THROW ERROR
                End Try

                'Pass emtry string to image Name to contact record
                Dim objContact As New CContacts
                objContact.DomainID = context.Session("DomainID")
                objContact.ContactID = contactID
                objContact.vcImageName = ""
                objContact.UpdateContactImage()

                context.Response.Write("Removed")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, context.Session("DomainID"), context.Session("UserContactID"), context.Request)
            context.Response.Write("Error occured!")
        End Try
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class