﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.TimeAndExpense

Public Class frmUserClockInOut
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                rdpDate.SelectedDate = DateTime.UtcNow.AddMinutes(-1 * Session("ClientMachineUTCTimeOffset"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

End Class