﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Newtonsoft.Json
Imports BACRM.BusinessLogic.CustomReports
Imports BACRM.BusinessLogic.Projects


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following lincontext.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class CustomReportsService
    Inherits System.Web.Services.WebService

    <WebMethod(EnableSession:=True)> _
    Public Function keepSessionAlive() As String
        If Not Session("DomainID") Is Nothing Then
            Return "OK"
        Else
            Return "EXP" 'expired session
        End If

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function GetDropDownData(ByVal numListID As Long, ByVal vcListItemType As String, vcDbColumnName As String, vcAssociatedControlType As String) As String
        Try
            Dim strData As String = ""
            Dim dtData As New DataTable
            Dim objCommon As New CCommon
            objCommon.DomainID = Session("DomainID")

            If vcAssociatedControlType = "CheckBox" Then
                dtData = New DataTable
                dtData.Columns.Add("numID")
                dtData.Columns.Add("vcData")
                Dim dr1 As DataRow
                dr1 = dtData.NewRow
                dr1("numID") = "1"
                dr1("vcData") = "Yes"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("numID") = "0"
                dr1("vcData") = "No"
                dtData.Rows.Add(dr1)
            ElseIf vcDbColumnName = "tintSource" Then
                dtData = objCommon.GetOpportunitySource()
                dtData.Columns(0).ColumnName = "numID"
            ElseIf vcDbColumnName = "numProjectID" Then
                Dim objProject As New Project
                objProject.DomainID = Session("DomainID")
                dtData = objProject.GetOpenProject()

                dtData.Columns(0).ColumnName = "numID"
                dtData.Columns(1).ColumnName = "vcData"

                Dim dr1 As DataRow
                dr1 = dtData.NewRow
                dr1("numID") = "0"
                dr1("vcData") = "--None--"
                dtData.Rows.Add(dr1)
            Else
                dtData = objCommon.GetDropDownValue(numListID, vcListItemType, vcDbColumnName)
            End If

            strData = JsonConvert.SerializeObject(dtData, Formatting.None)

            Return strData
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function GetReportResponse(ByVal lngRptID As Long, ByVal strReportJson As String, boolSave As Boolean, boolInit As Boolean) As String
        Try
            Dim objReportManage As New CustomReportsManage
            Dim ds As DataSet

            objReportManage.ReportID = lngRptID
            objReportManage.DomainID = Session("DomainID")
            objReportManage.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objReportManage.UserCntID = Session("UserContactID")

            Dim objReport As New ReportObject

            ds = objReportManage.GetReportListMasterDetail

            If boolInit Then
                'Dim json As String = "{""GroupColumn"": [],""LayoutFormatType"": ""0"",""FilterLogic"": """",""filters"": []," & _
                '"""ColumnList"": [],""SortDirection"": """",""SortColumn"": """",""RowBreaks"": [],""ColumnBreaks"": [],""Aggregate"": []}"

                If ds.Tables.Count > 0 Then
                    objReport = objReportManage.GetDatasetToReportObject(ds)
                End If
            Else
                If strReportJson.Trim.Length > 0 Then
                    objReport = Newtonsoft.Json.JsonConvert.DeserializeObject(Of ReportObject)(strReportJson)

                    If objReport Is Nothing Then
                        'Return "Error: Custom Report."
                        Throw New Exception("Error in Custom Report")
                    End If
                End If
            End If

            Dim dsColumnList As DataSet
            dsColumnList = objReportManage.GetReportModuleGroupFieldMaster

            Try
                objReportManage.textQuery = objReportManage.GetReportQuery(objReportManage, ds, objReport, boolInit, boolSave, dsColumnList)
            Catch ex As Exception
                'Return "Error: Can't display the report until you fix the error in Reports."
                Throw New Exception("Error in Custom Report")
            End Try

            If objReportManage.textQuery IsNot Nothing AndAlso boolSave = False Then
                Dim dtData As DataTable = objReportManage.USP_ReportQueryExecute()
                objReport.ResultData = JsonConvert.SerializeObject(dtData, Formatting.None)
            Else
                objReport.ResultData = Nothing
            End If

            strReportJson = JsonConvert.SerializeObject(objReport, Formatting.None)

            If boolSave Then
                objReportManage.ReportName = objReport.vcReportName
                objReportManage.ReportDescription = objReport.vcReportDescription

                objReportManage.tintReportType = objReport.ReportFormatType
                objReportManage.intNoRows = CCommon.ToInteger(objReport.NoRows)
                objReportManage.bitHideSummaryDetail = objReport.bitHideSummaryDetail

                If objReport.SortColumn.Contains("_") Then
                    objReportManage.SortColumnmReportFieldGroupID = CCommon.ToLong(objReport.SortColumn.Split("_")(0))
                    objReportManage.SortColumnFieldID = CCommon.ToLong(objReport.SortColumn.Split("_")(1))
                    objReportManage.bitSortColumnCustom = CCommon.ToBool(objReport.SortColumn.Split("_")(2))
                    objReportManage.SortColumnDirection = objReport.SortDirection
                Else
                    objReportManage.SortColumnmReportFieldGroupID = Nothing
                    objReportManage.SortColumnFieldID = Nothing
                    objReportManage.bitSortColumnCustom = False
                    objReportManage.SortColumnDirection = Nothing
                End If

                objReportManage.FilterLogic = objReport.FilterLogic

                If objReport.DateFieldFilter.Contains("_") Then
                    objReportManage.DateReportFieldGroupID = CCommon.ToLong(objReport.DateFieldFilter.Split("_")(0))
                    objReportManage.DateFieldID = CCommon.ToLong(objReport.DateFieldFilter.Split("_")(1))
                    objReportManage.bitDateFieldColumnCustom = CCommon.ToBool(objReport.DateFieldFilter.Split("_")(2))
                    objReportManage.DateFieldValue = objReport.DateFieldValue
                    objReportManage.dtFromDate = objReport.FromDate
                    objReportManage.dtToDate = objReport.ToDate
                Else
                    objReportManage.DateReportFieldGroupID = Nothing
                    objReportManage.DateFieldID = Nothing
                    objReportManage.bitDateFieldColumnCustom = Nothing
                    objReportManage.DateFieldValue = Nothing
                    objReportManage.dtFromDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value
                    objReportManage.dtToDate = System.Data.SqlTypes.SqlDateTime.MaxValue.Value
                End If

                objReportManage.tintRecordFilter = objReport.RecordFilter

                If objReportManage.tintReportType = 3 Or objReportManage.tintReportType = 4 Or objReportManage.tintReportType = 5 Then
                    objReportManage.KPIMeasureFieldID = objReport.KPIMeasureField
                End If

                Dim dr As DataRow

                Dim dtReportFields As New DataTable
                CCommon.AddColumnsToDataTable(dtReportFields, "numReportFieldGroupID,numFieldID,bitCustom")
                dtReportFields.TableName = "ReportFieldsList"

                For Each str As String In objReport.ColumnList
                    dr = dtReportFields.NewRow
                    dr("numReportFieldGroupID") = CCommon.ToLong(str.Split("_")(0))
                    dr("numFieldID") = CCommon.ToLong(str.Split("_")(1))
                    dr("bitCustom") = CCommon.ToBool(str.Split("_")(2))
                    dtReportFields.Rows.Add(dr)
                Next

                Dim dtReportFilter As New DataTable
                CCommon.AddColumnsToDataTable(dtReportFilter, "numReportFieldGroupID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText")
                dtReportFilter.TableName = "ReportFilterList"

                For Each str As filterObject In objReport.filters
                    dr = dtReportFilter.NewRow

                    dr("numReportFieldGroupID") = CCommon.ToLong(str.Column.Split("_")(0))
                    dr("numFieldID") = CCommon.ToLong(str.Column.Split("_")(1))
                    dr("bitCustom") = CCommon.ToBool(str.Column.Split("_")(2))
                    dr("vcFilterValue") = str.ColValue.Trim
                    dr("vcFilterOperator") = str.OperatorType.Trim
                    dr("vcFilterText") = str.ColText.Trim
                    dtReportFilter.Rows.Add(dr)
                Next

                Dim dtReportSummaryGroup As New DataTable
                CCommon.AddColumnsToDataTable(dtReportSummaryGroup, "numReportFieldGroupID,numFieldID,bitCustom,tintBreakType")
                dtReportSummaryGroup.TableName = "ReportSummaryGroupList"

                For Each str As String In objReport.GroupColumn
                    dr = dtReportSummaryGroup.NewRow

                    dr("numReportFieldGroupID") = CCommon.ToLong(str.Split("_")(0))
                    dr("numFieldID") = CCommon.ToLong(str.Split("_")(1))
                    dr("bitCustom") = CCommon.ToBool(str.Split("_")(2))
                    dr("tintBreakType") = 0
                    dtReportSummaryGroup.Rows.Add(dr)
                Next

                For Each str As String In objReport.ColumnBreaks
                    dr = dtReportSummaryGroup.NewRow

                    dr("numReportFieldGroupID") = CCommon.ToLong(str.Split("_")(0))
                    dr("numFieldID") = CCommon.ToLong(str.Split("_")(1))
                    dr("bitCustom") = CCommon.ToBool(str.Split("_")(2))
                    dr("tintBreakType") = 1
                    dtReportSummaryGroup.Rows.Add(dr)
                Next

                For Each str As String In objReport.RowBreaks
                    dr = dtReportSummaryGroup.NewRow

                    dr("numReportFieldGroupID") = CCommon.ToLong(str.Split("_")(0))
                    dr("numFieldID") = CCommon.ToLong(str.Split("_")(1))
                    dr("bitCustom") = CCommon.ToBool(str.Split("_")(2))
                    dr("tintBreakType") = 2
                    dtReportSummaryGroup.Rows.Add(dr)
                Next

                Dim dtReportMatrixAggregate As New DataTable
                CCommon.AddColumnsToDataTable(dtReportMatrixAggregate, "numReportFieldGroupID,numFieldID,bitCustom,vcAggType")
                dtReportMatrixAggregate.TableName = "ReportMatrixAggregateList"

                For Each str As AggregateObject In objReport.Aggregate
                    dr = dtReportMatrixAggregate.NewRow

                    dr("numReportFieldGroupID") = CCommon.ToLong(str.Column.Split("_")(0))
                    dr("numFieldID") = CCommon.ToLong(str.Column.Split("_")(1))
                    dr("bitCustom") = CCommon.ToBool(str.Column.Split("_")(2))
                    dr("vcAggType") = str.aggType.Trim
                    dtReportMatrixAggregate.Rows.Add(dr)
                Next

                Dim dtReportKPIThresold As New DataTable
                CCommon.AddColumnsToDataTable(dtReportKPIThresold, "vcThresoldType,decValue1,decValue2,decGoalValue")
                dtReportKPIThresold.TableName = "ReportKPIThresold"

                For Each str As KPIThresoldObject In objReport.KPIThresold
                    dr = dtReportKPIThresold.NewRow

                    dr("vcThresoldType") = CCommon.ToString(str.ThresoldType)
                    dr("decValue1") = CCommon.ToDecimal(str.Value1)
                    dr("decValue2") = CCommon.ToDecimal(str.Value2)
                    dr("decGoalValue") = CCommon.ToDecimal(str.GoalValue)
                    dtReportKPIThresold.Rows.Add(dr)
                Next

                ds = New DataSet
                ds.Tables.Add(dtReportFields)
                ds.Tables.Add(dtReportFilter)
                ds.Tables.Add(dtReportSummaryGroup)
                ds.Tables.Add(dtReportMatrixAggregate)
                ds.Tables.Add(dtReportKPIThresold)

                'objReportManage.textQuery = Sql.ToString()
                objReportManage.strText = ds.GetXml
                objReportManage.ManageReportListMaster()
            End If

            Return strReportJson
        Catch ex As Exception
            Throw ex
        End Try
    End Function

   End Class

