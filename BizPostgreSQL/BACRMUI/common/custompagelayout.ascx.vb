﻿Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Imports Microsoft.Web.UI.WebControls
Public Class custompagelayout
    Inherits BACRMUserControl

    'C- contacts      UI,partner point
    'P- projects      UI,partner point
    'A- accounts      UI,partner point
    'O- oppurtunities UI,partner point
    'S- Cases         UI,partner point
    'R- Projects      UI,partner point
    'L- leads         UI,partner point
    'I - Item         

    'Z- organization  Portal
    'T- oppurtunities Portal
    'E- Cases         Portal
    'N- contacts      Portal
    'J- Porject       Portal


    Dim objCommon As CCommon
    'Dim _BACRMPage As BACRMPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '_BACRMPage = New BACRMPage()
            If Not IsPostBack Then
                hfCtype.Value = GetQueryStringVal("Ctype").ToString
                hfFormId.Value = GetQueryStringVal("FormId").ToString
                hfPType.Value = GetQueryStringVal("PType").ToString

                If Not (GetQueryStringVal("type") = "") Then type.Text = GetQueryStringVal("type")
                getMax()
                binddata()

                If hfPType.Value = 2 Then
                    GetUserRightsForPage(13, 43) 'Bug Fix id:2117   
                End If
            End If
            btnUpdate.Attributes.Add("onclick", "getSort()")
            btnUpdate1.Attributes.Add("onclick", "getSort()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)

    End Sub
    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            savedata()
            binddata()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Sub getMax()
        Try
            'Dim i As Integer
            'Dim objlayout As New CcustPageLayout
            'objlayout.DomainID = Session("domainId")
            'objlayout.UserCntId = Session("UserContactId")
            ''objlayout.ContactID = Session("UserContactId")
            'objlayout.CoType = hfCtype.Value
            'Dim x As String
            'x = objlayout.GetMaxRows()
            rows.Text = 2
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

     Public Sub binddata()
        Try
            Dim contacttype As String = hfCtype.Value
            Dim str As String
            objCommon = New CCommon
            Dim dtddlContact As DataTable
            If (contacttype = "c") Then
                dtddlContact = objCommon.GetMasterListItems(8, Session("DomainID"))
            ElseIf (contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "T") Then
                dtddlContact = objCommon.GetMasterListItems(5, Session("DomainID"))
            End If

            Dim objlayout As New CcustPageLayout
            Dim dttable1 As DataTable
            Dim i, count, x, r As Integer
            Dim pageid As Integer = 0
            Dim strHeader As String = "Customize Page Layout"


            If hfPType.Value = "2" And (hfFormId.Value = 34 Or hfFormId.Value = 35 Or hfFormId.Value = 36) Then
                strHeader = "Customize New Relationship Fields"
            End If

            Select Case hfFormId.Value
                Case "86"
                    strHeader = "Customize New Inventory Item Fields"
                Case "87"
                    strHeader = "Customize New Non Inventory Item Fields"
                Case "88"
                    strHeader = "Customize New Serialized/LOT #s Item Fields"
            End Select

            lblTitle.Text = strHeader

            str = "<br/><table cellSpacing='0' cellPadding='0' width='100%'><tr>"
            Select Case contacttype
                Case "c"
                    pageid = 4
                    If Not (hfPType.Value = "2") Then
                        str = str & "<td align='left' style='WIDTH: 30%' vAlign='bottom' class ='normal1'>Contact Type : <select class ='normal1' id='dlh' onchange='dlChange()'>"
                    End If
                Case "A", "P", "L", "T"
                    pageid = 1
                    str = str & "<td align='left' style='WIDTH: 30%' vAlign='bottom' class ='normal1'>Relationship : <select class ='normal1' id='dlh' onchange='dlChange()'>"
                Case "S" : pageid = 3
                Case "O", "B" : pageid = 2
                Case "u" : pageid = 6
                Case "R" : pageid = 11
                Case "I"
                    pageid = 5
                    str = str & "<td align='left' style='WIDTH: 30%' vAlign='bottom' class ='normal1'>Item Type : <select class ='normal1' id='ddlItemType' onchange='ddlItemTypeChange()'>"
                    'Case "T" : pageid = 1
            End Select

            If Not (hfPType.Value = "2" And hfFormId.Value = 10) Then
                If (contacttype = "c" Or contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "T") Then
                    If (type.Text = "0") Then
                        str = str & "<option value ='0' selected='selected'>--Select One--</option>"
                    Else : str = str & "<option value ='0' >--Select One--</option>"
                    End If

                    If hfPType.Value = 2 And (hfFormId.Value = 34 Or hfFormId.Value = 35 Or hfFormId.Value = 36) Then
                        If (type.Text = "1") Then
                            str = str & "<option value ='1' selected='selected'>Leads</option>"
                        Else
                            str = str & "<option value ='1'>Leads</option>"
                        End If

                        If (type.Text = "2") Then
                            str = str & "<option value ='2' selected='selected'>Accounts</option>"
                        Else
                            str = str & "<option value ='2'>Accounts</option>"
                        End If

                        If (type.Text = "3") Then
                            str = str & "<option value ='3' selected='selected'>Prospects</option>"
                        Else
                            str = str & "<option value ='3'>Prospects</option>"
                        End If
                    ElseIf hfPType.Value = 3 Then
                        str = str & "<option value ='1'>Leads</option>"
                        str = str & "<option value ='3'>Prospects</option>"
                    End If

                    For r = 0 To dtddlContact.Rows.Count - 1
                        If (type.Text = dtddlContact.Rows(r).Item("numListItemID").ToString) Then
                            str = str & "<option value ='" + dtddlContact.Rows(r).Item("numListItemID").ToString + "' selected='selected'>" + CStr(dtddlContact.Rows(r).Item("vcdata").ToString) + "</option>"
                        Else : str = str & "<option value ='" + dtddlContact.Rows(r).Item("numListItemID").ToString + "' >" + CStr(dtddlContact.Rows(r).Item("vcdata").ToString) + "</option>"
                        End If
                    Next
                    str = str & "</select></td>"
                ElseIf contacttype = "I" Then
                    If hfPType.Value = 2 And (hfFormId.Value = 86 Or hfFormId.Value = 87 Or hfFormId.Value = 88) Then
                        If (type.Text = "1") Then
                            str = str & "<option value ='1' selected='selected'>Inventory</option>"
                        Else
                            str = str & "<option value ='1'>Inventory</option>"
                        End If

                        If (type.Text = "2") Then
                            str = str & "<option value ='2' selected='selected'>Non Inventory</option>"
                        Else
                            str = str & "<option value ='2'>Non Inventory</option>"
                        End If

                        If (type.Text = "3") Then
                            str = str & "<option value ='3' selected='selected'>Serialized/LOT</option>"
                        Else
                            str = str & "<option value ='3'>Serialized/LOT</option>"
                        End If
                    End If

                    str = str & "</select></td>"
                End If
            End If

            str = str & "<td align='right' height='23'>&nbsp;&nbsp;&nbsp;"
            'str = str & "<input class='button' id='btnColumn' style='width:100' onclick='AddColumn()' type='button' value='Add Column'>&nbsp;<input class='button' id='btnDeleteColumn1' style='width:100' onclick='DeleteColumn()' type='button' value='Delete Column'>&nbsp;"

            If hfPType.Value = 2 Then
                str = str & "<div style='float:right'><input class='button' id='btnsave1' style='width:50' onclick='show()' type='button' value='Save'></div></td></tr></table>"
            Else
                str = str & "<div style='float:right'><input class='button' id='btnsave1' style='width:50' onclick='show()' type='button' value='Save'>&nbsp;<input class='button' id='btnsave1' style='width:100' onclick='show1()' type='button' value='Save & Close'>&nbsp;<input class='button' id='btnClose' style='width:50' onclick='Close()' type='button' value='Close'>&nbsp;</div></td></tr></table>"
            End If


            str = str & "<table  class='aspTableDTL' cellpadding='0' cellspacing='0' width='100%' height='500px' border='0'><tr valign='top'><td>"
            str = str & "<br/><table align='center'valign='top' ><tr valign='top'><td class='hs' align='center'>Available Fields</td><td class='hs' align='center'>Fields added to column 1</td><td class='hs' align='center'>Fields added to column 2</td></tr><tr valign='top' >"
            x = 0
            Dim max As String
            max = rows.Text
            Dim maxHeight As Int16 = 0
            While (x <= max)
                objlayout.DomainID = Session("domainId")

                If contacttype = "B" Then
                    objlayout.UserCntID = GetQueryStringVal("SiteId")
                ElseIf hfPType.Value = 3 Then
                    objlayout.UserCntID = Session("UserContactId")
                Else
                    objlayout.UserCntID = 0
                End If

                objlayout.ColumnID = x
                objlayout.CoType = hfCtype.Value
                If (contacttype = "c" Or contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "S" Or contacttype = "O" Or contacttype = "u" Or contacttype = "R" Or contacttype = "T" Or contacttype = "B" Or contacttype = "I") Then
                    Dim ds As New DataSet
                    Dim dttable2 As DataTable
                    objlayout.PageId = pageid
                    If contacttype <> "I" Then
                        objlayout.numRelation = CInt(type.Text)
                    End If


                    objlayout.FormId = hfFormId.Value
                    objlayout.PageType = hfPType.Value

                    ds = objlayout.getValuesWithddl()
                    If (ds.Tables.Count = 2) Then
                        dttable1 = ds.Tables(0)
                        dttable2 = ds.Tables(1)
                    Else : dttable1 = ds.Tables(0)
                    End If
                    If contacttype = "T" Then
                        dttable1 = dttable2
                    Else
                        If (dttable2.Rows.Count > 0) Then dttable1.Merge(dttable2)
                    End If
                Else : dttable1 = objlayout.getValues()
                End If
                Dim dv As DataView
                If (contacttype = "c" Or contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "S" Or contacttype = "O" Or contacttype = "u" Or contacttype = "R" Or contacttype = "T" Or contacttype = "B" Or contacttype = "I") Then
                    dv = New DataView(dttable1)
                    If x <> 0 Then
                        dv.Sort = "tintRow"
                    ElseIf x = 0 Then
                        dv.Sort = "vcfieldName"
                    End If
                    dv.Table.AcceptChanges()
                    i = 0
                    count = 0
                    count = dv.Table.Rows.Count
                    str = str & "<td valign='top'>"
                    str = str & "<ul id='x" + x.ToString + "' class='sortable boxy' style='height:#MAXHEIGHT#' >"
                    While i < count
                        str = str & "<li id='" + dv(i).Item("numFieldId").ToString + "/" + dv(i).Item("bitCustomField").ToString + "/" + Convert.ToInt32(dv(i).Item("bitRequired")).ToString + "' >" + dv(i).Item("vcFieldName") + "</li>"
                        i += 1
                    End While
                    str = str & "</ul>"
                    str = str & " </td>"
                    x += 1
                    dttable1.Rows.Clear()
                Else
                    i = 0
                    count = 0
                    count = dttable1.Rows.Count
                    str = str & "<td valign='top'>"
                    str = str & "<ul id='x" + x.ToString + "' class='sortable boxy' style='height:#MAXHEIGHT#'>"
                    While i < count
                        str = str & "<li id='" + dttable1.Rows(i).Item("numFieldId").ToString + "' >" + dttable1.Rows(i).Item("vcFieldName") + "</li>"
                        i += 1
                    End While
                    str = str & "</ul>"
                    str = str & " </td>"
                    x += 1
                    dttable1.Rows.Clear()
                End If
                If count > maxHeight Then
                    maxHeight = count
                End If
            End While
            str = str & " </tr></table></td></tr></table>"
            str = str.Replace("#MAXHEIGHT#", CStr(maxHeight * 23) & "px;")
            lblMainContent.Text = str
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub savedata()
        Try
            Dim contacttype As String = hfCtype.Value
            Dim container(3) As String
            Dim header(10) As String
            Dim headerdata(10) As String
            Dim data1XML As String
            Dim temp(10) As String
            Dim data1(2) As String
            Dim i As Integer = 1
            Dim j As Integer = 0

            Dim objDT As System.Data.DataTable
            Dim objDT1 As System.Data.DataTable
            Dim objDR As System.Data.DataRow
            objDT = New System.Data.DataTable("Table")
            objDT1 = New System.Data.DataTable("Table")

            objDT.Columns.Add("numFieldID", GetType(Integer))
            objDT.Columns.Add("tintRow", GetType(Integer))
            objDT.Columns.Add("intColumn", GetType(Integer))
            objDT.Columns.Add("numUserCntId", GetType(Integer))
            objDT.Columns.Add("numDomainID", GetType(Integer))
            objDT.Columns.Add("bitCustomField", GetType(Integer))
            objDT.Columns.Add("numRelCntType", GetType(Integer))
            objDT.Columns.Add("Ctype", GetType(Char))

            Dim ds As New DataSet
            Dim objlayout As New CcustPageLayout
            Dim data As String = order.Value

            container = data.Split(":")

            Dim max As String
            max = rows.Text
            While (i <= max)
                temp = container(i).Split("(")
                header(i) = temp(0)
                temp = temp(1).Split(")")
                headerdata = temp(0).Split(",")
                j = 0
                While (j < headerdata.Length)
                    If (headerdata(j) <> "") Then
                        objDR = objDT.NewRow
                        data1 = headerdata(j).Split("/")
                        objDR("numFieldID") = data1(0)
                        objDR("tintRow") = j + 1
                        objDR("intColumn") = i

                        If contacttype = "B" Then
                            objDR("numUserCntId") = GetQueryStringVal("SiteId")
                        ElseIf hfPType.Value = 3 Then
                            objDR("numUserCntId") = Session("UserContactId")
                        Else
                            objDR("numUserCntId") = 0
                        End If

                        objDR("numDomainID") = Session("domainId")
                        If (contacttype = "c" Or contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "S" Or contacttype = "O" Or contacttype = "u" Or contacttype = "R" Or contacttype = "T" Or contacttype = "B") Then
                            If (data1(1) = "False" Or data1(1) = "0") Then
                                objDR("bitCustomField") = 0
                            ElseIf (data1(1) = "True" Or data1(1) = "1") Then
                                objDR("bitCustomField") = 1
                            End If
                            objDR("numRelCntType") = CInt(type.Text)
                        ElseIf contacttype = "I" Then
                            objDR("numRelCntType") = 0
                            If (data1(1) = "False" Or data1(1) = "0") Then
                                objDR("bitCustomField") = 0
                            ElseIf (data1(1) = "True" Or data1(1) = "1") Then
                                objDR("bitCustomField") = 1
                            End If
                        Else
                            objDR("numRelCntType") = 0
                            objDR("bitCustomField") = 0
                        End If
                        objDR("Ctype") = contacttype

                        objDT.Rows.Add(objDR)
                    End If
                    j += 1
                End While

                ds.Tables.Add(objDT)
                data1XML = ds.GetXml()
                objlayout.DomainID = Session("domainId")
                objlayout.FormId = hfFormId.Value
                objlayout.PageType = hfPType.Value

                If contacttype = "B" Then
                    objlayout.UserCntID = GetQueryStringVal("SiteId")
                ElseIf hfPType.Value = 3 Then
                    objlayout.UserCntID = Session("UserContactId")
                Else
                    objlayout.UserCntID = 0
                End If


                'objlayout.tintRow = i
                objlayout.ColumnID = i
                objlayout.CoType = hfCtype.Value
                objlayout.RowString = data1XML
                If (contacttype = "c" Or contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "T") Then
                    objlayout.numRelCntType = CInt(type.Text)
                End If
                objlayout.SaveList()
                objDT.Rows.Clear()
                If ds.Tables.Count > 0 Then ds.Tables.Remove(ds.Tables(0))
                i += 1
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub btnBindData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBindData.Click
        Try
            If hfPType.Value = 2 Then
                If hfFormId.Value = 34 Or hfFormId.Value = 35 Or hfFormId.Value = 36 Then
                    Select Case CInt(type.Text)
                        Case 1
                            hfFormId.Value = "34"
                            hfCtype.Value = "L"
                        Case 3
                            hfFormId.Value = "35"
                            hfCtype.Value = "P"
                        Case Else
                            hfFormId.Value = "36"
                            hfCtype.Value = "P"
                    End Select
                ElseIf hfFormId.Value = 86 Or hfFormId.Value = 87 Or hfFormId.Value = 88 Then
                    Select Case CInt(type.Text)
                        Case 1
                            hfFormId.Value = "86"
                            hfCtype.Value = "I"
                        Case 2
                            hfFormId.Value = "87"
                            hfCtype.Value = "I"
                        Case 3
                            hfFormId.Value = "88"
                            hfCtype.Value = "I"
                    End Select
                End If
            End If

            binddata()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnUpdate1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate1.Click
        Try
            savedata()
            binddata()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "close", "opener.location.reload(true);window.close()", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class