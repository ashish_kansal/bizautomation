Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Outlook
Imports Telerik.Web.UI
Imports System.Data.SqlClient
Imports DatabaseNotification
Imports System.Data
Imports System.Web.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization

Namespace BACRM.UserInterface.Common
    Public Class TicklerDisplay
        Inherits BACRMPage

#Region "Member Variables"
        Dim CustomDataProvider As CustomDataProvider
        Dim m_aryRightsForAdmin() As Integer
#End Region

#Region "Page Events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                Dim objTab As New Tabs
                objTab.GroupID = Session("UserGroupID")
                objTab.RelationshipID = 0
                objTab.ProfileID = 0
                objTab.DomainID = Session("DomainID")
                Dim str As String
                str = objTab.GetFirstPage
                If Not str Is Nothing Then mainframe.Attributes.Add("src", "../" & str)

                If Session("DomainID") > 0 Then

                    'Session("PopupURL") = hdnPopupURL.Value
                    If Not IsPostBack Then
                        'bindAlertPanel()
                        If Session("UserContactID") = "1" Then
                            tdSubscription.Visible = True
                            tdEditHelp.Visible = True
                        End If

                        ''Checking The View Rights For Tickler
                        If Session("UserContactID") <> Session("AdminID") Then 'Logged in User is Admin of Domain? yes then override permission and give him access

                            m_aryRightsForAdmin = GetUserRightsForPage_Other(13, 46)
                            If m_aryRightsForAdmin(RIGHTSTYPE.VIEW) = 0 Then tdAdmin.Visible = False

                            m_aryRightsForAdmin = GetUserRightsForPage_Other(13, 48)
                            If m_aryRightsForAdmin(RIGHTSTYPE.VIEW) = 0 Then tdAdvanced.Visible = False
                        End If
                        lblLoggedin.Text = "<b>Logged in as:</b> " & Session("ContactName")
                        If Session("Trial") = True Then
                            lblSubMessage.Text = "Trial Version"
                        ElseIf Session("NoOfDaysLeft") < 30 Then
                            lblSubMessage.Text = "Subscription will expire in " & Session("NoOfDaysLeft") & " days"
                        End If

                        'Following function is commentated because we are now not showing seperate reminders
                        'action item. it is merged with calender reminder popup.
                        'OpenActionItemAlert()
                        BuildTotalMenu()
                        BuildShortCutBar()
                        SetSessionTimeout()

                        hypConfMenu.Attributes.Add("onclick", "javascript:return OpenConf()")
                        hplEditHelp.Attributes.Add("onclick", "javascript:$(""#mainframe"")[0].contentWindow.location.href='../HelpModule/HelpList.aspx';UnSelectTabs();")
                        hplSubscription.Attributes.Add("onclick", "javascript:$(""#mainframe"")[0].contentWindow.location.href='../Service/frmSerNav.htm';UnSelectTabs();")
                        hplAdministration.Attributes.Add("onclick", "javascript:$(""#mainframe"")[0].contentWindow.location.href='../admin/frmAdminNav.aspx';UnSelectTabs();")
                        hplHelp.Attributes.Add("onclick", "javascript:UnSelectTabs();window.open('../admin/frmLinks.aspx','Help');")
                        hplAdvanced.Attributes.Add("onclick", "javascript:$(""#mainframe"")[0].contentWindow.location.href='../admin/frmAdvNav.aspx';UnSelectTabs();")

                        Dim objWF As New Workflow
                        objWF.DomainID = Session("DomainID")
                        Dim dtResult As DataSet = objWF.GetAlertMessageData()
                        If dtResult.Tables(0).Rows.Count > 0 Then
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "MyKey", "OpenAlertsWindow();", True)
                        Else
                            lblAlertMessage.Text = Nothing

                        End If
                    End If
                    bindCalendar()
                End If
                lnkbtnLogOut.Attributes.Add("onclick", "return Login()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
#End Region

#Region "Alert Panel"

        <WebMethod> _
        Public Shared Function OrgnizationAlert() As List(Of Organization)
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim lst As New List(Of Organization)()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim ds As New DataSet()
                    Dim da As Npgsql.NpgsqlDataAdapter
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_alertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "V"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 1})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            Dim org As New Organization()
                            org.recordId = Convert.ToInt32(ds.Tables(0).Rows(i)("numDivisionID"))
                            org.companyName = Convert.ToString(ds.Tables(0).Rows(i)("vcCompanyName"))
                            org.createdby = Convert.ToString(ds.Tables(0).Rows(i)("vcUserName"))
                            org.createdDate = Convert.ToString(ds.Tables(0).Rows(i)("bintCreatedDate"))
                            org.CRMType = Convert.ToString(ds.Tables(0).Rows(i)("tintCRMType"))
                            lst.Add(org)
                        Next
                    End If
                End Using
            End Using
            Return lst
        End Function

        <WebMethod>
        Public Shared Function CaseNAlert() As List(Of CaseAlert)
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim lst As New List(Of CaseAlert)()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim ds As New DataSet
                    Dim da As Npgsql.NpgsqlDataAdapter
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_alertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "CV"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 2})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            Dim _caseAlert As New CaseAlert()
                            _caseAlert.caseId = Convert.ToInt32(ds.Tables(0).Rows(i)("numCaseId"))
                            _caseAlert.OrganizationName = Convert.ToString(ds.Tables(0).Rows(i)("OrganizationName"))
                            _caseAlert.CreatedBy = Convert.ToString(ds.Tables(0).Rows(i)("vcUserName"))
                            _caseAlert.CreatedDate = Convert.ToString(ds.Tables(0).Rows(i)("bintCreatedDate"))
                            _caseAlert.ContactName = Convert.ToString(ds.Tables(0).Rows(i)("ContactName"))
                            lst.Add(_caseAlert)
                        Next
                    End If
                End Using
            End Using
            Return lst
        End Function

        ''' <summary>
        ''' For Purchase/Sales Opportunity and Order
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <WebMethod>
        Public Shared Function POSAlert() As List(Of CaseAlert)
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim lst As New List(Of CaseAlert)()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim ds As New DataSet
                    Dim da As Npgsql.NpgsqlDataAdapter
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_alertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "PSO"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 3})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            Dim _caseAlert As New CaseAlert()
                            _caseAlert.caseId = Convert.ToInt32(ds.Tables(0).Rows(i)("numOppId"))
                            _caseAlert.CaseName = Convert.ToString(ds.Tables(0).Rows(i)("vcPOppName"))
                            _caseAlert.OppType = Convert.ToString(ds.Tables(0).Rows(i)("tintOppType"))
                            _caseAlert.OppStatus = Convert.ToString(ds.Tables(0).Rows(i)("tintOppStatus"))
                            _caseAlert.OrganizationName = Convert.ToString(ds.Tables(0).Rows(i)("vcCompanyName"))
                            _caseAlert.CreatedBy = Convert.ToString(ds.Tables(0).Rows(i)("vcUserName"))
                            _caseAlert.CreatedDate = Convert.ToString(ds.Tables(0).Rows(i)("bintCreatedDate"))
                            _caseAlert.ContactName = Convert.ToString(ds.Tables(0).Rows(i)("ContactName"))
                            lst.Add(_caseAlert)
                        Next
                    End If
                End Using
            End Using
            Return lst
        End Function

        ''' <summary>
        ''' For Projects
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <WebMethod>
        Public Shared Function ProjectAlert() As List(Of CaseAlert)
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim lst As New List(Of CaseAlert)()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim ds As New DataSet
                    Dim da As Npgsql.NpgsqlDataAdapter
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_alertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "PV"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 7})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            Dim _caseAlert As New CaseAlert()
                            _caseAlert.caseId = Convert.ToInt32(ds.Tables(0).Rows(i)("numProId"))
                            _caseAlert.CaseName = Convert.ToString(ds.Tables(0).Rows(i)("vcProjectName"))
                            _caseAlert.OrganizationName = Convert.ToString(ds.Tables(0).Rows(i)("vcCompanyName"))
                            _caseAlert.CreatedBy = Convert.ToString(ds.Tables(0).Rows(i)("vcUserName"))
                            _caseAlert.CreatedDate = Convert.ToString(ds.Tables(0).Rows(i)("bintCreatedDate"))
                            lst.Add(_caseAlert)
                        Next
                    End If
                End Using
            End Using
            Return lst
        End Function

        ''' <summary>
        ''' For Email Alert
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <WebMethod>
        Public Shared Function EmailAlert() As List(Of CaseAlert)
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim lst As New List(Of CaseAlert)()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim ds As New DataSet
                    Dim da As Npgsql.NpgsqlDataAdapter
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_alertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "EV"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 8})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            Dim _caseAlert As New CaseAlert()
                            _caseAlert.caseId = Convert.ToInt32(ds.Tables(0).Rows(i)("numEmailHstrID"))
                            _caseAlert.CaseName = Convert.ToString(ds.Tables(0).Rows(i)("vcSubject"))
                            _caseAlert.CreatedBy = Convert.ToString(ds.Tables(0).Rows(i)("vcUserName"))
                            _caseAlert.CreatedDate = Convert.ToString(ds.Tables(0).Rows(i)("bintCreatedDate"))
                            lst.Add(_caseAlert)
                        Next
                    End If
                End Using
            End Using
            Return lst
        End Function

        ''' <summary>
        ''' For Tickler Alert
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <WebMethod>
        Public Shared Function TicklerAlert() As List(Of CaseAlert)
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim lst As New List(Of CaseAlert)()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim ds As New DataSet
                    Dim da As Npgsql.NpgsqlDataAdapter
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_alertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "TV"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 9})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            Dim _caseAlert As New CaseAlert()
                            _caseAlert.caseId = Convert.ToInt32(ds.Tables(0).Rows(i)("numCommId"))
                            _caseAlert.CaseName = Convert.ToString(ds.Tables(0).Rows(i)("vcCompanyName"))
                            _caseAlert.OppStatus = Convert.ToString(ds.Tables(0).Rows(i)("vcGivenName"))
                            _caseAlert.CreatedBy = Convert.ToString(ds.Tables(0).Rows(i)("vcUserName"))
                            _caseAlert.CreatedDate = Convert.ToString(ds.Tables(0).Rows(i)("bintCreatedDate"))
                            lst.Add(_caseAlert)
                        Next
                    End If
                End Using
            End Using
            Return lst
        End Function

        ''' <summary>
        ''' For Process Alert
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <WebMethod>
        Public Shared Function ProcessAlert() As List(Of CaseAlert)
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim lst As New List(Of CaseAlert)()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim ds As New DataSet
                    Dim da As Npgsql.NpgsqlDataAdapter
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_alertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "PVV"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 10})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            Dim _caseAlert As New CaseAlert()
                            _caseAlert.caseId = Convert.ToInt32(ds.Tables(0).Rows(i)("numStageDetailsId"))
                            _caseAlert.CaseName = Convert.ToString(ds.Tables(0).Rows(i)("ProgressOn"))
                            _caseAlert.OppStatus = Convert.ToString(ds.Tables(0).Rows(i)("tintOppStatus"))
                            _caseAlert.OppType = Convert.ToString(ds.Tables(0).Rows(i)("tintOppType"))
                            _caseAlert.OrganizationName = Convert.ToString(ds.Tables(0).Rows(i)("RecordId"))
                            _caseAlert.CreatedBy = Convert.ToString(ds.Tables(0).Rows(i)("vcUserName"))
                            _caseAlert.CreatedDate = Convert.ToString(ds.Tables(0).Rows(i)("bintCreatedDate"))
                            lst.Add(_caseAlert)
                        Next
                    End If
                End Using
            End Using
            Return lst
        End Function


        <WebMethod>
        Public Shared Function DeleteRecord(ModuleId As String, RecordId As String) As String
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim lst As New List(Of Organization)()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim dt As New DataTable()
                    Dim da As New Npgsql.NpgsqlDataAdapter()
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_alertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "D"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = ModuleId})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = RecordId})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim res As Int16
                    res = command.ExecuteNonQuery()
                End Using
            End Using
            Return "1"
        End Function

#End Region

#Region "Alert Count Panel"


        <WebMethod>
        Public Shared Function AllAlertcount() As List(Of String)
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim dt As New DataTable()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim da As New Npgsql.NpgsqlDataAdapter()
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_countalertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "ALLC"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 1})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    Dim ds As New DataSet
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End Using
            End Using
            Return DataSetToJSON(dt)
        End Function
        Public Shared Function DataSetToJSON(dt As DataTable) As List(Of String)

            Dim lst As New List(Of String)()
            'foreach (DataTable dt in ds.Tables)
            '{
            Dim arr As Object() = New Object(dt.Rows.Count) {}

            For i As Integer = 0 To dt.Columns.Count - 1
                lst.Add(dt.Rows(0)(i))
            Next

            '}
            Return lst
        End Function
        <WebMethod>
        Public Shared Function OrgnizationAlertcount() As Int32
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim dt As New DataTable()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim da As New Npgsql.NpgsqlDataAdapter()
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_countalertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "V"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 1})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    Dim ds As New DataSet
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End Using
            End Using
            HttpContext.Current.Cache("Alert_Company_" & HttpContext.Current.Session("UserContactId")) = Convert.ToInt32(dt.Rows(0)(0))
            Return Convert.ToInt32(dt.Rows(0)(0))
        End Function

        <WebMethod>
        Public Shared Function CaseNAlertcount() As Int32
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim dt As New DataTable()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim da As New Npgsql.NpgsqlDataAdapter()
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_countalertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "CV"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 2})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    Dim ds As New DataSet
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End Using
            End Using
            HttpContext.Current.Cache("Alert_Case_" & HttpContext.Current.Session("UserContactId")) = Convert.ToInt32(dt.Rows(0)(0))
            Return Convert.ToInt32(dt.Rows(0)(0))
        End Function

        ''' <summary>
        ''' For Purchase/Sales Opportunity and Order
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <WebMethod>
        Public Shared Function POSAlertcount(type As String) As Int32
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim dt As New DataTable()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim da As New Npgsql.NpgsqlDataAdapter()
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_countalertpanel"
                    If type = "1" Then
                        command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "PO"})
                    Else
                        command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "SO"})
                    End If
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 3})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    Dim ds As New DataSet
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End Using
            End Using

            If type = "1" Then
                HttpContext.Current.Cache("Alert_Opp_" & HttpContext.Current.Session("UserContactId")) = Convert.ToInt32(dt.Rows(0)(0))
            Else
                HttpContext.Current.Cache("Alert_Order_" & HttpContext.Current.Session("UserContactId")) = Convert.ToInt32(dt.Rows(0)(0))
            End If
            Return Convert.ToInt32(dt.Rows(0)(0))
        End Function

        ''' <summary>
        ''' For Projects
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <WebMethod>
        Public Shared Function ProjectAlertcount() As Int32
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim dt As New DataTable()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim da As New Npgsql.NpgsqlDataAdapter()
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_countalertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "PV"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 7})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    Dim ds As New DataSet
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End Using
            End Using
            HttpContext.Current.Cache("Alert_Project_" & HttpContext.Current.Session("UserContactId")) = Convert.ToInt32(dt.Rows(0)(0))
            Return Convert.ToInt32(dt.Rows(0)(0))
        End Function

        ''' <summary>
        ''' For Email Alert
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <WebMethod>
        Public Shared Function EmailAlertcount() As Int32
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim dt As New DataTable()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim da As New Npgsql.NpgsqlDataAdapter()
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_countalertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "EV"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 8})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    Dim ds As New DataSet
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End Using
            End Using
            HttpContext.Current.Cache("Alert_Email_" & HttpContext.Current.Session("UserContactId")) = Convert.ToInt32(dt.Rows(0)(0))
            Return Convert.ToInt32(dt.Rows(0)(0))
        End Function

        ''' <summary>
        ''' For Tickler Alert
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <WebMethod>
        Public Shared Function TicklerAlertcount() As Int32
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim dt As New DataTable()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim da As New Npgsql.NpgsqlDataAdapter()
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_countalertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "TV"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 9})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    Dim ds As New DataSet
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End Using
            End Using
            HttpContext.Current.Cache("Alert_Tickler_" & HttpContext.Current.Session("UserContactId")) = Convert.ToInt32(dt.Rows(0)(0))
            Return Convert.ToInt32(dt.Rows(0)(0))
        End Function

        ''' <summary>
        ''' For Process Alert
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <WebMethod>
        Public Shared Function ProcessAlertcount() As Int32
            Dim conStr As String = ConfigurationManager.AppSettings("ConnectionString")
            'Dim org As New Organization()
            Dim dt As New DataTable()
            Using connection As New Npgsql.NpgsqlConnection(conStr)
                'Dim query As String = "select vcCompanyName,numCompanyId from [dbo].[CompanyInfo]"

                Using command As New Npgsql.NpgsqlCommand()
                    Dim da As New Npgsql.NpgsqlDataAdapter()
                    command.CommandType = CommandType.StoredProcedure
                    command.Connection = connection
                    command.CommandText = "usp_countalertpanel"
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_chraction", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Char, .Value = "PPV"})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("DomainID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numuserid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = Convert.ToInt32(System.Web.HttpContext.Current.Session("UserContactID"))})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_nummoduleid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 10})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numrecordid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = 0})
                    command.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim j As Int32 = 0
                    Dim ds As New DataSet
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        command.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In command.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    da.Fill(ds.Tables(j))
                                    j += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End Using
            End Using
            HttpContext.Current.Cache("Alert_Process_" & HttpContext.Current.Session("UserContactId")) = Convert.ToInt32(dt.Rows(0)(0))
            Return Convert.ToInt32(dt.Rows(0)(0))
        End Function

#End Region

#Region "Private Methods"
        'Sub bindAlertPanel()
        '    Try
        '        Dim AlertPanelAccess As New UserGroups()
        '        AlertPanelAccess.DomainID = Convert.ToInt32(Session("DomainID"))
        '        AlertPanelAccess.GroupId = Convert.ToInt32(Session("UserGroupID"))
        '        AlertPanelAccess.ModuleId = Convert.ToInt32(42)
        '        Dim dtAlert As New DataTable()
        '        dtAlert = AlertPanelAccess.GetAllSelectedSystemModulesPagesAndAccesses()
        '        Dim i As Integer
        '        For i = 0 To dtAlert.Rows.Count - 1
        '            If dtAlert.Rows(i)("numPageID") = "118" AndAlso dtAlert.Rows(i)("intViewAllowed") = "3" Then
        '                SendNotifications()
        '                OrganizationList.Visible = True
        '            ElseIf dtAlert.Rows(i)("numPageID") = "119" AndAlso dtAlert.Rows(i)("intViewAllowed") = "3" Then
        '                SendCaseNotifications()
        '                CaseList.Visible = True
        '            ElseIf dtAlert.Rows(i)("numPageID") = "120" AndAlso dtAlert.Rows(i)("intViewAllowed") = "3" Then
        '                SendOpportunityNotifications()
        '                oppList.Visible = True
        '            ElseIf dtAlert.Rows(i)("numPageID") = "121" AndAlso dtAlert.Rows(i)("intViewAllowed") = "3" Then
        '                SendOpportunityNotifications()
        '                orderList.Visible = True
        '            ElseIf dtAlert.Rows(i)("numPageID") = "122" AndAlso dtAlert.Rows(i)("intViewAllowed") = "3" Then
        '                SendProjectNotifications()
        '                projectList.Visible = True
        '            ElseIf dtAlert.Rows(i)("numPageID") = "123" AndAlso dtAlert.Rows(i)("intViewAllowed") = "3" Then
        '                SendEmailNotifications()
        '                emailList.Visible = True
        '            ElseIf dtAlert.Rows(i)("numPageID") = "124" AndAlso dtAlert.Rows(i)("intViewAllowed") = "3" Then
        '                SendTicklerNotifications()
        '                TicklerList.Visible = True
        '            ElseIf dtAlert.Rows(i)("numPageID") = "125" AndAlso dtAlert.Rows(i)("intViewAllowed") = "3" Then
        '                SendProcessNotifications()
        '                ProcessList.Visible = True
        '            End If
        '        Next
        '    Catch ex As Exception

        '    End Try
        'End Sub
        Sub bindCalendar()
            Try
                If Session("DomainID") > 0 Then

                    If Session("ResourceId") Is Nothing Then
                        Dim objOutlook As New COutlook
                        Dim dtTable As DataTable
                        objOutlook.UserCntID = Session("UserContactId")
                        objOutlook.DomainID = Session("DomainId")
                        dtTable = objOutlook.GetResourceId()
                        If Not dtTable Is Nothing Then
                            If dtTable.Rows.Count > 0 Then
                                ' Dim activeuser As String = Session("ContactName").ToString
                                Session("ResourceId") = dtTable.Rows(0).Item("ResourceId")
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'replacing Aspnet Expert Menu Control with telerik as it doesn't support keeping tab highlated.-by chintan 30/01/2010
        Private Sub BuildTotalMenu()
            Try
                If Session("DomainID") > 0 Then
                    Dim dtTabData As DataTable
                    Dim objTab As New Tabs
                    objTab.GroupID = Session("UserGroupID")
                    objTab.RelationshipID = 0
                    objTab.DomainID = Session("DomainID")
                    dtTabData = objTab.GetTabData()
                    Dim i As Integer
                    For i = 0 To dtTabData.Rows.Count - 1
                        dtTabData.Rows(i).Item("vcURL") = Replace(dtTabData.Rows(i).Item("vcURL"), "RecordID", Session("UserContactID"))

                        If dtTabData.Rows(i).Item("bitFixed") = True Then
                            CreateItem(dtTabData.Rows(i).Item("numTabName"), dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"), CCommon.ToString(dtTabData.Rows(i).Item("vcImage")))
                        Else
                            CreateItem(dtTabData.Rows(i).Item("numTabName"), "include/SimpleMenu.aspx?Page=" & dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"), CCommon.ToString(dtTabData.Rows(i).Item("vcImage")))
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BuildShortCutBar()
            Try
                If Session("DomainID") > 0 Then

                    If Session("TabEvent") = 0 Then
                        RadTabStrip1.OnClientMouseOver = "onTabMouseOver"
                        RadTabStrip1.OnClientMouseOut = "onTabMouseOut"
                        RadTabStrip1.OnClientTabSelected = "onTabSelected"

                        RadTabStrip1.OnClientDoubleClick = ""
                        RadTabStrip1.OnClientTabSelecting = ""
                    ElseIf Session("TabEvent") = 1 Then
                        RadTabStrip1.OnClientMouseOver = ""
                        RadTabStrip1.OnClientMouseOut = ""
                        RadTabStrip1.OnClientTabSelected = ""

                        RadTabStrip1.OnClientDoubleClick = "onTabDoubleClick"
                        RadTabStrip1.OnClientTabSelecting = "OnTabSelecting"
                    End If

                    Dim objShortCutBar As New CShortCutBar
                    objShortCutBar.ContactId = Session("UserContactID")
                    objShortCutBar.DomainID = Session("DomainID")
                    objShortCutBar.GroupId = Session("UserGroupID")
                    Dim ds As New DataSet
                    ds = objShortCutBar.GetMenu()
                    Dim dtTab As DataTable
                    dtTab = Session("DefaultTab")

                    'divFavourite.Controls.Clear()

                    Dim dtTable As DataTable
                    dtTable = ds.Tables(0)

                    Dim name, link, NewLink As String

                    Dim sb As New System.Text.StringBuilder
                    Dim iRowIndex As Integer = 0
                    sb.Append(vbCrLf & "var arrTabFieldConfig = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript

                    Dim strFavourit As String = String.Empty

                    For Each row As DataRow In dtTable.Rows
                        If row.Item("id") = 145 Then 'Remove Assembly Items
                            Continue For
                        End If

                        NewLink = ""
                        link = ""
                        name = ""

                        row.Item("Link") = Replace(row.Item("Link"), "RecordID", Session("UserContactID"))

                        If CCommon.ToBool(row("bitNew")) = True AndAlso CCommon.ToString(row.Item("NewLink")).Length > 0 Then
                            If CCommon.ToBool(row("bitNewPopup")) = True Then
                                NewLink = "Goto('" & row.Item("NewLink") & "','cntOpenItem','divNew',1000,645)"
                            Else
                                NewLink = "reDirect('" & row.Item("NewLink") & "',1)"
                            End If
                        End If

                        If CCommon.ToBool(row("bitPopup")) = True Then
                            link = "Goto('" & row.Item("Link") & "','cntOpenItem','divNew',1000,645)"
                        Else
                            If CCommon.ToBool(row("bitNew")) = True AndAlso NewLink.Length = 0 Then
                                link = "reDirect('" & row.Item("Link") & "',1)"
                            Else
                                link = "reDirect('" & row.Item("Link") & "',0)"
                            End If
                        End If

                        Select Case row.Item("Name").ToString
                            Case "MyLeads" : name = "My " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                            Case "WebLeads" : name = "Web " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                            Case "PublicLeads" : name = "Public " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                            Case "Contacts" : name = dtTab.Rows(0).Item("vcContact").ToString & "s"
                            Case "Prospects" : name = dtTab.Rows(0).Item("vcProspect").ToString & "s"
                            Case "Accounts" : name = dtTab.Rows(0).Item("vcAccount").ToString & "s"
                            Case "Action-Item" : name = row.Item("Name").ToString & "s"
                            Case Else
                                name = row.Item("Name")
                        End Select
                        If link.ToLower().Contains("frmcompanylist.aspx") Then
                            name = row.Item("Name") & "s"
                        End If

                        sb.Append("arrTabFieldConfig[" & iRowIndex & "] = new TabMenuLink('" & row("id").ToString & "','" & name & "',""" & link & """,'" & CCommon.ToLong(row("numTabId")) & "','" & CCommon.ToString(row("vcImage")) & "','" & CCommon.ToBool(row("bitNew")) & "',""" & NewLink & """);" & vbCrLf)
                        iRowIndex += 1

                        If CCommon.ToBool(row("bitFavourite")) = True Then
                            strFavourit += "<a class=""normal11"" style=""cursor: pointer;font-size:11.5px;color:#F5F5DC;"" onclick=""" & link & """ title=""" & name & """>" & name & "</a>"

                            If CCommon.ToBool(row("bitNew")) = True AndAlso NewLink.Length > 0 Then
                                strFavourit += " <a class=""normal11"" style=""cursor: pointer;font-size:11.5px;"" onclick=""" & NewLink & """><img class=""alignbottom"" border=""0"" title=""" & name & """ src=""../images/AddRecord.png""></a>"
                            End If

                            strFavourit += "  |  "
                        End If
                    Next

                    divFavourite.InnerHtml = "&nbsp;&nbsp;&nbsp;" & strFavourit.TrimEnd().TrimEnd("|")
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "TabMenuScript", sb.ToString(), True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub SetSessionTimeout()
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = Session("DomainID")
                Dim dtTable As DataTable = objUserAccess.GetDomainDetails()
                If dtTable.Rows.Count > 0 Then
                    hdnSessionTimeout.Value = CCommon.ToInteger(dtTable.Rows(0)("tintSessionTimeOut")) * 60 * 60
                Else
                    hdnSessionTimeout.Value = 60 * 60
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub OpenActionItemAlert()
            Try
                Dim i As Integer
                Dim objAdmin As New ActionItem
                Dim dtActItemAlert As DataTable
                objAdmin.UserCntID = Session("UserContactID")
                objAdmin.DomainID = Session("DomainID")
                objAdmin.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtActItemAlert = objAdmin.GetActionItemAlert
                Dim sb As New System.Text.StringBuilder

                For i = 0 To dtActItemAlert.Rows.Count - 1
                    sb.Append("window.open('../admin/ActionItemDetailsOld.aspx?Popup=True&CommID=" & dtActItemAlert.Rows(i).Item("Id") & "&ContID=" & dtActItemAlert.Rows(i).Item("numContactId") & "&DivID=" & dtActItemAlert.Rows(i).Item("numDivisionID") & "&CompID=" & dtActItemAlert.Rows(i).Item("numCompanyId") & "','','width=800,height=600,status=no,scrollbar=yes,top=110,left=150');")
                Next
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), sb.ToString, True)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub CreateItem(ByVal text As String, ByVal URL As String, ByVal Value As Long, ByVal Image As String)
            Try
                Dim radtab As New RadTab(text, Value)
                radtab.Target = "mainframe"
                radtab.NavigateUrl = "../" & URL

                If Image.Length > 0 Then
                    radtab.ImageUrl = Image
                End If

                RadTabStrip1.Tabs.Add(radtab)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
#End Region

#Region "Event Handlers"
        Private Sub btnDismiss_Click(sender As Object, e As EventArgs) Handles btnDismiss.Click
            Try
                If Not String.IsNullOrEmpty(hdnActionItemID.Value) AndAlso Not String.IsNullOrEmpty(hdnType.Value) Then
                    If hdnType.Value = "1" Then
                        'Calender
                        Dim ObjOutlook As New COutlook
                        ObjOutlook.ActivityID = CCommon.ToLong(hdnActionItemID.Value)
                        ObjOutlook.Status = 0
                        ObjOutlook.LastReminderDateTimeUtc = DateTime.UtcNow
                        ObjOutlook.UpdateReminderByActivityId()
                    ElseIf hdnType.Value = "2" Then
                        'Action Item / Communication
                        Dim objActionItem As New ActionItem
                        objActionItem.CommID = CCommon.ToLong(hdnActionItemID.Value)
                        objActionItem.DismissReminder()
                    End If

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ReloadReminder", "RemindCall();", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BuildShortCutBar()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

    End Class
End Namespace

