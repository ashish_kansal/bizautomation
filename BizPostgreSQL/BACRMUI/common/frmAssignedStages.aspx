<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmAssignedStages.aspx.vb" Inherits="BACRM.UserInterface.Common.frmAssignedStages" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Assigned Stages</title>
		<script language="javascript">
		function ShowWindow(Page,q,att) 
		{
			if (att=='show')
			{
			    document.getElementById(Page).style.visibility = "visible";
				return false;
		
			}
			if (att=='hide')
			{
				document.getElementById(Page).style.visibility = "hidden";
				return false;
		
			}
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td class="text_bold">Assigned Stages
					</td>
				</tr>
				<tr>
					<td><asp:table id="tblDetails" GridLines="None" Width="100%" Runat="server"></asp:table></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
