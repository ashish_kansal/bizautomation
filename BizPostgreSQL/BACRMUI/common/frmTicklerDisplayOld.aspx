﻿<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false"
    MasterPageFile="~/common/DetailPage.Master" CodeBehind="frmTicklerDisplayOld.aspx.vb" 
    Inherits="BACRM.UserInterface.Common.frmTicklerDisplayOld" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Tickler Display</title>
    <script type="text/javascript" src='<%# ResolveUrl("~/JavaScript/colResizable-1.5.min.js")%>'></script>
        <style>
        
        .multiselect-group {
            background-color: #3e3e3e !important;
        }
         .multiselect-group a:hover{
             background-color:#3e3e3e !important;
         }
            .multiselect-group a {
                color: #fff !important;
            }

        .multiselect-container > li.multiselect-group label {
            padding: 3px 20px 3px 5px !important;
        }
        .multiselect-container {
    min-width: 250px;
    border: 1px solid #e1e1e1;
}
        .table-bordered > tbody > tr > th {
            border-bottom-width: 2px;
            overflow: inherit !important;
        }
    </style>
    <link href='<%# ResolveUrl("~/JavaScript/MultiSelect/bootstrap-multiselect.css")%>' rel="stylesheet" />
    <script src='<%# ResolveUrl("~/JavaScript/MultiSelect/bootstrap-multiselect.js")%>'></script>
    <style>
        .underline{
                text-decoration: underline;
        }
          .overlay1
        {
          position: fixed;
          z-index: 99;
          top: 0px;
          left: 0px;
          right: 0px;
          bottom: 0px;
          background-color:rgba(170, 170, 170, 0.5);
          filter: alpha(opacity=80);
        }

        .overlayContent
        {
          z-index: 99;
          margin: 250px auto;
        }
    </style>
    <script type="text/javascript">
        $(document).keydown(function (event) {
            if (event.keyCode == 13) {
                on_filter();
            }
        });
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
            if ($('.option-droup-multiSelection-Group') != null) {
                var prm = Sys.WebForms.PageRequestManager.getInstance();


                $('[id$=gvSearch] select').change(function () {
                    debugger;
                    if ($(this).prop("className") == "option-droup-multiSelection-Group") {
                        return false;
                    } else {
                        on_filter();
                    }
                });
            prm.add_endRequest(function () {
                $('.option-droup-multiSelection-Group').multiselect({
                    enableClickableOptGroups: true,
                    onSelectAll: function () {
                        console.log("select-all-nonreq");
                    },
                    optionClass: function (element) {
                        var value = $(element).attr("class");
                        return value;
                    }
                });
                $("#hplClearGridCondition").click(function () {
                    $('#txtGridColumnFilter').val("");
                    $('[id$=btnGo]').trigger('click');
                });
            });
            $('.option-droup-multiSelection-Group').multiselect({
                enableClickableOptGroups: true,
                onSelectAll: function () {
                    console.log("select-all-nonreq");
                },
                optionClass: function (element) {
                    var value = $(element).attr("class");
                    return value;
                }
            });
            }
        });
        function pageLoaded() {
            $("#hplClearGridCondition").click(function () {
                $('#txtGridColumnFilter').val("");
                $('[id$=btnGo]').trigger('click');
            });

            $('[id$=gvSearch] select').change(function () {
                debugger;
                if ($(this).prop("className") == "option-droup-multiSelection-Group") {
                    return false;
                } else {
                    on_filter();
                }
            });
        }
        function BindTicklerUserActivity() {
            var DomainID = '<%= Session("DomainId")%>';
            var UserContactID = '<%= Session("UserContactID")%>';
            var dataParam = "{DomainID:'" + DomainID + "',UserContactID:'" + UserContactID + "'}";
            var arrayToCheck = [];
            $.ajax({
                type: "POST",
                url: "../common/frmTicklerDisplay.aspx/WebMethodGetTaskList",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    debugger
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse.length > 0) {
                        $.each(Jresponse, function (index, value) {
                            var IsExist = $.inArray(value.numStageDetailsId, arrayToCheck);
                            debugger;
                            if (value.dtmDueDate == null) {
                                value.dtmDueDate = '-';
                            }
                            if (IsExist >= 0) {
                                $("#spanStageDetailsId" + value.numStageDetailsId + "").append("," + "<a class='underline' href='#'  onclick='openTask("+value.numOppID+","+value.numStagePercentageId+","+value.tintPercentage+","+value.tintConfiguration+")'>" + value.vcTaskName + "</a>");
                            } else {
                                arrayToCheck.push(value.numStageDetailsId);
                                $("#tblUserActivity").append("<tr><td>"+value.dtmDueDate+"</td><td id='spanStageDetailsId"+value.numStageDetailsId+"'><a class='underline' href='#' onclick='openTask("+value.numOppID+","+value.numStagePercentageId+","+value.tintPercentage+","+value.tintConfiguration+")'>"+value.vcTaskName+"</a></td><td>"+value.OrderType+"</td><td>"+value.vcMileStoneName+","+value.vcStageName+"</td><td><a class='underline' href='#' onclick='openSalesOrder("+value.numOppID+")'>"+value.vcPOppName+"</a></td></tr>");
                            }
                        });
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }
        function openSalesOrder(oppId) {
            window.open("../opportunity/frmOpportunities.aspx?opId=" + oppId, '_blank');
        }
        function openTask(oppId,numStagePercentageId,tinProgressPercentage,tintConfiguration) {
            window.open("../opportunity/frmOpportunities.aspx?opId=" + oppId+"&MileStoneId="+tintConfiguration+"_"+numStagePercentageId+"_"+tinProgressPercentage, '_blank');
        }
        // Grid Column Resize
        $(document).ready(function () {
            BindTicklerUserActivity()
            BindJquery();

            $("#hplClearGridCondition").click(function () {
                $('#txtGridColumnFilter').val("");
                $('[id$=btnGo]').trigger('click');
            });
            $("[id$=gvSearch]").colResizable({
                liveDrag: true,
                gripInnerHtml: "<div class='grip2'></div>",
                draggingClass: "dragging",
                minWidth: 30,
                onResize: onActionGridColumnResized
            });
            ManualStyleTuneUps();

        });

        function ManualStyleTuneUps() {
            $('.table-striped .chkSelect').closest('td').css('text-align', 'center');
            $('.table-striped #chkAll').closest('td').css('width', '30px');
        }

        var onActionGridColumnResized = function (e) {
            var columns = $(e.currentTarget).find("th");
            var msg = "";
            columns.each(function () {
                //console.log($(this));

                var thId = $(this).attr("id");
                var thWidth = $(this).width();
                msg += (thId || "anchor") + ':' + thWidth + ';';
            }
            )
            //console.log(msg);

            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/SaveGridColumnWidth",
                data: "{str:'" + msg + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });

            //on_filter();
        };
        function reDirect(a) {
            document.location.href = a;
        }

        function progress(ProgInPercent, Container, progress, prg_width) {
            //Set Total Width
            var OuterDiv = document.getElementById(Container);
            OuterDiv.style.width = prg_width + 'px';
            OuterDiv.style.height = '5px';

            if (ProgInPercent > 100) {
                ProgInPercent = 100;
            }
            //Set Progress Percentage
            var node = document.getElementById(progress);
            node.style.width = parseInt(ProgInPercent) * prg_width / 100 + 'px';
        }
        function OpenOpp(a, b) {

            var str;

            str = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" + a;
            window.open(str, '_blank');
            //document.location.href = str;
        }

        function OpenActionItem(a,b) {
            document.location.href = "../admin/actionitemdetailsold.aspx?CommId=" + a + "&lngType=" + b
            return false;
        }
        function OpenDocument(a) {

            document.location.href = "../Documents/frmAddSpecificDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=O&yunWE=12515&tyuTN=" + a + "";
            document.location.href = "../Documents/frmDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocId=" + a + "&frm=Tickler&SI1=0&SI2=0&TabID=1";
            return false;
        }
        function OpenBizDocActionItem(a, b, c) {
            var radTab = $find("radOppTab");
            var selectedIndex = radTab.get_selectedIndex();

            document.location.href = "../admin/BizDocActionDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BizDocActionId=" + a + "&frm=tickler" + "&SI1=" + selectedIndex +

                "&BizDocDate=" + b + "&ContactID=" + c
            return false
        }
        function OpenCalItem(a) {//qwwq
            if (a == null || a=="") { window.open("https://calendar.google.com/", '_blank') } else {
                window.open(a, '_blank')
            }
           
        }
        function OpenCompany(a, b, c) {
            var radTab = $find("radOppTab");
            //alert(radTab)
            var selectedIndex = radTab.get_selectedIndex();

            if (b == 0) {
                window.location.href = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=tickler&DivID=" + a + "&SI1=" + selectedIndex
            }

            else if (b == 1) {
                window.location.href = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=tickler&DivID=" + a + "&SI1=" + selectedIndex
            }
            else if (b == 2) {
                window.location.href = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=tickler&klds+7kldf=fjk-las&DivId=" + a + "&SI1=" + selectedIndex
            }

            return false;
        }
        function OpenContact(a, b) {
            var radTab = $find("radOppTab");
            var selectedIndex = radTab.get_selectedIndex();

            window.location.href = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=tickler&fda45s=oijf6d67s&CntId=" + a + "&SI1=" + selectedIndex

            return false;
        }

        function OpenEmail(str) {
            window.open(str)
            return true;
        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }
        function openStage(a, b, c, type, d) {
            document.getElementById('cntDoc').src = "../common/frmAssignedStages.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Oppid=" + a + "&startdate=" + b + "&enddate=" + c + "&TicklerType=" + d + "&OppType=" + type
            document.getElementById('divStg').style.visibility = "visible";
            return false;
        }
        function openAdmin() {
            window.location.href = "../admin/frmAdminSection.aspx";
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function OpenSelTeam() {
            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=1", '',

                'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function openEmpAvailability() {
            window.open("../ActionItems/frmEmpAvailability.aspx?frm=Tickler", '',

                'toolbar=no,titlebar=no,top=0,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.getElementById("btnGo").click();
        }
        function OpemEmail(URL) {
            window.open(URL, 'Compose','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenSetting() {
            window.open('../Items/frmConfItemList.aspx?FormID=43', '',

                'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function EditExpenses(id) {
            window.open('../TimeAndExpense/frmEditTimeExp.aspx?CatID=' + id + '', '', 'toolbar=no,titlebar=no,top=200,left=200,width=750,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenFilterSetting() {
            window.open('../common/frmTicklerListFilterSetting.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function SortColumn(a) {

            if ($("[id$=txtSortColumnActionItems]").val() == a) {
                if ($("[id$=txtSortOrderActionItems]").val() == 'D')
                    $("[id$=txtSortOrderActionItems]").val('A');
                else
                    $("[id$=txtSortOrderActionItems]").val('D');
            }
            else {
                $("[id$=txtSortColumnActionItems]").val(a);
                $("[id$=txtSortOrderActionItems]").val('D');
            }
            $('[id$=btnGo]').trigger('click');
            return false;
        }
        function BindJquery() {
            $("[id$=gvSearch] input:text").keydown(function (event) {
                if (event.keyCode == 13) {
                    console.log($('#txtGridColumnFilter').val());
                    on_filter();
                    return false;
                }
            });

            //$('[id$=gvSearch] select').change(function () {
            //    on_filter();
            //    console.log("Selected");
            //});

            $("#hplClearGridCondition").click(function () {
                $('#txtGridColumnFilter').val("");
                $('[id$=btnGo]').trigger('click');
            });
        }
        $(document).on('change', '[id$=gvSearch] tr th select', function () {
            debugger;
            var d = $(this);
            //Your code
            on_filter();
        });
        function on_filter() {

            var filter_condition = '';

            var dropDownControls = $('[id$=gvSearch] select');
            dropDownControls.each(function () {
                if ($(this).get(0).selectedIndex > 0) {
                    var ddId = $(this).attr("id");
                    var ddValue = $(this).val();
                    filter_condition += ddId + ':' + ddValue + ';';
                }
            }
            )

            var textBoxControls = $('[id$=gvSearch] input:text');
            textBoxControls.each(function () {
                if ($.trim($(this).val()).length > 0) {
                    var txtId = $(this).attr("id");
                    var txtValue = $.trim($(this).val());
                    filter_condition += txtId + ':' + txtValue + ';';
                }
            }
            )

            $('#txtGridColumnFilter').val(filter_condition);
            //console.log($('#txtGridColumnFilter').val());
            //$('#txtCurrrentPage').val('1');
            $('[id$=btnGo]').trigger('click');

        }
        function DeleteActionItemsConfirmation() {

            var gridName;
            var grdValue;
            grdValue = $find("radOppTab").get_selectedTab().get_value();
            console.log(grdValue);

            if (grdValue == 'ActionItemsMeetings') {
                gridName = 'gvSearch';
            }
            else if (grdValue == 'Cases') {
                gridName = 'dgCases'
            }
            else {
                alert('Error in javascript. Please contact System administrator.!');
                return false;
            }

            if (DeleteRecord(gridName) == true) {
                if (confirm('Selected Action Items will be deleted and removed from the list (N/A for BizDoc Approvals). Do you want to proceed ?')) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        function CloseActionItemsConfirmation() {
            if (confirm('Selected Action Items will be closed and removed from the list (N/A for BizDoc Approvals or Calendar items). Do you want to proceed ?')) {
                return true;
            }
            else {
                return false;
            }
        }

        //select all 
        function SelectAll(headerCheckBox, ItemCheckboxClass, gridName) {
            $("[id$=" + gridName + "] ." + ItemCheckboxClass + " input[type = 'checkbox']").each(function () {
                $(this).prop('checked', $('#' + gridName + ' #' + headerCheckBox).is(':checked'))
            });
        }

        function DeleteRecord(gridName) {
            var i = 0;
            $("[id$=" + gridName + "] #chkSelect").each(function () {
                //console.log($(this));
                if ($(this).is(':checked')) {
                    i += 1;
                }
            });

            if (i == 0) {
                alert('Please select atleast one record to delete!');
                return false;
            }
            return true;
        }
        function OpenHelp() {
            window.open('../Help/Tickler_Initial_Detail_Page_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function OnClientTabSelected(sender, eventArgs) {
            var tab = eventArgs.get_tab();
            PersistTab(tab.get_value());
            //Added By:Richa Garg ||Date:25Feb2019
            //Puprose : for persistent tabs
        }

       function PersistTab(index) {
            var key;
            var value;
            var formName;

            key = "index";
            value = index;

            var pagePath = window.location.pathname + "/PersistTab";
            var dataString = "{strKey:'" + key + "',strValue:'" + value + "'}";
            IsExists(pagePath, dataString);
        }

          function IsExists(pagePath, dataString) {
            $.ajax({
                type: "POST",
                url: pagePath,
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error:
                    function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log("Error");
                    },
                success:
                    function (result) {
                        console.log("success");
                    }
            });
        }


    </script>
    <script type="text/javascript">
        var arrFieldConfig = new Array();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div style="display: none;" class="pull-left">
                <telerik:RadComboBox ID="radBProcesses" runat="server" Skin="Default" CheckBoxes="true">
                    <FooterTemplate>
                        <asp:Button ID="btnBProcess" CssClass="button" runat="server" Text="Search" OnClick="btnBProcess_Click"></asp:Button>
                    </FooterTemplate>
                </telerik:RadComboBox>
            </div>
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Select Tickler:</label>
                        <telerik:RadComboBox ID="radTickler" runat="server" Width="210" Skin="Default" AutoPostBack="true" Style="padding-left: 1px">
                            <Items>
                                <telerik:RadComboBoxItem Value="0" Text="--Select--" Selected="true" />
                                <telerik:RadComboBoxItem Value="1" Text="Yesterday, Today, and Tomorrow" />
                                <telerik:RadComboBoxItem Value="2" Text="Yesterday through next 7 days" />
                                <telerik:RadComboBoxItem Value="3" Text="Last 7 days through next 7 days" />
                                <telerik:RadComboBoxItem Value="4" Text="Last month through next week" />
                                <telerik:RadComboBoxItem Value="5" Text="Last week through next month" />
                                <telerik:RadComboBoxItem Value="6" Text="Last month through next month" />
                                <telerik:RadComboBoxItem Value="7" Text="Last year through this year" />
                                <telerik:RadComboBoxItem Value="8" Text="Last year through next year" />
                            </Items>
                        </telerik:RadComboBox>
                    </div>
                    <div class="form-group">
                        <label>From</label>
                        <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <label>To</label>
                        <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" />

                </div>
            </div>
            <div class="pull-right">
                <asp:LinkButton ID="lnkAddNewActionItem" runat="server" OnClick="lnkAddNewActionItem_Click" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</asp:LinkButton>
                <asp:Button runat="server" ID="btnCloseAction" CssClass="btn btn-primary" Text="Close Action Item(s)" />
                <asp:LinkButton ID="btnDeleteCases" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" AutoPostBack="false" SelectedIndex="0" OnClientTabSelected="OnClientTabSelected" 
        MultiPageID="radMultiPage_OppTab" ClientIDMode="Static">
        <Tabs>
            <telerik:RadTab Text="Action Items & Meetings" Value="ActionItemsMeetings" PageViewID="radPageView_ActionItemsMeetings">
            </telerik:RadTab>
            <telerik:RadTab Text="Business Process Tasks" Value="OpportunitiesProjects" PageViewID="radPageView_OpportunitiesProjects">
            </telerik:RadTab>
            <telerik:RadTab Text="Cases" Value="Cases" PageViewID="radPageView_Cases">
            </telerik:RadTab>
            <telerik:RadTab Text="Approval Requests" Value="TEApproval" PageViewID="radPageView_ApprovalApplication">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_ActionItemsMeetings" runat="server">
            <div class="row">
                <div class="col-xs-12">
                    
                    <div class="table-responsive">
                        <div class="pull-right">

                            <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        ShowPageIndexBox="Never"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>


                        </div>
                        <div class="pull-left">
                <div class="form-inline">
                    <asp:RadioButton ID="radOpen" GroupName="rad" AutoPostBack="true"
                        runat="server" Text="Open" />
                    <asp:RadioButton ID="radClosed" GroupName="rad" AutoPostBack="true" runat="server"
                        Text="Closed" />
                    <asp:RadioButton ID="radAll" GroupName="rad" AutoPostBack="true" runat="server"
                        Text="All" />
                    </div>
                        </div>

                        <asp:HiddenField ID="hdnDeleteType" runat="server" />
            <asp:HiddenField ID="hdnlngCommID" runat="server" />
            <asp:HiddenField ID="hdnlngRecOwnID" runat="server" />
             <asp:HiddenField ID="hdnnumContactID" runat="server" />
            <asp:HiddenField ID="hdnlngTerrID" runat="server" />
            <div class="overlay1" id="divActivityDeletepopup" runat="server" visible="false">
                <div class="overlayContent" style="color: #000; background-color: #FFF; width: 50%; border: 1px solid #ddd; max-height: 500px; overflow: hidden">
                    <div class="dialog-header">
                        <div>
                           
                            <b style="font-size: 14px">Some records have a link to an external Calendar.</b>
                        </div>
                         <br>
                          <div><b style="font-size: 14px;padding-left:10px;"> How would you like the deletion command to be carried out:</b> </div>
                    </div>
                    <br>
                    <div class="dialog-body" style="width: 100%; padding-left:10px;">
                         <div>
 
                        <asp:RadioButtonList ID="rblDeleteType" runat="server">
                             <asp:ListItem Text="Delete the entry only from BizAutomation" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Delete the entry from both BizAutomation & your external calendar" Value="1" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList></div>
                    </div>

                    <div style="padding-left: 240px; padding-bottom:10px;">
                            <asp:Button ID="btnActivityDelete" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Execute Command & Close" OnClick="btnActivityDelete_Click" />
                        </div>
                </div>
            </div>

                        <asp:UpdatePanel ID="UpdatePanelHiddenField" runat="server" UpdateMode="Always">
        <ContentTemplate>
            
        </ContentTemplate>
    </asp:UpdatePanel>

                        <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                            CssClass="table table-bordered table-striped" Width="100%" DataKeyNames="ID,CaseId,CaseTimeId,CaseExpId,type,itemid,numContactID,numCreatedBy,numTerId">
                            <Columns>
                            </Columns>
                            <EmptyDataTemplate>
                                No records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>

        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_OpportunitiesProjects" runat="server">
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tblUserActivity">
                            <thead>
                                <tr>
                                    <th>Due Date</th>
                                    <th>Tasks</th>
                                    <th>Type</th>
                                    <th>Milestone & Stages</th>
                                    <th>Record Name</th>
                                </tr>
                            </thead>
                        </table>
                        <asp:DataGrid ID="dgOppPro" Visible="false" runat="server" Width="100%" CssClass="table table-bordered table-striped" AutoGenerateColumns="false"
                            AllowSorting="True" UseAccessibleHeader="true">
                            <Columns>
                                <asp:BoundColumn Visible="False" DataField="ID"></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="numStageDetailsId"></asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="CloseDate" HeaderText="Due Date">
                                    <ItemTemplate>
                                        <%# ReturnDate(DataBinder.Eval(Container.DataItem, "CloseDate"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn SortExpression="type" HeaderText="Type" DataField="type"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="vcPOppname" SortExpression="vcPOppname" HeaderText="Name" CommandName="Opportunity"></asp:ButtonColumn>
                                <asp:BoundColumn DataField="vcStageName" SortExpression="vcStageName" HeaderText="Assigned Stage"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Tprogress" SortExpression="Tprogress" HeaderText="Total Progress"></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="numTerId"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>

        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Cases" runat="server">
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:DataGrid ID="dgCases" runat="server" Width="100%" CssClass="table table-bordered table-striped" AutoGenerateColumns="False"
                            AllowSorting="True" UseAccessibleHeader="true">
                            <Columns>
                                <asp:BoundColumn Visible="False" DataField="numCaseId"></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="numTerId"></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="numRecOwner"></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="textSubject"></asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="intTargetResolveDate" HeaderText="Due Date">
                                    <ItemTemplate>
                                        <%-- <%# ReturnDate(DataBinder.Eval(Container.DataItem, "intTargetResolveDate")) %>--%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:ButtonColumn DataTextField="vcCaseNumber" SortExpression="vcCaseNumber" HeaderText="Case #"
                                    CommandName="Case"></asp:ButtonColumn>
                                <asp:ButtonColumn DataTextField="Contact" SortExpression="Contact" HeaderText="Name"
                                    CommandName="Contact"></asp:ButtonColumn>
                                <asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="Phone - Ext"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="vcCompanyName" SortExpression="vcCompanyName" HeaderText="Company"
                                    CommandName="Company"></asp:ButtonColumn>
                                <asp:TemplateColumn SortExpression="vcEmail" HeaderText="Email">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hplCaseEmail" runat="server" Target="_blank" Text='<%#DataBinder.Eval(Container.DataItem, "vcEmail") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn HeaderText="Assigned To" DataField="AssignedTo" SortExpression="AssignedTo"></asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkAll" ToolTip="Select All" runat="server" CssClass="chkAll" onclick="SelectAll('chkAll','chkSelect','dgCases')" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkSelect" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                    <HeaderStyle HorizontalAlign="Center" Width="10px" />
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ApprovalApplication" runat="server">
            <div class="row" style="margin-right: 10px; margin-bottom: 10px;">
                <div class="pull-right">
                    <div class="form-inline">
                        <div class="form-group">
                            <label>Filter</label>
                            <asp:DropDownList ID="ddlFilter" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" CssClass="form-control" AutoPostBack="true" runat="server">
                                <asp:ListItem Value="0">All</asp:ListItem>
                                <asp:ListItem Value="1">T&E Approval Request</asp:ListItem>
                                <asp:ListItem Value="2">Price Margin Approval Request</asp:ListItem>
                                <asp:ListItem Value="3">Deal Status Approval Request</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:GridView ID="grdApproveTE" OnRowCommand="grdApproveTE_RowCommand" OnRowEditing="grdApproveTE_RowEditing" OnRowDataBound="grdApproveTE_RowDataBound" CssClass="table table-bordered" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" EmptyDataText="No Pending Request for Approval" runat="server">
                            <Columns>
                                <%--<asp:BoundField DataField="ApprovalRequests" HeaderText="Approval Requests"></asp:BoundField>--%>
                                <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkEdit" Visible="false" runat="server" OnClientClick='<%# string.Format("javascript:return EditExpenses({0})", Eval("RecordId"))%>' CommandName="TEEdit" CommandArgument='<%#Eval("RecordId")%>'><%#Eval("ApprovalRequests")%></asp:LinkButton>
                                        <asp:Label ID="lblApprovalRequest" runat="server" Text='<%#Eval("ApprovalRequests")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="CreatedDate" HeaderText="Entered By, On"></asp:BoundField>
                                <asp:TemplateField HeaderText="Sales Order (Service)">
                                    <ItemTemplate>
                                        <a href="<%#If(Eval("numOppId") > 0,"javascript:OpenOpp(" & Eval("numOppId") & ")","") %>"><%#Eval("vcPOppname")%></a> <%#Eval("vcItemName") %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="ClientCharge" HeaderText="Amt billed to client"></asp:BoundField>
                                <asp:BoundField DataField="EmployeeCharge" HeaderText="Employee Cost"></asp:BoundField>
                                <asp:BoundField DataField="vcEmployee" HeaderText="Employee"></asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>
                                <asp:BoundField DataField="Notes" HeaderText="Notes"></asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div style="white-space: nowrap;">
                                            <asp:LinkButton ID="lnkApprove" runat="server" CssClass="btn btn-xs btn-primary" CommandName="Approve" CommandArgument='<%#Eval("RecordId")%>'><i class="fa fa-thumbs-o-up"></i></asp:LinkButton>
                                            <asp:LinkButton ID="lnkDecline" runat="server" CssClass="btn btn-xs btn-danger" CommandName="Decline" CommandArgument='<%#Eval("RecordId")%>'><i class="fa fa-thumbs-o-down"></i></asp:LinkButton>

                                            <asp:HiddenField ID="hdnApprovalType" runat="server" Value='<%#Eval("TypeApproval")%>' />
                                            <asp:HiddenField ID="hdnApprovalStaus" runat="server" Value='<%#Eval("ApprovalStatus")%>' />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <asp:Button ID="btnGo" runat="server" Style="display: none" EnableViewState="false" />
    <asp:TextBox ID="txtSortColumnActionItems" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtSortOrderActionItems" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtSortColumnOppPro" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtSortOrderOppPro" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtSortColumnCases" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtSortOrderCases" runat="server" Style="display: none">
    </asp:TextBox>

    

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Tickler Details&nbsp;<a href="#" onclick="return OpenHelp()"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <a href="#" onclick="return OpenFilterSetting()" class="btn btn-xs btn-default">
        <i class="fa fa-gear"></i>&nbsp;<b style="vertical-align: middle">List Filter</b>
    </a>
    <asp:HyperLink ID="hplEmpAvaliability" runat="server" NavigateUrl="#" CssClass="btn btn-xs btn-default">Availability</asp:HyperLink>
    <asp:LinkButton ID="btnTeam" CssClass="btn btn-xs btn-default" runat="server">Choose Teams</asp:LinkButton>
    <a href="#" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
    <div id="divGridConfiguration" runat="server" style="display: inline">
        <a href="#" onclick="return OpenSetting()" title="Show Grid Settings." class="btn-box-tool"><i class="fa fa-2x fa-gear"></i></a>
    </div>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
       <asp:TextBox ID="txtTotalPageDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsDeals" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
