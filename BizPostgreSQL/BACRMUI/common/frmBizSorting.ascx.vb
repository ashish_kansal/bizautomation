﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Projects

Imports BACRM.BusinessLogic.Common

Public Class frmBizSorting
    Inherits BACRMUserControl

#Region "PRIVATE VARIABLES"

    Private _strSortChar As String
    Private _intCurrentPage As Integer

#End Region

#Region "PUBLIC PROPERTIES"

    Public Property SortChar() As String
        Get
            Return _strSortChar
        End Get
        Set(ByVal value As String)
            _strSortChar = value
        End Set
    End Property

    Public Property CurrentPage() As Integer
        Get
            Return _intCurrentPage
        End Get
        Set(ByVal value As Integer)
            _intCurrentPage = value
        End Set
    End Property

#End Region

#Region "FORM EVENTS"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'If Not String.IsNullOrEmpty(_strSelected) Then
            '    __CreateLinks(_strSelected)
            'Else
            '    __CreateLinks("0")
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub
#End Region

#Region "CUSTOM SUBROUTINES"

    Private Sub __CreateLinks(ByVal selectedChar As String)
        Try
            Dim objTable As New HtmlControls.HtmlTable
            Dim objTR As HtmlControls.HtmlTableRow = New HtmlControls.HtmlTableRow
            Dim intColLower As Integer = 97
            Dim iCol As Integer = 0

            For intColUpper As Integer = 65 To 90
                Dim objTD As HtmlControls.HtmlTableCell = New HtmlControls.HtmlTableCell

                objTD.Attributes.Add("bgcolor", "#52658C")
                objTD.Attributes.Add("onmousemove", "bgColor='#c6d3e7'; this.color='black'")
                objTD.Attributes.Add("onmouseout", "bgColor='#52658C'; this.color='white'")

                Dim aDiv As New System.Web.UI.HtmlControls.HtmlGenericControl("DIV")
                aDiv.InnerHtml = ChrW(intColUpper)
                aDiv.Style.Add(HtmlTextWriterStyle.Height, "15px")
                aDiv.Style.Add(HtmlTextWriterStyle.Width, "25px")

                If String.Compare(selectedChar.ToLower, ChrW(intColLower)) = 0 Then
                    aDiv.Attributes.Add("class", "A2Zn")
                Else
                    aDiv.Attributes.Add("class", "A2Z")
                End If

                Dim objA As New HtmlAnchor
                objA.ID = ChrW(intColLower)
                objA.Attributes.Add("href", "javascript:fnSortByChar('" & ChrW(intColLower) & "')")

                objA.Controls.Add(aDiv)
                objTD.Controls.Add(objA)
                objTR.Controls.Add(objTD)

                intColLower += 1
                iCol += 1
            Next

            Dim objTDAll As HtmlControls.HtmlTableCell = New HtmlControls.HtmlTableCell
            objTDAll.Attributes.Add("bgcolor", "#52658C")
            objTDAll.Attributes.Add("onmousemove", "bgColor='#c6d3e7'; this.color='black'")
            objTDAll.Attributes.Add("onmouseout", "bgColor='#52658C'; this.color='white'")

            Dim aAllDiv As New System.Web.UI.HtmlControls.HtmlGenericControl("DIV")
            aAllDiv.InnerHtml = "ALL"
            aAllDiv.Style.Add(HtmlTextWriterStyle.Height, "15px")
            aAllDiv.Style.Add(HtmlTextWriterStyle.Width, "25px")

            If String.Compare(selectedChar.ToLower, "0") = 0 Then
                aAllDiv.Attributes.Add("class", "A2Zn")
            Else
                aAllDiv.Attributes.Add("class", "A2Z")
            End If

            Dim objAll As New HtmlAnchor
            objAll.ID = "0"
            objAll.Attributes.Add("href", "javascript:fnSortByChar('0')")

            objAll.Controls.Add(aAllDiv)
            objTDAll.Controls.Add(objAll)
            objTR.Controls.Add(objTDAll)

            objTable.Rows.Add(objTR)
            'pnlSort.Controls.Add(objTable)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region


End Class