﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Workflow
Partial Public Class Milestone
    Inherits BACRMUserControl

    Dim _dtSalesProcess, dtStageStatus, dtAssignto, dtTime As DataTable
    Dim _objCommon As CCommon
    Dim trHeader As HtmlTableRow
    Dim previousStagePercentage, CurrentStagePercentage As Integer
    Dim objOutLook As COutlook
    Dim objActionItem As ActionItem
    Dim _ModuleType As ModuleType
    Dim _RecordOwner, _Customer, _OppProName As String
    Dim _RecID, _DivisionID, _RecordOwnerID, _RecordContactID, _CusPrgMgr, _IntPrgMgr As Long
    Dim _OppAmount As Decimal

    Enum ModuleType As Integer
        Opportunity
        Projects
    End Enum


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Sub LoadStageStatus()
        Try
            dtStageStatus = _objCommon.GetMasterListItems(42, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadAssignTo()
        Try
            '_objCommon.DomainID = Session("DomainId")
            '_objCommon.ContactType = 92
            'dtAssignto = _objCommon.AssignTo
            If Session("PopulateUserCriteria") = 1 Then
                _objCommon.DivisionID = _DivisionID
                _objCommon.charModule = "D"
                _objCommon.GetCompanySpecificValues1()
                dtAssignto = _objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, _objCommon.TerittoryID)
            ElseIf Session("PopulateUserCriteria") = 2 Then
                dtAssignto = _objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
            Else
                dtAssignto = _objCommon.ConEmpList(Session("DomainID"), 0, 0)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Sub BindRepeaterControl()
        Try


            If _dtSalesProcess.Rows.Count > 0 Then
                LoadStageStatus()
                LoadAssignTo()
                Dim dv As New DataView(_dtSalesProcess)
                dv.Sort = "numStagePercentage"
                dv.RowFilter = " numStagePercentage <> 0 and numStagePercentage <> 100"
                rptMilestone.DataSource = dv
                rptMilestone.DataBind()

                If _dtSalesProcess.Rows.Count >= 2 Then
                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("chkDealLost"), CheckBox).Checked = _dtSalesProcess.Rows(0).Item("bitStageCompleted")
                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("txtDealLostComment"), TextBox).Text = _dtSalesProcess.Rows(0).Item("vcComments")
                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdDealLost"), HiddenField).Value = _dtSalesProcess.Rows(0).Item("bitStageCompleted")
                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdLostOppStageID"), HiddenField).Value = _dtSalesProcess.Rows(0).Item("StageID")
                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdLostOp_Flag"), HiddenField).Value = _dtSalesProcess.Rows(0).Item("Op_Flag")

                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("chkDealWon"), CheckBox).Checked = _dtSalesProcess.Rows(_dtSalesProcess.Rows.Count - 1).Item("bitStageCompleted")
                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("txtDealWonComment"), TextBox).Text = _dtSalesProcess.Rows(_dtSalesProcess.Rows.Count - 1).Item("vcComments")
                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdDealWon"), HiddenField).Value = _dtSalesProcess.Rows(_dtSalesProcess.Rows.Count - 1).Item("bitStageCompleted")
                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdWonOppStageID"), HiddenField).Value = _dtSalesProcess.Rows(_dtSalesProcess.Rows.Count - 1).Item("StageID")
                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdWonOp_Flag"), HiddenField).Value = _dtSalesProcess.Rows(_dtSalesProcess.Rows.Count - 1).Item("Op_Flag")
                Else

                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdLostOppStageID"), HiddenField).Value = 0

                    CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdWonOppStageID"), HiddenField).Value = 0
                End If
            Else
                rptMilestone.DataSource = _dtSalesProcess
                rptMilestone.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub




    Public Sub CreateTableMilestone()
        Try

            _objCommon.intOption = 17
            dtTime = _objCommon.GetDefaultValues

            If _dtSalesProcess Is Nothing Then
                _dtSalesProcess = New DataTable
                _dtSalesProcess.Columns.Add("numStagePercentage")
                _dtSalesProcess.Columns.Add("numstagedetailsID")
                _dtSalesProcess.Columns.Add("vcstageDetail")
                _dtSalesProcess.Columns.Add("StageID")
                _dtSalesProcess.Columns.Add("numCreatedBy")
                _dtSalesProcess.Columns.Add("bintCreatedDate") ', GetType(DateTime))
                _dtSalesProcess.Columns.Add("numModifiedBy")
                _dtSalesProcess.Columns.Add("bintModifiedDate") ', GetType(DateTime))
                _dtSalesProcess.Columns.Add("bintDueDate") ', GetType(DateTime))
                _dtSalesProcess.Columns.Add("bintStageComDate") ', GetType(DateTime))
                _dtSalesProcess.Columns.Add("vcComments")
                _dtSalesProcess.Columns.Add("numAssignTo")
                _dtSalesProcess.Columns.Add("bitStageCompleted")
                _dtSalesProcess.Columns.Add("Op_Flag")
                _dtSalesProcess.Columns.Add("numModifiedByName")
                _dtSalesProcess.Columns.Add("Depend")
                _dtSalesProcess.Columns.Add("numTCategoryHDRID")
                _dtSalesProcess.Columns.Add("numECategoryHDRID")
                _dtSalesProcess.Columns.Add("Expense")
                _dtSalesProcess.Columns.Add("Time")
                _dtSalesProcess.Columns.Add("SubStg")
                _dtSalesProcess.Columns.Add("numStage")
                _dtSalesProcess.Columns.Add("vcStagePercentageDtl")
                _dtSalesProcess.Columns.Add("numTemplateId")
                _dtSalesProcess.Columns.Add("tintPercentage")
                _dtSalesProcess.Columns.Add("numEvent")
                _dtSalesProcess.Columns.Add("numReminder")
                _dtSalesProcess.Columns.Add("numET")
                _dtSalesProcess.Columns.Add("numActivity")
                _dtSalesProcess.Columns.Add("numStartDate")
                _dtSalesProcess.Columns.Add("numStartTime")
                _dtSalesProcess.Columns.Add("numStartTimePeriod")
                _dtSalesProcess.Columns.Add("numEndTime")
                _dtSalesProcess.Columns.Add("numEndTimePeriod")
                _dtSalesProcess.Columns.Add("txtCom")
                _dtSalesProcess.Columns.Add("numChgStatus")
                _dtSalesProcess.Columns.Add("bitChgStatus")
                _dtSalesProcess.Columns.Add("bitClose")
                _dtSalesProcess.Columns.Add("numType")
                _dtSalesProcess.Columns.Add("numCommActId")
                _dtSalesProcess.Columns.Add("bitAlert")
            End If

            Dim i As Integer
            Dim cal As UserControl
            Dim chkAlert As CheckBox
            Dim strOldDate As String
            Dim ddlAssignTo As DropDownList
            Dim boolModified, boolStageClosedNow, boolPreviousStageClosedNow, boolDealWon, boolDealLost, boolFirstStage, boolJustWon As Boolean


            boolDealWon = CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("chkDealWon"), CheckBox).Checked
            If CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdDealWon"), HiddenField).Value = "False" And boolDealWon Then
                boolJustWon = True
            End If

            boolDealLost = CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("chkDealLost"), CheckBox).Checked


            Dim dr, drPrevious As DataRow
            dr = _dtSalesProcess.NewRow
            AddNewRow(dr, 0, "Deal Lost", _
                      CCommon.ToLong(CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdLostOppStageID"), HiddenField).Value), _
                      CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("txtDealLostComment"), TextBox).Text, _
                      CCommon.ToInteger(CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdLostOp_Flag"), HiddenField).Value), _
                      boolDealLost, False)
            _dtSalesProcess.Rows.Add(dr)



            boolFirstStage = True
            For Each rptItems As RepeaterItem In rptMilestone.Items
                If rptItems.ItemType = ListItemType.AlternatingItem Or rptItems.ItemType = ListItemType.Item Then


                    boolModified = False
                    boolStageClosedNow = False
                    dr = _dtSalesProcess.NewRow

                    dr("vcstageDetail") = CType(rptItems.FindControl("txtStageName"), TextBox).Text
                    If dr("vcstageDetail") <> CType(rptItems.FindControl("hdStageDetail"), HiddenField).Value Then boolModified = True
                    dr("numStage") = CCommon.ToLong(CType(rptItems.FindControl("ddlStageStatus"), DropDownList).SelectedValue)
                    If dr("numStage") <> CType(rptItems.FindControl("hdStage"), HiddenField).Value Then boolModified = True
                    cal = rptItems.FindControl("calStageDueDate")
                    dr("bintDueDate") = CCommon.ToSqlDate(cal.GetType.GetProperty("SelectedDate").GetValue(cal, Nothing))
                    strOldDate = CType(rptItems.FindControl("hdDueDate"), HiddenField).Value

                    If Not IsDBNull(dr("bintDueDate")) Then
                        If strOldDate = "" Then boolModified = True Else If CDate(dr("bintDueDate")) <> CDate(CType(rptItems.FindControl("hdDueDate"), HiddenField).Value) Then boolModified = True
                    ElseIf IsDBNull(dr("bintDueDate")) And strOldDate <> "" Then
                        boolModified = True
                    End If



                    dr("tintPercentage") = CType(rptItems.FindControl("txtStagePercentage"), TextBox).Text
                    If dr("tintPercentage") <> CType(rptItems.FindControl("hdPercentage"), HiddenField).Value Then boolModified = True
                    dr("bitStageCompleted") = CType(rptItems.FindControl("chkStageClosed"), CheckBox).Checked
                    If dr("bitStageCompleted") <> CType(rptItems.FindControl("hdStageCompleted"), HiddenField).Value Then boolModified = True
                    If boolModified = True Then
                        dr("numModifiedBy") = Session("UserContactID")
                        dr("bintModifiedDate") = CCommon.ToSqlDate(Date.UtcNow)
                        If dr("bitStageCompleted") = True And CType(rptItems.FindControl("hdStageCompleted"), HiddenField).Value = "False" Then
                            dr("bintStageComDate") = CCommon.ToSqlDate(Date.UtcNow)
                            boolStageClosedNow = True
                        ElseIf dr("bitStageCompleted") = False Then
                            dr("bintStageComDate") = System.DBNull.Value
                        Else
                            If CType(rptItems.FindControl("hdStageComDate"), HiddenField).Value <> "" Then
                                dr("bintStageComDate") = CCommon.ToSqlDate(CType(rptItems.FindControl("hdStageComDate"), HiddenField).Value)
                            End If

                        End If
                    Else
                        dr("numModifiedBy") = CType(rptItems.FindControl("hdModifiedBy"), HiddenField).Value
                        dr("bintModifiedDate") = CCommon.ToSqlDate(CType(rptItems.FindControl("hdModifiedDate"), HiddenField).Value)
                        dr("bintStageComDate") = IIf(dr("bitStageCompleted") = True, CCommon.ToSqlDate(CType(rptItems.FindControl("hdStageComDate"), HiddenField).Value), System.DBNull.Value)
                    End If

                    ddlAssignTo = CType(rptItems.FindControl("ddlAssignTo"), DropDownList)

                    dr("numAssignTo") = ddlAssignTo.SelectedValue
                    If dr("numAssignTo") <> CType(rptItems.FindControl("hdAssignTo"), HiddenField).Value Then boolModified = False
                    dr("vcComments") = CType(rptItems.FindControl("txtStageComment"), TextBox).Text
                    If dr("vcComments") <> CType(rptItems.FindControl("hdComments"), HiddenField).Value Then boolModified = False


                    dr("StageID") = CType(rptItems.FindControl("hdOppStageID"), HiddenField).Value
                    'dr("vcstageDetail") = CType(rptItems.FindControl("hdStageDetail"), textbox).text
                    'dr("bintDueDate") = CType(rptItems.FindControl("hdDueDate"), textbox).text

                    'dr("bitStageCompleted") = CType(rptItems.FindControl("hdStageCompleted"), textbox).text
                    dr("numTCategoryHDRID") = CType(rptItems.FindControl("hdTCategoryHDRID"), HiddenField).Value
                    dr("numECategoryHDRID") = CType(rptItems.FindControl("hdECategoryHDRID"), HiddenField).Value
                    dr("Depend") = CType(rptItems.FindControl("hdDepend"), HiddenField).Value
                    dr("Expense") = CType(rptItems.FindControl("hdExpense"), HiddenField).Value
                    dr("Time") = CType(rptItems.FindControl("hdTime"), HiddenField).Value
                    'dr("numStage") = CType(rptItems.FindControl("hdStage"), textbox).text
                    dr("numTemplateId") = CType(rptItems.FindControl("hdTemplateId"), HiddenField).Value
                    dr("bintCreatedDate") = CCommon.ToSqlDate(CType(rptItems.FindControl("hdCreatedDate"), HiddenField).Value)
                    'dr("tintPercentage") = CType(rptItems.FindControl("hdPercentage"), textbox).text
                    dr("numEvent") = CType(rptItems.FindControl("hdEvent"), HiddenField).Value
                    dr("numReminder") = CType(rptItems.FindControl("hdReminder"), HiddenField).Value
                    'dr("vcComments") = CType(rptItems.FindControl("hdComments"), textbox).text
                    dr("vcStagePercentageDtl") = CType(rptItems.FindControl("hdStagePercentageDtl"), HiddenField).Value
                    dr("numET") = CType(rptItems.FindControl("hdET"), HiddenField).Value
                    dr("numActivity") = CType(rptItems.FindControl("hdActivity"), HiddenField).Value
                    dr("numStartDate") = CType(rptItems.FindControl("hdStartDate"), HiddenField).Value
                    dr("numStartTime") = CType(rptItems.FindControl("hdStartTime"), HiddenField).Value
                    dr("numStartTimePeriod") = CType(rptItems.FindControl("hdStartTimePeriod"), HiddenField).Value
                    dr("numEndTime") = CType(rptItems.FindControl("hdEndTime"), HiddenField).Value
                    dr("numEndTimePeriod") = CType(rptItems.FindControl("hdEndTimePeriod"), HiddenField).Value
                    dr("txtCom") = CType(rptItems.FindControl("hdCom"), HiddenField).Value
                    dr("numChgStatus") = CType(rptItems.FindControl("hdChgStatus"), HiddenField).Value
                    dr("bitClose") = CType(rptItems.FindControl("hdOnClose"), HiddenField).Value
                    dr("numType") = CType(rptItems.FindControl("hdType"), HiddenField).Value
                    dr("numCommActId") = CType(rptItems.FindControl("hdCommActId"), HiddenField).Value
                    dr("Op_Flag") = CType(rptItems.FindControl("hdOp_Flag"), HiddenField).Value
                    dr("numStagePercentage") = CType(rptItems.FindControl("lblStagePercentage"), Label).Text


                    chkAlert = CType(rptItems.FindControl("chkAlert"), CheckBox)
                    If chkAlert.Checked = True Then _dtSalesProcess.Rows(i).Item("bitAlert") = True Else _dtSalesProcess.Rows(i).Item("bitAlert") = False

                    'Sending Mail to the next Stage assignee
                    If boolPreviousStageClosedNow = True And boolStageClosedNow = False Then

                        'Alert ID is 2 for sending mail to actionee
                        SendMailToActionee(2, chkAlert.Checked, ddlAssignTo.SelectedItem.Text, dr("vcstageDetail"), dr("bintDueDate"), ddlAssignTo.SelectedValue)



                    End If


                    If chkAlert.Checked = True And dr("numAssignTo") > 0 Then

                        'Alert ID is 1 for sending mail to actionee
                        SendMailToActionee(1, chkAlert.Checked, ddlAssignTo.SelectedItem.Text, dr("vcstageDetail"), dr("bintDueDate"), ddlAssignTo.SelectedValue)



                    End If

                    StageEvents(boolFirstStage, dr, drPrevious, boolDealWon, boolDealLost)
                    drPrevious = dr
                    _dtSalesProcess.Rows.Add(dr)
                    If boolStageClosedNow = True Then boolPreviousStageClosedNow = True Else boolPreviousStageClosedNow = False
                End If
            Next

            dr = _dtSalesProcess.NewRow
            AddNewRow(dr, 100, "Deal Completed", _
                      CCommon.ToLong(CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdWonOppStageID"), HiddenField).Value), _
                      CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("txtDealWonComment"), TextBox).Text, _
                      CCommon.ToInteger(CType(rptMilestone.Controls(rptMilestone.Controls.Count - 1).Controls(0).FindControl("hdWonOp_Flag"), HiddenField).Value), _
                      boolDealWon, False)


            If boolJustWon Then
                'btnReceivedOrShipped.Visible = True
                'btnReceivedOrShipped.Attributes.Add("onclick", "return AlertMsg()")
                'uwOppTab.Tabs(0).Text = "Deal Details"
                'If OppType.Value = 1 Then
                '    btnReceivedOrShipped.Text = "Shipped"
                'Else : btnReceivedOrShipped.Text = "Received"
                'End If
                ViewState("Deal") = "True"
                ' Sending Mail to Record Owner's Manager

                SendMailOnDealCompletion()



                dr("bitStageCompleted") = True
            Else
                dr("bitStageCompleted") = False
            End If
            _dtSalesProcess.Rows.Add(dr)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub StageEvents(ByVal boolFirstStage As Boolean, ByRef drCurrentStage As DataRow, ByRef drPreviousStage As DataRow, ByVal boolDealWon As Boolean, ByVal boolDealLost As Boolean)
        Try


            'If Deal Is lost Delete All the Action Items and Calendar Items
            If Not boolDealLost Then
                'Must Have an assigneTo and Start Date Set
                'Stage Should not be 100 or 0 (Deal completed or Deal Lost)
                If boolDealWon Then
                    If drCurrentStage("bitStageCompleted") <> True Then
                        drCurrentStage("bitStageCompleted") = True
                        drCurrentStage("bintStageComDate") = CCommon.ToSqlDate(Date.UtcNow)
                    End If
                End If
                If drCurrentStage("numAssignTo") <> 0 _
                And CCommon.ToInteger(drCurrentStage("numStartDate")) <> 0 Then

                    Dim startDate As String
                    Dim startTime As String
                    Dim endTime As String
                    Dim bitAddUpdate As Boolean = True

                    '0 is deal closed ,for First Stage If Start date is selected as previous stage closedate Exit 
                    If boolFirstStage = True And drCurrentStage("numStartDate") >= 3 Then bitAddUpdate = False

                    If drCurrentStage("numStartDate") = 1 Then
                        startDate = CType(drCurrentStage("bintDueDate"), DateTime).Date
                    ElseIf drCurrentStage("numStartDate") = 2 Then
                        startDate = CType(drCurrentStage("bintCreatedDate"), DateTime).Date
                    Else
                        ' If Previous Stage Is Not Closed Exit
                        If drPreviousStage Is Nothing Then Exit Sub
                        If Not CBool(drPreviousStage("bitStageCompleted")) Then
                            Exit Sub
                        End If
                        startDate = CType(DateAdd(DateInterval.Day, drCurrentStage("numStartDate") - 3, _
                                    drPreviousStage("bintStageComDate")), DateTime).Date
                    End If
                    Dim strStartTimeText As String = IIf(drCurrentStage("numStartTimePeriod") > 0, "", ReturnTimeString(drCurrentStage("numStartTimePeriod")) & IIf(drCurrentStage("numStartTimePeriod") = 0, " AM", " PM"))
                    Dim strEndTimeText As String = IIf(drCurrentStage("numEndTimePeriod") > 0, "", ReturnTimeString(drCurrentStage("numEndTimePeriod")) & IIf(drCurrentStage("numEndTimePeriod") = 0, " AM", " PM"))
                    startTime = startDate.Trim & " " & strStartTimeText
                    endTime = startDate.Trim & " " & strEndTimeText

                    Dim CommActID As Long = drCurrentStage("numCommActId")

                    Select Case CInt(drCurrentStage("numEvent"))                    'Type Of Event

                        Case 2
                            'If no Action Item type is selected Exit
                            If drCurrentStage("numType") = 0 Then bitAddUpdate = False
                            If bitAddUpdate = True Then
                                If objActionItem Is Nothing Then objActionItem = New ActionItem
                                _objCommon.ContactID = _RecordContactID
                                _objCommon.charModule = "C"
                                _objCommon.GetCompanySpecificValues1()

                                With objActionItem
                                    .CommID = CommActID
                                    .Task = drCurrentStage("numType")
                                    .ContactID = _RecordContactID
                                    .DivisionID = _objCommon.DivisionID
                                    .Details = drCurrentStage("txtCom")
                                    .AssignedTo = drCurrentStage("numAssignTo")
                                    .UserCntID = _RecordContactID
                                    .DomainID = Session("DomainID")
                                    If drCurrentStage("numType") = 973 Or boolDealWon = True Then
                                        .BitClosed = 1
                                    Else : .BitClosed = 0
                                    End If
                                    If drCurrentStage("numType") = 973 Or drCurrentStage("numType") = 974 Then
                                        .StartTime = startDate
                                        .EndTime = DateAdd(DateInterval.Day, 1, .StartTime)
                                    Else
                                        .StartTime = startTime
                                        .EndTime = endTime
                                    End If
                                    .Activity = drCurrentStage("numActivity")
                                    .Status = 0
                                    .Snooze = 0
                                    .SnoozeStatus = 0
                                    .Remainder = drCurrentStage("numReminder")
                                    .RemainderStatus = 0
                                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                    .bitOutlook = 0
                                    .SendEmailTemplate = 0
                                    .EmailTemplate = 0
                                    .Hours = 0
                                    .Alert = 0
                                    .ActivityId = 0
                                    .CaseID = 0
                                End With
                                drCurrentStage("numCommActId") = objActionItem.SaveCommunicationinfo()
                                'Added By Sachin Sadhu||Date:4thAug2014
                                'Purpose :To Added Ticker data in work Flow queue based on created Rules
                                '          Using Change tracking
                                Dim objWfA As New Workflow()
                                objWfA.DomainID = Session("DomainID")
                                objWfA.UserCntID = Session("UserContactID")
                                objWfA.RecordID = CCommon.ToLong(drCurrentStage("numCommActId"))
                                objWfA.SaveWFActionItemsQueue()
                                'ss//end of code
                            End If
                        Case 3

                            If objOutLook Is Nothing Then objOutLook = New COutlook
                            objOutLook.UserCntID = drCurrentStage("numAssignTo")
                            objOutLook.DomainId = Session("DomainId")
                            Dim dtResource As DataTable
                            dtResource = objOutLook.GetResourceId
                            If dtResource.Rows.Count <= 0 Then bitAddUpdate = False
                            If bitAddUpdate = True Then
                                If boolDealWon = False Then
                                    Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                                    Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                                    strSplitStartTimeDate = CType(startTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                                    strSplitEndTimeDate = CType(endTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC

                                    objOutLook.AllDayEvent = False
                                    objOutLook.ActivityDescription = drCurrentStage("txtCom")
                                    objOutLook.Location = ""
                                    objOutLook.Subject = drCurrentStage("vcComments")
                                    objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                    objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                    objOutLook.EnableReminder = True
                                    objOutLook.ReminderInterval = 900
                                    objOutLook.ShowTimeAs = 3
                                    objOutLook.Importance = 0
                                    objOutLook.RecurrenceKey = -999
                                    objOutLook.ResourceName = dtResource.Rows(0).Item("ResourceId")

                                    If CommActID = 0 Then
                                        CommActID = objOutLook.AddActivity()
                                        drCurrentStage("numCommActId") = CommActID
                                    Else
                                        objOutLook.ActivityID = CommActID
                                        objOutLook.UpdateActivity()
                                    End If
                                Else
                                    If CommActID <> 0 Then
                                        objOutLook.ActivityID = CommActID
                                        objOutLook.RemoveActivity()
                                        drCurrentStage("numCommActId") = 0
                                    End If
                                End If
                            End If
                    End Select
                End If
            Else
                If drCurrentStage("numEvent") = 2 And drCurrentStage("numCommActId") <> 0 Then
                    If objActionItem Is Nothing Then
                        objActionItem = New ActionItem
                    End If
                    objActionItem.CommID = drCurrentStage("numCommActId")
                    objActionItem.DeleteActionItem()
                    drCurrentStage("numCommActId") = 0
                End If
                If drCurrentStage("numEvent") = 3 And drCurrentStage("numCommActId") <> 0 Then
                    If objOutLook Is Nothing Then
                        objOutLook = New COutlook
                    End If
                    objOutLook.ActivityID = drCurrentStage("numCommActId")
                    objOutLook.RemoveActivity()
                    drCurrentStage("numCommActId") = 0
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub rptMilestone_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptMilestone.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                CreateTableMilestone()
                _dtSalesProcess.Rows.RemoveAt(e.Item.ItemIndex + 1)
                BindRepeaterControl()
            ElseIf e.CommandName = "Add" Then
                CreateTableMilestone()
                Dim lblStagePercentage As Label
                lblStagePercentage = e.Item.FindControl("lblStagePercentage")

                Dim myRow As DataRow

                AddNewRow(myRow, lblStagePercentage.Text, "", 0, "", 0, False, True)

                _dtSalesProcess.Rows.InsertAt(myRow, _dtSalesProcess.Rows.Count - 2)


                BindRepeaterControl()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Public Sub AddNewRow(ByRef myRow As DataRow, ByVal Percentage As String, ByVal StageDetail As String, _
        ByVal OppStageID As Long, ByVal Comments As String, ByVal OpFlag As Integer, ByVal StageCompleted As Boolean, ByVal boolNewRow As Boolean)
        myRow = _dtSalesProcess.NewRow
        myRow("numStagePercentage") = Percentage
        myRow("numstagedetailsID") = CType(IIf(IsDBNull(_dtSalesProcess.Compute("MAX(numstagedetailsID)", "")), "0", _dtSalesProcess.Compute("MAX(numstagedetailsID)", "")), Integer) + 1 'dtSalesDetails.Rows.Count + 1 '
        myRow("vcstageDetail") = StageDetail
        myRow("StageID") = OppStageID
        If boolNewRow = True Then
            myRow("numCreatedBy") = Session("UserContactID")
            myRow("bintCreatedDate") = CCommon.ToSqlDate(Date.UtcNow)
            myRow("numModifiedBy") = Session("UserContactID")
            myRow("bintModifiedDate") = CCommon.ToSqlDate(Date.UtcNow)
            myRow("bintDueDate") = CCommon.ToSqlDate(Date.UtcNow)
            myRow("bintStageComDate") = System.DBNull.Value
        End If
        myRow("vcComments") = Comments
        myRow("numAssignTo") = 0
        myRow("bitAlert") = 0
        myRow("bitStageCompleted") = StageCompleted
        myRow("Op_Flag") = OpFlag
        myRow("numModifiedByName") = ""
        myRow("numStage") = 0
        myRow("numTemplateId") = 0
        myRow("numEvent") = 0
        myRow("numReminder") = 0
        myRow("numET") = 0
        myRow("numActivity") = 0
        myRow("numStartDate") = 0
        myRow("numStartTime") = 0
        myRow("numStartTimePeriod") = 0
        myRow("numEndTime") = 0
        myRow("numEndTimePeriod") = 0
        myRow("txtCom") = 0
        myRow("numChgStatus") = 0
        myRow("bitClose") = 0
        myRow("numType") = 0
        myRow("numCommActId") = 0
    End Sub

    Private Sub rptMilestone_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMilestone.ItemDataBound
        Try


            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                trHeader = e.Item.FindControl("trHeader")
                Dim ddlAssignTo, ddlStageStaus As DropDownList
                Dim hplStageTime, hplStageExpense, hplStageDependency, hplStageSubStages, hplEvent As HyperLink
                Dim hdOppStageID, hdAssignTo, hdStage, hdEvent, hdCommActId As HiddenField
                Dim btnStageDelete As Button
                Dim lblStageEvent, lblStageTime, lblStageExpense As Label




                CurrentStagePercentage = CType(e.Item.FindControl("lblStagePercentage"), Label).Text
                If previousStagePercentage = CType(e.Item.FindControl("lblStagePercentage"), Label).Text Then
                    trHeader.Visible = False
                End If


                ddlAssignTo = e.Item.FindControl("ddlAssignTo")
                ddlStageStaus = e.Item.FindControl("ddlStageStatus")
                ddlAssignTo.DataSource = dtAssignto
                ddlAssignTo.DataTextField = "vcUserName"
                ddlAssignTo.DataValueField = "numContactID"
                ddlAssignTo.DataBind()
                ddlAssignTo.Items.Insert(0, "--Select One--")
                ddlAssignTo.Items.FindByText("--Select One--").Value = "0"

                ddlStageStaus.DataSource = dtStageStatus
                ddlStageStaus.DataTextField = "vcData"
                ddlStageStaus.DataValueField = "numListItemID"
                ddlStageStaus.DataBind()

                hplStageTime = e.Item.FindControl("hplStageTime")
                hplStageExpense = e.Item.FindControl("hplStageExpense")
                hplStageDependency = e.Item.FindControl("hplStageDependency")
                hplStageSubStages = e.Item.FindControl("hplStageSubStages")
                hdOppStageID = e.Item.FindControl("hdOppStageID")
                btnStageDelete = e.Item.FindControl("btnStageDelete")
                hdOppStageID = e.Item.FindControl("hdOppStageID")
                hdAssignTo = e.Item.FindControl("hdAssignTo")
                hdStage = e.Item.FindControl("hdStage")
                hplEvent = e.Item.FindControl("hplEvent")
                hdEvent = e.Item.FindControl("hdEvent")
                hdCommActId = e.Item.FindControl("hdCommActId")
                lblStageEvent = e.Item.FindControl("lblStageEvent")
                lblStageTime = e.Item.FindControl("lblStageTime")
                lblStageExpense = e.Item.FindControl("lblStageExpense")

                If Not ddlAssignTo.Items.FindByValue(hdAssignTo.Value) Is Nothing Then
                    ddlAssignTo.Items.FindByValue(hdAssignTo.Value).Selected = True
                End If

                If Not ddlStageStaus.Items.FindByValue(hdStage.Value) Is Nothing Then
                    ddlStageStaus.Items.FindByValue(hdStage.Value).Selected = True
                End If


                'hdStagePercentage = e.Item.FindControl("hdStagePercentage")
                If _ModuleType = ModuleType.Opportunity Then
                    hplStageTime.Visible = False
                    lblStageTime.Visible = False
                    hplStageExpense.Visible = False
                    lblStageExpense.Visible = False
                End If

                hplStageTime.Attributes.Add("onclick", "return OpenTime(" & hdOppStageID.Value & "," & _RecID & "," & _DivisionID & ");")

                hplStageExpense.Attributes.Add("onclick", "return OpenExpense(" & hdOppStageID.Value & "," & _RecID & "," & _DivisionID & ");")

                hplStageDependency.Attributes.Add("onclick", "return OpenDependency(" & hdOppStageID.Value & "," & _RecID & ");")

                hplStageSubStages.Attributes.Add("onclick", "return OpenSubStage(" & hdOppStageID.Value & "," & _RecID & ");")


                If IsNumeric(hdEvent.Value) And IsNumeric(hdCommActId.Value) Then

                    If hdCommActId.Value > 0 Then
                        hplEvent.Attributes.Add("onclick", "return OpenEvent(" & hdEvent.Value & "," & hdCommActId.Value & ");")
                        If hdEvent.Value = 1 Then
                            lblStageEvent.Text = "Sent Email"
                        ElseIf hdEvent.Value = 2 Then
                            lblStageEvent.Text = "Created Action Item"
                        ElseIf hdEvent.Value = 3 Then
                            lblStageEvent.Text = "Created Calendar Item"
                        End If
                    End If
                End If

                previousStagePercentage = CurrentStagePercentage
            ElseIf e.Item.ItemType = ListItemType.Footer Then

                If _ModuleType = ModuleType.Projects Then
                    Dim pnlDealLost As Panel = e.Item.FindControl("pnlDealLost")
                    Dim chkDealWon As CheckBox = e.Item.FindControl("chkDealWon")
                    pnlDealLost.Visible = False
                    chkDealWon.Text = "Project Completed"
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function FormmattedDate(ByVal dtDate) As String
        If IsDBNull(dtDate) Or dtDate.ToString = "" Then
            Return ""
        Else
            Return FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), dtDate), Session("DateFormat"))
        End If
    End Function


    Function ReturnTime(ByVal Time) As String
        If Not IsDBNull(Time) Then
            Return "<font color=red>&nbsp;" & Time & "</font>"
        End If
    End Function

    Function ReturnExpense(ByVal Expense) As String
        If Not IsDBNull(Expense) Then
            Return "<font color=red>&nbsp;" & String.Format("{0:#,##0.00}", CDec(Expense)) & "</font>"
        End If
    End Function

    Function ReturnSubStage(ByVal SubStage) As String
        If SubStage = "1" Then Return "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>" Else Return ""
    End Function

    Function ReturnDependency(ByVal Dependency) As String
        If Dependency = "1" Then Return "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>" Else Return ""
    End Function

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnTimeString(ByVal intVal As Integer) As String
        For Each dr As DataRow In dtTime.Rows
            If dr("vcSQLValue") = intVal Then
                Return dr("vcOptionName")
            End If
        Next
        Return ""
    End Function

    Public Property dtBusinessProcess() As DataTable
        Get
            Try
                Return _dtSalesProcess
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As DataTable)
            Try
                _dtSalesProcess = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property



    Public WriteOnly Property RecordOwner() As String
        Set(ByVal Value As String)
            Try
                _RecordOwner = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property


    Public WriteOnly Property CustomerName() As String
        Set(ByVal Value As String)
            Try
                _Customer = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property


    Public WriteOnly Property RecordOwnerID() As Long
        Set(ByVal Value As Long)
            Try
                _RecordOwnerID = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property


    Public WriteOnly Property TargetModuleType() As ModuleType
        Set(ByVal Value As ModuleType)
            Try
                _ModuleType = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property



    Public WriteOnly Property RecordID() As Long
        Set(ByVal Value As Long)
            Try
                _RecID = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property


    Public WriteOnly Property DivisionID() As Long
        Set(ByVal Value As Long)
            Try
                _DivisionID = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Public WriteOnly Property OppProName() As String
        Set(ByVal Value As String)
            Try
                _OppProName = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Public WriteOnly Property OppAmount() As Decimal
        Set(ByVal Value As Decimal)
            Try
                _OppAmount = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property


    Public WriteOnly Property RecordContactID() As Long
        Set(ByVal Value As Long)
            Try
                _RecordContactID = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property


    Public WriteOnly Property objCommon() As CCommon
        Set(ByVal Value As CCommon)
            Try
                _objCommon = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property


    Public WriteOnly Property CusPrgMgr() As Long
        Set(ByVal Value As Long)
            Try
                _CusPrgMgr = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property


    Public WriteOnly Property IntPrgMgr() As Long
        Set(ByVal Value As Long)
            Try
                _IntPrgMgr = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property









    Sub SendMailToActionee(ByVal AlertDTLID As Long, ByVal boolAlert As Boolean, ByVal Assigneee As String, ByVal StageDetail As String, ByVal DueDate As Date, ByVal AssignToID As Long)
        Try

            Dim objAlerts As New CAlerts
            Dim dtDetails As DataTable
            objAlerts.AlertDTLID = AlertDTLID
            objAlerts.DomainID = Session("DomainID")
            dtDetails = objAlerts.GetIndAlertDTL
            If dtDetails.Rows.Count > 0 Then
                If boolAlert Then
                    Dim dtEmailTemplate As DataTable
                    Dim objDocuments As New DocumentList
                    objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                    objDocuments.DomainID = Session("DomainID")
                    dtEmailTemplate = objDocuments.GetDocByGenDocID
                    If dtEmailTemplate.Rows.Count > 0 Then
                        Dim objSendMail As New Email
                        Dim dtMergeFields As New DataTable
                        Dim drNew As DataRow
                        dtMergeFields.Columns.Add("Assignee")
                        dtMergeFields.Columns.Add("Stage")
                        dtMergeFields.Columns.Add("DueDate")
                        dtMergeFields.Columns.Add("OppID")
                        drNew = dtMergeFields.NewRow
                        drNew("Assignee") = Assigneee
                        drNew("Stage") = StageDetail
                        drNew("DueDate") = FormattedDateFromDate(DueDate, Session("DateFormat"))
                        drNew("OppID") = _OppProName
                        dtMergeFields.Rows.Add(drNew)
                        _objCommon.byteMode = 1
                        _objCommon.ContactID = AssignToID
                        objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, _objCommon.GetManagerEmail, ""), Session("UserEmail"), _objCommon.GetContactsEmail, dtMergeFields)
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub



    Sub SendMailOnDealCompletion()
        Try

            If _ModuleType = ModuleType.Opportunity Then
                Dim objAlerts As New CAlerts
                Dim dtDetails As DataTable
                objAlerts.AlertDTLID = 7 'Alert DTL ID for sending alerts in opportunities
                objAlerts.DomainID = Session("DomainID")
                dtDetails = objAlerts.GetIndAlertDTL
                If dtDetails.Rows.Count > 0 Then
                    Dim dtEmailTemplate As DataTable
                    Dim objDocuments As New DocumentList
                    objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                    objDocuments.DomainID = Session("DomainID")
                    dtEmailTemplate = objDocuments.GetDocByGenDocID
                    If dtEmailTemplate.Rows.Count > 0 Then
                        Dim objSendMail As New Email
                        Dim dtMergeFields As New DataTable
                        Dim drNew As DataRow
                        dtMergeFields.Columns.Add("Employee")
                        dtMergeFields.Columns.Add("DealAmount")
                        dtMergeFields.Columns.Add("Organization")
                        drNew = dtMergeFields.NewRow
                        drNew("Employee") = _RecordOwner
                        drNew("DealAmount") = _OppAmount
                        drNew("Organization") = _Customer

                        Dim dtEmail As DataTable
                        objAlerts.AlertDTLID = 7
                        objAlerts.DomainID = Session("DomainId")
                        dtEmail = objAlerts.GetAlertEmails
                        Dim strCC As String = ""
                        Dim p As Integer
                        For p = 0 To dtEmail.Rows.Count - 1
                            strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ","
                        Next
                        strCC = strCC.TrimEnd(",")
                        dtMergeFields.Rows.Add(drNew)
                        _objCommon.byteMode = 1
                        _objCommon.ContactID = _RecordOwnerID
                        objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), strCC, Session("UserEmail"), _objCommon.GetManagerEmail, dtMergeFields)
                    End If
                End If
                ''Sending mail if the order is big

                Dim dtBigDealDetails As DataTable
                objAlerts.AlertDTLID = 9 'Alert DTL ID for sending alerts in opportunities
                objAlerts.DomainID = Session("DomainID")
                dtBigDealDetails = objAlerts.GetIndAlertDTL
                If dtBigDealDetails.Rows.Count > 0 Then
                    If _OppAmount > dtBigDealDetails.Rows(0).Item("monAmount") Then
                        Dim dtEmailTemplate As DataTable
                        Dim objDocuments As New DocumentList
                        objDocuments.GenDocID = dtBigDealDetails.Rows(0).Item("numEmailTemplate")
                        objDocuments.DomainID = Session("DomainID")
                        dtEmailTemplate = objDocuments.GetDocByGenDocID
                        If dtEmailTemplate.Rows.Count > 0 Then
                            Dim objSendMail As New Email
                            Dim dtMergeFields As New DataTable
                            Dim drNew As DataRow
                            dtMergeFields.Columns.Add("Employee")
                            dtMergeFields.Columns.Add("DealAmount")
                            dtMergeFields.Columns.Add("Organization")
                            drNew = dtMergeFields.NewRow
                            drNew("Employee") = _RecordOwner
                            drNew("DealAmount") = _OppAmount
                            drNew("Organization") = _Customer

                            Dim dtEmail As DataTable
                            objAlerts.AlertDTLID = 9
                            objAlerts.DomainID = Session("DomainId")
                            dtEmail = objAlerts.GetAlertEmails
                            Dim strCC As String = ""
                            Dim p As Integer
                            For p = 0 To dtEmail.Rows.Count - 1
                                strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ";"
                            Next
                            strCC = strCC.TrimEnd(";")
                            dtMergeFields.Rows.Add(drNew)
                            _objCommon.byteMode = 0
                            _objCommon.ContactID = _RecordOwnerID
                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", Session("UserEmail"), strCC, dtMergeFields)
                        End If
                    End If

                End If
            ElseIf _ModuleType = ModuleType.Projects Then

                Dim dtDetails As DataTable
                Dim objAlerts As New CAlerts
                If _objCommon Is Nothing Then
                    _objCommon = New CCommon
                End If
                objAlerts.AlertID = 9
                dtDetails = objAlerts.GetAlertDetails
                Dim strTo As String = ""
                If dtDetails.Rows(2).Item("tintAlertOn") = 1 Then
                    _objCommon.ContactID = _IntPrgMgr
                    If _IntPrgMgr > 0 Then
                        strTo = _objCommon.GetContactsEmail
                    End If
                End If
                Dim dtEmailTemplate As DataTable
                Dim objDocuments As New DocumentList
                objDocuments.GenDocID = dtDetails.Rows(2).Item("numEmailTemplate")
                objDocuments.DomainID = Session("DomainID")
                dtEmailTemplate = objDocuments.GetDocByGenDocID
                If dtEmailTemplate.Rows.Count > 0 Then
                    Dim objSendMail As New Email
                    Dim dtMergeFields As New DataTable
                    Dim drNew As DataRow
                    dtMergeFields.Columns.Add("OppID")
                    dtMergeFields.Columns.Add("Organization")
                    drNew = dtMergeFields.NewRow
                    drNew("OppID") = _OppProName
                    drNew("Organization") = _Customer

                    If strTo <> "" Then
                        If dtDetails.Rows(3).Item("tintAlertOn") = 1 Then
                            _objCommon.ContactID = _CusPrgMgr
                            If _CusPrgMgr > 0 Then
                                strTo = strTo & "," & _objCommon.GetContactsEmail
                            Else
                                strTo = _objCommon.GetContactsEmail
                            End If
                        End If
                    Else
                        _objCommon.ContactID = _CusPrgMgr
                        strTo = _objCommon.GetContactsEmail
                    End If
                    Dim dtEmail As DataTable
                    objAlerts.AlertDTLID = 14
                    objAlerts.DomainID = Session("DomainId")
                    dtEmail = objAlerts.GetAlertEmails
                    _objCommon.byteMode = 0
                    _objCommon.ContactID = _IntPrgMgr
                    Dim strCC As String = ""
                    Dim p As Integer
                    For p = 0 To dtEmail.Rows.Count - 1
                        strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ","
                    Next
                    strCC = strCC.TrimEnd(",")
                    If dtDetails.Rows(2).Item("tintCCManager") = 1 Then
                        Dim strCCAssignee As String = _objCommon.GetManagerEmail
                        If strCCAssignee <> "" Then strCC = strCC & "," & strCCAssignee
                    End If
                    dtMergeFields.Rows.Add(drNew)
                    objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), strCC, Session("UserEmail"), strTo, dtMergeFields)
                End If

            End If

        Catch ex As Exception

        End Try
    End Sub



End Class