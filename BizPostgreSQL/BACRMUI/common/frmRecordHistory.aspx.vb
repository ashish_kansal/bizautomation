﻿Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI

Public Class frmRecordHistory
    Inherits BACRM.BusinessLogic.Common.BACRMPage

#Region "Member Variables"

    Private numMouduleID As Short
    Private numRecordID As Long
    Dim filterFromDate As Date?
    Dim filterToDate As Date?
    Dim filterEvent As String
    Dim filterUserName As String
    Dim filterFieldName As String
    Dim filterUser As String

#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            numMouduleID = CCommon.ToShort(GetQueryStringVal("numMouduleID"))
            numRecordID = CCommon.ToLong(GetQueryStringVal("numRecordID"))

            If numMouduleID > 0 AndAlso numRecordID > 0 Then
                BindData()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindData()
        Try
            If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1

            Dim objRecordHistory As New RecordHistory
            objRecordHistory.DomainID = CCommon.ToLong(Session("DomainID"))
            objRecordHistory.ModuleID = numMouduleID
            objRecordHistory.RecordID = numRecordID
            objRecordHistory.PageNumber = txtCurrrentPage.Text.Trim()
            objRecordHistory.PageSize = Session("PagingRows")
            objRecordHistory.TotalRecords = 0
            objRecordHistory.filterEvent = filterEvent
            objRecordHistory.filterUserName = filterUserName
            objRecordHistory.filterFieldName = filterFieldName
            objRecordHistory.filterFromDate = filterFromDate
            objRecordHistory.filterToDate = filterToDate
            Dim dt As DataTable = objRecordHistory.GetHistory()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                gvHistory.DataSource = dt
                gvHistory.DataBind()

                lblNoData.Visible = False
            Else
                lblNoData.Visible = True
            End If

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objRecordHistory.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Protected Sub lbClearGridCondition_Click(sender As Object, e As EventArgs)
        Try
            filterEvent = Nothing
            filterUserName = Nothing
            filterFieldName = ""
            filterUser = ""

            BindData()
            If Not gvHistory.HeaderRow Is Nothing AndAlso Not gvHistory.HeaderRow.FindControl("radDateFrom") Is Nothing Then
                DirectCast(gvHistory.HeaderRow.FindControl("radDateFrom"), RadDatePicker).SelectedDate = filterFromDate
            End If
            If Not gvHistory.HeaderRow Is Nothing AndAlso Not gvHistory.HeaderRow.FindControl("radDateTo") Is Nothing Then
                DirectCast(gvHistory.HeaderRow.FindControl("radDateTo"), RadDatePicker).SelectedDate = filterToDate
            End If
            If Not gvHistory.HeaderRow Is Nothing AndAlso Not gvHistory.HeaderRow.FindControl("txtOrderName") Is Nothing Then
                DirectCast(gvHistory.HeaderRow.FindControl("vcEvent"), TextBox).Text = filterEvent
            End If
            If Not gvHistory.HeaderRow Is Nothing AndAlso Not gvHistory.HeaderRow.FindControl("vcUserName") Is Nothing Then
                DirectCast(gvHistory.HeaderRow.FindControl("vcUserName"), TextBox).Text = filterUserName
            End If
            If Not gvHistory.HeaderRow Is Nothing AndAlso Not gvHistory.HeaderRow.FindControl("vcFieldName") Is Nothing Then
                DirectCast(gvHistory.HeaderRow.FindControl("vcFieldName"), TextBox).Text = filterFieldName
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Protected Sub btnFilter_Click(sender As Object, e As EventArgs)
        Try
            If Not gvHistory.HeaderRow Is Nothing AndAlso Not gvHistory.HeaderRow.FindControl("radDateFrom") Is Nothing Then
                filterFromDate = DirectCast(gvHistory.HeaderRow.FindControl("radDateFrom"), RadDatePicker).SelectedDate
            End If
            If Not gvHistory.HeaderRow Is Nothing AndAlso Not gvHistory.HeaderRow.FindControl("radDateTo") Is Nothing Then
                filterToDate = DirectCast(gvHistory.HeaderRow.FindControl("radDateTo"), RadDatePicker).SelectedDate
            End If
            If Not gvHistory.HeaderRow Is Nothing AndAlso Not gvHistory.HeaderRow.FindControl("vcEvent") Is Nothing Then
                filterEvent = DirectCast(gvHistory.HeaderRow.FindControl("vcEvent"), TextBox).Text
            End If
            If Not gvHistory.HeaderRow Is Nothing AndAlso Not gvHistory.HeaderRow.FindControl("vcUserName") Is Nothing Then
                filterUserName = DirectCast(gvHistory.HeaderRow.FindControl("vcUserName"), TextBox).Text
            End If
            If Not gvHistory.HeaderRow Is Nothing AndAlso Not gvHistory.HeaderRow.FindControl("vcFieldName") Is Nothing Then
                filterFieldName = DirectCast(gvHistory.HeaderRow.FindControl("vcFieldName"), TextBox).Text
            End If
            'If Not gvHistory.HeaderRow Is Nothing AndAlso Not gvHistory.HeaderRow.FindControl("rcbUsers") Is Nothing Then
            '    Dim selectedUsers As String = ""
            '    For Each objItem As RadComboBoxItem In DirectCast(gvHistory.HeaderRow.FindControl("rcbUsers"), RadComboBox).CheckedItems
            '        selectedUsers = (selectedUsers & If(selectedUsers <> "", ",", "") & objItem.Value)
            '    Next
            '    filterUser = selectedUsers
            'End If
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

End Class