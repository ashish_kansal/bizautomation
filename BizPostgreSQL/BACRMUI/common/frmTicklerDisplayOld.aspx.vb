﻿' Modified By Anoop Jayaraj
Imports System.Data.OleDb
Imports System.Text
Imports System.Net

Imports System.Security.Cryptography
Imports System.Collections.Specialized
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Contacts
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.Collections.Generic
Imports Newtonsoft.Json
Imports System.Web.Services
Imports HtmlAgilityPack

Namespace BACRM.UserInterface.Common
    Partial Public Class frmTicklerDisplayOld
        Inherits BACRMPage
        Dim m_aryRightsForAva() As Integer
        Dim m_aryRightsForPer() As Integer
        Dim m_aryRightsForTeams() As Integer
        Dim m_aryRightsForCase() As Integer
        Dim m_aryRightsForActItem() As Integer
        Dim m_aryRightsForOpportunity(), m_aryRightsForViewGridConfiguration() As Integer
        Dim objTickler As Tickler
        Dim strDtTo As Date     'Date To.
        Dim strDtFrom As Date   'Date From.
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim RegularSearch As String
        Dim CustomSearch As String

        <WebMethod()>
        Public Shared Function PersistTab(ByVal strKey As String, ByVal strValue As String) As String

            Dim PersistTable As New Hashtable
            PersistTable.Clear()
            PersistTable.Add(strKey, strValue)
            PersistTable.Add("Page", "1")

            PersistTable.Save(strPageName:="frmticklerdisplayOld.aspx")

        End Function

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                txtCurrrentPage_TextChanged(Nothing, Nothing)
                sb_DisplayTaskMeeting()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub txtCurrrentPage_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
            Try
                sb_DisplayTaskMeeting()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Enum TicklerType As Integer
            YESTERDAY_TODAY_TOMORROW = 1 'Yesterday, Today, and Tomorrow
            YESTERDAY_THROUGH_NEXT_7DAYS = 2 'Yesterday through next 7 days
            LAST_7DAYS_THROUGH_NEXT_7DAYS = 3 'Last 7 days through next 7 days
            LAST_MONTH_THROUGH_NEXT_WEEK = 4 'Last month through next week
            LAST_WEEK_THROUGH_NEXT_MONTH = 5 'Last week through next month
            LAST_MONTH_THROUGH_NEXT_MONTH = 6 'Last month through next month
            LAST_YEAR_THROUGH_THIS_YEAR = 7 'Last year through this year
            LAST_YEAR_THROUGH_NEXT_YEAR = 8 'Last year through next year
        End Enum

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                btnTeam.Attributes.Add("onclick", "return OpenSelTeam()")
                hplEmpAvaliability.Attributes.Add("onclick", "return openEmpAvailability()")
                radBProcesses.Visible = Session("IsEnableResourceScheduling")
                btnDeleteCases.Attributes.Add("onclick", "return DeleteActionItemsConfirmation();")
                btnCloseAction.Attributes.Add("onclick", "return CloseActionItemsConfirmation();")

                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If Not IsPostBack Then

                    radOpen.Checked = True

                    Session("Help") = "Tickler"

                    If GetQueryStringVal("Date") <> "" Then
                        If GetQueryStringVal("Date") = 0 Then
                            calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                            calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, 1, Date.UtcNow))
                        Else
                            calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, -1, Date.UtcNow))
                            calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                        End If
                    Else
                        calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                        calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, 1, Date.UtcNow))
                    End If
                    radOppTab.SelectedIndex = SI
                    radMultiPage_OppTab.SelectedIndex = SI
                    objCommon.GetAuthorizedSubTabs(radOppTab, Session("DomainID"), 17, Session("UserGroupID"))
                    ''Checking The View Rights For Tickler
                    GetUserRightsForPage(1, 1)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS ")
                    End If
                    m_aryRightsForAva = GetUserRightsForPage_Other(1, 5)
                    If m_aryRightsForAva(RIGHTSTYPE.VIEW) = 0 Then hplEmpAvaliability.Visible = False

                    m_aryRightsForTeams = GetUserRightsForPage_Other(1, 6)
                    If m_aryRightsForTeams(RIGHTSTYPE.VIEW) = 0 Then btnTeam.Visible = False
                    If objTickler Is Nothing Then objTickler = New Tickler
                    ''''call function for sub-tab management  - added on 29jul09 by Mohan
                    'objCommon.ManageSubTabs(radOppTab, Session("DomainID"), 1)
                    '''''
                    Dim dtEmployees As DataTable
                    objTickler.UserCntID = Session("UserContactID")
                    objTickler.DomainID = Session("DomainID")
                    objTickler.TeamType = 1
                    dtEmployees = objTickler.GetEmployees

                    BindBusinessProcess()
                    If GetQueryStringVal("SelectedIndex") <> "" Then
                        radOppTab.SelectedIndex = CCommon.ToInteger(GetQueryStringVal("SelectedIndex"))
                        radMultiPage_OppTab.SelectedIndex = CCommon.ToInteger(GetQueryStringVal("SelectedIndex"))

                    End If

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumnActionItems.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName & "ActionItems"))
                        txtSortOrderActionItems.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder & "ActionItems"))

                        txtSortColumnOppPro.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName & "OppPro"))
                        txtSortOrderOppPro.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder & "OppPro"))

                        txtSortColumnCases.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName & "Cases"))
                        txtSortOrderCases.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder & "Cases"))
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                        If radTickler.Items.FindItemByValue(CCommon.ToString(PersistTable(radTickler.ID))) IsNot Nothing Then
                            radTickler.Items.FindItemByValue(CCommon.ToString(PersistTable(radTickler.ID))).Selected = True
                        End If

                        calFrom.SelectedDate = CCommon.ToString(PersistTable(calFrom.ID))
                        calTo.SelectedDate = CCommon.ToString(PersistTable(calTo.ID))
                    End If

                    'Check if user has rights to edit grid configuration
                    m_aryRightsForViewGridConfiguration = GetUserRightsForPage_Other(1, 10)
                    If m_aryRightsForViewGridConfiguration(RIGHTSTYPE.VIEW) = 0 Then
                        divGridConfiguration.Visible = False
                    End If

                    'KEEP IT AT LAST ABOVE LoadTicklerDetails
                    If CCommon.ToShort(GetQueryStringVal("IsFormDashboard")) = 1 AndAlso radOppTab.Tabs.FindTabByValue("TEApproval").Visible Then
                        radOppTab.Tabs.FindTabByValue("TEApproval").Selected = True
                        radMultiPage_OppTab.SelectedIndex = radOppTab.Tabs.FindTabByValue("TEApproval").Index

                        calFrom.SelectedDate = System.Data.SqlTypes.SqlDateTime.MinValue.ToString()
                        calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, 1, Date.UtcNow))

                        If CCommon.ToString(GetQueryStringVal("ReminderType")) = "1" Then
                            ddlFilter.SelectedValue = "1"
                        ElseIf CCommon.ToString(GetQueryStringVal("ReminderType")) = "2" Then
                            ddlFilter.SelectedValue = "2"
                        ElseIf CCommon.ToString(GetQueryStringVal("ReminderType")) = "3" Then
                            ddlFilter.SelectedValue = "3"
                        End If
                    End If

                    LoadTicklerDetails()
                End If

                'Added By Richa --Start :25Feb2019
                PersistTable.Load(strPageName:="frmticklerdisplayOld.aspx")

                If PersistTable.Count > 0 Then
                    If radOppTab.Tabs.FindTabByValue(PersistTable("index")) IsNot Nothing Then
                        radOppTab.Tabs.FindTabByValue(PersistTable("index")).Selected = True
                    Else
                        radOppTab.SelectedIndex = 0
                    End If
                End If

                If radOppTab.SelectedTab IsNot Nothing Then
                    If radOppTab.SelectedTab.Visible = False Then
                        radOppTab.SelectedIndex = 0
                        radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    Else
                        radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    End If
                Else
                    radOppTab.SelectedIndex = 0
                    radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                End If



                'By Richa --End :25Feb2019

                If Page.IsPostBack Then
                    If radOppTab.SelectedIndex = 0 Then
                        LoadTicklerDetails()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub radOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOpen.CheckedChanged
            Try
                sb_DisplayTaskMeeting()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub radClosed_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radClosed.CheckedChanged
            Try
                sb_DisplayTaskMeeting()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub radAll_CheckedChanged(sender As Object, e As EventArgs) Handles radAll.CheckedChanged
            Try
                sb_DisplayTaskMeeting()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub grdApproveTE_RowCommand(sender As Object, e As GridViewCommandEventArgs)
            Try
                Dim ApprovalTrans As New ApprovalConfig
                ApprovalTrans.strOutPut = "INPUT"
                Dim row As GridViewRow
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                row = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim hdnApprovalType As HiddenField
                hdnApprovalType = DirectCast(row.FindControl("hdnApprovalType"), HiddenField)


                If e.CommandName = "Approve" Then
                    If hdnApprovalType.Value = "3" Then
                        Dim hdnApprovalStaus As HiddenField
                        hdnApprovalStaus = DirectCast(row.FindControl("hdnApprovalStaus"), HiddenField)

                        If Session("intOpportunityApprovalProcess") = "1" Then
                            ApprovalTrans.ApprovalStatus = 0
                            ApprovalTrans.numConfigId = Convert.ToInt32(index)
                            ApprovalTrans.chrAction = "UTP"
                            ApprovalTrans.UserId = Session("UserContactID")
                            ApprovalTrans.DomainID = Session("DomainID")
                            ApprovalTrans.numModuleId = 2
                            ApprovalTrans.UpdateApprovalTransaction()
                            'Validate Debit and CashCreditCard Account
                            Response.Redirect("../Common/frmticklerdisplayOld.aspx?SelectedIndex=3", False)
                        ElseIf Session("intOpportunityApprovalProcess") = "2" Then
                            ApprovalTrans.chrAction = "CHP"
                            ApprovalTrans.numConfigId = Convert.ToInt32(index)
                            If Convert.ToInt32(hdnApprovalStaus.Value) < 5 Then
                                ApprovalTrans.ApprovalStatus = Convert.ToInt32(hdnApprovalStaus.Value) + 1
                            Else
                                ApprovalTrans.ApprovalStatus = 5
                            End If
                            ApprovalTrans.UpdateApprovalTransaction()
                            If ApprovalTrans.strOutPut = "VALID" Then
                                ApprovalTrans.numConfigId = Convert.ToInt32(index)
                                ApprovalTrans.chrAction = "UTP"
                                ApprovalTrans.UserId = Session("UserContactID")
                                ApprovalTrans.DomainID = Session("DomainID")
                                ApprovalTrans.numModuleId = 2
                                ApprovalTrans.UpdateApprovalTransaction()
                            Else
                                ApprovalTrans.numConfigId = Convert.ToInt32(index)
                                ApprovalTrans.ApprovalStatus = 0
                                ApprovalTrans.chrAction = "UTP"
                                ApprovalTrans.UserId = Session("UserContactID")
                                ApprovalTrans.DomainID = Session("DomainID")
                                ApprovalTrans.numModuleId = 2
                                ApprovalTrans.UpdateApprovalTransaction()
                            End If
                            Response.Redirect("../Common/frmticklerdisplayOld.aspx?SelectedIndex=3", False)
                        Else
                            ApprovalTrans.ApprovalStatus = 0
                        End If
                    End If
                    If hdnApprovalType.Value = "2" Then
                        Dim objOpportunity As New MOpportunity
                        objOpportunity.DomainID = Session("DomainID")
                        objOpportunity.OpportunityId = index
                        objOpportunity.ApproveOpportunityItemsPrice()
                        Response.Redirect("../Common/frmticklerdisplayOld.aspx?SelectedIndex=3", False)
                    End If
                    If hdnApprovalType.Value = "1" Then
                        Dim dt As DataTable
                        ApprovalTrans.numDomainId = Session("DomainID")
                        ApprovalTrans.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        ApprovalTrans.numRecordId = index
                        dt = ApprovalTrans.GetTimeExpenseByRecord()
                        Dim hdnApprovalComplete As New HiddenField()
                        Dim hdnItemDesc As New HiddenField()
                        Dim hdnUserCntID As New HiddenField()
                        Dim hdnEmployee As New HiddenField()
                        Dim hdnAmount As New HiddenField()
                        Dim hdntype As New HiddenField()
                        Dim hdnCategory As New HiddenField()
                        Dim hdnDivisionID As New HiddenField()
                        Dim hdnStageID As New HiddenField()
                        Dim hdnProid As New HiddenField()
                        Dim hdnCaseid As New HiddenField()
                        Dim hdnOppId As New HiddenField()
                        Dim hdnServiceItemId As New HiddenField()
                        Dim hdnExpId As New HiddenField()
                        Dim hdnFromDate As New HiddenField()
                        Dim hdnToDate As New HiddenField()
                        Dim hdnClassId As New HiddenField()
                        hdnApprovalComplete.Value = Convert.ToString(dt.Rows(0)("numApprovalComplete"))
                        hdnItemDesc.Value = Convert.ToString(dt.Rows(0)("vcItemDesc"))
                        hdnUserCntID.Value = Convert.ToString(dt.Rows(0)("numUserCntID"))
                        hdnEmployee.Value = Convert.ToString(dt.Rows(0)("vcEmployee"))
                        hdnAmount.Value = Convert.ToString(dt.Rows(0)("monAmount"))
                        hdntype.Value = Convert.ToString(dt.Rows(0)("numtype"))
                        hdnCategory.Value = Convert.ToString(dt.Rows(0)("numCategory"))
                        hdnDivisionID.Value = Convert.ToString(dt.Rows(0)("numDivisionID"))
                        hdnStageID.Value = Convert.ToString(dt.Rows(0)("numStageID"))
                        hdnProid.Value = Convert.ToString(dt.Rows(0)("numProid"))
                        hdnCaseid.Value = Convert.ToString(dt.Rows(0)("numCaseid"))
                        hdnOppId.Value = Convert.ToString(dt.Rows(0)("numOppId"))
                        hdnServiceItemId.Value = Convert.ToString(dt.Rows(0)("numServiceItemID"))
                        hdnExpId.Value = Convert.ToString(dt.Rows(0)("numExpId"))
                        hdnFromDate.Value = Convert.ToString(dt.Rows(0)("dtFromDate"))
                        hdnToDate.Value = Convert.ToString(dt.Rows(0)("dtToDate"))
                        hdnClassId.Value = Convert.ToString(dt.Rows(0)("numEClassId"))
                        hdnServiceItemId.Value = Convert.ToString(dt.Rows(0)("numServiceItemID"))

                        ApprovalTrans.numConfigId = Convert.ToInt32(index)
                        ApprovalTrans.strOutPut = "INPUT"
                        'Find Current row values 
                        Dim lngEmpAccountID As Long = 0
                        Dim lngEmpAccountIDB As Long = 0
                        Dim lngDebtAccountID As Long = 0
                        Dim SOId As Long
                        Dim DivID As Long
                        Dim ExpID As Long
                        Dim ProID As Long
                        Dim lngProjAccountID As Long
                        Dim lngEmpPayrollExpenseAcntID As Long
                        Dim objProject As New Project

                        SOId = hdnOppId.Value
                        ProID = hdnProid.Value
                        ExpID = hdnExpId.Value
                        SOId = hdnOppId.Value
                        DivID = hdnDivisionID.Value

                        'Save Expense Module
                        If hdnCategory.Value = 2 Then

                            If hdntype.Value = 1 Then
                                If SOId > 0 Then
                                    'Validate default Payroll Expense Account
                                    lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID")) 'Employee Payeroll Expense
                                    lngDebtAccountID = lngEmpPayrollExpenseAcntID
                                    lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                                    If lngDebtAccountID = 0 Then
                                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Receiveable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                        Exit Sub
                                    End If
                                    If lngEmpAccountID = 0 Then
                                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Reimbursable Expense Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                        Exit Sub
                                    End If
                                End If
                            End If

                            If hdntype.Value = 2 Then
                                lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("RE", Session("DomainID")) 'Employee Payeroll Expense
                                lngDebtAccountID = lngEmpPayrollExpenseAcntID
                                lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                                If lngDebtAccountID = 0 Then
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Reimbursable Expense Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                    Exit Sub
                                End If
                                If lngEmpAccountID = 0 Then
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Payable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                    Exit Sub
                                End If
                            End If
                            If hdntype.Value = 6 Then
                                lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("RE", Session("DomainID")) 'Employee Payeroll Expense
                                lngDebtAccountID = lngEmpPayrollExpenseAcntID
                                If lngDebtAccountID = 0 Then
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                    Exit Sub
                                End If
                                lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                                If lngEmpAccountID = 0 Then
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Reimbursable Expense Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                    Exit Sub
                                End If
                            End If

                        End If

                        'Save Authorities for Time Module
                        If hdnCategory.Value = 1 Then
                            lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                            ' Validate Project Chart of Account 
                            If SOId = 0 AndAlso ProID > 0 Then
                                Dim dtRCordProject As DataTable
                                objProject.DomainID = Session("DomainID")
                                objProject.ProjectID = ProID
                                objProject.DivisionID = DivID
                                dtRCordProject = objProject.GetOpenProject()
                                If dtRCordProject.Rows.Count > 0 Then
                                    lngProjAccountID = CCommon.ToLong(dtRCordProject.Rows(0).Item("numAccountId"))
                                    lngDebtAccountID = lngProjAccountID
                                End If

                                If lngProjAccountID = 0 Then
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "AccountValidation", "alert('Please Set Time & Expense Account for Current Project from ""Administration->Domain Details->Accounting->Accounts for Project""')", False)
                                    Exit Sub
                                End If
                            End If

                            If SOId > 0 Then
                                'Validate default Payroll Expense Account
                                lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID")) 'Employee Payeroll Expense
                                lngDebtAccountID = lngEmpPayrollExpenseAcntID
                                If lngEmpPayrollExpenseAcntID = 0 Then
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Employee Payroll Expense Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                    Exit Sub
                                End If
                            End If
                            If ExpID > 0 Then
                                lngDebtAccountID = ExpID
                            End If
                            If lngEmpAccountID = 0 Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Employee Payroll Expense Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                Exit Sub
                            End If
                            If hdntype.Value = 2 Then
                                lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID")) 'Employee Payeroll Expense
                                lngDebtAccountID = lngEmpPayrollExpenseAcntID
                                If lngEmpPayrollExpenseAcntID = 0 Then
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Employee Payroll Expense Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                    Exit Sub
                                End If
                            End If
                        End If
                        If Session("bitApprovalforTImeExpense") = "1" Then
                            If Session("intTimeExpApprovalProcess") = "1" Then
                                ApprovalTrans.ApprovalStatus = 0
                                ApprovalTrans.numConfigId = Convert.ToInt32(index)
                                ApprovalTrans.chrAction = "UT"
                                ApprovalTrans.UserId = Session("UserContactID")
                                ApprovalTrans.DomainID = Session("DomainID")
                                ApprovalTrans.numModuleId = 1
                                ApprovalTrans.UpdateApprovalTransaction()
                                'Validate Debit and CashCreditCard Account

                                If SOId > 0 Then
                                    AddItemtoExistingInvoice(hdnServiceItemId.Value, hdnItemDesc.Value, hdnAmount.Value, hdnFromDate.Value, hdnToDate.Value, SOId, index, ProID, hdnClassId.Value)
                                End If
                                SaveExpenseJournal(index, hdnAmount.Value, hdnFromDate.Value, hdnToDate.Value, hdnUserCntID.Value, hdnCategory.Value, DivID, ProID, hdnClassId.Value, lngEmpAccountID, lngDebtAccountID, lngEmpAccountIDB, hdntype.Value)
                                Response.Redirect("../Common/frmticklerdisplayOld.aspx?SelectedIndex=3", False)
                            ElseIf Session("intTimeExpApprovalProcess") = "2" Then
                                ApprovalTrans.chrAction = "CH"
                                If Convert.ToInt32(hdnApprovalComplete.Value) < 5 Then
                                    ApprovalTrans.ApprovalStatus = Convert.ToInt32(hdnApprovalComplete.Value) + 1
                                Else
                                    ApprovalTrans.ApprovalStatus = 5
                                End If
                                ApprovalTrans.UpdateApprovalTransaction()
                                If ApprovalTrans.strOutPut = "VALID" Then
                                    ApprovalTrans.numConfigId = Convert.ToInt32(index)
                                    ApprovalTrans.chrAction = "UT"
                                    ApprovalTrans.UserId = Session("UserContactID")
                                    ApprovalTrans.DomainID = Session("DomainID")
                                    ApprovalTrans.numModuleId = 1
                                    ApprovalTrans.UpdateApprovalTransaction()
                                Else
                                    ApprovalTrans.numConfigId = Convert.ToInt32(index)
                                    ApprovalTrans.ApprovalStatus = 0
                                    ApprovalTrans.chrAction = "UT"
                                    ApprovalTrans.UserId = Session("UserContactID")
                                    ApprovalTrans.DomainID = Session("DomainID")
                                    ApprovalTrans.numModuleId = 1
                                    ApprovalTrans.UpdateApprovalTransaction()
                                    If SOId > 0 Then
                                        AddItemtoExistingInvoice(hdnServiceItemId.Value, hdnItemDesc.Value, hdnAmount.Value, hdnFromDate.Value, hdnToDate.Value, SOId, index, ProID, hdnClassId.Value)
                                    End If
                                    SaveExpenseJournal(index, hdnAmount.Value, hdnFromDate.Value, hdnToDate.Value, hdnUserCntID.Value, hdnCategory.Value, DivID, ProID, hdnClassId.Value, lngEmpAccountID, lngDebtAccountID, lngEmpAccountIDB, hdntype.Value)
                                End If
                                Response.Redirect("../Common/frmticklerdisplayOld.aspx?SelectedIndex=3", False)
                            End If
                        Else
                            ApprovalTrans.ApprovalStatus = 0
                        End If


                    End If
                End If
                If e.CommandName = "Decline" Then
                    If hdnApprovalType.Value = "3" Then
                        ApprovalTrans.ApprovalStatus = -1
                        ApprovalTrans.numConfigId = Convert.ToInt32(index)
                        ApprovalTrans.chrAction = "UTP"
                        ApprovalTrans.UserId = Session("UserContactID")
                        ApprovalTrans.DomainID = Session("DomainID")
                        ApprovalTrans.numModuleId = 2
                        ApprovalTrans.UpdateApprovalTransaction()
                    Else
                        ApprovalTrans.ApprovalStatus = -1
                        ApprovalTrans.numConfigId = Convert.ToInt32(index)
                        ApprovalTrans.chrAction = "UT"
                        ApprovalTrans.UserId = Session("UserContactID")
                        ApprovalTrans.DomainID = Session("DomainID")
                        ApprovalTrans.numModuleId = 1
                        ApprovalTrans.UpdateApprovalTransaction()
                    End If

                    Response.Redirect("../Common/frmticklerdisplayOld.aspx?SelectedIndex=3", False)
                End If
                If e.CommandName = "TEEdit" Then
                    Response.Redirect("../TimeAndExpense/frmEditTimeExp.aspx?CatID=" & index & "")
                End If
            Catch ex As Exception

            End Try
        End Sub
        Private Sub SaveExpenseJournal(ByVal CategoryHDRID As Integer, ByVal RatePerHour As Double, ByVal dtFromDate As Date, ByVal dtToDate As Date, ByVal Employee As Long, ByVal category As Long, ByVal DivID As Long, ByVal ProID As Long, ByVal ClassID As Long, ByVal lngEmpAccountID As Long, ByVal lngDebtAccountID As Long, ByVal lngEmpAccountIDB As Long, ByVal Type As Long)
            If CategoryHDRID > 0 Then
                Dim totalAmount As Decimal
                'Create Journal Entry
                If category = 1 Then
                    totalAmount = RatePerHour * DateDiff(DateInterval.Minute, dtFromDate, dtToDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday) / 60
                Else
                    totalAmount = RatePerHour
                End If

                If totalAmount > 0 Then

                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        Dim lngJournalId As Long
                        Dim objJEHeader As New JournalEntryHeader

                        With objJEHeader
                            .JournalId = lngJournalId
                            .RecurringId = 0
                            .EntryDate = Date.UtcNow
                            .Description = "TimeAndExpense"
                            .Amount = CCommon.ToDecimal(totalAmount)
                            .CheckId = 0
                            .CashCreditCardId = 0
                            .ChartAcntId = 0
                            .OppId = 0
                            .OppBizDocsId = 0
                            .DepositId = 0
                            .BizDocsPaymentDetId = 0
                            .IsOpeningBalance = 0
                            .LastRecurringDate = Date.Now
                            .NoTransactions = 0
                            .CategoryHDRID = CategoryHDRID
                            .ReturnID = 0
                            .CheckHeaderID = 0
                            .BillID = 0
                            .BillPaymentID = 0
                            .UserCntID = CCommon.ToLong(Employee)
                            .DomainID = Session("DomainID")
                        End With

                        lngJournalId = objJEHeader.Save()

                        'SaveDataToGeneralJournalDetails(lngJournalId)

                        Dim objJEList As New JournalEntryCollection

                        Dim objJE As New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = totalAmount
                        objJE.CreditAmt = 0
                        objJE.ChartAcntId = lngDebtAccountID 'If(SOId > 0, lngEmpPayrollExpenseAcntID, lngProjAccountID) If(txtMode.Text = "BD", lngEmpPayrollExpenseAcntID, lngProjAccountID)
                        objJE.Description = "TimeAndExpense"
                        objJE.CustomerId = CCommon.ToLong(DivID)
                        objJE.MainDeposit = 0
                        objJE.MainCheck = 0
                        objJE.MainCashCredit = 0
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = ""
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 0
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = CCommon.ToLong(ProID)
                        objJE.ClassID = CCommon.ToLong(ClassID)
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = 0
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)

                        objJE = New JournalEntryNew()
                        objJE.TransactionId = 0
                        objJE.DebitAmt = 0
                        objJE.CreditAmt = totalAmount
                        objJE.ChartAcntId = lngEmpAccountID
                        objJE.Description = "TimeAndExpense"
                        objJE.CustomerId = CCommon.ToLong(DivID)
                        objJE.MainDeposit = 0
                        objJE.MainCheck = 0
                        objJE.MainCashCredit = 0
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = ""
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 0
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = CCommon.ToLong(ProID)
                        objJE.ClassID = CCommon.ToLong(ClassID)
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = 0
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0
                        objJEList.Add(objJE)


                        objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
                        objTransactionScope.Complete()
                    End Using
                End If
            End If
        End Sub

        Private Sub AddItemtoExistingInvoice(ByVal ServiceItemID As Long, ByVal Notes As String, ByVal RatePerHour As Double, ByVal dtFromDate As Date, ByVal dtToDate As Date, ByVal SOId As Long, ByVal CategoryHDRID As Long, ByVal ProID As Long, ByVal ClassID As Long)
            Dim objinvoice As New OppBizDocs
            If ServiceItemID > 0 Then
                objinvoice.ItemCode = ServiceItemID
                objinvoice.ItemDesc = Notes
                objinvoice.Price = RatePerHour
                objinvoice.UnitHour = DateDiff(DateInterval.Minute, dtFromDate, dtToDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday) / 60
                objinvoice.OppId = SOId
                objinvoice.OppBizDocId = 0
                objinvoice.CategoryHDRID = CategoryHDRID
                objinvoice.ProID = ProID
                objinvoice.StageId = 0
                objinvoice.ClassId = ClassID
                objinvoice.AddItemToExistingInvoice()
            End If
        End Sub

        Sub PersistData()
            PersistTable.Clear()

            PersistTable.Add(PersistKey.SortColumnName & "ActionItems", txtSortColumnActionItems.Text.Trim())
            PersistTable.Add(PersistKey.SortOrder & "ActionItems", txtSortOrderActionItems.Text.Trim())

            PersistTable.Add(PersistKey.SortColumnName & "OppPro", txtSortColumnOppPro.Text.Trim())
            PersistTable.Add(PersistKey.SortOrder & "OppPro", txtSortOrderOppPro.Text.Trim())

            PersistTable.Add(PersistKey.SortColumnName & "Cases", txtSortColumnCases.Text.Trim())
            PersistTable.Add(PersistKey.SortOrder & "Cases", txtSortOrderCases.Text.Trim())

            PersistTable.Add(radTickler.ID, radTickler.SelectedItem.Value)
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)

            PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)

            PersistTable.Save()
        End Sub

        Sub LoadTicklerDetails()
            Try
                PersistData()
                Select Case radOppTab.SelectedIndex
                    Case 0
                        sb_DisplayTaskMeeting()
                        btnCloseAction.Visible = True
                        btnDeleteCases.Visible = True
                    Case 1
                        'sb_DisplayOppPro()
                        btnCloseAction.Visible = False
                        btnDeleteCases.Visible = False
                    Case 2
                        sb_DisplayCase()
                        btnCloseAction.Visible = False
                        btnDeleteCases.Visible = True
                    Case 3
                        BindPendingApproval()
                        btnCloseAction.Visible = False
                        btnDeleteCases.Visible = False
                    Case 4
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindPendingApproval()
            Dim _approvalconfig As New ApprovalConfig
            _approvalconfig.numDomainId = Session("DomainID")
            _approvalconfig.UserId = Session("UserContactID")
            _approvalconfig.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            _approvalconfig.dtStartDate = CDate(calFrom.SelectedDate)
            _approvalconfig.dtEndDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))
            Dim dtPendingExp As DataTable
            dtPendingExp = _approvalconfig.PendingApprovalForTickler()
            Dim view As New DataView(dtPendingExp)

            If (ddlFilter.SelectedValue = 0) Then
                grdApproveTE.DataSource = dtPendingExp
            ElseIf ddlFilter.SelectedValue = 1 Then
                view.RowFilter = "TypeApproval=1"
                grdApproveTE.DataSource = view
            ElseIf ddlFilter.SelectedValue = 2 Then
                view.RowFilter = "TypeApproval=2"
                grdApproveTE.DataSource = view
            ElseIf ddlFilter.SelectedValue = 3 Then
                view.RowFilter = "TypeApproval=3"
                grdApproveTE.DataSource = view
            End If
            grdApproveTE.DataBind()
        End Sub
        Private Sub sb_DisplayTaskMeeting()
            Try
                m_aryRightsForActItem = GetUserRightsForPage_Other(1, 2)
                If m_aryRightsForActItem(RIGHTSTYPE.VIEW) = 0 Then Exit Sub


                Dim strSplit As String()

                If objTickler Is Nothing Then objTickler = New Tickler
                Dim ds As DataSet
                Dim dtActIems As DataTable
                Dim dtCalItems As DataTable
                Dim dtBizActItem As DataTable
                objTickler.UserCntID = Session("UserContactID")
                objTickler.DomainID = Session("DomainID")
                objTickler.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset") 'Added By Debasish

                strDtFrom = CDate(calFrom.SelectedDate)
                strDtTo = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))

                objTickler.StartDate = strDtFrom
                objTickler.EndDate = strDtTo

                If radOpen.Checked Then
                    objTickler.OppStatus = "0" 'Open
                ElseIf radClosed.Checked Then
                    objTickler.OppStatus = "1" 'Closed
                Else
                    objTickler.OppStatus = "2" 'All
                End If

                If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                objTickler.CurrentPage = txtCurrrentPage.Text.Trim()

                objTickler.PageSize = Session("PagingRows")
                objTickler.TotalRecords = 0

                GridColumnSearchCriteria()
                objTickler.RegularSearchCriteria = RegularSearch
                objTickler.CustomSearchCriteria = CustomSearch
                If txtSortColumnActionItems.Text <> "" Then
                    objTickler.columnName = IIf(txtSortColumnActionItems.Text = "FormattedDate", "EndTime", txtSortColumnActionItems.Text)
                Else : objTickler.columnName = "EndTime"
                End If
                If txtSortOrderActionItems.Text = "D" Then
                    objTickler.columnSortOrder = "Desc"
                Else : objTickler.columnSortOrder = "Asc"
                End If
                Dim htGridColumnSearch As New Hashtable
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For j = 0 To strValues.Length - 1
                        strIDValue = strValues(j).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If
                Dim strBProcessValue As String = ""
                For Each item As RadComboBoxItem In radBProcesses.CheckedItems
                    If CCommon.ToLong(item.Value) > 0 Then
                        strBProcessValue = strBProcessValue & item.Value & ","
                    End If
                Next

                ds = objTickler.GetOldActItem(strBProcessValue:=strBProcessValue.TrimEnd(","))
                dtActIems = ds.Tables(0)
                Dim dvActItems As DataView
                dvActItems = dtActIems.DefaultView

                dtActIems.Columns.Add("FormattedDate", Type.GetType("System.String"))
                Dim dataRowItem As DataRow
                ' traverse all the rows and take appropriate values
                Dim strCorrTickler As String = ""
                Dim lngCommID As Long
                Dim lngCaseId As Long
                Dim lngCasetimeId As Long
                Dim lngCaseExpId As Long

                For Each dataRowItem In dtActIems.Rows

                    lngCaseId = IIf(IsDBNull(dataRowItem("caseid")), 0, dataRowItem("caseid"))
                    lngCommID = IIf(IsDBNull(dataRowItem("Id")), 0, dataRowItem("Id"))
                    lngCasetimeId = IIf(IsDBNull(dataRowItem("casetimeid")), 0, dataRowItem("casetimeid"))
                    lngCaseExpId = IIf(IsDBNull(dataRowItem("CaseExpId")), 0, dataRowItem("CaseExpId"))

                    If lngCasetimeId = 0 And lngCaseExpId = 0 And lngCaseId = 0 And dataRowItem("type") = 0 Then
                        strCorrTickler = strCorrTickler + "," + String.Format("{0}:{1}:{2}:{3}", lngCommID, lngCaseId, lngCasetimeId, lngCaseExpId)
                    End If

                    dataRowItem("FormattedDate") = ReturnDateTime(dataRowItem("EndTime"), dataRowItem("Startdate"), dataRowItem("Task"), dataRowItem("bitFollowUpAnyTime"))
                    Dim _act As String
                    Dim _Status As String
                    If (Not dataRowItem("Activity") Is Nothing And dataRowItem("Activity").ToString().Length > 0) Then
                        _act = dataRowItem("Activity").ToString()
                        If (_act.Trim = "") Then dataRowItem("Activity") = "-"
                    Else : dataRowItem("Activity") = "-"
                    End If

                    If (Not dataRowItem("Status") Is Nothing And dataRowItem("Status").ToString().Length > 0) Then
                        _Status = dataRowItem("Status").ToString()
                        If (_Status.Trim = "") Then
                            dataRowItem("Status") = "-"

                        End If
                    Else : dataRowItem("Status") = "-"
                    End If

                Next

                If objTickler.TotalRecords = 0 Then
                    txtTotalRecordsDeals.Text = 0
                Else
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = objTickler.TotalRecords / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (objTickler.TotalRecords Mod Session("PagingRows")) = 0 Then
                        txtTotalPageDeals.Text = strTotalPage(0)
                    Else
                        txtTotalPageDeals.Text = strTotalPage(0) + 1
                    End If
                    txtTotalRecordsDeals.Text = objTickler.TotalRecords
                End If

                If strCorrTickler.Length > 0 Then
                    strCorrTickler = strCorrTickler.Remove(0, 1)
                    Session("strCorrTickler") = strCorrTickler
                End If

                Dim dtColumns As DataTable
                dtColumns = ds.Tables(1)

                Dim i As Integer
                Dim bField As BoundField
                Dim Tfield As TemplateField
                gvSearch.Columns.Clear()
                For Each drRow As DataRow In dtColumns.Rows
                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New MyTemp1(ListItemType.Header, m_aryRightsForActItem, htGridColumnSearch, 43, drRow, False, txtSortColumnActionItems.Text, txtSortOrderActionItems.Text)
                    Tfield.ItemTemplate = New MyTemp1(ListItemType.Item, m_aryRightsForActItem, htGridColumnSearch, 43, drRow, False)
                    'Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, m_aryRightsForActItem(RIGHTSTYPE.UPDATE), htGridColumnSearch, 43, , False)
                    'Tfield.ItemTemplate = New GridTemplate(ListItemType.Header, drRow, m_aryRightsForActItem(RIGHTSTYPE.UPDATE), htGridColumnSearch, 43, , False)
                    gvSearch.Columns.Add(Tfield)
                Next

                Tfield = New TemplateField
                Tfield.HeaderTemplate = New MyTemp1(ListItemType.Header, m_aryRightsForActItem, htGridColumnSearch, 43, Nothing, True, txtSortColumnActionItems.Text, txtSortOrderActionItems.Text)
                Tfield.ItemTemplate = New MyTemp1(ListItemType.Item, m_aryRightsForActItem, htGridColumnSearch, 43, Nothing, True)
                'Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dtColumns.Rows(i), m_aryRightsForActItem(RIGHTSTYPE.UPDATE), htGridColumnSearch, 43, , False)
                'Tfield.ItemTemplate = New GridTemplate(ListItemType.Header, dtColumns.Rows(i), m_aryRightsForActItem(RIGHTSTYPE.UPDATE), htGridColumnSearch, 43, , False)
                gvSearch.Columns.Add(Tfield)


                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objTickler.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text
                gvSearch.DataSource = dtActIems
                gvSearch.DataBind()

                'For Each row As GridViewRow In gvSearch.Rows
                '    If ViewState(row.FindControl("chkSelect").UniqueID) IsNot Nothing Then
                '        CType(row.FindControl("chkSelect"), CheckBox).Checked = ViewState(row.FindControl("chkSelect").UniqueID)
                '    End If

                'Next

                Dim objPageControls As New PageControls
                Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtColumns)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "InlineEditValidation", strValidation, True)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("Cust") Then
                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" Div.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(1) & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" Div.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(1) & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" Div.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values CFWInner WHERE CFWInner.RecId=DivisionMaster.numDivisionID AND CFWInner.fld_id =" & strID(1) & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" Div.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(1) & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" Div.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(1) & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" Div.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(1) & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " fn_GetCustFldStringValue(" & strID(1) & ",DivisionMaster.numDivisionID,CFW.Fld_Value) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" Div.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(1) & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" Div.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(1) & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                            'Select Case strID(3).Trim()
                            '    Case "TextBox"
                            '        strCustom = " like '%" & strIDValue(1).Replace("'", "''") & "%'"
                            '    Case "SelectBox"
                            '        strCustom = "=" & strIDValue(1)
                            'End Select

                            'strCustomCondition.Add("CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom)
                        ElseIf strID(0) = "OBD.vcBizDocsList" Then
                            strRegularCondition.Add("(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId = Opp.numOppId AND vcBizDocID IN ('" & strIDValue(1).Replace("'", "").Replace(",", "','") & "')) > 0")
                        ElseIf strID(0) = "OI.vcInventoryStatus" Then
                            strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                        ElseIf strID(0) = "cmp.vcPerformance" Then
                            strRegularCondition.Add("cmp" & strID(0) & "=" & strIDValue(1))
                        ElseIf strID(0) = "FormattedDate" Then
                            If strID(4) = "From" Then
                                Dim fromDate As Date
                                If Date.TryParse(strIDValue(1), fromDate) Then
                                    strRegularCondition.Add("comm.dtStartTime >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                End If
                            ElseIf strID(4) = "To" Then
                                Dim toDate As Date
                                If Date.TryParse(strIDValue(1), toDate) Then
                                    strRegularCondition.Add("comm.dtStartTime <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                End If
                            End If
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox", "Label"
                                    strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "SelectBox"
                                    If strID(0) = ".Task" Then
                                        strRegularCondition.Add("Comm.bitTask IN (" & strIDValue(1) & ")")
                                    ElseIf strID(0) = "Activity" Then
                                        strRegularCondition.Add("Comm.numActivity=" & strIDValue(1))
                                    ElseIf strID(0) = "Status" Then
                                        strRegularCondition.Add("Comm.numStatus=" & strIDValue(1))
                                    ElseIf strID(0) = "numAssignedTo" Then
                                        strRegularCondition.Add("Comm.numAssign=" & strIDValue(1))
                                    ElseIf strID(0) = "numAssignedBy" Then
                                        strRegularCondition.Add("Comm.numAssignedBy=" & strIDValue(1))
                                    ElseIf strID(0) = "numRecOwner" Then
                                        strRegularCondition.Add("Comm.numCreatedBy=" & strIDValue(1))
                                    ElseIf strID(0).Contains("numFollowUpStatus") Then
                                        strRegularCondition.Add(strID(0) & " IN(" & strIDValue(1) & ")")
                                    Else
                                        strRegularCondition.Add(strID(0) & "='" & strIDValue(1).Replace("'", "''") & "'")
                                    End If
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        <Services.WebMethod(EnableSession:=True)>
        Public Shared Function WebMethodGetTaskList(ByVal DomainID As Integer, ByVal UserContactID As Long) As String
            Try
                Dim dtOpportunity As DataTable
                Dim objTickler = New Tickler
                objTickler.UserCntID = UserContactID
                objTickler.DomainID = DomainID
                dtOpportunity = objTickler.GetUserActivity()
                For Each row As DataRow In dtOpportunity.Rows
                    Dim isValidDate As Boolean = IsDate(row("dtmDueDate"))
                    If (isValidDate = True) Then
                        row("dtmDueDate") = FormattedDateFromDate(Convert.ToDateTime(row("dtmDueDate")), HttpContext.Current.Session("DateFormat"))
                    End If
                Next row
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtOpportunity, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""
                strError = ex.Message
                Throw New Exception(strError)
            End Try
        End Function

        Private Sub sb_DisplayOppPro()
            Try
                m_aryRightsForOpportunity = GetUserRightsForPage_Other(1, 3)
                If m_aryRightsForOpportunity(RIGHTSTYPE.VIEW) = 0 Then Exit Sub

                Dim strSplit As String()
                Dim i As Integer
                Dim dtOpportunity As DataTable
                If objTickler Is Nothing Then objTickler = New Tickler
                objTickler.UserCntID = Session("UserContactID")
                objTickler.DomainID = Session("DomainID")

                strDtFrom = CDate(calFrom.SelectedDate)
                strDtTo = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))

                objTickler.StartDate = strDtFrom
                objTickler.EndDate = strDtTo
                dtOpportunity = objTickler.GetOpportunity

                If Not dtOpportunity Is Nothing Then
                    Dim dv As DataView
                    dv = New DataView(dtOpportunity)

                    If txtSortColumnOppPro.Text <> "" And txtSortOrderOppPro.Text <> "" Then
                        dv.Sort = txtSortColumnOppPro.Text.ToLower & " " & IIf(txtSortOrderOppPro.Text = "D", "DESC", "ASC")
                    Else
                        dv.Sort = "closedate desc"
                    End If

                    dgOppPro.DataSource = dv
                    dgOppPro.DataBind()
                Else
                    dgOppPro.DataSource = dtOpportunity
                    dgOppPro.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnTime(ByVal CloseDate) As String
            Try
                Dim timePart As String
                If Not IsDBNull(CloseDate) Then
                    timePart = CloseDate.ToShortTimeString.Substring(0, CloseDate.ToShortTimeString.Length - 1)
                    ' remove gaps
                    If timePart.Split(" ").Length >= 2 Then timePart = timePart.Split(" ").GetValue(0) + timePart.Split(" ").GetValue(1)
                    Return timePart
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub sb_DisplayCase()
            Try
                m_aryRightsForCase = GetUserRightsForPage_Other(1, 4)
                If m_aryRightsForCase(RIGHTSTYPE.VIEW) = 0 Then Exit Sub

                Dim strSplit As String()

                Dim dtCases As DataTable
                If objTickler Is Nothing Then objTickler = New Tickler
                objTickler.UserCntID = Session("UserContactID")
                objTickler.DomainID = Session("DomainID")

                strDtFrom = CDate(calFrom.SelectedDate)
                strDtTo = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))

                objTickler.StartDate = strDtFrom
                objTickler.EndDate = strDtTo
                dtCases = objTickler.GetCases

                Dim dv As DataView
                dv = New DataView(dtCases)

                If txtSortColumnCases.Text <> "" And txtSortOrderCases.Text <> "" Then
                    dv.Sort = txtSortColumnCases.Text.ToLower & " " & IIf(txtSortOrderCases.Text = "D", "DESC", "ASC")
                End If

                dgCases.DataSource = dv
                dgCases.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function AddQuotes(ByVal strValue)
            Try
                Dim strQUOTE As String
                strQUOTE = """"
                AddQuotes = strQUOTE & Replace(strValue, strQUOTE, strQUOTE & strQUOTE) & strQUOTE
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnName(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                If Format(CloseDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                    strTargetResolveDate = "<b><font color=red>Today</font></b>"
                ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                    strTargetResolveDate = "<b><font color=orange>" & strTargetResolveDate & "</font></b>"
                ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, -1, Now()), "yyyyMMdd") Then
                    strTargetResolveDate = "<b><font color=purple>" & strTargetResolveDate & "</font></b>"
                End If
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to display the Date and the time (in format + Time)
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/10/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Function ReturnDateTime(ByVal CloseDate, ByVal StartDate, ByVal bitTask, ByVal bitFollowUpAnyTime) As String
            Try
                Dim strTargetResolveDate As String = ""
                Dim temp As String = ""
                If Not IsDBNull(CloseDate) Then
                    strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                    Dim timePart As String = CloseDate.ToShortTimeString.Substring(0, CloseDate.ToShortTimeString.Length - 1)
                    Dim timePart2 As String = StartDate.ToShortTimeString.Substring(0, StartDate.ToShortTimeString.Length - 1)
                    ' remove gaps

                    'If bitTask.ToString().Trim().ToLower() = "Follow-up Anytime".ToLower() Then
                    If bitFollowUpAnyTime = True Then
                        timePart = ""
                        timePart2 = "Any Time"
                    Else
                        If timePart.Split(" ").Length >= 2 Then timePart = timePart.Split(" ").GetValue(0) + timePart.Split(" ").GetValue(1)
                        If timePart2.Split(" ").Length >= 2 Then timePart2 = timePart2.Split(" ").GetValue(0) + timePart2.Split(" ").GetValue(1)
                        timePart2 = timePart2 & ", "
                    End If

                    ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it is today
                    Dim strNow As Date
                    strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    If (StartDate.Date = strNow.Date And StartDate.Month = strNow.Month And StartDate.Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=red><b>Today</b>" & ", " + timePart2 & timePart & "</font>"
                        Return strTargetResolveDate

                        ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                        ' if both are same it was Yesterday
                    ElseIf (StartDate.Date.AddDays(1).Date = strNow.Date And StartDate.AddDays(1).Month = strNow.Month And StartDate.AddDays(1).Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=purple><b>Yesterday</b>" & ", " + timePart2 & timePart & "</font>"
                        Return strTargetResolveDate

                        ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                        ' if both are same it will Tomorrow
                    ElseIf (StartDate.Date = strNow.AddDays(1).Date And StartDate.Month = strNow.AddDays(1).Month And StartDate.Year = strNow.AddDays(1).Year) Then
                        temp = StartDate.Hour.ToString + ":" + StartDate.Minute.ToString
                        strTargetResolveDate = "<font color=orange><b>Tomorrow</b>" & ", " + timePart2 & timePart + "</font>"
                        Return strTargetResolveDate
                    Else
                        strTargetResolveDate = WeekdayName(Weekday(StartDate)) & ", " & strTargetResolveDate & ", " + timePart2 & timePart
                        Return strTargetResolveDate
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnDate(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                Dim temp As String = ""
                If Not IsDBNull(CloseDate) Then
                    strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                    ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it is today
                    Dim strNow As Date
                    strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    If (CloseDate.Date = strNow.Date And CloseDate.Month = strNow.Month And CloseDate.Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=red><b>Today</b></font>"
                        Return strTargetResolveDate

                        ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                        ' if both are same it was Yesterday
                    ElseIf (CloseDate.Date.AddDays(1).Date = strNow.Date And CloseDate.AddDays(1).Month = strNow.Month And CloseDate.AddDays(1).Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                        Return strTargetResolveDate

                        ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                        ' if both are same it will Tomorrow
                    ElseIf (CloseDate.Date = strNow.AddDays(1).Date And CloseDate.Month = strNow.AddDays(1).Month And CloseDate.Year = strNow.AddDays(1).Year) Then
                        temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                        strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                        Return strTargetResolveDate
                    Else
                        strTargetResolveDate = strTargetResolveDate
                        Return strTargetResolveDate
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgCases_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCases.ItemCommand
            Try
                Dim lngCaseID As Long
                If Not e.CommandName = "Sort" Then lngCaseID = e.Item.Cells(0).Text

                If e.CommandName = "Case" Then
                    Response.Redirect("../cases/frmCases.aspx?frm=tickler&CaseID=" & lngCaseID & "&SI1=" & radOppTab.SelectedIndex)

                ElseIf e.CommandName = "Contact" Then
                    objCommon.CaseID = lngCaseID
                    objCommon.charModule = "S"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../contact/frmContacts.aspx?frm=tickler&fdas89iu=098jfd&CntId=" & objCommon.ContactID & "&SI1=" & radOppTab.SelectedIndex)

                ElseIf e.CommandName = "Company" Then
                    objCommon.CaseID = lngCaseID
                    objCommon.charModule = "S"
                    objCommon.GetCompanySpecificValues1()
                    If objCommon.CRMType = 0 Then
                        Response.Redirect("../Leads/frmLeads.aspx?frm=tickler&DivID=" & objCommon.DivisionID & "&SI1=" & radOppTab.SelectedIndex)

                    ElseIf objCommon.CRMType = 1 Then
                        Response.Redirect("../prospects/frmProspects.aspx?frm=tickler&DivID=" & objCommon.DivisionID & "&SI1=" & radOppTab.SelectedIndex)

                    ElseIf objCommon.CRMType = 2 Then
                        Response.Redirect("../account/frmAccounts.aspx?frm=tickler&klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&SI1=" & radOppTab.SelectedIndex)

                    End If
                ElseIf e.CommandName = "Delete" Then
                    Dim objCases As New CCases
                    objCases.CaseID = lngCaseID
                    objCases.DomainID = Session("DomainID")
                    If objCases.DeleteCase() = False Then
                        ShowMessage("Dependent Records Exists.Cannot be deleted.")
                    Else
                        sb_DisplayCase()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgCases_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCases.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    e.Item.Cells(6).ToolTip = e.Item.Cells(4).Text
                    Dim hplCaseEmail As HyperLink
                    hplCaseEmail = e.Item.FindControl("hplCaseEmail")
                    hplCaseEmail.NavigateUrl = "#"
                    If Session("CompWindow") = 1 Then
                        hplCaseEmail.NavigateUrl = "mailto:" & hplCaseEmail.Text
                    Else : hplCaseEmail.Attributes.Add("onclick", "return OpemEmail('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=" & hplCaseEmail.Text & "&CaseID=" & e.Item.Cells(0).Text & "')")
                    End If

                    Dim chkSelect As CheckBox
                    chkSelect = e.Item.FindControl("chk")
                    If m_aryRightsForCase(RIGHTSTYPE.DELETE) = 0 Then
                        chkSelect.Visible = False

                    ElseIf m_aryRightsForCase(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If e.Item.Cells(2).Text = Session("UserContactID") Then
                                chkSelect.Visible = True
                            Else
                                chkSelect.Visible = False
                            End If
                        Catch ex As Exception
                        End Try

                    ElseIf m_aryRightsForCase(RIGHTSTYPE.DELETE) = 2 Then
                        Try
                            Dim i As Integer
                            Dim dtTerritory As DataTable
                            dtTerritory = Session("UserTerritory")
                            If e.Item.Cells(1).Text = 0 Then
                                chkSelect.Visible = True
                            Else
                                Dim chkDelete As Boolean = False
                                For i = 0 To dtTerritory.Rows.Count - 1
                                    If e.Item.Cells(1).Text = dtTerritory.Rows(i).Item("numTerritoryId") Then chkDelete = True
                                Next
                                If chkDelete = True Then
                                    chkSelect.Visible = True
                                Else
                                    chkSelect.Visible = False
                                End If
                            End If
                        Catch ex As Exception
                        End Try

                    ElseIf m_aryRightsForCase(RIGHTSTYPE.DELETE) = 3 Then
                        btnDeleteCases.Attributes.Add("onclick", "return DeleteActionItemsConfirmation()")

                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgCases_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgCases.SortCommand
            Try
                m_aryRightsForCase = GetUserRightsForPage_Other(1, 2)
                If m_aryRightsForCase(RIGHTSTYPE.VIEW) = 0 Then Exit Sub

                Dim strColumn As String = e.SortExpression.ToString()
                If txtSortColumnCases.Text <> strColumn Then
                    txtSortColumnCases.Text = strColumn
                    txtSortOrderCases.Text = "A"
                Else
                    If txtSortOrderCases.Text = "D" Then
                        txtSortOrderCases.Text = "A"
                    Else : txtSortOrderCases.Text = "D"
                    End If
                End If
                LoadTicklerDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                LoadTicklerDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        'Private Sub tsVert_SelectedIndexChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsVert.SelectedIndexChange
        '    If Session("Date") = 0 Then
        '        strDtFrom = New Date(1900, 1, 1)
        '        strDtTo = DateAdd(DateInterval.Day, +5, Now())
        '    Else
        '        strDtTo = New Date(3000, 1, 1)
        '        strDtFrom = DateAdd(DateInterval.Day, -1, Now())
        '    End If
        '    loadData()
        'End Sub

        Private Sub dgOppPro_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOppPro.ItemCommand
            Try
                Dim lngID As Long
                If Not e.CommandName = "Sort" Then lngID = e.Item.Cells(0).Text
                If e.CommandName = "Opportunity" Then
                    If e.Item.Cells(3).Text = "Project" Then
                        Response.Redirect("../projects/frmProjects.aspx?frm=tickler&ProId=" & lngID & "&StageId=" & e.Item.Cells(1).Text & "&SI1=" & radOppTab.SelectedIndex & "&SelectedIndex=1")

                    Else
                        Response.Redirect("../opportunity/frmOpportunities.aspx?frm=tickler&OpID=" & lngID & "&StageId=" & e.Item.Cells(1).Text & "&SI1=" & radOppTab.SelectedIndex & "&SelectedIndex=1")

                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgOppPro_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgOppPro.SortCommand
            Try
                m_aryRightsForOpportunity = GetUserRightsForPage_Other(1, 4)
                If m_aryRightsForOpportunity(RIGHTSTYPE.VIEW) = 0 Then Exit Sub

                Dim strColumn As String = e.SortExpression.ToString()
                If txtSortColumnOppPro.Text <> strColumn Then
                    txtSortColumnOppPro.Text = strColumn
                    txtSortOrderOppPro.Text = "A"
                Else
                    If txtSortOrderOppPro.Text = "D" Then
                        txtSortOrderOppPro.Text = "A"
                    Else : txtSortOrderOppPro.Text = "D"
                    End If
                End If
                LoadTicklerDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSearch.RowCommand
            Try

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub radOppTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles radOppTab.TabClick
            Try
                LoadTicklerDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                radTickler.SelectedValue = "0"
                LoadTicklerDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub radTickler_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radTickler.SelectedIndexChanged
            Try
                If radTickler.SelectedValue = "0" Then
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, 1, Date.UtcNow))

                ElseIf CCommon.ToInteger(radTickler.SelectedValue) = CCommon.ToInteger(TicklerType.YESTERDAY_TODAY_TOMORROW) Then
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -1, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, Date.UtcNow))

                ElseIf CCommon.ToInteger(radTickler.SelectedValue) = CCommon.ToInteger(TicklerType.YESTERDAY_THROUGH_NEXT_7DAYS) Then
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -1, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 7, Date.UtcNow))

                ElseIf CCommon.ToInteger(radTickler.SelectedValue) = CCommon.ToInteger(TicklerType.LAST_7DAYS_THROUGH_NEXT_7DAYS) Then
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 7, Date.UtcNow))

                ElseIf CCommon.ToInteger(radTickler.SelectedValue) = CCommon.ToInteger(TicklerType.LAST_MONTH_THROUGH_NEXT_WEEK) Then
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, -1, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 7, Date.UtcNow))

                ElseIf CCommon.ToInteger(radTickler.SelectedValue) = CCommon.ToInteger(TicklerType.LAST_WEEK_THROUGH_NEXT_MONTH) Then
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, 1, Date.UtcNow))

                ElseIf CCommon.ToInteger(radTickler.SelectedValue) = CCommon.ToInteger(TicklerType.LAST_MONTH_THROUGH_NEXT_MONTH) Then
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, -1, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, 1, Date.UtcNow))

                ElseIf CCommon.ToInteger(radTickler.SelectedValue) = CCommon.ToInteger(TicklerType.LAST_YEAR_THROUGH_THIS_YEAR) Then
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Year, -1, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Year, 0, Date.UtcNow))

                ElseIf CCommon.ToInteger(radTickler.SelectedValue) = CCommon.ToInteger(TicklerType.LAST_YEAR_THROUGH_NEXT_YEAR) Then
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Year, -1, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Year, 1, Date.UtcNow))

                End If

                LoadTicklerDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvSearch_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvSearch.RowDataBound
            Try

                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim lngTerrID As Long = CCommon.ToLong(gvSearch.DataKeys(e.Row.RowIndex).Values("numOrgTerId"))
                    Dim lngRecOwnID As Long = CCommon.ToLong(gvSearch.DataKeys(e.Row.RowIndex).Values("numCreatedBy"))

                    Dim chkSelect As CheckBox
                    chkSelect = e.Row.FindControl("chkSelect")
                    If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 0 Then
                        chkSelect.Visible = False

                    ElseIf m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If lngRecOwnID = Session("UserContactID") Then
                                chkSelect.Visible = True
                            Else
                                chkSelect.Visible = False
                            End If
                        Catch ex As Exception
                        End Try

                    ElseIf m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 2 Then
                        Try
                            Dim i As Integer
                            Dim dtTerritory As DataTable
                            dtTerritory = Session("UserTerritory")
                            If lngTerrID = 0 Then
                                chkSelect.Visible = True
                            Else
                                Dim chkDelete As Boolean = False
                                For i = 0 To dtTerritory.Rows.Count - 1
                                    If lngTerrID = dtTerritory.Rows(i).Item("numTerritoryId") Then chkDelete = True
                                Next
                                If chkDelete = True Then
                                    chkSelect.Visible = True
                                Else
                                    chkSelect.Visible = False
                                End If
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvSearch_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSearch.RowDeleting

        End Sub

        Private Sub BindBusinessProcess()
            Try
                Dim dtColums As New DataTable
                Dim objOpportunity As New MOpportunity
                objOpportunity.ProType = 0
                objOpportunity.DomainID = Session("DomainID")
                dtColums = objOpportunity.BusinessProcess

                radBProcesses.DataSource = dtColums
                radBProcesses.DataTextField = "slp_name"
                radBProcesses.DataValueField = "slp_id"
                radBProcesses.ShowDropDownOnTextboxClick = True
                radBProcesses.CheckBoxes = True
                radBProcesses.EmptyMessage = "-- Filter by Business Processes --"
                radBProcesses.Width = 200
                radBProcesses.EnableTextSelection = True
                radBProcesses.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnBProcess_Click(sender As Object, e As EventArgs)
            Try
                sb_DisplayTaskMeeting()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnCloseAction_Click(sender As Object, e As EventArgs) Handles btnCloseAction.Click
            Try
                For Each row As GridViewRow In gvSearch.Rows

                    If row.FindControl("chkSelect") IsNot Nothing AndAlso CType(row.FindControl("chkSelect"), CheckBox).Checked = True Then

                        'Update Communication, Close Action Item
                        Dim objCommon As New CCommon
                        objCommon.Mode = 46
                        objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                        objCommon.UpdateRecordID = CCommon.ToLong(gvSearch.DataKeys(row.RowIndex).Values("ID"))
                        objCommon.UpdateValueID = 1
                        Try
                            objCommon.UpdateSingleFieldValue()
                        Catch ex As Exception

                        End Try
                    End If
                Next

                sb_DisplayTaskMeeting()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnDeleteCases_Click(sender As Object, e As EventArgs) Handles btnDeleteCases.Click
            Try

                If radOppTab.SelectedTab.Value = "ActionItemsMeetings" Then
                    Dim refreshGrid As Boolean = True
                    refreshGrid = ValidateMeetings()
                    If refreshGrid = True Then
                        LoadTicklerDetails()
                    End If

                ElseIf radOppTab.SelectedTab.Value = "OpportunitiesProjects" Then
                    LoadTicklerDetails()
                ElseIf radOppTab.SelectedTab.Value = "radPageView_Cases" Then
                    Dim lngCaseId As Long = 0
                    For Each dgItem As DataGridItem In dgCases.Items
                        If dgItem.FindControl("chkSelect") IsNot Nothing AndAlso CType(dgItem.FindControl("chkSelect"), CheckBox).Checked = True Then

                            Dim objCases As New CCases
                            objCases.CaseID = lngCaseId
                            objCases.DomainID = Session("DomainID")
                            If objCases.DeleteCase() = False Then
                                ShowMessage("Dependent Records Exists.Cannot be deleted.")
                            Else
                                sb_DisplayCase()
                            End If
                        End If
                    Next
                    LoadTicklerDetails()
                End If


            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Function ValidateMeetings() As Boolean

            Dim RefreshGrid As Boolean = True
            Try
                For Each row As GridViewRow In gvSearch.Rows

                    Dim lngCommID As Long = 0
                    Dim lngCaseId As Long = 0
                    Dim lngCasetimeId As Long = 0
                    Dim lngCaseExpId As Long = 0
                    Dim lngTerrID As Long = 0
                    Dim lngRecOwnID As Long = 0

                    'If ViewState(row.FindControl("chkSelect").UniqueID) IsNot Nothing Then
                    '    CType(row.FindControl("chkSelect"), CheckBox).Checked = ViewState(row.FindControl("chkSelect").UniqueID)
                    'End If

                    If row.FindControl("chkSelect") IsNot Nothing AndAlso CType(row.FindControl("chkSelect"), CheckBox).Checked = True Then

                        lngCommID = gvSearch.DataKeys(row.RowIndex).Values("ID")
                        lngCaseId = gvSearch.DataKeys(row.RowIndex).Values("CaseId")
                        lngCasetimeId = gvSearch.DataKeys(row.RowIndex).Values("CaseTimeId")
                        lngCaseExpId = gvSearch.DataKeys(row.RowIndex).Values("CaseExpId")
                        lngTerrID = CCommon.ToLong(gvSearch.DataKeys(row.RowIndex).Values("numOrgTerId"))
                        lngRecOwnID = CCommon.ToLong(gvSearch.DataKeys(row.RowIndex).Values("numCreatedBy"))

                        hdnlngCommID.Value = lngCommID
                        hdnlngRecOwnID.Value = lngRecOwnID
                        hdnlngTerrID.Value = lngTerrID
                        hdnnumContactID.Value = gvSearch.DataKeys(row.RowIndex).Values("numContactID")

                        If gvSearch.DataKeys(row.RowIndex).Values("type") = "0" Then
                            Dim objActionItem As New ActionItem
                            objActionItem.CommID = lngCommID
                            If objActionItem.DeleteActionItem = False Then
                                ShowMessage("Dependent Record Exists. Cannot be Deleted.")
                            End If

                        ElseIf gvSearch.DataKeys(row.RowIndex).Values("type") = "1" Then

                            If HttpContext.Current.Session("BtoGCalendar") = True Then
                                If hdnDeleteType IsNot Nothing AndAlso New String() {"1", "2"}.Contains(hdnDeleteType.Value) Then
                                    divActivityDeletepopup.Visible = False
                                    If hdnDeleteType.Value = "1" Then
                                        Dim objCalendar As New GoogleCalendar
                                        divActivityDeletepopup.Visible = False
                                        objCalendar.GoogleActivityDelete(gvSearch.DataKeys(row.RowIndex).Values("numContactID"), Session("DomainID"), lngCommID)
                                        DeleteActivityFromBiz()
                                        ' ViewState.Remove(row.FindControl("chkSelect").UniqueID)
                                    Else
                                        divActivityDeletepopup.Visible = False
                                        DeleteActivityFromBiz()
                                        'ViewState.Remove(row.FindControl("chkSelect").UniqueID)
                                    End If
                                Else
                                    divActivityDeletepopup.Visible = True
                                    RefreshGrid = False
                                    'ViewState(row.FindControl("chkSelect").UniqueID) = True

                                End If
                            Else
                                divActivityDeletepopup.Visible = False
                                DeleteActivityFromBiz()
                                'ViewState.Remove(row.FindControl("chkSelect").UniqueID)

                            End If
                        End If
                    End If

                Next
            Catch ex As Exception
                RefreshGrid = False
            End Try
            Return RefreshGrid
        End Function

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub DeleteActivityFromBiz()
            Try
                Dim ObjOutlook As New COutlook
                ObjOutlook.ActivityID = CCommon.ToLong(hdnlngCommID.Value)

                If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 1 Then
                    Try
                        If CCommon.ToLong(hdnlngRecOwnID.Value) = Session("UserContactID") Then
                            ObjOutlook.RemoveActivity()
                        End If
                    Catch ex As Exception

                    End Try
                ElseIf m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 2 Then
                    Try
                        Dim j As Integer
                        Dim dtTerritory As New DataTable
                        dtTerritory = Session("UserTerritory")
                        Dim chkDelete As Boolean = False
                        If CCommon.ToLong(hdnlngTerrID.Value) = 0 Then
                            chkDelete = True
                        Else
                            For j = 0 To dtTerritory.Rows.Count - 1
                                If CCommon.ToLong(hdnlngTerrID.Value) = dtTerritory.Rows(j).Item("numTerritoryId") Then chkDelete = True
                            Next
                        End If

                        If chkDelete = True Then
                            ObjOutlook.RemoveActivity()
                        End If
                    Catch ex As Exception

                    End Try
                ElseIf m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 3 Then
                    ObjOutlook.RemoveActivity()
                End If
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub grdApproveTE_RowDataBound(sender As Object, e As GridViewRowEventArgs)
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim hdnApproval As HiddenField
                Dim hdnApprovalType As HiddenField
                Dim lblApprovalRequest As Label
                Dim lnkApprove As LinkButton
                Dim lnkDecline As LinkButton
                Dim lnkEdit As LinkButton
                lblApprovalRequest = DirectCast(e.Row.FindControl("lblApprovalRequest"), Label)
                hdnApproval = DirectCast(e.Row.FindControl("hdnApprovalStaus"), HiddenField)
                hdnApprovalType = DirectCast(e.Row.FindControl("hdnApprovalType"), HiddenField)
                lnkApprove = DirectCast(e.Row.FindControl("lnkApprove"), LinkButton)
                lnkDecline = DirectCast(e.Row.FindControl("lnkDecline"), LinkButton)
                lnkEdit = DirectCast(e.Row.FindControl("lnkEdit"), LinkButton)
                If hdnApprovalType.Value = "2" Then
                    lnkDecline.Visible = False
                End If
                If hdnApproval.Value = "-1" Then
                    lblApprovalRequest.Visible = False
                    lnkEdit.Visible = True
                    lnkDecline.Visible = False
                End If
            End If
        End Sub

        Protected Sub grdApproveTE_RowEditing(sender As Object, e As GridViewEditEventArgs)

        End Sub

        Protected Sub lnkAddNewActionItem_Click(sender As Object, e As EventArgs)
            Response.Redirect("../Admin/actionitemdetailsold.aspx")
        End Sub

        Protected Sub ddlFilter_SelectedIndexChanged(sender As Object, e As EventArgs)
            BindPendingApproval()
        End Sub

        Protected Sub btnActivityDelete_Click(sender As Object, e As EventArgs)
            Try
                hdnDeleteType.Value = CCommon.ToLong(rblDeleteType.SelectedValue)
                rblDeleteType.Items.Clear()
                Dim refreshGrid As Boolean = True
                refreshGrid = ValidateMeetings()
                If refreshGrid = True Then
                    LoadTicklerDetails()
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub


    End Class

    Public Class MyTemp1
        Implements ITemplate

        Dim TemplateType As ListItemType
        Dim FieldName, DBColumnName, AllowSorting, AllowFiltering, FormFieldId, ControlType, FormID As String
        Dim ColumnName, LookBackTableName, OrigDBColumnName, tableAlias, ListType, SortColumnName, SortColumnOrder, FieldDataType As String
        Dim Custom, AllowEdit As Boolean
        Dim EditPermission, ColumnWidth, ListID As Integer
        Dim m_aryRightsForActItem() As Integer
        Dim htGridColumnSearch As Hashtable
        Sub New(ByVal type As ListItemType, ByVal aryRightsForActItem() As Integer, GridColumnSearch As Hashtable, numFormID As Long, Optional ByVal dtRow As DataRow = Nothing, Optional ByVal IsDeleteTrue As Boolean = False, Optional ByVal vcSortColumnName As String = "", Optional ByVal vcSortColumnOrder As String = "")
            Try
                TemplateType = type
                FormID = numFormID

                If dtRow IsNot Nothing Then
                    FieldName = dtRow.Item("vcFieldName")
                    DBColumnName = dtRow.Item("vcDbColumnName")
                    OrigDBColumnName = dtRow.Item("vcOrigDbColumnName").ToString
                    AllowSorting = dtRow.Item("bitAllowSorting")
                    AllowFiltering = dtRow.Item("bitAllowFiltering")
                    FormFieldId = dtRow.Item("numFieldId")
                    AllowEdit = dtRow.Item("bitAllowEdit")
                    Custom = dtRow.Item("bitCustomField")
                    ColumnWidth = CCommon.ToInteger(dtRow("intColumnWidth"))
                    ControlType = dtRow.Item("vcAssociatedControlType")
                    ListType = dtRow.Item("vcListItemType")
                    LookBackTableName = dtRow.Item("vcLookBackTableName").ToString
                    ListID = CCommon.ToLong(dtRow.Item("numListID"))
                    EditPermission = aryRightsForActItem(RIGHTSTYPE.UPDATE)
                ElseIf IsDeleteTrue = True Then
                    ControlType = "DeleteCheckBox"
                Else
                    ControlType = "Button"
                End If

                ColumnName = DBColumnName & "~" & FormFieldId & "~" & IIf(Custom, 1, 0).ToString
                htGridColumnSearch = GridColumnSearch
                m_aryRightsForActItem = aryRightsForActItem

                SortColumnName = vcSortColumnName
                SortColumnOrder = vcSortColumnOrder
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
            Try
                Dim lbl1 As Label = New Label()
                Dim lnkButton As New LinkButton
                Dim lnk As New HyperLink
                Select Case TemplateType
                    Case ListItemType.Header
                        Dim cell As DataControlFieldHeaderCell = CType(Container, DataControlFieldHeaderCell)
                        Dim txtSearch As TextBox = New TextBox()
                        Dim ddlSearch As DropDownList = New DropDownList()
                        If ControlType <> "Button" AndAlso ControlType <> "DeleteCheckBox" Then
                            If AllowSorting = False Then
                                lbl1.ID = DBColumnName
                                lbl1.Text = FieldName
                                'lbl1.ForeColor = Color.White
                                Container.Controls.Add(lbl1)
                            ElseIf AllowSorting = True Then
                                lnkButton.ID = DBColumnName
                                lnkButton.Text = FieldName
                                'lnkButton.ForeColor = Color.White
                                lnkButton.Attributes.Add("onclick", "return SortColumn('" & DBColumnName & "')")
                                Container.Controls.Add(lnkButton)

                                If OrigDBColumnName = SortColumnName Or DBColumnName = SortColumnName Then
                                    Container.Controls.Add(New LiteralControl(String.Format("&nbsp;<font color='black'>{0}</font>", IIf(SortColumnOrder = "D", "&#9660;", "&#9650;"))))
                                End If
                            End If
                        ElseIf ControlType = "DeleteCheckBox" Then
                            Dim chk As New CheckBox
                            chk.ID = "chkAll"
                            chk.Attributes.Add("onclick", "return SelectAll('chkAll','chkSelect','gvSearch')")
                            Container.Controls.Add(chk)
                        Else
                            lbl1.Text = " "
                        End If
                        If AllowFiltering = "True" Then
                            If DBColumnName = "numFollowUpStatus" Then

                                Container.Controls.Add(New LiteralControl("<br />"))

                                ddlSearch = New DropDownList
                                ddlSearch.Items.Clear()

                                ddlSearch.CssClass = "option-droup-multiSelection-Group"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ClientIDMode = ClientIDMode.Static
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType
                                ddlSearch.Attributes("multiple") = "multiple"
                                Dim dt As New DataTable()
                                Dim objCommon As New CCommon()
                                objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
                                objCommon.ListID = CCommon.ToLong(30)
                                dt = objCommon.GetMasterListItemsWithRights()
                                Dim item As ListItem = New ListItem("-- All --", "0")
                                ddlSearch.Items.Add(item)
                                ddlSearch.AutoPostBack = False
                                For Each dr As DataRow In dt.Rows
                                    item = New ListItem(CCommon.ToString(dr("vcData")), CCommon.ToString(dr("numListItemID")))
                                    item.Attributes("OptionGroup") = CCommon.ToString(dr("vcListItemGroupName"))
                                    item.Attributes("class") = CCommon.ToString(dr("vcColorScheme"))
                                    ddlSearch.Items.Add(item)
                                Next
                                'ddlSearch.SelectedIndex = 0
                                'ddlSearch.SelectionMode = ListSelectionMode.Multiple
                                ddlSearch.ClearSelection()
                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    Dim stritemValues As String() = htGridColumnSearch(ddlSearch.ID).ToString.Split(",")
                                    If stritemValues.Length > 0 Then
                                        ddlSearch.ClearSelection()
                                    End If
                                    For Each indivItem As ListItem In ddlSearch.Items
                                        If stritemValues.Contains(indivItem.Value) Then
                                            indivItem.Selected = True
                                        End If
                                    Next
                                End If
                                Container.Controls.Add(ddlSearch)
                            ElseIf DBColumnName = "Task" Then

                                Container.Controls.Add(New LiteralControl("<br />"))

                                    ddlSearch = New DropDownList
                                    ddlSearch.Items.Clear()

                                    ddlSearch.CssClass = "option-droup-multiSelection-Group"
                                    ddlSearch.Width = Unit.Percentage(92)
                                    ddlSearch.ClientIDMode = ClientIDMode.Static
                                    ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType
                                    ddlSearch.Attributes("multiple") = "multiple"
                                    Dim dt As New DataTable()
                                    Dim objCommon As New CCommon()
                                    objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
                                    objCommon.ListID = CCommon.ToLong(ListID)
                                    dt = objCommon.GetMasterListItemsWithRights()
                                    Dim item As ListItem = New ListItem("-- All --", "0")
                                    ddlSearch.Items.Add(item)
                                    ddlSearch.AutoPostBack = False
                                    For Each dr As DataRow In dt.Rows
                                        item = New ListItem(CCommon.ToString(dr("vcData")), CCommon.ToString(dr("numListItemID")))
                                    'item.Attributes("OptionGroup") = CCommon.ToString(dr("vcListItemGroupName"))
                                    'item.Attributes("class") = CCommon.ToString(dr("vcColorScheme"))
                                    ddlSearch.Items.Add(item)
                                    Next
                                    'ddlSearch.SelectedIndex = 0
                                    'ddlSearch.SelectionMode = ListSelectionMode.Multiple
                                    ddlSearch.ClearSelection()
                                    If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                        Dim stritemValues As String() = htGridColumnSearch(ddlSearch.ID).ToString.Split(",")
                                        If stritemValues.Length > 0 Then
                                            ddlSearch.ClearSelection()
                                        End If
                                        For Each indivItem As ListItem In ddlSearch.Items
                                            If stritemValues.Contains(indivItem.Value) Then
                                                indivItem.Selected = True
                                            End If
                                        Next
                                    End If
                                    Container.Controls.Add(ddlSearch)
                                ElseIf DBColumnName = "vcPerformance" Then
                                Container.Controls.Add(New LiteralControl("<br />"))
                                ddlSearch = New DropDownList

                                ddlSearch.Items.Clear()

                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ID = "cmp." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                ddlSearch.Items.Add(New ListItem("Last 3 MTD", "1"))
                                ddlSearch.Items.Add(New ListItem("Last 6 MTD", "2"))
                                ddlSearch.Items.Add(New ListItem("Last 1 YTD", "3"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If

                                Container.Controls.Add(ddlSearch)
                            ElseIf ControlType = "TextBox" Or ControlType = "CheckBox" Or ControlType = "CheckBoxList" Or ControlType = "TextArea" Then
                                Container.Controls.Add(New LiteralControl("<br/>"))
                                'txtSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType
                                txtSearch.ID = ColumnName & "~" & ControlType

                                txtSearch.CssClass = "signup"
                                txtSearch.Width = Unit.Percentage(92)
                                'txtSearch.AutoPostBack = True
                                txtSearch.Attributes.CssStyle.Add("width", "100%;")
                                If htGridColumnSearch.ContainsKey(txtSearch.ID) Then
                                    txtSearch.Text = htGridColumnSearch(txtSearch.ID).ToString
                                End If

                                Container.Controls.Add(txtSearch)
                            ElseIf ControlType = "SelectBox" Then
                                Dim dtData As DataTable
                                'DBColumnName, ListType, ListID
                                dtData = GetDropDownData()

                                Container.Controls.Add(New LiteralControl("<br/>"))
                                ddlSearch = New DropDownList

                                ddlSearch.Items.Clear()
                                'ddlSearch.AutoPostBack = True
                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ID = ColumnName & "~" & ControlType
                                'ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType
                                ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))

                                If dtData IsNot Nothing AndAlso dtData.Rows.Count > 0 Then

                                    For i As Integer = 0 To dtData.Rows.Count - 1
                                        ddlSearch.Items.Add(New ListItem(dtData.Rows(i)(1).ToString, dtData.Rows(i)(0).ToString))
                                    Next

                                    If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                        If ddlSearch.Items.FindByText(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                            ddlSearch.ClearSelection()
                                            ddlSearch.Items.FindByText(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                        ElseIf ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                            ddlSearch.ClearSelection()
                                            ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                        End If
                                    End If

                                End If

                                Container.Controls.Add(ddlSearch)
                            ElseIf ControlType = "DateField" Then
                                Container.Controls.Add(New LiteralControl("<br/>"))

                                Dim table As New System.Web.UI.HtmlControls.HtmlGenericControl("table")
                                Dim tr As New System.Web.UI.HtmlControls.HtmlGenericControl("tr")

                                Dim td1 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                Dim td2 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                td2.Style.Add("padding-left", "5px")

                                td1.InnerHtml = "From<br/>"
                                td2.InnerHtml = "To<br/>"

                                Dim dtFrom As New TextBox
                                dtFrom.ID = ColumnName & "~" & ControlType & "~From"
                                dtFrom.ClientIDMode = ClientIDMode.Static
                                dtFrom.Width = New Unit(80, UnitType.Pixel)
                                If htGridColumnSearch.ContainsKey(dtFrom.ID) Then
                                    dtFrom.Text = htGridColumnSearch(dtFrom.ID).ToString
                                End If
                                td1.Controls.Add(dtFrom)

                                Dim ajaxFrom As New AjaxControlToolkit.CalendarExtender()
                                ajaxFrom.TargetControlID = dtFrom.ID
                                ajaxFrom.Format = "MM/dd/yyyy"
                                td1.Controls.Add(ajaxFrom)

                                Dim dtTo As New TextBox
                                dtTo.ClientIDMode = ClientIDMode.Static
                                dtTo.ID = ColumnName & "~" & ControlType & "~To"
                                dtTo.Width = New Unit(80, UnitType.Pixel)
                                If htGridColumnSearch.ContainsKey(dtTo.ID) Then
                                    dtTo.Text = htGridColumnSearch(dtTo.ID).ToString
                                End If
                                td2.Controls.Add(dtTo)

                                Dim ajaxTo As New AjaxControlToolkit.CalendarExtender()
                                ajaxTo.TargetControlID = dtTo.ID
                                ajaxTo.Format = "MM/dd/yyyy"
                                td1.Controls.Add(ajaxTo)

                                tr.Controls.Add(td1)
                                tr.Controls.Add(td2)

                                table.Controls.Add(tr)

                                Container.Controls.Add(table)
                            End If
                        End If


                        If ColumnWidth > 0 Then
                            cell.Width = ColumnWidth
                        Else
                            cell.Width = 25
                        End If
                        cell.Attributes.Add("id", FormID & "~" & FormFieldId & "~" & Custom)
                    Case ListItemType.Item
                        If DBColumnName = "intTotalProgress" Then
                            AddHandler lbl1.DataBinding, AddressOf BindProgressBar ' make progressbar html
                            Container.Controls.Add(lbl1)

                        ElseIf ControlType <> "Button" AndAlso ControlType <> "DeleteCheckBox" Then
                            AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                            Container.Controls.Add(lbl1)

                        ElseIf ControlType = "DeleteCheckBox" Then
                            Dim chk As New CheckBox
                            chk.ID = "chkSelect"
                            chk.CssClass = "chkSelect"
                            'chk.AutoPostBack = True
                            ' AddHandler chk.CheckedChanged, AddressOf chk_CheckedChanged
                            Container.Controls.Add(chk)
                        Else
                            AddHandler lbl1.DataBinding, AddressOf Bindvalue
                            Container.Controls.Add(lbl1)

                        End If
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Function GetDropDownData() As DataTable
            Try
                Dim dtData As New DataTable
                Dim objCommon As New CCommon
                If DBColumnName = "tintSource" AndAlso LookBackTableName = "OpportunityMaster" Then
                    objCommon.DomainID = HttpContext.Current.Session("DomainID")
                    dtData = objCommon.GetOpportunitySource()
                ElseIf (DBColumnName = "numPartner" Or DBColumnName = "numPartenerSource") AndAlso LookBackTableName = "OpportunityMaster" Then
                    objCommon.DomainID = HttpContext.Current.Session("DomainID")
                    dtData = objCommon.GetPartnerSource()
                ElseIf DBColumnName = "numPartenerContact" AndAlso LookBackTableName = "OpportunityMaster" Then
                    objCommon.DomainID = HttpContext.Current.Session("DomainID")
                    dtData = objCommon.GetPartnerAllContacts()
                ElseIf DBColumnName = "numPartenerSource" AndAlso LookBackTableName = "DivisionMaster" Then
                    objCommon.DomainID = HttpContext.Current.Session("DomainID")
                    dtData = objCommon.GetPartnerSource()
                ElseIf DBColumnName = "numAssignedTo" Or DBColumnName = "numAssignedBy" Then
                    dtData = objCommon.ConEmpList(CCommon.ToLong(HttpContext.Current.Session("DomainID")), False, 0)
                ElseIf ListID > 0 Then
                    dtData = objCommon.GetMasterListItems(ListID, CCommon.ToLong(HttpContext.Current.Session("DomainID")))
                    If DBColumnName = "Task" Then
                        Dim drTask As DataRow
                        drTask = dtData.NewRow
                        drTask("numListItemId") = "0"
                        drTask("vcData") = "Calendar"
                        dtData.Rows.Add(drTask)
                    End If
                ElseIf ListType.Trim() = "U" Then
                    dtData = objCommon.ConEmpList(CCommon.ToLong(HttpContext.Current.Session("DomainID")), False, 0)
                ElseIf ListType.Trim() = "AG" Then
                    dtData = objCommon.GetGroups()
                ElseIf ListType.Trim() = "C" Then
                    Dim objCampaign As New BACRM.BusinessLogic.Reports.PredefinedReports
                    objCampaign.byteMode = 2
                    objCampaign.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    dtData = objCampaign.GetCampaign()
                ElseIf ListType.Trim() = "DC" Then
                    Dim objCampaign As New BACRM.BusinessLogic.Marketing.Campaign
                    With objCampaign
                        .SortCharacter = "0"
                        .UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                        .PageSize = 100
                        .TotalRecords = 0
                        .DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        .columnSortOrder = "Asc"
                        .CurrentPage = 1
                        .columnName = "vcECampName"
                        dtData = objCampaign.ECampaignList
                        Dim drow As DataRow = dtData.NewRow
                        drow("numECampaignID") = -1
                        drow("vcECampName") = "-- Disengaged --"
                        dtData.Rows.Add(drow)
                    End With
                ElseIf ListType.Trim() = "SYS" Then
                    dtData = New DataTable
                    dtData.Columns.Add("Value")
                    dtData.Columns.Add("Text")
                    Dim dr1 As DataRow
                    dr1 = dtData.NewRow
                    dr1("Value") = "0"
                    dr1("Text") = "Lead"
                    dtData.Rows.Add(dr1)

                    dr1 = dtData.NewRow
                    dr1("Value") = "1"
                    dr1("Text") = "Prospect"
                    dtData.Rows.Add(dr1)

                    dr1 = dtData.NewRow
                    dr1("Value") = "2"
                    dr1("Text") = "Account"
                    dtData.Rows.Add(dr1)
                ElseIf DBColumnName = "numManagerID" Then
                    objCommon.ContactID = objCommon.ContactID
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    dtData = objCommon.GetManagers(CCommon.ToLong(HttpContext.Current.Session("DomainID")), objCommon.ContactID, objCommon.DivisionID)

                ElseIf DBColumnName = "vcInventoryStatus" Then
                    dtData = objCommon.GetInventoryStatus()

                ElseIf DBColumnName = "charSex" Then
                    dtData = New DataTable
                    dtData.Columns.Add("Value")
                    dtData.Columns.Add("Text")
                    Dim dr1 As DataRow
                    dr1 = dtData.NewRow
                    dr1("Value") = "M"
                    dr1("Text") = "Male"
                    dtData.Rows.Add(dr1)

                    dr1 = dtData.NewRow
                    dr1("Value") = "F"
                    dr1("Text") = "Female"
                    dtData.Rows.Add(dr1)
                ElseIf DBColumnName = "tintOppStatus" Then
                    dtData = New DataTable
                    dtData.Columns.Add("numListItemID")
                    dtData.Columns.Add("vcData")

                    Dim row As DataRow = dtData.NewRow
                    row("numListItemID") = 1
                    row("vcData") = "Deal Won"
                    dtData.Rows.Add(row)

                    row = dtData.NewRow
                    row("numListItemID") = 2
                    row("vcData") = "Deal Lost"
                    dtData.Rows.Add(row)
                    dtData.AcceptChanges()
                ElseIf ListType.Trim() = "WI" Then
                    dtData = New DataTable
                    dtData.Columns.Add("numListItemID", System.Type.GetType("System.String"))
                    dtData.Columns.Add("vcData", System.Type.GetType("System.String"))

                    Dim objAPI As New WebAPI
                    Dim dtWebApi As DataTable
                    objAPI.Mode = 1
                    dtWebApi = objAPI.GetWebApi

                    Dim row As DataRow = dtData.NewRow

                    For Each dr As DataRow In dtWebApi.Rows
                        row = dtData.NewRow
                        row(0) = dr("WebApiID")
                        row(1) = dr("vcProviderName")
                        dtData.Rows.Add(row)
                    Next

                    'If FormID = 38 Or FormID = 39 Or FormID = 40 Or FormID = 41 Then
                    Dim objSite As New Sites
                    Dim dtSites As DataTable
                    objSite.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    dtSites = objSite.GetSites()

                    For Each dr As DataRow In dtSites.Rows
                        row = dtData.NewRow
                        row(0) = "Sites~" & dr("numSiteID")
                        row(1) = dr("vcSiteName")
                        dtData.Rows.Add(row)
                    Next

                    'End If

                    dtData.AcceptChanges()
                End If

                Return dtData
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Sub BindProgressBar(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                Dim i As Integer = 0
                i = Container.RowIndex

                lbl1.Text = CType(Container.DataItem, DataRowView).Row(DBColumnName) & "% <div id=""TotalProgressContainer" & i.ToString & """ class=""ProgressContainer"">" &
                            "&nbsp;<div id=""TotalProgress" & i.ToString & """ class=""ProgressBar"" ></div></div>"


                ScriptManager.RegisterStartupScript(Container.Page, Me.GetType, "ProgressBar" & i.ToString, "progress(" & CType(Container.DataItem, DataRowView).Row(DBColumnName) & ",'TotalProgressContainer" & i.ToString & "','TotalProgress" & i.ToString & "',90);", True)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub Bindvalue(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)

                Dim btnDeleteAction As New Button
                Dim lnkDeleteAction As New LinkButton

                btnDeleteAction.Text = "X"
                btnDeleteAction.ID = "btnDeleteAction"
                btnDeleteAction.CssClass = "button Delete"
                btnDeleteAction.CommandName = "Delete"

                lnkDeleteAction.Text = "<font color=""#730000"">*</font>"
                lnkDeleteAction.ID = "lnkDeleteAction"
                lnkDeleteAction.Visible = False

                Dim lngCommID, lngCaseId, lngCasetimeId, lngCaseExpId, lngType As Long
                Dim HtmlLink As String
                Dim OrignalDesc As String = ""

                lngCaseId = DataBinder.Eval(Container.DataItem, "CaseId")
                lngCommID = DataBinder.Eval(Container.DataItem, "ID")
                lngCasetimeId = DataBinder.Eval(Container.DataItem, "CaseTimeId")
                lngCaseExpId = DataBinder.Eval(Container.DataItem, "CaseExpId")
                lngType = DataBinder.Eval(Container.DataItem, "type")
                OrignalDesc = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "OrignalDescription")), "", DataBinder.Eval(Container.DataItem, "OrignalDescription"))

                HtmlLink = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "HtmlLink")), "", DataBinder.Eval(Container.DataItem, "HtmlLink"))


                lnkDeleteAction.Attributes.Add("onclick", "return DeleteMessage()")

                If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 0 Then
                    btnDeleteAction.Visible = False
                    lnkDeleteAction.Visible = True
                    lnkDeleteAction.Attributes.Add("onclick", "return DeleteMessage()")
                ElseIf m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 1 Then
                    Try
                        If DataBinder.Eval(Container.DataItem, "numCreatedBy") = HttpContext.Current.Session("UserContactID") Then
                            btnDeleteAction.Attributes.Add("onclick", "return DeleteRecord()")
                        Else
                            btnDeleteAction.Visible = False
                            lnkDeleteAction.Visible = True
                            lnkDeleteAction.Attributes.Add("onclick", "return DeleteMessage()")
                        End If
                    Catch ex As Exception
                        Throw ex
                    End Try
                ElseIf m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 2 Then
                    Try
                        Dim i As Integer
                        Dim dtTerritory As DataTable
                        dtTerritory = HttpContext.Current.Session("UserTerritory")
                        If DataBinder.Eval(Container.DataItem, "numOrgTerId") = 0 Then
                            btnDeleteAction.Attributes.Add("onclick", "return DeleteRecord()")
                        Else
                            Dim chkDelete As Boolean = False
                            For i = 0 To dtTerritory.Rows.Count - 1
                                If DataBinder.Eval(Container.DataItem, "numOrgTerId") = dtTerritory.Rows(i).Item("numTerritoryId") Then chkDelete = True
                            Next
                            If chkDelete = True Then
                                btnDeleteAction.Attributes.Add("onclick", "return DeleteRecord()")
                            Else
                                btnDeleteAction.Visible = False
                                lnkDeleteAction.Visible = True
                                lnkDeleteAction.Attributes.Add("onclick", "return DeleteMessage()")
                            End If
                        End If
                    Catch ex As Exception
                        Throw ex
                    End Try
                ElseIf m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 3 Then
                    btnDeleteAction.Attributes.Add("onclick", "return DeleteRecord()")
                End If


                If lngCasetimeId = 0 And lngCaseExpId = 0 And lngCaseId = 0 And (lngType = 3 Or lngType = 2 Or lngType = 4) Then
                    lnkDeleteAction.Visible = True
                    btnDeleteAction.Visible = False
                End If

                Dim cell As DataControlFieldCell = CType(lbl1.Parent, DataControlFieldCell)

                lbl1.Controls.Add(lnkDeleteAction)
                lbl1.Controls.Add(btnDeleteAction)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim ControlClass As String = ""

                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                Dim cell As DataControlFieldCell = CType(lbl1.Parent, DataControlFieldCell)

                'Change color of Row
                If DirectCast(Container.DataItem, System.Data.DataRowView).DataView.Table.Columns("vcColorScheme") IsNot Nothing Then
                    If DataBinder.Eval(Container.DataItem, "vcColorScheme").ToString.Length > 0 Then
                        Container.CssClass = DataBinder.Eval(Container.DataItem, "vcColorScheme")
                    End If
                End If

                Dim lngCommID, lngCaseId, lngCasetimeId, lngCaseExpId, lngType As Long
                Dim HtmlLink As String
                Dim OrignalDesc As String = ""

                lngCaseId = DataBinder.Eval(Container.DataItem, "CaseId")
                lngCommID = DataBinder.Eval(Container.DataItem, "ID")
                lngCasetimeId = DataBinder.Eval(Container.DataItem, "CaseTimeId")
                lngCaseExpId = DataBinder.Eval(Container.DataItem, "CaseExpId")
                lngType = DataBinder.Eval(Container.DataItem, "type")


                HtmlLink = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "HtmlLink")), "", DataBinder.Eval(Container.DataItem, "HtmlLink"))
                OrignalDesc = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "OrignalDescription")), "", DataBinder.Eval(Container.DataItem, "OrignalDescription"))


                If DBColumnName = "vcEmail" Or DBColumnName = "vcCompanyName" Or DBColumnName = "vcFirstName" Or DBColumnName = "vcLastName" Or DBColumnName = "Task" Or DBColumnName = "Activity" Or DBColumnName = "Status" Or DBColumnName = "vcPoppName" Then

                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, DBColumnName)), "", DataBinder.Eval(Container.DataItem, DBColumnName))

                    Dim intermediatory As Integer
                    intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)
                    If DBColumnName = "vcCompanyName" Then
                        If CCommon.ToShort(DataBinder.Eval(Container.DataItem, "tintCRMType")) = 0 Then
                            lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/Leads/frmLeads.aspx?DivID=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numDivisionID"))) & "'>" & lbl1.Text & "</a>"
                        ElseIf CCommon.ToShort(DataBinder.Eval(Container.DataItem, "tintCRMType")) = 1 Then
                            lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/prospects/frmProspects.aspx?DivID=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numDivisionID"))) & "'>" & lbl1.Text & "</a>"
                        ElseIf CCommon.ToShort(DataBinder.Eval(Container.DataItem, "tintCRMType")) = 2 Then
                            lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/account/frmAccounts.aspx?DivID=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numDivisionID"))) & "'>" & lbl1.Text & "</a>"
                        End If
                    ElseIf DBColumnName = "vcPoppName" Then
                        lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/opportunity/frmOpportunities.aspx?frm=deallist&OpID=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numOppID"))) & "'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                    ElseIf DBColumnName = "vcFirstName" Or DBColumnName = "vcLastName" Then
                        lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/contact/frmContacts.aspx?CntId=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numContactID"))) & "'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                    ElseIf DBColumnName = "vcEmail" Then

                        If DataBinder.Eval(Container.DataItem, "type") = "0" Or DataBinder.Eval(Container.DataItem, "type") = "-1" Then
                            lbl1.Attributes.Add("onclick", "return OpemEmail('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=" & IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "vcEmail")), "", DataBinder.Eval(Container.DataItem, "vcEmail")) & "&CommID=" & DataBinder.Eval(Container.DataItem, "ID") & "')")
                        Else : lbl1.Attributes.Add("onclick", "return OpemEmail('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=" & IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "vcEmail")), "", DataBinder.Eval(Container.DataItem, "vcEmail")) & "')")
                        End If

                        lbl1.Text = "<a href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                    ElseIf DBColumnName = "Task" Or DBColumnName = "Status" Or DBColumnName = "Activity" Then


                        Dim strOnClick As String = ""
                        If lngCasetimeId = 0 And lngCaseExpId = 0 And lngCaseId = 0 And lngType = "3" Then
                            strOnClick = "return OpenBizDocActionItem('" & lngCommID & "','" & DataBinder.Eval(Container.DataItem, "DueDate") & "','" & DataBinder.Eval(Container.DataItem, "numContactID") & "')"
                        ElseIf lngCasetimeId = 0 And lngCaseExpId = 0 And lngCaseId = 0 And lngType = "2" Then
                            strOnClick = "return OpenDocument('" & lngCommID & "')"
                        ElseIf lngCasetimeId = 0 And lngCaseExpId = 0 And lngCaseId = 0 And lngType = "0" Or lngType = "1" Then
                            strOnClick = "return OpenActionItem('" & lngCommID & "','" & lngType & "')"
                        Else
                            strOnClick = "return OpenCalItem()"
                        End If

                        If strOnClick.Contains("OpenActionItem(") Then
                            If DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "communication" Then
                                lbl1.Text = "<a href='../admin/actionitemdetailsold.aspx?CommId=" & lngCommID & "&lngType=" & lngType & "' style='color:brown'>" & lbl1.Text & "</a>"
                            ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "task" Then
                                lbl1.Text = "<a href='../admin/actionitemdetailsold.aspx?CommId=" & lngCommID & "&lngType=" & lngType & "' style='color:purple'>" & lbl1.Text & "</a>"
                            ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.IndexOf("Approval Request") > -1 Then
                                lbl1.Text = "<a href='../admin/actionitemdetailsold.aspx?CommId=" & lngCommID & "&lngType=" & lngType & "' style='color:red'>" & lbl1.Text & "</a>"
                            ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "follow-up anytime" Then
                                lbl1.Text = "<a href='../admin/actionitemdetailsold.aspx?CommId=" & lngCommID & "&lngType=" & lngType & "' style='color:#000000'>" & lbl1.Text & "</a>"
                            ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "calendar" Then
                                lbl1.Text = "<a href='../admin/actionitemdetailsold.aspx?CommId=" & lngCommID & "&lngType=" & lngType & "' style='color:green'>" & lbl1.Text & "</a>&nbsp;&nbsp;<a href='#' onclick="" return OpenCalItem('" & HtmlLink & "')"" style='color:green'><img src='../images/GCalendar.png' width='15px'/></a>"
                            ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "phone call" Then
                                lbl1.Text = "<a href='../admin/actionitemdetailsold.aspx?CommId=" & lngCommID & "&lngType=" & lngType & "' style='color:orange'>" & lbl1.Text & "</a>"
                            ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "unknown" Then
                                lbl1.Text = "<a href='../admin/actionitemdetailsold.aspx?CommId=" & lngCommID & "&lngType=" & lngType & "' style='color:#000000'>" & lbl1.Text & "</a>"
                            Else
                                lbl1.Text = "<a href='../admin/actionitemdetailsold.aspx?CommId=" & lngCommID & "&lngType=" & lngType & "'>" & lbl1.Text & "</a>"
                            End If
                        Else
                            If DBColumnName = "Task" Then
                                If DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "communication" Then
                                    lbl1.Text = "<a href='#' onclick=""" & strOnClick & """ style='color:brown'>" & lbl1.Text & "</a>"
                                ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "task" Then
                                    lbl1.Text = "<a href='#' onclick=""" & strOnClick & """ style='color:purple'>" & lbl1.Text & "</a>"
                                ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.IndexOf("Approval Request") > -1 Then
                                    lbl1.Text = "<a href='#' onclick=""" & strOnClick & """ style='color:red'>" & lbl1.Text & "</a>"
                                ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "follow-up anytime" Then
                                    lbl1.Text = "<a href='#' onclick=""" & strOnClick & """ style='color:#000000'>" & lbl1.Text & "</a>"
                                ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "calendar" Then
                                    lbl1.Text = "<a href='#' onclick=""" & strOnClick & """ style='color:green'>" & lbl1.Text & "</a>&nbsp;&nbsp;<a href='#' onclick="" return OpenCalItem('" & HtmlLink & "')"" style='color:green'><img src='../images/GCalendar.png' width='15px'/></a>"
                                ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "phone call" Then
                                    lbl1.Text = "<a href='#' onclick=""" & strOnClick & """ style='color:orange'>" & lbl1.Text & "</a>"
                                ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).ToString.ToLower = "unknown" Then
                                    lbl1.Text = "<a href='#' onclick=""" & strOnClick & """ style='color:#000000'>" & lbl1.Text & "</a>"
                                Else
                                    lbl1.Text = "<a href='#' onclick=""" & strOnClick & """>" & lbl1.Text & "</a>"
                                End If

                            ElseIf DBColumnName = "Status" Then
                                If DataBinder.Eval(Container.DataItem, DBColumnName) IsNot Nothing AndAlso DataBinder.Eval(Container.DataItem, DBColumnName).ToString.Length > 0 Then
                                    If DataBinder.Eval(Container.DataItem, DBColumnName).Trim().ToLower = "high" Then
                                        lbl1.Text = "<a href='#' onclick=""" & strOnClick & """ style='color:red'>" & lbl1.Text & "</a>"
                                    ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).Trim().ToLower = "normal" Then
                                        lbl1.Text = "<a href='#' onclick=""" & strOnClick & """ style='color:#000000'>" & lbl1.Text & "</a>"
                                    ElseIf DataBinder.Eval(Container.DataItem, DBColumnName).Trim().ToLower = "critical" Then
                                        lbl1.Text = "<a href='#' onclick=""" & strOnClick & """ style='color:orange'>" & lbl1.Text & "</a>"
                                    Else
                                        lbl1.Text = "<a href='#' onclick=""" & strOnClick & """>" & lbl1.Text & "</a>"
                                    End If
                                End If
                            Else
                                lbl1.Text = "<a href='#' onclick=""" & strOnClick & """>" & lbl1.Text & "</a>"
                            End If
                        End If
                    End If
                Else
                    If ControlType = "CheckBox" Then
                        lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, DBColumnName)), "", IIf(DataBinder.Eval(Container.DataItem, DBColumnName), "Yes", "No"))
                    Else
                        lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, DBColumnName)), "", DataBinder.Eval(Container.DataItem, DBColumnName))
                    End If
                End If

                If DBColumnName = "Action-Item Participants" AndAlso lbl1.Text IsNot Nothing Then
                    lbl1.Text = lbl1.Text.Replace("^", "  ")
                End If

                If DBColumnName = "itemDesc" Then
                    If lngType = 1 AndAlso lbl1.Text IsNot Nothing Then
                        Dim Description As String = ParseHtml(OrignalDesc)
                        lbl1.Text = lbl1.Text.Replace("$", Description)
                    Else
                        lbl1.Text = lbl1.Text.TrimStart().TrimEnd()
                    End If

                End If
                If lngType = 0 Then
                    If AllowEdit = True And System.Web.HttpContext.Current.Session("InlineEdit") = True Then
                        Dim allowInlineEdit As Boolean = False

                        If EditPermission = 1 AndAlso CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numCreatedBy")) = CCommon.ToLong(HttpContext.Current.Session("UserContactID")) Then
                            allowInlineEdit = True
                        ElseIf EditPermission = 2 Then
                            Dim i As Integer
                            Dim dtTerritory As DataTable
                            dtTerritory = HttpContext.Current.Session("UserTerritory")
                            If CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numOrgTerId")) = 0 Then
                                allowInlineEdit = False
                            Else
                                Dim chkDelete As Boolean = False
                                For i = 0 To dtTerritory.Rows.Count - 1
                                    If CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numOrgTerId")) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                        chkDelete = True
                                        allowInlineEdit = True
                                    End If
                                Next
                                If chkDelete = False Then
                                    allowInlineEdit = False
                                End If
                            End If
                        ElseIf EditPermission = 3 Then
                            allowInlineEdit = True
                        End If

                        If allowInlineEdit Then
                            Select Case ControlType
                                Case "Website", "Email", "TextBox"
                                    ControlClass = "click"
                                Case "SelectBox"
                                    ControlClass = "editable_select"
                                Case "TextArea"
                                    ControlClass = "editable_textarea"
                                Case "CheckBox"
                                    ControlClass = "editable_CheckBox"
                                Case "DateField"
                                    ControlClass = ""
                            End Select

                            If ControlClass.Length > 0 Then
                                cell.Attributes.Add("id", "Tickler~" & FormFieldId & "~" & Custom & "~" & lngType & "~" & lngCommID)
                                cell.Attributes.Add("class", ControlClass)

                                cell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                                cell.Attributes.Add("onmouseout", "bgColor=''")
                            End If
                        End If
                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Function ParseHtml(ByVal htmlToParse As String) As String
            Dim Spantext As New StringBuilder()
            Dim ElementToParse As String = "span"
            Dim HtmlDocument As New HtmlDocument()
            HtmlDocument.LoadHtml(htmlToParse)
            For Each htmlNode As HtmlNode In HtmlDocument.DocumentNode.Descendants()
                If htmlNode.Name.Equals(ElementToParse, StringComparison.InvariantCultureIgnoreCase) Then
                    If (Spantext.Length = 0) Then
                        Spantext = Spantext.Append(htmlNode.InnerText)
                    Else
                        Spantext = Spantext.Append("<br>" + htmlNode.InnerText)
                    End If
                End If
            Next
            Return Spantext.ToString()
        End Function

    End Class

End Namespace