Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Public Class frmContractList
    Inherits BACRMPage

    Dim strColumn As String
    Dim lngContractId As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            GetUserRightsForPage(34, 1)

            If Not IsPostBack Then

                Page.Master.Master.FindControl("MainContent").FindControl("tdGridConfiguration").Visible = False

                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                    txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                    txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                    txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                    If Not ddlSort.Items.FindByValue(PersistTable(PersistKey.FilterBy)) Is Nothing Then
                        ddlSort.Items.FindByValue(PersistTable(PersistKey.FilterBy)).Selected = True
                    End If
                End If
                If GetQueryStringVal("srt") <> "" Then
                    ddlSort.ClearSelection()
                    ddlSort.Items.FindByValue(GetQueryStringVal("srt")).Selected = True
                End If
                BindDatagrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            Dim dtContacts As DataTable
            Dim objContract As New CContracts


            With objContract
                .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                .UserCntID = Session("UserContactID")
                .SortOrder = ddlSort.SelectedItem.Value
                .DomainID = Session("DomainID")
                .SortCharacter = txtSortChar.Text.Trim()

                If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                .CurrentPage = txtCurrrentPage.Text.Trim()
                .PageSize = Session("PagingRows")
                .TotalRecords = 0

                If txtSortColumn.Text <> "" Then
                    .columnName = txtSortColumn.Text
                Else : .columnName = "bintCreatedOn"
                End If

                If txtSortOrder.Text = "D" Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If
            End With

            dtContacts = objContract.GetContactList
            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objContract.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text.Trim()
            'If objContract.TotalRecords = 0 Then
            '    hidenav.Visible = False
            '    lblRecordCount.Text = 0
            'Else
            '    hidenav.Visible = True
            '    lblRecordCount.Text = String.Format("{0:#,###}", objContract.TotalRecords)
            '    Dim strTotalPage As String()
            '    Dim decTotalPage As Decimal
            '    decTotalPage = lblRecordCount.Text / Session("PagingRows")
            '    decTotalPage = Math.Round(decTotalPage, 2)
            '    strTotalPage = CStr(decTotalPage).Split(".")
            '    If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
            '        lblTotal.Text = strTotalPage(0)
            '        txtTotalPage.Text = strTotalPage(0)
            '    Else
            '        lblTotal.Text = strTotalPage(0) + 1
            '        txtTotalPage.Text = strTotalPage(0) + 1
            '    End If
            '    txtTotalRecords.Text = lblRecordCount.Text
            'End If
            dgContracts.DataSource = dtContacts
            dgContracts.DataBind()

            PersistTable.Clear()
            PersistTable.Add(PersistKey.CurrentPage, IIf(dtContacts.Rows.Count > 0, txtCurrrentPage.Text, "1"))
            PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
            PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
            PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
            'PersistTable.Add(PersistKey.OrgName, txtCustomer.Text.Trim())
            'PersistTable.Add(PersistKey.FirstName, txtFirstName.Text.Trim())
            'PersistTable.Add(PersistKey.LastName, txtLastName.Text.Trim())
            PersistTable.Add(PersistKey.FilterBy, ddlSort.SelectedValue)
            'PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
            PersistTable.Save()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



    Private Sub dgContracts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContracts.ItemCommand
        Try
            If Not e.CommandName = "Sort" Then lngContractId = e.Item.Cells(0).Text()
            If e.CommandName = "Name" Then
                Response.Redirect("../ContractManagement/frmContract.aspx?frm=ContractList&contractId=" & CStr(lngContractId), False)
            ElseIf e.CommandName = "Delete" Then
                Dim objContract As New CContracts
                objContract.ContractID = lngContractId
                objContract.DomainID = Session("DomainId")
                objContract.DeleteContract()
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgContracts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContracts.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False
                    lnkDelete.Visible = True
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgContracts_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgContracts.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If txtSortColumn.Text <> strColumn Then
                txtSortColumn.Text = strColumn
                'Session("Asc") = 0
                txtSortOrder.Text = "D"
            Else
                If txtSortOrder.Text = "A" Then
                    txtSortOrder.Text = "D"
                Else : txtSortOrder.Text = "A"
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub ddlSort_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSort.SelectedIndexChanged
        Try
            If (ddlSort.SelectedValue = "4") Then
                txtSortColumn.Text = "datediff(day,getdate(),c.bintExpDate)"
                Session("Asc") = 0
            Else
                txtSortColumn.Text = Nothing
                Session("Asc") = Nothing
            End If

            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

End Class