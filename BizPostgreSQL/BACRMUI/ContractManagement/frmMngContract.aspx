﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMngContract.aspx.vb"
    Inherits=".frmMngContract"  MasterPageFile="~/common/PopupBootstrap.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../ContractManagement/ContractsLog.ascx" TagName="ContractsLog" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link href="../JavaScript/MultiSelect/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../JavaScript/MultiSelect/bootstrap-multiselect.js"></script>
    <script language="javascript" type="text/javascript">
        
        $(document).ready(function () {
            $('.option-droup-multiSelection-Group').multiselect({
                enableClickableOptGroups: true,
                onSelectAll: function () {
                    console.log("select-all-nonreq");
                },
                optionClass: function (element) {
                    var value = $(element).attr("class");
                    return value;
                }
            });
        })

        function Close() {
            window.close();
        }
        function Save() {
            if ($("#lblType").text() == "Time") {
                if ($find('radCmbCompany').get_value() == "") {
                    alert("Select Company")
                    return false;
                }
                if ($("#<%=txtHours.ClientID%>").val() == "") {
                    alert("Please enter no of hours");
                    $("#<%=txtHours.ClientID%>").focus();
                    return false;
                }
                if (parseInt($("#<%=txtHours.ClientID%>").val()) < 1) {
                    alert("Please enter no of hours greater than 0");
                    $("#<%=txtHours.ClientID%>").focus();
                    return false;
                }
            } else if ($("#lblType").text() == "Incident") {
                if ($find('radCmbCompany').get_value() == "") {
                    alert("Select Company")
                    return false;
                }
                if ($("#<%=txtIncidents.ClientID%>").val() == "") {
                    alert("Please enter no of incidents");
                    $("#<%=txtIncidents.ClientID%>").focus();
                    return false;
                }
                if (parseInt($("#<%=txtIncidents.ClientID%>").val()) < 1 || parseInt($("#<%=txtIncidents.ClientID%>").val()) > 999) {
                    alert("Please enter no of incidents between 1 to 999");
                    $("#<%=txtIncidents.ClientID%>").focus();
                    return false;
                }
            } else {
                if ($("#<%=ddlItemClassification.ClientID%>").val() == null) {
                    alert("Please select atleast one Item Classification");
                    $("#<%=ddlItemClassification.ClientID%>").focus();
                    return false;
                }
                if ($("#<%=txtWarrantyDays.ClientID%>").val() == "") {
                    alert("Please enter Warranty Days");
                    $("#<%=txtWarrantyDays.ClientID%>").focus();
                    return false;
                }
                if (parseInt($("#<%=txtWarrantyDays.ClientID%>").val()) < 1 || parseInt($("#<%=txtWarrantyDays.ClientID%>").val()) > 3650) {
                    alert("Please enter Warranty Days between 1 to 3650");
                    $("#<%=txtWarrantyDays.ClientID%>").focus();
                    return false;
                }
                if ($("#hdnSelectedItems").val() == "" || $("#hdnSelectedItems").val() == "0") {
                    $("#hdnSelectedItems").val($("#<%=ddlItemClassification.ClientID%>").val().toString());
                }
            }
        }
    </script>
    <style type="text/css">
        #tblContract {
            border-spacing: 3px;
        }
        .table > tbody > tr > td{
            border-top:0px !important;
        }

        .tdLabel {
            padding-top: 3px;
            padding-right: 5px;
             background-color:#e5e5e5;
        }

        .tdField {
            padding-top: 3px;
            background-color: #f2f2f2;
            padding-left: 5px;
            padding-right: 5px;
        }

        #tblContract td:nth-child(1) {
            text-align: right;
            font-weight: bold;
        }
        .lblHeighlight {
    color: #FF9933;
    border-color: #FF9933;
    padding: 1px 10px;
    border: 2px solid;
    font-weight: bold;
    font-size: 18px;
}
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="pull-right">
            <asp:Button ID="btnSaveClose" OnClientClick="return Save();" runat="server" Text="Save &amp; Close" CssClass="btn btn-primary"
                ></asp:Button>
            <asp:Button ID="btnClose" runat="server" OnClientClick="Close()" CssClass="btn btn-primary" Text="Close" ></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblType" runat="server" Text="Label"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('contractmanagement/frmmngcontract.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    
    <div class="col-md-2">
        <h3>
            <asp:Label ID="lblContractNo" runat="server" Text=""></asp:Label></h3>
        <img src="../images/contract.png" />
    </div>
    <div class="col-md-10">
        <div class="panel panel-default">
        <div class="panel-body">
            <table class="table">
                <tr id="divCreatedSection" runat="server">
                    <td colspan="7">
                        <div class="pull-right">
                            <div class="form-inline">
                                <label>Created By : </label>
                                <asp:Label ID="lblCreatedBy" runat="server" Text="-"></asp:Label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 <label>Created On : </label>
                                <asp:Label ID="lblCreatedOn" runat="server" Text="-"></asp:Label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 <label>Last Updated On : </label>
                                <asp:Label ID="lblUpdatedOn" runat="server" Text="-"></asp:Label>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr id="contractWarrantyPRow" runat="server">
                    <td>Organization</td>
                    <td> <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="250" DropDownWidth="600px"
                            OnClientItemsRequested="OnClientItemsRequestedOrganization"
                            ClientIDMode="Static"
                            ShowMoreResultsBox="true"
                            Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                        </telerik:RadComboBox>
                    </td>
                    <td id="tdIncident1" runat="server">
                      <a href="#" title="Incident contracts reference whatever organization they’re assigned to. Once populated with a value from 1 to 999, the number of indicants remaining will display within case records, but only for full user employees enabled from within Administration | General settings.
Each case that’s created will decrement an incident by 1.
">[?]</a>  Incidents
                    </td>
                    <td id="tdIncident2" runat="server">
                        <asp:TextBox ID="txtIncidents" CssClass="form-control" runat="server"></asp:TextBox>
                    </td>
                    <td id="tdIncident3" runat="server">
                        <b>Incidents Left</b>
                    </td>
                    <td id="tdIncident4" runat="server">
                        <asp:Label ID="lblIncidentLeft" CssClass="lblHeighlight" runat="server" Text="0"></asp:Label>
                    </td>
                </tr>
                <tr  id="contractWarrantySRow" runat="server">
                    <td>
                      <a href="#" title="When populated with hours and/or minutes, time will display within action item and project records if enabled from global settings for the user logged in.. Time can be deducted or added from any activity or meeting that includes a start and end time, including action items (includes calendar entries from Google G-Suite or Office 365 connected to BizAutomation) or from time completed within Project Tasks.
">[?]</a>  <b>Time</b>&nbsp;<i>Hours</i>
                    </td>
                    <td>
                        <asp:TextBox ID="txtHours" CssClass="form-control" value="0" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <i>Minutes</i>
                    </td>
                    <td>
                        <asp:DropDownList ID="txtMinutes" runat="server" CssClass="form-control">
                            <asp:ListItem Text="00" Value="0"></asp:ListItem>
                            <asp:ListItem Text="15" Value="15"></asp:ListItem>
                            <asp:ListItem Text="30" Value="30"></asp:ListItem>
                            <asp:ListItem Text="45" Value="45"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <b>Time Left</b>
                    </td>
                    <td>
                        <asp:Label ID="lblTimeLeft" CssClass="lblHeighlight" runat="server" Text="0"></asp:Label>
                    </td>
                </tr>
                <tr id="Warranty1Row" runat="server">
                    <td><b>Item Classification</b></td>
                    <td>
                        <asp:DropDownList ID="ddlItemClassification" CssClass="form-control option-droup-multiSelection-Group" multiple="multiple" runat="server"></asp:DropDownList>
                        <asp:HiddenField ID="hdnSelectedItems" Value="0" runat="server" />
                    </td>
                    <td>
                       <a href="#" title="Warrantees are always tied to sales orders. They begin counting down when a fulfillment order is added to sales orders (i.e. when the order is shipped). They display within Sales Orders, Sales Returns (based on selection of a sales order) and Cases (but only after the Case references a sales order | item) for full user employees enabled from within Administration | General settings.
">[?]</a> <b>Warranty</b><br />
                        <i>Duration(days)</i>
                    </td>
                    <td>
                        <asp:TextBox ID="txtWarrantyDays" CssClass="form-control" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr id="Warranty2Row" runat="server">
                    <td><a href="#" title="Notes added here (256 characters max) will display in the tool-tip next to the Warrantee alert label in Sales Orders, Sales Returns (based on selection of a sales order) and Cases (but only after the Case references a sales order | item) for full user employees enabled from within Administration | General settings.
">[?]</a> Notes</td>
                    <td>
                        <asp:TextBox ID="txtNotes" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>

            <div id="divContractsLog" runat="server">
                <uc1:ContractsLog ID="ContractsLog" runat="server" />
            </div>
        </div>
    </div>
    </div>
</asp:Content>