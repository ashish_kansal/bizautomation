Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common

Partial Public Class frmAddContract
    Inherits BACRMPage

    Dim ContractId As Integer
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                
                radCmbCompany.Focus()
                txtInci.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtHrs.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                btnSaveClose.Attributes.Add("onclick", "return Save()")
                btnClose.Attributes.Add("onclick", "Close()")

                If GetQueryStringVal( "uihTR") <> "" Or GetQueryStringVal( "fghTY") <> "" Or GetQueryStringVal( "rtyWR") <> "" Or GetQueryStringVal( "tyrCV") <> "" Or GetQueryStringVal( "pluYR") <> "" Then
                    If GetQueryStringVal( "uihTR") <> "" Then
                        objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                        objCommon.charModule = "C"
                    ElseIf GetQueryStringVal( "rtyWR") <> "" Then
                        objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                        objCommon.charModule = "D"
                    ElseIf GetQueryStringVal( "tyrCV") <> "" Then
                        objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                        objCommon.charModule = "P"
                    ElseIf GetQueryStringVal( "pluYR") <> "" Then
                        objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                        objCommon.charModule = "O"
                    ElseIf GetQueryStringVal( "fghTY") <> "" Then
                        objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                        objCommon.charModule = "S"
                    End If

                    objCommon.GetCompanySpecificValues1()
                    Dim strCompany As String
                    strCompany = objCommon.GetCompanyName
                    radCmbCompany.Text = strCompany
                    radCmbCompany.SelectedValue = objCommon.DivisionID
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            saveContract()
            fncPageRedirect()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub fncPageRedirect()
        Try
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "window.opener.reDirectPage('../ContractManagement/frmContract.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ContractList&contractId=" & ContractId & "'); self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub saveContract()
        Try
            Dim objContract As New CContracts
            With objContract
                .DivisionId = radCmbCompany.SelectedValue
                .ContractName = txtContractName.Text
                .DomainId = Session("DomainID")
                .UserCntId = Session("UserContactID")
                .Amount = CCommon.ToLong(txtAmount.Text)
                .StartDate = CCommon.ToSqlDate(calStartDate.SelectedDate)
                .ExpDate = CCommon.ToSqlDate(calExpDate.SelectedDate)
                .Incidents = CCommon.ToLong(txtInci.Text)
                .Hours = CCommon.ToLong(txtHrs.Text)
                .rate = CCommon.ToDouble(txtRateHr.Text)
                .Notes = txtNotes.Text
                .EmailDays = 0
                .EmailIncidents = 0
                .EmailHours = 0
            End With
            ContractId = objContract.SaveContracts
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class