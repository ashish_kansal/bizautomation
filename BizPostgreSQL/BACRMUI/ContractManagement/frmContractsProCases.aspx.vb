Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Common
Partial Public Class frmContractsProCases
    Inherits BACRMPage

    Dim ObjContract As CContracts

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then LoadItems()
            
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadItems()
        Try
            If ObjContract Is Nothing Then ObjContract = New CContracts
            ObjContract.ContractID = CCommon.ToLong(GetQueryStringVal("ContractID"))
            ObjContract.DomainId = Session("DomainId")
            ObjContract.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dgItems.DataSource = ObjContract.getContractLinkedItems
            dgItems.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class