﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ContractsLog.ascx.vb" Inherits=".ContractsLog" %>
<asp:HiddenField ID="hdnContractsDomainId" ClientIDMode="Static" Value="0" runat="server" />
<asp:HiddenField ID="hdnContractsDivisonId" ClientIDMode="Static" Value="0" runat="server" />
<asp:HiddenField ID="hdnContractsRecordId" ClientIDMode="Static" Value="0" runat="server" />
<script src="../JavaScript/daterangepicker-master/moment.min.js"></script>
<table class="table table-bordered" id="tblContractsLog">
    <thead>
        <tr>
            <td>Record</td>
            <td>Contract used on</td>
            <td>Balance Left</td>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<script>
    $(document).ready(function () {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(getContractsLog);
        getContractsLog()
    })
    function getContractsLog() {
        var dataParam = '{numDomainID:' + $("#hdnContractsDomainId").val() + ',numContractId:' + $("#hdnContractsRecordId").val() + '}';
        $.ajax({
            type: 'POST',
            url: "../ContractManagement/frmMngContract.aspx/WebMethodGetContractLog",
            contentType: "application/json; charset=utf-8",
            data: dataParam,
            dataType: "json",
            success: function (response) {
                var Jresponse = $.parseJSON(response.d);
                $("#tblContractsLog tbody").html("");
                var dataToAppend = "";
                $.each(Jresponse, function (index, value) {
                    dataToAppend = "";
                    dataToAppend = dataToAppend + "<tr>";
                    if (value.intType == "1") {
                        dataToAppend = dataToAppend + "<td>" + value.vcType + "</td>"
                        dataToAppend = dataToAppend + "<td>" + formattedDate(value.dtmCreatedOn) + "(" + formattedTime(value.dtStartTime) + "-" + formattedTime(value.dtEndTime) + ")," + value.numUsedTime / 60 + " Mins</td>"
                        dataToAppend = dataToAppend + "<td>" + value.timeLeft+ "</td>"
                    } else {

                        dataToAppend = dataToAppend + "<td>" + value.vcType + "</td>"
                        dataToAppend = dataToAppend + "<td>" + formattedDate(value.dtmCreatedOn) + "</td>"
                        dataToAppend = dataToAppend + "<td>" + value.numBalance + "</td>"
                    }
                    dataToAppend = dataToAppend + "</tr>";
                    $("#tblContractsLog tbody").append(dataToAppend);
                });
            }
        })
    }
    function formattedDate(selectedDate) {
        return moment(selectedDate).format('DD-MM-YYYY');
    }
    function formattedTime(selectedDate) {
        return moment(selectedDate).format('hh:mm A');
    }
</script>