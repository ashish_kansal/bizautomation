<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmContract.aspx.vb" Inherits=".frmContract"
    MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>New Contract</title>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            InitializeValidation();
            $('#litMessage').fadeIn().delay(5000).fadeOut();
        });

        function CheckSchedule() {

            if (document.getElementById('ddlEmailTemp').value == 0) {
                alert("Select Email Template")
                document.form1.ddlEmailTemp.focus();
                return false;
            }
            if (document.getElementById('ddlContacts').value == 0) {
                alert("Select Contact")
                document.form1.ddlEmailTemp.focus();
                return false;
            }
            if (document.getElementById('chkEmailDays').checked == false && document.getElementById('chkEmailHrs').checked == false && document.getElementById('chkEmailInci').checked == false) {
                alert('Select Conditions')
                return false;
            }
            if (document.getElementById('chkEmailDays').checked == true && document.getElementById('txtEmailDays').value == '0') {
                alert('Enter Days before Contract Expiration Date')
                document.getElementById('txtEmailDays').focus();
                return false;
            }
            if (document.getElementById('chkEmailInci').checked == true && document.getElementById('txtEmailInci').value == '0') {
                alert('Enter support incidents left')
                document.getElementById('txtEmailInci').focus();
                return false;
            }
            if (document.getElementById('chkEmailHrs').checked == true && document.getElementById('txtEmailHrs').value == '0') {
                alert('Enter hours left')
                document.getElementById('txtEmailHrs').focus();
                return false;
            }
        }

        function Save() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Company")
                return false;
            }
            if (document.form1.txtContractName.value == "") {
                //alert("Date val=" & document.form1.calStartDate.value)
                alert("Enter Contract Name")
                document.form1.txtContractName.focus();
                return false;
            }
        }

        function Close() {
            window.close()
            return false;
        }

        function Redirect(type, id) {
            if (type == "Case") {
                window.location.href = "../cases/frmCases.aspx?CaseID=" + id;
            } else if (type == "Project") {
                window.location.href = "../projects/frmProjects.aspx?ProId=" + id;
            }
        }
    </script>
    <style type="text/css">
        #tblContract {
            border-spacing: 3px;
        }

        .tdLabel {
            padding-top: 3px;
            padding-right: 5px;
            background-color: #e5e5e5;
        }

        .tdField {
            padding-top: 3px;
            background-color: #f2f2f2;
            padding-left: 5px;
        }

        #tblContract td:nth-child(1), #tblContract td:nth-child(3), #tblContract td:nth-child(5) {
            text-align: right;
            font-weight: bold;
        }
    </style>
    <style>
        @media (max-width:60em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-md-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Cancel"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Cancel</asp:LinkButton>
                <asp:LinkButton ID="btnDel" runat="server" CssClass="btn btn-danger" Text="Delete"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Contract Details
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server"
    ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Button ID="btnClick" runat="server" Style="display: none" />
    <table width="100%">
        <tr>
            <td>
                <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
                    Skin="Default" ClickSelectedTab="True" SelectedIndex="0" MultiPageID="radMultiPage_OppTab">
                    <Tabs>
                        <telerik:RadTab Text="Contract" Value="Contract" PageViewID="radPageView_Contract">
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
                    <telerik:RadPageView ID="radPageView_Contract" runat="server">
                        <table style="width:100%">
                            <tr>
                                <td>
                                    <img alt="" src="../images/Contract128.png" />
                                </td>
                                <td>
                                    <table id="tblContract" class="table table-responsive table-bordered tblNoBorder"  width="100%">
                                        <tr>
                                            <td class="tdLabel">Account<font color="red">*</font></td>
                                            <td class="tdField">
                                                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" ClientIDMode="Static" Width="200px"
                                                    DropDownWidth="600px"
                                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                                    ShowMoreResultsBox="true" Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                                </telerik:RadComboBox>
                                            </td>
                                            <td class="tdLabel">Contract Name<font color="red">*</font></td>
                                            <td class="tdField">
                                                <asp:TextBox ID="txtContractName" Width="200px" runat="server" CssClass="signup form-control"></asp:TextBox>
                                            </td>
                                            <td class="tdLabel">Notes</td>
                                            <td class="tdField">
                                                <asp:TextBox CssClass="signup form-control" ID="txtNotes" runat="server" TextMode="MultiLine"
                                                    >
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="4" class="tdLabel" style="height: 25px;">Commitments</th>
                                            <th colspan="2" class="tdLabel" style="height: 25px;">Status</th>
                                        </tr>
                                        <tr>
                                            <td class="tdLabel">Amount
                                            </td>
                                            <td class="tdField">
                                                <asp:TextBox ID="txtAmount" runat="server" CssClass=" form-control required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}" Width="200px"></asp:TextBox>
                                            </td>
                                            <td class="tdLabel">
                                                <asp:Label ID="lblBalance" runat="server" CssClass="signup" Text="Balance of Deferred Income:"
                                                    Visible="true"></asp:Label>
                                            </td>
                                            <td class="tdField">
                                                <asp:Label ID="lblBalanceAmt" runat="server" CssClass="signup" Visible="true"></asp:Label>
                                            </td>
                                            <td class="tdLabel">Remaining Amount</td>
                                            <td class="tdField">
                                                <asp:Label runat="server" ID="lblRemAmount" Text="0" CssClass="normal1"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabel">Starting Date</td>
                                            <td class="tdField" Width="200px">
                                                <BizCalendar:Calendar ID="calStartDate" runat="server" ClientIDMode="AutoID" />
                                            </td>
                                            <td class="tdLabel">Expiration Date</td>
                                            <td class="tdField" Width="200px">
                                                <BizCalendar:Calendar ID="calExpDate" runat="server" ClientIDMode="AutoID" />
                                            </td>
                                            <td class="tdLabel">Remaining Days</td>
                                            <td class="tdField">
                                                <asp:Label runat="server" ID="lblRemDays" CssClass="normal1"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabel">Incidents (Support)
                                            </td>
                                            <td class="tdField">
                                                <asp:TextBox ID="txtInci" CssClass="signup form-control" runat="server" >
                                                </asp:TextBox>
                                            </td>
                                            <td class="tdLabel"></td>
                                            <td class="tdField"></td>
                                            <td class="tdLabel">Remaining Incidents</td>
                                            <td class="tdField">
                                                <asp:Label runat="server" ID="lblRemInci" CssClass="normal1"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabel">Hours
                                            </td>
                                            <td class="tdField">
                                                <asp:TextBox ID="txtHrs" CssClass=" form-control required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}"
                                                    runat="server" Width="200px">
                                                </asp:TextBox>
                                            </td>
                                            <td class="tdLabel">Rate/Hour (Support)</td>
                                            <td class="tdField">
                                                <asp:TextBox ID="txtRateHr" Width="50" Text="0" CssClass="form-control required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}"
                                                    runat="server">
                                                </asp:TextBox>
                                            </td>
                                            <td class="tdLabel">Remaining Hours</td>
                                            <td class="tdField">
                                                <asp:Label runat="server" ID="lblRemHrs" CssClass="normal1"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <asp:DataList ID="dtlContract" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                            <HeaderTemplate>
                                <table class="table table-striped table-bordered">
                                    <tr>
                                        <th style="height: 25px;">Record Type</th>
                                        <th style="height: 25px;">Record Name</th>
                                        <th style="height: 25px;">Amount Used</th>
                                        <th style="height: 25px;">Hours Used</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# Eval("RecordType")%>
                                    </td>
                                    <td>
                                        <a href="#" onclick="Redirect('<%# Eval("RecordType")%>',<%# Eval("RecordID")%>)"><%# Eval("RecordName")%></a>
                                    </td>
                                    <td>
                                        <%# Eval("Amount")%>
                                    </td>
                                    <td>
                                        <%# Eval("HoursUsed")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:DataList>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
</asp:Content>
