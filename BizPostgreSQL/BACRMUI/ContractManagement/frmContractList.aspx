<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmContractList.aspx.vb"
    Inherits=".frmContractList" MasterPageFile="~/common/GridMaster.Master" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Contract Management</title>
    <script language="javascript">
        function reDirectPage(url) {
            window.location.href = url
        }
        function OpemEmail(URL) {
            window.open(URL, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenNewContract() {

            window.open('../ContractManagement/frmAddContract.aspx', '', 'toolbar=no,titlebar=no,top=200,left=300,width=750,height=350,scrollbars=yes,resizable=yes')
            return false;
        }

        function DeleteRecord() {
            var str;
            if ($('#ddlSort').val() == 7) {
                str = 'Are you sure, you want to delete the selected contact from favorites?'
            }
            else {
                str = 'Are you sure, you want to delete the selected record?'
            }
            if (confirm(str)) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>View:</label>
                    <asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="true" CssClass="form-control">
                        <asp:ListItem Value="0" Selected="true">-- Select One --</asp:ListItem>
                        <asp:ListItem Value="1">Linked Cases</asp:ListItem>
                        <asp:ListItem Value="2">Linked Opportunities/Deals</asp:ListItem>
                        <asp:ListItem Value="3">Linked Projects</asp:ListItem>
                        <asp:ListItem Value="4">expire soonest</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Contracts
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        ShowPageIndexBox="Never"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgContracts" AllowSorting="true" runat="server" Width="100%" CssClass="table table-striped table-bordered" AutoGenerateColumns="false" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numContractID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numDivisionId"></asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="bintCreatedOn" HeaderText="Created On">
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "bintCreatedOn")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="bintExpDate" SortExpression="bintExpDate" HeaderText="Expiration Date"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcContractName" SortExpression="vcContractName"
                            HeaderText="Contract Name" CommandName="Name"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="vcCompanyName" SortExpression="vcCompanyName" HeaderText="Organization"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Days" SortExpression="bintStartDate" HeaderText="Days used"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Incidents" SortExpression="numIncidents" HeaderText="Incidents used"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Hours" SortExpression="numhours" HeaderText="Hours used"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Amount" SortExpression="numAmount" HeaderText="Amount used"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button runat="server" ID="btnGo" Style="display: none" />
    <asp:Button runat="server" ID="btnGo1" Style="display: none" />
</asp:Content>
