<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmContractsProCases.aspx.vb"
    Inherits=".frmContractsProCases" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Untitled Page</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table width="100%">
                <tr>
                    <td align="right" valign="bottom">
                        <asp:Button ID="Button1" runat="server" Text="Close" OnClientClick="javascript:self.close()"
                            CssClass="button" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Contracts Linked Items
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:DataGrid runat="server" ID="dgItems" Width="300px" AutoGenerateColumns="false"
        CssClass="dg">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="chrFrom" HeaderText="From"></asp:BoundColumn>
            <asp:BoundColumn DataField="Category" HeaderText="Category"></asp:BoundColumn>
            <asp:BoundColumn DataField="Detail" HeaderText="Detail"></asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
</asp:Content>
