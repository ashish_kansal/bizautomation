<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddContract.aspx.vb"
    Inherits=".frmAddContract" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>New Contract</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery-1.9.1.min.js"></script>
    <script language="javascript" type="text/javascript">
        

        $("#txtRateHr").onchange(function () {
            var hours = $("#txtHrs").val().parseInt() || 0;

            if (hours == 0) {
                alert("Enter hours");
            }
        })

        $("#txtHrs").onchange(function () {
            var hours = $("#txtHrs").val().parseInt() || 0;

            if (hours == 0) {
                $("#txtRateHr").attr("display", "none");
            } else {
                $("#txtRateHr").attr("display", "");
            }
        })


        function Close() {
            window.close();
        }
        function Save() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Company")
                return false;
            }
            if (document.getElementById("txtContractName").value == "") {

                alert("Enter Contract Name")
                document.getElementById("txtContractName").focus();
                return false;
            }

            if (document.getElementById("chkDays").checked == true) {
                if (document.getElementById("ctl00_Content_calStartDate_txtDate").value == "") {
                    alert("Select the Start Date")
                    document.getElementById("ctl00_Content_calStartDate_txtDate").focus();
                    return false;
                }
                if (document.getElementById("ctl00_Content_calExpDate_txtDate").value == "") {
                    alert("Select the Expiration Date")
                    document.getElementById("ctl00_Content_calExpDate_txtDate").focus();
                    return false;
                }

            }
        }
    </script>
    <style type="text/css">
        #tblContract {
            border-spacing: 3px;
        }

        .tdLabel {
            padding-top: 3px;
            padding-right: 5px;
             background-color:#e5e5e5;
        }

        .tdField {
            padding-top: 3px;
            background-color: #f2f2f2;
            padding-left: 5px;
            padding-right: 5px;
        }

        #tblContract td:nth-child(1) {
            text-align: right;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveClose" runat="server" Text="Save &amp; Close" CssClass="button"
                Width="100"></asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New Contract
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table id="tblContract" width="800px">
                <tr>
                    <td class="tdLabel">Account<font color="red">*</font></td>
                    <td class="tdField">
                        <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="250" DropDownWidth="600px"
                            OnClientItemsRequested="OnClientItemsRequestedOrganization"
                            ClientIDMode="Static"
                            ShowMoreResultsBox="true"
                            Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Contract Name<font color="red">*</font></td>
                    <td class="tdField">
                        <asp:TextBox ID="txtContractName" runat="server" Style="width: 250px !important" CssClass="signup"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Notes</td>
                    <td class="tdField">
                        <asp:TextBox CssClass="signup" ID="txtNotes" runat="server" TextMode="MultiLine" Style="width: 350px !important"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="tdLabel" style="height: 25px; text-align: center;">Commitments
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Amount</td>
                    <td class="tdField">
                        <asp:TextBox ID="txtAmount" runat="server" CssClass="signup" Text="" Style="width: 250px !important"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Starting Date
                    </td>
                    <td class="tdField">
                        <BizCalendar:Calendar ID="calStartDate" runat="server" ClientIDMode="AutoID" />
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Expiration Date
                    </td>
                    <td class="tdField">
                        <BizCalendar:Calendar ID="calExpDate" runat="server" ClientIDMode="AutoID" />
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Incidents (Support)
                    </td>
                    <td class="tdField">
                        <asp:TextBox ID="txtInci" CssClass="signup" Style="width: 250px !important" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Hours
                    </td>
                    <td class="tdField">
                        <asp:TextBox ID="txtHrs" CssClass="signup" Style="width: 250px !important" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel">Rate/Hour (Support)</td>
                    <td class="tdField">
                        <asp:TextBox ID="txtRateHr" Style="width: 250px !important" Text="0" CssClass="required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
