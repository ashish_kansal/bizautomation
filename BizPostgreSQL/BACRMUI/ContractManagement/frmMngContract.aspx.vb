﻿Imports System.Web.Services
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contract
Imports Newtonsoft.Json

Public Class frmMngContract
    Inherits BACRMPage
    Dim contracts As ContractsV2
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                LoadItemClassification()
                If GetQueryStringVal("type") = "1" Then
                    tdIncident1.Visible = False
                    tdIncident2.Visible = False
                    tdIncident3.Visible = False
                    tdIncident4.Visible = False
                    contractWarrantyPRow.Visible = True
                    contractWarrantySRow.Visible = True
                    Warranty1Row.Visible = False
                    Warranty2Row.Visible = False
                    lblType.Text = "Time"
                ElseIf GetQueryStringVal("type") = "3" Then
                    tdIncident1.Visible = True
                    tdIncident2.Visible = True
                    tdIncident3.Visible = True
                    tdIncident4.Visible = True
                    contractWarrantyPRow.Visible = True
                    contractWarrantySRow.Visible = False
                    Warranty1Row.Visible = False
                    Warranty2Row.Visible = False
                    lblType.Text = "Incident"
                Else
                    contractWarrantyPRow.Visible = False
                    contractWarrantySRow.Visible = False
                    Warranty1Row.Visible = True
                    Warranty2Row.Visible = True
                    lblType.Text = "Warranty"
                End If
                LoadContractDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            SaveContractDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadContractDetails()
        Try
            If CCommon.ToInteger(GetQueryStringVal("id")) > 0 Then
                radCmbCompany.Enabled = False
                ddlItemClassification.Enabled = False
                divCreatedSection.Visible = True
                contracts = New ContractsV2()
                contracts.DomainID = Session("DomainID")
                contracts.ContractID = CCommon.ToInteger(GetQueryStringVal("id"))
                Dim dtResult As New DataTable()
                dtResult = contracts.GetContactsDetails()
                If dtResult.Rows.Count > 0 Then
                    lblContractNo.Text = dtResult.Rows(0)("vcContractNo")
                    lblCreatedBy.Text = dtResult.Rows(0)("CreatedBy")
                    lblCreatedOn.Text = dtResult.Rows(0)("CreatedOn")
                    lblUpdatedOn.Text = dtResult.Rows(0)("ModifiedOn")
                    radCmbCompany.Text = dtResult.Rows(0)("vcCompanyName")
                    radCmbCompany.SelectedValue = dtResult.Rows(0)("numDivisonId")
                    txtIncidents.Text = dtResult.Rows(0)("numIncidents")
                    lblIncidentLeft.Text = dtResult.Rows(0)("numIncidentLeft")
                    txtHours.Text = dtResult.Rows(0)("numHours")
                    txtMinutes.Text = dtResult.Rows(0)("numMinutes")
                    lblTimeLeft.Text = dtResult.Rows(0)("timeLeft")
                    hdnSelectedItems.Value = dtResult.Rows(0)("vcItemClassification")
                    If ddlItemClassification.Items.FindByValue(CCommon.ToLong(dtResult.Rows(0)("vcItemClassification"))) IsNot Nothing Then
                        ddlItemClassification.Items.FindByValue(CCommon.ToLong(dtResult.Rows(0)("vcItemClassification"))).Selected = True
                    End If
                    txtWarrantyDays.Text = dtResult.Rows(0)("numWarrantyDays")
                    txtNotes.Text = dtResult.Rows(0)("vcNotes")

                    Dim hdnContractsDomainId As HiddenField = DirectCast(ContractsLog.FindControl("hdnContractsDomainId"), HiddenField)
                    Dim hdnContractsDivisonId As HiddenField = DirectCast(ContractsLog.FindControl("hdnContractsDivisonId"), HiddenField)
                    Dim hdnContractsRecordId As HiddenField = DirectCast(ContractsLog.FindControl("hdnContractsRecordId"), HiddenField)
                    hdnContractsDomainId.Value = Session("DomainID")
                    hdnContractsDivisonId.Value = dtResult.Rows(0)("numDivisonId")
                    hdnContractsRecordId.Value = CCommon.ToInteger(GetQueryStringVal("id"))
                    divContractsLog.Visible = True
                End If
            Else
                divContractsLog.Visible = False
                divCreatedSection.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub SaveContractDetails()
        Try
            contracts = New ContractsV2()
            If CCommon.ToInteger(GetQueryStringVal("id")) = 0 And GetQueryStringVal("type") = 2 Then
                Dim str As String()
                str = hdnSelectedItems.Value.Split(",")
                For Each i As String In str
                    contracts = New ContractsV2()
                    contracts.DomainID = Session("DomainID")
                    contracts.ContractID = CCommon.ToInteger(GetQueryStringVal("id"))
                    contracts.Type = GetQueryStringVal("type")
                    contracts.UserCntID = Session("UserContactID")
                    contracts.DivisionId = CCommon.ToInteger(radCmbCompany.SelectedValue)
                    contracts.Incidents = CCommon.ToInteger(txtIncidents.Text)
                    contracts.Hours = CCommon.ToInteger(txtHours.Text)
                    contracts.Minutes = CCommon.ToInteger(txtMinutes.Text)
                    contracts.ItemClassification = i
                    contracts.WarrantyDays = CCommon.ToInteger(txtWarrantyDays.Text)
                    contracts.Notes = txtNotes.Text
                    contracts.SaveContracts()
                Next
            Else
                contracts.DomainID = Session("DomainID")
                contracts.ContractID = CCommon.ToInteger(GetQueryStringVal("id"))
                contracts.Type = GetQueryStringVal("type")
                contracts.UserCntID = Session("UserContactID")
                contracts.DivisionId = CCommon.ToInteger(radCmbCompany.SelectedValue)
                contracts.Incidents = CCommon.ToInteger(txtIncidents.Text)
                contracts.Hours = CCommon.ToInteger(txtHours.Text)
                contracts.Minutes = CCommon.ToInteger(txtMinutes.Text)
                contracts.ItemClassification = hdnSelectedItems.Value
                contracts.WarrantyDays = CCommon.ToInteger(txtWarrantyDays.Text)
                contracts.Notes = txtNotes.Text
                contracts.SaveContracts()
            End If
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "window.opener.location.href ='../ContractManagement/frmContractListv2.aspx?type=" + GetQueryStringVal("type") + "';window.close();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            If GetQueryStringVal("type") = 1 Or GetQueryStringVal("type") = 3 Then
                Response.Write("Organization contract is already exist")
            Else
                Response.Write(ex.ToString())
            End If
        End Try
    End Sub
    Sub LoadItemClassification()
        Try
            Dim objCommon As New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlItemClassification, 36, Session("DomainID")) ''tem Classification
            contracts = New ContractsV2()
            contracts.DomainID = Session("DomainID")
            contracts.GetWarrantyItemClassification()
            Dim dtAddedItems As New DataTable()
            dtAddedItems = contracts.GetWarrantyItemClassification()
            If dtAddedItems.Rows.Count > 0 Then
                For Each dr As DataRow In dtAddedItems.Rows
                    If ddlItemClassification.Items.FindByValue(CCommon.ToLong(dr("vcItemClassification"))) IsNot Nothing Then
                        ddlItemClassification.Items.FindByValue(CCommon.ToLong(dr("vcItemClassification"))).Attributes.Add("disabled", "disabled")
                    End If
                Next
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    <WebMethod()>
    Public Shared Function WebMethodGetContractLog(ByVal numDomainID As Long, ByVal numContractId As Long) As String
        Try
            Dim objContracts = New ContractsV2()
            Dim dtTable As DataTable
            objContracts.DomainID = numDomainID
            objContracts.ContractID = numContractId
            dtTable = objContracts.GetContactsLog()
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dtTable, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try
    End Function
End Class