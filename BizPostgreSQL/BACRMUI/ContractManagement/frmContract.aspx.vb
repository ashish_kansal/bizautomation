Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Marketing
Partial Public Class frmContract
    Inherits BACRMPage
    Dim lngCntrId As Integer
   
    Dim objCommon As CCommon
    Dim objContract As CContracts

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            'objCommon.CheckdirtyForm(Page)
            If GetQueryStringVal("contractId") <> "" Then
                lngCntrId = CCommon.ToLong(GetQueryStringVal("contractId"))
                btnSave.Attributes.Add("onclick", "return Save()")
                btnSaveClose.Attributes.Add("onclick", "return Save()")
                txtAmount.Attributes.Add("onKeyPress", "CheckNumber(2)")
                txtHrs.Attributes.Add("onKeyPress", "CheckNumber(2)")
                txtInci.Attributes.Add("onKeyPress", "CheckNumber(2)")
                txtRateHr.Attributes.Add("onKeyPress", "CheckNumber(2)")
            End If

            If Not IsPostBack Then

                'To Set Permission
                objCommon = New CCommon
                GetUserRightsForPage(34, 2)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    btnSave.Visible = False
                    btnSaveClose.Visible = False
                ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDel.Visible = False
                End If

                If GetQueryStringVal("CntID") <> "" Or GetQueryStringVal("DivID") <> "" Or GetQueryStringVal("ProID") <> "" Or GetQueryStringVal("OpID") <> "" Or GetQueryStringVal("CaseID") <> "" Then

                    If GetQueryStringVal("DivID") <> "" Then
                        objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("DivID"))
                        objCommon.charModule = "D"
                    End If

                    objCommon.GetCompanySpecificValues1()
                    Dim strCompany, strContactID As String
                    strCompany = objCommon.GetCompanyName
                    strContactID = objCommon.ContactID
                    radCmbCompany.Text = strCompany
                    radCmbCompany.SelectedValue = objCommon.DivisionID
                End If

                BindData()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub BindData()
        Try
            If objContract Is Nothing Then objContract = New CContracts
            Dim dtTable As DataTable
            objContract.ContractID = lngCntrId
            objContract.UserCntId = Session("UserContactID")
            objContract.DomainId = Session("DomainID")
            dtTable = objContract.GetContractDtl()

            If dtTable.Rows.Count > 0 Then
                If Not IsDBNull(dtTable.Rows(0).Item("numDivisionId")) Then
                    radCmbCompany.SelectedValue = dtTable.Rows(0).Item("numDivisionId")
                    radCmbCompany.Text = dtTable.Rows(0).Item("CompanyName")
                End If

                txtContractName.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcContractName")), "", dtTable.Rows(0).Item("vcContractName"))
                txtNotes.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcNotes")), "", dtTable.Rows(0).Item("vcNotes"))
                txtAmount.Text = CCommon.ToString(dtTable.Rows(0).Item("numAmount"))
                lblRemAmount.Text = String.Format("{0:#,##0.00}", CCommon.ToDouble(dtTable.Rows(0).Item("RemAmount")))
                txtHrs.Text = CCommon.ToString(dtTable.Rows(0).Item("numhours"))
                txtRateHr.Text = CCommon.ToString(dtTable.Rows(0).Item("decrate"))
                lblRemHrs.Text = String.Format("{0:#,##0.00}", CCommon.ToDouble(dtTable.Rows(0).Item("RemHours")))

                If CCommon.ToDouble(dtTable.Rows(0).Item("BalanceAmt")) > 0 Then
                    lblBalance.Visible = True
                    lblBalanceAmt.Visible = True
                    lblBalanceAmt.Text = String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("BalanceAmt"))
                Else
                    lblBalance.Visible = False
                    lblBalanceAmt.Visible = False
                End If

                calStartDate.SelectedDate = DateAdd(DateInterval.Day, 0, CCommon.ToSqlDate(dtTable.Rows(0).Item("bintStartDate")))
                calExpDate.SelectedDate = DateAdd(DateInterval.Day, 0, CCommon.ToSqlDate(dtTable.Rows(0).Item("bintExpDate")))
                lblRemDays.Text = String.Format("{0:#,##0.00}", CCommon.ToDouble(dtTable.Rows(0).Item("Days")))

                txtInci.Text = CCommon.ToString(dtTable.Rows(0).Item("numIncidents"))
                lblRemInci.Text = String.Format("{0:#,##0}", CCommon.ToDouble(dtTable.Rows(0).Item("Incidents")))
            End If

            BindContract(lngCntrId)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            PageRedirect()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub Save()
        Try
            If objContract Is Nothing Then objContract = New CContracts
            With objContract
                .ContractID = lngCntrId
                .DivisionId = radCmbCompany.SelectedValue
                .ContractName = txtContractName.Text
                .DomainId = Session("DomainID")
                .UserCntId = Session("UserContactID")
                .Amount = IIf(txtAmount.Text = "", 0, CInt(txtAmount.Text))
                .StartDate = calStartDate.SelectedDate
                .ExpDate = calExpDate.SelectedDate
                .Incidents = IIf(txtInci.Text = "", 0, CInt(txtInci.Text))
                .Hours = IIf(txtHrs.Text = "", 0, CInt(txtHrs.Text))
                .rate = IIf(txtRateHr.Text = "", 0, txtRateHr.Text)
                .Notes = txtNotes.Text
            End With
            objContract.ModifyContracts()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub PageRedirect()
        Try
            If GetQueryStringVal( "frm") = "ContractList" Then
                Response.Redirect("../ContractManagement/frmContractList.aspx", False)
            ElseIf GetQueryStringVal( "frm") = "Account" Then
                Response.Redirect("../account/frmAccounts.aspx?DivId=" & GetQueryStringVal( "DivId"))
            Else
                Response.Redirect("../ContractManagement/frmContractList.aspx", False)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDel.Click
        Try
            If objContract Is Nothing Then objContract = New CContracts
            objContract.ContractID = lngCntrId
            objContract.DomainId = Session("DomainId")
            objContract.DeleteContract()
            PageRedirect()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            PageRedirect()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub BindContract(ByVal lngCntrId As Long)
        Try
            If objContract Is Nothing Then objContract = New CContracts
            objContract.DomainID = CCommon.ToLong(Session("DomainID"))
            objContract.ContractID = lngCntrId

            Dim dt As DataTable = objContract.GetContactDetail()
            dtlContract.DataSource = dt
            dtlContract.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class