﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contract

Public Class frmContractListv2
    Inherits BACRMPage

    Dim RegularSearch As String
    Dim CustomSearch As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            PersistTable.Load()
            If PersistTable.Count > 0 Then
                txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
            End If
            If GetQueryStringVal("type") = "1" Then
                lblType.Text = "Time"
            ElseIf GetQueryStringVal("type") = "3" Then
                lblType.Text = "Incident"
            Else
                lblType.Text = "Warranty"
            End If
            BindDatagrid()
        End If
    End Sub
    Private Sub BindDatagrid()
        Try
            Dim dtContracts As DataTable
            Dim objContracts As New ContractsV2
            objContracts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            Dim SortChar As Char


            Dim m_aryRightsDeleteContracts() As Integer = GetUserRightsForPage_Other(34, 1)
            If m_aryRightsDeleteContracts(RIGHTSTYPE.DELETE) <> 0 Then
                btnRemove.Visible = True
            End If
            ' Set Default Filter 
            With objContracts
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .Type = GetQueryStringVal("type")
                If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                .CurrentPage = txtCurrrentPage.Text.Trim()

                .PageSize = Session("PagingRows")
                .TotalRecords = 0

                'If txtSortOrder.Text = "D" Then
                '    .columnSortOrder = "Desc"
                'Else : .columnSortOrder = "Asc"
                'End If
                GridColumnSearchCriteria()

                .RegularSearchCriteria = RegularSearch

                .CustomSearchCriteria = ""
            End With
            Dim dsList As DataSet

            dsList = objContracts.GetContractsList()
            dtContracts = dsList.Tables(0)

            Dim dtTableInfo As DataTable = dsList.Tables(1)


            'Persist Form Settings
            PersistTable.Clear()
            PersistTable.Add(PersistKey.CurrentPage, IIf(dtContracts.Rows.Count > 0, txtCurrrentPage.Text, "1"))
            PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
            PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
            PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
            PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
            PersistTable.Save()

            If dtContracts.Rows.Count = 0 Then
                txtTotalRecords.Text = 0
            Else
                objContracts.TotalRecords = dtContracts.Rows(0)("TotalRecords")
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = objContracts.TotalRecords / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                txtTotalRecords.Text = objContracts.TotalRecords
            End If
            Dim htGridColumnSearch As New Hashtable

            If txtGridColumnFilter.Text.Trim.Length > 0 Then
                Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                Dim strIDValue() As String

                For i = 0 To strValues.Length - 1
                    strIDValue = strValues(i).Split(":")

                    htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                Next
            End If

            Dim bField As BoundField
            Dim Tfield As TemplateField
            gvSearch.Columns.Clear()

            For Each drRow As DataRow In dtTableInfo.Rows
                Tfield = New TemplateField

                Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, 0, htGridColumnSearch, 146, "numContractId", "desc")

                Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, 0, htGridColumnSearch, 146, "numContractId", "desc")
                gvSearch.Columns.Add(Tfield)
            Next

            Dim dr As DataRow
            dr = dtTableInfo.NewRow()
            dr("vcAssociatedControlType") = "DeleteCheckBox"
            dr("intColumnWidth") = "30"

            Tfield = New TemplateField
            Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, 0, htGridColumnSearch, 146)
            Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, 0, htGridColumnSearch, 146)
            gvSearch.Columns.Add(Tfield)

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objContracts.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text
            gvSearch.DataSource = dtContracts
            gvSearch.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Try
            If txtDelContractId.Text <> "" Then
                Dim i As Int16 = 0
                Dim lngWoID As Long = 0
                Dim objContracts As New ContractsV2
                objContracts.DomainID = Session("DomainID")
                objContracts.ContractIds = txtDelContractId.Text
                objContracts.DeleteContracts()
                txtDelContractId.Text = ""

            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            txtCurrrentPage_TextChanged(Nothing, Nothing)
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub GridColumnSearchCriteria()
        Try
            If txtGridColumnFilter.Text.Trim.Length > 0 Then
                Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                Dim strIDValue(), strID(), strCustom As String
                Dim strRegularCondition As New ArrayList
                Dim strCustomCondition As New ArrayList

                For i As Integer = 0 To strValues.Length - 1
                    strIDValue = strValues(i).Split(":")
                    strID = strIDValue(0).Split("~")

                    If strID(0).Contains("CFW.Cust") Then

                        Select Case strID(3).Trim()
                            Case "TextBox", "TextArea"
                                strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                            Case "CheckBox"
                                If strIDValue(1).ToLower() = "yes" Then
                                    strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                ElseIf strIDValue(1).ToLower() = "no" Then
                                    strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values_Opp CFWInner WHERE CFWInner.RecId=OpportunityMaster.numOppId AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                End If
                            Case "SelectBox"
                                strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                            Case "DateField"
                                If strID(4) = "From" Then
                                    Dim fromDate As Date
                                    If Date.TryParse(strIDValue(1), fromDate) Then
                                        strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                    End If
                                ElseIf strID(4) = "To" Then
                                    Dim toDate As Date
                                    If Date.TryParse(strIDValue(1), toDate) Then
                                        strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                    End If
                                End If
                            Case "CheckBoxList"
                                Dim items As String() = strIDValue(1).Split(",")
                                Dim searchString As String = ""

                                For Each item As String In items
                                    searchString = searchString & If(searchString.Length > 0, " OR ", "") & " GetCustFldValueOpp(" & strID(0).Replace("CFW.Cust", "") & ",OpportunityMaster.numOppID) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                Next

                                strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                            Case Else
                                strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                        End Select
                    Else
                        Select Case strID(3).Trim()
                            Case "Website", "Email", "TextBox", "Label"
                                If strID(0) = "WO.vcWorkOrderNameWithQty" Then
                                    strRegularCondition.Add("(WO.vcWorkOrderName ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                ElseIf strID(0) = "I.AssemblyItemSKU" Then
                                    strRegularCondition.Add("(I.vcItemName ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Else
                                    strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                End If
                            Case "SelectBox"
                                If strID(0) = "WO.MaxBuildQty" Then
                                    If strIDValue(1) = 1 Then
                                        strRegularCondition.Add("(select fn_CalculateMinBuildQtyByItem(WO.numItemCode,WHI.numWareHouseID)) >= WO.numQtyItemsReq")
                                    ElseIf strIDValue(1) = 2 Then
                                        strRegularCondition.Add("(select fn_CalculateMinBuildQtyByItem(WO.numItemCode,WHI.numWareHouseID)) < WO.numQtyItemsReq")
                                    End If
                                ElseIf strID(0) = "SP.Slp_Name" Then
                                    strRegularCondition.Add("SP.Slp_id" & " IN (" & strIDValue(1) & ")")
                                ElseIf strID(0) = "WO.vcAssignedTo" Then
                                    strRegularCondition.Add("WO.numAssignedTo" & " IN (" & strIDValue(1) & ")")
                                ElseIf strID(0) = ".vcContractsItemClassificationName" Then
                                    strRegularCondition.Add(strID(0) & " IN (" & strIDValue(1) & ")")
                                Else
                                    strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                End If

                            Case "TextArea"
                                strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                            Case "TextBoxRangePercentage"

                                If strID(4) = "RangeFrom" Then
                                    If CCommon.ToLong(strIDValue(1)) >= 0 Then
                                        strRegularCondition.Add(strID(0) & " >= '" & CCommon.ToLong(strIDValue(1)) & "'")
                                    End If
                                ElseIf strID(4) = "RangeTo" Then
                                    If CCommon.ToLong(strIDValue(1)) >= 0 Then
                                        strRegularCondition.Add(strID(0) & " <= '" & CCommon.ToLong(strIDValue(1)) & "'")
                                    End If
                                End If
                            Case "DateField"
                                If strID(4) = "From" Then
                                    Dim fromDate As Date
                                    If Date.TryParse(strIDValue(1), fromDate) Then
                                        strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                    End If
                                ElseIf strID(4) = "To" Then
                                    Dim toDate As Date
                                    If Date.TryParse(strIDValue(1), toDate) Then
                                        strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                    End If
                                End If
                        End Select
                    End If
                Next
                RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
            Else
                RegularSearch = ""
                CustomSearch = ""
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

End Class