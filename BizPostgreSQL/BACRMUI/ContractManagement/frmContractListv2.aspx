﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmContractListv2.aspx.vb" 
     Inherits=".frmContractListv2"  MasterPageFile="~/common/GridMaster.Master"%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script>
        function DeleteRecord() {
            var RecordIDs = GetCheckedRowValues()
            var RecordIDsData = '';
            var arr = RecordIDs.split(",");
            arr.forEach(function (index, value) {
                RecordIDsData = RecordIDsData + index.split('~')[0]+',';
            });
            document.getElementById('txtDelContractId').value = RecordIDsData;
            console.log(RecordIDsData);
            if (RecordIDsData.length > 0)
                return true
            else {
                alert('Please select atleast one record!!');
                return false
            }
        }
        $(document).ready(function () {
            debugger;
            $('[id$=gvSearch] select').change(function () {
                debugger;
                if ($(this).prop("className") == "option-droup-multiSelection-Group") {
                    return false;
                } else {
                    debugger;
                    on_filter();
                }
            });
        })
        function openContractPopup(id,type) {
            window.open('../ContractManagement/frmMngContract.aspx?type=' + type + '&id=' + id, "", "toolbar = no, titlebar = no, left = 100, top = 100, width = 1500, height = 645, scrollbars = yes, resizable = yes");
        }
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="pull-right">
         <asp:LinkButton ID="btnRemove" OnClientClick="return DeleteRecord()" Visible="false" runat="server" CssClass="btn  btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Remove</asp:LinkButton>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblType" runat="server" Text=""></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('contractmanagement/frmcontractlistv2.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        ShowPageIndexBox="Never"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
      
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
     <%-- OnPageChanged="bizPager_PageChanged"--%>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
      <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive"  style="min-height: 600px !important;">
                <%--OnRowDataBound="gvSearch_RowDataBound"--%>
                   <asp:GridView ID="gvSearch" runat="server" CssClass="table table-bordered table-striped" DataKeyNames="numContractId" EnableViewState="true" AutoGenerateColumns="false" Width="100%" ShowHeaderWhenEmpty="true" >
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
     <asp:TextBox ID="txtDelContractId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button runat="server" ID="btnGo" Style="display: none" />
    <asp:Button runat="server" ID="btnGo1" Style="display: none" />
</asp:Content>