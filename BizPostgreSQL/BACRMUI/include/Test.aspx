<%@ Page Language="vb" AutoEventWireup="false"   CodeBehind="Test.aspx.vb" Inherits="Test" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	
	<head id="Head1" runat="server">
	<title>Accounts</title>
         <link rel="stylesheet" href="~/CSS/Tab.css" type="text/css" />
         <script type="text/javascript" language="javascript">
          function ActiveTabChanged() 
    {
        document.form1.btnTabs.click();
        return false;
    }
         function alert()
         {
        
           document.form1.btnGo.click()
         }
         </script>

    
  
</head>
<body >
    <form id="form1" runat="server">
    <br />
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
    <ajaxToolkit:TabContainer CssClass="Test"   runat="server" ID="TabContainer1">
        <ajaxToolkit:TabPanel  runat="server"  ID="TabPanel1" HeaderText="TabPanel1">
            <ContentTemplate >
              
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel  runat="server" ID="TabPanel2" HeaderText="TabPanel2">
            <ContentTemplate>
                TabPanel2
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel3" HeaderText="TabPanel3">
            <ContentTemplate>
                TabPanel3
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <asp:Label ID="lblPanel1" runat="server" > </asp:Label>
    </form>
</body>
</html>
