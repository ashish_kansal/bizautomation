<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="webmenu.ascx.vb" Inherits="webmenu" ClientIDMode="AutoID" %>
<script type="text/javascript">

    function goto(target, frame, div, a, b) {
        target = target + '?DivID=' + document.getElementById('ctl00_webmenu1_txtDivision').value + '&CntID=' + document.getElementById('ctl00_webmenu1_txtContact').value + '&ProID=' + document.getElementById('ctl00_webmenu1_txtPro').value + '&OpID=' + document.getElementById('ctl00_webmenu1_txtOpp').value + '&CaseID=' + document.getElementById('ctl00_webmenu1_txtCase').value
        if (target == '../opportunity/frmNewSalesOrder.aspx' || target == '../opportunity/frmNewPurchaseOrder.aspx') {
            window.open(target, 'popNewOrder', 'toolbar=no,titlebar=no,left=150, top=150,width=' + a + ',height=' + b + ',scrollbars=yes,resizable=yes')
        }
        else {
            window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=' + a + ',height=' + b + ',scrollbars=yes,resizable=yes')
        }

    }
    function OpenPop(target) {
        window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=750,height=450,scrollbars=yes,resizable=yes')
    }

    function PreSetSearchCriteria() {
        if (document.getElementById('ctl00_webmenu1_ddlSearch').options.length > 1) {
            return true;
        } else {
            alert('Search fields has not been configured. please contact Administrator');
            return false;
        }
    }
    function fun_Search() {
        PreSetSearchCriteria()

    }

    function onOk() {
        alert("hi")
    }
    function fnRedirect(a) {

        document.location.href = a
        return false
    }
    function OpenRecentBizInvoice(a, b, c) {
        window.open('../opportunity/frmBizInvoice.aspx?OpID=' + a + '&OppBizId=' + b + '&Print=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
        return false;
    }
</script>
<body>
    <asp:Literal ID="ltrlLastViewdItem" runat="server"></asp:Literal>

    <input id="txtDivision" style="display: none" type="text" name="txtDivision" runat="server" />
    <input id="txtContact" style="display: none" type="text" name="txtContact" runat="server" />
    <input id="txtOpp" style="display: none" type="text" name="txtOpp" runat="server" />
    <input id="txtPro" style="display: none" type="text" name="txtPro" runat="server" />
    <input id="txtCase" style="display: none" type="text" name="txtCase" runat="server" />
</body>
