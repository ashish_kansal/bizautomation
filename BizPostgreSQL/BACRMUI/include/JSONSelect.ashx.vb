﻿Imports System.Web
Imports System.Web.Services
Imports System.Text
Imports BACRM.BusinessLogic.Common
Imports System.Web.Script.Serialization
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin

Public Class JSONSelect1
    Implements System.Web.IHttpHandler
    Implements System.Web.SessionState.IRequiresSessionState

    Dim objCommon As New CCommon
    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim id As String = context.Request.QueryString("id")

        Dim str As String() = id.Split("~")

        If str(0).ToLower = "organization" Or str(0).ToLower = "contact" Then
            objCommon.ContactID = str(3)
            objCommon.DivisionID = str(4)
            objCommon.TerittoryID = str(5)
            'added by chintan for dependant dropdown
            objCommon.Mode = IIf(str(0).ToLower = "contact", 5, 1)
            objCommon.RecordId = IIf(str(0).ToLower = "organization", objCommon.DivisionID, objCommon.ContactID)
            objCommon.ListID = str(6)
        ElseIf str(0).ToLower = "project" Or str(0).ToLower = "case" Then
            objCommon.DivisionID = str(4)
            objCommon.TerittoryID = str(5)
            'added by chintan for dependant dropdown
            objCommon.Mode = IIf(str(0).ToLower = "case", 4, 2)
            objCommon.RecordId = str(3)
            objCommon.ListID = str(6)
        ElseIf str(0).ToLower = "opp" Then
            objCommon.TerittoryID = str(4)
            'added by chintan for dependant dropdown
            objCommon.Mode = 3
            objCommon.RecordId = str(3)
            objCommon.ListID = str(5)
            If str.Length > 6 Then
                objCommon.DivisionID = str(6)
            End If
        End If


        objCommon.FormFieldId = str(1)
        objCommon.CustomField = str(2)
        objCommon.UserCntID = context.Session("UserContactID")
        objCommon.DomainID = context.Session("DomainID")

        Dim dtPropertyData As DataTable
        dtPropertyData = objCommon.GetInLineEditPropertyName()

        Dim strData As New StringBuilder()

        For Each dr As DataRow In dtPropertyData.Rows
            Dim dtData As DataTable

            If dr("vcPropertyName") = "AssignedTo" Or dr("vcPropertyName") = "IntPrjMgr" Or dr("vcPropertyName") = "AssignTo" Then
                If context.Session("PopulateUserCriteria") = 1 Then
                    dtData = objCommon.ConEmpListFromTerritories(context.Session("DomainID"), 0, 0, objCommon.TerittoryID)
                ElseIf context.Session("PopulateUserCriteria") = 2 Then
                    dtData = objCommon.ConEmpList(context.Session("DomainID"), context.Session("UserContactID"))
                Else
                    dtData = objCommon.ConEmpList(context.Session("DomainID"), 0, 0)
                End If

                If str(0).ToLower() = "workorder" AndAlso dr("vcPropertyName") = "AssignedTo" Then
                    If dtData.Columns.Contains("tintGroupType") Then
                        dtData = dtData.Select("tintGroupType <> 4").CopyToDataTable()
                    End If
                End If

                'Commissionable Contacts
                If str(0).ToLower = "opp" Then
                    Dim objUser As New UserAccess
                    objUser.DomainID = context.Session("DomainID")
                    objUser.byteMode = 1

                    Dim dt As DataTable = objUser.GetCommissionsContacts

                    Dim item As ListItem
                    Dim dr1 As DataRow

                    For Each dr2 As DataRow In dt.Rows
                        dr1 = dtData.NewRow
                        dr1(0) = dr2("numContactID")
                        dr1(1) = dr2("vcUserName")
                        dtData.Rows.Add(dr1)
                    Next
                End If
            ElseIf dr("vcPropertyName") = "ProjectID" Then
                Dim objProject As New BACRM.BusinessLogic.Projects.Project
                objProject.DomainID = context.Session("DomainID")
                Dim dtProject As DataTable = objProject.GetOpenProject()
                If Not dtProject Is Nothing Then
                    dtData = New DataTable
                    dtData.Columns.Add("Value")
                    dtData.Columns.Add("Text")
                    Dim dr1 As DataRow

                    For Each dr2 As DataRow In dtProject.Rows
                        dr1 = dtData.NewRow
                        dr1(0) = dr2("numProId")
                        dr1(1) = dr2("vcProjectName")
                        dtData.Rows.Add(dr1)
                    Next
                End If
            ElseIf dr("vcPropertyName") = "intDropShip" Then
                dtData = objCommon.GetDropShip()
            ElseIf dr("vcPropertyName") = "Sex" Then
                dtData = New DataTable
                dtData.Columns.Add("Value")
                dtData.Columns.Add("Text")
                Dim dr1 As DataRow
                dr1 = dtData.NewRow
                dr1("Value") = "M"
                dr1("Text") = "Male"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("Value") = "F"
                dr1("Text") = "Female"
                dtData.Rows.Add(dr1)
            ElseIf dr("vcPropertyName") = "Manager" Then
                objCommon.ContactID = objCommon.ContactID
                objCommon.charModule = "C"
                objCommon.GetCompanySpecificValues1()
                dtData = objCommon.GetManagers(context.Session("DomainID"), objCommon.ContactID, objCommon.DivisionID)
            ElseIf dr("vcPropertyName") = "GroupID" Then
                dtData = objCommon.GetGroups()
            ElseIf dr("vcPropertyName") = "CampaignID" Then
                Dim objCampaign As New BACRM.BusinessLogic.Reports.PredefinedReports
                objCampaign.byteMode = 2
                objCampaign.DomainID = context.Session("DomainID")
                dtData = objCampaign.GetCampaign()
            ElseIf dr("vcPropertyName") = "DripCampaign" Then
                Dim objCampaign As New BACRM.BusinessLogic.Marketing.Campaign
                With objCampaign
                    .SortCharacter = "0"
                    .UserCntID = context.Session("UserContactID")
                    .PageSize = 100
                    .TotalRecords = 0
                    .DomainID = context.Session("DomainID")
                    .columnSortOrder = "Asc"
                    .CurrentPage = 1
                    .columnName = "vcECampName"
                    dtData = objCampaign.ECampaignList
                    Dim drow As DataRow = dtData.NewRow
                    drow("numECampaignID") = -1
                    drow("vcECampName") = "-- Disengaged --"
                    dtData.Rows.Add(drow)
                End With
            ElseIf dr("vcPropertyName") = "CusPrjMgr" Then
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = objCommon.DivisionID
                    dtData = fillCombo.ListContact().Tables(0)
                End With
            ElseIf dr("vcPropertyName") = "ContractID" Then
                Dim objContracts As New CContracts
                objContracts.DivisionId = objCommon.DivisionID
                objContracts.UserCntID = context.Session("UserContactId")
                objContracts.DomainID = context.Session("DomainId")
                dtData = objContracts.GetContractDdlList()
            ElseIf dr("vcPropertyName") = "ContactID" Then
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = objCommon.DivisionID
                    dtData = fillCombo.ListContact().Tables(0)
                End With
            ElseIf dr("vcPropertyName") = "RecurringId" Then
                Dim lobjChecks As New Checks
                lobjChecks.DomainID = context.Session("DomainID")
                dtData = lobjChecks.GetRecurringDetails
            ElseIf dr("vcPropertyName") = "DealStatus" Then
                dtData = New DataTable
                dtData.Columns.Add("numListItemID")
                dtData.Columns.Add("vcData")

                Dim row As DataRow = dtData.NewRow
                row("numListItemID") = 1
                row("vcData") = "Deal Won"
                dtData.Rows.Add(row)

                row = dtData.NewRow
                row("numListItemID") = 2
                row("vcData") = "Deal Lost"
                dtData.Rows.Add(row)
                dtData.AcceptChanges()
            ElseIf dr("vcPropertyName") = "intReqPOApproved" Then
                dtData = New DataTable
                dtData.Columns.Add("numListItemID")
                dtData.Columns.Add("vcData")

                Dim row As DataRow = dtData.NewRow
                row("numListItemID") = 0
                row("vcData") = "Pending Approval"
                dtData.Rows.Add(row)

                row = dtData.NewRow
                row("numListItemID") = 1
                row("vcData") = "Approved"
                dtData.Rows.Add(row)

                row = dtData.NewRow
                row("numListItemID") = 2
                row("vcData") = "Rejected"
                dtData.Rows.Add(row)
                dtData.AcceptChanges()
            ElseIf dr("vcPropertyName") = "numPartenerContact" AndAlso str(0).ToLower = "organization" Then
                objCommon.DomainID = context.Session("DomainID")
                Dim _DivisonID As Long
                _DivisonID = objCommon.DivisionID
                objCommon.DivisionID = 0
                dtData = objCommon.GetDivPartnerContacts()
                objCommon.DivisionID = _DivisonID
            ElseIf dr("vcPropertyName") = "numPartenerSource" AndAlso str(0).ToLower = "organization" Then
                objCommon.DomainID = context.Session("DomainID")
                dtData = objCommon.GetPartnerSource()
            ElseIf dr("vcPropertyName") = "ReleaseStatus" Then
                dtData = New DataTable
                dtData.Columns.Add("numListItemID")
                dtData.Columns.Add("vcData")

                Dim row As DataRow = dtData.NewRow
                row("numListItemID") = 1
                row("vcData") = "Open"
                dtData.Rows.Add(row)

                row = dtData.NewRow
                row("numListItemID") = 2
                row("vcData") = "Purchased"
                dtData.Rows.Add(row)
                dtData.AcceptChanges()
            ElseIf dr("vcPropertyName") = "Source" AndAlso str(0).ToLower = "opp" Then
                objCommon.DomainID = context.Session("DomainID")
                dtData = objCommon.GetOpportunitySource()
            ElseIf (dr("vcPropertyName") = "numPartner" Or dr("vcPropertyName") = "numPartenerSource") AndAlso str(0).ToLower = "opp" Then
                objCommon.DomainID = context.Session("DomainID")
                dtData = objCommon.GetPartnerSource()
            ElseIf dr("vcPropertyName") = "numPartenerContact" AndAlso str(0).ToLower = "opp" Then
                objCommon.DomainID = context.Session("DomainID")
                Dim _DivisonID As Long
                _DivisonID = objCommon.DivisionID
                objCommon.DivisionID = 0
                dtData = objCommon.GetPartnerContacts()
                objCommon.DivisionID = _DivisonID
            ElseIf dr("vcPropertyName") = "ShippingServiceName" Or dr("vcPropertyName") = "ShippingService" Then
                dtData = objCommon.GetDropDownValue(0, "PSS", "")
            ElseIf CCommon.ToString(dr("vcOrigDbColumnName")) = "vcWareHouse" Then
                dtData = objCommon.GetDropDownValue(CCommon.ToLong(context.Session("DomainID")), "", "vcWarehouse")
            ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                If dr("ListRelID") > 0 Then

                    objCommon.DomainID = context.Session("DomainID")
                    'Get PrimaryList Item ID for given record from database

                    objCommon.PrimaryListItemID = objCommon.GetPrimaryListItemID()
                    objCommon.SecondaryListID = dr("numListId")
                    objCommon.Mode = 3
                    dtData = objCommon.GetFieldRelationships.Tables(0)
                Else

                    'Added by: Sachin Sadhu||Date:19thJune2014
                    'Purpose :To fill Order status dropdown as per Order type
                    If (CCommon.ToInteger(dr("numListID")) = 176) Then 'Order status

                        Dim objOpportunity As New MOpportunity
                        Dim ds As New DataSet
                        objOpportunity.OpportunityId = str(3)
                        ds = objOpportunity.GetOrderType()
                        Dim strType As String = ds.Tables(0).Rows(0)("tintOppType").ToString
                        Dim tintOppOrOrder As Short = ds.Tables(0).Rows(0)("tintOppStatus").ToString
                        dtData = objCommon.GetMasterListItems(dr("numListID"), context.Session("DomainID"), CCommon.ToLong(strType), If(tintOppOrOrder = 1, 2, 1)) 'sales
                    ElseIf dr("vcPropertyName") = "FollowUpStatus" Then
                        objCommon.ListID = dr("numListID")
                        objCommon.DomainID = context.Session("DomainID")
                        dtData = objCommon.GetMasterListItemsWithRights()
                    Else
                        dtData = objCommon.GetMasterListItems(dr("numListID"), context.Session("DomainID"))
                    End If
                    'end of code

                End If

                If dr("vcPropertyName") = "Type" And str(0).ToLower = "tickler" Then

                    Dim dtDataFilter() As DataRow = dtData.Select("numListItemId = 974")
                    For Each drDelete As DataRow In dtDataFilter
                        drDelete.Delete()
                    Next
                    dtData.AcceptChanges()

                End If
            End If

            If Not dtData Is Nothing AndAlso dtData.Columns.Count > 0 AndAlso dtData.Select(dtData.Columns(0).ColumnName & "='0'").Length = 0 Then
                strData.Append("{""id"":""0"",""value"":""-- Select One --"",""group"":"""",""color"":""""},")
            End If

            If dr("vcPropertyName") = "FollowUpStatus" Then
                For Each rs As DataRow In dtData.Rows
                    strData.Append("{""id"":""" & rs(dtData.Columns(0).ColumnName).ToString().Trim() & """,""value"":""" & rs(dtData.Columns(1).ColumnName).ToString().Trim() & """,""group"":""" & rs(dtData.Columns(2).ColumnName).ToString().Trim() & """,""color"":""" & rs(dtData.Columns(3).ColumnName).ToString().Trim() & """},")
                Next
            Else
                For Each rs As DataRow In dtData.Rows
                    strData.Append("{""id"":""" & rs(dtData.Columns(0).ColumnName).ToString().Trim() & """,""value"":""" & rs(dtData.Columns(1).ColumnName).ToString().Trim() & """,""group"":"""",""color"":""""},")
                Next
            End If
        Next

        context.Response.ContentType = "application/json"
        context.Response.ContentEncoding = Encoding.UTF8
        context.Response.Write("[" & strData.ToString.Trim(",") & "]")
        context.Response.End()
    End Sub


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class