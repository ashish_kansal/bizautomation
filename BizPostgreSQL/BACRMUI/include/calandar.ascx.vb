﻿Imports System.Reflection
Imports BACRM.BusinessLogic.Common
Namespace BACRM.Include
    Partial Public Class calandar
        Inherits BACRMUserControl

        Private _ShowTime As String = "false"
        Private __blnIsRequired As Boolean = False
        Private __strDateName As String = ""
        Private _width As Double = 100

#Region "PUBLIC PROPERTIES"

        Public Property ShowTime() As String
            Get
                Return _ShowTime
            End Get
            Set(ByVal value As String)
                _ShowTime = value
            End Set
        End Property

        Public Property Enabled() As Boolean
            Get
                Return txtDate.Enabled
            End Get
            Set(ByVal value As Boolean)
                IIf(IsDBNull(value), value = False, value)
                txtDate.Enabled = value
            End Set
        End Property

        Public Property SelectedDate() As String
            Get
                Try
                    If txtDate.Text <> "" Then
                        If ShowTime = "12" Then
                            Return txtDate.Text
                        Else
                            Return DateFromFormattedDate(txtDate.Text, Session("DateFormat"))
                        End If
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            End Get
            Set(ByVal Value As String)
                Try
                    IIf(IsDBNull(Value), Value = "", Value)
                    If Value <> "" Then
                        If ShowTime = "12" Then
                            txtDate.Text = fn_GetDateTimeFromNumber(CDate(Value), Session("DateFormat"))
                        Else
                            txtDate.Text = FormattedDateFromDate(CDate(Value), Session("DateFormat"))
                        End If
                        'txtDate.Text = IIf(ShowTime = "12", fn_GetDateTimeFromNumber(CDate(Value), Session("DateFormat")), FormattedDateFromDate(CDate(Value), Session("DateFormat")))
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            End Set
        End Property

        Public Property IsRequired() As Boolean
            Get
                Return __blnIsRequired
            End Get
            Set(ByVal value As Boolean)
                __blnIsRequired = value
            End Set
        End Property

        Public Property DateName() As String
            Get
                Return __strDateName
            End Get
            Set(ByVal value As String)
                __strDateName = value
            End Set
        End Property

        Public Property Width() As Double
            Get
                Return _width
            End Get
            Set(ByVal value As Double)
                _width = value
                txtDate.Width = New Unit(_width, UnitType.Pixel)
            End Set
        End Property

#End Region

#Region "FORM EVENTS"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'If Not IsPostBack Then

                txtDate.Attributes.Add("onchange", "validateDate('" & txtDate.ClientID & "','" & CCommon.GetValidationDateFormat() & "');")
                If __blnIsRequired = True Then
                    txtDate.Attributes.Remove("class")
                    txtDate.Attributes.Add("class", "{required:true, messages:{required:'" + __strDateName + " is required!'}}")
                Else
                    txtDate.Attributes.Remove("class")
                    txtDate.Attributes.Add("class", "BizDate")
                End If

                If (Not Page.ClientScript.IsStartupScriptRegistered(Me.Page.[GetType](), "Script" & txtDate.ClientID)) Then
                    RegisterCalanderScript()
                End If

                'RegisterCalanderScript()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

#End Region

#Region "PUBLIC FUNCTIONS"

        Public Function RegisterCalanderScript() As String
            Try
                litScript.Text = ""

                If (Not Page.ClientScript.IsStartupScriptRegistered(Me.Page.[GetType](), "Script" & txtDate.ClientID)) Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Script" & txtDate.ClientID, GetCalanderScript(), False)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCalanderScript() As String
            Try
                Dim strCalanderScript As New System.Text.StringBuilder

                strCalanderScript.Append("<script>")
                strCalanderScript.Append("setupCal('" & imgDate.ClientID & "','" & txtDate.ClientID & "','" & CCommon.GetDateFormat() & IIf(ShowTime = "12", " %l:%M %p", "") & "'," & ShowTime() & ");")
                strCalanderScript.Append("</script>")
                Return strCalanderScript.ToString
            Catch ex As Exception
                Throw ex
            End Try
        End Function

#End Region

    End Class
End Namespace