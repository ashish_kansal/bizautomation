Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Public Class webmenu
    Inherits BACRMUserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Public intPopupHeight As Integer
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private _HideBreak As Boolean
    Public Property HideBreak() As Boolean
        Get
            Return _HideBreak
        End Get
        Set(ByVal value As Boolean)
            _HideBreak = value
        End Set
    End Property


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim objAdmin As New ActionItem
            If GetQueryStringVal("DivID") <> "" Then txtDivision.Value = GetQueryStringVal("DivID")
            Dim divid As String = GetQueryStringVal("DivId")

            If divid <> "" And divid <> Nothing Then txtDivision.Value = GetQueryStringVal("DivId")
            If GetQueryStringVal("CntID") <> "" Then txtContact.Value = GetQueryStringVal("CntID")

            Dim CntID As String = GetQueryStringVal("CntId")
            If CntID <> "" And CntID <> Nothing Then txtContact.Value = GetQueryStringVal("CntId")
            If GetQueryStringVal("ProID") <> "" Then txtPro.Value = GetQueryStringVal("ProID")
            If GetQueryStringVal("CaseID") <> "" Then txtCase.Value = GetQueryStringVal("CaseID")
            If GetQueryStringVal("OpID") <> "" Then txtOpp.Value = GetQueryStringVal("OpID")


            objAdmin.UserCntID = Session("UserContactID")
            DisplayRecentItems(objAdmin.GetRecentItems)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub DisplayRecentItems(ByVal dtRecentItems As DataTable)
        Try
            Dim strLastViewed As String = ""
            Dim length As Int16 = 25

            For i = 0 To dtRecentItems.Rows.Count - 1
                Dim strlink As String = "'"
                Dim strLastViewdItem As String = ""


                If dtRecentItems.Rows(i).Item("Type") = "C" Then
                    If dtRecentItems.Rows(i).Item("tintCRMType") = 0 Then
                        strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/Leads/frmLeads.aspx") & "?DivID=" & dtRecentItems.Rows(i).Item("numDivisionID") & """  title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"
                    ElseIf dtRecentItems.Rows(i).Item("tintCRMType") = 1 Then
                        strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/prospects/frmProspects.aspx") & "?DivID=" & dtRecentItems.Rows(i).Item("numDivisionID") & """ title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"
                    ElseIf dtRecentItems.Rows(i).Item("tintCRMType") = 2 Then
                        strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/Account/frmAccounts.aspx") & "?klds+7kldf=fjk-las&DivId=" & dtRecentItems.Rows(i).Item("numDivisionID") & """ title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """ ><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"
                    End If

                ElseIf dtRecentItems.Rows(i).Item("Type") = "U" Then
                    strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/contact/frmContacts.aspx") & "?CntId=" & dtRecentItems.Rows(i).Item("numContactID") & """ title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"

                ElseIf dtRecentItems.Rows(i).Item("Type") = "O" Then
                    strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/opportunity/frmOpportunities.aspx") & "?opId=" & dtRecentItems.Rows(i).Item("numRecID") & """ title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"

                ElseIf dtRecentItems.Rows(i).Item("Type") = "S" Then
                    strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/cases/frmCases.aspx") & "?CaseID=" & dtRecentItems.Rows(i).Item("numRecID") & """ title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"

                ElseIf dtRecentItems.Rows(i).Item("Type") = "P" Then
                    strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/projects/frmProjects.aspx") & "?ProId=" & dtRecentItems.Rows(i).Item("numRecID") & """ title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"

                ElseIf dtRecentItems.Rows(i).Item("Type") = "A" Then
                    Dim str As String
                    str = Page.ResolveClientUrl("~/admin/ActionItemDetailsOld.aspx") & "?"
                    str = str & "CommId=" & dtRecentItems.Rows(i).Item("numRecID") & "&CaseId=" & dtRecentItems.Rows(i).Item("CaseID") & "&CaseTimeId=" & dtRecentItems.Rows(i).Item("caseTimeId") & "&CaseExpId=" & dtRecentItems.Rows(i).Item("caseExpId")
                    strLastViewdItem = "<li><a href=""" & str & """ title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"

                ElseIf dtRecentItems.Rows(i).Item("Type") = "I" Then
                    strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/Items/frmKitDetails.aspx") & "?ItemCode=" & dtRecentItems.Rows(i).Item("numRecID") & "&frm=All Items" & """ title = """ & dtRecentItems.Rows(i).Item("RecName").ToString.Split("~")(0) & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString.Split("~")(0).ToString().TrimLength(length) & "</span></a></li>"
                    strlink = Page.ResolveClientUrl("~/Items/frmKitDetails.aspx") & "?ItemCode=" & dtRecentItems.Rows(i).Item("numRecID") & "&frm=All Items"

                ElseIf dtRecentItems.Rows(i).Item("Type") = "AI" Then
                    strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/Items/frmKitDetails.aspx") & "?AssetCode=" & dtRecentItems.Rows(i).Item("numRecID") & "&frm=frmItemList" & """ title = """ & dtRecentItems.Rows(i).Item("RecName").ToString.Split("~")(0).ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString.Split("~")(0).ToString().TrimLength(length) & "</span></a></li>"
                    strlink = Page.ResolveClientUrl("~/Items/frmKitDetails.aspx") & "?AssetCode=" & dtRecentItems.Rows(i).Item("numRecID") & "&frm=frmItemList"

                ElseIf dtRecentItems.Rows(i).Item("Type") = "D" Then
                    strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/Documents/frmDocuments.aspx") & "?DocId=" & dtRecentItems.Rows(i).Item("numRecID") & "&frm=DocList"" title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"
                    strlink = Page.ResolveClientUrl("~/Documents/frmDocuments.aspx") & "?DocId=" & dtRecentItems.Rows(i).Item("numRecID") & "&frm=DocList"

                ElseIf dtRecentItems.Rows(i).Item("Type") = "M" Then
                    strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/Marketing/frmCampaignDetails.aspx") & "?CampID=" & dtRecentItems.Rows(i).Item("numRecID") & "&frm=CampaignList"" title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"
                    strlink = Page.ResolveClientUrl("~/Marketing/frmCampaignDetails.aspx") & "?CampID=" & dtRecentItems.Rows(i).Item("numRecID") & "&frm=CampaignList"

                ElseIf dtRecentItems.Rows(i).Item("Type") = "R" Then
                    strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/opportunity/frmReturnDetail.aspx") & "?ReturnID=" & dtRecentItems.Rows(i).Item("numRecID") & "&title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"
                    strlink = Page.ResolveClientUrl("~/opportunity/frmReturnDetail.aspx") & "?ReturnID=" & dtRecentItems.Rows(i).Item("numRecID")
                ElseIf dtRecentItems.Rows(i).Item("Type") = "B" Then
                    strLastViewdItem = "<li><a href=""#"" onclick=""return OpenRecentBizInvoice(" & CCommon.ToLong(dtRecentItems.Rows(i).Item("numOppID")) & "," & CCommon.ToLong(dtRecentItems.Rows(i).Item("numRecID")) & ",0)"" ><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"
                ElseIf dtRecentItems.Rows(i).Item("Type") = "W" Then
                    strLastViewdItem = "<li><a href=""" & Page.ResolveClientUrl("~/items/frmWorkOrder.aspx") & "?WOID=" & dtRecentItems.Rows(i).Item("numRecID") & "&title = """ & dtRecentItems.Rows(i).Item("RecName").ToString() & """><img src=""" & Page.ResolveClientUrl(dtRecentItems.Rows(i).Item("vcImageURL")) & """ alt="""" />&nbsp;<span>" & dtRecentItems.Rows(i).Item("RecName").ToString().TrimLength(length) & "</span></a></li>"
                    strlink = Page.ResolveClientUrl("~/items/frmWorkOrder.aspx") & "?WOID=" & dtRecentItems.Rows(i).Item("numRecID")
                End If
                strLastViewed = strLastViewed + strLastViewdItem
            Next


            ltrlLastViewdItem.Text = strLastViewed
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
