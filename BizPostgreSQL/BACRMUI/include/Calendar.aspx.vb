Imports BACRM.BusinessLogic.Common
Partial Class Calender
    Inherits BACRMPage
    Protected WithEvents lblMonYear As System.Web.UI.WebControls.LinkButton


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Put user code to initialize the page here
            If Not IsPostBack Then
                
                Call FillCombo()
                If GetQueryStringVal( "Value") = "" Then
                    Dim strNow As Date
                    strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    Calendar1.VisibleDate = strNow
                Else
                    Calendar1.VisibleDate = DateFromFormattedDate(GetQueryStringVal( "Value"), Session("DateFormat"))
                    Calendar1.SelectedDate = DateFromFormattedDate(GetQueryStringVal( "Value"), Session("DateFormat"))
                End If
                ddlYear.ClearSelection()
                ddlYear.Items.FindByValue(Year(Calendar1.VisibleDate)).Selected = True
                ddlMonth.ClearSelection()
                ddlMonth.Items.FindByValue(Month(Calendar1.VisibleDate)).Selected = True
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub Calendar1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Try
            Dim kalenderdatum As String
            kalenderdatum = FormattedDateFromDate(Calendar1.SelectedDate, Session("DateFormat"))
            Dim script As String = "<script language=""javascript"">"
            Dim bodyscript As String = "opener.document.getElementById('" & GetQueryStringVal( "obj") & "').value = '" & kalenderdatum & "';"
            'Dim focus As String = "window.opener.document.getElementById('" & GetQueryStringVal( "obj") & "').focus();"
            Dim closer As String = "window.close();"
            Dim endscript As String = "</script>"
            Response.Write(script + bodyscript + closer + endscript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#Region "Navigation Method"

    Private Sub btnFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFirst.Click
        Try
            Calendar1.VisibleDate = DateAdd(DateInterval.Year, -1, Calendar1.VisibleDate)
            ddlYear.ClearSelection()
            ddlYear.Items.FindByValue(Year(Calendar1.VisibleDate)).Selected = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrev.Click
        Try
            Calendar1.VisibleDate = DateAdd(DateInterval.Month, -1, Calendar1.VisibleDate)
            ddlMonth.ClearSelection()
            ddlMonth.Items.FindByValue(Month(Calendar1.VisibleDate)).Selected = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            Calendar1.VisibleDate = DateAdd(DateInterval.Month, 1, Calendar1.VisibleDate)
            ddlMonth.ClearSelection()
            ddlMonth.Items.FindByValue(Month(Calendar1.VisibleDate)).Selected = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
        Try
            Calendar1.VisibleDate = DateAdd(DateInterval.Year, 1, Calendar1.VisibleDate)
            ddlYear.ClearSelection()
            ddlYear.Items.FindByValue(Year(Calendar1.VisibleDate)).Selected = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "Fill Combo"

    Private Sub FillCombo()
        Try
            Dim i As Integer
            For i = 1900 To 2100
                ddlYear.Items.Add(i)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Functional Method"

    Private Sub LinkButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Try
            Dim script As String = "<script language=""javascript"">"
            Dim bodyscript As String = "window.opener.document.getElementById('" & GetQueryStringVal( "obj") & "').value = '';"
            Dim focus As String = "window.opener.document.getElementById('" & GetQueryStringVal( "obj") & "').focus();"
            Dim closer As String = "window.close();"
            Dim endscript As String = "</script" & ">"
            Response.Write(script + focus + bodyscript + closer + endscript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub Calendar1_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
        Try
            If e.Day.Date = DateTime.Now.ToString("d") Then e.Cell.BackColor = System.Drawing.Color.LightGray

            If e.Day.IsOtherMonth = True Then
                e.Cell.ForeColor = System.Drawing.Color.FromName("White")
                e.Cell.Attributes.Add("onclick", "return false;")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Function getIndex(ByVal MonthName As String) As Integer
        Try
            Return Microsoft.VisualBasic.Switch(MonthName = "January", 0, MonthName = "February", 1, MonthName = "March", 2, MonthName = "April", 3, MonthName = "May", 4, MonthName = "June", 5, MonthName = "July", 6, MonthName = "August", 7, MonthName = "September", 8, MonthName = "October", 9, MonthName = "November", 10, MonthName = "December", 11)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function getMonth(ByVal intMonth As Int16) As String
        Try
            Select Case intMonth
                Case 1 : Return "Jan"
                Case 2 : Return "Feb"
                Case 3 : Return "Mar"
                Case 4 : Return "Apr"
                Case 5 : Return "May"
                Case 6 : Return "Jun"
                Case 7 : Return "Jul"
                Case 8 : Return "Aug"
                Case 9 : Return "Sep"
                Case 10 : Return "Oct"
                Case 11 : Return "Nov"
                Case 12 : Return "Dec"
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

    Private Sub ddlYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        Try
            Calendar1.VisibleDate = New Date(ddlYear.SelectedItem.Value, Calendar1.VisibleDate.Month, 15)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        Try
            Calendar1.VisibleDate = New Date(Calendar1.VisibleDate.Year, ddlMonth.SelectedItem.Value, 15)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
