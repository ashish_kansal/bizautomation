Imports BACRM.BusinessLogic.Common
Partial Public Class NormalCalendar
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            imgDate.Attributes.Add("onclick", "return OpenCalendar(" & txtDate.ClientID & ",'" & txtDate.ClientID & "');")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class