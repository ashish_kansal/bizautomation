Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Public Class frmSelRelationship
    Inherits BACRMPage
    Dim m_aryRightsForAdd() As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'btnclose.Attributes.Add("onclick", "return Close();")
            'If Not IsPostBack Then
            '    
            '    
            '    GetUserRightsForPage( 32, 2)
            '    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
            '        Response.Redirect("../admin/authentication.aspx?mesg=AC")
            '    Else : If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnNext.Visible = False
            '    End If
            '    Dim dtTab As DataTable
            '    dtTab = Session("DefaultTab")
            '    Dim dtTabDataMenu As DataTable
            '    If Session("TabDataMenu") Is Nothing Then
            '        Dim objTab As New Tabs
            '        objTab.ListId = 5
            '        objTab.DomainID = Session("DomainID")
            '        objTab.mode = False
            '        dtTabDataMenu = objTab.GetTabMenuItem()
            '        Session("TabDataMenu") = dtTabDataMenu
            '    Else : dtTabDataMenu = Session("TabDataMenu")
            '    End If
            '    radRelationship.DataTextField = "vcData"
            '    radRelationship.DataValueField = "numListItemID"
            '    radRelationship.DataSource = dtTabDataMenu
            '    radRelationship.DataBind()
            '    radRelationship.Items.Insert(0, dtTab.Rows(0).Item("vcLead").ToString)
            '    radRelationship.Items.FindByText(dtTab.Rows(0).Item("vcLead").ToString).Value = 1
            '    radRelationship.Items.Insert(1, dtTab.Rows(0).Item("vcProspect").ToString)
            '    radRelationship.Items.FindByText(dtTab.Rows(0).Item("vcProspect").ToString).Value = 3
            '    radRelationship.Items.Insert(2, dtTab.Rows(0).Item("vcAccount").ToString)
            '    radRelationship.Items.FindByText(dtTab.Rows(0).Item("vcAccount").ToString).Value = 2
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
    '    Try
    '        If radRelationship.SelectedIndex = -1 Then
    '            litMessage.Text = "Please select a Relationship"
    '        Else
    '            'If radRelationship.SelectedValue > 2 Then
    '            '    Response.Redirect("../include/frmNewOrganization.aspx?RelID=" & radRelationship.SelectedValue)
    '            'Else : Response.Redirect("../include/frmNewOrganization.aspx?CRMType=" & radRelationship.SelectedValue)
    '            'End If
    '            Dim lngFormId As Long

    '            Select Case radRelationship.SelectedValue
    '                Case 1
    '                    lngFormId = 34
    '                Case 3
    '                    lngFormId = 35
    '                Case Else
    '                    lngFormId = 36
    '            End Select

    '            Response.Redirect("../include/frmAddOrganization.aspx?RelID=" & radRelationship.SelectedValue & "&FormID=" & lngFormId)
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class