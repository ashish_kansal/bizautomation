﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SimpleSearchControl.ascx.vb" Inherits=".SimpleSearchControl" %>
<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<script type="text/javascript">
    function FocusSimpleSearchType() {
        var comboBox = $find('<%=radCmbSimpleSearch.ClientID%>');
        var input = comboBox.get_inputDomElement();
        input.focus();
    }

    function OnSearchClientSelectedIndexChanged(sender, eventArgs) {
        var item = eventArgs.get_item();
        var hdnSearchType = $("#<%=hdnSearchType.ClientID%>")

        if (hdnSearchType.val() == "4" || hdnSearchType.val() == "2") {
            $("#<%=hdnOppType.ClientID%>").val(item.get_attributes().getAttribute("orderType"));
        }
        else {
            $("#<%=hdnOppType.ClientID%>").val("");
        }
        $("#<%=btnGo.ClientID%>").click();
    }

    function OpenSimpleSearchOrg(a, b, c) {
        var str;
        if (b == 0) {
            str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=simpleSearch&DivID=" + a + '&profileid=' + c;
        }
        else if (b == 1) {
            str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=simpleSearch&DivID=" + a + '&profileid=' + c;
        }
        else if (b == 2) {
            str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=simpleSearch&klds+7kldf=fjk-las&DivId=" + a + '&profileid=' + c;
        }

        $("#mainframe")[0].contentWindow.location.href = str;
    }

    function OpenSimpleSearchItem(a) {
        $("#mainframe")[0].contentWindow.location.href = '../Items/frmKitDetails.aspx?ItemCode=' + a;
    }

    function OpenSimpleSearchContact(a) {
        var str;
        str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=simplesearch&ft6ty=oiuy&CntId=" + a;
        document.location.href = str;
    }

    function OpenSimpleSearchBizDoc(url) {
        window.open(url, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
    }
</script>
<style type="text/css">
    .rcbInput {
        height: 19px !important;
        padding: 0px !important;
    }

    /*input[type='radio'] {
        background-color: #FFFF99;
        color: Navy;
        position: relative;
        bottom: 3px;
        vertical-align: middle;
    }*/

    /** Columns */
    .rcbHeader ul,
    .rcbFooter ul,
    .rcbItem ul,
    .rcbHovered ul,
    .rcbDisabled ul {
        margin: 0;
        padding: 0;
        width: 100%;
        display: inline-block;
        list-style-type: none;
    }

    .rcbTemplate {
        border: 0 !important;
        border-bottom: 1px solid #cccccc !important;
    }

    .rowSimpleSearch {
        margin: 0px;
        padding: 10px;
    }
</style>

<asp:UpdatePanel ID="UpdatePanelSimpleSearch" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnGo">
            <div class="rowSimpleSearch">
                <div class="col-xs-12">
                    <div class="form-group">
                        <asp:RadioButtonList ID="rblOrganizationSearchCriteria" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Text="Start With" Value="1">
                            </asp:ListItem>
                            <asp:ListItem Text="Contains" Value="0">
                            </asp:ListItem>
                        </asp:RadioButtonList>
                        <p>
                            <telerik:RadComboBox AccessKey="C" ID="radcmbSearchText" DropDownWidth="600px" Width="200"
                            Skin="Default" runat="server" AllowCustomText="false" ShowMoreResultsBox="true" EnableLoadOnDemand="true"
                            EnableScreenBoundaryDetection="true" ItemsPerRequest="10" EnableVirtualScrolling="true"
                            OnItemsRequested="radcmbSearchText_ItemsRequested" HighlightTemplatedItems="true" ClientIDMode="Static"
                            TabIndex="503" MaxHeight="230" OnClientSelectedIndexChanged="OnSearchClientSelectedIndexChanged">
                        </telerik:RadComboBox>
                        </p>
                        <p>
                            <telerik:RadComboBox ID="radCmbSimpleSearch" runat="server" AutoPostBack="true" Width="200"
                                OnSelectedIndexChanged="radCmbSearch_SelectedIndexChanged"
                            TabIndex="500">
                            <Items>
                                <telerik:RadComboBoxItem Text="Organizations" Value="1" Selected="true" />
                                <telerik:RadComboBoxItem Text="Items" Value="2" />
                                <telerik:RadComboBoxItem Text="Opps/Orders" Value="3" />
                                <telerik:RadComboBoxItem Text="BizDocs" Value="4" />
                                <telerik:RadComboBoxItem Text="Contacts" Value="5" />
                            </Items>
                        </telerik:RadComboBox>
                        </p>
                        <p>
                            <telerik:RadComboBox ID="radcmbAdditionalFilter" runat="server" TabIndex="501" Width="200"
                            AutoPostBack="true" OnSelectedIndexChanged="radcmbAdditionalFilter_SelectedIndexChanged">
                            <Items>
                                <telerik:RadComboBoxItem Text="-- Search For --" Value="1" Selected="true" />
                                <telerik:RadComboBoxItem Text="Items" Value="2" />
                                <telerik:RadComboBoxItem Text="Opps/Orders" Value="3" />
                                <telerik:RadComboBoxItem Text="BizDocs" Value="4" />
                            </Items>
                        </telerik:RadComboBox>
                        </p>
                        <p>
                            <telerik:RadComboBox ID="radCmbOrderTypes" runat="server" Visible="false" TabIndex="502" AutoPostBack="true" Width="200">
                            <Items>
                                <telerik:RadComboBoxItem Text="-- Select All --" Value="0" Selected="true" />
                                <telerik:RadComboBoxItem Text="Sales Order" Value="1" />
                                <telerik:RadComboBoxItem Text="Purchase Order" Value="2" />
                                <telerik:RadComboBoxItem Text="Sales Opportunity" Value="3" />
                                <telerik:RadComboBoxItem Text="Purchase Opportunity" Value="4" />
                                <telerik:RadComboBoxItem Text="Sales Return" Value="5" />
                                <telerik:RadComboBoxItem Text="Purchase Return" Value="6" />
                            </Items>
                        </telerik:RadComboBox>
                        </p>
                        <p>
                            <telerik:RadComboBox ID="radcmbBizDocTypes" runat="server" Visible="false" TabIndex="502" AutoPostBack="true" Width="200">
                        </telerik:RadComboBox>
                        </p>

                        <asp:Button ID="btnGo" Text="Go" CssClass="btn btn-block btn-xs btn-primary" runat="server" TabIndex="504" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hdnSearchType" runat="server" />
        <asp:HiddenField ID="hdnOppType" runat="server" />
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnGo"  />
    </Triggers>
</asp:UpdatePanel>
