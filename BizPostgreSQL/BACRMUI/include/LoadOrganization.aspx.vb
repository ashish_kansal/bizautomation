Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Partial Public Class LoadOrganization
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub radCmbOrganization_ItemsRequested(ByVal o As Object, ByVal e As Telerik.WebControls.RadComboBoxItemsRequestedEventArgs) Handles radCmbOrganization.ItemsRequested
        Try
            If e.Text <> "" And Len(e.Text) >= IIf(Session("ChrForCompSearch") = 0, 1, Session("ChrForCompSearch")) Then

                Dim objCommon As New CCommon
                With objCommon
                    .DomainID = Session("DomainID")
                    .Filter = Trim(e.Text) & "%"
                    .UserCntID = Session("UserContactID")
                    radCmbOrganization.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                    radCmbOrganization.DataTextField = "vcCompanyname"
                    radCmbOrganization.DataValueField = "numDivisionID"
                    radCmbOrganization.DataBind()
                End With
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class