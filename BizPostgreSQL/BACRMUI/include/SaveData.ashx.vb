﻿Imports System.Web
Imports System.Web.Services
Imports System.Text
Imports BACRM.BusinessLogic.Common
Imports System.Web.Script.Serialization
Imports System.Collections.Specialized
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin

Public Class SaveData
    Implements System.Web.IHttpHandler
    Implements System.Web.SessionState.IRequiresSessionState

    Dim objCommon As New CCommon

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Try
            Dim nvc As NameValueCollection = context.Request.Form

            If nvc("id") Is Nothing Or nvc("value") Is Nothing Then
                Exit Sub
            End If

            Dim id As String = nvc("id")

            Dim str As String() = id.Split("~")

            If str(0).ToLower = "organization" Then
                objCommon.ContactID = str(3)
                objCommon.DivisionID = str(4)
                objCommon.RecordId = str(4)
            ElseIf str(0).ToLower = "contact" Then
                objCommon.ContactID = str(3)
                objCommon.DivisionID = str(4)

                objCommon.RecordId = str(3)
            ElseIf str(0).ToLower = "wo" Then
                objCommon.numWOId = str(3)
                objCommon.numWODetailId = str(4)

                objCommon.RecordId = str(3)
            ElseIf str(0).ToLower = "workorder" Then
                objCommon.numWOId = str(3)
                objCommon.RecordId = str(3)
            ElseIf str(0).ToLower = "project" Then
                objCommon.ProID = str(3)

                objCommon.RecordId = str(3)
            ElseIf str(0).ToLower = "opp" Then
                objCommon.OppID = str(3)

                objCommon.RecordId = str(3)
            ElseIf str(0).ToLower = "oppitems" Then
                objCommon.OppItemID = str(3)
                objCommon.RecordId = str(3)

                If str.Length >= 8 Then
                    objCommon.OppID = str(7)
                End If
            ElseIf str(0).ToLower = "case" Then
                objCommon.CaseID = str(3)

                objCommon.RecordId = str(3)
            ElseIf str(0).ToLower = "tickler" Then
                objCommon.CommID = str(4)
            ElseIf str(0).ToLower = "item" Then
                objCommon.RecordId = str(3)
            ElseIf str(0).ToLower = "customerpartno" Then
                objCommon.RecordId = str(3)
                objCommon.DivisionID = str(4)
            ElseIf str(0).ToLower = "checkregister" Then
                objCommon.RecordId = CCommon.ToLong(str(3))
            End If

            objCommon.PageName = str(0).ToLower

            objCommon.FormFieldId = str(1)
            objCommon.CustomField = str(2)
            objCommon.UserCntID = context.Session("UserContactID")
            objCommon.DomainID = context.Session("DomainID")
            objCommon.InlineEditValue = nvc("value")

            Dim dtPropertyData As DataTable
            dtPropertyData = objCommon.GetInLineEditPropertyName()

            If str(0).ToLower = "workorder" Then
                If Not dtPropertyData Is Nothing AndAlso dtPropertyData.Rows.Count > 0 Then
                    If CCommon.ToString(dtPropertyData.Rows(0)("vcOrigDbColumnName")) = "dtmStartDate" Or CCommon.ToString(dtPropertyData.Rows(0)("vcOrigDbColumnName")) = "dtmEndDate" Then
                        If Convert.ToDateTime(nvc("value")).Date = DateTime.Now.Date Then
                            Dim input As DateTime = Convert.ToDateTime(nvc("value")).Date
                            Dim now As DateTime = DateTime.Now
                            Dim output As DateTime = New DateTime(input.Year, input.Month, input.Day, 0, 0, 0)

                            objCommon.InlineEditValue = output.AddMinutes(CCommon.ToInteger(context.Session("ClientMachineUTCTimeOffset")))
                        Else
                            Dim input As DateTime = Convert.ToDateTime(nvc("value")).Date
                            Dim output As DateTime = New DateTime(input.Year, input.Month, input.Day, 0, 0, 0)

                            objCommon.InlineEditValue = output.AddMinutes(CCommon.ToInteger(context.Session("ClientMachineUTCTimeOffset")))
                        End If
                    End If
                End If
            ElseIf str(0).ToLower = "project" Then
                If Not dtPropertyData Is Nothing AndAlso dtPropertyData.Rows.Count > 0 Then
                    If CCommon.ToString(dtPropertyData.Rows(0)("vcOrigDbColumnName")) = "dtmStartDate" Or CCommon.ToString(dtPropertyData.Rows(0)("vcOrigDbColumnName")) = "dtmEndDate" Then
                        If Convert.ToDateTime(nvc("value")).Date = DateTime.Now.Date Then
                            Dim input As DateTime = Convert.ToDateTime(nvc("value")).Date
                            Dim now As DateTime = DateTime.Now
                            Dim output As DateTime = New DateTime(input.Year, input.Month, input.Day, 0, 0, 0)

                            objCommon.InlineEditValue = output.AddMinutes(CCommon.ToInteger(context.Session("ClientMachineUTCTimeOffset")))
                        Else
                            Dim input As DateTime = Convert.ToDateTime(nvc("value")).Date
                            Dim output As DateTime = New DateTime(input.Year, input.Month, input.Day, 0, 0, 0)

                            objCommon.InlineEditValue = output.AddMinutes(CCommon.ToInteger(context.Session("ClientMachineUTCTimeOffset")))
                        End If
                    End If
                End If
            ElseIf str(0).ToLower() = "contact" Then
                If Not dtPropertyData Is Nothing AndAlso dtPropertyData.Rows.Count > 0 Then
                    If CCommon.ToString(dtPropertyData.Rows(0)("vcOrigDbColumnName")) = "vcPassword" Then
                        objCommon.InlineEditValue = objCommon.Encrypt(CCommon.ToString(nvc("value")))
                    End If
                End If
            End If

            Dim isDuplicateEmail As Boolean = False
            'Right now we are allowing user to add duplicate contact email but in future we may want to restrict it
            'If objCommon.FormFieldId = 53 Then 'Contact Email
            '    Dim objContact As New BACRM.BusinessLogic.Contacts.CContacts
            '    objContact.DomainID = objCommon.DomainID
            '    objContact.ContactID = objCommon.ContactID
            '    objContact.Email = objCommon.InlineEditValue
            '    isDuplicateEmail = objContact.IsDuplicateEmail()
            'End If

            If objCommon.FormFieldId = 53 AndAlso isDuplicateEmail Then
                Throw New Exception("DUPLICATE_EMAIL")
            Else
                If objCommon.FormFieldId = 101 And CCommon.ToLong(objCommon.InlineEditValue) = 15447 Then
                    Dim dtBizDocId As DataTable = objCommon.GetBizDocIdsForOrderStatus(objCommon.RecordId)

                    If (dtBizDocId IsNot Nothing And dtBizDocId.Rows.Count > 0) Then
                        If Not (CCommon.ToLong(dtBizDocId.Rows(0)("Invoice")) = 287 And CCommon.ToLong(dtBizDocId.Rows(0)("PackingSlip")) = 29397) Then
                            Throw New Exception("INVOICE/PACKINGSLIP_NOT_FOUND")
                        End If
                    Else
                        Throw New Exception("INVOICE/PACKINGSLIP_NOT_FOUND")
                    End If
                End If

                Dim returnValue As String = ""
                returnValue = CCommon.ToString(objCommon.SaveInLineEditData())

                ''Added By Sachin Sadhu||Date:29thApril12014
                ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                ''          Using Change tracking
                If str(0).ToLower = "opp" Then
                    Dim objWfA As New Workflow()
                    objWfA.DomainID = context.Session("DomainID")
                    objWfA.UserCntID = context.Session("UserContactID")
                    objWfA.RecordID = CCommon.ToLong(str(3))
                    objWfA.SaveWFOrderQueue()

                    'Create default bizdoc as per selected in Domain Details order settings, if deal is won then creating bizdocs
                    If objCommon.FormFieldId = 109 AndAlso CCommon.ToShort(objCommon.InlineEditValue) = 1 Then
                        Dim objOppBizDocs As New OppBizDocs
                        Dim objOpportunity As New OppotunitiesIP
                        Dim shouldReturn As Boolean
                        Dim OppBizDocID As Long = 0
                        Try
                            objOpportunity.OpportunityId = CCommon.ToLong(str(3))
                            objOpportunity.DomainID = context.Session("DomainID")
                            objOpportunity.ClientTimeZoneOffset = context.Session("ClientMachineUTCTimeOffset")
                            objOpportunity.OpportunityDetails()

                            objOppBizDocs.OppId = objOpportunity.OpportunityId
                            objOppBizDocs.OppType = objOpportunity.OppType
                            If (objOppBizDocs.OppType = frmNewOrder.PageTypeEnum.Sales) Then
                                shouldReturn = False
                            End If

                            objOppBizDocs.UserCntID = context.Session("UserContactID")
                            objOppBizDocs.DomainID = context.Session("DomainID")
                            objOppBizDocs.vcPONo = "" 'txtPO.Text
                            objOppBizDocs.vcComments = "" 'txtComments.Text
                            objOppBizDocs.BizDocStatus = CCommon.ToLong(context.Session("SOBizDocStatus"))

                            Dim objAdmin As New CAdmin
                            Dim dtBillingTerms As DataTable
                            objAdmin.DivisionID = objOpportunity.DivisionID
                            dtBillingTerms = objAdmin.GetBillingTerms()

                            'IMPORTANT - CREATE BIZDOC CODE IS REMOVE BECAUSE USER CAN NOW USE WORKFLOW WORKLFLOW AUTOMATION AUTOMATION RULES
                            'TO GET ORIGINAL CODE CHECK TFS VERSION CHECKED IN BEFORE 16 DECEMBER 2014
                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If

                ElseIf str(0).ToLower = "organization" Then
                    'Added By Sachin Sadhu||Date:22ndMay12014
                    'Purpose :To Added Organization data in work Flow queue based on created Rules
                    '          Using Change tracking
                    Dim objWfA As New Workflow()
                    objWfA.DomainID = context.Session("DomainID")
                    objWfA.UserCntID = context.Session("UserContactID")
                    objWfA.RecordID = str(4)
                    objWfA.SaveWFOrganizationQueue()
                    'end of code
                ElseIf str(0).ToLower = "contact" Then
                    'Added By Sachin Sadhu||Date:24thJuly2014
                    'Purpose :To Added Contact data in work Flow queue based on created Rules
                    '         Using Change tracking
                    Dim objWF As New Workflow()
                    objWF.DomainID = context.Session("DomainID")
                    objWF.UserCntID = context.Session("UserContactID")
                    objWF.RecordID = str(3)
                    objWF.SaveWFContactQueue()
                    'End of Code

                    If Not dtPropertyData Is Nothing AndAlso dtPropertyData.Rows.Count > 0 Then
                        If CCommon.ToString(dtPropertyData.Rows(0)("vcOrigDbColumnName")) = "vcPassword" Then
                            returnValue = CCommon.ToString(nvc("value"))
                        End If
                    End If
                ElseIf str(0).ToLower = "project" Then
                    If Not dtPropertyData Is Nothing AndAlso dtPropertyData.Rows.Count > 0 Then
                        If CCommon.ToString(dtPropertyData.Rows(0)("vcOrigDbColumnName")) = "dtmStartDate" Or CCommon.ToString(dtPropertyData.Rows(0)("vcOrigDbColumnName")) = "dtmEndDate" Then
                            Dim output As DateTime

                            If DateTime.TryParse(returnValue, output) Then
                                returnValue = Convert.ToDateTime(nvc("value")).AddMinutes(-1 * Convert.ToInt32(context.Session("ClientMachineUTCTimeOffset"))).ToString(CCommon.GetValidationDateFormat())
                            End If
                        End If
                    End If

                    Dim objWfA As New Workflow()
                    objWfA.DomainID = context.Session("DomainID")
                    objWfA.UserCntID = context.Session("UserContactID")
                    objWfA.RecordID = CCommon.ToLong(str(3))
                    objWfA.SaveWFProjectsQueue()
                    'end of code
                ElseIf str(0).ToLower = "case" Then
                    'Added By Sachin Sadhu||Date:29thJul2014
                    'Purpose :To Added Projects data in work Flow queue based on created Rules
                    '          Using Change tracking
                    Dim objWfA As New Workflow()
                    objWfA.DomainID = context.Session("DomainID")
                    objWfA.UserCntID = context.Session("UserContactID")
                    objWfA.RecordID = CCommon.ToLong(str(3))
                    objWfA.SaveWFCasesQueue()
                    'end of code
                ElseIf str(0).ToLower = "workorder" Then
                    If Not dtPropertyData Is Nothing AndAlso dtPropertyData.Rows.Count > 0 Then
                        If CCommon.ToString(dtPropertyData.Rows(0)("vcOrigDbColumnName")) = "dtmStartDate" Or CCommon.ToString(dtPropertyData.Rows(0)("vcOrigDbColumnName")) = "dtmEndDate" Then
                            Dim output As DateTime

                            If DateTime.TryParse(returnValue, output) Then
                                returnValue = Convert.ToDateTime(nvc("value")).AddMinutes(-1 * Convert.ToInt32(context.Session("ClientMachineUTCTimeOffset"))).ToString(CCommon.GetValidationDateFormat())
                            End If
                        End If
                    End If
                End If

                context.Response.Write(returnValue)

            End If
        Catch ex As Exception
            If ex.Message.Contains("PERCENT_COMPLETE_MUST_BE_100") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "Percentage complete must be 100 to set release date."
            ElseIf ex.Message.Contains("REMOVE_RELEASE_DATE") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "You have to remove the Order Release Date before you can reduce the percentage."
            ElseIf ex.Message.Contains("EMAIL_ADDRESS_REQUIRED") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "Email address required."
            ElseIf ex.Message.Contains("DUPLICATE_EMAIL") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "DUPLICATE_EMAIL_" & objCommon.InlineEditValue
            ElseIf ex.Message.Contains("INVOICE/PACKINGSLIP_NOT_FOUND") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "An Invoice and Fulfillment Order (aka Packing Slip) BizDoc must be added before you can send an 856 or 810/ASN."
            ElseIf ex.Message.Contains("DUPLICATE_CHECK_NUMBER") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "Another record with same check # already exists."
            ElseIf ex.Message.Contains("WORKORDR_QUANITY_CAN_NOT_BE_LESS_THEN_PICKED_QTY") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "Work order quantity can not be less then already picked quantity."
            ElseIf ex.Message.Contains("TASKS_ARE_ALREADY_STARTED") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "You can't change Work order quantity after tasks are started."
            ElseIf ex.Message.Contains("WORKORDR_COMPLETED") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "You can't change Work order quantity after work order is completed.."
            ElseIf ex.Message.Contains("WAREHOUSE_DOES_NOT_EXISTS") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "Selected warehouse does not exists."
            ElseIf ex.Message.Contains("WAREHOUSE_DOES_NOT_EXISTS_IN_ITEM") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "Selected warehouse does not exists for item(s)."
            ElseIf ex.Message.Contains("AUTHORITATIVE_BIZDOC_EXISTS") Then
                context.Response.StatusCode = 400
                context.Response.StatusDescription = "You can't change price once authoritative bizdoc added for item."
            Else
                context.Response.StatusCode = 403
                context.Response.Write(ex.Message)
            End If
        End Try
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property




End Class