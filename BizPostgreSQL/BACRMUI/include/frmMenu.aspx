<%@ Page Language="vb" AutoEventWireup="false" MaintainScrollPositionOnPostback="true"
    CodeBehind="frmMenu.aspx.vb" Inherits=".frmMenu" %>

<%@ Register TagPrefix="menu1" TagName="Menu" Src="../include/webmenu.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="~/CSS/NewUiStyle.css" type="text/css" />
    <link href="~/images/BizSkin/TabStrip.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <%--<script type="text/javascript" src="../javascript/ig_webscheduleinfo.js"></script>--%>
    <script type="text/javascript" language="javascript">
        //Calling function from frame:parent.frames['topframe'].SelectTabByName('Support')
        function SelectTabByValue(Id) {
            var tabStrip = $find("RadTabStrip1");
            var tab = tabStrip.findTabByValue(Id);
            if (tab) {
                tab.select();
            }
        }
        function UnSelectTabs() {
            var tabStrip = $find("RadTabStrip1");

            var tab = tabStrip.get_selectedTab();
            if (tab) {
                tab.unselect();
            }
        }


        //myFunc is being called from frmTicklerdisplay.aspx
        function myFunc(a) {

            var scheduleInfo = ig_getWebScheduleInfoById("WebScheduleInfo1");
            //alert(a)   
            /*  alert(scheduleInfo.getActivities().getItemFromKey(a).getIsVariance())  
            alert(scheduleInfo.getActivities().getItemFromKey(a))  */

            if (scheduleInfo.getActivities().getItemFromKey(a) == null) {

                document.form1.btnGo1.click();
                setTimeout('ig_getWebScheduleInfoById("WebScheduleInfo1").showUpdateAppointmentDialog(' + a + ',"")', 2500)

            }
            else {
                ig_getWebScheduleInfoById("WebScheduleInfo1").showUpdateAppointmentDialog(a, "")
            }


            /*if  (scheduleInfo._isInitializing == false)
            {
            document.getElementById('btnLoadCal').click();
            var i =0;
                   
            //scheduleInfo.showUpdateAppointmentDialog(a,"")
            }*/
            //scheduleInfo.showUpdateAppointmentDialog(a,"");
            return false
        }

        function myFuncOut(a) {
            var scheduleInfo = ig_getWebScheduleInfoById("WebScheduleInfo1");
            if (scheduleInfo.getActivities().getItemFromKey(a) == null) {
                setTimeout('', 5000);
                document.form1.btnGo2.click();

            }

        }
        function WebScheduleInfo1_ActivityUpdating(oScheduleInfo, oEvent, oActivityUpdateProps, oActivity, id) {

        }
        function OpenConf() {
            var tabStrip = $find("RadTabStrip1");
            var selectedTab = tabStrip.get_selectedTab();

            var tab = 0;
            if (selectedTab != null)
                tab = selectedTab.get_value();

            window.open("../admin/frmShortcutMngUsr.aspx?tab=" + tab, '', 'toolbar=no,titlebar=no,left=150, top=150,scrollbars=yes,resizable=yes')
            return false;
        }
        function Click_Button() {
            document.form1.btnGo1.click();
        }
        function OpenInMainFrame(url) {
            top.frames["mainframe"].location.href = url;
            return false;
        }

        var PopupURL;
        PopupURL = '';
        var Popup;
        var sOppType;
        sOppType = 0;
        function Goto(target, frame, div, a, b) {
            var Oper;
            var checkURL;
            checkURL = target;

            if (target.split('?').length != 1) {
                Oper = '&';
            }
            else {
                Oper = '?';
            }
            if (parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtDivision') == null) {
                if (parent.frames['mainframe'].frames['rightFrame'] != null) {
                    if (parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                        target = target + Oper + 'rtyWR=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision').value + '&uihTR=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtContact').value + '&tyrCV=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtPro').value + '&pluYR=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtOpp').value + '&fghTY=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtCase').value
                    }
                    else {
                        target = target + Oper + 'I=1'
                    }
                }

                else {
                    target = target + Oper + 'I=1'
                }
            }
            else {
                if (parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                    target = target + Oper + 'rtyWR=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtDivision').value + '&uihTR=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtContact').value + '&tyrCV=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtPro').value + '&pluYR=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtOpp').value + '&fghTY=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtCase').value
                }
            }

            //alert(checkURL);
            sOppType = 0;
            if (checkURL == '../opportunity/frmNewSalesOrder.aspx') {
                sOppType = 1;
            }
            else if (checkURL == '../Opportunity/frmNewPurchaseOrder.aspx') {
                sOppType = 2;
            }
            //alert(sOppType);

            if (checkURL == '../opportunity/frmNewSalesOrder.aspx' || checkURL == '../Opportunity/frmNewPurchaseOrder.aspx') {
                PopupURL = target;
                Popup = window.open(target, "popNewOrder", 'toolbar=no,titlebar=no,left=150, top=150,width=' + a + ',height=' + b + ',scrollbars=yes,resizable=yes')
                $('#hdnPopupURL').val(Popup);
            }
            else {
                window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=' + a + ',height=' + b + ',scrollbars=yes,resizable=yes')
            }

        }
        function gotoOpp(target, frame, div, a, b) {
            var Oper;
            if (target.split('?').length != 1) {
                Oper = '&';
            }
            else {
                Oper = '?';
            }

            if (parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtDivision') == null) {
                if (parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                    target = target + Oper + 'rtyWR=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision').value + '&uihTR=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtContact').value + '&tyrCV=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtPro').value + '&pluYR=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtOpp').value + '&fghTY=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtCase').value
                }

            }
            else {
                if (parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                    target = target + Oper + 'rtyWR=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtDivision').value + '&uihTR=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtContact').value + '&tyrCV=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtPro').value + '&pluYR=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtOpp').value + '&fghTY=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtCase').value
                }
                //frames[frame].location.href=target;
                //document.getElementById(div).style.visibility= 'visible'; 

            }
            window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=' + a + ',height=' + b + ',scrollbars=yes,resizable=yes')

        }
        var pop;
        var RPPopup;

        function CloseReceivePayment(a) {
            //console.log(parent.frames['mainframe'].frames['rightFrame']);
            //console.log(parent.frames['mainframe'].frames['rightFrame'].RPPopup);

            if (a.split('frmAmtPaid').length > 1) {
                if (parent.frames['mainframe'] != undefined && parent.frames['mainframe'].frames['rightFrame'] != undefined) {
                    if (parent.frames['mainframe'].frames['rightFrame'].RPPopup != undefined) {
                        pop = parent.frames['mainframe'].frames['rightFrame'].RPPopup;
                    }
                    else if (parent.frames['mainframe'].frames['rightFrame'].document.RPPopup != undefined) {
                        pop = parent.frames['mainframe'].frames['rightFrame'].document.RPPopup;
                    }
                    else if (document.RPPopup != undefined) {
                        pop = document.RPPopup;
                    }
                    else {
                        return;
                    }
                }
                else {
                    return;
                }

                console.log(pop);
                if (pop == undefined) {
                    return;
                }
                else {
                    console.log(pop.name);
                    pop.close();
                }
            }
        }

        function reDirect(a, b) {
            var target = '';
            var Oper;

            CloseReceivePayment(a);

            if (b == 1) {
                if (a.split('?').length != 1) {
                    Oper = '&';
                }
                else {
                    Oper = '?';
                }
                if (parent.frames['mainframe'].frames.length == 0) {
                    if ((parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtDivision') != undefined && parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtDivision').value != null) ||
                        (parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtContact') != undefined && parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtContact').value != null) ||
                        (parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtPro') != undefined && parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtPro').value != null) ||
                        (parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtOpp') != undefined && parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtOpp').value != null) ||
                        (parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtCase') != undefined && parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtCase').value != null)
                        ) {
                        target = Oper + 'DivID=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtDivision').value
                        + '&CntID=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtContact').value
                        + '&ProID=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtPro').value
                        + '&OpID=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtOpp').value
                        + '&CaseID=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtCase').value

                    } else {
                        target = ''
                    }
                }
                else {
                    if (parent.frames['mainframe'] != undefined && parent.frames['mainframe'].frames['rightFrame'] != null) {
                        if (parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                            target = Oper + 'DivID=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtDivision').value + '&CntID=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtContact').value + '&ProID=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtPro').value + '&OpID=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtOpp').value + '&CaseID=' + parent.frames['mainframe'].frames['rightFrame'].document.getElementById('ctl00_webmenu1_txtCase').value
                        }
                        else {
                            target = ''
                        }
                    }
                    else {
                        if (parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtDivision') != null) {
                            target = Oper + 'DivID=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtDivision').value + '&CntID=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtContact').value + '&ProID=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtPro').value + '&OpID=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtOpp').value + '&CaseID=' + parent.frames['mainframe'].document.getElementById('ctl00_webmenu1_txtCase').value
                        }
                        //frames[frame].location.href=target;
                        //document.getElementById(div).style.visibility= 'visible'; 

                    }
                }
            }

            console.log(target);
            //if (a.split('frmAmtPaid').length > 1) {
            //    target = '';
            //}

            if (a.split('TimeAndExpense').length == 2) {
                parent.frames['mainframe'].location.href = a;
            }
            else {

                if (target == "") {

                    //                    if (a.split('?').length == 1) {
                    //                        parent.frames['mainframe'].location.href = a + '?I=1';
                    //                    }
                    //                    else {
                    //                        parent.frames['mainframe'].location.href = a + '&I=1';
                    //                    }

                    parent.frames['mainframe'].location.href = a;
                }
                else {
                    parent.frames['mainframe'].location.href = a + target;
                }
            }
            
            return false;
        }
        function OpenPop(target) {
            window.open(target + '?R=1', '', 'toolbar=no,titlebar=no,left=150, top=150,width=750,height=450,scrollbars=yes,resizable=yes')
        }
        //How to call-> top.frames[0].GenericPopup('frmAccountsReceivableDetails.aspx?DomainId=' + a + '&DivisionID=' + b + '&Flag=' + c,'toolbar=no,titlebar=no,top=100,left=100,width=850,height=300,scrollbars=yes,resizable=yes')
        function GenericPopup(target, settings) {
            window.open(target, '', settings);
            return;
        }

        function Login() {
            if (parent.parent.frames.length > 0) {
                parent.parent.document.location.href = "../Login.aspx"
            }
            else if (parent.frames.length > 0) {
                parent.document.location.href = "../Login.aspx"
            }
            else {
                document.location.href = "../Login.aspx"
            }


            //parent.location.href('common/frmticklerdisplay.aspx?ClientMachineUTCTimeOffset='+new Date().getTimezoneOffset())
            return false;
        }
        function reDirectPage(a) {
            parent.frames['mainframe'].location.href = a;
        }
        var reminderPopUp;
        function OpenRemindersWindow() {
            if (reminderPopUp && !reminderPopUp.closed) {
                reminderPopUp.close();
            }
            reminderPopUp = window.open("../OutlookCalendar/Reminder.aspx", 'BizAutomation_Reminder_Popup', 'width=800,height=600,status=no,scrollbar=yes,top=110,left=150');
            reminderPopUp.focus();
        }
        function OpenAlertsWindow() {
            window.open("../WorkFlow/frmShowAlerts.aspx", '', 'width=800,height=600,status=no,scrollbar=yes,top=110,left=150')
        }
        function SetTextRemider(id, type, time) {
            $("#divReminder").show()
            $("#hdnActionItemID").val(id);
            $("#hdnType").val(type);
            $("#lblReminderTime").text(time);
        }
        function ClearTextRemider() {
            $("#divReminder").hide()
            $("#hdnActionItemID").val("");
            $("#hdnType").val("");
            $("#lblReminderTime").text("");
        }
    </script>
    <script src="../JavaScript/TabMenu.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.idletimer.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.idletimeout.js" type="text/javascript"></script>
    <%--<script src="../JavaScript/jquery.shortkeys.js" type="text/javascript"></script>--%>
    <style type="text/css">
        #idletimeout
        {
            background: #515E81;
            border: 3px solid #8B96B6;
            color: #fff;
            font-family: arial, sans-serif;
            text-align: center;
            font-size: 12px;
            padding: 10px;
            position: relative;
            top: 0px;
            left: 0;
            right: 0;
            z-index: 100000;
            display: none;
        }

            #idletimeout a
            {
                color: #fff;
                font-weight: bold;
            }

            #idletimeout span
            {
                font-weight: bold;
            }

        .alignbottom
        {
            vertical-align: text-bottom;
        }

        .navnext
        {
            width: 18px;
            height: 18px;
            background: url("../images/BizSkin/TabStrip/TabStripStates.png") -18px -208px;
            position: absolute;
            top: 0px;
            right: 0px;
            margin-top: -8px;
        }

        .navprev
        {
            width: 18px;
            height: 18px;
            background: url("../images/BizSkin/TabStrip/TabStripStates.png") 0px -208px;
            position: absolute;
            top: 0px;
            right: 18px;
            margin-top: -8px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="idletimeout">
            You will be logged off in <span>
                <!-- countdown place holder -->
            </span>&nbsp;seconds due to inactivity. <a id="idletimeout-resume" href="#">Click here
            to continue using BizAutomation.com</a>.
        </div>
        <telerik:RadScriptManager ID="ScriptManager" runat="server" />
        <asp:Button runat="server" ID="btnLoadCal" Style="display: none" />
        <asp:Button runat="server" ID="btnGo1" Style="display: none" />
        <asp:Button runat="server" ID="btnGo2" Style="display: none" />
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img src="../images/New LogoBiz1.JPG">
                </td>
                <td style="white-space: nowrap; text-align: center" width="100%">
                    <div id="divReminder" style="display: none;">
                        <asp:Label ID="lblReminder" ForeColor="Red" runat="server" Text="You have an action item at: "></asp:Label>&nbsp;<asp:Label ID="lblReminderTime" ForeColor="Red" runat="server"></asp:Label>&nbsp;<asp:Button ID="btnDismiss" runat="server" CssClass="button" Style="padding-left: 5px !important;" Text="Dismiss" />
                    </div>

                    <asp:HiddenField ID="hdnActionItemID" runat="server" />
                    <asp:HiddenField ID="hdnType" runat="server" />
                </td>
                <td align="right" style="white-space: nowrap">
                    <table>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td id="tdEditHelp" runat="server" visible="false">
                                            <asp:HyperLink NavigateUrl="#" CssClass="text_bold" ID="hplEditHelp" runat="server" Visible="false">Edit Help</asp:HyperLink>
                                            <%--|--%>
                                        </td>
                                        <td id="tdSubscription" runat="server" visible="false">
                                            <asp:HyperLink NavigateUrl="#" CssClass="text_bold" ID="hplSubscription" runat="server">Subscription</asp:HyperLink>
                                            |
                                        </td>
                                        <td id="tdAdmin" runat="server">
                                            <asp:HyperLink NavigateUrl="#" CssClass="text_bold" ID="hplAdministration" runat="server">Administration</asp:HyperLink>
                                            |
                                        </td>
                                        <%--<td id="tdImport" runat="server">
                            <asp:HyperLink NavigateUrl="#" CssClass="text_bold" ID="hplImport" runat="server">Import</asp:HyperLink>
                            |
                        </td>--%>
                                        <td id="tdAdvanced" runat="server">
                                            <asp:HyperLink NavigateUrl="#" CssClass="text_bold" ID="hplAdvanced" runat="server">Advanced Search</asp:HyperLink>
                                            <%--|--%>
                                        </td>
                                        <td>
                                            <asp:HyperLink CssClass="text_bold" NavigateUrl="#" ID="hplHelp" runat="server" Visible="false">Help</asp:HyperLink>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <img height="16" src="../images/search_icon.gif" width="16">
                                        </td>
                                        <td style="white-space: nowrap" align="left">
                                            <input class="signup" id="txtSearch1" type="text" runat="server" name="txtSearch1">
                                            <asp:DropDownList ID="ddlSearch1" Visible="false" CssClass="signup" runat="server">
                                            </asp:DropDownList>
                                            <asp:CheckBox ID="chkSearch1" runat="server" Visible="false" />
                                            <asp:DropDownList ID="ddlSearch" CssClass="signup" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnGo" Text="Go" CssClass="button" runat="server" />
                                        </td>

                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblSubMessage" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblAlertMessage" runat="server" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lblLoggedin" runat="server"></asp:Label>
                                <asp:LinkButton ID="lnkbtnLogOut" runat="server" Text="Logout" CssClass="PurpleText"></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="100%" border="0">
            <tr>
                <td>
                    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" EnableEmbeddedSkins="false"
                        Skin="BizSkin" SelectedIndex="0" ScrollChildren="true" OnClientLoad="ClientTabstripLoad">
                        <Tabs>
                        </Tabs>
                    </telerik:RadTabStrip>
                </td>
            </tr>
            <tr class="SearchStyle">
                <td height="20px" valign="top">
                    <table class="ShortCutBar" runat="server" id="Table1" cellspacing="0" cellpadding="0"
                        width="100%" border="0" height="20px">
                        <tr>
                            <td class="Text_W" runat="server" align="left" id="tdLink" width="94%" nowrap>
                                <%--<div id="div1" style="overflow: hidden; width: 66%; float: left;" nowrap>
                            </div>
                            <div style="width: 2%; float: left;">
                                <a href="#" onmouseover="scrollDivRight('divLink')" onmouseout="stopMe()">�</a>
                                | <a href="#" onmouseover="scrollDivLeft('divLink')" onmouseout="stopMe()">�</a></div>
                            <div id="div2" runat="server" style="overflow: hidden; width: 30%; float: left;" nowrap>
                            </div>
                            <div style="width: 2%; float: left;">
                                <a href="#" onmouseover="scrollDivRight('divFavourite')" onmouseout="stopMe()">�</a>
                                | <a href="#" onmouseover="scrollDivLeft('divFavourite')" onmouseout="stopMe()">�</a></div>--%>
                                <div id="divTop" style="overflow: hidden; white-space: nowrap; display: block;" nowrap>
                                    <div id="divLink" style="float: left; vertical-align: bottom;">
                                    </div>
                                    <div id="divFavourite" runat="server" style="vertical-align: bottom;">
                                    </div>
                                </div>
                            </td>
                            <%--  <td class="Text_W" align="left" width="2%">
                            <a href="#" onmouseover="scrollDivRight('divLink')" onmouseout="stopMe()">�</a>
                            | <a href="#" onmouseover="scrollDivLeft('divLink')" onmouseout="stopMe()">�</a>
                        </td>
                        <td class="Text_W" runat="server" align="right" id="tdFavourite" width="30%" nowrap>
                            <div id="divFavourite" runat="server" style="overflow: hidden; width: 450px;" nowrap>
                            </div>
                        </td>--%>
                            <td align="left">
                                <div style="position: absolute;">
                                    <a href="#" onmouseover="scrollDivRight('divTop')" onmouseout="stopMe()" class='navprev'></a><a href="#" onmouseover="scrollDivLeft('divTop')" onmouseout="stopMe()" class="navnext"></a>
                                </div>
                            </td>
                            <td class="Text_WB" align="right" width="6%">
                                <asp:HyperLink runat="server" ID="hypConfMenu" ToolTip="Manage shortcut bar"><img src="../images/Settings.png" style="cursor: pointer;"/></asp:HyperLink>&nbsp;&nbsp;
                            <img style="cursor: pointer; margin-right: 5px;" title="submit feedback" src="../images/UserFeeback.png"
                                border="0" onclick="reDirectPage('../common/frmReportBug.aspx?Mode=1')" />
                                <img style="cursor: pointer; margin-right: 5px;" title="Report a Bug" src="../images/bug1.png"
                                    border="0" onclick="reDirectPage('../common/frmReportBug.aspx?Mode=0')" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hdnSessionTimeout" runat="server" />
        <asp:HiddenField ID="hdnPopupURL" runat="server" />

        <script type="text/javascript">
            setMenuWidth();
            RemindCall();
            // AlertCall();

            $(window).resize(function () {
                setMenuWidth();
            });

            function setMenuWidth() {
                document.getElementById('RadTabStrip1').style.width = $(window).width() - 30 + "px";
                document.getElementById('RadTabStrip1').style.height = "26px";
                document.getElementById('divTop').style.width = $(window).width() - 130 + "px";

                //            document.getElementById('tdLink').style.width = $(window).width() / 2 + "px";
                //            document.getElementById('tdFavourite').style.width = $(window).width() / 3 + "px";

                //            document.getElementById('divLink').style.width = $(window).width() / 2 + "px";
                //            document.getElementById('divFavourite').style.width = $(window).width() / 3 + "px";
            }

            $(function () {
                $.idleTimeout('#idletimeout', '#idletimeout a', {
                    warningLength: 300, //5min show warning before 5 mins
                    idleAfter: $("#hdnSessionTimeout").val(), //45 mins=2700secs
                    pollingInterval: 600, //10 mins
                    keepAliveURL: '../Common/Common.asmx/keepSessionAlive',
                    serverResponseEquals: 'OK',
                    onTimeout: function () {
                        $(this).slideUp();
                        parent.window.location = "../Login.aspx?Msg=1";
                    },
                    onIdle: function () {
                        $(this).slideDown(); // show the warning bar
                    },
                    onCountdown: function (counter) {
                        $(this).find("span").html(counter); // update the counter
                    },
                    onResume: function () {
                        $(this).slideUp(); // hide the warning bar
                    }
                });
            });

            setInterval("RemindCall()", 300000);
            // setInterval("AlertCall()", 800000);
            function RemindCall() {
                $.ajax({
                    type: "POST", //
                    url: "../OutlookCalendar/DataFeedCalendar.aspx?method=reminders",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (data) {
                        if (data.IsSuccess) {
                            OpenRemindersWindow();
                        }
                    }
                });
            }
            function AlertCall() {
                $.ajax({
                    type: "POST", //
                    url: "../common/WorkFlowService.asmx/GetAlertData",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if ($.isEmptyObject(data)) {
                            console.log("data is empty");
                        }
                        else {
                            console.log("sachin" + data);
                            console.log("sachin empty" + data.length);
                            OpenAlertsWindow();
                        }


                    }
                });
            }
            //        $(document).shortkeys({
            //            'Space+P': function () { Goto('../opportunity/frmNewPurchaseOrder.aspx', 'cntOpenItem', 'divNew', 1000, 645) },
            //            'Space+S': function () { Goto('../opportunity/frmNewSalesOrder.aspx', 'cntOpenItem', 'divNew', 1000, 645) }
            //        });
        </script>
    </form>
</body>
</html>
