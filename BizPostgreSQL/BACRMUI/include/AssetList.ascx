﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AssetList.ascx.vb"
    Inherits=".AssetList" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<script type="text/javascript" language="javascript">
    function OpenNewAsset(a, b, c) {

        if (b == 0) {
            window.location.href = "../Items/frmKitDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" + c + "&AssetCode=" + a;
        }
        else {

            window.location.href = "../Items/frmKitDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" + c + "&AssetCode=" + a + "&DivisionID=" + b;
        }
        return false;
    }

    //function fn_doPage(pgno) {
    //    $('#txtCurrrentPageItems').val(pgno);
    //    if ($('#txtCurrrentPageItems').val() != 0 || $('#txtCurrrentPageItems').val() > 0) {
    //        $('#btnGo').trigger('click');
    //    }
    //}
    function fn_doPage(pgno, CurrentPageWebControlClientID, PostbackButtonClientID) {
        var currentPage;
        var postbackButton;

        if (CurrentPageWebControlClientID != '') {
            currentPage = CurrentPageWebControlClientID;
        }
        else {
            currentPage = 'txtCurrentPageCorr';
        }

        if (PostbackButtonClientID != '') {
            postbackButton = PostbackButtonClientID;
        }
        else {
            postbackButton = 'btnCorresGo';
        }

        $('#' + currentPage + '').val(pgno);
        if ($('#' + currentPage + '').val() != 0 || $('#' + currentPage + '').val() > 0) {
            $('#' + postbackButton + '').trigger('click');
        }
    }

    function SortColumn(a) {
        $("#txtSortColumnItems").val(a);
        if ($("#txtSortColumnItems").val() == a) {
            if ($("#txtSortOrderItems").val() == 'A')
                $("#txtSortOrderItems").val('D');
            else
                $("#txtSortOrderItems").val('A');
        }
        $('#btnGo').trigger('click');
        return false;
    }
</script>
<style>
    .pagn a {
        color: black !important;
    }
</style>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label>Search For</label>
             <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
     <div class="col-md-3">
        <div class="form-group">
            <label>&nbsp;</label>
            <div>
                 <asp:DropDownList ID="ddlSearch" runat="server" CssClass="form-control">
                            <asp:ListItem Text="-Select One--" Value=""></asp:ListItem>
                            <asp:ListItem Text="Item" Value="vcItemName"></asp:ListItem>
                            <%--<asp:ListItem Text="Warehouse" Value="vcWarehouse"></asp:ListItem>--%>
                            <asp:ListItem Text="Serial No" Value="vcSKU"></asp:ListItem>
                            <asp:ListItem Text="ModelID" Value="vcModelID"></asp:ListItem>
                            <asp:ListItem Text="Vendor" Value="vcCompanyName"></asp:ListItem>
                            <asp:ListItem Text="Description" Value="txtItemDesc"></asp:ListItem>
                        </asp:DropDownList>
            </div>
        </div>
    </div>
         <div class="col-md-2">
        <div class="form-group">
            <label>&nbsp;</label>
            <div>
                <asp:LinkButton ID="btnGo" runat="server" CssClass="btn btn-primary"><i class="fa fa-share-square-o"></i>&nbsp;&nbsp;Go</asp:LinkButton>
            </div>
        </div>
    </div>
       <div class="col-md-2">
        <div class="form-group">
            <label>Filter</label>
              <asp:DropDownList ID="ddlFilter" runat="server" AutoPostBack="True" CssClass="form-control">
                        </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>&nbsp;</label>
            <div>
                <asp:LinkButton ID="btnNew" runat="server" CssClass="btn btn-primary"><i class="fa fa-share-square-o"></i>&nbsp;&nbsp;New</asp:LinkButton>
            </div>
        </div>
    </div>
</div>
<asp:Table ID="Table1" Width="100%" runat="server" BorderWidth="0" Height="350" GridLines="None"
    BorderColor="black" CellSpacing="0" CellPadding="0">
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top">
            
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td id="hideItems" nowrap align="right" runat="server">
                        <webdiyer:AspNetPager ID="bizPager" runat="server"
                            PagingButtonSpacing="0"
                            CssClass="bizgridpager"
                            AlwaysShow="true"
                            CurrentPageButtonClass="active"
                            PagingButtonUlLayoutClass="pagination"
                            PagingButtonLayoutType="UnorderedList"
                            FirstPageText="<<"
                            LastPageText=">>"
                            NextPageText=">"
                            PrevPageText="<"
                            Width="100%"
                            UrlPaging="false"
                            NumericButtonCount="8"
                            ShowCustomInfoSection="Left"
                            ShowPageIndexBox="Never"
                            OnPageChanged="bizPager_PageChanged"
                            CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
                            CustomInfoClass="bizpagercustominfo">
                        </webdiyer:AspNetPager>
                    </td>
                </tr>
            </table>
            <table cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr>
                    <td>
                        <%--<uc1:frmBizSorting ID="frmBizSorting2" runat="server" />--%>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="gvAssetItem" runat="server" Width="100%" AutoGenerateColumns="False"
                GridLines="None" ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false"
                CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs">
                <MasterTableView DataKeyNames="numItemcode" HierarchyLoadMode="Client" DataMember="Asset">
                    <DetailTables>
                        <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                            AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            ShowFooter="true" DataKeyNames="numAssetItemId" DataMember="AssetSerial" CssClass="tbl"
                            AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs">
                            <ParentTableRelation>
                                <telerik:GridRelationFields DetailKeyField="numAssetItemId" MasterKeyField="numItemcode" />
                            </ParentTableRelation>
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="Serial No" ItemStyle-Width="10%" DataField="vcSerialNo">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Model Id" ItemStyle-Width="10%" DataField="vcModelId">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="BarCode Id" ItemStyle-Width="10%" DataField="vcBarCodeId">
                                </telerik:GridBoundColumn>
                                <%--    <telerik:GridTemplateColumn HeaderText="Purchase Date" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <%#ReturnDate(Eval("dtPurchase"))%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Warrante Expiration" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <%#ReturnDate(Eval("dtWarrante"))%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>--%>
                                <telerik:GridBoundColumn HeaderText="Location" ItemStyle-Width="10%" DataField="vcLocation">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </telerik:GridTableView>
                    </DetailTables>
                    <Columns>
                        <telerik:GridTemplateColumn HeaderText="Image" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1)%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Item Name" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <a href="javascript:void(0)" onclick="OpenNewAsset('<%#Eval("numItemCode")%>', <%#DivisionID%>, '<%#FormName%>');">
                                    <%#Eval("vcitemName")%>
                                </a>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn HeaderText="Model Id" ItemStyle-Width="15%" DataField="vcModelId">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Description" ItemStyle-Width="25%" DataField="txtItemDesc">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="SKU(NI)" ItemStyle-Width="10%" DataField="vcSKU">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Serial No" ItemStyle-Width="10%" DataField="vcSKU">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Vendor" ItemStyle-Width="10%" DataField="Vendor">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="UPC" ItemStyle-Width="10%" DataField="numBarCodeId">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Cost" ItemStyle-Width="10%" DataField="Cost">
                        </telerik:GridBoundColumn>

                        <%-- <telerik:GridTemplateColumn HeaderText="Purchase Date" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <%#ReturnDate(Eval("dtPurchase"))%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>--%>
                        <%--<telerik:GridTemplateColumn HeaderText="Warrante Expiration" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <%#ReturnDate(Eval("dtWarrentyTill"))%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>--%>
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowExpandCollapse="true" />
            </telerik:RadGrid>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<table width="100%">
    <tr>
        <td class="normal4" align="center">
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
        </td>
    </tr>
</table>
<asp:TextBox ID="txtDelItemIds" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtSaveAPIItemIds" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtTotalPageItems" Style="display: none" runat="server"></asp:TextBox>
<asp:TextBox ID="txtTotalRecordsItems" Style="display: none" runat="server"></asp:TextBox>
<asp:TextBox ID="txtSortCharItems" Text="0" Style="display: none" runat="server"></asp:TextBox>
<asp:TextBox ID="txtSortColumnItems" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtSortOrderItems" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtCurrrentPageItems" runat="server" Style="display: none"></asp:TextBox>
<asp:HiddenField ID="hdnDivisionID" runat="server" Value="0" />
<asp:HiddenField ID="hdnFormName" runat="server" Value="0" />
