﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Calandar.ascx.vb" Inherits="BACRM.Include.calandar" %>
<script src="<% Page.ResolveClientUrl("~/JavaScript/dateFormat.js")%>" type="text/javascript"></script>
<script type="text/javascript">

    function validateDate(CalDate, format) {
        if (!isDate(document.getElementById(CalDate).value, format)) {
            alert("Enter Valid Date");
        }
    }
</script>

<div class="input-group">
    <asp:TextBox ID="txtDate" runat="server" ClientIDMode="AutoID" CssClass="form-control" size="15px"></asp:TextBox>
    <span class="input-group-addon" style="height:30px;padding: 3px 3px;">
        <asp:Image ID="imgDate" runat="server" ClientIDMode="AutoID"  ImageUrl="../Images/calendar25.png" />
    </span>
</div>


<asp:Literal ID="litScript" runat="server"></asp:Literal>
