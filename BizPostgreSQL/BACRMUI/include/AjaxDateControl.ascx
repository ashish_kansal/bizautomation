<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AjaxDateControl.ascx.vb" Inherits="AjaxDateControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<link href="../css/master.css" type="text/css" rel="STYLESHEET" />

 <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode= "Conditional" ChildrenAsTriggers= "true"  >
  <ContentTemplate>
    <asp:TextBox ID="DateTextBox" runat="server" CssClass="signup" Width="80"></asp:TextBox>
    <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" TargetControlID="DateTextBox" PopupControlID="Panel1" Position="bottom"  />
          <asp:Panel ID="Panel1" runat="server" BorderWidth="1px" BorderColor="black" >
                        <table border="0" style="background-color:White;"  width="200px">
				            <tr>
					                    <td class="normal1" nowrap >
					                        <asp:linkbutton id="btnFirst" style="TEXT-DECORATION: none" accessKey="P" runat="server" Font-Names="Webdings" ForeColor="Black" CausesValidation="False" ToolTip="Previous Year" EnableViewState="False" Width="14px" Font-Size="Small" >9</asp:linkbutton>
					                        <asp:linkbutton id="btnPrev" style="TEXT-DECORATION: none" runat="server" Font-Names="Webdings" ForeColor="Black" ToolTip="Previous Month" EnableViewState="False" Width="14px" Font-Size="Small">3</asp:linkbutton>
					                        <asp:linkbutton id="lblMon" runat="server" Font-Names="Arial" ForeColor="Black" ToolTip="Month" EnableViewState="False"  style="text-decoration:none;font-weight:bold;font-family:arial"></asp:linkbutton>&nbsp;&nbsp;
					                        <asp:linkbutton id="lblYear" style="text-decoration:none;font-weight:bold;font-family:arial" runat="server"  ForeColor="Black" ToolTip="Year" EnableViewState="False"  ></asp:linkbutton>
					                        <asp:linkbutton id="btnNext" style="TEXT-DECORATION: none" runat="server" Font-Names="Webdings" ForeColor="Black" ToolTip="Next Month" EnableViewState="False" Width="14px" Font-Size="Small">4</asp:linkbutton>
					                        <asp:linkbutton id="btnLast" style="TEXT-DECORATION: none" runat="server" Font-Names="Webdings" ForeColor="Black" ToolTip="Next Year" EnableViewState="False" Width="14px" Font-Size="Small">:</asp:linkbutton>
					                        
            							</td>
            							<td align="right" class="normal1">
            							    <asp:linkbutton id="Linkbutton2" runat="server" Text="Close" CausesValidation="False" ToolTip="close" style="text-decoration:none;font-weight:bold;font-family:arial" EnableViewState="False"></asp:linkbutton>
            							</td>
            					</tr>
            				</table>
				            <table border="0" style="background-color:White;" width="200px">
            					<tr>
            							<td>
            							
            							
							            <asp:calendar id="Calendar1" runat="server" ForeColor="#00000" Width="191px" Font-Size="9" Height="167px"
							            ShowTitle="False"  DayNameFormat="FirstTwoLetters"  SelectionMode="Day" Font-Name="arial" SelectedDayStyle-BackColor="red" OtherMonthDayStyle-ForeColor="White" OtherMonthDayStyle-Font-Size="Smaller" OtherMonthDayStyle-BackColor="White">
							            <DayStyle Font-Size="10pt" ForeColor="DimGray" BackColor="white"></DayStyle>
							            <NextPrevStyle Font-Size="10pt" Font-Names="arial" Font-Bold="True" ForeColor="White" BackColor="#00A2FF"></NextPrevStyle>
							        
							            <TitleStyle Font-Size="10pt" Font-Names="arial" Font-Bold="True" ForeColor="White" BackColor="#00A2FF"></TitleStyle>
							            <OtherMonthDayStyle ForeColor="White"></OtherMonthDayStyle>
							            <SelectedDayStyle BackColor="Gray"  />
						            </asp:calendar></td>
				            </tr>
				            
				            <tr>
				                <td >
				                        <asp:literal id="Literal" runat="server"></asp:literal><asp:literal id="Literal1" runat="server"></asp:literal>
			                            <div language="javascript" id="Month" style="LEFT: 27px; WIDTH: 90px; POSITION: absolute; TOP: 0px; HEIGHT: 22px"
				                        runat="server"><asp:dropdownlist id="ddlMonth" runat="server"  ForeColor="White" Width="90px" 
					                        BackColor="#8ca2bd" Font-Bold="True" AutoPostBack="True" cssclass="text_bold">
					                        <asp:ListItem Value="1">January</asp:ListItem>
					                        <asp:ListItem Value="2">February</asp:ListItem>
					                        <asp:ListItem Value="3">March</asp:ListItem>
					                        <asp:ListItem Value="4">April</asp:ListItem>
					                        <asp:ListItem Value="5">May</asp:ListItem>
					                        <asp:ListItem Value="6">June</asp:ListItem>
					                        <asp:ListItem Value="7">July</asp:ListItem>
					                        <asp:ListItem Value="8">August</asp:ListItem>
					                        <asp:ListItem Value="9">September</asp:ListItem>
					                        <asp:ListItem Value="10">October</asp:ListItem>
					                        <asp:ListItem Value="11">November</asp:ListItem>
					                        <asp:ListItem Value="12">December</asp:ListItem>
				                        </asp:dropdownlist></div>
			                        <div id="Year" style="LEFT: 80px; WIDTH: 49px; POSITION: absolute; TOP: 0px; HEIGHT: 22px"
				                        runat="server"><asp:dropdownlist id="ddlYear" runat="server"  ForeColor="White" Width="53px"
					                       BackColor="#8ca2bd" Font-Bold="True" AutoPostBack="True" cssclass="text_bold"></asp:dropdownlist></div>
			                        <div id="Clear" style="FONT-SIZE: smaller; LEFT: 125px; BORDER-LEFT: maroon thin; WIDTH: 65px; COLOR: red; FONT-FAMILY: Arial; POSITION: absolute; TOP: 167px; HEIGHT: 20px; TEXT-DECORATION: underline"><asp:linkbutton id="LinkButton1" style="COLOR: red; TEXT-DECORATION: underline" runat="server">Clear Date</asp:linkbutton></div>
				                </td>
				            </tr>
				            </table>
    </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
    
   
    
    
    
  