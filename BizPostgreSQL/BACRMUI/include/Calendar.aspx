<%@ Page EnableViewState="true" Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="Calendar.aspx.vb" Inherits="Calender" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
		<title>Calendar</title>
		<style type="text/css">BODY {
	BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; MARGIN: 0pt; BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid
}
		</style>
		<script language="javascript">
		function ClearCross()
		{
			window.option 
		}
		</script>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%"  align="center" bgColor="#c6d3e7">
				<tr>
					<td align="center" valign="bottom">
					<asp:linkbutton id="btnFirst" style="TEXT-DECORATION: none" accessKey="P" runat="server" ForeColor="Black" CausesValidation="False" ToolTip="Previous Year" EnableViewState="False"  ><img border="0" src="../images/PrevArrow1.gif" /></asp:linkbutton>
					<asp:linkbutton id="btnPrev" style="TEXT-DECORATION: none" runat="server" ForeColor="Black" ToolTip="Previous Month" EnableViewState="False" ><img border="0" src="../images/PrevArrow.gif" /></asp:linkbutton>
					<asp:dropdownlist id="ddlMonth" runat="server" Font-Names="Arial" Font-Size="11px" ForeColor="White" Width="80px" BackColor="#8ca2bd" Font-Bold="True" AutoPostBack="True" cssclass="slt9Normal">
					<asp:ListItem Value="1">January</asp:ListItem>
					<asp:ListItem Value="2">February</asp:ListItem>
					<asp:ListItem Value="3">March</asp:ListItem>
					<asp:ListItem Value="4">April</asp:ListItem>
					<asp:ListItem Value="5">May</asp:ListItem>
					<asp:ListItem Value="6">June</asp:ListItem>
					<asp:ListItem Value="7">July</asp:ListItem>
					<asp:ListItem Value="8">August</asp:ListItem>
					<asp:ListItem Value="9">September</asp:ListItem>
					<asp:ListItem Value="10">October</asp:ListItem>
					<asp:ListItem Value="11">November</asp:ListItem>
					<asp:ListItem Value="12">December</asp:ListItem>
				</asp:dropdownlist>
					<asp:dropdownlist id="ddlYear" Font-Size="11px" runat="server" Font-Names="Arial" ForeColor="White" Width="55px" BackColor="#8ca2bd" Font-Bold="True" AutoPostBack="True" cssclass="slt9Normal"></asp:dropdownlist>
					<asp:linkbutton id="btnNext" style="TEXT-DECORATION: none" runat="server" ForeColor="Black" ToolTip="Next Month" EnableViewState="False" ><img border="0" src="../images/NextArrow.gif" /></asp:linkbutton>
					<asp:linkbutton id="btnLast" style="TEXT-DECORATION: none" runat="server" ForeColor="Black" ToolTip="Next Year" EnableViewState="False" ><img border="0" src="../images/NextArrow1.gif" /></asp:linkbutton>
				    <asp:linkbutton id="LinkButton1" Font-Bold="true" style="TEXT-DECORATION: none"  ForeColor="black" Font-Size="12px" Font-Names="Arial" runat="server">X</asp:linkbutton>
				    </td>
				</tr>
				<tr>
				    <td align="center">
				    <asp:calendar id="Calendar1" BorderWidth="0" runat="server" ForeColor="#00000" Width="220px" Font-Size="9" Height="167px" ShowTitle="False" DayNameFormat=FirstTwoLetters  SelectionMode="Day" Font-Name="arial" SelectedDayStyle-BackColor="red" OtherMonthDayStyle-ForeColor="white">
							<DayStyle Font-Size="10pt" ForeColor="DimGray" BackColor="white"></DayStyle>
							<NextPrevStyle Font-Size="10pt" Font-Names="arial" Font-Bold="True" ForeColor="White" BackColor="#00A2FF"></NextPrevStyle>
							<DayHeaderStyle ForeColor="Black" BackColor="#8ca2bd"></DayHeaderStyle>
							<TitleStyle Font-Size="10pt" Font-Names="arial" Font-Bold="True" ForeColor="White" BackColor="#00A2FF"></TitleStyle>
							<OtherMonthDayStyle ForeColor="Silver"></OtherMonthDayStyle>
							<SelectedDayStyle BackColor="DimGray"/>
						</asp:calendar></td>
				</tr>
			</table>
		</form>
	</body>
</html>
