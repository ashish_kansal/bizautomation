﻿Imports System.Web
Imports System.Web.Services
Imports System.Text
Imports BACRM.BusinessLogic.Common
Imports System.Web.Script.Serialization
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin

Public Class AutomationRuleJSONSelect
    Implements System.Web.IHttpHandler
    Implements System.Web.SessionState.IRequiresSessionState

    Dim objCommon As New CCommon
    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim id As String = context.Request.QueryString("id")

        Dim strData As New StringBuilder()

        If id = "U" Then
            Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
            objConfigWizard.DomainID = context.Session("DomainID")                                                 'set the domain id
            objConfigWizard.ListItemType = "U"                                     'set the listitem type
            Dim dtTable As DataTable                                                            'declare a datatable
            dtTable = objConfigWizard.GetMasterListByListId(0)                          'call function to fill the datatable

            For Each rs As DataRow In dtTable.Rows
                strData.Append("""" & rs("numItemID").ToString() & """:""" & rs("vcItemName").ToString() & """,")
            Next
        ElseIf id = "ActionItemTemp" Then
            Dim objActionItem As New ActionItem
            Dim dtTable As DataTable
            objActionItem.DomainID = context.Session("DomainID")
            objActionItem.UserCntID = 0
            dtTable = objActionItem.LoadActionItemTemplateData()

            For Each rs As DataRow In dtTable.Rows
                strData.Append("""" & rs("RowID").ToString() & """:""" & rs("TemplateName").ToString() & """,")
            Next
        Else
            Dim str As String() = id.Split("~")

            objCommon.FormFieldId = str(0)
            objCommon.CustomField = 0
            objCommon.UserCntID = context.Session("UserContactID")
            objCommon.DomainID = context.Session("DomainID")

            Dim dtPropertyData As DataTable
            dtPropertyData = objCommon.GetInLineEditPropertyName()


            For Each dr As DataRow In dtPropertyData.Rows
                Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
                objConfigWizard.DomainID = context.Session("DomainID")                                                 'set the domain id
                objConfigWizard.ListItemType = dr("vcListItemType")                                       'set the listitem type
                Dim dtTable As DataTable                                                            'declare a datatable
                dtTable = objConfigWizard.GetMasterListByListId(dr("numListID"))                          'call function to fill the datatable

                For Each rs As DataRow In dtTable.Rows
                    strData.Append("""" & rs("numItemID").ToString() & """:""" & rs("vcItemName").ToString() & """,")
                Next
            Next
        End If

        context.Response.ContentType = "application/json"
        context.Response.ContentEncoding = Encoding.UTF8
        context.Response.Write("{" & strData.ToString.Trim(",") & "}")
        context.Response.End()
    End Sub


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class