<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AjaxCalendar.ascx.vb"
    Inherits=".AjaxCalendar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<link href="../css/master.css" type="text/css" rel="STYLESHEET" />

<script type="text/javascript" language="javascript">
    function FormatDate(a, b, c) {
        var strDay, strMon, strYear, strFormat;
        strMon = document.getElementById(a).value.substring(0, 2);
        strDay = document.getElementById(a).value.substring(3, 5);
        strYear = document.getElementById(a).value.substring(6, 10);
        strFormat = document.getElementById(c).value;
        strFormat = strFormat.replace('MM', strMon);
        strFormat = strFormat.replace('DD', strDay);
        strFormat = strFormat.replace('YYYY', strYear);
        document.getElementById(b).value = strFormat;
        return false;
    }
</script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>
        <table align="left" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td align="left">
                    <asp:TextBox runat="server" ID="txtFormattedDate" Width="60" CssClass="signup" />
                    <asp:TextBox ID="txtDate" Width="0" BorderWidth="0" ForeColor="white" runat="server"></asp:TextBox>
                    <asp:Image runat="Server" CssClass="hyperlink" ID="imgCalendar" ImageUrl="../images/calender.gif" />
                    <asp:TextBox ID="txtDateFormat" Width="10" runat="server" Style="display: none"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" Format="MM/dd/yyyy" runat="server"
                        TargetControlID="txtDate" PopupButtonID="imgCalendar" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
