Imports BACRM.BusinessLogic.Common
Partial Public Class AjaxDateControl
    Inherits BACRMUserControl
    Protected WithEvents lblMonYear As System.Web.UI.WebControls.LinkButton

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Call FillCombo()
                If DateTextBox.Text <> "" Then
                    Calendar1.VisibleDate = DateFromFormattedDate(DateTextBox.Text, Session("DateFormat"))
                Else
                    Dim strNow As Date
                    strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    Calendar1.VisibleDate = strNow
                End If
            End If
            Call setLabel()
            Month.Visible = False
            Year.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Try
            PopupControlExtender1.Commit(FormattedDateFromDate(Calendar1.SelectedDate, Session("DateFormat")))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#Region "Navigation Method"

    Private Sub btnFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFirst.Click
        Try
            Calendar1.VisibleDate = DateAdd(DateInterval.Year, -1, Calendar1.VisibleDate)
            Call setLabel()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrev.Click
        Try
            Calendar1.VisibleDate = DateAdd(DateInterval.Month, -1, Calendar1.VisibleDate)
            Call setLabel()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            Calendar1.VisibleDate = DateAdd(DateInterval.Month, 1, Calendar1.VisibleDate)
            Call setLabel()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
        Try
            Calendar1.VisibleDate = CDate(DateAdd(DateInterval.Year, 1, Calendar1.VisibleDate))
            Call setLabel()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "Set Label"
    Private Sub setLabel()
        Try
            lblMon.Text = Calendar1.VisibleDate.ToString("MMMM")
            lblMon.Attributes.Add("align", "center")
            lblMon.Font.Bold = True
            lblYear.Text = Calendar1.VisibleDate.ToString("yyyy")
            lblYear.Attributes.Add("align", "center")
            lblYear.Font.Bold = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Fill Combo"
    Private Sub FillCombo()
        Try
            Dim i As Integer
            For i = 1900 To 2050
                ddlYear.Items.Add(i)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Functional Method"

    Private Sub LinkButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Try
            PopupControlExtender1.Commit("")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lblMon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblMon.Click
        Try
            ddlYear.SelectedIndex = getIndex(lblMon.Text)
            Month.Visible = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lblYear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblYear.Click
        Try
            ddlYear.SelectedIndex = CInt(lblYear.Text) - 1900
            Year.Visible = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ' Private Sub Calendar1_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
    '    If e.Day.Date = DateTime.Now.ToString("d") Then
    '       e.Cell.BackColor = System.Drawing.Color.LightGray
    '  End If

    ' If e.Day.IsOtherMonth = True Then
    '    e.Cell.ForeColor = System.Drawing.Color.FromName("White")
    '   e.Cell.Attributes.Add("onclick", "return false;")
    'End If
    'End Sub

    Private Function getIndex(ByVal MonthName As String) As Integer
        Try
            Return Microsoft.VisualBasic.Switch(MonthName = "January", 0, MonthName = "February", 1, MonthName = "March", 2, MonthName = "April", 3, MonthName = "May", 4, MonthName = "June", 5, MonthName = "July", 6, MonthName = "August", 7, MonthName = "September", 8, MonthName = "October", 9, MonthName = "November", 10, MonthName = "December", 11)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function getMonth(ByVal intMonth As Int16) As String
        Try
            Select Case intMonth
                Case 1 : Return "Jan"
                Case 2 : Return "Feb"
                Case 3 : Return "Mar"
                Case 4 : Return "Apr"
                Case 5 : Return "May"
                Case 6 : Return "Jun"
                Case 7 : Return "Jul"
                Case 8 : Return "Aug"
                Case 9 : Return "Sep"
                Case 10 : Return "Oct"
                Case 11 : Return "Nov"
                Case 12 : Return "Dec"
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

    Private Sub ddlYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        Try
            Year.Visible = False
            Calendar1.VisibleDate = New Date(Calendar1.VisibleDate.Year, ddlMonth.SelectedItem.Value, 15)
            Call setLabel()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        Try
            Month.Visible = False
            Calendar1.VisibleDate = New Date(Calendar1.VisibleDate.Year, ddlMonth.SelectedItem.Value, 15)
            Call setLabel()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Linkbutton2.Click
        Try
            PopupControlExtender1.Commit(DateTextBox.Text)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Property SelectedDate() As String
        Get
            Try
                If DateTextBox.Text <> "" Then Return DateFromFormattedDate(DateTextBox.Text, Session("DateFormat"))
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As String)
            Try
                IIf(IsDBNull(Value), Value = "", Value)
                If Value <> "" Then DateTextBox.Text = FormattedDateFromDate(CDate(Value), Session("DateFormat"))
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

End Class