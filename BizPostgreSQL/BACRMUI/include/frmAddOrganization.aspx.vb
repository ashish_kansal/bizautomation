﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Workflow


Public Class frmAddOrganization
    Inherits BACRMPage

    Dim lngDivisionId, lngGrpID, lngCntId, lngFormId, lngRelId, lngProfileId, lngTerritoryId As Long

    Dim dtTableInfo As DataTable
    Dim objPageControls As New PageControls
    Dim objLeads As New LeadsIP
    Dim strFirstName As String = ""
    Dim strLastName As String = ""
    Dim strOrgName As String = ""
    Dim strEmail As String = ""


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngFormId = GetQueryStringVal("FormID")
            lngRelId = GetQueryStringVal("RelID")
            lngProfileId = CCommon.ToLong(GetQueryStringVal("profileid"))
            lngTerritoryId = CCommon.ToLong(GetQueryStringVal("numTerritoryID"))
            lngGrpID = CCommon.ToInteger(GetQueryStringVal("GrpID"))

            Dim strModuleName, strPermissionName As String
            Dim m_aryRightsForAdd() As Integer = GetUserRightsForPage_Other(32, 2, strModuleName, strPermissionName)

            If m_aryRightsForAdd(RIGHTSTYPE.ADD) = 0 Then
                Response.Redirect("../admin/authenticationpopup.aspx?mesg=AC&Module=" & strModuleName & "&Permission=" & strPermissionName)
                Exit Sub
            End If

            If Not IsPostBack Then
                If Session("FromDetail") IsNot Nothing Then
                    Dim strFrom As String() = Session("FromDetail")

                    If strFrom.Length > 0 Then
                        strFirstName = If(strFrom(0).Split(" ").Length > 0, CCommon.ToString(strFrom(0).Split(" ")(0)), "")
                        strLastName = If(strFrom(0).Split(" ").Length > 1, CCommon.ToString(strFrom(0).Split(" ")(1)), "")
                    End If

                    If strFrom.Length > 1 Then strOrgName = If(strFrom(1).Split("@").Length > 1, CCommon.ToString(strFrom(1).Split("@")(1)), "")
                    If strOrgName.Split(".").Length > 0 Then strOrgName = CCommon.ToString(strOrgName.Split(".")(0))
                    If strFrom.Length > 1 Then strEmail = CCommon.ToString(strFrom(1))
                End If

                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")

                GetUserRightsForPage(32, 2)
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnSave.Visible = False
                Else : btnSave.Visible = True
                End If

                objCommon = New CCommon

                Dim dtTable As DataTable
                dtTable = objCommon.GetMasterListItems(5, Session("DomainID"))

                If lngRelId > 3 Then
                    Dim dRows() As DataRow
                    dRows = dtTable.Select("numListItemID = " & lngRelId, "")
                    lblRelationship.Text = dRows(0)("vcData").ToString()
                Else
                    If lngRelId = 1 Then
                        lblRelationship.Text = dtTab.Rows(0).Item("vcLead")

                        Select Case lngGrpID
                            Case 5
                                lblRelationship.Text = "My " & dtTab.Rows(0).Item("vcLead") & "s"
                            Case 2
                                lblRelationship.Text = "Public " & dtTab.Rows(0).Item("vcLead") & "s"
                            Case 1
                                lblRelationship.Text = "Web " & dtTab.Rows(0).Item("vcLead") & "s"
                        End Select
                    ElseIf lngRelId = 3 Then
                        lblRelationship.Text = dtTab.Rows(0).Item("vcProspect")
                    ElseIf lngRelId = 2 Then
                        lblRelationship.Text = dtTab.Rows(0).Item("vcAccount")
                    End If
                End If

                LoadControls()

                btnSave.Attributes.Add("onclick", "return Save()")
                btnClose.Attributes.Add("onclick", "return Close()")
            End If

            If IsPostBack Then
                LoadControls()
            End If

            DisplayDynamicFlds()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub DisplayDynamicFlds()
        Try
            objPageControls.DisplayDynamicFlds(0, lngRelId, Session("DomainID"), objPageControls.Location.Organization)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadControls()
        Try
            tblMain.Controls.Clear()
            Dim ds As DataSet
            Dim objPageLayout As New CPageLayout
            Dim fields() As String

            objPageLayout.UserCntID = 0
            objPageLayout.DomainID = Session("DomainID")
            objPageLayout.PageId = 1
            objPageLayout.numRelCntType = lngRelId
            objPageLayout.FormId = lngFormId
            objPageLayout.PageType = 2

            ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
            dtTableInfo = ds.Tables(0)

            Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
            Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
            Dim intCol1 As Int32 = 0
            Dim intCol2 As Int32 = 0
            Dim tblCell As TableCell

            Dim tblRow As TableRow
            Const NoOfColumns As Short = 2
            Dim ColCount As Int32 = 0

            ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
            objPageControls.CreateTemplateRow(tblMain)
            For Each dr As DataRow In dtTableInfo.Rows

                'Commented By:Sachin Sadhu||Date:17-Jan-2014
                'Purpose: To add facility to save Billing/Shipping address on New Record Froms||below If 
                '  If (dr("fld_type") <> "Popup" And dr("fld_type") <> "Label") Then

                If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                    If dr("vcPropertyName") = "GroupID" Then
                        dr("vcValue") = lngGrpID
                    Else
                        dr("vcValue") = objLeads.GetType.GetProperty(dr("vcPropertyName")).GetValue(objLeads, Nothing)

                        If Not IsDBNull(dr("vcPropertyName")) AndAlso CCommon.ToString(dr("vcPropertyName")) = "CompanyName" Then
                            dr("vcValue") = strOrgName
                        ElseIf Not IsDBNull(dr("vcPropertyName")) AndAlso CCommon.ToString(dr("vcPropertyName")) = "FirstName" Then
                            dr("vcValue") = strFirstName
                        ElseIf Not IsDBNull(dr("vcPropertyName")) AndAlso CCommon.ToString(dr("vcPropertyName")) = "LastName" Then
                            dr("vcValue") = strLastName
                        ElseIf Not IsDBNull(dr("vcPropertyName")) AndAlso CCommon.ToString(dr("vcPropertyName")) = "Email" Then
                            dr("vcValue") = strEmail
                        End If
                        dtTableInfo.AcceptChanges()
                    End If
                End If


                If ColCount = 0 Then
                    tblRow = New TableRow
                End If

                If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)

                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)
                    ColCount = 1
                End If

                If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                    Dim dtData As DataTable
                    'Dim dtSelOpportunity As DataTable

                    If Not IsDBNull(dr("vcPropertyName")) Then
                        If dr("vcPropertyName") = "AssignedTo" Then 'dr("numFieldId") = 84
                            If Session("PopulateUserCriteria") = 1 Then
                                dtData = objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, objLeads.TerritoryID)
                            ElseIf Session("PopulateUserCriteria") = 2 Then
                                dtData = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                            Else
                                dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                            End If
                        ElseIf dr("vcPropertyName") = "GroupID" Then
                            dtData = objCommon.GetGroups()
                        ElseIf dr("vcPropertyName") = "CampaignID" Then
                            Dim objCampaign As New BACRM.BusinessLogic.Reports.PredefinedReports
                            objCampaign.byteMode = 2
                            objCampaign.DomainID = Session("DomainID")
                            dtData = objCampaign.GetCampaign()
                        ElseIf dr("vcPropertyName") = "numPartenerSource" Then
                            objCommon.DomainID = Session("DomainID")
                            dtData = objCommon.GetPartnerSource()
                        ElseIf dr("vcPropertyName") = "DripCampaign" Then ' dr("numFieldId") = 291 Then
                            Dim objCampaign As New BACRM.BusinessLogic.Marketing.Campaign
                            With objCampaign
                                .SortCharacter = "0"
                                .UserCntID = Session("UserContactID")
                                .PageSize = 100
                                .TotalRecords = 0
                                .DomainID = Session("DomainID")
                                .columnSortOrder = "Asc"
                                .CurrentPage = 1
                                .columnName = "vcECampName"
                                dtData = objCampaign.ECampaignList
                                Dim drow As DataRow = dtData.NewRow
                                drow("numECampaignID") = -1
                                drow("vcECampName") = "-- Disengaged --"
                                dtData.Rows.Add(drow)

                            End With
                        ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                            If dr("ListRelID") > 0 Then
                                objCommon.Mode = 3
                                objCommon.DomainID = Session("DomainID")
                                objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objLeads)
                                objCommon.SecondaryListID = dr("numListId")
                                dtData = objCommon.GetFieldRelationships.Tables(0)
                            Else
                                dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                            End If
                        End If
                    End If

                    Dim ddl As DropDownList
                    ddl = objPageControls.CreateCells(tblRow, dr, False, dtData, boolAdd:=True)
                    If CLng(dr("DependentFields")) > 0 And Not False Then
                        ddl.AutoPostBack = True
                        AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                    End If
                    If CCommon.ToString(dr("vcPropertyName")) = "CompanyType" Then
                        If lngRelId = 1 Or lngRelId = 2 Or lngRelId = 3 Then
                            If Not ddl.Items.FindByValue("46") Is Nothing Then
                                ddl.SelectedValue = "46"
                                objLeads.CompanyType = 46
                            End If
                        ElseIf Not ddl.Items.FindByValue(lngRelId) Is Nothing Then
                            ddl.SelectedValue = lngRelId
                            objLeads.CompanyType = lngRelId
                        End If

                        ddl.Enabled = False
                    ElseIf CCommon.ToString(dr("vcPropertyName")) = "Profile" Then
                        If Not ddl.Items.FindByValue(lngProfileId) Is Nothing Then
                            ddl.Items.FindByValue(lngProfileId).Selected = True
                            objLeads.Profile = lngProfileId
                        End If
                    ElseIf CCommon.ToString(dr("vcPropertyName")) = "TerritoryID" Then
                        If Not ddl.Items.FindByValue(lngTerritoryId) Is Nothing Then
                            ddl.Items.FindByValue(lngTerritoryId).Selected = True
                            objLeads.TerritoryID = lngProfileId
                        End If
                    End If
                    ddl.EnableViewState = True
                ElseIf dr("fld_type") = "CheckBoxList" Then
                    If Not IsDBNull(dr("vcPropertyName")) Then
                        Dim dtData As DataTable

                        If dr("ListRelID") > 0 Then
                            objCommon.Mode = 3
                            objCommon.DomainID = Session("DomainID")
                            objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objLeads)
                            objCommon.SecondaryListID = dr("numListId")
                            dtData = objCommon.GetFieldRelationships.Tables(0)
                        Else
                            dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                        End If

                        objPageControls.CreateCells(tblRow, dr, False, dtData, boolAdd:=True)
                    End If
                Else
                    objPageControls.CreateCells(tblRow, dr, False, RecordID:=0, boolAdd:=True)
                End If

                If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)

                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)

                    ColCount = 1
                End If


                If dr("intcoulmn") = 2 Then
                    If intCol1 <> intCol1Count Then
                        intCol1 = intCol1 + 1
                    End If

                    If intCol2 <> intCol2Count Then
                        intCol2 = intCol2 + 1
                    End If
                End If

                ColCount = ColCount + 1

                If NoOfColumns = ColCount Then
                    ColCount = 0
                    tblMain.Rows.Add(tblRow)
                End If
                ' End If
            Next

            If ColCount > 0 Then
                tblMain.Rows.Add(tblRow)
            End If

            'objPageControls.CreateComments(tblMain, objLeads.Comments, False)

            'Add Client Side validation for custom fields
            Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
            ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
        objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, tblMain, Session("DomainID"))
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()

        Catch ex As Exception
            If ex.Message.Contains("DUPLICATE_EMAIL") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EmailValidation", "OpenEmailChangeWindow(" & objLeads.ContactID & ",'" & objLeads.Email & "');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Sub Save()
        Try
            With objLeads
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactId")

                .ContactType = 0 ' 70 'commented by chintan, contact type 70 is obsolete. default contact type is 0
                .PrimaryContact = True

                Select Case lngRelId
                    Case 1 'Lead
                        .CRMType = 0
                        .GroupID = lngGrpID
                    Case 2 'Account
                        .CRMType = 2
                    Case 3 'Prospect
                        .CRMType = 1
                    Case Else
                        .CRMType = 1
                        .CompanyType = lngRelId
                End Select

                .DivisionName = "-"
                .LeadBoxFlg = 1
                .Country = Session("DefCountry")
                .SCountry = Session("DefCountry")
                .BillAddressID = CCommon.ToLong(hdnBillToAddressID.Value)
                .ShipAddressID = CCommon.ToLong(hdnShipToAddressID.Value)

                For Each dr As DataRow In dtTableInfo.Rows
                    If (dr("fld_type") <> "Popup" And dr("fld_type") <> "Label") Then
                        If (dr("vcDbColumnName") = "numCompanyType" And lngRelId > 3) Then
                            Continue For
                        End If

                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                            If dr("vcPropertyName") = "numPartenerSource" Then
                                dr("vcPropertyName") = "PartenerSource"
                            End If

                            If (dr("vcPropertyName") = "AssignedTo" And Session("DomainID") = 201) Then 'Default Logged in User as "Assigned To" For SA Wines
                                objLeads.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(objLeads, CInt(Session("UserContactId")), Nothing)
                            Else
                                objPageControls.SetValueForStaticFields(dr, objLeads, tblMain)
                            End If

                        End If
                    End If
                Next
                'objPageControls.SetValueForComments(objLeads.Comments, tblMain)
            End With

            If objLeads.CompanyType = 0 Then
                If lngRelId <= 3 Then
                    objLeads.CompanyType = 46
                End If
            End If

            If Not String.IsNullOrEmpty(hdnChangedEmail.Value) Then
                objLeads.Email = hdnChangedEmail.Value
            End If

            Dim objContactIp As New CContacts
            objContactIp.DomainID = objLeads.DomainID
            objContactIp.ContactID = 0
            objContactIp.Email = objLeads.Email

            'Right now we are allowing user to add duplicate contact email but in future we may want to restrict it
            'If objContactIp.IsDuplicateEmail() Then
            '    Throw New Exception("DUPLICATE_EMAIL")
            'End If

            Dim bool As Boolean = True
            If hdnIsDuplicate.Value = "0" Then
                Dim strWhere As String = objLeads.GetStrWhere()
                objLeads.WhereCondition = strWhere
                If objLeads.CheckDuplicate = True Then
                    litMessage.Text = "A Duplicate Organization found !! Click Save to Continue"
                    bool = False
                    hdnIsDuplicate.Value = "1"
                End If
            End If

            If bool = True Then
                'objLeads.UpdateDefaultTax = 1
                Using objTreansaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})

                    If (objLeads.AssignedTo = 0 And Session("DomainID") = 201) Then  'Default Logged in User as "Assigned To" For SA Wines
                        objLeads.AssignedTo = CInt(Session("UserContactId"))
                    End If

                    objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                    objLeads.DivisionID = objLeads.CreateRecordDivisionsInfo
                    lngCntId = objLeads.CreateRecordAddContactInfo()

                    objTreansaction.Complete()
                End Using


                SaveCusField(objLeads.DivisionID)

                'Added By Sachin Sadhu||Date:22ndMay12014
                'Purpose :To Added Organization data in work Flow queue based on created Rules
                '          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = objLeads.DivisionID
                objWfA.SaveWFOrganizationQueue()
                'end of code

                'Added By Sachin Sadhu||Date:23rdJuly2014
                'Purpose :To Added Contact data in work Flow queue based on created Rules
                '         Using Change tracking
                Dim objWF As New Workflow()
                objWF.DomainID = Session("DomainID")
                objWF.UserCntID = Session("UserContactID")
                objWF.RecordID = lngCntId
                objWF.SaveWFContactQueue()

                ' ss//end of code


                'Redirect logic 
                Dim strScript As String = "<script language=JavaScript>"
                'If lngRelId > 3 Then
                '    strScript += "window.opener.reDirectPage('../prospects/frmProspects.aspx?profileid=" & objLeads.Profile & "&RelId=" & objLeads.CompanyType & "&DivID=" & objLeads.DivisionID & "'); self.close();"
                'Else
                If objLeads.CRMType = 0 Then
                    strScript += "window.opener.reDirectPage('../Leads/frmLeads.aspx?grpID=" & objLeads.GroupID & "&DivID=" & objLeads.DivisionID & "'); self.close();"
                ElseIf objLeads.CRMType = 1 Then
                    strScript += "window.opener.reDirectPage('../prospects/frmProspects.aspx?profileid=" & objLeads.Profile & "&RelId=" & objLeads.CompanyType & "&DivID=" & objLeads.DivisionID & "'); self.close();"
                ElseIf objLeads.CRMType = 2 Then
                    strScript += "window.opener.reDirectPage('../account/frmAccounts.aspx?profileid=" & objLeads.Profile & "&RelId=" & objLeads.CompanyType & "&DivId=" & objLeads.DivisionID & "'); self.close();"
                End If
                'End If

                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then
                    Page.RegisterStartupScript("clientScript", strScript)
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub SaveCusField(ByVal lngDivID As Long)
        Try
            objPageControls.SaveCusField(lngDivID, objLeads.CompanyType, Session("DomainID"), objPageControls.Location.Organization, tblMain)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class