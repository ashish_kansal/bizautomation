Imports System.Reflection
Imports BACRM.BusinessLogic.Common
Partial Public Class calandar1
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            imgDate.Attributes.Add("onclick", "return OpenCalendar('" & txtDate.ClientID & "');")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Property SelectedDate() As String
        Get
            Try
                If txtDate.Text <> "" Then Return DateFromFormattedDate(txtDate.Text, Session("DateFormat"))
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As String)
            Try
                IIf(IsDBNull(Value), Value = "", Value)
                If Value <> "" Then txtDate.Text = FormattedDateFromDate(CDate(Value), Session("DateFormat"))
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

End Class