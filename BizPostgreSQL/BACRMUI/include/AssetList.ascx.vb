﻿Option Explicit On
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.UltraWebTab
Partial Public Class AssetList
    Inherits BACRMUserControl
    'Private _DivisionID As Long = 0
    'Private _FormName As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
        Session("OwnerId") = Nothing
        Session("OwnerName") = Nothing
        
        If Not IsPostBack Then
            btnNew.Text = "New Asset"
            FillFilter()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Property FormName() As String
        Get
            Try
                Return hdnFormName.Value
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As String)
            Try
                hdnFormName.Value = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property
    Public Property DivisionID() As Long
        Get
            Try
                Return CCommon.ToLong(hdnDivisionID.Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Long)
            Try
                hdnDivisionID.Value = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property
    Sub FillFilter()
        
        objCommon.sb_FillComboFromDBwithSel(ddlFilter, 36, Session("DomainID"))
    End Sub
    Public Sub LoadAssets()
        Try
            Dim ds As DataSet
            Dim objItems As New CItems

            btnNew.Attributes.Add("onclick", "return OpenNewAsset(-1," & DivisionID & ",'" & FormName & "')")
            With objItems

                If ddlFilter.SelectedItem Is Nothing Then
                    FillFilter()
                End If
                If DivisionID = 0 Then
                    Table1.BackColor = Color.White
                Else
                    Table1.CssClass = "aspTable"
                End If
                If DivisionID > 0 Then .DivisionID = DivisionID

                .ItemClassification = ddlFilter.SelectedItem.Value
                .SortCharacter = txtSortCharItems.Text.Trim()

                If txtCurrrentPageItems.Text.Trim = "" Then txtCurrrentPageItems.Text = 1
                .CurrentPage = Convert.ToInt32(txtCurrrentPageItems.Text.Trim())

                If ddlSearch.SelectedItem.Value <> "" And txtSearch.Text <> "" And ddlSearch.SelectedItem.Value <> "vcWarehouse" Then
                    .KeyWord = ddlSearch.SelectedItem.Value & " ilike '%" & txtSearch.Text.Trim & "%'"
                Else
                    .KeyWord = ""
                End If

                .DomainID = Session("DomainID")
                .PageSize = Convert.ToInt32(Session("PagingRows"))
                .TotalRecords = 0

                If txtSortColumnItems.Text <> "" Then
                    If txtSortColumnItems.Text.Split("~").Length = 2 Then
                        .columnName = txtSortColumnItems.Text.Split("~")(1)
                    Else
                        .columnName = txtSortColumnItems.Text
                    End If
                Else : .columnName = "vcItemName"
                End If

                If txtSortOrderItems.Text = "D" Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If

                ds = .GetOrganizationAssets

            End With

            bizPager.PageSize = Session("PagingRows")
            bizPager.CurrentPageIndex = txtCurrrentPageItems.Text
            bizPager.RecordCount = objItems.TotalRecords

            'If objItems.TotalRecords <> 0 Then
            '    hideItems.Visible = True
            '    lblRecordsItems.Text = objItems.TotalRecords
            '    Dim strTotalPage As String()
            '    Dim decTotalPage As Decimal
            '    decTotalPage = lblRecordsItems.Text / Session("PagingRows")
            '    decTotalPage = Math.Round(decTotalPage, 2)
            '    strTotalPage = CStr(decTotalPage).Split(".")
            '    If (lblRecordsItems.Text Mod Session("PagingRows")) = 0 Then
            '        lblTotalItems.Text = strTotalPage(0)
            '        txtTotalPageItems.Text = strTotalPage(0)
            '    Else
            '        lblTotalItems.Text = strTotalPage(0) + 1
            '        txtTotalPageItems.Text = strTotalPage(0) + 1
            '    End If
            '    txtTotalRecordsItems.Text = lblRecordsItems.Text
            'End If

            If ds.Relations.Count < 1 Then
                ds.Tables(0).TableName = "Asset"
                ds.Tables(1).TableName = "AssetSerial"
                'ds.Tables(0).PrimaryKey = New DataColumn() {ds.Tables(0).Columns("numWareHouseItemID")}
                'ds.Tables(1).PrimaryKey = New DataColumn() {ds.Tables(1).Columns("numWareHouseItmsDTLID")}
                ds.Relations.Add("Asset", ds.Tables(0).Columns("numItemcode"), ds.Tables(1).Columns("numAssetItemId"))
            End If

            gvAssetItem.DataSource = ds
            gvAssetItem.DataBind()
            'uwItem.Bands(0).DataKeyField = "numItemcode"
            'uwItem.Bands(1).DataKeyField = "numAssetItemId"
            'uwItem.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        Try
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub



    'Private Sub dgItems_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgItems.SortCommand
    '    Try
    '        strColumn = e.SortExpression.ToString()
    '        If ViewState("Column") <> strColumn Then
    '            ViewState("Column") = strColumn
    '            Session("Asc") = 0
    '        Else
    '            If Session("Asc") = 0 Then
    '                Session("Asc") = 1
    '            Else : Session("Asc") = 0
    '            End If
    '        End If
    '        BindDatagrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

  
    'Private Sub uwItem_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwItem.InitializeRow
    '    Try

    '        If e.Row.HasParent = False Then
    '            'If e.Row.Cells(12).Value <> "" Then
    '            '    e.Row.Cells.FromKey("vcPathForTImage").Value = CCommon.GetImageHTML(e.Row.Cells(12).Value, 1) '"<img src='" & Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName") & "/Documents/Docs/" & e.Row.Cells(12).Value & "'>"
    '            'End If
    '            e.Row.Cells(8).Value = FormatCurrency(e.Row.Cells(8).Value, 2)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub
    'Private Sub dgItems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgItems.ItemCommand
    '    Try
    '        If e.CommandName = "Item" Then Response.Redirect("../Items/frmKitDetails.aspx?ItemCode=" & e.Item.Cells(0).Text & "&frm=" & Page)
    '        If e.CommandName = "Delete" Then
    '            Dim objItems As New CItems
    '            objItems.ItemCode = e.Item.Cells(0).Text
    '            If objItems.DeleteItems = False Then
    '                litMessage.Text = "Dependent Records Exists. Cannot Be deleted"
    '            Else : BindDatagrid()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
    Function ReturnDate(ByVal CloseDate) As String
        Try
            If CloseDate Is Nothing Then Exit Function
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                If strTargetResolveDate = "01/01/1900" Then Return ""
                ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                ' if both are same it is today
                Dim strNow As Date
                strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                If (CloseDate.Date = strNow.Date And CloseDate.Month = strNow.Month And CloseDate.Year = strNow.Year) Then
                    strTargetResolveDate = "<font color=red><b>Today</b></font>"
                    Return strTargetResolveDate

                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                ElseIf (CloseDate.Date.AddDays(1).Date = strNow.Date And CloseDate.AddDays(1).Month = strNow.Month And CloseDate.AddDays(1).Year = strNow.Year) Then
                    strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                    Return strTargetResolveDate

                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                ElseIf (CloseDate.Date = strNow.AddDays(1).Date And CloseDate.Month = strNow.AddDays(1).Month And CloseDate.Year = strNow.AddDays(1).Year) Then
                    temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                    strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                    Return strTargetResolveDate
                Else
                    strTargetResolveDate = strTargetResolveDate
                    Return strTargetResolveDate
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub gvAssetItem_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAssetItem.NeedDataSource
        Try
            'LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPageItems.Text = bizPager.CurrentPageIndex
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class