Imports ASPNETExpert.WebControls
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports System.Globalization
Imports Infragistics.WebUI.Misc
Imports Infragistics.WebUI.Shared
Imports BACRM.BusinessLogic.Outlook
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Workflow

Partial Public Class frmMenu
    Inherits BACRMPage
    Dim CustomDataProvider As CustomDataProvider
    Dim m_aryRightsForAdmin() As Integer

    'Private Property Timer1 As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("DomainID") > 0 Then

                'Session("PopupURL") = hdnPopupURL.Value
                If Not IsPostBack Then

                    If Session("UserContactID") = "1" Then
                        tdSubscription.Visible = True
                        tdEditHelp.Visible = True
                    End If

                    ''Checking The View Rights For Tickler
                    If Session("UserContactID") <> Session("AdminID") Then 'Logged in User is Admin of Domain? yes then override permission and give him access

                        m_aryRightsForAdmin = GetUserRightsForPage_Other(13, 46)
                        If m_aryRightsForAdmin(RIGHTSTYPE.VIEW) = 0 Then tdAdmin.Visible = False

                        'm_aryRightsForAdmin = GetUserRightsForPage_Other(13, 47)
                        'If m_aryRightsForAdmin(RIGHTSTYPE.VIEW) = 0 Then tdImport.Visible = False

                        m_aryRightsForAdmin = GetUserRightsForPage_Other(13, 48)
                        If m_aryRightsForAdmin(RIGHTSTYPE.VIEW) = 0 Then tdAdvanced.Visible = False
                    End If
                    lblLoggedin.Text = "<b>Logged in as:</b> " & Session("ContactName")
                    If Session("Trial") = True Then
                        lblSubMessage.Text = "Trial Version"
                    ElseIf Session("NoOfDaysLeft") < 30 Then
                        lblSubMessage.Text = "Subscription will expire in " & Session("NoOfDaysLeft") & " days"
                    End If

                    'Following function is commentated because we are now not showing seperate reminders
                    'action item. it is merged with calender reminder popup.
                    'OpenActionItemAlert()
                    BuildTotalMenu()
                    BuildShortCutBar()
                    SetSessionTimeout()

                    hypConfMenu.Attributes.Add("onclick", "javascript:return OpenConf()")
                    hplEditHelp.Attributes.Add("onclick", "javascript:parent.frames['mainframe'].location.href='../HelpModule/HelpList.aspx';UnSelectTabs();")
                    hplSubscription.Attributes.Add("onclick", "javascript:parent.frames['mainframe'].location.href='../Service/frmSerNav.htm';UnSelectTabs();")
                    hplAdministration.Attributes.Add("onclick", "javascript:parent.frames['mainframe'].location.href='../admin/frmAdminNav.aspx';UnSelectTabs();")
                    hplHelp.Attributes.Add("onclick", "javascript:UnSelectTabs();window.open('../admin/frmLinks.aspx','Help');")
                    'hplImport.Attributes.Add("onclick", "javascript:parent.frames['mainframe'].location.href='../admin/importRecord.aspx';UnSelectTabs();")
                    hplAdvanced.Attributes.Add("onclick", "javascript:parent.frames['mainframe'].location.href='../admin/frmAdvNav.aspx';UnSelectTabs();")

                    Dim objWF As New Workflow
                    objWF.DomainID = Session("DomainID")
                    Dim dtResult As DataSet = objWF.GetAlertMessageData()
                    If dtResult.Tables(0).Rows.Count > 0 Then
                        'lblAlertMessage.Text = CCommon.ToString(dtResult.Rows(0)("vcAlertMessage"))
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "MyKey", "OpenAlertsWindow();", True)


                    Else
                        lblAlertMessage.Text = Nothing

                    End If
                End If
                bindCalendar()
            End If
            lnkbtnLogOut.Attributes.Add("onclick", "return Login()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindCalendar()
        Try
            If Session("DomainID") > 0 Then



                'CustomDataProvider = New CustomDataProvider
                'WebScheduleInfo1.AppointmentFormPath = "../outlook/AppointmentAdd.aspx"
                'WebScheduleInfo1.ReminderFormPath = "../outlook/Remind.aspx"

                If Session("ResourceId") Is Nothing Then
                    Dim objOutlook As New COutlook
                    Dim dtTable As DataTable
                    objOutlook.UserCntID = Session("UserContactId")
                    objOutlook.DomainID = Session("DomainId")
                    dtTable = objOutlook.GetResourceId()
                    If Not dtTable Is Nothing Then
                        If dtTable.Rows.Count > 0 Then
                            ' Dim activeuser As String = Session("ContactName").ToString
                            Session("ResourceId") = dtTable.Rows(0).Item("ResourceId")
                        End If
                    End If
                End If
                'Me.WebScheduleInfo1.ActiveResourceName = Session("ResourceId")
                'Me.WebScheduleInfo1.LoggedOnUserName = Session("ResourceId")
                ''   Me.WebScheduleInfo1.ActiveResource.Key = Session("ResourceId")
                'WebScheduleInfo1.TimeZoneOffset = TimeSpan.FromMinutes(-Session("ClientMachineUTCTimeOffset"))
                ''WebScheduleInfo1.TimeZoneOffset = TimeSpan.FromHours(-8)
                'CustomDataProvider.WebScheduleInfo = WebScheduleInfo1
                'CustomDataProvider.SnoozePersistenceType = Infragistics.WebUI.WebSchedule.SnoozePersistenceType.Cookie
                'CustomDataProvider.Page = Me.Page
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BuildShortCutBar()
        Try
            If Session("DomainID") > 0 Then

                If Session("TabEvent") = 0 Then
                    RadTabStrip1.OnClientMouseOver = "onTabMouseOver"
                    RadTabStrip1.OnClientMouseOut = "onTabMouseOut"
                    RadTabStrip1.OnClientTabSelected = "onTabSelected"

                    RadTabStrip1.OnClientDoubleClick = ""
                    RadTabStrip1.OnClientTabSelecting = ""
                ElseIf Session("TabEvent") = 1 Then
                    RadTabStrip1.OnClientMouseOver = ""
                    RadTabStrip1.OnClientMouseOut = ""
                    RadTabStrip1.OnClientTabSelected = ""

                    RadTabStrip1.OnClientDoubleClick = "onTabDoubleClick"
                    RadTabStrip1.OnClientTabSelecting = "OnTabSelecting"
                End If

                Dim objShortCutBar As New CShortCutBar
                objShortCutBar.ContactId = Session("UserContactID")
                objShortCutBar.DomainID = Session("DomainID")
                objShortCutBar.GroupId = Session("UserGroupID")
                Dim ds As DataSet
                ds = objShortCutBar.GetMenu()
                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")

                'divFavourite.Controls.Clear()

                Dim dtTable As DataTable
                dtTable = ds.Tables(0)

                Dim name, link, NewLink As String

                Dim sb As New System.Text.StringBuilder
                Dim iRowIndex As Integer = 0
                sb.Append(vbCrLf & "var arrTabFieldConfig = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript

                Dim strFavourit As String = String.Empty

                For Each row As DataRow In dtTable.Rows
                    If row.Item("id") = 145 Then 'Remove Assembly Items
                        Continue For
                    End If
                    'If row.Item("id") = 43 Then
                    '    Dim strLink As String = row.Item("Link") & Session("UserContactId")
                    '    link = "reDirect('" & strLink & "')"
                    'Else
                    '    link = "reDirect('" & row.Item("Link") & "')"
                    'End If
                    NewLink = ""
                    link = ""
                    name = ""

                    row.Item("Link") = Replace(row.Item("Link"), "RecordID", Session("UserContactID"))

                    If CCommon.ToBool(row("bitNew")) = True AndAlso CCommon.ToString(row.Item("NewLink")).Length > 0 Then
                        If CCommon.ToBool(row("bitNewPopup")) = True Then
                            NewLink = "Goto('" & row.Item("NewLink") & "','cntOpenItem','divNew',1000,645)"
                        Else
                            NewLink = "reDirect('" & row.Item("NewLink") & "',1)"
                        End If

                        'Dim strLink As String = row("NewLink").ToString.ToLower

                        'If strLink.Contains("frmsalesfulfullment.aspx") Or strLink.Contains("frmsalesreturns") Or strLink.Contains("frmmanageworkorder") Or strLink.Contains("frmpurchasefulfillment") Or strLink.Contains("frmplangprocurement") Then
                        '    NewLink = "OpenInMainFrame('" & row.Item("NewLink") & "')"
                        'End If
                    End If

                    If CCommon.ToBool(row("bitPopup")) = True Then
                        link = "Goto('" & row.Item("Link") & "','cntOpenItem','divNew',1000,645)"
                    Else
                        If CCommon.ToBool(row("bitNew")) = True AndAlso NewLink.Length = 0 Then
                            link = "reDirect('" & row.Item("Link") & "',1)"
                        Else
                            link = "reDirect('" & row.Item("Link") & "',0)"
                        End If
                    End If

                    Select Case row.Item("Name").ToString
                        Case "MyLeads" : name = "My " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        Case "WebLeads" : name = "Web " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        Case "PublicLeads" : name = "Public " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        Case "Contacts" : name = dtTab.Rows(0).Item("vcContact").ToString & "s"
                        Case "Prospects" : name = dtTab.Rows(0).Item("vcProspect").ToString & "s"
                        Case "Accounts" : name = dtTab.Rows(0).Item("vcAccount").ToString & "s"
                        Case "Action-Item" : name = row.Item("Name").ToString & "s"
                        Case Else
                            name = row.Item("Name")
                    End Select
                    If link.ToLower().Contains("frmcompanylist.aspx") Then
                        name = row.Item("Name") & "s"
                    End If

                    sb.Append("arrTabFieldConfig[" & iRowIndex & "] = new TabMenuLink('" & row("id").ToString & "','" & name & "',""" & link & """,'" & CCommon.ToLong(row("numTabId")) & "','" & CCommon.ToString(row("vcImage")) & "','" & CCommon.ToBool(row("bitNew")) & "',""" & NewLink & """);" & vbCrLf)
                    iRowIndex += 1

                    If CCommon.ToBool(row("bitFavourite")) = True Then
                        strFavourit += "<a class=""normal11"" style=""cursor: pointer;font-size:11.5px;color:#F5F5DC;"" onclick=""" & link & """ title=""" & name & """>" & name & "</a>"

                        'Dim h As New HyperLink
                        'h.CssClass = "normal11"
                        'h.ToolTip = name
                        'h.Text = name
                        'h.Attributes.Add("onclick", link)
                        'h.Attributes.Add("style", "cursor: pointer;font-size:11.5px;color:#63D13B;")

                        'h.NavigateUrl = "#"

                        'divFavourite.Controls.Add(h)

                        If CCommon.ToBool(row("bitNew")) = True AndAlso NewLink.Length > 0 Then
                            '    divFavourite.Controls.Add(New LiteralControl("&nbsp;<a class=""normal11"" style=""cursor: pointer;font-size:11.5px;"" onclick=""" & NewLink & """><img class=""alignbottom"" border=""0"" title=""" & name & """ src=""../images/AddRecord.png""></a>"))
                            strFavourit += " <a class=""normal11"" style=""cursor: pointer;font-size:11.5px;"" onclick=""" & NewLink & """><img class=""alignbottom"" border=""0"" title=""" & name & """ src=""../images/AddRecord.png""></a>"
                        End If

                        strFavourit += "  |  "
                        'divFavourite.Controls.Add(New LiteralControl("&nbsp;&nbsp;|&nbsp;&nbsp;"))

                        'If row.Item("id") = 43 Then
                        '    Dim strLink As String = row.Item("Link") & Session("UserContactId")
                        '    h.Attributes.Add("onclick", "reDirect('" & strLink & "')")
                        'Else : h.Attributes.Add("onclick", "reDirect('" & row.Item("Link") & "')")
                        'End If

                        'Select Case row.Item("Name").ToString
                        '    Case "MyLeads" : h.Text = "My " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        '    Case "WebLeads" : h.Text = "Web " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        '    Case "PublicLeads" : h.Text = "Public " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        '    Case "Contacts" : h.Text = dtTab.Rows(0).Item("vcContact").ToString & "s"
                        '    Case "Prospects" : h.Text = dtTab.Rows(0).Item("vcProspect").ToString & "s"
                        '    Case "Accounts" : h.Text = dtTab.Rows(0).Item("vcAccount").ToString & "s"
                        '    Case Else
                        '        h.Text = row.Item("Name")
                        'End Select

                        ''h.NavigateUrl = row.Item("Link")

                        'h.ToolTip = row.Item("Name")
                        'h.Text = "<font color=white>" & h.Text & "</font>"
                        'h.NavigateUrl = "#"
                        'Dim l As New Label
                        'l.Text = " &nbsp"
                        'tdLink.Controls.Add(h)
                        'tdLink.Controls.Add(l)
                    End If

                Next

                divFavourite.InnerHtml = "&nbsp;&nbsp;&nbsp;" & strFavourit.TrimEnd().TrimEnd("|")

                ClientScript.RegisterClientScriptBlock(Me.GetType, "TabMenuScript", sb.ToString(), True)

                'tdNew.Controls.Clear()

                'For Each row As DataRow In ds.Tables(1).Rows
                '    Dim h As New HyperLink
                '    h.CssClass = "normal11"
                '    If row.Item("id") = 57 Then
                '        h.Attributes.Add("onclick", "reDirect('" & row.Item("Link") & "')")
                '    ElseIf row.Item("id") = 16 Then 'Or row.Item("id") = 66 Then
                '        h.Attributes.Add("onclick", "Goto('" & row.Item("Link") & "','cntOpenItem','divNew',1000,645)")
                '    ElseIf row.Item("id") = 66 Then
                '        h.Attributes.Add("onclick", "Goto('" & row.Item("Link") & "','cntOpenItem','divNew',1000,600)")
                '    ElseIf row.Item("id") = 87 Then
                '        h.Attributes.Add("onclick", "Goto('" & row.Item("Link") & "','cntOpenItem','divNew',1000,600)")
                '    Else : h.Attributes.Add("onclick", "Goto('" & row.Item("Link") & "','cntOpenItem','divNew',830,450)")
                '    End If

                '    Select Case row.Item("Name").ToString
                '        Case "Lead" : h.Text = dtTab.Rows(0).Item("vcLead").ToString
                '        Case "Contact" : h.Text = dtTab.Rows(0).Item("vcContact").ToString
                '        Case "Prospect" : h.Text = dtTab.Rows(0).Item("vcProspect").ToString
                '        Case "Account" : h.Text = dtTab.Rows(0).Item("vcAccount").ToString
                '        Case Else
                '            h.Text = row.Item("Name")
                '    End Select
                '    Dim strLink As String = row("Link").ToString.ToLower

                '    If strLink.Contains("frmsalesfulfullment.aspx") Or strLink.Contains("frmsalesreturns") Or strLink.Contains("frmmanageworkorder") Or strLink.Contains("frmpurchasefulfillment") Or strLink.Contains("frmplangprocurement") Then
                '        h.Attributes.Add("onclick", "OpenInMainFrame('" & row.Item("Link") & "')")
                '    End If

                '    'h.Text = row.Item("Name")
                '    h.ToolTip = row.Item("Name")
                '    h.NavigateUrl = "#"
                '    h.Text = "<font color=white>" & h.Text & "</font>"
                '    tdNew.Controls.Add(h)
                '    Dim l As New Label
                '    l.Text = " &nbsp"
                '    tdNew.Controls.Add(h)
                '    tdNew.Controls.Add(l)
                'Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub OpenActionItemAlert()
        Try
            Dim i As Integer
            Dim objAdmin As New ActionItem
            Dim dtActItemAlert As DataTable
            objAdmin.UserCntID = Session("UserContactID")
            objAdmin.DomainID = Session("DomainID")
            objAdmin.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtActItemAlert = objAdmin.GetActionItemAlert
            Dim sb As New System.Text.StringBuilder

            For i = 0 To dtActItemAlert.Rows.Count - 1
                sb.Append("window.open('../admin/ActionItemDetailsOld.aspx?Popup=True&CommID=" & dtActItemAlert.Rows(i).Item("Id") & "&ContID=" & dtActItemAlert.Rows(i).Item("numContactId") & "&DivID=" & dtActItemAlert.Rows(i).Item("numDivisionID") & "&CompID=" & dtActItemAlert.Rows(i).Item("numCompanyId") & "','','width=800,height=600,status=no,scrollbar=yes,top=110,left=150');")
            Next
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), sb.ToString, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'replacing Aspnet Expert Menu Control with telerik as it doesn't support keeping tab highlated.-by chintan 30/01/2010
    Private Sub BuildTotalMenu()
        Try
            If Session("DomainID") > 0 Then
                CallToLoadSimpleSearchFields()

                Dim dtTabData As DataTable
                Dim objTab As New Tabs
                objTab.GroupID = Session("UserGroupID")
                objTab.RelationshipID = 0
                objTab.DomainID = Session("DomainID")
                dtTabData = objTab.GetTabData()
                Dim i As Integer
                For i = 0 To dtTabData.Rows.Count - 1
                    '''Added by Debasish Nag (09/10/2005) to include dynamic fields in simple search / group
                    'btnGo.Attributes.Add("onclick", "javascript: fun_Search();")    'Call to validate form inputs when Go button is clicked

                    'If dtTabData.Rows(i).Item("numTabId") = 82 Then
                    '    dtTabData.Rows(i).Item("vcURL") = dtTabData.Rows(i).Item("vcURL") & Session("UserContactId")
                    'End If
                    dtTabData.Rows(i).Item("vcURL") = Replace(dtTabData.Rows(i).Item("vcURL"), "RecordID", Session("UserContactID"))

                    If dtTabData.Rows(i).Item("bitFixed") = True Then
                        CreateItem(dtTabData.Rows(i).Item("numTabName"), dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"), CCommon.ToString(dtTabData.Rows(i).Item("vcImage")))
                    Else
                        CreateItem(dtTabData.Rows(i).Item("numTabName"), "include/SimpleMenu.aspx?Page=" & dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"), CCommon.ToString(dtTabData.Rows(i).Item("vcImage")))
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Function CreateParentItem(ByVal text As String, ByVal leftIcon As String, ByVal leftIconOver As String) As MenuItem
    '    Try
    '        'Dim result As MenuItem = CreateItem(text, leftIcon, leftIconOver)
    '        'result.SubMenu = New MenuGroup
    '        Return result
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Private Sub CreateItem(ByVal text As String, ByVal URL As String, ByVal Value As Long, ByVal Image As String)
        Try
            'Dim result As MenuItem = New MenuItem(text)
            'result.NavigateUrl = "../" & leftIcon
            'result.Target = "mainframe"
            ' result.OnClientAfter.Click = ("OpeninFrame('" & leftIcon & "')")
            Dim radtab As New RadTab(text, Value)
            radtab.Target = "mainframe"
            radtab.NavigateUrl = "../" & URL

            If Image.Length > 0 Then
                radtab.ImageUrl = Image
            End If
            'radtab.SelectedCssClass = "TopItemTextHoveredClass"
            'radtab.HoveredCssClass = "TopItemTextHoveredClass"
            'If Value = 44 Then
            '    Dim objTab As New Tabs
            '    Dim lngCount As Long = 0
            '    objTab.UserCntID = Session("UserContactID")
            '    objTab.DomainID = Session("DomainID")
            '    lngCount = objTab.GetMailCount()

            '    radtab.Text = text & " (<span id='spnMail'>" & lngCount & "</span>)"
            'End If
            RadTabStrip1.Tabs.Add(radtab)
            'Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Attaches the filed list for simple search
    ''' </summary>
    ''' <remarks>
    '''     The Simple Search contains dynamic fields to search for each user groups. This dynamic fileds are displayed here
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/10/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Function CallToLoadSimpleSearchFields()
        Try
            Dim objConfigWizard As New FormGenericAdvSearch                                 'Create an object of Adv. Search config as the functionality is common to that of Adv Search
            objConfigWizard.AuthenticationGroupID = Session("UserGroupID")                  'Set value of authenticaiton group
            objConfigWizard.DomainID = Session("DomainID")                                  'Set value of authenticaiton group
            Dim dtSimpleSearchFieldNames As DataTable                                       'declare a datatable of field names
            ddlSearch.DataSource = objConfigWizard.GetSimpleSearchFields
            ddlSearch.DataTextField = "vcFieldName"                                   'set the text attribute to search field name
            ddlSearch.DataValueField = "ID"                                   'set the value attribute of the search field name
            ddlSearch.DataBind()                                                          'databind the drop down
            If ddlSearch.Items.Count = 0 Then
                ddlSearch.Items.Add("Not Configured")
                ddlSearch.Items.FindByText("Not Configured").Value = 0
            End If
            SimpleSearchConf()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ddlSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearch.SelectedIndexChanged
        Try
            SimpleSearchConf()
            BuildShortCutBar()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub SimpleSearchConf()
        Try
            If ddlSearch.Items.Count > 1 And ddlSearch.SelectedItem.Value <> "0" Then
                Dim str As String()
                str = ddlSearch.SelectedItem.Value.Split("~")
                If str(2) = "SelectBox" Then
                    txtSearch1.Visible = False
                    ddlSearch1.Visible = True
                    chkSearch1.Visible = False
                    If str(1) = "numCampaignID" Then
                        objCommon.sb_FillComboFromDB(ddlSearch1, 18, CCommon.ToLong(Session("DomainID")))
                    ElseIf str(5) = "S" Then
                        FillAllState(ddlSearch1, Session("DomainID"))
                    ElseIf str(5) = "T" Then
                        objCommon.sb_FillComboFromDB(ddlSearch1, 78, Session("DomainID"))
                    ElseIf str(5) = "G" Then
                        objCommon.sb_FillGroups(ddlSearch1)
                    Else : objCommon.sb_FillComboFromDB(ddlSearch1, str(4), Session("DomainID"))
                    End If
                ElseIf str(2) = "TextBox" Then
                    txtSearch1.Visible = True
                    ddlSearch1.Visible = False
                    chkSearch1.Visible = False
                ElseIf str(2) = "CheckBox" Then
                    txtSearch1.Visible = False
                    ddlSearch1.Visible = False
                    chkSearch1.Visible = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            BuildShortCutBar() 'moved from bottom to top- by chintan , reason: it doesn't get executed when searching for bizdocid and sales order with Exit Sub
            If ddlSearch.Items.Count > 0 And ddlSearch.SelectedItem.Value <> "0" Then
                Dim str As String()
                str = ddlSearch.SelectedItem.Value.Split("~")
                Dim strScript As String
                Dim strFieldValue As String = ""

                If str(3) = "R" Then
                    If str(1).ToLower = "vcBizDocID".ToLower Then
                        Session("WhereContditionOpp") = " and OpportunityBizDocs.vcBizDocId ilike '%" & Replace(txtSearch1.Value.Trim, "'", "''") & "%'"
                        strScript = "<script language='javascript'>parent.frames['mainframe'].location.href='../admin/frmItemSearchRes.aspx'</script>"
                        If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                        Exit Sub
                    End If
                    If str(1).ToLower = "vcPOppName".ToLower Then
                        Session("WhereContditionOpp") = " and OppMas.vcPOppName ilike '%" & Replace(txtSearch1.Value.Trim, "'", "''") & "%'"
                        strScript = "<script language='javascript'>parent.frames['mainframe'].location.href='../admin/frmItemSearchRes.aspx'</script>"
                        If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                        Exit Sub
                    End If
                    If str(1).ToLower = "vcRefOrderNo".ToLower Then
                        Session("WhereContditionOpp") = " and OpportunityBizDocs.vcRefOrderNo ilike '%" & Replace(txtSearch1.Value.Trim, "'", "''") & "%'"
                        strScript = "<script language='javascript'>parent.frames['mainframe'].location.href='../admin/frmItemSearchRes.aspx'</script>"
                        If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                        Exit Sub
                    End If
                    If str(1).ToLower = "vcMarketplaceOrderID".ToLower Then
                        Session("WhereContditionOpp") = " and oppMas.vcMarketplaceOrderID ilike '%" & Replace(txtSearch1.Value.Trim, "'", "''") & "%'"
                        strScript = "<script language='javascript'>parent.frames['mainframe'].location.href='../admin/frmItemSearchRes.aspx'</script>"
                        If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                        Exit Sub
                    End If
                    If str(1).ToLower = "numCampaignID".ToLower Then
                        Session("WhereCondition") = " and DM.numCampaignID = " & ddlSearch1.SelectedValue
                    End If
                    If str(2) = "SelectBox" Then
                        strFieldValue = ddlSearch1.SelectedValue
                        If str(1).ToLower = "numCountry".ToLower Or str(1).ToLower = "numShipCountry".ToLower Or str(1).ToLower = "numBillCountry".ToLower Then
                            If str(1).ToLower = "numShipCountry".ToLower Then
                                Session("WhereCondition") = " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 and numCountry=" & strFieldValue & ")"
                            ElseIf str(1).ToLower = "numBillCountry".ToLower Then
                                Session("WhereCondition") = " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 and numCountry=" & strFieldValue & ")"
                            Else
                                Session("WhereCondition") = " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=1 AND tintAddressType=0 and numCountry=" & strFieldValue & ")"
                            End If
                        ElseIf str(1).ToLower = "numState".ToLower Or str(1).ToLower = "numShipState".ToLower Or str(1).ToLower = "numBillState".ToLower Then
                            If str(1).ToLower = "numShipState".ToLower Then
                                Session("WhereCondition") = " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 and numState=" & strFieldValue & ")"
                            ElseIf str(1).ToLower = "numBillState".ToLower Then
                                Session("WhereCondition") = " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 and numState=" & strFieldValue & ")"
                            Else
                                Session("WhereCondition") = " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=1 AND tintAddressType=0 and numState=" & strFieldValue & ")"
                            End If
                        Else : Session("WhereCondition") = " and " & str(1) & " = " & strFieldValue
                        End If
                    ElseIf str(2) = "TextBox" Or str(2) = "TextArea" Then
                        strFieldValue = Replace(txtSearch1.Value.Trim, "'", "''")
                        If str(1).ToLower = "numAge".ToLower Then
                            If IsNumeric(txtSearch1.Value.Trim) = False Then
                                Session("WhereCondition") = " and  DM.numDivisionID=0"
                            Else : Session("WhereCondition") = " and Year(bintDOB) = '" & Year(DateAdd(DateInterval.Year, -IIf(IsNumeric(txtSearch1.Value.Trim), txtSearch1.Value.Trim, 0), Now())) & "'"
                            End If
                        ElseIf str(1).ToLower = "vcPostalCode" Or str(1).ToLower = "vcShipPostalCode".ToLower Or str(1).ToLower = "vcBillPostalCode".ToLower Then 'ShipPostal/BillPostal
                            If str(1).ToLower = "vcShipPostalCode".ToLower Then
                                Session("WhereCondition") = " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 and vcPostalCode ilike '%" & strFieldValue & "%')"
                            ElseIf str(1).ToLower = "vcBillPostalCode".ToLower Then
                                Session("WhereCondition") = " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 and vcPostalCode ilike '%" & strFieldValue & "%')"
                            Else
                                Session("WhereCondition") = " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=1 AND tintAddressType=0 and vcPostalCode ilike '%" & strFieldValue & "%')"
                            End If
                        ElseIf str(1).ToLower = "vcCity".ToLower Or str(1).ToLower = "vcShipCity".ToLower Or str(1).ToLower = "vcBillCity".ToLower Then
                            If str(1).ToLower = "vcShipCity".ToLower Then
                                Session("WhereCondition") = " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 and vcCity ilike '%" & strFieldValue & "%')"
                            ElseIf str(1).ToLower = "vcBillCity".ToLower Then
                                Session("WhereCondition") = " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 and vcCity ilike '%" & strFieldValue & "%')"
                            Else
                                Session("WhereCondition") = " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=1 AND tintAddressType=0 and vcCity ilike '%" & strFieldValue & "%')"
                            End If
                        ElseIf str(1).ToLower = "vcStreet".ToLower Or str(1).ToLower = "vcShipStreet".ToLower Or str(1).ToLower = "vcBillStreet".ToLower Then
                            If str(1).ToLower = "vcShipStreet".ToLower Then
                                Session("WhereCondition") = " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 and vcStreet ilike '%" & strFieldValue & "%')"
                            ElseIf str(1).ToLower = "vcBillStreet".ToLower Then
                                Session("WhereCondition") = " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 and vcStreet ilike '%" & strFieldValue & "%')"
                            Else
                                Session("WhereCondition") = " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=1 AND tintAddressType=0 and vcStreet ilike '%" & strFieldValue & "%')"
                            End If
                        Else : Session("WhereCondition") = " and " & str(1) & " ilike '%" & strFieldValue & "%'"
                        End If
                    End If
                ElseIf str(3) = "D" Then
                    If str(2) = "SelectBox" Or str(2) = "SelectBox" Then
                        Session("WhereCondition") = " and DM.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID=" & str(0) & " and Fld_Value =" & ddlSearch1.SelectedValue & ")"
                    ElseIf str(2) = "TextBox" Or str(2) = "TextArea" Then
                        Session("WhereCondition") = " and DM.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID=" & str(0) & " and Fld_Value ilike '%" & Replace(txtSearch1.Value.Trim, "'", "''") & "%')"
                    ElseIf str(2) = "CheckBox" Then
                        If chkSearch1.Checked Then
                            Session("WhereCondition") = " and DM.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID=" & str(0) & " and Fld_Value =1)"
                        Else
                            Session("WhereCondition") = " and DM.numDivisionID NOT IN (select RecId from CFW_FLD_Values where Fld_ID=" & str(0) & " and Fld_Value =1)"
                        End If
                    End If
                ElseIf str(3) = "C" Then
                    If str(2) = "SelectBox" Or str(2) = "SelectBox" Then
                        Session("WhereCondition") = " and ADC.numContactID in (select RecId from CFW_FLD_Values_Cont where Fld_ID=" & str(0) & " and Fld_Value =" & ddlSearch1.SelectedValue & ")"
                    ElseIf str(2) = "TextBox" Or str(2) = "TextArea" Then
                        Session("WhereCondition") = " and ADC.numContactID in (select RecId from CFW_FLD_Values_Cont where Fld_ID=" & str(0) & " and Fld_Value ilike '%" & Replace(txtSearch1.Value.Trim, "'", "''") & "%')"
                    End If

                End If
                Session("TimeQuery") = Nothing ' If user already used adv search then clean Previously stored session value
                strScript = "<script language='javascript'>parent.frames['mainframe'].location.href='../admin/frmAdvancedSearchRes.aspx'</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                '  Response.Redirect("../admin/frmAdvancedSearchRes.aspx?frmScreen=" & Request.Url.ToString)
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub WebScheduleInfo1_FrameIntervalChanging(ByVal sender As Object, ByVal e As Infragistics.WebUI.WebSchedule.FrameIntervalChangingEventArgs) Handles WebScheduleInfo1.FrameIntervalChanging
    '    Try
    '        e.FrameInterval.StartDate = Infragistics.WebUI.Shared.SmartDate.Today.AddDays(-30)
    '        e.FrameInterval.EndDate = Infragistics.WebUI.Shared.SmartDate.Today.AddDays(+30)
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BuildShortCutBar()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick


    '    bindCalendar()
    '    'BuildShortCutBar()
    'End Sub

    'Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    '    Try
    '        If Session("DomainID") > 0: Then
    '            OpenActionItemAlert()
    '            BuildShortCutBar()
    '            'Timer1.Enabled = True
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
    Private Sub SetSessionTimeout()
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            Dim dtTable As DataTable = objUserAccess.GetDomainDetails()
            If dtTable.Rows.Count > 0 Then
                hdnSessionTimeout.Value = CCommon.ToInteger(dtTable.Rows(0)("tintSessionTimeOut")) * 60 * 60
            Else
                hdnSessionTimeout.Value = 60 * 60
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub TimerReminders_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerReminders.Tick
    '    Try
    '        If Session("DomainID") > 0 Then
    '            OpenRemindersAlert()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub OpenRemindersAlert()
    '    Try
    '        Dim df As wdCalender.Code.DataFeedClass = New wdCalender.Code.DataFeedClass
    '        Dim dtReminderList As DataTable

    '        dtReminderList = df.listCalendarReminders()
    '        Dim sb As New System.Text.StringBuilder

    '        If dtReminderList.Rows.Count > 0 Then
    '            Session("dtReminderList") = dtReminderList
    '            sb.Append("window.open('../OutlookCalendar/Reminder.aspx','','width=800,height=600,status=no,scrollbar=yes,top=110,left=150');")
    '        End If

    '        ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), sb.ToString, True)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub btnDismiss_Click(sender As Object, e As EventArgs) Handles btnDismiss.Click
        Try
            If Not String.IsNullOrEmpty(hdnActionItemID.Value) AndAlso Not String.IsNullOrEmpty(hdnType.Value) Then
                If hdnType.Value = "1" Then
                    'Calender
                    Dim ObjOutlook As New COutlook
                    ObjOutlook.ActivityID = CCommon.ToLong(hdnActionItemID.Value)
                    ObjOutlook.Status = 0
                    ObjOutlook.LastReminderDateTimeUtc = DateTime.UtcNow
                    ObjOutlook.UpdateReminderByActivityId()
                ElseIf hdnType.Value = "2" Then
                    'Action Item / Communication
                    Dim objActionItem As New ActionItem
                    objActionItem.CommID = CCommon.ToLong(hdnActionItemID.Value)
                    objActionItem.DismissReminder()
                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ReloadReminder", "RemindCall();", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class