Imports BACRM.BusinessLogic.Common
Partial Public Class SyncFusionCalendar
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim dateformat As String = Session("DateFormat")
                If dateformat = "MM/DD/YYYY" Then
                    syncCalendar.CustomFormat = "MM/dd/yyyy"
                ElseIf dateformat = "MM-DD-YYYY" Then
                    syncCalendar.CustomFormat = "MM-dd-yyyy"
                ElseIf dateformat = "DD/MON/YYYY" Then
                    syncCalendar.CustomFormat = "dd/MMM/yyyy"
                ElseIf dateformat = "DD-MON-YYYY" Then
                    syncCalendar.CustomFormat = "dd-MMM-yyyy"
                ElseIf dateformat = "DD/MONTH/YYYY" Then
                    syncCalendar.CustomFormat = "dd/MMMM/yyyy"
                ElseIf dateformat = "DD-MONTH-YYYY" Then
                    syncCalendar.CustomFormat = "dd-MMMM-yyyy"
                ElseIf dateformat = "DD/MM/YYYY" Then
                    syncCalendar.CustomFormat = "dd/MM/yyyy"
                ElseIf dateformat = "DD-MM-YYYY" Then
                    syncCalendar.CustomFormat = "dd-MM-yyyy"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Property SelectedDate() As String
        Get
            Try
                If syncCalendar.Text = "" Then Return DateFromFormattedDate(syncCalendar.Text, Session("DateFormat"))
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As String)
            Try
                IIf(IsDBNull(Value), Value = "", Value)
                If Value <> "" Then syncCalendar.Text = FormattedDateFromDate(Value, Session("DateFormat"))
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

End Class