﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddOrganization.aspx.vb"
    Inherits=".frmAddOrganization" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Relationship</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" src="../javascript/CustomFieldValidation.js"></script>
    <script type="text/javascript" language="javascript">
        function Close() {
            window.close()
            return false;
        }
        function Save() {
            return validateCustomFields();
        }
        function OpenAdd(a) {
            window.open("../prospects/frmProspectsAdd.aspx?Reload=2&pwer=dfsdfdsfdsfdsf&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
        function UpdateAddress(BillID, ShipID) {
            $("#hdnBillToAddressID").val(BillID);
            $("#hdnShipToAddressID").val(ShipID);
        }
        $(document).ready(function () {
            $(':input:enabled:visible:first[type=text]').focus()
        });

        function OpenEmailChangeWindow(contactID, email) {
            window.open('../contact/frmDuplicateContactEmail.aspx?contactID=' + contactID + '&email=' + email, 'ContactChangeEmail', 'toolbar=no,titlebar=no,top=200,left=200,width=800,height=400,scrollbars=no,resizable=no')
            return false;
        }

        function SaveAfterExistingContactEmailChange() {
            $('[id$=btnSave]').click();
            return true;
        }

        function ChangeEmailAndSave(newEmail) {
            $('[id$=hdnChangedEmail]').val(newEmail);
            $('[id$=btnSave]').click();
            return true;
        }
    </script>
     <style type="text/css">
        #tblMain td:nth-child(1), td:nth-child(3) {
            font-weight:bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table id="Table1" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="right">
                        &nbsp;
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
                        </asp:Button>
                        &nbsp;
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close">
                        </asp:Button>
                        &nbsp;
                        <asp:HiddenField ID="hdnChangedEmail" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal4" align="center">
                                    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New
    <asp:Label ID="lblRelationship" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="840px"
        CssClass="aspTable" BorderColor="black" GridLines="None" BorderWidth="1" runat="server">
    </asp:Table>
    <br />
    <asp:HiddenField runat="server" ID="hdnIsDuplicate" Value="0" />
    <asp:HiddenField ID="hdnBillToAddressID" runat="server" />
    <asp:HiddenField ID="hdnShipToAddressID" runat="server" />
</asp:Content>
