Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Workflow
Partial Public Class frmNewOrganization
    Inherits BACRMPage
    Dim lngDivisionId As Long
    Dim lngCntId As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")
                 
                
               
                GetUserRightsForPage(32, 2)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnSave.Visible = False
                Else : btnSave.Visible = True
                End If
                BindCampaign()
                objCommon = New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlRelationShip, 5, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlTerritory, 78, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlInfoSource, 18, Session("DomainID"))

                objCommon.sb_FillGroupsFromDBSel(ddlGroup)

                If Not ddlGroup.Items.FindByValue(CCommon.ToInteger(GetQueryStringVal( "GrpID"))) Is Nothing Then
                    ddlGroup.ClearSelection()
                    ddlGroup.Items.FindByValue(CCommon.ToInteger(GetQueryStringVal( "GrpID"))).Selected = True
                End If

                For Each Item As ListItem In ddlGroup.Items
                    If Item.Text = "My Leads" Then
                        Item.Text = "My " & dtTab.Rows(0).Item("vcLead") & "s"
                    ElseIf Item.Text = "PublicLeads" Then
                        Item.Text = "Public " & dtTab.Rows(0).Item("vcLead") & "s"
                    ElseIf Item.Text = "WebLeads" Then
                        Item.Text = "Web " & dtTab.Rows(0).Item("vcLead") & "s"
                    End If
                Next
                If GetQueryStringVal( "RelID") <> "" Then
                    trRelationship.Visible = False
                    If Not ddlRelationShip.Items.FindByValue(GetQueryStringVal( "RelID")) Is Nothing Then
                        ddlRelationShip.Items.FindByValue(GetQueryStringVal( "RelID")).Selected = True
                    End If
                    lblCustomer.Text = ddlRelationShip.SelectedItem.Text
                    lblRelationship.Text = ddlRelationShip.SelectedItem.Text
                Else
                    If CCommon.ToInteger(GetQueryStringVal("CRMType")) = 0 Then
                        trGroups.Visible = True
                        trComments.Visible = True
                        lblCustomer.Text = dtTab.Rows(0).Item("vcLead")
                        lblRelationship.Text = dtTab.Rows(0).Item("vcLead")
                    ElseIf CCommon.ToInteger(GetQueryStringVal("CRMType")) = 1 Then
                        lblCustomer.Text = dtTab.Rows(0).Item("vcProspect")
                        lblRelationship.Text = dtTab.Rows(0).Item("vcProspect")
                    ElseIf CCommon.ToInteger(GetQueryStringVal("CRMType")) = 2 Then
                        lblCustomer.Text = dtTab.Rows(0).Item("vcAccount")
                        lblRelationship.Text = dtTab.Rows(0).Item("vcAccount")
                    End If
                    ddlRelationShip.Items.FindByValue("46").Selected = True
                End If
                LoadProfile()
                txtFirstName.Focus()
                btnSave.Attributes.Add("onclick", "return Save()")
                btnClose.Attributes.Add("onclick", "return Close()")
                btnBack.Attributes.Add("onclick", "return Back()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadProfile()
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.RelID = ddlRelationShip.SelectedValue
            objUserAccess.DomainID = Session("DomainID")
            ddlProfile.DataSource = objUserAccess.GetRelProfileD
            ddlProfile.DataTextField = "ProName"
            ddlProfile.DataValueField = "numProfileID"
            ddlProfile.DataBind()
            ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindCampaign()
        Try
            Dim objCampaign As New BACRM.BusinessLogic.Reports.PredefinedReports
            objCampaign.byteMode = 2
            objCampaign.DomainID = Session("DomainID")
            Dim dtData As DataTable = objCampaign.GetCampaign()
            ddlCampaign.DataSource = dtData
            ddlCampaign.DataTextField = "vcCampaignName"
            ddlCampaign.DataValueField = "numCampaignID"
            ddlCampaign.DataBind()
            ddlCampaign.Items.Insert(0, "--Select One--")
            ddlCampaign.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function InsertProspect() As Boolean
        Dim objLeads As New CLeads
        Try
            With objLeads
                If Len(txtCompany.Text) < 1 Then txtCompany.Text = txtLastName.Text & "," & txtFirstName.Text
                .CompanyName = txtCompany.Text.Trim
                .CampaignID = ddlCampaign.SelectedValue
                .InfoSource = ddlInfoSource.SelectedItem.Value
                .TerritoryID = ddlTerritory.SelectedItem.Value
                .CustName = txtCompany.Text.Trim
                .CompanyType = ddlRelationShip.SelectedValue
                If GetQueryStringVal( "RelID") <> "" Then
                    .CRMType = 1
                Else
                    .CRMType = CCommon.ToInteger(GetQueryStringVal("CRMType"))
                End If
                .DivisionName = "-"
                .LeadBoxFlg = 1
                .UserCntID = Session("UserContactId")
                .Country = Session("DefCountry")
                .SCountry = Session("DefCountry")
                .FirstName = txtFirstName.Text
                .LastName = txtLastName.Text
                .ContactPhone = txtPhone.Text
                .PhoneExt = txtextn.Text
                .ComPhone = txtCompanyPhone.Text
                .Email = txtEmail.Text
                .DomainID = Session("DomainID")
                .ContactType = 0 '70 commented by chintan, contact type 70 is obsolete. default contact type is 0
                .PrimaryContact = True

                .GroupID = ddlGroup.SelectedValue
                .Profile = ddlProfile.SelectedValue
                .WebSite = txtWebsite.Text
                .Comments = txtComments.Text
            End With
            Dim bool As Boolean = True
            If hdnIsDuplicate.Value = "0" Then
                Dim strWhere As String = objLeads.GetStrWhere()
                objLeads.WhereCondition = strWhere
                If objLeads.CheckDuplicate = True Then
                    litMessage.Text = "A Duplicate Organization found !! Click Save to Continue"
                    bool = False
                    hdnIsDuplicate.Value = "1"
                End If
            End If
            If bool = True Then
                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                lngDivisionId = objLeads.CreateRecordDivisionsInfo
                objLeads.DivisionID = lngDivisionId
                lngCntId = objLeads.CreateRecordAddContactInfo()
                'Added By Sachin Sadhu||Date:22ndMay12014
                'Purpose :To Added Organization data in work Flow queue based on created Rules
                '          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = lngDivisionId
                objWfA.SaveWFOrganizationQueue()
                'ss//end of code

                'Added By Sachin Sadhu||Date:24thJuly2014
                'Purpose :To Added Contact data in work Flow queue based on created Rules
                '         Using Change tracking
                Dim objWF As New Workflow()
                objWF.DomainID = Session("DomainID")
                objWF.UserCntID = Session("UserContactID")
                objWF.RecordID = lngCntId
                objWF.SaveWFContactQueue()
                ' ss//end of code

            End If
            Return bool
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            btnSave.Enabled = False
            If InsertProspect() = True Then
                Dim strScript As String = "<script language=JavaScript>"
                If GetQueryStringVal( "RelID") <> "" Then
                    strScript += "window.opener.reDirectPage('../prospects/frmProspects.aspx?profileid=" & ddlProfile.SelectedValue & "&RelId=" & ddlRelationShip.SelectedValue & "&DivID=" & lngDivisionId & "'); self.close();"
                Else
                    If CCommon.ToInteger(GetQueryStringVal("CRMType")) = 0 Then
                        strScript += "window.opener.reDirectPage('../Leads/frmLeads.aspx?grpID=" & ddlGroup.SelectedValue & "&DivID=" & lngDivisionId & "'); self.close();"
                    ElseIf CCommon.ToInteger(GetQueryStringVal("CRMType")) = 1 Then
                        strScript += "window.opener.reDirectPage('../prospects/frmProspects.aspx?profileid=" & ddlProfile.SelectedValue & "&RelId=" & ddlRelationShip.SelectedValue & "&DivID=" & lngDivisionId & "'); self.close();"
                    ElseIf CCommon.ToInteger(GetQueryStringVal("CRMType")) = 2 Then
                        strScript += "window.opener.reDirectPage('../account/frmAccounts.aspx?profileid=" & ddlProfile.SelectedValue & "&RelId=" & ddlRelationShip.SelectedValue & "&DivId=" & lngDivisionId & "'); self.close();"
                    End If
                End If

                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then
                    Page.RegisterStartupScript("clientScript", strScript)
                End If
            Else : btnSave.Enabled = True
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlRelationShip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationShip.SelectedIndexChanged
        Try
            LoadProfile()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class