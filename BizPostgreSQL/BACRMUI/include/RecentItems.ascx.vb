Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Public Class RecentItems
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim objAdmin As New ActionItem
                Dim i As Integer
                Dim dtActItemAlert As DataTable
                objAdmin.UserCntID = Session("UserContactID")
                objAdmin.DomainID = Session("DomainID")
                objAdmin.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtActItemAlert = objAdmin.GetActionItemAlert
                Session("ActItemAlert") = dtActItemAlert
                For i = 0 To dtActItemAlert.Rows.Count - 1
                    Response.Write("<script language=javascript>window.open('../admin/frmActionItemPopup.aspx?CommID=" & dtActItemAlert.Rows(i).Item("Id") & "&ContID=" & dtActItemAlert.Rows(i).Item("numContactId") & "&DivID=" & dtActItemAlert.Rows(i).Item("numDivisionID") & "&CompID=" & dtActItemAlert.Rows(i).Item("numCompanyId") & "','','width=700,height=500,status=no,scrollbar=yes,top=110,left=150');</script>")
                Next
                Dim dtRecentItems As DataTable
                objAdmin.UserCntID = Session("UserContactID")
                dtRecentItems = objAdmin.GetRecentItems
                DisplayRecentItems(dtRecentItems)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub DisplayRecentItems(ByVal dtRecentItems As DataTable)
        Try
            Dim i As Integer
            Dim hpl As HyperLink
            Dim tblCell As TableCell
            Dim lbl As Label
            Dim tblRow As TableRow
            tblRow = New TableRow
            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            tblCell.Text = "Last Viewed :&nbsp;&nbsp;"
            tblCell.VerticalAlign = VerticalAlign.Bottom
            tblRow.Cells.Add(tblCell)
            For i = 0 To dtRecentItems.Rows.Count - 1
                lbl = New Label
                hpl = New HyperLink
                tblCell = New TableCell
                hpl.CssClass = "normal1"
                If dtRecentItems.Rows(i).Item("Type") = "C" Then
                    If dtRecentItems.Rows(i).Item("tintCRMType") = 0 Then
                        hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                        hpl.NavigateUrl = "../Leads/frmLeads.aspx?CmpID=" & dtRecentItems.Rows(i).Item("numCompanyID") & "&DivID=" & dtRecentItems.Rows(i).Item("numDivisionID") & "&CntID=" & dtRecentItems.Rows(i).Item("numContactID")
                    ElseIf dtRecentItems.Rows(i).Item("tintCRMType") = 1 Then
                        hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                        hpl.NavigateUrl = "../prospects/frmProspects.aspx?frm=prospectlist&CmpID=" & dtRecentItems.Rows(i).Item("numCompanyID") & "&DivID=" & dtRecentItems.Rows(i).Item("numDivisionID") & "&CRMTYPE=1&CntID=" & dtRecentItems.Rows(i).Item("numContactID")
                    ElseIf dtRecentItems.Rows(i).Item("tintCRMType") = 2 Then
                        hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                        hpl.NavigateUrl = "../Account/frmAccounts.aspx?frm=accountlist&CmpID=" & dtRecentItems.Rows(i).Item("numCompanyID") & "&DivID=" & dtRecentItems.Rows(i).Item("numDivisionID") & "&CRMTYPE=2&CntID=" & dtRecentItems.Rows(i).Item("numContactID")
                    End If
                    lbl.Text = "&nbsp;&nbsp;<img src='../images/Building-16.gif'/>"
                ElseIf dtRecentItems.Rows(i).Item("Type") = "U" Then
                    hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                    hpl.NavigateUrl = "../contact/frmContacts.aspx?frm=contactlist&CmpID=" & dtRecentItems.Rows(i).Item("numCompanyID") & "&DivID=" & dtRecentItems.Rows(i).Item("numDivisionID") & "&CRMTYPE=2&CntId=" & dtRecentItems.Rows(i).Item("numContactID")
                    lbl.Text = "&nbsp;&nbsp;<img src='../images/Building-16.gif'/>"
                ElseIf dtRecentItems.Rows(i).Item("Type") = "O" Then
                    hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                    hpl.NavigateUrl = "../opportunity/frmOpportunities.aspx?frm=opportunitylist&opId=" & dtRecentItems.Rows(i).Item("numRecID")
                    lbl.Text = "&nbsp;&nbsp;<img src='../images/Building-16.gif'/>"
                ElseIf dtRecentItems.Rows(i).Item("Type") = "S" Then
                    hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                    hpl.NavigateUrl = "../cases/frmCases.aspx?frm=Caselist&CaseID=" & dtRecentItems.Rows(i).Item("numRecID")
                    lbl.Text = "&nbsp;&nbsp;<img src='../images/Building-16.gif'/>"
                ElseIf dtRecentItems.Rows(i).Item("Type") = "P" Then
                    hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                    hpl.NavigateUrl = "../projects/frmProjects.aspx?frm=ProjectList&ProId=" & dtRecentItems.Rows(i).Item("numRecID")
                    lbl.Text = "&nbsp;&nbsp;<img src='../images/Building-16.gif'/>"

                ElseIf dtRecentItems.Rows(i).Item("Type") = "R" Then
                    hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                    hpl.NavigateUrl = "../opportunity/frmReturnDetail.aspx?ReturnID=" & dtRecentItems.Rows(i).Item("numRecID")
                    lbl.Text = "&nbsp;&nbsp;<img src='../images/Return.png'/>"
                ElseIf dtRecentItems.Rows(i).Item("Type") = "B" Then
                    hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                    hpl.Attributes.Add("OnClick", "return OpenRecentBizInvoice(" & CCommon.ToLong(dtRecentItems.Rows(i).Item("numOppID")) & "," & CCommon.ToLong(dtRecentItems.Rows(i).Item("numRecID")) & ",0)")
                    lbl.Text = "&nbsp;&nbsp;<img src='../images/BizDoc.png'/>"
                End If

                hpl.Text = "<font color='#180073'>" & hpl.Text & "</font>"
                tblCell.Controls.Add(lbl)
                tblCell.Controls.Add(hpl)
                tblRow.Cells.Add(tblCell)
            Next
            If dtRecentItems.Rows.Count > 0 Then tblRecentItems.Rows.Add(tblRow)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class