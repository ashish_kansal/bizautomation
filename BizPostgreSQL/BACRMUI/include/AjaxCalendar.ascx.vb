Imports BACRM.BusinessLogic.Common
Partial Public Class AjaxCalendar
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            txtDateFormat.Text = Session("DateFormat")
            txtDate.Attributes.Add("onchange", "return FormatDate('" & txtDate.ClientID & "','" & txtFormattedDate.ClientID & "','" & txtDateFormat.ClientID & "')")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Property SelectedDate() As String
        Get
            Try
                If txtFormattedDate.Text = "" Then
                    Return ""
                Else : Return DateFromFormattedDate(txtFormattedDate.Text, Session("DateFormat"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As String)
            Try
                IIf(IsDBNull(Value), Value = "", Value)
                If Value <> "" Then
                    txtFormattedDate.Text = FormattedDateFromDate(Value, Session("DateFormat"))
                    txtDate.Text = Format(CDate(Value), "MM/dd/yyyy")
                Else : txtFormattedDate.Text = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

End Class