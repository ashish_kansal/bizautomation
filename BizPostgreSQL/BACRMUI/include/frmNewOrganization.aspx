<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmNewOrganization.aspx.vb"
    Inherits=".frmNewOrganization" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Relationship</title>
    <script type="text/javascript" language="javascript">
        function Save() {
            if (document.getElementById("txtFirstName").value == "") {
                alert("Enter First Name")
                document.getElementById("txtFirstName").focus()
                return false;
            }
            if (document.getElementById("txtLastName").value == "") {
                alert("Enter Last Name")
                document.getElementById("txtLastName").focus()
                return false;
            }
            if (document.getElementById("ddlGroup") != null) {
                if (document.getElementById("ddlGroup").value == 0) {
                    alert("Select Group ")
                    document.getElementById("ddlGroup").focus()
                    return false;
                }
            }
        }
        function Focus() {
            document.getElementById("txtFirstName").focus();
        }
        function Back() {
            window.location.href = "../include/frmSelRelationship.aspx"
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
            </asp:Button>
            <asp:Button ID="btnBack" runat="server" CssClass="button" Text="Back" Width="50">
            </asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New
    <asp:Label ID="lblRelationship" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="800px">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <br>
                <table cellspacing="2" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td rowspan="15" valign="top" style="background-image: url(../images/Building-48.gif);
                            background-repeat: no-repeat; background-position-x: 20px; background-position-y: 10px">
                            <br />
                            &nbsp;<img src="../images/Greenman-32.gif" style="position: relative; left: 16px;
                                top: 14px;" /><br />
                        </td>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblCustomer" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtCompany" runat="server" Width="200" MaxLength="50" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            First Name<font color="red">*</font>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtFirstName" runat="server" Width="200" MaxLength="50" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Last Name<font color="red">*</font>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtLastName" runat="server" Width="200" MaxLength="50" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Campaign
                        </td>
                        <td colspan="3">
                            <asp:DropDownList CssClass="signup" ID="ddlCampaign" runat="server" Width="200">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Source
                        </td>
                        <td colspan="3">
                            <asp:DropDownList CssClass="signup" ID="ddlInfoSource" runat="server" Width="200">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Contact Phone
                        </td>
                        <td colspan="3" class="normal1">
                            <asp:TextBox ID="txtPhone" runat="server" Width="150px" MaxLength="50" CssClass="signup"></asp:TextBox>&nbsp;
                            &nbsp;Ext&nbsp;
                            <asp:TextBox ID="txtextn" runat="server" Width="70" MaxLength="6" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Company Phone
                        </td>
                        <td colspan="3" class="normal1">
                            <asp:TextBox ID="txtCompanyPhone" runat="server" Width="150px" MaxLength="50" CssClass="signup"></asp:TextBox>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" style="width: 124px" align="right" colspan="1" rowspan="1">
                            Email
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtEmail" runat="server" Width="200" MaxLength="50" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trRelationship" runat="server">
                        <td class="normal1" align="right">
                            Relationship
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="ddlRelationShip" AutoPostBack="true" runat="server" Width="200"
                                CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="tr1" runat="server">
                        <td class="normal1" align="right">
                            Profile
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="ddlProfile" runat="server" Width="200" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Territory
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="ddlTerritory" runat="server" Width="200" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Web Site
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtWebsite" runat="server" Width="200" MaxLength="50" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trGroups" runat="server" visible="false">
                        <td class="normal1" align="right">
                            Group<font color="red">*</font>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="ddlGroup" runat="server" Width="200" CssClass="signup">
                            </asp:DropDownList>
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="trComments" runat="server">
                        <td class="normal1" align="right" valign="top">
                            Comments
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtComments" runat="server" CssClass="signup" Height="50" Width="200"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField runat="server" ID="hdnIsDuplicate" Value="0" />
</asp:Content>
