﻿Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Leads

Public Class SimpleSearchControl
    Inherits BACRMUserControl

#Region "Enum"
    Enum TemplateType
        Organization
        OrganizationItem
        Item
        Order
        BizDoc
        Contact
    End Enum
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                InitializeOrganizationClientSideTemplate()
                Dim objCommon As New CCommon
                objCommon.sb_FillComboFromDBwithSel(radcmbBizDocTypes, 27, Session("DomainID"))
                If (Convert.ToString(Session("OrganizationSearchCriteria")) = "1") Then
                    rblOrganizationSearchCriteria.SelectedValue = 1
                Else
                    rblOrganizationSearchCriteria.SelectedValue = 0
                End If
            Else
                'We have to reload template on each post back because it is dynamic
                If hdnSearchType.Value = "1" Then
                    InitializeOrganizationClientSideTemplate()
                ElseIf hdnSearchType.Value = "2" Then
                    InitializeOrgItemClientTemplate()
                ElseIf hdnSearchType.Value = "3" Then
                    InitializeItemClientSideTemplate()
                ElseIf hdnSearchType.Value = "4" Then
                    InitializeOrderClientSideTemplate()
                ElseIf hdnSearchType.Value = "5" Then
                    InitializeBizDocClientSideTemplate()
                ElseIf hdnSearchType.Value = "6" Then
                    InitializeContactClientSideTemplate()
                End If
            End If
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub ChangeSearchTemplateAndMethod()
        Try
            Select Case radCmbSimpleSearch.SelectedValue
                Case "1" 'Organizations
                    Select Case radcmbAdditionalFilter.SelectedValue
                        Case "1"
                            InitializeOrganizationClientSideTemplate()
                        Case "2"
                            InitializeOrgItemClientTemplate()
                        Case "3"
                            InitializeOrderClientSideTemplate()
                        Case "4"
                            InitializeBizDocClientSideTemplate()
                    End Select
                Case "2" 'Items
                    Select Case radcmbAdditionalFilter.SelectedValue
                        Case "1"
                            InitializeItemClientSideTemplate()
                        Case "2"
                            'This option is not visible for items
                        Case "3"
                            InitializeOrderClientSideTemplate()
                        Case "4"
                            InitializeBizDocClientSideTemplate()
                    End Select
                Case "3" 'Opps/Orders
                    InitializeOrderClientSideTemplate()
                Case "4" 'BizDocs
                    InitializeBizDocClientSideTemplate()
                Case "5" 'Contacts
                    InitializeContactClientSideTemplate()
            End Select
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
    Private Sub InitializeOrganizationClientSideTemplate()
        Try
            radcmbSearchText.DropDownWidth = New Unit(600)

            Dim objContact As BACRM.BusinessLogic.Contacts.CContacts
            objContact = New BACRM.BusinessLogic.Contacts.CContacts
            objContact.DomainID = CCommon.ToLong(Session("DomainID"))
            objContact.FormId = 96
            objContact.UserCntID = 0

            Dim ds As DataSet = objContact.GetColumnConfiguration

            Dim dtTable As New DataTable
            dtTable = ds.Tables(1)

            If dtTable.Rows.Count = 0 Then
                Dim drNew As DataRow
                drNew = dtTable.NewRow()
                drNew("numFieldID") = 97
                drNew("vcFieldName") = "Organization Name"
                drNew("vcDbColumnName") = "vcCompanyName"
                drNew("Custom") = 0
                drNew("tintOrder") = 0
                dtTable.Rows.InsertAt(drNew, 0)
            End If

            'genrate header template dynamically
            radcmbSearchText.HeaderTemplate = New SimpleSearchTemplate(ListItemType.Header, dtTable, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.Organization, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)

            'generate Clientside Template dynamically
            radcmbSearchText.ItemTemplate = New SimpleSearchTemplate(ListItemType.Item, dtTable, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.Organization, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)

            hdnSearchType.Value = "1"
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
    Private Sub InitializeOrgItemClientTemplate()
        Try
            'IMPORTANT - DO NOT CHANGE ORDER IN WHICH COLUMNS ARE ADDED BECAUSE IT WILL AFFECT DISPLAY
            radcmbSearchText.DropDownWidth = New Unit(850)

            Dim dtTable As New DataTable
            dtTable.Columns.Add("vcFieldName")
            dtTable.Columns.Add("vcDbColumnName")
            dtTable.Columns.Add("Custom")
            dtTable.Columns.Add("tintOrder")

            Dim drNew As DataRow
            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Date"
            drNew("vcDbColumnName") = "bintCreatedDate"
            drNew("Custom") = 0
            drNew("tintOrder") = 0
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Item"
            drNew("vcDbColumnName") = "vcItemName"
            drNew("Custom") = 0
            drNew("tintOrder") = 1
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Order"
            drNew("vcDbColumnName") = "vcPoppName"
            drNew("Custom") = 0
            drNew("tintOrder") = 2
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Type"
            drNew("vcDbColumnName") = "vcOppType"
            drNew("Custom") = 0
            drNew("tintOrder") = 3
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Units"
            drNew("vcDbColumnName") = "numUnitHour"
            drNew("Custom") = 0
            drNew("tintOrder") = 4
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Price"
            drNew("vcDbColumnName") = "monPrice"
            drNew("Custom") = 0
            drNew("tintOrder") = 5
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Organization"
            drNew("vcDbColumnName") = "vcCompanyName"
            drNew("Custom") = 0
            drNew("tintOrder") = 6
            dtTable.Rows.Add(drNew)

            'genrate header template dynamically
            radcmbSearchText.HeaderTemplate = New SimpleSearchTemplate(ListItemType.Header, dtTable, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.OrganizationItem, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)
            'generate Clientside Template dynamically
            radcmbSearchText.ItemTemplate = New SimpleSearchTemplate(ListItemType.Item, dtTable, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.OrganizationItem, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)

            hdnSearchType.Value = "2"
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
    Private Sub InitializeItemClientSideTemplate()
        Try
            radcmbSearchText.DropDownWidth = New Unit(600)

            Dim objContact As BACRM.BusinessLogic.Contacts.CContacts
            objContact = New BACRM.BusinessLogic.Contacts.CContacts
            objContact.DomainID = CCommon.ToLong(Session("DomainID"))
            objContact.FormId = 22
            objContact.UserCntID = 0

            Dim ds As DataSet = objContact.GetColumnConfiguration
            Dim dtTable As DataTable
            dtTable = ds.Tables(1)

            'genrate header template dynamically
            radcmbSearchText.HeaderTemplate = New SimpleSearchTemplate(ListItemType.Header, Nothing, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.Item, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)
            'generate Clientside Template dynamically
            radcmbSearchText.ItemTemplate = New SimpleSearchTemplate(ListItemType.Item, dtTable, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.Item, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)

            hdnSearchType.Value = "3"
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
    Private Sub InitializeOrderClientSideTemplate()
        Try
            'IMPORTANT - DO NOT CHANGE ORDER IN WHICH COLUMNS ARE ADDED BECAUSE IT WILL AFFECT DISPLAY
            radcmbSearchText.DropDownWidth = New Unit(600)

            Dim dtTable As New DataTable
            dtTable.Columns.Add("vcFieldName")
            dtTable.Columns.Add("vcDbColumnName")
            dtTable.Columns.Add("Custom")
            dtTable.Columns.Add("tintOrder")

            Dim drNew As DataRow
            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Created Date"
            drNew("vcDbColumnName") = "bintCreatedDate"
            drNew("Custom") = 0
            drNew("tintOrder") = 0
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Order"
            drNew("vcDbColumnName") = "vcPoppName"
            drNew("Custom") = 0
            drNew("tintOrder") = 1
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Type"
            drNew("vcDbColumnName") = "vcOppType"
            drNew("Custom") = 0
            drNew("tintOrder") = 2
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Organization"
            drNew("vcDbColumnName") = "vcCompanyName"
            drNew("Custom") = 0
            drNew("tintOrder") = 3
            dtTable.Rows.Add(drNew)

            'genrate header template dynamically
            radcmbSearchText.HeaderTemplate = New SimpleSearchTemplate(ListItemType.Header, dtTable, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.Order, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)
            'generate Clientside Template dynamically
            radcmbSearchText.ItemTemplate = New SimpleSearchTemplate(ListItemType.Item, dtTable, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.Order, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)

            hdnSearchType.Value = "4"
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
    Private Sub InitializeBizDocClientSideTemplate()
        Try
            'IMPORTANT - DO NOT CHANGE ORDER IN WHICH COLUMNS ARE ADDED BECAUSE IT WILL AFFECT DISPLAY
            radcmbSearchText.DropDownWidth = New Unit(600)

            Dim dtTable As New DataTable
            dtTable.Columns.Add("vcFieldName")
            dtTable.Columns.Add("vcDbColumnName")
            dtTable.Columns.Add("Custom")
            dtTable.Columns.Add("tintOrder")

            Dim drNew As DataRow
            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Created Date"
            drNew("vcDbColumnName") = "dtCreatedDate"
            drNew("Custom") = 0
            drNew("tintOrder") = 0
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "BizDoc"
            drNew("vcDbColumnName") = "vcBizDocID"
            drNew("Custom") = 0
            drNew("tintOrder") = 1
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Type"
            drNew("vcDbColumnName") = "vcBizDocType"
            drNew("Custom") = 0
            drNew("tintOrder") = 2
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Organization"
            drNew("vcDbColumnName") = "vcCompanyName"
            drNew("Custom") = 0
            drNew("tintOrder") = 3
            dtTable.Rows.Add(drNew)

            'genrate header template dynamically
            radcmbSearchText.HeaderTemplate = New SimpleSearchTemplate(ListItemType.Header, dtTable, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.BizDoc, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)
            'generate Clientside Template dynamically
            radcmbSearchText.ItemTemplate = New SimpleSearchTemplate(ListItemType.Item, dtTable, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.BizDoc, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)

            hdnSearchType.Value = "5"
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
    Private Sub InitializeContactClientSideTemplate()
        Try
            'IMPORTANT - DO NOT CHANGE ORDER IN WHICH COLUMNS ARE ADDED BECAUSE IT WILL AFFECT DISPLAY
            radcmbSearchText.DropDownWidth = New Unit(600)

            Dim dtTable As New DataTable
            dtTable.Columns.Add("vcFieldName")
            dtTable.Columns.Add("vcDbColumnName")
            dtTable.Columns.Add("Custom")
            dtTable.Columns.Add("tintOrder")

            Dim drNew As DataRow
            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "First Name"
            drNew("vcDbColumnName") = "vcFirstName"
            drNew("Custom") = 0
            drNew("tintOrder") = 0
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Last Name"
            drNew("vcDbColumnName") = "vcLastname"
            drNew("Custom") = 0
            drNew("tintOrder") = 1
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Email"
            drNew("vcDbColumnName") = "vcEmail"
            drNew("Custom") = 0
            drNew("tintOrder") = 2
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Organization"
            drNew("vcDbColumnName") = "vcCompanyName"
            drNew("Custom") = 0
            drNew("tintOrder") = 3
            dtTable.Rows.Add(drNew)


            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Phone"
            drNew("vcDbColumnName") = "numPhone"
            drNew("Custom") = 0
            drNew("tintOrder") = 4
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Ext."
            drNew("vcDbColumnName") = "numPhoneExtension"
            drNew("Custom") = 0
            drNew("tintOrder") = 5
            dtTable.Rows.Add(drNew)

            drNew = dtTable.NewRow()
            drNew("vcFieldName") = "Primary Contact"
            drNew("vcDbColumnName") = "bitPrimaryContact"
            drNew("Custom") = 0
            drNew("tintOrder") = 6
            dtTable.Rows.Add(drNew)

            'genrate header template dynamically
            radcmbSearchText.HeaderTemplate = New SimpleSearchTemplate(ListItemType.Header, dtTable, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.Contact, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)
            'generate Clientside Template dynamically
            radcmbSearchText.ItemTemplate = New SimpleSearchTemplate(ListItemType.Item, dtTable, CCommon.GetDocumentPath(Session("DomainID")), TemplateType.Contact, DropDownWidth:=radcmbSearchText.DropDownWidth.Value)

            hdnSearchType.Value = "6"
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
    Private Sub LogException(ByVal ex As Exception, ByVal request As HttpRequest)
        Try
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "SimpleSearchError", "alert('Error occured while searching.');", True)
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), request)
        Catch objEx As Exception

        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Try
            If Not String.IsNullOrEmpty(radcmbSearchText.SelectedValue) Then
                Select Case hdnSearchType.Value
                    Case "1" 'Organizations
                        Dim objLeads As New LeadsIP
                        objLeads.DivisionID = CCommon.ToLong(radcmbSearchText.SelectedValue)
                        objLeads.DomainID = CCommon.ToLong(Session("DomainID"))
                        objLeads.GetCompanyDetails()

                        If objLeads.CRMType = 0 Then
                            Response.Redirect("~/Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=simpleSearch&DivID=" & radcmbSearchText.SelectedValue & "&profileid=" & objLeads.Profile, False)
                        ElseIf objLeads.CRMType = 1 Then
                            Response.Redirect("~/prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=simpleSearch&DivID=" & radcmbSearchText.SelectedValue & "&profileid=" & objLeads.Profile, False)
                        ElseIf objLeads.CRMType = 2 Then
                            Response.Redirect("~/account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=simpleSearch&klds+7kldf=fjk-las&DivId=" & radcmbSearchText.SelectedValue & "&profileid=" & objLeads.Profile, False)
                        End If
                    Case "2" 'Organizations Items
                        If hdnOppType.Value = "1" Then
                            Response.Redirect("~/opportunity/frmOpportunities.aspx?frm=deallist&OpID=" & radcmbSearchText.SelectedValue, False)
                        Else
                            Response.Redirect("~/opportunity/frmReturnDetail.aspx?ReturnID=" & radcmbSearchText.SelectedValue, False)
                        End If
                    Case "3" 'Items
                        Response.Redirect("~/Items/frmKitDetails.aspx?ItemCode=" & radcmbSearchText.SelectedValue, False)
                    Case "4" 'Opp/Orders
                        If hdnOppType.Value = "1" Then
                            Response.Redirect("~/opportunity/frmOpportunities.aspx?frm=deallist&OpID=" & radcmbSearchText.SelectedValue, False)
                        Else
                            Response.Redirect("~/opportunity/frmReturnDetail.aspx?ReturnID=" & radcmbSearchText.SelectedValue, False)
                        End If
                    Case "5" 'BizDocs
                        Dim objOppBizDocs As New OppBizDocs
                        objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOppBizDocs.OppBizDocId = radcmbSearchText.SelectedValue
                        Dim ds As DataSet = objOppBizDocs.GetBizDocsDetails()

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            Dim url As String = Page.ResolveClientUrl("~/opportunity/frmBizInvoice.aspx") & "?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & ds.Tables(0).Rows(0)("numOppId") & "&OppBizId=" & radcmbSearchText.SelectedValue
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "OpenSimpleSearchBizDoc", "OpenSimpleSearchBizDoc('" & url & "');", True)
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "SimpleSearchError", "alert('Bizdoc does not exists.');", True)
                        End If
                    Case "6" 'Contacts
                        Response.Redirect("~/contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=simplesearch&ft6ty=oiuy&CntId=" & radcmbSearchText.SelectedValue, False)
                End Select
            Else
                Dim strScript As String = ""
                If hdnSearchType.Value = "1" Then
                    Session("WhereCondition") = " and vcCompanyName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%'"
                    strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmAdvancedSearchRes.aspx") & "'"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                    Exit Sub
                ElseIf hdnSearchType.Value = "2" Then
                    Session("WhereContditionOpp") = " AND vcCompanyName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%'"
                    strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmItemSearchRes.aspx") & "'"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                    Exit Sub
                ElseIf hdnSearchType.Value = "3" Then
                    Session("WhereConditionItem") = " AND vcItemName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%'"
                    strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmAdvSerInvItemsRes.aspx") & "'"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                    Exit Sub
                ElseIf hdnSearchType.Value = "4" Then
                    If radCmbSimpleSearch.SelectedValue = "1" Then
                        Session("WhereContditionOpp") = " AND vcCompanyName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%'"
                    ElseIf radCmbSimpleSearch.SelectedValue = "2" Then
                        Session("WhereContditionOpp") = " AND oppMas.numOppId IN (SELECT DISTINCT(OppItems.numOppId) FROM Item JOIN OpportunityItems OppItems ON Item.numItemCode = OppItems.numItemCode AND Item.numDomainID = " & Session("DomainID") & " AND (Item.vcItemName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%'  OR OppItems.vcItemName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%'))"
                    Else
                        Session("WhereContditionOpp") = " AND OppMas.vcPOppName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%'"
                    End If

                    strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmItemSearchRes.aspx") & "'"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                    Exit Sub
                ElseIf hdnSearchType.Value = "5" Then
                    If radCmbSimpleSearch.SelectedValue = "1" Then
                        Session("WhereContditionOpp") = " and vcCompanyName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%' and OpportunityBizDocs.vcBizDocID ilike '%%'"
                    ElseIf radCmbSimpleSearch.SelectedValue = "2" Then
                        Session("WhereContditionOpp") = " AND oppMas.numOppId IN (SELECT DISTINCT(OppItems.numOppId) FROM Item JOIN OpportunityItems OppItems ON Item.numItemCode = OppItems.numItemCode AND Item.numDomainID = " & Session("DomainID") & " AND (Item.vcItemName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%'  OR OppItems.vcItemName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%')) and OpportunityBizDocs.vcBizDocID like '%%'"
                    Else
                        Session("WhereContditionOpp") = " and OpportunityBizDocs.vcBizDocId ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%'"
                    End If

                    strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmItemSearchRes.aspx") & "'"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                    Exit Sub
                ElseIf hdnSearchType.Value = "6" Then
                    Session("WhereCondition") = " AND (vcFirstName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%' OR vcLastName ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%' OR vcEmail ilike '%" & Replace(radcmbSearchText.Text, "'", "''") & "%' OR numPhone ILIKE '%" & Replace(radcmbSearchText.Text, "'", "''") & "%')"
                    strScript = "document.location.href='" & Page.ResolveClientUrl("~/admin/frmAdvancedSearchRes.aspx") & "'"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RedirectAdvanceSearch", strScript, True)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
    Protected Sub radcmbSearchText_ItemsRequested(sender As Object, e As RadComboBoxItemsRequestedEventArgs)
        Try
            If e.Text.Trim().Length > 0 Then
                Dim searchCriteria As String
                Dim itemOffset As Integer = e.NumberOfItems


                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))
                Dim ds As DataSet = objCommon.GetSimpleSearchResult(e.Text.Trim(), radCmbSimpleSearch.SelectedValue, radcmbAdditionalFilter.SelectedValue, radCmbOrderTypes.SelectedValue, CCommon.ToInteger(radcmbBizDocTypes.SelectedValue), itemOffset, CCommon.ToInteger(rblOrganizationSearchCriteria.SelectedValue))

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    Dim endOffset As Integer

                    If radCmbSimpleSearch.SelectedValue = "1" AndAlso radcmbAdditionalFilter.SelectedValue = "1" Then
                        endOffset = Math.Min(itemOffset + radcmbSearchText.ItemsPerRequest, ds.Tables(0).Rows.Count)
                        e.EndOfItems = endOffset = ds.Tables(0).Rows.Count
                        e.Message = IIf(ds.Tables(0).Rows.Count <= 0, "No matches", String.Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, ds.Tables(0).Rows.Count))
                    Else
                        endOffset = Math.Min(itemOffset + radcmbSearchText.ItemsPerRequest, ds.Tables(1).Rows(0)(0))
                        e.EndOfItems = endOffset = ds.Tables(1).Rows(0)(0)
                        e.Message = IIf(ds.Tables(0).Rows.Count <= 0, "No matches", String.Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, ds.Tables(1).Rows(0)(0)))
                    End If

                    Dim dtFinal As DataTable
                    If ds.Tables(0).Rows.Count > 0 Then
                        If radCmbSimpleSearch.SelectedValue = "1" AndAlso radcmbAdditionalFilter.SelectedValue = "1" Then
                            dtFinal = ds.Tables(0).AsEnumerable().Skip(itemOffset).Take(radcmbSearchText.ItemsPerRequest).CopyToDataTable()
                        Else
                            dtFinal = ds.Tables(0)
                        End If
                    Else
                        dtFinal = ds.Tables(0)
                    End If

                    Dim textField As String = ""
                    Dim valueField As String = ""

                    Select Case radCmbSimpleSearch.SelectedValue
                        Case "1"
                            If radcmbAdditionalFilter.SelectedValue = "1" Then
                                textField = "vcCompanyName"
                                valueField = "numDivisionID"
                            ElseIf radcmbAdditionalFilter.SelectedValue = "2" Then
                                textField = "vcPOppName"
                                valueField = "numOppId"
                            ElseIf radcmbAdditionalFilter.SelectedValue = "3" Then
                                textField = "vcPOppName"
                                valueField = "numOppId"
                            ElseIf radcmbAdditionalFilter.SelectedValue = "4" Then
                                textField = "vcBizDocID"
                                valueField = "numOppBizDocsId"
                            End If
                        Case "2"
                            If radcmbAdditionalFilter.SelectedValue = "1" Then
                                textField = "vcItemName"
                                valueField = "numItemCode"
                            ElseIf radcmbAdditionalFilter.SelectedValue = "2" Then
                                'This Option is not available
                            ElseIf radcmbAdditionalFilter.SelectedValue = "3" Then
                                textField = "vcPOppName"
                                valueField = "numOppId"
                            ElseIf radcmbAdditionalFilter.SelectedValue = "4" Then
                                textField = "vcBizDocID"
                                valueField = "numOppBizDocsId"
                            End If
                        Case "3"
                            textField = "vcPOppName"
                            valueField = "numOppId"
                        Case "4"
                            textField = "vcBizDocID"
                            valueField = "numOppBizDocsId"
                        Case "5"
                            textField = "vcFullName"
                            valueField = "numContactId"
                    End Select

                    For Each dr As DataRow In dtFinal.Rows
                        Dim item As New RadComboBoxItem()

                        item.Text = DirectCast(dr(textField), String)
                        item.Value = dr(valueField).ToString()

                        If hdnSearchType.Value = "4" Or hdnSearchType.Value = "2" Then
                            item.Attributes.Add("orderType", dr("tintType"))
                        End If

                        item.DataItem = dr
                        radcmbSearchText.Items.Add(item)
                        item.DataBind()
                    Next
                End If
            End If
        Catch ex As Exception
            LogException(ex, Request)
            e.Message = "<b style=""color:Red"">Error occured while searching.</b>"
        End Try
    End Sub
    Protected Sub radcmbAdditionalFilter_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Try
            radcmbSearchText.Text = ""
            ChangeSearchTemplateAndMethod()
            radcmbAdditionalFilter.Focus()
           
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
    Protected Sub radCmbSearch_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        Try
            radcmbSearchText.Text = ""
            radcmbAdditionalFilter.SelectedValue = "1"
            Select Case radCmbSimpleSearch.SelectedValue
                Case "1" 'Organizations
                    radcmbAdditionalFilter.Visible = True
                    radcmbAdditionalFilter.Items.FindItemByText("Items").Visible = True
                    radCmbOrderTypes.Visible = False
                    radcmbBizDocTypes.Visible = False
                Case "2" 'Items
                    radcmbAdditionalFilter.Visible = True
                    radcmbAdditionalFilter.Items.FindItemByText("Items").Visible = False
                    radCmbOrderTypes.Visible = False
                    radcmbBizDocTypes.Visible = False
                Case "3" 'Opps/Orders
                    radcmbAdditionalFilter.Visible = False
                    radCmbOrderTypes.Visible = True
                    radcmbBizDocTypes.Visible = False
                Case "4" 'BizDocs
                    radcmbAdditionalFilter.Visible = False
                    radCmbOrderTypes.Visible = False
                    radcmbBizDocTypes.Visible = True
                Case "5" 'Contacts
                    radcmbAdditionalFilter.Visible = False
                    radCmbOrderTypes.Visible = False
                    radcmbBizDocTypes.Visible = False
            End Select

            ChangeSearchTemplateAndMethod()
            radCmbSimpleSearch.Focus()
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
    Private Sub radCmbOrderTypes_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbOrderTypes.SelectedIndexChanged
        Try
            radCmbOrderTypes.Focus()
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
    Private Sub radcmbBizDocTypes_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbBizDocTypes.SelectedIndexChanged
        Try
            radcmbBizDocTypes.Focus()
        Catch ex As Exception
            LogException(ex, Request)
        End Try
    End Sub
#End Region

#Region "RadComboBox Template"
    Public Class SimpleSearchTemplate
        Implements ITemplate

        Dim ItemTemplateType As ListItemType
        Dim dtTable1 As DataTable
        Dim i As Integer = 0
        Dim _documnetFolderPath As String
        Dim _DropDownWidth As Double
        Dim _templateType As TemplateType

        Sub New(ByVal type As ListItemType, ByVal dtTable As DataTable, ByVal documnetFolderPath As String, ByVal templateType As TemplateType, Optional ByVal DropDownWidth As Double = 600)
            Try
                ItemTemplateType = type
                dtTable1 = dtTable
                _DropDownWidth = DropDownWidth
                _templateType = templateType
                _documnetFolderPath = documnetFolderPath
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
            i = 0
            Dim label1 As Label
            Dim label2 As Label
            Dim img As System.Web.UI.WebControls.Image
            Dim table1 As New HtmlTable
            Dim tblCell As HtmlTableCell
            Dim tblRow As HtmlTableRow
            table1.Width = "100%"
            Dim ul As New HtmlGenericControl("ul")

            Select Case ItemTemplateType
                Case ListItemType.Header
                    If _templateType = TemplateType.Item Then
                        Dim li As New HtmlGenericControl("li")
                        li.Style.Add("text-align", "center")
                        li.Style.Add("font-weight", "bold")
                        li.InnerText = "Item"
                        ul.Controls.Add(li)
                    ElseIf _templateType = TemplateType.Contact Then
                        Dim li As New HtmlGenericControl("li")
                        li.Style.Add("text-align", "center")
                        li.Style.Add("font-weight", "bold")
                        li.InnerText = "Contact"
                        ul.Controls.Add(li)
                    Else
                        For Each dr As DataRow In dtTable1.Rows
                            Dim li As New HtmlGenericControl("li")
                            li.Style.Add("width", Unit.Pixel(CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)).Value & "px")
                            li.Style.Add("float", "left")
                            li.Style.Add("text-align", "center")
                            li.Style.Add("font-weight", "bold")
                            li.InnerText = dr("vcFieldName").ToString
                            ul.Controls.Add(li)
                        Next
                    End If
                    'table1.Rows.Add(tblRow)
                    container.Controls.Add(ul)
                Case ListItemType.Item
                    If _templateType = TemplateType.Item Then
                        i = -1
                        Dim li As New HtmlGenericControl("li")
                        li.Style.Add("width", Unit.Pixel(570).Value & "px")
                        li.Style.Add("float", "left")

                        Dim table As New HtmlGenericControl("table")
                        table.Style.Add("width", "100%")

                        Dim tr1 As New HtmlGenericControl("tr")

                        Dim td1 As New HtmlGenericControl("td")
                        td1.Style.Add("min-width", "80px")
                        img = New System.Web.UI.WebControls.Image
                        img.Style.Add("height", "75px")
                        img.Style.Add("width", "75px")
                        img.ImageUrl = Convert.ToString(_documnetFolderPath)
                        AddHandler img.DataBinding, AddressOf img_DataBinding
                        td1.Controls.Add(img)
                        tr1.Controls.Add(td1)

                        Dim td2 As New HtmlGenericControl("td")
                        Dim tableItem As New HtmlGenericControl("table")
                        tableItem.Style.Add("width", "480px")

                        Dim trItem As New HtmlGenericControl("tr")

                        Dim j As Int16 = 0
                        For Each dr As DataRow In dtTable1.Rows
                            If j = 4 Then
                                tableItem.Controls.Add(trItem)
                                trItem = New HtmlGenericControl("tr")
                                j = 0
                            End If

                            Dim tdlabel As New HtmlGenericControl("td")
                            tdlabel.Style.Add("font-weight", "bold")
                            tdlabel.Style.Add("white-space", "nowrap")

                            Dim labelTitle As New Label
                            labelTitle.Text = Convert.ToString(dr("vcFieldName"))
                            tdlabel.Controls.Add(labelTitle)

                            Dim tdValue As New HtmlGenericControl("td")
                            tdValue.Style.Add("white-space", "pre-wrap")
                            tdValue.Style.Add("width", "50%")

                            If dr("Custom") = "1" Then
                                label2 = New Label
                                label2.CssClass = "normal1"
                                AddHandler label2.DataBinding, AddressOf label2_DataBinding
                                tdValue.Controls.Add(label2)
                            Else
                                label1 = New Label
                                label1.CssClass = "normal1"
                                AddHandler label1.DataBinding, AddressOf label1_DataBinding
                                tdValue.Controls.Add(label1)
                            End If

                            trItem.Controls.Add(tdlabel)
                            trItem.Controls.Add(tdValue)

                            j = j + 2
                        Next

                        tableItem.Controls.Add(trItem)
                        td2.Controls.Add(tableItem)
                        tr1.Controls.Add(td2)
                        table.Controls.Add(tr1)
                        li.Controls.Add(table)

                        ul.Controls.Add(li)
                    ElseIf _templateType = TemplateType.Contact Then
                        Dim li As New HtmlGenericControl("li")
                        li.Style.Add("float", "left")

                        Dim tableContact As New HtmlGenericControl("table")
                        tableContact.Style.Add("width", "100%")

                        'ROW 1
                        Dim tr1 As New HtmlGenericControl("tr")

                        'COLUMN 1
                        Dim tr1td1 As New HtmlGenericControl("td")
                        tr1td1.Style.Add("font-weight", "bold")
                        tr1td1.Style.Add("white-space", "nowrap")
                        tr1td1.InnerText = "First Name"
                        tr1.Controls.Add(tr1td1)

                        'COLUMN 2
                        Dim tr1td2 As New HtmlGenericControl("td")
                        tr1td2.Style.Add("white-space", "pre-wrap")
                        tr1td2.Style.Add("width", "193px")
                        label1 = New Label
                        label1.CssClass = "normal1"
                        AddHandler label1.DataBinding, AddressOf label1_DataBinding
                        tr1td2.Controls.Add(label1)
                        tr1.Controls.Add(tr1td2)

                        'COLUMN 3
                        Dim tr1td3 As New HtmlGenericControl("td")
                        tr1td3.Style.Add("font-weight", "bold")
                        tr1td3.Style.Add("white-space", "nowrap")
                        tr1td3.InnerText = "Last Name"
                        tr1.Controls.Add(tr1td3)

                        'COLUMN 4
                        Dim tr1td4 As New HtmlGenericControl("td")
                        tr1td4.Style.Add("white-space", "pre-wrap")
                        tr1td4.Style.Add("width", "193px")
                        label1 = New Label
                        label1.CssClass = "normal1"
                        AddHandler label1.DataBinding, AddressOf label1_DataBinding
                        tr1td4.Controls.Add(label1)
                        tr1.Controls.Add(tr1td4)

                        tableContact.Controls.Add(tr1)

                        'ROW 2
                        Dim tr2 As New HtmlGenericControl("tr")

                        'COLUMN 1
                        Dim tr2td1 As New HtmlGenericControl("td")
                        tr2td1.Style.Add("font-weight", "bold")
                        tr2td1.Style.Add("white-space", "nowrap")
                        tr2td1.InnerText = "Email"
                        tr2.Controls.Add(tr2td1)

                        'COLUMN 2
                        Dim tr2td2 As New HtmlGenericControl("td")
                        tr2td2.Style.Add("white-space", "pre-wrap")
                        tr2td2.Style.Add("width", "193px")
                        label1 = New Label
                        label1.CssClass = "normal1"
                        AddHandler label1.DataBinding, AddressOf label1_DataBinding
                        tr2td2.Controls.Add(label1)
                        tr2.Controls.Add(tr2td2)

                        'COLUMN 3
                        Dim tr2td3 As New HtmlGenericControl("td")
                        tr2td3.Style.Add("font-weight", "bold")
                        tr2td3.Style.Add("white-space", "nowrap")
                        tr2td3.InnerText = "Organization"
                        tr2.Controls.Add(tr2td3)

                        'COLUMN 4
                        Dim tr2td4 As New HtmlGenericControl("td")
                        tr2td4.Style.Add("white-space", "pre-wrap")
                        tr2td4.Style.Add("width", "193px")
                        label1 = New Label
                        label1.CssClass = "normal1"
                        AddHandler label1.DataBinding, AddressOf label1_DataBinding
                        tr2td4.Controls.Add(label1)
                        tr2.Controls.Add(tr2td4)

                        tableContact.Controls.Add(tr2)

                        'ROW 3
                        Dim tr3 As New HtmlGenericControl("tr")

                        'COLUMN 1
                        Dim tr3td1 As New HtmlGenericControl("td")
                        tr3td1.Style.Add("font-weight", "bold")
                        tr3td1.Style.Add("white-space", "nowrap")
                        tr3td1.InnerText = "Phone"
                        tr3.Controls.Add(tr3td1)

                        'COLUMN 2
                        Dim tr3td2 As New HtmlGenericControl("td")
                        tr3td2.Style.Add("white-space", "pre-wrap")
                        tr3td2.Style.Add("width", "193px")
                        label1 = New Label
                        label1.CssClass = "normal1"
                        AddHandler label1.DataBinding, AddressOf label1_DataBinding
                        tr3td2.Controls.Add(label1)
                        tr3.Controls.Add(tr3td2)

                        'COLUMN 3
                        Dim tr3td3 As New HtmlGenericControl("td")
                        tr3td3.Style.Add("font-weight", "bold")
                        tr3td3.Style.Add("white-space", "nowrap")
                        tr3td3.InnerText = "Ext"
                        tr3.Controls.Add(tr3td3)

                        'COLUMN 4
                        Dim tr3td4 As New HtmlGenericControl("td")
                        tr3td4.Style.Add("white-space", "pre-wrap")
                        tr3td4.Style.Add("width", "193px")
                        label1 = New Label
                        label1.CssClass = "normal1"
                        AddHandler label1.DataBinding, AddressOf label1_DataBinding
                        tr3td4.Controls.Add(label1)
                        tr3.Controls.Add(tr3td4)

                        tableContact.Controls.Add(tr3)

                        'ROW 4
                        Dim tr4 As New HtmlGenericControl("tr")

                        'COLUMN 1
                        Dim tr5td1 As New HtmlGenericControl("td")
                        tr5td1.Style.Add("font-weight", "bold")
                        tr5td1.Style.Add("white-space", "nowrap")
                        tr5td1.InnerText = "Primary Contact"
                        tr4.Controls.Add(tr5td1)

                        'COLUMN 2
                        Dim tr5td2 As New HtmlGenericControl("td")
                        tr5td2.Attributes.Add("colspan", "3")
                        tr5td2.Style.Add("white-space", "pre-wrap")
                        label1 = New Label
                        label1.CssClass = "normal1"
                        AddHandler label1.DataBinding, AddressOf label1_DataBinding
                        tr5td2.Controls.Add(label1)
                        tr4.Controls.Add(tr5td2)

                        tableContact.Controls.Add(tr4)

                        li.Controls.Add(tableContact)
                        ul.Controls.Add(li)
                    Else
                        For Each dr As DataRow In dtTable1.Rows
                            Dim li As New HtmlGenericControl("li")
                            Dim width As Integer = CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)
                            li.Style.Add("width", Unit.Pixel(width).Value & "px")
                            li.Style.Add("float", "left")

                            If dr("Custom") = "1" Then
                                label2 = New Label
                                label2.CssClass = "normal1"
                                label2.Attributes.Add("style", "display:inline-block;width:" & width)
                                AddHandler label2.DataBinding, AddressOf label2_DataBinding
                                li.Controls.Add(label2)
                            Else
                                label1 = New Label
                                label1.CssClass = "normal1"
                                label1.Attributes.Add("style", "display:inline-block;width:" & width)
                                AddHandler label1.DataBinding, AddressOf label1_DataBinding
                                li.Controls.Add(label1)
                            End If

                            ul.Controls.Add(li)
                        Next
                    End If
                    container.Controls.Add(ul)
            End Select

        End Sub

        Private Sub label1_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
            Dim target As Label = CType(sender, Label)
            Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
            Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName").ToString()))
            'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcDbColumnName").ToString()), String)
            target.Text = itemText
            i = i + 1
        End Sub

        Private Sub label2_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
            Dim target As Label = CType(sender, Label)
            Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
            Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcFieldName").ToString()))
            'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcFieldName").ToString()), String)
            target.Text = itemText
            i = i + 1
        End Sub

        Private Sub img_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
            Dim img As System.Web.UI.WebControls.Image = CType(sender, System.Web.UI.WebControls.Image)
            Dim item As RadComboBoxItem = CType(img.NamingContainer, RadComboBoxItem)
            Dim imagePath As String = Convert.ToString(DirectCast(item.DataItem, DataRow)("vcPathForTImage"))
            'Dim imagePath As String = CType(DataBinder.Eval(item.DataItem, "vcPathForTImage"), String)
            If Not String.IsNullOrEmpty(imagePath) Then
                img.ImageUrl = img.ImageUrl + imagePath
            Else
                img.ImageUrl = "../images/icons/cart_large.png"
            End If

            i = i + 1
        End Sub
    End Class
#End Region

    
End Class