﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;

namespace MarketplaceService
{
    [RunInstaller(true)]
    public partial class Installer : System.Configuration.Install.Installer
    {
        private ServiceProcessInstaller process;
        private System.ServiceProcess.ServiceInstaller service;
        public Installer()
        {
            InitializeComponent();
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new System.ServiceProcess.ServiceInstaller();
            service.ServiceName = "OnlineMarketplaceIntegratorService";
            Installers.Add(process);
            Installers.Add(service);
        }
    }
}
