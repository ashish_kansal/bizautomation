﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Data;
using System.ServiceModel;

using BACRM.BusinessLogic;
using BACRMBUSSLOGIC.BussinessLogic;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Prospects;
using BACRM.BusinessLogic.Contract;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.WebAPI;
using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.Opportunities;

using CookComputing.XmlRpc;
using Ez.Newsletter.MagentoApi;

using MarketplaceService.Magento_Communicator;

namespace MarketplaceService
{
    class MagentoClient
    {
        #region Members

        private string OrderFilePath = ConfigurationManager.AppSettings["MagentoOrderFilePath"];
        private string ClosedOrders = ConfigurationManager.AppSettings["MagentoClosedOrders"];
        private string ProductImagePath = ConfigurationManager.AppSettings["MagentoProductImagePath"];

        DataSet ds = new DataSet();
        DataSet dsItems = new DataSet();
        DataTable dtItems = new DataTable();

        DataSet dsOrderItems = new DataSet();
        DataTable dtOrderItems = new DataTable();

        DataTable dtCompanyTaxTypes = new DataTable();
        DataTable dtItemSpecifics = new DataTable();
        DataRow drtax = null;

        long lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId;
        OppBizDocs objOppBizDocs = new OppBizDocs();
        string strdetails = "";
        string[] arrOutPut;
        string[] arrBillingIDs;

        #endregion Members

        #region Product Attribute Set

        /// <summary>
        /// Get Merchants specific Magento Attribute Set collection
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="API_URL">API_URL</param>
        /// <param name="API_User">API_User</param>
        /// <param name="API_Key">API_Key</param>
        public void GetProductAttributeSet(long DomainId, int WebApiId, string API_URL, string API_User, string API_Key)
        {
            string LogMessage = "", ErrorMessage = "";
            CCommon objCommon = new CCommon();
            string AttributeSets = "";
            Dictionary<string, string> AttributeSetColl = new Dictionary<string, string>();

            try
            {
                MagentoCommunicatorClient objClient = new MagentoCommunicatorClient();
                AttributeSetColl = objClient.GetProductAttributeSet(API_URL, API_User, API_Key);
                objClient.Close();

                foreach (KeyValuePair<string, string> KeyValue in AttributeSetColl)
                {
                    AttributeSets += KeyValue.Key + "|" + KeyValue.Value + ", ";
                }
                objCommon.DomainID = DomainId;
                objCommon.Mode = 25;
                objCommon.UpdateRecordID = WebApiId;
                objCommon.Comments = AttributeSets;
                objCommon.UpdateSingleFieldValue();
                LogMessage = "Magento Product Attribute Sets " + AttributeSets + " are Updated";
                GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Magento Service : " + er.msg;
                throw new Exception(ErrMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion Product Attribute Set

        #region New Product

        /// <summary>
        /// Adds/ Updates new Item to Magento Online Marketplace
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="dtNewItems">New/Modified Items list</param>
        /// <param name="API_URL">Merchant Site URL</param>
        /// <param name="API_User">API User</param>
        /// <param name="API_Key">API Key</param>
        public void AddNewProduct(long DomainId, int WebApiId, DataTable dtNewItems, string API_URL, string API_User, string API_Key)
        {
            string StoreId = "1";
            string ProductType = "";
            string Quantity = "0";
            string ErrorMessage = "", LogMessage = "";
            WebAPI objWebAPI = new WebAPI();

            foreach (DataRow dr in dtNewItems.Rows)
            {
                try
                {
                    if (CCommon.ToString(dr["vcItemName"]).Length > 0 & CCommon.ToString(dr["numItemCode"]).Length > 0)
                    {
                        Product objProduct = new Product();
                        string ListPrice = CCommon.ToString(dr["monListPrice"]);

                        if (CCommon.ToString(dr["vcSKU"]).Length > 0 & CCommon.ToString(dr["vcItemName"]).Length > 0)
                        {
                            // string Weight = "", Length = "", Width = "", Height = "";
                            WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                            string WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";
                            if (File.Exists(WebApi_ItemDetails))
                            {
                                StreamReader objStreamReader = new StreamReader(WebApi_ItemDetails);
                                XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                objStreamReader.Close();
                            }
                            Quantity = CCommon.ToString(dr["QtyOnHand"]);
                            objProduct.name = CCommon.ToString(dr["vcItemName"]);
                            objProduct.description = CCommon.ToString(dr["vcExtendedDescToAPI"]);
                            objProduct.short_description = CCommon.ToString(dr["txtItemDesc"]);
                            objProduct.sku = CCommon.ToString(dr["vcSKU"]);
                            objProduct.model = CCommon.ToString(dr["vcModelID"]);
                            objProduct.price = ListPrice;
                            objProduct.manufacturer = CCommon.ToString(dr["vcManufacturer"]);

                            if (CCommon.ToInteger(dr["IsArchieve"]) == 0)
                                objProduct.status = "1";
                            else
                                objProduct.status = "2";

                            objProduct.visibility = "4";

                            if (objWebAPIItemDetail.MagentoProductAttributeSet > 0)
                                objProduct.set = CCommon.ToString(objWebAPIItemDetail.MagentoProductAttributeSet);
                            else
                            {
                                ErrorMessage = "Magento Product Attribute Set Is Not assigned for Item SKU : " + objProduct.sku + " ,Name : " + objProduct.name;
                                throw new Exception(ErrorMessage);
                            }

                            if (!string.IsNullOrEmpty(objWebAPIItemDetail.MagentoProductType))
                                ProductType = objWebAPIItemDetail.MagentoProductType;
                            else
                            {
                                ErrorMessage = "Magento Product Type Is Not assigned for Item SKU : " + objProduct.sku + " ,Name : " + objProduct.name;
                                throw new Exception(ErrorMessage);
                            }

                            if (!string.IsNullOrEmpty(objWebAPIItemDetail.Weight))
                                objProduct.weight = objWebAPIItemDetail.Weight;

                            List<byte[]> lstItemImages = new List<byte[]>();
                            if (!string.IsNullOrEmpty(CCommon.ToString(dr["xmlItemImages"])))
                            {
                                XmlDocument xmlDocItemImages = new XmlDocument();
                                xmlDocItemImages.LoadXml(CCommon.ToString(dr["xmlItemImages"]));
                                XmlNodeList ItemImages = xmlDocItemImages.SelectNodes("Images/ItemImages");
                                string ImagePath = "";
                                foreach (XmlNode Image in ItemImages)
                                {
                                    ImagePath = ProductImagePath + DomainId + "\\" + CCommon.ToString(Image.Attributes["vcPathForImage"].Value);
                                    if (File.Exists(ImagePath))
                                    {
                                        byte[] imageFileBytes = File.ReadAllBytes(ImagePath);
                                        lstItemImages.Add(imageFileBytes);
                                    }
                                }
                            }
                            MagentoCommunicatorClient objClient = new MagentoCommunicatorClient();
                            MemoryStream msProduct = GeneralFunctions.SerializeToStream(objProduct);
                            string API_ItemId = "";
                            bool wasProductUpdated = false;
                            if (CCommon.ToString(dr["vcAPIItemId"]).Length <= 1)
                            {
                                API_ItemId = objClient.AddNewProduct(API_URL, API_User, API_Key, msProduct, Quantity, StoreId, ProductType, lstItemImages);

                                LogMessage = "Item Details for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item SKU : " + objProduct.sku + " ,Name : " + objProduct.name + " is Added in Magento Product Shopping Listings";
                                GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                                objWebAPI.UserCntID = BizCommonFunctions.GetRoutingRulesUserCntID(DomainId, "", "");
                                objWebAPI.ItemCode = CCommon.ToLong(dr["numItemCode"]);
                                objWebAPI.Product_Id = API_ItemId;
                                objWebAPI.DomainID = DomainId;
                                objWebAPI.WebApiId = WebApiId;
                                objWebAPI.AddItemAPILinking();
                                LogMessage = "Magento product Id " + API_ItemId + " is mapped in (BizDatabase) ItemAPI Details for  Item Code : " + CCommon.ToString(dr["numItemCode"]) + " ,Name : " + objProduct.name;
                                GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                            }
                            else
                            {
                                wasProductUpdated = objClient.UpdateProduct(API_URL, API_User, API_Key, CCommon.ToString(dr["vcAPIItemId"]), msProduct, Quantity, lstItemImages);
                                if (wasProductUpdated)
                                {
                                    LogMessage = "Item Details for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item SKU : " + objProduct.sku + " ,Name : " + objProduct.name + " is Updated in Magento Product Listings";
                                }
                                else
                                {
                                    LogMessage = "Sorry!., Error Updating Item Details for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item SKU : " + objProduct.sku + " ,Name : " + objProduct.name + " in Magento Product Listings";
                                }
                                GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                            }

                            objClient.Close();
                        }
                    }
                }
                catch (FaultException<error> ex)
                {
                    error er = ex.Detail;
                    string ErrMessage = "Error received from Magento Service : " + er.msg;
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                    //throw new Exception(ErrMessage);
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                }
            }
        }

        #endregion New Product

        #region Import Item Details list
        /// <summary>
        /// Imports (Adds/Updates) Merchant's Magento Product Listing into BizAutomation
        /// </summary>
        /// <param name="DomainId">Domain Id</param>
        /// <param name="WebApiId">WebApi Id</param>
        /// <param name="RecordOwner">Record Owner</param>
        /// <param name="UserContactID">User Contact ID</param>
        /// <param name="WareHouseID">WareHouse ID</param>
        /// <param name="API_URL">API URL</param>
        /// <param name="API_User">API User</param>
        /// <param name="API_Key">API Key</param>
        public void ImportProductListing(long DomainId, int WebApiId, long RecordOwner, long UserContactID, int WareHouseID, string API_URL, string API_User, string API_Key, string ListingReqType = "1")
        {
            string LogMessage = "", ErrorMessage = "";
            DataTable dtProductDetails = new DataTable();
            CCommon objCommon = new CCommon();
            List<Product> lstProducts = new List<Product>();
            try
            {
                MagentoCommunicatorClient objClient = new MagentoCommunicatorClient();
                lstProducts = objClient.GetProductListings(API_URL, API_User, API_Key, "");
                objClient.Close();

                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = DomainId;
                DataTable dtDomainDetails = objUserAccess.GetDomainDetails();
                int DefaultIncomeAccID = 0, DefaultCOGSAccID = 0, DefaultAssetAccID = 0;

                if (dtDomainDetails.Rows.Count > 0)
                {
                    DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numIncomeAccID"]);
                    DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numCOGSAccID"]);
                    DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numAssetAccID"]);
                    if (DefaultIncomeAccID == 0 || DefaultCOGSAccID == 0 || DefaultAssetAccID == 0)
                    {
                        string ExcepitonMessage = "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                        throw new Exception(ExcepitonMessage);
                    }
                }
                BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                foreach (Product objProduct in lstProducts)
                {
                    try
                    {
                        DataRow dr = dtProductDetails.NewRow();
                        if (ListingReqType == "2")
                        {
                            dr = FillMagentoItemDetails(DomainId, WebApiId, RecordOwner, DefaultIncomeAccID, DefaultCOGSAccID, DefaultAssetAccID, dr, objProduct);
                            dtProductDetails.Rows.Add(dr);
                        }
                        else if (ListingReqType == "1" & objProduct.status == "1")
                        {
                            dr = FillMagentoItemDetails(DomainId, WebApiId, RecordOwner, DefaultIncomeAccID, DefaultCOGSAccID, DefaultAssetAccID, dr, objProduct);
                            dtProductDetails.Rows.Add(dr);
                        }
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.StackTrace);
                    }
                }

                DataTable dtItemDetails = new DataTable();
                DataRow drMailItemDetail;
                BizCommonFunctions.CreateMailMessageItemDetail(dtItemDetails);
                int ItemCount = 0;

                string ItemClassificationName = "Magento_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                long ItemClassificationID = 0;
                ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);

                foreach (DataRow drItem in dtProductDetails.Rows)
                {
                    try
                    {
                        long ItemCode = BizCommonFunctions.AddNewItemToBiz(drItem, DomainId, WebApiId, RecordOwner, WareHouseID, ItemClassificationID);
                        ItemCount += 1;
                        drMailItemDetail = dtItemDetails.NewRow();
                        drMailItemDetail["SNo"] = CCommon.ToString(ItemCount);
                        drMailItemDetail["BizItemCode"] = CCommon.ToString(ItemCode);
                        drMailItemDetail["ItemName"] = drItem["vcItemName"];
                        drMailItemDetail["SKU"] = drItem["vcSKU"];
                        dtItemDetails.Rows.Add(drMailItemDetail);

                        LogMessage = "Added/Updated New Item in BizAutomation, Item Code : " + ItemCode + ", imported from Magento Online Marketplace ";
                        GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                        WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                        string FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml";
                        if (System.IO.File.Exists(FilePath))
                        {
                            System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                            XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                            objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                            objStreamReader.Close();
                        }

                        objWebAPIItemDetail.MagentoProductAttributeSet = CCommon.ToInteger(drItem["MagentoAttributeSetID"]);
                        objWebAPIItemDetail.MagentoProductType = CCommon.ToString(drItem["MagentoProductType"]);
                        // objWebAPIItemDetail.MsrPrice = CCommon.ToString(drItem["MSRPrice"]);

                        StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml");
                        XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                        Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                        objStreamWriter.Close();
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.StackTrace);
                    }
                }
                BizCommonFunctions.SendMailNewItemList(DomainId, WebApiId, RecordOwner, UserContactID, ItemCount, dtItemDetails, ItemClassificationName, "Magento");

            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Magento Service : " + er.msg;
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                throw new Exception(ErrMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        /// <summary>
        /// Fills(maps) MAgento Item Detail to BizAutomation Item Detail
        /// </summary>
        /// <param name="DomainId">Domain Id</param>
        /// <param name="WebApiId">WebApi Id</param>
        /// <param name="RecordOwner">Record Owner</param>
        /// <param name="DefaultIncomeAccID">Default Income Account ID</param>
        /// <param name="DefaultCOGSAccID">Default COGS Account ID</param>
        /// <param name="DefaultAssetAccID">Default Asset account ID</param>
        /// <param name="dr">BizAutomation Item Structire</param>
        /// <param name="objMagentoProduct">Magento Item Detail</param>
        /// <returns></returns>
        public DataRow FillMagentoItemDetails(long DomainId, int WebApiId, long RecordOwner, int DefaultIncomeAccID, int DefaultCOGSAccID, int DefaultAssetAccID, DataRow dr, Product objMagentoProduct)
        {
            try
            {
                double MSRPrice = 0;
                double SalePrice = 0;
                //if (!string.IsNullOrEmpty(objMagentoProduct.special_to_date))
                //{
                //    DateTime special_to_date = Convert.ToDateTime(objMagentoProduct.special_to_date);
                //    if (CCommon.ToDecimal(objMagentoProduct.special_price) > 0 & special_to_date > DateTime.Now)
                //    {
                //        SalePrice = CCommon.ToDouble(objMagentoProduct.special_price);
                //        MSRPrice = CCommon.ToDouble(objMagentoProduct.price);
                //    }
                //}
                //else
                //{
                //    SalePrice = CCommon.ToDouble(objMagentoProduct.price);
                //    MSRPrice = CCommon.ToDouble(objMagentoProduct.price);
                //}
                dr["numItemCode"] = 0;
                dr["vcItemName"] = objMagentoProduct.name;
                dr["txtItemDesc"] = objMagentoProduct.short_description;
                dr["charItemType"] = "P";
                dr["monListPrice"] = CCommon.ToDouble(objMagentoProduct.price);
                dr["txtItemHtmlDesc"] = objMagentoProduct.description; ;
                dr["bitTaxable"] = 0;
                dr["vcSKU"] = objMagentoProduct.sku;
                dr["numQuantity"] = CCommon.ToInteger(CCommon.ToDecimal(objMagentoProduct.Quantity));
                dr["bitKitParent"] = 0;
                dr["numDomainID"] = DomainId;
                dr["numUserCntID"] = RecordOwner;
                dr["numVendorID"] = 0;
                dr["bitSerialized"] = 0;
                dr["strFieldList"] = 0;
                if (objMagentoProduct.status == "1")
                    dr["IsArchieve"] = 0;
                else
                    dr["IsArchieve"] = 1;

                dr["vcModelID"] = objMagentoProduct.model;
                dr["numItemGroup"] = 0;
                dr["numCOGSChartAcntId"] = DefaultCOGSAccID;
                dr["numAssetChartAcntId"] = DefaultAssetAccID;
                dr["numIncomeChartAcntId"] = DefaultIncomeAccID;
                dr["monAverageCost"] = 0;
                dr["monTotAmtBefDiscount"] = 0;
                dr["monLabourCost"] = 0;
                dr["fltWeight"] = objMagentoProduct.weight;
                dr["fltHeight"] = objMagentoProduct.dimension;
                dr["fltLength"] = objMagentoProduct.dimension;
                dr["fltWidth"] = objMagentoProduct.dimension;
                dr["fltShippingWeight"] = objMagentoProduct.weight;
                dr["fltShippingHeight"] = "";
                dr["fltShippingLength"] = "";
                dr["fltShippingWidth"] = "";
                dr["DimensionUOM"] = "";
                dr["WeightUOM"] = "";
                dr["intWebApiId"] = WebApiId;
                dr["vcApiItemId"] = objMagentoProduct.product_id;
                dr["numBarCodeId"] = 0;
                dr["vcManufacturer"] = objMagentoProduct.manufacturer;
                dr["vcExportToAPI"] = WebApiId;
                dr["Warranty"] = "";
                dr["numEBayCategoryID"] = "";
                dr["EbayListingType"] = "";
                dr["EBayListingDuration"] = "";
                dr["EbayStandardShippingCost"] = "";
                dr["EbayExpressShippingCost"] = 0;
                dr["MagentoAttributeSetID"] = objMagentoProduct.set;
                dr["MagentoProductType"] = objMagentoProduct.type;
                dr["MSRPrice"] = MSRPrice;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dr;
        }

        /// <summary>
        /// Gets Item Detials from Magento Marketplace for given Magento Item ID
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="RecordOwner">RecordOwner</param>
        /// <param name="ProductId">Product Id</param>
        /// <returns>Magento Item Detail</returns>
        public Product GetProductDetailForProductId(long DomainId, int WebApiId, long RecordOwner, string ProductId)
        {
            WebAPI objWebAPi = new WebAPI();
            string LogMessage = "", ErrorMessage = "";
            CCommon objCommon = new CCommon();
            List<Product> lstProducts = new List<Product>();
            Product objProduct = new Product();
            try
            {
                objWebAPi.DomainID = DomainId;
                objWebAPi.WebApiId = WebApiId;
                objWebAPi.Mode = 3;
                DataTable dtWebAPI = objWebAPi.GetWebApi();
                foreach (DataRow drWebApi in dtWebAPI.Rows)
                {
                    if (CCommon.ToBool(drWebApi["bitEnableAPI"]) == true)
                    {
                        if (WebApiId == CCommon.ToInteger(drWebApi["WebApiId"]))//Amazon US
                        {
                            string API_URL = CCommon.ToString(drWebApi["vcFirstFldValue"]);
                            string API_User = CCommon.ToString(drWebApi["vcSecondFldValue"]);
                            string API_Key = CCommon.ToString(drWebApi["vcThirdFldValue"]);

                            MagentoCommunicatorClient objClient = new MagentoCommunicatorClient();
                            lstProducts = objClient.GetProductDetailForItemID(API_URL, API_User, API_Key, ProductId);
                            objClient.Close();
                            foreach (Product objProducts in lstProducts)
                            {
                                objProduct = objProducts;
                            }
                        }
                    }
                }
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Magento Service : " + er.msg;
                throw new Exception(ErrMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return objProduct;
        }

        #endregion Import Item Details list

        #region Inventory Management

        /// <summary>
        /// Updates Item Inventory to Magento Marketplace
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="dtNewItems">inventory Modified Item list</param>
        /// <param name="API_URL">Merchant Site URL</param>
        /// <param name="API_User">API User</param>
        /// <param name="API_Key">API Key</param>
        public void UpdateInventory(long DomainId, int WebApiId, DataTable dtNewItems, string API_URL, string API_User, string API_Key)
        {
            string ProductSKU = "";
            string Quantity = "0";
            string LogMessage = "";
            bool wasInventoryUpdated = false;

            foreach (DataRow dr in dtNewItems.Rows)
            {
                try
                {
                    if (CCommon.ToString(dr["vcItemName"]).Length > 0 & CCommon.ToString(dr["numItemCode"]).Length > 0)
                    {
                        string ListPrice = CCommon.ToString(dr["monListPrice"]);
                        TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);

                        if (CCommon.ToString(dr["vcSKU"]).Length > 0 & CCommon.ToString(dr["vcItemName"]).Length > 0)
                        {
                            Quantity = CCommon.ToString(dr["QtyOnHand"]);
                            ProductSKU = CCommon.ToString(dr["vcSKU"]);
                            try
                            {
                                MagentoCommunicatorClient objClient = new MagentoCommunicatorClient();
                                wasInventoryUpdated = objClient.UpdateInventory(API_URL, API_User, API_Key, ProductSKU, Quantity);
                                objClient.Close();
                                if (wasInventoryUpdated)
                                    LogMessage = "Magento Product Inventory Quantity : " + Quantity + " is Updated for item SKU: " + ProductSKU;
                                else
                                    LogMessage = "Error Updating Magento Product Inventory Quantity : " + Quantity + " for item SKU: " + ProductSKU;
                                GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                            }
                            catch (FaultException<error> ex)
                            {
                                error er = ex.Detail;
                                string ErrMessage = "Error received from Magento Service : " + er.msg;
                                //throw new Exception(ErrMessage);
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                }
            }
        }

        #endregion Inventory Management

        #region Order Processing

        /// <summary>
        /// Gets list of New Order IDs from Magento Marketplace
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="API_URL">Merchant's Site URL</param>
        /// <param name="API_User">API User</param>
        /// <param name="API_Key">API Key</param>
        /// <param name="CreatedAt"></param>
        public void GetOrderIDList(long DomainId, int WebApiId, string API_URL, string API_User, string API_Key, DateTime CreatedAt)
        {
            List<string> OrderIDs = new List<string>();
            WebAPI objWebAPI = new WebAPI();
            string LogMessage = "", ErrorMessage = "";
            try
            {
                MagentoCommunicatorClient objClient = new MagentoCommunicatorClient();
                OrderIDs = objClient.GetOrderIds(API_URL, API_User, API_Key, CreatedAt);
                objClient.Close();
                if (OrderIDs.Count > 0)
                {


                    string strOrderIds = "";
                    foreach (string OrderId in OrderIDs)
                    {
                        try
                        {
                            strOrderIds += OrderId + ", ";
                            objWebAPI.DomainID = DomainId;
                            objWebAPI.WebApiId = WebApiId;
                            objWebAPI.WebApiOrdDetailId = 0;
                            objWebAPI.API_OrderReportId = OrderId;
                            objWebAPI.WebApiReportTypeId = 1;
                            objWebAPI.API_ReportStatus = 0;
                            objWebAPI.RStatus = 0;
                            objWebAPI.ManageWebAPIOrderReports();

                        }
                        catch (Exception ex)
                        {
                            ErrorMessage = "Error occoured while adding Magento Order Id : " + OrderId + " in BizAutomation." + " - " + ex.Message;
                            //throw new Exception(ErrorMessage);
                            GeneralFunctions.WriteMessage(DomainId, "ERR", ErrorMessage);
                        }
                    }
                    LogMessage = "Magento Order Ids (" + strOrderIds + ") is added in BizAutomation.";
                    GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                }
                objWebAPI.UpdateAPISettingsDate(1);
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Magento Service : " + er.msg;
                throw new Exception(ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.msg);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {
                throw ex;
                // GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
            }
        }

        public void GetOrderIDListBetweenDates(long DomainId, int WebApiId, string API_URL, string API_User, string API_Key, DateTime StartDate, DateTime EndDate)
        {
            List<string> OrderIDs = new List<string>();
            WebAPI objWebAPI = new WebAPI();
            string LogMessage = "", ErrorMessage = "";
            try
            {
                MagentoCommunicatorClient objClient = new MagentoCommunicatorClient();
                OrderIDs = objClient.GetOrderIdBetweenDates(API_URL, API_User, API_Key, StartDate, EndDate);
                objClient.Close();
                if (OrderIDs.Count > 0)
                {
                    string strOrderIds = "";
                    foreach (string OrderId in OrderIDs)
                    {
                        try
                        {
                            strOrderIds += OrderId + ", ";
                            objWebAPI.DomainID = DomainId;
                            objWebAPI.WebApiId = WebApiId;
                            objWebAPI.WebApiOrdDetailId = 0;
                            objWebAPI.API_OrderReportId = OrderId;
                            objWebAPI.WebApiReportTypeId = 1;
                            objWebAPI.API_ReportStatus = 0;
                            objWebAPI.RStatus = 0;
                            objWebAPI.ManageWebAPIOrderReports();
                        }
                        catch (Exception ex)
                        {
                            ErrorMessage = "Error occoured while adding Magento Order Id : " + OrderId + " in BizAutomation." + " - " + ex.Message;
                            //throw new Exception(ErrorMessage);
                            GeneralFunctions.WriteMessage(DomainId, "ERR", ErrorMessage);
                        }
                    }
                    LogMessage = "Magento Order Ids (" + strOrderIds + ") is added in BizAutomation.";
                    GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                }
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Magento Service : " + er.msg;
                throw new Exception(ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.msg);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {
                throw ex;
                // GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
            }
        }

        /// <summary>
        /// Gets the Order Report for the given OrderID
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="API_URL">Merchant's Site URL</param>
        /// <param name="API_User">API User</param>
        /// <param name="API_Key">API Key</param>
        /// <param name="OrderId">Order Id</param>
        public void GetOrderReport(long DomainId, int WebApiId, string API_URL, string API_User, string API_Key, string OrderId)
        {
            try
            {
                // string ReportType = "OrderReport";
                MagentoCommunicatorClient objClient = new MagentoCommunicatorClient();
                MemoryStream ms = new MemoryStream();
                ms = objClient.GetOrderReportFromID(API_URL, API_User, API_Key, OrderId);
                objClient.Close();

                OrderInfo orderDetail; // = new OrderInfo();
                XmlSerializer serializer = new XmlSerializer(typeof(OrderInfo));
                XmlReader Reader = new XmlTextReader(ms);
                orderDetail = (OrderInfo)serializer.Deserialize(Reader);
                if (orderDetail != null)
                    GeneralFunctions.SerializeObjectToFile(orderDetail, GeneralFunctions.GetPath(OrderFilePath, DomainId) + OrderId + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Magento Service : " + er.msg;
                GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
                throw new Exception(ErrMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        /// <summary>
        /// Process Magento Orders and Creates Sales Order in BizDatabase
        /// Adds new Item into BizDatabase if the Order Item is Not available in BizDatabase
        /// Makes Deposit Entry
        /// Makes General Journal Header and Journal Detail entry
        /// </summary>
        /// <param name="Order"></param>
        /// <param name="DomainID"></param>
        /// <param name="WebApiId"></param>
        /// <param name="Source"></param>
        /// <param name="numWareHouseID"></param>
        /// <param name="numRecordOwner"></param>
        /// <param name="numAssignTo"></param>
        /// <param name="numRelationshipId"></param>
        /// <param name="numProfileId"></param>
        /// <param name="numBizDocId"></param>
        /// <param name="numOrderStatus"></param>
        /// <param name="ExpenseAccountId"></param>
        public void ProcessMagentoOrders(OrderInfo Order, long DomainID, int WebApiId, string Source,
          int numWareHouseID, long numRecordOwner, long numAssignTo, long numRelationshipId, long numProfileId, long numBizDocId, long BizDocStatusId,
            int numOrderStatus, int ExpenseAccountId, long DiscountItemMapping, long ShippingServiceItemID, long SalesTaxItemMappingID, string ShipToPhoneNo)
        {
            decimal SalesTaxRate = 0;
            CContacts objContacts = null;
            CLeads objLeads = null;
            ImportWizard objImpWzd = new ImportWizard();
            MOpportunity objOpportunity = new MOpportunity();
            CCommon objCommon = new CCommon();

            string LogMessage = "", ErrorMsg = "";
            decimal TransactionFee = 0;
            string OrderId = Order.increment_id;
            //OrderSummary OrderDetails = new OrderSummary();

            WebAPI objWebApi = new WebAPI();

            string ItemMessage = "";
            decimal ShipCost = 0;
            decimal OrderTotal = 0;
            long CurrencyID = 0;
            decimal dTotDiscount = 0;
            decimal SalesTaxAmount = 0;
            string CompanyName = "";

            string FirstName = "", LastName = "";

            if (!string.IsNullOrEmpty(OrderId))
            {
                objWebApi.DomainID = DomainID;
                objWebApi.WebApiId = WebApiId;
                objWebApi.vcAPIOppId = OrderId;
                if (objWebApi.CheckDuplicateAPIOpportunity())
                {

                    objContacts = new CContacts();
                    objLeads = new CLeads();
                    objLeads.DomainID = DomainID;

                    //Address ShippingAddress = new Address();
                    //ShippingAddress = OrderDetails.buyershippingaddress;
                    string Email = Order.customer_email;


                    if (!string.IsNullOrEmpty(Email))
                    {
                        objLeads.Email = Email;
                        //Check for Details for the Given DomainID and EmailID
                        objLeads.GetConIDCompIDDivIDFromEmail();
                    }
                    else
                    {
                        objLeads.Email = "";
                    }
                    if (objLeads.ContactID > 0 && objLeads.DivisionID > 0)
                    {
                        //If Email already registered
                        lngCntID = objLeads.ContactID;
                        lngDivId = objLeads.DivisionID;
                    }
                    else
                    {
                        if (Order.billing_address != null)
                        {
                            if (!string.IsNullOrEmpty(Order.billing_address.firstname))
                                FirstName = Order.billing_address.firstname;

                            if (!string.IsNullOrEmpty(Order.billing_address.lastname))
                                LastName = Order.billing_address.lastname;
                        }

                        CompanyName = FirstName + " " + LastName;
                        objLeads.CompanyName = CompanyName;
                        objLeads.CustName = CompanyName;
                        objLeads.FirstName = FirstName;
                        objLeads.LastName = LastName;

                        objLeads.ContactPhone = Order.billing_address.telephone;
                        objLeads.PhoneExt = "";

                        if (!string.IsNullOrEmpty(Order.billing_address.country_id))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 22;
                            objCommon.Str = Order.billing_address.country_id;
                            objLeads.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objLeads.SCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        objLeads.UserCntID = numRecordOwner;
                        objLeads.ContactType = 70;
                        objLeads.PrimaryContact = true;
                        objLeads.UpdateDefaultTax = false;
                        objLeads.NoTax = false;
                        objLeads.CRMType = 1;
                        objLeads.DivisionName = "-";

                        objLeads.CompanyType = numRelationshipId;
                        objLeads.Profile = numProfileId;

                        objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();//Creates Company Record
                        LogMessage = "New Company details added CompanyID : " + objLeads.CompanyID + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        lngDivId = objLeads.CreateRecordDivisionsInfo();
                        LogMessage = "New Divisions Informations added. Division Id : " + lngDivId + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        objLeads.ContactID = 0;
                        objLeads.DivisionID = lngDivId;
                        lngCntID = objLeads.CreateRecordAddContactInfo();//Creates Contact Info
                        LogMessage = "New Contact Informations added. Contact Id : " + lngCntID + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        if (Order.billing_address != null)
                        {
                            if (!string.IsNullOrEmpty(Order.billing_address.street))
                                objContacts.BillStreet = Order.billing_address.street;

                            if (!string.IsNullOrEmpty(Order.billing_address.city))
                                objContacts.BillCity = Order.billing_address.city;

                            if (!string.IsNullOrEmpty(Order.billing_address.region_id))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 21;
                                objCommon.Str = Order.billing_address.region_id;
                                objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }
                            if (!string.IsNullOrEmpty(Order.billing_address.postcode))
                                objContacts.BillPostal = Order.billing_address.postcode;
                        }


                        if (Order.shipping_address != null)
                        {
                            if (!string.IsNullOrEmpty(Order.shipping_address.street))
                                objContacts.ShipStreet = Order.shipping_address.street;

                            if (!string.IsNullOrEmpty(Order.shipping_address.city))
                                objContacts.ShipCity = Order.shipping_address.city;

                            if (!string.IsNullOrEmpty(Order.shipping_address.region_id))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 21;
                                objCommon.Str = Order.shipping_address.region;
                                objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }

                            if (!string.IsNullOrEmpty(Order.shipping_address.country_id))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 22;
                                objCommon.Str = Order.shipping_address.country_id;
                                objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }

                            if (!string.IsNullOrEmpty(Order.shipping_address.postcode))
                                objContacts.ShipPostal = Order.shipping_address.postcode;

                            if (!string.IsNullOrEmpty(Order.shipping_address.telephone))
                            {
                                objLeads.ContactPhone = Order.shipping_address.telephone;
                                objLeads.PhoneExt = "";
                                objContacts.ContactPhone = Order.shipping_address.telephone;
                            }
                            else
                            {
                                objLeads.ContactPhone = ShipToPhoneNo;
                                objContacts.ContactPhone = ShipToPhoneNo;
                            }
                        }

                        objContacts.FirstName = objLeads.FirstName;
                        objContacts.LastName = objLeads.LastName;
                        objContacts.ContactPhone = objLeads.ContactPhone;
                        objContacts.Email = objLeads.Email;
                        objContacts.BillingAddress = 0;
                        objContacts.DivisionID = lngDivId;
                        objContacts.ContactID = lngCntID;
                        objContacts.RecordID = lngDivId;
                        objContacts.DomainID = DomainID;
                        objContacts.IsPrimaryAddress = CCommon.ToBool(1);
                        objContacts.UpdateCompanyAddress();//Updates Company Details
                    }

                    objOpportunity.BillToType = 2;
                    objOpportunity.ShipToType = 2;
                    objOpportunity.OppRefOrderNo = OrderId;
                    objOpportunity.MarketplaceOrderID = OrderId;
                    BizCommonFunctions.CreateItemTable(dtItems);
                    ItemMessage = AddOrderItemDataset(DomainID, WebApiId, numRecordOwner, numWareHouseID, DiscountItemMapping, Order, Order.shipping_description, ShippingServiceItemID, SalesTaxItemMappingID, out CurrencyID, out OrderTotal, out ShipCost, out dTotDiscount, out SalesTaxRate, out SalesTaxAmount);

                    objOpportunity.CurrencyID = CurrencyID;
                    objOpportunity.Amount = OrderTotal;

                    //objContacts.UpdateCompanyAddress();//Updates Company Details
                    BizCommonFunctions.SaveTaxTypes(ds, lngDivId);
                    arrBillingIDs = new string[4];
                    arrBillingIDs[0] = CCommon.ToString(objLeads.CompanyID);
                    arrBillingIDs[1] = CCommon.ToString(lngDivId);
                    arrBillingIDs[2] = CCommon.ToString(lngCntID);
                    arrBillingIDs[3] = objLeads.CompanyName;
                    if (lngCntID == 0)
                        return;
                    //}
                    DateTime OrderDate = Convert.ToDateTime(Order.created_at);

                    objOpportunity.MarketplaceOrderDate = OrderDate;
                    objOpportunity.OpportunityId = 0;
                    objOpportunity.ContactID = lngCntID;
                    objOpportunity.DivisionID = lngDivId;
                    objOpportunity.UserCntID = numRecordOwner;
                    objOpportunity.AssignedTo = numAssignTo;

                    objOpportunity.OpportunityName = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM");
                    objOpportunity.EstimatedCloseDate = DateTime.Now;
                    objOpportunity.PublicFlag = 0;
                    objOpportunity.DomainID = DomainID;
                    objOpportunity.OppType = 1;
                    objOpportunity.OrderStatus = numOrderStatus;

                    dsItems.Tables.Clear();
                    dsItems.Tables.Add(dtItems.Copy());
                    if (dsItems.Tables[0].Rows.Count != 0)
                        objOpportunity.strItems = ((dtItems.Rows.Count > 0) ? ("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsItems.GetXml()) : "");
                    else
                        objOpportunity.strItems = "";
                    objOpportunity.OppComments = ItemMessage;
                    objOpportunity.Source = WebApiId;
                    objOpportunity.SourceType = 3;
                    objOpportunity.DealStatus = (long)MOpportunity.EnmDealStatus.DealWon;
                    arrOutPut = objOpportunity.Save();
                    lngOppId = CCommon.ToLong(arrOutPut[0]);

                    //Save Source 
                    objOpportunity.vcSource = Source;
                    objOpportunity.WebApiId = WebApiId;
                    objOpportunity.OpportunityId = lngOppId;
                    objOpportunity.ManageOppLinking();

                    objOpportunity.CompanyID = lngDivId;

                    if (Order.billing_address != null)
                    {
                        if (!string.IsNullOrEmpty(Order.billing_address.firstname))
                            FirstName = Order.billing_address.firstname;

                        if (!string.IsNullOrEmpty(Order.billing_address.lastname))
                            LastName = Order.billing_address.lastname;

                        objOpportunity.Mode = 0;
                        objOpportunity.AddressName = FirstName + " " + LastName;
                        objOpportunity.BillCompanyName = FirstName + " " + LastName;
                        objContacts.AddressID = 0;
                        objContacts.AddressName = FirstName + " " + LastName;
                        objContacts.AddresOf = CContacts.enmAddressOf.Opportunity;
                        objContacts.AddressType = CContacts.enmAddressType.BillTo;

                        if (!string.IsNullOrEmpty(Order.billing_address.street))
                        {
                            objContacts.BillStreet = Order.billing_address.street;
                            objContacts.Street = Order.billing_address.street;
                            objOpportunity.BillStreet = Order.billing_address.street;
                        }
                        if (!string.IsNullOrEmpty(Order.billing_address.city))
                        {
                            objContacts.BillCity = Order.billing_address.city;
                            objContacts.City = Order.billing_address.city;
                            objOpportunity.BillCity = Order.billing_address.city;
                        }
                        if (!string.IsNullOrEmpty(Order.billing_address.region_id))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 21;
                            objCommon.Str = Order.billing_address.region_id;
                            objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.State = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (!string.IsNullOrEmpty(Order.billing_address.country_id))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 22;
                            objCommon.Str = Order.billing_address.country_id;
                            objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (!string.IsNullOrEmpty(Order.billing_address.postcode))
                        {
                            objContacts.BillPostal = Order.billing_address.postcode;
                            objContacts.PostalCode = Order.billing_address.postcode;
                            objOpportunity.BillPostal = Order.billing_address.postcode;
                        }
                        //objContacts.ManageAddress();
                        objOpportunity.UpdateOpportunityAddress();
                    }
                    if (Order.shipping_address != null)
                    {

                        if (!string.IsNullOrEmpty(Order.shipping_address.firstname))
                            FirstName = Order.shipping_address.firstname;

                        if (!string.IsNullOrEmpty(Order.shipping_address.lastname))
                            LastName = Order.shipping_address.lastname;

                        objOpportunity.Mode = 1;
                        objOpportunity.AddressName = FirstName + " " + LastName;
                        objOpportunity.BillCompanyName = FirstName + " " + LastName;
                        objContacts.AddressID = 0;
                        objContacts.AddressName = FirstName + " " + LastName;
                        objContacts.AddresOf = CContacts.enmAddressOf.Opportunity;
                        objContacts.AddressType = CContacts.enmAddressType.ShipTo;

                        if (!string.IsNullOrEmpty(Order.shipping_address.street))
                        {
                            objContacts.ShipStreet = Order.shipping_address.street;
                            objContacts.Street = Order.shipping_address.street;
                            objOpportunity.BillStreet = Order.shipping_address.street;
                        }
                        if (!string.IsNullOrEmpty(Order.shipping_address.city))
                        {
                            objContacts.ShipCity = Order.shipping_address.city;
                            objContacts.City = Order.shipping_address.city;
                            objOpportunity.BillCity = Order.shipping_address.city;
                        }
                        if (!string.IsNullOrEmpty(Order.shipping_address.region_id))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 21;
                            objCommon.Str = Order.shipping_address.region;
                            objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.State = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (!string.IsNullOrEmpty(Order.shipping_address.country_id))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 22;
                            objCommon.Str = Order.shipping_address.country_id;
                            objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (!string.IsNullOrEmpty(Order.shipping_address.postcode))
                        {
                            objContacts.ShipPostal = Order.shipping_address.postcode;
                            objContacts.PostalCode = Order.shipping_address.postcode;
                            objOpportunity.BillPostal = Order.shipping_address.postcode;
                        }
                        //objContacts.ManageAddress();
                        objOpportunity.UpdateOpportunityAddress();
                    }



                    LogMessage = "Order Details updated for API OrderID : " + OrderId + " Opprtunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                    //if (SalesTaxRate > 0)
                    //{
                    //    CCommon objCommon1 = new CCommon();
                    //    objCommon1.Mode = 35;
                    //    objCommon1.UpdateRecordID = lngOppId;
                    //    objCommon1.Comments = CCommon.ToString(Math.Round(SalesTaxRate, 4));
                    //    objCommon1.DomainID = DomainID;
                    //    objCommon1.UpdateSingleFieldValue();
                    //    LogMessage = "Tax Percentage updated to " + CCommon.ToString(Math.Round(SalesTaxRate,4)) + " for API OrderID : " + OrderId + " Biz Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                    //    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                    //}

                    objWebApi.WebApiId = WebApiId;
                    objWebApi.DomainID = DomainID;
                    objWebApi.UserCntID = numRecordOwner;
                    objWebApi.OppId = lngOppId;
                    objWebApi.vcAPIOppId = OrderId;
                    objWebApi.AddAPIopportunity();

                    objWebApi.API_OrderId = OrderId;
                    objWebApi.API_OrderStatus = 1;
                    objWebApi.WebApiOrdDetailId = 1;
                    objWebApi.ManageAPIOrderDetails();

                    decimal OrderAmount = OrderTotal;// +ShipCost - dTotDiscount + SalesTaxAmount;

                    //string Reference = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM"); 
                    string Reference = arrOutPut[1];
                    if (ItemMessage == "")
                    {
                        bool IsAuthoritative = false;
                        BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                        BizCommonFunctions.CreateOppBizDoc(objOppBizDocs, numRecordOwner, DomainID, lngOppId, objOpportunity.OppRefOrderNo, numBizDocId, BizDocStatusId, lngDivId, OrderDate, ShipCost, dTotDiscount, out OppBizDocID, out IsAuthoritative, ExpenseAccountId, SalesTaxRate, OrderAmount, Reference);
                        UpdateApiOppItemDetails(DomainID, WebApiId, numRecordOwner, lngOppId, OppBizDocID, objOpportunity.OppRefOrderNo, Order.items);
                    }
                    else
                    {
                        GeneralFunctions.WriteMessage(DomainID, "LOG", ItemMessage);
                    }
                }
                else
                {
                    string ErrMessage = "Order details for Magento Order Id : " + OrderId + " is already exists in BizAutomation";
                    GeneralFunctions.WriteMessage(DomainID, "LOG", ErrMessage);
                }
            }
        }

        /// <summary>
        /// Gets the Order Items List and creates a New structure of Order Item details inorder to add in BizDatabase
        ///  Adds new Item into BizDatabase if the Order Item is Not available in BizDatabase
        /// </summary>
        /// <param name="objList">Items detail List</param>
        private string AddOrderItemDataset(long DomainId, int WebApiId, long RecordOwner, int WareHouseId, long DiscountItemMapping, OrderInfo Order, string ShippingServiceSelected, long ShippingServiceItemID, long SalesTaxItemMappingID, out long CurrencyID,
            out decimal OrderTotal, out decimal ShipCost, out decimal dTotDiscount, out decimal SalesTaxRate, out decimal SalesTaxAmount)
        {
            dtItems.Clear();
            dsItems.Clear();
            CCommon objCommon = new CCommon();
            string Message = "";
            DataRow drItem;
            int ItemCount = 0;
            string ItemType = "";
            string[] itemCodeType;
            string itemCode = "";
            WebAPI objWebApi = new WebAPI();
            objWebApi.DomainID = DomainId;
            objWebApi.WebApiId = WebApiId;
            dTotDiscount = 0;
            decimal decDiscount = 0;
            string DiscountDesc = "";
            decimal Tax0 = 0;
            OrderTotal = 0;
            ShipCost = 0;
            CurrencyID = 0;
            SalesTaxAmount = 0;
            decimal TaxableTotal = 0;
            SalesTaxRate = 0;

            OrderProduct[] OrderItems;
            if (Order.items != null)
                OrderItems = Order.items;
            else
            {
                OrderItems = null;
            }

            foreach (OrderProduct taxItem in OrderItems)
            {
                if (taxItem != null)
                {
                    if (CCommon.ToDecimal(taxItem.price) > 0)
                    {
                        TaxableTotal += CCommon.ToDecimal(taxItem.price) * CCommon.ToDecimal(taxItem.qty_ordered);
                        if (CCommon.ToDecimal(Order.tax_amount) > 0)
                        {
                            SalesTaxRate = (CCommon.ToDecimal(Order.tax_amount) * 100) / TaxableTotal;
                        }
                    }
                }
            }

            if (CCommon.ToDecimal(Order.tax_amount) > 0)
            {
                //SalesTaxRate = (CCommon.ToDecimal(Order.tax_amount) * 100) / TaxableTotal;
                SalesTaxAmount = CCommon.ToDecimal(Order.tax_amount);
            }

            if (OrderItems != null & OrderItems.Length > 0)
            {
                foreach (OrderProduct item in OrderItems)
                {
                    ItemCount += 1;
                    try
                    {
                        if (!string.IsNullOrEmpty(item.sku))
                        {
                            //objCommon.DomainID = DomainId;
                            //objCommon.Mode = 20;
                            //objCommon.Str = item.sku;
                            //itemCode = CCommon.ToString(objCommon.GetSingleFieldValue());

                            objCommon.DomainID = DomainId;
                            objCommon.Mode = 38;
                            objCommon.Str = item.sku;
                            itemCodeType = CCommon.ToString(objCommon.GetSingleFieldValue()).Split('~');
                            if (itemCodeType.Length > 1)
                            {
                                if (!string.IsNullOrEmpty(itemCodeType[0]))
                                {
                                    itemCode = itemCodeType[0];
                                }
                                if (!string.IsNullOrEmpty(itemCodeType[1]))
                                {
                                    ItemType = BizCommonFunctions.GetItemTypeNameByCharType(itemCodeType[1]);
                                }
                            }
                            if (itemCode == "")
                            {
                                Product objProduct = new Product();
                                DataTable dtProductDetails = new DataTable();
                                BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                                DataRow dr = dtProductDetails.NewRow();
                                objProduct = GetProductDetailForProductId(DomainId, WebApiId, RecordOwner, item.product_id);
                                UserAccess objUserAccess = new UserAccess();
                                objUserAccess.DomainID = DomainId;
                                DataTable dtDomainDetails = objUserAccess.GetDomainDetails();
                                int DefaultIncomeAccID = 0, DefaultCOGSAccID = 0, DefaultAssetAccID = 0;
                                if (dtDomainDetails.Rows.Count > 0)
                                {
                                    DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numIncomeAccID"]);
                                    DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numCOGSAccID"]);
                                    DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numAssetAccID"]);
                                    if (DefaultIncomeAccID == 0 || DefaultCOGSAccID == 0 || DefaultAssetAccID == 0)
                                    {
                                        Message += "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                        string ExcepitonMessage = "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                        throw new Exception(ExcepitonMessage);
                                    }
                                }
                                dr = FillMagentoItemDetails(DomainId, WebApiId, RecordOwner, DefaultIncomeAccID, DefaultCOGSAccID, DefaultAssetAccID, dr, objProduct);

                                string ItemClassificationName = "Magento_OrderItem_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                                long ItemClassificationID = 0;
                                ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);
                                itemCode = CCommon.ToString(BizCommonFunctions.AddNewItemToBiz(dr, DomainId, WebApiId, RecordOwner, WareHouseId, ItemClassificationID));
                                ItemType = BizCommonFunctions.GetItemTypeNameByCharType("P");
                                WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                                string FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml";
                                if (System.IO.File.Exists(FilePath))
                                {
                                    System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                                    XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                    objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                    objStreamReader.Close();
                                }

                                objWebAPIItemDetail.MagentoProductAttributeSet = CCommon.ToInteger(dr["MagentoAttributeSetID"]);
                                objWebAPIItemDetail.MagentoProductType = CCommon.ToString(dr["MagentoProductType"]);

                                StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml");
                                XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                                Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                                objStreamWriter.Close();
                            }

                            if (itemCode != "")
                            {
                                decDiscount = 0;
                                DiscountDesc = "";
                                drItem = dtItems.NewRow();
                                drItem["numoppitemtCode"] = ItemCount;
                                drItem["numItemCode"] = itemCode;
                                if (CCommon.ToDecimal(item.qty_ordered) != 0)
                                {
                                    drItem["numUnitHour"] = item.qty_ordered;

                                    drItem["monPrice"] = CCommon.ToDecimal(item.price);
                                    // CCommon.ToInteger(orderItem.Quantity);
                                    //OrderTotal += CCommon.ToDecimal(item.price);
                                    objCommon.DomainID = DomainId;
                                    objCommon.Mode = 15;
                                    objCommon.Str = Order.base_currency_code;
                                    CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue());
                                    //drItem["monTotAmount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                    //drItem["monTotAmtBefDiscount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                    drItem["monTotAmount"] = CCommon.ToDecimal(item.price) * CCommon.ToDecimal(item.qty_ordered);

                                    drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                    OrderTotal += CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;

                                    drItem["numUOM"] = 0; // get it from item table, in procedure
                                    drItem["vcUOMName"] = "";// get it from item table, in procedure
                                    drItem["UOMConversionFactor"] = 1.0000;
                                    drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                                    drItem["vcModelID"] = "";// get it from item table, in procedure
                                    drItem["numWarehouseID"] = WareHouseId;
                                    if (item.name != null && item.name != "")
                                    {
                                        drItem["vcItemName"] = item.name;
                                    }
                                    drItem["Warehouse"] = "";
                                    drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                                    drItem["ItemType"] = ItemType;// get it from item table, in procedure
                                    drItem["Attributes"] = "";
                                    drItem["Op_Flag"] = 1;
                                    drItem["bitWorkOrder"] = false;
                                    drItem["DropShip"] = false;
                                    drItem["fltDiscount"] = decimal.Negate(decDiscount);
                                    dTotDiscount += decimal.Negate(decDiscount);
                                    //if (fltDiscount > 0)
                                    drItem["bitDiscountType"] = 1; //Flat discount
                                    //else
                                    //    drItem["bitDiscountType"] = false; // Percentage

                                    //if (!string.IsNullOrEmpty(item.tax_amount))
                                    //{
                                    //    Tax0 = CCommon.ToDecimal(item.tax_amount);
                                    //}

                                    drItem["Tax0"] = Tax0;
                                    if (Tax0 > 0)
                                        drItem["bitTaxable0"] = true;
                                    else
                                        drItem["bitTaxable0"] = false;

                                    //drItem["Tax0"] = 0;
                                    drItem["numVendorWareHouse"] = 0;
                                    drItem["numShipmentMethod"] = 0;
                                    drItem["numSOVendorId"] = 0;
                                    drItem["numProjectID"] = 0;
                                    drItem["numProjectStageID"] = 0;
                                    drItem["charItemType"] = ""; // should be taken from procedure
                                    drItem["numToWarehouseItemID"] = 0;
                                    //drItem["Tax41"] = 0;
                                    //drItem["bitTaxable41"] = false;
                                    //drItem["Tax42"] = 0;
                                    //drItem["bitTaxable42"] = false;
                                    drItem["Weight"] = ""; // should take from procedure 
                                    drItem["WebApiId"] = WebApiId;
                                    dtItems.Rows.Add(drItem);
                                    drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                                }
                            }
                            else
                            {
                                Message += Environment.NewLine + "Item " + item.name + " (SKU: " + item.sku + ") not found in Magento-Biz linking database ";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.StackTrace);
                        throw ex;
                    }
                }
                if (!string.IsNullOrEmpty(Order.shipping_amount))
                {
                    ShipCost += CCommon.ToDecimal(Order.shipping_amount);
                }
                if (!string.IsNullOrEmpty(Order.discount_amount))
                {
                    dTotDiscount += decimal.Negate(CCommon.ToDecimal(Order.discount_amount));
                }
            }
            else
            {
                Message = "No Order Items are found";
            }
            if (SalesTaxAmount > 0)
            {
                OrderTotal += SalesTaxAmount;
                drItem = dtItems.NewRow();
                drItem = BizCommonFunctions.GetSalesTaxItem(drItem, SalesTaxAmount, SalesTaxItemMappingID, DomainId, WebApiId);
                dtItems.Rows.Add(drItem);
            }
            if (ShipCost > 0)
            {
                OrderTotal += ShipCost;
                drItem = dtItems.NewRow();
                drItem = BizCommonFunctions.GetShippingCostItem(drItem, ShipCost, ShippingServiceItemID, DomainId, WebApiId);
                dtItems.Rows.Add(drItem);
            }
            if (!string.IsNullOrEmpty(Order.shipping_method))
            {
                if (Order.shipping_method == "freeshipping_freeshipping")
                {
                    drItem = dtItems.NewRow();
                    drItem = BizCommonFunctions.GetShippingCostItem(drItem, 0, ShippingServiceItemID, DomainId, WebApiId);
                    dtItems.Rows.Add(drItem);
                }
            }

            if (dTotDiscount > 0)
            {
                OrderTotal -= dTotDiscount;
                drItem = dtItems.NewRow();
                string DiscountItemName = "Total Discount";
                drItem = BizCommonFunctions.GetDiscountOrderItem(drItem, decimal.Negate(dTotDiscount), DiscountItemMapping, DomainId, WebApiId, DiscountItemName);
                dtItems.Rows.Add(drItem);
            }
            return Message;
        }

        /// <summary>
        /// Update Api Oppurtunities Item Details
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="UserCntId">UserCntId</param>
        /// <param name="OppId">Opp Id</param>
        /// <param name="OppBizDocId">Opp Biz Doc Id</param>
        /// <param name="AmazonOrderId">Amazon Order Id</param>
        /// <param name="objOrderItems">Order Items Collection</param>
        private void UpdateApiOppItemDetails(long DomainId, int WebApiId, long UserCntId, long OppId, long OppBizDocId, string MagentoOrderId, OrderProduct[] objOrderItems)
        {
            DataTable dtOppItems;
            MOpportunity objOpportunity = new MOpportunity();
            objOpportunity.Mode = 5;
            objOpportunity.DomainID = DomainId;
            objOpportunity.OpportunityId = OppId;

            WebAPI objWebApi = new WebAPI();
            try
            {
                dtOppItems = objOpportunity.GetOrderItems().Tables[0];

                foreach (DataRow dr in dtOppItems.Rows)
                {
                    foreach (OrderProduct orderItem in objOrderItems)
                    {
                        if (orderItem.sku == CCommon.ToString(dr["vcSKU"]))
                        {
                            objWebApi.DomainID = DomainId;
                            objWebApi.WebApiId = WebApiId;
                            objWebApi.OppId = OppId;
                            objWebApi.OppItemId = CCommon.ToLong(dr["numOppItemtCode"]);
                            objWebApi.vcAPIOppId = MagentoOrderId;
                            objWebApi.vcApiOppItemId = orderItem.order_id + "-" + MagentoOrderId + "-" + orderItem.product_id + "-" + orderItem.quote_item_id;
                            objWebApi.RStatus = 0;
                            objWebApi.RecordOwner = UserCntId;
                            objWebApi.ManageApiOrderItems();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Tracking Number to Magento Order
        /// </summary>
        /// <param name="DomainId">Domain Id</param>
        /// <param name="WebApiId">WebApi Id</param>
        /// <param name="API_URL">Merchant Site URL</param>
        /// <param name="API_User">API User</param>
        /// <param name="API_Key">API Key</param>
        /// <param name="ItemCode">Item Code </param>
        /// <param name="APIItemId">Magento Item Id</param>
        /// <param name="OrderId">Order Id</param>
        /// <param name="ApiOppItemId">Magento Order line Item Id</param>
        /// <param name="TrackingNo">Tracking Number</param>
        /// <param name="ShippingCarrier">Shipping Carrier</param>
        /// <param name="Mode">First or Multiple Tracking Number Update </param>
        public void UpdateTrackingNumber(long DomainId, long WebApiId, string API_URL, string API_User, string API_Key, long ItemCode, string APIItemId, string OrderId, string ApiOppItemId, string TrackingNo, string ShippingCarrier, int Mode)
        {
            string ErrorMessage = "";
            bool IsTrackingNoAdded = false;
            try
            {
                MagentoCommunicatorClient objClient = new MagentoCommunicatorClient();
                IsTrackingNoAdded = objClient.UpdateTrackingNumber(API_URL, API_User, API_Key, ApiOppItemId, TrackingNo, ShippingCarrier.ToLower());
                objClient.Close();
                if (!IsTrackingNoAdded)
                {
                    ErrorMessage = "Tracking Number " + TrackingNo + " is not updated for Magento API Opportunity ID " + ApiOppItemId;
                    throw new Exception(ErrorMessage);
                }
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Magento Service : " + er.msg;
                // GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                throw new Exception(ErrMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion Order Processing

        #region Helper Functions


        #endregion Helper Functions

        #region Test Bulk Order Import

        public void TestBulkOrderImport(OrderInfo Order, long DomainID, int WebApiId, string Source,
  int numWareHouseID, long numRecordOwner, long numAssignTo, long numRelationshipId, long numProfileId, long numBizDocId, long BizDocStatusId,
    int numOrderStatus, int ExpenseAccountId, long DiscountItemMapping, long ShippingServiceItemID, long SalesTaxItemMappingID, string ShipToPhoneNo)
        {
            decimal SalesTaxRate = 0;
            CContacts objContacts = null;
            CLeads objLeads = null;
            ImportWizard objImpWzd = new ImportWizard();
            MOpportunity objOpportunity = new MOpportunity();
            CCommon objCommon = new CCommon();

            string LogMessage = "", ErrorMsg = "";
            decimal TransactionFee = 0;
            string OrderId = Order.increment_id;
            //OrderSummary OrderDetails = new OrderSummary();

            WebAPI objWebApi = new WebAPI();

            string ItemMessage = "";
            decimal ShipCost = 0;
            decimal OrderTotal = 0;
            long CurrencyID = 0;
            decimal dTotDiscount = 0;
            decimal SalesTaxAmount = 0;
            string CompanyName = "";

            string FirstName = "", LastName = "";

            if (!string.IsNullOrEmpty(OrderId))
            {
                objWebApi.DomainID = DomainID;
                objWebApi.WebApiId = WebApiId;
                objWebApi.vcAPIOppId = OrderId;


                    objContacts = new CContacts();
                    objLeads = new CLeads();
                    objLeads.DomainID = DomainID;

                    //Address ShippingAddress = new Address();
                    //ShippingAddress = OrderDetails.buyershippingaddress;
                    string Email = Order.customer_email;


                    if (!string.IsNullOrEmpty(Email))
                    {
                        objLeads.Email = Email;
                        //Check for Details for the Given DomainID and EmailID
                        objLeads.GetConIDCompIDDivIDFromEmail();
                    }
                    else
                    {
                        objLeads.Email = "";
                    }
                    if (objLeads.ContactID > 0 && objLeads.DivisionID > 0)
                    {
                        //If Email already registered
                        lngCntID = objLeads.ContactID;
                        lngDivId = objLeads.DivisionID;
                    }
                    else
                    {
                        if (Order.billing_address != null)
                        {
                            if (!string.IsNullOrEmpty(Order.billing_address.firstname))
                                FirstName = Order.billing_address.firstname;

                            if (!string.IsNullOrEmpty(Order.billing_address.lastname))
                                LastName = Order.billing_address.lastname;
                        }

                        CompanyName = FirstName + " " + LastName;
                        objLeads.CompanyName = CompanyName;
                        objLeads.CustName = CompanyName;
                        objLeads.FirstName = FirstName;
                        objLeads.LastName = LastName;

                        objLeads.ContactPhone = Order.billing_address.telephone;
                        objLeads.PhoneExt = "";

                        if (!string.IsNullOrEmpty(Order.billing_address.country_id))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 22;
                            objCommon.Str = Order.billing_address.country_id;
                            objLeads.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objLeads.SCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        objLeads.UserCntID = numRecordOwner;
                        objLeads.ContactType = 70;
                        objLeads.PrimaryContact = true;
                        objLeads.UpdateDefaultTax = false;
                        objLeads.NoTax = false;
                        objLeads.CRMType = 1;
                        objLeads.DivisionName = "-";

                        objLeads.CompanyType = numRelationshipId;
                        objLeads.Profile = numProfileId;

                        objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();//Creates Company Record
                        LogMessage = "New Company details added CompanyID : " + objLeads.CompanyID + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        lngDivId = objLeads.CreateRecordDivisionsInfo();
                        LogMessage = "New Divisions Informations added. Division Id : " + lngDivId + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        objLeads.ContactID = 0;
                        objLeads.DivisionID = lngDivId;
                        lngCntID = objLeads.CreateRecordAddContactInfo();//Creates Contact Info
                        LogMessage = "New Contact Informations added. Contact Id : " + lngCntID + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        if (Order.billing_address != null)
                        {
                            if (!string.IsNullOrEmpty(Order.billing_address.street))
                                objContacts.BillStreet = Order.billing_address.street;

                            if (!string.IsNullOrEmpty(Order.billing_address.city))
                                objContacts.BillCity = Order.billing_address.city;

                            if (!string.IsNullOrEmpty(Order.billing_address.region_id))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 21;
                                objCommon.Str = Order.billing_address.region_id;
                                objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }
                            if (!string.IsNullOrEmpty(Order.billing_address.postcode))
                                objContacts.BillPostal = Order.billing_address.postcode;
                        }


                        if (Order.shipping_address != null)
                        {
                            if (!string.IsNullOrEmpty(Order.shipping_address.street))
                                objContacts.ShipStreet = Order.shipping_address.street;

                            if (!string.IsNullOrEmpty(Order.shipping_address.city))
                                objContacts.ShipCity = Order.shipping_address.city;

                            if (!string.IsNullOrEmpty(Order.shipping_address.region_id))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 21;
                                objCommon.Str = Order.shipping_address.region;
                                objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }

                            if (!string.IsNullOrEmpty(Order.shipping_address.country_id))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 22;
                                objCommon.Str = Order.shipping_address.country_id;
                                objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }

                            if (!string.IsNullOrEmpty(Order.shipping_address.postcode))
                                objContacts.ShipPostal = Order.shipping_address.postcode;

                            if (!string.IsNullOrEmpty(Order.shipping_address.telephone))
                            {
                                objLeads.ContactPhone = Order.shipping_address.telephone;
                                objLeads.PhoneExt = "";
                                objContacts.ContactPhone = Order.shipping_address.telephone;
                            }
                            else
                            {
                                objLeads.ContactPhone = ShipToPhoneNo;
                                objContacts.ContactPhone = ShipToPhoneNo;
                            }
                        }

                        objContacts.FirstName = objLeads.FirstName;
                        objContacts.LastName = objLeads.LastName;
                        objContacts.ContactPhone = objLeads.ContactPhone;
                        objContacts.Email = objLeads.Email;
                        objContacts.BillingAddress = 0;
                        objContacts.DivisionID = lngDivId;
                        objContacts.ContactID = lngCntID;
                        objContacts.RecordID = lngDivId;
                        objContacts.DomainID = DomainID;
                        objContacts.IsPrimaryAddress = CCommon.ToBool(1);
                        objContacts.UpdateCompanyAddress();//Updates Company Details
                    }

                    objOpportunity.BillToType = 2;
                    objOpportunity.ShipToType = 2;
                    objOpportunity.OppRefOrderNo = OrderId;
                    objOpportunity.MarketplaceOrderID = OrderId;
                    BizCommonFunctions.CreateItemTable(dtOrderItems);
                    ItemMessage = TestBulkOrder_AddOrderItemDataset(DomainID, WebApiId, numRecordOwner, numWareHouseID, DiscountItemMapping, Order, Order.shipping_description, ShippingServiceItemID, SalesTaxItemMappingID, out CurrencyID, out OrderTotal, out ShipCost, out dTotDiscount, out SalesTaxRate, out SalesTaxAmount);

                    objOpportunity.CurrencyID = CurrencyID;
                    objOpportunity.Amount = OrderTotal;

                    //objContacts.UpdateCompanyAddress();//Updates Company Details
                    BizCommonFunctions.SaveTaxTypes(ds, lngDivId);
                    arrBillingIDs = new string[4];
                    arrBillingIDs[0] = CCommon.ToString(objLeads.CompanyID);
                    arrBillingIDs[1] = CCommon.ToString(lngDivId);
                    arrBillingIDs[2] = CCommon.ToString(lngCntID);
                    arrBillingIDs[3] = objLeads.CompanyName;
                    if (lngCntID == 0)
                        return;
                    //}
                    DateTime OrderDate = Convert.ToDateTime(Order.created_at);

                    objOpportunity.MarketplaceOrderDate = OrderDate;
                    objOpportunity.OpportunityId = 0;
                    objOpportunity.ContactID = lngCntID;
                    objOpportunity.DivisionID = lngDivId;
                    objOpportunity.UserCntID = numRecordOwner;
                    objOpportunity.AssignedTo = numAssignTo;

                    objOpportunity.OpportunityName = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM");
                    objOpportunity.EstimatedCloseDate = DateTime.Now;
                    objOpportunity.PublicFlag = 0;
                    objOpportunity.DomainID = DomainID;
                    objOpportunity.OppType = 1;
                    objOpportunity.OrderStatus = numOrderStatus;

                    dsOrderItems.Tables.Clear();
                    dsOrderItems.Tables.Add(dtOrderItems.Copy());
                    if (dsOrderItems.Tables[0].Rows.Count != 0)
                        objOpportunity.strItems = ((dtOrderItems.Rows.Count > 0) ? ("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsOrderItems.GetXml()) : "");
                    else
                        objOpportunity.strItems = "";
                    objOpportunity.OppComments = ItemMessage;
                    objOpportunity.Source = WebApiId;
                    objOpportunity.SourceType = 3;
                    objOpportunity.DealStatus = (long)MOpportunity.EnmDealStatus.DealWon;
                    arrOutPut = objOpportunity.Save();
                    lngOppId = CCommon.ToLong(arrOutPut[0]);

                    //Save Source 
                    objOpportunity.vcSource = Source;
                    objOpportunity.WebApiId = WebApiId;
                    objOpportunity.OpportunityId = lngOppId;
                    objOpportunity.ManageOppLinking();

                    objOpportunity.CompanyID = lngDivId;

                    if (Order.billing_address != null)
                    {
                        if (!string.IsNullOrEmpty(Order.billing_address.firstname))
                            FirstName = Order.billing_address.firstname;

                        if (!string.IsNullOrEmpty(Order.billing_address.lastname))
                            LastName = Order.billing_address.lastname;

                        objOpportunity.Mode = 0;
                        objOpportunity.AddressName = FirstName + " " + LastName;
                        objOpportunity.BillCompanyName = FirstName + " " + LastName;
                        objContacts.AddressID = 0;
                        objContacts.AddressName = FirstName + " " + LastName;
                        objContacts.AddresOf = CContacts.enmAddressOf.Opportunity;
                        objContacts.AddressType = CContacts.enmAddressType.BillTo;

                        if (!string.IsNullOrEmpty(Order.billing_address.street))
                        {
                            objContacts.BillStreet = Order.billing_address.street;
                            objContacts.Street = Order.billing_address.street;
                            objOpportunity.BillStreet = Order.billing_address.street;
                        }
                        if (!string.IsNullOrEmpty(Order.billing_address.city))
                        {
                            objContacts.BillCity = Order.billing_address.city;
                            objContacts.City = Order.billing_address.city;
                            objOpportunity.BillCity = Order.billing_address.city;
                        }
                        if (!string.IsNullOrEmpty(Order.billing_address.region_id))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 21;
                            objCommon.Str = Order.billing_address.region_id;
                            objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.State = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (!string.IsNullOrEmpty(Order.billing_address.country_id))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 22;
                            objCommon.Str = Order.billing_address.country_id;
                            objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (!string.IsNullOrEmpty(Order.billing_address.postcode))
                        {
                            objContacts.BillPostal = Order.billing_address.postcode;
                            objContacts.PostalCode = Order.billing_address.postcode;
                            objOpportunity.BillPostal = Order.billing_address.postcode;
                        }
                        //objContacts.ManageAddress();
                        objOpportunity.UpdateOpportunityAddress();
                    }
                    if (Order.shipping_address != null)
                    {

                        if (!string.IsNullOrEmpty(Order.shipping_address.firstname))
                            FirstName = Order.shipping_address.firstname;

                        if (!string.IsNullOrEmpty(Order.shipping_address.lastname))
                            LastName = Order.shipping_address.lastname;

                        objOpportunity.Mode = 1;
                        objOpportunity.AddressName = FirstName + " " + LastName;
                        objOpportunity.BillCompanyName = FirstName + " " + LastName;
                        objContacts.AddressID = 0;
                        objContacts.AddressName = FirstName + " " + LastName;
                        objContacts.AddresOf = CContacts.enmAddressOf.Opportunity;
                        objContacts.AddressType = CContacts.enmAddressType.ShipTo;

                        if (!string.IsNullOrEmpty(Order.shipping_address.street))
                        {
                            objContacts.ShipStreet = Order.shipping_address.street;
                            objContacts.Street = Order.shipping_address.street;
                            objOpportunity.BillStreet = Order.shipping_address.street;
                        }
                        if (!string.IsNullOrEmpty(Order.shipping_address.city))
                        {
                            objContacts.ShipCity = Order.shipping_address.city;
                            objContacts.City = Order.shipping_address.city;
                            objOpportunity.BillCity = Order.shipping_address.city;
                        }
                        if (!string.IsNullOrEmpty(Order.shipping_address.region_id))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 21;
                            objCommon.Str = Order.shipping_address.region;
                            objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.State = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (!string.IsNullOrEmpty(Order.shipping_address.country_id))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 22;
                            objCommon.Str = Order.shipping_address.country_id;
                            objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (!string.IsNullOrEmpty(Order.shipping_address.postcode))
                        {
                            objContacts.ShipPostal = Order.shipping_address.postcode;
                            objContacts.PostalCode = Order.shipping_address.postcode;
                            objOpportunity.BillPostal = Order.shipping_address.postcode;
                        }
                        //objContacts.ManageAddress();
                        objOpportunity.UpdateOpportunityAddress();
                    }



                    LogMessage = "Order Details updated for API OrderID : " + OrderId + " Opprtunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                    //if (SalesTaxRate > 0)
                    //{
                    //    CCommon objCommon1 = new CCommon();
                    //    objCommon1.Mode = 35;
                    //    objCommon1.UpdateRecordID = lngOppId;
                    //    objCommon1.Comments = CCommon.ToString(Math.Round(SalesTaxRate, 4));
                    //    objCommon1.DomainID = DomainID;
                    //    objCommon1.UpdateSingleFieldValue();
                    //    LogMessage = "Tax Percentage updated to " + CCommon.ToString(Math.Round(SalesTaxRate,4)) + " for API OrderID : " + OrderId + " Biz Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                    //    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                    //}

                    objWebApi.WebApiId = WebApiId;
                    objWebApi.DomainID = DomainID;
                    objWebApi.UserCntID = numRecordOwner;
                    objWebApi.OppId = lngOppId;
                    objWebApi.vcAPIOppId = OrderId;
                    objWebApi.AddAPIopportunity();

                    objWebApi.API_OrderId = OrderId;
                    objWebApi.API_OrderStatus = 1;
                    objWebApi.WebApiOrdDetailId = 1;
                    objWebApi.ManageAPIOrderDetails();

                    decimal OrderAmount = OrderTotal;// +ShipCost - dTotDiscount + SalesTaxAmount;

                    //string Reference = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM"); 
                    string Reference = arrOutPut[1];
                    if (ItemMessage == "")
                    {
                        bool IsAuthoritative = false;
                        BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                        BizCommonFunctions.CreateOppBizDoc(objOppBizDocs, numRecordOwner, DomainID, lngOppId, objOpportunity.OppRefOrderNo, numBizDocId, BizDocStatusId, lngDivId, OrderDate, ShipCost, dTotDiscount, out OppBizDocID, out IsAuthoritative, ExpenseAccountId, SalesTaxRate, OrderAmount, Reference);
                        UpdateApiOppItemDetails(DomainID, WebApiId, numRecordOwner, lngOppId, OppBizDocID, objOpportunity.OppRefOrderNo, Order.items);
                    }
                    else
                    {
                        GeneralFunctions.WriteMessage(DomainID, "LOG", ItemMessage);
                    }
              
            }
        }

        /// <summary>
        /// Gets the Order Items List and creates a New structure of Order Item details inorder to add in BizDatabase
        ///  Adds new Item into BizDatabase if the Order Item is Not available in BizDatabase
        /// </summary>
        /// <param name="objList">Items detail List</param>
        private string TestBulkOrder_AddOrderItemDataset(long DomainId, int WebApiId, long RecordOwner, int WareHouseId, long DiscountItemMapping, OrderInfo Order, string ShippingServiceSelected, long ShippingServiceItemID, long SalesTaxItemMappingID, out long CurrencyID,
            out decimal OrderTotal, out decimal ShipCost, out decimal dTotDiscount, out decimal SalesTaxRate, out decimal SalesTaxAmount)
        {
            dtOrderItems.Clear();
            dsOrderItems.Clear();
            CCommon objCommon = new CCommon();
            string Message = "";
            DataRow drItem;
            int ItemCount = 0;
            string ItemType = "";
            string[] itemCodeType;
            string itemCode = "";
            WebAPI objWebApi = new WebAPI();
            objWebApi.DomainID = DomainId;
            objWebApi.WebApiId = WebApiId;
            dTotDiscount = 0;
            decimal decDiscount = 0;
            string DiscountDesc = "";
            decimal Tax0 = 0;
            OrderTotal = 0;
            ShipCost = 0;
            CurrencyID = 0;
            SalesTaxAmount = 0;
            decimal TaxableTotal = 0;
            SalesTaxRate = 0;

            OrderProduct[] OrderItems;
            if (Order.items != null)
                OrderItems = Order.items;
            else
            {
                OrderItems = null;
            }

            foreach (OrderProduct taxItem in OrderItems)
            {
                if (taxItem != null)
                {
                    if (CCommon.ToDecimal(taxItem.price) > 0)
                    {
                        TaxableTotal += CCommon.ToDecimal(taxItem.price) * CCommon.ToDecimal(taxItem.qty_ordered);
                        if (CCommon.ToDecimal(Order.tax_amount) > 0)
                        {
                            SalesTaxRate = (CCommon.ToDecimal(Order.tax_amount) * 100) / TaxableTotal;
                        }
                    }
                }
            }

            if (CCommon.ToDecimal(Order.tax_amount) > 0)
            {
                //SalesTaxRate = (CCommon.ToDecimal(Order.tax_amount) * 100) / TaxableTotal;
                SalesTaxAmount = CCommon.ToDecimal(Order.tax_amount);
            }

            if (OrderItems != null & OrderItems.Length > 0)
            {
                foreach (OrderProduct item in OrderItems)
                {
                    ItemCount += 1;
                    try
                    {
                        if (!string.IsNullOrEmpty(item.sku))
                        {
                            //objCommon.DomainID = DomainId;
                            //objCommon.Mode = 20;
                            //objCommon.Str = item.sku;
                            //itemCode = CCommon.ToString(objCommon.GetSingleFieldValue());

                            objCommon.DomainID = DomainId;
                            objCommon.Mode = 38;
                            objCommon.Str = item.sku;
                            itemCodeType = CCommon.ToString(objCommon.GetSingleFieldValue()).Split('~');
                            if (itemCodeType.Length > 1)
                            {
                                if (!string.IsNullOrEmpty(itemCodeType[0]))
                                {
                                    itemCode = itemCodeType[0];
                                }
                                if (!string.IsNullOrEmpty(itemCodeType[1]))
                                {
                                    ItemType = BizCommonFunctions.GetItemTypeNameByCharType(itemCodeType[1]);
                                }
                            }
                            if (itemCode == "")
                            {
                                Product objProduct = new Product();
                                DataTable dtProductDetails = new DataTable();
                                BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                                DataRow dr = dtProductDetails.NewRow();
                                objProduct = GetProductDetailForProductId(DomainId, WebApiId, RecordOwner, item.product_id);
                                UserAccess objUserAccess = new UserAccess();
                                objUserAccess.DomainID = DomainId;
                                DataTable dtDomainDetails = objUserAccess.GetDomainDetails();
                                int DefaultIncomeAccID = 0, DefaultCOGSAccID = 0, DefaultAssetAccID = 0;
                                if (dtDomainDetails.Rows.Count > 0)
                                {
                                    DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numIncomeAccID"]);
                                    DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numCOGSAccID"]);
                                    DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numAssetAccID"]);
                                    if (DefaultIncomeAccID == 0 || DefaultCOGSAccID == 0 || DefaultAssetAccID == 0)
                                    {
                                        Message += "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                        string ExcepitonMessage = "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                        throw new Exception(ExcepitonMessage);
                                    }
                                }
                                dr = FillMagentoItemDetails(DomainId, WebApiId, RecordOwner, DefaultIncomeAccID, DefaultCOGSAccID, DefaultAssetAccID, dr, objProduct);

                                string ItemClassificationName = "Magento_OrderItem_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                                long ItemClassificationID = 0;
                                ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);
                                itemCode = CCommon.ToString(BizCommonFunctions.AddNewItemToBiz(dr, DomainId, WebApiId, RecordOwner, WareHouseId, ItemClassificationID));
                                ItemType = BizCommonFunctions.GetItemTypeNameByCharType("P");
                                WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                                string FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml";
                                if (System.IO.File.Exists(FilePath))
                                {
                                    System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                                    XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                    objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                    objStreamReader.Close();
                                }

                                objWebAPIItemDetail.MagentoProductAttributeSet = CCommon.ToInteger(dr["MagentoAttributeSetID"]);
                                objWebAPIItemDetail.MagentoProductType = CCommon.ToString(dr["MagentoProductType"]);

                                StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml");
                                XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                                Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                                objStreamWriter.Close();
                            }

                            if (itemCode != "")
                            {
                                decDiscount = 0;
                                DiscountDesc = "";
                                drItem = dtOrderItems.NewRow();
                                drItem["numoppitemtCode"] = ItemCount;
                                drItem["numItemCode"] = itemCode;
                                if (CCommon.ToDecimal(item.qty_ordered) != 0)
                                {
                                    drItem["numUnitHour"] = item.qty_ordered;

                                    drItem["monPrice"] = CCommon.ToDecimal(item.price);
                                    // CCommon.ToInteger(orderItem.Quantity);
                                    //OrderTotal += CCommon.ToDecimal(item.price);
                                    objCommon.DomainID = DomainId;
                                    objCommon.Mode = 15;
                                    objCommon.Str = Order.base_currency_code;
                                    CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue());
                                    //drItem["monTotAmount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                    //drItem["monTotAmtBefDiscount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                    drItem["monTotAmount"] = CCommon.ToDecimal(item.price) * CCommon.ToDecimal(item.qty_ordered);

                                    drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                    OrderTotal += CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;

                                    drItem["numUOM"] = 0; // get it from item table, in procedure
                                    drItem["vcUOMName"] = "";// get it from item table, in procedure
                                    drItem["UOMConversionFactor"] = 1.0000;
                                    drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                                    drItem["vcModelID"] = "";// get it from item table, in procedure
                                    drItem["numWarehouseID"] = WareHouseId;
                                    if (item.name != null && item.name != "")
                                    {
                                        drItem["vcItemName"] = item.name;
                                    }
                                    drItem["Warehouse"] = "";
                                    drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                                    drItem["ItemType"] = ItemType;// get it from item table, in procedure
                                    drItem["Attributes"] = "";
                                    drItem["Op_Flag"] = 1;
                                    drItem["bitWorkOrder"] = false;
                                    drItem["DropShip"] = false;
                                    drItem["fltDiscount"] = decimal.Negate(decDiscount);
                                    dTotDiscount += decimal.Negate(decDiscount);
                                    //if (fltDiscount > 0)
                                    drItem["bitDiscountType"] = 1; //Flat discount
                                    //else
                                    //    drItem["bitDiscountType"] = false; // Percentage

                                    //if (!string.IsNullOrEmpty(item.tax_amount))
                                    //{
                                    //    Tax0 = CCommon.ToDecimal(item.tax_amount);
                                    //}

                                    drItem["Tax0"] = Tax0;
                                    if (Tax0 > 0)
                                        drItem["bitTaxable0"] = true;
                                    else
                                        drItem["bitTaxable0"] = false;

                                    //drItem["Tax0"] = 0;
                                    drItem["numVendorWareHouse"] = 0;
                                    drItem["numShipmentMethod"] = 0;
                                    drItem["numSOVendorId"] = 0;
                                    drItem["numProjectID"] = 0;
                                    drItem["numProjectStageID"] = 0;
                                    drItem["charItemType"] = ""; // should be taken from procedure
                                    drItem["numToWarehouseItemID"] = 0;
                                    //drItem["Tax41"] = 0;
                                    //drItem["bitTaxable41"] = false;
                                    //drItem["Tax42"] = 0;
                                    //drItem["bitTaxable42"] = false;
                                    drItem["Weight"] = ""; // should take from procedure 
                                    drItem["WebApiId"] = WebApiId;
                                    dtOrderItems.Rows.Add(drItem);
                                    drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                                }
                            }
                            else
                            {
                                Message += Environment.NewLine + "Item " + item.name + " (SKU: " + item.sku + ") not found in Magento-Biz linking database ";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.StackTrace);
                        throw ex;
                    }
                }
                if (!string.IsNullOrEmpty(Order.shipping_amount))
                {
                    ShipCost += CCommon.ToDecimal(Order.shipping_amount);
                }
                if (!string.IsNullOrEmpty(Order.discount_amount))
                {
                    dTotDiscount += decimal.Negate(CCommon.ToDecimal(Order.discount_amount));
                }
            }
            else
            {
                Message = "No Order Items are found";
            }
            if (SalesTaxAmount > 0)
            {
                OrderTotal += SalesTaxAmount;
                drItem = dtOrderItems.NewRow();
                drItem = BizCommonFunctions.GetSalesTaxItem(drItem, SalesTaxAmount, SalesTaxItemMappingID, DomainId, WebApiId);
                dtOrderItems.Rows.Add(drItem);
            }
            if (ShipCost > 0)
            {
                OrderTotal += ShipCost;
                drItem = dtOrderItems.NewRow();
                drItem = BizCommonFunctions.GetShippingCostItem(drItem, ShipCost, ShippingServiceItemID, DomainId, WebApiId);
                dtOrderItems.Rows.Add(drItem);
            }
            if (!string.IsNullOrEmpty(Order.shipping_method))
            {
                if (Order.shipping_method == "freeshipping_freeshipping")
                {
                    drItem = dtOrderItems.NewRow();
                    drItem = BizCommonFunctions.GetShippingCostItem(drItem, 0, ShippingServiceItemID, DomainId, WebApiId);
                    dtOrderItems.Rows.Add(drItem);
                }
            }

            if (dTotDiscount > 0)
            {
                OrderTotal -= dTotDiscount;
                drItem = dtOrderItems.NewRow();
                string DiscountItemName = "Total Discount";
                drItem = BizCommonFunctions.GetDiscountOrderItem(drItem, decimal.Negate(dTotDiscount), DiscountItemMapping, DomainId, WebApiId, DiscountItemName);
                dtOrderItems.Rows.Add(drItem);
            }
            return Message;
        }

        #endregion Test Bulk Order Import

    }
}
