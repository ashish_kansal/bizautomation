﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.ServiceModel;

using eBay.Service.Core.Sdk;
using eBay.Service.Core.Soap;
using eBay.Service.Call;
using eBay.Service.Util;

using MarketplaceService.EBay_Communicator;

using BACRM.BusinessLogic;
using BACRMBUSSLOGIC.BussinessLogic;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Prospects;
using BACRM.BusinessLogic.Contract;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.WebAPI;
using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Alerts;

namespace MarketplaceService
{
    class eBayClient
    {
        #region Members

        DataSet ds = new DataSet();
        DataSet dsItems = new DataSet();

        DataTable dtItems = new DataTable();
        DataTable dtCompanyTaxTypes = new DataTable();
        DataTable dtItemSpecifics = new DataTable();
        DataRow drtax = null;
        DataTable dtItemVariationSpecifics = new DataTable();
        DataTable dtItemVariations = new DataTable();

        DataSet dsOrderItems = new DataSet();
        DataTable dtOrderItems = new DataTable();

        long lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId;
        OppBizDocs objOppBizDocs = new OppBizDocs();
        string strdetails = "";
        string[] arrOutPut;
        string[] arrBillingIDs;

        private string OrderFilePath = ConfigurationManager.AppSettings["EBayOrderFilePath"];
        private string EBayClosedOrders = ConfigurationManager.AppSettings["EBayClosedOrders"];
        private string ProductImagePath = ConfigurationManager.AppSettings["EBayProductImagePath"];
        private bool IsDebugMode = CCommon.ToBool(ConfigurationManager.AppSettings["DebugMode"]);

        #endregion Members

        #region Order Processing Functions

        public void GeteBayOrderById(long DomainId, int WebApiId, string Token, string eBaySite, string OrderId)
        {
            WebAPI objWebAPI = new WebAPI();
            try
            {
                string LogMessage = "";
                ApiContext Context = GetContext(Token, eBaySite);

                IeBayCommunicatorClient eBayClient = new IeBayCommunicatorClient();
                MemoryStream ms = eBayClient.GetOrderById(Context, OrderId);
                OrderTypeCollection orders = new OrderTypeCollection();
                XmlSerializer serializer = new XmlSerializer(typeof(OrderTypeCollection));
                XmlReader Reader = new XmlTextReader(ms);
                orders = (OrderTypeCollection)serializer.Deserialize(Reader);
                if (orders.Count > 0)
                    GeneralFunctions.SerializeObjectToFile(orders, GeneralFunctions.GetPath(OrderFilePath, DomainId) + "orders" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                else
                {
                    LogMessage = "Unable to process the import Order request. Error importing Order detail for e-Bay Order Id : " + OrderId;
                    LogMessage += " Order detail not found for e-Bay Order Id : " + OrderId;
                    GeneralFunctions.WriteMessage(DomainId, "LOG", "eBay:- From GeteBayOrderById : " + LogMessage);
                }

            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from E-Bay E-Bay Service : " + er.msg;
                throw new Exception("eBay:- From GeteBayOrderById : " + ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Gets Orders list from E-Bay for the specified time limit
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="Token">Merchant Token</param>
        /// <param name="eBaySite">eBaySite</param>
        /// <param name="fromDate">From Date</param>
        /// <param name="ToDate">To Date</param>
        public void GeteBayOrders(long DomainId, int WebApiId, string Token, string eBaySite, DateTime fromDate, DateTime ToDate)
        {
            WebAPI objWebAPI = new WebAPI();
            try
            {
                ApiContext Context = GetContext(Token, eBaySite);
                IeBayCommunicatorClient eBayClient = new IeBayCommunicatorClient();
                MemoryStream ms = eBayClient.GetOrders(Context, fromDate, ToDate);
                OrderTypeCollection orders = new OrderTypeCollection();
                XmlSerializer serializer = new XmlSerializer(typeof(OrderTypeCollection));
                XmlReader Reader = new XmlTextReader(ms);
                orders = (OrderTypeCollection)serializer.Deserialize(Reader);
                if (orders.Count > 0)
                    GeneralFunctions.SerializeObjectToFile(orders, GeneralFunctions.GetPath(OrderFilePath, DomainId) + "orders" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                objWebAPI.DomainID = DomainId;
                objWebAPI.WebApiId = WebApiId;
                objWebAPI.UpdateAPISettingsDate(1);
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from E-Bay E-Bay Service : " + er.msg;
                throw new Exception("eBay:- From GeteBayOrders : " + ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Adds E-Bay Orders to BizAutomation for the specified Domain
        /// </summary>
        /// <param name="orders">E-Bay Orders Collection for the Merchant's E-Bay Account</param>
        /// <param name="DomainID">Biz Domain ID for the Merchant</param>
        /// <param name="WebApiId">Web API ID</param>
        /// <param name="Source">Order's Source</param>
        /// <param name="numWareHouseID">Default Warehouse Id for the E-Bay Order Items</param>
        /// <param name="numRecordOwner">Orders Reocrd Owner's Id</param>
        /// <param name="numAssignTo">Assinged to User Id</param>
        /// <param name="numRelationshipId">E-Bay Orders Customer's default relationship Id</param>
        /// <param name="numProfileId">Default Profile Id</param>
        /// <param name="numBizDocId">Default Biz Document Id for E-Bay Orders</param>
        /// <param name="numOrderStatus">Default Order Status</param>
        /// <param name="ExpenseAccountId">Default Expense Account Id</param>
        public void ProcessEBayOrders(OrderTypeCollection orders, long DomainID, int WebApiId, string Source,
           int numWareHouseID, long numRecordOwner, long numAssignTo, long numRelationshipId, long numProfileId, long numBizDocId, long BizDocStatusId, int numOrderStatus, int ExpenseAccountId, long DiscountItemMapping, long ShippingServiceItemID, long SalesTaxItemMappingID, string ShipToPhoneNo)
        {
            string LogMessage = "", ErrorMsg = "";

            CContacts objContacts = null;
            CLeads objLeads = null;
            ImportWizard objImpWzd = new ImportWizard();
            MOpportunity objOpportunity = new MOpportunity();
            CCommon objCommon = new CCommon();
            foreach (OrderType Order in orders)
            {
                try
                {
                    decimal SalesTaxRate = 0;
                    string OrderId = Order.OrderID;
                    LogMessage += Environment.NewLine + "OrderId : " + OrderId;
                    string OrderStatus = "";

                    if (Order.OrderStatusSpecified)
                        OrderStatus = CCommon.ToString(Order.OrderStatus);

                    WebAPI objWebApi = new WebAPI();

                    string ItemMessage = "";
                    decimal ShipCost = 0;
                    decimal OrderTotal = 0;
                    long CurrencyID = 0;
                    decimal dTotDiscount = 0;
                    decimal SalesTaxAmount = 0;

                    if (!string.IsNullOrEmpty(OrderId))
                    {
                        objWebApi.DomainID = DomainID;
                        objWebApi.WebApiId = WebApiId;
                        objWebApi.vcAPIOppId = OrderId;
                        if (objWebApi.CheckDuplicateAPIOpportunity())
                        {
                            int BizPaymentMethodId = 4;
                            int a = -1;
                            string AddressStreet = "";//To concatenate Address Lines
                            objContacts = new CContacts();
                            objLeads = new CLeads();
                            objLeads.DomainID = DomainID;

                            AddressType ShippingAdress = Order.ShippingAddress;
                            string CompanyName = ShippingAdress.CompanyName;
                            LogMessage += Environment.NewLine + " CompanyName : " + CompanyName;

                            string FirstName = ShippingAdress.FirstName;
                            LogMessage += Environment.NewLine + " FirstName : " + FirstName;

                            string LastName = ShippingAdress.LastName;
                            LogMessage += Environment.NewLine + " LastName : " + LastName;

                            string Name = ShippingAdress.Name;
                            LogMessage += Environment.NewLine + " Name : " + Name;

                            string BuyerUserID = Order.BuyerUserID;
                            LogMessage += Environment.NewLine + " BuyerUserID : " + BuyerUserID;

                            if (Order.TransactionArray != null)
                            {
                                TransactionTypeCollection TransTypColl = new TransactionTypeCollection(Order.TransactionArray);

                                foreach (TransactionType TransTyp in TransTypColl)
                                {
                                    if (TransTyp.Buyer != null)
                                    {
                                        UserType Buyer = TransTyp.Buyer;
                                        //LogMessage += Environment.NewLine + " TransTyp.Buyer : " + CCommon.ToString(Buyer);
                                        LogMessage += Environment.NewLine + " TransTyp.Email : " + Buyer.Email;
                                        if (Buyer.Email != null && Buyer.Email != "" && Buyer.Email != "Invalid Request")//If contains Email Address
                                        {
                                            objLeads.Email = Buyer.Email;

                                            //Check for Details for the Given DomainID and EmailID
                                            objLeads.GetConIDCompIDDivIDFromEmail();
                                        }
                                        else
                                        {
                                            objLeads.Email = "";
                                        }
                                        if (objLeads.ContactID > 0 && objLeads.DivisionID > 0)
                                        {
                                            //If Email already registered
                                            lngCntID = objLeads.ContactID;
                                            lngDivId = objLeads.DivisionID;
                                        }
                                        else
                                        {
                                            if (ShippingAdress.Name != null && ShippingAdress.Name != "")
                                            {
                                                objLeads.CompanyName = ShippingAdress.Name;
                                                objLeads.CustName = ShippingAdress.Name;
                                            }
                                            else if (Order.ShippingAddress != null)
                                            {
                                                AddressType shippingAddress = Order.ShippingAddress;

                                                if (shippingAddress.Name != null && shippingAddress.Name != "")
                                                {
                                                    objLeads.CompanyName = shippingAddress.Name;
                                                    objLeads.CustName = shippingAddress.Name;
                                                }
                                            }

                                            if (Order.ShippingAddress != null)
                                            {
                                                AddressType shippingAddress = Order.ShippingAddress;
                                                if (shippingAddress.County != null && shippingAddress.County != "")
                                                {
                                                    //objLeads.Country = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                                    //objLeads.SCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                                }
                                                if (shippingAddress.Country != null && CCommon.ToString(shippingAddress.Country) != "")
                                                {
                                                    objCommon.DomainID = DomainID;
                                                    objCommon.Mode = 22;
                                                    objCommon.Str = CCommon.ToString(shippingAddress.Country);
                                                    objLeads.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                    objLeads.SCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                }

                                                if (shippingAddress.Name != null && shippingAddress.Name != "")
                                                {
                                                    a = shippingAddress.Name.IndexOf('.');
                                                    if (a == -1)
                                                        a = shippingAddress.Name.IndexOf(' ');
                                                    if (a == -1)
                                                    {
                                                        objLeads.FirstName = shippingAddress.Name;
                                                        objLeads.LastName = shippingAddress.Name;
                                                    }
                                                    else
                                                    {
                                                        objLeads.FirstName = shippingAddress.Name.Substring(0, a);
                                                        objLeads.LastName = shippingAddress.Name.Substring(a + 1, shippingAddress.Name.Length - (a + 1));
                                                    }
                                                }
                                                if (shippingAddress.Phone != null)
                                                {
                                                    objLeads.ContactPhone = shippingAddress.Phone;
                                                    objLeads.PhoneExt = "";
                                                }
                                                else if (shippingAddress.Phone2 != null)
                                                {
                                                    objLeads.ContactPhone = shippingAddress.Phone2;
                                                    objLeads.PhoneExt = "";
                                                }
                                                else
                                                {
                                                    objLeads.ContactPhone = ShipToPhoneNo;
                                                    objLeads.PhoneExt = "";
                                                }
                                            }
                                            objLeads.UserCntID = numRecordOwner;
                                            objLeads.ContactType = 70;
                                            objLeads.PrimaryContact = true;
                                            objLeads.UpdateDefaultTax = false;
                                            objLeads.NoTax = false;
                                            objLeads.CRMType = 1;
                                            objLeads.DivisionName = "-";

                                            objLeads.CompanyType = numRelationshipId;
                                            objLeads.Profile = numProfileId;

                                            objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();//Creates Company Record
                                            LogMessage = "New Company details added CompanyID : " + objLeads.CompanyID + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                            GeneralFunctions.WriteMessage(DomainID, "LOG", "eBay:- From ProcessEBayOrders : " + LogMessage);

                                            lngDivId = objLeads.CreateRecordDivisionsInfo();
                                            LogMessage = "New Divisions Informations added. Division Id : " + lngDivId + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                            GeneralFunctions.WriteMessage(DomainID, "LOG", "eBay:- From ProcessEBayOrders : " + LogMessage);

                                            objLeads.ContactID = 0;
                                            objLeads.DivisionID = lngDivId;
                                            lngCntID = objLeads.CreateRecordAddContactInfo();//Creates Contact Info
                                            LogMessage = "New Contact Informations added. Contact Id : " + lngCntID + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                            GeneralFunctions.WriteMessage(DomainID, "LOG", "eBay:- From ProcessEBayOrders : " + LogMessage);

                                            if (Order.ShippingAddress != null)
                                            {
                                                objContacts.FirstName = objLeads.FirstName;
                                                objContacts.LastName = objLeads.LastName;
                                                objContacts.ContactPhone = objLeads.ContactPhone;
                                                objContacts.Email = objLeads.Email;

                                                AddressType shippingAddress = Order.ShippingAddress;

                                                if (shippingAddress.Street != null && shippingAddress.Street != "")
                                                    AddressStreet = shippingAddress.Street;

                                                if (shippingAddress.Street1 != null && shippingAddress.Street1 != "")
                                                    AddressStreet = AddressStreet + " " + shippingAddress.Street1;

                                                if (shippingAddress.Street2 != null && shippingAddress.Street2 != "")
                                                    AddressStreet = AddressStreet + " " + shippingAddress.Street2;

                                                objContacts.BillStreet = AddressStreet;
                                                objContacts.ShipStreet = AddressStreet;

                                                if (shippingAddress.CityName != null && shippingAddress.CityName != "")
                                                {
                                                    objContacts.BillCity = shippingAddress.CityName;
                                                    objContacts.ShipCity = shippingAddress.CityName;
                                                }

                                                if (shippingAddress.StateOrProvince != null && shippingAddress.StateOrProvince != "")
                                                {
                                                    objCommon.DomainID = DomainID;
                                                    objCommon.Mode = 21;
                                                    objCommon.Str = shippingAddress.StateOrProvince;
                                                    objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                    objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                }

                                                if (shippingAddress.Country != null && CCommon.ToString(shippingAddress.Country) != "")
                                                {
                                                    objCommon.DomainID = DomainID;
                                                    objCommon.Mode = 22;
                                                    objCommon.Str = CCommon.ToString(shippingAddress.Country);
                                                    objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                    objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                }

                                                if (shippingAddress.PostalCode != null && shippingAddress.PostalCode != "")
                                                {
                                                    objContacts.BillPostal = shippingAddress.PostalCode;
                                                    objContacts.ShipPostal = shippingAddress.PostalCode;
                                                }

                                                objContacts.BillingAddress = 0;
                                                objContacts.DivisionID = lngDivId;
                                                objContacts.ContactID = lngCntID;
                                                objContacts.RecordID = lngDivId;
                                                objContacts.DomainID = DomainID;
                                                objContacts.IsPrimaryAddress = CCommon.ToBool(1);
                                                objContacts.UpdateCompanyAddress();
                                            }
                                        }

                                        if (Order.CheckoutStatus != null)
                                        {
                                            if (Order.CheckoutStatus.PaymentMethod != null)
                                            {
                                                BizPaymentMethodId = GetOrderPaymentMethod(Order.CheckoutStatus.PaymentMethod);
                                            }
                                        }

                                        decimal decAmountSaved = 0;
                                        if (Order.AmountSaved != null)
                                        {
                                            decAmountSaved = CCommon.ToDecimal(Order.AmountSaved.Value);
                                        }

                                        objOpportunity.OppRefOrderNo = Order.OrderID;
                                        objOpportunity.MarketplaceOrderID = Order.OrderID;
                                        BizCommonFunctions.CreateItemTable(dtItems);
                                        ItemMessage = AddOrderItemDataset(DomainID, WebApiId, numWareHouseID, numRecordOwner, DiscountItemMapping, TransTypColl, Order.ShippingServiceSelected.ShippingService, ShippingServiceItemID, SalesTaxItemMappingID, decAmountSaved, out CurrencyID, out OrderTotal, out ShipCost, out dTotDiscount, out SalesTaxAmount);

                                        objOpportunity.CurrencyID = CurrencyID;
                                        objOpportunity.Amount = OrderTotal;
                                        //objContacts.BillingAddress = 0;
                                        //objContacts.DivisionID = lngDivId;
                                        //objContacts.UpdateCompanyAddress();//Updates Company Details
                                        BizCommonFunctions.SaveTaxTypes(ds, lngDivId);
                                        arrBillingIDs = new string[4];
                                        arrBillingIDs[0] = CCommon.ToString(objLeads.CompanyID);
                                        arrBillingIDs[1] = CCommon.ToString(lngDivId);
                                        arrBillingIDs[2] = CCommon.ToString(lngCntID);
                                        arrBillingIDs[3] = objLeads.CompanyName;
                                        if (lngCntID == 0)
                                            return;
                                        //Create Opportunity Details
                                        DateTime OrderDate = Order.CreatedTime;
                                        objOpportunity.MarketplaceOrderDate = OrderDate;
                                        objOpportunity.OpportunityId = 0;
                                        objOpportunity.OpportunityId = 0;
                                        objOpportunity.ContactID = lngCntID;
                                        objOpportunity.DivisionID = lngDivId;
                                        objOpportunity.UserCntID = numRecordOwner;
                                        objOpportunity.AssignedTo = numAssignTo;
                                        objOpportunity.OpportunityName = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM");
                                        objOpportunity.EstimatedCloseDate = DateTime.Now;
                                        objOpportunity.PublicFlag = 0;
                                        objOpportunity.DomainID = DomainID;
                                        objOpportunity.OppType = 1;
                                        objOpportunity.OrderStatus = numOrderStatus;

                                        dsItems.Tables.Clear();
                                        dsItems.Tables.Add(dtItems.Copy());
                                        if (dsItems.Tables[0].Rows.Count != 0)
                                            objOpportunity.strItems = ((dtItems.Rows.Count > 0) ? ("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsItems.GetXml()) : "");
                                        else
                                            objOpportunity.strItems = "";
                                        objOpportunity.OppComments = ItemMessage;
                                        objOpportunity.Source = WebApiId;
                                        objOpportunity.SourceType = 3;
                                        objOpportunity.TaxOperator = 2; //Remove by default setting of adding sales tax while it comes through marketplace itself
                                        objOpportunity.DealStatus = (long)MOpportunity.EnmDealStatus.DealWon;
                                        objOpportunity.WebApiId = WebApiId;
                                        arrOutPut = objOpportunity.Save();
                                        lngOppId = CCommon.ToLong(arrOutPut[0]);

                                        //Save Source 
                                        objOpportunity.WebApiId = WebApiId;
                                        objOpportunity.vcSource = Source;
                                        objOpportunity.OpportunityId = lngOppId;
                                        objOpportunity.ManageOppLinking();

                                        objOpportunity.CompanyID = lngDivId;

                                        if (Order.ShippingAddress != null)
                                        {
                                            AddressType shippingAddress = Order.ShippingAddress;
                                            if (!string.IsNullOrEmpty(shippingAddress.CompanyName))
                                            {
                                                objOpportunity.BillCompanyName = shippingAddress.CompanyName;
                                            }
                                            if (!string.IsNullOrEmpty(shippingAddress.Name))
                                            {
                                                objOpportunity.AddressName = shippingAddress.Name;
                                            }
                                            if (shippingAddress.Street != null && shippingAddress.Street != "")
                                            {
                                                AddressStreet = shippingAddress.Street;
                                            }
                                            if (shippingAddress.Street1 != null && shippingAddress.Street1 != "")
                                            {
                                                AddressStreet = AddressStreet + " " + shippingAddress.Street1;
                                            }
                                            if (shippingAddress.Street2 != null && shippingAddress.Street2 != "")
                                            {
                                                AddressStreet = AddressStreet + " " + shippingAddress.Street2;
                                            }
                                            if (shippingAddress.CityName != null && shippingAddress.CityName != "")
                                            {
                                                AddressStreet = AddressStreet + " " + shippingAddress.CityName;
                                            }
                                            objContacts.BillStreet = AddressStreet;
                                            objContacts.ShipStreet = AddressStreet;
                                            objOpportunity.BillStreet = AddressStreet;

                                            if (shippingAddress.CityName != null && shippingAddress.CityName != "")
                                            {
                                                objContacts.BillCity = shippingAddress.CityName;
                                                objContacts.ShipCity = shippingAddress.CityName;
                                                objOpportunity.BillCity = shippingAddress.CityName;
                                            }
                                            if (shippingAddress.StateOrProvince != null && shippingAddress.StateOrProvince != "")
                                            {
                                                objCommon.DomainID = DomainID;
                                                objCommon.Mode = 21;
                                                objCommon.Str = shippingAddress.StateOrProvince;
                                                objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                objOpportunity.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                            }
                                            if (shippingAddress.County != null && shippingAddress.County != "")
                                            {
                                                //objContacts.BillCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                                //objContacts.ShipCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                            }
                                            if (shippingAddress.Country != null && CCommon.ToString(shippingAddress.Country) != "")
                                            {
                                                objCommon.DomainID = DomainID;
                                                objCommon.Mode = 22;
                                                objCommon.Str = CCommon.ToString(shippingAddress.Country);
                                                objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                objOpportunity.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                            }
                                            if (shippingAddress.PostalCode != null && shippingAddress.PostalCode != "")
                                            {
                                                objContacts.BillPostal = shippingAddress.PostalCode;
                                                objContacts.ShipPostal = shippingAddress.PostalCode;
                                                objOpportunity.BillPostal = shippingAddress.PostalCode;
                                            }

                                            objOpportunity.Mode = 0;
                                            objOpportunity.UpdateOpportunityAddress();
                                            objOpportunity.Mode = 1;
                                            objOpportunity.UpdateOpportunityAddress();
                                        }


                                        LogMessage = "Order Details is added, Ebay OrderID :  " + Order.OrderID + "  Biz OppID: : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;

                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "eBay:- From ProcessEBayOrders : " + LogMessage);

                                        objWebApi.WebApiId = WebApiId;
                                        objWebApi.DomainID = DomainID;
                                        objWebApi.UserCntID = numRecordOwner;
                                        objWebApi.OppId = lngOppId;
                                        objWebApi.vcAPIOppId = Order.OrderID;
                                        objWebApi.AddAPIopportunity();

                                        objWebApi.API_OrderId = Order.OrderID;
                                        objWebApi.API_OrderStatus = 1;
                                        objWebApi.WebApiOrdDetailId = 1;
                                        objWebApi.ManageAPIOrderDetails();

                                        decimal OrderAmount = OrderTotal;// +ShipCost - dTotDiscount;
                                        //string Reference = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM"); 
                                        string Reference = arrOutPut[1];

                                        //Insert mapping between Biz OrderItems and Amazon OrderItems 
                                        UpdateApiOppItemDetails(DomainID, WebApiId, numRecordOwner, lngOppId, OppBizDocID, objOpportunity.OppRefOrderNo, TransTypColl);
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "eBay:- From ProcessEBayOrders : " + "OppID:" + lngOppId + ",eBayOrderID:" + objOpportunity.OppRefOrderNo + ", Insert mapping between Biz OrderItems and eBay OrderItems,Message:" + ItemMessage + ", OrderTotal:" + OrderTotal);

                                        if (ItemMessage == "")
                                        {
                                            bool IsAuthoritative = false;
                                            BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                            BizCommonFunctions.CreateOppBizDoc(objOppBizDocs, numRecordOwner, DomainID, lngOppId, objOpportunity.OppRefOrderNo, numBizDocId, BizDocStatusId, lngDivId, OrderDate, ShipCost, dTotDiscount, out OppBizDocID, out IsAuthoritative, ExpenseAccountId, SalesTaxRate, OrderAmount, Reference);
                                            //UpdateApiOppItemDetails(DomainID, WebApiId, numRecordOwner, lngOppId, OppBizDocID, objOpportunity.OppRefOrderNo, TransTypColl);
                                        }
                                        else
                                        {
                                            GeneralFunctions.WriteMessage(DomainID, "LOG", "eBay:- From ProcessEBayOrders : " + "BizDoc is not Created for BizOpp Id : " + lngOppId + " due to " + ItemMessage);
                                        }
                                        //}
                                        //else
                                        //{
                                        //    string ErrMessage = "Order report doesnot have sufficient Buyer details to process the E-Bay Order : " + OrderId;
                                        //    GeneralFunctions.WriteMessage(DomainID, "LOG", ErrMessage);
                                        //}
                                    }
                                    else
                                    {
                                        string ErrMessage = "Order report doesnot have any Buyer details to process the E-Bay Order : " + OrderId;
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "eBay:- From ProcessEBayOrders : " + ErrMessage);
                                    }
                                }
                            }
                        }
                        else
                        {
                            string ErrMessage = "Order details for e-Bay Order Id : " + OrderId + " is already exists in BizAutomation";
                            GeneralFunctions.WriteMessage(DomainID, "LOG", "eBay:- From ProcessEBayOrders : " + ErrMessage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = "Error adding Order Details for Order ID " + Order.OrderID + ". " + Environment.NewLine;
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "eBay:- From ProcessEBayOrders : " + ErrorMsg + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "eBay:- From ProcessEBayOrders : " + ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// Gets the Items List and creates a New structure of Item details in order to add in BizDatabase
        /// </summary>
        /// <param name="DomainId">Merchant's BizAutomation Domain Id</param>
        /// <param name="WebApiId">Web API Id</param>
        /// <param name="WareHouseId">Deault warehouse Id</param>
        /// <param name="TransTypColl">E-Bay's Transaction Type that Contatins Order Item Details</param>
        /// <param type="Out Parameter" name="CurrencyID">CurrencyID</param>
        /// <param type="Out Parameter" name="OrderTotal">Order Total</param>
        /// <param type="Out Parameter" name="ShipCost">Shipping Cost</param>
        /// <param type="Out Parameter" name="dTotDiscount">Total Discount</param>
        /// <returns>Error Log Message if E-Bay Order Item is not found in Biz Database(Empty if the E-Bay Order Item is Mapped in Biz Database)</returns>
        private string AddOrderItemDataset(long DomainId, int WebApiId, int WareHouseId, long RecordOwner, long DiscountItemMapping, TransactionTypeCollection TransTypColl, string ShippingServiceSelected, long ShippingServiceItemID, long SalesTaxItemMappingID,
            decimal AmountSaved, out long CurrencyID, out decimal OrderTotal, out decimal ShipCost, out decimal dTotDiscount, out decimal SalesTaxAmount)
        {
            dtItems.Clear();
            dsItems.Clear();
            CCommon objCommon = new CCommon();
            string Message = "";
            DataRow drItem;
            int ItemCount = 0;
            string itemCode = "";
            string ItemType = "";
            string[] itemCodeType;
            WebAPI objWebApi = new WebAPI();
            objWebApi.DomainID = DomainId;
            objWebApi.WebApiId = WebApiId;
            dTotDiscount = 0;
            decimal decDiscount = 0;
            string DiscountDesc = "";
            decimal Tax0 = 0;
            OrderTotal = 0;
            ShipCost = 0;
            CurrencyID = 0;
            SalesTaxAmount = 0;

            foreach (TransactionType TransTyp in TransTypColl)
            {
                try
                {
                    ItemCount += 1;
                    ItemType OrderItem = TransTyp.Item;
                    if (OrderItem != null)
                    {
                        string vcSKU = TransTyp.Variation != null && TransTyp.Variation.SKU != null && !string.IsNullOrEmpty(TransTyp.Variation.SKU.ToString()) ? TransTyp.Variation.SKU.ToString() : OrderItem.SKU;

                        if (vcSKU != "" & vcSKU != null)
                        {
                            objCommon.DomainID = DomainId;
                            objCommon.Mode = 38;
                            objCommon.Str = vcSKU;
                            itemCodeType = CCommon.ToString(objCommon.GetSingleFieldValue()).Split('~');
                            if (itemCodeType.Length > 1)
                            {
                                if (!string.IsNullOrEmpty(itemCodeType[0]))
                                {
                                    itemCode = itemCodeType[0];
                                }
                                if (!string.IsNullOrEmpty(itemCodeType[1]))
                                {
                                    ItemType = BizCommonFunctions.GetItemTypeNameByCharType(itemCodeType[1]);
                                }
                            }
                            if (itemCode == "")
                            {
                                EBayProduct objProduct = new EBayProduct();
                                DataTable dtProductDetails = new DataTable();
                                BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                                DataRow dr = dtProductDetails.NewRow();
                                objProduct = GetProductDetailForItemID(DomainId, WebApiId, OrderItem.ItemID);
                                UserAccess objUserAccess = new UserAccess();
                                objUserAccess.DomainID = DomainId;
                                DataTable dtDomainDetails = objUserAccess.GetDomainDetails();
                                int DefaultIncomeAccID = 0, DefaultCOGSAccID = 0, DefaultAssetAccID = 0;
                                if (dtDomainDetails.Rows.Count > 0)
                                {
                                    DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numIncomeAccID"]);
                                    DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numCOGSAccID"]);
                                    DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numAssetAccID"]);
                                    if (DefaultIncomeAccID == 0 || DefaultCOGSAccID == 0 || DefaultAssetAccID == 0)
                                    {
                                        Message += "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                        string ExcepitonMessage = "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                        throw new Exception("eBay:- From AddOrderItemDataset : " + ExcepitonMessage);
                                    }
                                }
                                dr = FillEbayItemDetails(DomainId, WebApiId, RecordOwner, DefaultIncomeAccID, DefaultCOGSAccID, DefaultAssetAccID, dr, objProduct);

                                string ItemClassificationName = "Ebay_OrderItem_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                                long ItemClassificationID = 0;
                                ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);
                                itemCode = CCommon.ToString(BizCommonFunctions.AddNewItemToBiz(dr, DomainId, WebApiId, RecordOwner, WareHouseId, ItemClassificationID));
                                ItemType = BizCommonFunctions.GetItemTypeNameByCharType("P");
                                WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                                string FilePath = "";

                                CItems objItems = new CItems();
                                objItems.ItemCode = CCommon.ToInteger(itemCode);
                                TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);
                                objItems.ClientZoneOffsetTime = CCommon.ToInteger(diff1.TotalMinutes);
                                DataTable dtItemDetails = objItems.ItemDetails();
                                FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml";

                                if (System.IO.File.Exists(FilePath))
                                {
                                    System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                                    XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                    objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                    objStreamReader.Close();
                                }
                                //objWebAPIItemDetail.DimensionUOM = BizCommonFunctions.GetLengthUOMCode(CCommon.ToString(dr["DimensionUOM"]));
                                //objWebAPIItemDetail.WeightUOM = BizCommonFunctions.GetWeightUOMCode(CCommon.ToString(dr["WeightUOM"]));
                                //objWebAPIItemDetail.Length = CCommon.ToString(dr["fltLength"]);
                                //objWebAPIItemDetail.Height = CCommon.ToString(dr["fltHeight"]);
                                //objWebAPIItemDetail.Width = CCommon.ToString(dr["fltWidth"]);
                                //objWebAPIItemDetail.Weight = CCommon.ToString(dr["fltWeight"]);
                                //objWebAPIItemDetail.WarrantyDescription = CCommon.ToString(dr["Warranty"]);

                                objWebAPIItemDetail.CategoryId = CCommon.ToString(dr["numEBayCategoryID"]);
                                objWebAPIItemDetail.EbayListingStyles = CCommon.ToString(dr["EbayListingType"]);
                                objWebAPIItemDetail.EbayListingDuration = CCommon.ToString(dr["EBayListingDuration"]);
                                objWebAPIItemDetail.StandardShippingCharge = CCommon.ToString(dr["EbayStandardShippingCost"]);
                                objWebAPIItemDetail.ExpressShippingCharge = CCommon.ToString(dr["EbayExpressShippingCost"]);

                                StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml");

                                XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                                Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                                objStreamWriter.Close();

                            }
                            if (itemCode != "")
                            {
                                decDiscount = 0;
                                DiscountDesc = "";
                                drItem = dtItems.NewRow();
                                drItem["numoppitemtCode"] = ItemCount;
                                drItem["numItemCode"] = itemCode;
                                drItem["vcSKU"] = TransTyp.Variation != null && TransTyp.Variation.SKU != null && !string.IsNullOrEmpty(TransTyp.Variation.SKU.ToString()) ? TransTyp.Variation.SKU.ToString() : OrderItem.SKU;

                                if (CCommon.ToInteger(TransTyp.QuantityPurchased) != 0)
                                {
                                    drItem["numUnitHour"] = TransTyp.QuantityPurchased;
                                    if (TransTyp.TransactionPrice != null)
                                    {
                                        AmountType TransTypTransactionPrice = TransTyp.TransactionPrice;
                                        drItem["monPrice"] = CCommon.ToDecimal(TransTypTransactionPrice.Value);
                                        // CCommon.ToInteger(orderItem.Quantity);
                                        OrderTotal += CCommon.ToInteger(TransTyp.QuantityPurchased) * CCommon.ToDecimal(TransTypTransactionPrice.Value);
                                        objCommon.DomainID = DomainId;
                                        objCommon.Mode = 15;
                                        objCommon.Str = CCommon.ToString(TransTypTransactionPrice.currencyID);
                                        CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue());
                                        //drItem["monTotAmount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                        //drItem["monTotAmtBefDiscount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                        drItem["monTotAmount"] = TransTypTransactionPrice.Value * CCommon.ToInteger(TransTyp.QuantityPurchased);

                                    }

                                    if (TransTyp.ActualHandlingCost != null)
                                    {
                                        AmountType TransTypActualHandlingCost = TransTyp.ActualHandlingCost;
                                    }

                                    if (TransTyp.ActualShippingCost != null)
                                    {
                                        AmountType TransTypActualShippingCost = TransTyp.ActualShippingCost;
                                        ShipCost += CCommon.ToDecimal(TransTypActualShippingCost.Value);
                                    }

                                    if (TransTyp.AdjustmentAmount != null)
                                    {
                                        AmountType TransTypAdjustmentAmount = TransTyp.AdjustmentAmount;
                                        decDiscount += CCommon.ToDecimal(TransTypAdjustmentAmount.Value);
                                        DiscountDesc += "AdjustmentAmount fee : " + CCommon.ToString(TransTypAdjustmentAmount.Value) + Environment.NewLine;
                                    }
                                    if (TransTyp.Taxes != null)
                                    {
                                        if (TransTyp.Taxes.TotalTaxAmount != null)
                                        {
                                            AmountType TransTypTaxes = TransTyp.Taxes.TotalTaxAmount;
                                            SalesTaxAmount += CCommon.ToDecimal(TransTypTaxes.Value);
                                        }
                                    }
                                    //if (TransTyp.TotalPrice != null)
                                    //{
                                    //    AmountType TransTypTotalPrice = TransTyp.TotalPrice;
                                    //}

                                    //if (TransTyp.AmountPaid != null)
                                    //{
                                    //    AmountType TransTypTransAmountPaid = TransTyp.AmountPaid;
                                    //}

                                    drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                }

                                drItem["numUOM"] = 0; // get it from item table, in procedure
                                drItem["vcUOMName"] = "";// get it from item table, in procedure
                                drItem["UOMConversionFactor"] = 1.0000;
                                drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                                drItem["vcModelID"] = "";// get it from item table, in procedure
                                drItem["numWarehouseID"] = WareHouseId;
                                if (OrderItem.Title != null && OrderItem.Title != "")
                                {
                                    drItem["vcItemName"] = OrderItem.Title;
                                    if (OrderItem.Title.ToString().Contains("[") && OrderItem.Title.ToString().Contains("]"))
                                    {
                                        drItem["Attributes"] = OrderItem.Title.ToString().Substring(OrderItem.Title.ToString().IndexOf('[') + 1, OrderItem.Title.ToString().IndexOf(']') - OrderItem.Title.ToString().IndexOf('[') - 1);
                                    }
                                    else
                                    {
                                        drItem["Attributes"] = "";
                                    }
                                }
                                else
                                {
                                    drItem["Attributes"] = "";
                                }
                                drItem["Warehouse"] = "";
                                drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                                drItem["ItemType"] = ItemType;// get it from item table, in procedure
                                //drItem["Attributes"] = "";
                                drItem["Op_Flag"] = 1;
                                drItem["bitWorkOrder"] = false;
                                drItem["DropShip"] = false;
                                drItem["fltDiscount"] = decimal.Negate(decDiscount);
                                dTotDiscount += decimal.Negate(decDiscount);
                                //if (fltDiscount > 0)
                                drItem["bitDiscountType"] = 1; //Flat discount
                                //else
                                //    drItem["bitDiscountType"] = false; // Percentage

                                //if (orderItem.ItemTax != null)
                                //{
                                //    CMoney ItemTax = orderItem.ItemTax;
                                //    Tax0 = CCommon.ToDecimal(ItemTax.Amount);
                                //}

                                drItem["Tax0"] = Tax0;
                                if (Tax0 > 0)
                                    drItem["bitTaxable0"] = true;
                                else
                                    drItem["bitTaxable0"] = false;

                                //drItem["Tax0"] = 0;
                                drItem["numVendorWareHouse"] = 0;
                                drItem["numShipmentMethod"] = 0;
                                drItem["numSOVendorId"] = 0;
                                drItem["numProjectID"] = 0;
                                drItem["numProjectStageID"] = 0;
                                drItem["charItemType"] = ""; // should be taken from procedure
                                drItem["numToWarehouseItemID"] = 0;
                                //drItem["Tax41"] = 0;
                                //drItem["bitTaxable41"] = false;
                                //drItem["Tax42"] = 0;
                                //drItem["bitTaxable42"] = false;
                                drItem["Weight"] = ""; // should take from procedure 
                                drItem["WebApiId"] = WebApiId;
                                drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                                dtItems.Rows.Add(drItem);
                            }
                            else
                            {
                                Message += Environment.NewLine + "Order Item " + OrderItem.Title + " (SKU: " + OrderItem.SKU + ") not found in eBay-Biz linking database ";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From AddOrderItemDataset : " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From AddOrderItemDataset : " + ex.StackTrace);
                    throw ex;
                }
            }
            if (ShipCost > 0)
            {
                ShipCost -= AmountSaved;
                OrderTotal += ShipCost;
                drItem = dtItems.NewRow();
                drItem = BizCommonFunctions.GetShippingCostItem(drItem, ShipCost, ShippingServiceItemID, DomainId, WebApiId);
                dtItems.Rows.Add(drItem);
            }
            if (SalesTaxAmount > 0)
            {
                OrderTotal += SalesTaxAmount;
                drItem = dtItems.NewRow();
                drItem = BizCommonFunctions.GetSalesTaxItem(drItem, SalesTaxAmount, SalesTaxItemMappingID, DomainId, WebApiId);
                dtItems.Rows.Add(drItem);
            }

            if (dTotDiscount > 0)
            {
                OrderTotal -= SalesTaxAmount;
                string DiscountItemName = "Total Discount";
                drItem = dtItems.NewRow();
                drItem = BizCommonFunctions.GetDiscountOrderItem(drItem, dTotDiscount, DiscountItemMapping, DomainId, WebApiId, DiscountItemName);
                dtItems.Rows.Add(drItem);
            }
            return Message;
        }

        /// <summary>
        /// Creates API Order Items Entry
        /// </summary>
        /// <param name="DomainId">Merchant's Biz Domain Id</param>
        /// <param name="WebApiId">Web API id</param>
        /// <param name="UserCntId">Default Record Owner's Id</param>
        /// <param name="OppId">Opportunity Id</param>
        /// <param name="OppBizDocId">Opportunity BizDocument Id</param>
        /// <param name="EBayOrderId">E-Bay Order Id</param>
        /// <param name="TransTypColl">E-Bay's Transaction Type that Contatins Order Item Details</param>
        private void UpdateApiOppItemDetails(long DomainId, int WebApiId, long UserCntId, long OppId, long OppBizDocId, string EBayOrderId, TransactionTypeCollection TransTypColl)
        {
            DataTable dtOppItems;
            MOpportunity objOpportunity = new MOpportunity();
            objOpportunity.Mode = 5;
            objOpportunity.DomainID = DomainId;
            objOpportunity.OpportunityId = OppId;

            WebAPI objWebApi = new WebAPI();
            try
            {
                dtOppItems = objOpportunity.GetOrderItems().Tables[0];

                foreach (DataRow dr in dtOppItems.Rows)
                {
                    foreach (TransactionType TransTyp in TransTypColl)
                    {
                        ItemType OrderItem = TransTyp.Item;
                        if (OrderItem != null)
                        {
                            string vcSKU = TransTyp.Variation != null && TransTyp.Variation.SKU != null && !string.IsNullOrEmpty(TransTyp.Variation.SKU.ToString()) ? TransTyp.Variation.SKU.ToString() : OrderItem.SKU;

                            if (vcSKU == CCommon.ToString(dr["vcSKU"]))
                            {
                                objWebApi.DomainID = DomainId;
                                objWebApi.WebApiId = WebApiId;
                                objWebApi.OppId = OppId;
                                objWebApi.OppItemId = CCommon.ToLong(dr["numOppItemtCode"]);
                                objWebApi.vcAPIOppId = EBayOrderId;
                                objWebApi.vcApiOppItemId = TransTyp.OrderLineItemID;
                                objWebApi.RStatus = 0;
                                objWebApi.RecordOwner = UserCntId;
                                objWebApi.ManageApiOrderItems();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Order Processing Functions

        #region Product Operations

        /// <summary>
        /// Adds new Item Details to E-Bay Listings
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="dtNewItems">dtNewItems</param>
        /// <param name="Token">Merchant Token</param>
        /// <param name="eBaySite">eBay Selling Site</param>
        public void NewEBayProduct(long DomainId, int WebApiId, DataTable dtNewItems, string Token, string eBaySite, string PayPalEmailAddress)
        {
            DataRow drItemSpecific;
            string ErrorMessage = "", LogMessage = "";
            CreateItemSpecifics();
            WebAPI objWebAPI = new WebAPI();
            CItems objItems = new CItems();
            DataTable dtItemDetails = new DataTable();
            IeBayCommunicatorClient eBayClient = new IeBayCommunicatorClient();
            string ListingStyle = "";
            string ListingDuration = "";
            string RegionID = "0";
            //string PayPalEmailAddress = "joseph_1340030266_per@bizautomation.com";
            string ApplicationData = "BizAutomation";
            int condition = 1000;

            ApiContext Context = GetContext(Token, eBaySite);
            ItemType item = new ItemType();

            foreach (DataRow dr in dtNewItems.Rows)
            {
                try
                {
                    if (dtItemSpecifics.Rows.Count > 0)
                        dtItemSpecifics.Rows.Clear();

                    #region Add Item
                    if (CCommon.ToString(dr["vcItemName"]).Length > 0 & CCommon.ToString(dr["numItemCode"]).Length > 0 & CCommon.ToString(dr["vcAPIItemId"]).Length <= 1)
                    {
                        objItems.ItemCode = Convert.ToInt32(dr["numItemCode"]);
                        string ListPrice = CCommon.ToString(dr["monListPrice"]);
                        TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);
                        string PrimaryCategoryId = "";

                        objItems.ClientZoneOffsetTime = Convert.ToInt32(diff1.TotalMinutes);
                        dtItemDetails = objItems.ItemDetails();
                        if (CCommon.ToInteger(dtItemDetails.Rows[0]["IsArchieve"]) == 0)
                        {
                            if (CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]).Length > 0 && CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]).Length > 0)
                            {
                                string Weight = "", Length = "", Width = "", Height = "";
                                WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();

                                string WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";

                                if (File.Exists(WebApi_ItemDetails))
                                {
                                    StreamReader objStreamReader = new StreamReader(WebApi_ItemDetails);
                                    XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                    objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                    objStreamReader.Close();
                                }
                                item.SKU = CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]);
                                item.Quantity = CCommon.ToInteger(dr["QtyOnHand"]);
                                item.QuantityAvailable = CCommon.ToInteger(dr["QtyOnHand"]);
                                item.QuantitySpecified = true;
                                
                                if (objWebAPIItemDetail.CategoryId != null && objWebAPIItemDetail.CategoryId != "")
                                {
                                    PrimaryCategoryId = objWebAPIItemDetail.CategoryId;
                                }
                                else
                                {
                                    ErrorMessage = "E-Bay Category Id not found for Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                    throw new Exception("eBay:- From NewEBayProduct : " + ErrorMessage);
                                }

                                if (objWebAPIItemDetail.EbayListingStyles != null && objWebAPIItemDetail.EbayListingStyles != "")
                                {
                                    ListingStyle = objWebAPIItemDetail.EbayListingStyles;
                                }
                                else
                                {
                                    ErrorMessage = "Ebay Listing Styles Id not found for Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                    throw new Exception("eBay:- From NewEBayProduct : " + ErrorMessage);
                                }
                                item.ListingType = GetListingStyle(ListingStyle);

                                if (objWebAPIItemDetail.EbayListingDuration != null && objWebAPIItemDetail.EbayListingDuration != "")
                                {
                                    ListingDuration = objWebAPIItemDetail.EbayListingDuration;
                                }
                                else
                                {
                                    ErrorMessage = "Ebay Listing Duration not found for Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                    throw new Exception("eBay:- From NewEBayProduct : " + ErrorMessage);
                                }

                                item.PrimaryCategory = new CategoryType();
                                item.PrimaryCategory.CategoryID = PrimaryCategoryId;

                                item.Currency = CurrencyUtility.GetDefaultCurrencyCodeType(Context.Site);
                                item.Country = SiteUtility.GetCountryCodeType(Context.Site);

                                item.PaymentMethods = new BuyerPaymentMethodCodeTypeCollection();
                                item.PaymentMethods.AddRange(new BuyerPaymentMethodCodeType[] { BuyerPaymentMethodCodeType.PayPal });
                                if (PayPalEmailAddress.Length > 0)
                                    item.PayPalEmailAddress = PayPalEmailAddress;

                                item.RegionID = RegionID;
                                // Set specified values from the form
                                if (CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]).Length > 78)
                                    item.Title = CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]).Substring(0, 78);
                                else
                                    item.Title = CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                drItemSpecific = dtItemSpecifics.NewRow();
                                drItemSpecific["ItemSpecificsHeader"] = "Title";
                                drItemSpecific["ItemSpecificsValue"] = CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                dtItemSpecifics.Rows.Add(drItemSpecific);

                                // item.Description = (CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]) != "" ? CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]) : "-");
                                item.Description = (CCommon.ToString(dtItemDetails.Rows[0]["vcExtendedDescToAPI"]) != "" ? CCommon.ToString(dtItemDetails.Rows[0]["vcExtendedDescToAPI"]) : "-");

                                drItemSpecific = dtItemSpecifics.NewRow();
                                drItemSpecific["ItemSpecificsHeader"] = "Description";
                                drItemSpecific["ItemSpecificsValue"] = (CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]) != "" ? CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]) : "-");
                                dtItemSpecifics.Rows.Add(drItemSpecific);

                                if (CCommon.ToString(dtItemDetails.Rows[0]["vcManufacturer"]) != null & CCommon.ToString(dtItemDetails.Rows[0]["vcManufacturer"]) != "")
                                {
                                    drItemSpecific = dtItemSpecifics.NewRow();
                                    drItemSpecific["ItemSpecificsHeader"] = "Manufacturer";
                                    drItemSpecific["ItemSpecificsValue"] = CCommon.ToString(dtItemDetails.Rows[0]["vcManufacturer"]);
                                    dtItemSpecifics.Rows.Add(drItemSpecific);
                                }
                                if (item.ListingType == ListingTypeCodeType.Chinese)
                                {
                                    item.Quantity = 1;
                                }
                                if (ListPrice.Length > 0)
                                {
                                    item.StartPrice = new AmountType();
                                    item.StartPrice.currencyID = item.Currency;
                                    item.StartPrice.Value = CCommon.ToDouble(ListPrice);
                                }

                                item.Location = eBaySite;
                                item.CategoryMappingAllowed = true;
                                item.ConditionID = condition;
                                item.ListingDuration = ListingDuration;
                                if (ApplicationData.Length > 0)
                                    item.ApplicationData = ApplicationData;

                                //add handling time
                                item.DispatchTimeMax = 3;
                                //add policy
                                item.ReturnPolicy = GetPolicyForUS();

                                LookupAttributeTypeCollection lookupTypColl = new LookupAttributeTypeCollection();
                                LookupAttributeType lookupattribute = new LookupAttributeType();
                                lookupattribute.Name = "";
                                lookupattribute.Value = "";
                                lookupTypColl.Add(lookupattribute);
                                item.LookupAttributeArray = lookupTypColl;

                                #region Shipping Information
                                
                                //add shipping information
                                decimal StandardShippingCharge;
                                decimal ExpressShippingCharge;

                                if (objWebAPIItemDetail.StandardShippingCharge != null && objWebAPIItemDetail.StandardShippingCharge != "")
                                {
                                    StandardShippingCharge = CCommon.ToDecimal(objWebAPIItemDetail.StandardShippingCharge);
                                }
                                else
                                {
                                    ErrorMessage = "E-Bay Standard Shipping Charge not found for Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                    throw new Exception("eBay:- From NewEBayProduct : " + ErrorMessage);
                                }
                                if (objWebAPIItemDetail.ExpressShippingCharge != null && objWebAPIItemDetail.ExpressShippingCharge != "")
                                {
                                    ExpressShippingCharge = CCommon.ToDecimal(objWebAPIItemDetail.ExpressShippingCharge);
                                }
                                else
                                {
                                    ErrorMessage = "Ebay Express Shipping Charge not found for Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                    throw new Exception("eBay:- From NewEBayProduct : " + ErrorMessage);
                                }
                                item.ShippingDetails = getShippingDetails(StandardShippingCharge, ExpressShippingCharge);
                               
                                #endregion

                                #region Add Item Images
                                // Add Item Images
                                
                                item.PictureDetails = new PictureDetailsType();
                                item.PictureDetails.PhotoDisplay = PhotoDisplayCodeType.SuperSize;
                                string ImagePath = "";
                                List<string> lstItemImages = new List<string>();
                                if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["xmlItemImages"])))
                                {
                                    XmlDocument xmlDocItemImages = new XmlDocument();
                                    xmlDocItemImages.LoadXml(CCommon.ToString(dtItemDetails.Rows[0]["xmlItemImages"]));
                                    XmlNodeList ItemImages = xmlDocItemImages.SelectNodes("Images/ItemImages");
                                    foreach (XmlNode Image in ItemImages)
                                    {
                                        ImagePath = ProductImagePath + DomainId + "\\" + CCommon.ToString(Image.Attributes["vcPathForImage"].Value);
                                        if (File.Exists(ImagePath))
                                        {
                                            lstItemImages.Add(ImagePath);
                                        }
                                    }
                                }

                                #endregion

                                #region Add Item Specifics

                                if (objWebAPIItemDetail.Weight != null && objWebAPIItemDetail.Weight != "")
                                {
                                    Weight = objWebAPIItemDetail.Weight;
                                    drItemSpecific = dtItemSpecifics.NewRow();
                                    drItemSpecific["ItemSpecificsHeader"] = "Weight";
                                    if (objWebAPIItemDetail.WeightUOM != null && objWebAPIItemDetail.WeightUOM != "")
                                    {
                                        objWebAPIItemDetail.Weight += " " + objWebAPIItemDetail.WeightUOM;
                                    }
                                    drItemSpecific["ItemSpecificsValue"] = objWebAPIItemDetail.Weight;
                                    dtItemSpecifics.Rows.Add(drItemSpecific);
                                }

                                if (objWebAPIItemDetail.Length != null && objWebAPIItemDetail.Length != "")
                                {
                                    Length = objWebAPIItemDetail.Length;
                                    drItemSpecific = dtItemSpecifics.NewRow();
                                    drItemSpecific["ItemSpecificsHeader"] = "Length";
                                    if (objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "")
                                    {
                                        objWebAPIItemDetail.Length += " " + objWebAPIItemDetail.DimensionUOM;
                                    }
                                    drItemSpecific["ItemSpecificsValue"] = objWebAPIItemDetail.Length;
                                    dtItemSpecifics.Rows.Add(drItemSpecific);
                                }

                                if (objWebAPIItemDetail.Width != null && objWebAPIItemDetail.Width != "")
                                {
                                    Width = objWebAPIItemDetail.Width;
                                    drItemSpecific = dtItemSpecifics.NewRow();
                                    drItemSpecific["ItemSpecificsHeader"] = "Width";
                                    if (objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "")
                                    {
                                        objWebAPIItemDetail.Width += " " + objWebAPIItemDetail.DimensionUOM;
                                    }
                                    drItemSpecific["ItemSpecificsValue"] = objWebAPIItemDetail.Width;
                                    dtItemSpecifics.Rows.Add(drItemSpecific);
                                }

                                if (objWebAPIItemDetail.Height != null && objWebAPIItemDetail.Height != "")
                                {
                                    Height = objWebAPIItemDetail.Height;
                                    drItemSpecific = dtItemSpecifics.NewRow();
                                    drItemSpecific["ItemSpecificsHeader"] = "Height";
                                    if (objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "")
                                    {
                                        objWebAPIItemDetail.Height += " " + objWebAPIItemDetail.DimensionUOM;
                                    }
                                    drItemSpecific["ItemSpecificsValue"] = objWebAPIItemDetail.Height;
                                    dtItemSpecifics.Rows.Add(drItemSpecific);
                                }

                                if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["fltWeight"])))
                                {
                                    drItemSpecific = dtItemSpecifics.NewRow();
                                    drItemSpecific["ItemSpecificsHeader"] = "Shipping Weight";
                                    drItemSpecific["ItemSpecificsValue"] = CCommon.ToString(dtItemDetails.Rows[0]["fltWeight"]) + " " + ((objWebAPIItemDetail.WeightUOM != null && objWebAPIItemDetail.WeightUOM != "") ? objWebAPIItemDetail.WeightUOM : "");
                                    dtItemSpecifics.Rows.Add(drItemSpecific);
                                }

                                if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["fltLength"])))
                                {
                                    drItemSpecific = dtItemSpecifics.NewRow();
                                    drItemSpecific["ItemSpecificsHeader"] = "Shipping Length";
                                    drItemSpecific["ItemSpecificsValue"] = CCommon.ToString(dtItemDetails.Rows[0]["fltLength"]) + " " + ((objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "") ? objWebAPIItemDetail.DimensionUOM : "");
                                    dtItemSpecifics.Rows.Add(drItemSpecific);
                                }

                                if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["fltWidth"])))
                                {
                                    drItemSpecific = dtItemSpecifics.NewRow();
                                    drItemSpecific["ItemSpecificsHeader"] = "Shipping Width";
                                    drItemSpecific["ItemSpecificsValue"] = CCommon.ToString(dtItemDetails.Rows[0]["fltWidth"]) + " " + ((objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "") ? objWebAPIItemDetail.DimensionUOM : "");
                                    dtItemSpecifics.Rows.Add(drItemSpecific);
                                }

                                if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["fltHeight"])))
                                {
                                    drItemSpecific = dtItemSpecifics.NewRow();
                                    drItemSpecific["ItemSpecificsHeader"] = "Shipping Height";
                                    drItemSpecific["ItemSpecificsValue"] = CCommon.ToString(dtItemDetails.Rows[0]["fltHeight"]) + " " + ((objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "") ? objWebAPIItemDetail.DimensionUOM : "");
                                    dtItemSpecifics.Rows.Add(drItemSpecific);
                                }
                                item.ItemSpecifics = GetItemSpecifics();

                                #endregion

                                #region Add Variation Details
                                //Add variation details of Item if Item has ItemgroupID available.
                                if (CCommon.ToInteger(dtItemDetails.Rows[0]["numItemGroup"]) > 0)
                                {
                                    VariationCollection vCollection = new VariationCollection();
                                    VariationDetails vd = new VariationDetails();

                                    //Get all Item Attributes for an item to create Item Variation Specifics block
                                    DataSet dsAttr = new DataSet();
                                    objItems.ItemCode = CCommon.ToInteger(dtItemDetails.Rows[0]["numItemCode"]);
                                    objItems.WebApiId = (int)MarketplaceService.Service1.WebAPIList.EBayUS;
                                    objItems.DomainID = CCommon.ToInteger(dr["numDomainID"]);
                                    dsAttr = objItems.GetItemAttributesForAPI();

                                    if (dsAttr != null && dsAttr.Tables.Count > 0)
                                    {
                                        dtItemVariationSpecifics = dsAttr.Tables[0];
                                        dtItemVariations = dsAttr.Tables[1];

                                        VariationsType vts = new VariationsType();
                                        NameValueListTypeCollection objNVLTC;
                                        foreach (DataRow drVar in dtItemVariationSpecifics.Rows)
                                        {
                                            NameValueListType objNameValLstTyp = new NameValueListType();
                                            objNameValLstTyp.Name = CCommon.ToString(drVar["Name"]);

                                            //Do not add variation if any of the value is blank or "-"
                                            if (CCommon.ToString(drVar["Value"]) == "" || CCommon.ToString(drVar["Value"]) == "-")
                                                continue;

                                            StringCollection strColl = new StringCollection();
                                            strColl.AddRange(CCommon.ToString(drVar["Value"]).Split(','));

                                            objNameValLstTyp.Value = strColl;
                                            objNameValLstTyp.SourceSpecified = false;
                                            if (vts.VariationSpecificsSet == null)
                                            {
                                                objNVLTC = new NameValueListTypeCollection();
                                                objNVLTC.Add(objNameValLstTyp);
                                                vts.VariationSpecificsSet = objNVLTC;
                                            }
                                            else
                                            {
                                                if (!vts.VariationSpecificsSet.Contains(objNameValLstTyp))
                                                    vts.VariationSpecificsSet.Add(objNameValLstTyp);
                                            }
                                        }

                                        VariationType vt;
                                        foreach (DataRow drVar in dtItemVariations.Rows)
                                        {
                                            if (CCommon.ToDouble(drVar["OnHand"]) == 0)
                                                continue;

                                            vt = new VariationType();
                                            vt.Quantity = CCommon.ToInteger(drVar["OnHand"]);
                                            vt.SKU = CCommon.ToString(drVar["SKU"]);

                                            vt.StartPrice = new AmountType();
                                            vt.StartPrice.currencyID = item.Currency;
                                            vt.StartPrice.Value = CCommon.ToDouble(ListPrice);

                                            foreach (DataColumn dc in dtItemVariations.Columns)
                                            {
                                                if (dc.ColumnName.Contains("_c"))
                                                {
                                                    NameValueListType objNameValLstTyp = new NameValueListType();
                                                    objNameValLstTyp.Name = CCommon.ToString(dc.ColumnName).Replace("_c", "");

                                                    //Do not add variation if any of the value is blank or "-"
                                                    if (CCommon.ToString(drVar[dc.ColumnName]) == "" || CCommon.ToString(drVar[dc.ColumnName]) == "-")
                                                        continue;

                                                    StringCollection strColl = new StringCollection();
                                                    strColl.AddRange(CCommon.ToString(drVar[dc.ColumnName]).Split(','));

                                                    objNameValLstTyp.Value = strColl; 
                                                    objNameValLstTyp.SourceSpecified = false;
                                                    if (vt.VariationSpecifics == null)
                                                    {
                                                        objNVLTC = new NameValueListTypeCollection();
                                                        objNVLTC.Add(objNameValLstTyp);
                                                        vt.VariationSpecifics = objNVLTC;
                                                    }
                                                    else
                                                    {
                                                        if (!vt.VariationSpecifics.Contains(objNameValLstTyp))
                                                            vt.VariationSpecifics.Add(objNameValLstTyp);
                                                    }
                                                }
                                            }

                                            if (vts.Variation == null)
                                            {
                                                VariationTypeCollection vtc = new VariationTypeCollection();
                                                vtc.Add(vt);
                                                vts.Variation = vtc;
                                            }
                                            else
                                                vts.Variation.Add(vt);
                                        }

                                        item.Variations = vts;
                                    }
                                }

                                #endregion

                                MemoryStream msItem = GeneralFunctions.SerializeToStream(item);

                                #region Create XML Request File from MemoryStream if debug mode is "on"(1).
                                if (IsDebugMode == true)
                                {
                                    string strFileName = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_AddItemRequest_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";
                                    FileStream file = new FileStream(strFileName, FileMode.Create, FileAccess.Write);
                                    msItem.WriteTo(file);
                                    file.Close();
                                }
                                #endregion

                                eBayClient = new IeBayCommunicatorClient();
                                string ItemId = eBayClient.AddNewItem(Context, msItem, lstItemImages.ToArray());
                                eBayClient.Close();
                                LogMessage = "Item Details for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]) + ", E-Bay Item Code " + ItemId + " is Added in E-BayListings";
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "eBay:- From NewEBayProduct : " + LogMessage);
                                if (ItemId != null & ItemId != "")
                                {
                                    objWebAPI.UserCntID = GetRoutingRulesUserCntID(DomainId, "", "");
                                    objWebAPI.ItemCode = CCommon.ToLong(dr["numItemCode"]);
                                    objWebAPI.Product_Id = ItemId;
                                    objWebAPI.DomainID = DomainId;
                                    objWebAPI.WebApiId = WebApiId;
                                    objWebAPI.SKU = CCommon.ToString(item.SKU);
                                    objWebAPI.AddItemAPILinking();
                                    LogMessage = "E-Bay ItemID " + ItemId + " is mapped in (BizDatabase) ItemAPI Details for  Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                    GeneralFunctions.WriteMessage(DomainId, "LOG", "eBay:- From NewEBayProduct : " + LogMessage);
                                }
                                else
                                {

                                }
                            }
                            else
                            {
                                LogMessage = "Sorry.,SKU and Item Title is Mandatory to add an Item in E-Bay Listings. Verify Details of Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "eBay:- From NewEBayProduct : " + LogMessage);
                            }
                        }
                    }
#endregion

                    #region Revise Item
                    else if (CCommon.ToString(dr["vcItemName"]).Length > 0 & CCommon.ToString(dr["numItemCode"]).Length > 0 & CCommon.ToString(dr["vcAPIItemId"]).Length > 1)
                    {
                        objItems.ItemCode = Convert.ToInt32(dr["numItemCode"]);
                        string ListPrice = CCommon.ToString(dr["monListPrice"]);
                        TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);
                        string PrimaryCategoryId = "";
                        ItemType fetchedItem = null;
                        objItems.ClientZoneOffsetTime = Convert.ToInt32(diff1.TotalMinutes);
                        dtItemDetails = objItems.ItemDetails();
                        if (CCommon.ToInteger(dtItemDetails.Rows[0]["IsArchieve"]) == 0)
                        {
                            if (CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]).Length > 0 & CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]).Length > 0 & CCommon.ToInteger(dtItemDetails.Rows[0]["IsArchieve"]) == 0)
                            {
                                string Weight = "", Length = "", Width = "", Height = "";
                                WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();

                                string WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";

                                if (File.Exists(WebApi_ItemDetails))
                                {
                                    StreamReader objStreamReader = new StreamReader(WebApi_ItemDetails);
                                    XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                    objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                    objStreamReader.Close();
                                    //GeneralFunctions.DeSerializeFileToObject<WebAPIItemDetail>(out objWebAPIItemDetail, WebApi_ItemDetails);
                                }
                                item.ItemID = CCommon.ToString(dr["vcAPIItemId"]);
                                item.SKU = CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]);
                                item.Quantity = CCommon.ToInteger(dr["QtyOnHand"]);
                                item.QuantityAvailable = CCommon.ToInteger(dr["QtyOnHand"]);
                                item.QuantitySpecified = true;

                                if (objWebAPIItemDetail.CategoryId != null && objWebAPIItemDetail.CategoryId != "")
                                {
                                    PrimaryCategoryId = objWebAPIItemDetail.CategoryId;
                                }
                                else
                                {
                                    ErrorMessage = "E-Bay Category Id not found for Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                    throw new Exception("eBay:- From NewEBayProduct : " + ErrorMessage);
                                }
                                if (objWebAPIItemDetail.EbayListingStyles != null && objWebAPIItemDetail.EbayListingStyles != "")
                                {
                                    ListingStyle = objWebAPIItemDetail.EbayListingStyles;
                                }
                                else
                                {
                                    ErrorMessage = "Ebay Listing Styles Id not found for Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                    throw new Exception("eBay:- From NewEBayProduct : " + ErrorMessage);
                                }
                                item.PrimaryCategory = new CategoryType();
                                item.PrimaryCategory.CategoryID = PrimaryCategoryId;

                                if (objWebAPIItemDetail.EbayListingDuration != null && objWebAPIItemDetail.EbayListingDuration != "")
                                {
                                    ListingDuration = objWebAPIItemDetail.EbayListingDuration;
                                }
                                else
                                {
                                    ErrorMessage = "Ebay Listing Duration not found for Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                    throw new Exception("eBay:- From NewEBayProduct : " + ErrorMessage);
                                }
                                item.Currency = CurrencyUtility.GetDefaultCurrencyCodeType(Context.Site);
                                item.Country = SiteUtility.GetCountryCodeType(Context.Site);

                                // Set UP Defaults
                                item.ListingType = GetListingStyle(ListingStyle);
                                if (item.ListingType == ListingTypeCodeType.Chinese)
                                {
                                    item.Quantity = 1;
                                }
                                else if (ListPrice.Length > 0)
                                {
                                    item.StartPrice = new AmountType();
                                    item.StartPrice.currencyID = item.Currency;
                                    item.StartPrice.Value = CCommon.ToDouble(ListPrice);
                                }

                                //item.PaymentMethods = new BuyerPaymentMethodCodeTypeCollection();
                                //item.PaymentMethods.AddRange(new BuyerPaymentMethodCodeType[] { BuyerPaymentMethodCodeType.PayPal });
                                //if (PayPalEmailAddress.Length > 0)
                                //    item.PayPalEmailAddress = PayPalEmailAddress;

                                //item.RegionID = RegionID;
                                // Set specified values from the form
                                if (CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]).Length > 78)
                                    item.Title = CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]).Substring(0, 78);
                                else
                                    item.Title = CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                //item.Description = (CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]) != "" ? CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]) : "-");
                                item.Description = (CCommon.ToString(dtItemDetails.Rows[0]["vcExtendedDescToAPI"]) != "" ? CCommon.ToString(dtItemDetails.Rows[0]["vcExtendedDescToAPI"]) : "-");

                                //drItemSpecific = dtItemSpecifics.NewRow();
                                //drItemSpecific["ItemSpecificsHeader"] = "Title";
                                //drItemSpecific["ItemSpecificsValue"] = CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                //dtItemSpecifics.Rows.Add(drItemSpecific);

                                //drItemSpecific = dtItemSpecifics.NewRow();
                                //drItemSpecific["ItemSpecificsHeader"] = "Description";
                                //drItemSpecific["ItemSpecificsValue"] = (CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]) != "" ? CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]) : "-");
                                //dtItemSpecifics.Rows.Add(drItemSpecific);
                                //if (CCommon.ToString(dtItemDetails.Rows[0]["vcManufacturer"]) != null & CCommon.ToString(dtItemDetails.Rows[0]["vcManufacturer"]) != "")
                                //{
                                //    drItemSpecific = dtItemSpecifics.NewRow();
                                //    drItemSpecific["ItemSpecificsHeader"] = "Manufacturer";
                                //    drItemSpecific["ItemSpecificsValue"] = dtItemDetails.Rows[0]["vcManufacturer"].ToString();
                                //    dtItemSpecifics.Rows.Add(drItemSpecific);
                                //}

                                item.Location = eBaySite;
                                item.CategoryMappingAllowed = true;
                                //item.ConditionID = condition;
                                item.ListingDuration = ListingDuration;
                                if (ApplicationData.Length > 0)
                                    item.ApplicationData = ApplicationData;

                                //GetItemCall apiGetItemcall = new GetItemCall(Context);
                                //apiGetItemcall.DetailLevelList.Add(DetailLevelCodeType.ReturnAll);
                                //fetchedItem = apiGetItemcall.GetItem(item.ItemID);

                                //eBayClient = new IeBayCommunicatorClient();
                                //fetchedItem = eBayClient.GetItemDetailsForItemID(Context, item.ItemID);
                                //eBayClient.Close();

                                //add shipping information
                                //if (fetchedItem.ListingDetails.EndTime > DateTime.Now.AddHours(12))
                                //{
                                //    decimal StandardShippingCharge;
                                //    decimal ExpressShippingCharge;
                                //    if (objWebAPIItemDetail.StandardShippingCharge != null && objWebAPIItemDetail.StandardShippingCharge != "")
                                //    {
                                //        StandardShippingCharge = CCommon.ToDecimal(objWebAPIItemDetail.StandardShippingCharge);
                                //    }
                                //    else
                                //    {
                                //        ErrorMessage = "Ebay Standard Shipping Charge not found for Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                //        throw new Exception(ErrorMessage);
                                //    }
                                //    if (objWebAPIItemDetail.ExpressShippingCharge != null && objWebAPIItemDetail.ExpressShippingCharge != "")
                                //    {
                                //        ExpressShippingCharge = CCommon.ToDecimal(objWebAPIItemDetail.ExpressShippingCharge);
                                //    }
                                //    else
                                //    {
                                //        ErrorMessage = "E-Bay Express Shipping Charge not found for Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                //        throw new Exception(ErrorMessage);
                                //    }

                                //    item.ShippingDetails = getShippingDetails(StandardShippingCharge, ExpressShippingCharge);
                                //}

                                //add handling time
                                //item.DispatchTimeMax = 3;

                                LookupAttributeTypeCollection lookupTypColl = new LookupAttributeTypeCollection();
                                LookupAttributeType lookupattribute = new LookupAttributeType();
                                lookupattribute.Name = "";
                                lookupattribute.Value = "";
                                lookupTypColl.Add(lookupattribute);
                                item.LookupAttributeArray = lookupTypColl;
                                item.PictureDetails = new PictureDetailsType();
                                item.PictureDetails.PhotoDisplay = PhotoDisplayCodeType.SuperSize;
                                List<string> lstImagePath = new List<string>();
                                string ImagePath = "";
                                if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["xmlItemImages"])))
                                {
                                    XmlDocument xmlDocItemImages = new XmlDocument();
                                    xmlDocItemImages.LoadXml(CCommon.ToString(dtItemDetails.Rows[0]["xmlItemImages"]));
                                    XmlNodeList ItemImages = xmlDocItemImages.SelectNodes("Images/ItemImages");
                                    if (ItemImages != null && ItemImages.Count > 0)
                                    {
                                        foreach (XmlNode Image in ItemImages)
                                        {
                                            ImagePath = ProductImagePath + DomainId + "\\" + CCommon.ToString(Image.Attributes["vcPathForImage"].Value);
                                            if (File.Exists(ImagePath))
                                            {
                                                lstImagePath.Add(ImagePath);
                                            }
                                        }
                                    }
                                }

                                #region Commented Item Specific code
                                //if (objWebAPIItemDetail.Weight != null && objWebAPIItemDetail.Weight != "")
                                //{
                                //    Weight = objWebAPIItemDetail.Weight;
                                //    drItemSpecific = dtItemSpecifics.NewRow();
                                //    drItemSpecific["ItemSpecificsHeader"] = "Weight";
                                //    if (objWebAPIItemDetail.WeightUOM != null && objWebAPIItemDetail.WeightUOM != "")
                                //    {
                                //        objWebAPIItemDetail.Weight += " " + objWebAPIItemDetail.WeightUOM;
                                //    }
                                //    drItemSpecific["ItemSpecificsValue"] = objWebAPIItemDetail.Weight;
                                //    dtItemSpecifics.Rows.Add(drItemSpecific);
                                //}

                                //if (objWebAPIItemDetail.Length != null && objWebAPIItemDetail.Length != "")
                                //{
                                //    Length = objWebAPIItemDetail.Length;
                                //    drItemSpecific = dtItemSpecifics.NewRow();
                                //    drItemSpecific["ItemSpecificsHeader"] = "Length";
                                //    if (objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "")
                                //    {
                                //        objWebAPIItemDetail.Length += " " + objWebAPIItemDetail.DimensionUOM;
                                //    }
                                //    drItemSpecific["ItemSpecificsValue"] = objWebAPIItemDetail.Length;
                                //    dtItemSpecifics.Rows.Add(drItemSpecific);
                                //}

                                //if (objWebAPIItemDetail.Width != null && objWebAPIItemDetail.Width != "")
                                //{
                                //    Width = objWebAPIItemDetail.Width;
                                //    drItemSpecific = dtItemSpecifics.NewRow();
                                //    drItemSpecific["ItemSpecificsHeader"] = "Width";
                                //    if (objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "")
                                //    {
                                //        objWebAPIItemDetail.Width += " " + objWebAPIItemDetail.DimensionUOM;
                                //    }
                                //    drItemSpecific["ItemSpecificsValue"] = objWebAPIItemDetail.Width;
                                //    dtItemSpecifics.Rows.Add(drItemSpecific);
                                //}

                                //if (objWebAPIItemDetail.Height != null && objWebAPIItemDetail.Height != "")
                                //{
                                //    Height = objWebAPIItemDetail.Height;
                                //    drItemSpecific = dtItemSpecifics.NewRow();
                                //    drItemSpecific["ItemSpecificsHeader"] = "Height";
                                //    if (objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "")
                                //    {
                                //        objWebAPIItemDetail.Height += " " + objWebAPIItemDetail.DimensionUOM;
                                //    }
                                //    drItemSpecific["ItemSpecificsValue"] = objWebAPIItemDetail.Height;
                                //    dtItemSpecifics.Rows.Add(drItemSpecific);
                                //}

                                //if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["fltWeight"])))
                                //{
                                //    drItemSpecific = dtItemSpecifics.NewRow();
                                //    drItemSpecific["ItemSpecificsHeader"] = "Shipping Weight";
                                //    drItemSpecific["ItemSpecificsValue"] = CCommon.ToString(dtItemDetails.Rows[0]["fltWeight"]) + " " + ((objWebAPIItemDetail.WeightUOM != null && objWebAPIItemDetail.WeightUOM != "") ? objWebAPIItemDetail.WeightUOM : "");
                                //    dtItemSpecifics.Rows.Add(drItemSpecific);
                                //}

                                //if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["fltLength"])))
                                //{
                                //    drItemSpecific = dtItemSpecifics.NewRow();
                                //    drItemSpecific["ItemSpecificsHeader"] = "Shipping Length";
                                //    drItemSpecific["ItemSpecificsValue"] = CCommon.ToString(dtItemDetails.Rows[0]["fltLength"]) + " " + ((objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "") ? objWebAPIItemDetail.DimensionUOM : "");
                                //    dtItemSpecifics.Rows.Add(drItemSpecific);
                                //}

                                //if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["fltWidth"])))
                                //{
                                //    drItemSpecific = dtItemSpecifics.NewRow();
                                //    drItemSpecific["ItemSpecificsHeader"] = "Shipping Width";
                                //    drItemSpecific["ItemSpecificsValue"] = CCommon.ToString(dtItemDetails.Rows[0]["fltWidth"]) + " " + ((objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "") ? objWebAPIItemDetail.DimensionUOM : "");
                                //    dtItemSpecifics.Rows.Add(drItemSpecific);
                                //}

                                //if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["fltHeight"])))
                                //{
                                //    drItemSpecific = dtItemSpecifics.NewRow();
                                //    drItemSpecific["ItemSpecificsHeader"] = "Shipping Height";
                                //    drItemSpecific["ItemSpecificsValue"] = CCommon.ToString(dtItemDetails.Rows[0]["fltHeight"]) + " " + ((objWebAPIItemDetail.DimensionUOM != null && objWebAPIItemDetail.DimensionUOM != "") ? objWebAPIItemDetail.DimensionUOM : "");
                                //    dtItemSpecifics.Rows.Add(drItemSpecific);
                                //}
                                //item.ItemSpecifics = GetItemSpecifics();
                                #endregion

                                MemoryStream msItem = GeneralFunctions.SerializeToStream(item);

                                #region Create XML Request File from MemoryStream
                                if (IsDebugMode == true)
                                {
                                    string strFileName = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_ReviseRequest_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";
                                    FileStream file = new FileStream(strFileName, FileMode.Create, FileAccess.Write);
                                    msItem.WriteTo(file);
                                    file.Close();
                                }
                                #endregion
                                
                                StringCollection DeletedFields = new StringCollection();
                                eBayClient = new IeBayCommunicatorClient();
                                string ItemId = eBayClient.ReviseItem(Context, msItem, lstImagePath.ToArray(), DeletedFields.ToArray());
                                eBayClient.Close();

                                LogMessage = "Item Details for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]) + ", E-Bay Item Code " + ItemId + " is Updated in E-BayListings";
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "eBay:- From NewEBayProduct : " + LogMessage);
                            }
                            else
                            {
                                LogMessage = "Sorry.,SKU and Item Title is Mandatory to add an Item in E-Bay Listings. Verify Details of Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "eBay:- From NewEBayProduct : " + LogMessage);
                            }
                        }
                    }
                    #endregion

                }
                catch (FaultException<error> ex)
                {
                    error er = ex.Detail;
                    string ErrMessage = "Error received from E-Bay Service : " + er.msg;
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From NewEBayProduct : " + ErrMessage);
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From NewEBayProduct : " + er.request);
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From NewEBayProduct : " + ex.Message);
                    //throw ex;
                }
            }

        }

        /// <summary>
        /// Creates E-Bay Item Specific details Collection Datatable Structure
        /// </summary>
        private void CreateItemSpecifics()
        {
            try
            {
                if (dtItemSpecifics.Columns.Count == 0)
                {
                    dtItemSpecifics.Columns.Add("ItemSpecificsHeader");
                    dtItemSpecifics.Columns.Add("ItemSpecificsValue");
                    dtItemSpecifics.TableName = "ItemSpecifics";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Gets Item Specific Detail collection from datatable and returns as e-Bay Name Value Type 
        /// </summary>
        /// <returns></returns>
        public NameValueListTypeCollection GetItemSpecifics()
        {
            NameValueListTypeCollection objNamevalListColl = new NameValueListTypeCollection();
            foreach (DataRow dr in dtItemSpecifics.Rows)
            {
                NameValueListType objNameValLstTyp = new NameValueListType();
                objNameValLstTyp.Name = CCommon.ToString(dr["ItemSpecificsHeader"]);
                objNameValLstTyp.Value = new StringCollection(new string[] { CCommon.ToString(dr["ItemSpecificsValue"]) });
                objNameValLstTyp.SourceSpecified = false;
                objNamevalListColl.Add(objNameValLstTyp);
            }
            return objNamevalListColl;
        }

        /// <summary>
        /// set shipping information
        /// </summary>
        /// <returns>ShippingDetailsType</returns>
        private ShippingDetailsType getShippingDetails(decimal StdShippingCost, decimal ExpShippingCost)
        {
            try
            {
                // Shipping details.
                ShippingDetailsType sd = new ShippingDetailsType();
                //SalesTaxType salesTax = new SalesTaxType();
                //salesTax.SalesTaxPercent = 0.0825f;
                //salesTax.SalesTaxState = "CA";
                //sd.ApplyShippingDiscount = true;
                AmountType at = new AmountType();
                //at.Value = 2.8;
                //at.currencyID = CurrencyCodeType.USD;
                //sd.InsuranceFee = at;
                //sd.InsuranceOption = InsuranceOptionCodeType.Optional;
                //sd.PaymentInstructions = "eBay DotNet SDK test instruction.";

                // Set calculated shipping.
                sd.ShippingType = ShippingTypeCodeType.Flat;
                //
                ShippingServiceOptionsType st1 = new ShippingServiceOptionsType();
                st1.ShippingService = CCommon.ToString(ShippingServiceCodeType.ShippingMethodStandard);
                at = new AmountType();
                at.Value = 0.0;
                at.currencyID = CurrencyCodeType.USD;
                st1.ShippingServiceAdditionalCost = at;
                at = new AmountType();
                at.Value = (double)StdShippingCost;
                at.currencyID = CurrencyCodeType.USD;
                st1.ShippingServiceCost = at;
                st1.ShippingServicePriority = 1;
                //at = new AmountType();
                //at.Value = 1.0;
                //at.currencyID = CurrencyCodeType.USD;
                //st1.ShippingInsuranceCost = at;

                ShippingServiceOptionsType st2 = new ShippingServiceOptionsType();
                st2.ExpeditedService = true;
                st2.ShippingService = CCommon.ToString(ShippingServiceCodeType.ShippingMethodExpress);
                at = new AmountType();
                at.Value = 0.0;
                at.currencyID = CurrencyCodeType.USD;
                st2.ShippingServiceAdditionalCost = at;
                at = new AmountType();
                at.Value = (double)ExpShippingCost; ;
                at.currencyID = CurrencyCodeType.USD;
                st2.ShippingServiceCost = at;
                st2.ShippingServicePriority = 2;
                //at = new AmountType();
                //at.Value = 1.0;
                //at.currencyID = CurrencyCodeType.USD;
                //st2.ShippingInsuranceCost = at;

                //ShippingServiceOptionsType st3 = new ShippingServiceOptionsType();
                //st3.ExpeditedService = false;
                //st3.ShippingService = CCommon.ToString(ShippingServiceCodeType.Other);
                //at = new AmountType();
                //at.Value = 0.0;
                //at.currencyID = CurrencyCodeType.USD;
                //st2.ShippingServiceAdditionalCost = at;
                //at = new AmountType();
                //at.Value = 0.0 ;
                //at.currencyID = CurrencyCodeType.USD;
                //st3.ShippingServiceCost = at;
                //st3.ShippingServicePriority = 3;

                sd.ShippingServiceOptions = new ShippingServiceOptionsTypeCollection(new ShippingServiceOptionsType[] { st1, st2 });

                return sd;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// get a policy for us site only.
        /// </summary>
        /// <returns></returns>
        public static ReturnPolicyType GetPolicyForUS()
        {
            try
            {
                ReturnPolicyType policy = new ReturnPolicyType();
                policy.Refund = "MoneyBack";
                policy.ReturnsWithinOption = "Days_14";
                policy.ReturnsAcceptedOption = "ReturnsAccepted";
                policy.ShippingCostPaidByOption = "Buyer";

                return policy;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Imports (Add/Update) List of Products into BizAutomation from provided E-Bay Seller  Account 
        /// </summary>
        /// <param name="DomainId">Domain ID</param>
        /// <param name="WebApiId">Web Api ID</param>
        /// <param name="RecordOwner">Record Owner</param>
        /// <param name="WareHouseID">WareHouse ID</param>
        /// <param name="Token">Merchant Token</param>
        /// <param name="eBaySite">eBaySite Code</param>
        public void GetEbayProductListings(long DomainId, int WebApiId, long RecordOwner, long UserContactID, int WareHouseID, string Token, string eBaySite, int ListingReqType)
        {
            string LogMessage = "";
            EBayProductList objEBayProductList = new EBayProductList();
            DataTable dtProductDetails = new DataTable();
            try
            {
                ApiContext Context = GetContext(Token, eBaySite);
                IeBayCommunicatorClient eBayClient = new IeBayCommunicatorClient();
                List<EBayProduct> lstEBayProduct = new List<EBayProduct>();
                objEBayProductList = eBayClient.GetMerchantProductListings(Context, ListingReqType);

                lstEBayProduct = objEBayProductList.LstEBayProduct.OfType<EBayProduct>().ToList();
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = DomainId;
                DataTable dtDomainDetails = objUserAccess.GetDomainDetails();
                int DefaultIncomeAccID = 0, DefaultCOGSAccID = 0, DefaultAssetAccID = 0;

                if (dtDomainDetails.Rows.Count > 0)
                {
                    DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numIncomeAccID"]);
                    DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numCOGSAccID"]);
                    DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numAssetAccID"]);
                    if (DefaultIncomeAccID == 0 || DefaultCOGSAccID == 0 || DefaultAssetAccID == 0)
                    {
                        string ExcepitonMessage = "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                        throw new Exception("eBay:- From GetEbayProductListings : " + ExcepitonMessage);
                    }
                }

                long ItemGroupID = 0;
                DataTable dtCustomAttributes = new DataTable();
                dtCustomAttributes.Columns.Add("EbayItemID");
                dtCustomAttributes.Columns.Add("ItemName");
                dtCustomAttributes.Columns.Add("WListPrice");
                dtCustomAttributes.Columns.Add("OnHand");
                dtCustomAttributes.Columns.Add("ReOrder");
                dtCustomAttributes.Columns.Add("WSKU");
                dtCustomAttributes.Columns.Add("WBarCode");
                dtCustomAttributes.Columns.Add("strXML");

                dtCustomAttributes.AcceptChanges();

                foreach (EBayProduct objProduct in lstEBayProduct)
                {
                    try
                    {
                        BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                        DataRow dr = dtProductDetails.NewRow();
                        dr = FillEbayItemDetails(DomainId, WebApiId, RecordOwner, DefaultIncomeAccID, DefaultCOGSAccID, DefaultAssetAccID, dr, objProduct);
                        dtProductDetails.Rows.Add(dr);

                        // Check whether item has variations or not. If there is, then create respective attributes / custom fields for particular item.
                        // Check Item Variations - Which will work as Item Attributes fields

                        VariationCollection vCollection = new VariationCollection();
                        vCollection = objProduct.ItemVariations;

                        if (vCollection != null)
                        {
                            DataSet dsCustom = new DataSet();
                            DataTable dtCFields = new DataTable();
                            DataTable dtCFValues = new DataTable();

                            WebAPI objWebAPI = new WebAPI();
                            dsCustom = objWebAPI.GetCflList(DomainId, 9);

                            dtCFields = dsCustom.Tables[0];
                            dtCFValues = dsCustom.Tables[1];

                            string strItemGroupName = "";
                            if (dtCFields != null && dtCFields.Rows.Count > 0)
                            {
                                foreach (Variations objVarie in vCollection.VariationList)
                                {
                                    long ListID = 0;
                                    long CustomFieldID = 0;

                                    //if (dtCFields.Select("Fld_label = '" + objVarie.Name.ToString().ToLower() + "'").Length == 0)
                                    if (dtCFields.Select(String.Format("fld_label = '{0}'", objVarie.Name.ToString().ToLower().Replace("'", "''"))).Length == 0)
                                    {
                                        try
                                        {
                                            CustomFields objCusField = new CustomFields();
                                            objCusField.locId = 9;
                                            objCusField.TabId = 0;
                                            objCusField.UserId = UserContactID;
                                            objCusField.FieldLabel = objVarie.Name.ToString();
                                            objCusField.FieldType = "SelectBox";
                                            objCusField.CusFldId = ListID;
                                            objCusField.DomainID = DomainId;
                                            objCusField.URL = "";
                                            objCusField.vcToolTip = objVarie.Name.ToString();
                                            objCusField.CflSave();
                                            CustomFieldID = objCusField.CusFldId;
                                            ListID = objCusField.ListId;

                                            if (strItemGroupName.Contains(CCommon.ToString(objVarie.Name)) == false)
                                            {
                                                strItemGroupName = strItemGroupName + "_" + CCommon.ToString(objVarie.Name);
                                            }

                                            if (CustomFieldID > 0 && ListID > 0)
                                            {
                                                DataRow drFields = dtCFields.NewRow();
                                                drFields["fld_id"] = CustomFieldID;
                                                drFields["Fld_label"] = objVarie.Name.ToString();
                                                drFields["TabName"] = "";
                                                drFields["loc_name"] = "";
                                                drFields["bitIsRequired"] = false;
                                                drFields["bitIsEmail"] = false;
                                                drFields["bitIsAlphaNumeric"] = false;
                                                drFields["bitIsNumeric"] = false;
                                                drFields["bitIsLengthValidation"] = false;
                                                drFields["ListID"] = ListID;
                                                dtCFields.Rows.Add(drFields);
                                                dtCFields.AcceptChanges();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            LogMessage = "Error occurred while adding Item Attribute named : " + objVarie.Name.ToString() + ". Details of error :" + ex.Message.ToString();
                                            GeneralFunctions.WriteMessage(DomainId, "LOG", "eBay:- From GetEBayProductListings : " + LogMessage);
                                        }
                                    }
                                    else
                                    {
                                        ListID = CCommon.ToLong(dtCFields.Select(String.Format("fld_label = '{0}'", objVarie.Name.ToString().ToLower().Replace("'", "''")))[0]["ListID"]);
                                        if (strItemGroupName.Contains(CCommon.ToString(objVarie.Name)) == false)
                                        {
                                            strItemGroupName = strItemGroupName + "_" + CCommon.ToString(objVarie.Name);
                                        }
                                    }

                                    //if (dtCFValues.Select("ListID = " + ListID + " AND vcData = '" + objVarie.Value.ToString().ToLower() + "'").Length == 0)
                                    if (ListID > 0 && dtCFValues.Select(String.Format("vcData = '{0}' AND ListID = " + ListID, objVarie.Value.ToString().ToLower().Replace("'", "''"))).Length == 0)
                                    {
                                        try
                                        {
                                            CCommon objCommon = new CCommon();
                                            objCommon.DomainID = DomainId;
                                            objCommon.ListItemName = objVarie.Value.ToString();
                                            objCommon.ListID = ListID;
                                            objCommon.UserCntID = UserContactID;
                                            objCommon.ManageItemList();

                                            if (ListID > 0)
                                            {
                                                DataRow drFieldValues = dtCFValues.NewRow();
                                                drFieldValues["numListItemID"] = objCommon.ListItemID;
                                                drFieldValues["vcData"] = objVarie.Value.ToString();
                                                drFieldValues["constFlag"] = 0;
                                                drFieldValues["bitDelete"] = 0;
                                                drFieldValues["intSortOrder"] = false;
                                                drFieldValues["ListID"] = ListID;
                                                dtCFValues.Rows.Add(drFieldValues);
                                                dtCFValues.AcceptChanges();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            LogMessage = "Error occurred while adding Item Attribute value : " + objVarie.Value.ToString() + ". Details of error :" + ex.Message.ToString();
                                            GeneralFunctions.WriteMessage(DomainId, "LOG", "eBay:- From GetEBayProductListings : " + LogMessage);
                                        }
                                    }
                                }

                                foreach (VariationDetails vd in vCollection.VariationDetail)
                                {
                                    string[] strVariations = vd.VariationValue.Split(',');

                                    DataTable dtCA = new DataTable();
                                    dtCA.Columns.Add("Fld_ID");
                                    dtCA.Columns.Add("Fld_Value");
                                    dtCA.TableName = "CusFlds";
                                    dtCA.AcceptChanges();

                                    DataRow drAttr = dtCustomAttributes.NewRow();
                                    drAttr["EbayItemID"] = objProduct.EbayItemId;
                                    drAttr["ItemName"] = objProduct.Title;
                                    drAttr["WListPrice"] = vd.ListPrice;
                                    drAttr["OnHand"] = vd.Quantity;
                                    drAttr["ReOrder"] = vd.ReOrder;
                                    drAttr["WSKU"] = vd.SKU;
                                    drAttr["WBarCode"] = "";

                                    foreach (string strItem in strVariations)
                                    {
                                        string strKeyName = strItem.Split('~')[0];
                                        string strKeyValue = strItem.Split('~')[1];
                                        DataRow[] drCField = dtCFields.Select(String.Format("fld_label = '{0}'", strKeyName.ToString().Trim().ToLower().Replace("'", "''")));

                                        if (drCField.Length > 0)
                                        {
                                            long lngCFieldID = CCommon.ToLong(drCField[0]["fld_id"]);
                                            long lngListID = CCommon.ToLong(drCField[0]["ListID"]);
                                            if (lngCFieldID > 0)
                                            {
                                                DataRow[] drCValue = dtCFValues.Select("ListID = " + lngListID + " AND " + String.Format("vcData = '{0}'", strKeyValue.ToString().Trim().ToLower().Replace("'", "''")));
                                                if (drCValue.Length > 0)
                                                {
                                                    DataRow drCA = dtCA.NewRow();
                                                    drCA["Fld_ID"] = lngCFieldID;
                                                    drCA["Fld_Value"] = drCValue[0]["numListItemID"];
                                                    dtCA.Rows.Add(drCA);
                                                    dtCA.AcceptChanges();
                                                }
                                            }
                                        }
                                    }

                                    DataSet dsTemp = new DataSet();
                                    dsTemp.Tables.Add(dtCA);

                                    drAttr["strXML"] = dsTemp.GetXml();
                                    dtCustomAttributes.Rows.Add(drAttr);
                                    dtCustomAttributes.AcceptChanges();
                                }
                            }

                            //Create/ Update ItemGroup in Biz if not exist for new / old Item attributes
                            DataTable dtItemGroup = new DataTable();
                            dtItemGroup.TableName = "Table";
                            dtItemGroup.Columns.Add("numOppAccAttrID");

                            DataSet dsItemGroup = new DataSet();

                            CItems objItem = new CItems();
                            objItem.ItemGroupID = 0;
                            objItem.DomainID = DomainId;
                            dsItemGroup = objItem.GetItemGroups();

                            if (dsItemGroup != null && dsItemGroup.Tables.Count > 0 && dsItemGroup.Tables[0].Rows.Count > 0)
                            {
                                //string strItemGroupName = "";
                                foreach (DataRow drC in dtCFields.Rows)
                                {
                                    DataRow drIG = dtItemGroup.NewRow();
                                    drIG["numOppAccAttrID"] = drC["fld_id"];
                                    dtItemGroup.Rows.Add(drIG);
                                    dtItemGroup.AcceptChanges();
                                }

                                strItemGroupName = strItemGroupName.Trim('_');
                                if (dsItemGroup.Tables[0].Select(String.Format("vcItemGroup = '{0}'", strItemGroupName.ToString().Trim().ToLower().Replace("'", "''"))).Length == 0)
                                {
                                    DataSet dsTempIG = new DataSet();
                                    dsTempIG.Tables.Add(dtItemGroup);

                                    objItem.ItemGroupID = 0;
                                    objItem.ItemGroupName = strItemGroupName;
                                    objItem.str = dsTempIG.GetXml();
                                    objItem.DomainID = DomainId;
                                    ItemGroupID = objItem.ManageItemGroups();
                                }
                                else
                                {
                                    ItemGroupID = CCommon.ToLong(dsItemGroup.Tables[0].Select(String.Format("vcItemGroup = '{0}'", strItemGroupName.ToString().Trim().ToLower().Replace("'", "''")))[0]["numItemgroupID"]);
                                }

                                strItemGroupName = "";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From GetEBayProductListings : " + ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From GetEBayProductListings : " + ex.StackTrace);
                    }
                }



                DataTable dtItemDetails = new DataTable();
                DataRow drMailItemDetail;
                BizCommonFunctions.CreateMailMessageItemDetail(dtItemDetails);
                int ItemCount = 0;

                string ItemClassificationName = "Ebay_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                long ItemClassificationID = 0;
                ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);

                foreach (DataRow drItem in dtProductDetails.Rows)
                {
                    try
                    {
                        DataRow[] drAttributes = dtCustomAttributes.Select(String.Format("EbayItemID = '{0}'", CCommon.ToString(drItem["vcApiItemID"]).ToLower().Replace("'", "''")));
                        if (drAttributes.Length == 0)
                        {
                            ItemGroupID = 0;
                        }

                        long ItemCode = BizCommonFunctions.AddNewItemToBiz(drItem, DomainId, WebApiId, RecordOwner, WareHouseID, ItemClassificationID, ItemGroupID);
                        ItemCount += 1;

                        drMailItemDetail = dtItemDetails.NewRow();
                        drMailItemDetail["SNo"] = CCommon.ToString(ItemCount);
                        drMailItemDetail["BizItemCode"] = CCommon.ToString(ItemCode);
                        drMailItemDetail["ItemName"] = drItem["vcItemName"];
                        drMailItemDetail["SKU"] = drItem["vcSKU"];
                        dtItemDetails.Rows.Add(drMailItemDetail);

                        foreach (DataRow dr in drAttributes)
                        {
                            if (CCommon.ToString(dr["WSKU"]).Length > 0)
                            {
                                CItems objItems = new CItems();
                                objItems.ItemCode = (int)ItemCode;
                                objItems.WareHouseItemID = 0;
                                objItems.WarehouseID = WareHouseID;
                                objItems.WarehouseLocationID = 0;
                                objItems.WListPrice = CCommon.ToDouble(dr["WListPrice"]);
                                objItems.OnHand = CCommon.ToDouble(dr["OnHand"]);
                                objItems.ReOrder = CCommon.ToDouble(dr["ReOrder"]);
                                objItems.WSKU = CCommon.ToString(dr["WSKU"]);
                                objItems.WBarCode = CCommon.ToString(dr["WBarCode"]);
                                objItems.DomainID = DomainId;
                                objItems.UserCntID = UserContactID;
                                objItems.strFieldList = CCommon.ToString(dr["strXML"]);

                                DataTable dtWarehouseItems = new DataTable();
                                dtWarehouseItems = objItems.GetWareHouseItemsBySKU();
                                if(dtWarehouseItems != null && dtWarehouseItems.Rows.Count > 0)
                                {
                                    LogMessage = "Item variation with SKU of ' " + CCommon.ToString(objItems.WSKU) + "' & WarehouseID = " + CCommon.ToLong(objItems.WarehouseID) + " is already added in BizAutomation, Item Code : " + ItemCode + ", system can not update Item Variation";
                                    GeneralFunctions.WriteMessage(DomainId, "LOG", "eBay:- From GetEBayProductListings : " + LogMessage);
                                }
                                else
                                {
                                    objItems.AddUpdateWareHouseForItems();
                                    objItems.MakeItemQtyAdjustmentJournal(ItemCode, CCommon.ToString(drItem["vcItemName"]), CCommon.ToDouble(dr["OnHand"]), CCommon.ToDecimal(drItem["monAverageCost"]), CCommon.ToLong(drItem["numAssetChartAcntId"]), UserContactID, DomainId);

                                    LogMessage = "Added/Updated New Item in BizAutomation, Item Code : " + ItemCode + ", imported from E-Bay Online Marketplace ";
                                    GeneralFunctions.WriteMessage(DomainId, "LOG", "eBay:- From GetEBayProductListings : " + LogMessage);

                                    WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                                    string FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml";

                                    if (System.IO.File.Exists(FilePath))
                                    {
                                        System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                                        XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                        objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                        objStreamReader.Close();
                                    }
                                    //objWebAPIItemDetail.DimensionUOM = BizCommonFunctions.GetLengthUOMCode(CCommon.ToString(drItem["DimensionUOM"]));
                                    //objWebAPIItemDetail.WeightUOM = BizCommonFunctions.GetWeightUOMCode(CCommon.ToString(drItem["WeightUOM"]));
                                    //objWebAPIItemDetail.Length = CCommon.ToString(drItem["fltLength"]);
                                    //objWebAPIItemDetail.Height = CCommon.ToString(drItem["fltHeight"]);
                                    //objWebAPIItemDetail.Width = CCommon.ToString(drItem["fltWidth"]);
                                    //objWebAPIItemDetail.Weight = CCommon.ToString(drItem["fltWeight"]);
                                    //objWebAPIItemDetail.WarrantyDescription = CCommon.ToString(drItem["Warranty"]);

                                    objWebAPIItemDetail.CategoryId = CCommon.ToString(drItem["numEBayCategoryID"]);
                                    objWebAPIItemDetail.EbayListingStyles = CCommon.ToString(drItem["EbayListingType"]);
                                    objWebAPIItemDetail.EbayListingDuration = CCommon.ToString(drItem["EBayListingDuration"]);
                                    objWebAPIItemDetail.StandardShippingCharge = CCommon.ToString(drItem["EbayStandardShippingCost"]);
                                    objWebAPIItemDetail.ExpressShippingCharge = CCommon.ToString(drItem["EbayExpressShippingCost"]);

                                    StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml");

                                    XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                                    Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                                    objStreamWriter.Close();

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From GetEBayProductListings : " + ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From GetEBayProductListings : " + ex.StackTrace);
                    }
                }
                BizCommonFunctions.SendMailNewItemList(DomainId, WebApiId, RecordOwner, UserContactID, ItemCount, dtItemDetails, ItemClassificationName, "e-Bay");

            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from E-Bay Service : " + er.msg;
                GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From GetEBayProductListings : " + ErrMessage);
                GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From GetEBayProductListings : " + er.request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Fills(maps) E-Bay Item Details in BizAutomation Item Details Structure
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="RecordOwner">RecordOwner</param>
        /// <param name="DefaultIncomeAccID">DefaultIncomeAccID</param>
        /// <param name="DefaultCOGSAccID">DefaultCOGSAccID</param>
        /// <param name="DefaultAssetAccID">DefaultAssetAccID</param>
        /// <param name="dr">Data Row of BizAutomation Item Detail Structure </param>
        /// <param name="objEBayProduct">E-Bay Item Details </param>
        /// <returns></returns>
        public DataRow FillEbayItemDetails(long DomainId, int WebApiId, long RecordOwner, int DefaultIncomeAccID, int DefaultCOGSAccID, int DefaultAssetAccID, DataRow dr, EBayProduct objEBayProduct)
        {
            try
            {
                dr["numItemCode"] = 0;
                dr["vcItemName"] = objEBayProduct.Title;
                dr["txtItemDesc"] = "";
                dr["charItemType"] = "P";
                if (objEBayProduct.ListingType == "FixedPriceItem" || objEBayProduct.ListingType == "StoresFixedPrice")
                {
                    dr["monListPrice"] = CCommon.ToDouble(objEBayProduct.CurrentPriceValue);
                }
                else if (objEBayProduct.ListingType == "Chinese")
                {
                    dr["monListPrice"] = CCommon.ToDouble(objEBayProduct.CurrentPriceValue);
                }
                else if (objEBayProduct.ListingType == "")
                {
                    dr["monListPrice"] = CCommon.ToDouble(objEBayProduct.BuyItNowPriceValue);
                }
                dr["txtItemHtmlDesc"] = objEBayProduct.Description;
                dr["bitTaxable"] = 0;
                dr["vcSKU"] = objEBayProduct.SellerSKU;
                dr["numQuantity"] = CCommon.ToInteger(objEBayProduct.Quantity);
                dr["bitKitParent"] = 0;
                dr["numDomainID"] = DomainId;
                dr["numUserCntID"] = RecordOwner;
                dr["numVendorID"] = 0;
                dr["bitSerialized"] = 0;
                dr["strFieldList"] = 0;
                dr["vcModelID"] = objEBayProduct.ModelNumber;
                dr["numItemGroup"] = 0;
                dr["numCOGSChartAcntId"] = DefaultCOGSAccID;
                dr["numAssetChartAcntId"] = DefaultAssetAccID;
                dr["numIncomeChartAcntId"] = DefaultIncomeAccID;
                dr["monAverageCost"] = 0;
                dr["monTotAmtBefDiscount"] = 0;
                dr["monLabourCost"] = 0;
                dr["fltWeight"] = objEBayProduct.Weight;
                dr["fltHeight"] = objEBayProduct.Height;
                dr["fltLength"] = objEBayProduct.Length;
                dr["fltWidth"] = objEBayProduct.Width;
                dr["fltShippingWeight"] = objEBayProduct.ShippingWeight;
                dr["fltShippingHeight"] = objEBayProduct.ShippingHeight;
                dr["fltShippingLength"] = objEBayProduct.ShippingLength;
                dr["fltShippingWidth"] = objEBayProduct.ShippingWidth;
                dr["DimensionUOM"] = objEBayProduct.LengthUOM;
                dr["WeightUOM"] = objEBayProduct.WeightUOM;
                dr["intWebApiId"] = WebApiId;
                if (objEBayProduct.IsArchieve)
                    dr["IsArchieve"] = 1;
                else
                    dr["IsArchieve"] = 0;
                dr["vcApiItemId"] = objEBayProduct.EbayItemId;
                dr["numBarCodeId"] = 0;
                dr["vcManufacturer"] = objEBayProduct.Manufacturer;
                dr["vcExportToAPI"] = WebApiId;
                dr["Warranty"] = objEBayProduct.Warranty;
                dr["numEBayCategoryID"] = objEBayProduct.CategoryID;
                dr["EbayListingType"] = objEBayProduct.ListingType;
                dr["EBayListingDuration"] = objEBayProduct.ListingDuration;
                dr["EbayStandardShippingCost"] = objEBayProduct.StandardShippingCharge;
                dr["EbayExpressShippingCost"] = 0;

                //dr["EbayItemSpecifics"] = objEBayProduct.ItemSpecifics;
                //dr["EbayItemVariations"] = objEBayProduct.ItemVariations;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dr;
        }

        /// <summary>
        /// Gets Product Details from e-Bay for given Item ID
        /// </summary>
        /// <param name="DomainId">Domain Id</param>
        /// <param name="WebApiId">WebApi Id</param>
        /// <param name="EbayItemId">Ebay Item Id</param>
        /// <returns></returns>
        public EBayProduct GetProductDetailForItemID(long DomainId, int WebApiId, string EbayItemId)
        {
            EBayProduct objEbayProduct = new EBayProduct();
            WebAPI objWebAPi = new WebAPI();

            try
            {
                objWebAPi.DomainID = DomainId;
                objWebAPi.WebApiId = WebApiId;
                objWebAPi.Mode = 3;
                DataTable dtWebAPI = objWebAPi.GetWebApi();
                foreach (DataRow drWebApi in dtWebAPI.Rows)
                {
                    if (CCommon.ToBool(drWebApi["bitEnableAPI"]) == true)
                    {
                        if (WebApiId == CCommon.ToInteger(drWebApi["WebApiId"]))//Amazon US
                        {
                            string eBaySite = CCommon.ToString(drWebApi["vcFirstFldValue"]);
                            string Token = CCommon.ToString(drWebApi["vcEighthFldValue"]);
                            ApiContext Context = GetContext(Token, eBaySite);
                            IeBayCommunicatorClient eBayClient = new IeBayCommunicatorClient();
                            objEbayProduct = eBayClient.GetItemDetails(Context, EbayItemId);
                            eBayClient.Close();
                        }
                    }
                }
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from E-Bay Service : " + er.msg;
                GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From GetProductDetailForItemID : " + ErrMessage);
                GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From GetProductDetailForItemID : " + er.request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEbayProduct;
        }

        #endregion Product Operations

        #region Inventory Management

        /// <summary>
        /// Updates inventory to e-Bay Seller Account
        /// </summary>
        /// <param name="DomainId">Domain Id</param>
        /// <param name="WebApiId">WebApi Id</param>
        /// <param name="dtNewItems">Inventory modified Items list</param>
        /// <param name="Token">Merchant Token</param>
        /// <param name="eBaySite">eBay Site Code</param>
        /// <param name="PayPalEmailAddress">PayPal Email Address</param>
        public void UpdateInventory(long DomainId, int WebApiId, DataTable dtNewItems, string Token, string eBaySite, string PayPalEmailAddress)
        {
            string ErrorMessage = "", LogMessage = "";
            string ListingStyle = "";

            ApiContext Context = GetContext(Token, eBaySite);
            ItemType item = new ItemType();

            foreach (DataRow dr in dtNewItems.Rows)
            {
                try
                {
                    if (dtItemSpecifics.Rows.Count > 0)
                        dtItemSpecifics.Rows.Clear();

                    if (CCommon.ToString(dr["vcItemName"]).Length > 0 & CCommon.ToString(dr["numItemCode"]).Length > 0 & CCommon.ToString(dr["vcAPIItemId"]).Length >= 1)
                    {
                        WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                        string WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";
                        if (File.Exists(WebApi_ItemDetails))
                        {
                            StreamReader objStreamReader = new StreamReader(WebApi_ItemDetails);
                            XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                            objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                            objStreamReader.Close();
                        }
                        if (objWebAPIItemDetail.EbayListingStyles != null && objWebAPIItemDetail.EbayListingStyles != "")
                        {
                            ListingStyle = objWebAPIItemDetail.EbayListingStyles;
                        }
                        else
                        {
                            ErrorMessage = "Ebay Listing Styles Id not found for Item SKU : " + item.SKU + " ,Name : " + CCommon.ToString(dr["vcItemName"]);
                            throw new Exception("eBay:- From UpdateInventory : " + ErrorMessage);
                        }
                        item.SKU = CCommon.ToString(dr["vcSKU"]);
                        item.Quantity = CCommon.ToInteger(dr["QtyOnHand"]);
                        item.QuantityAvailable = CCommon.ToInteger(dr["QtyOnHand"]);
                        item.QuantitySpecified = true;
                        if (ListingStyle == "FixedPriceItem" || ListingStyle == "StoresFixedPrice" || ListingStyle == "Fixed Price")
                        {
                            IeBayCommunicatorClient eBayClient = new IeBayCommunicatorClient();
                            try
                            {
                                eBayClient.UpdateInventory(Context, CCommon.ToString(dr["vcAPIItemId"]), CCommon.ToString(dr["vcSKU"]), CCommon.ToInteger(dr["QtyOnHand"]));
                                LogMessage = "Ebay Product Inventory Quantity : " + CCommon.ToString(dr["QtyOnHand"]) + " is Updated for item SKU: " + CCommon.ToString(dr["vcSKU"]);
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "eBay:- From UpdateInventory : " + LogMessage);
                            }
                            catch (FaultException<error> ex)
                            {
                                error er = ex.Detail;
                                string ErrMessage = "Error received from E-Bay Service : " + er.msg;
                                GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From UpdateInventory : " + ErrMessage);
                                GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From UpdateInventory : " + er.request);
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From UpdateInventory : " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From UpdateInventory : " + ex.StackTrace);
                }
            }
        }

        #endregion Inventory Management

        #region Order Fulfillment

        /// <summary>
        /// Updates Tracking Number of the e-Bay Order 
        /// </summary>
        /// <param name="DomainId">Domain Id</param>
        /// <param name="WebApiId">WebApi Id</param>
        /// <param name="Token">Merchant Token</param>
        /// <param name="eBaySite">eBaySite Code</param>
        /// <param name="numItemCode">ItemCode</param>
        /// <param name="ItemAPIId">e-Bay Item Id</param>
        /// <param name="APIOrderId">e-Bay Order Id</param>
        /// <param name="APIOrderLineItemId">e-Bay Order Line ItemId</param>
        /// <param name="TrackingNo">TrackingNo</param>
        /// <param name="ShippingCarrier">Shipping Carrier</param>
        public void UpdateTrackingNo(long DomainId, long WebApiId, string Token, string eBaySite, long numItemCode, string ItemAPIId,
            string APIOrderId, string APIOrderLineItemId, string TrackingNo, string ShippingCarrier)
        {
            try
            {
                string ListingType = "";
                ApiContext Context = GetContext(Token, eBaySite);
                IeBayCommunicatorClient eBayClient = new IeBayCommunicatorClient();
                WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                string WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + numItemCode + ".xml";
                if (File.Exists(WebApi_ItemDetails))
                {
                    StreamReader objStreamReader = new StreamReader(WebApi_ItemDetails);
                    XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                    objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                    objStreamReader.Close();
                    //GeneralFunctions.DeSerializeFileToObject<WebAPIItemDetail>(out objWebAPIItemDetail, WebApi_ItemDetails);
                }
                if (objWebAPIItemDetail.EbayListingStyles != null && objWebAPIItemDetail.EbayListingStyles != "")
                {
                    ListingType = objWebAPIItemDetail.EbayListingStyles;

                }
                else
                {
                    string ErrorMessage = "Ebay Listing Styles Id not found for Item" + numItemCode;
                    throw new Exception("eBay:- From UpdateTrackingNo : " + ErrorMessage);
                }

                ShipmentType objShipmentTyp = new ShipmentType();
                objShipmentTyp.DeliveryStatus = ShipmentDeliveryStatusCodeType.LabelPrinted;
                objShipmentTyp.ShippingPackage = ShippingPackageCodeType.PackageThickEnvelope;
                objShipmentTyp.Status = ShipmentStatusCodeType.Active;
                objShipmentTyp.DeliveryDate = DateTime.Now.Date.AddDays(5);
                objShipmentTyp.DeliveryDateSpecified = true;
                objShipmentTyp.DeliveryStatusSpecified = false;

                ShipmentTrackingDetailsType objShipTracking = new ShipmentTrackingDetailsType();

                objShipTracking.ShipmentTrackingNumber = TrackingNo;
                objShipTracking.ShippingCarrierUsed = ShippingCarrier;

                objShipmentTyp.ShipmentTrackingDetails = new ShipmentTrackingDetailsTypeCollection(new ShipmentTrackingDetailsType[] { objShipTracking });
                //  objShipmentTyp.ShipmentTrackingNumber = "Jos123456";
                ListingTypeCodeType ListingTypeCode = GetListingStyle(ListingType);

                // objCompleteSale.CompleteSale(ItemAPIId, TrackingNo, null, true, true, ListingTypeCodeType.Auction,
                //objShipmentTyp, APIOrderId, APIOrderLineItemId);
                MemoryStream msShipmentTyp = GeneralFunctions.SerializeToStream(objShipmentTyp);

                eBayClient.UpdateTrackingNo(Context, ItemAPIId, APIOrderId, APIOrderLineItemId, TrackingNo, ListingTypeCode, msShipmentTyp);

            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from E-Bay Service : " + er.msg;
                GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From UpdateTrackingNo : " + ErrMessage);
                GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From UpdateTrackingNo : " + er.request);
            }
            catch (Exception ex)
            {
                GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From UpdateTrackingNo : " + ex.Message);
                GeneralFunctions.WriteMessage(DomainId, "ERR", "eBay:- From UpdateTrackingNo : " + ex.StackTrace);
            }

        }


        #endregion Order Fulfillment

        #region Helper Functions

        /// <summary>
        /// Creates the E-Bay Api Context for the E-Bay Seller
        /// </summary>
        /// <param name="Token">Merchant Token</param>
        /// <param name="eBaySite">eBay Selling Site</param>
        /// <returns></returns>
        public ApiContext GetContext(string Token, string eBaySite)
        {
            ApiContext context = new ApiContext();

            try
            {
                // Credentials for the call
                string appId = ConfigurationManager.AppSettings["Environment.AppId"];
                string devId = ConfigurationManager.AppSettings["Environment.DevId"]; ;
                string certId = ConfigurationManager.AppSettings["Environment.CertId"];

                context.ApiCredential.ApiAccount.Developer = devId;
                context.ApiCredential.ApiAccount.Application = appId;
                context.ApiCredential.ApiAccount.Certificate = certId;
                context.ApiCredential.eBayToken = Token;

                // Set the URL
                context.SoapApiServerUrl = ConfigurationManager.AppSettings["Environment.ApiServerUrl"];
                context.EPSServerUrl = ConfigurationManager.AppSettings["Environment.EpsServerUrl"];
                context.SignInUrl = ConfigurationManager.AppSettings["Environment.SignInUrl"];

                context.Site = GetEBaySiteCodeFromString(eBaySite);

                // Set the version
                context.Version = "735";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return context;

        }

        /// <summary>
        /// Get e-Bay's Listing Style from Listing Style ID
        /// </summary>
        /// <param name="ListingStyleId">Listing Style Id</param>
        /// <returns>e-Bay's Listing Style enum Value</returns>
        public ListingTypeCodeType GetListingStyle(string ListingStyleId)
        {
            ListingTypeCodeType ListingStyle;
            switch (ListingStyleId)
            {
                case "AdType":
                    ListingStyle = ListingTypeCodeType.AdType;
                    break;

                case "Auction1":
                    ListingStyle = ListingTypeCodeType.Auction;
                    break;

                case "Chinese":
                    ListingStyle = ListingTypeCodeType.Chinese;
                    break;

                case "Auction":
                    ListingStyle = ListingTypeCodeType.Chinese;
                    break;

                case "CustomCode":
                    ListingStyle = ListingTypeCodeType.CustomCode;
                    break;

                case "Dutch":
                    ListingStyle = ListingTypeCodeType.Dutch;
                    break;

                case "Express":
                    ListingStyle = ListingTypeCodeType.Express;
                    break;

                case "Fixed Price":
                    ListingStyle = ListingTypeCodeType.FixedPriceItem;
                    break;

                case "FixedPriceItem":
                    ListingStyle = ListingTypeCodeType.FixedPriceItem;
                    break;

                case "Half":
                    ListingStyle = ListingTypeCodeType.Half;
                    break;

                case "LeadGeneration":
                    ListingStyle = ListingTypeCodeType.LeadGeneration;
                    break;

                case "Live":
                    ListingStyle = ListingTypeCodeType.Live;
                    break;

                case "PersonalOffer":
                    ListingStyle = ListingTypeCodeType.PersonalOffer;
                    break;

                case "Shopping":
                    ListingStyle = ListingTypeCodeType.Shopping;
                    break;

                case "StoresFixedPrice":
                    ListingStyle = ListingTypeCodeType.StoresFixedPrice;
                    break;

                case "Unknown":
                    ListingStyle = ListingTypeCodeType.Unknown;
                    break;
                default:

                    ListingStyle = ListingTypeCodeType.Chinese;
                    break;
            }
            return ListingStyle;
        }

        /// <summary>
        /// Deserialize Memory steam to Object Type
        /// </summary>
        /// <typeparam name="T">Object Type</typeparam>
        /// <param name="obj">Return Object of Type T</param>
        /// <param name="ms">MemoryStream</param>
        public void DeSerializeMSToObject<T>(out T obj, MemoryStream ms)
        {
            try
            {
                //XmlSerializer serializer = new XmlSerializer(typeof(T));
                //// Create an XmlTextWriter using a FileStream.
                //Stream fs = File.Open(filename, FileMode.Open, FileAccess.Read);
                //XmlReader Reader = new XmlTextReader(fs);
                //fs.Flush();
                //fs.Close();
                //// DeSerialize using the XmlTextReader.
                //obj = (T)serializer.Deserialize(Reader);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                XmlReader Reader = new XmlTextReader(ms);
                obj = (T)serializer.Deserialize(Reader);
                ms.Flush();
                ms.Close();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// Gets eBay Site enum value from eBaySite Code
        /// </summary>
        /// <param name="eBaySite">eBaySite Code</param>
        /// <returns>eBay Site enum value</returns>
        private SiteCodeType GetEBaySiteCodeFromString(string eBaySite)
        {
            SiteCodeType Site = new SiteCodeType();
            try
            {
                switch (eBaySite.ToUpper())
                {
                    case "AUSTRALIA":
                        Site = SiteCodeType.Australia;
                        break;

                    case "AUSTRIA":
                        Site = SiteCodeType.Austria;
                        break;

                    case "BELGIUM_DUTCH":
                        Site = SiteCodeType.Belgium_Dutch;
                        break;

                    case "BELGIUM_FRENCH":
                        Site = SiteCodeType.Belgium_French;
                        break;

                    case "CANADA":
                        Site = SiteCodeType.Canada;
                        break;

                    case "CANADAFRENCH":
                        Site = SiteCodeType.CanadaFrench;
                        break;


                    case "CHINA":
                        Site = SiteCodeType.China;
                        break;

                    case "CUSTOMCODE":
                        Site = SiteCodeType.CustomCode;
                        break;

                    case "EBAYMOTORS":
                        Site = SiteCodeType.eBayMotors;
                        break;

                    case "FRANCE":
                        Site = SiteCodeType.France;
                        break;


                    case "GERMANY":
                        Site = SiteCodeType.Germany;
                        break;

                    case "HONGKONG":
                        Site = SiteCodeType.HongKong;
                        break;

                    case "INDIA":
                        Site = SiteCodeType.India;
                        break;

                    case "IRELAND":
                        Site = SiteCodeType.Ireland;
                        break;

                    case "ITALY":
                        Site = SiteCodeType.Italy;
                        break;

                    case "MALAYSIA":
                        Site = SiteCodeType.Malaysia;
                        break;

                    case "NETHERLANDS":
                        Site = SiteCodeType.Netherlands;
                        break;

                    case "PHILIPPINES":
                        Site = SiteCodeType.Philippines;
                        break;

                    case "POLAND":
                        Site = SiteCodeType.Poland;
                        break;

                    case "SINGAPORE":
                        Site = SiteCodeType.Singapore;
                        break;

                    case "SPAIN":
                        Site = SiteCodeType.Spain;
                        break;

                    case "SWEDEN":
                        Site = SiteCodeType.Sweden;
                        break;

                    case "SWITZERLAND":
                        Site = SiteCodeType.Switzerland;
                        break;

                    case "TAIWAN":
                        Site = SiteCodeType.Taiwan;
                        break;

                    case "UK":
                        Site = SiteCodeType.UK;
                        break;

                    case "US":
                        Site = SiteCodeType.US;
                        break;

                    //default:
                    //    Site = SiteCodeType.Switzerland;
                    //    break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Site;
        }

        /// <summary>
        /// GetRoutingRulesUserCntID
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="dbFieldName">dbFieldName</param>
        /// <param name="FieldValue">FieldValue</param>
        /// <returns></returns>
        private long GetRoutingRulesUserCntID(long DomainId, string dbFieldName, string FieldValue)
        {
            AutoRoutingRules objAutoRoutRles = new AutoRoutingRules();
            DataTable dtAutoRoutingRules = new DataTable();
            DataRow drAutoRow = null;
            DataSet ds = new DataSet();
            try
            {
                objAutoRoutRles.DomainID = DomainId;
                if (dtAutoRoutingRules.Columns.Count == 0)
                {
                    dtAutoRoutingRules.TableName = "Table";
                    dtAutoRoutingRules.Columns.Add("vcDbColumnName");
                    dtAutoRoutingRules.Columns.Add("vcDbColumnText");
                }
                dtAutoRoutingRules.Rows.Clear();
                drAutoRow = dtAutoRoutingRules.NewRow();
                drAutoRow["vcDbColumnName"] = dbFieldName;
                drAutoRow["vcDbColumnText"] = FieldValue;
                dtAutoRoutingRules.Rows.Add(drAutoRow);
                ds.Tables.Add(dtAutoRoutingRules);
                objAutoRoutRles.strValues = ds.GetXml();
                ds.Tables.Remove(ds.Tables[0]);
                return objAutoRoutRles.GetRecordOwner();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetOrderPaymentMethod
        /// </summary>
        public int GetOrderPaymentMethod(BuyerPaymentMethodCodeType PaymentType)
        {
            int BizPaymentTypeId = 0;
            try
            {
                switch (PaymentType)
                {
                    case BuyerPaymentMethodCodeType.PayPal:
                        BizPaymentTypeId = 2;
                        break;
                    default:
                        BizPaymentTypeId = 4;
                        break;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return BizPaymentTypeId;
        }

        #endregion Helper Functions

        #region Test Bulk Order Import

        /// <summary>
        /// Adds E-Bay Orders to BizAutomation for the specified Domain
        /// </summary>
        /// <param name="orders">E-Bay Orders Collection for the Merchant's E-Bay Account</param>
        /// <param name="DomainID">Biz Domain ID for the Merchant</param>
        /// <param name="WebApiId">Web API ID</param>
        /// <param name="Source">Order's Source</param>
        /// <param name="numWareHouseID">Default Warehouse Id for the E-Bay Order Items</param>
        /// <param name="numRecordOwner">Orders Reocrd Owner's Id</param>
        /// <param name="numAssignTo">Assinged to User Id</param>
        /// <param name="numRelationshipId">E-Bay Orders Customer's default relationship Id</param>
        /// <param name="numProfileId">Default Profile Id</param>
        /// <param name="numBizDocId">Default Biz Document Id for E-Bay Orders</param>
        /// <param name="numOrderStatus">Default Order Status</param>
        /// <param name="ExpenseAccountId">Default Expense Account Id</param>
        public void TestBulkOrderImport(OrderTypeCollection orders, long DomainID, int WebApiId, string Source,
           int numWareHouseID, long numRecordOwner, long numAssignTo, long numRelationshipId, long numProfileId, long numBizDocId, long BizDocStatusId, int numOrderStatus, int ExpenseAccountId, long DiscountItemMapping, long ShippingServiceItemID, long SalesTaxItemMappingID, string ShipToPhoneNo)
        {
            string LogMessage = "", ErrorMsg = "";

            CContacts objContacts = null;
            CLeads objLeads = null;
            ImportWizard objImpWzd = new ImportWizard();
            MOpportunity objOpportunity = new MOpportunity();
            CCommon objCommon = new CCommon();
            foreach (OrderType Order in orders)
            {
                try
                {
                    decimal SalesTaxRate = 0;
                    string OrderId = Order.OrderID;
                    LogMessage += Environment.NewLine + "OrderId : " + OrderId;
                    string OrderStatus = "";

                    if (Order.OrderStatusSpecified)
                        OrderStatus = CCommon.ToString(Order.OrderStatus);

                    WebAPI objWebApi = new WebAPI();

                    string ItemMessage = "";
                    decimal ShipCost = 0;
                    decimal OrderTotal = 0;
                    long CurrencyID = 0;
                    decimal dTotDiscount = 0;
                    decimal SalesTaxAmount = 0;

                    if (!string.IsNullOrEmpty(OrderId))
                    {
                        objWebApi.DomainID = DomainID;
                        objWebApi.WebApiId = WebApiId;
                        objWebApi.vcAPIOppId = OrderId;
                        int BizPaymentMethodId = 4;
                        int a = -1;
                        string AddressStreet = "";//To concatenate Address Lines
                        objContacts = new CContacts();
                        objLeads = new CLeads();
                        objLeads.DomainID = DomainID;

                        AddressType ShippingAdress = Order.ShippingAddress;
                        string CompanyName = ShippingAdress.CompanyName;
                        LogMessage += Environment.NewLine + " CompanyName : " + CompanyName;

                        string FirstName = ShippingAdress.FirstName;
                        LogMessage += Environment.NewLine + " FirstName : " + FirstName;

                        string LastName = ShippingAdress.LastName;
                        LogMessage += Environment.NewLine + " LastName : " + LastName;

                        string Name = ShippingAdress.Name;
                        LogMessage += Environment.NewLine + " Name : " + Name;

                        string BuyerUserID = Order.BuyerUserID;
                        LogMessage += Environment.NewLine + " BuyerUserID : " + BuyerUserID;

                        if (Order.TransactionArray != null)
                        {
                            TransactionTypeCollection TransTypColl = new TransactionTypeCollection(Order.TransactionArray);

                            foreach (TransactionType TransTyp in TransTypColl)
                            {
                                if (TransTyp.Buyer != null)
                                {
                                    UserType Buyer = TransTyp.Buyer;
                                    //LogMessage += Environment.NewLine + " TransTyp.Buyer : " + CCommon.ToString(Buyer);
                                    LogMessage += Environment.NewLine + " TransTyp.Email : " + Buyer.Email;
                                    if (Buyer.Email != null && Buyer.Email != "" && Buyer.Email != "Invalid Request")//If contains Email Address
                                    {
                                        objLeads.Email = Buyer.Email;

                                        //Check for Details for the Given DomainID and EmailID
                                        objLeads.GetConIDCompIDDivIDFromEmail();
                                    }
                                    else
                                    {
                                        objLeads.Email = "";
                                    }
                                    if (objLeads.ContactID > 0 && objLeads.DivisionID > 0)
                                    {
                                        //If Email already registered
                                        lngCntID = objLeads.ContactID;
                                        lngDivId = objLeads.DivisionID;
                                    }
                                    else
                                    {
                                        if (ShippingAdress.Name != null && ShippingAdress.Name != "")
                                        {
                                            objLeads.CompanyName = ShippingAdress.Name;
                                            objLeads.CustName = ShippingAdress.Name;
                                        }
                                        else if (Order.ShippingAddress != null)
                                        {
                                            AddressType shippingAddress = Order.ShippingAddress;

                                            if (shippingAddress.Name != null && shippingAddress.Name != "")
                                            {
                                                objLeads.CompanyName = shippingAddress.Name;
                                                objLeads.CustName = shippingAddress.Name;
                                            }
                                        }

                                        if (Order.ShippingAddress != null)
                                        {
                                            AddressType shippingAddress = Order.ShippingAddress;
                                            if (shippingAddress.County != null && shippingAddress.County != "")
                                            {
                                                //objLeads.Country = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                                //objLeads.SCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                            }
                                            if (shippingAddress.Country != null && CCommon.ToString(shippingAddress.Country) != "")
                                            {
                                                objCommon.DomainID = DomainID;
                                                objCommon.Mode = 22;
                                                objCommon.Str = CCommon.ToString(shippingAddress.Country);
                                                objLeads.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                objLeads.SCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                            }

                                            if (shippingAddress.Name != null && shippingAddress.Name != "")
                                            {
                                                a = shippingAddress.Name.IndexOf('.');
                                                if (a == -1)
                                                    a = shippingAddress.Name.IndexOf(' ');
                                                if (a == -1)
                                                {
                                                    objLeads.FirstName = shippingAddress.Name;
                                                    objLeads.LastName = shippingAddress.Name;
                                                }
                                                else
                                                {
                                                    objLeads.FirstName = shippingAddress.Name.Substring(0, a);
                                                    objLeads.LastName = shippingAddress.Name.Substring(a + 1, shippingAddress.Name.Length - (a + 1));
                                                }
                                            }
                                            if (shippingAddress.Phone != null)
                                            {
                                                objLeads.ContactPhone = shippingAddress.Phone;
                                                objLeads.PhoneExt = "";
                                            }
                                            else if (shippingAddress.Phone2 != null)
                                            {
                                                objLeads.ContactPhone = shippingAddress.Phone2;
                                                objLeads.PhoneExt = "";
                                            }
                                            else
                                            {
                                                objLeads.ContactPhone = ShipToPhoneNo;
                                                objLeads.PhoneExt = "";
                                            }
                                        }
                                        objLeads.UserCntID = numRecordOwner;
                                        objLeads.ContactType = 70;
                                        objLeads.PrimaryContact = true;
                                        objLeads.UpdateDefaultTax = false;
                                        objLeads.NoTax = false;
                                        objLeads.CRMType = 1;
                                        objLeads.DivisionName = "-";

                                        objLeads.CompanyType = numRelationshipId;
                                        objLeads.Profile = numProfileId;

                                        objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();//Creates Company Record
                                        LogMessage = "New Company details added CompanyID : " + objLeads.CompanyID + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                                        lngDivId = objLeads.CreateRecordDivisionsInfo();
                                        LogMessage = "New Divisions Informations added. Division Id : " + lngDivId + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                                        objLeads.ContactID = 0;
                                        objLeads.DivisionID = lngDivId;
                                        lngCntID = objLeads.CreateRecordAddContactInfo();//Creates Contact Info
                                        LogMessage = "New Contact Informations added. Contact Id : " + lngCntID + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                                        if (Order.ShippingAddress != null)
                                        {
                                            objContacts.FirstName = objLeads.FirstName;
                                            objContacts.LastName = objLeads.LastName;
                                            objContacts.ContactPhone = objLeads.ContactPhone;
                                            objContacts.Email = objLeads.Email;

                                            AddressType shippingAddress = Order.ShippingAddress;

                                            if (shippingAddress.Street != null && shippingAddress.Street != "")
                                                AddressStreet = shippingAddress.Street;

                                            if (shippingAddress.Street1 != null && shippingAddress.Street1 != "")
                                                AddressStreet = AddressStreet + " " + shippingAddress.Street1;

                                            if (shippingAddress.Street2 != null && shippingAddress.Street2 != "")
                                                AddressStreet = AddressStreet + " " + shippingAddress.Street2;

                                            objContacts.BillStreet = AddressStreet;
                                            objContacts.ShipStreet = AddressStreet;

                                            if (shippingAddress.CityName != null && shippingAddress.CityName != "")
                                            {
                                                objContacts.BillCity = shippingAddress.CityName;
                                                objContacts.ShipCity = shippingAddress.CityName;
                                            }

                                            if (shippingAddress.StateOrProvince != null && shippingAddress.StateOrProvince != "")
                                            {
                                                objCommon.DomainID = DomainID;
                                                objCommon.Mode = 21;
                                                objCommon.Str = shippingAddress.StateOrProvince;
                                                objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                            }

                                            if (shippingAddress.Country != null && CCommon.ToString(shippingAddress.Country) != "")
                                            {
                                                objCommon.DomainID = DomainID;
                                                objCommon.Mode = 22;
                                                objCommon.Str = CCommon.ToString(shippingAddress.Country);
                                                objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                                objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                            }

                                            if (shippingAddress.PostalCode != null && shippingAddress.PostalCode != "")
                                            {
                                                objContacts.BillPostal = shippingAddress.PostalCode;
                                                objContacts.ShipPostal = shippingAddress.PostalCode;
                                            }

                                            objContacts.BillingAddress = 0;
                                            objContacts.DivisionID = lngDivId;
                                            objContacts.ContactID = lngCntID;
                                            objContacts.RecordID = lngDivId;
                                            objContacts.DomainID = DomainID;
                                            objContacts.IsPrimaryAddress = CCommon.ToBool(1);
                                            objContacts.UpdateCompanyAddress();
                                        }
                                    }

                                    if (Order.CheckoutStatus != null)
                                    {
                                        if (Order.CheckoutStatus.PaymentMethod != null)
                                        {
                                            BizPaymentMethodId = GetOrderPaymentMethod(Order.CheckoutStatus.PaymentMethod);
                                        }
                                    }

                                    objOpportunity.OppRefOrderNo = Order.OrderID;
                                    objOpportunity.MarketplaceOrderID = Order.OrderID;
                                    BizCommonFunctions.CreateItemTable(dtOrderItems);
                                    ItemMessage = TestBulkOrder_AddOrderItemDataset(DomainID, WebApiId, numWareHouseID, numRecordOwner, DiscountItemMapping, TransTypColl, Order.ShippingServiceSelected.ShippingService, ShippingServiceItemID, SalesTaxItemMappingID, CCommon.ToDecimal(Order.AmountSaved), out CurrencyID, out OrderTotal, out ShipCost, out dTotDiscount, out SalesTaxAmount);

                                    objOpportunity.CurrencyID = CurrencyID;
                                    objOpportunity.Amount = OrderTotal;
                                    //objContacts.BillingAddress = 0;
                                    //objContacts.DivisionID = lngDivId;
                                    //objContacts.UpdateCompanyAddress();//Updates Company Details
                                    BizCommonFunctions.SaveTaxTypes(ds, lngDivId);
                                    arrBillingIDs = new string[4];
                                    arrBillingIDs[0] = CCommon.ToString(objLeads.CompanyID);
                                    arrBillingIDs[1] = CCommon.ToString(lngDivId);
                                    arrBillingIDs[2] = CCommon.ToString(lngCntID);
                                    arrBillingIDs[3] = objLeads.CompanyName;
                                    if (lngCntID == 0)
                                        return;
                                    //Create Opportunity Details
                                    DateTime OrderDate = Order.CreatedTime;
                                    objOpportunity.MarketplaceOrderDate = OrderDate;
                                    objOpportunity.OpportunityId = 0;
                                    objOpportunity.OpportunityId = 0;
                                    objOpportunity.ContactID = lngCntID;
                                    objOpportunity.DivisionID = lngDivId;
                                    objOpportunity.UserCntID = numRecordOwner;
                                    objOpportunity.AssignedTo = numAssignTo;
                                    objOpportunity.OpportunityName = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM");
                                    objOpportunity.EstimatedCloseDate = DateTime.Now;
                                    objOpportunity.PublicFlag = 0;
                                    objOpportunity.DomainID = DomainID;
                                    objOpportunity.OppType = 1;
                                    objOpportunity.OrderStatus = numOrderStatus;

                                    dsOrderItems.Tables.Clear();
                                    dsOrderItems.Tables.Add(dtOrderItems.Copy());
                                    if (dsOrderItems.Tables[0].Rows.Count != 0)
                                        objOpportunity.strItems = ((dtOrderItems.Rows.Count > 0) ? ("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsOrderItems.GetXml()) : "");
                                    else
                                        objOpportunity.strItems = "";
                                    objOpportunity.OppComments = ItemMessage;
                                    objOpportunity.Source = WebApiId;
                                    objOpportunity.SourceType = 3;
                                    objOpportunity.TaxOperator = 2; //Remove by default setting of adding sales tax while it comes through marketplace itself
                                    objOpportunity.DealStatus = (long)MOpportunity.EnmDealStatus.DealWon;
                                    arrOutPut = objOpportunity.Save();
                                    lngOppId = CCommon.ToLong(arrOutPut[0]);

                                    //Save Source 
                                    objOpportunity.WebApiId = WebApiId;
                                    objOpportunity.vcSource = Source;
                                    objOpportunity.OpportunityId = lngOppId;
                                    objOpportunity.ManageOppLinking();

                                    objOpportunity.CompanyID = lngDivId;

                                    if (Order.ShippingAddress != null)
                                    {
                                        AddressType shippingAddress = Order.ShippingAddress;
                                        if (!string.IsNullOrEmpty(shippingAddress.CompanyName))
                                        {
                                            objOpportunity.BillCompanyName = shippingAddress.CompanyName;
                                        }
                                        if (!string.IsNullOrEmpty(shippingAddress.Name))
                                        {
                                            objOpportunity.AddressName = shippingAddress.Name;
                                        }
                                        if (shippingAddress.Street != null && shippingAddress.Street != "")
                                        {
                                            AddressStreet = shippingAddress.Street;
                                        }
                                        if (shippingAddress.Street1 != null && shippingAddress.Street1 != "")
                                        {
                                            AddressStreet = AddressStreet + " " + shippingAddress.Street1;
                                        }
                                        if (shippingAddress.Street2 != null && shippingAddress.Street2 != "")
                                        {
                                            AddressStreet = AddressStreet + " " + shippingAddress.Street2;
                                        }
                                        if (shippingAddress.CityName != null && shippingAddress.CityName != "")
                                        {
                                            AddressStreet = AddressStreet + " " + shippingAddress.CityName;
                                        }
                                        objContacts.BillStreet = AddressStreet;
                                        objContacts.ShipStreet = AddressStreet;
                                        objOpportunity.BillStreet = AddressStreet;

                                        if (shippingAddress.CityName != null && shippingAddress.CityName != "")
                                        {
                                            objContacts.BillCity = shippingAddress.CityName;
                                            objContacts.ShipCity = shippingAddress.CityName;
                                            objOpportunity.BillCity = shippingAddress.CityName;
                                        }
                                        if (shippingAddress.StateOrProvince != null && shippingAddress.StateOrProvince != "")
                                        {
                                            objCommon.DomainID = DomainID;
                                            objCommon.Mode = 21;
                                            objCommon.Str = shippingAddress.StateOrProvince;
                                            objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                            objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                            objOpportunity.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                        }
                                        if (shippingAddress.County != null && shippingAddress.County != "")
                                        {
                                            //objContacts.BillCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                            //objContacts.ShipCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                        }
                                        if (shippingAddress.Country != null && CCommon.ToString(shippingAddress.Country) != "")
                                        {
                                            objCommon.DomainID = DomainID;
                                            objCommon.Mode = 22;
                                            objCommon.Str = CCommon.ToString(shippingAddress.Country);
                                            objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                            objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                            objOpportunity.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                        }
                                        if (shippingAddress.PostalCode != null && shippingAddress.PostalCode != "")
                                        {
                                            objContacts.BillPostal = shippingAddress.PostalCode;
                                            objContacts.ShipPostal = shippingAddress.PostalCode;
                                            objOpportunity.BillPostal = shippingAddress.PostalCode;
                                        }

                                        objOpportunity.Mode = 0;
                                        objOpportunity.UpdateOpportunityAddress();
                                        objOpportunity.Mode = 1;
                                        objOpportunity.UpdateOpportunityAddress();
                                    }


                                    LogMessage = "Order Details is added, Ebay OrderID :  " + Order.OrderID + "  Biz OppID: : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;

                                    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                                    objWebApi.WebApiId = WebApiId;
                                    objWebApi.DomainID = DomainID;
                                    objWebApi.UserCntID = numRecordOwner;
                                    objWebApi.OppId = lngOppId;
                                    objWebApi.vcAPIOppId = Order.OrderID;
                                    objWebApi.AddAPIopportunity();

                                    objWebApi.API_OrderId = Order.OrderID;
                                    objWebApi.API_OrderStatus = 1;
                                    objWebApi.WebApiOrdDetailId = 1;
                                    objWebApi.ManageAPIOrderDetails();

                                    decimal OrderAmount = OrderTotal;// +ShipCost - dTotDiscount;
                                    //string Reference = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM"); 
                                    string Reference = arrOutPut[1];

                                    if (ItemMessage == "")
                                    {
                                        bool IsAuthoritative = false;
                                        BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                        BizCommonFunctions.CreateOppBizDoc(objOppBizDocs, numRecordOwner, DomainID, lngOppId, objOpportunity.OppRefOrderNo, numBizDocId, BizDocStatusId, lngDivId, OrderDate, ShipCost, dTotDiscount, out OppBizDocID, out IsAuthoritative, ExpenseAccountId, SalesTaxRate, OrderAmount, Reference);
                                        UpdateApiOppItemDetails(DomainID, WebApiId, numRecordOwner, lngOppId, OppBizDocID, objOpportunity.OppRefOrderNo, TransTypColl);
                                    }
                                    else
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "BizDoc is not Created for BizOpp Id : " + lngOppId + " due to " + ItemMessage);
                                    }
                                    //}
                                    //else
                                    //{
                                    //    string ErrMessage = "Order report doesnot have sufficient Buyer details to process the E-Bay Order : " + OrderId;
                                    //    GeneralFunctions.WriteMessage(DomainID, "LOG", ErrMessage);
                                    //}
                                }
                                else
                                {
                                    string ErrMessage = "Order report doesnot have any Buyer details to process the E-Bay Order : " + OrderId;
                                    GeneralFunctions.WriteMessage(DomainID, "LOG", ErrMessage);
                                }
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = "Error adding Order Details for Order ID " + Order.OrderID + ". " + Environment.NewLine;
                    GeneralFunctions.WriteMessage(DomainID, "ERR", ErrorMsg + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// Gets the Items List and creates a New structure of Item details in order to add in BizDatabase
        /// </summary>
        /// <param name="DomainId">Merchant's BizAutomation Domain Id</param>
        /// <param name="WebApiId">Web API Id</param>
        /// <param name="WareHouseId">Deault warehouse Id</param>
        /// <param name="TransTypColl">E-Bay's Transaction Type that Contatins Order Item Details</param>
        /// <param type="Out Parameter" name="CurrencyID">CurrencyID</param>
        /// <param type="Out Parameter" name="OrderTotal">Order Total</param>
        /// <param type="Out Parameter" name="ShipCost">Shipping Cost</param>
        /// <param type="Out Parameter" name="dTotDiscount">Total Discount</param>
        /// <returns>Error Log Message if E-Bay Order Item is not found in Biz Database(Empty if the E-Bay Order Item is Mapped in Biz Database)</returns>
        private string TestBulkOrder_AddOrderItemDataset(long DomainId, int WebApiId, int WareHouseId, long RecordOwner, long DiscountItemMapping, TransactionTypeCollection TransTypColl, string ShippingServiceSelected, long ShippingServiceItemID, long SalesTaxItemMappingID,
           decimal AmountSaved, out long CurrencyID, out decimal OrderTotal, out decimal ShipCost, out decimal dTotDiscount, out decimal SalesTaxAmount)
        {
            dtOrderItems.Clear();
            dsOrderItems.Clear();
            CCommon objCommon = new CCommon();
            string Message = "";
            DataRow drItem;
            int ItemCount = 0;
            string itemCode = "";
            string ItemType = "";
            string[] itemCodeType;
            WebAPI objWebApi = new WebAPI();
            objWebApi.DomainID = DomainId;
            objWebApi.WebApiId = WebApiId;
            dTotDiscount = 0;
            decimal decDiscount = 0;
            string DiscountDesc = "";
            decimal Tax0 = 0;
            OrderTotal = 0;
            ShipCost = 0;
            CurrencyID = 0;
            SalesTaxAmount = 0;
            foreach (TransactionType TransTyp in TransTypColl)
            {
                try
                {
                    ItemCount += 1;
                    ItemType OrderItem = TransTyp.Item;
                    if (OrderItem != null)
                    {
                        if (OrderItem.SKU != "" & OrderItem.SKU != null)
                        {
                            objCommon.DomainID = DomainId;
                            objCommon.Mode = 38;
                            objCommon.Str = OrderItem.SKU;
                            itemCodeType = CCommon.ToString(objCommon.GetSingleFieldValue()).Split('~');
                            if (itemCodeType.Length > 1)
                            {
                                if (!string.IsNullOrEmpty(itemCodeType[0]))
                                {
                                    itemCode = itemCodeType[0];
                                }
                                if (!string.IsNullOrEmpty(itemCodeType[1]))
                                {
                                    ItemType = BizCommonFunctions.GetItemTypeNameByCharType(itemCodeType[1]);
                                }
                            }
                            if (itemCode == "")
                            {
                                EBayProduct objProduct = new EBayProduct();
                                DataTable dtProductDetails = new DataTable();
                                BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                                DataRow dr = dtProductDetails.NewRow();
                                objProduct = GetProductDetailForItemID(DomainId, WebApiId, OrderItem.ItemID);
                                UserAccess objUserAccess = new UserAccess();
                                objUserAccess.DomainID = DomainId;
                                DataTable dtDomainDetails = objUserAccess.GetDomainDetails();
                                int DefaultIncomeAccID = 0, DefaultCOGSAccID = 0, DefaultAssetAccID = 0;
                                if (dtDomainDetails.Rows.Count > 0)
                                {
                                    DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numIncomeAccID"]);
                                    DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numCOGSAccID"]);
                                    DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numAssetAccID"]);
                                    if (DefaultIncomeAccID == 0 || DefaultCOGSAccID == 0 || DefaultAssetAccID == 0)
                                    {
                                        Message += "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                        string ExcepitonMessage = "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                        throw new Exception(ExcepitonMessage);
                                    }
                                }
                                dr = FillEbayItemDetails(DomainId, WebApiId, RecordOwner, DefaultIncomeAccID, DefaultCOGSAccID, DefaultAssetAccID, dr, objProduct);

                                string ItemClassificationName = "Ebay_OrderItem_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                                long ItemClassificationID = 0;
                                ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);
                                itemCode = CCommon.ToString(BizCommonFunctions.AddNewItemToBiz(dr, DomainId, WebApiId, RecordOwner, WareHouseId, ItemClassificationID));
                                ItemType = BizCommonFunctions.GetItemTypeNameByCharType("P");
                                WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                                string FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml";
                                if (System.IO.File.Exists(FilePath))
                                {
                                    System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                                    XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                    objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                    objStreamReader.Close();
                                }
                                //objWebAPIItemDetail.DimensionUOM = BizCommonFunctions.GetLengthUOMCode(CCommon.ToString(dr["DimensionUOM"]));
                                //objWebAPIItemDetail.WeightUOM = BizCommonFunctions.GetWeightUOMCode(CCommon.ToString(dr["WeightUOM"]));
                                //objWebAPIItemDetail.Length = CCommon.ToString(dr["fltLength"]);
                                //objWebAPIItemDetail.Height = CCommon.ToString(dr["fltHeight"]);
                                //objWebAPIItemDetail.Width = CCommon.ToString(dr["fltWidth"]);
                                //objWebAPIItemDetail.Weight = CCommon.ToString(dr["fltWeight"]);
                                //objWebAPIItemDetail.WarrantyDescription = CCommon.ToString(dr["Warranty"]);

                                objWebAPIItemDetail.CategoryId = CCommon.ToString(dr["numEBayCategoryID"]);
                                objWebAPIItemDetail.EbayListingStyles = CCommon.ToString(dr["EbayListingType"]);
                                objWebAPIItemDetail.EbayListingDuration = CCommon.ToString(dr["EBayListingDuration"]);
                                objWebAPIItemDetail.StandardShippingCharge = CCommon.ToString(dr["EbayStandardShippingCost"]);
                                objWebAPIItemDetail.ExpressShippingCharge = CCommon.ToString(dr["EbayExpressShippingCost"]);

                                StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml");
                                XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                                Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                                objStreamWriter.Close();

                            }
                            if (itemCode != "")
                            {
                                decDiscount = 0;
                                DiscountDesc = "";
                                drItem = dtOrderItems.NewRow();
                                drItem["numoppitemtCode"] = ItemCount;
                                drItem["numItemCode"] = itemCode;

                                if (CCommon.ToInteger(TransTyp.QuantityPurchased) != 0)
                                {
                                    drItem["numUnitHour"] = TransTyp.QuantityPurchased;
                                    if (TransTyp.TransactionPrice != null)
                                    {
                                        AmountType TransTypTransactionPrice = TransTyp.TransactionPrice;
                                        drItem["monPrice"] = CCommon.ToDecimal(TransTypTransactionPrice.Value);
                                        // CCommon.ToInteger(orderItem.Quantity);
                                        OrderTotal += CCommon.ToDecimal(TransTypTransactionPrice.Value);
                                        objCommon.DomainID = DomainId;
                                        objCommon.Mode = 15;
                                        objCommon.Str = CCommon.ToString(TransTypTransactionPrice.currencyID);
                                        CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue());
                                        //drItem["monTotAmount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                        //drItem["monTotAmtBefDiscount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                        drItem["monTotAmount"] = TransTypTransactionPrice.Value * CCommon.ToInteger(TransTyp.QuantityPurchased);

                                    }

                                    if (TransTyp.ActualHandlingCost != null)
                                    {
                                        AmountType TransTypActualHandlingCost = TransTyp.ActualHandlingCost;
                                    }

                                    if (TransTyp.ActualShippingCost != null)
                                    {
                                        AmountType TransTypActualShippingCost = TransTyp.ActualShippingCost;
                                        ShipCost += CCommon.ToDecimal(TransTypActualShippingCost.Value);
                                    }

                                    if (TransTyp.AdjustmentAmount != null)
                                    {
                                        AmountType TransTypAdjustmentAmount = TransTyp.AdjustmentAmount;
                                        decDiscount += CCommon.ToDecimal(TransTypAdjustmentAmount.Value);
                                        DiscountDesc += "AdjustmentAmount fee : " + CCommon.ToString(TransTypAdjustmentAmount.Value) + Environment.NewLine;
                                    }
                                    if (TransTyp.Taxes != null)
                                    {
                                        if (TransTyp.Taxes.TotalTaxAmount != null)
                                        {
                                            AmountType TransTypTaxes = TransTyp.Taxes.TotalTaxAmount;
                                            SalesTaxAmount += CCommon.ToDecimal(TransTypTaxes.Value);
                                        }
                                    }
                                    //if (TransTyp.TotalPrice != null)
                                    //{
                                    //    AmountType TransTypTotalPrice = TransTyp.TotalPrice;
                                    //}

                                    //if (TransTyp.AmountPaid != null)
                                    //{
                                    //    AmountType TransTypTransAmountPaid = TransTyp.AmountPaid;
                                    //}

                                    drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                }
                                drItem["numUOM"] = 0; // get it from item table, in procedure
                                drItem["vcUOMName"] = "";// get it from item table, in procedure
                                drItem["UOMConversionFactor"] = 1.0000;
                                drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                                drItem["vcModelID"] = "";// get it from item table, in procedure
                                drItem["numWarehouseID"] = WareHouseId;
                                if (OrderItem.Title != null && OrderItem.Title != "")
                                {
                                    drItem["vcItemName"] = OrderItem.Title;
                                }
                                drItem["Warehouse"] = "";
                                drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                                drItem["ItemType"] = ItemType;// get it from item table, in procedure
                                drItem["Attributes"] = "";
                                drItem["Op_Flag"] = 1;
                                drItem["bitWorkOrder"] = false;
                                drItem["DropShip"] = false;
                                drItem["fltDiscount"] = decimal.Negate(decDiscount);
                                dTotDiscount += decimal.Negate(decDiscount);
                                //if (fltDiscount > 0)
                                drItem["bitDiscountType"] = 1; //Flat discount
                                //else
                                //    drItem["bitDiscountType"] = false; // Percentage

                                //if (orderItem.ItemTax != null)
                                //{
                                //    CMoney ItemTax = orderItem.ItemTax;
                                //    Tax0 = CCommon.ToDecimal(ItemTax.Amount);
                                //}

                                drItem["Tax0"] = Tax0;
                                if (Tax0 > 0)
                                    drItem["bitTaxable0"] = true;
                                else
                                    drItem["bitTaxable0"] = false;

                                //drItem["Tax0"] = 0;
                                drItem["numVendorWareHouse"] = 0;
                                drItem["numShipmentMethod"] = 0;
                                drItem["numSOVendorId"] = 0;
                                drItem["numProjectID"] = 0;
                                drItem["numProjectStageID"] = 0;
                                drItem["charItemType"] = ""; // should be taken from procedure
                                drItem["numToWarehouseItemID"] = 0;
                                //drItem["Tax41"] = 0;
                                //drItem["bitTaxable41"] = false;
                                //drItem["Tax42"] = 0;
                                //drItem["bitTaxable42"] = false;
                                drItem["Weight"] = ""; // should take from procedure 
                                drItem["WebApiId"] = WebApiId;
                                drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                                dtOrderItems.Rows.Add(drItem);
                            }
                            else
                            {
                                Message += Environment.NewLine + "Order Item " + OrderItem.Title + " (SKU: " + OrderItem.SKU + ") not found in eBay-Biz linking database ";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ex.StackTrace);
                    throw ex;
                }
            }
            if (ShipCost > 0)
            {
                ShipCost -= AmountSaved;
                OrderTotal += ShipCost;
                drItem = dtOrderItems.NewRow();
                drItem = BizCommonFunctions.GetShippingCostItem(drItem, ShipCost, ShippingServiceItemID, DomainId, WebApiId);
                dtOrderItems.Rows.Add(drItem);
            }
            if (SalesTaxAmount > 0)
            {
                OrderTotal += SalesTaxAmount;
                drItem = dtOrderItems.NewRow();
                drItem = BizCommonFunctions.GetSalesTaxItem(drItem, SalesTaxAmount, SalesTaxItemMappingID, DomainId, WebApiId);
                dtOrderItems.Rows.Add(drItem);
            }

            if (dTotDiscount > 0)
            {
                OrderTotal -= SalesTaxAmount;
                string DiscountItemName = "Total Discount";
                drItem = dtOrderItems.NewRow();
                drItem = BizCommonFunctions.GetDiscountOrderItem(drItem, dTotDiscount, DiscountItemMapping, DomainId, WebApiId, DiscountItemName);
                dtOrderItems.Rows.Add(drItem);
            }
            return Message;
        }


        #endregion Test Bulk Order Import


    }
}
