﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Data;
using System.ServiceModel;

using BACRM.BusinessLogic;
using BACRMBUSSLOGIC.BussinessLogic;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Prospects;
using BACRM.BusinessLogic.Contract;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.WebAPI;
using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.Opportunities;

using Google.GData.ContentForShopping;
using Google.GData.ContentForShopping.Elements;
using Google.GData.Client;
using GCheckout;
using GCheckout.OrderProcessing;
using GCheckout.Util;
using GCheckout.Checkout;
using GCheckout.AutoGen;
using GCheckout.AutoGen.Extended;
using GCheckout.MerchantCalculation;

using MarketplaceService.Google_Communicator;

namespace MarketplaceService
{
    class GoogleClient
    {
        #region Members

        private string OrderFilePath = ConfigurationManager.AppSettings["GoogleOrderFilePath"];
        private string ClosedOrders = ConfigurationManager.AppSettings["GoogleClosedOrders"];
        private string GoogleEnvironment = ConfigurationManager.AppSettings["GoogleEnvironment"];

        DataSet ds = new DataSet();
        DataSet dsItems = new DataSet();
        
        DataTable dtItems = new DataTable();
        DataTable dtCompanyTaxTypes = new DataTable();
        DataTable dtItemSpecifics = new DataTable();
        DataRow drtax = null;

        DataSet dsOrderItems = new DataSet();
        DataTable dtOrderItems = new DataTable();


        long lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId;
        OppBizDocs objOppBizDocs = new OppBizDocs();
        string strdetails = "";
        string[] arrOutPut;
        string[] arrBillingIDs;

        #endregion Members

        #region Product Operations

        /// <summary>
        /// Sends Batch Item Details to Add/Update to Google Merchant Center
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="dtNewItems">New/Modified Item details</param>
        /// <param name="AccountId">Google Merchant Account Id</param>
        /// <param name="Email">Gmail Id</param>
        /// <param name="Password">Password for Merchant email account</param>
        /// <param name="TargetCountry"></param>
        public void NewProductBatchProcessing(long DomainId, int WebApiId, DataTable dtNewItems, string AccountId, string Email, string Password, string TargetCountry)
        {
            string ErrorMessage = "", LogMessage = "";
            WebAPI objWebAPI = new WebAPI();
            List<Product> lstProducts = new List<Product>();

            foreach (DataRow dr in dtNewItems.Rows)
            {
                try
                {

                    if (CCommon.ToString(dr["vcItemName"]).Length > 0 & CCommon.ToString(dr["numItemCode"]).Length > 0)
                    {
                        Product objProduct = new Product();
                        string ListPrice = CCommon.ToString(dr["monListPrice"]);

                        if (CCommon.ToString(dr["vcSKU"]).Length > 0 & CCommon.ToString(dr["vcItemName"]).Length > 0)
                        {
                            WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                            string WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";
                            if (File.Exists(WebApi_ItemDetails))
                            {
                                StreamReader objStreamReader = new StreamReader(WebApi_ItemDetails);
                                XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                objStreamReader.Close();
                            }
                            //ProductEntry entry = new ProductEntry();
                            objProduct.BizItemCode = CCommon.ToString(dr["numItemCode"]);
                            objProduct.Title = CCommon.ToString(dr["vcItemName"]);
                            objProduct.Description = CCommon.ToString(dr["txtItemDesc"]);
                            objProduct.ProductId = CCommon.ToString(dr["vcSKU"]);
                            objProduct.ModelNumber = CCommon.ToString(dr["vcModelID"]);

                            if (objWebAPIItemDetail.GoogleProductCategory != null && objWebAPIItemDetail.GoogleProductCategory != "")
                            {
                                objProduct.GoogleProductCategory = objWebAPIItemDetail.GoogleProductCategory;
                            }
                            else
                            {
                                ErrorMessage = "Google Product Category is not found for Item SKU : " + objProduct.ProductId + " ,Name : " + objProduct.Title;
                                throw new Exception(ErrorMessage);
                            }

                            if (objWebAPIItemDetail.GoogleProductType != null && objWebAPIItemDetail.GoogleProductType != "")
                            {
                                objProduct.ProductType = objWebAPIItemDetail.GoogleProductType;
                            }
                            else
                            {
                                ErrorMessage = "Google Product Type is not found for Item SKU : " + objProduct.ProductId + " ,Name : " + objProduct.Title;
                                throw new Exception(ErrorMessage);
                            }

                            if (objWebAPIItemDetail.GoogleProductURL != null && objWebAPIItemDetail.GoogleProductURL != "")
                            {
                                objProduct.ProductURL = objWebAPIItemDetail.GoogleProductURL;
                            }
                            else
                            {
                                ErrorMessage = "Google Product URL is not defined for Item SKU : " + objProduct.ProductId + " ,Name : " + objProduct.Title;
                                throw new Exception(ErrorMessage);
                            }

                            if (objWebAPIItemDetail.ProductImageLink != null && objWebAPIItemDetail.ProductImageLink != "")
                            {
                                objProduct.ProductImageURL = objWebAPIItemDetail.ProductImageLink;
                            }
                            else
                            {
                                ErrorMessage = "Google Product image URL is not defined for Item SKU : " + objProduct.ProductId + " ,Name : " + objProduct.Title;
                                throw new Exception(ErrorMessage);
                            }
                            objProduct.TargetCountry = TargetCountry;
                            objProduct.UPC = CCommon.ToString(dr["numBarCodeId"]);
                            objProduct.Availability = "In Stock";
                            objProduct.Quantity = (CCommon.ToString(dr["QtyOnHand"]) == "" ? "0" : CCommon.ToString(dr["QtyOnHand"]));
                            objProduct.Manufacturer = CCommon.ToString(dr["vcManufacturer"]);
                            objProduct.Price = ListPrice;

                            if (!string.IsNullOrEmpty(objWebAPIItemDetail.Weight))
                            {
                                objProduct.Weight = objWebAPIItemDetail.Weight;
                            }
                            if (!string.IsNullOrEmpty(CCommon.ToString(dr["fltWeight"])))
                            {
                                objProduct.ShippingWeight = CCommon.ToString(dr["fltWeight"]);
                                objProduct.WeightUOM = (objWebAPIItemDetail.WeightUOM != null && objWebAPIItemDetail.WeightUOM != "") ? objWebAPIItemDetail.WeightUOM : "oz";
                            }
                            string StandardShippingCharge;

                            if (!string.IsNullOrEmpty(objWebAPIItemDetail.StandardShippingCharge))
                            {
                                StandardShippingCharge = objWebAPIItemDetail.StandardShippingCharge;
                                objProduct.StandardShippingCharge = objWebAPIItemDetail.StandardShippingCharge;
                            }
                            else
                            {
                                ErrorMessage = "Google Standard Shipping Charge not defined for Item SKU : " + objProduct.ProductId + " ,Name : " + objProduct.Title;
                                throw new Exception(ErrorMessage);
                            }

                            lstProducts.Add(objProduct);
                        }
                        else
                        {
                            LogMessage = "Item Name and Item SKU cannot be empty. Verify Item Details for Item Code " + CCommon.ToString(dr["numItemCode"]);
                            GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                        }
                    }
                    else
                    {
                        LogMessage = "Item Name cannot be empty. Verify Item Details for Item Code " + CCommon.ToString(dr["numItemCode"]);
                        GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                }
            }
            try
            {
                GoogleCommunicatorClient objGoogleCommunicatorClient = new GoogleCommunicatorClient();
                objGoogleCommunicatorClient.NewProductBatchProcessing(AccountId, Email, Password, lstProducts.ToArray());
                objWebAPI.UserCntID = BizCommonFunctions.GetRoutingRulesUserCntID(DomainId, "", "");
                foreach (Product objProduct in lstProducts)
                {
                    LogMessage = "Item Details for Item Code : " + objProduct.BizItemCode + ", Item SKU : " + objProduct.ProductId + " ,Name : " + objProduct.Title + " is Added in Google Product Shopping Listings";
                    GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                    objWebAPI.ItemCode = CCommon.ToLong(objProduct.BizItemCode);
                    objWebAPI.Product_Id = objProduct.ProductId;
                    objWebAPI.DomainID = DomainId;
                    objWebAPI.WebApiId = WebApiId;
                    objWebAPI.AddItemAPILinking();
                    LogMessage = "Google product Id " + objProduct.ProductId + " is mapped in (BizDatabase) ItemAPI Details for  Item Code : " + objProduct.BizItemCode + " ,Name : " + objProduct.Title;
                    GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                }
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Service : " + er.msg;
                GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {
                GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DomainId"></param>
        /// <param name="WebApiId"></param>
        /// <param name="RecordOwner"></param>
        /// <param name="UserContactID"></param>
        /// <param name="WareHouseID"></param>
        /// <param name="AccountId"></param>
        /// <param name="Email"></param>
        /// <param name="Password"></param>
        public void ImportProductListing(long DomainId, int WebApiId, long RecordOwner, long UserContactID, int WareHouseID, string AccountId, string Email, string Password)
        {
            string LogMessage = "", ErrorMessage = "";
            DataTable dtProductDetails = new DataTable();
            CCommon objCommon = new CCommon();
            List<Product> lstProducts = new List<Product>();
            try
            {
                GoogleCommunicatorClient objClient = new GoogleCommunicatorClient();
                lstProducts = objClient.GetProductListings(AccountId, Email, Password).OfType<Product>().ToList();
                objClient.Close();

                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = DomainId;
                DataTable dtDomainDetails = objUserAccess.GetDomainDetails();
                int DefaultIncomeAccID = 0, DefaultCOGSAccID = 0, DefaultAssetAccID = 0;

                if (dtDomainDetails.Rows.Count > 0)
                {
                    DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numIncomeAccID"]);
                    DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numCOGSAccID"]);
                    DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numAssetAccID"]);
                    if (DefaultIncomeAccID == 0 || DefaultCOGSAccID == 0 || DefaultAssetAccID == 0)
                    {
                        string ExcepitonMessage = "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                        throw new Exception(ExcepitonMessage);
                    }
                }
                BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                foreach (Product objProduct in lstProducts)
                {
                    try
                    {
                        DataRow dr = dtProductDetails.NewRow();
                        dr = FillGoogleItemDetails(DomainId, WebApiId, RecordOwner, DefaultIncomeAccID, DefaultCOGSAccID, DefaultAssetAccID, dr, objProduct);
                        dtProductDetails.Rows.Add(dr);
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.StackTrace);
                    }
                }

                DataTable dtItemDetails = new DataTable();
                DataRow drMailItemDetail;
                BizCommonFunctions.CreateMailMessageItemDetail(dtItemDetails);
                int ItemCount = 0;

                string ItemClassificationName = "Magento_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                long ItemClassificationID = 0;
                ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);

                foreach (DataRow drItem in dtProductDetails.Rows)
                {
                    try
                    {
                        long ItemCode =  BizCommonFunctions.AddNewItemToBiz(drItem, DomainId, WebApiId, RecordOwner, WareHouseID, ItemClassificationID);
                        ItemCount += 1;
                        drMailItemDetail = dtItemDetails.NewRow();
                        drMailItemDetail["SNo"] = CCommon.ToString(ItemCount);
                        drMailItemDetail["BizItemCode"] = CCommon.ToString(ItemCode);
                        drMailItemDetail["ItemName"] = drItem["vcItemName"];
                        drMailItemDetail["SKU"] = drItem["vcSKU"];
                        dtItemDetails.Rows.Add(drMailItemDetail);

                        LogMessage = "Added/Updated New Item in BizAutomation, Item Code : " + ItemCode + ", imported from Magento Online Marketplace ";
                        GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                        WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                        string FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml";
                        if (System.IO.File.Exists(FilePath))
                        {
                            System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                            XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                            objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                            objStreamReader.Close();
                        }

                        objWebAPIItemDetail.GoogleProductCategory = CCommon.ToString(drItem["GoogleProductCategory"]);
                        objWebAPIItemDetail.GoogleProductType = CCommon.ToString(drItem["GoogleProductType"]);
                        objWebAPIItemDetail.ProductImageLink = CCommon.ToString(drItem["ProductImageLink"]);
                        objWebAPIItemDetail.GoogleProductURL = CCommon.ToString(drItem["GoogleProductURL"]);

                        // objWebAPIItemDetail.MsrPrice = CCommon.ToString(drItem["MSRPrice"]);

                        StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml");
                        XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                        Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                        objStreamWriter.Close();
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ex.StackTrace);
                    }
                }
                BizCommonFunctions.SendMailNewItemList(DomainId, WebApiId, RecordOwner, UserContactID, ItemCount, dtItemDetails, ItemClassificationName, "Google");

            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Google Service : " + er.msg;
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                throw new Exception(ErrMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        /// <summary>
        /// Fills(maps) Google Item Detail to BizAutomation Item Detail
        /// </summary>
        /// <param name="DomainId">Domain Id</param>
        /// <param name="WebApiId">WebApi Id</param>
        /// <param name="RecordOwner">Record Owner</param>
        /// <param name="DefaultIncomeAccID">Default Income Account ID</param>
        /// <param name="DefaultCOGSAccID">Default COGS Account ID</param>
        /// <param name="DefaultAssetAccID">Default Asset account ID</param>
        /// <param name="dr">BizAutomation Item Structire</param>
        /// <param name="objMagentoProduct">Google Item Detail</param>
        /// <returns></returns>
        public DataRow FillGoogleItemDetails(long DomainId, int WebApiId, long RecordOwner, int DefaultIncomeAccID, int DefaultCOGSAccID, int DefaultAssetAccID, DataRow dr, Product objProduct)
        {
            try
            {
                double MSRPrice = 0;
                double SalePrice = 0;
                dr["numItemCode"] = 0;
                dr["vcItemName"] = objProduct.Title;
                dr["txtItemDesc"] = objProduct.Description;
                dr["charItemType"] = "P";
                dr["monListPrice"] = CCommon.ToDouble(objProduct.Price);
                dr["txtItemHtmlDesc"] = objProduct.Description; ;
                dr["bitTaxable"] = 0;
                dr["vcSKU"] = objProduct.ProductId;
                dr["numQuantity"] = CCommon.ToInteger(CCommon.ToDecimal(objProduct.Quantity));
                dr["bitKitParent"] = 0;
                dr["numDomainID"] = DomainId;
                dr["numUserCntID"] = RecordOwner;
                dr["numVendorID"] = 0;
                dr["bitSerialized"] = 0;
                dr["strFieldList"] = 0;
                dr["IsArchieve"] = 0;
                dr["vcModelID"] = objProduct.ModelNumber;
                dr["numItemGroup"] = 0;
                dr["numCOGSChartAcntId"] = DefaultCOGSAccID;
                dr["numAssetChartAcntId"] = DefaultAssetAccID;
                dr["numIncomeChartAcntId"] = DefaultIncomeAccID;
                dr["monAverageCost"] = 0;
                dr["monTotAmtBefDiscount"] = 0;
                dr["monLabourCost"] = 0;
                dr["fltWeight"] = objProduct.Weight;
                dr["fltHeight"] = objProduct.Height;
                dr["fltLength"] = objProduct.Length;
                dr["fltWidth"] = objProduct.Width;
                dr["fltShippingWeight"] = objProduct.ShippingWeight;
                dr["fltShippingHeight"] = "";
                dr["fltShippingLength"] = "";
                dr["fltShippingWidth"] = "";
                dr["DimensionUOM"] = "";
                dr["WeightUOM"] = "";
                dr["intWebApiId"] = WebApiId;
                dr["vcApiItemId"] = objProduct.ProductId;
                dr["numBarCodeId"] = 0;
                dr["vcManufacturer"] = objProduct.Manufacturer;
                dr["vcExportToAPI"] = WebApiId;
                dr["Warranty"] = "";
                dr["numEBayCategoryID"] = "";
                dr["EbayListingType"] = "";
                dr["EBayListingDuration"] = "";
                dr["EbayStandardShippingCost"] = "";
                dr["EbayExpressShippingCost"] = 0;
                dr["MagentoAttributeSetID"] = "";
                dr["MagentoProductType"] = "";
                dr["GoogleProductCategory"] = objProduct.GoogleProductCategory;
                dr["GoogleProductType"] = objProduct.ProductType;
                dr["GoogleProductURL"] = objProduct.ProductURL;
                dr["ProductImageLink"] = objProduct.ProductImageURL;
                dr["MSRPrice"] = MSRPrice;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dr;
        }

        public Product GetProductDetailBySKU(long DomainId, int WebApiId, string SKU)
        {
            WebAPI objWebAPi = new WebAPI();
            string LogMessage = "", ErrorMessage = "";
            CCommon objCommon = new CCommon();
            Product objProduct = new Product();
            try
            {
                objWebAPi.DomainID = DomainId;
                objWebAPi.WebApiId = WebApiId;
                objWebAPi.Mode = 3;
                DataTable dtWebAPI = objWebAPi.GetWebApi();
                foreach (DataRow drWebApi in dtWebAPI.Rows)
                {
                    if (CCommon.ToBool(drWebApi["bitEnableAPI"]) == true)
                    {
                        if (WebApiId == CCommon.ToInteger(drWebApi["WebApiId"]))//Amazon US
                        {
                            string TargetCountry = CCommon.ToString(drWebApi["vcFirstFldValue"]);
                            string AccountId = CCommon.ToString(drWebApi["vcSecondFldValue"]);
                            string EMail = CCommon.ToString(drWebApi["vcThirdFldValue"]);
                            string Password = CCommon.ToString(drWebApi["vcFourthFldValue"]);

                            GoogleCommunicatorClient objClient = new GoogleCommunicatorClient();
                            objProduct = objClient.GetProductById(AccountId, EMail, Password,TargetCountry, SKU);
                            objClient.Close();
                        }
                    }
                }
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Google Service : No Item details found in Google Merchant Center account for Item SKU : " + SKU + ". " + er.msg;
                throw new Exception(ErrMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return objProduct;
        }

        #endregion Product Operations

        #region Inventory Management

        /// <summary>
        /// Updates Invertory detail to Google Merchant Center
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="dtNewItems">modified Inventory value</param>
        /// <param name="AccountId">Google Merchant Account Id</param>
        /// <param name="Email">Merchant Gmail ID</param>
        /// <param name="Password">Merchant Mail id Password</param>
        /// <param name="TargetCountry"></param>
        public void UpdateInventoryBatchProcessing(long DomainId, int WebApiId, DataTable dtNewItems, string AccountId, string Email, string Password, string TargetCountry)
        {
            string ErrorMessage = "";
            WebAPI objWebAPI = new WebAPI();

            List<Product> lstProducts = new List<Product>();

            foreach (DataRow dr in dtNewItems.Rows)
            {
                try
                {

                    if (CCommon.ToString(dr["vcItemName"]).Length > 0 & CCommon.ToString(dr["numItemCode"]).Length > 0)
                    {
                        Product objProduct = new Product();

                        string ListPrice = CCommon.ToString(dr["monListPrice"]);

                        if (CCommon.ToString(dr["vcSKU"]).Length > 0 & CCommon.ToString(dr["vcItemName"]).Length > 0)
                        {
                            WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                            string WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";
                            if (File.Exists(WebApi_ItemDetails))
                            {
                                StreamReader objStreamReader = new StreamReader(WebApi_ItemDetails);
                                XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                objStreamReader.Close();
                            }

                            objProduct.ProductId = CCommon.ToString(dr["vcSKU"]);
                            objProduct.Quantity = (CCommon.ToString(dr["QtyOnHand"]) == "" ? "0" : CCommon.ToString(dr["QtyOnHand"]));
                            objProduct.Price = ListPrice;
                            objProduct.Title = CCommon.ToString(dr["vcItemName"]);
                            objProduct.Description = CCommon.ToString(dr["txtItemDesc"]);
                            objProduct.ModelNumber = CCommon.ToString(dr["vcModelID"]);

                            if (!string.IsNullOrEmpty(objWebAPIItemDetail.GoogleProductURL))
                            {
                                objProduct.ProductURL = objWebAPIItemDetail.GoogleProductURL;
                            }
                            else
                            {
                                ErrorMessage = "Google Product URL is not defined for Item SKU : " + objProduct.ProductId + " ,Name : " + objProduct.Title;
                                throw new Exception(ErrorMessage);
                            }
                            lstProducts.Add(objProduct);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorMessage = "Error in reading Item Quantity for Product Item Code : " + CCommon.ToString(dr["numItemCode"]) + ",Item Name : " + CCommon.ToString(dr["vcItemName"])
                        + Environment.NewLine + ex.Message;
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ErrorMessage);
                }
            }
            try
            {
                GoogleCommunicatorClient objGoogleCommunicatorClient = new GoogleCommunicatorClient();
                objGoogleCommunicatorClient.UpdateInventoryBatchProcessing(AccountId, Email, Password, lstProducts.ToArray());
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Service : " + er.msg;
                GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
        }

        #endregion Inventory Management

        #region Order Processing Functions

        /// <summary>
        /// Gets Orders Notifications from Google for the specified time limit
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="MerchantId">MerchantId</param>
        /// <param name="MerchantKey">MerchantKey</param>
        /// <param name="MerchantToken">MerchantToken</param>
        public void GetGoogleOrders(long DomainId, int WebApiId, string MerchantId, string MerchantKey, string MerchantToken)
        {
            WebAPI objWebAPI = new WebAPI();
            CCommon objCommon = new CCommon();
            //MerchantId = "928843481676221";
            //  MerchantKey = "7Fy0w_Y5SsEwfMvvMdya8w";
            // string Environment = "Production";

            try
            {
                if (MerchantToken == "" || MerchantToken == null)
                {
                    GoogleCommunicatorClient objGoogleCommunicatorClient = new GoogleCommunicatorClient();
                    MerchantToken = objGoogleCommunicatorClient.RequestMerchantToken(MerchantId, MerchantKey, GoogleEnvironment);
                    objCommon.DomainID = DomainId;
                    objCommon.Mode = 25;
                    objCommon.UpdateRecordID = WebApiId;
                    objCommon.Comments = MerchantToken;
                    objCommon.UpdateSingleFieldValue();
                }
                else
                {
                    MemoryStream ms = new MemoryStream();
                    GCheckout.AutoGen.NotificationDataResponse NotificationResponse = new GCheckout.AutoGen.NotificationDataResponse();

                    GoogleCommunicatorClient objGoogleCommunicatorClient = new GoogleCommunicatorClient();
                    ms = objGoogleCommunicatorClient.GetOrderNotifications(MerchantId, MerchantKey, GoogleEnvironment, MerchantToken);
                    XmlSerializer serializer = new XmlSerializer(typeof(GCheckout.AutoGen.NotificationDataResponse));
                    XmlReader Reader = new XmlTextReader(ms);
                    NotificationResponse = (GCheckout.AutoGen.NotificationDataResponse)serializer.Deserialize(Reader);

                    int i = 0;
                    //GeneralFunctions.WriteMessage(DomainId, "LOG", "Merchant Token : " + MerchantToken + ", Continue Token : " + NotificationResponse.continuetoken);

                    if (NotificationResponse.notifications.Items != null)
                    {
                        foreach (Object notification in NotificationResponse.notifications.Items)
                        {
                            i = i + 1;
                            //ProcessNotification(notification, i);
                            string NotificationType = CCommon.ToString(notification.GetType());
                            if (NotificationType == "GCheckout.AutoGen.ChargeAmountNotification")
                            {
                                ChargeAmountNotification N1 = (ChargeAmountNotification)notification;
                                string Filename = GeneralFunctions.GetPath(OrderFilePath, DomainId) + N1.googleordernumber + ".xml";
                                GeneralFunctions.SerializeObjectToFile<GCheckout.AutoGen.ChargeAmountNotification>(N1, Filename);
                            }
                        }
                    }
                    objCommon.DomainID = DomainId;
                    objCommon.Mode = 25;
                    objCommon.UpdateRecordID = WebApiId;
                    objCommon.Comments = NotificationResponse.continuetoken;
                    objCommon.UpdateSingleFieldValue();

                }
                //XmlSerializer serializer = new XmlSerializer(typeof(OrderTypeCollection));
                //XmlReader Reader = new XmlTextReader(ms);
                //orders = (OrderTypeCollection)serializer.Deserialize(Reader);
                //if (orders.Count > 0)
                //    GeneralFunctions.SerializeObjectToFile(orders, GeneralFunctions.GetPath(OrderFilePath, DomainId) + "orders" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                //objWebAPI.DomainID = DomainId;
                //objWebAPI.WebApiId = WebApiId;
                //objWebAPI.UpdateAPISettingsDate(1);
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Service : " + er.msg;
                GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Process Google CheckOut Order Details
        /// </summary>
        /// <param name="Gorder">Google Order Object</param>
        /// <param name="DomainID">Domain ID</param>
        /// <param name="WebApiId">WebApi Id</param>
        /// <param name="Source">Source</param>
        /// <param name="numWareHouseID">default WareHouse ID</param>
        /// <param name="numRecordOwner">default Record Owner</param>
        /// <param name="numAssignTo">default Assign To</param>
        /// <param name="numRelationshipId">default Relationship Id</param>
        /// <param name="numProfileId">default Profile Id</param>
        /// <param name="numBizDocId">default BizDoc Id</param>
        /// <param name="numOrderStatus">Order Status</param>
        /// <param name="ExpenseAccountId">Expense Account Id</param>
        /// <param name="DiscountItemMapping">Discount Item Mapping</param>
        public void ProcessGoogleCheckOutOrders(ChargeAmountNotification Gorder, long DomainID, int WebApiId, string Source,
                                                int numWareHouseID, long numRecordOwner, long numAssignTo, long numRelationshipId, long numProfileId,
            long numBizDocId, long BizDocStatusId, int numOrderStatus, int ExpenseAccountId, long DiscountItemMapping, long ShippingServiceItemID, long SalesTaxItemMappingID,string ShipToPhoneNo)
        {
            string LogMessage = "", ErrorMsg = "";
            decimal SaleTaxRate = 0;
            CContacts objContacts = null;
            CLeads objLeads = null;
            ImportWizard objImpWzd = new ImportWizard();
            MOpportunity objOpportunity = new MOpportunity();
            CCommon objCommon = new CCommon();
            decimal TransactionFee = 0;
            string OrderId = Gorder.googleordernumber;
            OrderSummary OrderDetails = new OrderSummary();
            OrderDetails = Gorder.ordersummary;

            if (Gorder.latestchargefee.total != null)
            {
                Money MoneyTransFee = new Money();
                MoneyTransFee = Gorder.latestchargefee.total;
                TransactionFee = MoneyTransFee.Value;

            }
            //Address ShippingAddress = new Address();
            //ShippingAddress = OrderDetails.buyershippingaddress;

            //string Email = ShippingAddress.email;

            WebAPI objWebApi = new WebAPI();

            string ItemMessage = "";
            decimal ShipCost = 0;
            decimal OrderTotal = 0;
            long CurrencyID = 0;
            decimal dTotDiscount = 0;
            decimal SalesTaxAmount = 0;
            if (!string.IsNullOrEmpty(OrderId))
            {
                objWebApi.DomainID = DomainID;
                objWebApi.WebApiId = WebApiId;
                objWebApi.vcAPIOppId = OrderId;
                if (objWebApi.CheckDuplicateAPIOpportunity())
                {

                    int a = -1;
                    string AddressStreet = "";//To concatenate Address Lines
                    objContacts = new CContacts();
                    objLeads = new CLeads();
                    objLeads.DomainID = DomainID;

                    Address ShippingAddress = new Address();
                    ShippingAddress = OrderDetails.buyershippingaddress;
                    string Email = ShippingAddress.email;

                    string CompanyName = ShippingAddress.companyname;
                    LogMessage += Environment.NewLine + " CompanyName : " + CompanyName;

                    string FirstName = ShippingAddress.structuredname.firstname;
                    LogMessage += Environment.NewLine + " FirstName : " + FirstName;

                    string LastName = ShippingAddress.structuredname.lastname;
                    LogMessage += Environment.NewLine + " LastName : " + LastName;

                    string Name = ShippingAddress.contactname;
                    LogMessage += Environment.NewLine + " Name : " + Name;

                    string BuyerUserID = CCommon.ToString(OrderDetails.buyerid);
                    LogMessage += Environment.NewLine + " BuyerUserID : " + BuyerUserID;

                    if (!string.IsNullOrEmpty(Email))
                    {
                        objLeads.Email = Email;
                        //Check for Details for the Given DomainID and EmailID
                        objLeads.GetConIDCompIDDivIDFromEmail();
                    }
                    else
                    {
                        objLeads.Email = "";
                    }
                    if (objLeads.ContactID > 0 && objLeads.DivisionID > 0)
                    {
                        //If Email already registered
                        lngCntID = objLeads.ContactID;
                        lngDivId = objLeads.DivisionID;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ShippingAddress.contactname))
                        {
                            objLeads.CompanyName = ShippingAddress.contactname;
                            objLeads.CustName = ShippingAddress.contactname;
                        }
                        else if (!string.IsNullOrEmpty(ShippingAddress.contactname))
                        {
                            objLeads.CompanyName = ShippingAddress.contactname;
                            objLeads.CustName = ShippingAddress.contactname;
                        }
                        if (ShippingAddress != null)
                        {
                            if (!string.IsNullOrEmpty(ShippingAddress.countrycode))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 22;
                                objCommon.Str = CCommon.ToString(ShippingAddress.countrycode);
                                objLeads.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objLeads.SCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }
                            if (ShippingAddress.structuredname != null)
                            {
                                objLeads.FirstName = ShippingAddress.structuredname.firstname;
                                objLeads.LastName = ShippingAddress.structuredname.lastname;

                            }
                            else if (!string.IsNullOrEmpty(ShippingAddress.contactname))
                            {
                                a = ShippingAddress.contactname.IndexOf('.');
                                if (a == -1)
                                    a = ShippingAddress.contactname.IndexOf(' ');
                                if (a == -1)
                                {
                                    objLeads.FirstName = ShippingAddress.contactname;
                                    objLeads.LastName = ShippingAddress.contactname;
                                }
                                else
                                {
                                    objLeads.FirstName = ShippingAddress.contactname.Substring(0, a);
                                    objLeads.LastName = ShippingAddress.contactname.Substring(a + 1, ShippingAddress.contactname.Length - (a + 1));
                                }
                            }
                            if (!string.IsNullOrEmpty(ShippingAddress.phone))
                            {
                                objLeads.ContactPhone = ShippingAddress.phone;
                                objLeads.PhoneExt = "";
                            }
                            else
                            {
                                objLeads.ContactPhone = ShipToPhoneNo;
                                objLeads.PhoneExt = "";
                            }
                            if (!string.IsNullOrEmpty(ShippingAddress.fax))
                            {
                                objLeads.Fax = ShippingAddress.fax;
                            }
                        }

                        objLeads.UserCntID = numRecordOwner;
                        objLeads.ContactType = 70;
                        objLeads.PrimaryContact = true;
                        objLeads.UpdateDefaultTax = false;
                        objLeads.NoTax = false;
                        objLeads.CRMType = 1;
                        objLeads.DivisionName = "-";

                        objLeads.CompanyType = numRelationshipId;
                        objLeads.Profile = numProfileId;

                        objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();//Creates Company Record
                        LogMessage = "New Company details added CompanyID : " + objLeads.CompanyID + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        lngDivId = objLeads.CreateRecordDivisionsInfo();
                        LogMessage = "New Divisions Informations added. Division Id : " + lngDivId + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        objLeads.ContactID = 0;
                        objLeads.DivisionID = lngDivId;
                        lngCntID = objLeads.CreateRecordAddContactInfo();//Creates Contact Info
                        LogMessage = "New Contact Informations added. Contact Id : " + lngCntID + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        if (ShippingAddress != null)
                        {
                            objContacts.FirstName = objLeads.FirstName;
                            objContacts.LastName = objLeads.LastName;
                            objContacts.ContactPhone = objLeads.ContactPhone;
                            objContacts.Email = objLeads.Email;

                            if (!string.IsNullOrEmpty(ShippingAddress.address1))
                                AddressStreet = ShippingAddress.address1;

                            if (!string.IsNullOrEmpty(ShippingAddress.address2))
                                AddressStreet = AddressStreet + " " + ShippingAddress.address2;

                            objContacts.BillStreet = AddressStreet;
                            objContacts.ShipStreet = AddressStreet;

                            if (!string.IsNullOrEmpty(ShippingAddress.city))
                            {
                                objContacts.BillCity = ShippingAddress.city;
                                objContacts.ShipCity = ShippingAddress.city;
                            }

                            if (!string.IsNullOrEmpty(ShippingAddress.region))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 21;
                                objCommon.Str = ShippingAddress.region;
                                objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }

                            if (!string.IsNullOrEmpty(ShippingAddress.countrycode))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 22;
                                objCommon.Str = CCommon.ToString(ShippingAddress.countrycode);
                                objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }

                            if (!string.IsNullOrEmpty(ShippingAddress.postalcode))
                            {
                                objContacts.BillPostal = ShippingAddress.postalcode;
                                objContacts.ShipPostal = ShippingAddress.postalcode;
                            }
                            objContacts.BillingAddress = 0;
                            objContacts.DivisionID = lngDivId;
                            objContacts.ContactID = lngCntID;
                            objContacts.RecordID = lngDivId;
                            objContacts.DomainID = DomainID;
                            objContacts.IsPrimaryAddress = CCommon.ToBool(1);
                            objContacts.UpdateCompanyAddress();
                        }
                    }
                    objOpportunity.OppRefOrderNo = Gorder.googleordernumber;
                    objOpportunity.MarketplaceOrderID = Gorder.googleordernumber;
                    //CreateItemTable();
                    BizCommonFunctions.CreateItemTable(dtItems);
                    ItemMessage = AddOrderItemDataset(DomainID, WebApiId, numRecordOwner,numWareHouseID, OrderDetails, ShippingServiceItemID, SalesTaxItemMappingID, out CurrencyID, out OrderTotal, out ShipCost, out dTotDiscount, TransactionFee, DiscountItemMapping, out SaleTaxRate, out SalesTaxAmount);

                    objOpportunity.CurrencyID = CurrencyID;
                    objOpportunity.Amount = OrderTotal;
                    //objContacts.BillingAddress = 0;
                    //objContacts.DivisionID = lngDivId;
                    //objContacts.UpdateCompanyAddress();//Updates Company Details
                    // SaveTaxTypes();
                    BizCommonFunctions.SaveTaxTypes(ds, lngDivId);
                    arrBillingIDs = new string[4];
                    arrBillingIDs[0] = CCommon.ToString(objLeads.CompanyID);
                    arrBillingIDs[1] = CCommon.ToString(lngDivId);
                    arrBillingIDs[2] = CCommon.ToString(lngCntID);
                    arrBillingIDs[3] = objLeads.CompanyName;
                    if (lngCntID == 0)
                        return;
                    // }
                    //Create Opportunity Details
                    DateTime OrderDate = OrderDetails.purchasedate;
                    objOpportunity.MarketplaceOrderDate = OrderDate;
                    objOpportunity.OpportunityId = 0;
                    objOpportunity.ContactID = lngCntID;
                    objOpportunity.DivisionID = lngDivId;
                    objOpportunity.UserCntID = numRecordOwner;
                    objOpportunity.AssignedTo = numAssignTo;

                    objOpportunity.OpportunityName = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM");
                    objOpportunity.EstimatedCloseDate = DateTime.Now;
                    objOpportunity.PublicFlag = 0;
                    objOpportunity.DomainID = DomainID;
                    objOpportunity.OppType = 1;
                    objOpportunity.OrderStatus = numOrderStatus;

                    dsItems.Tables.Clear();
                    dsItems.Tables.Add(dtItems.Copy());
                    if (dsItems.Tables[0].Rows.Count != 0)
                        objOpportunity.strItems = ((dtItems.Rows.Count > 0) ? ("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsItems.GetXml()) : "");
                    else
                        objOpportunity.strItems = "";
                    objOpportunity.OppComments = ItemMessage;
                    objOpportunity.Source = WebApiId;
                    objOpportunity.SourceType = 3;
                    objOpportunity.DealStatus = (long)MOpportunity.EnmDealStatus.DealWon;
                    arrOutPut = objOpportunity.Save();
                    lngOppId = CCommon.ToLong(arrOutPut[0]);

                    //Save Source 
                    objOpportunity.WebApiId = WebApiId;
                    objOpportunity.vcSource = Source;
                    objOpportunity.OpportunityId = lngOppId;
                    objOpportunity.ManageOppLinking();

                    objOpportunity.CompanyID = lngDivId;

                    if (ShippingAddress != null)
                    {
                        if (!string.IsNullOrEmpty(ShippingAddress.companyname))
                        {
                            objOpportunity.BillCompanyName = ShippingAddress.companyname;
                        }

                        if (!string.IsNullOrEmpty(ShippingAddress.contactname))
                        {
                            objOpportunity.AddressName = ShippingAddress.contactname;
                        }

                        if (!string.IsNullOrEmpty(ShippingAddress.address1))
                        {
                            AddressStreet = ShippingAddress.address1;
                        }

                        if (!string.IsNullOrEmpty(ShippingAddress.address2))
                        {
                            AddressStreet = AddressStreet + " " + ShippingAddress.address2;
                        }

                        objContacts.BillStreet = AddressStreet;
                        objContacts.ShipStreet = AddressStreet;
                        objOpportunity.BillStreet = AddressStreet;

                        if (!string.IsNullOrEmpty(ShippingAddress.city))
                        {
                            objContacts.BillCity = ShippingAddress.city;
                            objContacts.ShipCity = ShippingAddress.city;
                            objOpportunity.BillCity = ShippingAddress.city;
                        }
                        if (!string.IsNullOrEmpty(ShippingAddress.region))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 21;
                            objCommon.Str = ShippingAddress.region;
                            objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (!string.IsNullOrEmpty(ShippingAddress.countrycode))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 22;
                            objCommon.Str = CCommon.ToString(ShippingAddress.countrycode);
                            objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }

                        if (!string.IsNullOrEmpty(ShippingAddress.postalcode))
                        {
                            objContacts.BillPostal = ShippingAddress.postalcode;
                            objContacts.ShipPostal = ShippingAddress.postalcode;
                            objOpportunity.BillPostal = ShippingAddress.postalcode;
                        }
                        objOpportunity.Mode = 0;
                        objOpportunity.UpdateOpportunityAddress();
                        objOpportunity.Mode = 1;
                        objOpportunity.UpdateOpportunityAddress();
                    }

                    LogMessage = "Order Details is added, Google OrderID :  " + Gorder.googleordernumber + "  Biz OppID: : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                    //if (SaleTaxRate > 0)
                    //{
                    //    CCommon objCommon1 = new CCommon();
                    //    objCommon1.Mode = 35;
                    //    objCommon1.UpdateRecordID = lngOppId;
                    //    objCommon1.Comments = CCommon.ToString(Math.Round(SaleTaxRate, 4));
                    //    objCommon1.DomainID = DomainID;
                    //    objCommon1.UpdateSingleFieldValue();
                    //    LogMessage = "Tax Percentage updated to " + CCommon.ToString(Math.Round(SaleTaxRate, 4)) + " for API OrderID : " + OrderId + " Biz Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                    //    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                    //}

                    objWebApi.WebApiId = WebApiId;
                    objWebApi.DomainID = DomainID;
                    objWebApi.UserCntID = numRecordOwner;
                    objWebApi.OppId = lngOppId;
                    objWebApi.vcAPIOppId = Gorder.googleordernumber;
                    objWebApi.AddAPIopportunity();

                    objWebApi.API_OrderId = Gorder.googleordernumber;
                    objWebApi.API_OrderStatus = 1;
                    objWebApi.WebApiOrdDetailId = 1;
                    objWebApi.ManageAPIOrderDetails();

                    decimal OrderAmount = OrderTotal;// +ShipCost - dTotDiscount + SalesTaxAmount;

                    //string Reference = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM"); 
                    string Reference = arrOutPut[1];

                    if (ItemMessage == "")
                    {
                        bool IsAuthoritative = false;
                        BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                        BizCommonFunctions.CreateOppBizDoc(objOppBizDocs, numRecordOwner, DomainID, lngOppId, objOpportunity.OppRefOrderNo, numBizDocId, BizDocStatusId, lngDivId, OrderDate, ShipCost, dTotDiscount, out OppBizDocID, out IsAuthoritative, ExpenseAccountId, SaleTaxRate, OrderAmount, Reference);
                        UpdateApiOppItemDetails(DomainID, WebApiId, numRecordOwner, lngOppId, OppBizDocID, objOpportunity.OppRefOrderNo, OrderDetails.shoppingcart.items);
                    }
                    else
                    {
                        GeneralFunctions.WriteMessage(DomainID, "LOG", "BizDoc is not Created for BizOpp Id : " + lngOppId + " due to " + ItemMessage);
                    }
                }
                else
                {
                    string ErrMessage = "Order details for Google Order Id : " + OrderId + " is already exists in BizAutomation";
                    GeneralFunctions.WriteMessage(DomainID, "LOG", ErrMessage);
                }
            }
        }

        /// <summary>
        /// Gets the Items List and creates a New structure of Item details in order to add in BizDatabase
        /// </summary>
        /// <param name="DomainId">Merchant's BizAutomation Domain Id</param>
        /// <param name="WebApiId">Web API Id</param>
        /// <param name="WareHouseId">Deault warehouse Id</param>
        /// <param name="TransTypColl">E-Bay's Transaction Type that Contatins Order Item Details</param>
        /// <param type="Out Parameter" name="CurrencyID">CurrencyID</param>
        /// <param type="Out Parameter" name="OrderTotal">Order Total</param>
        /// <param type="Out Parameter" name="ShipCost">Shipping Cost</param>
        /// <param type="Out Parameter" name="dTotDiscount">Total Discount</param>
        /// <returns>Error Log Message if E-Bay Order Item is not found in Biz Database(Empty if the E-Bay Order Item is Mapped in Biz Database)</returns>
        private string AddOrderItemDataset(long DomainId, int WebApiId, long RecordOwner, int WareHouseId, OrderSummary OrderDetails, long ShippingServiceItemID, long SalesTaxItemMappingID,
            out long CurrencyID, out decimal OrderTotal, out decimal ShipCost, out decimal dTotDiscount, decimal TransactionFee, long DiscountItemMapping, out decimal SalesTaxRate, out decimal SalesTaxAmount)
        {
            dtItems.Clear();
            dsItems.Clear();
            CCommon objCommon = new CCommon();
            string Message = "";
            DataRow drItem;
            int ItemCount = 0;
            string ItemType = "";
            string[] itemCodeType;
            string itemCode = "";
            WebAPI objWebApi = new WebAPI();
            objWebApi.DomainID = DomainId;
            objWebApi.WebApiId = WebApiId;
            dTotDiscount = 0;
            decimal decDiscount = 0;
            string DiscountDesc = "";
            decimal Tax0 = 0;
            OrderTotal = 0;
            ShipCost = 0;
            CurrencyID = 0;

            Item[] CartItems = OrderDetails.shoppingcart.items;
            decimal TaxableTotal = 0;
            SalesTaxAmount = 0;
            SalesTaxRate = 0;
            MerchantCalculatedShippingAdjustment objShipping;


            foreach (Item taxItem in CartItems)
            {
                if (taxItem != null)
                {
                    Money UnitPrice = new Money();
                    UnitPrice = taxItem.unitprice;
                    if (CCommon.ToDecimal(UnitPrice.Value) > 0)
                    {
                        TaxableTotal += taxItem.quantity * UnitPrice.Value;
                    }
                }
            }
            if (OrderDetails.orderadjustment != null)
            {
                OrderAdjustment objOrderAdjustment = OrderDetails.orderadjustment;
                if (objOrderAdjustment.totaltax != null && objOrderAdjustment.totaltax.Value > 0)
                {
                    SalesTaxRate = (objOrderAdjustment.totaltax.Value * 100) / TaxableTotal;
                    SalesTaxAmount = CCommon.ToDecimal(objOrderAdjustment.totaltax.Value);
                }
                if (objOrderAdjustment.shipping != null && objOrderAdjustment.shipping.Item != null)
                {
                    objShipping = (MerchantCalculatedShippingAdjustment)objOrderAdjustment.shipping.Item;
                    ShipCost = objShipping.shippingcost.Value;
                }
            }
            foreach (Item CartItem in CartItems)
            {
                try
                {
                    if (CartItem != null)
                    {
                        if (!string.IsNullOrEmpty(CartItem.merchantitemid))
                        {
                            Money UnitPrice = new Money();
                            UnitPrice = CartItem.unitprice;
                            if (CCommon.ToDecimal(UnitPrice.Value) > 0)
                            {
                                Tax0 = 0;
                                //objCommon.DomainID = DomainId;
                                //objCommon.Mode = 20;
                                //objCommon.Str = CartItem.merchantitemid;
                                //itemCode = CCommon.ToString(objCommon.GetSingleFieldValue());
                                
                                objCommon.DomainID = DomainId;
                                objCommon.Mode = 38;
                                objCommon.Str = CartItem.merchantitemid;
                                itemCodeType = CCommon.ToString(objCommon.GetSingleFieldValue()).Split('~');
                                if (itemCodeType.Length > 1)
                                {
                                    if (!string.IsNullOrEmpty(itemCodeType[0]))
                                    {
                                        itemCode = itemCodeType[0];
                                    }
                                    if (!string.IsNullOrEmpty(itemCodeType[1]))
                                    {
                                        ItemType = BizCommonFunctions.GetItemTypeNameByCharType(itemCodeType[1]);
                                    }
                                }
                                if (itemCode == "")
                                {
                                    Product objProduct = new Product();
                                    DataTable dtProductDetails = new DataTable();
                                    BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                                    DataRow dr = dtProductDetails.NewRow();
                                    bool ItemDeatilsImported = false;

                                    try
                                    {
                                        objProduct = GetProductDetailBySKU(DomainId, WebApiId, CartItem.merchantitemid);
                                        ItemDeatilsImported = true;
                                    }
                                    catch (Exception ex)
                                    {
                                        ItemDeatilsImported = false;
                                    }
                                    if (ItemDeatilsImported)
                                    {
                                    UserAccess objUserAccess = new UserAccess();
                                    objUserAccess.DomainID = DomainId;
                                    DataTable dtDomainDetails = objUserAccess.GetDomainDetails();
                                    int DefaultIncomeAccID = 0, DefaultCOGSAccID = 0, DefaultAssetAccID = 0;
                                    if (dtDomainDetails.Rows.Count > 0)
                                    {
                                        DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numIncomeAccID"]);
                                        DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numCOGSAccID"]);
                                        DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numAssetAccID"]);
                                        if (DefaultIncomeAccID == 0 || DefaultCOGSAccID == 0 || DefaultAssetAccID == 0)
                                        {
                                            Message += "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                            string ExcepitonMessage = "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                            throw new Exception(ExcepitonMessage);
                                        }
                                    }
                                    dr = FillGoogleItemDetails(DomainId, WebApiId, RecordOwner, DefaultIncomeAccID, DefaultCOGSAccID, DefaultAssetAccID, dr, objProduct);

                                    string ItemClassificationName = "Magento_OrderItem_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                                    long ItemClassificationID = 0;
                                    ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);
                                    itemCode = CCommon.ToString(BizCommonFunctions.AddNewItemToBiz(dr, DomainId, WebApiId, RecordOwner, WareHouseId, ItemClassificationID));
                                    ItemType = BizCommonFunctions.GetItemTypeNameByCharType("P");
                                    WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                                    string FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml";
                                    if (System.IO.File.Exists(FilePath))
                                    {
                                        System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                                        XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                        objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                        objStreamReader.Close();
                                    }

                                    objWebAPIItemDetail.GoogleProductCategory = CCommon.ToString(dr["GoogleProductCategory"]);
                                    objWebAPIItemDetail.GoogleProductType = CCommon.ToString(dr["GoogleProductType"]);
                                    objWebAPIItemDetail.ProductImageLink = CCommon.ToString(dr["ProductImageLink"]);
                                    objWebAPIItemDetail.GoogleProductURL = CCommon.ToString(dr["GoogleProductURL"]);

                                    StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml");
                                    XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                                    Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                                    objStreamWriter.Close();
                                    }
                                }

                                if (itemCode != "")
                                {
                                    decDiscount = 0;
                                    DiscountDesc = "";
                                    drItem = dtItems.NewRow();
                                    drItem["numoppitemtCode"] = ItemCount;
                                    drItem["numItemCode"] = itemCode;

                                    if (CCommon.ToInteger(CartItem.quantity) != 0)
                                    {
                                        drItem["numUnitHour"] = CartItem.quantity;
                                        drItem["monPrice"] = CCommon.ToDecimal(CartItem.unitprice.Value);
                                        drItem["monTotAmount"] = UnitPrice.Value * CartItem.quantity;
                                        drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                        OrderTotal += CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                    }
                                    drItem["numUOM"] = 0; // get it from item table, in procedure
                                    drItem["vcUOMName"] = "";// get it from item table, in procedure
                                    drItem["UOMConversionFactor"] = 1.0000;
                                    drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                                    drItem["vcModelID"] = "";// get it from item table, in procedure
                                    drItem["numWarehouseID"] = WareHouseId;
                                    if (!string.IsNullOrEmpty(CartItem.itemname))
                                    {
                                        drItem["vcItemName"] = CartItem.itemname;
                                    }
                                    drItem["Warehouse"] = "";
                                    drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                                    drItem["ItemType"] = ItemType;// get it from item table, in procedure
                                    drItem["Attributes"] = "";
                                    drItem["Op_Flag"] = 1;
                                    drItem["bitWorkOrder"] = false;
                                    drItem["DropShip"] = false;
                                    drItem["fltDiscount"] = decimal.Negate(decDiscount);
                                    dTotDiscount += decimal.Negate(decDiscount);

                                    drItem["bitDiscountType"] = 1; //Flat discount

                                    Tax0 = (CCommon.ToDecimal(drItem["monTotAmount"]) * SalesTaxRate) / 100;


                                    drItem["Tax0"] = Tax0;
                                    if (Tax0 > 0)
                                        drItem["bitTaxable0"] = true;
                                    else
                                        drItem["bitTaxable0"] = false;

                                    drItem["numVendorWareHouse"] = 0;
                                    drItem["numShipmentMethod"] = 0;
                                    drItem["numSOVendorId"] = 0;
                                    drItem["numProjectID"] = 0;
                                    drItem["numProjectStageID"] = 0;
                                    drItem["charItemType"] = ""; // should be taken from procedure
                                    drItem["numToWarehouseItemID"] = 0;
                                    //drItem["Tax41"] = 0;
                                    //drItem["bitTaxable41"] = false;
                                    //drItem["Tax42"] = 0;
                                    //drItem["bitTaxable42"] = false;
                                    drItem["Weight"] = ""; // should take from procedure 
                                    drItem["WebApiId"] = WebApiId;

                                    // drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                                    dtItems.Rows.Add(drItem);
                                }
                                else
                                {
                                    Message += Environment.NewLine + "Order Item " + CartItem.itemname + " (SKU: " + CartItem.merchantitemid + ") not found in Google-Biz linking database ";
                                }
                            }
                            else if (CCommon.ToDecimal(UnitPrice.Value) < 0)
                            {
                                Tax0 = 0;
                                decDiscount = 0;
                                DiscountDesc = "";
                                drItem = dtItems.NewRow();
                                drItem["numoppitemtCode"] = ItemCount;
                                drItem["numItemCode"] = DiscountItemMapping;

                                if (CCommon.ToInteger(CartItem.quantity) != 0)
                                {
                                    decimal ItemPrice = CCommon.ToDecimal(CartItem.unitprice.Value);
                                    // decDiscount = decimal.Negate(ItemPrice);
                                    //DiscountDesc = "Internal Discount : " + CCommon.ToString(decDiscount);

                                    drItem["numUnitHour"] = CartItem.quantity;
                                    drItem["monPrice"] = ItemPrice;
                                    drItem["monTotAmount"] = ItemPrice;
                                    drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                    OrderTotal += CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                }
                                drItem["numUOM"] = 0; // get it from item table, in procedure
                                drItem["vcUOMName"] = "";// get it from item table, in procedure
                                drItem["UOMConversionFactor"] = 1.0000;
                                drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                                drItem["vcModelID"] = "";// get it from item table, in procedure
                                drItem["numWarehouseID"] = WareHouseId;
                                if (!string.IsNullOrEmpty(CartItem.itemname))
                                {
                                    drItem["vcItemName"] = CartItem.itemname;
                                }
                                drItem["Warehouse"] = "";
                                drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                                drItem["ItemType"] = "Service";// get it from item table, in procedure
                                drItem["Attributes"] = "";
                                drItem["Op_Flag"] = 1;
                                drItem["bitWorkOrder"] = false;
                                drItem["DropShip"] = false;
                                drItem["fltDiscount"] = decDiscount;
                                //dTotDiscount += decDiscount;

                                drItem["bitDiscountType"] = 1; //Flat discount

                                drItem["Tax0"] = Tax0;
                                if (Tax0 > 0)
                                    drItem["bitTaxable0"] = true;
                                else
                                    drItem["bitTaxable0"] = false;


                                drItem["numVendorWareHouse"] = 0;
                                drItem["numShipmentMethod"] = 0;
                                drItem["numSOVendorId"] = 0;
                                drItem["numProjectID"] = 0;
                                drItem["numProjectStageID"] = 0;
                                drItem["charItemType"] = ""; // should be taken from procedure
                                drItem["numToWarehouseItemID"] = 0;
                                //drItem["Tax41"] = 0;
                                //drItem["bitTaxable41"] = false;
                                //drItem["Tax42"] = 0;
                                //drItem["bitTaxable42"] = false;
                                drItem["Weight"] = ""; // should take from procedure 
                                drItem["WebApiId"] = WebApiId;

                                // drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                                dtItems.Rows.Add(drItem);
                            }
                        }
                        else
                        {
                            Message += Environment.NewLine + "Order Item " + CartItem.itemname + " does not have an valid Merchant SKU value";
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ex.StackTrace);
                    throw ex;
                }
            }

            if (TransactionFee != 0)
            {
                Tax0 = 0;
                decDiscount = 0;
                DiscountDesc = "";
                drItem = dtItems.NewRow();
                //decDiscount = TransactionFee;
                //DiscountDesc = "Transaction Fee : " + CCommon.ToString(decDiscount);

                drItem["numoppitemtCode"] = ItemCount;
                drItem["numItemCode"] = DiscountItemMapping;
                drItem["numUnitHour"] = 1;
                drItem["monPrice"] = Decimal.Negate(TransactionFee);
                drItem["monTotAmount"] = Decimal.Negate(TransactionFee);// Decimal.Negate(TransactionFee);
                drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                OrderTotal += CCommon.ToDecimal(drItem["monTotAmount"]);

                drItem["numUOM"] = 0; // get it from item table, in procedure
                drItem["vcUOMName"] = "";// get it from item table, in procedure
                drItem["UOMConversionFactor"] = 1.0000;
                drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                drItem["vcModelID"] = "";// get it from item table, in procedure
                drItem["numWarehouseID"] = WareHouseId;

                drItem["vcItemName"] = "Transaction Fee";

                drItem["Warehouse"] = "";
                drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                drItem["ItemType"] = "Service";// get it from item table, in procedure
                drItem["Attributes"] = "";
                drItem["Op_Flag"] = 1;
                drItem["bitWorkOrder"] = false;
                drItem["DropShip"] = false;
                drItem["fltDiscount"] = decDiscount;
                // dTotDiscount += decDiscount;

                drItem["bitDiscountType"] = 1; //Flat discount


                drItem["Tax0"] = Tax0;
                if (Tax0 > 0)
                    drItem["bitTaxable0"] = true;
                else
                    drItem["bitTaxable0"] = false;

                //drItem["Tax0"] = 0;
                drItem["numVendorWareHouse"] = 0;
                drItem["numShipmentMethod"] = 0;
                drItem["numSOVendorId"] = 0;
                drItem["numProjectID"] = 0;
                drItem["numProjectStageID"] = 0;
                drItem["charItemType"] = ""; // should be taken from procedure
                drItem["numToWarehouseItemID"] = 0;
                //drItem["Tax41"] = 0;
                //drItem["bitTaxable41"] = false;
                //drItem["Tax42"] = 0;
                //drItem["bitTaxable42"] = false;
                drItem["Weight"] = ""; // should take from procedure 
                drItem["WebApiId"] = WebApiId;


                // drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                dtItems.Rows.Add(drItem);
            }
            //dTotDiscount += TransactionFee;

            if (SalesTaxAmount > 0)
            {
                OrderTotal += SalesTaxAmount;
                drItem = dtItems.NewRow();
                drItem = BizCommonFunctions.GetSalesTaxItem(drItem, SalesTaxAmount, SalesTaxItemMappingID, DomainId, WebApiId);
                dtItems.Rows.Add(drItem);
            }
            if (ShipCost > 0)
            {
                OrderTotal += ShipCost;
                drItem = dtItems.NewRow();
                drItem = BizCommonFunctions.GetShippingCostItem(drItem, ShipCost, ShippingServiceItemID, DomainId, WebApiId);
                dtItems.Rows.Add(drItem);
            }
            if (dTotDiscount > 0)
            {
                OrderTotal -= dTotDiscount;
                drItem = dtItems.NewRow();
                string DiscountItemName = "Total Discount";
                drItem = BizCommonFunctions.GetDiscountOrderItem(drItem, dTotDiscount, DiscountItemMapping, DomainId, WebApiId, DiscountItemName);
                dtItems.Rows.Add(drItem);
            }
            return Message;
        }

        /// <summary>
        /// Creates API Order Items Entry
        /// </summary>
        /// <param name="DomainId">Merchant's Biz Domain Id</param>
        /// <param name="WebApiId">Web API id</param>
        /// <param name="UserCntId">Default Record Owner's Id</param>
        /// <param name="OppId">Opportunity Id</param>
        /// <param name="OppBizDocId">Opportunity BizDocument Id</param>
        /// <param name="EBayOrderId">E-Bay Order Id</param>
        /// <param name="TransTypColl">E-Bay's Transaction Type that Contatins Order Item Details</param>
        private void UpdateApiOppItemDetails(long DomainId, int WebApiId, long UserCntId, long OppId, long OppBizDocId, string GoogleOrderId, Item[] CartItems)
        {
            DataTable dtOppItems;
            MOpportunity objOpportunity = new MOpportunity();
            objOpportunity.Mode = 5;
            objOpportunity.DomainID = DomainId;
            objOpportunity.OpportunityId = OppId;

            WebAPI objWebApi = new WebAPI();
            try
            {
                dtOppItems = objOpportunity.GetOrderItems().Tables[0];

                foreach (DataRow dr in dtOppItems.Rows)
                {
                    int i = 0;
                    foreach (Item OrderItem in CartItems)
                    {
                        // ItemType OrderItem = TransTyp.Item;
                        i += 1;

                        if (OrderItem != null)
                        {
                            if (OrderItem.merchantitemid == CCommon.ToString(dr["vcSKU"]))
                            {
                                objWebApi.DomainID = DomainId;
                                objWebApi.WebApiId = WebApiId;
                                objWebApi.OppId = OppId;
                                objWebApi.OppItemId = CCommon.ToLong(dr["numOppItemtCode"]);
                                objWebApi.vcAPIOppId = GoogleOrderId;
                                objWebApi.vcApiOppItemId = GoogleOrderId + "-" + i.ToString("00");
                                objWebApi.RStatus = 0;
                                objWebApi.RecordOwner = UserCntId;
                                objWebApi.ManageApiOrderItems();
                            }
                        }
                    }

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Updates Tracking number to Google CheckOut Orders
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="MerchantId">MerchantId</param>
        /// <param name="MerchantKey">MerchantKey</param>
        /// <param name="numItemCode">Item Code</param>
        /// <param name="ItemAPIId">Google Item Id</param>
        /// <param name="APIOrderId">Google Checkout Order ID</param>
        /// <param name="APIOrderLineItemId">Google Checkout Order line Item ID</param>
        /// <param name="TrackingNo">Tracking Number</param>
        /// <param name="ShippingCarrier">Shipping Carrier</param>
        /// <param name="Mode">Update Mode</param>
        public void UpdateTrackingNo(long DomainId, long WebApiId, string MerchantId, string MerchantKey, long numItemCode, string ItemAPIId,
         string APIOrderId, string APIOrderLineItemId, string TrackingNo, string ShippingCarrier, int Mode)
        {
            try
            {
                GoogleCommunicatorClient objGoogleCommunicatorClient = new GoogleCommunicatorClient();
                if (Mode == 0)
                {
                    objGoogleCommunicatorClient.UpdateTrackingNo(MerchantId, MerchantKey, GoogleEnvironment, APIOrderId, ShippingCarrier, TrackingNo);
                }
                else if (Mode == 1)
                {
                    objGoogleCommunicatorClient.AddTrackingNo(MerchantId, MerchantKey, GoogleEnvironment, APIOrderId, ShippingCarrier, TrackingNo);
                }
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Service : " + er.msg;
                GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {
                GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                GeneralFunctions.WriteMessage(DomainId, "ERR", ex.StackTrace);
            }
        }

        #endregion Order Processing Functions

        #region Helper Functions


        #endregion Helper Functions

        #region Test Bulk Order Import


        public void TestBulkOrderImport(ChargeAmountNotification Gorder, long DomainID, int WebApiId, string Source,
                                             int numWareHouseID, long numRecordOwner, long numAssignTo, long numRelationshipId, long numProfileId,
         long numBizDocId, long BizDocStatusId, int numOrderStatus, int ExpenseAccountId, long DiscountItemMapping, long ShippingServiceItemID, long SalesTaxItemMappingID, string ShipToPhoneNo)
        {
            string LogMessage = "", ErrorMsg = "";
            decimal SaleTaxRate = 0;
            CContacts objContacts = null;
            CLeads objLeads = null;
            ImportWizard objImpWzd = new ImportWizard();
            MOpportunity objOpportunity = new MOpportunity();
            CCommon objCommon = new CCommon();
            decimal TransactionFee = 0;
            string OrderId = Gorder.googleordernumber;
            OrderSummary OrderDetails = new OrderSummary();
            OrderDetails = Gorder.ordersummary;

            if (Gorder.latestchargefee.total != null)
            {
                Money MoneyTransFee = new Money();
                MoneyTransFee = Gorder.latestchargefee.total;
                TransactionFee = MoneyTransFee.Value;

            }
            //Address ShippingAddress = new Address();
            //ShippingAddress = OrderDetails.buyershippingaddress;

            //string Email = ShippingAddress.email;

            WebAPI objWebApi = new WebAPI();

            string ItemMessage = "";
            decimal ShipCost = 0;
            decimal OrderTotal = 0;
            long CurrencyID = 0;
            decimal dTotDiscount = 0;
            decimal SalesTaxAmount = 0;
            if (!string.IsNullOrEmpty(OrderId))
            {
                objWebApi.DomainID = DomainID;
                objWebApi.WebApiId = WebApiId;
                objWebApi.vcAPIOppId = OrderId;
               

                    int a = -1;
                    string AddressStreet = "";//To concatenate Address Lines
                    objContacts = new CContacts();
                    objLeads = new CLeads();
                    objLeads.DomainID = DomainID;

                    Address ShippingAddress = new Address();
                    ShippingAddress = OrderDetails.buyershippingaddress;
                    string Email = ShippingAddress.email;

                    string CompanyName = ShippingAddress.companyname;
                    LogMessage += Environment.NewLine + " CompanyName : " + CompanyName;

                    string FirstName = ShippingAddress.structuredname.firstname;
                    LogMessage += Environment.NewLine + " FirstName : " + FirstName;

                    string LastName = ShippingAddress.structuredname.lastname;
                    LogMessage += Environment.NewLine + " LastName : " + LastName;

                    string Name = ShippingAddress.contactname;
                    LogMessage += Environment.NewLine + " Name : " + Name;

                    string BuyerUserID = CCommon.ToString(OrderDetails.buyerid);
                    LogMessage += Environment.NewLine + " BuyerUserID : " + BuyerUserID;

                    if (!string.IsNullOrEmpty(Email))
                    {
                        objLeads.Email = Email;
                        //Check for Details for the Given DomainID and EmailID
                        objLeads.GetConIDCompIDDivIDFromEmail();
                    }
                    else
                    {
                        objLeads.Email = "";
                    }
                    if (objLeads.ContactID > 0 && objLeads.DivisionID > 0)
                    {
                        //If Email already registered
                        lngCntID = objLeads.ContactID;
                        lngDivId = objLeads.DivisionID;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ShippingAddress.contactname))
                        {
                            objLeads.CompanyName = ShippingAddress.contactname;
                            objLeads.CustName = ShippingAddress.contactname;
                        }
                        else if (!string.IsNullOrEmpty(ShippingAddress.contactname))
                        {
                            objLeads.CompanyName = ShippingAddress.contactname;
                            objLeads.CustName = ShippingAddress.contactname;
                        }
                        if (ShippingAddress != null)
                        {
                            if (!string.IsNullOrEmpty(ShippingAddress.countrycode))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 22;
                                objCommon.Str = CCommon.ToString(ShippingAddress.countrycode);
                                objLeads.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objLeads.SCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }
                            if (ShippingAddress.structuredname != null)
                            {
                                objLeads.FirstName = ShippingAddress.structuredname.firstname;
                                objLeads.LastName = ShippingAddress.structuredname.lastname;

                            }
                            else if (!string.IsNullOrEmpty(ShippingAddress.contactname))
                            {
                                a = ShippingAddress.contactname.IndexOf('.');
                                if (a == -1)
                                    a = ShippingAddress.contactname.IndexOf(' ');
                                if (a == -1)
                                {
                                    objLeads.FirstName = ShippingAddress.contactname;
                                    objLeads.LastName = ShippingAddress.contactname;
                                }
                                else
                                {
                                    objLeads.FirstName = ShippingAddress.contactname.Substring(0, a);
                                    objLeads.LastName = ShippingAddress.contactname.Substring(a + 1, ShippingAddress.contactname.Length - (a + 1));
                                }
                            }
                            if (!string.IsNullOrEmpty(ShippingAddress.phone))
                            {
                                objLeads.ContactPhone = ShippingAddress.phone;
                                objLeads.PhoneExt = "";
                            }
                            else
                            {
                                objLeads.ContactPhone = ShipToPhoneNo;
                                objLeads.PhoneExt = "";
                            }
                            if (!string.IsNullOrEmpty(ShippingAddress.fax))
                            {
                                objLeads.Fax = ShippingAddress.fax;
                            }
                        }

                        objLeads.UserCntID = numRecordOwner;
                        objLeads.ContactType = 70;
                        objLeads.PrimaryContact = true;
                        objLeads.UpdateDefaultTax = false;
                        objLeads.NoTax = false;
                        objLeads.CRMType = 1;
                        objLeads.DivisionName = "-";

                        objLeads.CompanyType = numRelationshipId;
                        objLeads.Profile = numProfileId;

                        objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();//Creates Company Record
                        LogMessage = "New Company details added CompanyID : " + objLeads.CompanyID + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        lngDivId = objLeads.CreateRecordDivisionsInfo();
                        LogMessage = "New Divisions Informations added. Division Id : " + lngDivId + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        objLeads.ContactID = 0;
                        objLeads.DivisionID = lngDivId;
                        lngCntID = objLeads.CreateRecordAddContactInfo();//Creates Contact Info
                        LogMessage = "New Contact Informations added. Contact Id : " + lngCntID + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        if (ShippingAddress != null)
                        {
                            objContacts.FirstName = objLeads.FirstName;
                            objContacts.LastName = objLeads.LastName;
                            objContacts.ContactPhone = objLeads.ContactPhone;
                            objContacts.Email = objLeads.Email;

                            if (!string.IsNullOrEmpty(ShippingAddress.address1))
                                AddressStreet = ShippingAddress.address1;

                            if (!string.IsNullOrEmpty(ShippingAddress.address2))
                                AddressStreet = AddressStreet + " " + ShippingAddress.address2;

                            objContacts.BillStreet = AddressStreet;
                            objContacts.ShipStreet = AddressStreet;

                            if (!string.IsNullOrEmpty(ShippingAddress.city))
                            {
                                objContacts.BillCity = ShippingAddress.city;
                                objContacts.ShipCity = ShippingAddress.city;
                            }

                            if (!string.IsNullOrEmpty(ShippingAddress.region))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 21;
                                objCommon.Str = ShippingAddress.region;
                                objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }

                            if (!string.IsNullOrEmpty(ShippingAddress.countrycode))
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 22;
                                objCommon.Str = CCommon.ToString(ShippingAddress.countrycode);
                                objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }

                            if (!string.IsNullOrEmpty(ShippingAddress.postalcode))
                            {
                                objContacts.BillPostal = ShippingAddress.postalcode;
                                objContacts.ShipPostal = ShippingAddress.postalcode;
                            }
                            objContacts.BillingAddress = 0;
                            objContacts.DivisionID = lngDivId;
                            objContacts.ContactID = lngCntID;
                            objContacts.RecordID = lngDivId;
                            objContacts.DomainID = DomainID;
                            objContacts.IsPrimaryAddress = CCommon.ToBool(1);
                            objContacts.UpdateCompanyAddress();
                        }
                    }
                    objOpportunity.OppRefOrderNo = Gorder.googleordernumber;
                    objOpportunity.MarketplaceOrderID = Gorder.googleordernumber;
                    //CreateItemTable();
                    BizCommonFunctions.CreateItemTable(dtOrderItems);
                    ItemMessage = TestBulkOrder_AddOrderItemDataset(DomainID, WebApiId,numRecordOwner, numWareHouseID, OrderDetails, ShippingServiceItemID, SalesTaxItemMappingID, out CurrencyID, out OrderTotal, out ShipCost, out dTotDiscount, TransactionFee, DiscountItemMapping, out SaleTaxRate, out SalesTaxAmount);

                    objOpportunity.CurrencyID = CurrencyID;
                    objOpportunity.Amount = OrderTotal;
                    //objContacts.BillingAddress = 0;
                    //objContacts.DivisionID = lngDivId;
                    //objContacts.UpdateCompanyAddress();//Updates Company Details
                    // SaveTaxTypes();
                    BizCommonFunctions.SaveTaxTypes(ds, lngDivId);
                    arrBillingIDs = new string[4];
                    arrBillingIDs[0] = CCommon.ToString(objLeads.CompanyID);
                    arrBillingIDs[1] = CCommon.ToString(lngDivId);
                    arrBillingIDs[2] = CCommon.ToString(lngCntID);
                    arrBillingIDs[3] = objLeads.CompanyName;
                    if (lngCntID == 0)
                        return;
                    // }
                    //Create Opportunity Details
                    DateTime OrderDate = OrderDetails.purchasedate;
                    objOpportunity.MarketplaceOrderDate = OrderDate;
                    objOpportunity.OpportunityId = 0;
                    objOpportunity.ContactID = lngCntID;
                    objOpportunity.DivisionID = lngDivId;
                    objOpportunity.UserCntID = numRecordOwner;
                    objOpportunity.AssignedTo = numAssignTo;

                    objOpportunity.OpportunityName = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM");
                    objOpportunity.EstimatedCloseDate = DateTime.Now;
                    objOpportunity.PublicFlag = 0;
                    objOpportunity.DomainID = DomainID;
                    objOpportunity.OppType = 1;
                    objOpportunity.OrderStatus = numOrderStatus;

                    dsOrderItems.Tables.Clear();
                    dsOrderItems.Tables.Add(dtOrderItems.Copy());
                    if (dsOrderItems.Tables[0].Rows.Count != 0)
                        objOpportunity.strItems = ((dtOrderItems.Rows.Count > 0) ? ("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsOrderItems.GetXml()) : "");
                    else
                        objOpportunity.strItems = "";
                    objOpportunity.OppComments = ItemMessage;
                    objOpportunity.Source = WebApiId;
                    objOpportunity.SourceType = 3;
                    objOpportunity.DealStatus = (long)MOpportunity.EnmDealStatus.DealWon;
                    arrOutPut = objOpportunity.Save();
                    lngOppId = CCommon.ToLong(arrOutPut[0]);

                    //Save Source 
                    objOpportunity.WebApiId = WebApiId;
                    objOpportunity.vcSource = Source;
                    objOpportunity.OpportunityId = lngOppId;
                    objOpportunity.ManageOppLinking();

                    objOpportunity.CompanyID = lngDivId;

                    if (ShippingAddress != null)
                    {
                        if (!string.IsNullOrEmpty(ShippingAddress.companyname))
                        {
                            objOpportunity.BillCompanyName = ShippingAddress.companyname;
                        }

                        if (!string.IsNullOrEmpty(ShippingAddress.contactname))
                        {
                            objOpportunity.AddressName = ShippingAddress.contactname;
                        }

                        if (!string.IsNullOrEmpty(ShippingAddress.address1))
                        {
                            AddressStreet = ShippingAddress.address1;
                        }

                        if (!string.IsNullOrEmpty(ShippingAddress.address2))
                        {
                            AddressStreet = AddressStreet + " " + ShippingAddress.address2;
                        }

                        objContacts.BillStreet = AddressStreet;
                        objContacts.ShipStreet = AddressStreet;
                        objOpportunity.BillStreet = AddressStreet;

                        if (!string.IsNullOrEmpty(ShippingAddress.city))
                        {
                            objContacts.BillCity = ShippingAddress.city;
                            objContacts.ShipCity = ShippingAddress.city;
                            objOpportunity.BillCity = ShippingAddress.city;
                        }
                        if (!string.IsNullOrEmpty(ShippingAddress.region))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 21;
                            objCommon.Str = ShippingAddress.region;
                            objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (!string.IsNullOrEmpty(ShippingAddress.countrycode))
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 22;
                            objCommon.Str = CCommon.ToString(ShippingAddress.countrycode);
                            objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }

                        if (!string.IsNullOrEmpty(ShippingAddress.postalcode))
                        {
                            objContacts.BillPostal = ShippingAddress.postalcode;
                            objContacts.ShipPostal = ShippingAddress.postalcode;
                            objOpportunity.BillPostal = ShippingAddress.postalcode;
                        }
                        objOpportunity.Mode = 0;
                        objOpportunity.UpdateOpportunityAddress();
                        objOpportunity.Mode = 1;
                        objOpportunity.UpdateOpportunityAddress();
                    }

                    LogMessage = "Order Details is added, Google OrderID :  " + Gorder.googleordernumber + "  Biz OppID: : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                    //if (SaleTaxRate > 0)
                    //{
                    //    CCommon objCommon1 = new CCommon();
                    //    objCommon1.Mode = 35;
                    //    objCommon1.UpdateRecordID = lngOppId;
                    //    objCommon1.Comments = CCommon.ToString(Math.Round(SaleTaxRate, 4));
                    //    objCommon1.DomainID = DomainID;
                    //    objCommon1.UpdateSingleFieldValue();
                    //    LogMessage = "Tax Percentage updated to " + CCommon.ToString(Math.Round(SaleTaxRate, 4)) + " for API OrderID : " + OrderId + " Biz Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                    //    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                    //}

                    objWebApi.WebApiId = WebApiId;
                    objWebApi.DomainID = DomainID;
                    objWebApi.UserCntID = numRecordOwner;
                    objWebApi.OppId = lngOppId;
                    objWebApi.vcAPIOppId = Gorder.googleordernumber;
                    objWebApi.AddAPIopportunity();

                    objWebApi.API_OrderId = Gorder.googleordernumber;
                    objWebApi.API_OrderStatus = 1;
                    objWebApi.WebApiOrdDetailId = 1;
                    objWebApi.ManageAPIOrderDetails();

                    decimal OrderAmount = OrderTotal;// +ShipCost - dTotDiscount + SalesTaxAmount;

                    //string Reference = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM"); 
                    string Reference = arrOutPut[1];

                    if (ItemMessage == "")
                    {
                        bool IsAuthoritative = false;
                        BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                        BizCommonFunctions.CreateOppBizDoc(objOppBizDocs, numRecordOwner, DomainID, lngOppId, objOpportunity.OppRefOrderNo, numBizDocId, BizDocStatusId, lngDivId, OrderDate, ShipCost, dTotDiscount, out OppBizDocID, out IsAuthoritative, ExpenseAccountId, SaleTaxRate, OrderAmount, Reference);
                        UpdateApiOppItemDetails(DomainID, WebApiId, numRecordOwner, lngOppId, OppBizDocID, objOpportunity.OppRefOrderNo, OrderDetails.shoppingcart.items);
                    }
                    else
                    {
                        GeneralFunctions.WriteMessage(DomainID, "LOG", "BizDoc is not Created for BizOpp Id : " + lngOppId + " due to " + ItemMessage);
                    }
            }
        }

        /// <summary>
        /// Gets the Items List and creates a New structure of Item details in order to add in BizDatabase
        /// </summary>
        /// <param name="DomainId">Merchant's BizAutomation Domain Id</param>
        /// <param name="WebApiId">Web API Id</param>
        /// <param name="WareHouseId">Deault warehouse Id</param>
        /// <param name="TransTypColl">E-Bay's Transaction Type that Contatins Order Item Details</param>
        /// <param type="Out Parameter" name="CurrencyID">CurrencyID</param>
        /// <param type="Out Parameter" name="OrderTotal">Order Total</param>
        /// <param type="Out Parameter" name="ShipCost">Shipping Cost</param>
        /// <param type="Out Parameter" name="dTotDiscount">Total Discount</param>
        /// <returns>Error Log Message if E-Bay Order Item is not found in Biz Database(Empty if the E-Bay Order Item is Mapped in Biz Database)</returns>
        private string TestBulkOrder_AddOrderItemDataset(long DomainId, int WebApiId,long RecordOwner, int WareHouseId, OrderSummary OrderDetails, long ShippingServiceItemID, long SalesTaxItemMappingID,
            out long CurrencyID, out decimal OrderTotal, out decimal ShipCost, out decimal dTotDiscount, decimal TransactionFee, long DiscountItemMapping, out decimal SalesTaxRate, out decimal SalesTaxAmount)
        {
            dtOrderItems.Clear();
            dsOrderItems.Clear();
            CCommon objCommon = new CCommon();
            string Message = "";
            DataRow drItem;
            int ItemCount = 0;
            string ItemType = "";
            string[] itemCodeType;
            string itemCode = "";
            WebAPI objWebApi = new WebAPI();
            objWebApi.DomainID = DomainId;
            objWebApi.WebApiId = WebApiId;
            dTotDiscount = 0;
            decimal decDiscount = 0;
            string DiscountDesc = "";
            decimal Tax0 = 0;
            OrderTotal = 0;
            ShipCost = 0;
            CurrencyID = 0;

            Item[] CartItems = OrderDetails.shoppingcart.items;
            decimal TaxableTotal = 0;
            SalesTaxAmount = 0;
            SalesTaxRate = 0;
            MerchantCalculatedShippingAdjustment objShipping;


            foreach (Item taxItem in CartItems)
            {
                if (taxItem != null)
                {
                    Money UnitPrice = new Money();
                    UnitPrice = taxItem.unitprice;
                    if (CCommon.ToDecimal(UnitPrice.Value) > 0)
                    {
                        TaxableTotal += taxItem.quantity * UnitPrice.Value;
                    }
                }
            }
            if (OrderDetails.orderadjustment != null)
            {
                OrderAdjustment objOrderAdjustment = OrderDetails.orderadjustment;
                if (objOrderAdjustment.totaltax != null && objOrderAdjustment.totaltax.Value > 0)
                {
                    SalesTaxRate = (objOrderAdjustment.totaltax.Value * 100) / TaxableTotal;
                    SalesTaxAmount = CCommon.ToDecimal(objOrderAdjustment.totaltax.Value);
                }
                if (objOrderAdjustment.shipping != null && objOrderAdjustment.shipping.Item != null)
                {
                    objShipping = (MerchantCalculatedShippingAdjustment)objOrderAdjustment.shipping.Item;
                    ShipCost = objShipping.shippingcost.Value;
                }
            }
            foreach (Item CartItem in CartItems)
            {
                try
                {
                    if (CartItem != null)
                    {
                        if (!string.IsNullOrEmpty(CartItem.merchantitemid))
                        {
                            Money UnitPrice = new Money();
                            UnitPrice = CartItem.unitprice;
                            if (CCommon.ToDecimal(UnitPrice.Value) > 0)
                            {
                                Tax0 = 0;
                                //objCommon.DomainID = DomainId;
                                //objCommon.Mode = 20;
                                //objCommon.Str = CartItem.merchantitemid;
                                //itemCode = CCommon.ToString(objCommon.GetSingleFieldValue());

                                objCommon.DomainID = DomainId;
                                objCommon.Mode = 38;
                                objCommon.Str = CartItem.merchantitemid;
                                itemCodeType = CCommon.ToString(objCommon.GetSingleFieldValue()).Split('~');
                                if (itemCodeType.Length > 1)
                                {
                                    if (!string.IsNullOrEmpty(itemCodeType[0]))
                                    {
                                        itemCode = itemCodeType[0];
                                    }
                                    if (!string.IsNullOrEmpty(itemCodeType[1]))
                                    {
                                        ItemType = BizCommonFunctions.GetItemTypeNameByCharType(itemCodeType[1]);
                                    }
                                }
                                if (itemCode == "")
                                {
                                    Product objProduct = new Product();
                                    DataTable dtProductDetails = new DataTable();
                                    BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                                    DataRow dr = dtProductDetails.NewRow();
                                    objProduct = GetProductDetailBySKU(DomainId, WebApiId, CartItem.merchantitemid);
                                    UserAccess objUserAccess = new UserAccess();
                                    objUserAccess.DomainID = DomainId;
                                    DataTable dtDomainDetails = objUserAccess.GetDomainDetails();
                                    int DefaultIncomeAccID = 0, DefaultCOGSAccID = 0, DefaultAssetAccID = 0;
                                    if (dtDomainDetails.Rows.Count > 0)
                                    {
                                        DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numIncomeAccID"]);
                                        DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numCOGSAccID"]);
                                        DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numAssetAccID"]);
                                        if (DefaultIncomeAccID == 0 || DefaultCOGSAccID == 0 || DefaultAssetAccID == 0)
                                        {
                                            Message += "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                            string ExcepitonMessage = "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                                            throw new Exception(ExcepitonMessage);
                                        }
                                    }
                                    dr = FillGoogleItemDetails(DomainId, WebApiId, RecordOwner, DefaultIncomeAccID, DefaultCOGSAccID, DefaultAssetAccID, dr, objProduct);

                                    string ItemClassificationName = "Magento_OrderItem_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                                    long ItemClassificationID = 0;
                                    ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);
                                    itemCode = CCommon.ToString(BizCommonFunctions.AddNewItemToBiz(dr, DomainId, WebApiId, RecordOwner, WareHouseId, ItemClassificationID));
                                    ItemType = BizCommonFunctions.GetItemTypeNameByCharType("P");
                                    WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                                    string FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml";
                                    if (System.IO.File.Exists(FilePath))
                                    {
                                        System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                                        XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                        objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                        objStreamReader.Close();
                                    }

                                    objWebAPIItemDetail.GoogleProductCategory = CCommon.ToString(dr["GoogleProductCategory"]);
                                    objWebAPIItemDetail.GoogleProductType = CCommon.ToString(dr["GoogleProductType"]);
                                    objWebAPIItemDetail.ProductImageLink = CCommon.ToString(dr["ProductImageLink"]);
                                    objWebAPIItemDetail.GoogleProductURL = CCommon.ToString(dr["GoogleProductURL"]);

                                    StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml");
                                    XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                                    Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                                    objStreamWriter.Close();
                                }
                                if (itemCode != "")
                                {
                                    decDiscount = 0;
                                    DiscountDesc = "";
                                    drItem = dtOrderItems.NewRow();
                                    drItem["numoppitemtCode"] = ItemCount;
                                    drItem["numItemCode"] = itemCode;

                                    if (CCommon.ToInteger(CartItem.quantity) != 0)
                                    {
                                        drItem["numUnitHour"] = CartItem.quantity;
                                        drItem["monPrice"] = CCommon.ToDecimal(CartItem.unitprice.Value);
                                        drItem["monTotAmount"] = UnitPrice.Value * CartItem.quantity;
                                        drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                        OrderTotal += CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                    }
                                    drItem["numUOM"] = 0; // get it from item table, in procedure
                                    drItem["vcUOMName"] = "";// get it from item table, in procedure
                                    drItem["UOMConversionFactor"] = 1.0000;
                                    drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                                    drItem["vcModelID"] = "";// get it from item table, in procedure
                                    drItem["numWarehouseID"] = WareHouseId;
                                    if (!string.IsNullOrEmpty(CartItem.itemname))
                                    {
                                        drItem["vcItemName"] = CartItem.itemname;
                                    }
                                    drItem["Warehouse"] = "";
                                    drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                                    drItem["ItemType"] = ItemType;// get it from item table, in procedure
                                    drItem["Attributes"] = "";
                                    drItem["Op_Flag"] = 1;
                                    drItem["bitWorkOrder"] = false;
                                    drItem["DropShip"] = false;
                                    drItem["fltDiscount"] = decimal.Negate(decDiscount);
                                    dTotDiscount += decimal.Negate(decDiscount);

                                    drItem["bitDiscountType"] = 1; //Flat discount

                                    Tax0 = (CCommon.ToDecimal(drItem["monTotAmount"]) * SalesTaxRate) / 100;


                                    drItem["Tax0"] = Tax0;
                                    if (Tax0 > 0)
                                        drItem["bitTaxable0"] = true;
                                    else
                                        drItem["bitTaxable0"] = false;

                                    drItem["numVendorWareHouse"] = 0;
                                    drItem["numShipmentMethod"] = 0;
                                    drItem["numSOVendorId"] = 0;
                                    drItem["numProjectID"] = 0;
                                    drItem["numProjectStageID"] = 0;
                                    drItem["charItemType"] = ""; // should be taken from procedure
                                    drItem["numToWarehouseItemID"] = 0;
                                    //drItem["Tax41"] = 0;
                                    //drItem["bitTaxable41"] = false;
                                    //drItem["Tax42"] = 0;
                                    //drItem["bitTaxable42"] = false;
                                    drItem["Weight"] = ""; // should take from procedure 
                                    drItem["WebApiId"] = WebApiId;

                                    // drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                                    dtOrderItems.Rows.Add(drItem);
                                }
                                else
                                {
                                    Message += Environment.NewLine + "Order Item " + CartItem.itemname + " (SKU: " + CartItem.merchantitemid + ") not found in Google-Biz linking database ";
                                }
                            }
                            else if (CCommon.ToDecimal(UnitPrice.Value) < 0)
                            {
                                Tax0 = 0;
                                decDiscount = 0;
                                DiscountDesc = "";
                                drItem = dtOrderItems.NewRow();
                                drItem["numoppitemtCode"] = ItemCount;
                                drItem["numItemCode"] = DiscountItemMapping;

                                if (CCommon.ToInteger(CartItem.quantity) != 0)
                                {
                                    decimal ItemPrice = CCommon.ToDecimal(CartItem.unitprice.Value);
                                    // decDiscount = decimal.Negate(ItemPrice);
                                    //DiscountDesc = "Internal Discount : " + CCommon.ToString(decDiscount);

                                    drItem["numUnitHour"] = CartItem.quantity;
                                    drItem["monPrice"] = ItemPrice;
                                    drItem["monTotAmount"] = ItemPrice;
                                    drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                    OrderTotal += CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                                }
                                drItem["numUOM"] = 0; // get it from item table, in procedure
                                drItem["vcUOMName"] = "";// get it from item table, in procedure
                                drItem["UOMConversionFactor"] = 1.0000;
                                drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                                drItem["vcModelID"] = "";// get it from item table, in procedure
                                drItem["numWarehouseID"] = WareHouseId;
                                if (!string.IsNullOrEmpty(CartItem.itemname))
                                {
                                    drItem["vcItemName"] = CartItem.itemname;
                                }
                                drItem["Warehouse"] = "";
                                drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                                drItem["ItemType"] = "Service";// get it from item table, in procedure
                                drItem["Attributes"] = "";
                                drItem["Op_Flag"] = 1;
                                drItem["bitWorkOrder"] = false;
                                drItem["DropShip"] = false;
                                drItem["fltDiscount"] = decDiscount;
                                //dTotDiscount += decDiscount;

                                drItem["bitDiscountType"] = 1; //Flat discount

                                drItem["Tax0"] = Tax0;
                                if (Tax0 > 0)
                                    drItem["bitTaxable0"] = true;
                                else
                                    drItem["bitTaxable0"] = false;


                                drItem["numVendorWareHouse"] = 0;
                                drItem["numShipmentMethod"] = 0;
                                drItem["numSOVendorId"] = 0;
                                drItem["numProjectID"] = 0;
                                drItem["numProjectStageID"] = 0;
                                drItem["charItemType"] = ""; // should be taken from procedure
                                drItem["numToWarehouseItemID"] = 0;
                                //drItem["Tax41"] = 0;
                                //drItem["bitTaxable41"] = false;
                                //drItem["Tax42"] = 0;
                                //drItem["bitTaxable42"] = false;
                                drItem["Weight"] = ""; // should take from procedure 
                                drItem["WebApiId"] = WebApiId;

                                // drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                                dtOrderItems.Rows.Add(drItem);
                            }
                        }
                        else
                        {
                            Message += Environment.NewLine + "Order Item " + CartItem.itemname + " does not have an valid Merchant SKU value";
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ex.StackTrace);
                    throw ex;
                }
            }

            if (TransactionFee != 0)
            {
                Tax0 = 0;
                decDiscount = 0;
                DiscountDesc = "";
                drItem = dtOrderItems.NewRow();
                //decDiscount = TransactionFee;
                //DiscountDesc = "Transaction Fee : " + CCommon.ToString(decDiscount);

                drItem["numoppitemtCode"] = ItemCount;
                drItem["numItemCode"] = DiscountItemMapping;
                drItem["numUnitHour"] = 1;
                drItem["monPrice"] = Decimal.Negate(TransactionFee);
                drItem["monTotAmount"] = Decimal.Negate(TransactionFee);// Decimal.Negate(TransactionFee);
                drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                OrderTotal += CCommon.ToDecimal(drItem["monTotAmount"]);

                drItem["numUOM"] = 0; // get it from item table, in procedure
                drItem["vcUOMName"] = "";// get it from item table, in procedure
                drItem["UOMConversionFactor"] = 1.0000;
                drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                drItem["vcModelID"] = "";// get it from item table, in procedure
                drItem["numWarehouseID"] = WareHouseId;

                drItem["vcItemName"] = "Transaction Fee";

                drItem["Warehouse"] = "";
                drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                drItem["ItemType"] = "Service";// get it from item table, in procedure
                drItem["Attributes"] = "";
                drItem["Op_Flag"] = 1;
                drItem["bitWorkOrder"] = false;
                drItem["DropShip"] = false;
                drItem["fltDiscount"] = decDiscount;
                // dTotDiscount += decDiscount;

                drItem["bitDiscountType"] = 1; //Flat discount


                drItem["Tax0"] = Tax0;
                if (Tax0 > 0)
                    drItem["bitTaxable0"] = true;
                else
                    drItem["bitTaxable0"] = false;

                //drItem["Tax0"] = 0;
                drItem["numVendorWareHouse"] = 0;
                drItem["numShipmentMethod"] = 0;
                drItem["numSOVendorId"] = 0;
                drItem["numProjectID"] = 0;
                drItem["numProjectStageID"] = 0;
                drItem["charItemType"] = ""; // should be taken from procedure
                drItem["numToWarehouseItemID"] = 0;
                //drItem["Tax41"] = 0;
                //drItem["bitTaxable41"] = false;
                //drItem["Tax42"] = 0;
                //drItem["bitTaxable42"] = false;
                drItem["Weight"] = ""; // should take from procedure 
                drItem["WebApiId"] = WebApiId;


                // drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                dtOrderItems.Rows.Add(drItem);
            }
            //dTotDiscount += TransactionFee;

            if (SalesTaxAmount > 0)
            {
                OrderTotal += SalesTaxAmount;
                drItem = dtOrderItems.NewRow();
                drItem = BizCommonFunctions.GetSalesTaxItem(drItem, SalesTaxAmount, SalesTaxItemMappingID, DomainId, WebApiId);
                dtOrderItems.Rows.Add(drItem);
            }
            if (ShipCost > 0)
            {
                OrderTotal += ShipCost;
                drItem = dtOrderItems.NewRow();
                drItem = BizCommonFunctions.GetShippingCostItem(drItem, ShipCost, ShippingServiceItemID, DomainId, WebApiId);
                dtOrderItems.Rows.Add(drItem);
            }
            if (dTotDiscount > 0)
            {
                OrderTotal -= dTotDiscount;
                drItem = dtOrderItems.NewRow();
                string DiscountItemName = "Total Discount";
                drItem = BizCommonFunctions.GetDiscountOrderItem(drItem, dTotDiscount, DiscountItemMapping, DomainId, WebApiId, DiscountItemName);
                dtOrderItems.Rows.Add(drItem);
            }
            return Message;
        }

        #endregion Test Bulk Order Import

    }
}
