﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;

using BACRM.BusinessLogic;
using BACRMBUSSLOGIC.BussinessLogic;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Prospects;
using BACRM.BusinessLogic.Contract;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.WebAPI;

using MarketplaceService.AMWS_Communicator;
using AmazonSource;

using eBay.Service.Core.Soap;

namespace MarketplaceService
{
    public partial class Service1 : ServiceBase
    {

        #region Members

        public ServiceHost serviceHost = null;
        CCommon objCommon = new CCommon();
        private static ManualResetEvent m_closingEvent = new ManualResetEvent(false);
        private static List<Task> m_tasksList = new List<Task>();
        private const int M_POLL_TIME = 60000;

        private int CreateFeed_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["CreateFeed"]);
        private int SubmitFeed_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["SubmitFeed"]);
        private int UpdateItemAPI_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["UpdateItemAPI"]);
        private int UpdateInventory_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["UpdateInventory"]);
        private int OrderList_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["OrderList"]);
        private int OrderDetails_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["OrderDetails"]);
        private int OrderReport_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["OrderReport"]);
        private int ReadOrders_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["ReadOrders"]);
        private int ClearLog_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["ClearLog"]);
        private int ClearLogFiles = CCommon.ToInteger(ConfigurationManager.AppSettings["ClearLogFiles"]);
        private int MagentoAttributeSet_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["MagentoAttributeSet"]);
        private int OrderFullfilment_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["OrderFullfilment"]);
        private int ProdListings_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["ProdListings"]);
        private int GetFBAOrderIDList_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["GetFBAOrderIDList"]);
        private int GetFBAOrderDetails_Timer = CCommon.ToInteger(ConfigurationManager.AppSettings["GetFBAOrderDetails"]);

        private string AmazonOrderFilePath = ConfigurationManager.AppSettings["AmazonOrderFilePath"];
        private string AmazonClosedOrdersPath = ConfigurationManager.AppSettings["AmazonClosedOrders"];

        private string EBayOrderFilePath = ConfigurationManager.AppSettings["EBayOrderFilePath"];
        private string EBayClosedOrders = ConfigurationManager.AppSettings["EBayClosedOrders"];

        private string GoogleOrderFilePath = ConfigurationManager.AppSettings["GoogleOrderFilePath"];
        private string GoogleClosedOrders = ConfigurationManager.AppSettings["GoogleClosedOrders"];

        private string MagentoOrderFilePath = ConfigurationManager.AppSettings["MagentoOrderFilePath"];
        private string MagentoClosedOrders = ConfigurationManager.AppSettings["MagentoClosedOrders"];

        private string AmazonMWSAccessKey = ConfigurationManager.AppSettings["AmazonMWSAccessKey"];
        private string AmazonMWSSecretKey = ConfigurationManager.AppSettings["AmazonMWSSecretKey"];

        #endregion Members

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Service1()
        {
            InitializeComponent();
        }

        #endregion Constructor

        #region WinService Activation

        public static void Main()
        {
            ServiceBase.Run(new Service1());
        }

        protected override void OnStart(string[] args)
        {
            HostService();
        }

        protected override void OnStop()
        {
            StopService();
        }

        #endregion WinService Activation

        #region Service Handlers

        /// <summary>
        /// Host Service and opens the Service Host
        /// </summary>
        public void HostService()
        {
            try
            {
                if (serviceHost != null)
                {
                    serviceHost.Close();
                }
                serviceHost = new ServiceHost(typeof(Service1));
                serviceHost.Open();// Starts the Service
                StartTimer();
                GeneralFunctions.WriteMessage(0, "LOG", "Service1 : From HostService:" + " Online Marketplace Communicator Service Started..");
            }
            catch (Exception ex)
            {
                GeneralFunctions.WriteMessage(0, "ERR", "Service1 : From HostService:" + "Online Marketplace Communicator Service Aborted due to..");
                GeneralFunctions.WriteMessage(0, "ERR", "Service1 : From HostService:" + ex.Message);
                GeneralFunctions.WriteMessage(0, "ERR", "Service1 : From HostService:" + ex.StackTrace);
                serviceHost.Abort();
            }
        }

        /// <summary>
        /// Stop the Service
        /// </summary>
        public void StopService()
        {
            try
            {
                GeneralFunctions.WriteMessage(0, "LOG", "Service1 : From StopService:" + "Online Marketplace Communicator Service Stoped.");
                if (serviceHost != null && serviceHost.State == CommunicationState.Opened)
                {
                    serviceHost.Close();
                    serviceHost = null;
                }
                StopTimer();
            }
            catch (Exception ex)
            {
                GeneralFunctions.WriteMessage(0, "ERR", "Service1 : From StopService:" + "Online Marketplace Communicator Service Aborted due to..");
                GeneralFunctions.WriteMessage(0, "ERR", "Service1 : From StopService:" + ex.Message);
                GeneralFunctions.WriteMessage(0, "ERR", "Service1 : From StopService:" + ex.StackTrace);
                serviceHost.Abort();
            }
        }

        #endregion Service Handlers

        #region Task Handlers

        /// <summary>
        /// Starts the Timer//Resets the ManualResetEvent
        /// Creates seperate task for each operations 
        /// </summary>
        public void StartTimer()
        {
            // First reset the closing event;
            m_closingEvent.Reset();

            // Create a task and start the process.

            //Task tskAddNewItems = Task.Factory.StartNew(new Action(CalleBayFunctions), TaskCreationOptions.PreferFairness);
            //m_tasksList.Add(tskAddNewItems);

            Task tskAddNewItems = Task.Factory.StartNew(new Action(AddItems_To_API), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskAddNewItems);

            Task tskProcessAmazonFeeds = Task.Factory.StartNew(new Action(ProcessAmazonFeeds), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskProcessAmazonFeeds);

            Task tskAmazonItemAPIMapping = Task.Factory.StartNew(new Action(AmazonItemAPIMapping), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskAmazonItemAPIMapping);

            Task tskUpdateInventory = Task.Factory.StartNew(new Action(InventoryManagement), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskUpdateInventory);

            Task tskGetOrderReportList = Task.Factory.StartNew(new Action(GetOrderReportList), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskGetOrderReportList);

            Task tskGetOrderReport = Task.Factory.StartNew(new Action(GetOrderReport), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskGetOrderReport);

            Task tskReadOrdersReport = Task.Factory.StartNew(new Action(ReadOrdersReport), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskReadOrdersReport);

            Task tskOrdFulfillment = Task.Factory.StartNew(new Action(UpdateOrderFulfillment), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskOrdFulfillment);

            Task tskClearLogs = Task.Factory.StartNew(new Action(ClearLogs), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskClearLogs);

            Task tskMagentoAttributeSets = Task.Factory.StartNew(new Action(UpdateMagentoProductAttributeSet), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskMagentoAttributeSets);

            Task tskGetProductListingsCommand = Task.Factory.StartNew(new Action(GetProductListingsCommand), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskGetProductListingsCommand);

            Task tskGetAmazonPrdListingReport = Task.Factory.StartNew(new Action(GetAmazonProductListingReport), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskGetAmazonPrdListingReport);

            Task tskReadAmazonPrdListingReport = Task.Factory.StartNew(new Action(ReadAmazonProductListingReport), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskReadAmazonPrdListingReport);

            Task tskGetFBAOrderIDList = Task.Factory.StartNew(new Action(GetFBAOrderIDList), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskGetFBAOrderIDList);

            Task tskGetOrderDetailsForFBAOrderID = Task.Factory.StartNew(new Action(GetOrderDetailsForFBAOrderID), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskGetOrderDetailsForFBAOrderID);

            Task tskReadAmazonFBAOrderFile = Task.Factory.StartNew(new Action(ReadAmazonFBAOrderFile), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskReadAmazonFBAOrderFile);

            Task tskValidateAPISettings = Task.Factory.StartNew(new Action(ValidateAPISettings), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskValidateAPISettings);

            Task tskProcessImportApiOrderRequest = Task.Factory.StartNew(new Action(ProcessImportApiOrderRequest), TaskCreationOptions.PreferFairness);
            m_tasksList.Add(tskProcessImportApiOrderRequest);

            //Task tskTestBulkOrderImport = Task.Factory.StartNew(new Action(TestBulkOrderImport), TaskCreationOptions.PreferFairness);
            //m_tasksList.Add(tskTestBulkOrderImport);

        }

        /// <summary>
        /// Stops the Timer//Sets the ManualResetEvent
        /// Collects task List and set infinite wait Timeout for the tasks to complete.
        /// </summary>
        public void StopTimer()
        {
            // Set the manual reset event.
            m_closingEvent.Set();

            // Wait for all tasks to complete.
            if (m_tasksList.Count > 0)
            {
                Task[] arTasks = m_tasksList.ToArray();
                Task.WaitAll(arTasks, Timeout.Infinite);
                m_tasksList.Clear();
            }
        }

        #endregion Task Handlers

        #region Product Listing Operation

        /// <summary>
        /// Imports Complete Product Listing from various Marketplaces and Adds to BizDatabase
        /// </summary>
        public void GetProductListingsCommand()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * ProdListings_Timer))
            // while (!m_closingEvent.WaitOne(M_POLL_TIME * 0))
            {
                AmazonClient objAmazonClient = null;
                eBayClient objeBayClient = null;
                GoogleClient objGoogleClient = null;
                MagentoClient objMagentoClient = null;

                WebAPI objWebAPI = new WebAPI();
                long DomainID = 0;
                int WebApiId = 0;
                int OrderStatus = 0;
                int WareHouseID = 0;
                long BizDocId = 0;
                long RecordOwner = 0;
                long AssignTo = 0;
                long RelationshipId = 0;
                long ProfileId = 0;
                long UserContactID = 0;

                string Source = "";
                string ReportType = "", ReportOptions = "";

                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                Source = CCommon.ToString(dr["vcProviderName"]);
                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                {
                                    objAmazonClient = new AmazonClient();
                                    ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                    objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                    objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                    objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                    objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);
                                    objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));

                                    OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                    WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                    BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                    RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                    AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                    RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                    ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                    UserContactID = CCommon.ToLong(dr["vcFifteenthFldValue"]);
                                    objSvcConfigInfo.ServiceURL_Type = 2;
                                    string ListingRequest = CCommon.ToString((dr["vcFourteenthFldValue"]));
                                    if (!string.IsNullOrEmpty(ListingRequest))
                                    {
                                        string[] Requesttype = ListingRequest.Split('|');
                                        if (!string.IsNullOrEmpty(Requesttype[0]) & Requesttype[0] == "1") //Request for Product Listing
                                        {
                                            if (!string.IsNullOrEmpty(Requesttype[1]))
                                            {
                                                ReportType = "ActiveListing";
                                                //string ListingReqType = Requesttype[1];
                                                //if (ListingReqType == "1")                             // Only Active Product List
                                                //    ReportType = "ActiveListing";
                                                //else if (ListingReqType == "2")                     // Active and InActive Product List
                                                //    ReportType = "OpenListing1";

                                                ReportOptions = "";

                                                objAmazonClient.RequestReport(objSvcConfigInfo, DomainID, WebApiId, ReportType, ReportOptions);

                                                objWebAPI.DomainID = DomainID;
                                                objWebAPI.UserContactID = UserContactID;
                                                objWebAPI.WebApiId = WebApiId;
                                                objWebAPI.FlagItemImport = "0|1";// +ListingReqType;
                                                objWebAPI.ManageWebApiItemImport();
                                                //objWebAPI.UpdateAPISettingsDate(4);

                                                // CCommon objCommon = new CCommon();
                                                //objCommon.Mode = 34;
                                                //objCommon.DomainID = DomainID;
                                                //objCommon.UpdateRecordID = WebApiId;
                                                //objCommon.UpdateValueID = 0;
                                                //objCommon.UpdateSingleFieldValue();
                                            }
                                        }
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.EBayUS) //E-Bay US)
                                {
                                    objeBayClient = new eBayClient();
                                    string eBaySite = CCommon.ToString(dr["vcFirstFldValue"]);
                                    string Token = CCommon.ToString(dr["vcEighthFldValue"]);
                                    RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                    WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                    UserContactID = CCommon.ToLong(dr["vcFifteenthFldValue"]);

                                    string ListingRequest = CCommon.ToString((dr["vcFourteenthFldValue"]));
                                    if (!string.IsNullOrEmpty(ListingRequest))
                                    {
                                        string[] Requesttype = ListingRequest.Split('|');
                                        if (!string.IsNullOrEmpty(Requesttype[0]) & Requesttype[0] == "1")
                                        {
                                            if (!string.IsNullOrEmpty(Requesttype[1]))
                                            {
                                                int ListingReqType = CCommon.ToInteger(Requesttype[1]);
                                                try
                                                {
                                                    objeBayClient.GetEbayProductListings(DomainID, WebApiId, RecordOwner, UserContactID, WareHouseID, Token, eBaySite, ListingReqType);
                                                }
                                                catch (Exception ex)
                                                {
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetProductListingsCommand:" + ex.Message);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetProductListingsCommand:" + ex.StackTrace);
                                                }
                                                objWebAPI.DomainID = DomainID;
                                                objWebAPI.UserContactID = UserContactID;
                                                objWebAPI.WebApiId = WebApiId;
                                                objWebAPI.FlagItemImport = "0|0";
                                                objWebAPI.ManageWebApiItemImport();
                                            }
                                        }
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.GoogleUS) //GoogleUS
                                {
                                    objGoogleClient = new GoogleClient();
                                    string TargetCountry = CCommon.ToString(dr["vcFirstFldValue"]);
                                    string AccountId = CCommon.ToString(dr["vcSecondFldValue"]);
                                    string EMail = CCommon.ToString(dr["vcThirdFldValue"]);
                                    string Password = CCommon.ToString(dr["vcFourthFldValue"]);

                                    RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                    WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                    UserContactID = CCommon.ToLong(dr["vcFifteenthFldValue"]);

                                    //objGoogleClient.GetProductDetailBySKU(DomainID, WebApiId, "");
                                    string ListingRequest = CCommon.ToString((dr["vcFourteenthFldValue"]));
                                    if (!string.IsNullOrEmpty(ListingRequest))
                                    {
                                        string[] Requesttype = ListingRequest.Split('|');
                                        if (!string.IsNullOrEmpty(Requesttype[0]) & Requesttype[0] == "1")
                                        {
                                            if (!string.IsNullOrEmpty(Requesttype[1]))
                                            {
                                                string ListingReqType = Requesttype[1];
                                                objGoogleClient.ImportProductListing(DomainID, WebApiId, RecordOwner, UserContactID, WareHouseID, AccountId, EMail, Password);
                                                objWebAPI.DomainID = DomainID;
                                                objWebAPI.UserContactID = UserContactID;
                                                objWebAPI.WebApiId = WebApiId;
                                                objWebAPI.FlagItemImport = "0|0";
                                                objWebAPI.ManageWebApiItemImport();
                                            }
                                        }
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.Magento) //Magento
                                {
                                    objMagentoClient = new MagentoClient();
                                    string API_URL = CCommon.ToString(dr["vcFirstFldValue"]);
                                    string API_User = CCommon.ToString(dr["vcSecondFldValue"]);
                                    string API_Key = CCommon.ToString(dr["vcThirdFldValue"]);
                                    RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                    WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                    UserContactID = CCommon.ToLong(dr["vcFifteenthFldValue"]);

                                    string ListingRequest = CCommon.ToString((dr["vcFourteenthFldValue"]));
                                    if (!string.IsNullOrEmpty(ListingRequest))
                                    {
                                        string[] Requesttype = ListingRequest.Split('|');
                                        if (!string.IsNullOrEmpty(Requesttype[0]) & Requesttype[0] == "1")
                                        {
                                            if (!string.IsNullOrEmpty(Requesttype[1]))
                                            {
                                                string ListingReqType = Requesttype[1];
                                                objMagentoClient.ImportProductListing(DomainID, WebApiId, RecordOwner, UserContactID, WareHouseID, API_URL, API_User, API_Key, ListingReqType);
                                                objWebAPI.DomainID = DomainID;
                                                objWebAPI.UserContactID = UserContactID;
                                                objWebAPI.WebApiId = WebApiId;
                                                objWebAPI.FlagItemImport = "0|0";
                                                objWebAPI.ManageWebApiItemImport();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (FaultException<error> ex)
                        {
                            error er = ex.Detail;
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetProductListingsCommand:" + er.msg);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetProductListingsCommand:" + er.request);
                        }
                        catch (Exception ex)
                        {
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetProductListingsCommand:" + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetProductListingsCommand:" + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetProductListingsCommand:" + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetProductListingsCommand:" + ex.StackTrace);
                }
            }
        }

        #region Amazon Specific Product Listing Functions

        /// <summary>
        /// (This Method is Applicable only for Amazon MWS as of Now)
        /// Looks for Product Listing Report from Amazon's MWS 
        /// (If it finds any Product Listing report generated)
        /// Adds the Report Id to WebApiOrderReport Table in Bizdatabase setting Flag to identify ReportType as ProductListing Report
        /// </summary>
        private void GetAmazonProductListingReport()
        {
            // while (!m_closingEvent.WaitOne(M_POLL_TIME * OrderReport_Timer))
            while (!m_closingEvent.WaitOne(M_POLL_TIME * 17))
            //while (!m_closingEvent.WaitOne(M_POLL_TIME * 0))
            {
                AmazonClient objAmazonClient = null;

                WebAPI objWebAPI = new WebAPI();
                long DomainID = 0;
                int WebApiId = 0;
                DateTime FromDate, ToDate;
                string ReportType = "ActiveListing";
                //ReportType = "OpenListing1";

                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                long UserContactID = 0;

                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                {
                                    objAmazonClient = new AmazonClient();
                                    ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                    objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                    objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                    objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                    objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);
                                    objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.ServiceURL_Type = 2;
                                    UserContactID = CCommon.ToLong(dr["vcFifteenthFldValue"]);

                                    string ListingRequest = CCommon.ToString((dr["vcFourteenthFldValue"]));
                                    if (!string.IsNullOrEmpty(ListingRequest))
                                    {
                                        string[] Requesttype = ListingRequest.Split('|');
                                        if (Requesttype.Length > 1)
                                        {
                                            if (!string.IsNullOrEmpty(Requesttype[1]) & Requesttype[1] != "0")
                                            {
                                                string ListingReqType = Requesttype[1];
                                                ReportType = "ActiveListing";
                                                //if (ListingReqType == "1")
                                                //    ReportType = "ActiveListing";
                                                //else if (ListingReqType == "2")
                                                //    ReportType = "OpenListing1";
                                                if (CCommon.ToString(dr["vcThirteenthFldValue"]) != "")
                                                    FromDate = Convert.ToDateTime(CCommon.ToString(dr["vcThirteenthFldValue"]));
                                                else
                                                    FromDate = DateTime.UtcNow.AddDays(-1);

                                                ToDate = DateTime.UtcNow.AddHours(8);
                                                objAmazonClient.GetReportIDListToBiz(objSvcConfigInfo, DomainID, WebApiId, FromDate, ToDate, ReportType, UserContactID);
                                                WebAPI objWebApi = new WebAPI();
                                                objWebApi.DomainID = DomainID;
                                                objWebApi.WebApiId = WebApiId;
                                                objWebApi.UpdateAPISettingsDate(4);  //Update WebAPIDetail vcThirteenthFldValue to Current Time
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (FaultException<error> ex)
                        {
                            error er = ex.Detail;
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetAmazonProductListingReport:" + er.msg);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetAmazonProductListingReport:" + er.request);
                        }
                        catch (Exception ex)
                        {
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetAmazonProductListingReport:" + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetAmazonProductListingReport:" + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetAmazonProductListingReport:" + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetAmazonProductListingReport:" + ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// (This Method is Applicable only for Amazon MWS as of Now)
        /// Reads the Product Listing Report File from Specific folder and creates Items in BizDatabase 
        /// and Maps Item with WebApis by ItemAPI Table entry in BizDatabase
        /// </summary>

        //DataTable dtProductListingsMain;
        private void ReadAmazonProductListingReport()
        {
            //while (!m_closingEvent.WaitOne(M_POLL_TIME * ReadOrders_Timer))
            while (!m_closingEvent.WaitOne(M_POLL_TIME * 10))
            //while (!m_closingEvent.WaitOne(M_POLL_TIME * 0))
            {
                AmazonClient objAmazonClient = null;
                WebAPI objWebAPI = new WebAPI();

                int WebApiId = 0;
                int OrderStatus = 0;
                int WareHouseID = 0;
                int ExpenseAccountId = 0;
                long DomainID = 0;
                long BizDocId = 0;
                long RecordOwner = 0;
                long AssignTo = 0;
                long RelationshipId = 0;
                long ProfileId = 0;
                long DiscountItemMapping = 0;
                long UserContactID = 0;
                string Source = "", ErrorMsg = "";

                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US)
                                {
                                    objAmazonClient = new AmazonClient();
                                    string sourceDir = GeneralFunctions.GetPath("ProductListing", DomainID);
                                    if (Directory.Exists(sourceDir))
                                    {
                                        string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                                        if (fileEntries.Length > 0)
                                        {
                                            Source = CCommon.ToString(dr["vcProviderName"]);
                                            OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                            WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                            BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                            RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                            AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                            RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                            ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                            // ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                            ExpenseAccountId = 0;
                                            DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                            UserContactID = CCommon.ToLong(dr["vcFifteenthFldValue"]);
                                            foreach (string filepath in fileEntries)
                                            {
                                                try
                                                {
                                                    DataTable dtProductListings;
                                                    DataSet ds = GeneralFunctions.GetDataSetFromDelimitedFlatFile(filepath, "\t");
                                                    dtProductListings = ds.Tables[0];

                                                    DataView dvProdList = dtProductListings.DefaultView;
                                                    dvProdList.RowFilter = "[item-name] <> ''";
                                                    dtProductListings = dvProdList.ToTable();

                                                    //dtProductListingsMain = dtProductListings.Copy();
                                                    if (dtProductListings.Rows.Count > 0)
                                                    {
                                                        dtProductListings.Columns.Add("IsChild", typeof(short)); // 0  for Parent, 1 for Child record

                                                        if (CCommon.ToString(Path.GetFileNameWithoutExtension(filepath)).Substring(0, 1) == "1")
                                                            dtProductListings.Columns["seller-sku"].ColumnName = "SellerSKU";
                                                        else if (CCommon.ToString(Path.GetFileNameWithoutExtension(filepath)).Substring(0, 1) == "2")
                                                            dtProductListings.Columns["sku"].ColumnName = "SellerSKU";
                                                        else
                                                            throw new Exception("Service1 : From ReadAmazonProductListingReport: " + "Product Listing type conflicts, please check File Name " + Path.GetFileNameWithoutExtension(filepath) + ". 1-> Active Listing, 2-> Complete Listing");
                                                        //objAmazonClient.ProcessProductListingReport(DomainID, WebApiId, RecordOwner,UserContactID, dtProductListings, WareHouseID);
                                                        objAmazonClient.ProcessOpenProductListingReport(DomainID, WebApiId, RecordOwner, UserContactID, ref dtProductListings, WareHouseID);
                                                        File.Copy(filepath, GeneralFunctions.GetPath("ClosedAmazonProductListingReport", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".txt");
                                                        File.Delete(filepath);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    ErrorMsg = "Service1 : From ReadAmazonProductListingReport: " + "Error in reading Amazon Product listing report" + Path.GetFileNameWithoutExtension(filepath);
                                                    File.Copy(filepath, GeneralFunctions.GetPath("ReadingErrorAmazonProdListingReport", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".txt");
                                                    File.Delete(filepath);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ErrorMsg + " -" + ex.Message);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ex.StackTrace);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ex.StackTrace);
                }
            }
        }

        #endregion Amazon Specific Product Listing Functions

        #endregion Product Listing Operation

        #region Add/Update Product(Item) Operations

        /// <summary>
        /// Creates Feed Files to Create new Item in WebAPI or to Update existing Item's Details
        /// </summary>
        private void AddItems_To_API()
        {
            AmazonClient objAmazonClient = null;
            eBayClient objeBayClient = null;
            GoogleClient objGoogleClient = null;
            MagentoClient objMagentoClient = null;
            string LogMessage = "", ErrorMessage = "";
            while (!m_closingEvent.WaitOne(M_POLL_TIME * CreateFeed_Timer))
            // while (!m_closingEvent.WaitOne(0))
            {
                long DomainID = 0;
                int WebApiId = 0;
                DataTable dtItems = new DataTable();
                WebAPI objWebAPI = new WebAPI();
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    string DateCreated, DateModified, Token = "";

                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableItemUpdate"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                objAmazonClient = new AmazonClient();
                                objeBayClient = new eBayClient();
                                objGoogleClient = new GoogleClient();
                                objMagentoClient = new MagentoClient();
                                Token = CCommon.ToString(dr["vcEighthFldValue"]);
                                if (CCommon.ToString(dr["vcTenthFldValue"]).Length > 0)
                                {
                                    DateCreated = CCommon.ToString(dr["vcTenthFldValue"]);
                                }
                                else
                                {
                                    DateCreated = "";//get all products for first time 
                                }
                                DateModified = "";

                                objWebAPI.DomainID = DomainID;
                                objWebAPI.WebApiId = WebApiId;
                                objWebAPI.DateCreated = DateCreated;
                                objWebAPI.DateModified = DateModified;
                                objWebAPI.Token = Token;
                                dtItems = objWebAPI.GetItemsApi();
                                if (dtItems.Rows.Count > 0)
                                {
                                    if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From AddItems_To_API: " + "No of New Products received to List in AmazonUS : " + dtItems.Rows.Count);
                                        objAmazonClient.NewProductFeed(DomainID, dtItems, Token);
                                        //objAmazonClient.UpdateItemFeed(DomainID, dtItems, Token);

                                    }
                                    else if (WebApiId == (int)WebAPIList.EBayUS)//E-Bay US
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From AddItems_To_API: " + "No of New Products received to List in EBayUS : " + dtItems.Rows.Count);
                                        string eBaySite = CCommon.ToString(dr["vcFirstFldValue"]);
                                        string PayPalEmailAddress = CCommon.ToString(dr["vcSecondFldValue"]);
                                        objeBayClient.NewEBayProduct(DomainID, WebApiId, dtItems, Token, eBaySite, PayPalEmailAddress);
                                    }
                                    else if (WebApiId == (int)WebAPIList.GoogleUS)//Google US
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From AddItems_To_API: " + "No of New Products received to List in GoogleUS : " + dtItems.Rows.Count);
                                        string TargetCountry = CCommon.ToString(dr["vcFirstFldValue"]);
                                        string AccountId = CCommon.ToString(dr["vcSecondFldValue"]);
                                        string EMail = CCommon.ToString(dr["vcThirdFldValue"]);
                                        string Password = CCommon.ToString(dr["vcFourthFldValue"]);
                                        objGoogleClient.NewProductBatchProcessing(DomainID, WebApiId, dtItems, AccountId, EMail, Password, TargetCountry);
                                    }
                                    else if (WebApiId == (int)WebAPIList.Magento)//Magento
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From AddItems_To_API: " + "No of New Products received to List in Magento : " + dtItems.Rows.Count);
                                        string API_URL = CCommon.ToString(dr["vcFirstFldValue"]);
                                        string API_User = CCommon.ToString(dr["vcSecondFldValue"]);
                                        string API_Key = CCommon.ToString(dr["vcThirdFldValue"]);
                                        //string Password = CCommon.ToString(dr["vcFourthFldValue"]);
                                        objMagentoClient.AddNewProduct(DomainID, WebApiId, dtItems, API_URL, API_User, API_Key);
                                    }
                                }
                                if (DateCreated == "" && DateModified == "")
                                {
                                    objWebAPI.UpdateAPISettingsDate(0);
                                    break;
                                }
                                if (CCommon.ToString(dr["vcTenthFldValue"]).Length > 0)
                                {
                                    DateModified = CCommon.ToString(dr["vcTenthFldValue"]);
                                }
                                else
                                {
                                    DateModified = "";
                                }
                                DateCreated = "";
                                objWebAPI.DomainID = DomainID;
                                objWebAPI.WebApiId = WebApiId;
                                objWebAPI.DateCreated = DateCreated;
                                objWebAPI.DateModified = DateModified;
                                objWebAPI.Token = Token;
                                dtItems = objWebAPI.GetItemsApi();
                                if (dtItems.Rows.Count > 0)
                                {
                                    if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From AddItems_To_API: " + "No of New Products received to List in AmazonUS : " + dtItems.Rows.Count);
                                        objAmazonClient.NewProductFeed(DomainID, dtItems, Token);
                                        //objAmazonClient.UpdateItemFeed(DomainID, dtItems, Token);

                                    }
                                    else if (WebApiId == (int)WebAPIList.EBayUS)//E-Bay US
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From AddItems_To_API: " + "No of New Products received to List in EBayUS : " + dtItems.Rows.Count);
                                        string eBaySite = CCommon.ToString(dr["vcFirstFldValue"]);
                                        string PayPalEmailAddress = CCommon.ToString(dr["vcSecondFldValue"]);
                                        objeBayClient.NewEBayProduct(DomainID, WebApiId, dtItems, Token, eBaySite, PayPalEmailAddress);
                                    }
                                    else if (WebApiId == (int)WebAPIList.GoogleUS)//Google US
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From AddItems_To_API: " + "No of New Products received to List in GoogleUS : " + dtItems.Rows.Count);
                                        string TargetCountry = CCommon.ToString(dr["vcFirstFldValue"]);
                                        string AccountId = CCommon.ToString(dr["vcSecondFldValue"]);
                                        string EMail = CCommon.ToString(dr["vcThirdFldValue"]);
                                        string Password = CCommon.ToString(dr["vcFourthFldValue"]);
                                        objGoogleClient.NewProductBatchProcessing(DomainID, WebApiId, dtItems, AccountId, EMail, Password, TargetCountry);
                                    }
                                    else if (WebApiId == (int)WebAPIList.Magento)//Magento
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From AddItems_To_API: " + "No of New Products received to List in Magento : " + dtItems.Rows.Count);
                                        string API_URL = CCommon.ToString(dr["vcFirstFldValue"]);
                                        string API_User = CCommon.ToString(dr["vcSecondFldValue"]);
                                        string API_Key = CCommon.ToString(dr["vcThirdFldValue"]);
                                        //string Password = CCommon.ToString(dr["vcFourthFldValue"]);
                                        objMagentoClient.AddNewProduct(DomainID, WebApiId, dtItems, API_URL, API_User, API_Key);
                                    }
                                }

                                objWebAPI.UpdateAPISettingsDate(0);
                            }
                        }
                        catch (Exception ex)
                        {
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From AddItems_To_API: " + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From AddItems_To_API: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From AddItems_To_API: " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From AddItems_To_API: " + ex.StackTrace);
                }
            } 
        }

        #region Amazon Specific

        #region Process Amazon Feed Files

        /// <summary>
        /// Process and Executes Amazon Feed Content Files
        /// </summary>
        private void ProcessAmazonFeeds()
        {
            AmazonClient objAmazonClient = null;
            while (!m_closingEvent.WaitOne(M_POLL_TIME * SubmitFeed_Timer))
            //while (!m_closingEvent.WaitOne(0))
            {
                long DomainID = 0;
                int WebApiId = 0;
                WebAPI objWebAPI = new WebAPI();
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();

                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                        {
                            DomainID = CCommon.ToLong(dr["numDomainId"]);
                            WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                            if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                            {
                                objAmazonClient = new AmazonClient();

                                ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);
                                objSvcConfigInfo.ServiceCountryCode = CCommon.ToString(dr["vcFifthFldValue"]);
                                objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));

                                // objSvcConfigInfo.FeedsServiceURL = CCommon.ToString(dr["vcSixthFldValue"]);
                                //objSvcConfigInfo.OrdersServiceURL = CCommon.ToString(dr["vcSeventhFldValue"]);
                                objSvcConfigInfo.ServiceURL_Type = 2;
                                objAmazonClient.ProcessFeedRequests(objSvcConfigInfo, CCommon.ToString(dr["vcEighthFldValue"]), DomainID);
                            }

                        }
                    }
                }
                catch (Exception Ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ProcessAmazonFeeds: " + Ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ProcessAmazonFeeds: " + Ex.StackTrace);
                }
            }
        }

        #endregion Process Amazon Feed Files

        #region Amazon Item_Api Mapping

        /// <summary>
        /// Gets the WebAPI Item's Unique Indentifier and Updates to ItemAPI in BizDatabase
        /// </summary>
        private void AmazonItemAPIMapping()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * UpdateItemAPI_Timer))
            {
                AmazonClient objAmazonClient = null;
                WebAPI objWebAPI = new WebAPI();
                DataTable dtItems = new DataTable();
                long DomainID = 0;
                int WebApiId = 0;
                string DateCreated, DateModified, Token = "";
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();

                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                        {
                            DomainID = CCommon.ToLong(dr["numDomainId"]);
                            WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                            Token = CCommon.ToString(dr["vcEighthFldValue"]);

                            if (CCommon.ToString(dr["vcEleventhFldValue"]).Length > 0)
                            {
                                DateCreated = CCommon.ToString(dr["vcEleventhFldValue"]);
                            }
                            else
                            {
                                DateCreated = "";//get all products for first time 
                            }
                            DateModified = "";

                            objWebAPI.DomainID = DomainID;
                            objWebAPI.WebApiId = WebApiId;
                            objWebAPI.DateCreated = DateCreated;
                            objWebAPI.DateModified = DateModified;
                            objWebAPI.Token = Token;
                            dtItems = objWebAPI.GetItemsApi();
                            //Get newly created product from last ProductSyncTime
                            if (dtItems.Rows.Count > 0)
                            {
                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                {
                                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From AmazonItemAPIMapping: " + "No of Products Received To Update Item_API Linking for AmazonUS: " + dtItems.Rows.Count);
                                    objAmazonClient = new AmazonClient();

                                    ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                    objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                    objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                    objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                    objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);
                                    objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));

                                    //objSvcConfigInfo.FeedsServiceURL = CCommon.ToString(dr["vcSixthFldValue"]);
                                    //objSvcConfigInfo.OrdersServiceURL = CCommon.ToString(dr["vcSeventhFldValue"]);
                                    objSvcConfigInfo.ServiceURL_Type = 3;
                                    UpdateItem_API_Linking(objSvcConfigInfo, DomainID, WebApiId, dtItems);
                                }
                            }
                            if (DateCreated == "" && DateModified == "")
                            {
                                objWebAPI.UpdateAPISettingsDate(2);
                                break;
                            }
                            if (CCommon.ToString(dr["vcEleventhFldValue"]).Length > 0)
                            {
                                DateModified = CCommon.ToString(dr["vcEleventhFldValue"]);
                            }
                            else
                            {
                                DateModified = "";
                            }
                            DateCreated = "";
                            objWebAPI.DomainID = DomainID;
                            objWebAPI.WebApiId = WebApiId;
                            objWebAPI.DateCreated = DateCreated;
                            objWebAPI.DateModified = DateModified;
                            objWebAPI.Token = Token;
                            dtItems = objWebAPI.GetItemsApi();
                            //Get newly modified products from last ProductSyncTime
                            if (dtItems.Rows.Count > 0)
                            {
                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                {
                                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From AmazonItemAPIMapping: " + "No of Products Received To Update Item_API Linking for AmazonUS: " + dtItems.Rows.Count);
                                    objAmazonClient = new AmazonClient();

                                    ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                    objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                    objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                    objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                    objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);

                                    objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));

                                    //objSvcConfigInfo.FeedsServiceURL = CCommon.ToString(dr["vcSixthFldValue"]);
                                    //objSvcConfigInfo.OrdersServiceURL = CCommon.ToString(dr["vcSeventhFldValue"]);
                                    objSvcConfigInfo.ServiceURL_Type = 3;
                                    UpdateItem_API_Linking(objSvcConfigInfo, DomainID, WebApiId, dtItems);
                                }

                            }
                            objWebAPI.UpdateAPISettingsDate(2);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From AmazonItemAPIMapping: " + ex.Message);
                    // GeneralFunctions.WriteMessage(DomainID, "ERR", ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// Used Only for Amazon
        /// Collects SKU for New Items and Gets the WebAPI Item Codes from Service and updates BizDatabase respectively 
        /// </summary>
        /// <param name="objSvcConfigInfo">Amazon Service configuration informations</param>
        /// <param name="DomainId">Domain Id</param>
        /// <param name="WebApiId">Web API Id</param>
        /// <param name="dtItems">Items datatable</param>
        private void UpdateItem_API_Linking(ServiceConfigInfo objSvcConfigInfo, long DomainId, int WebApiId, DataTable dtItems)
        {
            AmazonClient objAmazonClient = null;
            List<string> lstSKU = new List<string>();
            WebAPI objWebAPI = new WebAPI();
            Dictionary<string, string> ItemCode_SKU = new Dictionary<string, string>();
            Dictionary<string, string> SKU_ASIN = new Dictionary<string, string>();
            string ErrorMsg = "";
            string LogMessage = "";
            try
            {
                foreach (DataRow dr in dtItems.Rows)
                {
                    try
                    {
                        if (CCommon.ToString(dr["numItemCode"]).Length > 0 && CCommon.ToString(dr["vcAPIItemID"]) == "0")
                        {
                            if (CCommon.ToString(dr["vcSKU"]).Length > 0 & CCommon.ToString(dr["vcItemName"]).Length > 0 & CCommon.ToString(dr["numBarCodeId"]).Length > 0)
                            {
                                lstSKU.Add(CCommon.ToString(dr["vcSKU"]));

                                if (!ItemCode_SKU.ContainsKey(CCommon.ToString(dr["vcSKU"])))
                                {
                                    ItemCode_SKU.Add(CCommon.ToString(dr["numItemCode"]), CCommon.ToString(dr["vcSKU"]));
                                }
                            }
                        }
                        else
                        {
                            //LogMessage = "API Item Id is already updated for Item Code : " + CCommon.ToString(dr["numItemCode"]) + " ";
                            //GeneralFunctions.WriteMessage("LOG", LogMessage);
                        }
                    }
                    catch (Exception Ex)
                    {
                        ErrorMsg = "Error Getting Item Details for Item Code : " + CCommon.ToString(dr["numItemCode"] + " to Update Item_API linking");
                        GeneralFunctions.WriteMessage(DomainId, "ERR", ErrorMsg + Ex.Message);
                    }
                }

                if (lstSKU.Count > 0)
                {
                    objAmazonClient = new AmazonClient();
                    List<string> lstSKU_Part = new List<string>();
                    Dictionary<string, string> SKU_ASIN_Part = new Dictionary<string, string>();
                    if (lstSKU.Count > 20)
                    {
                        for (int Cntr = 0; Cntr < lstSKU.Count; Cntr++)
                        {
                            lstSKU_Part.Add(lstSKU[Cntr]);
                            if (lstSKU_Part.Count % 20 == 0)
                            {
                                SKU_ASIN_Part = objAmazonClient.GetASIN_From_SKU(objSvcConfigInfo, lstSKU_Part);
                                foreach (KeyValuePair<string, string> skupart in SKU_ASIN_Part)
                                {
                                    SKU_ASIN.Add(skupart.Key, skupart.Value);
                                }
                                lstSKU_Part.Clear();
                                SKU_ASIN_Part.Clear();
                            }
                        }
                    }
                    else
                    {
                        SKU_ASIN = objAmazonClient.GetASIN_From_SKU(objSvcConfigInfo, lstSKU);
                    }
                    objWebAPI.UserCntID = GetRoutingRulesUserCntID(DomainId, "", "");

                    foreach (KeyValuePair<string, string> KeyValue in ItemCode_SKU)
                    {
                        if (SKU_ASIN.ContainsKey(CCommon.ToString(KeyValue.Value)))
                        {
                            objWebAPI.ItemCode = CCommon.ToLong(KeyValue.Key);
                            objWebAPI.Product_Id = SKU_ASIN[CCommon.ToString(KeyValue.Value)];
                            objWebAPI.DomainID = DomainId;
                            objWebAPI.WebApiId = WebApiId;
                            objWebAPI.SKU = CCommon.ToString(KeyValue.Value);
                            objWebAPI.AddItemAPILinking();
                            LogMessage = "Service1 : From UpdateItem_API_Linking: " + "Amazon Item Id (ASIN) " + objWebAPI.Product_Id + " is updated for Item Code : " + CCommon.ToString(objWebAPI.ItemCode);
                            GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                        }
                        else
                        {
                            ErrorMsg = "Couldnot find Amazon Item Id (ASIN) for ItemCode  : " + CCommon.ToString(KeyValue.Key);
                            GeneralFunctions.WriteMessage(DomainId, "LOG", "Service1 : From UpdateItem_API_Linking: " + ErrorMsg);
                            throw new Exception("Service1 : From UpdateItem_API_Linking: " + ErrorMsg);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        #endregion Amazon Item_Api Mapping

        #endregion Amazon Specific

        #endregion Add/Update Product(Item) Operations

        #region Inventory Management

        private void InventoryManagement()
        {
            AmazonClient objAmazonClient = null;
            eBayClient objeBayClient = null;
            GoogleClient objGoogleClient = null;
            MagentoClient objMagentoClient = null;
            string LogMessage = "", ErrorMessage = "";
            while (!m_closingEvent.WaitOne(M_POLL_TIME * UpdateInventory_Timer))
            // while (!m_closingEvent.WaitOne(0))
            {
                long DomainID = 0;
                int WebApiId = 0;
                DataTable dtItems = new DataTable();
                WebAPI objWebAPI = new WebAPI();
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    string DateModified = "", Token = "";

                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableInventoryUpdate"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                objAmazonClient = new AmazonClient();
                                objeBayClient = new eBayClient();
                                objGoogleClient = new GoogleClient();
                                objMagentoClient = new MagentoClient();
                                Token = CCommon.ToString(dr["vcEighthFldValue"]);
                                if (CCommon.ToString(dr["vcTwelfthFldValue"]).Length > 0)
                                {
                                    DateModified = CCommon.ToString(dr["vcTwelfthFldValue"]);
                                }
                                else
                                {
                                    DateModified = "";//get all products for first time 
                                }
                                objWebAPI.DomainID = DomainID;
                                objWebAPI.WebApiId = WebApiId;
                                objWebAPI.DateModified = DateModified;
                                objWebAPI.Token = Token;
                                dtItems = objWebAPI.GetItemsInventoryApi();
                                if (dtItems.Rows.Count > 0)
                                {
                                    if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From InventoryManagement: " + "No of New Products received to update Inventory in AmazonUS : " + dtItems.Rows.Count);
                                        objAmazonClient.UpdateInventoryFeed(DomainID, dtItems, Token);
                                    }
                                    else if (WebApiId == (int)WebAPIList.EBayUS)//E-Bay US
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From InventoryManagement: " + "No of New Products received to update Inventory in EBayUS : " + dtItems.Rows.Count);
                                        string eBaySite = CCommon.ToString(dr["vcFirstFldValue"]);
                                        string PayPalEmailAddress = CCommon.ToString(dr["vcSecondFldValue"]);
                                        objeBayClient.UpdateInventory(DomainID, WebApiId, dtItems, Token, eBaySite, PayPalEmailAddress);
                                    }
                                    else if (WebApiId == (int)WebAPIList.GoogleUS)//Google US
                                    {
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From InventoryManagement: " + "No of New Products received to update Inventory in GoogleUS : " + dtItems.Rows.Count);
                                        string TargetCountry = CCommon.ToString(dr["vcFirstFldValue"]);
                                        string AccountId = CCommon.ToString(dr["vcSecondFldValue"]);
                                        string EMail = CCommon.ToString(dr["vcThirdFldValue"]);
                                        string Password = CCommon.ToString(dr["vcFourthFldValue"]);
                                        objGoogleClient.UpdateInventoryBatchProcessing(DomainID, WebApiId, dtItems, AccountId, EMail, Password, TargetCountry);
                                    }
                                    else if (WebApiId == (int)WebAPIList.Magento)//Magento
                                    {

                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From InventoryManagement: " + "No of New Products received to update Inventory in Magento : " + dtItems.Rows.Count);
                                        string API_URL = CCommon.ToString(dr["vcFirstFldValue"]);
                                        string API_User = CCommon.ToString(dr["vcSecondFldValue"]);
                                        string API_Key = CCommon.ToString(dr["vcThirdFldValue"]);
                                        //string Password = CCommon.ToString(dr["vcFourthFldValue"]);
                                        objMagentoClient.UpdateInventory(DomainID, WebApiId, dtItems, API_URL, API_User, API_Key);
                                    }
                                }
                                objWebAPI.UpdateAPISettingsDate(3);
                            }
                        }
                        catch (Exception ex)
                        {
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From InventoryManagement: " + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From InventoryManagement: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From InventoryManagement: " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From InventoryManagement: " + ex.StackTrace);
                }
            }
        }

        #endregion Inventory Management

        #region Report Operations

        /// <summary>
        ///  Gets Order List from different Marketplaces 
        ///  (and Saves to Order report (.xml) file for Google and Ebay to Seperate folder for each Marketplaces respectively.)  
        ///  (and Saves Order ID to Amazon and Magento)
        /// </summary>
        private void GetOrderReportList()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * OrderList_Timer))
            // while (!m_closingEvent.WaitOne(M_POLL_TIME * 0))
            {
                AmazonClient objAmazonClient = null;
                eBayClient objEBayClient = null;
                GoogleClient objGoogleClient = null;
                MagentoClient objMagentoClient = null;

                WebAPI objWebAPI = new WebAPI();
                long DomainID = 0;
                int WebApiId = 0;
                DateTime FromDate, ToDate;
                string ReportType = "ScheduledXMLOrderReport";
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableOrderImport"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);

                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                {
                                    objAmazonClient = new AmazonClient();
                                    ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                    objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                    objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                    objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                    objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);
                                    objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.ServiceURL_Type = 2;

                                    if (CCommon.ToString(dr["vcNinthFldValue"]) != "")
                                        FromDate = Convert.ToDateTime(CCommon.ToString(dr["vcNinthFldValue"]));
                                    else
                                    {
                                        //FromDate = new DateTime(2013, 1, 2, 0, 00, 00);
                                        FromDate = DateTime.Now.AddDays(-10);
                                    }
                                    //ToDate = DateTime.UtcNow;
                                    ToDate = FromDate.AddDays(1);
                                    if (ToDate > DateTime.UtcNow)
                                    {
                                        ToDate = DateTime.UtcNow;
                                    }

                                    objAmazonClient.GetReportIDListToBiz(objSvcConfigInfo, DomainID, WebApiId, FromDate, ToDate, ReportType);
                                    CCommon objCommon = new CCommon();
                                    objCommon.DomainID = DomainID;
                                    objCommon.Mode = 38;
                                    objCommon.UpdateRecordID = WebApiId;
                                    objCommon.Comments = CCommon.ToString(ToDate);
                                    objCommon.UpdateSingleFieldValue();

                                    //WebAPI objWebApi = new WebAPI();
                                    //objWebApi.DomainID = DomainID;
                                    //objWebApi.WebApiId = WebApiId;
                                    //objWebApi.UpdateAPISettingsDate(1);  //Update WebAPIDetail vcNinthFldValue to Current UTC Time
                                }
                                else if (WebApiId == (int)WebAPIList.EBayUS)//E-Bay US
                                {
                                    objEBayClient = new eBayClient();
                                    string eBaySite = CCommon.ToString(dr["vcFirstFldValue"]);
                                    string Token = CCommon.ToString(dr["vcEighthFldValue"]);

                                    if (CCommon.ToString(dr["vcNinthFldValue"]) != "")
                                        FromDate = Convert.ToDateTime(CCommon.ToString(dr["vcNinthFldValue"]));
                                    else
                                    {
                                        FromDate = DateTime.Now.AddDays(-10);
                                        // FromDate = new DateTime(2013, 3, 1, 0, 00, 00);
                                    }

                                    //ToDate = FromDate.AddMinutes(20);
                                    //if (ToDate > DateTime.Now)
                                    //{
                                    //    ToDate = DateTime.Now;
                                    //}
                                    //ToDate = FromDate.AddMinutes(20);
                                    ToDate = DateTime.UtcNow;
                                    objEBayClient.GeteBayOrders(DomainID, WebApiId, Token, eBaySite, FromDate, ToDate);
                                    //CCommon objCommon = new CCommon();
                                    //objCommon.DomainID = DomainID;
                                    //objCommon.Mode = 38;
                                    //objCommon.UpdateRecordID = WebApiId;
                                    //objCommon.Comments = CCommon.ToString(ToDate);
                                    //objCommon.UpdateSingleFieldValue();

                                }
                                else if (WebApiId == (int)WebAPIList.GoogleUS)//Google US
                                {
                                    objGoogleClient = new GoogleClient();
                                    string MerchantId = CCommon.ToString(dr["vcFifthFldValue"]);
                                    string MerchantKey = CCommon.ToString(dr["vcSixthFldValue"]);
                                    string Token = CCommon.ToString(dr["vcEighthFldValue"]);
                                    objGoogleClient.GetGoogleOrders(DomainID, WebApiId, MerchantId, MerchantKey, Token);
                                }
                                else if (WebApiId == (int)WebAPIList.Magento)// Magento
                                {
                                    objMagentoClient = new MagentoClient();
                                    string API_URL = CCommon.ToString(dr["vcFirstFldValue"]);
                                    string API_User = CCommon.ToString(dr["vcSecondFldValue"]);
                                    string API_Key = CCommon.ToString(dr["vcThirdFldValue"]);

                                    if (CCommon.ToString(dr["vcNinthFldValue"]) != "")
                                        FromDate = Convert.ToDateTime(CCommon.ToString(dr["vcNinthFldValue"]));
                                    else
                                    {
                                        FromDate = DateTime.Now.AddDays(-10);
                                        // FromDate = new DateTime(2013, 1, 1, 0, 00, 00);
                                    }
                                    objMagentoClient.GetOrderIDList(DomainID, WebApiId, API_URL, API_User, API_Key, FromDate);
                                }
                            }
                        }
                        catch (FaultException<error> ex)
                        {
                            error er = ex.Detail;
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReportList: " + er.msg);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReportList: " + er.request);
                        }
                        catch (Exception ex)
                        {
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReportList: " + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReportList: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReportList: " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReportList: " + ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// (As of Now this method is Applicable for Amazon and Magento)
        /// Gets Top WebAPIOrderReportID from WebApiOrderReports Table from BizDatabase for each webApiId and DomainId 
        /// Gets Order details for the WebAPIOrderReportID and Saves to Order report (.xml) file  to Seperate folder for each Marketplaces respectively.
        /// and Updates the BizDatabase as WebAPIOrderReportID is Processed
        /// </summary>
        private void GetOrderReport()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * OrderReport_Timer))
            // while (!m_closingEvent.WaitOne(M_POLL_TIME * 0))
            {
                AmazonClient objAmazonClient = null;
                MagentoClient objMagentoClient = null;
                WebAPI objWebAPI = new WebAPI();
                long DomainID = 0;
                int WebApiId = 0;
                string LogMessage = "";
                string Source = "";
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                Source = CCommon.ToString(dr["vcProviderName"]);
                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                {
                                    string OrderReportID = "";
                                    int ReportTypeId = 0;
                                    objAmazonClient = new AmazonClient();
                                    DataTable dtTopReport = GetTopOrderReportID(DomainID, WebApiId); //Gets top 1 WebApiReportId and ReportType
                                    if (dtTopReport.Rows.Count > 0)
                                    {
                                        OrderReportID = CCommon.ToString(dtTopReport.Rows[0]["ReportId"]);
                                        ReportTypeId = CCommon.ToInteger(dtTopReport.Rows[0]["tintReportTypeId"]);

                                        if (OrderReportID != "" & OrderReportID != "0")
                                        {
                                            ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                            objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                            objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                            objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                            objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);
                                            objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                            objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                            objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                            objSvcConfigInfo.ServiceURL_Type = 2;

                                            objAmazonClient.GetReportByID(objSvcConfigInfo, OrderReportID, ReportTypeId, DomainID, WebApiId);

                                            objWebAPI.DomainID = DomainID;
                                            objWebAPI.WebApiId = WebApiId;
                                            objWebAPI.WebApiOrdReportId = 1;
                                            objWebAPI.API_OrderReportId = OrderReportID;
                                            objWebAPI.API_ReportStatus = 1;
                                            objWebAPI.RStatus = 0;
                                            objWebAPI.ManageWebAPIOrderReports();
                                        }
                                    }
                                    else
                                    {
                                        //LogMessage = "Order Details are UpTo Date for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                        //GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.Magento) //Magento
                                {
                                    objMagentoClient = new MagentoClient();
                                    string OrderReportID = "";
                                    int ReportTypeId = 0;

                                    DataTable dtTopReport = GetTopOrderReportID(DomainID, WebApiId); //Gets top 1 WebApiReportId and ReportType
                                    if (dtTopReport.Rows.Count > 0)
                                    {
                                        OrderReportID = CCommon.ToString(dtTopReport.Rows[0]["ReportId"]);
                                        ReportTypeId = CCommon.ToInteger(dtTopReport.Rows[0]["tintReportTypeId"]);

                                        if (OrderReportID != "" & OrderReportID != "0")
                                        {
                                            string API_URL = CCommon.ToString(dr["vcFirstFldValue"]);
                                            string API_User = CCommon.ToString(dr["vcSecondFldValue"]);
                                            string API_Key = CCommon.ToString(dr["vcThirdFldValue"]);
                                            objMagentoClient.GetOrderReport(DomainID, WebApiId, API_URL, API_User, API_Key, OrderReportID);

                                            objWebAPI.DomainID = DomainID;
                                            objWebAPI.WebApiId = WebApiId;
                                            objWebAPI.WebApiOrdReportId = 1;
                                            objWebAPI.API_OrderReportId = OrderReportID;
                                            objWebAPI.API_ReportStatus = 1;
                                            objWebAPI.RStatus = 0;
                                            objWebAPI.ManageWebAPIOrderReports();
                                        }
                                    }
                                    else
                                    {
                                        //LogMessage = "Order Details are UpTo Date for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                        //GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                                    }
                                }
                            }
                        }
                        catch (FaultException<error> ex)
                        {
                            error er = ex.Detail;
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReport: " + er.msg);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReport: " + er.request);
                        }
                        catch (Exception ex)
                        {
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReport: " + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReport: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReport: " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderReport: " + ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// Reads the Order Report from respective folder for each Markeplace and makes Sales Order entry into BizDatabase
        /// </summary>
        private void ReadOrdersReport()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * ReadOrders_Timer))
            // while (!m_closingEvent.WaitOne(M_POLL_TIME * 0))
            {
                AmazonClient objAmazonClient = null;
                eBayClient objEBayClient = null;
                GoogleClient objGoogleClient = null;
                MagentoClient objMagentoClient = null;

                WebAPI objWebAPI = new WebAPI();
                int WebApiId = 0;
                int OrderStatus = 0;
                int WareHouseID = 0;
                int ExpenseAccountId = 0;
                long DomainID = 0;
                long BizDocId = 0;
                long BizDocStatusId = 0;
                long RecordOwner = 0;
                long AssignTo = 0;
                long RelationshipId = 0;
                long ProfileId = 0;
                long DiscountItemMapping = 0;
                long SalesTaxItemMappingID = 0;
                long ShippingServiceItemID = 0;
                string ShipToPhoneNo = "";
                string Source = "", ErrorMsg = "";

                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableOrderImport"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US)
                                {
                                    objAmazonClient = new AmazonClient();
                                    string FileName = "", ReportIdFromFile = "";
                                    string sourceDir = GeneralFunctions.GetPath(AmazonOrderFilePath, DomainID);
                                    if (Directory.Exists(sourceDir))
                                    {
                                        string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                                        if (fileEntries.Length > 0)
                                        {
                                            Source = CCommon.ToString(dr["vcProviderName"]);
                                            OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                            WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                            BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                            BizDocStatusId = CCommon.ToLong(dr["numBizDocStatusId"]);
                                            RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                            AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                            RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                            ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                            //ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                            ExpenseAccountId = 0;
                                            DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                            ShippingServiceItemID = CCommon.ToLong(dr["numShippingServiceItemID"]);
                                            SalesTaxItemMappingID = CCommon.ToLong(dr["numSalesTaxItemMapping"]);
                                            ShipToPhoneNo = CCommon.ToString(dr["vcShipToPhoneNo"]);
                                            foreach (string filepath in fileEntries)
                                            {
                                                FileName = Path.GetFileNameWithoutExtension(filepath);
                                                ReportIdFromFile = FileName.Substring(0, (FileName.IndexOf('_')));
                                                try
                                                {
                                                    AmazonEnvelope objAmaEnv = new AmazonEnvelope();
                                                    GeneralFunctions.DeSerializeFileToObject<AmazonEnvelope>(out objAmaEnv, filepath);
                                                    if (objAmaEnv != null & objAmaEnv.MessageType == AmazonEnvelopeMessageType.OrderReport)
                                                    {
                                                        foreach (AmazonEnvelopeMessage Message in objAmaEnv.Message)
                                                        {
                                                            OrderReport objOrdReport = new OrderReport();
                                                            objOrdReport = (OrderReport)Message.Item;
                                                            objAmazonClient.NewReadAmazonOrders(objOrdReport, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID, ReportIdFromFile, ShipToPhoneNo);
                                                        }
                                                        // objAmazonClient.ProcessAmazonOrders(objAmaEnv, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID);
                                                    }
                                                    File.Copy(filepath, GeneralFunctions.GetPath(AmazonClosedOrdersPath, DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                    File.Delete(filepath);
                                                }
                                                catch (Exception ex)
                                                {
                                                    ErrorMsg = "Error in reading Amazon order report file : " + Path.GetFileNameWithoutExtension(filepath);
                                                    File.Copy(filepath, GeneralFunctions.GetPath("Error_AmazonOrderReport", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                    File.Delete(filepath);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ErrorMsg + ex.Message);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ex.StackTrace);
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.EBayUS) //E-Bay US
                                {
                                    objEBayClient = new eBayClient();
                                    string sourceDir = GeneralFunctions.GetPath(EBayOrderFilePath, DomainID);
                                    if (Directory.Exists(sourceDir))
                                    {
                                        string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                                        if (fileEntries.Length > 0)
                                        {
                                            Source = CCommon.ToString(dr["vcProviderName"]);
                                            OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                            WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                            BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                            BizDocStatusId = CCommon.ToLong(dr["numBizDocStatusId"]);
                                            RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                            AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                            RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                            ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                            //ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                            ExpenseAccountId = 0;
                                            DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                            ShippingServiceItemID = CCommon.ToLong(dr["numShippingServiceItemID"]);
                                            SalesTaxItemMappingID = CCommon.ToLong(dr["numSalesTaxItemMapping"]);
                                            ShipToPhoneNo = CCommon.ToString(dr["vcShipToPhoneNo"]);
                                            foreach (string filepath in fileEntries)
                                            {
                                                try
                                                {
                                                    OrderTypeCollection OrderCollections = new OrderTypeCollection();
                                                    GeneralFunctions.DeSerializeFileToObject<OrderTypeCollection>(out OrderCollections, filepath);
                                                    if (OrderCollections != null)
                                                        objEBayClient.ProcessEBayOrders(OrderCollections, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID, ShipToPhoneNo);
                                                    File.Copy(filepath, GeneralFunctions.GetPath(EBayClosedOrders, DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                    File.Delete(filepath);
                                                }
                                                catch (Exception ex)
                                                {
                                                    ErrorMsg = "Error in reading E-Bay order report file : " + Path.GetFileNameWithoutExtension(filepath);
                                                    File.Copy(filepath, GeneralFunctions.GetPath("Error_EBayOrderReport", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                    File.Delete(filepath);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ErrorMsg + ex.Message);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ex.StackTrace);
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.GoogleUS) //Google US
                                {
                                    objGoogleClient = new GoogleClient();
                                    string sourceDir = GeneralFunctions.GetPath(GoogleOrderFilePath, DomainID);
                                    if (Directory.Exists(sourceDir))
                                    {
                                        string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                                        if (fileEntries.Length > 0)
                                        {
                                            Source = CCommon.ToString(dr["vcProviderName"]);
                                            OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                            WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                            BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                            BizDocStatusId = CCommon.ToLong(dr["numBizDocStatusId"]);
                                            RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                            AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                            RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                            ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                            //ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                            ExpenseAccountId = 0;
                                            DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                            ShippingServiceItemID = CCommon.ToLong(dr["numShippingServiceItemID"]);
                                            SalesTaxItemMappingID = CCommon.ToLong(dr["numSalesTaxItemMapping"]);
                                            ShipToPhoneNo = CCommon.ToString(dr["vcShipToPhoneNo"]);
                                            foreach (string filepath in fileEntries)
                                            {
                                                try
                                                {
                                                    GCheckout.AutoGen.ChargeAmountNotification N1;// = new GCheckout.AutoGen.NewOrderNotification();
                                                    GeneralFunctions.DeSerializeFileToObject<GCheckout.AutoGen.ChargeAmountNotification>(out N1, filepath);
                                                    if (N1 != null)
                                                    {
                                                        objGoogleClient.ProcessGoogleCheckOutOrders(N1, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID, ShipToPhoneNo);
                                                        File.Copy(filepath, GeneralFunctions.GetPath(GoogleClosedOrders, DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                        File.Delete(filepath);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    ErrorMsg = "Error in reading Google order report file : " + Path.GetFileNameWithoutExtension(filepath);
                                                    File.Copy(filepath, GeneralFunctions.GetPath("Error_GoogleOrderReport", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                    File.Delete(filepath);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ErrorMsg + ex.Message);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ex.StackTrace);
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.Magento) //Magento
                                {
                                    objMagentoClient = new MagentoClient();
                                    string sourceDir = GeneralFunctions.GetPath(MagentoOrderFilePath, DomainID);
                                    if (Directory.Exists(sourceDir))
                                    {
                                        string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                                        if (fileEntries.Length > 0)
                                        {

                                            Source = CCommon.ToString(dr["vcProviderName"]);
                                            OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                            WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                            BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                            BizDocStatusId = CCommon.ToLong(dr["numBizDocStatusId"]);
                                            RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                            AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                            RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                            ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                            //ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                            ExpenseAccountId = 0;
                                            DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                            ShippingServiceItemID = CCommon.ToLong(dr["numShippingServiceItemID"]);
                                            SalesTaxItemMappingID = CCommon.ToLong(dr["numSalesTaxItemMapping"]);
                                            ShipToPhoneNo = CCommon.ToString(dr["vcShipToPhoneNo"]);

                                            foreach (string filepath in fileEntries)
                                            {
                                                try
                                                {
                                                    Ez.Newsletter.MagentoApi.OrderInfo OrderDetail;// = new GCheckout.AutoGen.NewOrderNotification();
                                                    GeneralFunctions.DeSerializeFileToObject<Ez.Newsletter.MagentoApi.OrderInfo>(out OrderDetail, filepath);
                                                    if (OrderDetail != null)
                                                    {
                                                        objMagentoClient.ProcessMagentoOrders(OrderDetail, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID, ShipToPhoneNo);
                                                        File.Copy(filepath, GeneralFunctions.GetPath(MagentoClosedOrders, DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                        File.Delete(filepath);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    ErrorMsg = "Error in reading Magento order report file : " + Path.GetFileNameWithoutExtension(filepath);
                                                    File.Copy(filepath, GeneralFunctions.GetPath("Error_MagentoOrderReport", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                    File.Delete(filepath);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ErrorMsg + ex.Message);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ex.StackTrace);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadOrdersReport: " + ex.StackTrace);
                }
            }
        }

        #endregion  Report Operations

        #region Fulfillment Operations

        /// <summary>
        /// This Function Reads for Tracking Number Update in BizDatabase. If it finds any new Tracking Number updated in BizDatabase 
        /// it Updates Tracking Number to respective Online Marketplace and Confirms to BizDatabase by setting flag as it is Updated to Online Marketplace (OMP)
        /// </summary>
        private void UpdateOrderFulfillment()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * OrderFullfilment_Timer))
            // while (!m_closingEvent.WaitOne(0))
            {
                AmazonClient objAmazonClient = null;
                eBayClient objeBayClient = null;
                GoogleClient objGoogleClient = null;
                MagentoClient objMagentoClient = null;

                WebAPI objWebApi = new WebAPI();
                long DomainId = 0, WebApiId = 0;
                long numOppBizDocID = 0;
                long OppBizDocItemID = 0;
                long numItemCode = 0;
                string vcAPIItemId = "";
                string vcAPIOppId = "";
                string vcApiOppItemId = "";
                string vcTrackingNo = "";
                string vcShippingMethod = "";
                string MerchantToken = "";
                string LogMessage = "";
                CCommon objCommon = new CCommon();
                DataTable dtTrackingDetails = new DataTable();

                try
                {
                    objWebApi.Mode = 2;
                    DataTable dtWebAPI = objWebApi.GetWebApi();

                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        if (CCommon.ToBool(dr["bitEnableTrackingUpdate"]) == true)
                        {
                            DomainId = CCommon.ToLong(dr["numDomainId"]);
                            WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                            MerchantToken = CCommon.ToString(dr["vcEighthFldValue"]);
                            objWebApi.WebApiId = (int)WebApiId;
                            objWebApi.DomainID = DomainId;
                            dtTrackingDetails = objWebApi.GetTrackingDetails();

                            if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US)
                            {
                                objAmazonClient = new AmazonClient();
                                string ShippingCarrier = "FedEx";
                                foreach (DataRow datarow in dtTrackingDetails.Rows)
                                {
                                    try
                                    {
                                        numOppBizDocID = CCommon.ToLong(datarow["numOppBizDocID"]);
                                        numItemCode = CCommon.ToLong(datarow["numItemCode"]);
                                        OppBizDocItemID = CCommon.ToLong(datarow["numOppBizDocItemID"]);
                                        vcAPIOppId = CCommon.ToString(datarow["vcAPIOppId"]);
                                        vcTrackingNo = CCommon.ToString(datarow["vcTrackingNo"]);
                                        vcShippingMethod = CCommon.ToString(datarow["vcShippingMethod"]);
                                        vcApiOppItemId = CCommon.ToString(datarow["vcApiOppItemId"]);
                                        ShippingCarrier = CCommon.ToString(datarow["ShippingCarrier"]);
                                        // vcShippingMethod = "Standard";
                                        // string[] TrackingNum = vcTrackingNo.Split(',');
                                        objAmazonClient.CreateOrderFulfillmentFeed(DomainId, vcAPIOppId, vcApiOppItemId, ShippingCarrier, vcTrackingNo, vcShippingMethod, numOppBizDocID, MerchantToken);

                                        objCommon.Mode = 22;
                                        objCommon.UpdateRecordID = OppBizDocItemID;
                                        objCommon.UpdateValueID = 1;
                                        objCommon.UpdateSingleFieldValue();//Set Flag as Tracking details Update Processed for BizDocItem
                                        LogMessage = "Feed content created for Merchant : " + MerchantToken + " FeedType : ORDER FULFILLMENT" + ", for AmazonOrderId : " + vcAPIOppId;
                                        GeneralFunctions.WriteMessage(DomainId, "LOG", "Service1 : From UpdateOrderFulfillment: " + LogMessage);
                                    }
                                    catch (Exception ex)
                                    {
                                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Service1 : From UpdateOrderFulfillment: " + ex.Message);
                                    }
                                }
                            }
                            else if (WebApiId == (int)WebAPIList.EBayUS) //E-Bay US
                            {
                                objeBayClient = new eBayClient();
                                string eBaySite = CCommon.ToString(dr["vcFirstFldValue"]);
                                string Token = CCommon.ToString(dr["vcEighthFldValue"]);
                                string ShippingCarrier = "FedEx";
                                foreach (DataRow datarow in dtTrackingDetails.Rows)
                                {
                                    try
                                    {
                                        numOppBizDocID = CCommon.ToLong(datarow["numOppBizDocID"]);
                                        numItemCode = CCommon.ToLong(datarow["numItemCode"]);
                                        OppBizDocItemID = CCommon.ToLong(datarow["numOppBizDocItemID"]);
                                        vcAPIOppId = CCommon.ToString(datarow["vcAPIOppId"]);
                                        vcAPIItemId = CCommon.ToString(datarow["vcAPIItemId"]);
                                        vcTrackingNo = CCommon.ToString(datarow["vcTrackingNo"]);
                                        vcShippingMethod = CCommon.ToString(datarow["vcShippingMethod"]);
                                        vcApiOppItemId = CCommon.ToString(datarow["vcApiOppItemId"]);
                                        ShippingCarrier = CCommon.ToString(datarow["ShippingCarrier"]);

                                        objeBayClient.UpdateTrackingNo(DomainId, WebApiId, Token, eBaySite, numItemCode, vcAPIItemId, vcAPIOppId, vcApiOppItemId, vcTrackingNo, ShippingCarrier);

                                        objCommon.Mode = 22;
                                        objCommon.UpdateRecordID = OppBizDocItemID;
                                        objCommon.UpdateValueID = 1;
                                        objCommon.UpdateSingleFieldValue();//Set Flag as Tracking details Update Processed for BizDocItem
                                        LogMessage = "Tracking Number : " + vcTrackingNo + " is updated for E-Bay OrderId : " + vcAPIOppId + " and E-Bay Order Line Item Id : " + vcApiOppItemId;
                                        GeneralFunctions.WriteMessage(DomainId, "LOG", "Service1 : From UpdateOrderFulfillment: " + LogMessage);
                                    }
                                    catch (Exception ex)
                                    {
                                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Service1 : From UpdateOrderFulfillment: " + ex.Message);
                                    }
                                }
                            }
                            else if (WebApiId == (int)WebAPIList.GoogleUS) //Google US
                            {
                                objGoogleClient = new GoogleClient();
                                string eBaySite = CCommon.ToString(dr["vcFirstFldValue"]);
                                string Token = CCommon.ToString(dr["vcEighthFldValue"]);
                                string ShippingCarrier = "FedEx";
                                string MerchantId = CCommon.ToString(dr["vcFifthFldValue"]);
                                string MerchantKey = CCommon.ToString(dr["vcSixthFldValue"]);

                                foreach (DataRow datarow in dtTrackingDetails.Rows)
                                {
                                    try
                                    {
                                        numOppBizDocID = CCommon.ToLong(datarow["numOppBizDocID"]);
                                        numItemCode = CCommon.ToLong(datarow["numItemCode"]);
                                        OppBizDocItemID = CCommon.ToLong(datarow["numOppBizDocItemID"]);
                                        vcAPIOppId = CCommon.ToString(datarow["vcAPIOppId"]);
                                        vcAPIItemId = CCommon.ToString(datarow["vcAPIItemId"]);
                                        vcTrackingNo = CCommon.ToString(datarow["vcTrackingNo"]);
                                        vcShippingMethod = CCommon.ToString(datarow["vcShippingMethod"]);
                                        vcApiOppItemId = CCommon.ToString(datarow["vcApiOppItemId"]);
                                        int TrackingNoUpdated = CCommon.ToInteger(datarow["numTrackingNoUpdated"]);
                                        ShippingCarrier = CCommon.ToString(datarow["ShippingCarrier"]);

                                        if (TrackingNoUpdated == 0)
                                            objGoogleClient.UpdateTrackingNo(DomainId, WebApiId, MerchantId, MerchantKey, numItemCode, vcAPIItemId, vcAPIOppId, vcApiOppItemId, vcTrackingNo, ShippingCarrier, 0);
                                        else
                                            objGoogleClient.UpdateTrackingNo(DomainId, WebApiId, MerchantId, MerchantKey, numItemCode, vcAPIItemId, vcAPIOppId, vcApiOppItemId, vcTrackingNo, ShippingCarrier, 1);

                                        objCommon.Mode = 22;
                                        objCommon.UpdateRecordID = OppBizDocItemID;
                                        objCommon.UpdateValueID = 1;
                                        objCommon.UpdateSingleFieldValue();//Set Flag as Tracking details Update Processed for BizDocItem
                                        LogMessage = "Tracking Number : " + vcTrackingNo + " is updated for Google OrderId : " + vcAPIOppId + " and Google Order Line Item Id : " + vcApiOppItemId;
                                        GeneralFunctions.WriteMessage(DomainId, "LOG", "Service1 : From UpdateOrderFulfillment: " + LogMessage);
                                    }
                                    catch (Exception ex)
                                    {
                                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Service1 : From UpdateOrderFulfillment: " + ex.Message);
                                    }
                                }
                            }
                            else if (WebApiId == (int)WebAPIList.Magento) //Magento US)
                            {
                                objMagentoClient = new MagentoClient();
                                string API_URL = CCommon.ToString(dr["vcFirstFldValue"]);
                                string API_User = CCommon.ToString(dr["vcSecondFldValue"]);
                                string API_Key = CCommon.ToString(dr["vcThirdFldValue"]);
                                string ShippingCarrier = "ups";

                                foreach (DataRow datarow in dtTrackingDetails.Rows)
                                {
                                    try
                                    {
                                        numOppBizDocID = CCommon.ToLong(datarow["numOppBizDocID"]);
                                        numItemCode = CCommon.ToLong(datarow["numItemCode"]);
                                        OppBizDocItemID = CCommon.ToLong(datarow["numOppBizDocItemID"]);
                                        vcAPIOppId = CCommon.ToString(datarow["vcAPIOppId"]);
                                        vcAPIItemId = CCommon.ToString(datarow["vcAPIItemId"]);
                                        vcTrackingNo = CCommon.ToString(datarow["vcTrackingNo"]);
                                        vcShippingMethod = CCommon.ToString(datarow["vcShippingMethod"]);
                                        vcApiOppItemId = CCommon.ToString(datarow["vcApiOppItemId"]);
                                        int TrackingNoUpdated = CCommon.ToInteger(datarow["numTrackingNoUpdated"]);
                                        ShippingCarrier = CCommon.ToString(datarow["ShippingCarrier"]);

                                        if (TrackingNoUpdated == 0)
                                            objMagentoClient.UpdateTrackingNumber(DomainId, WebApiId, API_URL, API_User, API_Key, numItemCode, vcAPIItemId, vcAPIOppId, vcApiOppItemId, vcTrackingNo, ShippingCarrier, 0);

                                        else
                                            objMagentoClient.UpdateTrackingNumber(DomainId, WebApiId, API_URL, API_User, API_Key, numItemCode, vcAPIItemId, vcAPIOppId, vcApiOppItemId, vcTrackingNo, ShippingCarrier, 1);

                                        objCommon.Mode = 22;
                                        objCommon.UpdateRecordID = OppBizDocItemID;
                                        objCommon.UpdateValueID = 1;
                                        objCommon.UpdateSingleFieldValue();//Set Flag as Tracking details Update Processed for BizDocItem
                                        LogMessage = "Tracking Number : " + vcTrackingNo + " is updated for Magento OrderId : " + vcAPIOppId + " and Magento Order Line Item Id : " + vcApiOppItemId;
                                        GeneralFunctions.WriteMessage(DomainId, "LOG", "Service1 : From UpdateOrderFulfillment: " + LogMessage);
                                    }
                                    catch (Exception ex)
                                    {
                                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Service1 : From UpdateOrderFulfillment: " + ex.Message);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "Service1 : From UpdateOrderFulfillment: " + Ex.Message);
                }
            }
        }

        #endregion Fulfillment Operations

        #region Amazon FBA Order Processing

        /// <summary>
        /// Gets the List of Orders for each Web API Id and Domain ID and adds the respective WebAPIOrderID to BizDatabase
        /// </summary>
        private void GetFBAOrderIDList()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * GetFBAOrderIDList_Timer))
            // while (!m_closingEvent.WaitOne(M_POLL_TIME * 0))
            {
                AmazonClient objAmazonClient = null;
                long DomainID = 0;
                int WebApiId = 0;

                DateTime CreatedAfter, CreatedBefore;
                WebAPI objWebAPI = new WebAPI();
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableOrderImport"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);

                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                {
                                    objAmazonClient = new AmazonClient();
                                    ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                    objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                    objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                    objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                    objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);

                                    objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));

                                    //objSvcConfigInfo.FeedsServiceURL = CCommon.ToString(dr["vcSixthFldValue"]);
                                    //objSvcConfigInfo.OrdersServiceURL = CCommon.ToString(dr["vcSeventhFldValue"]);
                                    objSvcConfigInfo.ServiceURL_Type = 1;

                                    if (CCommon.ToString(dr["vcSixteenthFldValue"]) != "")
                                        CreatedAfter = Convert.ToDateTime(CCommon.ToString(dr["vcSixteenthFldValue"]));
                                    else
                                        //CreatedAfter = new DateTime(2013, 4,11, 0, 00, 00);
                                        CreatedAfter = DateTime.Now.AddDays(-10);

                                    // CreatedBefore = DateTime.Now.AddMinutes(-10);

                                    CreatedBefore = CreatedAfter.AddDays(1);
                                    if (CreatedBefore > DateTime.Now)
                                    {
                                        CreatedBefore = DateTime.Now.AddMinutes(-10);
                                    }

                                    //CreatedAfter = new DateTime(2012, 10, 1, 20, 00, 00);
                                    //CreatedBefore = new DateTime(2012, 10, 2, 23, 00, 00);
                                    objAmazonClient.GetOrdersIDListToBiz(objSvcConfigInfo, CreatedAfter, CreatedBefore, DomainID, WebApiId);

                                    CCommon objCommon = new CCommon();
                                    objCommon.DomainID = DomainID;
                                    objCommon.Mode = 39;
                                    objCommon.UpdateRecordID = WebApiId;
                                    objCommon.Comments = CCommon.ToString(CreatedBefore);
                                    objCommon.UpdateSingleFieldValue();
                                    //objWebAPI.DomainID = DomainID;
                                    //objWebAPI.WebApiId = WebApiId;
                                    //objWebAPI.UpdateAPISettingsDate(5);
                                }
                            }
                        }
                        catch (FaultException<error> ex)
                        {
                            error er = ex.Detail;
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetFBAOrderIDList: " + er.msg);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetFBAOrderIDList: " + er.request);
                        }
                        catch (Exception ex)
                        {
                            string Error = "Error in importing Amazon FBA Order Id list";
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetFBAOrderIDList: " + Error + "-" + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetFBAOrderIDList: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetFBAOrderIDList: " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetFBAOrderIDList: " + ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// Gets Top WebAPIOrderID for each webApiId and DomainId 
        /// Gets details of the WebAPIOrderID
        /// </summary>
        private void GetOrderDetailsForFBAOrderID()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * GetFBAOrderDetails_Timer))
            // while (!m_closingEvent.WaitOne(M_POLL_TIME * 0))
            {
                AmazonClient objAmazonClient = null;
                WebAPI objWebAPI = new WebAPI();

                int WebApiId = 0;
                int numOrderStatus = 0;
                int numWareHouseID = 0;

                long DomainID = 0;
                long numRecordOwner = 0;
                long numAssignTo = 0;
                long numRelationshipId = 0;
                long numProfileId = 0;
                long numBizDocId = 0;

                string LogMessage = "";
                string Source = "";

                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableOrderImport"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                Source = CCommon.ToString(dr["vcProviderName"]);
                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                {
                                    string AmazonOrderId = GetTopAmazonOrderID(DomainID, WebApiId);//Gets top 1 WebApiOrderId
                                    if (AmazonOrderId != "" & AmazonOrderId != "0")
                                    {
                                        objAmazonClient = new AmazonClient();
                                        ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                        objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                        objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                        objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                        objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);
                                        objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                        objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                        objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));

                                        numOrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                        numWareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                        numBizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                        numRecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                        numAssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                        numRelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                        numProfileId = CCommon.ToLong(dr["numProfileId"]);
                                        objSvcConfigInfo.ServiceURL_Type = 1;
                                        objAmazonClient.GetFBAOrderReportByOrderID(objSvcConfigInfo, DomainID, WebApiId, AmazonOrderId);

                                        //objAmazonClient.GetOrderDetails(objSvcConfigInfo, DomainID, WebApiId, AmazonOrderId, Source,
                                        //    numWareHouseID, numRecordOwner, numAssignTo, numRelationshipId, numProfileId, numBizDocId, numOrderStatus);
                                        objWebAPI.DomainID = DomainID;
                                        objWebAPI.WebApiId = WebApiId;
                                        objWebAPI.WebApiOrdDetailId = 1;
                                        objWebAPI.API_OrderId = AmazonOrderId;
                                        objWebAPI.API_OrderStatus = 1;
                                        objWebAPI.ManageAPIOrderDetails();
                                    }
                                    else//If No new WebAPIOrderId is found for the DomainId and WebApiId 
                                    {
                                        // LogMessage = "Order Details are UpTo Date for DomainID : " + DomainID + " and WebApiID : " + WebApiId + Environment.NewLine;
                                        // GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                                    }
                                }

                            }
                        }
                        catch (FaultException<error> ex)
                        {
                            error er = ex.Detail;
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderDetailsForFBAOrderID: " + er.msg);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderDetailsForFBAOrderID: " + er.request);
                        }
                        catch (Exception ex)
                        {
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderDetailsForFBAOrderID: " + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderDetailsForFBAOrderID: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderDetailsForFBAOrderID: " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From GetOrderDetailsForFBAOrderID: " + ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// Reads Amazon FBA Order files
        /// </summary>
        private void ReadAmazonFBAOrderFile()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * 20))
            //while (!m_closingEvent.WaitOne(M_POLL_TIME * 0))
            {
                AmazonClient objAmazonClient = null;
                WebAPI objWebAPI = new WebAPI();
                int WebApiId = 0;
                int OrderStatus = 0;
                int ShippedOrderStatus = 0;
                int WareHouseID = 0;
                int ExpenseAccountId = 0;
                long DomainID = 0;
                long BizDocId = 0;
                long BizDocStatusId = 0;
                long FBABizDocId = 0;
                long FBABizDocStatusId = 0;
                long RecordOwner = 0;
                long AssignTo = 0;
                long RelationshipId = 0;
                long ProfileId = 0;
                long DiscountItemMapping = 0;
                long ShippingServiceItemID = 0;
                long SalesTaxItemMappingID = 0;
                string ShipToPhoneNo = "";
                string Source = "", ErrorMsg = "";
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableOrderImport"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US)
                                {
                                    objAmazonClient = new AmazonClient();
                                    string sourceDir = GeneralFunctions.GetPath("AmazonFBAOrderFilePath", DomainID);
                                    if (Directory.Exists(sourceDir))
                                    {
                                        string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                                        if (fileEntries.Length > 0)
                                        {
                                            Source = CCommon.ToString(dr["vcProviderName"]);
                                            OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                            ShippedOrderStatus = CCommon.ToInteger(dr["numUnShipmentOrderStatus"]);
                                            WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                            BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                            BizDocStatusId = CCommon.ToLong(dr["numBizDocStatusId"]);
                                            FBABizDocId = CCommon.ToLong(dr["numFBABizDocId"]);
                                            FBABizDocStatusId = CCommon.ToLong(dr["numFBABizDocStatusId"]);

                                            RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                            AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                            RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                            ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                            //ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                            ExpenseAccountId = 0;
                                            DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                            ShippingServiceItemID = CCommon.ToLong(dr["numShippingServiceItemID"]);
                                            SalesTaxItemMappingID = CCommon.ToLong(dr["numSalesTaxItemMapping"]);
                                            ShipToPhoneNo = CCommon.ToString(dr["vcShipToPhoneNo"]);

                                            foreach (string filepath in fileEntries)
                                            {
                                                try
                                                {
                                                    COrdersList objOrdersList = new COrdersList();
                                                    GeneralFunctions.DeSerializeFileToObject<COrdersList>(out objOrdersList, filepath);
                                                    if (objOrdersList != null)
                                                    {
                                                        objAmazonClient.ReadAmazonFBAOrders(objOrdersList, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, FBABizDocId, FBABizDocStatusId, ShippedOrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID, ShipToPhoneNo);
                                                        // objAmazonClient.ReadOrder(objOrdersList, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, FBABizDocId, FBABizDocStatusId, ShippedOrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID);
                                                    }

                                                    File.Copy(filepath, GeneralFunctions.GetPath(AmazonClosedOrdersPath, DomainID) + "FBA_" + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                    File.Delete(filepath);
                                                }
                                                catch (Exception ex)
                                                {
                                                    ErrorMsg = "Error Reading FBA Order File " + Path.GetFileNameWithoutExtension(filepath);
                                                    File.Copy(filepath, GeneralFunctions.GetPath("AmazonErrorOrderReport", DomainID) + "FBA_" + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                    File.Delete(filepath);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonFBAOrderFile: " + ErrorMsg + ex.Message);
                                                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonFBAOrderFile: " + ex.StackTrace);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorMsg = "";
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonFBAOrderFile: " + ErrorMsg + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonFBAOrderFile: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = "";
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonFBAOrderFile: " + ErrorMsg + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonFBAOrderFile: " + ex.StackTrace);
                }
            }
        }

        #endregion Amazon FBA Order Processing

        #region Clear Logs

        /// <summary>
        /// Clears Old Log Files
        /// </summary>
        private void ClearLogs()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * ClearLog_Timer))
            {
                long DomainID = 0;
                WebAPI objWebAPI = new WebAPI();
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                        {
                            DomainID = CCommon.ToLong(dr["numDomainId"]);
                            DeleteLogFiles(DomainID, ClearLogFiles);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ClearLogs: " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ClearLogs: " + ex.StackTrace);
                }
            }

        }

        #endregion Clear Logs

        #region Magento Specific functions

        /// <summary>
        /// Gets Magento's Attribute Sets specific fro the Seller and Adds to BizDatabase
        /// Seller's specific Magento attribute sets are needed in order to Map a item to a attribute Set While Adding / Updating Item to Magento
        /// </summary>
        public void UpdateMagentoProductAttributeSet()
        {
            MagentoClient objMagentoClient = null;
            while (!m_closingEvent.WaitOne(M_POLL_TIME * MagentoAttributeSet_Timer))
            //while (!m_closingEvent.WaitOne(M_POLL_TIME * 5))
            {
                WebAPI objWebAPI = new WebAPI();
                long DomainID = 0;
                int WebApiId = 0;
                DateTime LastUpdated = new DateTime();
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                if (WebApiId == (int)WebAPIList.Magento)
                                {
                                    objMagentoClient = new MagentoClient();
                                    string API_URL = CCommon.ToString(dr["vcFirstFldValue"]);
                                    string API_User = CCommon.ToString(dr["vcSecondFldValue"]);
                                    string API_Key = CCommon.ToString(dr["vcThirdFldValue"]);

                                    if (CCommon.ToString(dr["vcEleventhFldValue"]) != "")
                                        LastUpdated = Convert.ToDateTime(CCommon.ToString(dr["vcEleventhFldValue"]));

                                    if (CCommon.ToString(dr["vcEleventhFldValue"]) == "" || DateTime.Now.Subtract(LastUpdated).TotalHours > 24)
                                    {
                                        objMagentoClient.GetProductAttributeSet(DomainID, WebApiId, API_URL, API_User, API_Key);
                                        objWebAPI.DomainID = DomainID;
                                        objWebAPI.WebApiId = WebApiId;
                                        objWebAPI.UpdateAPISettingsDate(2);
                                    }
                                }
                            }
                        }
                        catch (FaultException<error> ex)
                        {
                            error er = ex.Detail;
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From UpdateMagentoProductAttributeSet: " + er.msg);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From UpdateMagentoProductAttributeSet: " + er.request);
                        }
                        catch (Exception ex)
                        {
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From UpdateMagentoProductAttributeSet: " + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From UpdateMagentoProductAttributeSet: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From UpdateMagentoProductAttributeSet: " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From UpdateMagentoProductAttributeSet: " + ex.StackTrace);
                }
            }
        }

        #endregion Magento Specific functions

        #region Helper Functions

        /// <summary>
        /// Enum for List of WebAPIs supported by BizAutomation
        /// </summary>
        public enum WebAPIList
        {
            //Mapping the WebApiId in WebApiDetail Table
            AmazonUS = 2,
            EBayUS = 5,
            GoogleUS = 7,
            Magento = 8
        }

        /// <summary>
        /// Deletes Log Files older than the number of days specified
        /// And Deletes Log Files thats been in Trash Folder for morethan 30 Days.
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="DaysOld">DaysOld</param>
        private void DeleteLogFiles(long DomainId, int DaysOld)
        {
            try
            {
                CreateLogFiles objLogFiles = new CreateLogFiles();
                objLogFiles.DeleteLogFiles(DomainId, DaysOld);
                objLogFiles = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// GetRoutingRulesUserCntID
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="dbFieldName">dbFieldName</param>
        /// <param name="FieldValue">FieldValue</param>
        /// <returns></returns>
        private long GetRoutingRulesUserCntID(long DomainId, string dbFieldName, string FieldValue)
        {
            AutoRoutingRules objAutoRoutRles = new AutoRoutingRules();
            DataTable dtAutoRoutingRules = new DataTable();
            DataRow drAutoRow = null;
            DataSet ds = new DataSet();
            try
            {
                objAutoRoutRles.DomainID = DomainId;
                if (dtAutoRoutingRules.Columns.Count == 0)
                {
                    dtAutoRoutingRules.TableName = "Table";
                    dtAutoRoutingRules.Columns.Add("vcDbColumnName");
                    dtAutoRoutingRules.Columns.Add("vcDbColumnText");
                }
                dtAutoRoutingRules.Rows.Clear();
                drAutoRow = dtAutoRoutingRules.NewRow();
                drAutoRow["vcDbColumnName"] = dbFieldName;
                drAutoRow["vcDbColumnText"] = FieldValue;
                dtAutoRoutingRules.Rows.Add(drAutoRow);
                ds.Tables.Add(dtAutoRoutingRules);
                objAutoRoutRles.strValues = ds.GetXml();
                ds.Tables.Remove(ds.Tables[0]);
                return objAutoRoutRles.GetRecordOwner();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the Top WebApiOrderId for the given DomainId and WebApiId
        /// </summary>
        /// <param name="DomainID">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <returns>WebApiOrderId</returns>
        private string GetTopAmazonOrderID(long DomainID, int WebApiId)
        {
            string AmazonOrderId = "";
            WebAPI objWebAPI = new WebAPI();
            objWebAPI.WebApiId = WebApiId;
            objWebAPI.DomainID = DomainID;
            AmazonOrderId = objWebAPI.GetTopWebApiOrderID();
            return AmazonOrderId;
        }

        /// <summary>
        /// Gets the Top WebApiOrderReportId for the given DomainId and WebApiId
        /// </summary>
        /// <param name="DomainID">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <returns>WebApiOrderReportId</returns>
        private DataTable GetTopOrderReportID(long DomainID, int WebApiId)
        {
            DataTable dtTopReport;
            string OrderReportID = "";
            WebAPI objWebAPI = new WebAPI();
            objWebAPI.WebApiId = WebApiId;
            objWebAPI.DomainID = DomainID;
            objWebAPI.Mode = 0;
            dtTopReport = objWebAPI.GetTopAPIOrderReport();
            return dtTopReport;
        }

        private DataTable GetTopImportOrderRequest(long DomainID, int WebApiId)
        {
            DataTable dtTopOrderImportRequest;
            WebAPI objWebAPI = new WebAPI();
            objWebAPI.WebApiId = WebApiId;
            objWebAPI.DomainID = DomainID;
            objWebAPI.Mode = 1;
            dtTopOrderImportRequest = objWebAPI.GetTopAPIOrderReport();
            return dtTopOrderImportRequest;
        }


        #endregion Helper Functions

        #region Amazon Specific Functions

        ///Not Used as of Now.
        /// <summary>
        /// Scheduling Order Report Generation(Amazon generate Order report automatically for the scheduled interval of time) from Amazon (One time work)
        /// Even it can be Scheduled from Amazon MWS Scratchpad https://mws.amazonservices.com/scratchpad/index.html
        /// </summary>
        public void ManageAmazonReportSchedule()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * 1440))
            {
                AmazonClient objAmazonClient = null;
                WebAPI objWebAPI = new WebAPI();
                long DomainID = 0;
                int WebApiId = 0;

                int numOrderStatus = 0;
                int numWareHouseID = 0;
                long numBizDocId = 0;
                long numRecordOwner = 0;
                long numAssignTo = 0;
                long numRelationshipId = 0;
                long numProfileId = 0;

                string Source = "";
                string strReportId = "", strScheduleInterval = "";

                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                Source = CCommon.ToString(dr["vcProviderName"]);
                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                {
                                    objAmazonClient = new AmazonClient();
                                    ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                    objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                    objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                    objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                    objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);

                                    objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                    objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));

                                    numOrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                    numWareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                    numBizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                    numRecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                    numAssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                    numRelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                    numProfileId = CCommon.ToLong(dr["numProfileId"]);

                                    objSvcConfigInfo.ServiceURL_Type = 2;
                                    strReportId = "ScheduledXMLOrderReport";
                                    strScheduleInterval = "_15_MINUTES_";
                                    objAmazonClient.ManageReportSchedule(objSvcConfigInfo, strReportId, strScheduleInterval, DomainID, WebApiId);
                                }

                            }
                        }
                        catch (FaultException<error> ex)
                        {
                            error er = ex.Detail;
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ManageAmazonReportSchedule: " + er.msg);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ManageAmazonReportSchedule: " + er.request);
                        }
                        catch (Exception ex)
                        {
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ManageAmazonReportSchedule: " + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ManageAmazonReportSchedule: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ManageAmazonReportSchedule: " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ManageAmazonReportSchedule: " + ex.StackTrace);
                }
            }
        }

        #endregion Amazon Specific Functions

        #region E-Bay Test Works

        /// <summary>
        /// Not Used for Application Flow.(Just holding for Testing and Debugging (Method Invoke with credentials))
        /// </summary>
        public void CalleBayFunctions()
        {
            try
            {
                Thread.Sleep(5000);
                eBayClient objEBayClient = new eBayClient();
                string Token = "AgAAAA**AQAAAA**aAAAAA**lsVoUg**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GhCJmEogidj6x9nY+seQ**OtUBAA**AAMAAA**DK1ntvvNQgyEHyfZUgPYm6UScTXX/LcWJmPzf5nsNu4ddIPMN883c/UN3JkT3O+KweghfymtDTI3vyopqfueTuC04m1AQlA8udvj8E3qm81wRQBIxzZGnCxvp6XzhVrfObexxRwWAhhCWDhSQUabjULVBQiO/rjhpWWKQzlxRGkIoBw/o1+xilh4fUVG2rei4VcNG8PlkWCqwlMqcfbNqY/DUKhtRVyDMtpeCjpeHBs5FvyJiVbV4uL3OYNL8swpWcf6X1OFUo27oT7WfhW1EoZampoDmFSXIz8NQNMvnVQWv9JuFswGlMuH6MaivSf7pCUqrBkUwCTF5mpAQfrL5I2S5OmA4YB8S4P3j1IRvPrfmXy3r8VNmxXPlVv5kjlE7RVgUG3vICfUax1TEA1w6Uy3T/CAQ+h5Ko4pMGUryBrSgVHuT74iT1+6ufyHSraMltpMYq9BvpC+CwuCcPxxStJGnLSzf7n5hU+Cak7uihK19MhEbc59AVmjSHcMSekeqSKNY//MUJYRx//yOfUtC5yb2IGypXXOs3rcEesh4d0v0JwEeNgsdNk9sScky7d1Ko2Y0SyjUO1vMeex1VfOXIVt5LMjAI8hXynIknyz4oeqo9pZQTzupA6kXRzslMcwpkyBGQuWrZ+qJ/uafPmlCZOQLorN58T50c6jNJZdMyucI/KUtOcLuBuJvVvmU7gjG/qHbqS3QestR+qUt2rR22a7esfBfJlpfr/5sn+qcXjH/oAz3dDrtX2jSQGm+eJ8";
                string eBaySite = "US";

                //objEBayClient.GetEbayProductListings(170, 5, 229543, 229543, 1064, Token, eBaySite, 1);
                //objEBayClient.GeteBayOrders(Token, eBaySite, DateTime.Now.AddDays(-5), DateTime.Now);

                //GetOrderReportList();
                //ReadOrdersReport();

                AddItems_To_API();
            }
            catch (Exception ex)
            {
                GeneralFunctions.WriteMessage(0, "ERR", ex.Message);
            }
        }

        #endregion E-Bay Works

        #region API Settings Validation

        private void ValidateAPISettings()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * 1440))
            // while (!m_closingEvent.WaitOne(M_POLL_TIME * 0))
            {
                long DomainID = 0;
                int WebApiId = 0;
                DataTable dtItems;
                string Source = "", ErrorMsg = "";

                WebAPI objWebAPI = new WebAPI();
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        string MarketplaceName = CCommon.ToString(dr["vcProviderName"]);
                        string LogMessage = "";
                        bool IsValid = true;
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                objWebAPI.DomainID = DomainID;
                                objWebAPI.WebApiId = WebApiId;
                                objWebAPI.DateCreated = "";
                                objWebAPI.DateModified = "";
                                dtItems = objWebAPI.GetItemsCOAApi();

                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                {
                                    //LogMessage = "Amazon Marketplace";
                                    string MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                    string MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                    string SiteCode = CCommon.ToString(dr["vcFifthFldValue"]);
                                    string MerchantToken = CCommon.ToString(dr["vcEighthFldValue"]);
                                    int AdminID = CCommon.ToInteger(dr["numAdminID"]);

                                    if (string.IsNullOrEmpty(MerchantID))
                                    {
                                        IsValid = false;
                                        LogMessage += " Merchant ID is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(MarketPlaceID))
                                    {
                                        IsValid = false;
                                        LogMessage += " MarketPlace ID is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(SiteCode))
                                    {
                                        IsValid = false;
                                        LogMessage += " Site Code is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(MerchantToken))
                                    {
                                        IsValid = false;
                                        LogMessage += " Merchant Token is empty.<br/>";
                                    }
                                    if (dtItems.Rows.Count > 0)
                                    {
                                        string ItemTypeName = "";
                                        string ItemCode = "";
                                        string ItemName = "";
                                        string ItemType = "";

                                        foreach (DataRow drItem in dtItems.Rows)
                                        {
                                            ItemCode = CCommon.ToString(drItem["numItemCode"]);
                                            ItemName = CCommon.ToString(drItem["vcItemName"]);
                                            ItemType = CCommon.ToString(drItem["charItemType"]);
                                            if (ItemType == "P")
                                                ItemTypeName = "Inventory Item";

                                            if (ItemType == "N")
                                                ItemTypeName = "Non-Inventory Item";

                                            if (ItemType == "S")
                                                ItemTypeName = "Service Item";
                                            LogMessage += "Chart Of Account mapping error for Item Type : " + ItemTypeName
                                                + ", Item Code : " + ItemCode + ", Item Name : " + ItemName + " ";
                                        }
                                        IsValid = false;
                                    }

                                    BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                    string Message = "";
                                    bool SettingsCheck = false;
                                    SettingsCheck = objBizCommonFunctions.ValidateApiSetting(dr, out Message);
                                    if (!SettingsCheck)
                                    {
                                        IsValid = false;
                                        LogMessage += Message;
                                    }
                                    if (!IsValid)
                                    {
                                        Message = "Your Amazon Online Marketplace Integration is Disabled on " + DateTime.Now.ToString("ddMMMyyy HH:MM") +
                                             Environment.NewLine + "Due to.. " + Environment.NewLine;
                                        Message += LogMessage;
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ValidateAPISettings: " + Message);
                                        CCommon objCommon = new CCommon();
                                        objCommon.Mode = 40;
                                        objCommon.DomainID = DomainID;
                                        objCommon.UpdateRecordID = WebApiId;
                                        objCommon.UpdateValueID = 0;
                                        objCommon.UpdateSingleFieldValue();
                                        BizCommonFunctions.SendMailMarketplaceDisabled(DomainID, AdminID, MarketplaceName, "Service1 : From ValidateAPISettings, via SendMailMarketplaceDisabled: " + LogMessage);
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.EBayUS) //E-Bay US
                                {
                                    string SiteCode = CCommon.ToString(dr["vcFirstFldValue"]);
                                    string PayPalAddress = CCommon.ToString(dr["vcSecondFldValue"]);
                                    string MarketplaceCode = CCommon.ToString(dr["vcFifthFldValue"]);
                                    string MerchantToken = CCommon.ToString(dr["vcEighthFldValue"]);
                                    int AdminID = CCommon.ToInteger(dr["numAdminID"]);

                                    if (string.IsNullOrEmpty(SiteCode))
                                    {
                                        IsValid = false;
                                        LogMessage += " E-Bay Site Code is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(PayPalAddress))
                                    {
                                        IsValid = false;
                                        LogMessage += " E-BayPaypal Address is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(MarketplaceCode))
                                    {
                                        IsValid = false;
                                        LogMessage += " E-Bay Marketplace Code is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(MerchantToken))
                                    {
                                        IsValid = false;
                                        LogMessage += " E-Bay Merchant Token is empty.<br/>";
                                    }

                                    BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                    string Message = "";
                                    bool SettingsCheck = false;
                                    SettingsCheck = objBizCommonFunctions.ValidateApiSetting(dr, out Message);
                                    if (!SettingsCheck)
                                    {
                                        IsValid = false;
                                        LogMessage += Message;
                                    }
                                    if (!IsValid)
                                    {
                                        Message = "Your e-Bay Online Marketplace Integration is Disabled on " + DateTime.Now.ToString("ddMMMyyy HH:MM") +
                                             Environment.NewLine + "Due to.. " + Environment.NewLine;
                                        Message += LogMessage;
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ValidateAPISettings: " + Message);
                                        CCommon objCommon = new CCommon();
                                        objCommon.Mode = 40;
                                        objCommon.DomainID = DomainID;
                                        objCommon.UpdateRecordID = WebApiId;
                                        objCommon.UpdateValueID = 0;
                                        objCommon.UpdateSingleFieldValue();
                                        BizCommonFunctions.SendMailMarketplaceDisabled(DomainID, AdminID, MarketplaceName, "Service1 : From ValidateAPISettings, via SendMailMarketplaceDisabled: " + LogMessage);
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.GoogleUS) //Google US
                                {
                                    string SiteCode = CCommon.ToString(dr["vcFirstFldValue"]);
                                    string MerchantAccountId = CCommon.ToString(dr["vcSecondFldValue"]);
                                    string LoginEmail = CCommon.ToString(dr["vcThirdFldValue"]);
                                    string Password = CCommon.ToString(dr["vcFourthFldValue"]);
                                    string CheckOutMerchantId = CCommon.ToString(dr["vcFifthFldValue"]);
                                    string CheckOutMerchantKey = CCommon.ToString(dr["vcsixthFldValue"]);
                                    int AdminID = CCommon.ToInteger(dr["numAdminID"]);

                                    if (string.IsNullOrEmpty(SiteCode))
                                    {
                                        IsValid = false;
                                        LogMessage += " Google Site Code is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(MerchantAccountId))
                                    {
                                        IsValid = false;
                                        LogMessage += " Google Merchant Center Account Id is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(LoginEmail))
                                    {
                                        IsValid = false;
                                        LogMessage += " Google Merchant Center login email is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(Password))
                                    {
                                        IsValid = false;
                                        LogMessage += " Google Merchant Center Password is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(CheckOutMerchantId))
                                    {
                                        IsValid = false;
                                        LogMessage += " Google Checkout Merchant Id is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(CheckOutMerchantKey))
                                    {
                                        IsValid = false;
                                        LogMessage += " Google Checkout Merchant Key is empty.<br/>";
                                    }

                                    BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                    string Message = "";
                                    bool SettingsCheck = false;
                                    SettingsCheck = objBizCommonFunctions.ValidateApiSetting(dr, out Message);
                                    if (!SettingsCheck)
                                    {
                                        IsValid = false;
                                        LogMessage += Message;
                                    }
                                    if (!IsValid)
                                    {
                                        Message = "Your Google Online Marketplace Integration is Disabled on " + DateTime.Now.ToString("ddMMMyyy HH:MM") +
                                             Environment.NewLine + "Due to.. " + Environment.NewLine;
                                        Message += LogMessage;
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ValidateAPISettings: " + Message);
                                        CCommon objCommon = new CCommon();
                                        objCommon.Mode = 40;
                                        objCommon.DomainID = DomainID;
                                        objCommon.UpdateRecordID = WebApiId;
                                        objCommon.UpdateValueID = 0;
                                        objCommon.UpdateSingleFieldValue();
                                        BizCommonFunctions.SendMailMarketplaceDisabled(DomainID, AdminID, MarketplaceName, "Service1 : From ValidateAPISettings, via SendMailMarketplaceDisabled: " + LogMessage);
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.Magento) //Magento
                                {
                                    string DomainURL = CCommon.ToString(dr["vcFirstFldValue"]);
                                    string ApiUserId = CCommon.ToString(dr["vcSecondFldValue"]);
                                    string ApiUserKey = CCommon.ToString(dr["vcThirdFldValue"]);
                                    int AdminID = CCommon.ToInteger(dr["numAdminID"]);

                                    if (string.IsNullOrEmpty(DomainURL))
                                    {
                                        IsValid = false;
                                        LogMessage += " Magento Domain URL is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(ApiUserId))
                                    {
                                        IsValid = false;
                                        LogMessage += " Magento API User Name is empty.<br/>";
                                    }
                                    if (string.IsNullOrEmpty(ApiUserKey))
                                    {
                                        IsValid = false;
                                        LogMessage += " Magento API User Key is empty.<br/>";
                                    }

                                    BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                    string Message = "";
                                    bool SettingsCheck = false;
                                    SettingsCheck = objBizCommonFunctions.ValidateApiSetting(dr, out Message);
                                    if (!SettingsCheck)
                                    {
                                        IsValid = false;
                                        LogMessage += Message;
                                    }
                                    if (!IsValid)
                                    {
                                        Message = "Your Magento Online Marketplace Integration is Disabled on " + DateTime.Now.ToString("ddMMMyyy HH:MM") +
                                            Environment.NewLine + "Due to.. " + Environment.NewLine;
                                        Message += LogMessage;
                                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ValidateAPISettings: " + Message);
                                        CCommon objCommon = new CCommon();
                                        objCommon.Mode = 40;
                                        objCommon.DomainID = DomainID;
                                        objCommon.UpdateRecordID = WebApiId;
                                        objCommon.UpdateValueID = 0;
                                        objCommon.UpdateSingleFieldValue();
                                        BizCommonFunctions.SendMailMarketplaceDisabled(DomainID, AdminID, MarketplaceName, "Service1 : From ValidateAPISettings, via SendMailMarketplaceDisabled: " + LogMessage);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorMsg = "Error while Validating Marketplace Settings for " + MarketplaceName + ".,";
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ValidateAPISettings: " + ErrorMsg + " -" + ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ValidateAPISettings: " + ErrorMsg + " -" + ex.Message);
                }
            }
        }

        #endregion API Settings Validation

        #region Process Api Order Import Request

        private void ProcessImportApiOrderRequest()
        {
            while (!m_closingEvent.WaitOne(M_POLL_TIME * 60))
            //while (!m_closingEvent.WaitOne(M_POLL_TIME * 0))
            {
                AmazonClient objAmazonClient = null;
                MagentoClient objMagentoClient = null;
                eBayClient objeBayClient = null;
                WebAPI objWebAPI = new WebAPI();
                long DomainID = 0;
                int WebApiId = 0;
                string LogMessage = "";
                string Source = "";
                string OrderId = "";
                int RequestType = 0;
                DateTime FromDate, ToDate;
                long RecordOwner = 0;
                bool CanImportOrder = false;

                int OrderStatus = 0;
                int WareHouseID = 0;
                int ExpenseAccountId = 0;
                long BizDocId = 0;
                long BizDocStatusId = 0;
                long FBABizDocId = 0;
                long FBABizDocStatusId = 0;
                int ShippedOrderStatus = 0;
                long AssignTo = 0;
                long RelationshipId = 0;
                long ProfileId = 0;
                long DiscountItemMapping = 0;
                long SalesTaxItemMappingID = 0;
                long ShippingServiceItemID = 0;
                string ShipToPhoneNo = "";
                string ReportId = "";
                try
                {
                    objWebAPI.Mode = 2;
                    DataTable dtWebAPI = objWebAPI.GetWebApi();
                    foreach (DataRow dr in dtWebAPI.Rows)
                    {
                        try
                        {
                            if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                            {
                                DomainID = CCommon.ToLong(dr["numDomainId"]);
                                WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                Source = CCommon.ToString(dr["vcProviderName"]);
                                RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                                {
                                    long RequestId = 0;
                                    DataTable dtTopOrderToImport = GetTopImportOrderRequest(DomainID, WebApiId); //Gets top 1 WebApiOrderId 
                                    if (dtTopOrderToImport.Rows.Count > 0)
                                    {
                                        OrderId = CCommon.ToString(dtTopOrderToImport.Rows[0]["vcApiOrderId"]);
                                        RequestId = CCommon.ToLong(dtTopOrderToImport.Rows[0]["RequestId"]);
                                        RequestType = CCommon.ToInteger(dtTopOrderToImport.Rows[0]["tintRequestType"]);
                                        FromDate = Convert.ToDateTime(dtTopOrderToImport.Rows[0]["dtFromDate"]);
                                        ToDate = Convert.ToDateTime(dtTopOrderToImport.Rows[0]["dtToDate"]);
                                        ReportId = CCommon.ToString(dtTopOrderToImport.Rows[0]["vcMarketplaceOrderReportId"]);
                                        long OppId = CCommon.ToLong(dtTopOrderToImport.Rows[0]["numOppId"]);
                                        BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                        CanImportOrder = objBizCommonFunctions.RemoveApiOrdersForReImport(DomainID, WebApiId, Source, RecordOwner, dtTopOrderToImport.Rows[0]);
                                        if (CanImportOrder)
                                        {

                                            objAmazonClient = new AmazonClient();
                                            ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                            objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                            objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                            objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                            objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);
                                            objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                            objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                            objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                            objSvcConfigInfo.ServiceURL_Type = 1;

                                            if (RequestType == 2)
                                            {
                                                string ReportTypeId = "ScheduledXMLOrderReport";
                                                objAmazonClient = new AmazonClient();
                                                objAmazonClient.GetOrdersIDListToBiz(objSvcConfigInfo, FromDate, ToDate, DomainID, WebApiId);
                                                objSvcConfigInfo.ServiceURL_Type = 2;
                                                objAmazonClient.GetReportIDListToBiz(objSvcConfigInfo, DomainID, WebApiId, FromDate, ToDate, ReportTypeId);
                                                LogMessage = "Your import Order request is processed. Order detail from date " + CCommon.ToString(FromDate) + " till to date " + CCommon.ToString(ToDate) + " is imported from Amazon";
                                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                            }
                                            else if (RequestType == 1)
                                            {
                                                if (OrderId != "" & OrderId != "0")
                                                {
                                                    objAmazonClient = new AmazonClient();
                                                    Source = CCommon.ToString(dr["vcProviderName"]);
                                                    OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                                    WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                                    BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                                    BizDocStatusId = CCommon.ToLong(dr["numBizDocStatusId"]);
                                                    FBABizDocId = CCommon.ToLong(dr["numFBABizDocId"]);
                                                    FBABizDocStatusId = CCommon.ToLong(dr["numFBABizDocStatusId"]);
                                                    ShippedOrderStatus = CCommon.ToInteger(dr["numUnShipmentOrderStatus"]);
                                                    RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                                    AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                                    RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                                    ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                                    //ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                                    ExpenseAccountId = 0;
                                                    DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                                    ShippingServiceItemID = CCommon.ToLong(dr["numShippingServiceItemID"]);
                                                    SalesTaxItemMappingID = CCommon.ToLong(dr["numSalesTaxItemMapping"]);
                                                    ShipToPhoneNo = CCommon.ToString(dr["vcShipToPhoneNo"]);

                                                    if (ReportId == OrderId)
                                                    {
                                                        objAmazonClient.GetFBAOrderReportByOrderID(objSvcConfigInfo, DomainID, WebApiId, OrderId);
                                                    }
                                                    else
                                                    {
                                                        string[] fileEntries = Directory.GetFiles(GeneralFunctions.GetPath(AmazonClosedOrdersPath, DomainID), "*", SearchOption.AllDirectories);//reads all feed files
                                                        if (fileEntries.Length > 0)
                                                        {
                                                            bool found = false;
                                                            foreach (string filepath in fileEntries)
                                                            {
                                                                try
                                                                {
                                                                    if (found) break;

                                                                    //if (ReportId == OrderId)
                                                                    //{
                                                                    //    string FileName = Path.GetFileNameWithoutExtension(filepath);
                                                                    //    int offset = FileName.IndexOf('_');
                                                                    //    offset = FileName.IndexOf('_', offset + 1);
                                                                    //    int result = FileName.IndexOf(':', offset + 1);
                                                                    //    string ReportIdFromFile = FileName.Substring(4, offset - 4);
                                                                    //    if (ReportIdFromFile == ReportId)
                                                                    //    {
                                                                    //        COrdersList objOrdersList = new COrdersList();
                                                                    //        GeneralFunctions.DeSerializeFileToObject<COrdersList>(out objOrdersList, filepath);
                                                                    //        if (objOrdersList != null)
                                                                    //        {
                                                                    //            objAmazonClient.ReadAmazonFBAOrders(objOrdersList, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, FBABizDocId, FBABizDocStatusId, ShippedOrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID, ShipToPhoneNo);
                                                                    //            // objAmazonClient.ReadOrder(objOrdersList, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, FBABizDocId, FBABizDocStatusId, ShippedOrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID);
                                                                    //        }
                                                                    //    }
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    string FileName = Path.GetFileNameWithoutExtension(filepath);
                                                                    string ReportIdFromFile = FileName.Substring(0, (FileName.IndexOf('_')));
                                                                    if (ReportIdFromFile == ReportId)
                                                                    {
                                                                        AmazonEnvelope objAmaEnv = new AmazonEnvelope();
                                                                        GeneralFunctions.DeSerializeFileToObject<AmazonEnvelope>(out objAmaEnv, filepath);
                                                                        if (objAmaEnv != null & objAmaEnv.MessageType == AmazonEnvelopeMessageType.OrderReport)
                                                                        {
                                                                            foreach (AmazonEnvelopeMessage Message in objAmaEnv.Message)
                                                                            {
                                                                                OrderReport objOrdReport = new OrderReport();
                                                                                objOrdReport = (OrderReport)Message.Item;
                                                                                if (objOrdReport.AmazonOrderID == OrderId)
                                                                                {
                                                                                    objAmazonClient.NewReadAmazonOrders(objOrdReport, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID, ReportIdFromFile, ShipToPhoneNo);
                                                                                    found = true;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    //}
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    LogMessage = "Error occured while reading existing Order reports to Re-Import Order for Amazon Order Id : " + OrderId;
                                                                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    LogMessage = "Your import Order request is processed. Order detail is imported for Amazon Order Id : " + OrderId;
                                                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                                }
                                                else
                                                {
                                                    LogMessage = "Import Order request for Amazon Order Id " + OrderId + " cannot be processed., Please provide a valid Amazon Order Id.";
                                                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                                }
                                            }
                                            objWebAPI.OrderImportReqType = CCommon.ToShort(RequestType);
                                            objWebAPI.DomainID = DomainID;
                                            objWebAPI.WebApiId = WebApiId;
                                            objWebAPI.API_OrderId = OrderId;
                                            objWebAPI.OrdersFromDate = DateTime.Now;
                                            objWebAPI.OrdersToDate = DateTime.Now;
                                            objWebAPI.ImportApiOrderReqId = RequestId;
                                            objWebAPI.Status = CCommon.ToBool(0);
                                            objWebAPI.OppId = OppId;
                                            objWebAPI.ManageAPIImportOrderRequest();
                                        }
                                        else
                                        {
                                            LogMessage = "Unable to Process Import Order Request for " + Source + " Marketplace.";
                                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                        }
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.Magento) //Magento
                                {
                                    objMagentoClient = new MagentoClient();
                                    long RequestId = 0;
                                    DataTable dtTopOrderToImport = GetTopImportOrderRequest(DomainID, WebApiId); //Gets top 1 WebApiOrderId 
                                    if (dtTopOrderToImport.Rows.Count > 0)
                                    {
                                        OrderId = CCommon.ToString(dtTopOrderToImport.Rows[0]["vcApiOrderId"]);
                                        RequestId = CCommon.ToLong(dtTopOrderToImport.Rows[0]["RequestId"]);
                                        RequestType = CCommon.ToInteger(dtTopOrderToImport.Rows[0]["tintRequestType"]);
                                        FromDate = Convert.ToDateTime(dtTopOrderToImport.Rows[0]["dtFromDate"]);
                                        ToDate = Convert.ToDateTime(dtTopOrderToImport.Rows[0]["dtToDate"]);
                                        long OppId = CCommon.ToLong(dtTopOrderToImport.Rows[0]["numOppId"]);

                                        BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                        CanImportOrder = objBizCommonFunctions.RemoveApiOrdersForReImport(DomainID, WebApiId, Source, RecordOwner, dtTopOrderToImport.Rows[0]);
                                        if (CanImportOrder)
                                        {

                                            string API_URL = CCommon.ToString(dr["vcFirstFldValue"]);
                                            string API_User = CCommon.ToString(dr["vcSecondFldValue"]);
                                            string API_Key = CCommon.ToString(dr["vcThirdFldValue"]);

                                            if (RequestType == 2)
                                            {
                                                objMagentoClient.GetOrderIDListBetweenDates(DomainID, WebApiId, API_URL, API_User, API_Key, FromDate, ToDate);
                                                LogMessage = "Your import Order request is processed. Order detail from date " + CCommon.ToString(FromDate) + " till to date " + CCommon.ToString(ToDate) + " is imported from Magento";
                                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                            }
                                            else if (RequestType == 1)
                                            {
                                                if (OrderId != "" & OrderId != "0")
                                                {
                                                    objMagentoClient.GetOrderReport(DomainID, WebApiId, API_URL, API_User, API_Key, OrderId);
                                                    LogMessage = "Your import Order request is processed. Order detail is imported for Magento Order Id : " + OrderId;
                                                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                                }
                                                else
                                                {
                                                    LogMessage = "Import Order request for Magento Order Id " + OrderId + " cannot be processed., Please provide a valid Magento Order Id.";
                                                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                                }
                                            }
                                            objWebAPI.OrderImportReqType = CCommon.ToShort(RequestType);
                                            objWebAPI.DomainID = DomainID;
                                            objWebAPI.WebApiId = WebApiId;
                                            objWebAPI.API_OrderId = OrderId;
                                            objWebAPI.OrdersFromDate = DateTime.Now;
                                            objWebAPI.OrdersToDate = DateTime.Now;
                                            objWebAPI.ImportApiOrderReqId = RequestId;
                                            objWebAPI.Status = CCommon.ToBool(0);
                                            objWebAPI.OppId = OppId;
                                            objWebAPI.ManageAPIImportOrderRequest();
                                        }
                                        else
                                        {
                                            LogMessage = "Unable to Process Import Order Request for " + Source + " Marketplace.";
                                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                        }
                                    }
                                    else
                                    {
                                        //LogMessage = "Order Details are UpTo Date for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                        //GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                                    }
                                }
                                else if (WebApiId == (int)WebAPIList.EBayUS) //E-Bay US
                                {
                                    objeBayClient = new eBayClient();
                                    long RequestId = 0;
                                    DataTable dtTopOrderToImport = GetTopImportOrderRequest(DomainID, WebApiId); //Gets top 1 WebApiOrderId 
                                    if (dtTopOrderToImport.Rows.Count > 0)
                                    {
                                        OrderId = CCommon.ToString(dtTopOrderToImport.Rows[0]["vcApiOrderId"]);
                                        RequestType = CCommon.ToInteger(dtTopOrderToImport.Rows[0]["tintRequestType"]);
                                        FromDate = Convert.ToDateTime(dtTopOrderToImport.Rows[0]["dtFromDate"]);
                                        ToDate = Convert.ToDateTime(dtTopOrderToImport.Rows[0]["dtToDate"]);
                                        long OppId = CCommon.ToLong(dtTopOrderToImport.Rows[0]["numOppId"]);

                                        BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                        CanImportOrder = objBizCommonFunctions.RemoveApiOrdersForReImport(DomainID, WebApiId, Source, RecordOwner, dtTopOrderToImport.Rows[0]);

                                        if (CanImportOrder)
                                        {
                                            string eBaySite = CCommon.ToString(dr["vcFirstFldValue"]);
                                            string Token = CCommon.ToString(dr["vcEighthFldValue"]);

                                            if (RequestType == 2)
                                            {
                                                objeBayClient.GeteBayOrders(DomainID, WebApiId, Token, eBaySite, FromDate, ToDate);
                                                LogMessage = "Your import Order request is processed. Order detail from date " + CCommon.ToString(FromDate) + " till to date " + CCommon.ToString(ToDate) + " is imported from Magento";
                                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                            }
                                            else if (RequestType == 1)
                                            {
                                                if (OrderId != "" & OrderId != "0")
                                                {
                                                    objeBayClient.GeteBayOrderById(DomainID, WebApiId, Token, eBaySite, OrderId);
                                                    LogMessage = "Your import Order request is processed. Order detail is imported for e-Bay Order Id : " + OrderId;
                                                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                                }
                                                else
                                                {
                                                    LogMessage = "Import Order request for e-Bay Order Id " + OrderId + " cannot be processed., Please provide a valid e-Bay Order Id.";
                                                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                                }
                                            }
                                            objWebAPI.OrderImportReqType = CCommon.ToShort(RequestType);
                                            objWebAPI.DomainID = DomainID;
                                            objWebAPI.WebApiId = WebApiId;
                                            objWebAPI.API_OrderId = OrderId;
                                            objWebAPI.OrdersFromDate = DateTime.Now;
                                            objWebAPI.OrdersToDate = DateTime.Now;
                                            objWebAPI.ImportApiOrderReqId = RequestId;
                                            objWebAPI.OppId = OppId;
                                            objWebAPI.Status = CCommon.ToBool(0);
                                            objWebAPI.ManageAPIImportOrderRequest();
                                        }
                                        else
                                        {
                                            LogMessage = "Unable to Process Import Order Request for " + Source + " Marketplace.";
                                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage);
                                        }
                                    }
                                }
                            }
                        }
                        catch (FaultException<error> ex)
                        {
                            LogMessage = "Unable to process the import Order request. Error importing Order detail of " + Source + " Order Id : " + OrderId;
                            error er = ex.Detail;
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ProcessImportApiOrderRequest: " + er.msg);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ProcessImportApiOrderRequest: " + er.request);
                        }
                        catch (Exception ex)
                        {
                            LogMessage = "Unable to process the import Order request. Error importing Order detail of " + Source + " Order Id : " + OrderId;
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage + "  " + ex.Message);
                            GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ProcessImportApiOrderRequest: " + ex.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ProcessImportApiOrderRequest: " + LogMessage + "  " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ProcessImportApiOrderRequest: " + ex.StackTrace);
                }
            }
        }

        #endregion Process Api Order Import Request

        #region Test Bulk Order Import

        private void TestBulkOrderImport()
        {
            // while (!m_closingEvent.WaitOne(M_POLL_TIME * ReadOrders_Timer))
            while (!m_closingEvent.WaitOne(M_POLL_TIME * 60))
            {
                AmazonClient objAmazonClient = null;
                eBayClient objEBayClient = null;
                GoogleClient objGoogleClient = null;
                MagentoClient objMagentoClient = null;

                WebAPI objWebAPI = new WebAPI();
                int WebApiId = 0;
                int OrderStatus = 0;
                int WareHouseID = 0;
                int ExpenseAccountId = 0;
                long DomainID = 0;
                long BizDocId = 0;
                long BizDocStatusId = 0;
                long RecordOwner = 0;
                long AssignTo = 0;
                long RelationshipId = 0;
                long ProfileId = 0;
                long DiscountItemMapping = 0;
                long SalesTaxItemMappingID = 0;
                long ShippingServiceItemID = 0;
                string ShipToPhoneNo = "";
                string Source = "", ErrorMsg = "";

                string AmazonTestOrdersFolder = "Test_BulkImport\\AmazonTestOrdersFolder";
                string eBayTestOrdersFolder = "Test_BulkImport\\eBayTestOrdersFolder";
                string MagentoTestOrdersFolder = "Test_BulkImport\\MagentoTestOrdersFolder";
                string GoogleTestOrdersFolder = "Test_BulkImport\\GoogleTestOrdersFolder";

                int IsEnabled = 1;
                int count = 1;

                int EnableTestBulkImportOrders = CCommon.ToInteger(ConfigurationManager.AppSettings["EnableTestBulkImportOrders"]);
                int RoutineCount = CCommon.ToInteger(ConfigurationManager.AppSettings["RoutineCount"]);
                IsEnabled = EnableTestBulkImportOrders;
                count = RoutineCount;

                try
                {
                    if (CCommon.ToBool(IsEnabled))
                    {
                        objWebAPI.Mode = 2;
                        DataTable dtWebAPI = objWebAPI.GetWebApi();
                        for (int i = 0; i < count; i++)
                        {
                            foreach (DataRow dr in dtWebAPI.Rows)
                            {
                                try
                                {
                                    if (CCommon.ToBool(dr["bitEnableOrderImport"]) == true)
                                    {
                                        DomainID = CCommon.ToLong(dr["numDomainId"]);
                                        WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                                        if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US)
                                        {
                                            objAmazonClient = new AmazonClient();
                                            string FileName = "", ReportIdFromFile = "";
                                            string sourceDir = GeneralFunctions.GetPath(AmazonTestOrdersFolder, DomainID);
                                            if (Directory.Exists(sourceDir))
                                            {
                                                string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                                                if (fileEntries.Length > 0)
                                                {
                                                    Source = CCommon.ToString(dr["vcProviderName"]);
                                                    OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                                    WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                                    BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                                    BizDocStatusId = CCommon.ToLong(dr["numBizDocStatusId"]);
                                                    RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                                    AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                                    RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                                    ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                                    //ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                                    ExpenseAccountId = 0;
                                                    DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                                    ShippingServiceItemID = CCommon.ToLong(dr["numShippingServiceItemID"]);
                                                    SalesTaxItemMappingID = CCommon.ToLong(dr["numSalesTaxItemMapping"]);
                                                    ShipToPhoneNo = CCommon.ToString(dr["vcShipToPhoneNo"]);
                                                    foreach (string filepath in fileEntries)
                                                    {
                                                        FileName = Path.GetFileNameWithoutExtension(filepath);
                                                        ReportIdFromFile = FileName.Substring(0, (FileName.IndexOf('_')));
                                                        try
                                                        {
                                                            AmazonEnvelope objAmaEnv = new AmazonEnvelope();
                                                            GeneralFunctions.DeSerializeFileToObject<AmazonEnvelope>(out objAmaEnv, filepath);
                                                            if (objAmaEnv != null & objAmaEnv.MessageType == AmazonEnvelopeMessageType.OrderReport)
                                                            {
                                                                foreach (AmazonEnvelopeMessage Message in objAmaEnv.Message)
                                                                {
                                                                    OrderReport objOrdReport = new OrderReport();
                                                                    objOrdReport = (OrderReport)Message.Item;
                                                                    objAmazonClient.TestBulkOrderImport(objOrdReport, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID, ReportIdFromFile, ShipToPhoneNo);
                                                                }
                                                                // objAmazonClient.ProcessAmazonOrders(objAmaEnv, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID);
                                                            }
                                                            //File.Copy(filepath, GeneralFunctions.GetPath(AmazonClosedOrdersPath, DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                            //File.Delete(filepath);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            ErrorMsg = "Error in reading Amazon order report file : " + Path.GetFileNameWithoutExtension(filepath);
                                                            File.Copy(filepath, GeneralFunctions.GetPath(AmazonTestOrdersFolder + "_ErrorOrderReports", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                            File.Delete(filepath);
                                                            GeneralFunctions.WriteMessage(DomainID, "ERR", ErrorMsg + ex.Message);
                                                            GeneralFunctions.WriteMessage(DomainID, "ERR", ex.StackTrace);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else if (WebApiId == (int)WebAPIList.EBayUS) //E-Bay US
                                        {
                                            objEBayClient = new eBayClient();
                                            string sourceDir = GeneralFunctions.GetPath(eBayTestOrdersFolder, DomainID);
                                            if (Directory.Exists(sourceDir))
                                            {
                                                string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                                                if (fileEntries.Length > 0)
                                                {
                                                    Source = CCommon.ToString(dr["vcProviderName"]);
                                                    OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                                    WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                                    BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                                    BizDocStatusId = CCommon.ToLong(dr["numBizDocStatusId"]);
                                                    RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                                    AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                                    RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                                    ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                                    //ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                                    ExpenseAccountId = 0;
                                                    DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                                    ShippingServiceItemID = CCommon.ToLong(dr["numShippingServiceItemID"]);
                                                    SalesTaxItemMappingID = CCommon.ToLong(dr["numSalesTaxItemMapping"]);
                                                    ShipToPhoneNo = CCommon.ToString(dr["vcShipToPhoneNo"]);
                                                    foreach (string filepath in fileEntries)
                                                    {
                                                        try
                                                        {
                                                            OrderTypeCollection OrderCollections = new OrderTypeCollection();
                                                            GeneralFunctions.DeSerializeFileToObject<OrderTypeCollection>(out OrderCollections, filepath);
                                                            if (OrderCollections != null)
                                                                objEBayClient.TestBulkOrderImport(OrderCollections, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID, ShipToPhoneNo);
                                                            //File.Copy(filepath, GeneralFunctions.GetPath(EBayClosedOrders, DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                            //File.Delete(filepath);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            ErrorMsg = "Error in reading E-Bay order report file : " + Path.GetFileNameWithoutExtension(filepath);
                                                            File.Copy(filepath, GeneralFunctions.GetPath(eBayTestOrdersFolder + "_ErrorOrderReports", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                            File.Delete(filepath);
                                                            GeneralFunctions.WriteMessage(DomainID, "ERR", ErrorMsg + ex.Message);
                                                            GeneralFunctions.WriteMessage(DomainID, "ERR", ex.StackTrace);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else if (WebApiId == (int)WebAPIList.GoogleUS) //Google US
                                        {
                                            objGoogleClient = new GoogleClient();
                                            string sourceDir = GeneralFunctions.GetPath(GoogleTestOrdersFolder, DomainID);
                                            if (Directory.Exists(sourceDir))
                                            {
                                                string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                                                if (fileEntries.Length > 0)
                                                {
                                                    Source = CCommon.ToString(dr["vcProviderName"]);
                                                    OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                                    WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                                    BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                                    BizDocStatusId = CCommon.ToLong(dr["numBizDocStatusId"]);
                                                    RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                                    AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                                    RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                                    ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                                    //ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                                    ExpenseAccountId = 0;
                                                    DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                                    ShippingServiceItemID = CCommon.ToLong(dr["numShippingServiceItemID"]);
                                                    SalesTaxItemMappingID = CCommon.ToLong(dr["numSalesTaxItemMapping"]);
                                                    ShipToPhoneNo = CCommon.ToString(dr["vcShipToPhoneNo"]);
                                                    foreach (string filepath in fileEntries)
                                                    {
                                                        try
                                                        {
                                                            GCheckout.AutoGen.ChargeAmountNotification N1;// = new GCheckout.AutoGen.NewOrderNotification();
                                                            GeneralFunctions.DeSerializeFileToObject<GCheckout.AutoGen.ChargeAmountNotification>(out N1, filepath);
                                                            if (N1 != null)
                                                            {
                                                                objGoogleClient.TestBulkOrderImport(N1, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID, ShipToPhoneNo);
                                                                //File.Copy(filepath, GeneralFunctions.GetPath(GoogleClosedOrders, DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                                //File.Delete(filepath);
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            ErrorMsg = "Error in reading Google order report file : " + Path.GetFileNameWithoutExtension(filepath);
                                                            File.Copy(filepath, GeneralFunctions.GetPath(GoogleTestOrdersFolder + "_ErrorOrderReports", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                            File.Delete(filepath);
                                                            GeneralFunctions.WriteMessage(DomainID, "ERR", ErrorMsg + ex.Message);
                                                            GeneralFunctions.WriteMessage(DomainID, "ERR", ex.StackTrace);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else if (WebApiId == (int)WebAPIList.Magento) //Magento
                                        {
                                            objMagentoClient = new MagentoClient();
                                            string sourceDir = GeneralFunctions.GetPath(MagentoTestOrdersFolder, DomainID);
                                            if (Directory.Exists(sourceDir))
                                            {
                                                string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                                                if (fileEntries.Length > 0)
                                                {
                                                    Source = CCommon.ToString(dr["vcProviderName"]);
                                                    OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                                    WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                                    BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                                    BizDocStatusId = CCommon.ToLong(dr["numBizDocStatusId"]);
                                                    RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                                    AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                                    RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                                    ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                                    //ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                                    ExpenseAccountId = 0;
                                                    DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                                    ShippingServiceItemID = CCommon.ToLong(dr["numShippingServiceItemID"]);
                                                    SalesTaxItemMappingID = CCommon.ToLong(dr["numSalesTaxItemMapping"]);
                                                    ShipToPhoneNo = CCommon.ToString(dr["vcShipToPhoneNo"]);

                                                    foreach (string filepath in fileEntries)
                                                    {
                                                        try
                                                        {
                                                            Ez.Newsletter.MagentoApi.OrderInfo OrderDetail;// = new GCheckout.AutoGen.NewOrderNotification();
                                                            GeneralFunctions.DeSerializeFileToObject<Ez.Newsletter.MagentoApi.OrderInfo>(out OrderDetail, filepath);
                                                            if (OrderDetail != null)
                                                            {
                                                                objMagentoClient.TestBulkOrderImport(OrderDetail, DomainID, WebApiId, Source, WareHouseID, RecordOwner, AssignTo, RelationshipId, ProfileId, BizDocId, BizDocStatusId, OrderStatus, ExpenseAccountId, DiscountItemMapping, ShippingServiceItemID, SalesTaxItemMappingID, ShipToPhoneNo);
                                                                //File.Copy(filepath, GeneralFunctions.GetPath(MagentoClosedOrders, DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                                //File.Delete(filepath);
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            ErrorMsg = "Error in reading Magento order report file : " + Path.GetFileNameWithoutExtension(filepath) + "." + Environment.NewLine;
                                                            File.Copy(filepath, GeneralFunctions.GetPath(MagentoTestOrdersFolder + "_ErrorOrderReports", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                                            File.Delete(filepath);
                                                            GeneralFunctions.WriteMessage(DomainID, "ERR", ErrorMsg + " " + ex.Message);
                                                            GeneralFunctions.WriteMessage(DomainID, "ERR", ex.StackTrace);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    GeneralFunctions.WriteMessage(DomainID, "ERR", ex.Message);
                                    GeneralFunctions.WriteMessage(DomainID, "ERR", ex.StackTrace);
                                }
                            }
                        }
                    }
                    else
                    {
                        string LogMessage = "Testing Bulk Order Import is not Enabled!.,";
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainID, "ERR", ex.Message);
                    GeneralFunctions.WriteMessage(DomainID, "ERR", ex.StackTrace);
                }
            }
        }

        #endregion Test Bulk Order Import
    }
}
